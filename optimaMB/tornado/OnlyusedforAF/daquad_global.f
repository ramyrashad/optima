      double precision function daquad(rule,n,f,a,b,tol,maxlev,sing,
     *                                 errest)

c     for complete documentation see aquad.
c
      implicit none
      integer levmax,iarr
      parameter(levmax=5000,iarr=levmax+1)
      integer maxlev,n,j,k,i,lev,level,nf
      integer ptr(iarr)
      double precision qright(30),xsave(30),f,dquad,
     *                 a,b,tol,sing,errest,fac,sum,corr,flag,
     *                 xl,xr,tolpu,qprev,xm,step,qleft,qnow,qdiff,temp
      double precision x(iarr),er(iarr),q(iarr),qr,ql
      double precision qrr(iarr),qll(iarr)
c      integer lsave(30)
      external rule,f
c
C      ***************
C      *
C      * STRAIGHT INTEGRATION IF MAXLEV = 0.
C      *
C      ***************
C
         if (maxlev .ne. 0) go to 4
         daquad = dquad(rule,n,f,a,b)
         return
C
C      ***************
C      *
C      * OBTAIN  THE  APPROPRIATE SCALING FACTOR  FAC  (FOR THE
C      * CONVERGENCE CRITERION AND THE ER ESTIMATE) FROM THE
C      * SUBROUTINE DEFINED BY RULE.
C      *
C      ***************
C
   4     NF = -N
         CALL RULE(NF,A,B,XSAVE,QRIGHT)
         FAC = XSAVE(1)
C
C      ***************
C      *
C      * INITIALIZATION AND CALCULATIONS FOR THE ZERO-TH LEVEL.
C      *
C      ***************
C
c   8     SUM = 0.0D0
c         CORR = 0.0D0
         ERREST = 0.0D0
         FLAG = 0.0D0
         SING = 0.0D0
c         ICOUNT = 0
c         XL = A
c         XR = B
c         TOLPU = TOL*FAC/(B - A)
c         QPREV = DQUAD(RULE,N,F,A,B)
C
C      ***************
C      *
C      * SUCCESSIVELY BISECT THE SUBINTERVALS UNTIL EITHER THE MAXIMUM
C      * ALLOWED LEVEL IS REACHED OR THE sum of the estimated errors of all
c      * the intervals is less than the predetermined tolerance.
C      *
C      ***************
C
c
c      **********************************************************************
c        - Lev    =>  points to the interval with the largest estimated
c                     error.
c        - Level  =>  counter determining number of intervals being used.
c
c        - q(i)   =>  value of integral for each interval i
c        - er(i)  =>  estimated error associated with each interval i
c        - x(i)   =>  stores location of start of interval i
c        - ptr(i) =>  array used to build and maintain a heap pointing to  
c                     elements in er(i) array  
c                     i.e.   ptr(1) points to interval i which has the
c                            largest error associated with it.
c
c        - qll(i) =>  stores value of integral for left half of interval i 
c        - qrr(i) =>  stores value of integral for right half of interval i 
c      **********************************************************************
c
         LEV = 1
         LEVEL = 1
c
c        - calculate the integral on the original interval spanning x=[a,b]
         x(1)=a
         x(2)=b
         i=1
         xl=x(i)
         xr=x(i+1)
         xm=.5*(xl+xr)
         qprev=dquad(rule,n,f,xl,xr) 
         ql=dquad(rule,n,f,xl,xm)
         qr=dquad(rule,n,f,xm,xr)
         qll(i)=ql
         qrr(i)=qr
         q(i)=ql+qr
         er(i)=q(i)-qprev
c
         ptr(1)=1
c
c
c        - test to see if estimated error is less than tolerance 
         if (dabs(er(i)).le.tol*fac) then
            daquad= q(i) + er(i)/fac
            errest=dabs(er(i))/fac
            maxlev=level
            tol=0
            return
         else
c           - split the only interval in half
            level=level+1
            x(i+2)=x(i+1)
            x(i+1)=xm
         endif
c
c        - compute the integral for two halves and their associated error
c         k=0
 2       do 10 i=lev,lev+1
c            k=k+1
c            print *,6*k+6
            xl=x(i)
            xr=x(i+1)
            xm=.5*(xl+xr)
            if (i.eq.lev) then
               qprev=qll(lev)
            else
               qprev=qrr(lev)
               qll(lev)=ql
               qrr(lev)=qr
            endif
c            qprev=dquad(rule,n,f,xl,xr) 
            ql=dquad(rule,n,f,xl,xm)
            qr=dquad(rule,n,f,xm,xr)
            if (i.eq.lev+1) then
               qll(i)=ql
               qrr(i)=qr
            endif
c               
            q(i)=ql+qr
            er(i)=q(i)-qprev
c            write(6,3) i,x(i),x(i+1),q(i)
c 3          format(i4,3e15.5)
 10      continue
c
         sum=0.
         do 15 j=1,level
            sum=sum + dabs(er(j))
c            write(6,3) j,x(j),x(j+1),q(j),er(j)/fac
c 3          format(i4,4e15.5)            
 15      continue
c
c        - test to see if global error is less than tolerance 
         if (sum.le.tol*fac) then
            daquad=0.
            errest=0.
            do 20 j=1,level
               daquad=daquad+(q(j)+er(j)/fac)
               errest=errest+dabs(er(j))
 20         continue
            errest=errest/fac
            maxlev=level
            tol=0.d0
            return
         else
c           - add current local error estimate of the two subintervals
c             to the heap and heapify
            ptr(level-1)=lev
            ptr(level)=lev+1
            call heap_add(iarr,level-1,ptr,er)
            call heap_add(iarr,level,ptr,er)
            lev=ptr(1)
c
c           - delete top node of tree and heapify to make room for
c             two new intervals 
            ptr(1)=ptr(level)
            ptr(level)=0
            call heapify(iarr,level-1,ptr,er)
c
c           - shift the pointers over to allow for the added interval
c             next sweep
            do 19 j=1,level-1
               if (ptr(j).ge.lev+1) ptr(j)=ptr(j)+1
 19         continue
c
            level=level+1
            if (level.gt.maxlev) then
               daquad=0.
               do 21 j=1,level
                  daquad=daquad+(q(j)+er(j)/fac)
 21            continue
               maxlev=level
               sing=xm
               flag=1
               return
            endif
c
c           - split interval with largest error in half
c           - shift the array elements over to allow for the
c             added interval next sweep
            do 25 i=level+1,lev+2,-1
               x(i)=x(i-1)
               er(i)=er(i-1)
               q(i)=q(i-1)
               qrr(i)=qrr(i-1)
               qll(i)=qll(i-1)
 25         continue
            x(lev+1)=.5*(x(lev) + x(lev+2))
            goto 2
         endif
      END
