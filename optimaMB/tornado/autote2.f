c *******************************************************************
c ** subroutine to set the (sharp) trailing edge point copies      **
c *******************************************************************
c calling routine: ioall
c
c
c
      subroutine autote2(mglev)

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer nbs,ns,ii,iblkup,iblkdn,jblock,iblock,iteflag,mglev
c
c C-MESH TOPOLOGY : 
c
      if (.not.viscous .and. .not.sngvalte) then
        nteavg=0
        goto 777
      endif
c
      if (mg .or. gseq) then
        nbs=nblkstot
        ns=nstgtot
      else
        nbs=nblks
        ns=nstg
      endif

c      -original
c      if (nblks.eq.3) then
c        nteavg = 1
c        ibteavg1(1) = 3
c        jteavg1(1) = nhalo + 1
c        kteavg1(1) = nhalo + 1
c        ibteavg2(1) = 1
c        jteavg2(1) = nhalo + jbmax(1)
c        kteavg2(1) = nhalo + 1
c      endif

      if (nblks.eq.3) then
        nteavg = mglev
        do ii=1,mglev
           ibteavg1(ii) = ii*nblks
           jteavg1(ii) = 1
           kteavg1(ii) = 1
           ibteavg2(ii) = 1 +(ii-1)*nblks
           jteavg2(ii) = jbmax(1 + (ii-1)*nblks)
           kteavg2(ii) = 1
        enddo
      endif
c
c H-MESH TOPOLOGY :
c
      if (nblks.gt.4) then
        nteavg = 0
        do 100 ii=1,ns
c
c ensure that the stagnation point is a trailing edge
c
          if (istgtyp(ii).eq.-1) then
            iteflag = 0
            iblock=istgblk(ii)
            iblock = lblkpt(iblock,4)
c
c stagnation point is the LR corner
c
            if (istgcnr(ii).eq.1) then
              jblock = lblkpt(iblock,1)
c t.e. is sharp only!
              if (ibctype(jblock,2).ne.1) then
                iteflag = 1
                iblkup = iblock
                iblkdn = jblock
              endif
            elseif (istgcnr(ii).eq.4) then
              jblock = lblkpt(iblock,3)
              if (ibctype(jblock,2).ne.1) then
                iteflag = 1
                iblkup = jblock
                iblkdn = iblock
              endif
            endif
c
            if (iteflag.eq.1) then
              nteavg = nteavg + 1
              ibteavg1(nteavg) = iblkup
              jteavg1(nteavg) = 1
              kteavg1(nteavg) = 1
              ibteavg2(nteavg) = iblkdn
              jteavg2(nteavg) = 1
              kteavg2(nteavg) = kbmax(iblkdn)
            endif
          endif
  100   continue
      endif
c
 777  continue
      return
      end
