# /****************************************************************
# Copyright (C) 2001 Lucent Technologies
# All Rights Reserved
#
# Permission to use, copy, modify, and distribute this software and
# its documentation for any purpose and without fee is hereby
# granted, provided that the above copyright notice appear in all
# copies and that both that the copyright notice and this
# permission notice and warranty disclaimer appear in supporting
# documentation, and that the name of Lucent or any of its entities
# not be used in advertising or publicity pertaining to
# distribution of the software without specific, written prior
# permission.
#
# LUCENT DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL LUCENT OR ANY OF ITS ENTITIES BE LIABLE FOR ANY
# SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
# ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
# THIS SOFTWARE.
# ****************************************************************/

# For making libsnopt.a libsnprint.a and libblas.a with SNOPT source
# converted to C by f2c.
# Libraries will be cross-compiled to generate snopt.exe for Windows.

# Invoke with "make -f makefile.u" (or copy to "makefile", edit
# if necessary, and invoke "make").

#CC = cc
CC      = i686-pc-mingw32-gcc
AR      = i686-pc-mingw32-ar
RANLIB  = i686-pc-mingw32-ranlib
ARFLAGS = ruv

S = ../..
# Assumes solvers directory is ../..
# -- e.g., that this directory is solvers/snopt/src.
# The .c files are assumed to come from f2c applied to
# the SNOPT source.  $S is assumed to have f2c.h.

CFLAGS = -O  -I$S

.c.o:
	$(CC) -c $(CFLAGS) $*.c

o = \
	sn02lib.c  \
	sn05wrpa.c \
	sn05wrpb.c \
	sn05wrpc.c \
	sn05wrpn.c \
	sn10mach.c \
	sn12ampl.c \
	sn17util.c \
	sn20amat.c \
	sn25bfac.c \
	sn27lu.c   \
	sn30spec.c \
	sn35mps.c  \
	sn37wrap.c \
	sn40bfil.c \
	sn50lp.c   \
	sn55qp.c   \
	sn56qncg.c \
	sn57qopt.c \
	sn60srch.c \
	sn65rmod.c \
	sn70nobj.c \
	sn80ncon.c \
	sn85hess.c \
	sn87sopt.c \
	sn90lmqn.c \
	sn95fmqn.c \
	np02lib.c  \
	sq02lib.c  \
	snoptq.c   \
	sqopt.c    \
	snopta.c   \
	snoptb.c   \
	snoptc.c   \
	npopt.c

all: libsnopt.a libsnprint.a libblas.a

libsnopt.a: $o
	$(CC) -c $(CFLAGS) $?
	x=`echo $? | sed 's/\.c/.o/g'` && $(AR) $(ARFLAGS) libsnopt.a $$x && rm $$x
	$(RANLIB) libsnopt.a || true

libsnprint.a: sn03prnt.c
	$(CC) -c $(CFLAGS) sn03prnt.c
	$(AR) $(ARFLAGS) libsnprint.a sn03prnt.o
	rm sn03prnt.o
	$(RANLIB) libsnprint.a || true

libblas.a: sn15blas.c
	$(CC) -c $(CFLAGS) sn15blas.c
	$(AR)  $(ARFLAGS) libblas.a sn15blas.o
	rm sn15blas.o
	$(RANLIB) libblas.a || true
