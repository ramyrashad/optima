      subroutine fixteavg1(jmax,kmax,q,xyj,j1,k1,j2,k2)
#include "../include/common.inc"
c
      dimension q(jmax,kmax,4), xyj(jmax,kmax)
c
      rr1 = 1./xyj(j1,k1)
      rr2 = 1./xyj(j2,k2)
c
      qsav1 = 0.5*(q(j1,k1,1)*xyj(j1,k1)+q(j2,k2,1)*xyj(j2,k2))
      qsav2 = 0.5*(q(j1,k1,2)*xyj(j1,k1)+q(j2,k2,2)*xyj(j2,k2))
      qsav3 = 0.5*(q(j1,k1,3)*xyj(j1,k1)+q(j2,k2,3)*xyj(j2,k2))
c
      ppp1 = gami*(q(j1,k1,4)-0.5*
     &          (q(j1,k1,2)**2+q(j1,k1,3)**2)/q(j1,k1,1))*xyj(j1,k1)
      ppp2 = gami*(q(j2,k2,4)-0.5*
     *          (q(j2,k2,2)**2+q(j2,k2,3)**2)/q(j2,k2,1))*xyj(j2,k2)
      psav = 0.5*(ppp1+ppp2)

      q(j1,k1,1) = qsav1*rr1
      q(j2,k2,1) = qsav1*rr2
c
      q(j1,k1,2) = qsav2*rr1
      q(j2,k2,2) = qsav2*rr2
c
      q(j1,k1,3) = qsav3*rr1
      q(j2,k2,3) = qsav3*rr2
c
      q(j1,k1,4) = psav/gami*rr1 +
     *          0.5*(q(j1,k1,2)**2+q(j1,k1,3)**2)/q(j1,k1,1)
      q(j2,k2,4) = psav/gami*rr2 +
     *          0.5*(q(j2,k2,2)**2+q(j2,k2,3)**2)/q(j2,k2,1)
      return
      end
