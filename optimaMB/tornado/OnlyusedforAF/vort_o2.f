      subroutine vort_o2(iblk,jmax,kmax,jvort1,jvort2,
     &                   jlow,jup,klow,kup,kbegin,kend,
     &                   bnd,workq,xy,vorticity)
c     calling subroutine: spalart_allmaras
c
#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/trip.inc"
c
      logical bnd
      integer jvort1,jvort2,incr
      dimension workq(jmax,kmax,3),vorticity(jmax,kmax)
      dimension bnd(4),xy(jmax,kmax,4)

      do k=klow,kup
         kp1=k+1
         km1=k-1
         do j=jlow,jup
           jp1=j+1
           jm1=j-1
           ujm1=workq(jm1,k,2)
           ujp1=workq(jp1,k,2)
           ukm1=workq(j,km1,2)
           ukp1=workq(j,kp1,2)
           vjm1=workq(jm1,k,3)
           vjp1=workq(jp1,k,3)
           vkm1=workq(j,km1,3)
           vkp1=workq(j,kp1,3)
c
           vorticity(j,k)=max(0.5d0*abs(
     &            (vjp1-vjm1)*xy(j,k,1)+(vkp1-vkm1)*xy(j,k,3)
     &           -(ujp1-ujm1)*xy(j,k,2)-(ukp1-ukm1)*xy(j,k,4)),
     &            8.5d-10)
         enddo
      enddo

      if (bnd(1)  .and. trip_present(iblk)) then
c       -we only need vorticity on airfoil surface
c        at transition points
        k=kbegin
        kp1=k+1
        kp2=k+2
        incr=max(jvort2-jvort1,1)
        do j=jvort1,jvort2,incr
          jp1=j+1
          jm1=j-1
          ujk=workq(j,k,2)
          ujm1=workq(jm1,k,2)
          ujp1=workq(jp1,k,2)
          ukp1=workq(j,kp1,2)
          ukp2=workq(j,kp2,2)
          vjk=workq(j,k,3)
          vjm1=workq(jm1,k,3)
          vjp1=workq(jp1,k,3)
          vkp1=workq(j,kp1,3)
          vkp2=workq(j,kp2,3)
c       
          vorticity(j,k)=max(0.5d0*abs(
     &      (vjp1-vjm1)*xy(j,k,1)+(-3.d0*vjk+4.d0*vkp1-vkp2)*xy(j,k,3)
     &     -(ujp1-ujm1)*xy(j,k,2)-(-3.d0*ujk+4.d0*ukp1-ukp2)*xy(j,k,4)),
     &      8.5d-10)
c          write(98,*) j-3+33,vjk,vkp1,vkp2
c          write(98,*) j-3+33,vorticity(j,k)
        enddo
      endif
c
      if (bnd(3) .and. trip_present(iblk)) then
        k=kend
        km1=k-1
        km2=k-2
        incr=max(jvort2-jvort1,1)
        do j=jvort1,jvort2,incr
c        do j=jvort1,jvort2,jvort2-jvort1
           jp1=j+1
           jm1=j-1
           ujk=workq(j,k,2)
           ujm1=workq(jm1,k,2)
           ujp1=workq(jp1,k,2)
           ukm1=workq(j,km1,2)
           ukm2=workq(j,km2,2)
           vjk=workq(j,k,3)
           vjm1=workq(jm1,k,3)
           vjp1=workq(jp1,k,3)
           vkm1=workq(j,km1,3)
           vkm2=workq(j,km2,3)
c       
c           vorticity(j,k)=max(-0.5d0*abs(
c     &      (vjp1-vjm1)*xy(j,k,1)+(-3.d0*vjk+4.d0*vkm1-vkm2)*xy(j,k,3)
c     &     -(ujp1-ujm1)*xy(j,k,2)-(-3.d0*ujk+4.d0*ukm1-ukm2)*xy(j,k,4)),
c     &      8.5d-10)
           vorticity(j,k)=max(0.5d0*abs(
     &      (vjp1-vjm1)*xy(j,k,1)+(3.d0*vjk-4.d0*vkm1+vkm2)*xy(j,k,3)
     &     -(ujp1-ujm1)*xy(j,k,2)-(3.d0*ujk-4.d0*ukm1+ukm2)*xy(j,k,4)),
     &      8.5d-10)
        enddo
      endif
c
      return
      end
