c #######################
c ##                   ##
c ##  subroutine rhsx  ##
c ##                   ##
c #######################
c
      subroutine rhsx(jdim,kdim,q,s,s0,press,xy,xyj,xit,flux)
c
      use disscon_vars
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),press(jdim,kdim),s(jdim,kdim,4)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),xit(jdim,kdim)
      dimension flux(jdim,kdim,4),s0(jdim,kdim,4)
c
c
c---- compute flux vectors ----
c     : r0 is d_xi/dt
c     : r1, r2 are either d_xi/dx, d_xi/dy
c     : rr is j/rho
c     : qs is either cap-u or cap-v (contravariant velocities)
c     : pressj is pressure / j
      do 200 k =klow,kup
      do 200 j =jbegin,jend
         r0 = xit(j,k)
         r1 = xy(j,k,1)
         r2 = xy(j,k,2)
         rr = 1.d0/q(j,k,1)
         u  = q(j,k,2)*rr
         v  = q(j,k,3)*rr
         qs = r0 +r1*u + r2*v
         pressj = press(j,k)
           flux(j,k,1) = q(j,k,1)*qs
           flux(j,k,2) = q(j,k,2)*qs + r1*pressj
           flux(j,k,3) = q(j,k,3)*qs + r2*pressj
           flux(j,k,4) = qs*(q(j,k,4)+pressj) - r0*pressj
c           write(72,72) j,k,flux(j,k,3),flux(j,k,4)
c           s(j,k,3)=0.d0
c           s(j,k,4)=0.d0
 200  continue
c
c
c---- central differencing used in xi ----
      if (iord.eq.4) then  !-- Use 4th order centered differences
         r0 = -dt/(12.d0*(1.d0+phidt))
         r02= 2.d0*r0
         if (.not.periodic) then
            do 300 n =1,4
            do 300 k =klow,kup
            do 300 j =jlow+1,jup-1
              s(j,k,n) = s(j,k,n) + r0*(flux(j-2,k,n) +
     >              8.d0*(-flux(j-1,k,n)+flux(j+1,k,n))-flux(j+2,k,n))
 300        continue
c
c            rt0 = -.5*dt / (1.+phidt)
            do 301 n =1,4
            do 301 k =klow,kup
               j=jlow
c               s(j,k,n) = s(j,k,n) + rt0*( flux(j+1,k,n) - flux(j-1,k,n))
c              -third order
               s(j,k,n) =s(j,k,n) + r02*(-2.d0*flux(j-1,k,n) -
     >            3.d0*flux(j,k,n) + 6.d0*flux(j+1,k,n) - flux(j+2,k,n))
c               s(j,k,n) =s(j,k,n) +
c     >                      rt0*(-5.d0*flux(j-1,k,n)-2.d0*flux(j,k,n)
c     >                           +6.d0*flux(j+1,k,n)+2.d0*flux(j+2,k,n)
c     >                           -flux(j+3,k,n))
c
               j=jup
c               s(j,k,n) = s(j,k,n) + rt0*( flux(j+1,k,n) - flux(j-1,k,n))
c              -third order
               s(j,k,n) =s(j,k,n) + r02*(flux(j-2,k,n) - 6.d0*
     >            flux(j-1,k,n) + 3.d0*flux(j,k,n) + 2.d0*flux(j+1,k,n))
c               s(j,k,n) =s(j,k,n) +
c     >                      rt0*(flux(j-3,k,n)-2.d0*flux(j-2,k,n)
c     >                           -6.d0*flux(j-1,k,n)+2.d0*flux(j,k,n)
c     >                           +5.d0*flux(j+1,k,n))
 301        continue
            if (flbud .and.
     &       (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
              n=2
              r0 = -dt/(12.d0*(1.d0+phidt))
              r02= 2.d0*r0
              do 500 k =klow,kup
              do 500 j =jlow+1,jup-1
                budget(j,k,1) = r0*(flux(j-2,k,n) +
     >             8.d0*(-flux(j-1,k,n)+flux(j+1,k,n))-flux(j+2,k,n))
 500          continue
c
              do 501 k =klow,kup
                j=jlow
                budget(j,k,1) =r02*(-2.d0*flux(j-1,k,n) -
     >            3.d0*flux(j,k,n) + 6.d0*flux(j+1,k,n) - flux(j+2,k,n))
c
                j=jup
                budget(j,k,1) =r02*(flux(j-2,k,n) - 6.d0*
     >            flux(j-1,k,n) + 3.d0*flux(j,k,n) + 2.d0*flux(j+1,k,n))
 501          continue
            endif
         else
            do 305 n =1,4
            do 305 k =klow,kup
            do 305 j =jlow,jup
               jp1=jplus(j)
               jp2=jplus(jp1)
               jm1=jminus(j)
               jm2=jminus(jm1)
               s(j,k,n) = s(j,k,n) + r0*(flux(jm2,k,n) +
     >                8.d0*(-flux(jm1,k,n)+flux(jp1,k,n))-flux(jp2,k,n))
 305        continue
         endif
c
c         if (.not.cmesh) call fixmet(jdim,kdim,1,s,flux,xy,xyj)

      else !-- Use 2nd order centered differences (Default)
         rt0 = -.5d0*dt / (1.d0+phidt)        
         if (dissCon) then
            do 400 n =1,4
            do 400 k =klow,kup
            do 400 j =jlow,jup
              jp1=jplus(j)
              jm1=jminus(j)
              s0(j,k,n) = s0(j,k,n) 
     &            + rt0*( flux(jp1,k,n) - flux(jm1,k,n))
              s(j,k,n) = s(j,k,n) 
     &            + rt0*( flux(jp1,k,n) - flux(jm1,k,n))
 400        continue
         else
            do 402 n =1,4
            do 402 k =klow,kup
            do 402 j =jlow,jup
              jp1=jplus(j)
              jm1=jminus(j)
              s(j,k,n) = s(j,k,n) + rt0*( flux(jp1,k,n) - flux(jm1,k,n))
 402        continue
         end if

c
         if (flbud .and.
     &      (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
            n=2
            do 401 k =klow,kup
            do 401 j =jlow,jup
               jp1=jplus(j)
               jm1=jminus(j)
               budget(j,k,1) = rt0*(flux(jp1,k,n) - flux(jm1,k,n))
 401        continue
         endif
      endif
c
      return
      end
