c #######################
c ##                   ##
c ##  subroutine rhsy  ##
c ##                   ##
c #######################
c
      subroutine rhsy (jdim,kdim,q,s,s0,press,xy,xyj,ett,flux)
c
      use disscon_vars
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),press(jdim,kdim),s(jdim,kdim,4)
      dimension s0(jdim,kdim,4)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),ett(jdim,kdim)
      dimension flux(jdim,kdim,4)
c
c
c---- compute flux vectors ----
c     : r0 is d_eta/dt
c     : r1, r2 are d_eta/dx, d_eta/dy
c     : rr is j/rho
c     : qs is cap-v ( contravariant velocity )
c     : pressj is pressure / j
      do 20 k =kbegin,kend
      do 20 j =jlow,jup
         r1 = xy(j,k,3)
         r2 = xy(j,k,4)
         r0 = ett(j,k)
         rr = 1.d0/q(j,k,1)
         u  = q(j,k,2)*rr
         v  = q(j,k,3)*rr
         qs = r0 +r1*u + r2*v
         pressj = press(j,k)
            flux(j,k,1)= q(j,k,1)*qs
            flux(j,k,2)= q(j,k,2)*qs + r1*pressj
            flux(j,k,3)= q(j,k,3)*qs + r2*pressj
            flux(j,k,4) = qs*( q(j,k,4) + pressj) - pressj*r0
c            s(j,k,3)=0.d0
c            s(j,k,4)=0.d0
 20   continue
c
c
c---- central differencing used in eta direction ----
      if (iord.eq.4) then
         r0 = -dt/(12.d0*(1.d0+phidt))
         r02= 2.d0*r0
         do 30 n =1,4
         do 30 k =klow+1,kup-1
         do 30 j =jlow,jup
            s(j,k,n) = s(j,k,n) + r0*(flux(j,k-2,n)
     >            +8.d0*(-flux(j,k-1,n)+flux(j,k+1,n))-flux(j,k+2,n))
 30      continue
c
c         r0 = -.5d0*dt / (1.d0+phidt)
         k=klow
         do 31 n=1,4
         do 31 j=jlow,jup
c         s(j,k,n) = s(j,k,n) + r0*( flux(j,k+1,n) - flux(j,k-1,n))
c        -third order
            s(j,k,n) =s(j,k,n) + r02*(-2.d0*flux(j,k-1,n)
     >            -3.d0*flux(j,k,n)+6.d0*flux(j,k+1,n)-flux(j,k+2,n))
c            s(j,k,n) =s(j,k,n) +
c     >                  r0*(-5.d0*flux(j,k-1,n)-2.d0*flux(j,k,n)
c     >                      +6.d0*flux(j,k+1,n)+2.d0*flux(j,k+2,n)
c     >                      -flux(j,k+3,n))
 31      continue
c
c         r0 = -dt/(12.d0*(1.d0+phidt))
         k=kup
         do 32 n=1,4
         do 32 j=jlow,jup
c         s(j,k,n) = s(j,k,n) + r0*( flux(j,k+1,n) - flux(j,k-1,n))
c        -third order
         s(j,k,n) =s(j,k,n) + r02*(flux(j,k-2,n)
     >          -6.d0*flux(j,k-1,n)+3.d0*flux(j,k,n)+2.d0*flux(j,k+1,n))
c            s(j,k,n) =s(j,k,n) +
c     >                  r0*(flux(j,k-3,n)-2.d0*flux(j,k-2,n)
c     >                      -6.d0*flux(j,k-1,n)+2.d0*flux(j,k,n)
c     >                      +5.d0*flux(j,k+1,n))
 32      continue
c
c
         if (flbud .and.
     &      (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
            r0 = -dt/(12.d0*(1.d0+phidt))
            r02= 2.d0*r0
            n=2
            do 50 k =klow+1,kup-1
            do 50 j =jlow,jup                                          	
               budget(j,k,1) = budget(j,k,1) + r0*(-flux(j,k+2,n)
     >            +8.d0*(flux(j,k+1,n)-flux(j,k-1,n))+flux(j,k-2,n))
 50         continue
c
            k=klow
            do 51 j=jlow,jup
               budget(j,k,1)=budget(j,k,1) + r02*(-2.d0*flux(j,k-1,n)
     >            -3.d0*flux(j,k,n)+6.d0*flux(j,k+1,n)-flux(j,k+2,n))
 51         continue
c
            k=kup
            do 52 j=jlow,jup
               budget(j,k,1) =budget(j,k,1) + r02*(flux(j,k-2,n)
     >         -6.d0*flux(j,k-1,n)+3.d0*flux(j,k,n)+2.d0*flux(j,k+1,n))
 52         continue
         endif
      else
         r0 = -.5d0*dt / (1.d0+phidt)
         if (dissCon) then
            do 40 n =1,4
            do 40 k =klow,kup
            do 40 j =jlow,jup
               s0(j,k,n) = s0(j,k,n) 
     &              + r0*( flux(j,k+1,n) - flux(j,k-1,n))
               s(j,k,n) = s(j,k,n)
     &              + r0*( flux(j,k+1,n) - flux(j,k-1,n))
 40         continue
         else
            do 42 n =1,4
            do 42 k =klow,kup
            do 42 j =jlow,jup
               s(j,k,n) = s(j,k,n) + r0*( flux(j,k+1,n) - flux(j,k-1,n))
 42         continue
         end if
c
         if (flbud .and.
     &      (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
            n=2
            do 41 k =klow,kup
            do 41 j =jlow,jup
              budget(j,k,1) = budget(j,k,1) +
     &                        r0*(flux(j,k+1,n) - flux(j,k-1,n))
 41         continue
         endif
      endif
c
c      do k=klow,kup
c      do j=jlow,jup
c         write(72,72) j,k,s(j,k,3),s(j,k,4)
c      enddo
c      enddo
c 72   format(2I4,2e25.12)
      return
      end
