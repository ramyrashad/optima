      subroutine subit(jdim,kdim,qp,q,qold,s,akk)
c                                                                      
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4)
      dimension qold(jdim,kdim,4),qp(jdim,kdim,4)      
      double precision akk
c
      if (jesdirk.eq.4 .or. jesdirk.eq.3 )then 
         if(jsubit.gt.0)then
            do 500 n=1,4
            do 500 k=klow,kup
            do 500 j=jlow,jup
               s(j,k,n)=-dt*(qp(j,k,n)-q(j,k,n))/(akk*dt2)
 500        continue
         endif
      else
        if (ismodel .eq. 1) then
           do 600 n = 1,4
           do 600 k = klow,kup
           do 600 j = jlow,jup
              s(j,k,n) =-(qp(j,k,n) + (-4.*q(j,k,n) + qold(j,k,n))
     &             /3.d0)
 600       continue
        else
           do 700 n = 1,4
           do 700 k = klow,kup
           do 700 j = jlow,jup
              s(j,k,n) =-dt*(3.*qp(j,k,n)-4.*q(j,k,n)+qold(j,k,n))
     &             /(2.*dt2)
 700       continue
        endif
      endif

      return
      end                                                       
                                                       
