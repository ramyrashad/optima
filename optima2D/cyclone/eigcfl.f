      subroutine eigcfl(jdim,kdim,q,sndsp,xy,ds,cfldt,precon) 
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),sndsp(jdim,kdim)                         
      dimension xy(jdim,kdim,4),ds(jdim,kdim)        
c                                                                       
      dimension cfldt(jdim,kdim),precon(jdim,kdim,6)                 
c                                                                       
c       computes an the maximum an minimum courant number           
c       over the whole grid.
c                                                                       
c                                                                       
      if (unsted .and. ismodel.gt.1) then
         dtd=dt2
      else
         dtd=dt
      endif
c
      if (prec.eq.0) then
         do 100 k=kbegin,kend                                          
         do 100 j=jbegin,jend                                          
c           ri is j/rho                                                
            ri = 1./q(j,k,1)                                           
            u = q(j,k,2)*ri                                             
            v = q(j,k,3)*ri                                             
c                                                                       
c                                                                       
c                                                                       
c    note xit and ett not in calculation                             
c                                                                    
c                                                                     
            sigab = abs(u*xy(j,k,1)+v*xy(j,k,2))                       
     *            + abs(u*xy(j,k,3)+v*xy(j,k,4))                       
     *            + sndsp(j,k)*sqrt(xy(j,k,1)**2 + xy(j,k,2)**2         
     *            +                  xy(j,k,3)**2 + xy(j,k,4)**2)       
c                                                                     
            cfldt(j,k) = dtd*ds(j,k)*sigab                             
c                                                                     
  100    continue                                                  
      else
         do 101 k=kbegin,kend
         do 101 j=jbegin,jend
            sigab=abs(precon(j,k,3))+
     +           abs(precon(j,k,5))+
     +           precon(j,k,4)+precon(j,k,6)
            cfldt(j,k)=dtd*ds(j,k)*sigab
 101     continue
      endif
c                                                                       
c                                                                       
c  compute max cfl                                                      
c                                                                       
         jmx = jbegin                                                   
         kmx = kbegin                                                   
         abscfl = 0.d0
         absmin = 1.d9
         do 15 k=klow,kup                                               
            do 12 j = jlow, jup                                         
               abss = abs( cfldt( j, k) )                               
               if ( abss .le. abscfl ) then
                   if (abss .lt. absmin) then
                      absmin = abss
                      jmn = j
                      kmn = k
                   endif
               else
                   abscfl = abss                                         
                   jmx = j
                   kmx = k
               endif                                                           
 12         continue
 15      continue         
c                                                                       
c  output max cfl and location                                          
c                                                                       
         write (out_unit,*) '---numiter=',numiter,
     &         '-------------------------------'  
      write (out_unit,90) abscfl,jmx,kmx
      write (out_unit,91) dtd*ds(jmx,kmx)
      write (out_unit,*)
     &      '|                                                    |'
      write (out_unit,92) absmin,jmn,kmn
      write (out_unit,91) dtd*ds(jmn,kmn)
      write (out_unit,*)
     &      '------------------------------------------------------'  
 90   format(14h |  max cfl = ,e13.6,12h   at j,k = ,2I5,5x,1h|)
 91   format(14h |  dt used = ,e13.6,27x,1h|)
 92   format(14h |  min cfl = ,e13.6,12h   at j,k = ,2I5,5x,1h|)
c                                                                       
      if(periodic .and. jmax.ne.jmaxold)then                            
c        special logic for overlap o grid case                                
c        set jmaxold point from j = 1 point                                   
c                                                                       
         do 10 k = 1,kmax                                                  
            cfldt(jmaxold,k) = cfldt(1,k)                                     
10       continue                                                          
      endif                                                             
                                                                       
      return                                                            
      end                                                               
