c     ------------------------------------------------------------------
c     -- differentiation of production and destruction terms for the S-A model -- 
c     -- m. nemec, oct. 2001 --
c     modified by Markus Rumpfkeil
c     date: June 2006
c     ------------------------------------------------------------------
      subroutine dsa_pd(ib, jmax, kmax, jbegin, jend, jlow, jup, kbegin,
     &     kend, klow, kup, q, turre, xy, xyj, bm2, bm1, db, bp1,
     &     bp2, chi, dchi,fv1, dfv1, vort, smin, re)

      implicit none
      
#include "../include/parms.inc"
#include "../include/sam.inc"
#include "../include/mbcom.inc"

      integer ib,jmax,kmax,jbegin,jend,jlow,jup,kbegin,kend,klow,kup
      integer j,k,jp1,jm1,kp1,km1,kp,jp,n,jm,km
      double precision ujm1,ujp1,ukm1,ukp1,vjm1,vjp1,vkm1,vkp1,e_dfw,ri
 
      double precision q(jmax,kmax,4), xy(jmax,kmax,4), xyj(jmax,kmax)
      double precision chi(jmax,kmax), dchi(5,jmax,kmax)
      double precision fv1(jmax,kmax), dfv1(jmax,kmax), vort(jmax,kmax)

      double precision bm2(5,jmax,kmax), bm1(5,jmax,kmax),fv3(jmax,kmax)
      double precision db(5,jmax,kmax),  bp1(5,jmax,kmax)
      double precision bp2(5,jmax,kmax),turre(jmax,kmax),smin(jmax,kmax)

c     -- local arrays --
      double precision wk1(maxjb,maxkb,3),wk2(maxjb,maxkb,3)
      double precision dprod(5,maxjb,maxkb),  ddest(5,maxjb,maxkb) 
      double precision dsxip(5,maxjb,maxkb),  dsxim(5,maxjb,maxkb)
      double precision dsetap(5,maxjb,maxkb), dsetam(5,maxjb,maxkb)

      double precision dchifv1(5),dfv2(5),dfv3(5),dts(5),dr(5),dg(5)
      double precision dfw(5),wk3(maxjb,maxkb),const,r,fw,g,ts2,g6,rinv
      double precision cw,cb,tnu,t1,fv2,ts,re

      const = (1.0d0+cw3_6)**expon

c     -- calculate vorticity without absolute value and cut-off --
      do k = klow,kup
         kp1 = k+1
         km1 = k-1
         do j = jlow,jup
            jp1 = j+1
            jm1 = j-1
            ujm1 = q(jm1,k,2)/q(jm1,k,1)
            ujp1 = q(jp1,k,2)/q(jp1,k,1)
            ukm1 = q(j,km1,2)/q(j,km1,1)
            ukp1 = q(j,kp1,2)/q(j,kp1,1)
            vjm1 = q(jm1,k,3)/q(jm1,k,1)
            vjp1 = q(jp1,k,3)/q(jp1,k,1)
            vkm1 = q(j,km1,3)/q(j,km1,1)
            vkp1 = q(j,kp1,3)/q(j,kp1,1)

            vort(j,k) = 0.5d0*( (vjp1-vjm1)*xy(j,k,1) + (vkp1-vkm1)
     &           *xy(j,k,3) - (ujp1-ujm1)*xy(j,k,2) - (ukp1-ukm1)
     &           *xy(j,k,4) )
         end do
      end do

c     -- off - diagonal entries only from vorticity --
c     -- wk1 holds d(u)/d(Q^) --
c     -- wk2 holds d(v)/d(Q^) --
      do k = kbegin,kend
         do j = jbegin,jend     
            
            ri = 1.0d0/q(j,k,1)
            
            wk1(j,k,1) = - 0.5d0*q(j,k,2)*ri**2
            wk1(j,k,2) = 0.5d0*ri
            wk1(j,k,3) = 0.0d0
   
            wk2(j,k,1) = - 0.5d0*q(j,k,3)*ri**2
            wk2(j,k,2) = 0.0d0
            wk2(j,k,3) = 0.5d0*ri
         end do
      end do

c     -- d(S)/d(Q^) --
c     -- special treatment due to absolute value and constant in
c     vorticity expression -- 
      do k = klow,kup
         kp1 = k+1
         km1 = k-1
         do j = jlow,jup 
            if ( dabs(vort(j,k)) .le. 8.5d-10 ) then
               do n = 1,3
                  dsxip(n,j,k) = 0.d0
                  dsxim(n,j,k) = 0.d0
                  dsetap(n,j,k)= 0.d0
                  dsetam(n,j,k)= 0.d0
               end do
            else if ( vort(j,k) .lt. - 8.5d-10 ) then
               jp1 = j+1
               jm1 = j-1
               do n = 1,3
                  dsxip(n,j,k) =-wk2(jp1,k,n)*xy(j,k,1)+
     &                 wk1(jp1,k,n)*xy(j,k,2)
                  dsxim(n,j,k) = wk2(jm1,k,n)*xy(j,k,1)-
     &                 wk1(jm1,k,n)*xy(j,k,2)
                  dsetap(n,j,k)=-wk2(j,kp1,n)*xy(j,k,3)+
     &                 wk1(j,kp1,n)*xy(j,k,4)
                  dsetam(n,j,k)= wk2(j,km1,n)*xy(j,k,3)-
     &                 wk1(j,km1,n)*xy(j,k,4)                   
               end do
            else        
               jp1 = j+1
               jm1 = j-1
               do n = 1,3
                  dsxip(n,j,k) = wk2(jp1,k,n)*xy(j,k,1)-
     &                 wk1(jp1,k,n)*xy(j,k,2)
                  dsxim(n,j,k) =-wk2(jm1,k,n)*xy(j,k,1)+
     &                 wk1(jm1,k,n)*xy(j,k,2)
                  dsetap(n,j,k)= wk2(j,kp1,n)*xy(j,k,3)-
     &                 wk1(j,kp1,n)*xy(j,k,4)
                  dsetam(n,j,k)=-wk2(j,km1,n)*xy(j,k,3)+
     &                 wk1(j,km1,n)*xy(j,k,4)
               end do
            end if
         end do
      end do


c     -- production and destruction terms --
c     -- note: storing factors for the off-diagonal terms from vorticity
c     due to the destruction term in wk3 --

      cb = -cb1/re
      cw = cw1/re
      e_dfw = -7.0d0/6.0d0

      do k = klow,kup
         do j = jlow,jup

            tnu = turre(j,k)

c     -- t1 = 1/(1+chi/cv2) --
            t1   = 1.d0/( 1.d0+chi(j,k)/cv2 )
            fv2 = t1**3
c     -- d(fv2)/d(Q^) --
            do n = 1,5
               dfv2(n) = -3.d0*(t1**4)*dchi(n,j,k)/cv2          
            end do

            fv3(j,k)=(1+chi(j,k)*fv1(j,k))*(1-fv2)/chi(j,k)
c     -- d(chi.fv1)/d(Q^) --
            do n = 1,5
               dchifv1(n) = (chi(j,k)*dfv1(j,k) + fv1(j,k))*dchi(n,j,k)
            end do
c     -- d(fv3)/d(Q^) --
            do n = 1,5
               dfv3(n) = ( ( dchifv1(n)*(1-fv2) - (1+chi(j,k)*fv1(j,k))*
     &              dfv2(n) )*chi(j,k) - (1+chi(j,k)*fv1(j,k))*
     &              (1-fv2)*dchi(n,j,k)  ) / chi(j,k)**2       
            end do
          
c     -- t1 = 1/(k**2.d**2) --
            t1 = (akarman*smin(j,k))**(-2)
 

c     -- S_tilde --
            ts = max(dabs(vort(j,k)),8.5d-10)*re*fv3(j,k) + tnu*t1*fv2


c     -- d(s_tilde)/d(Q^) --
c     -- vorticity (S) only contributes off-diagonal entries --
            do n = 1,5
               dts(n)=t1*tnu*dfv2(n)+
     &              max(dabs(vort(j,k)),8.5d-10)*re*dfv3(n)
            end do
            dts(5) = dts(5) + t1*fv2*xyj(j,k)

            r = tnu*t1/ts
          
            if (dabs(r).ge.10.d0) then

               fw = const
c        -- dfw/d(Q^) --
               do n = 1,5
                  dfw(n) = 0.d0
               end do
               wk3(j,k) = 0.d0

            else

               g = r + cw2*(r**6-r)
               fw = g*( (1.d0+cw3_6)/(g**6+cw3_6) )**expon 
            
c     -- dr/d(Q^) --
               ts2 = ts**2
               do n = 1,5
                  dr(n) = - tnu*t1*dts(n)/ts2
               end do
               dr(5) = dr(5) + t1*xyj(j,k)/ts
               wk3(j,k) = - tnu*t1/ts2
               
c        -- dg/d(Q^) --
               t1 = 1.d0 + cw2*(6.d0*r**5 - 1.d0)
               do n = 1,5
                  dg(n) = dr(n)*t1
               end do
               wk3(j,k) = wk3(j,k)*t1

c        -- dfw/d(Q^) --
               g6 = g**6
               t1 = const*( (g6+cw3_6)**(-expon)-g6*(g6+cw3_6)**e_dfw )
               do n = 1,5
                  dfw(n) = dg(n)*t1
               end do
               wk3(j,k) = wk3(j,k)*t1

            endif

c     -- node j,k production and destruction terms --
            t1 = cb*turre(j,k)/xyj(j,k)
            do n = 1,5
               dprod(n,j,k) = t1*dts(n)
            end do
            dprod(5,j,k) = dprod(5,j,k) + cb*ts
 
            rinv = 1.d0/xyj(j,k)
            t1 = (tnu/smin(j,k))**2         
            do n = 1,5 
               ddest(n,j,k) = rinv*cw*t1*dfw(n)
            end do
            ddest(5,j,k) = ddest(5,j,k)+rinv*cw*fw*2.d0*xyj(j,k)*t1/tnu
            wk3(j,k) = wk3(j,k)*rinv*cw*t1

         end do
      end do

c     -- add result to jacobian arrays --

c     -- No production term in cove area as well as at blunt trailing edge 
c     !!Be careful hardwired!!
c$$$      if (ib.ne.8.and.ib.ne.9 .or. (nblks.eq.4.and.ib.ne.4)) then 
             
         do k = klow,kup
            do j = jlow,jup
               t1=fv3(j,k)*re*(turre(j,k)*cb/xyj(j,k) + wk3(j,k) )
               do n = 1,3
                  bm2(n,j,k) = bm2(n,j,k) + dsxim(n,j,k)*t1 
                  bm1(n,j,k) = bm1(n,j,k) + dsetam(n,j,k)*t1
                  db(n,j,k)  = db(n,j,k) + dprod(n,j,k)+ ddest(n,j,k) 
                  bp1(n,j,k) = bp1(n,j,k) + dsetap(n,j,k)*t1
                  bp2(n,j,k) = bp2(n,j,k) + dsxip(n,j,k)*t1
               end do
               do n = 4,5              
                  db(n,j,k)  = db(n,j,k) + dprod(n,j,k)+ ddest(n,j,k) 
               end do
            end do
         end do

c$$$      else
c$$$
c$$$         do k = klow,kup
c$$$            do j = jlow,jup
c$$$               do n = 1,5              
c$$$                  db(n,j,k)  = db(n,j,k) + ddest(n,j,k) 
c$$$               end do
c$$$            end do
c$$$         end do
c$$$
c$$$      end if

      return                                                            
      end                       !dsa_pd
