c ##########################
c ##                      ##
c ##  subroutine etaexpl  ##
c ##                      ##
c ##########################
c
      subroutine etaexpl(jdim,kdim,q,s,s0,press,sndsp,turmu,fmu,vort,
     &       x,y,xy,xyj,ett,ds,uu,vv,ccy,coef2y,coef4y,spect,precon,gam)
c
c     Calling subroutine 'integrat or cinteg'
c     Main integration calls                            
c                                                                       
      implicit none

#include "../include/arcom.inc"
c
      integer 
     &     jdim, kdim, ix, iy, j, k, n, ixy, nres 
      double precision
     &     q(jdim,kdim,4), press(jdim,kdim),sndsp(jdim,kdim),
     &     s(jdim,kdim,4),s0(jdim,kdim,4),xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     ett(jdim,kdim),ds(jdim,kdim),x(jdim,kdim),y(jdim,kdim),
     &     turmu(jdim,kdim),vort(jdim,kdim),fmu(jdim,kdim),
     &     precon(jdim,kdim,6),gam(jdim*kdim*16),uu(jdim,kdim),
     &     spect(jdim,kdim,3), vv(jdim,kdim),ccy(jdim,kdim),
     &     coef2y(jdim,kdim),coef4y(jdim,kdim),a(maxpen,16),
     &     b(maxpen,16),
     &     c(maxpen,16),d(maxjk,16),e(maxpen,4),work1(maxjk,10),
     &     c2xf(maxj,maxk),c4xf(maxj,maxk),c2yf(maxj,maxk),
     &     c4yf(maxj,maxk),fmuf(maxj,maxk),turmuf(maxj,maxk),
     &     vortf(maxj,maxk),sapdf(maxj,maxk),sadx(maxj,maxk), 
     &     sady(maxj,maxk),suuf(maxj,maxk),svvf(maxj,maxk),
     &     maxcoef2y,mincoef2y

      common/scratch/ a,b,c,d,e,work1

      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &     sapdf, sadx, sady, suuf, svvf 

c     -- inviscid eta flux --
      call rhsy (jdim,kdim,q,s,s0,press,xy,xyj,ett,work1)
c  
c---- 3-a-1. calculate eigenvalues for xi and eta ----
      ix = 3                                                  
      iy = 4                                                  
      call eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,ett,vv,ccy)     

c     -- optimization debug: freezing absolute values --
      if (frozen .and. ( ifrz_what.eq.1 .or. ifrz_what.eq.3)) then
         do k = 1,kend
            do j = 1,jend
               vv(j,k) = svvf(j,k)
            end do
         end do
      end if      

c     -- viscous terms --
      if (viscous) then    
         if (turbulnt) then                                   
            if (itmodel.eq.1) then
c
c              -compute vorticity at k+1/2 nodes for Baldwin-Lomax
c               turbulence model
c              -the Spalart-Allmaras model computes it internally at
c               k nodes
               call vortdeta(jdim,kdim,q,vort,xy,work1)       
c
c              -compute eddy viscosity
               if (.not.cmesh .or. .not.wake) then                  
                  if (iord.eq.2) then
                     call blomax(jdim,kdim,                            
     &                    q,press,vort,turmu,fmu,xy,xyj,x,y)
                  else
                     call blomax_ho(jdim,kdim,q,press,vort,turmu,
     &                    fmu,xy,xyj,x,y,work1,work1(1,2),work1(1,3),
     &                    work1(1,4),work1(1,5))
                  endif
               elseif (cmesh .and. wake) then                           
                  call cwkblomax(jdim,kdim,               
     &                 q,press,vort,turmu,fmu,xy,xyj)       
               endif
            else
c              -Spalart-Allmaras model.
 777           call spalart(jdim,kdim,q,press,turmu,fmu,x,y,xy,
     &               xyj,uu,vv,work1,work1(1,2),work1(1,3),work1(1,4),
     &               work1(1,5),work1(1,6),work1(1,7),work1(1,8),
     &               work1(1,9),work1(1,10),d(1,11),d,d(1,2),d(1,3),
     &               d(1,4),d(1,5),d(1,6),d(1,7),d(1,8),d(1,9),d(1,10),
     &               d(1,14),d(1,15),d(1,16))
            endif                                              
c     -- used for frozen dissipation tests in gradient calculations --
c             if (frozen .and. ifrz_what.eq.1) then
c               write (*,*) 'FROZEN IS ON!!'
c               do k = 1,kend
c                 do j = 1,jend
c                   turmu(j,k) = turmuf(j,k)
c                end do
c              end do
c            end if
         endif                                              
c
c
c---- viscous rhs ----
         if(viseta)call visrhsny(jdim,kdim,q,press,s,s0,turmu,fmu,xy,
     &         xyj,work1(1,1), work1(1,2), work1(1,3), work1(1,4),
     &         work1(1,5), work1(1,6), work1(1,7),            
     &         work1(1,8), d)                      
       endif 
c
c                     explicit artificial dissipation
c                     ===============================
c

      if (idmodel.eq.2) then
c ********************************************************************
c **                       matrix diss model                        **
c ********************************************************************
         ixy=2
         call mspecty(jdim,kdim,q,vv,ccy,xyj,xy,spect,sndsp)
         call gradcoef(ixy,jdim,kdim,press,xyj,coef2y,work1)
         call mcoef24y(jdim,kdim,coef2y,coef4y,work1)
         call expmaty(jdim,kdim,q,coef2y,coef4y,sndsp,s,spect,
     &         xyj,press,ccy,vv,xy,x,y,work1,work1(1,9), 
     &         work1(1,10),d,d(1,2),d(1,3),d(1,4),d(1,5),d(1,9))
c     
       else if (idmodel.eq.1) then
c ********************************************************************
c **                       scalar diss model                        **
c ********************************************************************
        ixy = 2                                                 
c---- form spectral radii for scalar dissipation model ----
        call specty(jdim,kdim,vv,ccy,xyj,xy,coef4y,precon)
        call gradcoef(ixy,jdim,kdim,press,xyj,coef2y,work1) 
     
c     -- optimization debug: frozen pressure switch --
        if (frozen .and. ifrz_what.ge.2) then
           do k = 1,kend
              do j = 1,jend
                 coef2y(j,k) = c2yf(j,k)
              end do
           end do
        end if
        call coef24y(jdim,kdim,coef2y,coef4y,work1,work1(1,2),
     &        work1(1,5),work1(1,6))
     
        if (prec.gt.0) then
          call filtery2(jdim,kdim,q,s,xyj,gam,sndsp,precon,
     &          work1,work1(1,5),work1(1,6),work1(1,7),d)
        else
           
          call filtery(jdim,kdim,q,s,s0,xyj,coef2y,coef4y,sndsp,precon,
     &          work1,work1(1,4))

        endif

      else if (idmodel.eq.3) then     
c ********************************************************************
c **             constant coefficient scalar diss model             **
c ********************************************************************
c---- form spectral radii
        call specty (jdim,kdim,vv,ccy,xyj,xy,coef4y,precon)
        call smoothy(jdim,kdim,q,s,xyj,coef4y,work1)          
c     
      else if (idmodel.eq.4) then
c ********************************************************************
c **                       cusp diss model                          **
c ********************************************************************
        ixy = 2
        call dclay ( jdim,kdim,q,xyj,x,y,work1,d,press,coef2y,d(1,9) )
        if (prec.gt.0) then
          call dcexpy2 ( jdim,kdim,q,s,xyj,xy,press,x,y,precon,d,
     $          work1,work1(1,2),a,b,d(1,9),spect )
        else
          call dcexpy ( jdim,kdim,q,s,xyj,xy,press,x,y,spect,
     &          d,d(1,9),d(1,13),d(1,14),b,c,d(1,15) )
        end if
c     
      endif                                                         

      return                                                          
      end                       !etaexpl
