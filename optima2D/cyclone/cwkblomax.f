      subroutine cwkblomax(jdim,kdim,q,press,vort,turmu,fmu,xy,xyj)
c     parameter (maxj=497, maxk=97)                                     
#include "../include/arcom.inc"
c                                                                       
c                                                                       
      dimension q(jdim,kdim,4),turmu(jdim,kdim),vort(jdim,kdim)         
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)         
      dimension fmu(jdim,kdim)                                          
c                                                                       
      dimension snor(maxj),tmo(maxj),tmi(maxj),uu(maxj)
      dimension tas(maxj),work(maxj,25)
      common/worksp/snor,tmo,tmi,uu,tas,work
c                                                                       
c                                                                       
      data f27 /1.6/                                                    
      data fk,fkk,ydumf /0.4,0.0168,1.0/                                
      data fkleb /0.3/                                                  
      data fmutm/14./                                                   
c                                                                       
c                                                                       
c  note::: must be cmesh = true                                         
c                                                                       
       if(.not.cmesh)then                                               
          print *,'  error in wktur must be cmesh'                      
          stop                                                          
       endif                                                            
c                                                                       
      if(turbulnt) then                                                 
c                                                                       
c  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
c  ******************  turmu comes in with vorticity *****************  
c  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
c                                                                       
c  in cmesh logic the wake has dimension kend = kmax*2-1                
c                                                                       
c  for the baldwin lomax turbulence model we will integrate from the    
c  "c" mesh wake centerline to the upper and lower boundaries.          
c                                                                       
c     for the lower half we must store the eddy viscosity at k-1/2.     
c     for the upper half we must store the eddy viscosity at k+1/2.     
c                                                                       
                                                                        
       khalf = kmax                                                     
       k1    = khalf                                                    
       k2    = kend                                                     
       inc   = 1                                                        
       ihalf = 0                                                        
c                                                                       
 801  continue                                                          
                                                                        
       kedge = k1 + inc*(kmax*0.75 - ihalf)                             
c                                                                       
c  note xyj(j,k) stores transform jacobian by which dep vars are divided
c                                                                       
      do 80 j = jlow,jup                                                
c                                                                       
c     find vorticity tas(k) and total velocity utot(k)                  
c                                                                       
        do 11 k = k1,k2,inc                                             
         utot       = q(j,k,2)**2 + q(j,k,3)**2                         
         utot       = sqrt(utot)/q(j,k,1)                               
         uu(k)      = utot                                              
         tas(k)     = vort(j,k)                                         
         snor(k)    = 0.0                                               
         turmu(j,k) = 0.0                                               
   11   continue                                                        
c                                                                       
c      compute ra                                                       
c                                                                       
c                                                                       
        k=k1                                                            
        wmu = .5*( fmu(j,k) + fmu(j,k+inc))                             
        tau = abs(tas(k+ihalf))                                         
        rhoav = 0.5*(xyj(j,k)*q(j,k,1) + xyj(j,k+inc)*q(j,k+inc,1))     
        ra = sqrt( re*rhoav*tau/wmu)/26.                                
        ra = 1000.*ra                                                   
c                                                                       
c      compute normal distance snor(k) and y*dudy                       
c                                                                       
        snor(k1) = 0.                                                   
        ydum = 0.0                                                      
        umin = uu(k1)                                                   
        ydus = 0.0                                                      
        do 20 k = k1+inc,k2-inc,inc                                     
         if(uu(k) .lt. umin) umin = uu(k)                               
         scis = abs(xy(j,k-inc,3)*xy(j,k,3)+xy(j,k-inc,4)*xy(j,k,4))    
         scal = 1.0/sqrt(scis)                                          
         snor(k) = snor(k-inc) + scal                                   
         if(inc.eq.1 .and. k .gt. k1+inc) go to 18                      
         if(inc.eq.-1 .and. k .lt. k1+inc) go to 18                     
           km2 = k1                                                     
           ydum = 1.e-3                                                 
           um = ydum                                                    
           ym = 0.5*snor(k1+inc)                                        
   18   continue                                                        
        snora = 0.5*(snor(k) + snor(k-inc))                             
c        write(6,19) k,ra,snora,exp(-ra*snora)
c   19  format(2Hk=,I3,3x,3Hra=,f12.3,3x,6Hsnora=,f11.7,2x,4Hexp=,f20.16)  
c        call flush(6)
        ydu = snora*abs(tas(k-inc+ihalf))*(1.-exp(-ra*snora))           
        if(inc.eq.1 .and. k .gt. kedge) go to 20                        
        if(inc.eq.-1 .and. k .lt. kedge) go to 20                       
          if(ydu .lt. ydum) go to 20                                    
            km2 = k - inc                                               
            ydum = ydu                                                  
            um = 0.5*(uu(k-inc) + uu(k))                                
            ym = snora                                                  
   20   continue                                                        
c                                                                       
c     interpolate to find ym, ydum, and um                              
c                                                                       
        if ( (inc.eq.1).and.((km2.lt.k1+inc).or.(km2.gt.(kedge-1))) )   
     >      go to 22                                                    
        if ( (inc.eq.-1).and.((km2.gt.k1+inc).or.(km2.lt.(kedge+1))) )  
     >      go to 22                                                    
          ym3 = 0.5*(snor(km2+inc) + snor(km2+2*inc))                   
          ym1 = 0.5*(snor (km2-inc) + snor (km2))                       
          ydum1 = ym1*abs(tas(km2-inc+ihalf))*(1.0 - exp(-ra*ym1))      
          ydum3 = ym3*abs(tas(km2+inc+ihalf))*(1.0 - exp(-ra*ym3))      
          c2 = ydum - ydum1                                             
          c3 = ydum3 - ydum                                             
          dy1 = ym - ym1                                                
          dy3 = ym3 - ym                                                
          am = (dy3*dy3*c2 + dy1*dy1*c3)/(dy1*dy3*(dy1+dy3))            
          bm = (dy1*c3 - dy3*c2)/(dy1*dy3*(dy1 + dy3))                  
          if(bm .ge. 0.) go to 22                                       
            ymm = ym - 0.5*am/bm                                        
            ydu  = ydum - 0.25*am*am/bm                                 
            ymm = ym - 0.5*am/bm                                        
            if(ydu .lt. ydum .or. ymm .lt. ym1 .or. ymm .gt. ym3)       
     1       go to 22                                                   
               ydum = ydu                                               
               ym = ymm                                                 
               if(ym .gt. snor(km2+inc)) km2 = km2 + inc                
               if(ym .lt. snor(km2)) km2 = km2 - inc                    
               um= ((snor(km2+inc) - ym)*uu(km2)+                       
     1         (ym-snor(km2))*uu(km2+inc))/(snor(km2+inc) - snor(km2))  
   22 continue                                                          
c                                                                       
c      compute outer eddy viscosity                                     
c                                                                       
        do 25 k=k1,kedge,inc                                            
           snor(k) = 0.5*(snor(k) + snor(k+inc))                        
           rhoav = 0.5*(xyj(j,k)*q(j,k,1) + xyj(j,k+inc)*q(j,k+inc,1))  
           ffc = fkk*f27*re*rhoav                                       
           tmo(k) = ffc*ym*ydum                                         
           ffcwk = ydumf*ydumf*ffc                                      
           udiff = abs(um - umin)                                       
           if(ydum .gt. udiff*ydumf)                                    
     >        tmo(k) = ffcwk*ym*udiff*udiff/ydum                        
           fia = fkleb*snor(k)/ym                                       
           if(fia .gt. 1.e5) fia = 1.e5                                 
           fi = 1.0 + 5.5*fia**6                                        
           tmo(k) = tmo(k)/fi                                           
           tmo(k) = abs(tmo(k))                                         
   25   continue                                                        
c                                                                       
c      compute inner eddy viscosity                                     
c                                                                       
        do 30 k=k1,kedge,inc                                            
         tau = abs(tas(k+ihalf))                                        
         rhoav = 0.5*(xyj(j,k)*q(j,k,1) + xyj(j,k+inc)*q(j,k+inc,1))    
        tmi(k) =                                                       
     >         rhoav*re*tau*(fk*snor(k)*(1.-exp(-ra*snor(k))))**2       
         tmi(k) = abs(tmi(k))                                           
   30   continue                                                        
c                                                                       
c      load viscosity coeffs. into array, use inner value until         
c      match point is reached                                           
c                                                                       
        k = k1                                                          
   40   turmu(j,k) = tmi(k)                                             
        k = k +inc                                                      
        if( inc.eq.1 .and. k.gt. kedge) go to 10                        
        if( inc.eq.-1 .and. k.lt. kedge) go to 10                       
          if( tmi(k) .le. tmo(k)) go to 40                              
   41     turmu(j,k) = tmo(k)                                           
          k = k +inc                                                    
          if(inc.eq.1 .and. k.le. kedge) go to 41                       
          if(inc.eq.-1 .and. k.ge. kedge) go to 41                      
   10   continue                                                        
c                                                                       
80    continue                                                          
                                                                        
      if (inc.eq.1) then                                                
        inc = -1                                                        
        ihalf = -1                                                      
        k1 = khalf                                                      
        k2 = 1                                                          
        go to 801                                                       
      endif                                                             
c                                                                       
c  if not turbulnt set turmu = 0.0                                      
c                                                                       
      else                                                              
          do 800 k = kbegin,kend                                        
          do 800 j = jbegin,jend                                        
          turmu(j,k) = 0.0                                              
800       continue                                                      
c                                                                       
      endif                                                             
c                                                                       
      return                                                            
      end                                                               
