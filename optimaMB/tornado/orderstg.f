c *********************************************************************
c ** This routine re-orders the stagnation point data so that L.E.'s **
c ** fall on either the UL or LL corners and the T.E.'s on UR or LR  **
c ** corners.  This makes things much easier latter!!                **
c *********************************************************************
c calling routine: ioall
c
c
      subroutine orderstg

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer ns,ii,iblock
c
c ********************************************************************
c
c  VARIABLE DESCRIPTION :
c
c  stgblk(ii)   is the block number containing stagnation point # ii
c
c  stgcnr(ii)   is the corner of the block that is stagnation point
c               number ii and is denoted as corner N, where corner N
c               is the point that seperates sides N and (N-1) :
c                             1 = Lower Right
c                             2 = Lower Left
c                             3 = Upper Left
c                             4 = Upper Right
c
c  stgtyp(ii)   is the stagnation point type for point # ii,
c               defined as :
c                             1 = Leading Edge
c                            -1 = Trailing Edge
c ********************************************************************
c
c Reorder the stagnation point data so that LE's fall on either the
c UL or LL corners and TE's on UR or LR corners
c
      if (mg .or. gseq) then
        ns=nstgtot
      else
        ns=nstg
      endif
c
      do 100 ii=1,ns
c
c move LR LE's to LL LE's
c
        if (istgcnr(ii).eq.1.and.istgtyp(ii).eq.1) then
          istgblk(ii) = lblkpt(istgblk(ii),4)
          istgcnr(ii) = 2
c
c move UR LE's to UL LE's
c
        elseif (istgcnr(ii).eq.4.and.istgtyp(ii).eq.1) then
          istgblk(ii) = lblkpt(istgblk(ii),4)
          istgcnr(ii) = 3
c
c move LL TE's to LR TE's (OR to UR TE's in the case of a blunt TE)
c
        elseif (istgcnr(ii).eq.2.and.istgtyp(ii).eq.-1) then
          iblock = lblkpt(istgblk(ii),2)
          if (iblock.eq.0) then
            istgblk(ii) = lblkpt(istgblk(ii),1)
c Check if decomposition is a C-Mesh type
            if (lblkpt(istgblk(ii),2).ne.0) then
              istgblk(ii) = lblkpt(istgblk(ii),2)
              istgcnr(ii) = 4
            else
              istgblk(ii) = lblkpt(istgblk(ii),4)
              istgcnr(ii) = 1
            endif
          else
            istgblk(ii) = iblock
            istgcnr(ii) = 1
          endif
c
c move UL TE's to UR TE's (OR to LR TE's in the case of a blunt TE)
c
        elseif (istgcnr(ii).eq.3.and.istgtyp(ii).eq.-1) then
          iblock = lblkpt(istgblk(ii),2)
          if (iblock.eq.0) then
            istgblk(ii) = lblkpt(istgblk(ii),3)
            istgblk(ii) = lblkpt(istgblk(ii),2)
            istgcnr(ii) = 1
          else
            istgblk(ii) = iblock
            istgcnr(ii) = 4
          endif
        endif
  100 continue
c
      return
      end
