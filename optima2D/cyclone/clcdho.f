      subroutine clcdho(jdim,kdim,q,press,x,y,xy,xyj,nscal,cl,cd,cm,
     *                    clvv,cdvv,cmvv,zeta,work,doit)
c
#include "../include/arcom.inc"
c                                                                       
c     Written by Stan De Rango 23/06/97
c
#include "integparm.inc"
      integer count,jd,inter
      dimension q(jdim,kdim,4),press(jdim,kdim)                         
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension x(jdim,kdim),y(jdim,kdim)                               
      external f,f2,f3,f4,dglegq
      common/var/jd,count
      logical doit
c                                                                       
c                                                                       
      common/wksp/cp,xn,yn,sn,cp2,x2,y2
      dimension cp(maxj),xn(maxj),yn(maxj),sn(maxj),cp2(maxj)
      dimension x2(maxj),y2(maxj),zeta(maxj),work(maxj,3)
c                                                                       
c     cp  = used to store both coefficient of pressure and friction
c     xn  = chordwise direction (pos. in direction of LE to TE)
c     yn  = normal to chord
c     sn  = parameter approximating arclength (going from lower TE to
c           upper TE)
c     zeta= dummy variable for parameterization of xn,yn,cp
c     dx  = d(xn)/d(sn)
c     dy  = d(yn)/d(sn)
c     x2,y2,cp2 = double derivative of corresponding variable
c
c
c
c     -the parameter nscal determines whether the q variables are scaled 
c      by the metric jacobians xyj.                                      
c                                                                       
c     -this routine supplies force and moment coeffs.  on airfoil surface
c
      cl = 0.d0                                                           
      cd = 0.d0                                                           
      cm = 0.d0                                                           
      clvv = 0.d0                                                         
      cdvv = 0.d0                                                         
      cmvv = 0.d0                                                         
c                                                                       
c     set limits of integration
c                                                                       
      j1 = jlow 
      j2 = jup
      jtp = j1+1                                                        
      if(.not.periodic)then                                             
      j1 = jtail1                                                       
      j2 = jtail2                                                       
      jtp = j1+1                                                    
      endif                                                             
      jd=j2-j1+1
c      write(out_unit,*) 'jd=',jd
c
c     -spline airfoil surface so you have y',y'',x',and x''  w.r.t s.
c     -here we simply store values of x and y in work arrays xn and yn
      k=1
      if (periodic) then
        jj=jplus(j2)
        zeta(jd+1)=dble(jd)
        xn(jd+1)=x(jj,k)
        yn(jd+1)=y(jj,k)
        jd=jd+1
      endif
      if (doit) then
        zeta(1)=0.d0
        xn(1)=x(j1,k)
        yn(1)=y(j1,k)
        do 1 j=jtp,j2
          jj=j-jtp+2
          zeta(jj)=dble(jj-1)
          xn(jj)=x(j,k)
          yn(jj)=y(j,k)
 1      continue
c        
c       We begin by splining y & x w.r.t zeta, a dummy variable.
c        
c       ****************************************************      
c       *                  Funct: y(zeta)                  *
c       ****************************************************      
c       Boundary Conditions
        if (.not.periodic) then
          yp1=bc(jd,1,1,zeta,yn)
          ypn=bc(jd,jd,-1,zeta,yn)
        else
c         -for circular cylinder case yp1=ypn=0 (for coarse meshes "bc"
c          calculates a small but finite second derivative)
c         -since I don't have an O-mesh generator for an airfoil I'll
c          assume that if the flow is periodic its a cylinder case. 
          yp1=0.d0
          ypn=0.d0
        endif
c        
c        print *,'bc of y',yp1,ypn
        call spline(jd,zeta,yn,yp1,ypn,y2,work,work(1,2),work(1,3))
c        
c       ****************************************************      
c       *                  Funct: x(zeta)                  *
c       ****************************************************      
c       Boundary Conditions
        yp1=bc(jd,1,1,zeta,xn)
        ypn=bc(jd,jd,-1,zeta,xn)
c        
c        print *,'bc of x',yp1,ypn
        call spline(jd,zeta,xn,yp1,ypn,x2,work,work(1,2),work(1,3))
c        
c         do 6 j=1,jd
c            call splintder(jd,sn,xn,x2,sn(j),dx)
c            print *,j,sn(j)/sn(jd),dx
c 6       continue
c         stop
c        
c       Now, using y(zeta) & x(zeta), we interpolate as many points as we
c       wish between zeta points to get an accurate calculation of
c       arclength.  The value is stored in sn.  Then we respline y & x,
c       but this time we do w.r.t. sn.
c       The value 'inter' is the number of intervals between two
c       zeta points. So a total of 'inter+1' points discretize the
c       arclength between the two points.
c        
        inter=39
        sn(1)=0.d0
        do 5 j=2,jd
          ds=(zeta(j)-zeta(j-1))/dble(inter)
          sntmp=sn(j-1)
          xp=xn(j-1)
          yp=yn(j-1)
          do 6 k=2,inter+1
            stmp=zeta(j-1)+dble(k-1)*ds
            call splint(jd,zeta,xn,x2,stmp,xtmp)
            call splint(jd,zeta,yn,y2,stmp,ytmp)
            sntmp=sntmp + dsqrt((xtmp-xp)**2 + (ytmp-yp)**2)
            xp=xtmp
            yp=ytmp
 6        continue
          sn(j)=sntmp
 5      continue
c        
c       ****************************************************      
c       *                   Funct: y(sn)                   *
c       ****************************************************      
c       Boundary Conditions
        if (.not.periodic) then
          yp1=bc(jd,1,1,sn,yn)
          ypn=bc(jd,jd,-1,sn,yn)
        else
          yp1=0.d0
          ypn=0.d0
        endif
c        
c        print *,'bc of y',yp1,ypn
        call spline(jd,sn,yn,yp1,ypn,y2,work,work(1,2),work(1,3))
c        
c       ****************************************************      
c       *                   Funct: x(sn)                   *
c       ****************************************************      
c       Boundary Conditions
        yp1=bc(jd,1,1,sn,xn)
        ypn=bc(jd,jd,-1,sn,xn)
c        
c        print *,'bc of x',yp1,ypn
        call spline(jd,sn,xn,yp1,ypn,x2,work,work(1,2),work(1,3))
      endif
      if (periodic) j2=jmaxold
c      
c     ****************************************************      
c     *                   Funct: Cp(sn)                  *
c     ****************************************************      
      cpc = 2.d0/(gamma*fsmach**2)                                        
c
c     compute cp at grid points     
c                                                                       
      do 10 j=j1,j2
        pp = press(j,1)                                                
        if(nscal.eq.0) pp = pp*xyj(j,1)                              
        jj=j-j1+1
        cp(jj) = (pp*gamma -1.d0)*cpc
c        write(90,*) xn(jj),yn(jj),cp(jj)         
 10   continue
c
c     Boundary Conditions
      yp1=bc(jd,1,1,sn,cp)
      ypn=bc(jd,jd,-1,sn,cp)
c
c      print *,'bc of cp',yp1,ypn
      call spline(jd,sn,cp,yp1,ypn,cp2,work,work(1,2),work(1,3))
c
c     ****************************************************
c
c     -integrate along arclength
c     -compute normal force coefficient and chord directed force coeff     
c     -chord taken as one in all cases

      sstart=sn(1)
      send=sn(jd)
      count=0
      maxlev = levmax
      tol=1.d-11

      cc = daquad(dglegq,3,f,sstart,send,tol,maxlev,sing,errest)
c      print *,'count for cc',count,errest
      if (tol.ne.0.d0) write (out_unit,*)
     &      'Routine failed for cc. tol = ',tol,'  sing = ',sing
c
      count=0
      maxlev = levmax
      tol=1.d-11
      cn = daquad(dglegq,3,f2,sstart,send,tol,maxlev,sing,errest)
c      print *,'count for cn',count,errest
      if (tol.ne.0.d0) write (out_unit,*)
     &      'Routine failed for cn. tol = ',tol,'  sing = ',sing
c
      cmle = 0.d0
      do 11 j=jtp,j2                                                    
        jm1 = jminus(j)
        jj = j-jtp+2
        if (jj.eq.1) then
          jjm1 = jd
        else
          jjm1 = jj-1
        endif
        cpav = ( cp(jj) + cp(jjm1))*.5d0
        cmle = cmle + cpav*( x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))    
 11   continue
      cl = cn*cos(alpha*pi/1.8d2) - cc*sin(alpha*pi/1.8d2)
      cd = cn*sin(alpha*pi/1.8d2) + cc*cos(alpha*pi/1.8d2)                
      cmqc = cmle + .25d0*cn
      cm = cmqc                                                         
c                                                                       
c
c     now for the viscous contribution
      if(viscous)then                                                 
c                                                                       
c       viscous coefficent of friction calculation        
c       taken from P. Buning
c                                                                      
c       calculate the skin friction coefficient  (w => wall) 
c                                                                      
c        c  = tau   /  q    
c         f      w      inf 
c       
c        where
c       
c              tau  = mu*(du/dy-dv/dx)
c                 w                   w
c       
c              q    =  1/2 * rhoinf * uinf * uinf
c               inf
c                                                                      
c              du/dy = (du/dxi * dxi/dy) + (du/deta * deta/dy) 
c              dv/dx = (dv/dxi * dxi/dx) + (dv/deta * deta/dx) 
c       
c       (definition from F.M. White, Viscous Fluid Flow, Mcgraw-Hill Inc.,
c       York,1974, p.50, but use freestream values instead of edge values.
c                                                                      
c                                                                      
c       for calculating cf, we need the coefficient of viscosity, mu. use
c       re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1,rhoinf=1.
c                                                                       
        cinf  = 1.d0
        alngth= 1.d0
c       re already has fsmach scaling              
        amu   = rhoinf*alngth/re                                  
        uinf2 = fsmach**2
c                                                                         
        k= 1                                                             
        j1 = jlow
        j2 = jmaxold
c                                                                       
        if(.not.periodic)then                                         
          j1 = jtail1                                               
          j2 = jtail2                                               
        endif                                                          
c                                                                       
        tmp=1.d0/12.d0
        do 110 j = j1,j2                                            
          jp1 = jplus(j)                                              
          jp2 = jplus(jp1)                                              
          jm1 = jminus(j)                                             
          jm2 = jminus(jm1)
c     
c         Note: for viscous flows u and v = 0 on body ...
c               consequently, the xi direction derivatives can be skipped.
c               -only during first strtit iterations is u and v non-zero
c                but we dont't care about cf then. 
c
csdc         xi direction    
csd          rm2=1.d0/q(jm2,k,1)
csd          rm1=1.d0/q(jm1,k,1)
csd          rp1=1.d0/q(jp1,k,1)
csd          rp2=1.d0/q(jp2,k,1)
csdc
csd          um2=q(jm2,k,2)*rm2
csd          um1=q(jm1,k,2)*rm1
csd          up1=q(jp1,k,2)*rp1
csd          up2=q(jp2,k,2)*rp2
csdc
csd          vm2=q(jm2,k,3)*rm2
csd          vm1=q(jm1,k,3)*rm1
csd          vp1=q(jp1,k,3)*rp1
csd          vp2=q(jp2,k,3)*rp2
csdc
csd          uxi = tmp*(um2 + 8.d0*(-um1+up1) - up2)
csd          vxi = tmp*(vm2 + 8.d0*(-vm1+vp1) - vp2)
          uxi=0.d0
          vxi=0.d0
c
c
c         eta direction
          r=1.d0/q(j,k,1)
          rp1=1.d0/q(j,k+1,1)
          rp2=1.d0/q(j,k+2,1)
          rp3=1.d0/q(j,k+3,1)
          rp4=1.d0/q(j,k+4,1)
c
          u  =q(j,k,2)*r
          up1=q(j,k+1,2)*rp1
          up2=q(j,k+2,2)*rp2
          up3=q(j,k+3,2)*rp3
          up4=q(j,k+4,2)*rp4
c
          v  =q(j,k,3)*r
          vp1=q(j,k+1,3)*rp1
          vp2=q(j,k+2,3)*rp2
          vp3=q(j,k+3,3)*rp3
          vp4=q(j,k+4,3)*rp4
c
          ueta= tmp*(-25.d0*u +48.d0*up1-36.d0*up2+16.d0*up3-3.d0*up4) 
          veta= tmp*(-25.d0*v +48.d0*vp1-36.d0*vp2+16.d0*vp3-3.d0*vp4) 
c
c         metric derivatives
          xix = xy(j,k,1)                                             
          xiy = xy(j,k,2)                                             
          etax = xy(j,k,3)                                            
          etay = xy(j,k,4)                                            
c
          tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))         
          jj=j-j1+1
c          print *,'tauw=',jj,sn(jj)/sn(jd),tauw
          cp(jj)= tauw/(.5d0*rhoinf*uinf2)
 110    continue
        if (sngvalte) then
c         -average trailing edge value
          cp(j1)=.5d0*(cp(j1)+cp(j2))
          cp(j2)=cp(j1)
        endif
c
c       -the array cp now contains the coefficient of friction
c       -spline for coefficient of friction below
c
c       ****************************************************      
c       *                   Funct: Cf(sn)                  *
c       ****************************************************      
c
c       Boundary Conditions
        yp1=bc(jd,1,1,sn,cp)
        ypn=bc(jd,jd,-1,sn,cp)
c        
c        print *,'bc of cp',yp1,ypn
        call spline(jd,sn,cp,yp1,ypn,cp2,work,work(1,2),work(1,3))
c        
c       ****************************************************
c
c       -integrate along arclength
c       -compute viscous normal and axial forces 
c       -chord taken as one in all cases

        sstart=sn(1)
        send=sn(jd)
        count=0
        maxlev = levmax
        tol=1.d-11
         
        ccv = daquad(dglegq,3,f3,sstart,send,tol,maxlev,sing,errest)
c        print *,'count for ccv',count,errest
        if (tol.ne.0.d0) write (out_unit,*) 
     &    'Routine failed for ccv. tol = ',tol,'  sing = ',sing
c        
        count=0
        maxlev = levmax
        tol=1.d-11
        cnv = daquad(dglegq,3,f4,sstart,send,tol,maxlev,sing,errest)
c        print *,'count for cnv',count,errest
        if (tol.ne.0.d0) write (out_unit,*)
     &    'Routine failed for cnv. tol = ',tol,'  sing = ',sing
        cmlev = 0.d0                                             
        do 111 j=jtp,j2                                          
          jm1 = jminus(j)                                      
          jj = j-jtp+2
          if (jj.eq.1) then
            jjm1 = jd
          else
            jjm1 = jj-1
          endif
          cfav = ( cp(jj) + cp(jjm1))*.5d0                             
          cmlev = cmlev + cfav*                                       
     *     (  (x(j,1)+x(jm1,1))*.5*(y(j,1) -y(jm1,1)) -               
     *        (y(j,1)+y(jm1,1))*.5*(x(j,1) -x(jm1,1))   )              
 111    continue                                                        
        clvv = cnv*cos(alpha*pi/1.8d2) - ccv*sin(alpha*pi/1.8d2)
        cdvv = cnv*sin(alpha*pi/1.8d2) + ccv*cos(alpha*pi/1.8d2)        
        cmqcv = cmlev +.25*cnv                                   
        cmvv = cmqcv
c     -this is the endif for viscous case         
      endif
c
      return                                                            
      end                                                               
c
c
c     *************** External Functions for Integration ***************
c
      double precision function f(stmp)
c     -chord directed component of pressure
c
#include "../include/parm.inc"
      integer count,jd
      double precision dx,dy,cptmp,denom,ndotx,stmp
      common/var/jd,count
      dimension cp(maxj),xn(maxj),yn(maxj),sn(maxj),cp2(maxj)
      dimension x2(maxj),y2(maxj)
      common/wksp/cp,xn,yn,sn,cp2,x2,y2
c
c     cp = coefficient of pressure
c     n  = normal to surface
c     xn = chordwise direction (pos. in direction of LE to TE)
c     yn = normal to chord
c     sn = parameter approximating arclength (going from lower TE to
c          upper TE)
c     dx = d(xn)/d(sn)
c     dy = d(yn)/d(sn)
c     x2,y2,cp2 = double derivative of corresponding variable
c
      call splintder(jd,sn,xn,x2,stmp,dx)
      call splintder(jd,sn,yn,y2,stmp,dy)
      call splint(jd,sn,cp,cp2,stmp,cptmp)
c
      denom=dsqrt(dx**2+dy**2)
      ndotx=-dy/denom
      f=-cptmp*ndotx
      count=count+1
      return
      end
c
c
      double precision function f2(stmp)
c     -normal component of pressure
c
#include "../include/parm.inc"
      integer count,jd
      double precision dx,dy,cptmp,denom,ndoty,stmp
      common/var/jd,count
      dimension cp(maxj),xn(maxj),yn(maxj),sn(maxj),cp2(maxj)
      dimension x2(maxj),y2(maxj)
      common/wksp/cp,xn,yn,sn,cp2,x2,y2
c
      call splintder(jd,sn,xn,x2,stmp,dx)
      call splintder(jd,sn,yn,y2,stmp,dy)
      call splint(jd,sn,cp,cp2,stmp,cptmp)
c
      denom=dsqrt(dx**2+dy**2)
      ndoty=dx/denom
      f2=-cptmp*ndoty
      count=count+1
      return
      end
c
c
      double precision function f3(stmp)
c     -chord directed component of shear stress
c
#include "../include/parm.inc"
      integer count,jd
      double precision dx,dy,cftmp,denom,tdotx,stmp
      common/var/jd,count
      dimension cf(maxj),xn(maxj),yn(maxj),sn(maxj),cf2(maxj)
      dimension x2(maxj),y2(maxj)
      common/wksp/cf,xn,yn,sn,cf2,x2,y2
c
c     cf = coefficient of friction
c     t  = tangent to surface
c     xn = chordwise direction (pos. in direction of LE to TE)
c     yn = normal to chord
c     sn = parameter approximating arclength (going from lower TE to
c          upper TE)
c     dx = d(xn)/d(sn)
c     dy = d(yn)/d(sn)
c     x2,y2,cf2 = double derivative of corresponding variable
c
      call splintder(jd,sn,xn,x2,stmp,dx)
      call splintder(jd,sn,yn,y2,stmp,dy)
      call splint(jd,sn,cf,cf2,stmp,cftmp)
c
      denom=dsqrt(dx**2+dy**2)
      tdotx=dx/denom
      f3=cftmp*tdotx
      count=count+1
      return
      end
c
c
      double precision function f4(stmp)
c     -normal component of shear stress
c
#include "../include/parm.inc"
      integer count,jd
      double precision dx,dy,cptmp,denom,tdoty,stmp
      common/var/jd,count
      dimension cp(maxj),xn(maxj),yn(maxj),sn(maxj),cp2(maxj)
      dimension x2(maxj),y2(maxj)
      common/wksp/cp,xn,yn,sn,cp2,x2,y2
c
      call splintder(jd,sn,xn,x2,stmp,dx)
      call splintder(jd,sn,yn,y2,stmp,dy)
      call splint(jd,sn,cp,cp2,stmp,cptmp)
c
      denom=dsqrt(dx**2+dy**2)
      tdoty=dy/denom
      f4=cptmp*tdoty
      count=count+1
      return
      end
