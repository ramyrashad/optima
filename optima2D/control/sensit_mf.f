c----------------------------------------------------------------------
c     -- sensitivity gradient calculation --
c     -- uses matrix-free approach --
c----------------------------------------------------------------------

      subroutine sensit_mf (grad, jdim, kdim, ndim, indx, icol, iex, q,
     &     cp, xy, xyj, x, y, cp_tar, bap, bcp, bt, bknot, dvs, idv, ia,
     &     ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat, press,
     &     sndsp, tmet, ds, turmu, fmu, vort, spect, uu, vv, ccx, ccy,
     &     coef4, coef2, precon, gam, rhs, sol, dOdQ, dQdX, dRdX, s,
     &     bcf, qsave, work1, xys, xyjs, xs, ys, turres, xOrig, yOrig,
     &     dx, dy, obj0)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim, kdim, indx(jdim,kdim), icol(9), iex(jdim,kdim,4)
      integer idv(nc+mpopt)
      integer ia(jdim*kdim*ndim+2),iat(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*5*ndim+1)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*5*ndim+1)

      integer iatmp(maxjk*nblk+2), ipatmp(maxjk*nblk+2)
      integer jatmp(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      integer jpatmp(maxjk*nblk2*5+1)

c     -- ILU arrays --
      integer ierr, nz
      integer,dimension(:),allocatable :: levs,jw
      double precision alu(iwk)
      integer jlu(iwk),ju(maxjk*nblk)

c     -- gmres work array --
      integer iwk_gmres
      double precision,allocatable :: wk_gmres(:)

      double precision grad(nc+mpopt), cp(jdim,kdim)
      double precision cp_tar(jbody,2), dvs(nc+mpopt), bap(jbody,2)
      double precision bcp(nc,2), bt(jbody), bknot(jbsord+nc)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*5*ndim+1)
      double precision pat(jdim*kdim*ndim*5*ndim+1)
      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision xOrig(jdim,kdim), yOrig(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)

      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)
      double precision s(jdim,kdim,ndim), rhs(jdim*kdim*ndim+1)
      double precision sol(jdim*kdim*ndim+1)

      double precision dQdX(jdim,kdim,ndim), dOdQ(jdim,kdim,ndim)
      double precision dRdX(jdim,kdim,ndim), bcf(jdim,kdim,4)
      double precision qsave(jdim,kdim,ndim)

      double precision xys(jdim,kdim,4), xyjs(jdim,kdim), xs(jdim,kdim)
      double precision ys(jdim,kdim), turres(jdim,kdim) 

      double precision dRdXna, dOdQna, dQdXna, resultnorm, obj0
      double precision multipl(jdim*kdim*ndim+1),reslt(jdim*kdim*ndim+1)

c     -- work arrays: temp storage --
      double precision work1(maxjk,24)

c     -- centered differences for frechet derivatives --
      external framux_o2

c     -- timing variables --
      real*4 time1, time2, t, tarray(2)

      logical jac_tmp, prec_tmp, imps, scalena

      na = jdim*kdim*ndim
      nas = jdim*kdim*ndim*9*ndim
      nap = jdim*kdim*ndim*5*ndim

      if (clopt) then
         na = na+1
         nap = nap+1
         nas = jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1
      end if

cmpr  Allocate some of the big arrays dynamically
      iwk_gmres=(na+1)*(IM_GMRES+2)+(IM_GMRES+1)*IM_GMRES
      allocate(levs(iwk),jw(3*maxjk*nblk),wk_gmres(iwk_gmres))


      dt_tmp = dt
      dt = 1.d0

      jac_tmp = jac_mat
      prec_tmp = prec_mat

      jac_mat = .false.
      prec_mat = .true.

      do j=1,na+1
        iatmp(j) = ia(j)
        ipatmp(j) = ipa(j)
      end do
      do j = 1,nas
        jatmp(j) = ja(j)
      end do
      do j = 1,nap
        jpatmp(j) = jpa(j)
      end do 

      write (*,10)
 10   format (/3x,
     &      '<< Solving sensitivity equation to compute gradient. >>',
     &      /3x,'<< Using matrix-free approach. >>' )
      
c     -- store q in qsave, this makes repeated gradient calculation
c     more consistent -- 
      do n = 1,ndim
         do k = 1,kend
            do j = 1,jend
               qsave(j,k,n)=q(j,k,n)
            end do
         end do
      end do

c     -- determine dJ/dQ --
      call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
      call dOBJdQ (jdim, kdim, ndim, x, y, xy, xyj, q, dOdQ, cp_tar, cp,
     &      press, obj0)

      if (clopt) then
         call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
         call dOBJdQna (jdim, kdim, ndim, x, y, xy, xyj, q, dOdQna,
     &        cp_tar,cp, press, obj0)
      end if

c     -- determine dQdX --

c     -- form LHS: preconditioner and Jacobian -- 
c     -- arrays ia, ja, as store O(2) Jacobian --
c     -- arrays ipa, jpa, pa store O(1) preconditioner --

c     -- put in jacobian --
      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
            q(j,k,n) = q(j,k,n)/xyj(j,k)
          end do
        end do
      end do 

c     -- ensure tmet is zero --
      do k = 1,kend
        do j = 1,jend
          tmet(j,k) = 0.d0
        end do
      end do

      call calcps (jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- compute generalized distance function --
      if (viscous .and. turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kend
          do j = jbegin,jend
            j_point=j
            if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
            xpt1 = x(j_point,kbegin)
            ypt1 = y(j_point,kbegin)
            smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
          end do
        end do
      end if
      imps=.true.
      call get_lhs (jdim, kdim, ndim, indx, icol, iex, ia, ja, as, ipa,
     &      jpa, pa, xy, xyj, x, y, q, press, sndsp, fmu, turmu, uu, vv,
     &      ccx, ccy, coef2, coef4, tmet, precon, bcf, work1, ast, pdc,
     &      a_jktmp,imps)
      
c     -- order preconditioner array --
      call csrcsc (na, 1, 1, pa, jpa, ipa, pat, jpt, ipt)
      call csrcsc (na, 1, 1, pat, jpt, ipt, pa, jpa, ipa)
      if (icg.eq.0) write (opt_unit,20) ipa(na+1) - ipa(1)
 20   format (3x,'Non-zeros in preconditioner:',i8)

c     -- used for framux_o1 -------------------------------------------
c      call get_rhs (jdim, kdim, ndim, q, xy, xyj, x, y, precon, coef2,
c     &      coef4, uu, vv, ccx, ccy, ds, press, tmet, spect, gam,
c     &      turmu, fmu, vort, sndsp, s, work1)

c      prec_mat = .false.
c      call scale_bc (1, jdim, kdim, ndim, indx, iex, ia, ja, as, ipa,
c     &      jpa, pa, s, bcf, work1)
c      prec_mat = .true.
c     -----------------------------------------------------------------

c     -- take out jacobian --
      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
c            q(j,k,n) = q(j,k,n)*xyj(j,k)
             q(j,k,n) = qsave(j,k,n)
          end do
        end do
      end do           

c     -- store grid and metrics: done for speed --
      do n = 1,4
        do k=kbegin,kend
          do j=jbegin,jend
            xys(j,k,n) = xy(j,k,n)
          end do
        end do
      end do
              
      do k=kbegin,kend
        do j=jbegin,jend
          xs(j,k) = x(j,k)
          ys(j,k) = y(j,k)
          xyjs(j,k) = xyj(j,k)
        end do
      end do

c     -- solve sensitivity equation for each design variable --
      do in = 1,ndv
c     -- form rhs => dR/dX --      
         call dResdX (in, jdim, kdim, ndim, q, xy, xyj, x, y, bap,
     &        bcp, bt, bknot, dvs, idv, dRdX, rhs, sol, precon, coef2,
     &        coef4, uu, vv, ccx, ccy, ds, press, tmet, spect, gam,
     &        turmu, fmu, vort, sndsp, work1, xys, xyjs, xs, ys, turres,
     |        xOrig, yOrig, dx, dy, -1.d0, dRdXna)

c     -- diagonal scaling for all b.c. equations --
         call scale_bc (1, jdim, kdim, ndim, indx, iex, ia, ja, as, ipa,
     &        jpa, pa, dRdX, bcf, work1, rhsnamult, scalena)

         if ( prec_mat ) call prec_ilu (jdim, kdim, ndim, ilu_meth,
     &        lfil, opt_unit, ipa, jpa, pa, alu, jlu, ju, levs,
     &        jw, work1)

c     -- gmres --
         ipar(1) = 0
         ipar(2) = 2
         ipar(3) = 1
         ipar(4) = iwk_gmres
         ipar(5) = im_gmres
         ipar(6) = iter_gmres

c     -- inner iterations convergence tolerance -- 
         fpar(1) = eps_gmres
         fpar(2) = 1.d-14
         fpar(11) = 0.0

         write (*,55) ipar(5)
 55      format (/3x,'Starting GMRES(',i2,') ...')

         do n = 1,ndim
            do k = 1,kend
               do j = 1,jend
                  jk = ( indx(j,k) - 1 )*ndim + n
                  rhs(jk) = dRdX(j,k,n)
                  q(j,k,n) = q(j,k,n)/xyj(j,k)
               end do
            end do
         end do 
         if (clopt) rhs(na) = dRdXna*rhsnamult !*1.d2

c     -- flag to indicate the first iteration of GMRES, see framux --
         ifirst = 1

         ierr = 0
         t=etime(tarray)
         time=tarray(1)
         call run_gmres (jdim, kdim, ndim, indx, iex, q, xy, xyj, x, y,
     &        precon, coef2, coef4, uu, vv, ccx, ccy, ds, press, tmet,
     &        spect, gam, turmu, fmu, vort, sndsp, s, bcf, ifirst, rhs,
     &        sol, as, ja, ia, alu, jlu, ju, work1(1,1), work1(1,6),
     &        wk_gmres, work1(1,11), 6, gmres_unit, framux_o2,rhsnamult)
         t=etime(tarray)
         time=tarray(1)-time

         write (*,60) im_gmres, ipar(1), time
         write (*,61) ipar(7), fpar(5)
         write (opt_unit,60) im_gmres, ipar(1), time
         write (opt_unit,61) ipar(7), fpar(5)

 60      format (3x,'GMRES(',i2,') return code:',i4,2x,'Run-time:',
     &        f7.2,'s')
 61      format (3x,'Number of inner iterations',i5,' and residual',
     &        e12.4)

         if ( ipar(1).lt.0 ) then 
            write (*,*) '!!SENSITIVITY: gmres convergence problems!!'
            stop
         end if

         do n = 1,ndim
            do k = 1,kend
               do j = 1,jend
                  jk = ( indx(j,k) - 1 )*ndim + n
                  dQdX(j,k,n) = sol(jk)
c     q(j,k,n) = q(j,k,n)*xyj(j,k)
               end do
            end do
         end do

         if (clopt) then
            dQdXna = sol(na)!/1.d2
         end if

         do n = 1,ndim
            do k = 1,kend
               do j = 1,jend
                  q(j,k,n)=qsave(j,k,n)
               end do
            end do
         end do 

c     -- for viscous flow, zero out the sensitivity vector at the
c     boundary  for the momentum equation --
         if (viscous) then
            k = 1
            do n = 2,3
               do j = jtail1,jtail2
                  dQdX(j,k,n) = 0.d0
               end do
            end do 
         end if

         grad(in) = 0.d0
         do n = 1,ndim
            do k = 1,kend
               do j = 1,jend
                  grad(in) = grad(in) + dOdQ(j,k,n)*dQdX(j,k,n)
               end do
            end do
         end do
         if (clopt) grad(in) = grad(in) + dOdQna*1.d2*dQdXna
c     -- dJ/dX --
         call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
         call dObjdX (in, dOdX, jdim, kdim, dvs, idv, x, y, bap, bcp,
     &        bt, bknot, q, press, xy, xyj, xys, xyjs, xs, ys,
     |        xOrig, yOrig, dx, dy, -1.d0, obj0)

         grad(in) = grad(in) + dOdX

         if (in.eq.1) prec_mat = .false.
      end do

      do j=1,na+1
         ia(j) = iatmp(j)
         ipa(j) = ipatmp(j) 
      end do
      do j = 1,nas
         ja(j) = jatmp(j)
      end do
      do j = 1,nap
         jpa(j) = jpatmp(j) 
      end do   
      
      dt = dt_tmp
      jac_mat = jac_tmp
      prec_mat = prec_tmp

cmpr  Deallocate memory
      deallocate(levs,jw,wk_gmres)

      return 
      end                       !sensit_mf
