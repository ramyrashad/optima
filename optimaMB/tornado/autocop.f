c *******************************************************************
c ** subroutine to automatically generate the data for copy        **
c ** between blocks.  This is needed because the point in the      **
c ** adjacent block which is in the halo block with a singular     **
c ** point as a corner will not be on the surface !!!!             **
c *******************************************************************
c calling routine: ioall
c
c
c
      subroutine autocop(mglev)

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer mglev,nbs,ns,nlevel,nsadj,nstmp,ii,jj,iblkup,iblkdn
      integer iblunt,iblock
c
c ** DETERMINE COPY & ADJUSTMENT POINTS AROUND THE STAGNATION POINTS **
c
      if (mg .or. gseq) then
        nbs=nblkstot
        ns=nstgtot
      else
        nbs=nblks
        ns=nstg
      endif

      if (nblks.eq.3) nlevel = 0
      if (nblks.eq.4) then
        nlevel=1
        call set_level(nlevel)
      endif
      
      nsadj = 0
      ncopy = 0
      nsing = 0
      nstmp = nstg

      do 100 ii=1,ns
c
c
c C-MESH TOPOLOGY :
c
        if (mg .or. gseq) then
c         Sharp T.E.
          if (nblks.eq.3) then
            if (ii.eq.nstmp) then
c             -note: for 3-blk grid, nstg=1 and this statement
c              is true right off the bat.
c             -go to coarser grid/next level
              nlevel=nlevel+1
              nstmp=nstmp+nstg
              call set_level(nlevel)
            endif
            nsadj = 0
            ncopy = 2*mglev
            jj= 2*nlevel-1
            ibte1(jj)   = nblkstart + 1
            ijte1(jj)   = jbmax(nblkstart+1)
            ikte1(jj)   = 1
            ibte2(jj)   = nblkend
            ijte2(jj)   = 1
            ikte2(jj)   = 1
            ibte1(jj+1) = nblkstart + 1
            ijte1(jj+1) = 1
            ikte1(jj+1) = 1
            ibte2(jj+1) = nblkstart
            ijte2(jj+1) = jbmax(nblkstart)
            ikte2(jj+1) = 1
          endif
     
c         Blunt T.E.         
          if (nblks.eq.4) then
            if (ii.gt.nstmp) then
              nlevel=nlevel+1
              nstmp=nstmp+nstg
              call set_level(nlevel)
            endif
            if (ii.le.nstg) then
              jj=1
            else
              jj=2**nlevel+1
            endif
            nsadj = 0
            ncopy = 4*nlevel
            ibte1(jj) = nblkstart + 1 
            ijte1(jj) = jbmax(nblkstart+1)
            ikte1(jj) = 1
            ibte2(jj) = nblkstart + 2
            ijte2(jj) = 1
            ikte2(jj) = 1
            ibte1(jj+1) = nblkstart + 1
            ijte1(jj+1) = jbmax(nblkstart+1)
            ikte1(jj+1) = 1
            ibte2(jj+1) = nblkend
            ijte2(jj+1) = 1
            ikte2(jj+1) = kbmax(nblkend)
            ibte1(jj+2) = nblkstart + 1
            ijte1(jj+2) = 1
            ikte1(jj+2) = 1
            ibte2(jj+2) = nblkstart
            ijte2(jj+2) = jbmax(nblkstart)
            ikte2(jj+2) = 1
            ibte1(jj+3) = nblkstart + 1
            ijte1(jj+3) = 1
            ikte1(jj+3) = 1
            ibte2(jj+3) = nblkend
            ijte2(jj+3) = 1
            ikte2(jj+3) = 1
          endif
        else
c         Sharp T.E.
          if (nblks.eq.3) then
            nsadj = 0
            ncopy = 2
            ibte1(1) = 2
            ijte1(1) = jbmax(2)
            ikte1(1) = 1
            ibte2(1) = 3
            ijte2(1) = 1
            ikte2(1) = 1
            ibte1(2) = 2
            ijte1(2) = 1
            ikte1(2) = 1
            ibte2(2) = 1
            ijte2(2) = jbmax(1)
            ikte2(2) = 1
          endif

c         Blunt T.E.         
          if (nblks.eq.4) then
            nsadj = 0
            ncopy = 4
            ibte1(1) = 2
            ijte1(1) = jbmax(2)
            ikte1(1) = 1
            ibte2(1) = 3
            ijte2(1) = 1
            ikte2(1) = 1
            ibte1(2) = 2
            ijte1(2) = jbmax(2)
            ikte1(2) = 1
            ibte2(2) = 4
            ijte2(2) = 1
            ikte2(2) = kbmax(4)
            ibte1(3) = 2
            ijte1(3) = 1
            ikte1(3) = 1
            ibte2(3) = 1
            ijte2(3) = jbmax(1)
            ikte2(3) = 1
            ibte1(4) = 2
            ijte1(4) = 1
            ikte1(4) = 1
            ibte2(4) = 4
            ijte2(4) = 1
            ikte2(4) = 1
          endif
        endif


c H-MESH TOPOLOGY :

c     New warning message - un-tested feature
c$$$        if (nblks.le.4) then
c$$$           write(*,*) 'Warning: possible error in autocop:
c$$$     >          blunt-TE for C-mesh has not been checked!'
c$$$        endif

        if (istgtyp(ii).eq.1.and.nblks.gt.4) then

c find the upper and lower blocks if the stagnation point is a le
          if (istgcnr(ii).eq.2) then
            iblkup = istgblk(ii)
            iblock = lblkpt(istgblk(ii),2)
            iblock = lblkpt(iblock,1)
            iblkdn = lblkpt(iblock,4)
          else
c     New warning message for stg point location
            if (istgcnr(ii).ne.3) then
               write(*,*) 'Warning: possible error in autocop:
     >              LE stg corner should be UL or LL'
            endif

            iblkdn = istgblk(ii)
            iblock = lblkpt(istgblk(ii),2)
            iblock = lblkpt(iblock,3)
            iblkup = lblkpt(iblock,4)
          endif
          
c
c set the copy blocks and points around the le
          ncopy = ncopy + 1
          ibte1(ncopy) = iblkup
          ijte1(ncopy) = 1
          ikte1(ncopy) = 1
          ibte2(ncopy) = lblkpt(iblkup,2)
          ijte2(ncopy) = jbmax(ibte2(ncopy))
          ikte2(ncopy) = 1

          ncopy = ncopy + 1
          ibte1(ncopy) = iblkdn
          ijte1(ncopy) = 1
          ikte1(ncopy) = kbmax(ibte1(ncopy))
          ibte2(ncopy) = lblkpt(iblkdn,2)
          ijte2(ncopy) = jbmax(ibte2(ncopy))
          ikte2(ncopy) = kbmax(ibte2(ncopy))

c
c Set the default inviscid singular point descriptors on boundary for bcsing
c
          if (.not.viscous) then
            nsing = nsing + 1
            ibs1(nsing) = iblkup
            js1(nsing) = 1
            ks1(nsing) = 1
            ibs2(nsing) = iblkdn
            js2(nsing) = 1
            ks2(nsing) = kbmax(iblkdn)
            javg1(nsing) = 2
            kavg1(nsing) = 1
            javg2(nsing) = 2
            kavg2(nsing) = kbmax(iblkdn)
          endif
c
        elseif (istgtyp(ii).eq.-1.and.nblks.gt.4) then

c find the upper block (and blunt te block if it exists)
c if the stagnation point is a te on the upper surface
          iblunt = 0
          if (istgcnr(ii).eq.1) then
            iblkup = istgblk(ii)
            iblock = lblkpt(istgblk(ii),4)
            iblock = lblkpt(iblock,1)
            if (ibctype(iblock,2).eq.1) then
              iblunt = iblock
              iblock = lblkpt(iblock,1)
            endif
            iblkdn = lblkpt(iblock,2)

c find the upper block (and blunt te block if it exists)
c if the stagnation point is a te on the lower surface
          else
c     New warning message for stg point location
            if (istgcnr(ii).ne.4) then
               write(*,*) 'Warning: possible error in autocop
     >              TE stg corner should be UR or LR'
            endif
            iblkdn = istgblk(ii)
            iblock = lblkpt(istgblk(ii),4)
            iblock = lblkpt(iblock,3)
            if (ibctype(iblock,2).eq.1) then
              iblunt = iblock
              iblock = lblkpt(iblock,3)
            endif
            iblkup = lblkpt(iblock,2)
          endif

          if (iblunt.eq.0) then

             ! set the copy blocks and points around the te
             ncopy = ncopy + 1
             ibte1(ncopy) = iblkup
             ijte1(ncopy) = jbmax(iblkup)
             ikte1(ncopy) = 1
             ibte2(ncopy) = lblkpt(iblkup,4)
             ijte2(ncopy) = 1
             ikte2(ncopy) = 1

             ! lower block too!
             ncopy = ncopy + 1
             ibte1(ncopy) = iblkdn
             ijte1(ncopy) = jbmax(iblkdn)
             ikte1(ncopy) = kbmax(iblkdn)
             ibte2(ncopy) = lblkpt(iblkdn,4)
             ijte2(ncopy) = 1
             ikte2(ncopy) = kbmax(ibte2(ncopy))

          else
             if (istgcnr(ii).eq.1) then
                ! blunt TE upper stagnation point

c                 set the copy blocks and points around the 
c                 te upper block
                ncopy = ncopy + 1
                ibte1(ncopy) = iblkup
                ijte1(ncopy) = jbmax(iblkup)
                ikte1(ncopy) = 1
                ibte2(ncopy) = lblkpt(iblkup,4)
                ijte2(ncopy) = 1
                ikte2(ncopy) = 1

c                 set the copy blocks and points around the 
c                 te iff it is blunt
                ncopy = ncopy + 1
                ibte1(ncopy) = iblkup
                ijte1(ncopy) = jbmax(ibte1(ncopy))
                ikte1(ncopy) = 1
                ibte2(ncopy) = iblunt
                ijte2(ncopy) = 1
                ikte2(ncopy) = kbmax(ibte2(ncopy))

             else
                ! blunt TE lower stagnation point

                ncopy = ncopy + 1
                ibte1(ncopy) = iblkdn
                ijte1(ncopy) = jbmax(iblkdn)
                ikte1(ncopy) = kbmax(iblkdn)
                ibte2(ncopy) = lblkpt(iblkdn,4)
                ijte2(ncopy) = 1
                ikte2(ncopy) = kbmax(ibte2(ncopy))
 
                ncopy = ncopy + 1
                ibte1(ncopy) = iblkdn
                ijte1(ncopy) = jbmax(ibte1(ncopy))
                ikte1(ncopy) = kbmax(ibte1(ncopy))
                ibte2(ncopy) = iblunt
                ijte2(ncopy) = 1
                ikte2(ncopy) = 1

             endif
          endif
      endif

 100  continue

      return
      end
