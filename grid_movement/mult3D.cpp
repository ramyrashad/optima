

#ifndef mult3D_H
#include "mult3D.h"
#endif

#include <iostream>
#include <iomanip>

//void read_mult3D (char *filename, vector <Node>& nodes, vector <Vol>& cells,
//		  vector<int>& imax, vector<int>& jmax, vector<int>& kmax,
//		  int& nblk)
void read_mult3D (char *filename, vector <Node>& nodes, vector <Vol>& cells,
		  int* &imax, int* &jmax, int* &kmax, int& nblk)
{
  int i, j, k, n, tmp;

  vector <double> x;
  vector <double> y;
  vector <double> z;

  ofstream fout;

  ifstream fin(filename, ios::in | ios::binary);
  if (! fin.is_open())
    {
      cout << "Error opening file grid file in function read_grid3D (grid.cpp)"
	   << endl;
      exit(2);
    }
 
  // Reads the number of blocks in file
  fin.read(reinterpret_cast < char * > (&nblk), sizeof(int));
  ByteSwap5(nblk);
  //  cout << "number of blks " << nblk << endl;

  //  jmax.resize(nblk);
  // kmax.resize(nblk);
  //imax.resize(nblk);
  imax = new int [nblk];
  jmax = new int [nblk];
  kmax = new int [nblk];

  i = 0;
  tmp = 0; 
  for (n=0; n<nblk; n++)
    {
      fin.read(reinterpret_cast < char * > (&imax[n]), sizeof(int));
      ByteSwap5(imax[n]);

      fin.read(reinterpret_cast < char * > (&jmax[n]), sizeof(int));
      ByteSwap5(jmax[n]);

      fin.read(reinterpret_cast < char * > (&kmax[n]), sizeof(int));
      ByteSwap5(kmax[n]);

      tmp += imax[n]*jmax[n]*kmax[n];
      i += (imax[n]-1)*(jmax[n]-1)*(kmax[n]-1);
    }

  nodes.resize(tmp);
  cells.resize(i);
  

  k = 0;
  for (i=0; i<nblk; i++)
    {

      tmp = imax[i]*jmax[i]*kmax[i];
      x.resize(tmp);
      y.resize(tmp);
      z.resize(tmp);

      for (j=0; j<tmp; j++)
	{
	  fin.read(reinterpret_cast < char * > (&x[j]), sizeof(double));
	  ByteSwap5(x[j]);
	}
      for (j=0; j<tmp; j++)
	{
	  fin.read(reinterpret_cast < char * > (&y[j]), sizeof(double));
	  ByteSwap5(y[j]);
	}
      for (j=0; j<tmp; j++)
	{
	  fin.read(reinterpret_cast < char * > (&z[j]), sizeof(double));
	  ByteSwap5(z[j]);
	}

      for (j=0; j<tmp; j++)
	{
	  nodes[k] = Node(x[j], y[j], z[j], k);
	  k++;
	}
    }

  /*  
  fout.open("coord.dat");
  for (i=0; i<nblk; i++)
    fout <<setw(15) << imax[i] << setw(15) << jmax[i] << setw(15) 
     << kmax[i] << endl;
  fout.close();
  */
    // exit(8);

  
  //  cout << "// find overlaps and mark boundary nodes " << endl;
  read_topo (filename, nodes, nblk, imax, jmax, kmax);


  //  cout << "form cells well  ... " << endl;   
  form_cells (nodes, nblk, imax, jmax, kmax, cells);

  /* fout.open("cells.test1.dat");
  j = (int) cells.size();
  for (i=0; i<j; i++)
    fout << cells[i];
  fout.close();
  exit(9);
  */

     /*
  fout.open("coord.dat");
  for (i=0; i<tmp; i++)
    fout <<setw(15) << x[i] << setw(15) << y[i] << setw(15) 
	 << z[i] << endl;
  */
   
  //cout << "number of nodes " << nodes.size() << endl;
  /*
  Node* overlap;
  fout.open("overlap.dat");
  for (i=0; i< nodes.size(); i++)
    {
      overlap = nodes[i].get_overlap();
      if (overlap != NULL)
	{
	fout << setw(9) << overlap->get_ID() << endl;
	//	cout << "in here not null " << i << endl;
	fout <<i << "  " << nodes[i];
	}
    }
  fout.close(); // exit(5);
  cout << "finished writing overlap" << endl;

  fout.open("coord.dat");
  for (i=0; i< nodes.size(); i++)
    {
      fout << nodes[i];
    }
  fout.close();  //exit(5);
  cout << "finished writing nodes" << endl;
  */   

  //  cout << "adding neighbours3D " << endl;
  add_neighbours3D (nodes, imax, jmax, kmax, nblk);
  // cout <<"finish adding neighbour " << endl;

  /*
  fout.open("nei.dat");
  Node **nei;  int nn;
  // checking connectivity
  for (i=0; i< nodes.size(); i++)
    {
      cout << "checking connectiivty of node " << i << "  out of " 
       << nodes.size() << endl;
      nn = nodes[i].get_num_nei();  
      //cout << "num_nei " << nn << endl; exit(7);
      nei = nodes[i].get_nei();
      fout << setw(9) << i;
      for (j=0; j<nn; j++)
	fout << setw(9) << nei[j]->get_ID();
      fout<<endl;
    }
  fout.close();
  exit(88);
  */
  /*
  cout << "got to here line 103 " << endl;
  char gridout [] = "test.g";
  write_mult3D(gridout, nodes, imax, jmax, kmax, nblk);
  cout << "finished writing out grid " << endl;  exit (7);
  */

  return;

}


void read_topo (const char* gridfile, vector <Node>& nodes, const int& nblk,
		int* &imax, int* &jmax, int* &kmax)
{
  char * topofile = strtok((char*) gridfile,".");
  strcat(topofile,".topo");
  //  cout << "opening topofile haha " << topofile << endl;

  int ii, jj, kk, imaxjmax;
  int ista, jsta, ksta, iend, jend, kend, dit1, dit2;
  int adblk, adista, adjsta, adksta, adiend, adjend, adkend; 
  Node* overlap;
  Node* overlap0;

  string bnd;
  ifstream fin (topofile);

  int j = 5000; // Maximum number of char expected in a single line in the header
  fin.ignore(j, '\n');
  fin.ignore(j, '\n');
  fin.ignore(j, '\n');
  
  int tmp=0, nsurf=0, i=0;

  int *ncumu = new int[nblk];
  ncumu[0] = 0;   // cumulative node
  ii = nblk - 1;
  for (i=0; i<ii; i++)
    ncumu[i+1] = ncumu[i] + imax[i]*jmax[i]*kmax[i];

  fin >> tmp;  
  //  cout << "tot num of blks " << tmp << endl;
  if (tmp != nblk)
    {
      cerr << "Wrong topology file, different number of blocks encountered " 
	   << nblk << "  " << endl;
      exit (9);
    }

  vector <int> ID (1);

  for (tmp=0; tmp<nblk; tmp++)
    {
      fin.ignore(j, '\n');
      fin.ignore(j, '\n');
      fin >> nsurf;
      //cout << "blk number " << nsurf << endl;

      fin >> nsurf;
      //cout << "imax of blk is " << nsurf << endl;
      if (nsurf != imax[tmp])
	{
	  cerr << "Wrong topology file, different imax encountered " 
	       << imax[tmp] << "  " << nsurf << endl;
	  exit (9);
	}
      fin >> nsurf;
      //cout << "jmax is " << nsurf << endl;
      if (nsurf != jmax[tmp])
	{
	  cerr << "Wrong topology file, different jmax encountered " 
	       << jmax[tmp] << "  " << nsurf << endl;
	  exit (9);
	}
      fin >> nsurf;
      //cout << "kmax is " << nsurf << endl;
      if (nsurf != kmax[tmp])
	{
	  cerr << "Wrong topology file, different kmax encountered " 
	       << kmax[tmp] << "  " << nsurf << endl;
	  exit (9);
	}
      
      fin >> nsurf;
      fin.ignore(j, '\n');      
      fin.ignore(j, '\n');
      
      imaxjmax = imax[tmp]*jmax[tmp];

      for (i=0; i<nsurf; i++)
	{
	  fin >> bnd;
	  //  cout << "bnd is " << bnd <<endl;
	  
	  fin >> ista >> jsta >> ksta >> iend >> jend >> kend >> dit1;
	  //cout << ista << " " << jsta << "  " << ksta << "  "
	  // << iend << " " << jend << "  " << kend << endl;
	  
	  if (bnd == "CON")
	    {
	      fin >> adblk >> adista >> adjsta >> adksta >> adiend 
		  >> adjend >> adkend >> dit1 >> dit2;

	      //change the blk numbering to starting at 0
	      adblk = adblk-1;

	      //cout << adblk << "  " << adista << " " << adjsta << "  " 
	      //  << adksta << "  " << adiend << " " << adjend << "  " 
	      //   << adkend << "  " << dit1 << endl;
	      
	      if (adblk < tmp)
		{
		  // cout << adista << " " << adiend <<"  " << adjsta << "  "
		  //   << adjend << endl;  exit (3);
		  find_overlap(imax, jmax, ncumu, jsta, jend, adista, 
			       adjsta, adksta, adiend, adjend, adkend, 
		  	       adblk, dit1, dit2, ID);
		  // cout << ID[0] << endl;  exit(8);
		  
		  // ofstream fout("dit.dat", ios::app);
		  // fout << "blk  " << tmp << " adblk-1  " << adblk << endl;
		  //if (ksta > kend || ista >iend || jsta > jend)
		  //exit (32);

		  dit1 = 0;
		  for (kk=ksta-1; kk<kend; kk++)
		    {
		      for (jj=jsta-1; jj<jend; jj++)
			{
			  adblk = jj*imax[tmp] + kk*imaxjmax + ncumu[tmp];

			  for (ii=ista-1; ii<iend; ii++)
			    {
			      overlap = nodes[ID[dit1]].get_overlap();
			      overlap0 = nodes[ii+adblk].get_overlap();
			      if (overlap0 != NULL)
				{
				  dit2 = overlap0->get_ID();
				  if (overlap != NULL)
				    {
				      if (dit2 < overlap->get_ID())
					{
					  nodes[ID[dit1]].overlaps(overlap0);
					  //if (nodes[ID[dit1]].if_surf() == true ||
					  //  overlap->if_surf()==true)
					  //overlap0->is_surf();
					  overlap->overlaps(overlap0);
					}
				      else if (dit2 > overlap->get_ID() || dit2 > ID[dit1])
					{
					  nodes[ii+adblk].overlaps(overlap);
					  //if (nodes[ii+adblk].if_surf() == true ||
					  //     overlap0->if_surf()==true)
					  //overlap->is_surf();
					  overlap0->overlaps(overlap);
					}
				    }
				  else 
				    { 
				      if (dit2 < ID[dit1])
					{
					  //if (nodes[ID[dit1]].if_surf()==true)
					  //overlap0->is_surf();
					  nodes[ID[dit1]].overlaps(overlap0);
					}
				      else if (dit2 > ID[dit1])
					{
					  nodes[ii+adblk].overlaps(&nodes[ID[dit1]]);
					  //if (nodes[ID[dit1]].if_surf() == true ||
					  //  overlap0->if_surf()==true)
					  //nodes[ID[dit1]].is_surf();
					  overlap0->overlaps(&nodes[ID[dit1]]);
					}
				    }
				}
			      else
			      {
				  if (overlap != NULL)
				    {
				      //if ( nodes[ii+adblk].if_surf() == true )
				      //overlap->is_surf();
				      nodes[ii+adblk].overlaps(overlap);
				      // if (overlap0 != NULL)
				      //{
				      //  if (overlap0->get_ifoil()>0)
				      //    overlap->is_surf();
				      //  overlap0->overlaps(overlap);
				      //}
				    }
				  else
				    {
				      //if (nodes[ii+adblk].if_surf() == true)
				      //nodes[ID[dit1]].is_surf();
				      nodes[ii+adblk].overlaps(&nodes[ID[dit1]]);
				      //if (overlap0 != NULL)
				      //{
				      //  if (overlap0->get_ifoil() > 0)
				      //    nodes[ID[dit1]].is_surf();
				      //  overlap0->overlaps(&nodes[ID[dit1]]);
				      //}
				    }
			      }

			      dit1++;
			    }
			}
		    }
		  // fout.close();
		}
	    }
	  else  // bnd_type is wall
	    {
	      
	      for (kk=ksta-1; kk<kend; kk++)
		{
		  for (jj=jsta-1; jj<jend; jj++)
		    {
		      dit2 = jj*imax[tmp] + kk*imaxjmax + ncumu[tmp];
		      for (ii=ista-1; ii<iend; ii++)
			{
			  nodes[dit2+ii].is_surf();
			  nodes[dit2+ii].set_ifoil(dit1);
			  /*
			  overlap = nodes[dit2+ii].get_overlap();
			  if (overlap != NULL)
			    {
			      overlap->is_surf();
			      overlap->set_ifoil(dit1);
			      }*/
			}
		    }
		}
	    }
	  
	}
      //exit (22);

    }

  fin.close();

  jj = (int) nodes.size();
  for (ii=0; ii<jj; ii++)
    {
      overlap = nodes[ii].get_overlap();
      if (overlap != NULL)
	{
	  if (nodes[ii].if_surf()==true)
	    overlap->is_surf();
	  else
	    nodes[ii].is_surf();
	}
    }

  delete [] ncumu;  ncumu = NULL;

  return;
} 



void find_overlap(int* &imax, int* &jmax,
		  int* &ncumu, const int& jsta, const int& jend, 
		  const int& adista, const int& adjsta, const int& adksta, 
		  const int& adiend, const int& adjend, const int& adkend, 
		  const int& adblk, const int& dit1, const int& dit2, 
		  vector <int>& ID)
{
  // icomp stores the orientation of the connecting blk
  // icomp = 1 (i-j plane)
  // icomp = 2 (i-k plane)
  // icomp = 3 (j-k plane)

  int nnode, i, j, k, count;

  //  cout << "adblk is " << adblk << endl;
  //  cout << "dits " << dit1 <<  "   " << dit2 << endl;
  count = 0;

  //  connects to i-j plane of adj. blk
  if ( (dit1 == 0 && dit2 == 1 && jsta != jend) ||
       (dit1 == 1 && dit2 == 0 && jsta == jend) )
    {
      nnode = abs( (adiend - adista + 1) * (adjend - adjsta + 1) );
      ID.resize(nnode);	  

      //cout << "in 0 1, adista, adjsta ... " << adista << "  " << adjsta
      //   << "  " << adksta << "  " << adiend << "  " << adjend << endl;

      // cumulative number of nodes in the k-dir
      nnode = (adksta - 1)*imax[adblk]*jmax[adblk];
      
      // check if there is index reversal
      if (adista < adiend && adjsta < adjend)
	{
	  //cout << "should have no index reverseal :"<< endl;
	  for (j=adjsta-1; j<adjend; j++)
	    {
	      k = j*imax[adblk] + nnode;
	      for (i=adista-1; i<adiend; i++)
		{
		  //cout << "line 368 " << i << "  " << j << "  " << nnode <<"  "
		  // << i + j*imax[adblk-1] + nnode << endl; 
		  ID[count] = i + k;
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adista > adiend && adjsta < adjend)
	{
	  for (j=adjsta-1; j<adjend; j++)
	    {
	      k = j*imax[adblk] + nnode;
	      for (i=adista-1; i>=adiend-1; i--)
		{
		  ID[count] = i + k;
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adista < adiend && adjsta > adjend)
	{
	  for (j=adjsta-1; j>=adjend-1; j--)
	    {
	      k = j*imax[adblk] + nnode;
	      for (i=adista-1; i<adiend; i++)
		{
		  ID[count] = i + k;
		  count ++;
		}
	    }
	}
      else if (adista > adiend && adjsta > adjend)
	{
	  for (j=adjsta-1; j>=adjend-1; j--)
	    {
	      k = j*imax[adblk] + nnode;
	      for (i=adista-1; i>=adiend-1; i--)
		{
		  ID[count] = i + k;
		  count ++;
		}
	    }
	}
    }
  //  connects to j-i plane of adj. blk
  else if ( (dit1 == 1 && dit2 == 0 && jsta != jend) ||
	    (dit1 == 0 && dit2 == 1 && jsta == jend) )
    {
      nnode = abs( (adiend - adista + 1) * (adjend - adjsta + 1) );
      ID.resize(nnode);	  

      // cumulative number of nodes in the k-dir
      nnode = (adksta - 1)*imax[adblk]*jmax[adblk];

      // no index reversal
      if (adista < adiend && adjsta < adjend)
	{
	  for (i=adista-1; i<adiend; i++)
	    {
	      k = i + nnode;
	      for (j=adjsta-1; j<adjend; j++)
		{
		  ID[count] = j*imax[adblk] + k;
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adista > adiend && adjsta < adjend)
	{
	  for (i=adista-1; i>=adiend-1; i--)
	    {
	      k = i + nnode;
	      for (j=adjsta-1; j<adjend; j++)
		{
		  ID[count] = j*imax[adblk] + k;
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adista < adiend && adjsta > adjend)
	{
	  for (i=adista-1; i<adiend; i++)
	    {
	      k = i + nnode;
	      for (j=adjsta-1; j>=adjend-1; j--)
		{
		  ID[count] = j*imax[adblk] + k;
		  count ++;
		}
	    }
	}
      else if (adista > adiend && adjsta > adjend)
	{
	  for (i=adista-1; i>=adiend-1; i--)
	    {
	      k = i + nnode;
	      for (j=adjsta-1; j>=adjend-1; j--)
		{
		  ID[count] = j*imax[adblk] + k;
		  count ++;
		}
	    }
	}
    }
  //  connects to j-k plane of adj. blk
  else if ( (dit1 == 1 && dit2 == 2 && jsta != jend ) || 
	    (dit1 == 2 && dit2 == 1 && jsta == jend ) )   
    {
      nnode = abs( (adjend - adjsta + 1) * (adkend - adksta + 1) );
      ID.resize(nnode);
	  
      // cumulative number of nodes in the k-dir
      nnode = jmax[adblk]*imax[adblk];

      // there is no index reversal
      if (adksta < adkend && adjsta < adjend)
	{
	  for (k=adksta-1; k<adkend; k++)
	    {
	      i = adista-1 + k*nnode;
	      for (j=adjsta-1; j<adjend; j++)
		{
		  ID[count] = i + j*imax[adblk];
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adksta > adkend && adjsta < adjend)
	{
	  for (k=adksta-1; k>=adkend-1; k--)
	    {
	      i = adista-1 + k*nnode;
	      for (j=adjsta-1; j<adjend; j++)
		{
		  ID[count] = i + j*imax[adblk];
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adksta < adkend && adjsta > adjend)
	{
	  for (k=adksta-1; k<adkend; k++)
	    {
	      i = adista-1 + k*nnode;
	      for (j=adjsta-1; j>=adjend-1; j--)
		{
		  ID[count] = i + j*imax[adblk];
		  count ++;
		}
	    }
	}
      else if (adksta > adkend && adjsta > adjend)
	{
	  for (k=adksta-1; k>=adkend-1; k--)
	    {
	      i = adista-1 + k*nnode;
	      for (j=adjsta-1; j>=adjend-1; j--)
		{
		  ID[count] = i + j*imax[adblk];
		  count ++;
		}
	    }
	}
    }
  //  connects to k-j plane of adj. blk
  else if ( (dit1 == 2 && dit2 == 1 && jsta != jend)  ||
	    (dit1 == 1 && dit2 == 2 && jsta == jend) )
    {
      nnode = abs( (adjend - adjsta + 1) * (adkend - adksta + 1) );
      ID.resize(nnode);
	  
      // cumulative number of nodes in the k-dir
      nnode = jmax[adblk]*imax[adblk];
      
	  // check if there is index reversal
	  if (adksta < adkend && adjsta < adjend)
	    {
	      for (j=adjsta-1; j<adjend; j++)
		{
		  i = adista-1 + j*imax[adblk];
		  for (k=adksta-1; k<adkend; k++)
		    {
		      ID[count] = i +  k*nnode;
		      count ++;
		    }
		}
	    }
	  // check if there is index reversal
	  else if (adksta > adkend && adjsta < adjend)
	    {
	      for (j=adjsta-1; j<adjend; j++)
		{
		  i = adista-1 + j*imax[adblk];
		  for (k=adksta-1; k>=adkend-1; k--)
		    {
		      ID[count] = i + k*nnode;
		      count ++;
		    }
		}
	    }
	  // check if there is index reversal
	  else if (adksta < adkend && adjsta > adjend)
	    {
	      for (j=adjsta-1; j>=adjend-1; j--)
		{
		  i = adista-1 + j*imax[adblk];
		  for (k=adksta-1; k<adkend; k++)
		    {
		      ID[count] = i + k*nnode;
		      count ++;
		    }
		}
	    }
	  else if (adksta > adkend && adjsta > adjend)
	    {
	      for (j=adjsta-1; j>=adjend-1; j--)
		{
		  i = adista-1 + j*imax[adblk];
		  for (k=adksta-1; k>=adkend-1; k--)
		    {
		      ID[count] = i + k*nnode;
		      count ++;
		    }
		}
	    }
	}
  else if ( (dit1 == 2 && dit2 == 0 && jsta != jend) || 
	    (dit1 == 0 && dit2 == 2 && jsta == jend) )
    {  

      //  connects to k-i plane of adj. blk
      nnode = abs( (adiend - adista + 1) * (adkend - adksta + 1) );
      ID.resize(nnode);	  

      // cumulative number of nodes in the k-dir
      nnode = imax[adblk]*jmax[adblk];
      j = (adjsta-1)*imax[adblk];

      // no index reversal
      if (adista < adiend && adksta < adkend)
	{
	  for (i=adista-1; i<adiend; i++)
	    {
	      for (k=adksta-1; k<adkend; k++)
		{
		  ID[count] = i + j + k*nnode;
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adista < adiend && adksta > adkend)
	{
	  for (i=adista-1; i<adiend; i++)
	    {
	      for (k=adksta-1; k>=adkend-1; k--)
		{
		  ID[count] = i + j + k*nnode;
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adista > adiend && adksta < adkend)
	{
	  // cout << "line 628 " << endl; exit (9);
	  for (i=adista-1; i>=adiend-1; i--)
	    {
	      for (k=adksta-1; k<adkend; k++)
		{
		  ID[count] = i + j + k*nnode;
		  count ++;
		}
	    }
	}
      else if (adista > adiend && adksta > adkend)
	{
	  for (i=adista-1; i>=adiend-1; i--)
	    {
	      for (k=adksta-1; k>=adkend-1; k--)
		{
		  ID[count] = i + j + k*nnode;
		  count ++;
		}
	    }
	}
    }
   else if ( (dit1 == 2 && dit2 == 0 && jsta == jend) || 
	    (dit1 == 0 && dit2 == 2 && jsta != jend) )
    {  

      //  connects to k-i plane of adj. blk
      nnode = abs( (adiend - adista + 1) * (adkend - adksta + 1) );
      ID.resize(nnode);	  

      //cout << "line 658 " << endl; 
      //cumulative number of nodes in the k-dir
      nnode = imax[adblk]*jmax[adblk];
      j = (adjsta-1)*imax[adblk];

      // no index reversal
      if (adista < adiend && adksta < adkend)
	{
	  // cout << "in the 1st place" << endl;
	  for (k=adksta-1; k<adkend; k++)
	    {
	      for (i=adista-1; i<adiend; i++)
		{
		  ID[count] = i + j + k*nnode;
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adista < adiend && adksta > adkend)
	{
	  //cout << "in the 2nd place" << endl;  exit(9);
	  for (k=adksta-1; k>=adkend-1; k--)
	    {
	      for (i=adista-1; i<adiend; i++)
		{
		  ID[count] = i + j + k*nnode;
		  count ++;
		}
	    }
	}
      // check if there is index reversal
      else if (adista > adiend && adksta < adkend)
	{
	  //cout << "in the third place " << endl;
	  for (k=adksta-1; k<adkend; k++)
	    {
	      for (i=adista-1; i>=adiend-1; i--)
		{
		  ID[count] = i + j + k*nnode;
		  count ++;
		}
	    }
	}
      else if (adista > adiend && adksta > adkend)
	{
	  for (k=adksta-1; k>=adkend-1; k--)
	    {
	      for (i=adista-1; i>=adiend-1; i--)
		{
		  ID[count] = i + j + k*nnode;
		  count ++;
		}
	    }
	}
    }

  
  // ofstream fout("overlap.dat");
  //cout << "ID[0] ncumu[adblk] " << ncumu[adblk]<< endl;
  //cout << ID[0] << endl;

  for (i=0; i<count; i++)
    {
      //fout << ID[i] << endl;
      ID[i] = ID[i] + ncumu[adblk];
    }
  // fout << ncumu[adblk] <<" ncumu[i] " << endl;
  //fout.close();  exit(9);
  //cout << "exiting read_topo " << endl;

  return; 
      
}



void change_dihedral (vector<Node>& nodes, int* &imax, int* &jmax, 
		      int* &kmax, const int& nblk, double& delta)
{
  
  double* coord;
   double* coordp;
  Node* overlap;
  
  int ii, jj, kk;

  double* t;
  int *ncumu = new int[nblk];
  ncumu[0] = 0;   // cumulative node
  jj = nblk - 1;
  for (ii=0; ii<jj; ii++)
    ncumu[ii+1] = ncumu[ii] + imax[ii]*jmax[ii]*kmax[ii];

  // blk 34
  kk = (kmax[33]-1)*imax[33]*jmax[33] + ncumu[33];
  
  for (ii=1; ii<imax[33]; ii++)
    {
      nodes[ii + imax[33] + kk].is_surf();

      coord = nodes[ii + imax[33] + kk].get_coord();
      coordp = nodes[ii-1 + imax[33] + kk].get_coord();

      coord[1] = -tan(delta)*(coord[2]-coordp[2]) + coordp[1];  

      overlap = nodes[ii + imax[33] + kk].get_overlap();
      if (overlap != NULL)
	overlap->set_coord(coord);
    }
    	
  // blk 35
  kk = ncumu[34];
  for (ii=1; ii<imax[34]; ii++)
    {
      nodes[ii + imax[34] + kk].is_surf();

      coord = nodes[ii + imax[34] + kk].get_coord();
      coordp = nodes[ii-1 + imax[34] + kk].get_coord();

      coord[1] = -tan(delta)*(coord[2]-coordp[2]) + coordp[1];  

      overlap = nodes[ii + imax[34] + kk].get_overlap();
      if (overlap != NULL)
	overlap->set_coord(coord);
    }



  // blk 36
  kk = (kmax[35]-1)*imax[35]*jmax[35] + ncumu[35];
  for (jj=1; jj<jmax[35]; jj++)
    {
      for (ii=0; ii<imax[35]; ii++)
	{
	  nodes[ii + jj*imax[35] + kk].is_surf();

	  coord = nodes[ii + jj*imax[35] + kk].get_coord();
	  coordp = nodes[ ii + (jj-1)*imax[35] + kk].get_coord();

	  coord[1] = -tan(delta)*(coord[2]-coordp[2]) + coordp[1];  

	  overlap = nodes[ii + jj*imax[35] + kk].get_overlap();
	  if (overlap != NULL)
	    overlap->set_coord(coord);
	}
    }

  // blk 37
  kk = ncumu[36];
  for (jj=1; jj<jmax[36]; jj++)
    {
      for (ii=0; ii<imax[36]; ii++)
	{
	  nodes[ii + jj*imax[36] + kk].is_surf();

	  coord = nodes[ii + jj*imax[36] + kk].get_coord();
	  coordp = nodes[ ii + (jj-1)*imax[36] + kk].get_coord();
	  coord[1] = -tan(delta)*(coord[2]-coordp[2]) + coordp[1];  
	  overlap = nodes[ii + jj*imax[36] + kk].get_overlap();
	  if (overlap != NULL)
	    overlap->set_coord(coord);
	}
    }
		    
    
	  

  //blk 38
  for (jj=0; jj<jmax[37]; jj++)
    {
      kk = jj*imax[37] + ncumu[37];
      nodes[1 + kk].is_surf();

      overlap = nodes[kk].get_overlap();
      if (overlap != NULL)
	coord = overlap->get_coord();
      else
	coord = nodes[kk].get_coord();
			    
      overlap = nodes[2 + kk].get_overlap();
      if (overlap != NULL)
	coordp = overlap->get_coord();
      else
	coordp = nodes[2 + kk].get_coord();
			    
      t = nodes[1 + kk].get_coord();
			  
      t[1] = 0.5*(coord[1]+coordp[1]);  			  

      overlap = nodes[1 + kk].get_overlap();
      if (overlap != NULL)
	overlap->set_coord(t);

    }

  delete [] ncumu;  ncumu = NULL;
  
  return;
}


void write_mult3D(const char* gridout, const vector <Node>& node, 
		  int* &jmax,  int* &kmax,  int* &mmax,  int& nblk)
{
  int i (0), j (0), k (0), count (0);
  double *xy;

  vector <double> x ; 
  vector <double> y ; 
  vector <double> z ;

  ofstream fout(gridout, ios::out | ios::binary);
  fout.write(reinterpret_cast < char * > (&nblk), sizeof(int));

  for (i=0; i<nblk; i++)
    {
      fout.write(reinterpret_cast < char * > (&jmax[i]), sizeof(int));
      fout.write(reinterpret_cast < char* > (&kmax[i]), sizeof(int));
      fout.write(reinterpret_cast < char* > (&mmax[i]), sizeof(int));
    }   

  
  for (i=0; i<nblk; i++)
    {
  
      k = jmax[i]*kmax[i]*mmax[i];
      x.resize(k);         y.resize(k);    z.resize(k);
 
      for (j=0; j<k; j++)
	{
	  xy = node[count].get_coord();
	  x[j] = xy[0];   y[j] = xy[1];  z[j] = xy[2];
	  count ++;

	}

      for (j=0; j<k; j++)
	fout.write (reinterpret_cast < char * > (&x[j]), sizeof(double));
	

      for (j=0; j<k; j++)
	fout.write (reinterpret_cast < char * > (&y[j]), sizeof(double));
	

      for (j=0; j<k; j++)
	fout.write (reinterpret_cast < char * > (&z[j]), sizeof(double));
      
    }	

  fout.close();

  return;
}


void write_mult3D_small_endian(const char* gridout, const vector <Node>& node, 
		  int* &jmax,  int* &kmax,  int* &mmax,  int& nblk)
{
  int i (0), j (0), k (0), count (0);
  double *xy;

  vector <double> x ; 
  vector <double> y ; 
  vector <double> z ;

  ofstream fout(gridout, ios::out | ios::binary);

  ByteSwap5(nblk);
  fout.write(reinterpret_cast < char * > (&nblk), sizeof(int));

  ByteSwap5(nblk);
  for (i=0; i<nblk; i++)
    {
      ByteSwap5(jmax[i]);
      fout.write(reinterpret_cast < char * > (&jmax[i]), sizeof(int));
      ByteSwap5(jmax[i]);

      ByteSwap5(kmax[i]);
      fout.write(reinterpret_cast < char* > (&kmax[i]), sizeof(int));
      ByteSwap5(kmax[i]);

      ByteSwap5(mmax[i]);
      fout.write(reinterpret_cast < char* > (&mmax[i]), sizeof(int));
      ByteSwap5(mmax[i]);
    }   

  
  for (i=0; i<nblk; i++)
    {
  
      k = jmax[i]*kmax[i]*mmax[i];
      x.resize(k);         y.resize(k);    z.resize(k);
 
      for (j=0; j<k; j++)
	{
	  xy = node[count].get_coord();
	  x[j] = xy[0];   y[j] = xy[1];  z[j] = xy[2];
	  ByteSwap5(x[j]);  ByteSwap5(y[j]);  ByteSwap5(z[j]);
	  count ++;

	}

      for (j=0; j<k; j++)
	fout.write (reinterpret_cast < char * > (&x[j]), sizeof(double));
	

      for (j=0; j<k; j++)
	fout.write (reinterpret_cast < char * > (&y[j]), sizeof(double));
	

      for (j=0; j<k; j++)
	fout.write (reinterpret_cast < char * > (&z[j]), sizeof(double));
      
    }	

  fout.close();

  return;
}


void change_sweep (vector<Node>& nodes, int* &imax, int* &jmax, 
	    int* &kmax, const int& nblk, double& delta)
{
  
  double* coord;
  double* coordp;
  Node* overlap;
  
  int ii, jj, kk;

  double* t;
  int *ncumu = new int[nblk];
  ncumu[0] = 0;   // cumulative node
  jj = nblk - 1;
  for (ii=0; ii<jj; ii++)
    ncumu[ii+1] = ncumu[ii] + imax[ii]*jmax[ii]*kmax[ii];

  // blk 34
  kk = (kmax[33]-1)*imax[33]*jmax[33] + ncumu[33];
  
  for (ii=1; ii<imax[33]; ii++)
    {
      nodes[ii + imax[33] + kk].is_surf();

      coord = nodes[ii + imax[33] + kk].get_coord();
      coordp = nodes[ii-1 + imax[33] + kk].get_coord();

      coord[0] = tan(delta)*(coord[2]-coordp[2]) + coordp[0];  

      overlap = nodes[ii + imax[33] + kk].get_overlap();
      if (overlap != NULL)
	overlap->set_coord(coord);
    }
    	
  // blk 35
  kk = ncumu[34];
  for (ii=1; ii<imax[34]; ii++)
    {
      nodes[ii + imax[34] + kk].is_surf();

      coord = nodes[ii + imax[34] + kk].get_coord();
      coordp = nodes[ii-1 + imax[34] + kk].get_coord();

      coord[0] = tan(delta)*(coord[2]-coordp[2]) + coordp[0];  

      overlap = nodes[ii + imax[34] + kk].get_overlap();
      if (overlap != NULL)
	overlap->set_coord(coord);
    }



  // blk 36
  kk = (kmax[35]-1)*imax[35]*jmax[35] + ncumu[35];
  for (jj=1; jj<jmax[35]; jj++)
    {
      for (ii=0; ii<imax[35]; ii++)
	{
	  nodes[ii + jj*imax[35] + kk].is_surf();

	  coord = nodes[ii + jj*imax[35] + kk].get_coord();
	  coordp = nodes[ ii + (jj-1)*imax[35] + kk].get_coord();

	  coord[0] = tan(delta)*(coord[2]-coordp[2]) + coordp[0];  

	  overlap = nodes[ii + jj*imax[35] + kk].get_overlap();
	  if (overlap != NULL)
	    overlap->set_coord(coord);
	}
    }

  // blk 37
  kk = ncumu[36];
  for (jj=1; jj<jmax[36]; jj++)
    {
      for (ii=0; ii<imax[36]; ii++)
	{
	  nodes[ii + jj*imax[36] + kk].is_surf();

	  coord = nodes[ii + jj*imax[36] + kk].get_coord();
	  coordp = nodes[ ii + (jj-1)*imax[36] + kk].get_coord();
	  coord[0] = tan(delta)*(coord[2]-coordp[2]) + coordp[0];  
	  overlap = nodes[ii + jj*imax[36] + kk].get_overlap();
	  if (overlap != NULL)
	    overlap->set_coord(coord);
	}
    }
		    
    
	  

  //blk 38
  for (jj=0; jj<jmax[37]; jj++)
    {
      kk = jj*imax[37] + ncumu[37];
      nodes[1 + kk].is_surf();

      overlap = nodes[kk].get_overlap();
      if (overlap != NULL)
	coord = overlap->get_coord();
      else
	coord = nodes[kk].get_coord();
			    
      overlap = nodes[2 + kk].get_overlap();
      if (overlap != NULL)
	coordp = overlap->get_coord();
      else
	coordp = nodes[2 + kk].get_coord();
			    
      t = nodes[1 + kk].get_coord();
			  
      t[0] = 0.5*(coord[0]+coordp[0]);  			  

      overlap = nodes[1 + kk].get_overlap();
      if (overlap != NULL)
	overlap->set_coord(t);

    }

  delete [] ncumu;  ncumu = NULL;
  
  return;
}

void change_twist (vector<Node>& nodes, int* &imax, int* &jmax, 
	    int* &kmax, const int& nblk, double& delta)
{
  
  double* coord;
  double* coordp;
  Node* overlap;
  
  int ii, jj, kk;
  double incr;

  double* t;
  int *ncumu = new int[nblk];
  ncumu[0] = 0;   // cumulative node
  jj = nblk - 1;
  for (ii=0; ii<jj; ii++)
    ncumu[ii+1] = ncumu[ii] + imax[ii]*jmax[ii]*kmax[ii];

  // find AOA at root
  coordp = nodes[imax[33] + ncumu[33]].get_coord();  // leading edge
  coord = nodes[imax[34] + ncumu[34]].get_coord();  // trailing edge
  double tmp = atan( (coordp[1]-coord[1])/(coordp[0]-coord[0]) );
  cout <<"AOA at root is " << tmp/3.14*180 << "  deg" << endl;  
  delta += tmp;
  cout << "new alpha " << delta/3.14*180 << endl;   exit(9);

  double* root = nodes[imax[33] + ncumu[33]].get_coord();  //root
  t = nodes[imax[33] +imax[33]-1 + ncumu[33]].get_coord(); // tip
  tmp = t[2] - coordp[2];  // span

  // blk 36
  kk = (kmax[35]-1)*imax[35]*jmax[35] + ncumu[35];
  for (jj=1; jj<jmax[35]; jj++)
    {
      ii = jj + imax[33] + (kmax[33]-1)*imax[33]*jmax[33] + ncumu[33];
      coord = nodes[ii].get_coord();  // LE present section on blk 34
      coordp = nodes[jj + imax[34] + ncumu[34]].get_coord();  // TE pres sect
      incr = delta*(coord[2]-root[2])/tmp;  // increment in AOA

      coord[1] = -tan(incr)*(coordp[0]-coord[0])+coordp[1];  // twist this section
      overlap = nodes[ii].get_overlap();
      if (overlap != NULL)
	overlap->set_coord(coord);

      for (ii=imax[35]-2; ii>=1; ii--)
	{
	  t = nodes[ii + jj*imax[35] + kk].get_coord();
	  nodes[ii + jj*imax[35] + kk].is_surf();

	  t[1] += tan(incr)*(t[2]-coordp[2]);

	  overlap = nodes[ii + jj*imax[35] + kk].get_overlap();
	  if (overlap != NULL)
	    overlap->set_coord(coord);

	  t = nodes[ii + jj*imax[36] + ncumu[36]].get_coord();
	  nodes[ii + jj*imax[36] + ncumu[36]].is_surf();

	  t[1] += tan(incr)*(t[2]-coordp[2]);

	  overlap = nodes[ii + jj*imax[36] + ncumu[36]].get_overlap();
	  if (overlap != NULL)
	    overlap->set_coord(coord);	  

	}
    }

  //blk 38
  for (jj=0; jj<jmax[37]; jj++)
    {
      kk = jj*imax[37] + ncumu[37];
      nodes[1 + kk].is_surf();

      overlap = nodes[kk].get_overlap();
      if (overlap != NULL)
	coord = overlap->get_coord();
      else
	coord = nodes[kk].get_coord();
			    
      overlap = nodes[2 + kk].get_overlap();
      if (overlap != NULL)
	coordp = overlap->get_coord();
      else
	coordp = nodes[2 + kk].get_coord();
			    
      t = nodes[1 + kk].get_coord();
			  
      t[1] = 0.5*(coord[1]+coordp[1]);  			  

      overlap = nodes[1 + kk].get_overlap();
      if (overlap != NULL)
	overlap->set_coord(t);

    }

  delete [] ncumu;  ncumu = NULL;
  
  return;
}

