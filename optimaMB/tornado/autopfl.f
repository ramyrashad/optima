c *******************************************************************
c ** subroutine to automatically generate the points in the domain **
c ** that are used to determine the upstream pressure for the wind **
c ** tunnel case - the pressure of the two points is averaged and  **
c ** used in the upstream boundary condition.                      **
c *******************************************************************
c calling routine: ioall
c
c
c
      subroutine autopfl

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer nbs,iblock
c
c ** set pressure averageing points to be in upper and lower **
c ** left points in the upper and lower blocks respectively. **
c
      if (mg .or. gseq) then
        nbs=nblkstot
      else
        nbs=nblks
      endif

      npavg = 2
      npblk(1) = 1
      npj(1) = nhalo + 2
      npk(1) = nhalo + jbmax(1)
c
      do 100 iblock=2,nbs
        if (ibctype(iblock,1).eq.2.and.ibctype(iblock,2).eq.2) then
          npblk(2) = iblock
          npj(2) = nhalo + 2
          npk(2) = nhalo + 1
        endif
  100 continue
c
      return
      end
