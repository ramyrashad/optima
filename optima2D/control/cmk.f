c***********************************************************************
c 
c     Contents:
c     --------
c     cmk     : driver for Cuthill-McKee reordering 
c     mddlia  : convert from a modified csr storage to a csr one
c     mdiadl  : convert from a csr storage to a modified csr one
c
c***********************************************************************
      subroutine cmk (ireord, nnzero, n, irenu, ja, ia, nbvoi, iout,
     &      nw, izs, ier, jdim, kdim)
c.......................................................................
c
c     We reorder the vertices in a symmetric graph. The chosen method
c     is indicated by IREORD. The structure of the matrix is not 
c     modified.
c
c     On entry:
c     --------
c     I*4 IREORD  : Determines the method to be used.
c                      IREORD = 1: Cuthill-McKee algorithm
c                      IREORD = 2: Reverse Cuthill-McKee algorithm
c     I*4  NNZERO : Number of nonzero coefficients in the matrix = 
c                   N + Cardinality of E (we include here the pairs of 
c                   type (vi,vi), not included in E).
c     I*4  N      : Number of vertices in V (order of the matrix).
c     I*4  IA     : Vector of length N+1: ia(1) = 1, ia(N+1) = NNZERO+1. 
c                   Nombre cumulated de neighbours of each vertex.
c     I*4  JA     : Vector of length NNZERO. Neighbours of each vertex.
c                   ia(i) <= j < ia(i+1), and ja(j) = k  <==>  (vi,vk) 
c                   is in E (initial ordering of the nodes). 
c                   (CSR storage).
c     I*4 IOUT    : impression parameter. If 0 < iout < 100, we print
c                   comments and error messages on unit IOUT.  
c 
c     On return: 
c     --------- 
c     I*4  IRENU  : Vector of length N. Final ordre of the vertices.
c     I*4  IER    : integer. Error message. Normal return ier = 0.
c
c
c     Work space:
c     ----------
c     I*4  NBVOI  : Integer vector of length N.
c     I*4  IZS    : integer vector, already initialized.
c
c     Reference for comments: 
c     ----------------------
c     Laura C. Dutto (1993), 'The effect of ordering on preconditioned
c     GMRES algorithm, for solving the compressible Navier-Stokes
c     equations', Int. J. Numer. Methods Eng., Vol 36, pp. 457-497.
c
c     written by: Laura C. Dutto - e-mail: dutto@cerca.umontreal.ca
c     April 1991 - Update: March 1994
c     minor modifications: marian nemec, july 2000
c     root node modifications: john gatis, march 2006
c.......................................................................

      implicit none
C     
C     GLOBAL VARIABLES, ARRAYS, ...
C     ------------------------
      integer*4 nnzero, n, nw, iout, ier, ireord, jdim, kdim
      integer*4 izs(nw), irenu(n), nbvoi(n), ia(n+1), ja(nnzero)
C     
C     LOCAL  VARIABLES, ARRAYS, ...
C     ------------------------
      integer*4 ibfs(1), iiend, i, mnvoi, nfree, iend, nodmin, init
      integer*4 maskval, ilevel, nlev,lband1, lband2, lband3
      logical*1 iprint
      character chsubr*6
      integer*4 largbd
      external largbd

      ier  = 0
      iprint = iout .gt. 0 .and. iout   .le. 200

c     -- vector irenu initialize as unity --
      call numini(n, irenu)
      iiend = 1
      iend  = iiend

      if(n .le. 1) go to 100

      if(iprint) then
        if( ireord.eq.1 ) then
          write(iout,440)  'Cuthill-McKee.'
        else
          write(iout,440)  'Reverse Cuthill-McKee.'
        endif
      endif
c     
c     -- largeur de bande initiale --
      call iplusa (n+1,-1,1,ia)
      lband1 = largbd (n,ia,ja,irenu)
      call iplusa (n+1,1,1,ia)
c     
c     -- choosing a node of minimum degree --
c
c     dutto's version is biased toward the first node of minimum
c     degree that occurs
c
c      maskval = 1
c      mnvoi   = n + 1
c      init    = 0
c      do i = 1, n
c        nbvoi(i) = maskval
c        if(ia(i+1) - ia(i) .lt. mnvoi) then
c          mnvoi = ia(i+1) - ia(i)
c          init  = i
c        endif
c      enddo
c
c     instead, let's add some flexibility to the root node
c     selection process
      maskval = 1
      mnvoi   = n + 1
      init    = 0
      do i = 1, n
        nbvoi(i) = maskval
c        if(ia(i+1) - ia(i) .le. mnvoi) then
c          mnvoi = ia(i+1) - ia(i)
c          init  = i
c     show connectivity
c          write (*,*) i , ja(ia(i)) , ja(ia(i)+1)
c     show j,k node
c          if (mod(i,41).eq.0) then
c            write (*,88) i,mnvoi,(i-mod(i,41))/41,41
c          else
c            write (*,88) i,mnvoi,(i-mod(i,41))/41+1,mod(i,41)
c          end if
c 88       format('cmk: i,mnvoi,j,k',4I8)  
c        endif
      enddo
c      write (*,*) 'cmk',init
c      stop
c
c
c     for sweep of several root nodes
c      open (UNIT = 226, FILE = 'startindex.inp', STATUS = 'OLD')
c      read (226,*) i
c      close(226)
c      write (*,*) 'cmk: start index',i
c      stop
c
c     inviscid best i = (1,kdim) = kdim
c      i = 41
c     turbulent best i = (1,kdim) = kdim
c      i = 65
      i = kdim
c      mnvoi = ia(i+1) - ia(i)
      init  = i
c      write (*,*) 'cmk',init
c     stop
c
      ilevel = iiend
      iend   = ilevel  + n + 1
      if(iend .gt. nw) go to 220
      nodmin = init
c     
      call perphn(n, ja, ia, init, nbvoi, maskval, nlev, irenu,
     *      izs(ilevel))
      call invlpw (n,irenu,nbvoi)
      call icopy_cmk (n,nbvoi,irenu)
      call iplusa (n,maskval,0,nbvoi)
c     
c     -- largeur de bande finale --
      call iplusa (n+1,-1,1,ia)
      lband2 = largbd (n,ia,ja,irenu)
      call iplusa (n+1,1,1,ia)
c     
      if(nodmin .ne. init) then
        irenu(1) = nodmin
        nodmin   = 1
        ibfs(1)  = 0
        call BFS(n, ja, ia, nodmin, ibfs, nbvoi, maskval,
     *        irenu, izs(ilevel), nlev)
        call invlpw(n, irenu, nbvoi)
        call icopy_cmk(n, nbvoi, irenu)
c     -- largeur de bande finale --
        call iplusa(n+1, -1, 1, ia)
        lband3 = largbd(n, ia, ja, irenu)
        call iplusa(n+1, 1, 1, ia)
        if(lband3 .lt. lband2) then
          lband2 = lband3
        else if((lband3 .gt. lband2) .and. (lband2 .lt. lband1))
     *          then
          call iplusa(n, maskval, 0, nbvoi)
          irenu(1) = init
          nodmin   = 1
          ibfs(1)  = 0
          call BFS (n, ja, ia, nodmin, ibfs, nbvoi, maskval,
     *          irenu, izs(ilevel), nlev)
          call invlpw(n, irenu, nbvoi)
          call icopy_cmk(n, nbvoi, irenu)
        endif
      endif

      if(lband2 .ge. lband1) then
        call numini(n, irenu)
        if(iprint) write(iout,1010) lband1
      endif

c     -- reverse Cuthill-McKee: lpw(i) = ns+1 - lpw(i) --
      if ( ireord.eq. 2) call iplusa(n,n+1,-1,irenu)

c     -- clean memory and exit --
 100  continue
      nfree = iend - iiend
      if(nfree .gt. 0) call iplusa(nfree, 0, 0, izs(iiend))
      iend  = iiend
      return

 220  if(iprint) write(iout,420) iend, nw
      ier = -1
      go to 100
 230  if(iprint) write(iout,430) chsubr,ier
      go to 100

 420  format(' ***RENUME*** THERE IS NOT ENOUGH MEMORY IN THE WORK',
     1      ' VECTOR. '/ 13x,' NEEDED  INTEGER MEMORY = ',i10/
     2      13x,' ALLOWED INTEGER MEMORY = ',i10)
 430  format(' ***RENUME*** ERROR IN ',a6,'. IER = ',i8)
 440  format(3x,'Reordering equations with method: ',a22/)
 1010 format(' Sorry! This algorithm can not disminish the initial',
     *      ' bandwidth of ', i8)

      return
      end

C **********************************************************************
      SUBROUTINE MDDLIA(n, ia, ja, iout, ier)
C.......................................................................
C
C     On rearrange le vecteur JA pour avoir les colonnes en ordre
c     croissante.
C
C.......................................................................
      implicit none

      integer*4 ja(*), ia(*)
      integer*4 n, ier, iout, i, is
      character*6 chsubr
      logical*1   impr
C.......................................................................
      ier = 0
      if(n .le. 0) return
      do 30 i=1,n
         is=ia(i)
         chsubr = 'ORDLIG'
         call ordlig(ia(i+1)-is, ja(is), iout, ier)
         if(ier .ne. 0) go to 220
 30   continue
      return
c
 220  impr = iout .gt. 0 .and. iout .le. 99
      if(impr) write(iout, 320) ier
      return
 320  format(' ***MDDLIA*** ERREUR DANS ',A,'. IER = ',I8)
      end
C **********************************************************************
      SUBROUTINE MDIADL(n, ia, ja, iout, ier)
C.......................................................................
C
C     On modifie le vecteur ja de facon d'indiquer l'element diagonal  
C     au debut de la ligne. 
C
C.......................................................................
      implicit none

      integer*4 ja(*), ia(*)
      integer*4 n, iout, ier, i, is, ir, ie, j
      character chsubr*6
      logical*1 impr
C.......................................................................
      impr = iout.gt.0 .and. iout.le.99
      ier  = 0
      if(n .le. 0) return
c
      is = ia(1)
      do 30 i=1,n
         ie=ia(i+1) - 1
         if(is.eq.ie) go to 25
         do 10 ir=is,ie
            j=ja(ir)
            if(j.eq.i) then
                 ja(ir)=ja(is)
                 ja(is)=j
                 go to 15
            endif
 10      continue
 15      continue
         if(ie-is.lt.0) go to 230
         chsubr = 'ORDLIG'
         call ordlig(ie-is,ja(is+1),iout, ier)
         if(ier .ne. 0) go to 220
 25      is=ie+1
 30   continue
c
 100  continue
      return
c
 220  if(impr) write(iout,320) chsubr, ier
      go to 100
 230  ier = -1
      if(impr) write(iout,330) i,is,ie
      go to 100
 320  format(' ***MDIADL*** Erreur dans ',a,' . IER = ',i8)
 330  format(' ***MDIADL*** Erreur dans la definition de la matrice.'/
     *       13x,' Ligne ',i8,' - IS, IE = ',2i8)
      end
c
C***********************************************************************
C***LARGBD
C*********    LARGEUR DE BANDE AVEC PERMUTATION.
C*********
C
      INTEGER*4 FUNCTION LARGBD(NNT,MAX,NNCNI,IPERM)
C
C
      IMPLICIT  NONE
c     INTEGER*2 NNCNI(1)
      INTEGER*4 NNCNI(1)
      INTEGER*4 MAX(0:*),IPERM(1)
      INTEGER*4 NNT,I,JFIN  , JDEB  , IN    , IMAX  , J ,
     *          IV    , LARG
C
C             LARGEUR DE BANDE.
C
      largbd=0
      jfin=0
      do 20 i=1,nnt
         jdeb=jfin+1
         jfin=max(i)
c
         in=iperm(i)
         imax=in
c
         do 10 j=jdeb,jfin
            iv=iperm(nncni(j))
 10      if(imax.lt.iv) imax=iv
c
            larg=imax-in
            if(largbd.lt.larg) largbd=larg
 20   continue
c
      return
      end
C***********************************************************************
      SUBROUTINE ICOPY_CMK(N,IX,IY)
C.......................................................................
C     ON COPIE LE VECTEUR IX SUR LE VECTEUR IY
C.......................................................................
      IMPLICIT  NONE
      INTEGER*4 N, IX(1),IY(1), I
C.......................................................................
      IF(N.LE.0) RETURN
      DO 10 I = 1,N
         IY(I) = IX(I)
   10 CONTINUE
C
      RETURN
      END
C***********************************************************************
      SUBROUTINE ORDLIG(N,JA,IOUT,IER)
C.......................................................................
      IMPLICIT  NONE
      INTEGER*4 N, IOUT, IER
      INTEGER*4 JA(N)
C
      INTEGER*4 I, J, IMIN, JAMIN
      LOGICAL IMPR
C.......................................................................
C
C     ON ORDONNE LES ELEMENTS DU VECTEUR JA SELON LES COLONNES.
C
C.......................................................................
C     LAURA C. DUTTO - OCTOBRE 1990
C.......................................................................
      IMPR=IOUT.GT.0.AND.IOUT.LE.99
      IER = 0
      IF(N.LE.0) GO TO 230
C
      DO 80 I=1,N
         IMIN=I
         JAMIN=JA(I)
         DO 60 J=I+1,N
            IF(JA(J).LT.JAMIN) THEN
                 IMIN=J
                 JAMIN=JA(J)
            ENDIF
 60      CONTINUE
         JA(IMIN)=JA(I)
         JA(I)=JAMIN
 80   CONTINUE
      RETURN
C
 230  IF(IMPR) WRITE(IOUT,310) N
      IER = -1
      RETURN
C
 310  FORMAT(/' ***ORDLIG*** LE NOMBRE DE COMPOSANTES DU VECTEUR',
     *        ' DOIT ETRE POSITIF. N  = ',I8)
      END

