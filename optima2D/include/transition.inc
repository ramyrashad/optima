      !-- common block for transition prediction --

#include "../include/indx.inc"

      ! _____________________
      ! variable declarations
      logical
     &     flagBLEdgeFound(maxj)    !< flag TRUE if BL edge was successfully found

      integer
     &     k_delta(maxj)        !< lower off-wall index of the two nodes which bound the BL edge

      double precision
     &     delta(maxj),         !< boundary layer thickness (off-wall height)
     &     velBLEdge(maxj)      !< magnitude of velocity at boundary layer edge

      ! ____________
      ! common block
      common/transition/                                                      
     &      flagBLEdgeFound, k_delta, delta, velBLEdge

