       subroutine iomarkus(junit,timeindex,jdim,kdim,q,xyj,turre)

      implicit none 

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

     
c     junit=1 write output_file
c     junit=2 write target_file
c     junit=3 read output_file
c     junit=4 read target_file
c     junit=5 write adjoint_file

      integer strlen,junit,timeindex,namelen,numberlen,jdim,kdim,i,jj
      double precision dummy
      character number*4
c
      double precision q(jdim*kdim*4), xyj(jdim*kdim), turre(jdim*kdim)
      double precision qtmp(jdim*kdim*4),turretmp(jdim*kdim)

      logical writeok



      call i_to_s(timeindex,number) 

      if (junit .eq. 1 .or. junit .eq. 3 .or. junit .eq. 5) then
         namelen = strlen(output_file_prefix)
         filena=output_file_prefix
      else if (junit.eq.2 .or. junit .eq. 4) then
         namelen = 3
         filena='tar'
      end if
     
      numberlen = len(number)      
      filena(namelen+1:namelen+numberlen) = number
      filena(namelen+numberlen+1:namelen+numberlen+2) ='.q'

      open(unit=n_all,file=filena,status='unknown',form='unformatted') 

                                                        

      if (junit.eq.1 .or. junit.eq.2) then


         writeok=.false.
         jj=0

         do while (.not. writeok .and. jj.lt.3)
 
            write(n_all) nblks
            write(n_all) (jbmax(i),kbmax(i),i=1,nblks)
            do i=1,nblks
               write(n_all) fsmach, alpha, re*fsmach
               call writempr(n_all,q(lqptr(i)),xyj(lgptr(i)),
     &              turre(lgptr(i)),jmax(i),kmax(i),
     &              jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
            end do

            write(n_all) numiter
            rewind(n_all)

            writeok=.true.

            read(n_all) nblks 
            read(n_all) (jbmax(i),kbmax(i),i=1,nblks)
            do i=1,nblks
               read(n_all) dummy, dummy, dummy
               call readmpr(n_all,qtmp(lqptr(i)),turretmp(lgptr(i)), 
     &              jmax(i),kmax(i),jminbnd(i),jmaxbnd(i),
     &              kminbnd(i),kmaxbnd(i))
            end do
            read(n_all) numiter
            rewind(n_all)

            do i=1,nblks
              call compoutmpr(q(lqptr(i)),xyj(lgptr(i)),turre(lgptr(i)),
     &             qtmp(lqptr(i)),turretmp(lgptr(i)),jmax(i),kmax(i),
     &             jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i),writeok)
            end do

            if (.not. writeok) then
              jj=jj+1
              print *, 'IO error -> will try to rewrite file',numiter,jj
            end if
            
         end do                 !while

c        stop if trying to write it three times did not work
         if (jj.ge.3) stop




       
      else if (junit.eq.3 .or. junit .eq. 4) then
c     q contains the physical quantities (not scaled with J=xyj)

         read(n_all) nblks 
         read(n_all) (jbmax(i),kbmax(i),i=1,nblks)

         do i=1,nblks
           read(n_all) dummy, dummy, dummy
           call readmpr(n_all,q(lqptr(i)),turre(lgptr(i)), 
     &      jmax(i),kmax(i),jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
         end do

         read(n_all) numiter



      else if (junit.eq.5) then 

         write(n_all) nblks
         write(n_all) (jbmax(i),kbmax(i),i=1,nblks)
         do i=1,nblks
            write(n_all) fsmach, alpha, re*fsmach, fsmach
            call writeq(n_all,q(lqptr(i)),xyj(lgptr(i)),jmax(i),kmax(i),
     &           jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
c$$$            call writearr4(n_all, turre(lgptr(i)), turre(lgptr(i)),
c$$$     &           turre(lgptr(i)), turre(lgptr(i)), jmax(i),kmax(i),
c$$$     &           jminbnd(i), jmaxbnd(i), kminbnd(i), kmaxbnd(i))
         end do
         
         write(n_all) numiter

      endif




      close (n_all) 

 
    
      return                                                            
      end !iomarkus




      subroutine i_to_s(intval,s)

      implicit none

      integer idig,intval,ipos,ival,i
      character ( len = * ) s

      s = ' '  
      ival = intval

c  Working from right to left, strip off the digits of the integer
c  and place them into S(1:len ( s )).
c
      ipos = len(s) 

      do while ( ival /= 0 )

         idig = mod( ival, 10 )
         ival = ival / 10
         s(ipos:ipos) = char(idig + 48 )
         ipos = ipos - 1

      end do
c
c  Fill the empties with zeroes.
c
      do i = 1, ipos
         s(i:i) = '0'
      end do
 
      return
      end   !i_to_s                                                           




      subroutine writempr(junit,q,xyj,turre,jdim,kdim,j1,j2,k1,k2)

      implicit none
#include "../include/common.inc"
      integer jdim,kdim,j1,j2,k1,k2,j,k,n,junit
      double precision  q(jdim,kdim,4), xyj(jdim,kdim), turre(jdim,kdim)

      write(junit) (((q(j,k,n)*xyj(j,k),j=j1,j2),k=k1,k2),n=1,4)

      if (turbulnt) write(junit) ((turre(j,k),j=j1,j2),k=k1,k2)

      return 
      end  !writempr

      subroutine writemprtar(junit,q,turre,jdim,kdim,j1,j2,k1,k2)

      implicit none
#include "../include/common.inc"
      integer jdim,kdim,j1,j2,k1,k2,j,k,n,junit
      double precision  q(jdim,kdim,4), turre(jdim,kdim)

      write(junit) (((q(j,k,n),j=j1,j2),k=k1,k2),n=1,4)

      if (turbulnt) write(junit) ((turre(j,k),j=j1,j2),k=k1,k2)

      return 
      end  !writemprtar



      subroutine readmpr(junit,q,turre,jdim,kdim,j1,j2,k1,k2)

      implicit none
#include "../include/common.inc"
      integer jdim,kdim,j1,j2,k1,k2,j,k,n,junit
      double precision q(jdim,kdim,4),turre(jdim,kdim)

      read(junit) (((q(j,k,n),j=j1,j2),k=k1,k2),n=1,4)

      if (turbulnt) read(junit) ((turre(j,k),j=j1,j2),k=k1,k2)

      return 
      end !readmpr


      subroutine compoutmpr(q,xyj,turre,qtmp,turretmp,jdim,kdim,
     &     j1,j2,k1,k2,writeok)

      implicit none
#include "../include/common.inc"
      integer jdim,kdim,j1,j2,k1,k2,j,k,n
      double precision q(jdim,kdim,4), xyj(jdim,kdim), turre(jdim,kdim)
      double precision qtmp(jdim,kdim,4), turretmp(jdim,kdim)
      logical writeok

c     debug: check for small densities

      do k=k1,k2
         do j=j1,j2
            if ( q(j,k,1)*xyj(j,k).lt.1.d-10 ) 
     &           print *,'Small density in iomarkus',
     &           j,k,q(j,k,1),xyj(j,k)
         end do
      end do

      do n=1,4
         do k=k1,k2
            do j=j1,j2
               if (abs(q(j,k,n)*xyj(j,k)-qtmp(j,k,n)).gt.1e-12) then 
                  writeok=.false.
                  goto 111
               end if
            end do
         end do
      end do

      if (turbulnt) then
         do k=k1,k2
            do j=j1,j2
               if (abs(turre(j,k)-turretmp(j,k)).gt.1e-12) then
                  writeok=.false.
                  goto 111
               end if
            end do
         end do
      end if

 111  continue

      return 
      end  !compoutmpr



