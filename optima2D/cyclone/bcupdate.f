      subroutine bcupdate(jdim,kdim,q,press,sndsp,xy,xit,ett,xyj,x,y,   
     >                    a,b,c,f,g,z,qbcj,qbck)  
c                                                                       
c     note modified all bc routines except bcplate to use with only        
c     q's which are needed, for instance at body we only need              
c     q(j,1,n), q(j,2,n), q(j,3,n)                                         
c     also, note arrays press and sndsp should not be used in bc routines  
c                                                                       
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)
c                                                                       
      dimension a(jdim),b(jdim),c(jdim),f(jdim),g(jdim)
      dimension z(jdim),qbcj(3,kdim,4),qbck(jdim,3,4)                   
c     I declare this extra array because adjusting qbck and the
c     common array 'worksp' where qbck came from screws things up.
c     
c     update boundary conditions
c     
c     
      if (.not.ibc) then
c     
c     surface k = 1 boundary conditions
c     
c     do 1 n = 1,4                                                 
c     do 1 k = 1,3                                                 
c     do 1 j = jbegin,jend                                         
c     1          qbck(j,k,n) = q(j,k,n)
c     
c     call bcbody(jdim,kdim,qbck,3,xy,xit,ett,xyj,x,y,a,b,c,f,g,z)
c     k = kbegin                                                   
c     do 11 n = 1,4                                                
c     do 11 j = jbegin,jend                                        
c11   q(j,k,n) = qbck(j,1,n)                                       
c     sd      call bcbody(jdim,kdim,qbck,5,xy,xit,ett,xyj,x,y,a,b,c,f,g
c     ,z)
        call bcbody_ho(jdim,kdim,press,q,xy,xit,ett,xyj,x,y,
     &        a,b,c,f,g,z)
        if (sngvalte) call bcte(jdim,kdim,q,xyj)
c     
c     ==================================================================
c     -- for uniform flow test --
c     do 1 n = 1,4                                                 
c     do 1 k = 1,3
c     do 1 j = jbegin,jend                                         
c     qbck(j,k,n) = q(j,k,n)
c     1       continue
c     call bcfarbody(jdim,kdim,qbck,3,xy,xit,ett,xyj,x,y)               
c     k = kbegin                                                   
c     do 11 n = 1,4                                                
c     do 11 j = jbegin,jend
c     q(j,k,n) = qbck(j,1,n)
c     11      continue
c     turn circulation off
c     circul=.false.
c     call bcfar2(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,
c     &                                              jtail1,jtail2,1,1)
c     ==================================================================
c 
c     -- set variables for far field vortex correction --
        if (circul .and. fsmach.le.1.0) call bccirc   

c     
c     option to iterate on alpha for a specified lift               
c     
c     start cl modification only after iclstrt iterations               
c     1. only perform logic if clalpha = true
c     
c     2. claculate new circulation at far field boundary based on     
c     clinput  
c                                                       
c     3. only update delcl and alpha every iclfreq steps and check
c     
c     abs(delcl) against cltol, if abs(delcl) lt cltol                
c     ``do not'' update alpha                                         

  
        if(numiter .gt. iclstrt .and. clalpha)then                       

           if(mod((numiter - istart)   , iclfreq) .eq. 0  )then         
              delcl = clt - clinput
              if(abs(delcl).ge.cltol)then                              
                 delalp = -relaxcl*delcl                               
                 alpha = alpha + delalp                                

                 uinf = fsmach*cos(alpha*pi/180.)
                 vinf = fsmach*sin(alpha*pi/180.)
          
                 write (out_unit,*)
     &            '----------------------------------------------------'
                 write (out_unit,*)
     &            '|                                                  |'
                 write (out_unit,10) numiter,alpha
 10           format (1x,'| numiter = ',i6,' new alpha = ',f12.8,8x,'|')
                 write (out_unit,*)
     &            '|                                                  |'
                 write (out_unit,*)
     &            '----------------------------------------------------'
                 isw = 1                                               
              else                                                     
                 if(isw.eq.1)then                                      
                    isw = 0                                               
                    write (out_unit,*)
     &            '----------------------------------------------------'
                    write (out_unit,*)
     &            '|                                                  |'
                    write (out_unit,20) dabs(delcl)
 20                 format (1x,'| abs(delcl) = ',e10.4,
     &              ' falls below cl tolerance |')
                    write (out_unit,*)
     &            '|                                                  |'
                    write (out_unit,*)
     &            '----------------------------------------------------'
                 endif                                                 
              endif                                                    
           endif                                                       
        endif                                  

c     --------------------------------------------
c     far field k = kmax boundary conditions
c     --------------------------------------------

c     -- riemann invariants (old arc2d far-field) --
        do n = 1,4                                                 
          do k = kend-2,kend                                         
            do j = jbegin,jend                                         
              qbck(j,k-kend+3,n) = q(j,k,n)
            end do
          end do
        end do
        call bcfar (jdim,kdim,qbck,3,xy,xit,ett,xyj,x,y)               
        k = kend                                                     
        do n = 1,4                                                
          do j = jbegin,jend                                        
            q(j,k,n) = qbck(j,3,n)
          end do
        end do
c     
c     prectmp=prec
c     prec=3
c     call bcfar2(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,
c     &                                              jlow,jup,kend,-1)
c     prec=0
c     prec=prectmp
c     
cmn   -- this is the routine for 'barth' type bc's (cyclone) --
cmn   call bcfar3(jdim,kdim,q,press,sndsp,xy,xit,ett,xyj,x,y,
c     &                                              jlow,jup,kend,-1)
      endif
c     
c
      if(.not.periodic)then                                           
         if (.not.ibc) then

cmn   -- bout commented out --           
cmn         call bcout(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,
cmn     &                                             kbegin,kend,jbegin,1)
cmn         call bcout(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,
cmn     &                                             kbegin,kend,jend,-1)

cmn   -----------------------
c           far field outflow j=1  boundary conditions                   
            do 3 n = 1,4                                                 
            do 3 j = 1,3                                                 
            do 3 k = kbegin,kend                                         
               qbcj(j,k,n) = q(j,k,n)
 3          continue
            call bcout1 (jdim,kdim,qbcj,3,xy,xit,ett,xyj,x,y)            
            j = jbegin                                                   
            do 13 n = 1,4                                                
            do 13 k = kbegin,kend                                        
               q(j,k,n) = qbcj(1,k,n)
 13         continue

c           far field outflow  j=jmax boundary conditions                  
            do 4 n = 1,4                                                 
            do 4 j = jend-2,jend                                         
            do 4 k = kbegin,kend                                         
               qbcj(j-jend+3,k,n) = q(j,k,n)                                
 4          continue
            call bcout2 (jdim,kdim,qbcj,3,xy,xit,ett,xyj,x,y)         
            j = jend                                                     
            do 14 n = 1,4                                                
            do 14 k = kbegin,kend                                        
               q(j,k,n) = qbcj(3,k,n)
 14         continue
cmn   -------------------------------
         endif
c
c        c mesh wake cut j = 1,jtail1 : jtail2,jmax k = 1
c        boundary conditions if cmesh logic used this part
c        is bypassed since wake cut will be integrated
c        implicitly.  THP                                          
         if(.not.cmesh)then                                              
c            if (prec.eq.0) then
               call bcwake(jdim,kdim,q,press,xy,xit,ett,xyj,x,y)
c               if (numiter.eq.10) then
c                  iord=4
c                  call bcwake(jdim,kdim,q,press,xy,xit,ett,xyj,x,y)
c                  iord=2
c               endif
c            else
c               call bcwakeprec(jdim,kdim,q,xy,xit,ett,xyj,x,y,
c     &                         sndsp,press) 
c            endif
c
         endif                                                             
c     this is the endif for (periodic if statement)
      endif                                                           
c                                                                       
      if(periodic .and. jmax.ne.jmaxold)then                            
c        special logic for overlap o grid case                              
c        set jmaxold point from j = 1 point 
         do 910 k = 1,kmax                                                 
c           note jacobian scaling ok                                       
            q(jmaxold,k,1) = q(1,k,1)                                     
            q(jmaxold,k,2) = q(1,k,2)                                      
            q(jmaxold,k,3) = q(1,k,3)                                     
            q(jmaxold,k,4) = q(1,k,4)                                  
910      continue                                                         
      endif                                                             
c                                                                       
c
c---------------------------------------------------------------------  
c  update pressure and sound speed at all boundaries                    
c---------------------------------------------------------------------  
c                                                                       
c     For implicit boundary conditions most of the following loop
c     is redundant.  All you really have to do is update the wake cut.
c
c
      if (.not.ibc .or. (ibc .and. .not.cmesh)) then
        if (periodic) then
c         -loop to jmaxold which is jend+1 ... 
c          press(jmaxold,1) used in clcdho.f
          j1=jbegin
          j2=jmaxold
        else
          j1=jbegin
          j2=jend
        endif
        do 500 k=kbegin,kend,kend-kbegin
        do 500 j=j1,j2                                       
c          write(99,*) k,j
          press(j,k) = gami*(q(j,k,4) -                               
     &          0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))       
          sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))                 
 500    continue                                                   
      endif
c     
      if(.not.periodic .and. .not.ibc)then
        do 501 j=jbegin,jend,jend-jbegin
        do 501 k=kbegin,kend
          press(j,k) = gami*(q(j,k,4) -
     &          0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))          
          sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))
 501    continue
      endif                                                             
c                                                                       
 999  return                                                            
      end                                                               
