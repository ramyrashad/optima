c     ------------------------------------------------------------------
c     -- residual for leading edge singular points --
c     -- inviscid flow --
c     -- m. nemec, sept. 2001 --
c     ------------------------------------------------------------------
      subroutine rhssng( js1, ks1, js2, ks2, javg1, kavg1, javg2, kavg2,
     &     jmax1, kmax1, q1, xyj1, press1, s1, jmax2, kmax2, q2, xyj2,
     &     press2, s2)

      implicit none
      
      integer js1, ks1, js2, ks2, javg1, kavg1, javg2, kavg2
      integer jmax1,kmax1,jmax2,kmax2,n
      double precision q1(jmax1,kmax1,4),    q2(jmax2,kmax2,4)
      double precision xyj1(jmax1,kmax1),    xyj2(jmax2,kmax2)
      double precision press1(jmax1,kmax1),  press2(jmax2,kmax2)
      double precision s1(jmax1,kmax1,4),    s2(jmax2,kmax2,4)

      do n = 1,3
c     -- average leading edge for (js1,ks1) block  --  
         s1(js1,ks1,n) = -( xyj1(js1,ks1)*q1(js1,ks1,n) - 0.5*(
     &        xyj1(javg1,kavg1)*q1(javg1,kavg1,n) + xyj2(javg2,kavg2)
     &        *q2(javg2,kavg2,n) ))
c     -- average leading edge for (js2,ks2) block  --
         s2(js2,ks2,n) = -( xyj2(js2,ks2)*q2(js2,ks2,n) - 0.5*(
     &        xyj1(javg1,kavg1)*q1(javg1,kavg1,n) + xyj2(javg2,kavg2)
     &        *q2(javg2,kavg2,n) )) 
      end do

c     -- average pressure --
      s1(js1,ks1,4) = -( press1(js1,ks1)*xyj1(js1,ks1) - 0.5*(
     &     press1(javg1,kavg1)*xyj1(javg1,kavg1) +
     &     press2(javg2,kavg2)*xyj2(javg2,kavg2) ))

      s2(js2,ks2,4) = -( press2(js2,ks2)*xyj2(js2,ks2) - 0.5*(
     &     press1(javg1,kavg1)*xyj1(javg1,kavg1) +
     &     press2(javg2,kavg2)*xyj2(javg2,kavg2) ))
      
      return
      end                       !bcravg
