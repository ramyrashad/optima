                a1=xy(t,k,1)
                a2=xy(t,k,2)
                cs=gami*(h(j,k) - 0.5d0*(u(j,k)**2 + v(j,k)**2))
c                cs=sndsp(t,k)**2
c       
c for second difference
                dot1=phi(t,k)*work(1,j,k,1)
     &                -u(t,k)*work(2,j,k,1)
     &                -v(t,k)*work(3,j,k,1)
     &                      + work(4,j,k,1)
                dot2=-uu(t,k)*work(1,j,k,1)
     &                   + a1*work(2,j,k,1)
     &                   + a2*work(3,j,k,1)
c
c tried using blas functions => slower
c                dot1=ddot(4,r2,1,work(1,j,k,1),1)
c                dot2=ddot(4,r4,1,work(1,j,k,1),1)
c
                fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
                fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c       
                temp(1,j,k)=specthalf(t,k,1)*work(1,j,k,1)
     &                     +fact1                  
                temp(2,j,k)=specthalf(t,k,1)*work(2,j,k,1)
     &                     +fact1*u(t,k)                
     &                     +fact2*a1               
                temp(3,j,k)=specthalf(t,k,1)*work(3,j,k,1)
     &                     +fact1*v(t,k)                
     &                     +fact2*a2               
                temp(4,j,k)=specthalf(t,k,1)*work(4,j,k,1)
     &                      +fact1*h(t,k)
     &                      +fact2*uu(t,k)
c for fourth difference
                dot1=phi(t,k)*work(1,j,k,2)
     &                -u(t,k)*work(2,j,k,2)
     &                -v(t,k)*work(3,j,k,2)
     &                      + work(4,j,k,2)
                dot2=-uu(t,k)*work(1,j,k,2)
     &                   + a1*work(2,j,k,2)
     &                   + a2*work(3,j,k,2)
c                dot1=ddot(4,r2,1,work(1,j,k,2),1)
c                dot2=ddot(4,r4,1,work(1,j,k,2),1)
c       
                fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
                fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c
                temp2(1,j,k)=specthalf(t,k,1)*work(1,j,k,2)
     &                      +fact1                   
                temp2(2,j,k)=specthalf(t,k,1)*work(2,j,k,2)
     &                      +fact1*u(t,k)                 
     &                      +fact2*a1                
                temp2(3,j,k)=specthalf(t,k,1)*work(3,j,k,2)
     &                      +fact1*v(t,k)                 
     &                      +fact2*a2                
                temp2(4,j,k)=specthalf(t,k,1)*work(4,j,k,2)
     &                      +fact1*h(t,k)
     &                      +fact2*uu(t,k)
