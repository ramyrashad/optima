module disscon_vars

implicit none

save

integer dc_its, dcT_its

integer, parameter :: jtempRR = 321
integer, parameter :: ktempRR = 385
!integer, parameter :: jtempRR = 449
!integer, parameter :: ktempRR = 385
!integer, parameter :: jtempRR = 511
!integer, parameter :: ktempRR = 245
!integer, parameter :: jtempRR = 541
!integer, parameter :: ktempRR = 245
!integer, parameter :: jtempRR = 513
!integer, parameter :: ktempRR = 241

double precision lamDiss, lamDiss_old,  residF, residF0, dcTol, lamDissT
double precision spectx_dc(jtempRR,ktempRR), specty_dc(jtempRR,ktempRR), resid_reset 
double precision sol0(jtempRR*ktempRR*4+1), lamDissMax, lamKill, Bresid, BresidF
double precision residM, residT, residFM, residFT, residT0, residM0
double precision total_residM, total_residT
double precision total_residFM, total_residFT
double precision BresidM, BresidT, BresidFM, BresidFT
double precision residFM0, residFT0, lamDissMaxT, lamDissT_old
double precision total_residFM0, total_residFT0
double precision lamRed, lamTRed

logical dissCon, lamLockstep

end module disscon_vars
