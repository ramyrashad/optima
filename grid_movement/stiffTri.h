#ifndef STIFFTRI_H
#define STIFFTRI_H

#define Poisson 0.3
#define PowLaw 4   // power-law model used to stiffen mesh

#include "Node.h"
#include "Face.h"

void get_K_Tri (const vector<double*>& vertices, double** K,
	const double& elemQo, double& meshQ, const Face& face);

void calc_B (const vector <double*>& vertices, 
	     vector < vector <double> >& B);

void calc_Ke (const vector < vector <double> >& B, double** I, const
	      double* D);

void get_Q (const vector<double*>& vertices, 
	    const double& area, double& Q);

//void area_tri (const vector<double*>& vertices, double& area);



#endif  // STIFF_H
