c ********************************************************************
c ** LU Backsubstitution - for pentadiagonal systems                **
c ** x sweep routine                                                **
c ** Crout's method of factorization applied to pentadiagonals      **
c ********************************************************************
      subroutine xlubcsb(dm2,dm1,dd,dp1,dp2,b,jmax,kmax,
     &                   jlow,jup,klow,kup)
c ********************************************************************
c Variables:   dm2: vector 2 below diagonal
c              dm1: vector 1 below diagonal    
c              dd : vector of diagonal entries
c              dp1: vector 1 above diagonal
c              dp2: vector 2 above diagonal
c ********************************************************************
      dimension dm2(jmax,kmax), dm1(jmax,kmax), dd(jmax,kmax)
      dimension dp1(jmax,kmax), dp2(jmax,kmax), b(jmax,kmax) 
      do 199 k=klow,kup    
c       /* Forward Substitution     */
        j = jlow + 1                   
        b(j,k) = b(j,k) - dm1(j,k)*b(j-1,k)
        do 50 j = jlow+2,jup
          b(j,k) = b(j,k) - dm2(j,k)*b(j-2,k) - dm1(j,k)*b(j-1,k)
   50   continue
c       /* Back Substitution        */
        j = jup                         
        b(j,k) = b(j,k)/dd(j,k)
        j = jup - 1
        b(j,k) = (b(j,k) - dp1(j,k)*b(j+1,k))/dd(j,k)
        do 75 j = jup-2,jlow,-1
          b(j,k)=(b(j,k)-dp2(j,k)*b(j+2,k)-dp1(j,k)*b(j+1,k))/dd(j,k)
   75   continue
  199 continue
      return
      end
