c----------------------------------------------------------------------
c     -- newgrid: grid perturbation algorithm --
c     
c     written by: marian nemec
c     date: march, 2000
c     mods: dec. 2000
c----------------------------------------------------------------------

      subroutine newgrid ( ndv, dvs, idv, xgrid, ygrid, bap, bcp, bt,
     &      bknot )

      implicit none

#include "parm.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer jmax, kmax, jtail1, jtail2
      common/grid/ jmax, kmax, jtail1, jtail2

      integer i, j, k, idv(nc), ndv

      double precision dvs(nc), dely, tmp, segment(maxk)
      double precision xgrid(jmax,kmax), ygrid(jmax,kmax)
c     -- b-spline arrays --
      double precision bap(jbody,2), bcp(nc,2), bt(jbody)
      double precision bknot(iorder+nc)

      write (*,8) 
 8    format (1x,'** ... Generating New Grid ... **')

      if (ndv .ne. 0) then
        do i = 1,ndv
          bcp(idv(i),2) = dvs(i)
          write (*,10) i, bcp(idv(i),2)
        end do
 10     format (3x,'Design variable',i3,' has value',e14.5)
      end if

c     -- adjusting normal direction --
      call new_body( bknot, bt, bap(1,2), bcp, 2)

      do j = jtail1,jtail2

        dely = bap(j-jtail1+1,2) - ygrid(j,1)

        if ( dabs( dely ) .gt. 5.d-15 ) then
c     -- calculate arclength for k-line at location j --
          segment(1) = 0.d0
          do k = 2, kmax 
            tmp = dsqrt( (xgrid(j,k) - xgrid(j,k-1) )**2 +
     &            ( ygrid(j,k) - ygrid(j,k-1) )**2 )
            segment(k) = segment(k-1) + tmp
          end do
          do k = 1, kmax
            ygrid(j,k) = ygrid(j,k) + dely*
     &            ( 1.d0 - segment(k)/segment(kmax) )
          end do
        end if
      end do

c     -- adjusting streamwise direction --
      if (ndv.eq.0) then
        call new_body( bknot, bt, bap(1,1), bcp, 1)

        do j = jtail1,jtail2

          dely = bap(j-jtail1+1,1) - xgrid(j,1)

          if ( dabs( dely ) .gt. 5.d-15 ) then
c     -- calculate arclength for k-line at location j --
            segment(1) = 0.d0
            do k = 2, kmax 
              tmp = dsqrt( (xgrid(j,k) - xgrid(j,k-1) )**2 +
     &              ( ygrid(j,k) - ygrid(j,k-1) )**2 )
              segment(k) = segment(k-1) + tmp
            end do
            do k = 1, kmax
              xgrid(j,k) = xgrid(j,k) + dely*
     &              ( 1.d0 - segment(k)/segment(kmax) )
            end do
          end if
        end do
      end if

      return
      end                       !newgrid
c----------------------------------------------------------------------

c----------------------------------------------------------------------
c     -- new_body: generate new coordinates for airfoil --
c     -- ixy = 1 streamwise direction, 2 normal direction --
c
c     written by: marian nemec
c     date: march, 2000
c----------------------------------------------------------------------

      subroutine new_body( bknot, bt, y, bcp, ixy)

      implicit none

#include "parm.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer jmax, kmax, jtail1, jtail2
      common/grid/ jmax, kmax, jtail1, jtail2

      integer i, j, left, ixy

      double precision bt(jbody), bknot(iorder+nc), y(jbody)
      double precision bcp(nc,2), tmp, BIatT(ibsord)

      tmp = -1.d0
      do j = 1,jbody
        do left = iorder,nc

          if ( bknot(left).le.bt(j) .and. bt(j).le.bknot(left+1)
     &          .and. bt(j).ne.tmp ) then 
            tmp = bt(j)
            call Bval ( bknot, bt(j), left, BIatT )

            y(j) = 0.d0
            do i=1,iorder
              y(j) = y(j) + bcp(i+left-iorder,ixy)*BIatT(i)
            end do

          end if

        end do
      end do    

      return
      end                       !new_body
c-----------------------------------------------------------------------

