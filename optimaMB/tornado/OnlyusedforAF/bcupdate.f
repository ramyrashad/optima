C ********************************************************************  
C ********************* BOUNDARY CONDITIONS **************************  
C ********************************************************************  
c calling routine: integrat
c
      subroutine bcupdate(iblk,jdim,kdim,q,press,sndsp,turre,vk,ve,
     &                    xy,xit,ett,xyj,x,y,f,
     &                    jbegin,jend,kbegin,kend,
     &                    jminbnd,jmaxbnd,kminbnd,kmaxbnd,
     &                    ibc,ibcdir,iside)
C 
c     Boundary conditions are applied to lines begin/end
c                             with the range   low/up
C                                                                          
c     Note modified all bc routines except BCPLATE to use with only        
c     Q's which are needed, for instance at body we only need              
c     Q(j,1,n), Q(j,2,n), Q(j,3,n)                                         
c     Also, note arrays press and sndsp should not be used in bc routines  
c                                                                       
#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/units.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      dimension turre(jdim,kdim),vk(jdim,kdim),ve(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim),f(jdim)
c                                                                       
c                                                                       
c     notes: 1) new parameter ibc for boundary condition type
c
c             ibc = 1   wall     
c                   2   far field 
c                   3   outflow xi = jbegin
c                   4   outflow xi = jend
c                   5   avg bc's for wake cut
c
c 
c     ********************************************************************
c       update boundary conditions                                       
c     ********************************************************************
      goto (100,200,300,400) ibc

c     ********************************************************************
c      surface k = 1 boundary conditions                                 
c     ********************************************************************
  100 continue
      if (.not.bcairf) return
      goto (130,110,140,120) iside
c
 110  continue
c      call bcvisc(jdim,kdim,kdim,iside,kbegin,kend,jbegin,
c     &      q,xy,xyj,xit,ett,x,y,f)
c
      call bcvisc(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,f,
     &      kminbnd,kmaxbnd,jbegin,1)
c
c      j = jbegin
c      do 115 k = kminbnd,kmaxbnd
c        press(j,k) = gami*(q(j,k,4) -
c     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))
c        sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))
c 115  continue
      goto 999
c
c
 120  continue
c      call bcvisc(jdim,kdim,kdim,iside,kminbnd,kmaxbnd,jend,
c     &      q,xy,xyj,xit,ett,x,y,f)
      call bcvisc(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,f,
     &      kminbnd,kmaxbnd,jend,-1)
c
c      j = jend
c      do 125 k = kminbnd,kmaxbnd
c        press(j,k) = gami*(q(j,k,4) -
c     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))
c        sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))
c 125  continue
      goto 999
c
c
 130  continue
      call bcbody(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,f,
     &      jminbnd,jmaxbnd,ibcdir,kbegin,1)
c      k = kbegin
c      do 135 j = jminbnd,jmaxbnd
c        press(j,k) = gami*(q(j,k,4) -
c     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))
c        sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))
c 135  continue
      goto 999
c
c
 140  continue
      call bcbody(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,f,
     &      jminbnd,jmaxbnd,ibcdir,kend,-1)
c      k = kend  
c      do 145 j = jminbnd,jmaxbnd
c        press(j,k) = gami*(q(j,k,4) -
c     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))
c        sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))
c 145  continue
      goto 999
c
c
c ********************************************************************
c **                    far field                                   **
c **             k = kmax boundary conditions                       **
c ********************************************************************
  200 continue
      if (.not.bcfarf) return
      if (prec.gt.0) then
      write(n_out,*) 
     &        'Farfield boundaries for Preconditioning must be fixed'
      stop
      endif
      goto (210,220,230,240) iside
c
 210  continue
c      write (*,*) 'iside', iside, iblk
c      write (*,*) 'kbegin,jbegin,jend',kbegin,jbegin,jend
c      call bcfar(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
c     &                  jbegin,jend,kbegin,1,iside)
      call bcfarall(jdim,kdim,q,turre,vk,ve,
     &      xy,xit,ett,xyj,x,y,kbegin,jbegin,jend,1,iside)
c      k = kbegin
c      do 215 j = jbegin,jend
c        press(j,k) = gami*(q(j,k,4) -
c     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))
c        sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))
c 215  continue
      goto 999
      

 220  continue
c      write (*,*) 'iside', iside, iblk
c      write (*,*) 'kbegin,jbegin,kend',kbegin,jbegin,kend
c      call bcfar(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
c     &      kbegin,kend,jbegin,1,iside)
c     -- mn: changed kbegin to kbegin+1 --
c     -- mn: changed kend to kend-1 --
      call bcfarall(jdim,kdim,q,turre,vk,ve,
     &      xy,xit,ett,xyj,x,y,jbegin,kbegin+1,kend-1,1,iside)
c      j = jbegin                                                   
c      do 225  k = kbegin,kend
c        press(j,k) = gami*(q(j,k,4) -
c     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))
c        sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))
c 225  continue
      goto 999
c      
 230  continue

c     -- mn, oct 4 2001 --
c     -- commented out the next if statement since optima2d does not use
c     barth type bc conditions --
c      if (matcharc2d) then
c        call bcfar(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
c     &        jbegin,jend,kend,-1,iside)
c      else
        call bcfarall(jdim,kdim,q,turre,vk,ve,
     &        xy,xit,ett,xyj,x,y,kend,jbegin,jend,-1,iside)
c      endif

      goto 999
      
 240  continue
c      call bcfar(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
c     &                  kbegin,kend,jend,-1,iside)
c      write (*,*) 'iside', iside, iblk
c      write (*,*) 'jend,kbegin,kend',jend,kbegin,kend
c     -- mn: changed kbegin to kbegin+1 --
c     -- mn: changed kend to kend-1 --
      call bcfarall(jdim,kdim,q,turre,vk,ve,
     &      xy,xit,ett,xyj,x,y,jend,kbegin+1,kend-1,-1,iside)
c      j = jend
c      do 245  k = kbegin,kend
c        press(j,k) = gami*(q(j,k,4) -
c     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))
c        sndsp(j,k) = sqrt(gamma*press(j,k)/q(j,k,1))
c 245  continue
      goto 999
      
      
c ********************************************************************
c **                    Far Field Outflow                           **
c **           j=jbegin & j=jend boundary conditions                **
c ********************************************************************

c     mn: added +1 and -1 to kbegin and kend in bcout calls bellow
c     mn: this is consistent with linearization

  300 continue
      
      if (.not.bcfarf) return
c     iside=2
      call bcout(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
     &      kbegin+1,kend-1,jbegin,1)              
      goto 999
c
 400  continue
      if (.not.bcfarf) return
c     iside=4
      call bcout(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
     &      kbegin+1,kend-1,jend,-1)              
c
c
c ********************************************************************
  999 continue
      return                                                            
      end                                                               
