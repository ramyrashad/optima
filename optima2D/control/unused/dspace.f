      subroutine dspace (jdim, kdim, ifirst, opt_indx, nk_indx, iex,
     &      q, cp, xy, xyj, x, y, cp_tar, dvs, idv, bap, bcp, bt, bknot,
     &      work1, ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt,
     &      pat, cp_his, ac_his)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     -- solver arrays --
      integer jdim, kdim, ifirst, iex(jdim,kdim,4)
      double precision q(jdim,kdim,4), cp(jdim,kdim), y(jdim,kdim)
      double precision xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim)

c     -- optimization and b-spline arrays --
      integer idv(nc), opt_indx(jdim,kdim), nk_indx(jdim,kdim)
      double precision cp_tar(jbody,2), bap(jbody,2), bcp(nc,2)
      double precision bt(jbody), bknot(jbsord+nc), dvs(nc)
      double precision cp_his(jbody,200), ac_his(jbody,2,200)

c     -- sparse adjoint arrays --
      integer ia(jdim*kdim*4+1), ja(jdim*kdim*4*9*4)
      integer ipa(jdim*kdim*4+1), jpa(jdim*kdim*4*5*4)
      integer iat(jdim*kdim*4+1), jat(jdim*kdim*4*9*4)
      integer ipt(jdim*kdim*4+1), jpt(jdim*kdim*4*5*4)

      double precision as(jdim*kdim*4*9*4), ast(jdim*kdim*4*9*4)
      double precision pa(jdim*kdim*4*5*4), pat(jdim*kdim*4*5*4)

      double precision obj(1), df(1,ibsnc)
c     -- objective function --
      double precision CalcObj

c     -- work arrays --
c     -- compatible with cyclone --
      double precision work1(jdim*kdim,95)

      double precision a(maxpen,4), b(maxpen,16), c(maxpen,16)
      double precision d(maxjk,16), e(maxpen,4), work2(maxjk,10)
      common/scratch/ a,b,c,d,e,work2

      double precision a2(maxj),b2(maxj),c2(maxj),d2(maxj),e2(maxj)
      double precision f2(maxj),g2(maxj),work3(maxj,30)
      common/wksp/ a2,b2,c2,d2,e2,f2,g2
      common/worksp/ work3 

c     -- copy airfoil points -- 
      i = 1
      do j = jtail1,jtail2
        bap(i,1) = x(j,1)
        bap(i,2) = y(j,1)
        i = i + 1
      end do

      start1 = 0.03d0
      start2 = 1.2d0

      final1 = 0.06d0
      final2 = 1.3d0
      
      step1  = 0.01
      step2  = 0.025

      do dv1 = start1, final1, step1
        do dv2 = start2, final2, step2

          dvs(1) = dv1
          dvs(2) = dv2

c     -- adjust grid --
          call regrid( -1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot)

          alpha = dvs(2)
          cosang = cos(pi*alpha/180.d0)                               
          sinang = sin(pi*alpha/180.d0)
          uinf   = fsmach*cosang
          vinf   = fsmach*sinang

          frozen = .false.      !flag used for freezing dissipation
          
          write (*,*) 'Current DVS1 = ', dvs(1)
          write (*,*) 'Current DVS2 = ', dvs(2)

          obj(1) = CalcObj (jdim, kdim, ifirst, nk_indx, iex, q, cp, xy,
     &          xyj, x, y, cp_tar, work1, ia, ja, ipa, jpa, iat, jat,
     &          ipt, jpt, as, ast, pa, pat)
c     -- if ifirst=0 => warm starts for subsequent cyclone runs --
          ifirst = 1                
          ico = ico + 1

c     -- write current airfoil geometry and control points to file --
          rewind (ac_unit)
          rewind (bc_unit)
          do j = jtail1,jtail2
            write (ac_unit,100) x(j,1),y(j,1)
          end do
          do i = 1,nc
            write (bc_unit,100) bcp(i,1), bcp(i,2)
          end do
 100      format (3e24.16)
          call flush (ac_unit)

c     -- write current design variables and objective function --
          write (ohis_unit,200) ico, obj(1), clt, cdt, alpha
          write (*,210) ico, icg, obj(1), slope
          write (dvhis_unit,220) ico, (dvs(j),j=1,ndv)
 200      format (i4, 4e16.8)
 210      format (/3x,'ICO:',i4,1x,'ICG:',i4,1x,'OBJECTIVE:',e12.4,/3x
     &          ,'T.E. SLOPE:',f6.2)
 220      format (i4, 30e16.8)

c     -- compute obj. func. gradient --

          if ( coef_frz ) frozen = .true.

          call CalcGrad (obj(1), df, jdim, kdim, ifirst, q, cp, xy, xyj,
     &          x, y, cp_tar, bap, bcp, bt, bknot, dvs, idv, opt_indx,
     &          nk_indx, iex, work1, ia, ja, ipa, jpa, iat, jat, ipt,
     &          jpt, as, ast, pa, pat)

          icg = icg + 1

c     -- calculate magnitude of gradient --
          gradmag = 0.d0
          do i=1,ndv
            gradmag = gradmag + df(1,i)*df(1,i)
          end do
          write (ghis_unit,300) ico, icg, dsqrt(gradmag), obj(1)
          write (gvhis_unit,310) icg, (df(1,j),j=1,ndv)
 300      format(2i4, 2e16.8)
 310      format(i4,30e16.8)
        end do
      end do

      return
      end                       !dspace
