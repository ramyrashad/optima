c----------------------------------------------------------------------
c     -- unsteady adjoint calculation for each time step--
c
c     written by: markus rumpfkeil
c     date: November 2008
c----------------------------------------------------------------------
      subroutine unsteadyadjoint(jdim,kdim,ndim,indx,icol,psi,psies, 
     &      psiold,iex,ia,ja,as,ipa,jpa,pa,iat,jat,ast,ipt,jpt,pat,q,
     &      press,sndsp,xy,xyj,x,y,uu,vv,ccx,ccy,coef2,coef4,tmet,
     &      precon,bcf,fmu,vort,turmu,work1,a_jk,count,nout,skipend,
     &      po,dpod,win,ns,J1,J2,deltat,deltatbig,deltattmp,scalef,jsta,
     &      jsto,na,cdmean,clmean,psiout)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim, kdim, ndim,icol(9),count,nout,skipend,j,k,i,na
      integer indx(jdim,kdim),iex(jdim*kdim*ndim),stopstage2,stage,jki

      double precision q(jdim,kdim,ndim),xy(jdim,kdim,4),cdmean,clmean
      double precision press(jdim,kdim),sndsp(jdim,kdim),tmet(jdim,kdim)  
      double precision xyj(jdim,kdim),x(jdim,kdim),y(jdim,kdim),scalef
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)

      double precision psi(jdim,kdim,ndim),psiold(jdim,kdim,ndim),psitmp
      double precision psies(jdim,kdim,ndim,6),qtar(jdim,kdim,4)

      double precision uu(jdim,kdim),vv(jdim,kdim),a_jk(6,6),fac
      double precision ccx(jdim,kdim),ccy(jdim,kdim),rhs(jdim,kdim,ndim) 
      double precision coef4(jdim,kdim),coef2(jdim,kdim)
      double precision turretmp(jdim,kdim)
      double precision precon(jdim,kdim,6),win(tdim),po(tdim,sdim)
      double precision bcf(jdim,kdim,4),dfobjdQ(jdim,kdim,ndim)

c     -- sparse adjoint arrays --
      integer ia(jdim*kdim*ndim+2),iat(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)

      complex*16 dpod(Nobs,tdim,sdim,3)

      double precision rhsmat(jdim*kdim*ndim),psimat(jdim*kdim*ndim)
      double precision ns(sdim,2),tmpmat(jdim*kdim*ndim),xpt1,ypt1
      integer inttmp(jdim*kdim*ndim),jsta,jsto,j_point
      

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100)  
      double precision deltat,deltatbig,deltattmp,dtbigtmp,dtsmalltmp
      double precision xyjout(jdim,kdim),J1(tdim),J2(tdim),dttp,dttpp

      logical psiout,imps


c     -- account for big timejump in the begining
      if (.not.errest ) then
         if (skipend.gt.0 .and. count.le.skipend) then
            deltattmp=deltatbig
         else
            deltattmp=deltat
         end if
      else
         deltattmp=dta(count)
         dttp=dta(count+1)
         dttpp=dta(count+2)
cmpr     trick to avoid NaN for last time steps with BDF2 due to division
         if (count .eq. nout) then
            dttp=1.d0
            dttpp=1.d0
         else if (count .eq. nout-1) then 
            dttpp=1.d0
         end if            
      end if

c     -- read target distribution at this time step if existent --
      if (jstage.eq.jstagemx .and. count.gt.skipend .and.
     &     (obj_func.eq.1.or.obj_func.eq.9.or.obj_func.eq.12)) then 
         call iomarkus(4,count,jdim,kdim,qtar,xyj,turmu)
      end if
      
c     -- read flow field at this time step --
      if (jstage.ne.jstagemx) then
         call iomarkusstage(3,jstage,count,jdim,kdim,q,xyj,turmu)
      else 
         call iomarkus(3,count,jdim,kdim,q,xyj,turmu)
      end if

c     -- calculate dfobj/dQhat
      do i=1,ndim
         do k = kbegin,kend  
            do j = jbegin,jend              
               dfobjdQ(j,k,i)=0.d0
            end do
         end do
      end do

      if (jstage.eq.jstagemx .and. count.gt.skipend) then
         
         call calcdfobjdQ(dfobjdQ,jdim,kdim,ndim,q,qtar,xy,xyj,x,y,
     &        nout,skipend,deltattmp,scalef,jsta,jsto,dpod,
     &        win,po,ns,count,J1,J2,cdmean,clmean)
         
      end if

c     -- put in jacobian Q->Qhat and ensure tmet is zero--
      do k = kbegin,kend
         do j = jbegin,jend            
            tmet(j,k) = 0.d0
            q(j,k,5) = turre(j,k)/xyj(j,k)
            do i = 1,4
               q(j,k,i) = q(j,k,i)/xyj(j,k)
            end do
         end do
      end do 
         
c     -- calculate pressure and sound speed --
      call calcps (jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- compute laminar viscosity based on sutherland's law --
      if (viscous) call fmun(jdim, kdim, q, press, fmu, sndsp)

c     -- compute generalized distance function --
      if (viscous .and. turbulnt .and. itmodel.eq.2) then
         do k = kbegin,kend
            do j = jbegin,jend
               j_point=j
               if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
               xpt1 = x(j_point,kbegin)
               ypt1 = y(j_point,kbegin)
               smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
            end do
         end do
      end if



      
      if (jesdirk.eq.4 .or. jesdirk.eq.3) then
c     evolve psi for ESDIRK4:

c     -- adjoint lhs without implicit boundary conditions and unsteady term: dR/dQhat -- 
         jac_mat = .true.
         prec_mat = .true.
         imps=.false.
         unsted=.false.
         dt=1.d0
               
         call get_lhs (jdim, kdim, ndim, indx, icol, iex, ia, ja, as,
     &        ipa, jpa, pa, xy, xyj, x, y, q, press, sndsp, fmu,
     &        turmu, uu, vv, ccx, ccy, coef2, coef4, tmet, precon,
     &        bcf, work1, ast, pdc,a_jk(jstage,jstage),imps)

         unsted=.true.


c     -- calculate RHS for ESDIRK adjoint problem, store temporarily in Matrix format in rhsmat  
c        and copy in the end to rhs. Use psimat as temporary storage in Matrix format 
c        for psies at the correct stage k multiplied with the correct factor -a_kj/a_kk.
c        Use tmpmat as temporary storage for the product (dR/dQ)^T*psimat.


         do j=1,na
            rhsmat(j)=0.d0
            psimat(j)=0.d0
         end do

         if (jstage.eq.jstagemx .and. count.gt.skipend) then
c        dfobjdQ contribution

            do i=1,ndim
               do k = kbegin,kend  
                  do j = jbegin,jend 
                     jki = ( indx(j,k) - 1 )*ndim + i
                     rhsmat(jki)=-dfobjdQ(j,k,i)
                  end do
               end do
            end do

         end if


         if (jstage.lt.jstagemx .or. count.eq.nout) then
            stopstage2=jstage+1
         else
            stopstage2=2
         endif


         do stage=jstagemx,stopstage2,-1

            if (stopstage2.ne.2) then
               fac=-a_jk(stage,jstage)/a_jk(stage,stage)
            else
               fac=-a_jk(stage,1)/a_jk(stage,stage)
            end if

            do i=1,ndim
               do k = kbegin,kend  
                  do j = jbegin,jend 
                     jki = ( indx(j,k) - 1 )*ndim + i
                     psimat(jki)=fac*psies(j,k,i,stage)
                  end do
               end do
            end do

            if (stopstage2.eq.2) then
               do j=1,na
                  tmpmat(j)=0.d0
               end do
               if (skipend.gt.0 .and. count.le.skipend-1) then
                  fac = -1.0d0/(deltatbig*a_jk(stage,1))
               else
                  fac = -1.0d0/(deltattmp*a_jk(stage,1))
               end if 
               do i=1,ndim
                  do k = klow,kup  
                     do j = jlow,jup 
                        jki = ( indx(j,k) - 1 )*ndim + i
                        tmpmat(jki)=fac
                     end do
                  end do
               end do               
               call apldia (na,0,as,ja,ia,tmpmat,as,ja,ia,inttmp)
            end if


c           Multiply (dR/dQ)^T with psimat store in tmpmat and add to rhsmat
            call atmux(na,psimat,tmpmat,as,ja,ia)
            do j=1,na
               rhsmat(j)=rhsmat(j)+tmpmat(j)
            end do

c           Undo addition of fac
            if (stopstage2.eq.2) then
               do j=1,na
                  tmpmat(j)=0.d0
               end do
               fac=-fac
               do i=1,ndim
                  do k = klow,kup  
                     do j = jlow,jup 
                        jki = ( indx(j,k) - 1 )*ndim + i
                        tmpmat(jki)=fac
                     end do
                  end do
               end do     
               call apldia (na,0,as,ja,ia,tmpmat,as,ja,ia,inttmp)
            end if

         end do   !loop over stages

c        Copy rhsmat into rhs
         do i=1,ndim
            do k = kbegin,kend  
               do j = jbegin,jend 
                  jki = ( indx(j,k) - 1 )*ndim + i
                  rhs(j,k,i)=rhsmat(jki)
               end do
            end do
         end do



c     -- adjoint lhs including implicit boundary conditions and unsteady term: dRstar/dQhat --
         jac_mat = .true.
         prec_mat = .true.
         imps=.true.
         dt=1.d0
         dtbigtmp=dtbig
         dtbig=deltatbig
         dtsmalltmp=dtsmall
         dtsmall=deltat
         dt2=deltattmp
        
         call get_lhs (jdim, kdim, ndim, indx, icol, iex, ia, ja, as,
     &        ipa, jpa, pa, xy, xyj, x, y, q, press, sndsp, fmu,
     &        turmu, uu, vv, ccx, ccy, coef2, coef4, tmet, precon,
     &        bcf, work1, ast, pdc,a_jk(jstage,jstage),imps)

         dtbig=dtbigtmp
         dtsmall=dtsmalltmp


c     -- Solve adjoint problem with latest psi as initial guess

c$$$         if (jstage.lt.jstagemx) then
c$$$            stage=jstage+1
c$$$         else
c$$$            stage=2
c$$$         endif
c$$$
c$$$         do i=1,ndim
c$$$            do k = kbegin,kend
c$$$               do j = jbegin,jend               
c$$$                  psi(j,k,i)=psies(j,k,i,stage)                                         
c$$$               end do
c$$$            end do
c$$$         end do

         do i=1,ndim
            do k = kbegin,kend
               do j = jbegin,jend               
                  psi(j,k,i)=0.d0                                         
               end do
            end do
         end do


         call solveadj(jdim,kdim,ndim,indx,iex,as,ja,ia,ast,jat,iat,na,
     &        rhs,psi,pa,jpa,ipa,pat,jpt,ipt,work1)

c     -- Copy solution into correct psi stage
         if (jstage.lt.jstagemx .or. count.eq.nout) then
            stage=jstage
         else
            stage=1
         endif

         do i=1,ndim   
            do k = kbegin,kend       
               do j = jbegin,jend                  
                  psies(j,k,i,stage) = psi(j,k,i)                                
               end do
            end do
         end do
                



      else if (jesdirk.eq.1) then
c     evolve psi for BDF2: 

c     -- calculate RHS: rhs=(4*psi^{n+1} - psi^{n+2})/(2*dt) - (dfobj/dQhat)^T 
         do i=1,ndim
            do k = kbegin,kend
               do j = jbegin,jend              
                      
                  if ( k.lt.klow .or. k.gt.kup .or. 
     &                 j.lt.jlow .or. j.gt.jup ) then
              
                     rhs(j,k,i) = - dfobjdQ(j,k,i)
 
                  else  !interior

                     if (.not.errest ) then

                        if (count .eq. skipend) then
                           rhs(j,k,i)=(deltat+deltatbig)*psi(j,k,i)/
     &                    (deltat*deltatbig)-psiold(j,k,i)/(2.d0*deltat)
                        else if (count .eq. skipend-1) then
                           rhs(j,k,i)=2.d0*psi(j,k,i)/deltatbig-deltat* 
     &                      psiold(j,k,i)/(deltatbig*(deltat+deltatbig))
                        else
                           rhs(j,k,i)=( 4.d0*psi(j,k,i)-psiold(j,k,i) )
     &                          /(2.d0*deltattmp)-dfobjdQ(j,k,i)
                        end if
                        
                     else

                        rhs(j,k,i)=(dttp+deltattmp)*psi(j,k,i)/
     &                      (dttp*deltattmp) - dttpp*psiold(j,k,i)/
     &                      (dttp*(dttpp+dttp)) - dfobjdQ(j,k,i)
                       
                     end if !errest?
                     
                  end if !interior?

               end do
            end do
         end do


c     -- adjoint lhs: dRstar/dQhat --
         jac_mat = .true.
         prec_mat = .true.
         imps=.true.
         dt=1.d0
         dtbigtmp=dtbig
         dtbig=deltatbig
         dtsmalltmp=dtsmall
         dtsmall=deltat
         dt2=deltattmp

        
         call get_lhs (jdim, kdim, ndim, indx, icol, iex, ia, ja, as,
     &        ipa, jpa, pa, xy, xyj, x, y, q, press, sndsp, fmu,
     &        turmu, uu, vv, ccx, ccy, coef2, coef4, tmet, precon,
     &        bcf, work1, ast, pdc,a_jk(1,1),imps)

         dtbig=dtbigtmp
         dtsmall=dtsmalltmp


c     -- copy psi into psiold and use linear or constant extrapolation 
c        for next initial guess
         do i=1,ndim   
            do k = kbegin,kend       
               do j = jbegin,jend 
                  psitmp = psi(j,k,i)
c                  psi(j,k,i) = 2.d0*psitmp - psiold(j,k,i)
                  psiold(j,k,i) = psitmp                 
               end do
            end do
         end do



c     -- Calculate psi^(n)= inv(dRstar/dQhat)]^T) * rhs
         call solveadj(jdim,kdim,ndim,indx,iex,as,ja,ia,ast,jat,iat,na,
     &        rhs,psi,pa,jpa,ipa,pat,jpt,ipt,work1)
                

      end if   !evolve psi for BDF2 or ESDIRK4


c     -- output psi if desired
      if (psiout) then

         do j = jbegin,jdim
            do k = kbegin,kend                  
               xyjout(j,k)=1.0d0
               turretmp(j,k)=turre(j,k)
               turre(j,k)=psi(j,k,5)
            end do
         end do
         
         if (jstage.ne.jstagemx) then
            call iomarkusstage(1,jstage,5000+count,jdim,kdim,psi,
     &           xyjout,turmu)
         else 
            call iomarkus(1,5000+count,jdim,kdim,psi,xyjout,turmu)
         end if

         do j = jbegin,jdim
            do k = kbegin,kend                                 
               turre(j,k)=turretmp(j,k)
            end do
         end do

      end if

c     -- remove jacobian scaling Qhat->Q--         
      do k = kbegin,kend
         do j = jbegin,jend            
            press(j,k)=press(j,k)*xyj(j,k)
            do i = 1,ndim
               q(j,k,i) = q(j,k,i)*xyj(j,k)             
            end do
         end do
      end do

      return
      end  !unsteadyadjoint










      subroutine calcdfobjdQ(dfobjdQ,jdim,kdim,ndim,q,qtar,xy,xyj,x,y,
     &     nout,skipend,deltat,scalef,jsta,jsto,dpod,
     &     win,po,ns,count,J1,J2,cdmean,clmean)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim,kdim,ndim,nout,skipend,jsta,jsto,count
      integer i,j,k,numitertmp
      double precision dfobjdQ(jdim,kdim,4),cdmean,clmean,fac
      double precision q(jdim,kdim,ndim),qtar(jdim,kdim,4),x(jdim,kdim)
      double precision xy(jdim,kdim,4),xyj(jdim,kdim),y(jdim,kdim)
      double precision c_cn,c_cc,c_cm,sin_alpha,cos_alpha,pre,pret
      double precision deltat,scalef,win(tdim),po(tdim,sdim),vorti
      double precision J1(tdim),J2(tdim),ns(sdim,2),dfobjdQtmp
      double precision fobjpp,fobjp,fobjm,fobjmm,qtmp,stepsize
      complex*16 dpod(Nobs,tdim,sdim,3)
       
      if (obj_func .eq. 1) then 

         do i=1,4
            do k = kbegin,kend  
               do j = jbegin,jend 
                  
                  dfobjdQ(j,k,i)=(q(j,k,i)-qtar(j,k,i))*deltat*xyj(j,k)
                  
               end do
            end do
         end do                                                              
            
      else if (obj_func .eq. 0) then
c     --  mean drag  minimization --

         c_cn = sin(alpha*pi/180.0)
         c_cc = cos(alpha*pi/180.0)
         c_cm = 0.d0

         if (.not. errest) then
c        simply sum them up and divide by total number
            c_cn = c_cn/real(nout-skipend)
            c_cc = c_cc/real(nout-skipend)
         else
c        trapezoidal rule for every time step
            if (count.ne.skipend .and. count.ne.nout) then
c           interior points
               c_cn = c_cn*(dta(count)+dta(count+1))/(2.d0*sumdt)
               c_cc = c_cc*(dta(count)+dta(count+1))/(2.d0*sumdt)
            else if (count.eq.skipend) then
c           first point
               c_cn = c_cn*dta(count+1)/(2.d0*sumdt)
               c_cc = c_cc*dta(count+1)/(2.d0*sumdt)
            else if (count.eq.nout) then
c           last point
               c_cn = c_cn*dta(count)/(2.d0*sumdt)
               c_cc = c_cc*dta(count)/(2.d0*sumdt)
            end if
         end if 

         call calcdJdQc(jdim,kdim,ndim,q,xy,xyj, x, y, c_cc, c_cn,
     &        c_cm, dfobjdQ)
            
      else if (obj_func .eq. 5) then

         sin_alpha=sin(alpha*pi/180.0)
         cos_alpha=cos(alpha*pi/180.0)

         if (.not. errest) then
c        simply sum them up and divide by total number
            sin_alpha=sin_alpha/(real(nout-skipend)*scalef)
            cos_alpha=cos_alpha/(real(nout-skipend)*scalef)            
         else
c        trapezoidal rule for every time step
            if (count.ne.skipend .and. count.ne.nout) then
c           interior points
              sin_alpha=sin_alpha*(dta(count)+dta(count+1))/(2.d0*sumdt)
              cos_alpha=cos_alpha*(dta(count)+dta(count+1))/(2.d0*sumdt)
            else if (count.eq.skipend) then
c           first point
              sin_alpha=sin_alpha*dta(count+1)/(2.d0*sumdt)
              cos_alpha=cos_alpha*dta(count+1)/(2.d0*sumdt)
            else if (count.eq.nout) then
c           last point
              sin_alpha=sin_alpha*dta(count)/(2.d0*sumdt)
              cos_alpha=cos_alpha*dta(count)/(2.d0*sumdt)
            end if
         end if          
         
         c_cn = sin_alpha/clmean - cos_alpha*cdmean/clmean**2
         c_cc = cos_alpha/clmean + sin_alpha*cdmean/clmean**2
         c_cm = 0.d0

         call calcdJdQc(jdim,kdim,ndim,q,xy,xyj, x, y, c_cc, c_cn,
     &        c_cm, dfobjdQ)
            

c$$$               do i=1,ndim
c$$$                  do k=kbegin,kbegin+2
c$$$                     do j = jbegin,jend
c$$$                         print *, j,k,i,dfobjdQ(j,k,i)
c$$$                      enddo
c$$$                   enddo
c$$$                enddo
                        

c$$$            do i=1,ndim
c$$$               do k=kbegin,kbegin+2
c$$$                 do j = jbegin,jend
c$$$                  
c$$$                     qtmp = q(j,k,i)
c$$$
c$$$                     q(j,k,i) = q(j,k,i) + fd_eta 
c$$$            
c$$$                     presstmp = press(j,k)
c$$$                     press(j,k) = gami*( q(j,k,4)-0.5d0*
c$$$     &                    ( q(j,k,2)**2 + q(j,k,3)**2 )/q(j,k,1) )
c$$$                     call clcd (jdim, kdim, q, press,x,y,xy,xyj,0,cli,
c$$$     &                    cdi, cmi, clv, cdv, cmv)
c$$$                     cdtp = cdi + cdv
c$$$                     press(j,k) = presstmp
c$$$                     
c$$$                     q(j,k,i) = qtmp - fd_eta
c$$$                     
c$$$                     presstmp = press(j,k)
c$$$                     press(j,k) =  gami*( q(j,k,4)-0.5d0*
c$$$     &                    ( q(j,k,2)**2 + q(j,k,3)**2 )/q(j,k,1) )
c$$$                     call clcd (jdim, kdim, q, press,x,y,xy,xyj,0,cli,
c$$$     &                    cdi, cmi, clv, cdv, cmv)
c$$$                     cdtm = cdi + cdv
c$$$                     press(j,k) = presstmp
c$$$                     q(j,k,i) = qtmp  
c$$$                     
c$$$                     dfobjdQtmp= (cdtp - cdtm)/
c$$$     &                    (2.d0*fd_eta*real(nout-skipend))
c$$$                     
c$$$                     print *, j,k,i,dfobjdQtmp
c$$$                     
c$$$                  end do                  
c$$$               end do
c$$$            end do

      else if (obj_func .eq. 9) then

c           Part I: C-shape

         k=klineconst
         
         do j = jsta,jsto
               
            pre=gami*(q(j,k,4)-0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/
     &           q(j,k,1))
            
            pret=gami*(qtar(j,k,4)-0.5d0*(qtar(j,k,2)**2+
     &           qtar(j,k,3)**2)/qtar(j,k,1))
               
            dfobjdQ(j,k,1)=gami*0.5d0*( (q(j,k,2)**2+q(j,k,3)**2)/
     &           q(j,k,1)**2 )*deltat*xyj(j,k)*(pre-pret)/scalef
            
            dfobjdQ(j,k,2)=gami*( -q(j,k,2)/q(j,k,1) )*deltat*
     &              xyj(j,k)*(pre-pret)/scalef
            
            dfobjdQ(j,k,3)=gami*( -q(j,k,3)/q(j,k,1) )*deltat*
     &           xyj(j,k)*(pre-pret)/scalef
               
            dfobjdQ(j,k,4)=gami*xyj(j,k)*deltat*(pre-pret)/scalef
            
               
         end do

c            Part II: straight down close to x=remxmax above wake cut

         j=jsto
         
         do k = klineconst-1,1,-1
            
            pre=gami*(q(j,k,4)-0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/
     &           q(j,k,1))
            
            pret=gami*(qtar(j,k,4)-0.5d0*(qtar(j,k,2)**2+
     &           qtar(j,k,3)**2)/qtar(j,k,1))

            fac=deltat*xyj(j,k)*(pre-pret)/scalef
            
            dfobjdQ(j,k,1)=gami*0.5d0*( (q(j,k,2)**2+q(j,k,3)**2)/
     &           q(j,k,1)**2 )*fac
            
            dfobjdQ(j,k,2)=gami*( -q(j,k,2)/q(j,k,1) )*fac
            
            dfobjdQ(j,k,3)=gami*( -q(j,k,3)/q(j,k,1) )*fac
               
            dfobjdQ(j,k,4)=gami*fac
                  
         end do



c            Part III: straight down close to x=remxmax below wake cut

         j=jsta
            
         do k = 1,klineconst-1
            
            pre=gami*(q(j,k,4)-0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/
     &           q(j,k,1))
               
            pret=gami*(qtar(j,k,4)-0.5d0*(qtar(j,k,2)**2+
     &           qtar(j,k,3)**2)/qtar(j,k,1))
            
            dfobjdQ(j,k,1)=gami*0.5d0*( (q(j,k,2)**2+q(j,k,3)**2)/
     &           q(j,k,1)**2 )*deltat*xyj(j,k)*(pre-pret)/scalef
            
            dfobjdQ(j,k,2)=gami*( -q(j,k,2)/q(j,k,1) )*deltat*
     &           xyj(j,k)*(pre-pret)/scalef
            
            dfobjdQ(j,k,3)=gami*( -q(j,k,3)/q(j,k,1) )*deltat*
     &           xyj(j,k)*(pre-pret)/scalef
               
            dfobjdQ(j,k,4)=gami*xyj(j,k)*deltat*(pre-pret)/scalef
               
         end do

                
      else if (obj_func .eq. 10 .or. obj_func .eq. 14) then         
c     -- design using FWH--
         
         call calcdJdQnoise(dfobjdQ,q,count,nout,skipend,jdim,kdim,
     &        deltat,scalef,xyj,jsta,jsto,dpod,win,po,ns)

c$$$               print *, 'Start calc dfdq fourth order',count
c$$$
c$$$               numitertmp=numiter
c$$$               stepsize=fd_eta
c$$$
c$$$               do k=klineconst,klineconst
c$$$                  do j = jsta,jsto
c$$$                     do i=1,4
c$$$                                          
c$$$                        qtmp = q(j,k,i)
c$$$
c$$$                        q(j,k,i) = qtmp + 2.d0*stepsize 
c$$$
c$$$                        call calcobjairnoisefd(fobjpp,q,count,nout,skipend,
c$$$     &                      jdim,kdim,deltat,scalef,x,y,jsta,jsto,xy,ns)
c$$$
c$$$                        q(j,k,i) = qtmp + stepsize 
c$$$            
c$$$                        call calcobjairnoisefd(fobjp,q,count,nout,skipend,
c$$$     &                      jdim,kdim,deltat,scalef,x,y,jsta,jsto,xy,ns)
c$$$                     
c$$$                        q(j,k,i) = qtmp - stepsize
c$$$
c$$$                        call calcobjairnoisefd(fobjm,q,count,nout,skipend,
c$$$     &                      jdim,kdim,deltat,scalef,x,y,jsta,jsto,xy,ns)
c$$$                     
c$$$                        q(j,k,i) = qtmp - 2.d0*stepsize
c$$$
c$$$                        call calcobjairnoisefd(fobjmm,q,count,nout,skipend,
c$$$     &                      jdim,kdim,deltat,scalef,x,y,jsta,jsto,xy,ns)
c$$$                                         
c$$$                        q(j,k,i) = qtmp  
c$$$                        
c$$$                        dfobjdQtmp=(-fobjpp+8.d0*fobjp-
c$$$     &                 8.d0*fobjm+fobjmm )*xyj(j,k) / (12.d0*stepsize) 
c$$$
c$$$c                      if (abs(dfobjdQtmp-dfobjdQ(j,k,i)).gt.5.d-2) then
c$$$                     
c$$$                        print *, j,k,i,dfobjdQtmp,dfobjdQ(j,k,i)
c$$$
c$$$c                      end if
c$$$                                          
c$$$                     end do                  
c$$$                  end do
c$$$               end do
c$$$
c$$$               numiter=numitertmp
c$$$
c$$$               print *, 'Done calc dfdq fourth order'


      else if (obj_func .eq. 11) then
         
         call calcdJdQ11(jdim,kdim,ndim,q,xy,xyj,x,y,ns,dfobjdQ,
     &        deltat,scalef,nout,skipend,count,J1,J2)

               
c$$$               print *, 'Start calc dfdq',count
c$$$
c$$$               stepsize=fd_eta
c$$$
c$$$               do k=kbegin,kbegin+2 
c$$$                  do j = jtail2-3,jtail2
c$$$                     do i=1,4
c$$$                  
c$$$                        qtmp = q(j,k,i)
c$$$
c$$$                        q(j,k,i) = qtmp + stepsize 
c$$$            
c$$$                        call calcobjair11fd(fobjp,q,count,nout,skipend,
c$$$     &                       jdim,kdim,deltat,scalef,x,y,xy,xyj,ns)
c$$$                     
c$$$                        q(j,k,i) = qtmp - stepsize
c$$$
c$$$                        call calcobjair11fd(fobjm,q,count,nout,skipend,
c$$$     &                       jdim,kdim,deltat,scalef,x,y,xy,xyj,ns)
c$$$                                         
c$$$                        q(j,k,i) = qtmp  
c$$$                        
c$$$                     dfobjdQtmp=(fobjp-fobjm)*xyj(j,k)/(2.d0*stepsize)
c$$$
c$$$                      if (abs(dfobjdQtmp-dfobjdQ(j,k,i)).gt.0.5d-2) then
c$$$                     
c$$$                        print *, j,k,i,dfobjdQtmp,dfobjdQ(j,k,i)
c$$$
c$$$                      end if
c$$$                     
c$$$                     end do                  
c$$$                  end do
c$$$               end do
c$$$
c$$$               print *, 'Done calc dfdq'

c$$$         print *, 'Start calc dfdq fourth order',count
c$$$         
c$$$         numitertmp=numiter
c$$$         stepsize=fd_eta
c$$$
c$$$         do k=kbegin,kbegin+2
c$$$            do j = jtail1,jtail2
c$$$               do i=1,4
c$$$                  
c$$$                  qtmp = q(j,k,i)
c$$$                  
c$$$                  q(j,k,i) = qtmp + 2.d0*stepsize 
c$$$
c$$$                  call calcobjair11fd(fobjpp,q,count,nout,skipend,
c$$$     &                 jdim,kdim,deltat,scalef,x,y,xy,xyj,ns)
c$$$
c$$$                  q(j,k,i) = qtmp + stepsize 
c$$$                  
c$$$                  call calcobjair11fd(fobjp,q,count,nout,skipend,
c$$$     &                 jdim,kdim,deltat,scalef,x,y,xy,xyj,ns)
c$$$                  
c$$$                  q(j,k,i) = qtmp - stepsize
c$$$                  
c$$$                  call calcobjair11fd(fobjm,q,count,nout,skipend,
c$$$     &                 jdim,kdim,deltat,scalef,x,y,xy,xyj,ns)
c$$$                     
c$$$                  q(j,k,i) = qtmp - 2.d0*stepsize
c$$$                  
c$$$                  call calcobjair11fd(fobjmm,q,count,nout,skipend,
c$$$     &                 jdim,kdim,deltat,scalef,x,y,xy,xyj,ns)
c$$$                  
c$$$                  q(j,k,i) = qtmp  
c$$$                        
c$$$                  dfobjdQtmp=(-fobjpp+8.d0*fobjp-
c$$$     &                 8.d0*fobjm+fobjmm )*xyj(j,k) / (12.d0*stepsize) 
c$$$
c$$$c$$$                      if (abs(dfobjdQtmp-dfobjdQ(j,k,i)).gt.5.d-2) then
c$$$c$$$                     
c$$$                        print *, j,k,i,dfobjdQtmp,dfobjdQ(j,k,i)
c$$$c$$$
c$$$c$$$                      end if
c$$$                                          
c$$$               end do                  
c$$$            end do
c$$$         end do
c$$$
c$$$         numiter=numitertmp
c$$$
c$$$         print *, 'Done calc dfdq fourth order'

      else if (obj_func .eq. 12) then

         print *, 'obj_func .eq. 12 not implemented yet'
         stop

      else if (obj_func .eq. 13) then
c     --  vorticity minimization --
               
         do k = kbegin,kend
            do j = jbegin,jend
             
               if (k==kend) then
                  
                  vorti=(q(j,k,2)/q(j,k,1)-q(j,k-1,2)/q(j,k-1,1))-
     &                 (q(jplus(j),k,3)/q(jplus(j),k,1) - 
     &                 q(jminus(j),k,3)/q(jminus(j),k,1))*0.5d0                      

                  call calcdJdQ13(jdim,kdim,q,j,k,dfobjdQ,
     &                 vorti,deltat)
                  
               else if (k==kbegin) then
                  
                  vorti=(q(j,k+1,2)/q(j,k+1,1)-q(j,k,2)/q(j,k,1))-
     &                 (q(jplus(j),k,3)/q(jplus(j),k,1) - 
     &                 q(jminus(j),k,3)/q(jminus(j),k,1))*0.5d0
                     
                  
                  call calcdJdQ13(jdim,kdim,q,j,k,dfobjdQ,
     &                 vorti,deltat)
                            
               else 
                        
                  vorti=(q(j,k+1,2)/q(j,k+1,1)-q(j,k-1,2)/
     &                 q(j,k-1,1))*0.5d0 -
     &                 (q(jplus(j),k,3)/q(jplus(j),k,1) - 
     &                 q(jminus(j),k,3)/q(jminus(j),k,1))*0.5d0

                  call calcdJdQ13(jdim,kdim,q,j,k,dfobjdQ,
     &                 vorti,deltat)
                  
               end if           ! k choice

                  
            end do
         end do
         
      end if                    ! obj_func

      return
      end  !calcdfobjdQ





      subroutine solveadj(jdim,kdim,ndim,indx,iex,as,ja,ia,ast,jat,iat, 
     &     na,rhstmp,psi,pa,jpa,ipa,pat,jpt,ipt,work1)
      
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim,kdim,ndim,its,js,je,ks,ke,na
      integer indx(jdim,kdim)
      double precision rhstmp(jdim,kdim,ndim),psi(jdim,kdim,ndim)

      integer ia(jdim*kdim*ndim+2),iat(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)  

      integer i, j, k, jki, iex(jdim,kdim,4)
      double precision work1(maxjk,100),rhsnamult
      
      double precision diag(jdim*kdim*5), work2(jdim*kdim*5)
      double precision rhs(jdim*kdim*5+1), sol(jdim*kdim*5+1)

c     -- local temp. sparse adjoint arrays --
c     work array size *95 for gmres *8 for bcgstab--
      integer wkg
      parameter (wkg=maxjk*nblk*8)
      double precision,allocatable :: wk_gmres(:)

c     -- ILU arrays --
      integer ierr
      integer,dimension(:),allocatable :: levs,jw
      double precision alu(iwk)
      integer jlu(iwk),ju(maxjk*nblk)

      logical scalena

cmpr  Allocate some of the big arrays dynamically
      allocate(levs(iwk),jw(3*maxjk*nblk),wk_gmres(wkg))

c     -- transpose -> ast=[dRstar/dQhat]^T --

      call csrcsc (na, 1, 1, as, ja, ia, ast, jat, iat)

c     -- same for preconditioner
      call csrcsc (na, 1, 1, pa, jpa, ipa, pat, jpt, ipt)

c     -- diagonal scaling for all b.c. equations --
c     -- this operation is performed after taking the transpose,
c     special case for the adjoint problem. --
      scalena=.true.
      call scale_bc (2, jdim, kdim, ndim, indx, iex, iat, jat, ast,
     &     ipt, jpt, pat, rhstmp, work1(1,1), work1(1,5), rhsnamult,
     &     scalena)

c     -- set up rhs and initial guess--

      do i=1,4   
         do k = kbegin,kend 
            do j = jbegin,jend

               jki = ( indx(j,k) - 1 )*ndim + i
               rhs(jki)=rhstmp(j,k,i)
c               sol(jki)=psi(j,k,i)
               sol(jki)=psi(j,k,iex(j,k,i))
               
            end do
         end do
      end do

      if (viscous .and. turbulnt .and. itmodel .eq.2) then 
         i = 5 
         do k = kbegin,kend
            do j = jbegin,jend 
               jki = ( indx(j,k) - 1 )*ndim + i
               rhs(jki)=rhstmp(j,k,i)
               sol(jki) = psi(j,k,i) 
            end do
         end do
      end if

c     update psi=inv(ast)*rhs with bcgstab

c     -- set the parameters for the iterative solvers

      ipar(1) = 0
      ipar(2) = 2
      ipar(3) = 1
      ipar(4) = wkg
      ipar(5) = IM_GMRES
      ipar(6) = ITER_GMRES
      fpar(1) = EPS_GMRES
      fpar(2) = 1.d-14

      its=0
      call iluk (na,pat,jpt,ipt,LFIL,alu,jlu,ju,levs,iwk,
     &     work1,jw,ierr)

      if (ierr .ne. 0) print *, 'LU preconditioner error:', ierr

 77   call BCGSTAB(na,rhs,sol,ipar,fpar,wk_gmres)
            
c     --  output the residuals and update--

      if (ipar(7).ne.its) then
c         write (*,*) its, real(fpar(5))
         its = ipar(7)
      endif
      
      if (ipar(1).eq.1) then
         call amux(na,wk_gmres(ipar(8)),wk_gmres(ipar(9)),ast,jat,iat)
         goto 77
      else if (ipar(1).eq.2) then
         call atmux(na,wk_gmres(ipar(8)),wk_gmres(ipar(9)),ast,jat,iat)
         goto 77
      else if (ipar(1).eq.3 .or. ipar(1).eq.5) then
         call lusol(na,wk_gmres(ipar(8)),wk_gmres(ipar(9)),alu,jlu,ju)
         goto 77
      else if (ipar(1).eq.4 .or. ipar(1).eq.6) then
         call lutsol(na,wk_gmres(ipar(8)),wk_gmres(ipar(9)),alu,jlu,ju)
         goto 77
      else if (ipar(1).le.0) then
         if (ipar(1).eq.0) then
c            print *, 'Iterations to converge:', its,real(fpar(5))
         else if (ipar(1).eq.-1) then
            print *, 'Iterative solver has iterated too many times.'
         else if (ipar(1).eq.-2) then
            print *, 'Iterative solver was not given enough work space.'
            print *, 'The work space should at least have ', ipar(4),
     &           ' elements.'
         else if (ipar(1).eq.-3) then
            print *, 'Iterative solver is facing a break-down.'
         else
            print *, 'Iterative solver terminated. code =', ipar(1)
         endif
      endif         

c     -- copy solution vector to psi and unshuffle it--

      do i=1,4
         do k = kbegin,kend
            do j = jbegin,jend            
               jki = ( indx(j,k) - 1 )*ndim + i
               psi(j,k,iex(j,k,i))=sol(jki)              
            end do
         end do
      end do

      if (viscous .and. turbulnt .and. itmodel .eq.2) then 
         i = 5 
         do k = kbegin,kend
            do j = jbegin,jend 
               jki = ( indx(j,k) - 1 )*ndim + i
               psi(j,k,i) = sol(jki)
            end do
         end do
      end if

cmpr  Deallocate the big arrays
      deallocate(levs,jw,wk_gmres)

      return
      end ! solveadj
