c----------------------------------------------------------------------
c     -- interpolate Cp distribution for target Cp --
c     
c     written by: marian nemec
c     date: sept 2000
c----------------------------------------------------------------------

      subroutine cp_interp (jdim, kdim, cp_tar, x)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      double precision cp_tar(jbody,2), x(jdim,kdim)
      double precision cpi(ibody), ysp(ibody), xsp(ibody)

      logical spline_cp

c     -- check if target pressure distribution x-coordinate matches 
c     body coordinates of initial grid --
      i = 1
      spline_cp = .false.
      do j = jtail1,jtail2
        if ( dabs( cp_tar(i,1) - x(j,1) ) .gt. 1.d-8 ) then
          spline_cp = .true.
          goto 100
        end if
        i = i+1
      end do

 100  continue

      if (spline_cp) then
        
        write (*,70) 
 70     format (3x,'Target Cp splined to match current grid.')

c     -- find leading edge and initialize node index --
c     -- spline is done wrt node index with linear interpolation for
c     the x-coordinate -- 
        jle = 0
        do i = 1,jbody
          if ( dabs(cp_tar(i,1)) .lt. 1.d-12 ) jle = i
          xsp(i) = dble(i)
        end do

c     -- numerical recipes subroutines: spline and splint --
c     -- natural spline end points --
        yp1 = 2.d30
        ypn = 2.d30
        call spline ( xsp, cp_tar(1,2), jbody, yp1, ypn, ysp)
c     -- interpolate lower surface --
        j = jtail1+1

 200    continue

        if (j.eq.jtail1+jle) goto 300

        do i = 1, jle-1

          if (x(j,1).lt.cp_tar(i,1) .and. x(j,1).gt.cp_tar(i+1,1)) then 
            xint = xsp(i) + (x(j,1) - cp_tar(i,1)) / (cp_tar(i+1,1) -
     &            cp_tar(i,1)) 
            ii = j - jtail1 + 1
c     -- obtain interpolated cp at xint, store in cpi --
            call splint (xsp, cp_tar(1,2), ysp, jbody, xint, cpi(ii)) 
            j = j + 1
            goto 200
          end if

        end do

 300    continue

c     -- interpolate upper surface --
        j = jtail1 + jle

 400    continue

        if (j.eq.jtail2) goto 500

        do i = jle, jbody-1

          if (x(j,1).gt.cp_tar(i,1) .and. x(j,1).lt.cp_tar(i+1,1)) then 
            xint = xsp(i) + (x(j,1) - cp_tar(i,1)) / (cp_tar(i+1,1) -
     &            cp_tar(i,1)) 
            ii = j - jtail1 + 1 
c     -- obtain interpolated cp at xint, store in cpi --
            call splint (xsp, cp_tar(1,2), ysp, jbody, xint, cpi(ii))
            j = j + 1
            goto 400
          end if

        end do

 500    continue
        
c     -- store cp interpolant --
        do i = 2,jle-1
          cp_tar(i,1) = x(jtail1+i-1,1)
          cp_tar(i,2) = cpi(i)
        end do      

        do i = jle+1,jbody-1
          cp_tar(i,1) = x(jtail1+i-1,1)
          cp_tar(i,2) = cpi(i)
        end do  

c     do i = 1,jbody
c     write (gmres_unit,80) i, cp_tar(i,1), cp_tar(i,2)
c     end do
c     80     format (i5,5e18.8)

      end if

      return
      end                       !cp_interp
