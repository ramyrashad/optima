c     ------------------------------------------------------------------
c     -- perturb q for finite difference matrix-vector approximation --
c     -- m. nemec, sept. 2001 --
c     -- calling routine: framux.f --
c     ------------------------------------------------------------------
      subroutine fr_dq( ntvar, jmax, kmax, nmax, ks, ke, js, je, indx,
     &     q, qr, turre, turrer, xyj, eps, v1)

      implicit none
      integer ntvar, jmax, kmax, nmax, ks, ke, js, je, indx(jmax,kmax)
      integer j,k,n,jk
      double precision q(jmax,kmax,4),qr(jmax,kmax,4),xyj(jmax,kmax)
      double precision turre(jmax,kmax),turrer(jmax,kmax),v1(ntvar)
      double precision eps
      
      do n = 1,4
         do k = ks,ke
            do j = js,je
               jk = ( indx(j,k)-1 )*nmax + n
               q(j,k,n) = qr(j,k,n) + eps*v1(jk)
            end do
         end do
      end do

      if (nmax.eq.5) then
         do k = ks,ke
            do j = js,je
               jk = ( indx(j,k)-1 )*5 + 5
               turre(j,k) = turrer(j,k) + eps*v1(jk)*xyj(j,k)
            end do
         end do
      end if

      return
      end                       !fr_dq
