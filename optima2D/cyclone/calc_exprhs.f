      subroutine calc_exprhs(jdim,kdim,D_hat,exprhs,a_jk)
 
#include "../include/arcom.inc"

      double precision exprhs(jdim,kdim,4)
      double precision D_hat(jdim,kdim,4,6)
      double precision a_jk(6,6)

      do 400 n=1,4
      do 400 k=1,kdim
      do 400 j=1,jdim
         exprhs(j,k,n)=0.0
 400  continue
      if (jstage.gt.1)then
         do 500 n=1,4
         do 500 k=klow,kup
         do 500 j=jlow,jup
         do 500 i_stage=1,jstage-1
            exprhs(j,k,n)=exprhs(j,k,n)+
     &           (((a_jk(jstage,i_stage))/(a_jk(jstage,jstage)))*
     &           D_hat(j,k,n,i_stage))
 500     continue
      endif


      return
      end                                                       
