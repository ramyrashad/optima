#***********************************************************************
#    Module:  Makefile (examples directory)
# 
#    Copyright (C) 1996 Harold Youngren, Mark Drela 
# 
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Library General Public
#    License as published by the Free Software Foundation; either
#    version 2 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Library General Public License for more details.
#
#    You should have received a copy of the GNU Library General Public
#    License along with this library; if not, write to the Free
#    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# 
#    Report problems to:    guppy@netcom.com 
#                        or drela@henry.mit.edu  
#***********************************************************************



########################################
# makefile for Xplot11 library examples
########################################

#PLTLIB  = -L.. -lPlt 
#PLTLIB  = -L.. -lPltDP 
#PLTLIB  = $(HOME)/lib/libPltDP.a 
PLTLIB  = ../../../lib/libPltDP.a 
#
LINKLIB =  -L/usr/X11R6/lib -lX11


#### Link libs required for xlf90 at ABB  (HHY 9/96)
####LINKLIB =  -lX11   -L/venus/u1/fortran/libfor -lxlfabb 
 

###====================================
###  Default compilers and flags, install commands
F77 = f77
CC  = cc
DP = 
# Uncomment to make double-precision version
DP = -r8
#FFLAGS  = -O $(DP)
#CFLAGS  = -O
# MN flags
FFLAGS  = -O2 -mips4 $(DP)
CFLAGS  = -O2 -mips4 
AR = ar r 
RANLIB = ranlib

###------------------------------------
### For Linux
###  Note compiler options for Linux:
###   f77    script (calls f2c/gcc) note bug in Slackware 3.0 f77 script
###   fort77 script (calls f2c/gcc) perl script from RH or from yaf77
###   g77    the GNU F77 compiler
###
#F77 = fort77 
#F77 = g77 
#CC  = cc
# Uncomment to make double-precision version
#DP = -r8
#FFLAGS  = -O2 $(DP)
#CFLAGS  = -O2
#AR = ar r
#RANLIB = ranlib 


###------------------------------------
###  Uncomment for DEC OSF/Alpha
# Uncomment to make double-precision version
#F77 = f77
#DP = -r8
#
#CFLAGS = -O4 -float 
#FFLAGS = -O4 $(DP)
#CFLAGS = -O0 -float -g 
#FFLAGS = -O0 $(DP) -g


###------------------------------------
###  Uncomment for RS/6000
# Uncomment to make double-precision version
#F77 = f77
#DP = -qautodbl=dblpad4
#
#FFLAGS = -O -qextname -qfixed $(DP)


###------------------------------------
###  Uncomment for Sun Open-Windows 
###  (give location of X11/xxx.h include files)
#F77 = f77
#CFLAGS = -O -I/usr/openwin/share/include
# Uncomment to make double-precision version
#DP = -r8
#FFLAGS = -O $(DP)


###------------------------------------
###  Uncomment for HP-9000
###  (use ANSI-C standard, use underscored C-routine names)
# Uncomment to make double-precision version
#F77 = f77
#DP = -r8
#
#CFLAGS = -O -Aa
#FFLAGS = -O +ppu $(DP)



PROGS = volts volts_old \
        symbols symbolsall \
	squares squaresdoublebuff \
	spectrum cmap2 cmap3 defmap \
        gridtest zoomtest contest


examples:  $(PROGS)


test:  $(PROGS)

clean:
	-/bin/rm -f *.o
	-/bin/rm -f $(PROGS)
	-/bin/rm -f plot*.ps



#Test routines for package

volts: volts.o 
	$(F77) -o volts volts.o $(PLTLIB) $(LINKLIB)

volts_old: volts_old.o 
	$(F77) -o volts_old volts_old.o $(PLTLIB) $(LINKLIB)

symbols: symbols.o 
	$(F77) -o symbols symbols.o $(PLTLIB) $(LINKLIB)

symbolsall: symbolsall.o 
	$(F77) -o symbolsall symbolsall.o $(PLTLIB) $(LINKLIB)

squares: squares.o
	$(F77) -o squares squares.o $(PLTLIB) $(LINKLIB)

squaresdoublebuff: squaresdoublebuff.o
	$(F77) -o squaresdoublebuff squaresdoublebuff.o $(PLTLIB) $(LINKLIB)

spectrum: spectrum.o 
	$(F77) -o spectrum spectrum.o $(PLTLIB) $(LINKLIB)

cmap2: cmap2.o 
	$(F77) -o cmap2 cmap2.o $(PLTLIB) $(LINKLIB)

cmap3: cmap3.o 
	$(F77) -o cmap3 cmap3.o $(PLTLIB) $(LINKLIB)

defmap: defmap.o
	$(F77) -o defmap defmap.o $(PLTLIB) $(LINKLIB)

gridtest: gridtest.o 
	$(F77) -o gridtest gridtest.o $(PLTLIB) $(LINKLIB)

zoomtest: zoomtest.o 
	$(F77) -o zoomtest zoomtest.o $(PLTLIB) $(LINKLIB)

contest: contest.o 
	$(F77) -o contest contest.o $(PLTLIB) $(LINKLIB)



volts.o: volts.f
	$(F77) -c $(FFLAGS) $<

volts_old.o: volts_old.f
	$(F77) -c $(FFLAGS) $<

symbols.o: symbols.f
	$(F77) -c $(FFLAGS) $<

symbolsall.o: symbolsall.f
	$(F77) -c $(FFLAGS) $<

squares.o: squares.f
	$(F77) -c $(FFLAGS) $<

squaresdoublebuff.o: squaresdoublebuff.f
	$(F77) -c $(FFLAGS) $<

spectrum.o: spectrum.f
	$(F77) -c $(FFLAGS) $<

cmap2.o: cmap2.f
	$(F77) -c $(FFLAGS) $<

cmap3.o: cmap3.f
	$(F77) -c $(FFLAGS) $<

defmap.o: defmap.f
	$(F77) -c $(FFLAGS) $<

gridtest.o: gridtest.f
	$(F77) -c $(FFLAGS) $<

zoomtest.o: zoomtest.f
	$(F77) -c $(FFLAGS) $<

contest.o: contest.f
	$(F77) -c $(FFLAGS) $<

#May need to specify these on a brain-dead make system
#.f.o:	$(F77) -c $(FFLAGS) $<
#.c.o:	$(CC)  -c $(CFLAGS) $<







