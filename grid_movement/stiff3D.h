#ifndef STIFF3D_H
#define STIFF3D_H

#define Gauss 0.577350269189626   // position of gauss point 
#define nu 0.2               // poisson's ratio

#include "Node.h"
#include <fstream>

void get_K_hex (const vector <double*>& vertices, double** K, 
		double* J, double* invJ, vector < vector<double> >& dN,
		vector < vector<double> >& B, 
		vector < vector<double> >& tempI, const double& elemQo, 
		double& meshQ);

void get_Q_hex (const vector <double*>& vertices, double& Q);

void aspect_tet (const double* x1, const double* x2, const double* x3,
		 const double* x4, double& Q);

void volume_tet(const double* a, const double* b, const double* c,
		const double* d, double& volume, double& area);

void max_length_tet (const double* x1, const double* x2, 
		     const double* x3, const double* x4, double& maxL);

void calc_len (const double* x1, const double* x2, double& len);

void mag_cross_vec (const double* x1, const double* x2, double& mag);

void cross_vec3D (const double* x0, const double* x1,  
		  const double* x2, double* prod);

void area_tet (const double* a1, const double* a2, const double* a3,
	       const double* a4, double& area);

void form_dN ( vector <vector<double> >& dN, const int& gaussPT);

void invert_3x3 (const double* A, double *invA, double& detA);

void get_B (const vector <double*>& vertices, 
	    const vector <vector <double> >& dN, double* J, 
            double* invJ, vector < vector <double> >& B, double& detJ);

void calc_K (const vector < vector <double> >& B, 
	     const double* D, double** K, const double& detJ,
	     vector < vector <double> >& tempI);

void get_K_Tet (const vector <double*>& vertices, double** K);

void volume_hex (const vector <double*>& corners, double& volume);

#endif  // STIFF_H
