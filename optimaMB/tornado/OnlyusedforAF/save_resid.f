****************************************************************
****    subroutine to save residual info when using MG      ****
****************************************************************
c     Calling subroutine: multigrid.f
c
      subroutine save_resid(svresid,svresidmx,svmaxres)
c
      include '../include/common.inc'
c
      dimension svmaxres(3)
c
      svresid = resid
      do i=1,3
        svmaxres(i) = maxres(i)
      enddo
      svresidmx = residmx
c     
      return
      end
