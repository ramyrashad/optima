#ifndef STIFF_H
#define STIFF_H

//#define Young 4.0   // modulus of elasticity
#define Gauss 0.577350269189626   // position of gauss point 
#define poisson -0.3
#define PowerLaw 10   // power-law model used to stiffen mesh

#include "Node.h"

void get_K (const vector<double*>& vertices, double** K,
	const double& elemQo, double& meshQ);

void get_Q (const vector<double*>& vertices, double& Q);

void area_tri (double& a, double& b, double& c, double& area);

void calc_lengths (const vector<double*>& vertices, double* len);

void form_dN ( vector <vector<double> >& dNa,  vector <vector<double> >& dNb, 
	       vector <vector <double> >& dNc,  vector <vector <double> >& dNd);

void calc_B (const vector <double*>& vertices, 
	     const vector <vector <double> >& dN, 
	     double* J, vector < vector <double> >& B, double& detJ);

void calc_I (const vector < vector <double> >& B, 
	     const vector < vector <double> >& D, 
	     double** I, const double& detJ);

void area_quad (const vector<double*>& vertices, double& a);


#endif  // STIFF_H
