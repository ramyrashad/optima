c        *********************
c        advective terms in xi
c        *********************

         zz=1.d0/6.d0
         do k=klow,kup
         do j=jlow+1,jup-1
            jm2=j-2
            jm1=j-1
            jp1=j+1
            jp2=j+2
            sgnu = sign(1.,uu(j,k))
            app  = .5*(1.+sgnu)
            apm  = .5*(1.-sgnu)
c
            tm2=turre(jm2,k)
            tm1=turre(jm1,k)
            tt =turre(j,k)
            tp1=turre(jp1,k)
            tp2=turre(jp2,k)
c
            fy(j,k)= fy(j,k) - uu(j,k)*(
c
c           -third-order biased upwinding
     &      app*(tm2 - 6.d0*tm1 + 3.d0*tt + 2.d0*tp1)
     &     +apm*(-2.d0*tm1 - 3.d0*tt + 6.d0*tp1 - tp2))*zz
c
            bx(k,j)   = bx(k,j)   - uu(j,k)*app
            cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
            dx(k,j)   = dx(k,j)   + uu(j,k)*apm
         enddo
         enddo
c
         do k=klow,kup
           j=jlow
           tm1=turre(j-1,k)
           tt =turre(j,k)
           tp1=turre(j+1,k)
           tp2=turre(j+2,k)
           
           sgnu = sign(1.,uu(j,k))
           app  = .5*(1.+sgnu)
           apm  = .5*(1.-sgnu)

c           app  = 0.
c           apm  = 1.

           fy(j,k)= fy(j,k) - uu(j,k)*(
c                -first-order / third-order mix upwinding 
c     &           app*(-3.d0*tm1+4.d0*tt-tp1)*.5d0
     &           app*(tt-tm1)
     &         + apm*(tp1-tt))
c     &         + apm*(-2.d0*tm1 - 3.d0*tt + 6.d0*tp1 - tp2)*zz)
           bx(k,j)   = bx(k,j)   - uu(j,k)*app
           cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
           dx(k,j)   = dx(k,j)   + uu(j,k)*apm
c
c
c
           j=jup

           tm2=turre(j-2,k)
           tm1=turre(j-1,k)
           tt =turre(j,k)
           tp1=turre(j+1,k)

           sgnu = sign(1.,uu(j,k))
           app  = .5*(1.+sgnu)
           apm  = .5*(1.-sgnu)

c           app  = 1.
c           apm  = 0.

           fy(j,k)= fy(j,k) - uu(j,k)*(
c                -first-order / third-order mix upwinding 
c     &           app*(tm2 - 6.d0*tm1 + 3.d0*tt + 2.d0*tp1)*zz
     &           app*(tt-tm1)
     &         + apm*(tp1-tt))
c     &         + apm*(tm1-4.d0*tt+3.d0*tp1)*.5d0)
           bx(k,j)   = bx(k,j)   - uu(j,k)*app
           cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
           dx(k,j)   = dx(k,j)   + uu(j,k)*apm
         enddo
