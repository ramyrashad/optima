      subroutine etaexpl(lvl,lev_top,mglev,jdim,kdim,q,s,press,sndsp,   
     &      turmu,fmu,vort,turre,vk,ve,x,y,xy,xyj,ett,ds,vv,ccy,uu,
     &      coef2y,coef4y,spect,precon,tmp)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/trip.inc"      
#include "../include/scratch.inc"
#include "../include/turb.inc"

      integer jdim,kdim,ncopystart,ncopyend,i,lq,lg,ix,iy,ii,nside1,i2
      integer nside2,lvl,lev_top,mglev,nteavgstart,nteavgend,iblk1,ixy
      integer iblk2,n

c     -ptmax is used in original version of Spalart-Almaras turb. model.
      double precision ptmax(maxblk)

      double precision q(jdim*kdim*4),spect(jdim*kdim*3),s(jdim*kdim*4)
      double precision press(jdim*kdim),precon(jdim*kdim*8)
      double precision sndsp(jdim*kdim),xy(jdim*kdim*4),xyj(jdim*kdim)
      double precision ett(jdim*kdim),  ds(jdim*kdim),tmp(jdim*kdim*4)
      double precision x(jdim*kdim),    y(jdim*kdim),turmu(jdim*kdim)
      double precision vort(jdim*kdim), turre(jdim*kdim),fmu(jdim*kdim)
      double precision vk(jdim*kdim),   ve(jdim*kdim)
      double precision vv(jdim*kdim),   ccy(jdim*kdim),  uu(jdim*kdim)
      double precision coef2y(jdim*kdim),coef4y(jdim*kdim)

      logical first,frozen,meanflow

      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

c ********************************************************************
c **                      explicit operators                        **
c ********************************************************************

      if (mg .or. gseq) then
c       note: divide operation higher priority than multiplication
        ncopystart = ncopy/mglev*(lvl-1) + 1
        ncopyend   = ncopy/mglev*lvl
        nteavgstart= nteavg/mglev*(lvl-1) + 1
        nteavgend  = nteavg/mglev*lvl
      else
        ncopystart = 1
        ncopyend   = ncopy
        nteavgstart= 1
        nteavgend  = nteavg
      endif

c     -explicit operator in eta
      DO i=nblkstart,nblkend
         lq=lqptr(i)
         lg=lgptr(i)
         CALL rhsy(jmax(i),kmax(i),q(lq),s(lq),
     &        press(lg),xy(lq),xyj(lg),ett(lg),d,
     &        jminbnd(i),jmaxbnd(i),kbegin(i),kend(i),klow(i),kup(i))
      ENDDO

      ix = 3
      iy = 4
      DO i=nblkstart,nblkend
         lg=lgptr(i)
         lq=lqptr(i)
         call eigval(ix,iy,jmax(i),kmax(i),q(lq),press(lg),
     &        sndsp(lg),xyj(lg),xy(lq),ett(lg),vv(lg),ccy(lg),
     &        jminbnd(i),jmaxbnd(i),kbegin2(i),kend2(i))
      ENDDO

c$$$c     -- optimization debug: freezing absolute values --
c$$$      if (frozen .and. ifrzw.eq.1) then
c$$$         do ii=nblkstart,nblkend
c$$$            lg = lgptr(ii)
c$$$            call copy_array( jmax(ii), kmax(ii), 1, fra2(lg),
c$$$     &           vv(lg))
c$$$         end do
c$$$      end if

c     -range for s is low/up so no halo copy is required

c     -- mn --
c     -- tresid2 holds the s-a residual which is required in residl2 --
c     -- must be zeroed here to ensure no contribution for non turbulent
c     cases --

      tresid2 = 0.d0

c     -viscous routines
      IF (viscous) THEN
c     -compute vorticity for turbulence model
         IF (turbulnt .AND. lvl.EQ.lev_top) THEN

c     -compute eddy viscosity                
c     -only permitted on sides 1 and 3

            tresid = 0.0
            tresmx = 0.0
            
            if (numiter.eq.istart) then
               first = .true.
            else
               first = .false.
            endif

            DO ii=nblkstart,nblkend
               IF (iturb.EQ.3) THEN  
c     -for S.D revision of Spalart-Allmaras turb. model
c     -note that the turb. model sweeps in both directions
c     and thus must sweep from jbegin-jend and kbegin-kend
c     ... not jminbnd-jmaxbnd and kbegin-kend.
                  lg=lgptr(ii)
                  lq=lqptr(ii)

                  CALL calcfnu(ii,jmax(ii),kmax(ii),
     &                 jbegin(ii),jend(ii),
     &                 kbegin2(ii),kend2(ii),
     &                 q(lq),fmu(lg),work1(lg),xyj(lg))

                  CALL spalart_allmaras(ii,jmax(ii),kmax(ii),
     &                 jtlo(ii),jtup(ii),
     &                 jbegin(ii),jend(ii),
     &                 kbegin(ii),kend(ii),
     &                 jlow(ii),jup(ii),
     &                 klow(ii),kup(ii),
     &                 q(lq),turmu(lg),x(lg),y(lg),xy(lq),
     &                 xyj(lg),press(lg),vort(lg),
     &                 turre(lg),work1(lg),uu(lg),vv(lg),
     &                 c,c(1,2),c(1,3),c(1,4),c(1,5),
     &                 c(1,6),c(1,7),c(1,8),c(1,9),
     &                 c(1,10),c(1,11),zdamp1m(lg),
     &                 smin(lg),kwall(lg),jmid(ii),bnd(1,ii),first,
     &                 d,d(1,4),d(1,5),d(1,6),d(1,7),d(1,8),d(1,9),
     &                 d(1,10),d(1,11),d(1,12),d(1,13),
     &                 d(1,14),d(1,15),d(1,16),
     &                 itrans,itranj,itranside,nhalo,
     &                 jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii),
     &                 nsd1(ii),nsd2(ii),nsd3(ii),nsd4(ii),
     &                 icoord,xcoord,ycoord)
               ELSEIF (iturb.NE.3) THEN
                  WRITE(*,*)"WARNING: This version only supports"
                  WRITE(*,*)"Spalart-Allmaras Turbulence Model"
                  WRITE(*,*)"Check older versions for other models"
                  WRITE(*,*)"Stop at etaexpl.f"
                  STOP
               ENDIF
            ENDDO

c         -interface condition turre
c           -copy turre to halo points (do halo columns first)
c           -apply averaged condition 
            DO ii=nblkstart,nblkend
               DO nside1=2,4,2
                  i2 = lblkpt(ii,nside1)
                  IF (i2.NE.0) THEN
                     nside2 = lsidept(ii,nside1)
                     CALL interf(nhalo,nside1,nside2,
     &                    jmax(ii),kmax(ii),1,turre(lgptr(ii)),
     &                    jminbnd(ii),jmaxbnd(ii),
     &                    kminbnd(ii),kmaxbnd(ii),
     &                    jmax(i2),kmax(i2),turre(lgptr(i2)),
     &                    jminbnd(i2),jmaxbnd(i2),
     &                    kminbnd(i2),kmaxbnd(i2))
                     CALL halocp(ii,nhalo,1,nside1,nside2,
     &                    jmax(ii),kmax(ii),turre(lgptr(ii)),
     &                    jbegin2(ii),jend2(ii),
     &                    kminbnd(ii),kmaxbnd(ii),
     &                    jmax(i2),kmax(i2),turre(lgptr(i2)),
     &                    jbegin2(i2),jend2(i2),
     &                    kminbnd(i2),kmaxbnd(i2))

c     Stan D.R 29/09/98
c     -the following two subroutine calls 
c     not in  original code.  Put in because zdamp1m
c     needs to be fixed before using it in call to calcturmu 
c     below.
                  ENDIF
               ENDDO
            ENDDO

c     -do halo rows now
            DO ii=nblkstart,nblkend
               DO nside1=1,3,2
                  i2 = lblkpt(ii,nside1)
                  IF (ibctype(ii,nside1).EQ.5) THEN
                     nside2 = lsidept(ii,nside1)
                     call halocp(ii,nhalo,1,nside1,nside2,
     &                    jmax(ii),kmax(ii),turre(lgptr(ii)),
     &                    jbegin2(ii),jend2(ii),
     &                    kbegin2(ii),kend2(ii),
     &                    jmax(i2),kmax(i2),turre(lgptr(i2)),
     &                    jbegin2(i2),jend2(i2),
     &                    kbegin2(i2),kend2(i2))
                  ENDIF
               ENDDO
            ENDDO

            meanflow=.false.    !bcavg for meanflow is done in integrat.f

            DO ii=nblkstart,nblkend
               DO nside1=1,3,2
                  i2 = lblkpt(ii,nside1)
                  IF (i2.NE.0 .AND. ibctype(ii,nside1).EQ.5) THEN
                     nside2 = lsidept(ii,nside1)
                     CALL bcavg(nside1,nside2,
     &                    jmax(ii),kmax(ii),q(lqptr(ii)),
     &                    xyj(lgptr(ii)),press(lgptr(ii)),
     &                    turre(lgptr(ii)),turmu(lgptr(ii)),
     &                    vk(lgptr(ii)),ve(lgptr(ii)),
     &                    jminbnd(ii),jmaxbnd(ii),
     &                    kminbnd(ii),kmaxbnd(ii),
     &                    jmax(i2),kmax(i2),q(lqptr(i2)),
     &                    xyj(lgptr(i2)),press(lgptr(i2)),
     &                    turre(lgptr(i2)),turmu(lgptr(i2)),
     &                    vk(lgptr(i2)),ve(lgptr(i2)),
     &                    jminbnd(i2),jmaxbnd(i2),
     &                    kminbnd(i2),kmaxbnd(i2),
     &                    meanflow)
                  ENDIF
               ENDDO
            ENDDO

c     -copy turre at any trailing edge points
c     -even though turre is zero on element boundary, the
c     zero value must be copied to adjacent blocks which
c     might have non-zero values (after applying bcavg) for
c     corresponding trailing edge node.  Bcavg will average
c     two-non-zero values and put them in t.e. location.

            meanflow=.true.

            DO ii=ncopystart,ncopyend
               iblk1=ibte1(ii)
               iblk2=ibte2(ii)
               CALL fixte(jmax(iblk1),kmax(iblk1),
     &              ijte1(ii),ikte1(ii),
     &              q(lqptr(iblk1)),xyj(lgptr(iblk1)),
     &              press(lgptr(iblk1)),sndsp(lgptr(iblk1)),
     &              turre(lgptr(iblk1)),
     &              vk(lgptr(iblk1)),ve(lgptr(iblk1)),
     &              jmax(iblk2),kmax(iblk2),
     &              ijte2(ii),ikte2(ii),
     &              q(lqptr(iblk2)),xyj(lgptr(iblk2)),
     &              press(lgptr(iblk2)),sndsp(lgptr(iblk2)),
     &              turre(lgptr(iblk2)),
     &              vk(lgptr(iblk2)),ve(lgptr(iblk2)),
     &              meanflow)
            ENDDO

c     Stan D.R/Luis M.  29/09/98
c     Note: Originally, routines like bcavg and calls to interf and 
c     fixarray took into account the variables
c     q,turre,vk, and ve.  We felt, that since the code
c     uses turmu for the viscous subroutines and that
c     turmu is dependent on turre, it would be wise to also
c     average and fix turmu.  But for grids where neighbouring
c     blocks have side 2 as a common interface or side 4 as a
c     common interface, one could get into trouble calling interf
c     for turmu.  Turmu is calculated at k+1/2 nodes.  One block
c     will have k increasing in direction from airfoil to
c     farfield while the other will have k decreasing.
c     Hence, the two coincident points really do not have the
c     same turmu.  Think about it.  So we do all the fixing
c     necessary on turre and then compute turmu after 
c     ... like right here.

c     iordtmp=iord
c     iord=2

         ENDIF   ! if turbulnt

c     -viscous rhs
         IF (viseta) THEN
            DO ii=nblkstart,nblkend
               CALL visrhsny(ii,jmax(ii),kmax(ii),
     &              q(lqptr(ii)),press(lgptr(ii)),
     &              s(lqptr(ii)),turmu(lgptr(ii)),
     &              fmu(lgptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &              d,d(1,2),d(1,3),d(1,4),d(1,5), 
     &              d(1,6),d(1,7),d(1,8),d(1,9),d(1,10),
     &              jminbnd(ii),jmaxbnd(ii),
     &              kbegin(ii)-nsd1(ii),kend(ii)+nsd3(ii),
     &              klow(ii)-nsd1(ii),kup(ii)+nsd3(ii))
            ENDDO
         ENDIF

c     -range of s is low/up so copy isn't required

      ENDIF   ! viscous then 

c     ****************************************************************
c     *                                                              *
c     *       matrix artificial dissipation in eta direction         *
c     *                                                              *
c     ****************************************************************

      ix = 3
      iy = 4

      IF (idmodel.EQ.2) THEN
         do 105 ii=nblkstart,nblkend
            lg=lgptr(ii)
            lq=lqptr(ii)
            call mspecty(jmax(ii),kmax(ii),vv(lg),ccy(lg),
     &           xyj(lg),xy(lq),spect(lspptr(ii)),
     &           jminbnd(ii),jmaxbnd(ii),
     &           kbegin2(ii),kend2(ii))
 105     continue

c     -calculate the gradient coef in y & store it in array coef2y
c     -bypass logic if dis2y = 0.0
         if (dis2y.ne.0.0) then
            ixy=2
            do 106 ii=nblkstart,nblkend
               call gradcoef(ixy,jmax(ii),kmax(ii),press(lgptr(ii)),
     &              xyj(lgptr(ii)),coef2y(lgptr(ii)),work1(lgptr(ii)),
     &              jminbnd(ii),jmaxbnd(ii),
     &              kbegin2(ii),kend2(ii),
     &              xi1mesh(ii),xi2mesh(ii),.true.,.true.)
 106        continue
         endif

c     -compute the second and fourth order dissipation coefficients
         do 120 ii=nblkstart,nblkend
            call mcoef24y(jmax(ii),kmax(ii),
     &           coef2y(lgptr(ii)),coef4y(lgptr(ii)),
     &           jminbnd(ii),jmaxbnd(ii),
     &           kbegin2(ii),kend2(ii))
 120     continue

c     -compute the second and fourth difference dissipation
         do 130  ii=nblkstart,nblkend
            call expmaty(jmax(ii),kmax(ii),q(lqptr(ii)),
     &           coef2y(lgptr(ii)),
     &           coef4y(lgptr(ii)),sndsp(lgptr(ii)),s(lqptr(ii)),
     &           spect(lspptr(ii)),xyj(lgptr(ii)),press(lgptr(ii)),
     &           ccy(lgptr(ii)),vv(lgptr(ii)),xy(lqptr(ii)),
     &           d(1,1),d(1,5),d(1,9),
     &           c(1,1),c(1,2),c(1,3),c(1,4),c(1,5),c(1,6),
     &           jminbnd(ii),jmaxbnd(ii),
     &           kbegin2(ii),kend2(ii))
 130     continue

      ELSE

c     ****************************************************************
c     *                                                              *
c     *       scalar artificial dissipation in eta direction         *
c     *                                                              *
c     ****************************************************************

c     -form spectral radii for dissipation model
         DO i=nblkstart,nblkend
            lq = lqptr(i)
            lg = lgptr(i)
            CALL specty(jmax(i),kmax(i),vv(lg),ccy(lg),
     &           xyj(lg),xy(lq),coef4y(lg),precon(lprecptr(i)),
     &           jlow(i),jup(i),kbegin2(i),kend2(i))
         ENDDO

c     **************************************************************
c     Dissipation terms : meth = 2, 3                        
c     -set up coefficients and variables for meth = 2,3 
c     -form pressure coefficients                     
c     -artificial dissipation coefficients            
c     -artificial dissipation                           
c     **************************************************************

         IF (dis2y.NE.0.d0) THEN
            ixy = 2

c     -calculate the gradient coef in y and store it in array coef2y
            DO i=nblkstart,nblkend
               lq = lqptr(i)
               lg = lgptr(i)
               CALL gradcoef(ixy,jmax(i),kmax(i),press(lg),
     &              xyj(lg),coef2y(lg),work1(lg),
     &              jminbnd(i),jmaxbnd(i),kbegin2(i),kend2(i),
     &              xi1mesh(i),xi2mesh(i),.true.,.true.)
            ENDDO
         ENDIF

c     -compute the second and fourth order dissipation coefficients
         DO i=nblkstart,nblkend
            lq = lqptr(i)
            lg = lgptr(i)
            CALL coef24y(jmax(i),kmax(i),coef2y(lg),coef4y(lg),
     &           d,d(1,2),jlow(i),jup(i), kbegin2(i),kend2(i))
         ENDDO

c     Stan D.R.
c     -in order to use all halo rows, must avoid
c     problems near corners/trailing edges.
c     -so just traverse within block boundaries for
c     streamwise indices i.e remove affect of jkhalo
         DO n=1,4
            DO i=nblkstart,nblkend
               lq = lqptr(i)
               lg = lgptr(i)
               CALL filtery(n,jmax(i),kmax(i),q(lq),xyj(lg),
     &              coef2y(lg),coef4y(lg),work1(lwf1ptr(i)),tmp(lq),
     &              jlow(i),jup(i),kbegin(i),kend(i))
            ENDDO
         ENDDO

         DO i=nblkstart,nblkend
            lq = lqptr(i)
            lg = lgptr(i)
            CALL predis(jmax(i),kmax(i),q(lq),s(lq),
     &           xyj(lg),sndsp(lg),precon(lprecptr(i)),tmp(lq),
     &           jlow(i),jup(i),klow(i),kup(i))
         ENDDO

      ENDIF
      
      RETURN
      END

