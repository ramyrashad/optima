c ##########################
c ##                      ##
c ##  subroutine etaexpl  ##
c ##                      ##
c ##########################
c
      subroutine etaexpl(jdim,kdim,q,s,press,sndsp,turmu,fmu,vort,x,y,  
     >         xy,xyj,ett,ds,uu,vv,ccy,coef2y,coef4y,spect,precon)      
c                                                                       
c  main integration calls                                               
c                                                                       
csg      parameter (maxj=385, maxk=75)
c                                                                       
      include '../include/arcom.inc'
c     parameter (maxj=497, maxk=97)                                     
      parameter (maxjk=maxj*maxk, maxpen=maxjk)                  
c
      dimension q(jdim*kdim*4)                                          
      dimension press(jdim*kdim),sndsp(jdim*kdim)                       
      dimension s(jdim,kdim,4),xy(jdim*kdim*4),xyj(jdim*kdim)           
      dimension ett(jdim*kdim),ds(jdim*kdim)                            
      dimension x(jdim*kdim),y(jdim*kdim),turmu(jdim*kdim)              
      dimension vort(jdim*kdim),fmu(jdim*kdim)
      dimension precon(jdim,kdim,8)                          
c                                                                       
      dimension uu(jdim,kdim),spect(jdim,kdim,3)                 
      dimension vv(jdim*kdim),ccy(jdim*kdim)                            
      dimension coef2y(jdim*kdim),coef4y(jdim*kdim)                     
c                                                                       
      common/worksp/a0(maxj),b0(maxj),c0(maxj),f0(maxj),g0(maxj),       
     >       z0(maxj),qbcj(3,maxj,4),qbck(maxj,3,4),work0(maxj,62)      
c                                                                       
csd      common/keep/ gfun(maxj), kfmax(maxj), kdelt(maxj), gtran0,       
csd     >              cpx1, cpx2  
c                                                                       
      common/work/ a(maxpen,4), b(maxpen,16), c(maxjk,16), 
     >             d(maxjk,16), e(maxpen,4)          
c                                                                       
c  !!!!!! note : this use of d and e  array in equivalence to save      
c                storage is very dangerous.  in cases where d is        
c                used, such as the stepx and stepy routines values      
c                residing in equivalenced arrays may be bad.            
c
      dimension work1(maxjk,10) 
      equivalence (d(1,1),work1)                                        
      dimension worka(maxjk),workb(maxjk,8),                            
     >          workc(maxjk),workd(maxjk,4)                             
      equivalence (d(1,1),worka),(d(1,2),workb),                        
     >            (d(1,10),workc),(d(1,11),workd)                       
c                                                                       
c                                                                       
c
c
c                       explicit fluxes rhs
c                       ===================
c
c
c---- explicit operators in eta ----
      call rhsy (jdim,kdim,q,s,press,xy,xyj,ett,work1)                  
c                                                                       
c---- 3-a-1. calculate eigenvalues for xi and eta ----
      ix = 3                                                  
      iy = 4                                                  
      call eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,ett,vv,ccy)     
c
c                          viscous rhs
c                          ===========
c
c
      if(viscous) then                                                  
c  --    compute vorticity for turbulence model ---- 
         if(turbulnt)then                                   
            call vortdeta(jdim,kdim,q,vort,xy,xyj,work1)       
c  --       compute eddy viscosity --
            if (itmodel.eq.1) then                             
               if (.not.cmesh .or. .not.wake) then                  
                  call blomax(jdim,kdim,                            
     >                       q,press,vort,turmu,fmu,xy,xyj)       
               elseif (cmesh .and. wake) then                           
                  call cwkblomax(jdim,kdim,               
     >                        q,press,vort,turmu,fmu,xy,xyj)       
               endif                                            
c           elseif (itmodel.eq.2) then
            else   
               call mutur(jdim,kdim,q,press,turmu,fmu,x,y,xy,xyj,uu,vv,
     >                           worka,workc,workd)
            endif                                              
         endif                                              
c                                                                       
c
c---- viscous rhs ----
         if(viseta)call visrhsny(jdim,kdim,q,press,s,turmu,fmu,xy,xyj, 
     >                  worka, workb(1,1), workb(1,2), workb(1,3),      
     >                  workb(1,4), workb(1,5), workb(1, 6),            
     >                  workb(1, 7), workc, workd)                      
c
      endif                                                             
c                                                                       
c
c                     explicit artificial dissipation                    
c                     ===============================
c
c
c
c ********************************************************************
c **                       matrix diss model                        **
c ********************************************************************
      if (idmodel.eq.2) then
         ixy=2
         call mspecty(jdim,kdim,vv,ccy,xyj,xy,spect)
         call gradcoef(ixy,jdim,kdim,press,xyj,coef2y,work1)
         call mcoef24y(jdim,kdim,coef2y,coef4y,worka)
         if (iord.eq.4) then
            call expmaty3(jdim,kdim,q,coef2y,coef4y,sndsp,s,spect,
     >                   xyj,press,ccy,vv,xy,x,y,work1,c,c(1,5), 
     >                   b,b(1,2),b(1,3),b(1,4),b(1,5),b(1,6))
         else
            call expmaty(jdim,kdim,q,coef2y,coef4y,sndsp,s,spect,
     >                   xyj,press,ccy,vv,xy,x,y,work1,c,c(1,5), 
     >                   b,b(1,2),b(1,3),b(1,4),b(1,5),b(1,6))
         endif
      elseif(idmodel.eq.3) then     
c ********************************************************************
c **             constant coefficient scalar diss model             **
c ********************************************************************
c----    form spectral radii
         call specty (jdim,kdim,vv,ccy,xyj,xy,coef4y,precon)
         call smoothy(jdim,kdim,q,s,xyj,coef4y,work1)          
c
      else
c     
c ********************************************************************
c **                       scalar diss model                        **
c ********************************************************************
         ixy = 2                                                 
c----    form spectral radii for scalar dissipation model ----
         call specty(jdim,kdim,vv,ccy,xyj,xy,coef4y,precon)               
         call gradcoef(ixy,jdim,kdim,s,press,xyj,coef2y,work1)     
         call coef24y(jdim,kdim,coef2y,coef4y,worka,workc)
         call filtery(jdim,kdim,q,s,xyj,coef2y,coef4y,sndsp,
     >                                                work1,precon,a)           
      endif                                                         
c
c
      return                                                          
      end                                                             
