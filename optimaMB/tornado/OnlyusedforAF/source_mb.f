c   (BLOCK III) k-e equations
c
      do j=jlow,jup
         do k=klow,kup
            p_k_o=q(j,k,1)*(turnu(j,k)*p1(j,k)-2./3.*vk(j,k)*p2(j,k))
            d_k=q(j,k,1)*betas(j,k)*ve(j,k)*vk(j,k)
c***** added pre-transitionnal laminar region
            p_k=min(p_k_o,20.*d_k)
            if (trip_present(iblk)) then
            if (multi_trans(iblk)) then
               if ((j.ge.jtranlo(j,k)).and.(j.le.jtranup(j,k))) 
     $              p_k=p_k*.1
               if ((j.ge.jtranlo(j,k)+1).and.(j.le.jtranup(j,k)-1)) 
     $              p_k=p_k*.1
               if ((j.ge.jtranlo(j,k)+2).and.(j.le.jtranup(j,k)-2)) 
     $              p_k=0.0
            else
               if ((jtranlo(j,k).gt.0).and.(j.ge.jtranlo(j,k))) 
     $              p_k=p_k*.1 
               if ((jtranlo(j,k).gt.0).and.(j.ge.jtranlo(j,k)+1))
     $              p_k=p_k*.1
               if ((jtranlo(j,k).gt.0).and.(j.ge.jtranlo(j,k)+2))
     $              p_k=0.0
               if ((jtranup(j,k).gt.0).and.(j.le.jtranup(j,k)))
     $              p_k=p_k*.1
               if ((jtranup(j,k).gt.0).and.(j.le.jtranup(j,k)-1))
     $              p_k=p_k*.1
               if ((jtranup(j,k).gt.0).and.(j.le.jtranup(j,k)-2))
     $              p_k=0.0
            endif
            endif
            p_e=p_k_o*gam(j,k)/(turnu(j,k)+vk_inf/ve_inf*1.e-30)
c
            d_e=q(j,k,1)*beta(j,k)*ve(j,k)**2
c
            g11=xy(j,k,1)**2+xy(j,k,2)**2
            g22=xy(j,k,3)**2+xy(j,k,4)**2
            g12=xy(j,k,1)*xy(j,k,3)+xy(j,k,2)*xy(j,k,4)
            dk_dxi=0.5*(vk(j+1,k)-vk(j-1,k))
            dk_deta=0.5*(vk(j,k+1)-vk(j,k-1))
            de_dxi=0.5*(ve(j+1,k)-ve(j-1,k))
            de_deta=0.5*(ve(j,k+1)-ve(j,k-1))
            c_e=2.*q(j,k,1)*cdiff(j,k)/(ve(j,k)+ve_inf*1.e-30)*
     $           (g11*dk_dxi*de_dxi+g22*dk_deta*de_deta
     $           +g12*(dk_dxi*de_deta+dk_deta*de_dxi))
c
            rhs_k(j,k)=rhs_k(j,k)+p_k-d_k
            rhs_e(j,k)=rhs_e(j,k)+p_e-d_e+c_e
            q11(j,k)=-d_k/(vk(j,k)+vk_inf*1.e-30)
            q22(j,k)=-(abs(c_e)+2.*d_e)/(ve(j,k)+ve_inf*1.e-30)
         enddo
      enddo
