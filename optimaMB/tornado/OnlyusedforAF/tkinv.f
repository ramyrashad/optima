c***********************************************************************
c***************** tk inverse multiply on s ****************************
c***********************************************************************
c
c  ix = 1, iy = 2  xi  metrics
c  ix = 3, iy = 4  eta metrics
c
      subroutine tkinv(ix,iy,jdim,kdim,q,sndsp,s,xyj,xy,precon,
     &                 jlow,jup,klow,kup)
c
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),sndsp(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),precon(jdim,kdim,8)
c
c      bt = 1./sqrt(2.)
      bt = 0.7071067811865475 
c
c     t inverse matrix multiply for diagonal algorithm
c      k
c
      if (prec.eq.0) then
        do 10 k = klow, kup
        do 10 j = jlow,jup
          rx        = xy(j,k,ix)
          ry        = xy(j,k,iy)
          rrxy      = 1./sqrt(rx**2+ry**2)
          rx        = rx*rrxy
          ry        = ry*rrxy
          rho       = q(j,k,1)*xyj(j,k)
          rinv    = 1./rho
          rinvr   = rinv*xyj(j,k)
          u = q(j,k,2)*rinvr
          v = q(j,k,3)*rinvr
          snr   = 1./sndsp(j,k)
          bet0  = bt*rinv*snr
c     
          term1 = gami*(-0.5*(u**2+v**2)*s(j,k,1)
     &               + u*s(j,k,2) + v*s(j,k,3) - s(j,k,4))
c     
          s1    =  s(j,k,1) + term1*snr**2
          s2    = ( (-ry*u + rx*v)*s(j,k,1)
     &               + ry         *s(j,k,2) 
     &               - rx         *s(j,k,3) )*rinv
c     
          term1 =  term1*bet0
          term2 =  ( ( rx*u + ry*v)*s(j,k,1)
     &                -rx          *s(j,k,2) 
     &                -ry          *s(j,k,3) )*bt*rinv
c     
          s(j,k,1) = s1
          s(j,k,2) = s2
          s(j,k,3) = -(term1 + term2)
          s(j,k,4) = -(term1 - term2)
c     
 10     continue
      else
cdu   use the preconditioned tkinv
cdu
        do 1000 k=klow,kup
        do 1000 j=jlow,jup
          dkx=xy(j,k,ix)
          dky=xy(j,k,iy)
c     
          u=q(j,k,2)/q(j,k,1)
          v=q(j,k,3)/q(j,k,1)
          uk=dkx*u+dky*v
c     
          dkb=sqrt(dkx**2+dky**2)
          dkx=dkx/dkb
          dky=dky/dkb
c     
          du1=dkb*sndsp(j,k)
          du2=(uk-precon(j,k,ix+4)+precon(j,k,iy+4))
          du3=(uk-precon(j,k,ix+4)-precon(j,k,iy+4))
c     
          t1=s(j,k,4)
          t2=dky*s(j,k,2)-dkx*s(j,k,3)
          t3=(du1*s(j,k,1)+du2*(dkx*s(j,k,2)+
     +          dky*s(j,k,3)))/precon(j,k,iy+4)/2.0
          t4=(du1*s(j,k,1)+du3*(dkx*s(j,k,2)+
     +          dky*s(j,k,3)))/precon(j,k,iy+4)/2.0
c     
          s(j,k,1)=t1
          s(j,k,2)=t2
          s(j,k,3)=t3
          s(j,k,4)=t4
 1000   continue
      endif
c     
      return
      end
