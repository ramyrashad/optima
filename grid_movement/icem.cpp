#ifndef ICEM_H
#include "icem.h"
#endif

void read_icem_grid(const char* filename, vector <Node>& nodes, 
		    vector <Face>& elems, vector <Node*>& bnd_nodes)
{
  int i, j, ier, file_no;
  const bool ThreeD = false;

  file_no = df_open(filename, MODE_READ, UNSTRUCTURED_DOMAIN);

  ier = 0;
  ier = df_n_nodes(file_no, &j);
  if (ier < 0)
    cerr <<"Error reading nnodes " << df_get_error() << endl;
  nodes.resize(j);

  cout << "tot num of nodes " << j << endl;
  
  double* tmp = new double[3*j];

  i = 0;    // start reading from node #0
  ier = 0;
  ier = df_unstruct_read_nodes(file_no, i, j, tmp);
  if (ier < 0)
    cerr <<"Error reading node data " << df_get_error() << endl;

  for (i=0; i<j; i++)
    nodes[i] = Node(&tmp[i*3], i, ThreeD);

  delete [] tmp;         tmp = NULL;
  
  df_n_elements (file_no, &j);
  elems.resize(j);

  /// working with subdomains
  int ndomain, domain_type, pid, wall, farfield;
  const char* subdom_name;
  ier = 0;
  ier = df_n_domain(file_no, &ndomain);
  if (ier < 0)
    cerr <<"Error reading ndomain " << df_get_error() << endl;

  for (i=0; i<ndomain; i++)
    {
      ier = 0;
      ier = df_domain_name(file_no, i, &subdom_name);
      if (ier < 0)
	cerr <<"Error reading domain name " << df_get_error() << endl;

      ier = 0;
      ier = df_domain_type(file_no, i, &domain_type);
      if (ier < 0)
	cerr <<"Error reading domain type " << df_get_error() << endl;

      //cout << "domain_type " << domain_type << endl;
      if (domain_type == BY_PID)
	{
	  ier = 0;
	  ier = df_get_pid_of_domain(file_no, i, &pid);
	  if (ier < 0)
	    cerr <<"Error reading pid_of_domain " << df_get_error() << endl;
	  
	  if (subdom_name[0] == 'W')
	    wall = pid;
	  else if (subdom_name[0] == 'F')
	    farfield = pid;
	}
    }
  subdom_name = NULL;

  // ndomain is used to store nsections 
  ier = 0;
  ier = df_n_sections(file_no, &ndomain);  // get num. of sections
  if (ier < 0)
    cerr <<"Error reading nsections " << df_get_error() << endl;

  // stores the start and end element number in a section
  int start; int end;    
  int* data;    // data array

  domain_type = 0;  // holds counter of number of elements in mesh

  Node** corners = new Node*[4];	  

  for (i=0; i<ndomain; i++)
    {
      ier = 0;
      ier = df_section_info(file_no, i, &start, &end, &pid);
      if (ier < 0)
	cerr <<"Error reading domain name " << df_get_error() << endl;

      if (pid == QUAD_4)
	{
	  j = end-start + 1;     // total number of quadrilateral elements
	  cout << "numberof quads "  << j <<endl;

	  data = new int[j*5];
	  pid = df_read_elements(file_no, start, j, data);
	  
	  for (ier=0; ier<j; ier++)
	    {
	      end = ier*5 + 1;
	      
	      corners[0] = &nodes[data[end]];
	      corners[1] = &nodes[data[end+1]];
	      corners[2] = &nodes[data[end+2]];
	      corners[3] = &nodes[data[end+3]];
	      
	      elems[domain_type] = Face(corners, domain_type);
	      elems[domain_type].add_nei(corners);
	      domain_type++;
	    }
	  delete [] data;   
	}		    
      else if (pid == TRI_3)    // triangular elements with 3 corner nodes 
	{ 
	  j = end-start + 1;     // total number of triangular elements
	  cout << "numberof triangles "  << j <<endl;

	  data = new int[j*4];
	  pid = df_read_elements(file_no, start, j, data);

	      start = 3;     // number of corner nodes in element
	      for (ier=0; ier<j; ier++)
		{
		  end = ier*4 + 1; 
		  
		  corners[0] = &nodes[data[end]];
		  corners[1] = &nodes[data[end+1]];
		  corners[2] = &nodes[data[end+2]];
		  
		  elems[domain_type] = Face(corners, domain_type, start);
		  elems[domain_type].add_nei(corners);
		  domain_type++;
		}
	  delete [] data;        
	}
      else if (pid == BAR_2)  
	{ 
	  // line elements on boundaries with 2 end nodes 
	  end = end-start +1;
	  cout << "number of line elements " << end << endl;

	  bnd_nodes.resize(j);

	  j = end*3;
	  data = new int[j];
	  pid = df_read_elements(file_no, start, end, data);

	  end = 0;  // counter for number of moving_boundary nodes
	  for (ier=0; ier<j; ier+=3)
	    {
	      
	      if (data[ier] == wall)
		{
		  if (nodes[data[ier+1]].if_surf() == false)
		    {
		      nodes[data[ier+1]].is_surf();
		      bnd_nodes[end] = &nodes[data[ier+1]];
		      end ++;
		    }
		  if (nodes[data[ier+2]].if_surf() == false)
		    {
		      nodes[data[ier+2]].is_surf();
		      bnd_nodes[end] = &nodes[data[ier+2]];
		      end ++;
		      }
		}
	      else if (data[ier] == farfield)
		{
		  nodes[data[ier+1]].is_surf();
		  nodes[data[ier+2]].is_surf();
		}
	    }
	  bnd_nodes.resize(end);
	  delete [] data;    
	  
	}
    }
  data = NULL;
  delete [] corners;           corners = NULL;
  elems.resize(domain_type);

  /*
   j = 0;  // number of pre_bnd_nodes
  end = nodes.size();
  // set number of pre_bnd_nodes
  for (i=0; i<end; i++)
    {
      ier = i-j;
      if (nodes[i].if_surf() == true)
	j++;
      nodes[i].set_ind(ier);
    }
  */  
  ier = df_close(file_no);   
 
   return;  
}


void read_icem_grid(const char* filename, vector <Node>& nodes, 
		    vector <Vol>& elems, vector <Node*>& bnd_nodes)
{
  int i, j, ier, file_no;
  const bool ThreeD = true;

  file_no = df_open(filename, MODE_READ, UNSTRUCTURED_DOMAIN);

  ier = 0;
  ier = df_n_nodes(file_no, &j);
  if (ier < 0)
    cerr <<"Error reading nnodes " << df_get_error() << endl;
  nodes.resize(j);

  cout << "tot num of nodes " << j << endl;
  
  double* tmp = new double[3*j];

  i = 0;    // start reading from node #0
  ier = 0;
  ier = df_unstruct_read_nodes(file_no, i, j, tmp);
  if (ier < 0)
    cerr <<"Error reading node data " << df_get_error() << endl;

  for (i=0; i<j; i++)
    nodes[i] = Node(&tmp[i*3], i, ThreeD);

  delete [] tmp;         tmp = NULL;
  
  df_n_elements (file_no, &j);
  elems.resize(j);

  /// working with subdomains
  int ndomain, domain_type, pid, farfield, symmetry;
  const char* subdom_name;
  ier = 0;
  ier = df_n_domain(file_no, &ndomain);
  if (ier < 0)
    cerr <<"Error reading ndomain " << df_get_error() << endl;
  for (i=0; i<ndomain; i++)
    {
      ier = 0;
      ier = df_domain_name(file_no, i, &subdom_name);
      if (ier < 0)
	cerr <<"Error reading domain name " << df_get_error() << endl;

      ier = 0;
      ier = df_domain_type(file_no, i, &domain_type);
      if (ier < 0)
	cerr <<"Error reading domain type " << df_get_error() << endl;

      //cout << "domain_type " << domain_type << endl;
      if (domain_type == BY_PID)
	{
	  ier = 0;
	  ier = df_get_pid_of_domain(file_no, i, &pid);
	  if (ier < 0)
	    cerr <<"Error reading pid_of_domain " << df_get_error() << endl;
	  
	  if (subdom_name[0] == 'S')
	    symmetry = pid;
	  else if (subdom_name[0] == 'F')
	    farfield = pid;
	}
    }
  subdom_name = NULL;

  // ndomain is used to store nsections 
  ier = 0;
  ier = df_n_sections(file_no, &ndomain);  // get num. of sections
  if (ier < 0)
    cerr <<"Error reading nsections " << df_get_error() << endl;

  // stores the start and end element number in a section
  int start; int end;    
  int* data;    // data array

  domain_type = 0;  // holds counter of number of elements in mesh

  Node** corners = new Node*[4];	  

  for (i=0; i<ndomain; i++)
    {
      ier = 0;
      ier = df_section_info(file_no, i, &start, &end, &pid);
      if (ier < 0)
	cerr <<"Error reading domain name " << df_get_error() << endl;

      if (pid == TETRA_4)  // 4 noded tets
	{
	  j = end-start + 1;     // total number of triangular elements
	  cout << "number of tets "  << end << " tetra_4 is " << TETRA_4<<endl;

	  data = new int[end*5];
	  pid = df_read_elements(file_no, start, j, data);

	  start = 4;     // number of corner nodes in element
	  for (ier=0; ier<j; ier++)
	    {
	      end = ier*5 + 1; 

	      corners[0] = &nodes[data[end]];
	      corners[1] = &nodes[data[end+1]];
	      corners[2] = &nodes[data[end+2]];
	      corners[3] = &nodes[data[end+3]];
	      
	      elems[domain_type] = Vol(corners, domain_type, start);
	      // adding nodes in the same volume as neighbours
	      elems[domain_type].add_nei(corners);  
	      domain_type++;
	    }

	  delete [] data;        
	}
      if (pid == TRI_3)  // 3 noded triangles on surface
	{
	  end = end-start + 1;     // total number of triangular elements
	  cout << "number of tris "  << end <<endl;

	  bnd_nodes.resize(end);

	  j = end*4;
	  data = new int[j];
	  pid = df_read_elements(file_no, start, end, data);

	  end = 0;  // counter for number of moving_boundary nodes
	  for (ier=0; ier<j; ier+=4)
	    {
	      if (data[ier] != symmetry || data[ier] != farfield)
		{
		  if (nodes[data[ier+1]].if_surf() == false)
		    {
		      bnd_nodes[end] = &nodes[data[ier+1]];
		      end ++;
		    }
		  if (nodes[data[ier+2]].if_surf() == false)
		    {
		      bnd_nodes[end] = &nodes[data[ier+2]];
		      end ++;
		      }
		  if (nodes[data[ier+3]].if_surf() == false)
		    {
		      bnd_nodes[end] = &nodes[data[ier+3]];
		      end ++;
		    }
		  nodes[data[ier+1]].is_surf();
		  nodes[data[ier+2]].is_surf();
		  nodes[data[ier+3]].is_surf();
		}
	    }
	  bnd_nodes.resize(end);

	  delete [] data;        
	}
    }
  elems.resize(domain_type);
  data = NULL;
  delete [] corners;   corners = NULL;


  return;
 
}

void write_icem_grid(char* filename, const vector <Node>& nodes)
{
  int i, j;

  //  i = MODE_MODIFY;  
  //j = UNSTRUCTURED_DOMAIN;
  int file_no = df_open(filename, MODE_MODIFY, UNSTRUCTURED_DOMAIN);
  double* coord;

  cerr << "Error opening file in modify mode: " << df_get_error() << endl;


  j = nodes.size();
  double *dd = new double[j*3];
  // write node to icemcfd unstructured file
  for (i=0; i<j; i++)
    {
      coord = nodes[i].get_coord();
      dd[i*3] = coord[0]; 
      dd[i*3+1] = coord[1];
      dd[i*3+2] = 0.0;
    }

  i = 0;
  i = df_unstruct_update_nodes(file_no, i, j, dd);
  if (i < 0)
    cerr <<"Error writing nodes: " << df_get_error() << endl;

  delete [] dd;   dd = NULL;
  coord = NULL;

  i = 0;
  i = df_close(file_no);
  if (i < 0)
    cerr <<"Error reading domain name " << df_get_error() << endl;

  //  cout << df_get_error() << endl;
  cout << endl << " error closing icem " << file_no << " after writing?? " << i << endl;
  return;
}


