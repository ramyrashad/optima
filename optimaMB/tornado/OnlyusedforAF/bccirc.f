C ********************************************************************
C ********************* BOUNDARY CONDITIONS **************************
C ********************************************************************
      subroutine bccirc
C
#include "../include/common.inc"
C
C --------------------------------------------------------------------
C --------------------------------------------------------------------
C --         BOUNDARY CONDITIONS FOR AIRFOIL TOPOLOGY               --
C --                  C  OR  O MESH TYPE                            --
C --------------------------------------------------------------------
C --------------------------------------------------------------------
c
C  Compute variables for circulation correction for far field logic.
c
C.....................................................................
C  Far field circulation based on potential vortex added to reduce
C  the dependency on outer boundary location.  Works well.  For
c  instance, a NACA0012 at M=0.63 a=2. shows no dependency on ob from
c  4 - 96 chords.
C.....................................................................
c
      circb = 0.
      beta = sqrt(1.-fsmach**2)
      chord = 1.
      if(circul)then
        cl = clt
        circb = 0.25*chord*cl*beta*fsmach/pi
      endif
c
c     Option to iterate on alpha for a specified lift
c
c     Start cl modification only after iclstrt iterations
c     1. Only perform logic if clalpha = true
c     2. Calculate new circulation at far field boundary based on
c        clinput
c     3. only update delcl and alpha every iclfreq steps and check
c        abs(delcl) against cltol, if abs(delcl) lt cltol
c        ``do not'' update alpha
c
c
      if(numiter .gt. iclstrt .and. clalpha)then
        circb = 0.25*chord*clinput*beta*fsmach/pi
        if(mod( numiter - istart + 1, iclfreq) .eq. 0  )then
          delcl = cl - clinput
          if(abs(delcl).ge.cltol)then
            delalp = -relaxcl*delcl
            alpha = alpha + delalp
            cosang = cos(pi*alpha/180.)
            sinang = sin(pi*alpha/180.)
            uinf   = fsmach*cosang
            vinf   = fsmach*sinang
      print *,'------------------------------------------------------'
      print *,'|                                                    |'
      print *,'| numiter = ',numiter,'  new alpha = ',alpha
      print *,'|                                                    |'
      print *,'------------------------------------------------------'
            isw = 1
          else
            if(isw.eq.1)then
              isw = 0
      print *,'------------------------------------------------------'
      print *,'|                                                    |'
      print *,'| abs(delcl) = ',abs(delcl),
     *                    ' falls below cl tolerance = ',cltol
      print *,'|                                                    |'
      print *,'------------------------------------------------------'
            endif
          endif
        endif
      endif
c
      return
      end
