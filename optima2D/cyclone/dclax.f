c     CUSP Dissipation
c     -- calculation of limited average --
c     Written by: Marian Nemec
c     Date: April 1998
c     Mods: March 2000

      subroutine dclax (jdim,kdim,q,xyj,x,y,tmp,wq,p,swr,swr2)

#include "../include/arcom.inc"

      integer j,k,n

      dimension q(jdim,kdim,4),xyj(jdim,kdim),x(jdim,kdim)
      dimension y(jdim,kdim),tmp(jdim,kdim,4),swr2(jdim,kdim,4)
      dimension wq(2,jdim,kdim,4),p(jdim,kdim),swr(jdim,kdim)

      double precision sw,qjp1,qj

      if ( limiter.gt.3 ) then
c     -- using full limiting --
        do n = 1,4
          call dclimx (jdim,kdim,q,xyj,x,y,tmp,p,(n),swr)
          do k = klow,kup
            do j = jbegin,jup
              swr2(j,k,n) = swr(j,k)
            end do
          end do
        end do

        do n = 1,4  
          do k = klow,kup
            do j = jlow,jup-1
              qjp1 = q(j+1,k,n)*xyj(j+1,k)
              qj = q(j,k,n)*xyj(j,k)
              sw = swr2(j,k,n)
              sw = 2.5d-1*sw*( tmp(j+1,k,n) + tmp(j-1,k,n) )
              wq(1,j,k,n) = qjp1 - sw
              wq(2,j,k,n) = qj + sw
            end do
          end do
        end do
c     -- boundary nodes --

        j = jbegin
        do n = 1,4
          do k = klow,kup
            qjp1 = q(jbegin+1,k,n)*xyj(jbegin+1,k)
            qj = q(jbegin,k,n)*xyj(jbegin,k)
            sw = swr2(j,k,n)
            sw = 2.5d-1*sw*( tmp(j+1,k,n) + tmp(j,k,n) )
            wq(1,j,k,n) = qjp1 - sw
            wq(2,j,k,n) = qj + sw
          end do
        end do 

        j = jup
        do n = 1,4
          do k = klow,kup
            qjp1 = q(jend,k,n)*xyj(jend,k)
            qj = q(jup,k,n)*xyj(jup,k)
            sw = swr2(j,k,n)
            sw = 2.5d-1*sw*( tmp(j,k,n) + tmp(j-1,k,n) )
            wq(1,j,k,n) = qjp1 - sw
            wq(2,j,k,n) = qj + sw
          end do
        end do
      else
c     -- Nemec-Zingg limiter or O(1) or O(3) dissipation --
        n = 1
        call dclimx (jdim,kdim,q,xyj,x,y,tmp,p,(n),swr)

        do n = 1,4
          do k = klow,kup
            do j = jlow,jup-1
              qjp1 = q(j+1,k,n)*xyj(j+1,k)
              qj = q(j,k,n)*xyj(j,k)
              sw = swr(j,k)
              sw = 2.5d-1*sw*( tmp(j+1,k,n) + tmp(j-1,k,n)  ) 
              wq(1,j,k,n) = qjp1 - sw
              wq(2,j,k,n) = qj + sw
            end do
          end do
        end do
c     -- boundary nodes --

        j = jbegin
        do n = 1,4
          do k = klow,kup
            qjp1 = q(jbegin+1,k,n)*xyj(jbegin+1,k)
            qj = q(jbegin,k,n)*xyj(jbegin,k)
            sw = swr(j,k)
            sw = 2.5d-1*sw*( tmp(j+1,k,n) + tmp(j,k,n) )
            wq(1,j,k,n) = qjp1 - sw
            wq(2,j,k,n) = qj + sw
          end do
        end do 

        j = jup
        do n = 1,4
          do k = klow,kup
            qjp1 = q(jend,k,n)*xyj(jend,k)
            qj = q(jup,k,n)*xyj(jup,k)
            sw = swr(j,k)
            sw = 2.5d-1*sw*( tmp(j,k,n) + tmp(j-1,k,n) )
            wq(1,j,k,n) = qjp1 - sw
            wq(2,j,k,n) = qj + sw
          end do
        end do
      end if

      return 
      end                       !dclax
