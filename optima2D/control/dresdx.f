c----------------------------------------------------------------------
c     -- calculate the sensitivity of the rhs residual dR/dX
c     where Xs are the design variables -- 
c
c     written by: marian nemec
c     date: april 2000
c----------------------------------------------------------------------
      subroutine dResdX (in, jdim, kdim, ndim, q, xy, xyj, x, y, bap,
     &      bcp, bt, bknot, dvs, idv, dRdX, Rm, Rp, precon, coef2,
     &      coef4, uu, vv, ccx, ccy, ds, press, tmet, spect, gam, turmu,
     &      fmu, vort, sndsp, work, xys, xyjs, xs, ys, turres,
     |      xOrig, yOrig, dx, dy, given_stepsize, dRdXna)

#ifdef _MPI_VERSION
      use mpi
#endif
      
      use disscon_vars
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/sam.inc"
#include "../include/mpi_info.inc"

      integer in, j, k, jdim, kdim, idv(nc+mpopt)

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4),xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim), tmp2
      double precision xOrig(jdim,kdim), yOrig(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision dvs(nc+mpopt), bap(jbody,2), H
      double precision bcp(nc,2), bt(jbody), bknot(jbsord+nc)

      double precision Rm(jdim,kdim,ndim), Rp(jdim,kdim,ndim)
      double precision Rm0(jdim,kdim,ndim), Rp0(jdim,kdim,ndim)
      double precision dRdX(jdim,kdim,ndim), dRdXna
      double precision rhsnap, rhsnam
      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision turmu(jdim,kdim), fmu(jdim,kdim) 
      double precision vort(jdim,kdim), spect(jdim,kdim,3)
      double precision uu(jdim,kdim), vv(jdim,kdim) 
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)
      double precision work(jdim*kdim,4)

      double precision xys(jdim,kdim,4), xyjs(jdim,kdim), xs(jdim,kdim)
      double precision ys(jdim,kdim), turres(jdim,kdim) 

c     -- frozen jacobian arrays --
      double precision c2xf(maxj,maxk), c4xf(maxj,maxk)
      double precision c2yf(maxj,maxk), c4yf(maxj,maxk)
      double precision fmuf(maxj,maxk), turmuf(maxj,maxk)
      double precision vortf(maxj,maxk), sapdf(maxj,maxk)
      double precision sadx(maxj,maxk), sady(maxj,maxk)
      double precision suuf(maxj,maxk), svvf(maxj,maxk)
      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &      sapdf, sadx, sady, suuf, svvf 

c     Some value to set the stepsize to (Chad Oldfield)
      double precision given_stepsize

c     -- local variables --
      double precision tmp, stepsize, dt_tmp

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c  in - the design variable being varied to calculate derivative.
c  idv - index for which control point is which design variable.
c  dvs - vector of design variables.
c  Rm - residual when dvs(in) = dvs(in) - stepsize.
c  Rp - residual when dvs(in) = dvs(in) + stepsize.
c  dRdX(j,k,n) - derivative of residual (j,k,n) wrt design variable in.
c  dRdXna - derivative of Cl residual equation for variable angle of 
c    attack problems wrt design variable in.
c  rhsnap - Cl residual equation when dvs(in) = dvs(in) + stepsize.
c  rhsnam - Cl residual equation when dvs(in) = dvs(in) - stepsize.
c

      !-- For calculation of dR/dX, dissipation-based continuation 
      !-- modifications to the residual must be avoided
      dbc_tmp = dissCon
      dissCon = .false.

      dt_tmp = dt
      dt = 1.d0

c     -- using cenral-differences to evaluate rhs sensitivity --
c     -- second order accurate --
      tmp = dvs(in)
      tmp2 = clinput
      stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
      if ( abs(stepsize).lt.1.e-10 ) stepsize = 1.e-10*sign(1.0,
     &     stepsize)

c     Fix the stepsize to be what was given (Chad Oldfield)
      if (given_stepsize > 0.d0) stepsize = given_stepsize

c     -- calculate rhs at (+) --
      dvs(in) = dvs(in) + stepsize
      stepsize = dvs(in) - tmp

      if ( in.le.ngdv ) then

c     -- regrid domain --
        call regrid ( (in), jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
        call xidif ( jdim, kdim, x, y, xy)
        call etadif ( jdim, kdim, x, y, xy)
        call calcmet ( jdim, kdim, xy, xyj)
      else if ( in.eq.ngdv+1 ) then ! either AOA or Mach # is a DV
        if (.not.dvmach(rank+1)) then
           alpha = dvs(in) ! dvs(ngdv+1) is an AOA DV
           cosang = cos(pi*alpha/180.d0)                               
           sinang = sin(pi*alpha/180.d0)
           uinf   = fsmach*cosang
           vinf   = fsmach*sinang
        else if (dvmach(rank+1)) then 
           fsmach = dvs(in) ! dvs(ngdv+1) is a Mach # DV

           !-- Perturb target lift coefficient
           H = fsmach/tmp
           clinput = tmp2*H**-2

           cosang = cos(pi*alpha/180.d0)                               
           sinang = sin(pi*alpha/180.d0)
           uinf   = fsmach*cosang
           vinf   = fsmach*sinang
        end if
      else if (in.eq.ngdv+2) then ! Both AOA and Mach # are DV's
         fsmach = dvs(in) ! dvs(ngdv+2) is a Mach # DV

         !-- Perturb target lift coefficient
         H = fsmach/tmp
         clinput = tmp2*H**-2

         cosang = cos(pi*alpha/180.d0)                               
         sinang = sin(pi*alpha/180.d0)
         uinf   = fsmach*cosang
         vinf   = fsmach*sinang
      end if

c     -- recalculate rhs of cyclone --
      do n=1,ndim
        do k=1,kend
          do j=1,jend
            Rp(j,k,n) = 0.d0
            q(j,k,n) = q(j,k,n)/xyj(j,k)
          end do
        end do
      end do

      call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- compute laminar viscosity based on sutherland's law --
c     -- note laminar viscosity is independent of the grid perturbation
c     but it is recalculated here when sensit-mf gradients are used --
      if (viscous .and. gradient.eq.3) call fmun(jdim, kdim, q, press,
     &     fmu, sndsp)

      call xiexpl ( jdim, kdim, q, Rp, Rp0, press, sndsp, turmu, fmu,
     &      vort, x, y, xy, xyj, tmet, ds, uu, ccx, coef2, coef4,
     &      spect, precon, gam)

c     -- logic to avoid calling spalart in etaexpl --        
      iflag = 0
      if (turbulnt .and. itmodel.eq.2) then
        turbulnt = .false.
        iflag = 1
        do k = kbegin,kup
          kp1 = k+1
          do j = jbegin,jend
            rn = q(j,k,5)*q(j,k,1)*xyj(j,k)**2
            chi = rn/fmu(j,k)
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            tur1 = fv1*rn
            
            rn = q(j,kp1,5)*q(j,kp1,1)*xyj(j,kp1)**2
            chi = rn/fmu(j,kp1)
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            tur2 = fv1*rn
            
            turmu(j,k) = 0.5d0*(tur1+tur2)

c            if (frozen) then
c              turmu(j,k) = turmuf(j,k)
c            end if
          end do
        end do
      endif

      call etaexpl ( jdim, kdim, q, Rp, Rp0, press, sndsp, turmu, fmu,
     &      vort, x, y, xy, xyj, tmet, ds, uu, vv, ccy, coef2, coef4,
     &      spect, precon, gam)

      if ( iflag.eq.1 ) turbulnt = .true.

      if (circul) then
        clt_tmp = clt
        call clcd (jdim, kdim, q, press, x, y, xy, xyj, 0, cli, cdi,
     &        cmi, clv, cdv, cmv) 
        clt = cli + clv
        call bccirc 
      end if

      if (viscous) then
        call vbcrhs (jdim, kdim, x, y, xy, xyj, q, Rp, Rp0, press)
        if (turbulnt .and. itmodel.eq.2) then

c     -- compute generalized distance function --
          do k = kbegin,kend
            do j = jbegin,jend
              j_point=j
              if (j.lt.jtail1) j_point = jtail1 !co
              if (j.gt.jtail2) j_point = jtail2 !co
              xpt1 = x(j_point,kbegin)
              ypt1 = y(j_point,kbegin)
              smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
            end do
          end do

          call res_sa (jdim, kdim, q, Rp, Rp0, x, y, xy, xyj, uu, vv, 
     &         fmu, vort, work(1,1), work(1,4)) 
        end if
      else
        call bcrhs (jdim, kdim, x, y, xy, xyj, q, Rp, Rp0, press)
      end if

      if (circul) then
        clt = clt_tmp
        call bccirc
      end if

      if (clopt) then
         call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 0,
     &        cli, cdi, cmi, clv, cdv, cmv) 
         cltp = cli + clv

         rhsnap=-(cltp-clinput)
      end if

c     -- !! note: wake cut is not included since no grid perturbations
c     occur there !! --

      do n=1,ndim
        do k=1,kend
          do j=1,jend
            q(j,k,n) = q(j,k,n)*xyj(j,k)
          end do
        end do
      end do

c     -- restore grid and turre values --
c     -- it is important to restore turre since when a design variable
c     is changed the S-A model has to iterate to get turre and
c     consistent results are obtained when the restart is from the same
c     values --
      if ( in.le.ngdv ) then
        do k=kbegin,kend
          do j=jbegin,jend
            x(j,k) = xs(j,k)
            y(j,k) = ys(j,k)
          end do
        end do
      end if

c     -- calculate rhs at (-) --
      dvs(in) = tmp - stepsize

      if ( in.le.ngdv ) then
c     -- regrid domain --
        call regrid ( (in), jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
        call xidif ( jdim, kdim, x, y, xy)
        call etadif ( jdim, kdim, x, y, xy)
        call calcmet ( jdim, kdim, xy, xyj)
      else if ( in.eq.ngdv+1 ) then ! either AOA or Mach # is a DV
        if (.not.dvmach(rank+1)) then
           alpha = dvs(in) ! dvs(ngdv+1) is an AOA DV
           cosang = cos(pi*alpha/180.d0)                               
           sinang = sin(pi*alpha/180.d0)
           uinf   = fsmach*cosang
           vinf   = fsmach*sinang
        else if (dvmach(rank+1)) then 
           fsmach = dvs(in) ! dvs(ngdv+1) is a Mach # DV

           !-- Perturb target lift coefficient
           H = fsmach/tmp
           clinput = tmp2*H**-2

           cosang = cos(pi*alpha/180.d0)                               
           sinang = sin(pi*alpha/180.d0)
           uinf   = fsmach*cosang
           vinf   = fsmach*sinang
        end if
      else if (in.eq.ngdv+2) then ! Both AOA and Mach # are DV's
         fsmach = dvs(in) ! dvs(ngdv+2) is a Mach # DV

         !-- Perturb target lift coefficient
         H = fsmach/tmp
         clinput = tmp2*H**-2

         cosang = cos(pi*alpha/180.d0)                               
         sinang = sin(pi*alpha/180.d0)
         uinf   = fsmach*cosang
         vinf   = fsmach*sinang
      end if

c     -- recalculate rhs of cyclone --
      do n=1,ndim
        do k=1,kend
          do j=1,jend
            Rm(j,k,n) = 0.d0
            q(j,k,n) = q(j,k,n)/xyj(j,k)
          end do
        end do
      end do

      call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- compute laminar viscosity based on sutherland's law --
c     -- note laminar viscosity is independent of the grid perturbation
c     but it is recalculated here when sensit-mf gradients are used --
      if (viscous .and. gradient.eq.3) call fmun(jdim, kdim, q, press,
     &     fmu, sndsp)

      call xiexpl ( jdim, kdim, q, Rm, Rm0, press, sndsp, turmu, fmu,
     &      vort, x, y, xy, xyj, tmet, ds, uu, ccx, coef2, coef4,
     &      spect, precon, gam)

c     -- logic to avoid calling spalart in etaexpl --        
      iflag = 0
      if (turbulnt .and. itmodel.eq.2) then
        turbulnt = .false.
        iflag = 1
        do k = kbegin,kup
          kp1 = k+1
          do j = jbegin,jend
            rn = q(j,k,5)*q(j,k,1)*xyj(j,k)**2
            chi = rn/fmu(j,k)
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            tur1 = fv1*rn
            
            rn = q(j,kp1,5)*q(j,kp1,1)*xyj(j,kp1)**2
            chi = rn/fmu(j,kp1)
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            tur2 = fv1*rn
            
            turmu(j,k) = 0.5d0*(tur1+tur2)
c            if (frozen) then
c              turmu(j,k) = turmuf(j,k)
c            end if
          end do
        end do
      endif

      call etaexpl ( jdim, kdim, q, Rm, Rm0, press, sndsp, turmu, fmu,
     &      vort, x, y, xy, xyj, tmet, ds, uu, vv, ccy, coef2, coef4,
     &      spect, precon, gam)

      if ( iflag.eq.1 ) turbulnt = .true.  

      if (circul) then
        clt_tmp = clt
        call clcd (jdim, kdim, q, press, x, y, xy, xyj, 0, cli, cdi,
     &        cmi, clv, cdv, cmv) 
        clt = cli + clv
        call bccirc 
      end if

      if (viscous) then
        call vbcrhs (jdim, kdim, x, y, xy, xyj, q, Rm, Rm0, press)
        if (turbulnt .and. itmodel.eq.2) then

c     -- compute generalized distance function --
          do k = kbegin,kend
            do j = jbegin,jend
              j_point=j
              if (j.lt.jtail1) j_point = jtail1 !co
              if (j.gt.jtail2) j_point = jtail2 !co
              xpt1 = x(j_point,kbegin)
              ypt1 = y(j_point,kbegin)
              smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
            end do
          end do

          call res_sa (jdim, kdim, q, Rm, Rm0, x, y, xy, xyj, uu, vv, 
     &         fmu, vort, work(1,1), work(1,4)) 
        end if
      else
        call bcrhs (jdim, kdim, x, y, xy, xyj, q, Rm, Rm0, press)
      end if

c     -- !! note: wake cut is not included since no grid perturbations
c     occur there !! --

      if (circul) then
        clt = clt_tmp
        call bccirc
      end if

      if (clopt) then
         call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)     
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 0,
     &        cli, cdi, cmi, clv, cdv, cmv)
         cltm = cli + clv
         rhsnam=-(cltm-clinput)
      end if

      do n=1,ndim
        do k=1,kend
          do j=1,jend
            q(j,k,n) = q(j,k,n)*xyj(j,k)
          end do
        end do
      end do

c     -- evaluate sensitivity of the flow residual --
      dxx = 1.d0/(2.d0*stepsize)
      do n=1,ndim
        do k = 1,kend
          do j= 1,jend
            dRdX(j,k,n) = ( Rp(j,k,n) - Rm(j,k,n) )*dxx
          end do
        end do
      end do
      if (clopt) then
         dRdXna = (rhsnap - rhsnam)*dxx
      end if

      dvs(in) = tmp
      clinput = tmp2
c     -- restore original turre and grid parameters --
      if ( in.eq.ngdv+1 ) then
        if (.not.dvmach(rank+1)) then
           alpha = dvs(in)
           cosang = cos(pi*alpha/180.d0)                               
           sinang = sin(pi*alpha/180.d0)
           uinf   = fsmach*cosang
           vinf   = fsmach*sinang
        else if (dvmach(rank+1)) then
           fsmach = dvs(in)
           cosang = cos(pi*alpha/180.d0)                               
           sinang = sin(pi*alpha/180.d0)
           uinf   = fsmach*cosang
           vinf   = fsmach*sinang
        end if
      else if ( (in.eq.ngdv+2).and.(dvmach(rank+1)) ) then
        fsmach = dvs(in)
        cosang = cos(pi*alpha/180.d0)                               
        sinang = sin(pi*alpha/180.d0)
        uinf   = fsmach*cosang
        vinf   = fsmach*sinang
      else
        do k=kbegin,kend
          do j=jbegin,jend
            x(j,k) = xs(j,k)
            y(j,k) = ys(j,k)
            xyj(j,k) = xyjs(j,k)
          end do
        end do
        do n = 1,4
          do k=kbegin,kend
            do j=jbegin,jend
              xy(j,k,n) = xys(j,k,n)
            end do
          end do
        end do
      end if
      
      if ( viscous .and. turbulnt .and. itmodel.eq.2 ) then
c     -- compute generalized distance function --
        do k = kbegin,kend
          do j = jbegin,jend
            j_point=j
            if (j.lt.jtail1) j_point = jtail1 !co
            if (j.gt.jtail2) j_point = jtail2 !co
            xpt1 = x(j_point,kbegin)
            ypt1 = y(j_point,kbegin)
            smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
          end do
        end do
      end if

      dt = dt_tmp

      !-- Restore dissipation-based continuation flag
      dissCon = dbc_tmp

      return
      end                       !drdx
