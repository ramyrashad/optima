C***********************************************************************
C****************** EXPLICIT VISCOUS TERM ******************************
C***********************************************************************
      subroutine visrhsnc(JDIM,KDIM,Q,PRESS,S,TURMU,FMU,XY,XYJ,C0,C1,C2,
     *                    C3, C4, C5, FMUTEMP, TTEMP, BETA, VFLUX,
     &                    jbegin,jend,jlow,jup,
     &                    kbegin,kend,klow,kup)
C                                                                       
#include "../include/common.inc"
C                                                                       
      DIMENSION Q(JDIM,KDIM,4),PRESS(JDIM,KDIM),TURMU(JDIM,KDIM)        
      DIMENSION S(JDIM,KDIM,4),XY(JDIM,KDIM,4),XYJ(JDIM,KDIM)           
      DIMENSION FMU(JDIM,KDIM)                                          
C                                                                       
      DIMENSION C0(JDIM,KDIM),BETA(JDIM,KDIM),VFLUX(JDIM,KDIM,4)        
      DIMENSION C1(JDIM,KDIM),C2(JDIM,KDIM),C3(JDIM,KDIM)               
      DIMENSION C4(JDIM,KDIM),C5(JDIM,KDIM), FMUTEMP(JDIM,KDIM),        
     >          TTEMP(JDIM,KDIM)                                        
C                                                                       
      PARAMETER ( PRLAM = .72 , PRTURB=.90 )                            
      PARAMETER ( PRLINV = 1./PRLAM , PRTINV = 1./PRTURB )              
      PARAMETER ( F43 = 4./3.  , F13 = 1./3., F23 = 2./3. )             
C                                                                       
C         PRLAM   =  LAMINAR PRANDTL NUMBER  = .72                      
C         PRTURB  =  TURBULENT PRANDTL NUMBER = .90                     
C         PRLINV  =  1./(LAMINAR PRANDTL NUMBER)                        
C         PRTINV  =  1./(TURBULENT PRANDTL NUMBER)                      
C         F13     =  1/3                                                
C         F23     =  2/3                                                
C         F43     =  4/3                                                
C         HRE     =  1/2 DT * REYNOLDS NUMBER                           
C         FMU     =  VISCOSITY                                          
C         TURMU   =  TURBULENT VISCOSITY                                
C         AA7      =  SOUND SPEED SQUARED ... ALSO TEMPERATURE          
C         BETA    =  FMU/PRLAM + MUTURB/PRTURB                          
C                                                                       
C      SET UP SOME TEMPORARY LOGICAL SWITCHES VISXI,VISETA,VISXX        
C                                                                       
      HRE   = .5*DT/(RE* (1. + PHIDT) )                                 
      G1    = 1./GAMI                                                   
C                                                                       
cthp10/23/87                                                            
c                                                                       
c  For CMESH logic apply cross terms at k = 1 along wake cut with       
c                                                                       
c                    k+1 = 2, k-1 = 1, k = 1                            
c                                                                       
cthp10/23/87                                                            
         kmm = 1                                                        
         if(cmesh .and. wake)kmm = 0                                    
                                                                        
cthp10/23/87                                                            
C*********************************************************************  
C*******  COMMON VARIABLES FOR XI, ETA AND CROSS TERMS  **************  
C*********************************************************************  
C                                                                       
C  AVERAGED FMU TO GET IT AT 1/2 GRID PNTS  (TURMU IS ALREADY AT 1/2 PTS
C                                                                       
      DO 5  K = KBEGIN, KUP                                             
      DO 5  J = JBEGIN, JEND                                            
        C0(J, K) = .5*(FMU(J, K)+FMU(J, K+1))                           
 5    CONTINUE                                                          
      DO 7 J = JBEGIN, JUP                                              
      JPL = j + 1
      DO 7 K = KBEGIN, KUP                                              
       FMUTEMP(J, K) = .5*(C0(J, K)+C0(JPL, K))                         
 7    CONTINUE                                                          
                                                                        
      DO 10 J = JBEGIN, JUP                                             
         JPL = j + 1
         DO 10 K = KBEGIN, KUP                                          
            TURMD = .5*(TURMU(JPL,K)+ TURMU(J,K))                       
            BETA(J,K) =                                                 
     >       (FMUTEMP(J,K)*PRLINV + TURMD*PRTINV)*HRE*G1                
            C0(J,K)  = (FMUTEMP(J,K) + TURMD)*HRE                       
 10   CONTINUE                                                          
C                                                                       
C*********************************************************************  
C*********************************************************************  
C                                                                       
C          MU, TURMU AT NODE POINTS FOR CROSS TERMS FOR MORE COMPACTNESS
C                                                                       
        DO 305 J = JBEGIN,JEND                                          
        TTEMP(J, 1) = 0.0                                               
        DO 305 K = KLOW,KEND                                            
        TTEMP(J,K) = .5*(TURMU(J,K)+TURMU(J,K-kmm))                     
  305   CONTINUE                                                        
         DO 350 K = KBEGIN,KEND                                         
         DO 350 J = JBEGIN,JEND                                         
            R1      = 1./XYJ(j,k)                                       
            BETA(J,K) =                                                 
     >      R1*(FMU(J,K)*PRLINV + TTEMP(J,K)*PRTINV)*HRE*G1             
            C0(J,K)  = R1*(FMU(J,K) + TTEMP(J,K))*HRE                   
 350   CONTINUE                                                         
C---------------------------------------------------------------------  
C       CROSS TERMS       (E_ETA)_XI  &  (F_XI)_ETA                     
C---------------------------------------------------------------------  
        DO 310 K=KLOW,KUP                                               
        DO 310 J=JBEGIN,JEND                                            
            T1      = XY(J,K,1)*XY(J,K,3)                               
            T2      = XY(J,K,1)*XY(J,K,4)                               
            T3      = XY(J,K,2)*XY(J,K,3)                               
            T4      = XY(J,K,2)*XY(J,K,4)                               
            C1(J,K) =  BETA(J,K)*( T1     +   T4    )                   
            C2(J,K) =  C0(J,K)*( F43*T1 +   T4    )                     
            C3(J,K) =  C0(J,K)*( T1     +   F43*T4)                     
            C4(J,K) =  C0(J,K)*(-F23*T2 +   T3    )                     
            C5(J,K) =  C0(J,K)*( T2     -   F23*T3)                     
 310     CONTINUE                                                       
C---------------------------------------------------------------------  
C                             (E_ETA)_XI    TERMS                       
C---------------------------------------------------------------------  
C                                                                       
C           FIRST CENTRAL DIFFERENCE IN ETA                             
C                                                                       
        DO 320 K=KLOW,KUP                                               
        KP1 = K+1                                                       
        KM1 = K-kmm                                                     
        DO 320 J=JBEGIN,JEND                                            
C                                                                       
C              NOTE FACTOR OF 0.5 IN BUILT INTO FMU & BETA              
C                                                                       
          RRM1  = 1./Q(J, KM1, 1)                                       
          RR1   = 1./Q(J, K, 1)                                         
          RRP1  = 1./Q(J, KP1, 1)                                       
          UUM1  = Q(J, KM1, 2)*RRM1                                     
          UUP1  = Q(J, KP1, 2)*RRP1                                     
          UU1   = Q(J, K,   2)*RR1                                      
          VVM1  = Q(J, KM1, 3)*RRM1                                     
          VVP1  = Q(J, KP1, 3)*RRP1                                     
          VV1   = Q(J, K,   3)*RR1                                      
          UETA  = UUP1 - UUM1                                           
          VETA  = VVP1 - VVM1                                           
        C2ETA =                                                         
     >    GAMMA*(PRESS(J, KP1)*RRP1  - PRESS(J, KM1)*RRM1)              
        VFLUX(J,K,2) = C2(J,K)*UETA + C4(J,K)*VETA                      
        VFLUX(J,K,3) = C5(J,K)*UETA + C3(J,K)*VETA                      
        VFLUX(J,K,4) = UU1*VFLUX(J,K,2) + VV1*VFLUX(J,K,3)              
     >            +    C1(J,K)*C2ETA                                    
 320    CONTINUE                                                        
      DO 400 J=JLOW,JUP                                                 
      JP1 = J + 1
      JM1 = J - 1
      DO 400 K=KLOW,KUP                                                 
         S(J,K,2) = S(J,K,2) + 0.5*(VFLUX(JP1,K,2) - VFLUX(JM1,K,2))    
         S(J,K,3) = S(J,K,3) + 0.5*(VFLUX(JP1,K,3) - VFLUX(JM1,K,3))    
         S(J,K,4) = S(J,K,4) + 0.5*(VFLUX(JP1,K,4) - VFLUX(JM1,K,4))    
 400    CONTINUE                                                        
C...................................................................    
C                             (F_XI)_ETA    TERMS                       
C...................................................................    
C                                                                       
C           FIRST CENTRAL DIFFERENCE IN XI                              
C                                                                       
        DO 410 J=JLOW,JUP                                               
        DO 410 K=KBEGIN,KEND                                            
            T1      = XY(J,K,1)*XY(J,K,3)                               
            T2      = XY(J,K,1)*XY(J,K,4)                               
            T3      = XY(J,K,2)*XY(J,K,3)                               
            T4      = XY(J,K,2)*XY(J,K,4)                               
            C1(J,K) =  BETA(J,K)*( T1     +   T4    )                   
            C2(J,K) =  C0(J,K)*( F43*T1 +   T4    )                     
            C3(J,K) =  C0(J,K)*( T1     +   F43*T4)                     
            C4(J,K) =  C0(J,K)*(-F23*T2 +   T3    )                     
            C5(J,K) =  C0(J,K)*( T2     -   F23*T3)                     
 410    CONTINUE                                                        
        DO 420 J=JLOW,JUP                                               
        JP1 = j + 1
        JM1 = J - 1
        DO 420 K=KBEGIN,KEND                                            
C                                                                       
C              NOTE FACTOR OF 0.5 IN BUILT INTO FMU & BETA              
C                                                                       
          RRM1  = 1./Q(JM1, K, 1)                                       
          RR1   = 1./Q(J, K, 1)                                         
          RRP1  = 1./Q(JP1, K, 1)                                       
          UUM1  = Q(JM1, K, 2)*RRM1                                     
          UUP1  = Q(JP1, K, 2)*RRP1                                     
          UU1   = Q(J, K,   2)*RR1                                      
          VVM1  = Q(JM1, K, 3)*RRM1                                     
          VVP1  = Q(JP1, K, 3)*RRP1                                     
          VV1   = Q(J, K,   3)*RR1                                      
          UXI  = UUP1 - UUM1                                            
          VXI  = VVP1 - VVM1                                            
        C2XI =                                                          
     >    GAMMA*(PRESS(JP1, K)*RRP1  - PRESS(JM1, K)*RRM1)              
        VFLUX(J,K,2) = C2(J,K)*UXI + C5(J,K)*VXI                        
        VFLUX(J,K,3) = C4(J,K)*UXI + C3(J,K)*VXI                        
        VFLUX(J,K,4) = UU1*VFLUX(J,K,2) + VV1*VFLUX(J,K,3)              
     >            +    C1(J,K)*C2XI                                     
 420    CONTINUE                                                        
      DO 500 K=KLOW,KUP                                                 
      KP1 = K+1                                                         
      KM1 = K-kmm                                                       
      DO 500 J=JLOW,JUP                                                 
         S(J,K,2) = S(J,K,2) + 0.5*(VFLUX(J,KP1,2) - VFLUX(J,KM1,2))    
         S(J,K,3) = S(J,K,3) + 0.5*(VFLUX(J,KP1,3) - VFLUX(J,KM1,3))    
         S(J,K,4) = S(J,K,4) + 0.5*(VFLUX(J,KP1,4) - VFLUX(J,KM1,4))    
 500  CONTINUE                                                          
C                                                                       
      RETURN                                                            
      END                                                               
