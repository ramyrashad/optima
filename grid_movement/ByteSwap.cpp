#ifndef BYTESWAP_H
#include "ByteSwap.h"
#endif

void ByteSwap (unsigned char * b, int n)
{
  register int i = 0;
  register int j = n-1;
  while (i<j)
    {
      std::swap(b[i], b[j]);
      i++, j--;
    }
}






