#!/bin/sh
#
# Mike Gertz - Jul 8, 1997
# 
# Usage: compileMex PROBLEM[.SIF] ...
#
# Attempts to compile and run prdmin on the problem named PROBLEM,
# which is assumed to be in "CUTE"-style SIF format. 
#
# This is the matlab version, so there is no point in specifying more
# than one problem on the command line. The matlab version can only compile
# the appropriate "mex" files. The solver must be run from within matlab. 
# It is possible, but more advanced, to run matlab in the background, but it
# is still preferable to run a matlab script in order to batch process a
# number of problems.
#
# See runon.m and matlabbg.
#
#set -e

CUTEDIR=$HOME/cuter/CUTEr.large.pc.lnx.g77
export CUTEDIR

if [ ! -d cutetmp ]
then
	mkdir cutetmp
	createdTmp=true
fi
cd cutetmp

for PROBLEM in $*; do
	PROBLEM=`basename $PROBLEM .SIF`  # Delete the .SIF suffix if present
	SIF=$PROBLEM.SIF
	ZIP=$SIF.gz
#	MASTSIF=$HOME/.cute/mastsif

	if [ -f $SIF ]
	then
		$CUTEDIR/bin/sdmx $PROBLEM
	else
		# A SIF file does not exist in the current directory.
		# Attempt to find an appropriately named .gz file
		if [ -f $ZIP ]
		then
			gunzip -c $ZIP > $SIF
		else
		    echo $MASTSIF
		# A .gz file does not exist in the current directory.
		# Try the MASTSIF directory
			if   [ -f $MASTSIF/$ZIP ]
			then
				gunzip -c $MASTSIF/$ZIP > $SIF
                        elif [ -f $MASTSIF/$SIF ]
                        then
                                cp $MASTSIF/$SIF $SIF
			else
				echo "Couldn't find a SIF file for $PROBLEM."
				exit 1
			fi
		fi
                echo "$CUTEDIR"
		$CUTEDIR/bin/sdmx $PROBLEM 
		rm $SIF

	fi
done

# ctools.mexsg and OUTSDIF.d are the files the matlab interface
# needs to function. Move them out of the temp directory
mv ctools.mexglx OUTSDIF.d ..
# Remove temporary files
rm -f ELFUN.[fo] GROUP.[fo] RANGE.[fo] AUTOMAT.d 
if [ $createdTmp ]
then
	# We made it, we clean it up
	cd ..
	rmdir cutetmp
fi
