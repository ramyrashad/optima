C ********************************************************************
C  Common Block for multiblock
C ********************************************************************
c
      integer pblk(5),nblks,nblkstot,jbmax,kbmax,nhalo,jkhalo

      common/mblock/ nblks,nblkstot,jbmax,kbmax,nhalo,jkhalo,pblk
      dimension jbmax(maxblk),kbmax(maxblk),jkhalo(maxblk,4)
c
c     nblks   : number of blocks in grid
c     fineblks: number of blocks in fine grid (for multigrid runs)
c     jbmax   : j max for block
c     kbmax   : k max for block
c     nhalo   : number of rows of halo points on each side of each block
c     pblk    : used in multigrid runs, points to first block of given level
c
C     **********************************************************
c     block connectivity data 
C     **********************************************************
c
      integer lblkpt, lsidept, idir, ibctype, ibcdir

      common/connect/ lblkpt, lsidept, idir, ibctype, ibcdir
      dimension lblkpt(maxblk,4),lsidept(maxblk,4),idir(maxblk,4)
      dimension ibctype(maxblk,4),ibcdir(maxblk,4)
c
c     lblkpt  : pointer to adjacent blocks
c     lsidept : pointer to adjacent sides
c     idir    : direction of adjacent side either -1 opposite
c                                              or  1 same direction
c               
c     ibctype : gives the boundary condition type: 0 interface
c                                                  1 wall
c                                                  2 far field
c                                                  3 outflow xi min
c                                                  4 outflow xi max
c                                                  5 avg between blks
c
C     ***********************************************************
c     pointers
C     ***********************************************************
c
      integer lgptr,  lqptr,  lwf1ptr, lwf2ptr, lwf3ptr, laaptr
      integer lgqptr,lspptr,intptr,lprecptr,ldhatqptr,ldhattptr

      common/ptrs/ lgptr,  lqptr,  lwf1ptr, lwf2ptr, lwf3ptr, laaptr,
     &             lgqptr,lspptr,intptr, lprecptr,ldhatqptr,ldhattptr
      dimension lgptr(maxblk), lqptr(maxblk)
      dimension lwf1ptr(maxblk), lwf2ptr(maxblk), lwf3ptr(maxblk)
      dimension laaptr(maxblk), lgqptr(maxblk)
      dimension lspptr(maxblk), lprecptr(maxblk), intptr(maxblk)
cmt ~~~
      dimension ldhatqptr(maxblk)
      dimension ldhattptr(maxblk)
cmt ~~~

c     lgptr   :   grid pointer
c     lqptr   :   pointer for solution vector q
c     laaptr  :   pointer for AA in Baldwin-Barth turb model
c     lgqptr  :   pointer for GQ in Baldwin-Barth turb model
c     lspptr  :   pointer for spectral radius used in matrix
c                 dissipation
c     lprecptr:   pointer for array precon used in preconditioner
cmt ~~~
c     ldhatqptr : dhatq pointer for ESDIRK routines
c     ldhattptr : dhatt pointer for ESDIRK routines
cmt ~~~
C
C     ***********************************************************
c     Trailing Edge Copy 
C     ***********************************************************
      integer ncopy,ibte1,ijte1,ikte1,ibte2,ijte2,ikte2
      common/tecopy/ncopy,ibte1,ijte1,ikte1,ibte2,ijte2,ikte2
      dimension ibte1(maxblk), ijte1(maxblk), ikte1(maxblk)
      dimension ibte2(maxblk), ijte2(maxblk), ikte2(maxblk)
c
C     ***********************************************************
c     Special Point Average condition
C     ***********************************************************
c     nsing : number of singular points 
c       
c          ** where to store results **
c     ibs1  : block 1 for singular point
c     js1   : j value 1 for singular point
c     ks1   : k value 1 for singular point
c     ibs2  : block 2 for singular point
c     js2   : j value 2 for singular point
c     ks2   : k value 2 for singular point
c
c          ** points to use to get avg. **
c     javg1 : j value 1 for singular point average
c     kavg1 : k value 1 for singular point average
c     javg2 : j value 2 for singular point average
c     kavg2 : k value 2 for singular point average
   
      integer nsing,ibs1,js1,ks1,ibs2,js2,ks2,javg1,kavg1,javg2,kavg2

      common/singavg/nsing,ibs1,js1,ks1,ibs2,js2,ks2,
     &               javg1,kavg1,javg2,kavg2
      dimension ibs1(maxblk), js1(maxblk), ks1(maxblk)
      dimension ibs2(maxblk), js2(maxblk), ks2(maxblk)
      dimension javg1(maxblk),kavg1(maxblk),javg2(maxblk),kavg2(maxblk)
c
C     ***********************************************************
c     Logicals for execution control
C     ***********************************************************
      common/control/eta1mesh, eta2mesh, xi1mesh, xi2mesh
      logical eta1mesh, eta2mesh, xi1mesh, xi2mesh
      dimension eta1mesh(maxblk), eta2mesh(maxblk)
      dimension xi1mesh(maxblk), xi2mesh(maxblk)
c
c     eta1mesh : mesh boundary conditions for start in eta direction
c     eta2mesh : mesh boundary conditions for end in eta direction
c     xi1mesh  : mesh boundary conditions for start in xi direction
c     xi2mesh  : mesh boundary conditions for end in xi direction
c
C     ***********************************************************
c     Common for special viscous data:
c     1) Transition control for viscous flows
c     2) Wall coordinates for turbulence model
C     ***********************************************************
c     maxfoil defined in parms.inc
      logical bnd
      integer icoord,jtranup,jtranlo
      double precision transup,translo,xcoord,ycoord,xtrailedge
      double precision ytrailedge
      
      dimension transup(maxfoil),translo(maxfoil)
      dimension jtranup(4,maxblk),jtranlo(4,maxblk),bnd(4,maxblk)
c     -if you change dimension of xcoord and ycoord here, don't
c      forget to change the dimensions in spalart-allmaras.f aswell.
      dimension xcoord(maxjfoil),ycoord(maxjfoil)
      dimension xtrailedge(maxfoil),ytrailedge(maxfoil)
      common/mbvisc/xcoord,ycoord,xtrailedge,ytrailedge,transup,translo,
     &              bnd,jtranup,jtranlo,icoord
c
c     ***********************************************************
c     Common for elemental force and moment coefficients
c     ***********************************************************
      double precision clfoil,cdfoil,cmfoil,clp,cdp,cmp,clf,cdf,cmf

      dimension clp(maxfoil),cdp(maxfoil),cmp(maxfoil)
      dimension clf(maxfoil),cdf(maxfoil),cmf(maxfoil)
      dimension clfoil(maxfoil),cdfoil(maxfoil),cmfoil(maxfoil)
      common/foilcoefs/clfoil,cdfoil,cmfoil,clp,cdp,cmp,clf,cdf,cmf
c
c     ***********************************************************
c     Common for segment data (i.e., list of block & side 
c                  combo's that define the individual elements)
c     ***********************************************************

      integer  maxsegs,maxtrans,nsegments,isegblk,isegside
      integer  nfoils,itrans,itranj,itranside

      parameter (maxsegs=20,maxtrans=8)
      dimension nsegments(maxfoil)
      dimension isegblk(maxsegs,maxfoil),isegside(maxsegs,maxfoil)
      dimension itrans(maxblk),itranj(maxblk,maxtrans)
      dimension itranside(maxblk,maxtrans)
      common/sortedfoils/nsegments,isegblk,isegside,
     &                   nfoils,itrans,itranj,itranside
c
C     ***********************************************************
C     Data for point copy of dissipation values 
C     --- used only for sharp trailing edges when
c         wake line is calculated
C     ***********************************************************
      integer nydisp,ib1disp,ib2disp,jb1disp,kb1disp,jb2disp,kb2disp
      common/mbdispcp/ nydisp, ib1disp, ib2disp, jb1disp, kb1disp,
     &                                           jb2disp, kb2disp
      dimension ib1disp(maxblk), ib2disp(maxblk)
      dimension jb1disp(maxblk), kb1disp(maxblk) 
      dimension jb2disp(maxblk), kb2disp(maxblk) 
c      common/mbresid/ nyresid, ib1resid, ib2resid, jb1resid, kb1resid,
c     &                                             jb2resid, kb2resid
c      dimension ib1resid(maxblk), ib2resid(maxblk)
c      dimension jb1resid(maxblk), kb1resid(maxblk) 
c      dimension jb2resid(maxblk), kb2resid(maxblk) 
c
c     ***********************************************************
c     Special treatment of singular point version 2 (for viscous)
c     ***********************************************************
      integer nsng,ibsng1,jsng1,ksng1,jpsng1,kpsng1
      integer ibsng2,jsng2,ksng2,jpsng2,kpsng2
      common/sing2v/nsng,ibsng1,jsng1,ksng1,jpsng1,kpsng1,
     &                   ibsng2,jsng2,ksng2,jpsng2,kpsng2
      dimension ibsng1(maxblk), jsng1(maxblk), ksng1(maxblk)
      dimension jpsng1(maxblk), kpsng1(maxblk)
      dimension ibsng2(maxblk), jsng2(maxblk), ksng2(maxblk)
      dimension jpsng2(maxblk), kpsng2(maxblk)
c
C     **************************************************************
C     Interface information for block corners
C     S is avg between corner points in xiimpl and etaimpl
C     Up to 6 blocks may share a corner point  
C     **************************************************************

      integer mxcorner,mxconex,ncorner,ncornerblks,iblkcorner
      integer jcorner,kcorner

      parameter (mxcorner=10,mxconex=6)
      common/corner/ncorner,ncornerblks,iblkcorner,jcorner,kcorner
      dimension ncornerblks(mxcorner), iblkcorner(mxcorner,mxconex)
      dimension jcorner(mxcorner,mxconex), kcorner(mxcorner,mxconex)
c     ncorner     : corners in the mesh
c     ncornerblks : number of blocks sharing a corner point
c     iblkcorner  : array of block numbers sharing a corner point 
c     jcorner     : j location of corner in a block 
c     kcorner     : k location of corner in a block
c
c
C     **************************************************************
C     Variables for treatment of trailing edge points with full overlap
C     (Specification of pts to avg after t.e. pt copy
C     **************************************************************
c      parameter   (mxte=15)

      integer mxte,nteavg,ibteavg1,jteavg1,kteavg1,ibteavg2
      integer jteavg2,kteavg2

      parameter   (mxte=maxblk)
      common/teavg/nteavg,ibteavg1,jteavg1,kteavg1,
     &                    ibteavg2,jteavg2,kteavg2
      dimension ibteavg1(mxte), jteavg1(mxte), kteavg1(mxte)
      dimension ibteavg2(mxte), jteavg2(mxte), kteavg2(mxte)
c
c
C*    ************************************************************
C     STAGNATION POINT DATA AS READ IN FROM 'tornado.stg'         
C     ************************************************************
c      parameter   (mxstg=15)

      integer mxstg,nstg,nstgtot,istgblk,istgtyp,istgcnr

      parameter   (mxstg=maxblk)
      common/stgpt/nstg,nstgtot,istgblk,istgtyp,istgcnr
      dimension   istgblk(mxstg),istgtyp(mxstg),istgcnr(mxstg)
c     nstg        : number of stagnation points in the geometry
c     istgblk     : block number containing stagnation point(s)
c     istgtyp     : =1 for a LE and =-1 for a TE
c     istgcnr     : corner location for stagnation point (corner # N is
c                   the corner immediately preceeding side # N)
c
c     ************************************************************
c     Varibles for 6-block singular points
c     ************************************************************

      integer mx6sng,n6sng,ibs6,js6,ks6,javg6,kavg6

      parameter(mx6sng=12)
      common/sing6pt/n6sng,ibs6,js6,ks6,javg6,kavg6
      dimension ibs6(6,mx6sng)
      dimension js6(6,mx6sng),   ks6(6,mx6sng)
      dimension javg6(6,mx6sng), kavg6(6,mx6sng)
