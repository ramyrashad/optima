      subroutine prlng_turre(fblk,cblk,jdimf,kdimf,turref,
     &                                 jdimc,kdimc,turrec)
c
#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
c
      integer fblk,cblk,jdimf,kdimf,jdimc,kdimc
      dimension turref(jdimf,kdimf),turrec(jdimc,kdimc)
c
      jmnc=jminbnd(cblk)
      jmxc=jmaxbnd(cblk)
      kmnc=kminbnd(cblk)
      kmxc=kmaxbnd(cblk)
c
c     -finding fine q in from LL corner to UR interior corner
      do 70 jc = jmnc,jmxc-1
      do 70 kc = kmnc,kmxc-1
c        jf = (jc-jmnc)*2 + jmnc
c        kf = (kc-kmnc)*2 + kmnc
        jf = 2*jc-jmnc
        kf = 2*kc-kmnc
c       -copy value at node:
        turref(jf,kf) = turrec(jc,kc)
c       -node b/w coarse nodes in k-dir:
        turref(jf,kf+1) = .5d0*(turrec(jc,kc) + turrec(jc,kc+1))
c       -node b/w coarse nodes in j-dir:
        turref(jf+1,kf) = .5d0*(turrec(jc,kc) + turrec(jc+1,kc))
c       -fine node formed from four coarse nodes:
        turref(jf+1,kf+1) =.25d0*(turrec(jc,kc)   + turrec(jc+1,kc) + 
     &        turrec(jc,kc+1) + turrec(jc+1,kc+1))
 70   continue
c
c
c     -finding nodes on block side 3, except UR corner node
      kc = kmxc
      kf = 2*kc-kmnc
c
      do 80 jc = jmnc,jmxc-1
        jf = 2*jc-jmnc
c       -copy value at node:
        turref(jf,kf) = turrec(jc,kc)
c       -node b/w coarse nodes j & j+1:
        turref(jf+1,kf) = .5d0*(turrec(jc,kc) + turrec(jc+1,kc))
 80   continue
c     
c
c     -finding nodes on block side 4, except UR corner node ----c
      jc = jmaxbnd(cblk)
      jf = 2*jc-jmnc
c
      do 90 kc= kmnc,kmxc-1
        kf = 2*kc-kmnc
c       -copy value at node:
        turref(jf,kf) = turrec(jc,kc)
c       -node b/w coarse nodes j & j+1:
        turref(jf,kf+1) = .5d0*(turrec(jc,kc) + turrec(jc,kc+1))
 90   continue
c
c     -form fine UR node:
      jc = jmaxbnd(cblk)
      kc = kmaxbnd(cblk)
      jf = 2*jc-jmnc
      kf = 2*kc-kmnc
c
      do n = 1,4
        turref(jf,kf) = turrec(jc,kc)
      end do
c
      return
      end
