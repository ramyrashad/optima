C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          IIIII   NN    N  IIIII  TTTTTTT   GGGGG    AAAAA            C
C            I     N N   N    I       T     G        A     A           C
C            I     N  N  N    I       T     G  GGGG  AAAAAAA           C
C            I     N   N N    I       T     G     G  A     A           C
C          IIIII   N    NN  IIIII     T      GGGGG   A     A           C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE INITGA(IPOP,nchrom,ngene,nobfun,pop,fit,i_mnx,
     $     fit_init,init_ga,genemin,genemax,rand_init_fit,
     $     fitavg,fitmin,fitmax,compute_fit,i_ran_seed)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTINE INITIALIZES CERTAIN INTERNAL           C
C               PARAMETERS AS WELL AS THE INITIAL POPULATION           C
C               DISTRIBUTION (POP), BUT THE POP IS INITIALIZED         C
C               ONLY IF IPOP=0 ***                                     C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      dimension pop(ngene,nchrom),fit(nobfun,nchrom)
      dimension genemin(ngene),genemax(ngene)
      dimension fitmin(nobfun),fitmax(nobfun),fitavg(nobfun)
      logical compute_fit(nchrom)
      real*8 rand
      logical rand_init_fit
      dimension del_gene(ngene)
      integer seed
C
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** INITIALIZE PARAMETERS ***
C
      do nc = 1,nchrom
         compute_fit(nc) = .true.
      enddo

C
cthp_2008      fit_init = 0.0
cthp_2008      if(i_mnx.eq.0)fit_init = 1.0e30
      DO NO=1,NOBFUN
         FITAVG(NO) = fit_init 
         FITMAX(NO) = fit_init
         FITMIN(NO) = fit_init
      ENDDO
C

      call init_fit(nchrom,nobfun,fit,i_mnx,fit_init,rand_init_fit)
C
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** ESTABLISH INITIAL POPULATION DISTRIBUTION
C              USING RANDOM NUMBER GENERATOR (RAN3). IPOP
C              (GNERATION NUMBER) IS USED TO SEED THE RANDOM 
C              NUMBER GENERATOR AND THUS ENSURES A 
C              DIFFERENT SEQUENCE OF RANDOM NUMBERS FOR EACH 
C              GENERATION. MAKE SURE THAT THE MAXIMUM AND 
C              MINIMUM VALUES FOR EACH GENE ARE NOT VIOLATED.
C              THE FIRST CHROMOSOME IS ESTABLISHED USING 
C              SIMPLE AVERAGES BETWEEN EACH GENE VALUE. THIS
C              PRODUCES A SO-CALLED BASELINE chromo.i FILE
C              WITH A CORRESPONDING BASELINE SOLUTION ***
C
      IF (IPOP.EQ.0) THEN
         if(init_ga.eq.0)then
            DO NG=1,NGENE
               POP(NG,1) = 0.5*(GENEMAX(NG)+GENEMIN(NG))
            ENDDO 
            IDUM = IPOP+1
            DO NC=2,NCHROM
               CALL RAN3(IDUM,RAND)
               DO NG=1,NGENE
                  POP(NG,NC) = (1.0-rand)*GENEMAX(NG)+rand*GENEMIN(NG)
               enddo
            enddo
         elseif(init_ga.eq.1)then
            IDUM = IPOP+1
            DO NC=1,NCHROM
               DO NG=1,NGENE
                  CALL RAN3(IDUM,RAND)
                  POP(NG,NC) = 
     $                 RAND*(GENEMAX(NG)-GENEMIN(NG))+GENEMIN(NG)
               ENDDO
            ENDDO
         elseif(init_ga.eq.2)then
            if(i_ran_seed.ge.0.0)then
               call get_seed ( seed )

               call random_initialize ( seed )
               write ( *, '(a)' ) ' '
               write ( *, '(a,i12)' ) '  GET_SEED returns SEED = ', seed
            else
               seed = i_ran_seed
            endif
            call latin_random ( ngene, nchrom, seed, pop )
                                ! return 0-1 distribution in pop
            DO NC=1,NCHROM
               DO NG=1,NGENE
                  POP(NG,NC) = pop(ng,nc)*
     $                 (GENEMAX(NG)-GENEMIN(NG))+GENEMIN(NG)
               ENDDO
            ENDDO

         elseif(init_ga.lt.0 .and. abs(init_ga).gt.1)then
c     nchrom needs to be init_ga^ngene
c     e.g. for ngene = 3 and init_ga=10 nchrom = 10^3 = 1000
            if(abs(init_ga)**ngene .ne. nchrom)then
               print *,' nchrom need to be ',abs(init_ga)**ngene,
     $              ' but nchrom = ',nchrom
               stop
            endif
            if(ngene .gt. 4)then
               print *,' ngene needs to <=4 but ngene = ',ngene
               stop
            endif
            DO NG=1,NGENE
               del_gene(ng) = (GENEMAX(NG)-GENEMIN(NG))/(abs(init_ga)-1)
            enddo

            if(ngene.eq.1)then
               nc = 0
               ng = 1
               DO ni=1,abs(init_ga)
                  nc = nc + 1
                  POP(NG,NC) = del_gene(ng)*(ni-1) + genemin(ng)
               ENDDO
            elseif(ngene.eq.2)then
               nc = 0
               DO ni=1,abs(init_ga)
                  DO nii=1,abs(init_ga)
                     nc = nc + 1
                     ng = 1
                     POP(NG,NC) = del_gene(ng)*(ni-1) + genemin(ng)
                     ng = 2
                     POP(NG,NC) = del_gene(ng)*(nii-1) + genemin(ng)
                  ENDDO
               enddo
            elseif(ngene.eq.3)then
               nc = 0
               DO ni=1,abs(init_ga)
                  DO nii=1,abs(init_ga)
                     DO niii=1,abs(init_ga)
                        nc = nc + 1
                        ng = 1
                        POP(NG,NC) = del_gene(ng)*(ni-1) + genemin(ng)
                        ng = 2
                        POP(NG,NC) = del_gene(ng)*(nii-1) + genemin(ng)
                        ng = 3
                        POP(NG,NC) = del_gene(ng)*(niii-1) + genemin(ng)
                     ENDDO
                  enddo
               enddo
            elseif(ngene.eq.4)then
               nc = 0
               DO ni=1,abs(init_ga)
                  DO nii=1,abs(init_ga)
                     DO niii=1,abs(init_ga)
                        DO niiii=1,abs(init_ga)
                           nc = nc + 1
                           ng = 1
                           POP(NG,NC)=del_gene(ng)*(ni-1)+genemin(ng)
                           ng=2
                           POP(NG,NC)=del_gene(ng)*(nii-1)+genemin(ng)
                           ng=3
                           POP(NG,NC)=del_gene(ng)*(niii-1)+genemin(ng)
                           ng=4
                           POP(NG,NC)=del_gene(ng)*(niiii-1)+genemin(ng)
                        ENDDO
                     enddo
                  enddo
               enddo
            endif

         endif                  !init_ga
      endif                     !ipop.eq.0
C
C          *** INITIALIZATION COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
