C ********************************************************************
C *************  COMPUTE Nonlinear 2nd and 4th order COEFficients  ***
C ********************************************************************
      SUBROUTINE coef24x(jdim,kdim,coef2x,coef4x,pcoef,spect,
     &          jbegin,jend,kbegin,kend)

      implicit none

#include "../include/common.inc"

c*****************************************************************
c   coef2x comes in as pressure gradient coefficient         
c   coef4x comes in as specx (spectral radius)                          
c*****************************************************************

      INTEGER jdim,kdim,jbegin,jend,kbegin,kend,j,k
      DOUBLE PRECISION coef2x(jdim,kdim),coef4x(jdim,kdim)
      DOUBLE PRECISION pcoef(jdim,kdim),spect(jdim,kdim),c2,c4

      DO k = kbegin,kend
         DO j = jbegin,jend
            pcoef(j,k) = coef2x(j,k)
            spect(j,k) = coef4x(j,k)
         ENDDO
      ENDDO

c     form 2nd and 4th order dissipation coefficients in x

      DO j = jbegin,jend-1
         DO k = kbegin,kend
            c2 = dis2x*(pcoef(j+1,k)*spect(j+1,k)+pcoef(j,k)*spect(j,k))
            c4 = dis4x*(spect(j+1,k) + spect(j,k))
            c4 = c4 - min(c4,c2)
            coef2x(j,k) = c2
            coef4x(j,k) = c4
         ENDDO
      ENDDO

      DO k = kbegin,kend
        coef2x(jend,k) = coef2x(jend-1,k)
        coef4x(jend,k) = coef4x(jend-1,k)
      ENDDO

      RETURN
      END

