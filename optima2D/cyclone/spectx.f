c #########################
c ##                     ##
c ##  subroutine spectx  ##
c ##                     ##
c #########################
c
      subroutine spectx (jdim,kdim,uu,ccx,xyj,xy,spect,precon)
c
c**********************************************************************
c  for ispec =-1   spect =  64./xyj
c  for ispec = 0   spect =  1./xyj
c  for ispec = 1   form spect and specty independently
c                  spect = (abs(uu) + ccx)/xyj
c  for ispec = 3   form  spect independently with  limits
c                  spect = (abs(vv) + ccy)/xyj
c  for ispec = 4   spect = (abs(xy(1)+abs(xy(2)) /xyj
c**********************************************************************
c
#include "../include/arcom.inc"
c
      dimension xyj(jdim,kdim),xy(jdim,kdim,4),precon(jdim,kdim,6)
      dimension spect(jdim,kdim),uu(jdim,kdim),ccx(jdim,kdim)

c
c
c
c---- compute dissipation scaling ----
      if(ispec.eq.0) then
c
c   constant
c
        do 50 k = kbegin,kend
          do 50 j = jbegin,jend
            rj = 1./xyj(j,k)
            spect(j,k) = rj
50      continue
c
      elseif(ispec.eq.1)then
c
c  spectral radius
c
        if (prec.eq.0) then
          do 100 k=kbegin,kend
            do 100 j=jbegin,jend
              sigx = abs(uu(j,k)) + ccx(j,k)
              rj = 1./xyj(j,k)
              spect(j,k) = sigx*rj
100       continue
       else
          do 1000 k=kbegin,kend
            do 1000 j=jbegin,jend
              sigx=abs(precon(j,k,3))+precon(j,k,4)
              rj=1.0/xyj(j,k)
              spect(j,k)=sigx*rj
1000      continue
        endif
c
c  ispec = 3 use limit specral radii
c
      elseif(ispec.eq.3)then
        do 200 k=kbegin,kend
c
          do 250 j=jbegin,jend
            sigx = abs(uu(j,k)) + ccx(j,k)
c
            rj = 1./xyj(j,k)
            spect(j,k) = sigx
250       continue
c
          do 260 j=jbegin,jend
            if(spect(j,k).lt.21.)spect(j,k)=21.
            if(spect(j,k).gt.1000.)spect(j,k)=1000.
260       continue
c
          do 270 j=jbegin,jend
            rj = 1./xyj(j,k)
            spect(j,k) = spect(j,k)*rj
270       continue
c
200     continue
c
      elseif(ispec.eq.4)then
c
c  spectral radius
c
        do 400 k=kbegin,kend
          do 400 j=jbegin,jend
c
            sigx = abs(xy(j,k,1)) + abs(xy(j,k,2))
c
            rj = 1./xyj(j,k)
            spect(j,k) = sigx*rj
c
400     continue
c
      elseif(ispec.eq.-1)then
c
        do 500 k=kbegin,kend
          do 500 j=jbegin,jend
c
            spect(j,k) = 64./xyj(j,k)
c
500     continue
c
      endif
c
c
      return
      end
