      integer maxj, maxk, maxjk, maxpen, mbudj, mbudk, mturj, mturk
      integer nblk, nblk2, iwk
      complex*16 ci
      integer tdim,sdim

      !-- Grid Size Parameters

      parameter( maxj=289 , maxk=121 )      
      !parameter( maxj=321 , maxk=385 )      
      !parameter( maxj=449 , maxk=385 )      
      !parameter( maxj=511 , maxk=245 )      
      !parameter( maxj=541 , maxk=245 )      
      !parameter( maxj=513 , maxk=241 )      

      parameter(maxjk=maxj*maxk, maxpen=1, mbudj=1, mbudk=1,
     &     mturj=1, mturk=1, nblk=5)
      parameter(nblk2=nblk*nblk)  

      !-- memory for ILU preconditioner

      parameter (iwk = maxjk*nblk2*12) 

      !-- FWH parameters

      parameter ( tdim=1500, sdim=350, ci=(0.d0,1.d0) )
