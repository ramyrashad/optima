      subroutine stepf2dylu(jdim,kdim,q,turmu,fmu,s,press,sndsp,xy,xyj,
     *                    ds,coef2,coef4,uu,cc,work,spect,lu,akk)
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension ds(jdim,kdim),turmu(jdim,kdim),fmu(jdim,kdim)           
c                                                                       
      dimension spect(jdim,kdim,3)
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension uu(jdim,kdim),cc(jdim,kdim)
      double precision    lu(jdim,kdim,4,5)
      double precision akk
c
c spect added for matrix dissiaption by kyle frew 
c vorticity.inc added by k.frew june 95
c
c                                                                       
      dimension work(jdim,kdim,10)       
c                                                                       
c                                                                       
      dtd   = dt * thetadt/(1. + phidt)                                 
      hd    = 0.5*dtd                                                   
c  dissipation coefficients for implicit side                           
c                                                                       
       smudt = smuim*dt* thetadt/(1. + phidt)                           
c   
c                                                                       
c  eta inversion part of diagonal scheme                                
c                                                                       
c  set eigenvalue swtiches                                              
c                                                                       
c  do n = 1 and 2 together                                              
c
       if(idmodel.eq.2) call mspecty(jdim,kdim,uu,cc,xyj,xy,spect)
       do 445 n = 2,4                                              
          if ( n .eq. 2) sn = 0.                                   
          if ( n .eq. 3) sn = 1.                                   
          if ( n .eq. 4) sn = -1.  
          nm=n-1
c
c
      if(idmodel.eq.1)then
         if (meth.eq.5) then 
             k   = klow                                                    
             km1 = k-1                                                     
             do 422 j = jlow,jup                                           
               c2m = coef2(j,km1)*dtd                                      
               c4m = coef4(j,km1)*dtd                                      
               c2 = coef2(j,k)*dtd                                       
               c4 = coef4(j,k)*dtd                                       
               lu(j,k,n,1) =  0.                                           
               lu(j,k,n,2) = -(c2m + c4m + c4)*xyj(j,km1)                  
               lu(j,k,n,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                
               lu(j,k,n,4) = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)                
               lu(j,k,n,5) = xyj(j,k+2)*c4                                 
422          continue                                                      
c                                                                          
             k   = kup                                                     
             km1 = k-1                                                     
             do 426 j = jlow,jup                                           
               c2m = coef2(j,km1)*dtd                                      
               c4m = coef4(j,km1)*dtd                                      
                 c2 = coef2(j,k)*dtd                                       
                 c4 = coef4(j,k)*dtd                                       
               lu(j,k,n,1) =  xyj(j,k-2)*c4m                               
               lu(j,k,n,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)              
               lu(j,k,n,3)  = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)               
               lu(j,k,n,4)  = -(c2 + c4 + c4m)*xyj(j,k+1)                  
               lu(j,k,n,5)  = 0.                                           
426          continue                                                      
c                                                                          
c         fourth difference in interior                                            
          do 428 k = klow+1,kup-1                                         
             km1 = k-1                                                     
             do 430 j = jlow,jup                                           
               c2m = coef2(j,km1)*dtd                                      
               c4m = coef4(j,km1)*dtd                                      
               c2 = coef2(j,k)*dtd                                       
               c4 = coef4(j,k)*dtd                                       
               lu(j,k,n,1)  =  xyj(j,k-2)*c4m                              
               lu(j,k,n,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)              
               lu(j,k,n,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)               
               lu(j,k,n,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)               
               lu(j,k,n,5)  = xyj(j,k+2)*c4                                
430          continue                                                      
428        continue
c                                                                          
         elseif (meth.eq.4) then
c !!!!   !!!!!The next line is important if you go to 5 diagonals!!!!!!!
c          do 428 k = klow+1,kup-1                                         
           do 438 k = klow,kup                                         
             km1 = k-1                                                     
             do 432 j = jlow,jup                                           
               c2m = (coef2(j,km1) + 2.*coef4(j,km1))*dtd
               c2 = (coef2(j,k) + 2.*coef4(j,k))*dtd
               lu(j,k,n,2)  = -c2m*xyj(j,km1)  
               lu(j,k,n,3)  = xyj(j,k)*(c2m+c2)
               lu(j,k,n,4)  = -c2*xyj(j,k+1)   
432          continue      
438        continue
         endif
      else
c ********************************************************************
c **                       matrix diss model                        **
c ********************************************************************
         if (meth.eq.5) then 
             k   = klow                                                    
             km1 = k-1                                                     
             kp1 = k+1
             do 522 j = jlow,jup                                           
               c2m = (coef2(j,km1)*spect(j,km1,nm)+coef2(j,k)*
     $               spect(j,k,nm))*dtd 
               c4m = (coef4(j,km1)*spect(j,km1,nm)+coef4(j,k)*
     $               spect(j,k,nm))*dtd
               c2 = (coef2(j,k)*spect(j,k,nm)+coef2(j,kp1)*
     $              spect(j,kp1,nm))*dtd
               c4 = (coef4(j,k)*spect(j,k,nm)+coef4(j,kp1)*
     $              spect(j,kp1,nm))*dtd
               lu(j,k,n,1) =  0.                                           
               lu(j,k,n,2) = -(c2m + c4m + c4)*xyj(j,km1)                  
               lu(j,k,n,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                
               lu(j,k,n,4) = -(c2 + 3.*c4 + c4m)*xyj(j,kp1)                
               lu(j,k,n,5) = xyj(j,k+2)*c4                                 
522          continue                                                      
c                                                                          
             k   = kup                                                     
             km1 = k-1                                                     
             kp1 = k+1
             do 526 j = jlow,jup                                           
               c2m = (coef2(j,km1)*spect(j,km1,nm)+coef2(j,k)*
     $               spect(j,k,nm))*dtd          
               c4m = (coef4(j,km1)*spect(j,km1,nm)+coef4(j,k)*
     $               spect(j,k,nm))*dtd
               c2 = (coef2(j,k)*spect(j,k,nm)+coef2(j,kp1)*
     $              spect(j,kp1,nm))*dtd
               c4 = (coef4(j,k)*spect(j,k,nm)+coef4(j,kp1)*
     $              spect(j,kp1,nm))*dtd
               lu(j,k,n,1) =  xyj(j,k-2)*c4m                               
               lu(j,k,n,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)              
               lu(j,k,n,3)  = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)               
               lu(j,k,n,4)  = -(c2 + c4 + c4m)*xyj(j,k+1)                  
               lu(j,k,n,5)  = 0.                                           
526          continue                                                      
c                                                                          
c         fourth difference in interior                                            
          do 528 k = klow+1,kup-1                                         
             km1 = k-1                                                     
             kp1 = k+1
             do 530 j = jlow,jup                                           
               c2m = (coef2(j,km1)*spect(j,km1,nm)+coef2(j,k)*
     $               spect(j,k,nm))*dtd
               c4m = (coef4(j,km1)*spect(j,km1,nm)+coef4(j,k)*
     $               spect(j,k,nm))*dtd 
               c2 =(coef2(j,k)*spect(j,k,nm)+coef2(j,kp1)*
     $              spect(j,kp1,nm))*dtd
               c4 = (coef4(j,k)*spect(j,k,nm)+coef4(j,kp1)*
     $              spect(j,kp1,nm))*dtd
               lu(j,k,n,1)  =  xyj(j,k-2)*c4m                              
               lu(j,k,n,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)              
               lu(j,k,n,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)               
               lu(j,k,n,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,kp1)               
               lu(j,k,n,5)  = xyj(j,k+2)*c4                                
530          continue                                                      
528        continue
c                                                                          
         elseif (meth.eq.4) then
c !!!!   !!!!!The next line is important if you go to 5 diagonals!!!!!!!
c          do 538 k = klow+1,kup-1                                         
           do 538 k = klow,kup                                         
             km1 = k-1
             kp1 = k+1
             do 532 j = jlow,jup                                           
               c2m = (coef2(j,km1)*spect(j,km1,nm)+coef2(j,k)*
     $               spect(j,k,nm))*dtd
               c4m = (coef4(j,km1)*spect(j,km1,nm)+coef4(j,k)*
     $               spect(j,k,nm))*dtd 
               c2 =(coef2(j,k)*spect(j,k,nm)+coef2(j,kp1)*
     $              spect(j,kp1,nm))*dtd
               c4 = (coef4(j,k)*spect(j,k,nm)+coef4(j,kp1)*
     $              spect(j,kp1,nm))*dtd
               c2m = c2m + 2.*c4m
               c2 = c2 + 2.*c4
               lu(j,k,n,2)  = -c2m*xyj(j,km1)  
               lu(j,k,n,3)  = xyj(j,k)*(c2m+c2)
               lu(j,k,n,4)  = -c2*xyj(j,k+1)   
532          continue      
538        continue
         endif
      endif
c                                                                       
c add approx. viscous eigenvalues                                       
c                                                                       
        if(viscous .and. viseta )then                                   
           hre = hd/re                                                       
c                                                                       
c    sutherland equation                                                
c                                                                       
           do 4000 k = kbegin,kend                                      
           do 4000 j = jlow,jup                                         
               rinv = 1./q(j,k,1)                                       
c           eta_x**2 + eta_y**2                                         
               etamet = xy(j,k,3)**2 + xy(j,k,4)**2                     
               djac = hre/xyj(j,k)                                      
               work(j,k,6) = djac*etamet                                
c           xyj/rho                                                     
               work(j,k,7) = rinv                                       
4000           continue                                                 
c                                                                       
           do 4401 k = klow,kup                                         
           do 4401 j = jlow,jup                                         
c           turb. viscosity                                             
               vnum1 = 0.5*(fmu(j,k)+fmu(j,k-1)) + turmu(j,k-1)         
               vnum  = 0.5*(fmu(j,k)+fmu(j,k+1)) + turmu(j,k)           
               ctm1 = (work(j,k-1,6)+work(j,k,6))*vnum1                 
               ctp1 = (work(j,k+1,6)+work(j,k,6))*vnum                  
               cttt = ctm1 + ctp1                                       
               lu(j,k,n,2) = lu(j,k,n,2) - work(j,k-1,7)*ctm1           
               lu(j,k,n,3) = lu(j,k,n,3) + work(j,k,7)*cttt             
               lu(j,k,n,4) = lu(j,k,n,4) - work(j,k+1,7)*ctp1           
4401       continue                                                     
c                                                                       
          endif                                                         
c                                                                       
c  end viscous                                                          
c                                                                       
csi      add terms into lhs for time
         fct=1.d0
         if (ismodel.gt.1) then
            if (jesdirk .eq. 4 .or. jesdirk.eq.3) then
               fct=1.0d0*dt/(akk*dt2)
               do 436 k=klow,kup
               do 436 j=jlow,jup
                  lu(j,k,n,3) = lu(j,k,n,3) + fct
 436           continue
               fct=1.0d0/(akk*dt2)
            else
               fct=1.5d0*dt/dt2
               do 434 k=klow,kup
               do 434 j=jlow,jup
                  lu(j,k,n,3) = lu(j,k,n,3) + fct
 434           continue
               fct=1.d0
               if (ismodel.eq.2) then
                  fct=1.5d0/dt2
               endif
            endif
         endif
c                                                                       
c  add in flux jacobians, variable dt and identity                      
c                                                                       
        do 435 k = klow,kup                                             
          do 435 j = jlow,jup                                           
          lu(j,k,n,2) = lu(j,k,n,2) - (uu(j,k-1)+sn*cc(j,k-1))*hd       
          lu(j,k,n,4) = lu(j,k,n,4) + (uu(j,k+1)+sn*cc(j,k+1))*hd       
          lu(j,k,n,2) = lu(j,k,n,2)*ds(j,k)                             
          lu(j,k,n,3) = lu(j,k,n,3)*ds(j,k)                             
          lu(j,k,n,4) = lu(j,k,n,4)*ds(j,k)                             
          lu(j,k,n,3) = lu(j,k,n,3) + fct
435     continue                                                        
        if (meth.eq.5) then
            do 442 k = klow,kup                                         
               do 440 j = jlow,jup                                            
                   lu(j,k,n,1) = lu(j,k,n,1)*ds(j,k)                           
                   lu(j,k,n,5) = lu(j,k,n,5)*ds(j,k)                           
440            continue                                                    
442         continue
        endif
445   continue                                                    
      if (meth.eq.4) then 
         i1=2
         i2=4
      else
         i1=1
         i2=5
      endif
      do 450 i=i1,i2
      do 450 k=klow,kup
      do 450 j=jlow,jup
          lu(j,k,1,i)=lu(j,k,2,i)
450   continue                                                      
c                                                                       
      return                                                            
      end                                                               

