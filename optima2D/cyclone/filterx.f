c ##########################
c ##                      ##
c ##  subroutine filterx  ##
c ##                      ##
c ##########################
c
      subroutine filterx (jdim,kdim,q,s,s0,xyj,coef2,coef4,sndsp,precon,
     >                    work,tmp)
c
c******************************************************************
c   this subroutine follows very faithfully t. pulliam notes, p.31
c
c   fourth order smoothing, added explicitly to rhs
c   second order near shocks with pressure grd coeff.
c
c  xi direction
c
c   start differences each variable separately
c******************************************************************
      use disscon_vars
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)
      dimension work(jdim,kdim,3),precon(jdim,kdim,6),tmp(jdim,kdim,4)
      dimension s0(jdim,kdim,4)
c
      do 15 n = 1,4
c
c---- 1st-order forward difference ----
      do 16 k =klow,kup
      do 16 j =jlow,jup
         jp1=jplus(j)
         work(j,k,1) = q(jp1,k,n)*xyj(jp1,k) - q(j,k,n)*xyj(j,k)
 16   continue
c  -- c mesh bc --
      if (.not.periodic) then
         j1 = jbegin
         j2 = jend
         do 76 k = klow,kup
            work(j1,k,1) = q(j1+1,k,n)*xyj(j1+1,k) -
     >                     q(j1,k,n)*xyj(j1,k)
            work(j2,k,1) = work(j2-1,k,1)
 76      continue
      endif
c
c
c---- apply cent-dif to 1st-order forward ----
      do 17 k =klow,kup
      do 17 j =jlow,jup
         jp1=jplus(j)
         jm1=jminus(j)
         work(j,k,2) = work(jp1,k,1)-2.d0*work(j,k,1)+work(jm1,k,1)
 17   continue
c  -- c mesh bc --
      if (.not.periodic) then
         j1 = jbegin
         j2 = jend
         do 77 k =klow,kup
            work(j1,k,2) = q(j1+2,k,n)*xyj(j1+2,k) -
     >           2.d0*q(j1+1,k,n)*xyj(j1+1,k) + q(j1,k,n)*xyj(j1,k)
            work(j2,k,2) = 0.
 77      continue
      endif
c
c
      !-- Initialize work array before forming dissipation term
      do 78 k=1,kdim
      do 78 j=1,jdim
            work(j,k,3) = 0.0
 78   continue

c---- form dissipation term before last differentiation ----
      do 19 k =klow,kup
      do 19 j =jlow,jup
         work(j,k,3) = (coef2(j,k)*work(j,k,1)-coef4(j,k)*work(j,k,2))
 19   continue
c  -- c mesh bc --
      if (.not.periodic) then
         j1 = jbegin
         j2 = jend
         do 79 k = klow,kup
            work(j1,k,3) = (coef2(j1,k)*work(j1,k,1) -
     >                      coef4(j1,k)*work(j1,k,2))
            work(j2,k,3) = 0.d0
79       continue
      endif
c
c
      !-- Initialize tmp array before last differentiation      
      do 81 k=1,kdim
      do 81 j=1,jdim
         tmp(j,k,n) = 0.0
 81   continue

c---- last differentiation and add in dissipation ----
      dtd = dt / (1. + phidt)
      do 20 k = klow,kup
      do 20 j = jlow,jup
         jm1=jminus(j)
         tmp(j,k,n) = (work(j,k,3) - work(jm1,k,3))*dtd
 20   continue
c
 15   continue
c
cdu   precondition the dissipation operator
c
c      if (prec.gt.0)
c     >call predis(jdim,kdim,q,xyj,sndsp,precon,tmp)
c
      if (dissCon) then
         do 200 n=1,4
         do 200 k=klow,kup
         do 200 j=jlow,jup
           s0(j,k,n)=s0(j,k,n)+tmp(j,k,n)
           s(j,k,n)=s(j,k,n)+ tmp(j,k,n)
 200     continue
      else
         do 201 n=1,4
         do 201 k=klow,kup
         do 201 j=jlow,jup
           s(j,k,n)=s(j,k,n)+tmp(j,k,n)
 201     continue
      end if
c
      if (flbud .and.
     &   (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
         n=2
         do 210 k=klow,kup
         do 210 j=jlow,jup
            budget(j,k,2)=tmp(j,k,n)
 210     continue
      endif
c
      return
      end
