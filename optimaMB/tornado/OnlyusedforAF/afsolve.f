      subroutine afsolve(ifirst,q,qos,xy, xyj, x, y, turmu, vort, turre,
     &     turos,fmu, precon,press,  sndsp,xit, ett, ds, vk, ve, coef2x,
     &     coef4x,coef2y, coef4y, s, uu,vv, ccx, ccy, spectxi, specteta,
     &     tmp, q0, qp, pk, nmax) 

c     TTTTTTT  OOOO  RRRRR  N    N   AA   DDDD    OOOO
c        T    O    O R    R NN   N  A  A  D   D  O    O
c        T    O    O RRRRR  N N  N A    A D    D O    O
c        T    O    O R  R   N  N N AAAAAA D    D O    O
c        T    O    O R   R  N   NN A    A D   D  O    O
c        T     OOOO  R    R N    N A    A DDDD    OOOO
c
c
c     An Implicit Central-differenced Two-Dimensional
c     Multi-Block Navier-Stokes Code
c     T.E.Nelson & A.R.Wilkinson 1993
c     Modifications etc., by S. De Rango 1997-2000
c                            M. Nemec    2000-2002
c     
c     Loosely based on the code ARC2D
c     originally written by J. L. Steger (1976)
c     Modifications etc., by Barton  and Pulliam (1982-84)

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"
#include "../include/scratch.inc"
#include "../include/turb.inc"
#include "../include/trip.inc"

      integer msgtype,maxrestmp(3),fblk,cblk,junit
      logical fixtur,wflag
      dimension npts2(maxblk)

      dimension q(maxjkq),     press(maxjk),   sndsp(maxjk)
      dimension s(maxjkq),     xy(maxjkq),     xyj(maxjk)
      dimension xit(maxjk),    ett(maxjk),     ds(maxjk)
      dimension x(maxjk),      y(maxjk),       turmu(maxjk)
      dimension vort(maxjk),   turre(maxjk),   fmu(maxjk)
      dimension vk(maxjk),     ve(maxjk),      ccy(maxjk)
      dimension uu(maxjk),     vv(maxjk),      ccx(maxjk)
      dimension coef4x(maxjk), coef4y(maxjk),  qos(maxjkq)
      dimension coef2x(maxjk), coef2y(maxjk),  turos(maxjk)

c     -for multi-grid
      dimension q0(maxjkq),    qp(maxjkq),     pk(maxjkq)

c     next declaration is for matrix dissipation     
      dimension spectxi(maxjk*3),specteta(maxjk*3)
c     next declaration is for preconditioner
      dimension precon(maxjk*8),tmp(maxjkq)

c     -work space for high-order integration
      dimension cp(maxjfoil),cp2(maxjfoil),sn(maxjfoil)
      dimension xn(maxjfoil),yn(maxjfoil),x2(maxjfoil),y2(maxjfoil)
      common/wksp/cp,xn,yn,sn,cp2,x2,y2

      double precision dtiseq(-5:20),dtmins(-5:20),dtspiseq(-5:20)
      double precision vlx(-5:20),vnx(-5:20),vly(-5:20),vny(-5:20)
      integer iends(-5:20),jacdtseq(-5:20),isbiter(-5:20,2)      
      integer isequal
      common/mesup/ dtiseq, dtmins,dtspiseq, vlx, vnx, vly, vny, 
     &        isequal, iends, jacdtseq, isbiter

      its    = 0
      istart = 0
      isteps = 0
      junit  = 0

      fixtur=.false.

c     !!!!!!! mn: no multigrid, should be passed in !!!!!!!!!!!!
      mglev=1

      jdimen  = maxj
      kdimen  = maxk
c
c     - initialize variables
      call initia(mglev,ifirst,jdimen,kdimen,q,qos,press,sndsp,s,
     &     xy,xyj,xit,ett,ds,x,y,turmu,fmu,vort,turre,turos,vk,ve,
     &     coef2x,coef4x,coef2y,coef4y,precon,nmax)

c      write(*,*) 'done initia'

c     -- detection of negative Jacobian for optimization runs --
      if (badjac) return     

      istart=istart+1
c
cmnc     -find number of elements and defining block/side combo's (segments)
cmn      nblkstart = 1 
cmn      nblkend   = nblks

c     -- the call to autostan is required here for optimization runs,
c     since the values of xcoord and ycoord need to be updated --
      if (ifirst .eq. 1) then
         wflag = .true.
      else
         wflag = .false.
      end if
      if (.not.mg .and. .not.gseq) then
         if (iturb.eq.3 .and. turbulnt) then
           call autostan( 1, isequal, jdimen, kdimen, x, y, d, d(1,2),
     $           wflag)
c      write(*,*) 'done AUTOSTAN'
         end if
      end if

c     -calculate grid aspect ratios
      call asprat(x,y,wflag)
      if (mg .or. gseq) then
        nbs=nblkstot
      else
        nbs=nblks
      endif

      if(isequal.ne.0)then

      if (mg .or. gseq) then
        lev_start=mglev
        lev_end=mglev-isequal+1
        lev_skip=-1
        if (mg) then
c         -for cases where mulitgrid runs are started on fine level
c          instead of coarsest (i.e. not full multigrid), initialize q0
c         -otherwise you get a floating point error upon restriction 
c          from level 1 to level 2 in calcps in halo rows/columns.
          do n = 1,nblkstot
             call copy_array(jmax(n), kmax(n), 4, q(lqptr(n)),
     &            q0(lqptr(n)))
          enddo
        endif
      else
        lev_start=1
        lev_end=isequal
        lev_skip=1
      endif
c
      do 100 nn = lev_start,lev_end,lev_skip
c
c       -set up range of blocks to loop through 
c        for grid sequencing and eventually multi-grid
        iseqlev = nn
c
        if (iseqlev.lt.1) iseqlev=1
c
        if (mg .or. gseq) then
          call set_level(iseqlev)
        else
          call set_level(1)
c          nblkstart = 1 
c          nblkend   = nblks
        endif
c
        ip = 26
        if (autowrite) 
     &      call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &        sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &        coef2x,coef4x,coef2y,coef4y)
c
        istart = istart + isteps
c
c       -initialize pressure and sound speed
        do 150 ib=nblkstart,nblkend
          call calcps(ib,jmax(ib),kmax(ib),q(lqptr(ib)),
     &          press(lgptr(ib)),sndsp(lgptr(ib)),
     &          precon(lprecptr(ib)),xy(lqptr(ib)),xyj(lgptr(ib)),
     &          jbegin(ib)-nsd2(ib),jend(ib)+nsd4(ib),
     &          kbegin(ib)-nsd1(ib),kend(ib)+nsd3(ib))
 150    continue
c     
        if ((mg .or. gseq) .and. nn.gt.0) then
c         -sortfoils and autostan need to be called within this loop
c          (for grid-sequencing or multigrid only) to update arrays
c          like isegblk and to relocate transition points.
c         -sortfoils must be done before clcd.f
          call sortfoils(jdimen,kdimen,x,y)
          if (turbulnt .and. iturb.eq.3) then
             call autostan(iseqlev,mglev,jdimen,kdimen,x,y,d,d(1,2),
     $            .false.)
          endif
c
          if (fixtur) then
            do ib=nblkstart,nblkend
              call fixturre(ib,jmax(ib),kmax(ib),turre(lgptr(ib)))
            enddo
          endif
        endif
c
c       Compute Forces (important for restart conditions)
       
        call clcdmb(q,press,x,y,xy,xyj,.true.)
        
c       -set number of iterations each grid, reset dt etc.
        isteps   = iends(nn)
        dt       = dtiseq(nn)
        dtmin    = dtmins(nn)
        dtsp     = dtspiseq(nn)
        iend     = istart + isteps - 1
        jacdt    = jacdtseq(nn)
        vlxi     = vlx(nn)
        vnxi     = vnx(nn)
        vleta    = vly(nn)
        vneta    = vny(nn)
        itot     = 0
        itot2    = 0
        itot3    = 0
        if (wflag) then 
           write(n_out,*)
           write(n_out,*)
           write(n_out,157)
           if (mg .or. gseq) then
              write(n_out,156) iseqlev
           else
              write(n_out,155) iseqlev
           endif
           write(n_out,157)
           write(n_out,158)
           write(n_out,159)
        end if
 155    format(' |           Mesh Refinement Number ',i2,
     &        '                |')
 156    format(2h |,12x,'Mesh Refinement for Level',i2,13x,1h|)
 157    format(1x,54('-'))
 158    format(1x,1h|,52x,1h|)
 159    format(' |  Grid Sizes:',39x,1h|)
        do 160 iblk=nblkstart,nblkend
          if (wflag) write(n_out,161) iblk,jbmax(iblk), kbmax(iblk)
          inode=jbmax(iblk)*kbmax(iblk)
          itot =itot + inode
          itot2=itot2+ (jbmax(iblk)+2*nhalo)*(kbmax(iblk)+2*nhalo)
          itot3=itot3+ inode
c
c         -avoid adding contribution from common nodes along
c          block boundaries
          do 164 nside=1,4
            nbor =lblkpt(iblk,nside)
c           -don't nead to worry if nbor=0, which indicates
c            this side points to a geometry boundary and not
c            another block ... since nblkstart is always greater
c            than 0, next if statement takes care of if(nbor.ne.0)
            if (nbor.gt.iblk) then
c               -if neighbor blk number is greater then current
c                we haven't taken this particular side into
c                account yet.
                nside2=lsidept(iblk,nside)
                if (nside2.eq.1 .or. nside2.eq.3) then
                  itot3=itot3 -jbmax(iblk)
                else
                  itot3=itot3 -kbmax(iblk)
                endif
              endif
 164      continue
 161      format(' |    block #',i3,' :     ',i4,'  x  ',i4,
     &          '                  |')
 160    continue
        if (wflag) then
           write(n_out,*)
     &          '|    total nodes:',itot,'                        |' 
           write(n_out,*)
     &          '|  with halo pts:',itot2,'                        |'
           write(n_out,*)
     &          '|  sub. blk. bnd:',itot3,'                        |' 
           write(n_out,158)
           write(n_out,162) dtiseq(iseqlev),dtmins(iseqlev)
 162       format(' |  dt = ',f10.6,'    with  min dt = ',f10.6
     $          ,'      |')
           write(n_out,163) iends(iseqlev)
 163       format(' |  for  ',i5,
     &          '  time steps                            |')
           write(n_out,158)
           write(n_out,157)
           write(n_out,*)
        end if

c       ***************************************************************
c       ***************************************************************

c     -initialize variable dt
        if (mg .or. gseq) then
           ibs=1
           ibe=nbs
        else
c     -this loop might only require ib=1,nblks ...
           ibs=nblkstart
           ibe=nblkend
        endif
        do ib=ibs,ibe
           call varidt(ib,jacdt,jmax(ib),kmax(ib),q(lqptr(ib)),
     &          sndsp(lgptr(ib)),xy(lqptr(ib)),xyj(lgptr(ib)),
     &          ds(lgptr(ib)),precon(lprecptr(ib)),
     &          jbegin(ib),jend(ib),kbegin(ib),kend(ib))
        end do

c       *********************************************************
c       *********************************************************

        if(mg) then
          lev_bot = mglev
          lev_top = iseqlev
        else
          lev_bot = iseqlev
          lev_top = iseqlev
        end if

        totime1 = 0.0
        totime2 = 0.0

        do 10 numiter = istart, iend

#ifndef YMP
#ifdef IBM
            tt=etime_(tarray)
#else
            tt=etime(tarray)
#endif
            time1=tarray(1)
            time2=tarray(2)
#endif
c     
c     -update alpha, if needed for ramp change in alpha on restart
            if (restart .and. (targetalpha.ne.alpha)) then
               if (numiter-istart.ge.iramp) then
                  alpha = targetalpha
                  cosang = cos(pi*alpha/180.0)
                  sinang = sin(pi*alpha/180.0) 
                  uinf   = fsmach*cosang    
                  vinf   = fsmach*sinang 
                  write(n_out,*) ' finished ramp,  alpha=',alpha
               else
                  alpha = alptem + (numiter-istart)
     &                 *(targetalpha-alptem)/float(iramp)
                  cosang = cos(pi*alpha/180.0)
                  sinang = sin(pi*alpha/180.0) 
                  uinf   = fsmach*cosang    
                  vinf   = fsmach*sinang 
                  write(n_out,*) ' ramping,  alpha=',alpha
               end if
c     call flush(n_out)
            end if

 777        if (mg) then

c             -initializing q0 for multigrid runs and reset pk
c              q0(i)=q(i)
              do n = nblkstart,nblkend
                 call copy_array(jmax(n), kmax(n), 4, q(lqptr(n)),
     &                q0(lqptr(n)))
              enddo
c
              do n = 1,nblkstot
                call resets(jmax(n),kmax(n),pk(lqptr(n)))
              enddo
c
              call multigrd(iseqlev,mglev,lev_top,lev_bot,npts2,
     &              jdimen,kdimen,isbiter,
     &              q,q0,qp,pk,s,press,
     &              sndsp,turmu,fmu,vort,turre,vk,ve,
     &              x,y,xy,xyj,xit,ett,ds,
     &              uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &              spectxi,specteta,precon,tmp)
c
c             -reseting q array
c              q(i)=qp(i)
              do n = nblkstart,nblkend
                 call copy_array(jmax(n), kmax(n), 4, qp(lqptr(n)),
     &                q(lqptr(n)))
              enddo
c
            else
               call integrat(iseqlev, lev_top, mglev, npts2, jdimen,
     &              kdimen, q, pk, s, press, sndsp, turmu, fmu, vort,
     &              turre, vk, ve, x, y, xy, xyj, xit, ett, ds, uu, vv,
     &              ccx, ccy, coef2x, coef2y, coef4x, coef4y, spectxi,
     &              specteta, precon, tmp)
            endif

 445    format(1x,'Integration of forces on last iteration (',i5,
     & ') performed by clcdho2.f')

        call clcdmb(q,press,x,y,xy,xyj,.false.)
c
#ifndef YMP
#ifdef IBM
            tt=etime_ (tarray)
#else
            tt=etime  (tarray)
#endif
            time1=tarray(1)-time1
            time2=tarray(2)-time2
            totime1=totime1 + time1
            totime2=totime2 + time1 + time2
#endif

c         ***********************************************************
c         ***********************************************************
c         -output for residual history file
          ip = 19
          call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &          sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &          coef2x,coef4x,coef2y,coef4y)
c
          if (turbulnt .and. iturb.eq.3) then
            ip = 20
            call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &            sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &            coef2x,coef4x,coef2y,coef4y)
          endif
c
c         ***********************************************************
c         ***********************************************************
c         -save restart file every nq iterations
          if (mod(numiter-istart+1,nq).eq.0) then

c           -output cfl number to tape 6
            cflmax = 0.
            cflmin = 999999999.
            do 400 ib=nblkstart,nblkend
              call eigcfl(jmax(ib),kmax(ib),
     &              q(lqptr(ib)),sndsp(lgptr(ib)),
     &              xy(lqptr(ib)),ds(lgptr(ib)),ib,
     &              jbegin(ib),jend(ib),jlow(ib),jup(ib),
     &              kbegin(ib),kend(ib),klow(ib),kup(ib),
     &              cflmax,jcflmax,kcflmax,iblkcflmax,
     &              cflmin,jcflmin,kcflmin,iblkcflmin)
 400        continue

            if (wflag) then
               write(n_out,*)
     &              '---numiter=', numiter,
     &              '-------------------------------'
               write(n_out,*)
     &         '|                                                    |'
               write(n_out,401) cflmax
               write(n_out,402) iblkcflmax,jcflmax,kcflmax
 401           format(' |    max cfl  = ',f11.6,
     &              '                          |')
 402           format(' |       at block = ',i2,',  j =',i4,
     &              ',  k =',i4,'            |')
               write(n_out,403) cflmin
               write(n_out,404) iblkcflmin,jcflmin,kcflmin
 403           format(' |    min cfl  = ',f11.6,
     &              '                          |')
 404           format(' |       at block = ',i2,',  j =',i4,
     &              ',  k =',i4,'            |')
               write(n_out,*)
     &         '|                                                    |'
               write(n_out,*)
     &         '------------------------------------------------------'
            end if

c           -write solution to file
            ip = 10
            call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &            sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &            coef2x,coef4x,coef2y,coef4y)
c           -- store force and moment coefficients
            ip = 12
            junit=iseqlev
            call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &            sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &            coef2x,coef4x,coef2y,coef4y)
            do jj=1,6
              backspace(n_ld)
            enddo
c           -- store elemental force and moment coefficients --
            ip = 11
            call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &            sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &            coef2x,coef4x,coef2y,coef4y)
            backspace(n_eld)    ! only active when ioptm=2
            ip = 14
            if(viscous)
     &      call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &            sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &            coef2x,coef4x,coef2y,coef4y)
            ip = 23
            if(viscous.and.turbulnt)
     &      call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &            sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &            coef2x,coef4x,coef2y,coef4y)
            ip = 24
            if(writedisp)
     &      call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &            sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &            coef2x,coef4x,coef2y,coef4y)

          endif     ! if mod(numiter-istart+1,nq).eq.0
          if (mod(numiter-istart+1,ncp).eq.0) then
c           -write out formatted cp data to tape 31
            ip = 6
            call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &            sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &            coef2x,coef4x,coef2y,coef4y)
          endif
c
c
cmn          totime1=totime1 + time1
cmn          totime2=totime2 + time1 + time2
          if (timing) then
            if (numiter.eq.istart) then
c             -avoid contribution from overhead in first iteration
              totime1=0.
              totime2=0.
            endif
            write(n_time,444) totime1,resid,clt,cdt,totime2
            call flushit(n_time)
          endif
          call flushit(19)
          call flushit(18)
 444      format(5e15.6)

          if (resid .lt. afres) then
            if (mg.or.gseq) then
              if (numiter-istart+1.gt.25 .and. iseqlev.eq.1) then
                write(n_out,446)
                goto 101
              endif
            else
              if (numiter-istart+1.gt.25 .and. iseqlev.eq.isequal) then
                write(n_out,446)
                goto 101
              endif
            endif
          endif
 446      format(3x,'AF residual below min. resid., bail out')
c       -end loop on iterations
 10     continue
        numiter=numiter-1
c
c       -final output of forces for this grid
        ip = 12
        junit=iseqlev
        call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &        sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &        coef2x,coef4x,coef2y,coef4y)
c       -- store elemental force and moment coefficients --
        ip = 11
        call ioall(junit,mglev,ip,jdimen,kdimen,q,qos,press,
     &        sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &        coef2x,coef4x,coef2y,coef4y)
        backspace(n_eld) ! active when ioptm=2
c
c
        if ((mg.or.gseq) .and. iseqlev.ne.1 .and. iend.ne.0) then
c
c         -for grid sequencing or full multi-grid start-up
          fixtur=.true.
          do ii = nblkstart,nblkend
            fblk = ii - nblks
            cblk = ii
            call prolong (fblk,cblk,
     &            jmax(fblk),kmax(fblk), 
     &            q(lqptr(fblk)),xyj(lgptr(fblk)),turre(lgptr(fblk)),
     &            jmax(cblk),kmax(cblk),
     &            q(lqptr(cblk)),xyj(lgptr(cblk)),turre(lgptr(cblk)))
c
            if (turbulnt.and.iturb.lt.4) then
              call prlng_turre(fblk,cblk,
     &                         jmax(fblk),kmax(fblk),turre(lgptr(fblk)),
     &                         jmax(cblk),kmax(cblk),turre(lgptr(cblk)))
            endif
          enddo
c
c         -transferring to finer level:
          call set_level(iseqlev-1)
c
          call mg_bcupdate(iseqlev-1,mglev,
     &          npts2,jdimen,kdimen,q,pk,s,press,
     &          sndsp,turmu,fmu,vort,turre,vk,ve,
     &          x,y,xy,xyj,xit,ett,ds,
     &          uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &          spectxi,specteta,precon,tmp)
c
        endif
c
c
        its=its+(numiter-istart+1)
c     -end loop on sequences
 100  continue
 101  continue
c
      its=its-1 ! I don't include time from first iteration to
c                 avoid counting overhead ... so subtract 1 iteration
      if (timing .and. its.ne.0) then
c       -variable its=0 if you only perform one iteration
        write(n_time,443) totime1/float(its)
 443    format('#Average time per iteration=',f10.6)
      endif

c     -- write out formatted cp data --
      ip = 6
      call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,
     &     sndsp, turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &     coef2x, coef4x, coef2y, coef4y)

c     -when mg=true or grid-sequencing, must add 1 to level counter
c      to integrat appropriate blocks in clcd.
      iseqlev=iseqlev+1
c     *****************************************************************
c
      else
c
c       -no iterations of flow solver performed
        write(n_out,*) "???????????????????????????????????????????????"
        write(n_out,*) "???????????????????????????????????????????????"
        write(n_out,*) "? isequal = 0 so that no iterations of        ?"
        write(n_out,*) "? flow solver will be performed for this pass ?"
        write(n_out,*) "???????????????????????????????????????????????"
        write(n_out,*) "???????????????????????????????????????????????"
      endif

      return
      end                       !tornado
