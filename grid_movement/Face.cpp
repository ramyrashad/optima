#ifndef FACE_H
#include "Face.h"
#endif


// Definition of default constructor
Face::Face()
{
	ID = 0;
	vertexID = NULL;
	nvertices = 0;
}


Face:: Face(Node** corners, const int& faceID, const int& vertices)
{
  ID = faceID;
  nvertices = vertices;

  vertexID = new int[nvertices];


  /*         2
             $
           $  $     
     c   $     $     b
       $         &   
     $             $
    $$$$$$$$$$$$$$$$$
   0       a         1                   */

  calc_area(corners);
  

  return; 
}

void Face::calc_area(Node** corners)
{
  double* xyz = NULL;

  vector<vector<double*> > vertices (3, vector<double*>(2));  
  for (int i=0; i<3; i++)
    {
      xyz = corners[i]->xyz;
      vertices[i][0] = &xyz[0];    vertices[i][1] = &xyz[1];
      
      vertexID[i] = corners[i]->ID;
     
    }

  area = 0.5*((*vertices[0][0] - *vertices[2][0])*
	      (*vertices[1][1] - *vertices[2][1]) - 
	      (*vertices[1][0] - *vertices[2][0])*
	      (*vertices[0][1] - *vertices[2][1]));

  if (area < 0)
    {
      xyz = corners[2]->xyz;
      vertices[1][0] = &xyz[0];    vertices[1][1] = &xyz[1];
      xyz = corners[1]->xyz;
      vertices[2][0] = &xyz[0];    vertices[2][1] = &xyz[1];

      area = 0.5*((*vertices[0][0] - *vertices[2][0])*
		  (*vertices[1][1] - *vertices[2][1]) - 
		  (*vertices[1][0] - *vertices[2][0])*
		  (*vertices[0][1] - *vertices[2][1]));
 
      vertexID[1] = corners[2]->ID;
      vertexID[2] = corners[1]->ID;

      // cout << "in Face negative " << ID << endl;
    }

  xyz = NULL;

  return;
} 


Face:: Face(Node** corners, const int& faceID)
{
  ID = faceID;
  nvertices = 4;
  vertexID = new int[nvertices];

  Node* overlapind;
  
  for (int i=0; i<nvertices; i++)
    {
      if (corners[i]->overlap == NULL)
	vertexID[i] = corners[i]->ID;
      else
	{
	  overlapind = corners[i]->overlap;
	  vertexID[i] = overlapind->get_ID();
	}

    }

  overlapind = NULL;

  return; 
}


// Destructor - takes no action
Face::~Face() 
{
}


void Face::add_nei(Node** corners)
{

  if (nvertices == 3)  // triangle
    {
  corners[0]->add_nei(*corners[1]);
  corners[0]->add_nei(*corners[2]);

  corners[1]->add_nei(*corners[0]);
  corners[1]->add_nei(*corners[2]);

  corners[2]->add_nei(*corners[1]);
  corners[2]->add_nei(*corners[0]);
    }
  else if (nvertices == 4)
    {
  // add opposite corner nodes of face to be neighbours
  // use this info later to build global stiffness matrix
  corners[0]->add_nei(*corners[1]);
  corners[0]->add_nei(*corners[2]);
  corners[0]->add_nei(*corners[3]);

  corners[1]->add_nei(*corners[0]);
  corners[1]->add_nei(*corners[2]);
  corners[1]->add_nei(*corners[3]);

  corners[2]->add_nei(*corners[0]);
  corners[2]->add_nei(*corners[1]);
  corners[2]->add_nei(*corners[3]);

  corners[3]->add_nei(*corners[0]);
  corners[3]->add_nei(*corners[1]);
  corners[3]->add_nei(*corners[2]);

//   cout << corners[0]->num_nei << endl;  
//   Node** nei = corners[0]->get_nei() ;
//   for (int i=0; i<corners[0]->num_nei; i++)
//     cout << nei[i]->get_ID() << endl;
//   exit(9) ;
    }
  else
    {
      cerr << "Unknown element.. " << endl;
      exit(2);
    }

  return;
} 


void Face::clearmem()
{
  delete [] vertexID;   vertexID = NULL;
}


ostream& operator <<(ostream& out, const Face& a)
{
	out.setf(ios::showpoint);
	out.setf(ios::fixed);
	out.setf(ios::left);
	int nv = a.get_nvertices();
	int* vertex_ID = a.get_vertexID();
	out << setw(20) << a.get_ID();
	for (int i=0; i<nv; i++)
		out << setw(20) << vertex_ID[i];
	out << endl;

	vertex_ID = NULL;
	return out;
}
