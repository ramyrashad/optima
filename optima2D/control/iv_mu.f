c----------------------------------------------------------------------
c     -- differentiation of the lam. and turb. viscosities w.r.t. Q --
c     -- written by: marian nemec --
c     -- heavily modified by markus rumpfkeil --
c     -- date: jan. 2006
c----------------------------------------------------------------------

      subroutine iv_mu (jdim, kdim, ndim, xyj, q, fmu, turmu,
     &      press, sndsp, dmul, dmut, chi, dchi, fv1,dfv1)

      implicit none

#include "../include/arcom.inc"
#include "../include/sam.inc"
#include "../include/visc.inc"

      integer jdim, kdim, ndim,j,k,n

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision fmu(jdim,kdim), turmu(jdim,kdim), xyj(jdim,kdim)
      double precision press(jdim,kdim), sndsp(jdim,kdim)
                        
      double precision dmul(ndim,jdim,kdim),dmut(5,jdim,kdim)
      double precision fv1(jdim,kdim), dfv1(jdim,kdim)
      double precision chi(jdim,kdim), dchi(5,jdim,kdim) 

      double precision hre,g1,g2,hfmu,t0,t1,t2,t3,t4,t5,dmuda,tnu,chi3
      double precision dadq1,dadq2,dadq3,dadq4
      double precision c2b,c2bp,rho

c     prlam   =  laminar prandtl number  = .72                      
c     prturb  =  turbulent prandtl number = .90                     
c     prlinv  =  1./(laminar prandtl number)                        
c     prtinv  =  1./(turbulent prandtl number)                      
c     f13     =  1/3                                                
c     f43     =  4/3                                                
c     hre     =  1/2 * reynolds number                           
c     fmu     =  laminar viscosity

      hre = 1.d0/(2.d0*re)
      g2  = gamma*gami

      do k = kbegin,kend
        do j = jbegin,jend  
          do n = 1,ndim
            dmut(n,j,k) = 0.d0
          end do
        end do
      end do

c     -- compute laminar d(fmu)/dQ vector --
      c2b = 198.6d0/tinf                                                  
      c2bp = c2b + 1.d0
      do k = kbegin,kend
        do j = jbegin,jend

          t1 = sndsp(j,k)*sndsp(j,k)
          t2 = t1 + 3.d0*c2b
          t3 = 1.d0/(t1 + c2b)
          dmuda = c2bp*t1*t2*t3*t3

          t1 = 0.5d0/sndsp(j,k)
          t2 = q(j,k,2)*q(j,k,2)
          t3 = q(j,k,3)*q(j,k,3)
          t4 = 1.d0/q(j,k,1)
          t5 = t4*t4
          dadq1 = g2*t5*( (t2+t3)*t4 - q(j,k,4) )
          dadq2 = - g2*q(j,k,2)*t5
          dadq3 = - g2*q(j,k,3)*t5
          dadq4 = g2*t4

          dmul(1,j,k) = dmuda*dadq1*t1
          dmul(2,j,k) = dmuda*dadq2*t1
          dmul(3,j,k) = dmuda*dadq3*t1
          dmul(4,j,k) = dmuda*dadq4*t1

        end do
      end do

      if (turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kend
          do j = jbegin,jend
            dmul(5,j,k) = 0.d0
          end do
        end do
      end if

c     -- compute turbulent d(mut)/dQ vector --
      if (turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kend
          do j = jlow,jup

            tnu = q(j,k,5)*xyj(j,k)
            rho = q(j,k,1)*xyj(j,k)
            t0 = rho*tnu
c     -- t1 = 1/mul --
            t1 = 1.d0/fmu(j,k)
c     -- chi = rho*tnu/fmu  --
            chi(j,k) = t0*t1
c     -- chi3 = chi**3 --
            chi3 = chi(j,k)**3
c     -- t2 = 1/(chi**3+cv1**3) --
            t2 = 1.d0/(chi3 + cv1_3)
            fv1(j,k) = chi3*t2
            dfv1(j,k) = 3.d0*cv1_3*chi(j,k)*chi(j,k)*t2*t2

c     -- d(chi)/d(Qm) --
            dchi(1,j,k) = -t0*t1*t1*dmul(1,j,k) + tnu*t1*xyj(j,k)
            dchi(2,j,k) = -t0*t1*t1*dmul(2,j,k)
            dchi(3,j,k) = -t0*t1*t1*dmul(3,j,k)
            dchi(4,j,k) = -t0*t1*t1*dmul(4,j,k)
            dchi(5,j,k) =  rho*t1*xyj(j,k)

            dmut(1,j,k) = t0*dfv1(j,k)*dchi(1,j,k) + fv1(j,k)*tnu
     &            *xyj(j,k)
            dmut(2,j,k) = t0*dfv1(j,k)*dchi(2,j,k)
            dmut(3,j,k) = t0*dfv1(j,k)*dchi(3,j,k)
            dmut(4,j,k) = t0*dfv1(j,k)*dchi(4,j,k)
            dmut(5,j,k) = t0*dfv1(j,k)*dchi(5,j,k) + fv1(j,k)*rho
     &            *xyj(j,k)
          end do
        end do
      end if


      return                                                            
      end                       !iv_mu
