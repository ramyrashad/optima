c ##########################
c ##                      ##
c ##  subroutine maty     ##
c ##                      ##
c ##########################
c
      subroutine maty (jdim,kdim,q,s,xy,xyj,bb,sndsp,vv,ccy,
     >                              spect,coef2,coef4,work,tmp,tmp2)
c
c********************************************************************
c                                                                       
c      fourth difference smoothing, added explicitly to rhs                  
c      second difference near shocks with pressure grd coeff.           
c                                                                       
c                     eta direction
c                                                                       
c********************************************************************
c
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)
      dimension work(jdim,kdim,4),vv(jdim,kdim),ccy(jdim,kdim)
      dimension tmp(4,jdim,kdim),tmp2(4,jdim,kdim),bb(4,4,jdim,kdim)
      dimension vec(4),xy(jdim,kdim,4),spect(jdim,kdim,3)
c
      do 39 n = 1,4                                                     
c----    1st-order forward difference ----
         do 35 k = kbegin,kup                                              
         kpl = k+1
         do 35 j = jlow,jup                                                
c            tmp(n,j,k) = q(j,kpl,n)*xyj(j,kpl) - q(j,k,n)*xyj(j,k)    
            tmp(n,j,k) = - q(j,k,n)*xyj(j,k) + q(j,kpl,n)*xyj(j,kpl)       
 35      continue                                                          
c        
c        
c----    for fourth order: apply cent-dif to 1st-order forward ----
         do 36 k = klow,kup-1                                              
         kpl = k+1                                                         
         kmi = k-1                                                         
         do 36 j = jlow,jup                                                
            tmp2(n,j,k) = tmp(n,j,kmi) - 2.d0* tmp(n,j,k) + tmp(n,j,kpl)     
 36      continue                                                          
c  --    next points to boundaries: rows 2 & kmax-1 --
         kb  = kbegin
         kbp = kb + 1
         kbp2= kb + 2
         do 37 j = jlow,jup                                                
         tmp2(n,j,kbegin) = q(j,kb,n)*xyj(j,kb) -                  
     >              2.d0*q(j,kbp,n)*xyj(j,kbp) + q(j,kbp2,n)*xyj(j,kbp2)
         tmp2(n,j,kup) = tmp(n,j,kup-1) - tmp(n,j,kup)                   
 37      continue                                                          
 39   continue                                                          
c                                                                       
c
c
      gm1i=1.d0/gami
      do 19 k = kbegin,kend                                            
      do 19 j = jbegin,jend       
c        -need to compute  J^{-1} * T * |eigenvalue matrix| * T^{-1}
c
c        -compute magnitude of eigenvalues
c         rj =1.d0/xyj(j,k)
c         vec(1)=abs(vv(j,k))*rj
c         vec(2)=vec(1)
c         vec(3)=abs(vv(j,k)+ccy(j,k))*rj
c         vec(4)=abs(vv(j,k)-ccy(j,k))*rj
c
         vec(1)=spect(j,k,1)
c         vec(2)=vec(1)
         vec(3)=spect(j,k,2)
         vec(4)=spect(j,k,3)
c
         v3p4= vec(3) + vec(4)
         v4m3= vec(4) - vec(3)
c
         rho=q(j,k,1)*xyj(j,k)
         rhoinv=1.d0/q(j,k,1)
         u=q(j,k,2)*rhoinv
         v=q(j,k,3)*rhoinv
         rhoinv=1.d0/rho
c         bt=1.d0/sqrt(2.d0)
         vtot2=0.5d0*(u**2+v**2)
         c=sndsp(j,k)
         snr=1.d0/c
         snr2=snr**2
c         beta=bt*rhoinv*snr
         phi2=vtot2*gami
         phia2=(phi2+c**2)*gm1i
c         alp=bt*rho*snr
         abeta=.5d0*snr2
c
         rx=xy(j,k,3)
         ry=xy(j,k,4)
         rxy2=1.d0/sqrt(rx**2+ry**2)
         rx=rx*rxy2
         ry=ry*rxy2
         ryu=ry*u
         rxu=rx*u
         ryv=ry*v
         rxv=rx*v
         rxa=rx*c
         rya=ry*c
         theta=rxu+ryv
         atheta=c*theta
         theta2=ryu-rxv
c
         upx=(u+rxa)*vec(3)
         umx=(u-rxa)*vec(4)
         vpy=(v+rya)*vec(3)
         vmy=(v-rya)*vec(4)
c
         uv34m=upx - umx
         uv34p=upx + umx
         vv34m=vpy - vmy
         vv34p=vpy + vmy
c
         pav3=(phia2+atheta)*vec(3)
         pav4=(phia2-atheta)*vec(4)
c
         zz=1.d0-phi2*snr2
         bb(1,1,j,k)=vec(1)*zz + abeta*phi2*v3p4 + atheta*v4m3
         bb(2,1,j,k)=vec(1)*(zz*u - ry*theta2) + abeta*(
     &                       phi2*uv34p - atheta*uv34m)
         bb(3,1,j,k)=vec(1)*(zz*v + rx*theta2) + abeta*(
     &                       phi2*vv34p - atheta*vv34m)
         bb(4,1,j,k)=vec(1)*(zz*gm1i*phi2-theta2**2) + abeta*(
     &                       pav3*(phi2-atheta) + pav4*(phi2+atheta))
c
         yy=gami*u
         zz=yy*snr2
         bb(1,2,j,k)=vec(1)*zz - abeta*(rxa*v4m3 + yy*v3p4)
         bb(2,2,j,k)=vec(1)*(zz*u + ry**2) + abeta*(
     &                       rxa*uv34m - yy*uv34p)
         bb(3,2,j,k)=vec(1)*(zz*v - rx*ry) + abeta*(
     &                       rxa*vv34m - yy*vv34p)
         bb(4,2,j,k)=vec(1)*(zz*gm1i*phi2 + ry*theta2) + abeta*(
     &                       pav3*(rxa-yy) - pav4*(rxa+yy))   
c                 
         yy=gami*v
         zz=yy*snr2
         bb(1,3,j,k)=vec(1)*zz - abeta*(rya*v4m3 + yy*v3p4)       
         bb(2,3,j,k)=vec(1)*(zz*u - ry*rx) + abeta*(              
     &                       rya*uv34m - yy*uv34p)             
         bb(3,3,j,k)=vec(1)*(zz*v + rx**2) + abeta*(
     &                       rya*vv34m - yy*vv34p)
         bb(4,3,j,k)=vec(1)*(zz*gm1i*phi2 - rx*theta2) + abeta*(
     &                       pav3*(rya-yy) - pav4*(rya+yy))   
c
c         yy=abeta*gami
         zz=-vec(1)*snr2
c         gam(1,4,j,k)=-vec(1)*zz + yy*v3p4
c         gam(2,4,j,k)=-vec(1)*zz*u + yy*uv34p
c         gam(3,4,j,k)=-vec(1)*zz*v + yy*vv34p
c         gam(4,4,j,k)=-vec(1)*phi2*snr2 + yy*(pav3 + pav4)
         bb(1,4,j,k)=gami*(zz + abeta*v3p4)
         bb(2,4,j,k)=gami*(zz*u + abeta*uv34p)
         bb(3,4,j,k)=gami*(zz*v + abeta*vv34p)
         bb(4,4,j,k)=zz*phi2 + abeta*gami*(pav3 + pav4)
c
 19   continue
c
c     -See note in matx.f 
      if (dis2y.gt.0.) then
         do 50 k=kbegin,kup
            kk=k+1
            do 49 j=jlow,jup
               do 25 n=1,4
                  sum=0.
                  do 20 i=1,4
                     sum=sum+(coef2(j,k)*bb(n,i,j,k)+
     &                            coef2(j,kk)*bb(n,i,j,kk))*tmp(i,j,k)
 20               continue
c              
                  do 21 i=1,4
                     sum=sum-(coef4(j,k)*bb(n,i,j,k)+
     &                           coef4(j,kk)*bb(n,i,j,kk))*tmp2(i,j,k)
 21               continue
                  work(j,k,n)=sum
 25            continue
 49         continue
 50      continue
      else
         do 150 k=kbegin,kup
            kk=k+1
            do 149 j=jlow,jup
               do 125 n=1,4
                  sum=0.
                  do 121 i=1,4
                     sum=sum-(coef4(j,k)*bb(n,i,j,k)+
     &                           coef4(j,kk)*bb(n,i,j,kk))*tmp2(i,j,k)
 121              continue
                  work(j,k,n)=sum
 125           continue
 149        continue
 150     continue
      endif
c
c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)                                           
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
           s(j,k,n)=s(j,k,n) + (work(j,k,n) - work(j,k-1,n))*dtd
 200  continue
      return                                                            
      end                                                               
