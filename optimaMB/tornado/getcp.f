c----------------------------------------------------------------------
c     -- calculate inverse design objective function per given block --
c     -- m. nemec, june 2001 --
c----------------------------------------------------------------------
c     calling routine: flow_solve
c
      subroutine getcp(ibn, jdim, kdim, jbegin, jend, kbegin, kend,
     &     ifoil, iside, press, xyj)

      implicit none
c
#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
c
      integer ibn,jdim,kdim,jbegin,jend,kbegin,kend,ifoil,iside,j,k
      double precision press(jdim,kdim),xyj(jdim,kdim),cpc,pp
c
      cpc = 2.0/(gamma*fsmach*fsmach)
c
c     write (*,*) 'getcp,iside',iside
c     write (*,*) 'jdim,kdim',jdim,kdim
c     write (*,*) 'jbegin,jend,kbegin,kend',jbegin,jend,kbegin,kend
c
c     side # 1
      if (iside.eq.1) then
         k = kbegin
         do j = jbegin,jend
            pp = press(j,k)*xyj(j,k)
            cpi(ibn,ifoil) = (pp*gamma -1.0)*cpc
c            write (*,*) ibn,iside,cpi(ibn,ifoil)
            ibn = ibn+1
         end do
c
c     side # 2
      elseif (iside.eq.2) then
         j = jbegin
         do k= kend, kbegin, -1
            pp = press(j,k)*xyj(j,k)
            cpi(ibn,ifoil) = (pp*gamma -1.0)*cpc
            ibn = ibn+1
         end do
c
c     side # 3
      elseif (iside.eq.3) then
         k = kend
         do j = jend,jbegin,-1
            pp = press(j,k)*xyj(j,k)
            cpi(ibn,ifoil) = (pp*gamma -1.0)*cpc
c            write (*,*) ibn,iside,cpi(ibn,ifoil)
            ibn = ibn+1
         end do
c
c     side # 4
      elseif (iside.eq.4) then
         j = jend
         do k = kbegin,kend
            pp = press(j,k)*xyj(j,k)
            cpi(ibn,ifoil) = (pp*gamma -1.0)*cpc
            ibn = ibn+1
         end do
      endif
c
c     That's all folks!
      return
      end                       !getcp
