      subroutine nk_preproc(jdim, kdim, ndim,qp, q, qold)
c
#include "../include/arcom.inc"
c
      double precision qp(jdim,kdim,ndim),q(jdim,kdim,ndim)
      double precision qold(jdim,kdim,ndim)

c     
      if (jesdirk.eq.3 .or. jesdirk.eq.4)then
            
            do 7 n=1,ndim
            do 7 k=kbegin,kend
            do 7 j=jbegin,jend
               qp(j,k,n)=q(j,k,n)
 7          continue

      else

         do 3 k=kbegin,kend
         do 3 j=jbegin,jend
         do 3 n=1,4
            qp(j,k,n)=2*q(j,k,n)-qold(j,k,n)
 3       continue

         if (ndim.eq.5) then
            do 9 k=kbegin,kend
            do 9 j=jbegin,jend               
               qp(j,k,5)=q(j,k,5)
 9          continue
         end if

      endif
      return
      end

