#ifndef SOLVERS_H
#include "solvers.h"
#endif

void jacobi(int*& rowptr, int*& colind, double*& val, double*& du,
	    double*& F, const int& nrows, double& res)
{  /*
             perform jacobi iteration on the system K*du = 0 
	input:  stiffness matrix K in compressed sparse row format 
                (rowptr, colind, val), the change in nodal coordinates
		(du), and the number of rows (nrows)
        output:  updated vector of nodal displacements (du)
                 norm of the residual (res)
      ___________________________________________________________

  */
  
  int   ia, ja;
  double sum, denom;

  for (ia=0; ia<nrows; ia++)
  {
     sum = 0.0;
     for (ja=rowptr[ia]; ja<rowptr[ia+1]; ja++)
     {
        if (colind[ja] == ia)      
          denom = val[ja];
        else
          sum += val[ja]*du[colind[ja]];
     }
     du[ia] = (F[ia]-sum)/denom;

  }
 

  // calculate the norm of the residual
  res = 0.0;
  for (ia=0; ia<nrows; ia++)
  {
     sum = 0.0;
     for (ja=rowptr[ia]; ja<rowptr[ia+1]; ja++)
     {
          sum += val[ja]*du[colind[ja]];
     }
     sum -= F[ia];
     res += sum*sum;
  }
  res = res/nrows;
  res = sqrt(res);

  return;
}


// preconditioned conjugate gradient method (ILU(0) preconditioning)
int pcg (int*& rowptr, int*& colind, double*& val, double* du,
	 double* F, const int& nrows, double& tol, int& maxit)
{

  int i, j;   // iteration counter

  double *r0  = new double[nrows];
  double *r1  = new double[nrows];
  double *p0  = new double[nrows];
  double *tmp = new double[nrows];
  int* uptr = new int[nrows];
  for (i=0; i<nrows; i++)   // initialize arrays
  {
    r0[i] = 0.; 
    r1[i] = 0.; 
    p0[i] = 0.; 
    tmp[i] = 0.;
    uptr[i] = 0;
  }
  double alpha(0.), beta(0.), norm0(0.), res(0.), rho(0.);
  //double alpha, beta, norm0, res, rho;
  
  // calculate the initial residual  r0 = F - Kdu
  matrix_times_vector (rowptr, colind, val, du, r0, nrows);
  for (i=0; i<nrows; i++)
  {
     r0[i] = F[i] - r0[i];
     p0[i] = r0[i];
  }

  // find the norm of r0 and store in alpha
  norm(r0, nrows, alpha);

  norm(F, nrows, norm0);

  if (norm0 == 0.)
    norm0 = 1.;

  
  if ( (res=alpha/norm0) < tol)
  {
     tol = res;
     maxit = 0;
     return 0;
  }

  double* LUval = new double[rowptr[nrows]];
  for (i=0; i<rowptr[nrows]; i++)
    LUval[i] = 0.;
  // Do ILU precondition on K, 
  // r1 is used as a working array
  i = ILU0(rowptr, colind, val, LUval, nrows, uptr);
  if (i != 0) 
    cerr << "Error in preconditioning in ILU0 " << endl; 
  
  beta = 1.;
  // begin CG iteration
  for (i=0; i<maxit; i++)
  {
     
     // tmp = Preconditioner * r0
     LUsol(nrows, tmp, r0, LUval, rowptr, colind, uptr);
    // rho = r0.tmp
     dot(r0, tmp, rho, nrows);

     if (i==0)
        for (j=0; j<nrows; j++)
           p0[j] = tmp[j];
     else
     {
        beta = rho/beta;  
        for (j=0; j<nrows; j++)
          p0[j] = tmp[j] + beta*p0[j];
     }
     
     // r1 = K * p0
     matrix_times_vector (rowptr, colind, val, p0, r1, nrows);
     // alpha = p0.r1
     dot(r1, p0, alpha, nrows);
     alpha = rho/alpha;

     for (j=0; j<nrows; j++)
      {
        du[j] += alpha*p0[j];
        r0[j] -= alpha*r1[j];
      }

     // check for convergence
     norm(r0, nrows, alpha);
     res=alpha/norm0;

     if ( res < tol )
     {
        tol = res;
        maxit = i;

	delete [] r0 ;        r0  = NULL;
	delete [] r1 ;        r1  = NULL;
	delete [] p0 ;        p0  = NULL;
	delete [] tmp;        tmp = NULL;
	delete [] LUval;      LUval = NULL;
	delete [] uptr;       uptr = NULL;

        return 0;
     }     

    beta = rho;

  } 
  tol = res;
  
  delete [] r0 ;	r0  = NULL;
  delete [] r1 ;	r1  = NULL;
  delete [] p0 ;	p0  = NULL;
  delete [] tmp;	tmp = NULL;
  delete [] LUval; 	LUval = NULL;
  delete [] uptr; 	uptr = NULL;

  return 1;
}


// calculates the dot product of 2 vectors
void dot(double*& vec1, double*& vec2, double& ans, const int& nrows)
{ 
    ans = 0.0;
    for (int j=0; j<nrows; j++)
        ans += vec1[j]*vec2[j];

  return;
}

// calculates the norm of a vector
void norm(double*& vec, const int& nrows, double& ans)
{
  ans = 0.0;
  for (int i=0; i<nrows; i++)
     ans += vec[i]*vec[i];
  
  ans = sqrt(ans/nrows);
  return;
}



void matrix_times_vector (int*& rowptr, int*& colind, double*& val, 
	double*& vec, double*& sol, const int& nrows)
{

  /* 
  Calculates the product of a matrix times a vector
  input:  matrix in compressed sparse row format (CSR) - rowptr, colind, val
  output: vector sol
  */

  int ia, ja;
  double sum;
 
  for (ia=0; ia<nrows; ia++)
  {
     sum = 0.0;
     for (ja=rowptr[ia]; ja<rowptr[ia+1]; ja++)
        sum += val[ja]*vec[colind[ja]];
     sol[ia] = sum;
  }


  return;
}


int ILU0 (int*& rowptr, int*& colind, double*& val, double*& LUval,
	   	const int& nrows, int*& uptr)
{

   /*
   ILU(0) conditioning of a matrix 
   input: matrix in compressed sparse row format (CSR) - rowptr, colind, val
   output: matrix in CSR format which has the same nonzero pattern but with
           values stored in LUval
           error code indicates whether preconditioning was successful
           (errcode=0)
	  

   Since the column indices are not in an ascending order, a reorganization 
   of the columns is necessary in this function since one of the loop exits 
   when a diagonal element is reached.

   let unpack_col store the reordered columns and unpack_val the nonzero values

   */

   int i, j, jj, jrow;
   int* tmp = new int[nrows];

   for (i=0; i<rowptr[nrows]; i++)
      LUval[i] = val[i];
   for (i=0; i<nrows; i++)
      tmp[i] = 0;  


   for (i=0; i<nrows; i++)   // main loop
   {

      for (j=rowptr[i]; j<rowptr[i+1]; j++)
         tmp[colind[j]] = j;
      
      j = rowptr[i];
      // exit if diagonal element is reached
      while (colind[j] < i && j < rowptr[i+1])
      {
         jrow = colind[j];
         // compute the multiplier for jth row
         LUval[j] = LUval[j]*LUval[uptr[jrow]];

   	 // perform linear combination
         for (jj = uptr[jrow]+1; jj<rowptr[jrow+1]; jj++)
            if (tmp[colind[jj]] != 0)
                LUval[tmp[colind[jj]]] -= LUval[j]*LUval[jj];
         j ++;
      } 

      uptr[i] = j;

      if (colind[j] != i || LUval[j] == 0.0)
         return 1;

      LUval[j] = 1.0/LUval[j];
      
      for (j=rowptr[i]; j<rowptr[i+1]; j++)
         tmp[colind[j]] = 0;
   }
  
  delete [] tmp;

  return 0;
}


void LUsol(const int& nrows, double*& vec, double*& rhs, double*& LUval,
           int*& rowptr, int*& colind, int*& uptr)
{
   // Performs a forward and a backward solve for an ILU factorization 	
   // Parameters
   // n = dimension of the square matrix
   // vec = vector to be multiplied with the matrix
   // rhs = right hand side, unchanged on return
   // LUval = values of the LU matrix.  L and U are stored together in 
   //         CSR format.  In each row, the L values are followed by the
   //         diagonal element (inverted) and then the other U values
   // rowptr = pointers to the beginning of each row
   // colind = column indices of corresponding elements in LUval
   // uptr = pointer to the diagonal elements in LUval, colind

   int i, k;
   
   // Forward solve:  L * vec = rhs
  
   for (i=0; i<nrows; i++)
   {
      // compute vec[i] = rhs[i] - sum L[i,j] * vec[j]
      vec[i] = rhs[i];
      for (k=rowptr[i]; k<uptr[i]; k++)
      {
         vec[i] = vec[i] - LUval[k] * vec[colind[k]];
      }
   }


   // Backward solve:  vec = inv(U) * vec
   
   for (i=nrows-1; i>=0; i--)
   {
      // compute vec[i] = vec[i] - sum U[i,j] * vec[j]
      for (k=uptr[i]+1; k<rowptr[i+1]; k++)
      {
         vec[i] = vec[i] - LUval[k]*vec[colind[k]];
      }
      // compute vec[i] = vec[i]/U[i,j]
      vec[i] = LUval[uptr[i]]*vec[i];
   }

   return;
}

