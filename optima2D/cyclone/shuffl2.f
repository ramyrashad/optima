       subroutine shuffl2(jdim,kdim,ndim,var,jtail1,jtail2,            
     *            jmax,kmax,fwd,nb0,nb1)
#include "../include/parm.inc"                                
      dimension var(jdim*kdim*ndim),d(maxj*maxk*4*5)                  
       integer pointer                                                  
       logical fwd                                                      
c                                                                       
c                the goal here is to write a "smart" shuffler (ha,ha)   
c                                                                       
c           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$       
c           $$$ step (1)    store data in temporary array $$$$$$$       
c           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$       
c                                                                       
       do 10 i = 1,jdim*kdim*ndim                                       
            d(i)   = var(i)                                             
10     continue                                                         
c                                                                       
c                                                                       
c           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     
c           $$$ step (2)  determine starting addresses and     $$$$     
c           $$$           limits for subblocks.. then map      $$$$     
c           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     
c                                                                       
c     %%%%%  start airfoil block %%%%%%                                 
c                                                                       
      pointer = 0                                                       
      nb0     = pointer+1                                               
      jj      = jtail1                                                  
      jjj     = jtail2                                                  
      jint    = 1                                                       
      kk      = 1                                                       
      kkk     = kmax                                                    
      kint    = 1                                                       
      do 15 n = 1,ndim                                                  
      call  map(jdim,kdim,ndim,var,d,pointer,                           
     *                         jj,jjj,jint,kk,kkk,kint,n,fwd)           
15    continue                                                          
c                                                                       
c   pointer for begining of next array                                  
      nb1 = pointer+1                                                   
c                                                                       
c     &&&&&  start wake block  &&&&&&                                   
c                                                                       
      do 20 n = 1,ndim                                                  
         jj     = jtail1-1                                              
         jjj    = 1                                                     
         jint   = -1                                                    
         kk     = kmax                                                  
         kkk    = 2                                                     
         kint   = -1                                                    
         call map(jdim,kdim,ndim,var,d,pointer,                         
     *                           jj,jjj,jint,kk,kkk,kint,n,fwd)         
         jj     = jtail2+1                                              
         jjj    = jmax                                                  
         jint   = 1                                                     
         kk     = 1                                                     
         kkk    = kmax                                                  
         kint   = 1                                                     
         call map(jdim,kdim,ndim,var,d,pointer,                         
     *                           jj,jjj,jint,kk,kkk,kint,n,fwd)         
 20    continue                                                         
c                                                                       
       if( .not. fwd)then                                               
c                                                                       
c             fill in data along wake cut                               
c                                                                       
         do 100 n=1,ndim                                                
         k1n = (n-1)*jdim*kdim                                          
           do 90 j=1,jtail1-1                                           
             var(j+k1n) = var(jmax-j+1+k1n)                             
  90       continue                                                     
 100     continue                                                       
       endif                                                            
c                                                                       
       return                                                           
       end                                                              
