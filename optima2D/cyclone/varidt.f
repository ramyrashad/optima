      subroutine varidt(itype,jdim,kdim,q,sndsp,xy,xyj,ds,precon)  
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),sndsp(jdim,kdim),precon(jdim,kdim,6)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),ds(jdim,kdim)    
c                                                                       
c     -computes variable dt based on the maximum eigenvalues           
c     -dtmin/dt because we later use ds*dt                                  
c                                                                       
      dtmin0 = dtmin/dt                                                 
c                                                                       
      if(itype.le.1)then                                                
c        variable time step based on jacobian                                
         factor=1.d0                                                        
         if(dtrate.ne.0.0)factor = 1.d0/dtrate                               
         if(jacdt.eq.0) factor=0.d0                                         
         do 65 k=kbegin,kend                                               
         do 65 j=jbegin,jend                                               
            sqj = sqrt(xyj(j,k))                                           
            ds(j,k)=(1.d0+dtmin0*sqj)/(1.d0+factor*sqj)                      
   65    continue                                                          
      elseif(itype.eq.2)then                                            
c        itype = 2                                                           
c        var. dt based on eigenvalues  (coakley type)                        
c                                                                       
         if (prec.eq.0) then
            do 100 k=klow,kup                                                
            do 100 j=jlow,jup                                                
c              ri is j/rho                                                   
               ri = 1.d0/q(j,k,1)                                          
               u = q(j,k,2)*ri                                               
               v = q(j,k,3)*ri                                               
c
c              note xit and ett not in calculation                           
c                                                                            
               siga=abs(u*xy(j,k,1)+v*xy(j,k,2)) +                           
     1           sndsp(j,k)*sqrt(xy(j,k,1)**2+xy(j,k,2)**2)                
               sigb = abs(u*xy(j,k,3) +v*xy(j,k,4)) +                        
     1           sndsp(j,k)*sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                
               dtx = 1.d0/(siga + 1.d-11)                                    
               dty = 1.d0/(sigb + 1.d-11)                                    
c                                                                            
               ds(j,k) = min(dtx,dty)                                        
  100       continue
         else
            dure=re/(rhoinf*dt)
cdu         rescale by the new spectral radius
            do 2000 k=klow,kup                                               
            do 2000 j=jlow,jup                                       
               siga=abs(precon(j,k,3))+precon(j,k,4)
               sigb=abs(precon(j,k,5))+precon(j,k,6)
               dtx = 1.d0/(siga + 1.d-8)                                     
               dty = 1.d0/(sigb + 1.d-8)                              
               dtv=dure*q(j,k,1)*xyj(j,k)/max(
     +              xy(j,k,1)**2+xy(j,k,2)**2,
     +              xy(j,k,3)**2+xy(j,k,4)**2)
c
c               zz=max(dtx,dty)
c               if (numiter.eq.iend) then
c                  if (dtv.lt.zz) then
c                     write (*,*) j,k,'von Neumann'
c                  else
c                     write (*,*) j,k,'CFL',dabs(dtv-zz)/zz
c                  endif
c               endif
               ds(j,k) = min(dtv,max(dtx,dty))
 2000       continue
         endif
      elseif(itype.eq.3)then                                            
c        itype = 3                                                          
c        variable dt based on constant cfl number                            
c                                                                          
         do 200 k=kbegin,kend                                              
         do 200 j=jbegin,jend                                              
c           ri is j/rho                                                    
            ri = 1.d0/q(j,k,1)                                               
            u = q(j,k,2)*ri                                                
            v = q(j,k,3)*ri                                                
c                                                                          
c           note xit and ett not in calculation                              
            sigab = abs(u*xy(j,k,1)+v*xy(j,k,2))                           
     *            + abs(u*xy(j,k,3)+v*xy(j,k,4))                           
     *            + sndsp(j,k)*sqrt(xy(j,k,1)**2 + xy(j,k,2)**2            
     *            +                 xy(j,k,3)**2 + xy(j,k,4)**2)           
c
c            if (prec.gt.0) then
c            sigab = abs(precon(j,k,3))                           
c     *            + abs(precon(j,k,5))                           
c     *            + sqrt(precon(j,k,4)**2 + precon(j,k,6)**2)
c            endif

            ds(j,k) = 1.d0/sigab
 200     continue
c                                                                       
c        lower limit on dt                                                   
         do 300 k=kbegin,kend                                              
         do 300 j=jbegin,jend                                              
           if(ds(j,k).lt.dtmin0) ds(j,k) = dtmin0                           
  300    continue                                                          
      elseif(itype.eq.4)then                                      
c                                                                     
c        itype =4                                                   
c        var. dt based on eigenvalues  (venkataswaran type)
c                                                                   
         do 250 k=klow,kup                                           
         do 250 j=jlow,jup                                      
c           ri is j/rho                                        
            ri =1.d0/q(j,k,1)
            u  =q(j,k,2)*ri                                       
            v  =q(j,k,3)*ri                 
c                                                            
c           note xit and ett not in calculation  
c                                                                 
            siga = abs(u*xy(j,k,1)+v*xy(j,k,2)) +                         
     &                 sndsp(j,k)*sqrt(xy(j,k,1)**2+xy(j,k,2)**2)
            sigb = abs(u*xy(j,k,3) +v*xy(j,k,4)) +                  
     &                 sndsp(j,k)*sqrt(xy(j,k,3)**2+xy(j,k,4)**2)
            dtx = 1.d0/(siga + 1.d-11)                              
            dty = 1.d0/(sigb + 1.d-11)                                
c                                                                   
            ds(j,k) = min(max(dtx,dty),1.d2*min(dtx,dty))
c            ds(j,k) = max(dtx,dty)                             
  250    continue                                                          
      endif                                                             
      return                                                            
      end                                                               
