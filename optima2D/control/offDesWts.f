c-----------------------------------------------------------------------
c     Program name: offDesWts.f
c     Written By: Howard Buckley, July 2009
c     
c     This subroutine calculates off-design weight updates at the end
c     of a weight update cycle
c
c-----------------------------------------------------------------------
      	
      subroutine offDesWts(nflag, ifun, ac_his, cp_his, MaxMach_r,
     &     clmax, alphamax, jdim, kdim, ndim, ifirst, indx, icol, 
     &     iex, q, cp, xy, xyj, x_r, y_r, cp_tar, fmu, vort, turmu, 
     &     work1, ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, 
     &     pat, itfirst, compObj, ifsd, mp, cdt_r, clt_r, badfs)

#ifdef _MPI_VERSION

      use mpi

#endif

      !-- Declare variables

      implicit none
								
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"


      integer
     &     nflag, jdim, kdim, ndim, ifirst, indx(jdim,kdim), icol(9),
     &     iex(jdim,kdim,4), ia(jdim*kdim*ndim+2), 
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1),
     &     iat(jdim*kdim*ndim+2), itfirst, ifun, badfs,
     &     jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1),
     &     i, j, k, sendProc, ifsd, mp, namelen, EOF

      double precision
     &     psi, psi_star, wt_old, wt_new, alphamax, clmax, MaxMach_r,
     &     q(jdim,kdim,ndim), cp(jdim,kdim), xy(jdim,kdim,4), 
     &     xyj(jdim,kdim), x_r(jdim,kdim), y_r(jdim,kdim), 
     &     cp_tar(jbody,2), fmu(jdim,kdim), vort(jdim,kdim), 
     &     turmu(jdim,kdim), work1(jdim*kdim,100),
     &     as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), compObj,
     &     pat(jdim*kdim*ndim*ndim*5+1), 
     &     cp_his(jbody,300,mpopt), ac_his(jbody,300),
     &     offdp(noffcon), ondp(mpopt-noffcon), ondpsum,
     &     offdw(noffcon), offdwsum, cdt_r, clt_r
      
      logical 
     &     wt_updates(noffcon)

      character
     &     command*60, filename*40

      !-- Main program
      
      write(scr_unit,10)
 10   format(/3x,'Beginning weight update procedure')

      !-- Evaluate off-design constraint values. For processors 
      !-- assigned to off-design points, each processor will evaluate
      !-- its respective off-design constraint and use the constraint
      !-- value to calculate the corresponding weight update.

#ifdef _MPI_VERSION

      if ((off_flags(rank+1)).and.(obj_func.eq.8)) then !-- Max Mach # 
                                                        !-- pts
         psi = MaxMach_r
            
         psi_star = c_upp1(rank+1)

         wt_old = wmpo(rank+1)
         
         wt_new = wmpo(rank+1)*(psi/psi_star)**wt_exp

#else

      if ((off_flags(1)).and.(obj_func.eq.8)) then !-- Max Mach # 

         psi = MaxMach_r
            
         psi_star = c_upp1(1)

         wt_old = wmpo(1)
         
         wt_new = wmpo(1)*(psi/psi_star)**wt_exp

#endif


#ifdef _MPI_VERSION

      else if ((off_flags(rank+1)).and.(obj_func.eq.6)) then 

         !-- Perform angle of attack sweep to obtain Cl_max

         alpha = alphas_restart(rank+1)

         clmax_flag = .true.
           
         call get_clmax (clmax, alphamax, jdim, kdim, ndim, 
     &     ifirst, indx, icol, iex, q, cp, xy, xyj, x_r, y_r, cp_tar, 
     &     fmu, vort, turmu, work1, ia, ja, ipa, jpa, iat, jat, ipt, 
     &     jpt, as, ast, pa, pat, itfirst, ifun, clt_r)

         psi = 1/clmax

         psi_star = 1/c_low1(rank+1)

         wt_old = wmpo(rank+1)

         wt_new = wmpo(rank+1)*(psi/psi_star)**wt_exp

#else

      else if ((off_flags(1)).and.(obj_func.eq.6)) then 

         !-- Perform angle of attack sweep to obtain Cl_max

         alpha = alphas_restart(1)

         clmax_flag = .true.
           
         call get_clmax (clmax, alphamax, jdim, kdim, ndim, 
     &     ifirst, indx, icol, iex, q, cp, xy, xyj, x_r, y_r, cp_tar, 
     &     fmu, vort, turmu, work1, ia, ja, ipa, jpa, iat, jat, ipt, 
     &     jpt, as, ast, pa, pat, itfirst, ifun, clt_r)

         psi = 1/clmax

         psi_star = 1/c_low1(1)

         wt_old = wmpo(1)

         wt_new = wmpo(1)*(psi/psi_star)**wt_exp

#endif
      
      end if

#ifdef _MPI_VERSION

      !-- Wait for weight updates to be completed before proceeding

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      clmax_flag = .false.

      !-- Send updated off-design weight to array 'wmpo_new' on 
      !-- processor 0

      wmpo_new = wmpo

#ifdef _MPI_VERSION

      if (off_flags(rank+1)) then

         call MPI_SEND(wt_new, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)            

      end if

      if (rank==0) then

         do i = 1, mpopt

            if (off_flags(i)) then

               sendProc = i-1
               call MPI_RECV(wt_new, 1, MPI_DOUBLE_PRECISION, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

               wmpo_new(i) = wt_new

            end if
            
         end do
         
      end if

      !-- Wait for all updated off-design weights to be sent to array
      !-- 'wmpo_new' before proceeding

      call MPI_BARRIER(COMM_CURRENT, ierr)  

      !-- Broadcast the array of updated off-design weights 'wmpo_new'
      !-- to all processors

      call MPI_BCAST(wmpo_new,mpopt,MPI_DOUBLE_PRECISION,0,
     &              COMM_CURRENT,ierr) 

#else

      if (off_flags(1)) then

         wmpo_new(1) = wt_new

      end if

#endif

      !-- Send alfamax (for high lift points) to array 'alphas_new' 
      !-- on processor 0

      alphas_new = alphas_restart

#ifdef _MPI_VERSION

      if ((off_flags(rank+1)).and.(obj_func.eq.6)) then

         call MPI_SEND(alphamax, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)            

      end if

      if (rank==0) then

         do i = 1, mpopt

            if ((off_flags(i)).and.(objfuncs(i).eq.6)) then
            
               sendProc = i-1

               call MPI_RECV(alphamax, 1, MPI_DOUBLE_PRECISION, sendProc 
     &             , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

               alphas_new(i) = alphamax
         
            end if
   
         end do
         
      end if
         
      !-- Wait for all alphamax values to be sent to array
      !-- 'alphas_new' before proceeding

      call MPI_BARRIER(COMM_CURRENT, ierr)  

      !-- Broadcast the array of updated AOA's 'alphas_new'
      !-- to all processors

      call MPI_BCAST(alphas_new,mpopt,MPI_DOUBLE_PRECISION,0,
     &           COMM_CURRENT,ierr)  

#else

      if ((off_flags(1)).and.(obj_func.eq.6)) then

         alphas_new(1) = alphamax

      end if

#endif
      
      !-- Determine if a weight update is required. If the difference 
      !-- between the new weight and the old weight is greater than a 
      !-- user specified tolerance then set the weight update flag to 
      !-- 'true' for this off-design point OR if the difference 
      !-- between the constraint value and the target constraint 
      !-- value is greater than a user specified tolerance, then set
      !-- the weight update flag to 'true'

      wt_update = .false.

#ifdef _MPI_VERSION

      if (off_flags(rank+1)) then

         if (abs(wt_new - wt_old).gt.(0.0)) wt_update = .true.

         if (abs(psi - psi_star).le.(0.0)) wt_update = .false.

      end if

      !-- Collect values of weight update flags at each
      !-- off-design point and store in an array on processor 0:

      if (off_flags(rank+1)) then

         call MPI_SEND(wt_update, 1, MPI_LOGICAL, 0, 1, 
     &   COMM_CURRENT, ierr)            

      end if

      if (rank==0) then
         
         j = 1

         do i = 1, mpopt

            if (off_flags(i)) then

               sendProc = i-1
               call MPI_RECV(wt_update, 1, MPI_LOGICAL, sendProc 
     &             , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

               wt_updates(j) = wt_update

               j = j+1

            end if

         end do
         
      end if
        
      !-- Wait for all weight update flags to be sent to array
      !-- 'wt_updates' before proceeding

      call MPI_BARRIER(COMM_CURRENT, ierr)  

      !-- Broadcast the array of weight update flag values to
      !-- all processors

      call MPI_BCAST(wt_updates,mpopt,MPI_LOGICAL,0,COMM_CURRENT,
     &  ierr)      

#else

      if (off_flags(1)) then

         if (abs(wt_new - wt_old).gt.(0.0)) wt_update = .true.

         if (abs(psi - psi_star).le.(0.0)) wt_update = .false.

         wt_updates(1) = wt_update

      end if

#endif

      !-- Scan 'wt_updates' array to determine if any off-design 
      !-- point requires a weight update. 

      do i = 1, mpopt

         !-- If weight update flag is 'true' at any design point then 
         !-- set 'wt_update' equal to 'TRUE' at this design point.
         !-- This will force an optimization restart on all processors
         !-- using the updated off-design weights contained in 
         !-- 'wmpo_new'

         if (wt_updates(i)) then
            
            wt_update = .true.
            write(scr_unit,*)'wt_update = ',wt_update
            wmpo = wmpo_new
            write(scr_unit,*)'wmpo = ',wmpo
            alphas_restart = alphas_new
            write(scr_unit,*)'alphas_restart = ',alphas_restart

            goto 124

         end if

      end do

 124  continue

      !-- If a line search stall has occured, then force weight 
      !-- update flag to 'true' and start a new cycle with updated 
      !-- weights 
         
      if (ifsd.gt.20) then 
         wt_update = .true.
         write(scr_unit,125)
 125     format(/3x,'A line search stall has occured, so force weight
     & update flag to true')
      end if

      !-- If too many bad flow solves have occured, then force weight 
      !-- update flag to 'true' and start a new cycle with updated 
      !-- weights 
         
      if (badfs.gt.20) then 
         wt_update = .true.
         write(scr_unit,126)
 126     format(/3x,'Too many bad flow solves have occured, so force
     & weight update flag to true')
      end if

      if (wt_update) then

#ifdef _MPI_VERSION
         if(rank.eq.0) then
#endif
            if (acout) rewind (ac_unit)
            do i = 1,jbody
               if (acout) write (ac_unit,900) ac_his(i,1), 
     &                (ac_his(i,j+1),j=1,ifun)
            end do
            write(ghis_unit,500) icg,ifun,compObj,cgradmag
#ifdef _MPI_VERSION
         end if
#endif

         if (ifsd.gt.20) write (scr_unit, 3190)
         if (badfs.gt.10) write (scr_unit, 3191)
 3190    format(/3x,'A line search stall has occured so restart', 
     &               ' optimization',/)
 3191    format(/3x,'Too many bad flow solves have occured so restart', 
     &               ' optimization',/)
         if (wt_update) then
            write (scr_unit,319) wt_iter 
 319        format(/3x,'Restarting optimization after [',i3,
     &         ' ] optimization iterations',/3x, 
     &          'using updated off-design weights',/)
         end if

 900     format (500e16.6)
 500     format(4x,i5,3x,i6,2x,d15.8,2x,d15.8)

         !-- Set the initial angle of attack values for the 1st 
         !-- iteration of the next weight update cycle to the initial 
         !-- angle of attack values from the last successful design 
         !-- iteration of the current weight update cycle.

         alphas = alphas_restart

         !-- Reset the output and restart file prefixes and set the 
         !-- continuation restart flag to 'false' before starting the 
         !-- next weight update cycle. 
    
         output_file_prefix = 'rest00'
         restart_file_prefix = 'rest00'
         opt_restart = .false.
         obj_restart = .true.

#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif                  
            rewind(prefix_unit)
            write(prefix_unit,*)output_file_prefix
            write(prefix_unit,*)restart_file_prefix
            write(prefix_unit,*)opt_restart
            write(prefix_unit,*)obj_restart
#ifdef _MPI_VERSION
         end if
#endif

#ifdef _MPI_VERSION

         !-- Send off-design performance values to array 'offdp' on
         !-- processor 0

         if ((off_flags(rank+1)).and.(obj_func.eq.8)) then

            call MPI_SEND(MaxMach_r, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)
            
         else if ((off_flags(rank+1)).and.(obj_func.eq.6)) then

            call MPI_SEND(clmax, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)            

         end if

         if (rank==0) then
            
            j = 1

            do i = 1, mpopt

               if ((off_flags(i)).and.(objfuncs(i).eq.8)) then

                  sendProc = i-1

                  call MPI_RECV(MaxMach_r, 1, MPI_DOUBLE_PRECISION, 
     &                             sendProc , 1, COMM_CURRENT, 
     &                             MPI_STATUS_IGNORE, ierr)

                  offdp(j) = MaxMach_r

                  j = j+1

               else if ((off_flags(i)).and.(objfuncs(i).eq.6)) then
                  
                  sendProc = i-1

                  call MPI_RECV(clmax, 1, MPI_DOUBLE_PRECISION, 
     &                             sendProc , 1, COMM_CURRENT, 
     &                             MPI_STATUS_IGNORE, ierr)

                  offdp(j) = clmax

                  j = j+1

               end if

            end do
         
         end if

#else


         if ((off_flags(1)).and.(obj_func.eq.8)) then

            offdp(1) = MaxMach_r
            
         else if ((off_flags(1)).and.(obj_func.eq.6)) then
          
            offdp(1) = clmax

         end if

#endif

         !-- Write off-design performance values to data file
#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif

            open(unit=5440,file='off-design-performance.dat',
     &               status='unknown')

            EOF = 0
               
            do while (EOF.eq.0)

               read(5440,5444, IOSTAT = EOF)

            end do

            write(5440,5444) wt_cycle, offdp

            close(5440)
#ifdef _MPI_VERSION               
         end if
#endif

#ifdef _MPI_VERSION

         !-- Send off-design weights to array 'offdw' on
         !-- processor 0

         if (off_flags(rank+1)) then

            call MPI_SEND(wt_old, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)

         end if

         if (rank==0) then

            j = 1
            
            do i = 1, mpopt

               if (off_flags(i)) then

                  sendProc = i-1

                  call MPI_RECV(wt_old, 1, MPI_DOUBLE_PRECISION, 
     &                             sendProc , 1, COMM_CURRENT, 
     &                             MPI_STATUS_IGNORE, ierr)

                  offdw(j) = wt_old

                  j = j+1

               end if

            end do
         
         end if

#else


         if (off_flags(1)) then

            offdw(1) = wt_old

         end if

#endif

         !-- Write off-design weights to data file
#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif

            open(unit=5442,file='weights.dat',
     &               status='unknown')
            open(unit=5445,file='off-design-weights-sum.dat',
     &               status='unknown')

            offdwsum = 0.0

            do i=1,noffcon

               offdwsum = offdwsum + offdw(i)

            end do

            EOF = 0
               
            do while (EOF.eq.0)

               read(5442,5444, IOSTAT = EOF)

            end do

            write(5442,5444) wt_cycle, offdw

            close(5442)

            EOF = 0
               
            do while (EOF.eq.0)

               read(5445,*, IOSTAT = EOF)

            end do

            write(5445,*) wt_cycle, offdwsum

            close(5445)

#ifdef _MPI_VERSION               
         end if
#endif

#ifdef _MPI_VERSION

         !-- Send on-design performance values to array 'ondp' on
         !-- processor 0

         if ((off_flags(rank+1)).eq.(.false.)) then

            call MPI_SEND(cdt_r, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)

         end if

         if (rank==0) then

            j = 1

            do i = 1, mpopt

               if ((off_flags(i)).eq.(.false.)) then

                  sendProc = i-1

                  call MPI_RECV(cdt_r, 1, MPI_DOUBLE_PRECISION, 
     &                             sendProc , 1, COMM_CURRENT, 
     &                             MPI_STATUS_IGNORE, ierr)

                  ondp(j) = cdt_r

                  j = j+1

               end if

            end do
         
         end if

#else

         if ((off_flags(1)).eq.(.false.)) then

            ondp(1) = cdt_r

         end if

#endif

         !-- Write on-design performance values to data file

#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif

            open(unit=5443,file='on-design.dat',
     &               status='unknown')
            open(unit=5446,file='on-design-sum.dat',
     &               status='unknown')

            ondpsum = 0.0

            do i=1,mpopt-noffcon

               ondpsum = ondpsum + ondp(i)

            end do

            EOF = 0
               
            do while (EOF.eq.0)

               read(5443,5444, IOSTAT = EOF) 

            end do

            write(5443,5444) wt_cycle, ondp
 5444       format(i4,50(3x,f12.9))

            close(5443)

            EOF = 0
               
            do while (EOF.eq.0)

               read(5446,*, IOSTAT = EOF)

            end do

            write(5446,*) wt_cycle, ondpsum

            close(5446)
#ifdef _MPI_VERSION               
         end if
#endif               

#ifdef _MPI_VERSION

         !-- Wait until all processors have reached this point before
         !-- proceeding

         call MPI_BARRIER(COMM_CURRENT, ierr)

#endif
           
         !-- Exit and restart with updated off-design weights

         nflag = -1

         return
     
      else

         write (scr_unit,321) wt_iter 
 321     format(/3x,'Off-design weights remain unchanged after [',i3,
     &         ' ] optimization iterations',/)

         !-- Erase all stored AOA sweep values and create an empty
         !-- AOA sweep file before continuing with the current weight 
         !-- update cycle

         filename = output_file_prefix_backup            
         write(scr_unit,*)'filename=',filename
         write(scr_unit,*)'output_file_prefix_backup=',
     &output_file_prefix_backup
         namelen = len_trim(filename)
         write(scr_unit,*)namelen
         filename(namelen+1:namelen+4) = '.aoa'
         write(scr_unit,*)filename
         namelen = len_trim(filename)
         write(scr_unit,*)namelen
         command = 'rm -f '
         command(7:namelen+6) = filename
         write(scr_unit,*) command
         call system(command)
            
         !-- Close AOA file

         if (aoaout) close(aoa_unit)

         !-- Open a new AOA file

         if (aoaout) open(unit=aoa_unit,file=filename,status='new')

      end if ! (wt_update = .true.)
         
      !-- Reset weight update iteration counter

      wt_iter = 0

      return

      end
