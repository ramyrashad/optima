      subroutine vtribx(JDIM,KDIM,A,B,C,X,F,JL,JU,KL,KU)                
c                                                                       
      DIMENSION A(JDIM,KDIM),B(JDIM,KDIM),C(JDIM,KDIM),                 
     >          X(JDIM,KDIM),F(JDIM,KDIM)                               
c                                                                       
      DO 10 K = KL,KU                                                   
      X(JL,K)=C(JL,K)/B(JL,K)                                           
      F(JL,K)=F(JL,K)/B(JL,K)                                           
10    CONTINUE                                                          
      JLP1 = JL +1                                                      
      DO 1 J=JLP1,JU                                                    
         DO 11 K = KL,KU                                                
         Z=1./(B(J,K)-A(J,K)*X(J-1,K))                                  
         X(J,K)=C(J,K)*Z                                                
         F(J,K)=(F(J,K)-A(J,K)*F(J-1,K))*Z                              
11       CONTINUE                                                       
1     continue                                                          
c                                                                       
      JUPJL=JU+JL                                                       
      DO 2 J1=JLP1,JU                                                   
         DO 12 K = KL,KU                                                
         J=JUPJL-J1                                                     
         F(J,K)=F(J,K)-X(J,K)*F(J+1,K)                                  
12       CONTINUE                                                       
2     continue                                                          
c                                                                       
      RETURN                                                            
      END                                                               
