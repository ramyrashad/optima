c ********************************************************************
c ** LU Backsubstitution - for pentadiagonal systems                **
c ** x sweep routine                                                **
c ** Crout's method of factorization applied to pentadiagonals      **
c ********************************************************************
      subroutine xlubcsbb(dm2,dm1,dd,dp1,dp2,b,jmax,kmax,
     &                   jlow,jup,klow,kup)
c ********************************************************************
c Variables:   dm2: vector 2 below diagonal
c              dm1: vector 1 below diagonal    
c              dd : vector of diagonal entries
c              dp1: vector 1 above diagonal
c              dp2: vector 2 above diagonal
c ********************************************************************
      dimension dm2(jmax,kmax), dm1(jmax,kmax), dd(jmax,kmax)
      dimension dp1(jmax,kmax), dp2(jmax,kmax), b(jmax,kmax) 
      do 199 k=klow,kup                 /* outer loop over y        */
        j = jlow + 1                    /* Forward Substitution     */
c         if (k .ge. 46) print *,j,k,'  b=',b(j,k)
c         if (k .ge. 46) print *,j-1,k,'  b=',b(j-1,k)
        print *,j,k,b(j,k)                
        b(j,k) = b(j,k) - dm1(j,k)*b(j-1,k)
c         if (k .ge. 46) print *,j,k,'dm1=',dm1(j,k)
        do 50 j = jlow+2,jup
         if (k .ge. klow) print *,j,k,'  b=',b(j,k)
         if (k .ge. klow) print *,j-1,k,'  b=',b(j-1,k)
         if (k .ge. klow) print *,j-2,k,'  b=',b(j-2,k)
          b(j,k) = b(j,k) - dm2(j,k)*b(j-2,k) - dm1(j,k)*b(j-1,k)
         if (k .ge. klow) print *,j,k,dm1(j,k),dm2(j,k)
   50   continue
        j = jup                         /* Back Substitution        */
c        print *,j,k,dd(j,k)
        b(j,k) = b(j,k)/dd(j,k)
        j = jup - 1
c        print *,j,k,dd(j,k)
        b(j,k) = (b(j,k) - dp1(j,k)*b(j+1,k))/dd(j,k)
        do 75 j = jup-2,jlow,-1
c        print *,j,k,dd(j,k)
          b(j,k)=(b(j,k)-dp2(j,k)*b(j+2,k)-dp1(j,k)*b(j+1,k))/dd(j,k)
   75   continue
  199 continue
      return
      end
