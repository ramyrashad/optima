c     ------------------------------------------------------------------
c     -- calculate gradient of objective function --
c     -- m. nemec, june 2001 --
c     ------------------------------------------------------------------
      subroutine CalcGrad( ifirst, nmax, indx, q, xy, xyj, x, y, turmu,
     &     vort, turre, fmu, press, precon, sndsp, xit, ett, ds,vk, ve,
     &     coef2x, coef4x, coef2y, coef4y, s, uu, vv, ccx, ccy,spectxi,
     &     specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja, icol, xsave,
     &     ysave)

      implicit none

#include "../include/parms.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer ifirst, nmax, ntvar
      double precision q(*),             press(*),       sndsp(*)
      double precision s(*),             xy(*),          xyj(*)
      double precision xit(*),           ett(*),         ds(*)
      double precision x(*),             y(*),           turmu(*)
      double precision vort(*),          turre(*),       fmu(*)
      double precision vk(*),            ve(*),          tmp(*)
      double precision uu(*),            vv(*),          ccx(*)
      double precision ccy(*),           coef2x(*),      coef2y(*)
      double precision coef4x(*),        coef4y(*),      precon(*)
      double precision spectxi(*),       specteta(*),    wk1(*)

      integer ia(*), ja(*), ipa(*), jpa(*), icol(*), indx(*)

      double precision xsave(*), ysave(*) 

c     -- residual array for s-a model --
      double precision rsa(maxjk)

      if (igrad .eq. 0) then
c     -- gradient ala finite differences --
         call fd_grad( ifirst, nmax, indx, q, xy, xyj, x, y, turmu,
     &        vort, turre, fmu, press, precon, sndsp, xit, ett,ds, vk,
     &        ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,ccx, ccy,
     &        spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,ia, ja, icol,
     &        xsave, ysave)
      else if (igrad .eq. 1) then
c     -- gradient ala adjoint method --
         rewind (n_gmres)
         call adjoint( q, press, sndsp, s, xy, xyj, xit, ett, ds, x, y,
     &        turmu, vort, turre, fmu, vk, ve, tmp, uu, vv, ccx, ccy,
     &        coef2x, coef2y, coef4x, coef4y, precon, spectxi, specteta,
     &        rsa, indx, icol, ia, ja, ipa, jpa, ntvar, nmax, xsave,
     &        ysave, wk1, wk1(2*maxjk+1), wk1(3*maxjk+1),wk1(4*maxjk+1),
     &        wk1(8*maxjk+1))
      else if (igrad .eq. 2) then
c     -- gradient ala matrix-free sensitivity method --
         rewind (n_gmres)
         call sensit_mf( q, press, sndsp, s, xy, xyj, xit, ett, ds, x,
     &        y, turmu, vort, turre, fmu, vk, ve, tmp, uu, vv, ccx, ccy,
     &        coef2x, coef2y, coef4x, coef4y, precon, spectxi, specteta,
     &        rsa, indx, icol, ia, ja, ipa, jpa, ntvar, nmax, wk1,
     &        wk1(maxjk+1), wk1(2*maxjk+1), wk1(3*maxjk+1),
     &        wk1(7*maxjk+1), xsave, ysave)
      end if

      return
      end                       !CalcGrad
