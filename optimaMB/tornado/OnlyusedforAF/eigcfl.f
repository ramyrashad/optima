      subroutine eigcfl(jdim,kdim,q,sndsp,xy,ds,iblock,
     &      jbegin,jend,jlow,jup,kbegin,kend,klow,kup,
     &      cflmax,jcflmax,kcflmax,iblkcflmax,
     &      cflmin,jcflmin,kcflmin,iblkcflmin)
c
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),sndsp(jdim,kdim)
      dimension xy(jdim,kdim,4),ds(jdim,kdim)
c
c     -note xit and ett not in calculation */
      do 100 k=klow,kup
      do 100 j=jlow,jup
        ri = 1./q(j,k,1)
        u = q(j,k,2)*ri
        v = q(j,k,3)*ri
        sigab = abs(u*xy(j,k,1)+v*xy(j,k,2))
     &        + abs(u*xy(j,k,3)+v*xy(j,k,4))
     &        + sndsp(j,k)*sqrt(xy(j,k,1)**2 + xy(j,k,2)**2
     &        +                  xy(j,k,3)**2 + xy(j,k,4)**2)
        cfldt = abs(dt*ds(j,k)*sigab)
        if (cfldt.gt.cflmax) then
          jcflmax = j
          kcflmax = k
          cflmax  = cfldt
          iblkcflmax = iblock
        endif
        if (cfldt.lt.cflmin) then
          jcflmin = j
          kcflmin = k
          cflmin  = cfldt
          iblkcflmin = iblock
        endif
 100  continue
c     
      return
      end
