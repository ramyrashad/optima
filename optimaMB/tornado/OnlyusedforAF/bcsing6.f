c ********************************************************************
c **       Routine to apply avg boundary conditions                 **
c **       For a singular point where 6 blocks meet                 **
c **       Note: javg/kavg can also be j,k                          **
c ********************************************************************
c calling routines: integrat
c
      subroutine bcsing6(jdim1,kdim1,q1,xyj1,j1,k1,javg1,kavg1,
     &                   jdim2,kdim2,q2,xyj2,j2,k2,javg2,kavg2,
     &                   jdim3,kdim3,q3,xyj3,j3,k3,javg3,kavg3,
     &                   jdim4,kdim4,q4,xyj4,j4,k4,javg4,kavg4,
     &                   jdim5,kdim5,q5,xyj5,j5,k5,javg5,kavg5,
     &                   jdim6,kdim6,q6,xyj6,j6,k6,javg6,kavg6)
c
#include "../include/common.inc"
      dimension q1(jdim1,kdim1,4), xyj1(jdim1,kdim1)
      dimension q2(jdim2,kdim2,4), xyj2(jdim2,kdim2)
      dimension q3(jdim3,kdim3,4), xyj3(jdim3,kdim3)
      dimension q4(jdim4,kdim4,4), xyj4(jdim4,kdim4)
      dimension q5(jdim5,kdim5,4), xyj5(jdim5,kdim5)
      dimension q6(jdim6,kdim6,4), xyj6(jdim6,kdim6)
c
c     Average the values in locations (javg,kavg)
c     and then store the results in (j,k)
c 
c      write(6,*) 'Starting averaging of 6-block singular point'
c      write(6,*) j1,k1,javg1,kavg1
c      write(6,*) j2,k2,javg2,kavg2
c      write(6,*) j3,k3,javg3,kavg3
c      write(6,*) j4,k4,javg4,kavg4
c      write(6,*) j5,k5,javg5,kavg5
c      write(6,*) j6,k6,javg6,kavg6

      RR1 = 1./XYJ1(j1,k1)        
      RR2 = 1./XYJ2(j2,k2)        
      RR3 = 1./XYJ3(j3,k3)        
      RR4 = 1./XYJ4(j4,k4)        
      RR5 = 1./XYJ5(j5,k5)        
      RR6 = 1./XYJ6(j6,k6)        
c ************* average the density  *************************

      QSAV1 = ( Q1(javg1,kavg1,1)*XYJ1(javg1,kavg1)
     &        + Q2(javg2,kavg2,1)*XYJ2(javg2,kavg2) 
     &        + Q3(javg3,kavg3,1)*XYJ3(javg3,kavg3) 
     &        + Q4(javg4,kavg4,1)*XYJ4(javg4,kavg4) 
     &        + Q5(javg5,kavg5,1)*XYJ5(javg5,kavg5) 
     &        + Q6(javg6,kavg6,1)*XYJ6(javg6,kavg6) ) / 6.0

c ************* average rho*U        *************************

      QSAV2 = ( Q1(javg1,kavg1,2)*XYJ1(javg1,kavg1)
     &        + Q2(javg2,kavg2,2)*XYJ2(javg2,kavg2) 
     &        + Q3(javg3,kavg3,2)*XYJ3(javg3,kavg3) 
     &        + Q4(javg4,kavg4,2)*XYJ4(javg4,kavg4) 
     &        + Q5(javg5,kavg5,2)*XYJ5(javg5,kavg5) 
     &        + Q6(javg6,kavg6,2)*XYJ6(javg6,kavg6) ) / 6.0

c ************* average rho*V        *************************

      QSAV3 = ( Q1(javg1,kavg1,3)*XYJ1(javg1,kavg1)
     &        + Q2(javg2,kavg2,3)*XYJ2(javg2,kavg2) 
     &        + Q3(javg3,kavg3,3)*XYJ3(javg3,kavg3)
     &        + Q4(javg4,kavg4,3)*XYJ4(javg4,kavg4) 
     &        + Q5(javg5,kavg5,3)*XYJ5(javg5,kavg5) 
     &        + Q6(javg6,kavg6,3)*XYJ6(javg6,kavg6) ) / 6.0

c ************* average the pressure *************************

      PPP1 = GAMI*(Q1(javg1,kavg1,4)
     &             -0.5*(Q1(javg1,kavg1,2)**2+Q1(javg1,kavg1,3)**2)
     &                /Q1(javg1,kavg1,1))*XYJ1(javg1,kavg1)
      PPP2 = GAMI*(Q2(javg2,kavg2,4)
     &             -0.5*(Q2(javg2,kavg2,2)**2+Q2(javg2,kavg2,3)**2)
     &                /Q2(javg2,kavg2,1))*XYJ2(javg2,kavg2)
      PPP3 = GAMI*(Q3(javg3,kavg3,4)
     &             -0.5*(Q3(javg3,kavg3,2)**2+Q3(javg3,kavg3,3)**2)
     &                /Q3(javg3,kavg3,1))*XYJ3(javg3,kavg3)
      PPP4 = GAMI*(Q4(javg4,kavg4,4)
     &             -0.5*(Q4(javg4,kavg4,2)**2+Q4(javg4,kavg4,3)**2)
     &                /Q4(javg4,kavg4,1))*XYJ4(javg4,kavg4)
      PPP5 = GAMI*(Q5(javg5,kavg5,4)
     &             -0.5*(Q5(javg5,kavg5,2)**2+Q5(javg5,kavg5,3)**2)
     &                /Q5(javg5,kavg5,1))*XYJ5(javg5,kavg5)
      PPP6 = GAMI*(Q6(javg6,kavg6,4)
     &             -0.5*(Q6(javg6,kavg6,2)**2+Q6(javg6,kavg6,3)**2)
     &                /Q6(javg6,kavg6,1))*XYJ6(javg6,kavg6)
      PSAV = (PPP1+PPP2+PPP3+PPP4+PPP5+PPP6) / 6.0

C ************* store the results    *************************

      Q1(j1,k1,1) = QSAV1*RR1
      Q2(j2,k2,1) = QSAV1*RR2
      Q3(j3,k3,1) = QSAV1*RR3
      Q4(j4,k4,1) = QSAV1*RR4
      Q5(j5,k5,1) = QSAV1*RR5
      Q6(j6,k6,1) = QSAV1*RR6

      Q1(j1,k1,2) = QSAV2*RR1
      Q2(j2,k2,2) = QSAV2*RR2
      Q3(j3,k3,2) = QSAV2*RR3
      Q4(j4,k4,2) = QSAV2*RR4
      Q5(j5,k5,2) = QSAV2*RR5
      Q6(j6,k6,2) = QSAV2*RR6

      Q1(j1,k1,3) = QSAV3*RR1
      Q2(j2,k2,3) = QSAV3*RR2
      Q3(j3,k3,3) = QSAV3*RR3
      Q4(j4,k4,3) = QSAV3*RR4
      Q5(j5,k5,3) = QSAV3*RR5
      Q6(j6,k6,3) = QSAV3*RR6

      Q1(j1,k1,4) = PSAV/GAMI*RR1+        
     *          0.5*(Q1(j1,k1,2)**2+Q1(j1,k1,3)**2)/Q1(j1,k1,1)
      Q2(j2,k2,4) = PSAV/GAMI*RR2+        
     *          0.5*(Q2(j2,k2,2)**2+Q2(j2,k2,3)**2)/Q2(j2,k2,1)
      Q3(j3,k3,4) = PSAV/GAMI*RR3+        
     *          0.5*(Q3(j3,k3,2)**2+Q3(j3,k3,3)**2)/Q3(j3,k3,1)
      Q4(j4,k4,4) = PSAV/GAMI*RR4+        
     *          0.5*(Q4(j4,k4,2)**2+Q4(j4,k4,3)**2)/Q4(j4,k4,1)
      Q5(j5,k5,4) = PSAV/GAMI*RR5+        
     *          0.5*(Q5(j5,k5,2)**2+Q5(j5,k5,3)**2)/Q5(j5,k5,1)
      Q6(j6,k6,4) = PSAV/GAMI*RR6+        
     *          0.5*(Q6(j6,k6,2)**2+Q6(j6,k6,3)**2)/Q6(j6,k6,1)
c
      return
      end
