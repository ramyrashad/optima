c----------------------------------------------------------------------
c     -- calculate gradient using finite differences --
c     -- based on: numerical methods for unconstrained optimization
c     and nonlinear equations, dennis and schnabel, pg. 322. --
      
c     written by: marian nemec
c     date: april, 2000
c----------------------------------------------------------------------

      subroutine fd_grad (obj, grad, jdim, kdim, ndim, ifirst, indx,
     &      icol, iex, q, cp, xy, xyj, x, y, fmu, vort, turmu, cp_tar,
     &      bap, bcp, bt, bknot, dvs, idv, work1, ia, ja, ipa, jpa, iat,
     &      jat, ipt,jpt, as, ast, pa, pat, qs, xs, ys, turres,
     |      xOrig, yOrig, dx, dy, obj0, mp, ifun)

#ifdef _MPI_VERSION     
      use mpi
#endif


#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer i, j, k, jdim, kdim, ndim, ifirst, icol(9), idv(nc+mpopt)
      integer indx(jdim,kdim), iex(jdim,kdim,ndim), ifun

      double precision obj, obj0, grad(nc+mpopt), obj1, H
      double precision q(jdim,kdim,ndim), cp(jdim,kdim), xy(jdim,kdim,4)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision xOrig(jdim,kdim), yOrig(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)
      double precision cp_tar(jbody,2), dvs(nc+mpopt), bap(jbody,2)
      double precision bcp(nc,2), bt(jbody), bknot(jbsord+nc)

c     -- sparse adjoint arrays --
clb
      integer ia(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer iat(jdim*kdim*ndim+2)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)
clb

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100)

      double precision qs(jdim,kdim,4), xs(jdim,kdim), ys(jdim,kdim) 
      double precision turres(jdim,kdim)

c     -- local variables --
      double precision tmp, stepsize, objp, objm, lconp, lconm

      logical itfirst

c     Variables for finite difference stepsize selection (Chad Oldfield)
      logical new_stepsize
      double precision given_stepsize
      logical manysteps

      write (scr_unit,5),rank,ndv
      call flush(6)
 5    format(/3x,
     &'Proc [',i2,' ] performing finite difference gradient calculation',
     & /3x,'using [ ',i2,' ] design variables')


      manysteps = .false.  ! Set to true to try several finite
c         difference step sizes.

      itfirst = .false.

      clt_tmp = clt
      cdt_tmp = cdt

c     -- store base-state q, grid and turre --
      do n = 1,ndim
        do k = kbegin,kend
          do j = jbegin,jend
            qs(j,k,n) = q(j,k,n)
          end do
        end do
      end do

      do k = kbegin,kend
        do j = jbegin,jend
          xs(j,k) = x(j,k)
          ys(j,k) = y(j,k)
        end do
      end do

c      if ( itmodel .eq. 2 .and. turbulnt ) then
c        do k = kbegin,kend
c          do j = jbegin,jend
c            turres(j,k) = turre(j,k)
c          end do
c        end do
c      end if
      
c     -- central difference gradient approximation --
c     -- second order accurate --
      do i = 1,ndv
c$$$        if (manysteps) then
c$$$c         Tell new_stepsize to begin trying several step sizes.
c$$$          stepsize = 0.d0
c$$$        else
c$$$c         Just use default stepsize selection, and run the finite
c$$$c         difference once.
c$$$          stepsize = -1.d0
c$$$        endif
c$$$        do while(new_stepsize(grad(i), given_stepsize, 1.d-2, 1.d-8))
c$$$          tmp = dvs(i)
c$$$          if (given_stepsize < 0.d0) then !hack
c$$$            stepsize = fd_eta*dvs(i)
c$$$          else !hack
c$$$            stepsize = given_stepsize !hack
c$$$          endif !hack


          tmp = dvs(i)
          stepsize=fd_eta

c         -- evaluate (+) state --
          dvs(i) = dvs(i) + stepsize
c$$$          stepsize = dvs(i) - tmp

c         -- regrid domain --
          if ( i.le.ngdv ) then
            call regrid ( (i), jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &            bknot, xOrig, yOrig, dx, dy)
          else if (i.eq.ngdv+1) then
             if (.not.dvmach(rank+1)) then
                alpha = dvs(i)
                cosang = cos(pi*alpha/180.d0)
                sinang = sin(pi*alpha/180.d0)
                uinf   = fsmach*cosang
                vinf   = fsmach*sinang
             else if (dvmach(rank+1)) then
                fsmach = dvs(i)
                cosang = cos(pi*alpha/180.d0)
                sinang = sin(pi*alpha/180.d0)
                uinf   = fsmach*cosang
                vinf   = fsmach*sinang
             end if
          else if ((i.eq.ngdv+2).and.(dvmach(rank+1))) then 
             fsmach = dvs(i)
             cosang = cos(pi*alpha/180.d0)
             sinang = sin(pi*alpha/180.d0)
             uinf   = fsmach*cosang
             vinf   = fsmach*sinang         
          end if

          !-- Calculate new lift coefficient and Reynolds number
          !-- based on updated Mach number design variable
          if (dvmach(rank+1)) then
             H = fsmach/dvmach0 ! Ratio of current mach # to initial
             cl_tar = (H**-2)*cltars(mp)
             re_in = H*reno(mp)
          end if


c         -- calculate new objective function value at (+) --
          call CalcObj (objp,jdim, kdim, ndim, ifirst, indx, icol,iex,q,
     &         cp, xy, xyj, x, y, cp_tar, fmu, vort, turmu, work1, ia,
     &         ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat, 
     &         itfirst, obj0, mp, ifun, obj1, lconp)
          write(scr_unit,*)'objp = ', objp

c         -- restore base-state solution --
          do n = 1,ndim
            do k = kbegin,kend
              do j = jbegin,jend
                q(j,k,n) = qs(j,k,n)
              end do
            end do
          end do

c          if ( itmodel .eq. 2 .and. turbulnt ) then
c             do k = kbegin,kend
c                do j = jbegin,jend
c                   turre(j,k) = qs(j,k,5)
c                end do
c             end do
c          end if

          do k = kbegin,kend
            do j = jbegin,jend
              x(j,k) = xs(j,k)
              y(j,k) = ys(j,k)
            end do
          end do

c          if ( itmodel .eq. 2 .and. turbulnt ) then
c            do k = kbegin,kend
c              do j = jbegin,jend
c                turre(j,k) = turres(j,k)
c              end do
c            end do
c          end if

c         -- evaluate (-) state --
          dvs(i) = tmp - stepsize
          if ( i.le.ngdv ) then
            call regrid ( (i), jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &            bknot, xOrig, yOrig, dx, dy)
          else if (i.eq.ngdv+1) then
             if (.not.dvmach(rank+1)) then
                alpha = dvs(i)
                cosang = cos(pi*alpha/180.d0)
                sinang = sin(pi*alpha/180.d0)
                uinf   = fsmach*cosang
                vinf   = fsmach*sinang
             else if (dvmach(rank+1)) then
                fsmach = dvs(i)
                cosang = cos(pi*alpha/180.d0)
                sinang = sin(pi*alpha/180.d0)
                uinf   = fsmach*cosang
                vinf   = fsmach*sinang
             end if
          else if ((i.eq.ngdv+2).and.(dvmach(rank+1))) then 
             fsmach = dvs(i)
             cosang = cos(pi*alpha/180.d0)
             sinang = sin(pi*alpha/180.d0)
             uinf   = fsmach*cosang
             vinf   = fsmach*sinang         
          end if

          !-- Calculate new lift coefficient and Reynolds number
          !-- based on updated Mach number design variable
          if (dvmach(rank+1)) then
             H = fsmach/dvmach0 ! Ratio of current mach # to initial
             cl_tar = (H**-2)*cltars(mp)
             re_in = H*reno(mp)            
          end if


c         -- calculate new objective function value (-) --
          call CalcObj (objm,jdim, kdim, ndim, ifirst, indx, icol,iex,q,
     &         cp, xy, xyj, x, y, cp_tar, fmu, vort, turmu, work1, ia,
     &         ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat,
     &         itfirst, obj0, mp, ifun, obj1, lconm)
          write(scr_unit,*)'objm = ', objm

c         -- restore base-state solution --
          do n = 1,ndim
            do k = kbegin,kend
              do j = jbegin,jend
                q(j,k,n) = qs(j,k,n)
              end do
            end do
          end do

c         if ( itmodel .eq. 2 .and. turbulnt ) then
c            do k = kbegin,kend
c               do j = jbegin,jend
c                  turre(j,k) = qs(j,k,5)
c               end do
c            end do
c         end if

          do k = kbegin,kend
            do j = jbegin,jend
              x(j,k) = xs(j,k)
              y(j,k) = ys(j,k)
            end do
          end do

c         if ( itmodel .eq. 2 .and. turbulnt ) then
c           do k = kbegin,kend
c             do j = jbegin,jend
c               turre(j,k) = turres(j,k)
c             end do
c           end do
c         end if

c         -- evaluate gradient --
          grad(i) = (objp - objm) / (2.d0*stepsize)

c         if ( itmodel .eq. 2 .and. turbulnt ) then
c           write (opt_unit,10) i, resid, res_spl
c         else
            write (opt_unit,20) i, resid
c         end if

          dvs(i) = tmp
          if (i.eq.ngdv+1) then
             if (.not.dvmach(rank+1)) then
                alpha = dvs(i)
                cosang = cos(pi*alpha/180.d0)
                sinang = sin(pi*alpha/180.d0)
                uinf   = fsmach*cosang
                vinf   = fsmach*sinang
             else if (dvmach(rank+1)) then
                fsmach = dvs(i)
                cosang = cos(pi*alpha/180.d0)
                sinang = sin(pi*alpha/180.d0)
                uinf   = fsmach*cosang
                vinf   = fsmach*sinang                
             end if
          else if (i.eq.ngdv+2) then
             fsmach = dvs(i)
             cosang = cos(pi*alpha/180.d0)
             sinang = sin(pi*alpha/180.d0)
             uinf   = fsmach*cosang
             vinf   = fsmach*sinang 
          end if
          
c$$$        end do


      end do

#ifdef _MPI_VERSION
      ! syncronize all processors
      call MPI_BARRIER(COMM_CURRENT, ierr)
#endif

      clt = clt_tmp
      cdt = cdt_tmp

 10   format(3x, 'D.V. #', i3,'  CYCLONE Resid.:', e10.3,
     &      '  S-A Resid.:', e10.3)

 20   format(3x, 'D.V. #', i3,'  Flow Resid.:', e10.3)

      return
      end                       !fd_grad
