c----------------------------------------------------------------------
c     -- construct time vector for startup --
c
c     written by: john gatsis
c     modified by Markus Rumpfkeil for TORNADO
c     date: May 2006
c----------------------------------------------------------------------

      subroutine timevec (jdim,kdim,nmax,ks1,ke1,js1,je1,xyj,indx,turre,
     &    rsa,ipa,jpa,pa,nwtn,residfl,dtvc)

      implicit none
      
#include "../include/common.inc"
#include "../include/parms.inc"

c     -- input --

      integer jdim,kdim,nmax,nwtn,indx(jdim,kdim)
      integer ks1,js1,ke1,je1,j,k,jk,n,ir,idiag
      double precision xyj(jdim,kdim),beta
  
      double precision turre(jdim,kdim), rsa(jdim,kdim)

      integer ipa(maxjk*jacb+1), jpa(maxjk*jacb2*5)
      double precision pa(maxjk*jacb2*5)

c     -- output --

      double precision dtvc(5*maxjk)

c     -- local --

      integer unstable
      double precision delta_ref, delta_min,alphat,residfl
      double precision tau,r,Jn,Sn,dnut,nut,tflow,tturb

c     -------------------------
c     -- reference time step --
c     -------------------------
c     reference flow conditions:
c     
c     (1) Not Turbulent (viscious and inviscid)
c     (2) Viscous and Turbulent (Spalart-Allmaras)
c
c     Note: For unsteady run we don't need small timesteps 
c     in order to converge
c     --------------------------------------------


      if (.not.turbulnt) then
c     Not Turbulent (viscious and inviscid)

         delta_min = 5.0d2
         alphat    = 1.0d-4
         beta      = 1.0d0

         if (.not. unsteady .and. nwtn.le.3) then
            delta_ref = 1.0d0
         else if (.not. unsteady .and. nwtn.le.6) then
            delta_ref = 1.0d1
         else if (.not. unsteady .and. nwtn.le.9) then
            delta_ref = 1.0d2
         else if (.not. unsteady .and. nwtn.le.12) then
            delta_ref = 2.5d2 
         else
            delta_ref = max(alphat/residfl**beta,delta_min)
         end if
         
      else
c     Viscous and Turbulent
     
         if (fsmach .lt. 0.4d0 .and. alpha.lt.2.0d0) then
c     aggressive startup timestep

            delta_min = 5.0d1
            alphat = 1.0d-1
            beta = 1.0d0
            r = 0.4d0
            tau = 1.0d0
          
            if (.not. unsteady .and. nwtn.le.5) then
               delta_ref = 1.0d0
            else if (.not. unsteady .and. nwtn.le.10) then
               delta_ref = 2.0d1
            else if (.not. unsteady .and. nwtn.le.15) then
               delta_ref = 3.0d1
            else if (.not. unsteady .and. nwtn.le.20) then
               delta_ref = 5.0d1
            else if (.not. unsteady .and. nwtn.le.25) then
               delta_ref = 1.0d2
            else if (.not. unsteady .and. nwtn.le.30) then
               delta_ref = 3.0d2
            else if (.not. unsteady .and. nwtn.le.35) then
               delta_ref = 7.0d2
            else
               delta_ref = max(alphat/residfl**(beta),delta_min)
            end if
            
         else
c     conservative startup timestep

            delta_min = 6.0d1
            alphat = 1.0d-1
            beta = 1.0d0
            r = 0.4d0
            tau = 1.0d0
            
            if (.not. unsteady .and.nwtn.le.10) then
               delta_ref = 1.0d0
            else if (.not. unsteady .and.nwtn.le.15) then
               delta_ref = 2.0d0
            else if (.not. unsteady .and.nwtn.le.20) then
               delta_ref = 8.0d0
            else if (.not. unsteady .and.nwtn.le.25) then
               delta_ref = 1.5d1
            else if (.not. unsteady .and.nwtn.le.30) then
               delta_ref = 2.0d1
            else if (.not. unsteady .and.nwtn.le.35) then
               delta_ref = 3.0d1
            else if (.not. unsteady .and.nwtn.le.40) then
               delta_ref = 3.5d1
            else if (.not. unsteady .and.nwtn.le.45) then
               delta_ref = 4.0d1
            else if (.not. unsteady .and.nwtn.le.50) then
               delta_ref = 4.5d1
            else if (.not. unsteady .and.nwtn.le.55) then
               delta_ref = 5.5d1
            else
               delta_ref = max(alphat/residfl**(beta),delta_min)
            end if

         end if    !conservative             
         
      end if                    ! turbulnt?

c     ---------------
c     -- time step --
c     ---------------

      unstable = 0
      
      do k=ks1,ke1
         do j=js1,je1

            
            jk = (indx(j,k) - 1)*nmax               
c     mean flow
            tflow = delta_ref / (1.0d0 + dsqrt(xyj(j,k)))
            
            do n=1,4                  
               dtvc(jk+n) = 1.0d0/tflow
            end do
               
c     turbulence
            
            if (viscous .and. turbulnt) then

               ir = jk + nmax
               idiag = 0
               do n = ipa(ir),ipa(ir+1)-1
                  if ( jpa(n).eq.ir ) then
                     idiag = n
                  end if
               end do
               
               Jn = pa(idiag)
               
               Sn = rsa(j,k)
               
               nut = turre(j,k) 

               dnut= Sn/Jn
               
               if (dabs(dnut).gt.r*max(nut,1.0d0)) then 
c              estimated -ve update

                  tturb = Sn/r/max(nut,1.0d0) - Jn
                  tturb = dabs(1.d0/tturb)
                     
                  unstable = unstable + 1
                  
               else             ! proceed with scaled mean flow timestep
                  
                  tturb = tau * tflow
                  
               end if
               
               dtvc(jk+5) = 1.0d0/tturb
               
            end if              ! turbulnt
            
         end do
      end do


      return
      end                       !timevec
