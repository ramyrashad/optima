      subroutine calcmet2(jdim,kdim,xy,x,y,xyj,area)
c                                                                       
c     -subroutine to calculate grid metrics and jacobian
c     -jacobian calculated based on cell area
c
#include "../include/arcom.inc"
c                                                                       
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension area(jdim,kdim)
c
      double precision magA,magB
c
c     -- compute area of each cell --
      do k=1,kup
         kp1=k+1
         do j=1,jup
           jp1=j+1
           vectAx=x(jp1,kp1)-x(j,k)
           vectAy=y(jp1,kp1)-y(j,k)
           vectBx=x(j,kp1)-x(jp1,k)
           vectBy=y(j,kp1)-y(jp1,k)
c
           magA=sqrt(vectAx**2+ vectAy**2)
           magB=sqrt(vectBx**2+ vectBy**2)
c
           product=magA*magB
c           
           dotp      =vectAx*vectBx + vectAy*vectBy
           area(j,k) =0.5d0*product*sin(acos(dotp/product))
           if (area(j,k).le.0.) then
             write (out_unit,*) 'Cell Area(',j,k,')=',area(j,k)
           endif
         enddo
      enddo
c
c     ---- Jacobians according to cell area ----
c
c     --- interior nodes ---
      do k=klow,kup
         km1=k-1
         do j=jlow,jup
            jm1=j-1
c           -take average of four cell areas around node (j,k)
            tmp=0.25d0*(area(jm1,km1) + area(jm1,k) 
     &                    + area(j,k) + area(j,km1))
c           -form jacobian (i.e. inverse of average cell area.
            xyj(j,k)=1.d0/tmp
         enddo
      enddo

c     --- boundaries ---
c     - bcwake and bcbody
      k = 1
      j = 1
      xyj(j,k) = 1.d0/area(j,k)
      
      do j=2,jup
         xyj(j,k) = 1.d0/(0.5d0*(area(j,k) + area(j-1,k)))
      enddo
      
      j=jend
      xyj(j,k) = 1.d0/area(j-1,k)
      
c     - outflow boundary
      j = 1
      do k=2,kup
         xyj(j,k) = 1.d0/(0.5d0*(area(j,k) + area(j,k-1)))
      enddo
      
      j = jend
      do k=2,kup
         xyj(j,k) = 1.d0/(0.5d0*(area(j-1,k) + area(j-1,k-1)))
      enddo
     
c     - farfield boundary
      k  = kend
      km1= k-1
      j  = 1
      xyj(j,k) = 1.d0/area(j,km1)

      do j=2,jend-1
         xyj(j,k) = 1.d0/(0.5d0*(area(j-1,km1) + area(j,km1)))
      enddo
c
      j = jend
      xyj(j,k) = 1.d0/area(j-1,km1)

c---- end of Jacobian calculation according to cell area ----
      
c     -- calculate metrics --
      do k=1,kend
      do j=1,jend
c     -- xix = xy1, xiy = xy2, etax = xy3, etay = xy4
         xy(j,k,1)= xy(j,k,1)*xyj(j,k)
         xy(j,k,2)=-xy(j,k,2)*xyj(j,k)
         xy(j,k,3)=-xy(j,k,3)*xyj(j,k)
         xy(j,k,4)= xy(j,k,4)*xyj(j,k)
c         write(99,*) j,k,xyj(j,k)
      enddo
      enddo
c
c     for `o' mesh and overlapped 1 and jmaxold define jmaxold point      
c                                                                       
      if(periodic .and. jend.ne.jmaxold)then                            
c        -special logic for overlap o grid case                          
c        -set jmaxold point from j = 1 point                              
c                                                                       
         do 910 k = 1,kend                                                 
            xy(jmaxold,k,1) = xy(1,k,1)                                    
            xy(jmaxold,k,2) = xy(1,k,2)                                    
            xy(jmaxold,k,3) = xy(1,k,3)                                   
            xy(jmaxold,k,4) = xy(1,k,4)                                   
            xyj(jmaxold,k) = xyj(1,k)                                   
910      continue                                                         
      endif                                                             
c                                                                       
      return                                                            
      end                                                               
