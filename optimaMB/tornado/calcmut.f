c     ------------------------------------------------------------------
c     -- compute turbulent eddy viscosity (turmu) at 1/2 nodes --
c     -- m. nemec, sept. 2001 -- 
c     -- calling subroutine : get_rhs and nksolve --
c     ------------------------------------------------------------------
      subroutine calcmut( jmax, kmax, jbegin, jend, kbegin, kup, q,
     &     turmu, turre, fmu, xyj)

      implicit none

#include "../include/common.inc"
#include "../include/sam.inc"

      integer jmax,kmax,jbegin,jend,kbegin,kup,j,k,kp1
      double precision q(jmax,kmax,4),turmu(jmax,kmax),xyj(jmax,kmax)
      double precision turre(jmax,kmax),fmu(jmax,kmax)
      double precision rn,chi,chi3,fv1,tur1,tur2

      do k = kbegin,kup
         kp1 = k+1
         do j = jbegin,jend
            rn = turre(j,k)*q(j,k,1)*xyj(j,k)
            chi = rn/fmu(j,k)
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            tur1 = fv1*rn
            
            rn = turre(j,kp1)*q(j,kp1,1)*xyj(j,kp1)
            chi = rn/fmu(j,kp1)
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            tur2 = fv1*rn

            turmu(j,k) = 0.5*(tur1+tur2)
         end do
      end do

      return
      end                       !calcmut
