************************************************************************
      !-- Program name: snCon
      !-- Written by: Howard Buckley
      !-- Date: August 2008
      !-- 
      !-- This subroutine evaluates the thickness constraints and the 
      !-- thickness constraint gradients wrt to the design variables
************************************************************************
      subroutine snCon(sn_x, tcon, dCdx, Status, jdim, kdim, ndim, 
     &     dvs, idv, x, y, bap, bcp, bt, bknot, dvscale, 
     &     dx, dy, xsave, ysave)
      
      !-- Declare variables

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"


      integer
     &     i, j, k, Status, jdim, kdim, ndim, idv(ibsnc),ifirst, mp,
     &     ifun, itfirst, indx(jdim,kdim), icol(9), l, 
     &     iex(jdim,kdim,4), ia(jdim*kdim*ndim+2),
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1),
     &     iat(jdim*kdim*ndim+2),
     &     jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision
     &     dvs(nc+mpopt), sn_x(ibsnc), dvscale(ibsnc), bap(jbody,2),
     &     x(jdim,kdim), y(jdim,kdim), xsave(jdim,kdim), 
     &     ysave(jdim,kdim), bcp(nc,2), bt(jbody),
     &     bknot(jbsord+nc), tcon(ntcon), dCdx(maxtcon, 20),
     &     q(jdim,kdim,ndim), xy(jdim,kdim,4),
     &     xyj(jdim,kdim), xys(jdim,kdim,4), xyjs(jdim,kdim),
     &     xs(jdim,kdim), ys(jdim,kdim), dx(jdim*kdim*incr), 
     &     dy(jdim*kdim*incr), diff, grad(ibsnc), cp(jdim,kdim),
     &     fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim),
     &     as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), pat(jdim*kdim*ndim*ndim*5+1),
     &     cp_tar(jbody,2), obj0

      !----------------------------------------------------------------
      
      !-- Set the Optima2D design variable array 'dvs' equal to the 
      !-- current values of the SNOPT design variables

      do i=1,ndv
         dvs(i) = sn_x(i)
      enddo

      !-- Unscale the design variables

      do i=1,ndv
         dvs(i) = dvs(i)*dvscale(i)
      enddo

      !-- Use current design variables to perturb grid --

      !-- Restore original grid --
      do k = 1,kdim
         do j = 1,jdim
            x(j,k) = xsave(j,k)
            y(j,k) = ysave(j,k)
         end do
      end do

      call regrid( -1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

      !-- Evaluate geometric constraints with perturbed grid

      if (ntcon.gt.0) then

         !-- Evaluate thickness constraints

         call CalcSNTCon(jdim,kdim,y,tcon)

      end if

      if (nrtcon.gt.0) then
         
         !-- Evaluate range thickness constraint

         call CalcSNRTCon(jdim,kdim,y)

      end if

      !-- Evaluate constraint sensitivities wrt current
      !-- value of design variables

      call dCONdX (dCdx, jdim, kdim, ndim, dvs, idv, x, y, bap, 
     &            bcp, bt, bknot, xsave, ysave, dx, dy)
      print*,'unscaled dCdx=',dCdx
      return

      end ! snCon

************************************************************************
      !-- Program name: dCONdx
      !-- Written by: Howard Buckley 
      !-- Date: August 2008
      !-- 
      !-- This subroutisne evaulates the gradient of the thickness 
      !-- constraints wrt the design variables
************************************************************************

      subroutine dCONdX (dCdx, jdim, kdim, ndim, dvs, idv, x, y, bap,
     &             bcp, bt, bknot, xsave, ysave, dx, dy)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"


      integer 
     &     in, i, jdim, kdim, ndim, idv(nc+mpopt)

      double precision 
     &     tmp, stepsize, tconp(ntcon),
     &     tconm(ntcon), dCdx(maxtcon,20),
     &     x(jdim,kdim), y(jdim,kdim), bap(jbody,2), bcp(nc,2), 
     &     bt(jbody), dvs(ibsnc),
     &     bknot(jbsord+nc), tcon(ntcon),
     &     xsave(jdim,kdim), ysave(jdim,kdim),dx(jdim*kdim*incr),
     &     dy(jdim*kdim*incr), maxrthcp, maxrthcm

      if (ntcon.gt.0) then

         !-- Evaluate gradient of thickness constraint using 2nd order
         !-- centered differences

         do in = 1,ngdv
               
            tmp = dvs(in)
            stepsize = abs(fd_eta*dvs(in))

            !-- check for tiny stepsize --


            if ( abs(stepsize).lt.1.d-10 ) 
     &        stepsize = 1.d-10*sign(1.d0,stepsize)

            !-- Calculate CON at +ve stepsize

            dvs(in) = dvs(in) + stepsize

            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNTCon (jdim, kdim, y, tconp)

            !-- Calculate CON at -ve stepsize

            dvs(in) = tmp - stepsize
      
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNTCon (jdim, kdim, y, tconm)

            !-- restore dvs(in) and calculate 2nd order difference

            dvs(in) = tmp  

            do i=1,ntcon
               dCdx(i,in) = (tconp(i) - tconm(i)) / (2.d0*stepsize)

               print*,'i=',i
               print*,'in=',in
               print*,'tconp(i)=',tconp(i)
               print*,'tconm(i)=',tconm(i)
               print*,'stepsize=',stepsize
               print*,'dCdx(i,in)=',dCdx(i,in)

            enddo

         enddo

      end if

      if (nrtcon.gt.0) then

         !-- Evaluate gradient of range thickness constraint using 2nd 
         !-- order centered differences

         do in = 1,ngdv
               
            tmp = dvs(in)
            stepsize = abs(fd_eta*dvs(in))

            !-- check for tiny stepsize --

            if ( abs(stepsize).lt.1.d-10 ) 
     &        stepsize = 1.d-10*sign(1.d0,stepsize)

            !-- Calculate range thickness constraint value at +ve 
            !-- stepsize

            dvs(in) = dvs(in) + stepsize

            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNRTCon(jdim,kdim,y)

            maxrthcp = maxrthc

            !-- Calculate CON at -ve stepsize

            dvs(in) = tmp - stepsize
      
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNRTCon(jdim,kdim,y)

            maxrthcm = maxrthc

            !-- restore dvs(in) and calculate 2nd order difference

            dvs(in) = tmp  

            do i=ntcon+1,ntcon+nrtcon
               dCdx(i,in) = (maxrthcp - maxrthcm) / (2.d0*stepsize)

               print*,'i=',i
               print*,'in=',in
               print*,'maxrthcp=',maxrthcp
               print*,'maxrthcm=',maxrthcm
               print*,'stepsize=',stepsize
               print*,'dCdx(i,in)=',dCdx(i,in)

            enddo  

         enddo

      end if

      !-- dCdx (where C is thickness) with respect to AOA is zero --

      if (ndv .gt. ngdv) then
         do in = ngdv + 1, ndv
            do i=1,ntcon+nrtcon
               dCdx(i,in) = 0.d0
            enddo
         enddo
      endif

      return

      end ! dCONdx

************************************************************************
      !-- Program name: CalcSNTCon
      !-- Written by: Howard Buckley 
      !-- Date: August 2008
      !-- 
      !-- This subroutine evaulates the thickness constraints at the
      !-- current design variables
************************************************************************

      subroutine CalcSNTCon (jdim, kdim, y, tcon)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer
     &     jdim, kdim, i

      double precision
     &     tcon(maxtcon), y(jdim,kdim)

      !-- Evaluate airfoil thickness at each thickness constraint
      !-- location

      do i = 1, ntcon

         tcon(i) = y(juptx(i),1) - y(jlotx(i),1)

      end do

      return
 
      end ! CalcSNTCon

************************************************************************
      !-- Program name: CalcSNRTCon
      !-- Written by: Howard Buckley 
      !-- Date: Feb 2009
      !-- 
      !-- This subroutine evaulates the range thickness constraint at the
      !-- current design variables
************************************************************************
      subroutine CalcSNRTCon(jdim,kdim,y)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim,kdim
      double precision y(jdim,kdim)

      integer i

      maxrthc = 0.0
      maxrthctemp = 0.0

      do i=1,nrtcon

         do subi = 1, crtxn(i)+1

            maxrthctemp = y(juprtxsub(i,subi),1) - y(jlortxsub(i,subi
     &           ),1)

            if (maxrthc .lt. maxrthctemp) then

               crth(i) = maxrthctemp
               crthx(i) = crtxsub(i,subi)
               maxrthc = maxrthctemp

            end if

         end do
  
      end do

      return
      end                       !CalcRTCon
