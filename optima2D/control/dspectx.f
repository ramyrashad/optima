c----------------------------------------------------------------------
c     -- differentiation of spectral radius for JST dissipation --
c     written by: marian nemec
c     date: jan. 2001
c----------------------------------------------------------------------

      subroutine dspectx (jdim, kdim, ndim, indx, icol, q, xy, xyj, uu,
     &     sndsp, as, dspdq, work, coef2, coef4, pcoef, spect, c2, c4,
     &     db, bm1, bp1)

#include "../include/arcom.inc"

      integer jdim, kdim, ndim, indx(jdim,kdim), icol(9),jsta,jsto
      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
clb
      double precision xyj(jdim,kdim)
      double precision sndsp(jdim,kdim), dspdq(4,jdim,kdim)
      double precision work(jdim,kdim,2,4), duudq(3), uu(jdim,kdim)
      double precision coef2(jdim,kdim), coef4(jdim,kdim)
      double precision pcoef(jdim,kdim), spect(jdim,kdim)
      double precision c2(jdim,kdim), c4(jdim,kdim)
      double precision db(4,4,jdim,kdim), bm1(4,4,jdim,kdim)
      double precision bp1(4,4,jdim,kdim)

c     -- form derivative of spectral radius at node j,k --
c     -- first: take derivative of the speed of sound -- 
      g2  = gamma*gami
      do k = kbegin,kend
         do j = jbegin,jend

            ri  = 1.d0/q(j,k,1)
            ri2 = ri*ri

            t1 = 0.5d0/sndsp(j,k)
            t2 = q(j,k,2)*q(j,k,2)
            t3 = q(j,k,3)*q(j,k,3)

            dadq1 = g2*ri2*( (t2+t3)*ri - q(j,k,4) )
            dadq2 = - g2*q(j,k,2)*ri2
            dadq3 = - g2*q(j,k,3)*ri2
            dadq4 = g2*ri

            met = dsqrt( xy(j,k,1)**2+xy(j,k,2)**2 )*t1

            dspdq(1,j,k) = dadq1*met 
            dspdq(2,j,k) = dadq2*met 
            dspdq(3,j,k) = dadq3*met
            dspdq(4,j,k) = dadq4*met
         end do
      end do      

c     -- second: take derivative of contravariant velocity --
      if (.not. frozen .or. ( frozen .and. ifrz_what.eq.2 ) ) then
         do k = kbegin,kend
            do j = jbegin,jend

               ri  = 1.d0/q(j,k,1)
               ri2 = ri*ri
               duudq(1) = - ri2*(q(j,k,2)*xy(j,k,1) +q(j,k,3)*xy(j,k,2))
               duudq(2) = ri*xy(j,k,1) 
               duudq(3) = ri*xy(j,k,2)
               
               if ( uu(j,k) .lt. 0.d0 ) then
                  do n = 1,3
                     duudq(n) = - duudq(n)
                  end do
               end if
               dspdq(1,j,k) = duudq(1) + dspdq(1,j,k)
               dspdq(2,j,k) = duudq(2) + dspdq(2,j,k)
               dspdq(3,j,k) = duudq(3) + dspdq(3,j,k)
            end do
         end do      
      end if

c     -- form dissipation differences --

      do n = 1,4                                                     
c     -- 1st-order forward difference --
         do k =klow,kup                                                
            do j =jlow,jup
               jp1=jplus(j)
               work(j,k,1,n) = q(jp1,k,n)*xyj(jp1,k) - q(j,k,n)*xyj(j,k)
            end do
         end do

         if(.not.periodic) then

c     -- c mesh bc --
         j1 = jbegin                                                    
         j2 = jend                                                      
         do k = klow,kup                                             
            work(j1,k,1,n) = q(j1+1,k,n)*xyj(j1+1,k)-q(j1,k,n)*xyj(j1,k)
            work(j2,k,1,n) = work(j2-1,k,1,n)
         end do

         end if

c     -- apply cent-dif to 1st-order forward --
         do k =klow,kup                                                
            do j =jlow,jup
               jp1=jplus(j)
               jm1=jminus(j)
               work(j,k,2,n) = work(jp1,k,1,n) - 2.d0*work(j,k,1,n) +
     &              work(jm1,k,1,n) 
            end do
         end do

         if(.not.periodic) then

c     -- c mesh bc --
         j1 = jbegin                                                    
         j2 = jend                                                      
         do k =klow,kup                                             
            work(j1,k,2,n) = q(j1+2,k,n)*xyj(j1+2,k) - 2.d0*q(j1+1,k,n)
     &           *xyj(j1+1,k) + q(j1,k,n)*xyj(j1,k)         
            work(j2,k,2,n) = 0.d0
         end do

         end if

      end do

c     -- taken from coef24x.f --

      do k = kbegin,kend                                              
         do j = jbegin,jend
            pcoef(j,k) = coef2(j,k)
            spect(j,k) = coef4(j,k)
         end do
      end do

      do k = kbegin,kend                                            
         do j = jbegin,jup                                         
            jp = jplus(j)
            c2(j,k) = dis2x*(pcoef(jp,k)*spect(jp,k) +
     &           pcoef(j,k)*spect(j,k))
            coef2(j,k) = c2(j,k)
            c4(j,k) = dis4x*(spect(jp,k) + spect(j,k))
            coef4(j,k) = c4(j,k) - min( c4(j,k),c2(j,k) )
         end do
      end do

c     -- linearize c4 = c4 - min(c4,c2) --
c     -- pressure switch (pcoef) is treated as constant --
      do k = klow,kup
         do j = jlow,jup

            jm = jminus(j)
            jp = jplus(j)

            pcm = dis2x*pcoef(jm,k)/xyj(jm,k)
            pc  = dis2x*pcoef(j,k)/xyj(j,k)
            pcp = dis2x*pcoef(jp,k)/xyj(jp,k)

            d4m = dis4x/xyj(jm,k)
            d4  = dis4x/xyj(j,k)
            d4p = dis4x/xyj(jp,k)

            if ( c4(j,k) .le. c2(j,k) ) then
c     -- no O(3) dissipation: c4 = 0 --
               do n = 1,4
                  do m = 1,4
                     bm1(m,n,j,k) = pcm*dspdq(m,jm,k)*work(jm,k,1,n)
                     db(m,n,j,k) = - pc*dspdq(m,j,k)*(work(j,k,1,n) -
     &                    work(jm,k,1,n))
                     bp1(m,n,j,k) = - pcp*dspdq(m,jp,k)*work(j,k,1,n)
                  end do
               end do
            else
c     -- mostly O(3) dissipation: c4 = c4 - c2 --
               do n = 1,4
                  do m = 1,4
                     bm1(m,n,j,k) = - d4m*dspdq(m,jm,k)*work(jm,k,2,n) +
     &                    pcm*dspdq(m,jm,k)*work(jm,k,1,n)  
                     db(m,n,j,k) = d4*dspdq(m,j,k)*(work(j,k,2,n) -
     &                    work(jm,k,2,n) ) - pc*dspdq(m,j,k)*( work(j,k
     &                    ,1,n)-work(jm,k,1,n) ) 
                     bp1(m,n,j,k) = d4p*dspdq(m,jp,k)*work(j,k,2,n) -
     &                    pcp*dspdq(m,jp,k)*work(j,k,1,n) 
                  end do
               end do
            end if
         end do
      end do

c     -- add to jacobian --

      if(.not.periodic) then
         jsta=jlow+1
         jsto=jup-1
      else
         jsta=jlow
         jsto=jup
      end if

      do k = klow+1,kup-1

         do j = jsta,jsto

            itmp = ( indx(j,k) - 1 )*ndim

            do n = 1,4 

               ij = itmp + n
               ii = ( ij - 1 )*icol(9)
               if (clopt) ii = ( ij - 1 )*(icol(9)+1)

               do m =1,4
                  as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
                  as(ii+icol(7)+m) = as(ii+icol(7)+m) + bp1(m,n,j,k)
               end do
            end do
         end do

      end do  

      if(.not.periodic) then

      j = jlow
      do k = klow+1,kup-1
         itmp = ( indx(j,k) - 1 )*ndim
         do n = 1,4 

            ij = itmp + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)

            do m =1,4
               as(ii+        m) = as(ii+        m) + bm1(m,n,j,k) 
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k)
            end do
         end do
      end do

      end if

      k = klow

      do j = jsta,jsto

         itmp = ( indx(j,k) - 1 )*ndim
         do n = 1,4 

            ij = itmp + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)

            do m =1,4
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k) 
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k)
            end do
         end do
      end do


      if(.not.periodic) then

      j = jup
      do k = klow+1,kup-1
         itmp = ( indx(j,k) - 1 )*ndim
         do n = 1,4 

            ij = itmp + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)

            do m =1,4
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)  
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
               as(ii+icol(7)+m) = as(ii+icol(7)+m) + bp1(m,n,j,k) 
            end do
         end do
      end do

      end if

      k = kup

      do j = jsta,jsto

         itmp = ( indx(j,k) - 1 )*ndim
         do n = 1,4 

            ij = itmp + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)

            do m =1,4
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k) 
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k) 
            end do
         end do
      end do

      if(.not.periodic) then

      j = jlow
      k = klow
      itmp = ( indx(j,k) - 1 )*ndim
      do n = 1,4 

         ij = itmp + n
         ii = ( ij - 1 )*icol(9)
         if (clopt) ii = ( ij - 1 )*(icol(9)+1)

         do m =1,4
            as(ii+        m) = as(ii+        m) + bm1(m,n,j,k) 
            as(ii+icol(2)+m) = as(ii+icol(2)+m) + db(m,n,j,k)
            as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
         end do
      end do

      j = jlow
      k = kup
      itmp = ( indx(j,k) - 1 )*ndim
      do n = 1,4 

         ij = itmp + n
         ii = ( ij - 1 )*icol(9)
         if (clopt) ii = ( ij - 1 )*(icol(9)+1)

         do m =1,4
            as(ii+        m) = as(ii+        m) + bm1(m,n,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
            as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
         end do
      end do
      
      j = jup
      k = klow
      itmp = ( indx(j,k) - 1 )*ndim
      do n = 1,4 

         ij = itmp + n
         ii = ( ij - 1 )*icol(9)
         if (clopt) ii = ( ij - 1 )*(icol(9)+1)

         do m =1,4
            as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
            as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k)
         end do
      end do

      j = jup
      k = kup
      itmp = ( indx(j,k) - 1 )*ndim
      do n = 1,4 

         ij = itmp + n
         ii = ( ij - 1 )*icol(9)
         if (clopt) ii = ( ij - 1 )*(icol(9)+1)

         do m =1,4
            as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
            as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k)
         end do
      end do


      end if

      return
      end                       !dspectx
