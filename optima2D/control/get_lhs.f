c----------------------------------------------------------------------
c     -- construct dR/dQ and preconditioner --
c     
c     written by: marian nemec
c     date: august 2000
c----------------------------------------------------------------------
      subroutine get_lhs (jdim, kdim, ndim, indx, icol, iex, ia, ja,
     &      as, ipa, jpa, pa, xy, xyj, x, y, q, press, sndsp, fmu,
     &      turmu, uu, vv, ccx, ccy, coef2, coef4, tmet, precon, bcf,
     &      work1, work2, prec_dc,akk,imps)

#include "../include/arcom.inc"

      integer jdim, kdim, ndim
      integer indx(jdim,kdim), icol(9), iex(jdim,kdim,4)
      integer ia(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
   
      double precision bcf(jdim,kdim,4)
      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision fmu(jdim,kdim), turmu(jdim,kdim)
      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), spect(jdim,kdim,3)

      double precision dcltdq(jdim,kdim,4) 
      double precision drdalph(jdim,kdim,ndim),tmp(ndim)

      double precision work1(maxjk,18)
      double precision work2(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision astemp(jdim*kdim*ndim), onoff(jdim*kdim*ndim)

      double precision akk
      logical imps

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c  indx is the index of each node, assigned in subroutine order.
c  iex is used to re-order the left and right hand sides of the equation
c  based on the value.  iex(j,k,n) will switch equations for node j,k in
c  the n dimension.
c  The jacobian and preconditioner are stored in compressed sparse row 
c  format.
c  ia and ipa contain the number of values in each row, until "sorted", 
c  then they contain the number of the element that begins each row, of 
c  the jacobian and preconditioner respectively.
c  ja and jpa contain the column number in which each element is stored,
c  for jacobian and preconditioner respectively.
c  ia, ipa, ja, and jpa are all set in set_mat.f
c  as and pa contain the values stored in the jacobian and 
c  preconditioner, respectively.
c  dcltdq(j,k,n) contains the partial derivative of the coefficient of 
c  lift with respect to the flow variable q(j,k,n).
c  drdalph(j,k,n) contains the partial derivative of the residual 
c  (j,k,n) with respect to the angle of attack.
c  astemp and onoff are variables used for re-ordering the new row in 
c  the jacobian, using iex(j,k,n).

      na = jmax*kdim*ndim
      nas = jmax*kdim*ndim*ndim*9
      nap = jmax*kdim*ndim*ndim*5
     
      if (clalpha2 .or. clopt) then
cmpr         na=na+1
         nap=nap+1
         nas = jmax*kdim*ndim*(ndim*9+1)+jmax*ndim*3+1
      end if

c     -- zero sparse jacobian matrix as and transpose matrix ast --
      if ( jac_mat ) then
        do j = 1,nas
          as(j) = 0.d0
        end do
      end if

c     -- zero preconditioner matrix pa and transpose matrix pat --
      if ( prec_mat ) then
        do j = 1,nap
          pa(j) = 0.d0
        end do 
      end if

      if (jac_mat .and. clopt) then
         do j=1,jdim*kdim*ndim
            onoff(j) = 0
         end do
      end if

c     -- initialize row index to keep track of row shuffling --
      do n = 1,4
        do k = kbegin,kend
          do j = jbegin,jend        
            iex(j,k,n) = n
          end do
        end do
      end do

c     -- memory pointers --
c     -- work2 = jdim*kdim*ndim*ndim*9 = jdim*kdim*ndim*45 for S-A 
c     model cases -- 
c     -- present memory use is jdim*kdim*ndim*30 so the next available
c     memory is at jdim*kdim*ndim*31 --
  
      ip1 = jmax*kdim*ndim*4
      ip2 = jmax*kdim*ndim*3 + ip1
      ip3 = jmax*kdim*ndim
     
c     -- form second order flow jacobian and preconditioner --
      call dRdQ (jdim, kdim, ndim, indx, icol, iex, as, pa, xy, xyj, x,
     &     y, q, press, sndsp, fmu, turmu, uu, vv, ccx, ccy, coef2,
     &     coef4, tmet, precon, work1(1,1), work1(1,15), work1(1,16),
     &     work1(1,17), work2(1), work2(ip1+1), work2(ip2+1),
     &     work2(ip2+ip3+1),   work2(ip2+2*ip3+1), work2(ip2+3*ip3+1),
     &     work2(ip2+4*ip3+1), work2(ip2+5*ip3+1), work2(ip2+6*ip3+1),
     &     work2(ip2+7*ip3+1), work2(ip2+8*ip3+1), work2(ip2+9*ip3+1),
     &     work2(ip2+10*ip3+1),work2(ip2+11*ip3+1),work2(ip2+12*ip3+1),
     &     work2(ip2+13*ip3+1),work2(ip2+18*ip3+1),bcf,prec_dc,akk,imps)

clb   --------- Second order flow jacobian ---------

      if ( jac_mat .and. clopt) then

         call drdalpha (jdim, kdim, ndim, x, y, xy, xyj, q, press,
     &        drdalph)
         j=1
         do k = 1,kdim
            do n = 1,4
               tmp(n) = drdalph(j,k,iex(j,k,n))*1.d2
            end do
            do n=1,4
               jk  = ( indx(j,k) - 1 )*ndim + n
               jj = ( jk - 1 )*(icol(9)+1)
               as(jj+icol(1)+ndim+1)=tmp(n)
            end do
         end do

         j=jdim
         do k = 1,kdim
            do n = 1,4
               tmp(n) = drdalph(j,k,iex(j,k,n))*1.d2
            end do
            do n = 1,4
               jk  = ( indx(j,k) - 1 )*ndim + n
               jj = ( jk - 1 )*(icol(9)+1)
               as(jj+icol(1)+ndim+1)=tmp(n)
            end do
         end do

         k = kdim
         do j = 2,jdim-1
            do n = 1,4
               tmp(n) = drdalph(j,k,iex(j,k,n))*1.d2
            end do
            do n = 1,4
               jk  = ( indx(j,k) - 1 )*ndim + n
               jj = ( jk - 1 )*(icol(9)+1)
               as(jj+icol(1)+ndim+1)=tmp(n)
            end do
         end do      

      end if

      if ( jac_mat ) then
c     -- sort jacobian array --
        jcol = 1
        do ii = 1,na
          jj = ( ii-1 )*icol(9)
          if (clopt) jj = (ii-1)*(icol(9)+1)
          jdel = ia(ii+1) - ia(ii)
          do j = jj+1,jj+jdel
            as(jcol) = as(j)
            jcol = jcol + 1
          end do
        end do
      end if

      if ( jac_mat .and. clopt) then
         nscal = 0
clb   Add term for dcldq to second-order Jacobian
         call dcldq(jdim,kdim,q,press,x,y,xy,xyj,                    
     &        nscal, dcltdq)
         do k=1,3
            do j=jtail1,jtail2
               do n = 1,4
                  jk  = ( indx(j,k) - 1 )*ndim + n
                  astemp(jk) = dcltdq(j,k,n)
                  onoff(jk) = 1
               end do
            end do
         end do
         do j = 1,jdim*kdim*ndim
            if (onoff(j).eq.1) then
               as(jcol) = astemp(j)
               jcol = jcol+1
            end if
         end do 
clb   Add term for dcldalpha to second-order Jacobian
         call dcldalpha(jdim,kdim,q,press,x,y,xy,xyj,
     &        0, dclidalpha, dclvdalpha,cl_curr)
         dcltdalpha = dclidalpha + dclvdalpha
         as(jcol)= dcltdalpha*1.d2
      end if

clb   --------- First order preconditioner ---------
      if ( prec_mat ) then
c     -- sort preconditioner array --
        jcol = 1
        do ii = 1,na
          jj = ( ii-1 )*icol(5)
          jdel = ipa(ii+1) - ipa(ii)
          do j = jj+1,jj+jdel
            pa(jcol) = pa(j)
            jcol = jcol + 1
          end do
        end do
      end if

      if ( prec_mat .and. (clalpha2.or.clopt) ) then
clb   Add term for dcldalpha to first-order Jacobian
         call dcldalpha(jdim,kdim,q,press,x,y,xy,xyj,
     &         0, dclidalpha, dclvdalpha,cl_curr)
         dcltdalpha=dclidalpha+dclvdalpha
         pa(jcol)=dcltdalpha*1.d2
      endif

      return	
      end                       !get_lhs
