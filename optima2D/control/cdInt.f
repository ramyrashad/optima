************************************************************************
      !-- Program name: cdInt
      !-- Written by: Howard Buckley
      !-- Date: Aug 2010
      !-- 
      !-- This subroutine evaluates the integral of Cd over a range of
      !-- Cl and Mach numbers. The integral is approximated using 
      !-- a 2D traezoidal qudarature rule. 
************************************************************************


      subroutine cdInt (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &     ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, 
     &     qp, qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,
     &      a_jk,ifunc)

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      
      !-- Declare Optima2D variables

      integer 
     &     j, k, ifunc, ifun, jdim, kdim, ndim, ifirst, icol(9), EOF,
     &     indx(jdim,kdim), iex(jdim,kdim,ndim),
     &     ia(jdim*kdim*ndim+2), namelen,
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1),
     &     iat(jdim*kdim*ndim+2),
     &     jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1),
     &     fTotal, ii, i, ondpTotal


      double precision 
     &     q(jdim,kdim,ndim), sigma, mean, G0, a, h,
     &     cp(jdim,kdim), xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim),
     &     y(jdim,kdim),cp_tar(jbody,2), fmu(jdim,kdim),vort(jdim,kdim),
     &     turmu(jdim,kdim),as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), pat(jdim*kdim*ndim*ndim*5+1),
     &     work1(jdim*kdim,100), alpha_neg, alpha_pos, clt_neg, clt_pos,
     &     qp(maxjk,nblk), qold(maxjk,nblk), D_hat(maxjk,nblk,6),
     &     exprhs(maxjk,nblk), a_jk(6,6),delcl, delm, cdInteg,
     &     cdarray(nm,nwt,nalt), Z(nm,ncl), G(nm,ncl), cdInteg_Z, 
     &     cdInteg_G, cdAvg, DLinteg, DLavg,
     &     delwt, delalt, mrange, wtrange, altrange,
     &     wt_int_m(nm*nwt*nalt), wt_int_cl(nm*nwt*nalt),
     &     wt_int_re(nm*nwt*nalt), int_wts(nm*nwt*nalt),
     &     int_wts2(nm*nwt*nalt)


      call wt_int_dps(int_wts, wt_int_m, wt_int_cl, ondpTotal,
     &     delm, delwt, delalt, wt_int_re, int_wts2)

      mrange = mup - mlow
      wtrange = wtup - wtlow
      altrange = altup - altlow   

      open(unit=433,file='cdInt.out',status='new',
     &         form='formatted')

      write (433,9172)
 9172 format
     &     (3x,'Objective function: Integral of Cd over range of',
     &     /3x,'operating conditions')
      write(433,9173) wtlow, wtup
 9173 format(/6x, 'Aircraft Weight Range: ',f8.1,' - ',f8.1)
      write(433,9174) delwt
 9174 format(6x, 'Aircraft Weight Interval Spacing: ',f8.2)
      write(433,9175) mlow, mup
 9175 format(/6x, 'Cruise Mach # Range: ',f4.2,' - ',f4.2)
      write(433,9176) delm
 9176 format(6x, 'Cruise Mach # Interval Spacing: ',f6.4)
      write(433,9177) altlow, altup
 9177 format(/6x, 'Cruise Altitude Range: ',f8.1,' - ',f8.1)
      write(433,9178) delalt
 9178 format(6x, 'Cruise Altitude Interval Spacing: ',f8.2,/)

      !-- Total number of flow solves required
      fTotal = nm*nwt*nalt

      write(433,4) fTotal
 4    format(/3x, 'Total number of flow solves required: ', i4)
      write(433,*)''

      !-- Zero the D/L integral
      DLinteg = 0.0
 
      ii = 1
     
      do 15 i = 1,nm
      do 15 j = 1,nwt
      do 15 k = 1,nalt

         write(scr_unit,*)'i,j,k = ',i,j,k
         write(scr_unit,*)'wt_int_m = ',wt_int_m(ii)
         write(scr_unit,*)'wt_int_cl = ',wt_int_cl(ii)
         write(scr_unit,*)'wt_int_re = ',wt_int_re(ii)
         write(scr_unit,*)'int_wts(ii) = ',int_wts(ii)
         write(scr_unit,*)''


         fsmach = wt_int_m(ii)
         cl_tar = wt_int_cl(ii)
         re_in  = wt_int_re(ii)

         call flow_solve(jdim, kdim, ndim, ifirst, indx, icol, iex,
     &        ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q,
     &        qp, qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,
     &        a_jk,ifunc)
         
         ifirst = 1 !-- Cold-start every flow solve
         
         !-- Store Cd value in an array
         cdarray(i,j,k) = cdt

         !-- Calculate the integral of DL over the range of operating 
         !-- conditions   
         DLinteg = DLinteg + cdt*int_wts(ii)

         ii = ii + 1
         
 15   continue

      !-- Final operation in trapezoidal integral approximation
      !-- Also calc avg Cd over range of operating conditions
      if (nm.gt.1.and.nwt.gt.1.and.nalt.gt.1) then
         DLinteg = 0.125*delm*delwt*delalt*DLinteg
         DLavg = DLinteg/(mrange*wtrange*altrange)
      else if (nm.le.1.and.nwt.gt.1.and.nalt.gt.1) then
         DLinteg = 0.25*delwt*delalt*DLinteg
         DLavg = DLinteg/(wtrange*altrange)
      else if (nm.gt.1.and.nwt.le.1.and.nalt.gt.1) then
         DLinteg = 0.25*delm*delalt*DLinteg
         DLavg = DLinteg/(mrange*altrange)
      else if (nm.gt.1.and.nwt.gt.1.and.nalt.le.1) then
         DLinteg = 0.25*delm*delwt*DLinteg
         DLavg = DLinteg/(mrange*wtrange)
      else if (nm.gt.1.and.nwt.le.1.and.nalt.le.1) then
         DLinteg = 0.5*delm*DLinteg
         DLavg = DLinteg/(mrange)
      else if (nm.le.1.and.nwt.gt.1.and.nalt.le.1) then
         DLinteg = 0.5*delwt*DLinteg
         DLavg = DLinteg/(wtrange)
      else if (nm.le.1.and.nwt.le.1.and.nalt.gt.1) then
         DLinteg = 0.5*delalt*DLinteg
         DLavg = DLinteg/(altrange)
      else if (nm.le.1.and.nwt.le.1.and.nalt.le.1) then
         DLinteg = 1.0*DLinteg
         DLavg = DLinteg
      end if


      write(scr_unit,8) wt_func
 8    format
     & (//3x, 'Designer-priority weighting function is: ', i3,//)
    
      write(scr_unit,9) DLinteg
 9    format
     & (//3x, 'The approximate weighted integral of D/L is: ', e12.6,//)

      !-- The CDAVG calculation only makes sense if designer-priority
      !-- function assigns equal importance to all operating conditions
      !-- This is the case for WT_FUNC = 0
      if(wt_func.eq.0) then
         write(scr_unit,10) DLavg
 10      format
     &(//3x,'Avg value of D/L over range of op conditions is: ',e12.6,/)
      end if
      !-- Write out the values of cdarray to file
      call system('rm -f cdarray.out')
      open(unit=432,file='cdarray.out',status='new',
     &         form='formatted')
      do i = 1,nm
         write(432,11) cdarray(i,1:nwt,1:nalt)
      end do
 11   format(400f10.6)
      
      close(432)


      write(433,*)'OMGA   DLINTEG           DLAVG'
      write(433,12) omga, DLinteg, DLavg
 12   format(f4.1,3x,e14.7,3x,f16.7)

      stop 
      
      return
      
      end
            


      
      
      
      

      
       

