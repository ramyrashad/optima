C ********************************************************************  
C ********************* BOUNDARY CONDITIONS **************************  
C ********************************************************************  
      subroutine bcwake(JDIM,KDIM,Q,KBC,XY,XYJ,jbegin,jend)             
C                                                                       
#include "../include/common.inc"
c                                                                       
      DIMENSION Q(JDIM,KBC,4)                                           
      DIMENSION XY(JDIM,KBC,4),XYJ(JDIM,KBC)                          
C                                                                       
C --------------------------------------------------------------------  
C --------------------------------------------------------------------  
C --         BOUNDARY CONDITIONS                                   ---  
C --------------------------------------------------------------------  
C --------------------------------------------------------------------  
C   BC FOR WAKE CUT 
C                                                                       
C  PERMEABLE BOUNDARY CONDITION  (WAKE)                   
C                                                                       
C  AVERAGE PRESSURE ACROSS WAKE CUT                                     
C --------------------------------------------------------------------  
c ********************************************************************
c For arrays q,xy,xyj the data are as follows:
c       k=1  data to left of cut 
c       k=2  result  
c       k=3  data to right of cut
c ********************************************************************
      DO 20 J=JBEGIN,JEND                                         
        RR = 1./XYJ(J,2)                                          
        QSAV = 0.5*(Q(J,1,1)*XYJ(J,1)+Q(J,3,1)*XYJ(J,3))     
        Q(J,2,1) = QSAV*RR                                      
c
        QSAV = 0.5*(Q(J,1,2)*XYJ(J,1)+Q(J,3,2)*XYJ(J,3))   
        Q(J,2,2) = QSAV*RR                                    
c 
        QSAV = 0.5*(Q(J,1,3)*XYJ(J,1)+Q(J,3,3)*XYJ(J,3)) 
        Q(J,2,3) = QSAV*RR
c
        PPPL = GAMI*(Q(J,1,4) -
     &    0.5*(Q(J,1,2)**2+Q(J,1,3)**2)/Q(J,1,1))*XYJ(J,1)   
        PPPR = GAMI*(Q(J,3,4) -                                    
     *    0.5*(Q(J,3,2)**2+Q(J,3,3)**2)/Q(J,3,1))*XYJ(J,3)   
        PSAV = 0.5*(PPPR+PPPL)                                     
        Q(J,2,4) = PSAV/GAMI*RR +                                 
     *    0.5*(Q(J,2,2)**2+Q(J,2,3)**2)/Q(J,2,1)          
   20 CONTINUE                                                    
c                                                                       
      return                                                           
      end                                                              
