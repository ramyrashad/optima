c----------------------------------------------------------------------
c     -- generate b-spline curves, modify grid and define design
c     variables --
c     -- m. nemec --
c----------------------------------------------------------------------
      subroutine modify( x, y)

      implicit none

#include "mxblk.inc"
#include "mxfoil.inc"
#include "mxseg.inc"
#include "mxjk.inc"
#include "bsp.inc"
#include "units.inc"

      integer i, j, ifoil, iseg, is, ie, ipoints, ile, maxjk
      double precision xle, yle, dist, dist2, xs, xe
      parameter (maxjk=maxj*maxk)
      double precision ap(ibody,2), cp(ibsnc,2), t(ibody), knot(ibskt)
      double precision x(maxjk), y(maxjk), work(ibody,10)
      logical flag

      integer nfoils, nsegments(maxfoil), isegblk(maxsegs,maxfoil)
      integer isegside(maxsegs,maxfoil)
      common/sortedfoils/ nsegments, isegblk, isegside, nfoils

      integer lblkpt(maxblk,4), lsidept(maxblk,4), idir(maxblk,4)
      integer ibctype(maxblk,4), ibcdir(maxblk,4)
      common/connect/ lblkpt, lsidept, idir, ibctype, ibcdir

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      integer ndv, nbdv, ntdv, idvi(ibsnc), idvs(ibsnc), idvf(ibsnc)
      double precision dvs(ibsnc)
      common/dvinfo/ dvs, idvi, idvs, idvf, ndv, nbdv, ntdv
      
      logical rep

      ndv = 0
      do ifoil=1,nfoils
         ipoints = ibap(ifoil)

         write (*,5) ifoil, ipoints
 5       format (/3x,'Working on element',i3,', with',i4,
     &        ' surface nodes ...')

c     -- tecplot output --
         if (tecplot) then
            write (btec_unit,10)
            write (btec_unit,100) (bap(is,1,ifoil), bap(is,2,ifoil), 
     &           is=1, ipoints)
         end if
 10      format ('ZONE T="Original Points"')

c     -- find leading edge --
         do i = 1, ipoints
            work(i,1) = bap(i,1,ifoil)
            work(i,2) = bap(i,2,ifoil)
         end do
         call findLE (ipoints, work, ibody, xle, yle, ile)
         write (*,30) xle, yle, ile
 30      format (3x,'Leading edge (x,y):',2e12.4,4x,'Index:',i4)

         do iseg=1,nbseg(ifoil)

            write (*,20) ifoil, iseg
 20         format (/3x,'Generating B-spline for element',i3,
     &           ', segment',i3,' ...') 

            if ( isurf(ifoil,iseg) .eq. 2) then
c     -- b-spline on lower surface --
               if ( bstart(ifoil,iseg) .gt. 99.0 ) then
c     -- b-spline starts at trailing edge --
                  is = 1
                  xs = bap(is,1,ifoil)
               else
c     -- find start point on lower surface --
                  dist = 1.d0
                  do i = 1, ile
                     dist2 =  abs( bap(i,1,ifoil) - bstart(ifoil,iseg))
                     if (dist2.lt.dist) then
                        dist = dist2
                        is = i
                        xs = bap(i,1,ifoil)
                     end if
                  end do
               end if
               
               if ( bend(ifoil,iseg) .gt. 99.0 ) then
c     -- b-spline ends at leading edge --
                  jbody = ile - is + 1
                  ie = ile
                  xe = bap(ile,1,ifoil)
               else
c     -- find end point on lower surface --
                  dist = 1.d0
                  do i = 1, ile
                     dist2 =  abs( bap(i,1,ifoil) - bend(ifoil,iseg))
                     if (dist2.lt.dist) then
                        dist = dist2
                        ie = i
                        xe = bap(i,1,ifoil)
                     end if
                  end do
                  jbody = ie - is + 1
               end if

               ibstart(ifoil,iseg) = is
               ibend(ifoil,iseg) = ie

               write (*,40) is, xs, ie, xe
 40            format (3x,'B-spline on lower surface...'/3x,
     &              'Start index:',i4,4x,'X-coord:',e12.4,/3x,
     &              'End index:',1x,i4,4x,' X-coord:',e12.4)
            else 
c     -- b-spline on upper surface --
               if ( bstart(ifoil,iseg) .gt. 99.0 ) then
c     -- b-spline starts at leading edge --
                  is = ile
                  xs = bap(is,1,ifoil)
               else
c     -- find start point on upper surface --
                  dist = 1.d0
                  do i = ile,ipoints
                     dist2 =  abs( bap(i,1,ifoil) - bstart(ifoil,iseg))
                     if (dist2.lt.dist) then
                        dist = dist2
                        is = i
                        xs = bap(i,1,ifoil)
                     end if
                  end do                  
               end if   

               if ( bend(ifoil,iseg) .gt. 99.0 ) then
c     -- b-spline ends at trailing edge --
                  jbody = ipoints - is + 1
                  ie = ipoints
                  xe = bap(ie,1,ifoil)
               else
c     -- find end point on upper surface --
                  dist = 1.d0
                  do i = ile,ipoints
                     dist2 =  abs( bap(i,1,ifoil) - bend(ifoil,iseg))
                     if (dist2.lt.dist) then
                        dist = dist2
                        ie = i
                        xe = bap(i,1,ifoil)
                     end if
                  end do
                  jbody = ie - is + 1
               end if

               ibstart(ifoil,iseg) = is
               ibend(ifoil,iseg) = ie

               write (*,50) is, xs, ie, xe
 50            format (3x,'B-spline on upper surface...'/3x,
     &              'Start index:',i4,4x,'X-coord:',e12.4,/3x,
     &              'End index:',1x,i4,4x,' X-coord:',e12.4)
            end if

c     -- approximate airfoil with b-spline --
            nc = nbcp(ifoil,iseg)
            write (*,60) nc
 60         format (3x,'Number of control points:'i3)
            call genBSP(ifoil, iseg, is, ap, cp, t, knot, work)
         end do            

c     -- tecplot output --
         if (tecplot) then 
            if (nbseg(ifoil).eq.0) then
               write (btec_unit,80)
               write (btec_unit,90)
            else
               write (btec_unit,85)
               write (btec_unit,100) (bap(i,1,ifoil), bap(i,2,ifoil),
     &              i=1, ipoints)
               write (btec_unit,95)
               do iseg=1,nbseg(ifoil)
                  write (btec_unit,100) (bcp(i,1,ifoil,iseg),
     &                 bcp(i,2,ifoil,iseg), i=1, nbcp(ifoil,iseg))
               end do
            end if
         end if

 80      format ('ZONE T="Fitted Points", D=(1,2)')
 85      format ('ZONE T="Fitted Points"')      
 90      format ('ZONE T="Control Points", D=(1,2)')
 95      format ('ZONE T="Control Points"')        
 100     format (2e16.8)


c     -- modify all blocks next to airfoil --
c     -- flag = true modifies grid in both x and y directions --
         flag = .true.
         if ( nbseg(ifoil).ne.0 ) call modall (ifoil, flag, x, y,
     &        .false.)

c     -- define shape (b-spline) design variables --
         call defdv(ifoil)

         if (ndv.ne.0) then
            do iseg=1,nbseg(ifoil)

               write (*,*) ndv,nbseg(ifoil)
               do i = 1,ndv
                  if ( idvf(i).eq.ifoil .and. idvs(i).eq.iseg ) then
                     bcp(idvi(i),2,ifoil,iseg) = dvs(i)
                     write (*,110) i, bcp(idvi(i),2,ifoil,iseg)
                  end if
               end do
 110           format (3x,'Design variable',i3,' has value',e14.5)

               is = ibstart(ifoil,iseg)
               ie = ibend(ifoil,iseg) 
               jbody = ie - is + 1
               nc = nbcp(ifoil,iseg)

               j = 1
               do i = is,ie
                  t(j) = bt(j,ifoil,iseg)
                  j = j+1
               end do

               do i = 1,nc
                  cp(i,1) = bcp(i,2,ifoil,iseg)
               end do

               do i = 1,nc+iorder
                  knot(i) = bknot(i,ifoil,iseg)
               end do

               call new_body(is, ie, ifoil, knot, t, ap, cp)

            end do

c     -- modify all blocks next to airfoil --
c     -- only modify grid in y direction --
            flag = .false.
            if ( nbseg(ifoil).ne.0 ) call modall (ifoil, flag, x, y,
     &           .false.)
         end if 

      end do

c     -- define gap and overlap design variables --
c     -- nbdv: number of b-spline design variables --
c     -- ntdv: number of gap/overlap design variables --
      nbdv = ndv
      ntdv = 0
      rep = .false.
      call askl('Select gap & overlap design variables?^',rep)
      if (rep) call gapodv(x, y)

      return
      end                       !modify
