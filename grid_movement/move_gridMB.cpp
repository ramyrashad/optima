#include <cmath>
#include <vector>
#include "comprow_double.h"
#include "ilupre_double.h"
#include "cg.h"
#include "Node.h"
#include "grid.h"
#include "bicg.h"

using namespace std;

extern "C" {
  void movegridmb_(char *filena, const int& namelen, double* x, double* y, 
		   const int* ibap, const int* lgptr, const int& nblk,
		   const int& nfoils, const int& nhalo);

  void readarr_(const int* firstelem, const int* secondelem);
}
// bap is the array of nodes on airfoil (has "wall" boundary condition)
// ibap is the array of number of nodes on each airfoil 
// nhalo is the halo (ghost) points

void readarr_(const int* firstelem, const int* secondelem)
{
  cout << "in readarray function c++ ";
  cout << "first elem is " << firstelem[0] << setw(15) << secondelem[0] << endl;
  return;
}

void movegridmb_(char *filena, const int& namelen, double* x, double* y, 
		 const int* ibap, const int* lgptr, const int& nblk,
		 const int& nfoils, const int& nhalo)
{
  cout << "start moving multiblock grid " << endl; 

  int j;
  char *filename = new char[namelen+1];
  for (j=0; j<namelen; j++)
    filename[j] = filena[j];
  filename[namelen] = '\0';

  // declare and initialize objects
  int *jmax = new int [nblk];
  int *kmax = new int [nblk];

  // VARIABLES IN SOLVING MATRIX
  int num_inn_nodes(0), nonzeros (0);  
  

  for (j=0; j<nfoils; j++)
    nonzeros += ibap[j];   // total number of nodes on airfoil surface

	vector <Node> node_loc;
	vector <Node*> bnd_nodes(nonzeros);
	
	// CONJUGATE GRADIENT METHOD CONVERGENCE PARAMETERS
	double tolerance = 1.0e-5;
	int maxit = 500; 
	
	read_grid_mb (filename, node_loc,bnd_nodes, ibap, nfoils, nblk, jmax, kmax);

  	deform_airfoil (filename, bnd_nodes);
       
	// counts the number of nonzeros in the stiffness matrix	
	nonzeros = 0;
	get_num_nonzeros (node_loc, num_inn_nodes, nonzeros);

	double *stiff_val = new double [nonzeros];
	int *colind = new int [nonzeros];
	int *rowptr = new int [num_inn_nodes+1];
	
	// Solution, RHS vectors
	VECTOR_double deltay(num_inn_nodes, 0.0), deltax(num_inn_nodes,0.0),
		Fx(num_inn_nodes,0.0), Fy(num_inn_nodes,0.0);     
	
	//cout << "Forming stiffness matrix ... " << endl;
	calc_stiffness(node_loc, stiff_val, Fx, Fy, colind, rowptr);
	// ***** START SOLVING ... *****
	
	CompRow_Mat_double alpha(num_inn_nodes, num_inn_nodes, nonzeros, 
				 stiff_val, rowptr, colind);
	CompRow_ILUPreconditioner_double P(alpha);     // ILU preconditioner
	
	// Solve using the conjugate gradient method
	double tol = tolerance;
	int num_iter = 0;

	int maxitxy = maxit;

	num_iter = CG(alpha, deltax, Fx, P, maxitxy, tol);
	cout<< "CG flag:  " << num_iter << "     ";
	cout<< "Iterations performed x-dir:  " << maxitxy << "     ";
	cout<< "Tolerance achieved:  " << tol << endl;	


	tol = tolerance;
	maxitxy = maxit;	
	num_iter = CG(alpha, deltay, Fy, P, maxitxy, tol);  
	cout<< "CG flag:  " << num_iter << "     ";
	cout<< "Iterations performed y-dir:  " << maxitxy << "     ";
	cout<< "Tolerance achieved:  " << tol << endl;
	
	//cout << "Updating nodal locations ... " << endl;
	update (node_loc, deltax, deltay);
	
	delete [] stiff_val;
	delete [] colind;
	delete [] rowptr;
	cout <<"before write gridMB" << endl;
	write_gridMB (node_loc, nblk, jmax, kmax, x, y, nhalo, lgptr);

	// write grid out to plot 3D format
	write_plot3DMB (node_loc, nblk, jmax, kmax);


	delete [] jmax;
	delete [] kmax;
	delete [] filename;

	// clear all dynamically allocated memories associated with object Node
	num_iter = int (node_loc.size());
	for (j=0; j<num_iter; j++)
	  node_loc[j].clearmem();

	cout << "exitting movegridMB " << endl;
	return;
}













