      subroutine reset_s(jdim,kdim,s)
c
#include "../include/arcom.inc"
c
      dimension s(jdim,kdim,4)

      do 3 n=1,4
      do 3 k=1,kdim
      do 3 j=1,jdim
         s(j,k,n)=0.0d0
 3    continue

      return
      end

