      subroutine trib(JDIM,A,B,C,X,F,NL,NU)                             
c                                                                       
      DIMENSION A(JDIM),B(JDIM),C(JDIM),X(JDIM),F(JDIM)                 
c                                                                       
      X(NL)=C(NL)/B(NL)                                                 
      F(NL)=F(NL)/B(NL)                                                 
      NLP1 = NL +1                                                      
      DO 1 J=NLP1,NU                                                    
         Z=1./(B(J)-A(J)*X(J-1))                                        
         X(J)=C(J)*Z                                                    
         F(J)=(F(J)-A(J)*F(J-1))*Z                                      
1     continue                                                          
c                                                                       
      NUPNL=NU+NL                                                       
      DO 2 J1=NLP1,NU                                                   
         J=NUPNL-J1                                                     
         F(J)=F(J)-X(J)*F(J+1)                                          
2     continue                                                          
c                                                                       
      RETURN                                                            
      END                                                               
