c----------------------------------------------------------------------
c     -- RHS for Newton's method --
c
c     written by: marian nemec
c     date: august 2000
c----------------------------------------------------------------------
      subroutine get_rhs (jdim, kdim, ndim, q, qnow, qold, xy,
     &     xyj, x, y, precon, coef2, coef4, uu, vv, ccx, ccy, 
     &     ds, press, xit, spect, gam, turmu, fmu, vort, sndsp, 
     &     s, s0, work, D_hat, exprhs, a_jk, D_hatflag)

      use disscon_vars
#include "../include/arcom.inc"
#include "../include/sam.inc"
#include "../include/optcom.inc"

      integer jdim, kdim, ndim, n, k, j

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision qnow(jdim,kdim,ndim), qold(jdim,kdim,ndim)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)

      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision xit(jdim,kdim), ds(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)
      double precision s(jdim,kdim,ndim), work(jdim*kdim,4)
      double precision s0(jdim,kdim,ndim)

c     -- frozen jacobian arrays --
      double precision c2xf(maxj,maxk), c4xf(maxj,maxk)
      double precision c2yf(maxj,maxk), c4yf(maxj,maxk)
      double precision fmuf(maxj,maxk), turmuf(maxj,maxk)
      double precision vortf(maxj,maxk), sapdf(maxj,maxk)
      double precision sadx(maxj,maxk), sady(maxj,maxk)
      double precision suuf(maxj,maxk), svvf(maxj,maxk)
      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &      sapdf, sadx, sady, suuf, svvf 

      common/scratch/ d,work1
      dimension d(maxjk,16),work1(maxjk,10)

csi   variables used with esdirk for unsteady runs
      double precision D_hat(jdim,kdim,ndim,6),a_jk(6,6)
      double precision exprhs(jdim,kdim,ndim)
      logical D_hatflag


      !-- Set residual values at each node to zero
      do n=1,ndim
        do k=1,kend
          do j=1,jend
            s(j,k,n) = 0.d0
          end do
        end do
      end do

      !-- s0 contains the unmodified residual; i.e. the residual without
      !-- modifications due to the dissipation-based continuation scheme
      if (dissCon) then
         do n=1,ndim
            do k=1,kend
               do j=1,jend
                  s0(j,k,n) = 0.d0
               end do
            end do
         end do
      end if

      call xiexpl (jdim, kdim, q, s, s0, press, sndsp, turmu, fmu, vort,
     &      x, y, xy, xyj, xit, ds, uu, ccx, coef2, coef4, spect,
     &      precon, gam)

      if(viscous .and. viscross) then                                   
c        -viscous cross terms
         call visrhsnc(jdim,kdim,q,press,s,turmu,fmu,xy,xyj,        
     >                 work1(1,1), work1(1,2), work1(1,3), work1(1,4),
     >                 work1(1,5), work1(1,6), work1(1,7),             
     >                 work1(1,8), work1(1,9), d)
                       
      endif       

c     -- logic to avoid calling spalart in etaexpl --        
      iflag = 0
      if (viscous .and. turbulnt .and. itmodel.eq.2) then
        turbulnt = .false. ! flag set to avoid calling spalart in
                           ! etaexpl
        iflag = 1
        do k = kbegin,kup
          kp1 = k+1
          do j = jbegin,jend
            rn = q(j,k,5)*q(j,k,1)*xyj(j,k)**2
            chi = rn/fmu(j,k)
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            tur1 = fv1*rn
            
            rn = q(j,kp1,5)*q(j,kp1,1)*xyj(j,kp1)**2
            chi = rn/fmu(j,kp1)
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            tur2 = fv1*rn
            
            turmu(j,k) = 0.5d0*(tur1+tur2)

          end do
        end do
      endif

      call etaexpl (jdim, kdim, q, s, s0, press, sndsp, turmu, fmu,
     &      vort, x, y, xy, xyj, xit, ds, uu, vv, ccy, coef2, coef4,
     &      spect, precon, gam)

      if ( iflag.eq.1 ) turbulnt = .true.

      if ( turbulnt ) then

         call res_sa (jdim, kdim, q, s, s0, x, y, xy, xyj,
     &      uu, vv, fmu,vort, work(1,1), work(1,4))
      end if

c     -- circulation correction --
      if (circul) then
        call clcd (jdim, kdim, q, press, x, y, xy, xyj, 0, cli, cdi,
     &        cmi, clv, cdv, cmv) 
        clt = cli + clv
        cdt = cdi + cdv
        cmt = cmi + cmv
c     -- set variables for far field vortex correction --
        if(fsmach.le.1.0)    call bccirc                              
      end if

csi   for unsteady runs
      if (unsted.and. (jesdirk.eq.4.or.jesdirk.eq.3)) then
         if (D_hatflag) then
            call nk_saveDhat(jdim, kdim,ndim, s, D_hat)
         elseif (jstage.gt.1)then
            call nk_subit(jdim,kdim,ndim,q,qnow,qold,s,
     &           a_jk(jstage,jstage))
            call nkadd_exprhs(jdim, kdim, ndim, s, exprhs)
         endif
      elseif(unsted)then
         call nk_subit(jdim,kdim,ndim,q,qnow,qold,s,a_jk(jstage,jstage))
      endif
csi

      if (viscous) then
        call vbcrhs (jdim, kdim, x, y, xy, xyj, q, s, s0, press)
      else
        call bcrhs (jdim, kdim, x, y, xy, xyj, q, s, s0, press)
      end if

c     -- wake cut, explicit treatment -- 

      if(.not.periodic)then

         if (dissCon) then
            k = kbegin
            do j = jbegin+1,jtail1-1
               jf = jmax-j+1
               do n1 = 1,3
                  s0(j,k,n1)  = -(xyj(j,1)*q(j,1,n1) 
     &                 -0.5d0*( xyj(j,2)*q(j,2,n1)+
     &                 xyj(jf,2)*q(jf,2,n1) ))
                  s0(jf,k,n1) = -(xyj(jf,1)*q(jf,1,n1)
     &                 -0.5d0*( xyj(j,2)*q(j,2,n1)
     &                 + xyj(jf,2)*q(jf,2,n1) ))
                  s(j,k,n1)  = -(xyj(j,1)*q(j,1,n1) 
     &                 -0.5d0*( xyj(j,2)*q(j,2,n1)+
     &                 xyj(jf,2)*q(jf,2,n1) ))
                  s(jf,k,n1) = -(xyj(jf,1)*q(jf,1,n1)
     &                 -0.5d0*( xyj(j,2)*q(j,2,n1)
     &                 + xyj(jf,2)*q(jf,2,n1) ))
               end do
               s0(j,k,4)  = -(press(j,1)*xyj(j,1)
     &             -0.5d0*( press(j,2)*xyj(j,2) +
     &              press(jf,2)*xyj(jf,2) ))
               s0(jf,k,4) = -(press(jf,1)*xyj(jf,1) 
     &              -0.5d0*( press(j,2)*xyj(j,2)
     &              + press(jf,2)*xyj(jf,2) ))
               s(j,k,4)  = -(press(j,1)*xyj(j,1)
     &             -0.5d0*( press(j,2)*xyj(j,2) +
     &              press(jf,2)*xyj(jf,2) ))
               s(jf,k,4) = -(press(jf,1)*xyj(jf,1) 
     &              -0.5d0*( press(j,2)*xyj(j,2)
     &              + press(jf,2)*xyj(jf,2) ))

            end do

         else

            k = kbegin
            do j = jbegin+1,jtail1-1
               jf = jmax-j+1
               do n1 = 1,3
                  s(j,k,n1)  = -(xyj(j,1)*q(j,1,n1) 
     &                 -0.5d0*( xyj(j,2)*q(j,2,n1)+
     &                 xyj(jf,2)*q(jf,2,n1) ))
                  s(jf,k,n1) = -(xyj(jf,1)*q(jf,1,n1)
     &                 -0.5d0*( xyj(j,2)*q(j,2,n1)
     &                 + xyj(jf,2)*q(jf,2,n1) ))
               end do
               s(j,k,4)  = -(press(j,1)*xyj(j,1)
     &             -0.5d0*( press(j,2)*xyj(j,2) +
     &              press(jf,2)*xyj(jf,2) ))
               s(jf,k,4) = -(press(jf,1)*xyj(jf,1) 
     &              -0.5d0*( press(j,2)*xyj(j,2)
     &              + press(jf,2)*xyj(jf,2) ))
            end do
            
         end if

      end if !not periodic

      return
      end                       !get_rhs

