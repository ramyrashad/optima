c----------------------------------------------------------------------
c     -- generate new b-spline curve coordinates given control points
c     and parameter vectors --
c     -- m. nemec --
c----------------------------------------------------------------------
      subroutine new_body(is, ie, ifoil, knot, t, ap, cp)

      implicit none

#include "mxfoil.inc"
#include "bsord.inc"
#include "bsp.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      integer i, j, left, ifoil, is, ie

      double precision t(jbody), knot(iorder+nc), ap(jbody)
      double precision cp(nc), tmp, BIatT(ibsord)

      write (*,10) 
 10   format (3x,'new_body: Generating New Airfoil Coordinates ')

      tmp = -1.0
      do j = 1,jbody
         do left = iorder,nc

            if ( knot(left).le.t(j) .and. t(j).le.knot(left+1)
     &           .and. t(j).ne.tmp ) then 
               tmp = t(j)
               call Bval ( knot, t(j), left, BIatT )

               ap(j) = 0.0
               do i=1,iorder
                  ap(j) = ap(j) + cp(i+left-iorder)*BIatT(i)
               end do
            end if

         end do
      end do    

      write (*,*) jbody, ie-is+1
      j = 1
      do i = is,ie
c     write (*,*) i, bap(i,2,ifoil), ap(j)
         bap(i,2,ifoil) = ap(j)
         j = j+1
      end do

      return
      end                       !new_body
