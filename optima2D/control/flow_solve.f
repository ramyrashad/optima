c----------------------------------------------------------------------
c     -- solve flowfield --
c     -- AF solver: Cyclone --
c     -- NK solver: Probe --
c     
c     written by: marian nemec
c     date: august 2000
c----------------------------------------------------------------------
      subroutine flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &     ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, 
     &     qp, qold, press, xy, xyj, x, y, fmu, vort, turmu, work1,
     &     D_hat,exprhs,a_jk)

#ifdef _MPI_VERSION
      use mpi
#endif


#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

c     -- JG: store AF iterations --
      integer afiters
      common/afiter/afiters

      integer jdim, kdim, ndim, icol(9), ifunc, ifun
      integer indx(jdim,kdim), iex(jdim*kdim*ndim)

clb
      integer ia(jdim*kdim*ndim+2),iat(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)
clb

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision qp(jdim,kdim,ndim),qold(jdim,kdim,ndim)
      double precision press(jdim,kdim)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)

      double precision obj0s(mpopt)

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100)

      double precision dtiseq(20),dtmins(20),dtow2(20)
      integer jmxi(20),kmxi(20),jskipi(20),iends(20)
      integer isequal,iseqlev
      common/mesup/ dtiseq,dtmins,dtow2,isequal,iseqlev,
     &      jmxi,kmxi,jskipi,iends

csi
      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim)
      double precision a_jk(6,6)
csi
      ! transition prediction related declarations
      logical convergedAllTPs, transPredictAFonly
      integer surf, jConvergeTP(2)
      double precision xTransPredict(2), xConvergeTP(2), ldratio


      ! initialize transition prediction related variables
      iterTP = 1
      if (maxIterTP.eq.1) then
         underRelaxTP = 1.0
      end if
      ! RR: add to input parameters
      finalTP = .false.
      convergedAllTPs = .false.
      transPredictAFonly = .false.
      do surf = 1, 2
         convergedTPOld(surf) = .false.
         checkForFullyLam(surf) = .false.
         checkForVanishingBubble(surf) = .false.
         xConvergeTP(surf) = 666
         jConvergeTP(surf) = 666
      end do ! surf

      ! ____________________________________________________________
      ! For post-processing, debugging, and output purposes, you can
      ! read in previously saved solution data and skip the flow solve.
      ! Note: this is different than warm starting a flow solve
      if (postProcessOnly) then

         maxIterTP = 1
         underRelaxTP = 1.0

         call initia ( ifirst,jdim,kdim,q,qold,press,work1(1,6),
     &        work1(1,7),xy,xyj,work1(1,12),work1(1,13),work1(1,14),x,y,
     &        turmu,fmu,vort,work1(1,15) )
         call asprat (jdim,kdim,x,y)
 
         write(34,*) 'postProcessOnly = ', postProcessOnly
         goto 100 ! Skips the calls to the AF and NK flow solvers
 
      end if

 101  continue

      ! re-initialize transition points
      xTransPredict(1) = -1.0d0
      xTransPredict(2) = -1.0d0
      
      write (scr_unit,10) rank, fsmach, alpha, re_in
 10   format (//3x,'Computing flowfield solution on processor [',i3,
     & ' ]:',/3x,' M =',f6.3,', AOA =',  f7.3,' and Re =',e10.3)
      if (dvmach(rank+1)) write (scr_unit,11) cl_tar
 11   format(/3x,'New target lift coefficient = ',f10.6)

      if (afiters.gt.0) then

c     ---------------
c     -- AF solver --
c     ---------------

         write (scr_unit,15) 
         call flush(6)
 15      format (/3x,'<< Starting A-F iterations >>') 

         clinput = cl_tar

         call cyclone (jdim, kdim, ifirst, q, press, xy,xyj,x,y,turmu,
     &     fmu, vort, qp, qold, work1(1,9), 
     &     work1(1,10), work1(1,14), work1(1,15), work1(1,16),
     &     work1(1,17), work1(1,20), work1(1,23), work1(1,24),
     &     work1(1,25), work1(1,26), work1(1,27), work1(1,28),
     &     work1(1,29), work1(1,30), work1(1,31), work1(1,51),
     &     work1(1,71), work1(1,77),D_hat,exprhs,a_jk)

         !-- Check for negative jacobian
         if (badjac) then
            write(scr_unit,24) rank
 24         format(/3x, 'badjac detected on proc [',i2,']',/3x
     &        'exiting flow_solve',//)
            return
         end if

         write (scr_unit,20) resid, totime1, numiter
 20      format (3x,'CYCLONE residual:',e9.2,3x,'Run-time:',f7.2,'s',3x,
     &      'Iterations:',i7 )

c     -- check abnormal or nan resid --
         if ( abs( resid - 1000.0) .lt. 0.01 )  then
            write (scr_unit,26) 
 26          format(/3x,'Terminating flow solve after A-F,
     & resid too high or nan')

            return
         end if

c     -- set turbulent variable --
         if (viscous .and. turbulnt .and. itmodel.eq.2) then
            do k = kbegin,kend
               do j = jbegin,jend
                  q(j,k,5) = turre(j,k)/xyj(j,k)
                  qold(j,k,5) = turold(j,k)/xyj(j,k)
               end do
            end do
         end if

         ! _____________________
         ! Transition Prediciton 
         ! -- predict and update transition using results from AF solve
         if ( freeTransition .and. earlyTP .and. .not.finalTP ) then
            ! skip the NK solver and go straight to transition prediction
            go to 100
         end if
          
      else

c     ------------------
c     -- no AF solver --
c     ------------------

         if (.not. unsted) then
            write (scr_unit,25) 
            call flush(6)
 25         format (3x,'<< Not Running A-F >>')
         end if

c     -- JG: Initialize grid and vars without running AF

         call initia ( ifirst,jdim,kdim,q,qold,press,work1(1,6),
     &        work1(1,7),xy,xyj,work1(1,12),work1(1,13),work1(1,14),x,y,
     &        turmu,fmu,vort,work1(1,15) )
         call asprat (jdim,kdim,x,y)

         if (restart) iend = istart
          
         if (viscous .and. turbulnt .and. itmodel.eq.2) then
           
c     -- JG: need to initialize the turbulence parameters --        
 
            if (restart) then
               
               do k = kbegin,kend
                  do j = jbegin,jend
                     q(j,k,5) = turre(j,k)/xyj(j,k)
                     if (unsted) qold(j,k,5) = turold(j,k)/xyj(j,k)
                  end do
               end do
                
            else
               
               do k = kbegin,kend
                  do j = jbegin,jend
                     if (j.le.jtail2.and.j.ge.jtail1.and.k.eq.1) then
                        q(j,k,5) = 1.0d0/xyj(j,k)
                     else
                        q(j,k,5) = 1.0d0/xyj(j,k)
                     end if
                  end do
               end do
               
            end if
            
c     -- compute generalized distance function --
c     global variable SMIN which is 'd_w' or 'd' (Spalart or Chisholm)
            do k = kbegin,kend
               do j = jbegin,jend
                  j_point=j
                  if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
                  xpt1 = x(j_point,kbegin)
                  ypt1 = y(j_point,kbegin)
                  smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
               end do
            end do
            
         end if ! (viscous .and. turbulnt .and. itmodel.eq.2)
         
      end if !afiters.gt.0

 102  continue

c     ----------------
c     |   N-K solver |
c     ----------------

      if (nk_its.gt.0) then
         dt = 1.d0

         if (.not. unsted) then
            write (scr_unit,30)
 30         format (3x,'<< Starting N-K iterations >>')
            call flush(6)
         end if

         call probe (jdim, kdim, ndim, indx, icol, iex, ia, ja, as, ipa,
     &       jpa, pa, iat, jat, ast, ipt, jpt, pat, q, qp, qold, press, 
     &       xy,xyj, x, y, turmu, fmu, vort, work1(1,1),  work1(1,2),
     &       work1(1,3),  work1(1,4),  work1(1,7), work1(1,8),
     &       work1(1,9), work1(1,10), work1(1,11), work1(1,12),
     &       work1(1,13), work1(1,19), work1(1,35), work1(1,39),
     &       work1(1,78), work1(1,83), work1(1,88),D_hat,exprhs,a_jk)
         dt = dt2

c     -- check abnormal or nan resid --
         if ( abs( resid - 1000.0) .lt. 0.01 )  then
            write (scr_unit,*) 'Terminating flow solve after N-K'
            write (scr_unit,*) 'Resid too high or nan'
            return
         end if

      end if

      
      if (.not. unsted) then
         write (scr_unit,40) resid, totime1
 40      format (3x,'Flow solve converged to',e9.2,' in',e11.4,
     &        ' seconds.')
         write (out_unit,*)
      end if

 100  continue

      ! _____________________________________________
      ! Transition Prediction: an iterative procedure

      if (freeTransition .and. .not.convergedAllTPs .and. .not.finalTP)
     &     then

         write(34,'(/,/, ''TP ITERATION NUMBER = '',i6)') iterTP
         write(34,'(''-------------------'')') 

         !convergedAllTPs = .false.
         
         ! _________________________
         ! predict transition points
         call transitionPrediction(
     &        jdim,kdim,ndim,q,xy,xyj,x,y,fmu,vort,turmu,
     &        xTransPredict,xConvergeTP,jConvergeTP)

         ! ________________________
         ! update transition points
         call transitionUpdatePoints(
     &        jdim,kdim,x,xTransPredict,xConvergeTP,jConvergeTP,
     &        convergedAllTPs)

         ! _________________________________________
         ! TPs not converged, restart flow solver
         if (.not.convergedAllTPs) then 
            iterTP = iterTP + 1
            if (iterTP.le.maxIterTP .and. maxIterTP.ne.1) then 
               rewind(blprop_unit)
               rewind(blvel_unit)
               go to 101 
            end if
         else
         ! _____________________________________________________________
         ! TPs converged, restart flow solver with final TP locations
            iterTP = iterTP + 1
            if (iterTP.le.maxIterTP) then
               finalTP = .true.
               write(34,*) ' '
               write(34,*) 'TRANSITION PREDICTION CONVERGED'
               write(34,*) ' '
               write(34,*) 'Run flow solver with final TPs:'
               write(34,*) 'FINAL TRANSUP = ', transup
               write(34,*) 'FINAL TRANSLO = ', translo
               write(34,*) 
               write(34,*) 'Tecplot Output:'
               write(34,*) 
               write (34,*) 'TITLE="Transition Data"'
               write(34,*) 'VARIABLES="Re","M","Tu","alpha","Cl","Cd",
     &                      "Cm", "L/D","Top_Xtr","Bot_Xtr"'
               write(34,*) 'ZONE T="Results - AF"'
               write(34,*) 'IterTP = ', iterTP
               ldratio = clt/cdt
               write(34,'(10(F30.15))') re_in,fsmach,Tu,alpha,clt,cdt,
     &              cmt,ldratio,transup,translo

               if (bypassFinalTPRun) then
                  ! skip the final run with the final TP locations,
                  ! since you're only interested in knowing the final TP
                  ! locations. Careful if you need lift or drag info.
                  go to 103
               else
                  ! call the flow solver with the final TP locations
                  go to 101
               end if
            end if 
         end if

!         ! ________________________________________________________
!         ! if converged TPs by using only AF solver, call NK solver
!         if (transPredictAFonly) then
!            if (convergedAllTPs .or. (iterTP.gt.maxIterTP)) then
!               transPredictAFonly = .false.
!               go to 102
!            end if
!         end if
         
      end if ! (freeTransition .and. .not.convergedAllTPs .and. .not.finalTP)

      ! output final transition results in tecplot format
      if ( transitionalFlow ) then
         write(34,*) 
         write(34,*) 'Tecplot Output:'
         write(34,*) 
         write (34,*) 'TITLE="Transition Data"'
         write(34,*) 'VARIABLES="Re","M","Tu","alpha","Cl","Cd","Cm",
     & "L/D","Top_Xtr","Bot_Xtr"'
         write(34,*) 'ZONE T="Results"'
         ldratio = clt/cdt
         write(34,'(10(F30.15))') re_in,fsmach,Tu,alpha,clt,cdt,cmt,
     &        ldratio,transup,translo
      end if ! freeTransition .and. finalTP

 103  continue
c     ------------------
c     -- final output --
c     ------------------

c     --  output forces --
      ip = 11
      junit = 1
      call ioall(ip, junit, jdim, kdim, q, q, press, sndsp, turmu, fmu,
     &      vort, xy, xyj, x, y, obj0s)
      do j=1,5
         if (ldout) backspace (ld_unit)
      end do
c     -- write out formatted cp data to .cp file --
      if (meth.ne.1 .and. meth.ne.4 .and. meth.ne.5) then
        ip = 6                                                            
        call ioall(ip, junit, jdim, kdim, q, q, press, sndsp,
     &        turmu, fmu, vort, xy, xyj, x, y, obj0s)

c     -- store ascii cf data to .cf file --
        if (sknfrc) then
          ip = 26
          call ioall(ip, junit, jdim, kdim, q, q, press, sndsp,
     &          turmu, fmu, vort, xy, xyj, x, y, obj0s)
        endif

      end if

c     -- store solution data; write binary restart to .q file --
      if (store) then
        ip = 10
        call ioall(ip, junit, jdim, kdim, q, qold, press, sndsp,
     &        turmu, fmu, vort, xy, xyj, x, y, obj0s)
      end if

c     -- store turbulent quantities --
      if (writeturb) then
        ip = 25
        call ioall(ip, junit, jdim, kdim, q, qold, press, sndsp, turmu,
     &        fmu, vort, xy, xyj, x, y, obj0s)
      endif

c     -- store flux budget data --      
      if (flbud) then
        ip = 24
        call ioall(ip, junit, jdim, kdim, q, qold, press, sndsp,
     &        turmu, fmu, vort, xy, xyj, x, y, obj0s)
      endif

c      -- output viscous variables using visvar subroutine --
!      ip = 14
!      call ioall(ip, junit, jdim, kdim, q, qold, press, sndsp, turmu,
!     &     fmu, vort, xy, xyj, x, y, obj0s)

      if (opt_meth.eq.5.and.sknfrc) then
c     -- output skin friction and surface pressures in Tecplot format --
         call ioall(8, junit, jdim, kdim, q, qold, press, sndsp, turmu,
     &        fmu, vort, xy, xyj, x, y, obj0s)
      end if

c     -- optimization calls --

      if (.not.flowAnalysisOnly) then

c     -- store cp body in the press array for inverse design --
            ip = 1
            call ioall (ip, 0, jdim, kdim, q, qold, press, sndsp, turmu,
     &           fmu, vort, xy, xyj, x, y, obj0s)
        
c     -- remove jacobian scaling from q vector for restarts --
        
        do j = 1,jmax
          do k = 1,kmax
            do n = 1,ndim
              q(j,k,n) = q(j,k,n)*xyj(j,k)
              qold(j,k,n) = qold(j,k,n)*xyj(j,k)
            end do
          end do
        end do
        restart = .false.
        if (obj_func.eq.2) iclstrt = 300

      end if

      return
      end                       !flow_solve
