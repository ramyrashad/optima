      ! ================================================================
      ! ================================================================
      ! 
      subroutine transitionUpdatePoints(
     &     jdim,kdim,x,xTransPredict,xConvergeTP,jConvergeTP,
     &     convergedAllTPs)
      ! 
      ! ================================================================
      ! ================================================================
      ! 
      ! Purpose: This subroutine controls the iterative procedure
      !   required to predict transition. The subroutine will address
      !   any error flags that were raised, assess convergence, and
      !   update (i.e. move) the forced transition points toward the
      !   predicted transition points, until the points coincide (to
      !   whithin a tolerance) . The forward movement of the forced
      !   transition points is under-relaxed by a factor of
      !   underRelaxTP, as specified by the user (default is 0.5).
      !
      ! Author: Ramy Rashad
      !
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif
      implicit none

#include "../include/arcom.inc"
#include "../include/mpi_info.inc"

      ! decalarations
      logical
!     &     convergedTPOld(2),
     &     convergedAllTPs 
      integer
     &     jdim,
     &     kdim,
     &     surf,
     &     jConvergeTP(2)
      double precision
     &     x(jdim,kdim),
     &     xTransPredict(2),
     &     xConvergeTP(2)

      write (34,*) '------------------------------------------'
      write (34,*) '------------------------------------------'
      write(34,*) 'iterTP           = ', iterTP
      write(34,*) 'underRelaxTP     = ', underRelaxTP

      ! Update the transition point locations for each surface according
      ! to the errorFlag and convervence criterion
      do surf=1,2

         ! ______________________________________
         ! top and bottom surface index increment
         if (surf.eq.1) then
            jblinc = 1
         else if (surf.eq.2) then 
            jblinc = -1
         end if

         write (34,*) '------------------------------------------'
         write (34,*) '------------------------------------------'
         write(34,*) 'surface          = ', surf
         write(34,*) 'errorFlagTP      = ', errorFlagTP(surf)
         write(34,*) 'xTransForcedOld  = ', xTransForced(surf)
         write(34,*) 'xTransPredict    = ', xTransPredict(surf)

         ! _________________________________________
         ! Evalaute transition prediction Error Flag

         select case (errorFlagTP(surf))
            
         ! ------------------------------------------------------
         ! CASE 0: transition point successfully predicted
         ! ------------------------------------------------------
         case (0) 

            if (.not.convergedTPOld(surf)) then

               ! ___________________________________________
               ! Transition prediction convergence criteria:
               ! 1. distance between forced and predicted points less than distance tolerance
               ! 2. number of nodes between forced and predicted points less than streamwise node tolerance
               if ( (xConvergeTP(surf) .le. xConvTolTP) .and.
     &              (jConvergeTP(surf) .le. jConvTolTP) ) then
                  ! ACTION: move to final (converged) transition location
                  xTransForced(surf) = xTransPredict(surf)
                  convergedTP(surf) = .true.
                  ! SPECIAL ACTION if this is the first iteration:
                  if (iterTP.eq.1) then
                     write(34,*)
     &                    'Initial guess is very close to ',
     &                    'predicted point --> moving 5% downstream'
                     xTransForced(surf) = xTransPredict(surf) + 0.05
                     convergedTP(surf) = .false.
                   write(34,*) 'xTransForcedNew  = ', xTransForced(surf)
                  end if 
               end if
               
               ! ACTION: move transition locations upstream toward
               ! predicted transition locations in an under-relaxed fashion
               if (.not.convergedTP(surf)) then
                  if (underRelaxTP.ge.1d0 .and. maxIterTP.ne.1) then
                     ! immediately place transition point at predicted point
                     xTransForced(surf) = xTransPredict(surf)
                  else
                     xTransForced(surf) =
     &                    xTransForced(surf) -
     &                    underRelaxTP *
     &                    (xTransForced(surf)-xTransPredict(surf))
                  end if
               end if

               if (xTransPredict(surf) .gt. aftLimitTP) then
                  ! Constrain the downstream limit of transition
                  xTransForced(surf) = aftLimitTP
                  convergedTP(surf) = .true.
               end if

            else ! converged
               xConvergeTP(surf) = 777
               jConvergeTP(surf) = 777
            end if

         ! -------------------------------------------------------------
         ! CASE 1: transition point predicted downstream of forced point
         ! -------------------------------------------------------------
         case (1)

            if (.not.convergedTPOld(surf)) then
               if (xConvergeTP(surf) .lt. -xConvTolTP) then
                  ! If we're here, we already know that xConvergeTP is
                  ! negative. But if it's further downstream then the
                  ! convergence criterion, then we need to take the
                  ! following ACTION: move transition locations to
                  ! predicted downstream location plus another 5%
                  ! chord downstream (for some room)
                  xTransForced(surf) = xTransPredict(surf) + 0.05
               else
                  ! ACTION: do not move TP from previous iteration
                  convergedTP(surf) = .true.
                  ! SPECIAL ACTION if this is the first iteration:
                  if (iterTP.eq.1) then
                     write(34,*)
     &                    'Initial guess is very close to ',
     &                    'predicted point --> moving 5% downstream'
                     xTransForced(surf) = xTransPredict(surf) + 0.05
                     convergedTP(surf) = .false.
                   write(34,*) 'xTransForcedNew  = ', xTransForced(surf)
                  end if 
               end if
                  
            else ! converged
               xConvergeTP(surf) = 777
               jConvergeTP(surf) = 777
            end if

            if (xTransForced(surf) .gt. aftLimitTP) then
               ! Constrain the downstream limit of transition
               xTransForced(surf) = aftLimitTP
               checkForFullyLam(surf) = .true.
            end if
         ! -----------------------------------------------------------
         ! CASE 2: critical point not found
         ! -----------------------------------------------------------
         case(2)

            !  ACTION: move transition point upstream by 10% chord and
            ! restart flow solver

            if (.not.convergedTPOld(surf)) then
               xTransForced(surf) = xTransForced(surf) - 0.10
               if (xTransForced(surf).lt.0.0) then
                  xTransForced(surf) = 0.0
               end if 
            else ! converged
               xConvergeTP(surf) = 777
               jConvergeTP(surf) = 777
            end if

            write(34,*) 
            write(34,*) '*** Critical Point Not Found ***'

         ! -----------------------------------------------------------
         ! CASE 3: transition point not found
         ! -----------------------------------------------------------
         case(3)

            select case (transPredictMethod)
            case (1:3)
               ! ACTION: move transition point upstream by 10% chord and
               ! restart flow solver

               if (.not.convergedTPOld(surf)) then
                  xTransForced(surf) = xTransForced(surf) - 0.10
                  if (xTransForced(surf).lt.0.0) then
                     xTransForced(surf) = 0.0
                  end if 
               else ! converged
                  xConvergeTP(surf) = 777
                  jConvergeTP(surf) = 777
               end if

            case (6:7)
               ! If using the eN method and transition not found,
               ! then check for fully laminar flow on the next iteration
               xTransForced(surf) = aftLimitTP
               checkForFullyLam = .true.
            end select 

            write(34,*) 
            write(34,*) '*** Transition Point Not Found ***'

         ! -----------------------------------------------------------
         ! CASE 4: laminar flow separation detected
         ! -----------------------------------------------------------
         case (4)
            
            if (.not.convergedTPOld(surf)) then
               if (checkForVanishingBubble(surf)) then
                  ! If we're here, it means that this is the second
                  ! consecutive iteration in which laminar separation
                  ! has been detected upstream of predicted TP.

                  ! ACTION: do not move TP from previous iteration
                  convergedTP(surf) = .true.
                  write (34,*) 
                  write(34,*) '*** Separation Bubble Detected Again ***'
               else
                  ! ACTION: move transition points to separation point
                  xTransForced(surf) = xTransPredict(surf)
                  ! On the next iteration it is possible that the bubble
                  ! will completely vanish and thus the TP may continue
                  ! to iterate upstream until the convergence criteria
                  ! are satisfied. This flag will check if subsequent
                  ! iterations continue to detect a separation bubble.
                  checkForVanishingBubble(surf) = .true.
                  write (34,*) 
                  write(34,*) '*** Separation Bubble Detected ***'
               end if
            else ! converged
               xConvergeTP(surf) = 777
               jConvergeTP(surf) = 777
            end if

         ! -----------------------------------------------------------
         ! CASE 5: laminar separation bubble vanished
         ! -----------------------------------------------------------
         case (5)
            
            if (.not.convergedTPOld(surf)) then
               if (checkForVanishingBubble(surf)) then
                  ! If we're here, it means that the separation bubble
                  ! has vanished by moving the TP forward to the
                  ! separation point and no TP has been detected
                  ! UPSTREAM of the previous iteration's separation
                  ! point.

                  ! ACTION: do not move TP from previous iteration
                  convergedTP(surf) = .true.
                  write (34,*) 
                  write(34,*) '*** Separation Bubble Vanished ***'
               end if
            else ! converged
               xConvergeTP(surf) = 777
               jConvergeTP(surf) = 777
            end if

         ! -----------------------------------------------------------
         ! CASE 6: eddy viscosity detected in BL
         ! -----------------------------------------------------------
         case (6)
            
            ! ACTION: move transition point to the point where
            ! eddy-viscosity was first detected.

            ! Note: This may seem confusing, but the SA model may (in
            ! high AoA or separated flow cases) introduce eddy-viscosity
            ! upstream of the specified forced transition locations
            if (.not.convergedTPOld(surf)) then
               convergedTP(surf) = .true.
               !xTransForced(surf) = x(jTransPredict(surf),1)
               xTransForced(surf) = xTransPredict(surf)
            else ! converged
               xConvergeTP(surf) = 777
               jConvergeTP(surf) = 777
            end if

            write(34,*)
            write(34,*) '*** Eddy Viscosity Detected ***'

         ! -----------------------------------------------------------
         ! CASE 7: fully laminar surface
         ! -----------------------------------------------------------
         case (7)
            
            ! ACTION: move transition point to aftLimitTP chord to avoid
            ! convergence issues with the RANS solver.
            ! See J. Driver's thesis for details

            if (.not.convergedTPOld(surf)) then
               convergedTP(surf) = .true.
               xTransForced(surf) = aftLimitTP
            else ! converged
               xConvergeTP(surf) = 777
               jConvergeTP(surf) = 777
            end if

            write(34,*)
            write(34,*) '*** Fully Laminar Flow Detected ***'

         ! -----------------------------------------------------------
         ! CASE default: stop program and report invalid flag
         ! -----------------------------------------------------------
         case default

            stop 'Invalid error flag found in transition prediction'

         end select

         ! Write output
         write(34,*) 'xTransForcedNew  = ', xTransForced(surf)
         write(34,*) 'xConvergeTP      = ', xConvergeTP(surf)
         write(34,*) 'jConvergeTP      = ', jConvergeTP(surf)
         write(34,*) 'convergedTP      = ', convergedTP(surf)

         ! Final check on upper and lower bounds of transition point
         if (xTransForced(surf).gt.aftLimitTP) then 
            xTransForced(surf) = aftLimitTP
            write(34,*) 'Final checks are forcing TP to aftLimitTP = ',
     &          aftLimitTP 
         else if (xTransForced(surf).lt.0.0) then
            xTransForced(surf) = 0.00
            write(34,*) 'Final bounds check forcing TP to 0.00'
         end if 

         ! __________________________________
         ! update global transition positions
         ! (as used in subroutine "setup")
         if (.not.convergedTPOld(surf)) then
            if (surf.eq.1) then
               transup = xTransForced(surf)
            else if (surf.eq.2) then 
               translo = xTransForced(surf)
            end if
         end if

         select case (surf)
         case (1)
            write(34,*) 'NEW TRANSUP      = ', transup
         case (2)
            write(34,*) 'NEW TRANSLO      = ', translo
         end select ! surf

         ! update flag for TP convergence
         if (convergedTP(surf)) then
            convergedTPOld(surf) = .true.
         end if

      end do ! surf

      ! _____________________________________________
      ! check if all transition points have converged
      do surf=1,2
         if (convergedTP(surf)) then
            convergedAllTPs = .true.
         else 
            convergedAllTPs = .false.
            exit ! takes you out of do-loop
         end if
      end do
      write (34,*) '------------------------------------------'
      write (34,*) '------------------------------------------'
      write(34,*) 'convergedAllTPs  = ', convergedAllTPs
!     &     'Top = ', convergedTP(1), 
!     &     'Bot = ', convergedTP(2), 
!      write(34,*) iterTP, transup, translo, xConvergeTP(1),
!     &     xConvergeTP(2)

      write(34,*)
      write(34,906)
 906  format(
     &     ' Top_Xtr',T15,   
     &     ' Bot_Xtr',T30,
     &     ' convergedAll', T45,
     &     ' convergedTop', T60,
     &     ' xConvergedTop',T75,
     &     ' convergedBot', T90, 
     &     ' xConvergedBot',T105,
     &     ' alpha',T120,
     &     ' iterTP')
      write(34,907) transup, translo, convergedAllTPs, convergedTP(1),
     &     xConvergeTP(1), convergedTP(2), xConvergeTP(2),alpha,iterTP
 907  format(1p,
     &     g14.7,T15,
     &     g14.7,T30,
     &     L,T45,
     &     L,T60,
     &     g14.7,T75,
     &     L,T90,
     &     g14.7,T105,
     &     g14.7,T120,
     &     I3)

      return
      end subroutine transitionUpdatePoints
