c----------------------------------------------------------------------
c     -- viscous residuals at boundary nodes --
c     -- includes body and farfield residual --      
c     -- based on probe (a. pueyo: impfarv.f impbodyv.f) --
c     -- m. nemec,  may 2000 --
c----------------------------------------------------------------------
      subroutine vbcrhs (jdim, kdim, x, y, xy, xyj, q, s, s0, press)

      use disscon_vars
#include "../include/arcom.inc"
#include "../include/optcom.inc"

      double precision hinf, xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim)
      double precision q(jdim,kdim,4), s(jdim,kdim,4), press(jdim,kdim)
      double precision utilde,vtilde,den,timetmp,s0(jdim,kdim,4)

      integer jsta,jsto

      alphar = alpha*pi/180.d0     
      hstfs  = 1.d0/gami + 0.5d0*fsmach**2.d0

c     -----------------------------------------------------------------
c     -- outer-boundary  k = kmax --
c     -----------------------------------------------------------------

      if(.not.periodic) then
         jsta=jbegin+1
         jsto=jend-1
      else
         jsta=jbegin
         jsto=jend
      end if

      do j = jsta,jsto

        k = kend
c     -- metrics terms --
        par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
        xy3 = xy(j,kend,3)/par
        xy4 = xy(j,kend,4)/par

        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = press(j,k)*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn1 = xy3*u + xy4*v
        vt1 = xy4*u - xy3*v
        a1  = a
        rho1= rho
        pr1 = pr
c     -- reset free stream values with circulation correction --
        if (circul) then
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = dsqrt(af2)
        else
          uf = uinf
          vf = vinf
          af = dsqrt(gamma*pinf/rhoinf)
        endif
c     
        vninf = xy3*uf + xy4*vf
        vtinf = xy4*uf - xy3*vf
        ainf = af

c     -- k=kend-1 --
        k = kend-1
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = press(j,k)*xyj(j,k)
        a   = sqrt(gamma*pr/rho)                    
        vn2 = xy3*u + xy4*v
        vt2 = xy4*u - xy3*v
        a2  = a
        rho2= rho
        pr2 = pr

        k = kend
        if (dissCon) then
           s0(j,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s0(j,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
           s(j,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s(j,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
        else
           s(j,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s(j,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
        end if

c     -- inflow --
        if (vn1.le.0.d0) then
           if (dissCon) then
              s0(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s0(j,k,4) = vtinf - vt1
              s(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s(j,k,4) = vtinf - vt1
           else
              s(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s(j,k,4) = vtinf - vt1
           end if

c     -- outflow --
        else
           if (dissCon) then
              s0(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s0(j,k,4) = vt2 - vt1
              s(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s(j,k,4) = vt2 - vt1
           else
              s(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s(j,k,4) = vt2 - vt1
           end if
        endif
      end do

      if(.not.periodic) then

c     -----------------------------------------------------------------
c     -- inflow boundary  j = 1 --
c     -----------------------------------------------------------------
      do k = kbegin,kend
        pr1 = press(1,k)*xyj(1,k)
        pr2 = press(2,k)*xyj(2,k)
        if (dissCon) then
           s0(1,k,1) = -( q(1,k,1)*xyj(1,k)-q(2,k,1)*xyj(2,k) )
           s0(1,k,2) = -( q(1,k,2)*xyj(1,k)-q(2,k,2)*xyj(2,k) )
           s0(1,k,3) = -( q(1,k,3)*xyj(1,k)-q(2,k,3)*xyj(2,k) )
           s0(1,k,4) = -( pr1-pr2 )
           s(1,k,1) = -( q(1,k,1)*xyj(1,k)-q(2,k,1)*xyj(2,k) )
           s(1,k,2) = -( q(1,k,2)*xyj(1,k)-q(2,k,2)*xyj(2,k) )
           s(1,k,3) = -( q(1,k,3)*xyj(1,k)-q(2,k,3)*xyj(2,k) )
           s(1,k,4) = -( pr1-pr2 )
        else
           s(1,k,1) = -( q(1,k,1)*xyj(1,k)-q(2,k,1)*xyj(2,k) )
           s(1,k,2) = -( q(1,k,2)*xyj(1,k)-q(2,k,2)*xyj(2,k) )
           s(1,k,3) = -( q(1,k,3)*xyj(1,k)-q(2,k,3)*xyj(2,k) )
           s(1,k,4) = -( pr1-pr2 )
        end if
      end do

c     -----------------------------------------------------------------
c     -- outflow boundary  j = jend --
c     -----------------------------------------------------------------
      do k = kbegin,kend
        pr1 = press(jmax,k)*xyj(jmax,k)
        pr2 = press(jmax-1,k)*xyj(jmax-1,k)
        j = jmax
        if (dissCon) then
           s0(j,k,1) = -( q(j,k,1)*xyj(j,k)-q(j-1,k,1)*xyj(j-1,k) )
           s0(j,k,2) = -( q(j,k,2)*xyj(j,k)-q(j-1,k,2)*xyj(j-1,k) )
           s0(j,k,3) = -( q(j,k,3)*xyj(j,k)-q(j-1,k,3)*xyj(j-1,k) )
           s0(j,k,4) = -( pr1-pr2 )
           s(j,k,1) = -( q(j,k,1)*xyj(j,k)-q(j-1,k,1)*xyj(j-1,k) )
           s(j,k,2) = -( q(j,k,2)*xyj(j,k)-q(j-1,k,2)*xyj(j-1,k) )
           s(j,k,3) = -( q(j,k,3)*xyj(j,k)-q(j-1,k,3)*xyj(j-1,k) )
           s(j,k,4) = -( pr1-pr2 )
        else
           s(j,k,1) = -( q(j,k,1)*xyj(j,k)-q(j-1,k,1)*xyj(j-1,k) )
           s(j,k,2) = -( q(j,k,2)*xyj(j,k)-q(j-1,k,2)*xyj(j-1,k) )
           s(j,k,3) = -( q(j,k,3)*xyj(j,k)-q(j-1,k,3)*xyj(j-1,k) )
           s(j,k,4) = -( pr1-pr2 )
        endif
      end do

      end if !not periodic

c     -----------------------------------------------------------------
c     -- airfoil body --
c     -----------------------------------------------------------------

c     : Calculate Omega=omegaa*sin(2*pi*omegaf*t) or Omega=omegaa

      if (omegaf .eq. 0.d0) then
         omega=omegaa
      else
         if (numiter-istart .le. nk_skip) then
            timetmp=(numiter-istart)*dt2
         else
            timetmp=(numiter-istart-nk_skip)*dt2+nk_skip*dtbig
         end if
         omega=omegaa*dsin(2.d0*pi*omegaf*timetmp)
      endif

      do j = jtail1,jtail2
         
         if (dissCon) then
            s0(j,1,1) = -( q(j,1,1)*xyj(j,1)-q(j,2,1)*xyj(j,2) )
            s(j,1,1) = -( q(j,1,1)*xyj(j,1)-q(j,2,1)*xyj(j,2) 
     &           + lamDiss*(q(j,1,1)*xyj(j,1) - rhoinf) )
         else
            s(j,1,1) = -( q(j,1,1)*xyj(j,1)-q(j,2,1)*xyj(j,2) )
         end if
         
         if (periodic .and. omega .ne. 0.d0) then  !rotating cylinder BC

            den=SQRT(xy(j,1,3)*xy(j,1,3) + xy(j,1,4)*xy(j,1,4))

            utilde=0.5d0*omega*xy(j,1,4) / den
            vtilde=-0.5d0*omega*xy(j,1,3) / den 

            s(j,1,2) = -( q(j,1,2)/q(j,1,1)-utilde )
            s(j,1,3) = -( q(j,1,3)/q(j,1,1)-vtilde )
         
         else

           if (.not.sbbc.or.(sbbc.and.(j.lt.jsls.or.j.gt.jsle)) ) then 
              if (dissCon) then
                 s0(j,1,2) = -( q(j,1,2)*xyj(j,1) )
                 s0(j,1,3) = -( q(j,1,3)*xyj(j,1) )
                 s(j,1,2) = -( q(j,1,2)*xyj(j,1)
     &                + lamDiss*(q(j,1,2)*xyj(j,1) - rhoinf*uinf) )
                 s(j,1,3) = -( q(j,1,3)*xyj(j,1)
     &                + lamDiss*(q(j,1,3)*xyj(j,1) - rhoinf*vinf) )
              else
                 s(j,1,2) = -( q(j,1,2)*xyj(j,1) )
                 s(j,1,3) = -( q(j,1,3)*xyj(j,1) )
              end if
           else  !suction/blowing BC
             s(j,1,2)=-( q(j,1,2) - q(j,1,1)*omega*
     &             dcos(sbeta*pi/180.d0-sdelta) )*xyj(j,1)
             s(j,1,3)=-( q(j,1,3)- q(j,1,1)*omega*
     &            dsin(sbeta*pi/180.d0-sdelta) )*xyj(j,1)    
           end if

         endif

         if (dissCon) then
            s0(j,1,4) = -( press(j,1)*xyj(j,1)-press(j,2)*xyj(j,2) )
            s(j,1,4) = -( press(j,1)*xyj(j,1)-press(j,2)*xyj(j,2) 
     &           + lamDiss*(press(j,1)*xyj(j,1) - pinf) )
         else
            s(j,1,4) = -( press(j,1)*xyj(j,1)-press(j,2)*xyj(j,2) )
         end if
         
      end do


c     -----------------------------------------------------------------
c     -- Periodic boundary -- set jmaxold point from j = 1 point
c     -----------------------------------------------------------------

c$$$      if(periodic) then
c$$$c           note jacobian scaling ok 
c$$$
c$$$         
c$$$      
c$$$         do k = kbegin,kend
c$$$        
c$$$            s(jmaxold,k,1)=-(q(jmaxold,k,1)-q(1,k,1))
c$$$            s(jmaxold,k,2)=-(q(jmaxold,k,2)-q(1,k,2))
c$$$            s(jmaxold,k,3)=-(q(jmaxold,k,3)-q(1,k,3))
c$$$            s(jmaxold,k,4)=-(q(jmaxold,k,4)-q(1,k,4))
c$$$         end do
c$$$
c$$$      end if

      return 
      end                       !vbcrhs
