      subroutine gradcoef(ixy,jdim,kdim,press,xyj,coef,prss)            
cCO   Computes the coefficient epsilon/kappa2 for the artificial
cCO   dissipation, and returns it as coef.  prss is just some space
cCO   to use temporarialy.
cCO   Note: coef at some nodes = 0, if zerote is .true.
c                                                                       
c     ixy = 1  for xi  direction                                        
c     ixy = 2  for eta direction                                        
#include "../include/arcom.inc"
c                                                                       
      dimension press(jdim,kdim),xyj(jdim,kdim)
      dimension coef(jdim,kdim),prss(jdim,kdim)     
c                                                                       
      if (ixy.eq.1) then 
c                                                                       
c       compute pressure grad                                
c                                                                       
        do 1 k = kbegin,kend
        do 1 j = jbegin,jend                                          
          coef(j,k) = 0.                                     
          prss(j,k) = press(j,k)*xyj(j,k)                   
 1      continue
c                                                                       
c       bypass logic if dis2x = 0.0                      
c                                                                       
        if (dis2x.eq.0.0) return             

        do 2 k = klow,kup
        do 2 j = jlow,jup
          jp1=jplus(j)
          jm1=jminus(j)
          r1 = prss(jp1,k) -2.* prss(j,k) + prss(jm1,k)      
          r2 = prss(jp1,k) +2.* prss(j,k) + prss(jm1,k)    
          coef(j,k) = abs( r1/r2 )                           
 2      continue
c                                                                    
c       cmesh bc                                            
c                                                                    
        if(.not.periodic)then                                          
          do 3 k = klow,kup
            coef(jbegin,k) = coef(jlow,k)                    
            coef(jend,k) = coef(jup,k)                       
            prss(jbegin,k) = coef(jlow,k)                        
            prss(jend,k) = coef(jup,k)                       
 3        continue
        endif                                                          
c                                                                    
c       smooth out pressure coefficient                    
c                                                                    
        do 4 k = klow,kup
        do 4 j = jlow,jup          
          jp1=jplus(j)
          jm1=jminus(j)
          prss(j,k) = max(coef(jm1,k),coef(j,k),coef(jp1,k))
 4      continue
c                                                                    
        do 5 k = klow,kup
        do 5 j = jlow,jup
          jp1=jplus(j)
          jm1=jminus(j)
          coef(j,k) = prss(j,k)                                
          coef(j,k) = 0.5*prss(j,k)+0.25*(prss(jp1,k)+prss(jm1,k))
 5      continue
c                                                                   
        if(.not.periodic)then
c         c mesh bc                                                    
          do 12 k = klow,kup
            coef(jbegin,k) = coef(jlow,k)           
            coef(jend,k) = coef(jup,k)
 12       continue
        endif                                                          
c                                                                    
        if (zerote) then
          if(.not.periodic .and. jtail1.ne.jlow)then         
c           -stay away from trailing edge                       
            jedge1l = jtail1-1                                          
            jedge1u = jtail1+1                                          
            jedge2l = jtail2-1                                          
            jedge2u = jtail2+1                                          
c                                                                       
c           -turn off second order near trailing edge
            do 10 k = klow,kup                                 
            do 10 j = jedge1l,jedge1u                    
               coef(j,k) = 0.
 10         continue
            do 11 k = klow,kup                                
            do 11 j = jedge2l,jedge2u                       
               coef(j,k) = 0.
 11         continue
          endif
        endif
      elseif(ixy .eq. 2)then                                           
c                                                                       
c       gradiant coefficent in eta direction                     
c                                                                          
        do 21 k = kbegin,kend
        do 21 j = jbegin,jend                                             
          coef(j,k) = 0.                                       
          prss(j,k) = press(j,k)*xyj(j,k)                        
 21     continue
c                                                                          
c       bypass logic if dis2y = 0.0                        
c                                                                       
        if(dis2y.eq.0.0)return
c                                                                       
        do 23 k = klow,kup
          kpl = k+1
          kmi = k-1
          do 22 j = jlow,jup
            r1 = prss(j,kpl) -2.* prss(j,k) + prss(j,kmi)    
            r2 = prss(j,kpl) +2.* prss(j,k) + prss(j,kmi)          
            coef(j,k) = abs( r1/r2 )                               
 22       continue
 23     continue
c                                                                       
c       bc                                                     
c     
        do 24 j = jlow,jup                                             
          coef(j,kbegin) = coef(j,klow)
          coef(j,kend) = coef(j,kup)
          prss(j,kbegin) = coef(j,klow)
          prss(j,kend) = coef(j,kup)
 24     continue                                                       
c                                                                       
c       smooth out pressure coefficient                    
c                                                                       
        do 26 k = klow,kup
        do 26 j = jlow,jup
          prss(j,k) = coef(j,k)
 26     continue
c                                                                       
        do 28 k = klow,kup
          kpl = k+1                                                 
          kmi = k-1                                               
          do 27 j = jlow,jup
            coef(j,k) = 0.25*prss(j,kmi)+0.5*prss(j,k)+0.25*prss(j,kpl)
 27       continue
 28     continue
c                                                                       
        if (zerote) then
          if(.not.periodic .and. jtail1.ne.jlow)then
c           -stay away from trailing edge                   
            jedge1l = jtail1-1
            jedge1u = jtail1+1
            jedge2l = jtail2-1
            jedge2u = jtail2+1
c
c           -turn off second order near trailing edge
            do 110 k = klow,kup
            do 110 j = jedge1l,jedge1u
              coef(j,k) = 0.
 110        continue
            do 111 k = klow,kup
            do 111 j = jedge2l,jedge2u
              coef(j,k) = 0.
 111        continue
          endif
        endif
      endif 
c                                                                       
 999   return                                                         
       end                                                            
