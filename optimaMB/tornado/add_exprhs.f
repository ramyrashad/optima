      SUBROUTINE add_exprhs(jdim,kdim,nmax,jlow,jup,klow,kup,
     &     s,rsa,exprhs,exprsa)

      implicit none

#include "../include/common.inc"
#include "../include/parms.inc"

      INTEGER jdim,kdim,jlow,jup,klow,kup,j,k,n,nmax
      DOUBLE PRECISION s(jdim,kdim,4),rsa(jdim,kdim)
      DOUBLE PRECISION exprhs(jdim,kdim,4),exprsa(jdim,kdim)

      DO n = 1,4
         DO k = klow,kup
            DO j = jlow,jup
               s(j,k,n) = s(j,k,n) + exprhs(j,k,n)
            ENDDO
         ENDDO
      ENDDO

      if (nmax .eq. 5) then
         DO k = klow,kup
            DO j = jlow,jup
               rsa(j,k) = rsa(j,k) + exprsa(j,k)
            ENDDO
         ENDDO
      end if

      RETURN
      END
