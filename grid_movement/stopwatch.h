// This is a simple class for keeping track of the CPU time spent running 
// different parts of a program.
//
// Chad Oldfield, Novemeber 2005
//
// Modified by Anh Truong, October 2006

#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <ctime>

class Stopwatch
{
  private:
    clock_t start, previous;
    double seconds;  // = 1.0/((double) CLOCKS_PER_SEC);
  public:
    Stopwatch();
    ~Stopwatch() {};
    void Reset() {start = previous = clock();}
    double Elapsed();
    double Interval();
};
#endif
