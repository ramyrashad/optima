c :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
c ::                                                                     ::
c :: mo's corner:                                                        ::
c ::                                                                     ::
c :: this is the subroutine which creates the extra term                 ::
c :: introduced to the RHS by subiterations:                             ::
c ::                                                                     ::
c ::                                                                     ::
c ::                p    n  n-1                                          ::
c ::             3*q -4*q +q                                             ::
c ::         -  ---------------                                          ::
c ::                 2*dt                                                ::
c ::                                                                     ::
c ::                                                                     ::
c :: modifications for subiteration Oct 6, 2004 by mt                    ::
c :: calling routine: get_rhs                                            ::
c ::                                                                     ::
c :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

      SUBROUTINE nk_subit(q,qps,qos,s,jdim,kdim,jlow,jup,klow,kup,nmax,
     &     turre,turps,turos,rsa,xyj)

      implicit none

#include "../include/common.inc"

      integer n,k,j, jdim, kdim,jlow,jup,klow,kup,nmax
      double precision qps(jdim,kdim,4),q(jdim,kdim,4),qos(jdim,kdim,4)
      double precision turps(jdim,kdim),turre(jdim,kdim),xyj(jdim,kdim)
      double precision s(jdim,kdim,4),rsa(jdim,kdim),turos(jdim,kdim)         
      double precision akk,mpr1,mpr2,mpr3


      IF(imarch.EQ.2)THEN

c$$$         if (sbbc .and. numiter-istart .eq. 1) then
c$$$c        Implicit Euler at first iteration for suction/blowing BC
c$$$            mpr1=1.d0/dt2
c$$$            mpr2=-1.d0/dt2
c$$$            mpr3=0.d0

         if (numiter.ne.iend+nkskip+1.or.nkskip.eq.0) then
c        BDF2 otherwise
            mpr1=3.d0/(2.d0*dt2)
            mpr2=-4.d0/(2.d0*dt2)
            mpr3=1.d0/(2.d0*dt2)

         else
c        special BDF2 at time step size change
            mpr1=(2.d0*dtsmall+dtbig)/(dtsmall*(dtsmall+dtbig))
            mpr2=-(dtsmall+dtbig)/(dtsmall*dtbig)
            mpr3=dtsmall/(dtbig*(dtsmall+dtbig))
         end if

         DO n=1,4
            DO k=klow,kup
               DO j=jlow,jup
                  
                  s(j,k,n)=s(j,k,n)-(mpr1*qps(j,k,n)+mpr2*q(j,k,n)+
     &                 mpr3*qos(j,k,n) )

                    
               ENDDO
            ENDDO
         ENDDO


         if (nmax .eq. 5) then
            DO k=klow,kup
               DO j=jlow,jup
                  
                  rsa(j,k)=rsa(j,k)-(mpr1*turps(j,k)+mpr2*turre(j,k)+
     &                 mpr3*turos(j,k) )/xyj(j,k)
                   
               ENDDO
            ENDDO
         end if

      ENDIF

      IF(imarch.EQ.4)THEN

         akk = ajk(jstage,jstage)
         DO n=1,4
            DO k=klow,kup
               DO j=jlow,jup
                  s(j,k,n)=s(j,k,n)-(qps(j,k,n)-q(j,k,n))/(akk*dt2)
               ENDDO
            ENDDO
         ENDDO

         if (nmax .eq. 5) then
            DO k=klow,kup
               DO j=jlow,jup
                  
                  rsa(j,k)=rsa(j,k)-(turps(j,k)-turre(j,k)) / 
     &                 (akk*dt2*xyj(j,k))
                    
               ENDDO
            ENDDO
         end if

      ENDIF

      RETURN
      END
     
