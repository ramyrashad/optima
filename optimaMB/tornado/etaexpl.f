      subroutine etaexpl(lvl,lev_top,mglev,jdim,kdim,q,s,press,sndsp,   
     &      turmu,fmu,vort,turre,vk,ve,x,y,xy,xyj,ett,ds,vv,ccy,uu,
     &      coef2y,coef4y,spect,precon,tmp)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/trip.inc"      
#include "../include/scratch.inc"
#include "../include/turb.inc"

      integer jdim,kdim,ncopystart,ncopyend,i,lq,lg,ix,iy,ii
      integer lvl,lev_top,mglev,nteavgstart,nteavgend,ixy,n

      double precision q(jdim*kdim*4),spect(jdim*kdim*3),s(jdim*kdim*4)
      double precision press(jdim*kdim),precon(jdim*kdim*8)
      double precision sndsp(jdim*kdim),xy(jdim*kdim*4),xyj(jdim*kdim)
      double precision ett(jdim*kdim),  ds(jdim*kdim),tmp(jdim*kdim*4)
      double precision x(jdim*kdim),    y(jdim*kdim),turmu(jdim*kdim)
      double precision vort(jdim*kdim), turre(jdim*kdim),fmu(jdim*kdim)
      double precision vk(jdim*kdim),   ve(jdim*kdim)
      double precision vv(jdim*kdim),   ccy(jdim*kdim),  uu(jdim*kdim)
      double precision coef2y(jdim*kdim),coef4y(jdim*kdim)

      logical frozen

      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

c ********************************************************************
c **                      explicit operators                        **
c ********************************************************************

      if (mg .or. gseq) then
c       note: divide operation higher priority than multiplication
        ncopystart = ncopy/mglev*(lvl-1) + 1
        ncopyend   = ncopy/mglev*lvl
        nteavgstart= nteavg/mglev*(lvl-1) + 1
        nteavgend  = nteavg/mglev*lvl
      else
        ncopystart = 1
        ncopyend   = ncopy
        nteavgstart= 1
        nteavgend  = nteavg
      endif

c     -explicit operator in eta
      DO i=nblkstart,nblkend
         lq=lqptr(i)
         lg=lgptr(i)
         CALL rhsy(jmax(i),kmax(i),q(lq),s(lq),
     &        press(lg),xy(lq),xyj(lg),ett(lg),d,
     &        jminbnd(i),jmaxbnd(i),kbegin(i),kend(i),klow(i),kup(i))
      ENDDO

      ix = 3
      iy = 4
      DO i=nblkstart,nblkend
         lg=lgptr(i)
         lq=lqptr(i)
         call eigval(ix,iy,jmax(i),kmax(i),q(lq),press(lg),
     &        sndsp(lg),xyj(lg),xy(lq),ett(lg),vv(lg),ccy(lg),
     &        jminbnd(i),jmaxbnd(i),kbegin2(i),kend2(i))
      ENDDO

c     -viscous rhs
cmt ~~~ limits are a little bit strange, needs more thoughts!
      IF (viseta) THEN
         DO ii=nblkstart,nblkend
            CALL visrhsny(ii,jmax(ii),kmax(ii),
     &           q(lqptr(ii)),press(lgptr(ii)),
     &           s(lqptr(ii)),turmu(lgptr(ii)),
     &           fmu(lgptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &           d,d(1,2),d(1,3),d(1,4),d(1,5), 
     &           d(1,6),d(1,7),d(1,8),d(1,9),d(1,10),
     &           jminbnd(ii),jmaxbnd(ii),
     &           kbegin2(ii),kend2(ii),
     &           kbegin2(ii)-1,kend2(ii)-1)
         ENDDO
      ENDIF


c     ****************************************************************
c     *                                                              *
c     *       matrix artificial dissipation in eta direction         *
c     *                                                              *
c     ****************************************************************

      ix = 3
      iy = 4

      IF (idmodel.EQ.2) THEN
         do 105 ii=nblkstart,nblkend
            lg=lgptr(ii)
            lq=lqptr(ii)
            call mspecty(jmax(ii),kmax(ii),vv(lg),ccy(lg),
     &           xyj(lg),xy(lq),spect(lspptr(ii)),
     &           jminbnd(ii),jmaxbnd(ii),
     &           kbegin2(ii),kend2(ii))
 105     continue

cmt ~~~ dis2x=0 does not work, stops at drdq.f! 
cmt ~~~ that's why this part is commented out. 

c     -calculate the gradient coef in y & store it in array coef2y
c     -bypass logic if dis2y = 0.0
c         if (dis2y.ne.0.0) then
c            ixy=2
c            do 106 ii=nblkstart,nblkend
c               call gradcoef(ixy,jmax(ii),kmax(ii),press(lgptr(ii)),
c     &              xyj(lgptr(ii)),coef2y(lgptr(ii)),work1(lgptr(ii)),
c     &              jminbnd(ii),jmaxbnd(ii),
c     &              kbegin2(ii),kend2(ii),
c     &              xi1mesh(ii),xi2mesh(ii),.true.,.true.)
c 106        continue
c         endif

c     -compute the second and fourth order dissipation coefficients
         do 120 ii=nblkstart,nblkend
            call mcoef24y(jmax(ii),kmax(ii),
     &           coef2y(lgptr(ii)),coef4y(lgptr(ii)),
     &           jminbnd(ii),jmaxbnd(ii),
     &           kbegin2(ii),kend2(ii))
 120     continue

c     -compute the second and fourth difference dissipation
         do 130  ii=nblkstart,nblkend
            call expmaty(jmax(ii),kmax(ii),q(lqptr(ii)),
     &           coef2y(lgptr(ii)),
     &           coef4y(lgptr(ii)),sndsp(lgptr(ii)),s(lqptr(ii)),
     &           spect(lspptr(ii)),xyj(lgptr(ii)),press(lgptr(ii)),
     &           ccy(lgptr(ii)),vv(lgptr(ii)),xy(lqptr(ii)),
     &           d(1,1),d(1,5),d(1,9),
     &           c(1,1),c(1,2),c(1,3),c(1,4),c(1,5),c(1,6),
     &           jminbnd(ii),jmaxbnd(ii),
     &           kbegin2(ii),kend2(ii))
 130     continue

      ELSE

c     ****************************************************************
c     *                                                              *
c     *       scalar artificial dissipation in eta direction         *
c     *                                                              *
c     ****************************************************************

c     -form spectral radii for dissipation model
         DO i=nblkstart,nblkend
            lq = lqptr(i)
            lg = lgptr(i)
            CALL specty(jmax(i),kmax(i),vv(lg),ccy(lg),
     &           xyj(lg),xy(lq),coef4y(lg),precon(lprecptr(i)),
     &           jlow(i),jup(i),kbegin2(i),kend2(i))
         ENDDO

c     **************************************************************
c     Dissipation terms : meth = 2, 3                        
c     -set up coefficients and variables for meth = 2,3 
c     -form pressure coefficients                     
c     -artificial dissipation coefficients            
c     -artificial dissipation                           
c     **************************************************************

cmt ~~~ dis2x=0 does not work, stops at drdq.f! 
cmt ~~~ that's why this part is commented out. 

c         IF (dis2y.NE.0.d0) THEN
c            ixy = 2
c     -calculate the gradient coef in y and store it in array coef2y
c            DO i=nblkstart,nblkend
c               lq = lqptr(i)
c               lg = lgptr(i)
c               CALL gradcoef(ixy,jmax(i),kmax(i),press(lg),
c     &              xyj(lg),coef2y(lg),work1(lg),
c     &              jminbnd(i),jmaxbnd(i),kbegin2(i),kend2(i),
c     &              xi1mesh(i),xi2mesh(i),.true.,.true.)
c            ENDDO
c         ENDIF

c     -compute the second and fourth order dissipation coefficients
         DO i=nblkstart,nblkend
            lq = lqptr(i)
            lg = lgptr(i)
            CALL coef24y(jmax(i),kmax(i),coef2y(lg),coef4y(lg),
     &           d,d(1,2),jlow(i),jup(i), kbegin2(i),kend2(i))
         ENDDO

c     Stan D.R.
c     -in order to use all halo rows, must avoid
c     problems near corners/trailing edges.
c     -so just traverse within block boundaries for
c     streamwise indices i.e remove affect of jkhalo
         DO n=1,4
            DO i=nblkstart,nblkend
               lq = lqptr(i)
               lg = lgptr(i)
               CALL filtery(n,jmax(i),kmax(i),q(lq),xyj(lg),
     &              coef2y(lg),coef4y(lg),work1(lwf1ptr(i)),tmp(lq),
     &              jlow(i),jup(i),kbegin(i),kend(i))
            ENDDO
         ENDDO

         DO i=nblkstart,nblkend
            lq = lqptr(i)
            lg = lgptr(i)
            CALL predis(jmax(i),kmax(i),q(lq),s(lq),
     &           xyj(lg),sndsp(lg),precon(lprecptr(i)),tmp(lq),
     &           jlow(i),jup(i),klow(i),kup(i))
         ENDDO

      ENDIF
      
      RETURN
      END

