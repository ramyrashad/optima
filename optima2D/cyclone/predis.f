      subroutine predis(jdim,kdim,q,xyj,sndsp,precon,tmp)
c
#include "../include/arcom.inc"
cdu
cdu  this routine multiplies the vector tmp by the preconditioner
cdu  in conversative variables
c
      double precision q(jdim,kdim,4),xyj(jdim,kdim),sndsp(jdim,kdim)
      double precision precon(jdim,kdim,6),tmp(jdim,kdim,4)
c
c     note: gami=gamma - 1
c
      do 1000 k=klow,kup
         do 1000 j=jlow,jup
c
            rho=q(j,k,1)*xyj(j,k)
            u=q(j,k,2)/q(j,k,1)
            v=q(j,k,3)/q(j,k,1)
            c=sndsp(j,k)
c
c     multiply by M inverse
c
            t4=gami*(precon(j,k,2)*tmp(j,k,1)-u*tmp(j,k,2)-
     +           v*tmp(j,k,3)+tmp(j,k,4))
            t1=t4/rho/c
            t4=t4-tmp(j,k,1)*c**2
            t2=(tmp(j,k,2)-u*tmp(j,k,1))/rho
            t3=(tmp(j,k,3)-v*tmp(j,k,1))/rho
c
c     multiply by Gamma
c
            t1=t1/precon(j,k,1)
c
c     multiply by M
c
            tmp(j,k,1)=(rho*t1-t4/c)/c
            tmp(j,k,2)=u*tmp(j,k,1)+rho*t2
            tmp(j,k,3)=v*tmp(j,k,1)+rho*t3
            tmp(j,k,4)=rho*((precon(j,k,2)/c+c/gami)*t1+
     +           u*t2+v*t3)-precon(j,k,2)*t4/c**2
c
 1000 continue
      return
      end
