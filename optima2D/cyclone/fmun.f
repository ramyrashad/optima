      subroutine fmun(jdim,kdim,q,press,fmu,sndsp) 

#include "../include/arcom.inc"

      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension sndsp(jdim,kdim),fmu(jdim,kdim)          

c     -- frozen jacobian arrays --
      double precision c2xf(maxj,maxk), c4xf(maxj,maxk)
      double precision c2yf(maxj,maxk), c4yf(maxj,maxk)
      double precision fmuf(maxj,maxk), turmuf(maxj,maxk)
      double precision vortf(maxj,maxk), sapdf(maxj,maxk)
      double precision sadx(maxj,maxk), sady(maxj,maxk)
      double precision suuf(maxj,maxk), svvf(maxj,maxk)
      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &      sapdf, sadx, sady, suuf, svvf
      
      c2b = 198.6d0/tinf                                                  
      c2bp = c2b + 1.d0                                                   
      do k = kbegin,kend                                           
        do j = jbegin,jend
          tt    = sndsp(j,k)*sndsp(j,k)  
          fmu(j,k) = c2bp*tt*sndsp(j,k)/(c2b+tt) 
        end do
      end do

      return                                                        
      end                                                           
