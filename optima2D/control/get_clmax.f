c-----------------------------------------------------------------------
c     Program name: get_clmax
c     
c     This subroutine performs an angle of attack sweep to deterime
c     the AOA that gives Cl_max for an airfoil with a given set of
c     operating conditions
c
c-----------------------------------------------------------------------

      subroutine get_clmax (clmax, alphamax, jdim, kdim, ndim, 
     &  ifirst, indx, icol, iex, q, cp, xy, xyj, x, y, cp_tar, 
     &  fmu, vort, turmu, work1, ia, ja, ipa, jpa, iat, jat, ipt, 
     &  jpt, as, ast, pa, pat, itfirst, ifun, clt_r)

#ifdef _MPI_VERSION
      use mpi
#endif


      !-- Declare variables

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer 
     &     ifunc, ifun, jdim, kdim, ndim, ifirst, icol(9), EOF,
     &     indx(jdim,kdim), iex(jdim,kdim,ndim),
     &     ia(jdim*kdim*ndim+2), namelen,
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1),
     &     iat(jdim*kdim*ndim+2),
     &     jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1),
     &     d1, d10, d100

      double precision 
     &     clmax, alphamax, delta, q(jdim,kdim,ndim), 
     &     cp(jdim,kdim), xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim), 
     &     y(jdim,kdim),cp_tar(jbody,2), fmu(jdim,kdim),vort(jdim,kdim),
     &     turmu(jdim,kdim),as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), pat(jdim*kdim*ndim*ndim*5+1),
     &     work1(jdim*kdim,100), alpha_neg, alpha_pos, clt_neg, clt_pos,
     &     qp(maxjk,nblk), qold(maxjk,nblk), D_hat(maxjk,nblk,6),
     &     exprhs(maxjk,nblk), a_jk(6,6), clt_r

      !-- Execute subroutine
      i=1
      ifunc = ifun
      delta = 0.15

      !-- If angle of attack sweeps are turned off, then set clmax 
      !-- and alphamax to the values obtained from the last successful 
      !-- iteration before the weight update procedure and exit 
      !-- subroutine

      if (.not.wt_aoa) then
         
         clmax = clt_r
         alphamax = alpha

         write(scr_unit,9) rank
 9       format
     & (/3x,'Skipping angle-of-attack sweep on processor [',i3,' ]')

         return

      end if

      !-- Evaluate lift at (alpha + delta) and (alpha - delta)

      write(scr_unit,10) rank
 10   format
     & (/3x,'Executing angle-of-attack sweep on processor [',i3,' ]')

      clmax = 0.0
      alphamax = 0.0
      EOF = 0
      
      !-- Store lift value and corresponding AOA
      
      clt_0 = clt_r
      alpha_0 = alpha

      !-- Evaluate lift at (alpha + delta)

      !-- Read AOA sweep values from file

      read(aoa_unit,11, IOSTAT = EOF) clt_pos, alpha_pos
 11   format(2e40.32)

      !-- If there are AOA sweep values on file, then skip flow solve

      if (EOF==0) then
      
         write(scr_unit,13) i
 13      format(/3x, 'Reading AOA sweep value [',i1,'] from file
     & to obtain Cl at (alpha_0 + delta)')   
         i=i+1
         goto 140

      end if

      write(scr_unit,14)
 14   format
     &(///3x,'Performing flow solve to obtain Cl at (alpha_0 + delta)')

      alpha = alpha + delta

      call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex, ia,
     &      ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, qp, 
     &      qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,a_jk,
     &      ifunc)

      clt_pos = clt
      alpha_pos = alpha

      !-- Reset alpha

      alpha = alpha_0

      !-- Write AOA sweep values to file
         
      if (EOF.ne.0) write(aoa_unit,11) clt_pos, alpha_pos

 140  continue

      !-- Evaluate lift at (alpha - delta)

      !-- Read AOA sweep values from file

      read(aoa_unit,11, IOSTAT = EOF) clt_neg, alpha_neg

      !-- If there are AOA sweep values on file, then skip flow solve

      if (EOF==0) then
      
         write(scr_unit,143) i
 143     format(///3x, 'Reading AOA sweep value [',i1,'] from file
     & to obtain Cl at (alpha_0 - delta)')   
         i=i+1

         goto 142

      end if
      
      write(scr_unit,141)
 141  format
     & (///3x,'Performing flow solve to obtain Cl at (alpha_0 - delta)')

      alpha = alpha - delta

      call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex, ia,
     &      ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, qp, 
     &      qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,a_jk,
     &      ifunc)

      clt_neg = clt   
      alpha_neg = alpha

      !-- Reset alpha

      alpha = alpha_0

      !-- Write AOA sweep values to file
         
      if (EOF.ne.0) write(aoa_unit,11) clt_neg, alpha_neg

 142  continue
  
      !-- Cl_max scenario 1: (alpha_0 = alpha*)

 15   if ((clt_neg.lt.clt_0).and.(clt_0.gt.clt_pos)) then

         write(scr_unit,155) clt_neg, clt_0, clt_pos, clt_0, alpha_0 
 155     format(/3x, 'clt_neg = ',f10.6,'  clt_0 = ',f10.6,
     & '  clt_pos = ',f10.6,
     &   /3x,'Therefore scenario 1: (alpha_0 = alpha*)',
     &   /3x,'Set clmax = clt_0 = ',f10.6,    
     &   /3x,'Set alphamax = alpha_0 = ',f10.6,/)
         clmax = clt_0
         alphamax = alpha_0
      
         goto 18

      !-- Cl_max scenario 2: (alpha_0 < alpha*)

      elseif ((clt_neg.lt.clt_0).and.(clt_0.lt.clt_pos)) then

         write(scr_unit,151) clt_neg, clt_0, clt_pos
 151     format(/3x, 'clt_neg = ',f10.6,'  clt_0 = ',f10.6,
     & '  clt_pos = ',f10.6,
     &   /3x,'Therefore scenario 2: (alpha_0 < alpha*)',
     &   /3x,'Set alpha_0 = alpha_pos',/)  

         !-- Shift stencil of Cl values to the right

         clt_neg = clt_0
         clt_0 = clt_pos

         !-- Set alpha_0 = alpha_pos

         alpha_0 = alpha_pos

         !-- Evaluate Cl at (alpha_0 + delta) and store in clt_pos

         !-- Read AOA sweep values from file

         read(aoa_unit,11, IOSTAT = EOF) clt_pos, alpha_pos

         !-- If there are AOA sweep values on file, then skip flow solve

         if (EOF==0) then

            write(scr_unit,152) i
 152        format(///3x, 'Reading AOA sweep value [',i1,'] from file
     & to obtain Cl at (alpha_0 + delta)')   
            i=i+1

            goto 154

         end if

         write(scr_unit,153)
 153     format
     & (///3x,'Performing flow solve to obtain Cl at (alpha_0 + delta)')

         !-- Set value of alpha to be used for the flow solve
         
         alpha = alpha_0 + delta

         call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex, ia,
     &      ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, qp, 
     &      qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,a_jk,
     &      ifunc)

         clt_pos = clt
         alpha_pos = alpha

         !-- Write AOA sweep values to file
         
         if (EOF.ne.0) write(aoa_unit,11) clt_pos, alpha_pos

 154     continue
         
         goto 15

      !-- Cl_max scenario 3: (alpha_0 > alpha*)

      elseif ((clt_neg.gt.clt_0).and.(clt_0.gt.clt_pos)) then

         write(scr_unit,171) clt_neg, clt_0, clt_pos
 171     format(/3x, 'clt_neg = ',f10.6,'  clt_0 = ',f10.6,
     & '  clt_pos = ',f10.6,
     &   /3x,'Therefore scenario 3: (alpha_0 > alpha*)',
     &   /3x,'Set alpha_0 = alpha_neg',/)  

         !-- Shift stencil of Cl values to the left

         clt_pos = clt_0
         clt_0 = clt_neg

         !-- Set alpha_0 = alpha_neg

         alpha_0 = alpha_neg

         !-- Evaluate Cl at (alpha_0 - delta) and store in clt_neg

         !-- Read AOA sweep values from file

         read(aoa_unit,11, IOSTAT = EOF) clt_neg, alpha_neg

         !-- If there are AOA sweep values on file, then skip flow solve
 
         if (EOF==0) then

            write(scr_unit,172) i
 172        format(///3x, 'Reading AOA sweep value [',i1,'] from file
     & to obtain Cl at (alpha_0 - delta)')   
            i=i+1
 
            goto 170

         end if

         write(scr_unit,173)
 173     format
     & (///3x,'Performing flow solve to obtain Cl at (alpha_0 - delta)')

         !-- Set value of alpha to be used for the flow solve
         
         alpha = alpha_0 - delta

         call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex, ia,
     &      ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, qp, 
     &      qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,a_jk,
     &      ifunc)

         clt_neg = clt
         alpha_neg = alpha

         !-- Write AOA sweep values to file
         
         if (EOF.ne.0) write(aoa_unit,11) clt_neg, alpha_neg

 170     continue
         
         goto 15

      end if

 18   continue
     
      rewind(aoa_unit)
         
      return

      end
