c-----------------------------------------------------------------------
c     -- differentiation of advective terms for the S-A model --
c     -- m. nemec, oct. 2001 --
c-----------------------------------------------------------------------
      subroutine dsa_adv(jmax, kmax, jminbnd, jmaxbnd, kminbnd, kmaxbnd,
     &     q, turre, xy, xyj, uu, vv, bm2, bm1, db, bp1, bp2)

      implicit none

#include "../include/parms.inc"

      integer jmax, kmax, jminbnd, jmaxbnd, kminbnd, kmaxbnd
      integer js,je,ks,ke,j,k,jp,kp,n
      double precision q(jmax,kmax,4),   xy(jmax,kmax,4), xyj(jmax,kmax)
      double precision uu(jmax,kmax),    vv(jmax,kmax), turre(jmax,kmax)
      double precision bm2(5,jmax,kmax), bm1(5,jmax,kmax)
      double precision bp1(5,jmax,kmax), bp2(5,jmax,kmax)
      double precision db(5,jmax,kmax)

      double precision sgnu,app,apm,tmp,ri,ri2

c     -- local variables --
      double precision duudq(3), dvvdq(3)
      double precision wk1(maxjb,maxkb), wk2(maxjb,maxkb)

      logical frozen
      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

      js = jminbnd
      je = jmaxbnd
      ks = kminbnd+1
      ke = kmaxbnd-1

c     -- d(advective terms in xi direction)/d(q5^) --
      do k = ks,ke
         do j = js,je
            sgnu = sign(1.0,uu(j,k))
            app  = 0.5*(1.0 + sgnu)
            apm  = 0.5*(1.0 - sgnu)
            bm2(5,j,k) =  - uu(j,k)*app/xyj(j,k)*xyj(j-1,k)
            bp2(5,j,k) =  + uu(j,k)*apm/xyj(j,k)*xyj(j+1,k)
            db(5,j,k)  =  + uu(j,k)*(app-apm)
         end do
      end do

c     -- d(advective terms in eta direction)/d(q5^) --
      do k = ks,ke
         do j = js,je
            sgnu = sign(1.0,vv(j,k))
            app  = 0.5*(1.0 + sgnu)
            apm  = 0.5*(1.0 - sgnu)
            bm1(5,j,k) =  - vv(j,k)*app/xyj(j,k)*xyj(j,k-1)
            bp1(5,j,k) =  + vv(j,k)*apm/xyj(j,k)*xyj(j,k+1)
            db(5,j,k)   =  db(5,j,k) + vv(j,k)*(app-apm)
         end do
      end do

      js = jminbnd-1
      je = jmaxbnd
      ks = kminbnd
      ke = kmaxbnd-1

c     -- d(advective terms)/d(Qm^) where Qm => mean flow variables --
      do k = ks,ke
         kp = k + 1
         do j = js,je
            jp = j + 1
            tmp = turre(j,k)
            wk1(j,k) = turre(jp,k) - tmp
            wk2(j,k) = turre(j,kp) - tmp
         end do
      end do

      js = jminbnd
      je = jmaxbnd
      ks = kminbnd+1
      ke = kmaxbnd-1

      if (.not. frozen) then 
         do k = ks,ke
            do j = js,je
               
               ri  = 1.0/q(j,k,1)
               ri2 = ri*ri/xyj(j,k)
               ri = ri/xyj(j,k)

               duudq(1) = - ( q(j,k,2)*ri2*xy(j,k,1) + q(j,k,3)*ri2
     &              *xy(j,k,2) )
               duudq(2) = ri*xy(j,k,1) 
               duudq(3) = ri*xy(j,k,2)

               dvvdq(1) = - ( q(j,k,2)*ri2*xy(j,k,3) + q(j,k,3)*ri2
     &              *xy(j,k,4) )
               dvvdq(2) = ri*xy(j,k,3) 
               dvvdq(3) = ri*xy(j,k,4)

               if ( uu(j,k) .ge. 0.0 ) then
                  do n = 1,3
                     db(n,j,k) = db(n,j,k) + wk1(j-1,k)*duudq(n)
                  end do
               else
                  do n = 1,3
                     db(n,j,k) = db(n,j,k) + wk1(j,k)*duudq(n)
                  end do
               end if

               if ( vv(j,k) .ge. 0.0 ) then
                  do n = 1,3
                     db(n,j,k) = db(n,j,k) + wk2(j,k-1)*dvvdq(n)
                  end do
               else
                  do n = 1,3
                     db(n,j,k) = db(n,j,k) + wk2(j,k)*dvvdq(n)
                  end do
               end if

            end do
         end do      
      end if

      return
      end                       !dsa_adv
