C     
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C                      SSSS    OOOO   RRRRR   TTTTTTT                  C
C                     S       O    O  R    R     T                     C
C                      SSSS   O    O  RRRRR      T                     C
C                          S  O    O  R    R     T                     C
C                      SSSS    OOOO   R    R     T                     C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE SORT(NOBFUN,NCHROM,FITSAV,ISAV)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTINE PERFORMS A SORT BASED ON THE FIRST     C
C               FITNESS VALUE ***                                      C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      DIMENSION FITSAV(nobfun,nchrom)
      DIMENSION ISAV(2,nchrom*10)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C
      NCHROMM = NCHROM-1
      DO NCC=1,NCHROM
         DO NC=1,NCHROMM
            IF (FITSAV(1,NC).LT.FITSAV(1,NC+1)) THEN
               DO NO=1,NOBFUN
                  FITDUM          = FITSAV(NO,NC)
                  FITSAV(NO,NC)   = FITSAV(NO,NC+1)
                  FITSAV(NO,NC+1) = FITDUM
               ENDDO
               IDUM1 = ISAV(1,NC)
               ISAV(1,NC) = ISAV(1,NC+1)
               ISAV(1,NC+1) = IDUM1
               IDUM2 = ISAV(2,NC)
               ISAV(2,NC) = ISAV(2,NC+1)
               ISAV(2,NC+1) = IDUM2
            ENDIF
         ENDDO
      ENDDO
C
C          *** SORT COMPLETED ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
