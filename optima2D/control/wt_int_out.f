************************************************************************
      !-- Program name: wt_int_out
      !-- Written by: Howard Buckley
      !-- Date: June 2011
      !-- 
      !-- This subroutine evaluates the integral of D/L over a range of
      !-- operating conditions and writes output to file at each design
      !-- iteration. The integral is approximated using 
      !-- a 2D traezoidal qudarature rule. 
************************************************************************

      subroutine wt_int_out(compObj1, compObj2, ifun, ondpobjs, dvs)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      
      !-- Declare Optima2D variables

      integer 
     &     ifun, i


      double precision 
     &     mrange, wtrange, altrange, compObj1, compObj2,
     &     DLinteg, DLinteg_no_dp, DLavg, LDavg, delm, delwt, delalt,
     &     ondpobjs(mpopt-noffcon), range_int, endur_int, dvs(nc+mpopt)

      
      !-- Calculate operating condition ranges
      mrange = mup - mlow
      wtrange = wtup - wtlow
      altrange = altup - altlow  


      !-- Calculate the interval spacing for the ranges of 
      !-- Mach number, aircraft weight, anb aircraft cruise alt
      if (nm.le.1) then
         delm = 0.0
      else
         delm = (mup-mlow)/(nm-1)
      end if
      
      if (nwt.le.1) then
         delwt = 0.0
      else
         delwt = (wtup-wtlow)/(nwt-1)
      end if

      if (nalt.le.1) then
         delalt = 0.0
      else
         delalt = (altup - altlow)/(nalt-1)
      end if

      !-- Calculate aircraft range and endurance integrals
      if (objfuncs(1).eq.20.or.objfuncs(1).eq.21) then

         range_int = 0.d0
         endur_int = 0.d0
      
         do i=1,nwt
            if (1.lt.i.and.nwt.gt.i) then
               range_int = range_int + 2*ondpobjs(i)**-1
            else
               range_int = range_int + 1*ondpobjs(i)**-1
            end if
         end do

         do i=1,nwt
            if (1.lt.i.and.nwt.gt.i) then
               endur_int = endur_int + 2*ondpobjs(i+nwt)**-1
            else
               endur_int = endur_int + 1*ondpobjs(i+nwt)**-1
            end if
         end do

         range_int = 0.5*delwt*range_int
         endur_int = 0.5*delwt*endur_int

         if (ifun.eq.1) write(wtint_unit,*)
     & 'Design Iter | Range Integral | Range Mach # |
     & Endur Integral | Endur Mach #'
      
         if (ndvmach.eq.0) then
            write(wtint_unit,11) 
     &           ifun, range_int, 0, endur_int, 
     &           0
 11         format(i4,5x,e14.7,5x,f10.6,5x,e14.7,5x,f10.6)
         else if (ndvmach.eq.1) then
            write(wtint_unit,120) 
     &           ifun, range_int, dvs(machdv1_index), endur_int, 
     &           dvs(machdv2_index)
 120        format(i4,5x,e14.7,5x,f10.6,5x,e14.7,5x,f10.6)
         else if (ndvmach.eq.2) then
            write(wtint_unit,130) 
     &           ifun, range_int, dvs(machdv1_index), endur_int, 
     &           dvs(machdv2_index)
 130        format(i4,5x,e14.7,5x,f10.6,5x,e14.7,5x,f10.6)
         end if


      else


         !-- Initialize the D/L integral to the weighted sum of design
         !-- point (Cd/Cl*) values contained in compObj1.
         !-- 
         !-- DLinteg includes designer-priority weighting
         !-- DLinteg_no_dp does not include designer-priority weighting
         DLinteg = compObj1 !-- Sum of weighted objective functions
         DLinteg_no_dp = compObj2
 
         !-- Final operation in trapezoidal integral approximation
         !-- Also calc avg D/L over range of operating conditions
         if (nm.gt.1.and.nwt.gt.1.and.nalt.gt.1) then
            DLinteg = 0.125*delm*delwt*delalt*DLinteg
            DLinteg_no_dp = 0.125*delm*delwt*delalt*DLinteg_no_dp
            DLavg = DLinteg_no_dp/(mrange*wtrange*altrange)
            LDavg = 1.d0/DLavg
         else if (nm.le.1.and.nwt.gt.1.and.nalt.gt.1) then
            DLinteg = 0.25*delwt*delalt*DLinteg
            DLinteg_no_dp = 0.25*delwt*delalt*DLinteg_no_dp
            DLavg = DLinteg_no_dp/(wtrange*altrange)      
            LDavg = 1.d0/DLavg
         else if (nm.gt.1.and.nwt.le.1.and.nalt.gt.1) then
            DLinteg = 0.25*delm*delalt*DLinteg
            DLinteg_no_dp = 0.25*delm*delalt*DLinteg_no_dp
            DLavg = DLinteg_no_dp/(mrange*altrange)
            LDavg = 1.d0/DLavg
         else if (nm.gt.1.and.nwt.gt.1.and.nalt.le.1) then
            DLinteg = 0.25*delm*delwt*DLinteg
            DLinteg_no_dp = 0.25*delm*delwt*DLinteg_no_dp
            DLavg = DLinteg_no_dp/(mrange*wtrange)
            LDavg = 1.d0/DLavg
         else if (nm.gt.1.and.nwt.le.1.and.nalt.le.1) then
            DLinteg = 0.5*delm*DLinteg
            DLinteg_no_dp = 0.5*delm*DLinteg_no_dp
            DLavg = DLinteg_no_dp/(mrange)
            LDavg = 1.d0/DLavg
         else if (nm.le.1.and.nwt.gt.1.and.nalt.le.1) then !
            DLinteg = 0.5*delwt*DLinteg
            DLinteg_no_dp = 0.5*delwt*DLinteg_no_dp
            DLavg = DLinteg_no_dp/(wtrange)
            LDavg = 1.d0/DLavg
         else if (nm.le.1.and.nwt.le.1.and.nalt.gt.1) then
            DLinteg = 0.5*delalt*DLinteg
            DLinteg_no_dp = 0.5*delalt*DLinteg_no_dp
            DLavg = DLinteg_no_dp/(altrange)
            LDavg = 1.d0/DLavg
         else if (nm.le.1.and.nwt.le.1.and.nalt.le.1) then
            DLinteg = 1.0*DLinteg
            DLinteg_no_dp = 1.0*DLinteg_no_dp
            DLavg = DLinteg_no_dp
            LDavg = 1.d0/DLavg
         end if

         if (ifun.eq.1) then
            if (dl_int) then 
               write(wtint_unit,*)
     &        'Design Iteration | DP-weighted integral of D/L | Avg L/D'
            else
               write(wtint_unit,*)
     &        'Design Iter | DP-weighted integral of Cd | 1/(Avg Cd)'
            end if
         end if
      
         write(wtint_unit,12) ifun, DLinteg, LDavg
 12      format(i4,15x,e14.7,15x,f10.4)

      end if
      
      return
      
      end
            


      
      
      
      

      
       

