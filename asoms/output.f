c     ------------------------------------------------------------------
c     -- output grid, control points, and design variables --
c     -- m. nemec --
c     ------------------------------------------------------------------
      subroutine output(x, y)

      implicit none

#include "mxblk.inc"
#include "mxfoil.inc"
#include "mxseg.inc"
#include "mxjk.inc"
#include "bsp.inc"
#include "units.inc"

      integer maxjk
      parameter (maxjk=maxj*maxk)
      double precision x(maxjk), y(maxjk)

      integer nblks, jbmax(maxblk), kbmax(maxblk)
      common/mblock/ nblks, jbmax, kbmax

      integer lgptr(maxblk)
      common/ptrs/ lgptr

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      integer nfoils, nsegments(maxfoil), isegblk(maxsegs,maxfoil)
      integer isegside(maxsegs,maxfoil)
      common/sortedfoils/ nsegments, isegblk, isegside, nfoils

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer ndv, nbdv, ntdv, idvi(ibsnc), idvs(ibsnc), idvf(ibsnc)
      double precision dvs(ibsnc)
      common/dvinfo/ dvs, idvi, idvs, idvf, ndv, nbdv, ntdv

c     -- local variables --
      integer i, ifoil, iseg

      open ( unit=new_grid_unit, file='grid-new.g', status='unknown', 
     &      form='unformatted')
      open (unit=bsp_unit, file='grid-new.bsp', status='unknown')
      open (unit=dvs_unit, file='grid-new.dvs', status='unknown')

c     -- output new grid --
      write(new_grid_unit) nblks
      write(new_grid_unit) (jbmax(i),kbmax(i),i=1,nblks)
      do i=1,nblks
         call writeGRD( new_grid_unit, jbmax(i), kbmax(i), x(lgptr(i)),
     &        y(lgptr(i)))
      end do

c     -- output bspline control file --
      write (bsp_unit,100) iorder
      do ifoil=1,nfoils
         if ( nbseg(ifoil).ne.0) then
            write (bsp_unit,100) ifoil, nbseg(ifoil), ibap(ifoil)
            do iseg = 1,nbseg(ifoil)
c               write (bsp_unit,100) ifoil, ibap(ifoil)
               write (bsp_unit,100) nbcp(ifoil,iseg), isurf(ifoil,iseg)
               write (bsp_unit,100) ibstart(ifoil,iseg),
     &              ibend(ifoil,iseg)
               write (bsp_unit,110) ( bcp(i,1,ifoil,iseg),
     &              bcp(i,2,ifoil,iseg), i=1,nbcp(ifoil,iseg) )

               jbody = abs(ibstart(ifoil,iseg)-ibend(ifoil,iseg))+1
               write (bsp_unit,120) ( bt(i,ifoil,iseg), i=1, jbody )
               write (bsp_unit,120) ( bknot(i,ifoil,iseg), i=1, 
     &              iorder+nbcp(ifoil,iseg))
            end do
         else 
            write (bsp_unit,100) ifoil, nbseg(ifoil), ibap(ifoil)
         end if

      end do

c     -- output design variables --
      write (dvs_unit,100) ndv, nbdv, ntdv
      write (dvs_unit,130) (idvf(i), idvs(i), idvi(i), dvs(i), i=1,ndv)
c
 100  format (3i5)
 110  format (2e24.16)
 120  format (e24.16)
 130  format(2i3,i4,e24.16)

      close (new_grid_unit)
      close (bsp_unit)
      close (dvs_unit)

      return
      end                       ! output
