*********************************************************************
****  subroutine to make the coarse grids from the finest        ****
*********************************************************************
c     Calling subroutine: ioall

      subroutine mkcoarse (cblk,jdimf,kdimf,xf,yf,jdimc,kdimc,    
     >                     xc,yc)

      implicit none
c
#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
c
      integer cblk,jdimf,kdimf,jdimc,kdimc,jc,kc,jf,kf
      double precision xf(jdimf,kdimf),yf(jdimf,kdimf)
      double precision xc(jdimc,kdimc),yc(jdimc,kdimc)
c

      do 20 jc = jminbnd(cblk),jmaxbnd(cblk)
      do 20 kc = kminbnd(cblk),kmaxbnd(cblk)
         jf = (jc-(nhalo+1))*2 + (nhalo+1)
         kf = (kc-(nhalo+1))*2 + (nhalo+1)
c        copy value at node:
         xc(jc,kc) = xf(jf,kf) 
         yc(jc,kc) = yf(jf,kf)
 20   continue
c
      return
      end
