c----------------------------------------------------------------------
c     -- body boundary condition for viscous flow --
c     -- based on a. pueyo's routine impbodyv --
c     -- m. nemec, november 2000 --
c----------------------------------------------------------------------

      subroutine vimpbody (jdim, kdim, ndim, indx, icol, iex, q, xy,
     &      xyj, as, pa, imps, bcf, press)

      use disscon_vars

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer j, k, n1, n2, nn, jdim, kdim, ndim,ii,ij,ip
      integer indx(jdim,kdim), iex(jdim,kdim,ndim), icol(9)

      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision m(4,4), pn(4,4), cc(4,4), c1(4,4)
      double precision press(jdim,kdim), bcf(jdim,kdim,4)
      double precision u,v,den,utilde,vtilde,timetmp

      logical imps

      if (omegaf .eq. 0.d0) then
         omega=omegaa
      else
         if (numiter-istart .le. nk_skip) then
            timetmp=(numiter-istart)*dt2
         else
            timetmp=(numiter-istart-nk_skip)*dt2+nk_skip*dtbig
         end if
         omega=omegaa*dsin(2.d0*pi*omegaf*timetmp)
      endif

      do j = jtail1,jtail2

        do n1 = 1,4
          do n2 = 1,4
            cc(n1,n2) = 0.d0
            c1(n1,n2) = 0.d0
          enddo
        enddo

c     -- matrix for k=1 --
        k = 1                

c       -- ro1 = ro2  or Tw = const. --
        if (dissCon) then
           cc(1,1) = (1.d0 + lamDiss)*xyj(j,k)
        else
           cc(1,1) = xyj(j,k)
        end if

        if (periodic .and. omega.ne.0.d0) then !rotating cylinder BC

c          -- u-utilde = 0    utilde=0.5*omega*ett_y/den--
           cc(2,1) = -q(j,k,2)/q(j,k,1)**2
           cc(2,2) = 1.d0/q(j,k,1)
c          -- v-vtilde = 0   vtilde=-0.5*omega*ett_x/den--
           cc(3,1) = -q(j,k,3)/q(j,k,1)**2
           cc(3,3) = 1.d0/q(j,k,1)

        else

           if (.not.sbbc.or.(sbbc.and.(j.lt.jsls.or.j.gt.jsle)) ) then 
              
              if (dissCon) then
c          -- ro*u = const. --
                 cc(2,2) = (1.d0 + lamDiss)*xyj(j,k)
c          -- ro*v = const. --
                 cc(3,3) = (1.d0 + lamDiss)*xyj(j,k)
              else
c          -- ro*u = const. --
                 cc(2,2) = xyj(j,k)
c          -- ro*v = const. --
                 cc(3,3) = xyj(j,k)
              end if

           else                 !suction/blowing BC

c          -- ro*u = ro*const --
              cc(2,1) = -omega*dcos(sbeta*pi/180.d0-sdelta)*xyj(j,1)
              cc(2,2) = 1.d0*xyj(j,k)
c          -- ro*v = ro*const -- 
              cc(3,1) = -omega*dsin(sbeta*pi/180.d0-sdelta)*xyj(j,1) 
              cc(3,3) = 1.d0*xyj(j,k)

           end if

        endif

c       -- press1 = press2 or press1=const. for inflow suction/blowing BC --
        u = q(j,k,2)/q(j,k,1)
        v = q(j,k,3)/q(j,k,1)

        if (dissCon) then
           cc(4,1) = .5d0*(u**2+v**2)*gami*xyj(j,k)*(1.d0 + lamDiss)
           cc(4,2) = -u*gami*xyj(j,k)*(1.d0 + lamDiss)
           cc(4,3) = -v*gami*xyj(j,k)*(1.d0 + lamDiss)
           cc(4,4) = gami*xyj(j,k)*(1.d0 + lamDiss)
        else
           cc(4,1) = .5d0*(u**2+v**2)*gami*xyj(j,k)
           cc(4,2) = -u*gami*xyj(j,k)
           cc(4,3) = -v*gami*xyj(j,k)
           cc(4,4) = gami*xyj(j,k)
        end if

c     -- matrix for k=2 --
        k = 2
           
c       -- ro1 = ro2  or Tw = const. --
        c1(1,1) = -1.d0*xyj(j,k)
        

c       -- press1 = press2 or press1=const. for inflow suction/blowing BC --

c$$$        if (.not.sbbc.or.
c$$$     &      (sbbc.and.(j.lt.jsls.or.j.gt.jsle).and.omega.lt.0.d0) ) then 
c$$$c       -- not inflow suction/blowing BC --
           u = q(j,k,2)/q(j,k,1)
           v = q(j,k,3)/q(j,k,1)
           c1(4,1) = -.5d0*(u**2+v**2)*gami*xyj(j,k)
           c1(4,2) =  u*gami*xyj(j,k)
           c1(4,3) =  v*gami*xyj(j,k)
           c1(4,4) = -gami*xyj(j,k)           
c$$$        endif

c     -- diagonal scaling --
        do n1 =1,4
          bcf(j,1,n1) = 1.d0
        end do
        
c     -- store in jacobian matrix --
        if (imps) then
          k = 1
          do n1 = 1,4
            ij = ( indx(j,k)-1)*ndim + n1
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            ip = ( ij - 1 )*icol(5)

            do n2 = 1,4
              as(ii+        n2) = cc(n1,n2) 
              as(ii+icol(1)+n2) = c1(n1,n2)

              pa(ip+        n2) = cc(n1,n2) 
              pa(ip+icol(1)+n2) = c1(n1,n2)
            end do
          end do
        end if

      end do

      return
      end                       !vimpbody
