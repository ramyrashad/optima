program soboltest

use Sampling_Mod

   implicit none

   ! declarations
   character(128) :: filename
   integer dims, n, ii
   real(kind=dp), allocatable :: point(:)

   ! begin execution
   dims = 2
   n = 4
   allocate(point(dims))

   filename = 'joe-kuo-6.21201'

   call readDirNums(filename, dims)
   call getSobolPt(n, dims, point) 

   do ii = 1, dims
      write(*,*) point(ii)
   end do 

end program
