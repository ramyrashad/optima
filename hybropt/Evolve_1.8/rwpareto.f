C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C   RRRRR   W     W  PPPPP     AA    RRRRR   EEEEEE  TTTTTTT   OOOO    C
C   R    R  W  W  W  P    P   A  A   R    R  E          T     O    O   C
C   RRRRR   W W W W  PPPPP   AAAAAA  RRRRR   EEEEE      T     O    O   C
C   R    R  WW   WW  P       A    A  R   R   E          T     O    O   C
C   R    R  W     W  P       A    A  R    R  EEEEEE     T      OOOO    C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE RWPARETO(IPOP,ndim,NOBFUN,NACC,I_ACC,FIT_ACC,
     $     ngene,pop_acc,IRW,io_34,ga_file_prefix,
     $     arc2d_io,field_pop,field_fit,field_log,
     $     field_ngene,field_nobfun)
      LOGICAL FILEX,io_34,arc2d_io
      character*80 ga_file_prefix
      integer strlen
      character*80 filena
      character*40 field_pop(ngene),field_fit(nobfun),field_log,
     $     field_ngene,field_nobfun
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** THIS SUBROUTINE READS AND WRITES THE PARETO             C
C              FRONT ACCUMULATION FILE USING THE FILE NAME             C
C              accum.dat. (NOBFUN>1, IAACUM=1)  ***                    C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      dimension fit_acc(nobfun,ndim), i_acc(2,ndim)
      dimension pop_acc(ngene,ndim)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** FIRST HALF OF SUBROUTINE EXECUTES A accum.dat 
C              READ (IRW=1) AND THE SECOND HALF EXECUTES A 
C              WRITE (IRW=2) ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** IRW=1...READ accum.dat FILE ***
C-----------------------------------------------------------------------
C          *** CHECK FOR EXISTENCE OF FILE (accum.dat). IF
C              IT DOES NOT EXIST, WRITE ERROR MESSAGE AND 
C              TERMINATE EXECUTION ***
C
      IF (IRW.EQ.1) THEN
         namelen =  strlen(ga_file_prefix)
         filena = ga_file_prefix
         filena(namelen+1:namelen+7) = '.accum'
c$$$         INQUIRE (FILE='accum.dat',EXIST=FILEX)
         INQUIRE (FILE=filena,EXIST=FILEX)
         IF (.NOT.FILEX) THEN
            INQUIRE (FILE='accum.dat',EXIST=FILEX)
            IF (.NOT.FILEX) THEN
               OPEN (50,FILE='accum.error',STATUS='UNKNOWN',
     $              FORM='FORMATTED')
               WRITE (50,2001)
               close(50)
               WRITE (*,2001)
               if(io_34)WRITE (34,2001)
 2001          FORMAT ('RWPARETO: CANNOT FIND accumulation file',
     1              ', EXECUTION TERMINATED'/)
               STOP
            else
               filena = 'accum.dat'
            endif
         ENDIF
C
C-----------------------------------------------------------------------
C          *** OPEN FILE AND EXECUTE READ ***
C
         OPEN (4,FILE=filena,STATUS='UNKNOWN',FORM='FORMATTED')
         READ (4,*,ERR=1001) IPOPDUM,NACC
         if(arc2d_io)then
            DO NA=1,NACC
               READ (4,*,ERR=1001) I_ACC(1,NA),I_ACC(2,NA)
               READ (4,*,ERR=1001) NGENE
               READ (4,*,ERR=1001)(pop_acc(ng,na),ng=1,ngene)
               READ (4,*,ERR=1001) NOBFUN
               READ (4,*,ERR=1001)(FIT_ACC(NO,NA),NO=1,NOBFUN)
            ENDDO
         else
            DO NA=1,NACC
               READ (4,*,ERR=1001) I_ACC(1,NA),I_ACC(2,NA)
               READ (4,*,ERR=1001) field_ngene,NGENE
               do ng = 1,ngene
                  READ (4,*,ERR=1001)field_pop(ng),pop_acc(ng,na)
               enddo
               READ (4,*,ERR=1001) field_nobfun,NOBFUN
               do nf = 1,nobfun
                  READ (4,*,ERR=1001)field_fit(nf),FIT_ACC(Nf,NA)
               enddo
            ENDDO
         endif
         CLOSE (4)
         RETURN
C
C-----------------------------------------------------------------------
C          *** WRITE ERROR MESSAGE INDICATING ERROR
C              DURING READ AND TERMINATE EXECUTION ***
C
 1001    CONTINUE
         CLOSE (4)
         OPEN (50,FILE='accum.error',STATUS='UNKNOWN',
     $        FORM='FORMATTED')
         WRITE (50,2003)
         close(50)
         WRITE (*,2003)
         if(io_34)WRITE (34,2003)
 2003    FORMAT ('RWPARETO: READING ERROR DETECTED IN accum.dat',
     1           ', EXECUTION TERMINATED'/)
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** IRW=2...WRITE accum.dat FILE ***
C
      IF (IRW.EQ.2) THEN
         namelen =  strlen(ga_file_prefix)
         filena = ga_file_prefix
         filena(namelen+1:namelen+7) = '.accum'
         OPEN (4,FILE=filena,STATUS='UNKNOWN',FORM='FORMATTED')
c$$$         OPEN (4,FILE='accum.dat',STATUS='UNKNOWN',FORM='FORMATTED')
         WRITE (4,'(2(8x,i6))') IPOP,NACC
         if(arc2d_io)then
            DO NA=1,NACC
               WRITE (4,'(2(8x,i6))') I_ACC(1,NA),I_ACC(2,NA)
               WRITE (4,'(8x,i6)') NGENE
               WRITE (4,'(3((2x,e24.16)))')(pop_acc(ng,na),ng=1,ngene)
               WRITE (4,'(8x,i6)') NOBFUN
               WRITE (4,'(3((2x,e24.16)))')(FIT_ACC(NO,NA),NO=1,NOBFUN)
            ENDDO
         else
            DO NA=1,NACC
               WRITE (4,'(2(8x,i6))') I_ACC(1,NA),I_ACC(2,NA)
               Write(4,'(2x,a40,i8)')field_ngene,ngene
               do ng = 1,ngene
                  write(4,'(2x,a40,e16.8)')field_pop(ng),pop_acc(ng,na)
               enddo
               write(4,'(2x,a40,i8)')field_nobfun,nobfun
               do nf = 1,nobfun
                  write(4,'(2x,a40,e16.8)')field_fit(nf),fit_acc(nf,na)
               enddo
            enddo
         endif      
         CLOSE (4)
      ENDIF
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
