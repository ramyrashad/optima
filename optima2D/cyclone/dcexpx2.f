c     CUSP Dissipation
c     -- preconditioned CUSP scheme --
c     Written by: Marian Nemec
c     Date: April, 1998
c
      subroutine dcexpx2 ( jdim,kdim,q,s,xyj,xy,press,x,y,precon,wq,
     $                     enth,d_op,tmp,pgam,astar,cbeta )
c
#include "../include/arcom.inc"
c
      integer i,j,k,n
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)
      dimension press(jdim,kdim),xy(jdim,kdim,4),x(jdim,kdim)
      dimension y(jdim,kdim),precon(jdim,kdim,6),wq(2,4,jdim,kdim)
      dimension enth(jdim,kdim),d_op(jdim,kdim,4),tmp(4),pgam(4,4)
      dimension astar(jdim,kdim),cbeta(jdim,kdim,3)
c     
      double precision Roe,u,v,renth,met1,met2,rss,ruu,rm,cap_ave,jave
      double precision dtd,up1,vp1,sum,epsinv,alp,beta,snd2,rc,eps
c
c     tmp(4),pgam(4,4),d_op(maxj,maxk,4),enth(maxj,maxk)
c
c     Monitor arrays:
c      double precision rma(maxj,maxk),alphc(maxj,maxk),
c      double precision betac(maxj,maxk),ad(maxj,maxk,4)
c
c     Monitor output files:
c      open (unit=95,file='adx1.out')
c      open (unit=98,file='adx2.out')
c
c     Calculate enthalpy (H=E+p/rho)
      do k=kbegin,kend
         do j=jbegin,jend
            enth(j,k) = q(j,k,4)/q(j,k,1) + press(j,k)/q(j,k,1)
         end do
      end do
c     
      do k=klow,kup
         do j=jbegin,jup
c     Calculate sound speed and mach number at state j+1/2 using 
c     Roe average.
            Roe = dsqrt((q(j+1,k,1)*xyj(j+1,k))/(q(j,k,1)*xyj(j,k)))
            u = ( Roe*q(j+1,k,2)/q(j+1,k,1) + q(j,k,2)/q(j,k,1) )
     $           /(Roe + 1.d0)
            v = ( Roe*q(j+1,k,3)/q(j+1,k,1) + q(j,k,3)/q(j,k,1) )
     $           /(Roe + 1.d0)
            renth = ( Roe*enth(j+1,k) + enth(j,k) )/(Roe + 1.d0)
c
c     Use contravariant velocity, taken from subroutine 'eigval.f'.
c     Metric transformations for j+1/2 state. 
            met1 = ( xy(j+1,k,1) + xy(j,k,1) )/2.d0
            met2 = ( xy(j+1,k,2) + xy(j,k,2) )/2.d0
            eps = ( precon(j+1,k,1) + precon(j,k,1) )/2.d0 
c
c     Do Not include the metric terms in sound speed for calculating
c     Gamma matrix or preconditioned eigenvalue. 
            rss = dsqrt( gami*( renth - ( u**2+v**2 )/2.d0 ) )
            ruu = ( u*met1 + v*met2 )
            rm = ruu/(rss*dsqrt( met1**2 + met2**2 ))
c
c     Calculate the preconditioner Gamma = M Gamma^ M^(-1)
            snd2 = 1.d0/rss**2
            epsinv = 1.d0/eps
            alp = epsinv - 1.d0
            beta = alp*gami*( u**2 + v**2 )/2.d0*snd2
c     
            pgam(1,1) = beta + 1.d0
            pgam(2,1) = beta*u
            pgam(3,1) = beta*v
            pgam(4,1) = (u**2+v**2)/2.d0*(beta+alp)
c     
            rc = gami*u*snd2
            pgam(1,2) = -rc*alp
            pgam(2,2) = -rc*alp*u + 1.d0
            pgam(3,2) = -rc*v*alp
            pgam(4,2) = -u*(beta + alp)
c     
            rc = gami*v*snd2
            pgam(1,3) = -rc*alp
            pgam(2,3) = -rc*u*alp
            pgam(3,3) = -rc*v*alp + 1.d0
            pgam(4,3) = -v*(beta + alp)
c     
            rc = gami*snd2
            pgam(1,4) = rc*alp
            pgam(2,4) = rc*alp*u
            pgam(3,4) = rc*alp*v
            pgam(4,4) = beta + epsinv
c     
c     Calculate average contravariant velocity using arithmetic mean.
            u = q(j,k,2)/q(j,k,1)
            v = q(j,k,3)/q(j,k,1)
            up1 = q(j+1,k,2)/q(j+1,k,1)
            vp1 = q(j+1,k,3)/q(j+1,k,1)
            cap_ave = met1*0.5d0*( u + up1 ) + met2*0.5d0*( v + vp1 )
c
c     Determine constant alpha* for curvilinear coordinates.
c     Mean value of Jacobian:
            jave = ( xyj(j+1,k) + xyj(j,k) )/2.d0
            astar(j,k) = dabs(cap_ave) /jave
c
c     Monitor arrays:
c           rma(j,k)=rm
c           alphc(j,k)=ruu
c           betac(j,k)=rss
c
            do n = 1,4
               tmp(n) = wq(1,n,j,k) - wq(2,n,j,k)
            end do
c     
            do n = 1,4
               sum=0.d0
               do i = 1,4
                  sum = sum + pgam(n,i)*tmp(i)
               end do
               d_op(j,k,n) = sum
            end do     
         end do
      end do
c     
c-------------------Dissipation for R.H.S.------------------------------
c
      dtd = dt / (1. + phidt)  
c     Add dissipation to R.H.S.
      do n = 1,4
         do k = klow,kup
            do j = jlow,jup
               s(j,k,n) = s(j,k,n) + 0.5d0*dtd*(
     $              astar(j,k)*d_op(j,k,n)
     $              - astar(j-1,k)*d_op(j-1,k,n) )
c               ad(j,k,n) = 0.5d0*dtd*(
c     $              alphc(j,k)*d_op(j,k,n)
c     $              - alphc(j-1,k)*d_op(j-1,k,n)
c     Flux budget array:
               if (flbud.and.n.eq.2) then
                  budget(j,k,2) = 0.5d0*dtd*(
     $                 astar(j,k)*d_op(j,k,n)
     $                 - astar(j-1,k)*d_op(j-1,k,n) )
               end if
            end do
         end do
      end do
c
c     For LHS dissipation -> beta * eigenvalues
      do n = 1,3
         do k=klow,kup
            do j=jbegin,jup
               cbeta(j,k,n) = astar(j,k)
            end do
         end do
      end do
c
c     Tecplot output:
c      if (numiter.eq.iend) then
c         write (95,*) 'variables="x","y","sc1"'
c         write (95,*) '"sc3","pc3","sc4","pc4"'
c         write (95,*) 'zone f=point,i=',jmax-2,', j=',kmax-2
c         do k=2,kmax-1
c            do j=2,jmax-1
c               write(95,12) x(j,k),y(j,k),precon(j,k,1)
c            end do
c         end do
c      end if
c
 12   format(10(e16.8,1x))
 25   format(4e15.5)
      return
      end
