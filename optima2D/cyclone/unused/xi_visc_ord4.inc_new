c        ***********************
c        E_xi_xi Viscous Terms
c        ***********************
c        
         scal=1.0d0
         ww=1.d0/3.d0
         xx=1.d0/8.d0
         yy=1.d0/6.d0
         zz=1.d0/16.d0
         do k=klow,kup
            do j=jlow+2,jup-2
               jm3=j-3
               jm2=j-2
               jm1=j-1
               jp1=j+1
               jp2=j+2
               jp3=j+3
c
               xy1m2=xy(jm2,k,1)
               xy1m1=xy(jm1,k,1)
               xy1  =xy(j,k,1)
               xy1p1=xy(jp1,k,1)
               xy1p2=xy(jp2,k,1)
c
               xy2m2=xy(jm2,k,2)
               xy2m1=xy(jm1,k,2)
               xy2  =xy(j,k,2)
               xy2p1=xy(jp1,k,2)
               xy2p2=xy(jp2,k,2)
c
c              -h stands for half
c              -interpolate metrics to +1/2 (ph) and -1/2 (mh) nodes
               xy1ph     = zz*(-xy1m1+9.d0*(xy1+xy1p1)-xy1p2)
               xy2ph     = zz*(-xy2m1+9.d0*(xy2+xy2p1)-xy2p2)
               xy1mh     = zz*(-xy1m2+9.d0*(xy1m1+xy1)-xy1p1)
               xy2mh     = zz*(-xy2m2+9.d0*(xy2m1+xy2)-xy2p1)

c              -compute (xi_x(j)*xi_x(j+1/2) + xi_y(j)*xi_y(j+1/2))
c                       (xi_x(j)*xi_x(j-1/2) + xi_y(j)*xi_y(j-1/2))
               ttp1      =  (xy1*xy1p1 + xy2*xy2p1)
               ttph      =  (xy1*xy1ph + xy2*xy2ph)
               ttmh      =  (xy1*xy1mh + xy2*xy2mh)
               ttm1      =  (xy1*xy1m1 + xy2*xy2m1)
               
c              note: work(j,k) = fnu(j,k) + turre(j,k)
c              - interpolate work to +1/2 (ph) and -1/2 (mh) nodes
               workmh =zz*(-work(jm2,k)+9.d0*(work(jm1,k)+work(j,k))-
     &                    work(jp1,k))
               workph =zz*(-work(jm1,k)+9.d0*(work(j,k)+work(jp1,k))-
     &                    work(jp2,k))
c               
c              -interpolate turre to +3/2, +1/2, -1/2, -3/2 points
               turm3h =zz*(-turre(jm3,k)+9.d0*(turre(jm2,k)+
     &                    turre(jm1,k))-turre(j,k))
               turmh  =zz*(-turre(jm2,k)+9.d0*(turre(jm1,k)+turre(j,k))-
     &                    turre(jp1,k))
               turph  =zz*(-turre(jm1,k)+9.d0*(turre(j,k)+turre(jp1,k))-
     &                    turre(jp2,k))
               turp3h =zz*(-turre(j,k)+9.d0*(turre(jp1,k)+turre(jp2,k))-
     &                    turre(jp3,k))

c              -here we're computing contribution from 
c               [(1+cb2)/sigma] grad dot [(fnu+turre)*grad(turre)

               ztmp=(1.0+cb2)*resiginv

               tm1  =  ttm1*ztmp*work(jm1,k)
               tmh  =  ttmh*ztmp*workmh
               tph  =  ttph*ztmp*workph
               tp1  =  ttp1*ztmp*work(jp1,k)
               
c        
c              -differentiate turre w.r.t xi using half-nodes
               dvp1=(-turre(jp2,k)+8.d0*(turp3h-turph)+turre(j,k))*yy
               dvph=(-turp3h+8.d0*(turre(jp1,k)-turre(j,k))+turmh)*yy
               dvmh=(-turph+8.d0*(turre(j,k)-turre(jm1,k))+turm3h)*yy
               dvm1=(-turre(j,k)+8.d0*(turmh-turm3h)+turre(jm2,k))*yy

               bx(k,j)   = -tmh
               cx(k,j)   = tph+tmh
               dx(k,j)   = -tph

               fy(j,k)  = fy(j,k) + yy*(
     &              -tp1*dvp1 + 8.d0*(tph*dvph-tmh*dvmh) + tm1*dvm1)
               
c              -here we're computing contribution from 
c               cb2/sigma * (fnu+turre) * grad **2 turre
c        
               cnud=cb2*resiginv*work(j,k)
               
               tm1  =  ttm1*cnud
               tmh  =  ttmh*cnud
               tph  =  ttph*cnud
               tp1  =  ttp1*cnud

               bx(k,j)   = bx(k,j) + tmh
               cx(k,j)   = cx(k,j) - (tph+tmh)*scal
               dx(k,j)   = dx(k,j) + tph

               fy(j,k)  = fy(j,k) - yy*(
     &              -tp1*dvp1 + 8.d0*(tph*dvph-tmh*dvmh) + tm1*dvm1)
            enddo

            do j=jlow+1,jup-1,jup-jlow-2
               jm2=j-2
               jm1=j-1
               jp1=j+1
               jp2=j+2
c
               xy1m2=xy(jm2,k,1)
               xy1m1=xy(jm1,k,1)
               xy1  =xy(j,k,1)
               xy1p1=xy(jp1,k,1)
               xy1p2=xy(jp2,k,1)
c
               xy2m2=xy(jm2,k,2)
               xy2m1=xy(jm1,k,2)
               xy2  =xy(j,k,2)
               xy2p1=xy(jp1,k,2)
               xy2p2=xy(jp2,k,2)
c
               xy1ph     = zz*(-xy1m1+9.d0*(xy1+xy1p1)-xy1p2)
               xy2ph     = zz*(-xy2m1+9.d0*(xy2+xy2p1)-xy2p2)
               xy1mh     = zz*(-xy1m2+9.d0*(xy1m1+xy1)-xy1p1)
               xy2mh     = zz*(-xy2m2+9.d0*(xy2m1+xy2)-xy2p1)

c              -compute (xi_x(j)*xi_x(j+1/2) + xi_y(j)*xi_y(j+1/2))
c                       (xi_x(j)*xi_x(j-1/2) + xi_y(j)*xi_y(j-1/2))
               ttp1      =  (xy1*xy1p1+xy2*xy2p1)
               ttph      =  (xy1*xy1ph+xy2*xy2ph)
               ttmh      =  (xy1*xy1mh+xy2*xy2mh)
               ttm1      =  (xy1*xy1m1+xy2*xy2m1)
               
c              note: work(j,k) = fnu(j,k) + turre(j,k)
c              - interpolate work to +1/2 (ph) and -1/2 (mh) nodes
               workmh =zz*(-work(jm2,k)+9.d0*(work(jm1,k)+work(j,k))-
     &                    work(jp1,k))
               workph =zz*(-work(jm1,k)+9.d0*(work(j,k)+work(jp1,k))-
     &                    work(jp2,k))
c               
c              -interpolate turre to +3/2, +1/2, -1/2, -3/2 points
c
c              -second-order interpolation for next two
c               turm3h=.5d0*(turre(jm1,k)+turre(jm2,k))
c               turp3h=.5d0*(turre(jp1,k)+turre(jp2,k))
c
c              -third-order interpolation for next two
               turm3h=xx*
     &                 (3.d0*turre(jm2,k)+6.d0*turre(jm1,k)-turre(j,k))
               turp3h=xx*
     &                 (3.d0*turre(jp2,k)+6.d0*turre(jp1,k)-turre(j,k))
c
c              -fourth-order interpolation
               turmh  =zz*(-turre(jm2,k)+9.d0*(turre(jm1,k)+turre(j,k))-
     &                    turre(jp1,k))
               turph  =zz*(-turre(jm1,k)+9.d0*(turre(j,k)+turre(jp1,k))-
     &                    turre(jp2,k))

c              -here we're computing contribution from 
c               [(1+cb2)/sigma] grad dot [(fnu+turre)*grad(turre)

               ztmp=(1.0+cb2)*resiginv

               tm1  =  ttm1*ztmp*work(jm1,k)
               tmh  =  ttmh*ztmp*workmh
               tph  =  ttph*ztmp*workph
               tp1  =  ttp1*ztmp*work(jp1,k)
               
c        
c              -differentiate turre w.r.t xi using half-nodes
               dvp1=(-turre(jp2,k)+8.d0*(turp3h-turph)+turre(j,k))*yy
               dvph=(-turp3h+8.d0*(turre(jp1,k)-turre(j,k))+turmh)*yy
               dvmh=(-turph+8.d0*(turre(j,k)-turre(jm1,k))+turm3h)*yy
               dvm1=(-turre(j,k)+8.d0*(turmh-turm3h)+turre(jm2,k))*yy

               bx(k,j)   = -tmh
               cx(k,j)   = tph+tmh
               dx(k,j)   = -tph

               fy(j,k)  = fy(j,k) + yy*(
     &              -tp1*dvp1 + 8.d0*(tph*dvph-tmh*dvmh) + tm1*dvm1)
               
c              -here we're computing contribution from 
c               cb2/sigma * (fnu+turre) * grad **2 turre
c        
               cnud=cb2*resiginv*work(j,k)
               
               tm1  =  ttm1*cnud
               tmh  =  ttmh*cnud
               tph  =  ttph*cnud
               tp1  =  ttp1*cnud

               bx(k,j)   = bx(k,j) + tmh
               cx(k,j)   = cx(k,j) - (tph+tmh)*scal
               dx(k,j)   = dx(k,j) + tph

               fy(j,k)  = fy(j,k) - yy*(
     &              -tp1*dvp1 + 8.d0*(tph*dvph-tmh*dvmh) + tm1*dvm1)
            enddo
         enddo


         do k=klow,kup
           j=jlow
           jp2 = j+2
           jp1 = j+1
           jm1 = j-1
c
           xy1m1=xy(jm1,k,1)
           xy1  =xy(j,k,1)
           xy1p1=xy(jp1,k,1)
c     
           xy2m1=xy(jm1,k,2)
           xy2  =xy(j,k,2)
           xy2p1=xy(jp1,k,2)
c     
           xy1ph     = xx*(-xy1m1+6.d0*xy1+3.d0*xy1p1)
           xy2ph     = xx*(-xy2m1+6.d0*xy2+3.d0*xy2p1)
           xy1mh     = xx*(3.d0*xy1m1+6.d0*xy1-xy1p1)
           xy2mh     = xx*(3.d0*xy2m1+6.d0*xy2-xy2p1)

           ttp1      =  (xy1*xy1p1+xy2*xy2p1)
           ttph      =  (xy1*xy1ph+xy2*xy2ph)
           ttmh      =  (xy1*xy1mh+xy2*xy2mh)
           tt        =  (xy1*xy1  +xy2*xy2  )
               
           workmh =xx*(3.d0*work(jm1,k)+6.d0*work(j,k)-work(jp1,k))
           workph =xx*(-work(jm1,k)+6.d0*work(j,k)+3.d0*work(jp1,k))
           
           turmh  =xx*(3.d0*turre(jm1,k)+6.d0*turre(j,k)-
     &                 turre(jp1,k))
           turph  =xx*(-turre(jm1,k)+6.d0*turre(j,k)+
     &                 3.d0*turre(jp1,k))
           turp3h =xx*(-turre(j,k)+6.d0*turre(jp1,k)+
     &                 3.d0*turre(jp2,k))
c
           ztmp=(1.0+cb2)*resiginv

           t   =  tt*ztmp*work(j,k)
           tmh =ttmh*ztmp*workmh
           tph =ttph*ztmp*workph
           tp1 =ttp1*ztmp*work(j,kp1)
c
           dvp1=(-turre(jp2,k)+6.d0*turp3h-3.d0*turre(jp1,k)
     &            - 2.d0*turph)*ww
           dvph=(-turp3h+6.d0*turre(jp1,k)-3.d0*turph
     &            - 2.d0*turre(j,k))*ww
           dv  =(-turre(jp1,k)+6.d0*turph-3.d0*turre(j,k)
     &            - 2.d0*turmh)*ww
           dvmh=(-turph+6.d0*turre(j,k)-3.d0*turmh
     &            - 2.d0*turre(jm1,k))*ww

           bx(k,j)   = -tmh
           cx(k,j)   = tph+tmh
           dx(k,j)   = -tph
           
           fy(j,k)   = fy(j,k) + ww*(
     &            -tp1*dvp1 +6.d0*tph*dvph -3.d0*t*dv -2.d0*tmh*dvmh)
c
           cnud=cb2*resiginv*work(j,k)
           
           t   =  tt*cnud
           tmh =ttmh*cnud
           tph =ttph*cnud
           tp1 =ttp1*cnud
               
           bx(k,j)   = bx(k,j) + tmh
           cx(k,j)   = cx(k,j) - (tph+tmh)*scal
           dx(k,j)   = dx(k,j) + tph
               
           fy(j,k)   = fy(j,k) - ww*(
     &            -tp1*dvp1 +6.d0*tph*dvph -3.d0*t*dv -2.d0*tmh*dvmh)
c     
c          ******************
c          ******************
           
           j=jup
           jp1 = j+1
           jm1 = j-1
           jm2 = j-2
c
           xy1m1=xy(jm1,k,1)
           xy1  =xy(j,k,1)
           xy1p1=xy(jp1,k,1)
c     
           xy2m1=xy(jm1,k,2)
           xy2  =xy(j,k,2)
           xy2p1=xy(jp1,k,2)
c
           xy1ph     = xx*(-xy1m1+6.d0*xy1+3.d0*xy1p1)
           xy2ph     = xx*(-xy2m1+6.d0*xy2+3.d0*xy2p1)
           xy1mh     = xx*(3.d0*xy1m1+6.d0*xy1-xy1p1)
           xy2mh     = xx*(3.d0*xy2m1+6.d0*xy2-xy2p1)

           ttph      =  (xy1*xy1ph+xy2*xy2ph)
           tt        =  (xy1*xy1  +xy2*xy2  )
           ttmh      =  (xy1*xy1mh+xy2*xy2mh)
           ttm1      =  (xy1*xy1m1+xy2*xy2m1)
c               
           workmh =xx*(3.d0*work(jm1,k)+6.d0*work(j,k)-work(jp1,k))
           workph =xx*(-work(jm1,k)+6.d0*work(j,k)+3.d0*work(jp1,k))
c           
           turm3h=xx*(3.d0*turre(jm2,k)+6.d0*turre(jm1,k)
     &             -turre(j,k))
           turmh =xx*(3.d0*turre(jm1,k)+6.d0*turre(j,k)
     &             -turre(jp1,k))
           turph =xx*(-turre(jm1,k)+6.d0*turre(j,k)
     &             +3.d0*turre(jp1,k))
c
           ztmp=(1.0+cb2)*resiginv
c
           tm1 =ttm1*ztmp*work(j,km1)
           tmh =ttmh*ztmp*workmh
           t   =  tt*ztmp*work(j,k)
           tph =ttph*ztmp*workph
c
           dvph=(turmh-6.d0*turre(j,k)+3.d0*turph
     &             + 2.d0*turre(jp1,k))*ww
           dv  =(turre(jm1,k)-6.d0*turmh+3.d0*turre(j,k)
     &             + 2.d0*turph)*ww
           dvmh=(turm3h-6.d0*turre(jm1,k)+3.d0*turmh
     &             + 2.d0*turre(j,k))*ww
           dvm1=(turre(jm2,k)-6.d0*turm3h+3.d0*turre(jm1,k)
     &             + 2.d0*turmh)*ww
c
           bx(k,j)   = -tmh
           cx(k,j)   = tph+tmh
           dx(k,j)   = -tph
c     
           fy(j,k)   = fy(j,k) + ww*(
     &             2.d0*tph*dvph+3.d0*t*dv-6.d0*tmh*dvmh+tm1*dvm1)
c
c
           cnud=cb2*resiginv*work(j,k)
c           
           tm1 =ttm1*cnud
           tmh =ttmh*cnud
           t   =  tt*cnud
           tph =ttph*cnud
c
           bx(k,j)   = bx(k,j) + tmh
           cx(k,j)   = cx(k,j) - (tph+tmh)*scal
           dx(k,j)   = dx(k,j) + tph
c               
           fy(j,k)   = fy(j,k) - ww*(
     &             2.d0*tph*dvph+3.d0*t*dv-6.d0*tmh*dvmh+tm1*dvm1)
         enddo
         
c         do j=jlow,jup
c         do k=klow,kup
c           bx(k,j) = bx(k,j)*1.5d0
c           cx(k,j) = cx(k,j)*1.5d0
c           dx(k,j) = dx(k,j)*1.5d0
c         enddo
c         enddo
         
c         do k=klow,kup,kup-klow
c            do j=jlow,jup
c               jp1 = j+1
c               jm1 = j-1
c               xy1p     = .5*(xy(j,k,1)+xy(jp1,k,1))
c               xy2p     = .5*(xy(j,k,2)+xy(jp1,k,2))
c               ttp      =  (xy1p*xy(j,k,1)+xy2p*xy(j,k,2))
c               
c               xy1m     = .5*(xy(j,k,1)+xy(jm1,k,1))
c               xy2m     = .5*(xy(j,k,2)+xy(jm1,k,2))
c               ttm      =  (xy1m*xy(j,k,1)+xy2m*xy(j,k,2))
c               
c               cnud=cb2*resiginv*work(j,k)
c               
c               cdp       =    ttp*cnud
c               cdm       =    ttm*cnud               
cc        
c               trem =.5*(work(jm1,k)+work(j,k))
c               trep =.5*(work(j,k)+work(jp1,k))
cc               
c               cap  =  ttp*trep*(1.0+cb2)*resiginv
c               cam  =  ttm*trem*(1.0+cb2)*resiginv
c               
cc        
c               bx(k,j)   = cdm-cam
c               cx(k,j)   = -cdp+cap-cdm+cam
c               dx(k,j)   = cdp-cap
c               
c               fy(j,k)   =  fy(j,k) - bx(k,j)*turre(jm1,k)
c     &              - cx(k,j)*turre(j,k  )
c     &              - dx(k,j)*turre(jp1,k) 
c               
c            enddo
c         enddo
