      subroutine smoothy(jdim,kdim,q,s,xyj,spect,dd)                    
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension spect(jdim,kdim),dd(jdim,kdim)                          
c                                                                       
c     fourth order smoothing, added explicitly to rhs                     
c     third order at fringes                                              
c                                                                       
      smudt = smu*dt /(1. + phidt)                                      
c                                                                       
c     first unscale jacobian from q                                        
c                                                                       
      do 2 n = 1,4                                                      
      do 2 k = kbegin,kend                                              
      do 2 j = jbegin,jend                                              
         q(j,k,n) = q(j,k,n)*xyj(j,k)                                      
2     continue                                                          
c                                                                       
c     eta direction smoothing                                             
c                                                                       
      do 600 n=1,4                                                      
c                                                                       
c        second order difference                                              
c                                                                       
         do 400 k = klow,kup                                              
         do 400 j = jlow,jup                                              
            dd(j,k) = q(j,k+1,n) - 2.*q(j,k,n) + q(j,k-1,n)                  
 400     continue

c                                                                       
c        third order at boundaries                                            
c                                                                       
         do 401 j = jlow,jup                                           
            dd(j,kbegin) = dd(j,klow)                                     
            dd(j,kend) = dd(j,kup)                                        
 401     continue                                                      
c                                                                       
c        fourth order                                                         
c                                                                       
         do 402 k = klow,kup                                              
         do 402 j = jlow,jup                                              
            s(j,k,n) = s(j,k,n)                                              
     *        - smudt * (dd(j,k+1) - 2.*dd(j,k) + dd(j,k-1))*spect(j,k)       
 402     continue
c                                                                       
 600  continue
c                                                                       
c                                                                       
c     put back in jacobian scaling on q                                    
c                                                                       
      do 701 n = 1,4                                                    
      do 701 k = kbegin,kend                                            
      do 701 j = jbegin,jend                                            
         q(j,k,n) = q(j,k,n)/xyj(j,k)                                      
701   continue                                                          
c                                                                       
      return                                                            
      end                                                               
