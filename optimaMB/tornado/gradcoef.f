c     ****************************************************
c     ** gradient coefficent for artificial dissipation **
c     ****************************************************
c     ixy = 1  for xi  direction
c     ixy = 2  for eta direction
c
      subroutine gradcoef(ixy,jdim,kdim,press,xyj,coef,prss,
     &          jbegin,jend,kbegin,kend,
     &          xi1mesh,xi2mesh,eta1mesh,eta2mesh)

      implicit none

#include "../include/common.inc"

      integer ixy,jdim,kdim,jbegin,jend,kbegin,kend,j,k
      integer jlow,jup,klow,kup,jpl,jmi,kpl,kmi
      double precision press(jdim,kdim),xyj(jdim,kdim)                         
      double precision coef(jdim,kdim),prss(jdim,kdim)
      double precision r1,r2
                         
      logical xi1mesh,xi2mesh,eta1mesh,eta2mesh

      jlow=jbegin+1
      jup=jend-1
      klow=kbegin+1
      kup=kend-1

      if(ixy.eq.1)then                                                 

c       ******************************************
c          gradient coefficient in xi direction
c       ******************************************

        do 110 k = kbegin,kend
        do 110 j = jbegin,jend
          coef(j,k) = 0.
          prss(j,k) = press(j,k)*xyj(j,k)
  110   continue

        do 120 k = klow,kup
        do 120 j = jlow,jup
          jpl=j+1
          jmi=j-1
          r1 = prss(jpl,k) -2.* prss(j,k) + prss(jmi,k)
          r2 = prss(jpl,k) +2.* prss(j,k) + prss(jmi,k)
          coef(j,k) = abs( r1/r2 )
 120    continue

        if(xi1mesh)then
          do 130 k = klow,kup
            coef(jbegin,k) = coef(jlow,k)
  130     continue
        endif
        if(xi2mesh) then
          do 135 k = klow,kup 
            coef(jend,k) = coef(jup,k)
  135     continue
        endif  

  
      elseif(ixy.eq.2)then                                           

c       ******************************************
c          gradiant coefficent in eta direction   
c       ******************************************

        do 210 k = kbegin,kend
        do 210 j = jbegin,jend
          coef(j,k) = 0.
          prss(j,k) = press(j,k)*xyj(j,k)
  210   continue

        do 220 k = klow,kup
          kpl = k+1
          kmi = k-1
          do 220 j = jlow,jup
            r1 = prss(j,kpl) -2.* prss(j,k) + prss(j,kmi)
            r2 = prss(j,kpl) +2.* prss(j,k) + prss(j,kmi)
            coef(j,k) = abs( r1/r2 )
  220   continue
        if(eta1mesh) then
          do 230 j=jlow,jup
            coef(j,kbegin) = coef(j,klow)
 230      continue
        endif
        if(eta2mesh) then
          do 240 j=jlow,jup
            coef(j,kend) = coef(j,kup)
 240      continue
        endif
      endif                                                            

      if(ixy.eq.1)then

c ********************************************************************
c  xi direction -- take the maximum of neighboring points
c ********************************************************************

        do 140 k = klow,kup
        do 140 j = jlow,jup
          jpl=j+1
          jmi=j-1
          prss(j,k) = max(coef(jmi,k),coef(j,k),coef(jpl,k))
 140    continue

c ********************************************************************
c  mesh b.c. in xi direction
c ********************************************************************

        if(xi1mesh)then
          do 142 k = klow,kup
            prss(jbegin,k) = coef(jlow,k)
  142     continue
        endif
        if(xi2mesh) then
          do 144 k = klow,kup
            prss(jend,k) = coef(jup,k)
  144     continue
        endif

      elseif(ixy .eq. 2)then

c ********************************************************************
c  gradiant coefficent in eta direction
c ********************************************************************

c  for eta direction, just copy the coefficient
c  to match arc2d --- include max for multiblock)

        if (matcharc2d) then
          do 260 k = klow,kup
          do 260 j = jlow,jup
            prss(j,k) = coef(j,k)
  260     continue
        else
          do 270 k = klow,kup
          kmi = k - 1
          kpl = k + 1
          do 270 j = jlow,jup
            prss(j,k) = max(coef(j,kmi),coef(j,k),coef(j,kpl))
  270     continue
        endif

c ********************************************************************
c  mesh b.c. in eta direction
c ********************************************************************

        if(eta1mesh) then
          do 262 j = jlow,jup
            prss(j,kbegin) = coef(j,klow)
  262     continue
        endif
        if(eta2mesh) then
          do 264 j = jlow,jup
            prss(j,kend) = coef(j,kup)
  264     continue
        endif
      endif

      if(ixy.eq.1)then                                                 

c ********************************************************************
c **    smooth out the pressure gradient coefficient                **
c ********************************************************************

        do 15 k = klow,kup
        do 15 j = jlow,jup
          jpl=j+1
          jmi=j-1
          coef(j,k) = 0.5*prss(j,k) + 0.25*(prss(jpl,k)+prss(jmi,k))
 15     continue

        if(xi1mesh)then                                             
          do 32 k = klow,kup                   
            coef(jbegin,k) = coef(jlow,k)       
 32       continue                          
        endif                                                          

        if(xi2mesh)then                                             
          do 34 k = klow,kup                   
            coef(jend,k) = coef(jup,k)         
 34       continue                          
        endif                                                          

      elseif(ixy .eq. 2)then                                           

c ********************************************************************
c  smooth pressure gradient coefficient in eta direction
c ********************************************************************

        do 28 k = klow,kup
          kpl = k+1
          kmi = k-1
          do 28 j = jlow,jup
            coef(j,k) = 0.5*prss(j,k) + 0.25*(prss(j,kpl)+prss(j,kmi))
   28   continue

c note:  arc2d does not apply the mesh b.c. in the eta direction
c        don't apply it for multi-block in either direction

      endif                                                            
      return                                                         
      end                                                            
