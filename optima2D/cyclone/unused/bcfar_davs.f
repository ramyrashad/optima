      subroutine bcfar(jdim,kdim,q,kbc,xy,xit,ett,xyj,x,y)              
c     parameter (maxj=497)                                              
      include '../include/arcom.inc'
c                                                                       
      dimension duqinf(4),duqext(4),duqbar(4),duqdif(4)
      dimension q(jdim,kbc,4)                                           
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c                                                                       
c ====================================================================
c ---         Boundary conditions for airfoil topology             ---  
c ---                  c  or  o mesh type                          ---  
c ====================================================================
c                                                                       
c  -far field bc for o mesh outer boundary and c mesh k = kmax          
c  -far field characteristic like bc                          
c                                                                       
c  -set check for supersonic flow                        
      if(fsmach.gt.1.0) go to 65                                   
c                                                                       
c.....................................................................  
c  Far field circulation based on potential vortex added to reduce      
c  the dependency on outer boundary location.  works well.  for         
c  instance, a naca0012 at m=0.63 a=2. shows no dependency on ob from   
c  4 - 96 chords.                                                       
c.....................................................................  
c                                                                       
      gi = 1./gamma                                                 
      gm1i = 1./gami                                                
c                                                                    
      alphar = alpha*pi/180.                                         
      cosang = cos( alphar )                                         
      sinang = sin( alphar )                                         
      ainf = sqrt(gamma*pinf/rhoinf)                                 
      hstfs = 1./gami + 0.5*fsmach**2                                
c                                                                       
c.....................................................................  
c.....................................................................  
c                                                                       
      k = kmax                                                          
      kq = kbc                                                          
      if (prec.eq.0) then
         do 60 j = jlow,jup                                                
c                                                                          
c           -reset free stream values with circulation correction                
            xa = x(j,k) - chord/4.                                         
            ya = y(j,k)                                                    
            radius = sqrt(xa**2+ya**2)                                     
            angl = atan2(ya,xa)                                            
            cjam = cos(angl)                                               
            sjam = sin(angl)                                               
            qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))    
            uf = uinf + qcirc*sjam                                         
            vf = vinf - qcirc*cjam                                         
            af2 = gami*(hstfs - 0.5*(uf**2+vf**2))                         
            af = sqrt(af2)                                                 
c                                                                          
c.....   ..........................................................  
c           choose a reference frame in terms of normal and          
c           tangential components                                    
c.....   ..........................................................  
c                                                                          
c                                                                          
c           -metric terms                                        
            snorm = 1./sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                        
            xy3h = xy(j,k,3)*snorm                                            
            xy4h = xy(j,k,4)*snorm                                            
c                                                                          
c.....   .................................................................  
c           -check for inflow or outflow                          
c                for inflow : three variables are specified  r1 and        
c                        qt  ( q_tangential)  and   s~ r**gamma/p  (~entrop
c                        with one var. computed.  r2                       
c                for outflow : one variable is fixed r1  with three compute
c                        r2, qt and s                                      
c                                                                          
c                compute riemann invariants                                
c                fix          r1 = qn - 2.*a/(gamma-1)  at free stream     
c                compute      r2 = qn + 2.*a/(gamma-1)  from interior      
c                             extrapolation of flow variables              
c                                                                          
c.....   ................................................................  
c           -get extrapolated variables                                
c.....   ................................................................  
c                                                                          
             rhoext = q(j,kq-1,1)*xyj(j,k-1)                                   
             rjm1   = 1./q(j,kq-1,1)                                           
             uext   = q(j,kq-1,2)*rjm1                                         
             vext   = q(j,kq-1,3)*rjm1                                         
             eext   = q(j,kq-1,4)*xyj(j,k-1)                                   
             pext   = gami*(eext - 0.5*rhoext*(uext**2+vext**2))               
c                                                                          
c.....   ................................................................  
c           -set riemann invariants                                  
c.....   ................................................................  
c                                                                          
             r1 = xy3h*uf + xy4h*vf - 2.*af*gm1i                               
             r2 = xy3h*uext + xy4h*vext +2.*sqrt(gamma*pext/rhoext)*gm1i      
c                                                                              
             qn = (r1 + r2)*0.5                                                
             cspe = (r2 - r1)*gami*0.25                                        
             c2 = cspe**2                                                      
c                                                                          
c.....   ................................................................  
c           -set other fixed or extrapolated variables               
c.....   ................................................................  
c                                                                          
             if(qn .le. 0.0)then                                          
                qt = xy4h*uf - xy3h*vf                                       
                entro = gamma                                                
             else                                                         
                qt = xy4h*uext - xy3h*vext                                   
                entro = rhoext**gamma/pext                                   
             endif                                                        
c                                                                          
c.....   ................................................................  
c           -compute flow variables                                   
c.....   ................................................................  
c                                                                          
             u = xy3h*qn + xy4h*qt                                             
             v = xy4h*qn - xy3h*qt                                             
c                                                                              
             q(j,kq,1) = (c2*entro*gi)**gm1i                                   
             pres = c2*q(j,kq,1)*gi                                            
c                                                                          
c           -add jacobian                                             
             rjj = 1./xyj(j,k)                                             
             q(j,kq,1) = q(j,kq,1)*rjj                                     
             q(j,kq,2) = q(j,kq,1)*u                                       
             q(j,kq,3) = q(j,kq,1)*v                                       
             q(j,kq,4) = pres*gm1i*rjj + 0.5*q(j,kq,1)*(u**2+v**2)         
60       continue                                                          
      else
c
         if (prec.ne.3) then
            print *,'BCFAR only written for prec=3'
            stop
         endif
c
c
cdu      -use the preconditioned characteristics to solve the bc's
c
         dulimit=prphi*fsmach**2
c
         do 1000 j=jlow,jup
c     
c          -do circulation correction
            xa = x(j,k) - chord/4.                                         
            ya = y(j,k)                                                    
            radius = sqrt(xa**2+ya**2)                                     
            angl = atan2(ya,xa)                                            
            cjam = cos(angl)                                               
            sjam = sin(angl)                                               
            qcirc = circb/( radius* (1.- (fsmach*SIN(ANGL-ALPHAR))**2))    
            uf = uinf + qcirc*sjam                                         
            vf = vinf - qcirc*cjam                                         
            af2 = gami*(hstfs - 0.5*(uf**2+vf**2))                         
            af = sqrt(af2)                                                 
c
c          -calculate values for duqinf
            duqinf(1)=af2**gm1i
            duqinf(2)=duqinf(1)*uf
            duqinf(3)=duqinf(1)*vf
            dupinf=duqinf(1)*af2*gi
            duqinf(4)=dupinf*gm1i+0.5*(uf**2+vf**2)*duqinf(1)
c
c       -calculate values for duqext (no Jacobian in these)
         if ((j.lt.(jup-3)).and.(j.gt.(jlow+3))) then
            duqext(1)=2.0*q(j,kq-1,1)*xyj(j,k-1)-q(j,kq-2,1)*xyj(j,k-2)
            duqext(2)=2.0*q(j,kq-1,2)*xyj(j,k-1)-q(j,kq-2,2)*xyj(j,k-2)
            duqext(3)=2.0*q(j,kq-1,3)*xyj(j,k-1)-q(j,kq-2,3)*xyj(j,k-2)
            duqext(4)=2.0*q(j,kq-1,4)*xyj(j,k-1)-q(j,kq-2,4)*xyj(j,k-2)
         else
            duqext(1)=q(j,kq-1,1)*xyj(j,k-1)
            duqext(2)=q(j,kq-1,2)*xyj(j,k-1)
            duqext(3)=q(j,kq-1,3)*xyj(j,k-1)
            duqext(4)=q(j,kq-1,4)*xyj(j,k-1)
         endif
c
c          -form simple average
            duqbar(1)=0.5*(duqinf(1)+duqext(1))
            duqbar(2)=0.5*(duqinf(2)+duqext(2))
            duqbar(3)=0.5*(duqinf(3)+duqext(3))
            duqbar(4)=0.5*(duqinf(4)+duqext(4))
c
c          -form difference
            duqdif(1)=0.5*(duqinf(1)-duqext(1))
            duqdif(2)=0.5*(duqinf(2)-duqext(2))
            duqdif(3)=0.5*(duqinf(3)-duqext(3))
            duqdif(4)=0.5*(duqinf(4)-duqext(4))
c
c          -calculate properties at mean state
            durho=duqbar(1)
            duu=duqbar(2)/durho
            duv=duqbar(3)/durho
            duuv2=duu**2+duv**2
            dup=gami*(duqbar(4)-0.5*(duuv2)*durho)
            duc2=gamma*dup/durho
            duc=sqrt(duc2)
            duma2=duuv2/duc2
c
            duw=xy(j,k,1)**2+xy(j,k,2)**2
            dul=xy(j,k,3)**2+xy(j,k,4)**2
c
            dulim=max(dulimit,prxi*rhoinf*sqrt(max(duw,dul))
     +           /re/durho/fsmach)
            due=min(1.0,max(duma2,dulim))
c            due=1.0
c
            dukx=-xy(j,k,3)
            duky=-xy(j,k,4)
            duuk=dukx*duu+duky*duv
c
            dua=(1.0+due)/2.0*duuk
            dub=sqrt(((1.0-due)*duuk/2.0)**2+due*dul*duc2)
c
            dul=sqrt(dul)
            dukx=dukx/dul
            duky=duky/dul
c
c          -multiply by Minv
            du4=gami*(duuv2/2.0*duqdif(1)-duu*duqdif(2)-
     +           duv*duqdif(3)+duqdif(4))
            du1=du4/durho/duc
            du4=du4-duc2*duqdif(1)
            du2=(duqdif(2)-duu*duqdif(1))/durho
            du3=(duqdif(3)-duv*duqdif(1))/durho
c
c          -multiply by Tkinv
            dut1=du4
            dut2=duky*du2-dukx*du3
            dut3=(dul*duc*du1+(duuk-dua+dub)*
     +           (dukx*du2+duky*du3))/dub/2.0
            dut4=(dul*duc*du1+(duuk-dua-dub)*
     +           (dukx*du2+duky*du3))/dub/2.0
c
c          -multiply by sign(Lambda)
            du1=sign(1.0,duuk)*dut1
            du2=sign(1.0,duuk)*dut2
            du3=sign(1.0,dua+dub)*dut3
            du4=sign(1.0,dua-dub)*dut4
c
c          -multiply by Tk
            dut1=((dua+dub-duuk)*du3+(duuk-dua+dub)*du4)/dul/duc
            dut2=duky*du2+dukx*(du3-du4)
            dut3=-dukx*du2+duky*(du3-du4)
            dut4=du1
c
c          -multiply by M
            du1=(durho*dut1-dut4/duc)/duc
            du2=duu*du1-durho*dut2
            du3=duv*du1-durho*dut3
            du4=durho*((duuv2/2.0/duc+duc/gami)*dut1+
     +           duu*dut2+duv*dut3)-duuv2/2.0/duc2*dut4
c
c          -calculate the boundry Q
            dut1=duqbar(1)-du1
            dut2=duqbar(2)-du2
            dut3=duqbar(3)-du3
            dut4=duqbar(4)-du4
c
c          -apply
            q(j,kq,1)=dut1/xyj(j,k)
            q(j,kq,2)=dut2/xyj(j,k)
            q(j,kq,3)=dut3/xyj(j,k)
            q(j,kq,4)=dut4/xyj(j,k)
 1000    continue
      endif
c
      go to 67                                                          
65    continue                                                          
c                                                                       
c=====================================================================  
c                      Supersonic flow option                                   
c=====================================================================  
c                                                                       
      k = kmax                                                          
      kq = kbc                                                          
      do 66 j = jlow,jup                                                
c
c       -Choose a reference frame in terms of normal and         
c        tangential components                                   
c                                                                       
         rhoinv = 1./q(j,kq,1)                                             
         u = q(j,kq,2)*rhoinv                                              
         v = q(j,kq,3)*rhoinv                                              
c                                                                          
c       -metric terms                                            
         snorm = 1./sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                        
         xy3h = xy(j,k,3)*snorm                                            
         xy4h = xy(j,k,4)*snorm                                            
         qn = xy3h*u + xy4h*v                                              
c                                                                       
         if(qn.ge.0.)then                                             
            rmet = xyj(j,k-1)/xyj(j,k)                                   
            do 69 n = 1,4                                                
               q(j,kq,n) = q(j,kq-1,n)*rmet
 69         continue
         endif                                                        
66    continue                                                          
c                                                                       
67    continue                                                          
      return                                                            
      end                                                               
