      subroutine xymets(JDIM,KDIM,X,Y,XY,XIT,ETT,XYJ,WORK)              
c                                                                       
      INCLUDE '../include/arcom.inc'
c                                                                       
      DIMENSION XY(JDIM*KDIM*4),XYJ(JDIM*KDIM)                          
      DIMENSION XIT(JDIM*KDIM),ETT(JDIM*KDIM)                           
      DIMENSION X(JDIM*KDIM),Y(JDIM*KDIM)                               
      DIMENSION WORK(JDIM*KDIM*4)                                       
C                                                                       
C                                                                       
C    COMPUTE XI DERIVATIVES OF X, Y                                     
C                                                                       
       CALL XIDIF(JDIM,KDIM,X,Y,XY)                                     
C                                                                       
C   COMPUTE  ETA DERIVATIVES OF X, Y                                    
C                                                                       
       CALL ETADIF(JDIM,KDIM,X,Y,XY)                                    
C                                                                       
C   FORM METRICS AND JACOBIAN                                           
C                                                                       
       CALL CALCMET(JDIM,KDIM,XY,XIT,ETT,XYJ)                           
C                                                                       
      RETURN                                                            
      END                                                               
