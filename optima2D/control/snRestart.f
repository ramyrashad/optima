c-----------------------------------------------------------------------
c     Program name: snRestart
c     Written By: Howard Buckley, April 2009
c     
c     This subroutine reads and writes values of F and G to file for 
c     use in the event of a restart
c
c-----------------------------------------------------------------------
      	
      subroutine snRestart(rStat,F,G,EOF,nF,lenG,ifun,Status)

#ifdef _MPI_VERSION
      use mpi
#endif


      !-- Declare variables

      implicit none
								
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
		
      integer 	
     &     EOF, rStat, nF, lenG, ifun, i, Status

      double precision 
     &     F(nf), G(lenG)
c-----------------------------------------------------------------------

      if (rStat.eq.0) then
         
         !-- Read function, constraint, and 'Status' values from file

         read(snfg_unit,IOSTAT = EOF)
     &     (F(i),i=1,nf),
     &     (G(i),i=1,lenG),
     &     Status    
 !11      format(e50.40)

         !-- If there are function and constraint values on file, then 
         !-- skip function and constraint evaluations in usrfun

         if (EOF==0) then

            write(scr_unit,12)
 12         format
     &(/3x,'Reading function, constraint, and Status values from file
     &...skipping usrfun',/3x,'evaluations')
                                     
         else

            write(scr_unit,13)
 13         format
     &(/3x,'No stored function and gradient values on file for this
     & iteration...',/3x,'proceed with usrfun evaluations')    
 
         end if
            
      else if (rStat.eq.1) then

         !-- Write function and constraint values to file

         write(snfg_unit)
     &     (F(i),i=1,nf),
     &     (G(i),i=1,lenG),
     &     Status    

         write(scr_unit,14) ifun
 14      format
     &   (/3x,'Writting function, constraint, and Status values to file 
     & at iteration [',i3,' ]')

      end if

      return

      end
