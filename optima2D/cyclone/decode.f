        SUBROUTINE DECODE(JDIM,KDIM,LDIM,NDIM,Q,JMAX,KMAX,LMAX,CONSER)  
C                                                                       
C===============================================================        
        DIMENSION Q(JDIM,KDIM,LDIM,NDIM)                                
        LOGICAL CONSER                                                  
                                                                        
        IF(CONSER)RETURN                                                
        G = 1.4                                                         
        GB= G-1.                                                        
                                                                        
        IF(LMAX.NE.1 .AND. LDIM.NE.1)THEN                               
C                                                                       
C  THREE-D                                                              
C                                                                       
        DO 10 L=1,LMAX                                                  
        DO 10 K=1,KMAX                                                  
        DO 10 J=1,JMAX                                                  
                                                                        
        RHO = Q(J,K,L,1)                                                
        U = Q(J,K,L,2)/RHO                                              
        V = Q(J,K,L,3)/RHO                                              
        W = Q(J,K,L,4)/RHO                                              
        EN= Q(J,K,L,5)                                                  
                                                                        
        VEL2 = U**2 + V**2 + W**2                                       
        PRESS = GB*(EN - .5*RHO*VEL2 )                                  
        Q(J,K,L,2) = U                                                  
        Q(J,K,L,3) = V                                                  
        Q(J,K,L,4) = W                                                  
        Q(J,K,L,5) = PRESS                                              
 10     CONTINUE                                                        
                                                                        
      ELSE                                                              
C                                                                       
C  TWO-D                                                                
C                                                                       
        L = 1                                                           
        DO 20 K=1,KMAX                                                  
        DO 20 J=1,JMAX                                                  
                                                                        
        RHO = Q(J,K,L,1)                                                
        U = Q(J,K,L,2)/RHO                                              
        V = Q(J,K,L,3)/RHO                                              
        EN= Q(J,K,L,4)                                                  
                                                                        
        VEL2 = U**2 + V**2                                              
        PRESS = GB*(EN - .5*RHO*VEL2 )                                  
        Q(J,K,L,2) = U                                                  
        Q(J,K,L,3) = V                                                  
        Q(J,K,L,4) = PRESS                                              
 20     CONTINUE                                                        
                                                                        
                                                                        
        ENDIF                                                           
                                                                        
        RETURN                                                          
        END                                                             
