      subroutine calcfnu(iblk,jmax,kmax,jbegin,jend,kbegin,kend,
     &                   q,fmu,fnu,xyj)
c
c     calling subroutine : etaexpl
c
c     -This routine computes dynamic? (laminar) viscosity 
c
#include "../include/common.inc"
c
      dimension q(jmax,kmax,4)
      dimension fnu(jmax,kmax),fmu(jmax,kmax),xyj(jmax,kmax)
c
c     -For Spalart-Allmaras
      do 2 k=kbegin,kend
      do 2 j=jbegin,jend
        rho = q(j,k,1)*xyj(j,k)
c       -form inverse of nu from mu (avoids makeing 
c        multiple divisions later)
        fnu(j,k) = rho/fmu(j,k)
 2    continue
c
      return
      end

