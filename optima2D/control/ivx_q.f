c----------------------------------------------------------------------
c     -- implicit  viscous terms in x-direction --
c     -- differentiation of the viscous flux vector w.r.t. Q^ --
c     -- written by:  markus rumpfkeil  --
c     -- date: jan. 2006
c     Note: we need -Re^(-1)*d_xi Ehat
c----------------------------------------------------------------------

      subroutine ivx_q (jdim,kdim,ndim,xy,xyj,q,fmu,turmu,press,dmul,  
     &     dmut,dj,dpj)

      implicit none

#include "../include/arcom.inc"
#include "../include/visc.inc"

      integer jdim, kdim, ndim,j,k,n,jp

      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision fmu(jdim,kdim), turmu(jdim,kdim)
      double precision dj(ndim,3,jdim,kdim), dpj(ndim,3,jdim,kdim)
      double precision q2(jdim,kdim,4)
      double precision press(jdim,kdim)

      double precision dmul(ndim,jdim,kdim),dmut(5,jdim,kdim)

      double precision a1(jdim,kdim), a2(jdim,kdim), a3(jdim,kdim)
      double precision a4(jdim,kdim)

      double precision hre,hfmu,t1,t2,t3,r1,ah1,ah2,ah3,ah4
      double precision rp1,u1,v1,up1,vp1,uxi,vxi,c2xi,uavg,vavg
      double precision vflux1,vflux2,vflux3,S1,S2

c     prlam   =  laminar prandtl number  = .72                      
c     prturb  =  turbulent prandtl number = .90                     
c     prlinv  =  1./(laminar prandtl number)                        
c     prtinv  =  1./(turbulent prandtl number)                      
c     f13     =  1/3                                                
c     f43     =  4/3                                                
c     hre     =  1/2 * reynolds number                           
c     fmu     =  laminar viscosity
c     turmu   =  turbulent viscosity      

      hre = 1.d0/(2.d0*re)
 
c     -- calculate alfas and non-conserv. variables --
      do k = klow,kend                                              
        do j = jbegin,jup 
c     t1 = xi_x **2
c     t2 = xi_x*xi_y
c     t3 = xi_y **2          
          t1      = xy(j,k,1)*xy(j,k,1)                               
          t2      = xy(j,k,1)*xy(j,k,2)                               
          t3      = xy(j,k,2)*xy(j,k,2)                               
          r1      = hre/xyj(j,k)
          
          a1(j,k) = r1*( f43*t1 + t3    ) 
          a2(j,k) = r1*(     t2 * f13   ) 
          a3(j,k) = r1*( t1     + f43*t3)                        
          a4(j,k) = r1*( t1     + t3    )

          r1   = 1.d0/q(j,k,1)                                        
          q2(j,k,1) = r1                                           
          q2(j,k,2) = r1*q(j,k,2)                                  
          q2(j,k,3) = r1*q(j,k,3)                                  
          q2(j,k,4) = r1*q(j,k,4) - (q2(j,k,2)**2 +q2(j,k,3)**2)
        end do
      end do

c     -- entries for viscous Jacobian, treating mu as constant -- 
      do j = jbegin,jup
        jp = jplus(j)
        do k = klow,kup

c     -- extrapolate fmu to get it at 1/2 nodes --
          hfmu = 0.5d0*(fmu(j,k)+fmu(jp,k))
c     -- laminar + turbulent viscosity at 1/2 nodes --
          t1 = hfmu + turmu(j,k)
c     -- mu_l/Pr_l + mu_t/Pr_t --
          t2 = hfmu*prlinv + turmu(j,k)*prtinv

c     -- evaluate alfas at j+1/2 factor 1/2 already in hre --
          ah1 = ( a1(j,k) + a1(jp,k) )*t1
          ah2 = ( a2(j,k) + a2(jp,k) )*t1
          ah3 = ( a3(j,k) + a3(jp,k) )*t1
          ah4 = ( a4(j,k) + a4(jp,k) )*t2*gamma

          uxi  = q2(jp,k,2)-q2(j,k,2)  
          vxi  = q2(jp,k,3)-q2(j,k,3)   
          uavg  = 0.5d0*(q2(jp,k,2)+q2(j,k,2))
          vavg  = 0.5d0*(q2(jp,k,3)+q2(j,k,3))

          dj  (1,1,j,k) = -( ah1*q2(j,k,2)  + ah2*q2(j,k,3)) *q2(j,k,1)
          dpj (1,1,j,k) =  ( ah1*q2(jp,k,2) + ah2*q2(jp,k,3))*q2(jp,k,1)
          dj  (2,1,j,k) =  ah1*q2(j,k,1)                              
          dpj (2,1,j,k) = -ah1*q2(jp,k,1)
          dj  (3,1,j,k) =  ah2*q2(j,k,1)
          dpj (3,1,j,k) = -ah2*q2(jp,k,1)
          dj  (4,1,j,k) = 0.d0
          dpj (4,1,j,k) = 0.d0
          dj  (1,2,j,k) = -( ah2*q2(j,k,2) + ah3*q2(j,k,3))  *q2(j,k,1)
          dpj (1,2,j,k) =  ( ah2*q2(jp,k,2)+ ah3*q2(jp,k,3))*q2(jp,k,1)
          dj  (2,2,j,k) =  ah2*q2( j, k,1)                              
          dpj (2,2,j,k) = -ah2*q2( jp, k,1)                            
          dj  (3,2,j,k) =  ah3*q2( j, k,1)                              
          dpj (3,2,j,k) = -ah3*q2( jp, k,1)                            
          dj  (4,2,j,k) = 0.d0                                          
          dpj (4,2,j,k) = 0.d0

          S1=ah1*uxi+ah2*vxi
          S2=ah2*uxi+ah3*vxi
                                         
          dj  (1,3,j,k) = (-ah4*q2(j,k,4)+0.5d0*S1*q2(j,k,2)  
     &         +0.5d0*S2*q2(j,k,3) )*q2(j,k,1)
     &         + uavg*dj(1,1,j,k) + vavg*dj(1,2,j,k)
          dpj (1,3,j,k) = ( ah4*q2(jp,k,4)+0.5d0*S1*q2(jp,k,2)  
     &         +0.5d0*S2*q2(jp,k,3) )*q2(jp,k,1) 
     &         + uavg*dpj(1,1,j,k) + vavg*dpj(1,2,j,k) 

          dj  (2,3,j,k) =-ah4*q2(j,k,2)*q2(j,k,1) +uavg*dj(2,1,j,k)+
     &         vavg*dj(2,2,j,k)  - 0.5d0*S1 * q2(j,k,1)        
          dpj (2,3,j,k) = ah4*q2(jp,k,2)*q2(jp,k,1) +uavg*dpj(2,1,j,k)+
     &         vavg*dpj(2,2,j,k) - 0.5d0*S1 * q2(jp,k,1) 
    
          dj  (3,3,j,k) =-ah4*q2(j,k,3)*q2(j,k,1) +uavg*dj(3,1,j,k)+
     &         vavg*dj(3,2,j,k) - 0.5d0*S2 * q2(j,k,1)        
          dpj (3,3,j,k) = ah4*q2(jp,k,3)*q2(jp,k,1) +uavg*dpj(3,1,j,k)+
     &         vavg*dpj(3,2,j,k)- 0.5d0*S2 * q2(jp,k,1)  
   
          dj  (4,3,j,k) =  ah4*q2(j,k,1)                              
          dpj (4,3,j,k) = -ah4*q2(jp,k,1)                            
        end do
      end do

      if (turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kup
          do j = jlow,jup
            do n = 1,3              
              dj(5,n,j,k) = 0.d0
              dpj(5,n,j,k) = 0.d0
            end do
          end do
        end do
      end if

c     -- compute derivative terms due to laminar and turbulent (sa)
c     viscosity: dmu is negative to be consistent with inviscid terms --
      do j = jbegin,jup
        jp = jplus(j)
        do k = klow,kup

c     -- calculate viscous flux vector without viscosity terms --
          r1    = 1.d0/q(j,k,1)                                         
          rp1   = 1.d0/q(jp,k,1)                                       
          up1   = q(jp,k,2)*rp1                                     
          u1    = q(j,k,2)*r1                                      
          vp1   = q(jp,k,3)*rp1                                     
          v1    = q(j,k,3)*r1                                      
          uxi  = up1 - u1                                            
          vxi  = vp1 - v1                                            
          c2xi = gamma*(press(jp,k)*rp1 - press(j,k)*r1)/gami

          ah1 = a1(jp,k) + a1(j,k)
          ah2 = a2(jp,k) + a2(j,k)
          ah3 = a3(jp,k) + a3(j,k)
          ah4 = a4(jp,k) + a4(j,k)

          vflux1 = ah1*uxi + ah2*vxi
          vflux2 = ah2*uxi + ah3*vxi
          vflux3 = 0.5d0*(up1 + u1)*vflux1 + 0.5d0*(vp1 +v1)*vflux2 

c     -- dmu/dQ terms --
          do n = 1,ndim
            dj(n,1,j,k) = dj(n,1,j,k) - 0.5d0*vflux1*(dmul(n,j,k) +
     &            dmut(n,j,k))  
            dj(n,2,j,k) = dj(n,2,j,k) - 0.5d0*vflux2*(dmul(n,j,k) +
     &            dmut(n,j,k)) 
            dj(n,3,j,k) = dj(n,3,j,k) - 0.5d0*( vflux3*(dmul(n,j,k) +
     &            dmut(n,j,k)) +ah4*c2xi*(dmul(n,j,k)*prlinv +
     &            dmut(n,j,k)*prtinv) )  

            dpj(n,1,j,k) = dpj(n,1,j,k) - 0.5d0*vflux1*(dmul(n,jp,k) +
     &            dmut(n,jp,k)) 
            dpj(n,2,j,k) = dpj(n,2,j,k) - 0.5d0*vflux2*(dmul(n,jp,k) +
     &            dmut(n,jp,k)) 
            dpj(n,3,j,k) = dpj(n,3,j,k) - 0.5d0*( vflux3*(dmul(n,jp,k)
     &            + dmut(n,jp,k)) + ah4*c2xi*(dmul(n,jp,k)*prlinv + 
     &            dmut(n,jp,k)*prtinv))

            if (n.eq.5) then
              if (abs(dj(5,3,j,k)) .lt. 5.d-16) then
                dj(5,1,j,k) = 0.d0
                dj(5,2,j,k) = 0.d0
                dj(5,3,j,k) = 0.d0
              end if
              if (abs(dpj(5,3,j,k)) .lt. 5.d-16) then
                dpj(5,1,j,k) = 0.d0
                dpj(5,2,j,k) = 0.d0
                dpj(5,3,j,k) = 0.d0
              end if
            end if

          end do
        end do
      end do

      return                                                            
      end                       !ivx_q
