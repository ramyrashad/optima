C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C               SSSSS   TTTTTTT   AAAA    TTTTTTT   SSSSS              C
C              S           T     A    A      T     S                   C
C               SSSSS      T     AAAAAA      T      SSSSS              C
C                    S     T     A    A      T           S             C
C               SSSSS      T     A    A      T      SSSSS              C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE STATS(IMOVE,nchrom,ngene,nobfun,pop,fit,fit_init,i_mnx,
     $     fitmin,fitmax,fitavg,ncfitmin,ncfitmax,fitrmsmin,fitrmsmax)
      SAVE
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      dimension pop(ngene,nchrom),fit(nobfun,nchrom)
      dimension fitmin(nobfun),fitmax(nobfun),fitavg(nobfun)
      dimension ncfitmin(nobfun),ncfitmax(nobfun)
C
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** IF ANY FITNESS VALUES ARE ZERO, SET ALL
C              VALUES TO fit_init FOR THAT CHOMOSOME ***
C
c$$$      fit_init = 0.0
c$$$      if(i_mnx.eq.0)fit_init = 1.0e30
      DO NC=1,NCHROM
         DO NOO=1,NOBFUN
            IF (FIT(NOO,NC).EQ.fit_init) THEN
               DO NO=1,NOBFUN
                  FIT(NO,NC) = fit_init
               ENDDO
               GO TO 1001
            ENDIF
         ENDDO
 1001    CONTINUE
      ENDDO
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** COMPUTE AVERAGE FITNESS ***
C
      DO NO=1,NOBFUN
         SUM = 0.0
         DO NC=1,NCHROM
            SUM = SUM+FIT(NO,NC)
         ENDDO
         FITAVG(NO) = SUM/FLOAT(NCHROM)
      ENDDO
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** COMPUTE MINIMUM AND MAXIMUN FITNESS VALUES. THE
C              IMOVE PARAMETER IS SET EQUAL TO ONE IF THE
C              NO=1 MAXIMUM FITNESS WAS FOUND TO BE IN A
C              LOCATION AWAY FROM NC=1 ***
C
      IMOVE = 0
      DO NO=1,NOBFUN
         FITMAX(NO) = -999999999.0
         FITMIN(NO) =  999999999.0
         DO NC=1,NCHROM
            IF (FITMAX(NO).LT.FIT(NO,NC)) THEN
               FITMAX(NO)   = FIT(NO,NC)
               NCFITMAX(NO) = NC
               IF (NC.GT.1.AND.NO.EQ.1.and. i_mnx.eq.1) THEN
                  IMOVE = 1
               ENDIF
            ENDIF
            IF (FITMIN(NO).GT.FIT(NO,NC)) THEN
               FITMIN(NO) = FIT(NO,NC)
               NCFITMIN(NO) = NC
               IF (NC.GT.1.AND.NO.EQ.1 .and. i_mnx.eq.0) THEN
                  IMOVE = 1
               ENDIF
            ENDIF
         ENDDO
      ENDDO
C
C          *** FITNESS STATISTICS ESTABLISHED ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
