      subroutine testmet(jdim,kdim,xy,xyj)
c
#include "../include/arcom.inc"
c
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xix_xi(maxj,maxk),xiy_xi(maxj,maxk)
      dimension etax_eta(maxj,maxk),etay_eta(maxj,maxk)
c
      if (iord.eq.4) then
         tmp=1.d0/12.d0
         do 10 k=kbegin,kend
         do 10 j=jbegin+2,jend-2
            tp2=xy(j+2,k,1)/xyj(j+2,k)
            tp1=xy(j+1,k,1)/xyj(j+1,k)
            t=xy(j,k,1)/xyj(j,k)
            tm1=xy(j-1,k,1)/xyj(j-1,k)
            tm2=xy(j-2,k,1)/xyj(j-2,k)
            xix_xi(j,k)=tmp*(-tp2 + 8.d0*(tp1 - tm1) + tm2)
c        
            tp2=xy(j+2,k,2)/xyj(j+2,k)
            tp1=xy(j+1,k,2)/xyj(j+1,k)
            t=xy(j,k,2)/xyj(j,k)
            tm1=xy(j-1,k,2)/xyj(j-1,k)
            tm2=xy(j-2,k,2)/xyj(j-2,k)
            xiy_xi(j,k)=tmp*(-tp2 + 8.d0*(tp1 - tm1) + tm2)
 10      continue
c        
         do 15 k=kbegin+2,kend-2
         do 15 j=jbegin,jend
            tp2=xy(j,k+2,3)/xyj(j,k+2)
            tp1=xy(j,k+1,3)/xyj(j,k+1)
            t=xy(j,k,3)/xyj(j,k)
            tm1=xy(j,k-1,3)/xyj(j,k-1)
            tm2=xy(j,k-2,3)/xyj(j,k-2)
            etax_eta(j,k)=tmp*(-tp2 + 8.d0*(tp1 - tm1) + tm2)
c        
            tp2=xy(j,k+2,4)/xyj(j,k+2)
            tp1=xy(j,k+1,4)/xyj(j,k+1)
            t=xy(j,k,4)/xyj(j,k)
            tm1=xy(j,k-1,4)/xyj(j,k-1)
            tm2=xy(j,k-2,4)/xyj(j,k-2)
            etay_eta(j,k)=tmp*(-tp2 + 8.d0*(tp1 - tm1) + tm2)
 15      continue
c        
         tmp=1.d0/6.d0
         do 20 j=jbegin,jend
            k=kbegin+1
            tp2=xy(j,k+2,3)/xyj(j,k+2)
            tp1=xy(j,k+1,3)/xyj(j,k+1)
            t=xy(j,k,3)/xyj(j,k)
            tm1=xy(j,k-1,3)/xyj(j,k-1)
            etax_eta(j,k)=tmp*(-2.d0*tm1 - 3.d0*t +6.d0*tp1 - tp2)
c        
            tp2=xy(j,k+2,4)/xyj(j,k+2)
            tp1=xy(j,k+1,4)/xyj(j,k+1)
            t=xy(j,k,4)/xyj(j,k)
            tm1=xy(j,k-1,4)/xyj(j,k-1)
            etay_eta(j,k)=tmp*(-2.d0*tm1 - 3.d0*t +6.d0*tp1 - tp2)
c        
            k=kend-1
            tp1=xy(j,k+1,3)/xyj(j,k+1)
            t=xy(j,k,3)/xyj(j,k)
            tm1=xy(j,k-1,3)/xyj(j,k-1)
            tm2=xy(j,k-2,3)/xyj(j,k-2)
            etax_eta(j,k)=tmp*(2.d0*tp1+ 3.d0*t -6.d0*tm1 + tm2)
c        
            tp1=xy(j,k+1,4)/xyj(j,k+1)
            t=xy(j,k,4)/xyj(j,k)
            tm1=xy(j,k-1,4)/xyj(j,k-1)
            tm2=xy(j,k-2,4)/xyj(j,k-2)
            etay_eta(j,k)=tmp*(2.d0*tp1+ 3.d0*t -6.d0*tm1 + tm2)
 20      continue
         
         do 25 k=kbegin,kend
            j=jbegin+1
            tp2=xy(j+2,k,1)/xyj(j+2,k)
            tp1=xy(j+1,k,1)/xyj(j+1,k)
            t=xy(j,k,1)/xyj(j,k)
            tm1=xy(j-1,k,1)/xyj(j-1,k)
            tm2=xy(j-2,k,1)/xyj(j-2,k)
            xix_xi(j,k)=tmp*(-2.d0*tm1 - 3.d0*t +6.d0*tp1 - tp2)
c        
            tp2=xy(j+2,k,2)/xyj(j+2,k)
            tp1=xy(j+1,k,2)/xyj(j+1,k)
            t=xy(j,k,2)/xyj(j,k)
            tm1=xy(j-1,k,2)/xyj(j-1,k)
            tm2=xy(j-2,k,2)/xyj(j-2,k)
            xiy_xi(j,k)=tmp*(-2.d0*tm1 - 3.d0*t +6.d0*tp1 - tp2)
c        
            j=jend-1
            tp2=xy(j+2,k,1)/xyj(j+2,k)
            tp1=xy(j+1,k,1)/xyj(j+1,k)
            t=xy(j,k,1)/xyj(j,k)
            tm1=xy(j-1,k,1)/xyj(j-1,k)
            tm2=xy(j-2,k,1)/xyj(j-2,k)
            xix_xi(j,k)=tmp*(2.d0*tp1+ 3.d0*t -6.d0*tm1 + tm2)
c        
            tp2=xy(j+2,k,2)/xyj(j+2,k)
            tp1=xy(j+1,k,2)/xyj(j+1,k)
            t=xy(j,k,2)/xyj(j,k)
            tm1=xy(j-1,k,2)/xyj(j-1,k)
            tm2=xy(j-2,k,2)/xyj(j-2,k)
            xiy_xi(j,k)=tmp*(2.d0*tp1+ 3.d0*t -6.d0*tm1 + tm2)
 25      continue
c
         do 45 k=kbegin,kend
            j=jbegin
            tp3=xy(j+3,k,1)/xyj(j+3,k)
            tp2=xy(j+2,k,1)/xyj(j+2,k)
            tp1=xy(j+1,k,1)/xyj(j+1,k)
            t=xy(j,k,1)/xyj(j,k)
            xix_xi(j,k)=tmp*(-1.1d1*t + 1.8d1*tp1 -9.d0*tp2 + 2.d0*tp3)
c        
            tp3=xy(j+3,k,2)/xyj(j+3,k)
            tp2=xy(j+2,k,2)/xyj(j+2,k)
            tp1=xy(j+1,k,2)/xyj(j+1,k)
            t=xy(j,k,2)/xyj(j,k)
            xiy_xi(j,k)=tmp*(-1.1d1*t + 1.8d1*tp1 -9.d0*tp2 + 2.d0*tp3)
c        
            j=jend
            tm3=xy(j-3,k,1)/xyj(j-3,k)
            tm2=xy(j-2,k,1)/xyj(j-2,k)
            tm1=xy(j-1,k,1)/xyj(j-1,k)
            t=xy(j,k,1)/xyj(j,k)
            xix_xi(j,k)=tmp*(1.1d1*t - 1.8d1*tm1 +9.d0*tm2 - 2.d0*tm3)
c        
            tm3=xy(j-3,k,2)/xyj(j-3,k)
            tm2=xy(j-2,k,2)/xyj(j-2,k)
            tm1=xy(j-1,k,2)/xyj(j-1,k)
            t=xy(j,k,2)/xyj(j,k)
            xiy_xi(j,k)=tmp*(1.1d1*t - 1.8d1*tm1 +9.d0*tm2 - 2.d0*tm3)
 45      continue
c
         do 50 j=jbegin,jend
            k=kbegin
            tp3=xy(j,k+3,3)/xyj(j,k+3)
            tp2=xy(j,k+2,3)/xyj(j,k+2)
            tp1=xy(j,k+1,3)/xyj(j,k+1)
            t=xy(j,k,3)/xyj(j,k)
            etax_eta(j,k)=tmp*(-1.1d1*t + 1.8d1*tp1 -9.d0*tp2 +2.d0*tp3)
c        
            tp3=xy(j,k+3,4)/xyj(j,k+3)
            tp2=xy(j,k+2,4)/xyj(j,k+2)
            tp1=xy(j,k+1,4)/xyj(j,k+1)
            t=xy(j,k,4)/xyj(j,k)
            etay_eta(j,k)=tmp*(-1.1d1*t + 1.8d1*tp1 -9.d0*tp2 +2.d0*tp3)
c        
            k=kend
            t=xy(j,k,3)/xyj(j,k)
            tm1=xy(j,k-1,3)/xyj(j,k-1)
            tm2=xy(j,k-2,3)/xyj(j,k-2)
            tm3=xy(j,k-3,3)/xyj(j,k-3)
            etax_eta(j,k)=tmp*(1.1d1*t - 1.8d1*tm1 +9.d0*tm2 - 2.d0*tm3)
c        
            t=xy(j,k,4)/xyj(j,k)
            tm1=xy(j,k-1,4)/xyj(j,k-1)
            tm2=xy(j,k-2,4)/xyj(j,k-2)
            tm3=xy(j,k-3,4)/xyj(j,k-3)
            etay_eta(j,k)=tmp*(1.1d1*t - 1.8d1*tm1 +9.d0*tm2 - 2.d0*tm3)
 50      continue
      else
         tmp=.5d0
         do 30 k=kbegin,kend
         do 30 j=jbegin+1,jend-1
            tp1=xy(j+1,k,1)/xyj(j+1,k)
            tm1=xy(j-1,k,1)/xyj(j-1,k)
            xix_xi(j,k)=tmp*(tp1 - tm1)
c        
            tp1=xy(j+1,k,2)/xyj(j+1,k)
            tm1=xy(j-1,k,2)/xyj(j-1,k)
            xiy_xi(j,k)=tmp*(tp1 - tm1)
 30      continue
c        
         do 35 k=kbegin+1,kend-1
         do 35 j=jbegin,jend
            tp1=xy(j,k+1,3)/xyj(j,k+1)
            tm1=xy(j,k-1,3)/xyj(j,k-1)
            etax_eta(j,k)=tmp*(tp1 - tm1)
c        
            tp1=xy(j,k+1,4)/xyj(j,k+1)
            tm1=xy(j,k-1,4)/xyj(j,k-1)
            etay_eta(j,k)=tmp*(tp1 - tm1)
 35      continue
      endif
c
c
c
c     test to see if sums are zero.
c
      if (iord.eq.4) then
         tol=1.d-15
      else
         tol=2.d0*2.221d-16
      endif
c
      klo=kbegin
      kup=kend
      jlo=jbegin
      jup=jend
c
      open(unit=99,file='zero')
      do 900 k=klo,kup
      do 900 j=jlo,jup
         zero1=xix_xi(j,k)+etax_eta(j,k)
         if (abs(zero1).gt.tol) write(99,997) j,k,zero1
 900  continue
c
      write(99,*) ' '
      write(99,*) ' '
c
      do 910 k=klo,kup
      do 910 j=jlo,jup
         zero2=xiy_xi(j,k)+etay_eta(j,k)
         if (abs(zero2).gt.tol) write(99,998) j,k,zero2
 910  continue
c
 997  format(2I5,2x,7Hzero1= ,e25.15)
 998  format(2I5,2x,7Hzero2= ,e25.15)
 999  format(2I5,2x,2e25.15)
      close(unit=99)
      return
      end

