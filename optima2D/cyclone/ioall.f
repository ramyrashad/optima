      subroutine ioall(iwhat,junit,jdim,kdim,q,qold,press,sndsp,turmu,
     &                 fmu,vort,xy,xyj,x,y,obj0s)                           
#ifdef _MPI_VERSION
      use mpi
#endif

c
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"


c                                                                    
      integer strlen,KODE
c                                                                     
      dimension q(jdim,kdim,4),turmu(jdim,kdim),vort(jdim,kdim)       
      dimension press(jdim,kdim),sndsp(jdim,kdim),fmu(jdim,kdim)      
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),qold(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension obj0s(mpopt)
c                                                                     
      dimension work2(maxj,30)
      common/worksp/work2              
c                                                                     
      dimension dtiseq(20),dtmins(20),dtow2(20)
      dimension jmxi(20),kmxi(20),jskipi(20),iends(20)
      common/mesup/ dtiseq,dtmins,dtow2,isequal,iseqlev,
     &              jmxi,kmxi,jskipi,iends

c     -- array used to store skin friction --
      dimension cftmp(maxj) 

c     -- JG: store AF iterations --
      integer afiters
      common/afiter/afiters

c     -- store the command to call the system --
      character command*40
      integer namelen

c     -- array used to store bl profiles -- 
      integer jj, k
      integer jbltemp(7)
      double precision yy, vel, uu, vv, p, rho, a, M
      double precision pchordtemp(7)

      go to (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22
     &     ,23,24,25,26,27,28,29,30) iwhat  
c                                                                     
c   -note: You must do numbers in order ... so don't do 19,20,21,24
c                                           it won't work properly
c
c     **************************************************************
c     summary of ioall
c     
c     number  description
c     ------  -----------
c       1     optimization: cp body for inverse design
c       2     read polar file for sweeps
c       3     read grid from unit 8
c       4     read restart soln and turb. from rq_unit
c       5     write PROBE info to screen
c       6     output cp dist at klineconst to cp_unit
c       7     output aerodynamic coefficients, tecplot format
c       8     write coefficient of pressure to n_cphis 
c             & write coefficient of friction to n_cfhis
c       9     grid sequencing input
c      10     output q file for restart to q_unit
c      11     final output of forces to ld_unit
c      12     nothing
c      13     nothing
c      14     output viscous data to out_unit, y+ u+ cf
c      15     writes grid to file
c      16     nothing
c      17     nothing
c      18     nothing
c      19     Open meanflow convergence history file to his_unit
c      20     nothing
c      21     write binary residual for tecplot viewing to res_unit
c      22     store solutions on coarse frids during grid sequencing
c      23     read restart solution from coarser grid (GRDSEQ_REST=TRUE)
c      24     output flux budgets to bud_unit
c      25     output turbulent variables to turb_unit in plot3d format
c      26     output skinfriction (Cf) to cf_unit
c      27     output boundary layer properties to blprop_unit
c      28     output flow residual to gnuplot_res_unit for GNUplot
c      29     output boundary layer velocity profiles to blvel_unit
c      30     output transition prediction information to tp_unit
c
c     *****************************************************************
 1    continue                                                        

c     -- optimization --
c     -- write coefficient of pressure to press array for 
c        inverse design --
      
      cpc = 2.d0/(gamma*fsmach*fsmach)                                 
      do j = jtail1,jtail2                                          
        pp = press(j,1)*xyj(j,1)                                        
        press(j,1) = (pp*gamma-1.d0)*cpc                                       
c        write(scr_unit,*) j,press(j,1)
      end do
      go to 10000                                                     
c     *****************************************************************
 2    continue      
c     -- read polar file for sweeps --
      read (ip_unit,*) ipolar

      if (ipolar.gt.100) stop 'ioall: ipolar > 100'

      do i=1,ipolar
         read(ip_unit,*) polar(i)
      end do

      write(scr_unit,*) 'ipolar',ipolar
      do i=1,ipolar
         write(scr_unit,*) polar(i)
      end do      

      write (op_unit,200) 

 200  format('VARIABLES= "No" "M" "C_D" "C_L" "C_M" "ALPHA" "RESID"',
     &     '"L_2 GRAD" "TIME"')
      go to 10000                                                      
c     *****************************************************************
 3    continue

c     -- read geometry --

      if ( iread.eq.1 ) then                                               
        read(grid_unit,*) jmax2, kmax2                             
        if ( ( jmax2 .ne. jmaxold) .or. ( kmax2 .ne. kmax) )then    
          write (out_unit,*)
     &          'jmax, kmax do not match in ioall grid input'     
          stop                                                       
        end if                                                   

c        read(grid_unit,310)  ((x(j,k),j=1,jmax2) ,k=1,kmax2), 
c     &               ((y(j,k),j=1,jmax2) ,k=1,kmax2) 

        read(grid_unit,*)  ((x(j,k),j=1,jmax2) ,k=1,kmax2), 
     &               ((y(j,k),j=1,jmax2) ,k=1,kmax2) 

      else if ( iread.eq.2 ) then                                           
        read(grid_unit) jmax2, kmax2                                          
        if ( ( jmax2 .ne. jmaxold) .or. ( kmax2 .ne. kmax) ) then      
          write (out_unit,*)
     &          'jmax, kmax do not match in ioall grid input'     
          stop                                                       
        end if                                                         

        read(grid_unit)  ((x(j,k),j=1,jmax2) ,k=1,kmax2),
     &           ((y(j,k),j=1,jmax2) ,k=1,kmax2)

c     -- write out grid in ascii after reading it in binary --
c        write(80,300) jmax2,kmax2
c        write(80,310) ( ( x(j,k),j=1,jmax2) ,k=1,kmax2) ,
c     &        ( ( y(j,k),j=1,jmax2) ,k=1,kmax2)             
c        stop
      endif                                                          
c     
 300  format(2i5)                                                      
 310  format(4e16.8)                                                   
      go to 10000                                                      
c     *****************************************************************
 4    continue

c     -- read the restart  --
c     -- read in a binary restart file from rq_unit --
c     -- note: the q read in is not yet scaled by the metric jacobian 
c        xyj, the rescaling is done in initia.
c     dtold holds the time step size that was used to get from the second
c     to last to the last time step. Important for not self starting time-marching
c     method BDF2

      dtold=0.d0

      if ((opt_restart).or.(warm1st).or.(postProcessOnly)) then
         write (*, *) 'RR: debug in ioall(4) - a'
         
         if (unsted .and. jesdirk.eq.1) then
           read(rq2_unit) jmax2,kmax2                                           
           read(rq2_unit) fsmtem,alptem,retem,totime                            
           read(rq2_unit) (((qold(j,k,n),j=1,jmax2) ,k=1,kmax2) ,n=1,4)         
           read(rq2_unit,IOSTAT=KODE) jtitem, istart, dtold 

           if (itmodel.eq.2 .and. turbulnt) then
             read(rq2_unit,end=420) ((turold(j,k), j=1,jmax2),k=1,kmax2)
             read(rq2_unit,end=420) ((turmu(j,k), j=1,jmax2),k=1,kmax2)
           endif
           rewind rq2_unit
         endif

         read(rq_unit) jmax2,kmax2                                              
         read(rq_unit) fsmtem,alptem,retem,totime                               
         read(rq_unit) ((( q(j,k,n),j=1,jmax2) ,k=1,kmax2) ,n=1,4)            
         read(rq_unit,IOSTAT=KODE) jtitem, istart, dtold

         if (dtold.eq.0.d0 .and. nk_skip.gt.0) then
            dtold=dtbig
         else if (dtold.eq.0.d0 .and. nk_skip.eq.0) then
            dtold=dtiseq(1)
         end if

         write(out_unit, 410) totime, istart+1                                  
 410     format(12htotal time =,f12.2,4x,20hstarting iteration =, i7)

         ! turbulent variables  
         if (itmodel.eq.2 .and. turbulnt) then

           read(rq_unit,end=421) ((turre(j,k), j=1,jmax2),k=1,kmax2) 
           read(rq_unit,end=421) ((turmu(j,k), j=1,jmax2),k=1,kmax2)

           ! Avoid resetting turre to freestream and turmu to zero
           if (postProcessOnly .or. .not.zeroturre) then 
              rewind rq_unit
              go to 10000
           end if

        end if

         write (*, *) 'RR: debug in ioall(4) - b'

         rewind rq_unit

         goto 430

 420     do k=1,kmax2
           do j=1,jmax2
             turold(j,k)=retinf
             turmu(j,k)=0.d0
           enddo
         enddo
         write (out_unit,*) 'WARNING!!!'
         write (out_unit,*)
     &      'Restart data for old turbulence data not available.'
         write (out_unit,*) 'Execution continuing ...'
         call flush(out_unit)

 421     do k=1,kmax2
           do j=1,jmax2
             turre(j,k)=retinf
             turmu(j,k)=0.d0
           enddo
         enddo
         write (out_unit,*) 'WARNING!!!'
         write (out_unit,*)
     &      'Restart data for turbulence model not available.'
         write (out_unit,*) 'Execution continuing ...'
         call flush(out_unit)

      end if ! (opt_restart)

clb
      if (obj_restart) then
         do mp=1,mpopt
            read(rqo_unit,400) obj0s(mp)
            if (rank==0) write(obj_unit,400) obj0s(mp)
 400        format(e40.34)
         end do
      end if
clb

 430  go to 10000

 5    continue

c     -- PROBE convergence screen output --
      write (out_unit,50) junit,resid,totime1,fpar(1),fpar(12),ipar(7)
      call flush(out_unit)

 50   format (i8,e12.4,3f10.3,i6)

      go to 10000                                                     
c     *****************************************************************
 6    continue

c     -- write coefficient of pressure and pressure to cp_unit --
      if (unsted) then
         cpc = 2.d0/(gamma*fsmach*fsmach)
         do j = jtail1,jtail2      
           pp = press(j,klineconst)*xyj(j,klineconst)  
           cp = (pp*gamma-1.d0)*cpc       
           if (cpout) write(cp_unit,610) x(j,klineconst), cp, pp
         end do
      else
         cpc = 2.d0/(gamma*fsmach*fsmach)
         do j = jtail1,jtail2 
           pp = press(j,1)*xyj(j,1)   
           cp = (pp*gamma-1.d0)*cpc  
           if (cpout) write(cp_unit,611) x(j,1), cp
         end do
      end if

 610  format(3e25.16)
 611  format(2e25.16)
      rewind(cp_unit)
      go to 10000
c     *****************************************************************
 7    continue 
      
c     -- output aerodynamic coefficients, tecplot format --
      write (op_unit,70) junit, fsmach, cdt, clt, cmt, alpha, resid,
     &     gradmag, totime1
      call flush(op_unit)
      totime1 = 0.0
 70   format (i3, f8.4, 4e15.7, 2e11.3, e11.4)

      go to 10000                                                      
c     *****************************************************************
 8    continue

c     -- write coefficient of pressure to n_cphis --
c     -- write coefficient of friction to n_cfhis --

      write (n_cphis,800) alpha
      write (n_cfhis,800) alpha

      call skinfrct(jdim,kdim,q,x,y,xy,cftmp)

      cpc = 2.d0/(gamma*fsmach*fsmach)                                 
      do j = jtail1,jtail2                                          
         pp = press(j,1)*xyj(j,1)
         cp = (pp*gamma-1.d0)*cpc                                       
         write(n_cphis,610) x(j,1), cp
         write(n_cfhis,610) x(j,1), cftmp(j)
      end do
      call flush(n_cphis)
      call flush(n_cfhis)
      
 800  format('ZONE T=" AOA ',f6.3,'"')

      go to 10000                                                      
c     *****************************************************************
 9    continue

c     -- grid sequencing input --

      read(input_unit,*)                                                        
      read(input_unit,*)isequal

      if (sv_grdseq .and. isequal.eq.1) then
        write (out_unit,*) 'Error in Input'
        write (out_unit,*) 'sv_grdseq was set to true but isequal=1'
        stop
      endif
      if (rank==0)then
         if ( isequal .ne. 1) then
           write(scr_unit,*)'!! WARNING !!'
           write(scr_unit,*)'Grid sequencing and optimization not setup'
           write(scr_unit,*)'Check restart flag in routine initia.f and'
           write(scr_unit,*)'calls to routine setup ...'
         end if
      end if

      if (grdseq_rest .and. isequal.eq.1) then
        write (out_unit,*) 'Error in Input'
        write (out_unit,*)
     &        'ISEQUAL must be greater than 1 for GRDSEQ_REST'
        stop
      endif
      if(isequal .ge. 1)  then                                         
      read(input_unit,*)     
      if (isequal.gt.1 .and. ismodel.gt.1 .and. meth.ne.3) then
        write (out_unit,*)
     &        'DUAL TIME-STEPPING SCHEME NOT SET UP FOR GRID-SEQ.'
      stop
      endif
      write(out_unit,*)
      write (out_unit,*)
     &      '------------------------------------------------------'
      write(out_unit,910)
 910  format(2H |,10x,29HGrid and time-step Sequencing,13x,1H|)
      write (out_unit,*)
     &      '------------------------------------------------------'
      do ii = 1, isequal                                              
        read(input_unit,*) jmxi(ii),kmxi(ii),iends(ii),dtiseq(ii),
     &                     dtmins(ii),dtow2(ii) 
        jskipi(ii) = (jmaxold-1)/(jmxi(ii)-1)
        write(out_unit,920) ii
 920    format(7H | Grid,I4,43x,1H|)
        write(out_unit,925) jmxi(ii),kmxi(ii)
        write(out_unit,930) jtail1,jtail2
        write(out_unit,935) jskipi(ii),iends(ii)
        write(out_unit,940) dtiseq(ii),dtmins(ii)
        write(out_unit,945) dtow2(ii)
 925    format(12H |   jmax = ,I7,17H          kmax = ,I7,11x,1H|)
 930    format(12H | jtail1 = ,I7,17H        jtail2 = ,I7,11x,1H|)
 935    format(12H |  jskip = ,I7,17H         steps = ,I7,11x,1H|)
 940    format(12H |     dt = ,e12.4,12H    dtmin = ,e12.4,6x,1H|)
 945    format(12H |   dtow = ,e12.4,30x,1H|)
      end do
c     -- JG: store max AF iterations to common block -- 
      afiters = iends(1)
      dt2=dtiseq(1)
      if (unsted) afiters=0
      write (out_unit,*)
     &      '------------------------------------------------------'
      else                                                           
         write(out_unit,*)'Error in input,  isequal < 1'
         stop
      endif
      go to 10000                                                       
c     *****************************************************************
 10   continue

c     -- store a binary restart file --
      if(periodic .and. jmax.ne.jmaxold)then                            
c     special logic for overlap o grid case                          
c     set jmaxold point from j = 1 point                             
        do k = 1,kmax                                             
c     note jacobian scaling ok                                    
          q(jmaxold,k,1) = q(1,k,1)                                   
          q(jmaxold,k,2) = q(1,k,2)                                   
          q(jmaxold,k,3) = q(1,k,3)                                   
          q(jmaxold,k,4) = q(1,k,4)                                   
          xyj(jmaxold,k) = xyj(1,k)                                   
        end do
      endif                                                             
c     
c     rewind because this may be called periodically                  
      if (unsted) then
        write(q2_unit) jmaxold,kmax                                        
        write(q2_unit) fsmach,alpha,re*fsmach,totime                        
        write(q2_unit) (((qold(j,k,n)*xyj(j,k),j=1,jmaxold),k=1,kmax), 
     &        n=1,4)
        write(q2_unit) jtail1, numiter-1, dt2 
c
        if (itmodel.eq.2) then
          write(q2_unit) ((turold(j,k), j=1,jmaxold),k=1,kmax)
          write(q2_unit) ((turmu(j,k), j=1,jmaxold) ,k=1,kmax)
        endif
        rewind (q2_unit)
      endif
c
      write(q_unit) jmaxold,kmax                                             
      write(q_unit) fsmach,alpha,re*fsmach,totime                            
      write(q_unit) (((q(j,k,n)*xyj(j,k),j=1,jmaxold) ,k=1,kmax) ,n=1,4)
      write(q_unit) jtail1, numiter, dt2                                          
c                                                                       
      if (itmodel.eq.2) then
        write(q_unit) ((turre(j,k), j=1,jmaxold) ,k=1,kmax)
        write(q_unit) ((turmu(j,k), j=1,jmaxold) ,k=1,kmax)
      endif
      rewind q_unit                                                          
c     
      go to 10000                                                       
c     *****************************************************************
 11   continue

c     -- final output of forces to ld_unit --

      if (ldout) write (ld_unit,1100) junit,cli,cdi,cmi
      if (viscous) then
        if (ldout) write(ld_unit,1200) clv,cdv,cmv
        if (ldout) write(ld_unit,1300) clt,cdt,cmt
      endif
      if (ldout) write (ld_unit,*)
      if (ldout) write (ld_unit,*)

 1100 format(5hGrid ,i1,1x,13hPressure Cl= ,f12.9,2x,4hCd= ,f12.9,2x,
     &      4hCm= ,f12.9)
 1200 format(7x,13hFriction Cl= ,f12.9,2x,4hCd= ,f12.9,2x,4hCm= ,f12.9)
 1300 format(7x,13hTotal    Cl= ,f12.9,2x,4hCd= ,f12.9,2x,4hCm= ,f12.9)
      junit=0
      if (ldout) call flush (ld_unit)

      goto 10000                                                        
c     *****************************************************************
 12   continue                                                          
      goto 10000                                                        
c     *****************************************************************
 13   continue                                                          
      goto 10000                                                        
c     *****************************************************************
 14   continue                                                          

c     -- viscous data --

      call visvar(jdim,kdim,q,press,vort,turmu,fmu,x,y,xy,xyj)          

      goto 10000                                                        
c     *****************************************************************
 15   continue
c     -- After the optimization run has converged, write the final grid
c     -- coordinates to file
 
      namelen = strlen(output_file_prefix0)
      filena = output_file_prefix0
      filena(namelen+1:namelen+3)='.g'

      if (iread.eq.1) then
         open(unit=grid_unit,file=filena,status='unknown',
     &        form='formatted')
      endif
      if (iread.eq.2) then
         open(unit=grid_unit,file=filena,status='unknown',
     &        form='unformatted')
      endif

      write(grid_unit) jdim,kdim
      write(grid_unit) ((x(j,k),j=1,jdim),k=1,kdim),
     &     ((y(j,k),j=1,jdim),k=1,kdim)

      call flush(grid_unit)
      close(grid_unit)

      write(scr_unit,151)
 151  format
     &(/3x,'Grid file overwritten for this successful design iteration')

      goto 10000
c     *****************************************************************
 16   continue   

      iread = 1

      namelen = strlen(output_file_prefix0)
      filena = output_file_prefix0
      filena(namelen+1:namelen+3)='.g'

      namelen = strlen(filena)
      command = 'rm -f '
      command(7:6+namelen) = filena

      call system(command)

      if (iread.eq.1) then
         open(unit=grid_unit,file=filena,status='new',
     &        form='formatted')
      endif
      if (iread.eq.2) then
         open(unit=grid_unit,file=filena,status='new',
     &        form='unformatted')
      endif

      write(grid_unit,*) jdim,kdim
      write(grid_unit,*) ((x(j,k),j=1,jdim),k=1,kdim),
     &     ((y(j,k),j=1,jdim),k=1,kdim)

      call flush(grid_unit)
      close(grid_unit)

      iread = 2

      write(scr_unit,161)
 161  format(3x,'Grid written to file',/)
                                                       
      go to 10000                                                       
c     *****************************************************************
 17   continue                                                          
      go to 10000                                                       
c     *****************************************************************
 18   continue                                                          
      go to 10000                                                       
c     *****************************************************************
 19   continue                                                          

c     -- write residual convergence history to unit 19 --

      if (omega .eq. 0.d0) then

         if (hisout) write(his_unit,1900) numiter, totime1, resid, 
     &   maxres(1), maxres(2), residmx, clt, cdt, alpha, cli,clv,cdi,cdv
         if (hisout) call flush(his_unit)

c 1900    format(i6,e11.4,e12.5,i5,i4,e12.5,2e22.14,e18.10,4e16.8)
 1900    format(i6,e11.4,e12.5,i5,i4,e12.5,2e16.8,e16.8,4e16.8)

      else

         if (hisout) write(his_unit,1901) numiter, totime1, resid,
     &   maxres(1), maxres(2), residmx, clt, cdt, alpha, cli,clv,cdi,
     &   cdv,omega
         if (hisout) call flush(his_unit)

c 1901    format(i6,e11.4,e12.5,i5,i4,e12.5,2e22.14,e18.10,4e16.8,e16.5)
 1901    format(i6,e11.4,e12.5,i5,i4,e12.5,2e16.8,e16.8,4e16.8,e16.5)

      end if

      go to 10000                                                       
c     *****************************************************************
 20   continue 
      go to 10000                                                       
c     *****************************************************************
 21   continue
c     
c     -- write binary residual to file (like you do a solution)
c     note: that press here is really ds ... the time-step scaling.
      write(res_unit) jmaxold,kmax
      write(res_unit) fsmach,alpha,re*fsmach,totime
      kmx=kmax
      jmx=jmaxold
      if (cmesh) then
        write(res_unit) (((abs(q(j,k,n))/press(j,k), j=1,jmx),k=1,kmx),
     &        n=1,4)
      else
        write(res_unit) (((abs(q(j,k,n)),j=1,jmx) ,k=1,kmx) ,n=1,4)
      endif
      rewind res_unit
      go to 10000
c     *****************************************************************
 22   continue
     
      namelen =  strlen(output_file_prefix)
      if (junit.eq.qc_unit) then
        filena = output_file_prefix
        filena(namelen+1:namelen+4) = 'c.q'
        open(unit=qc_unit,file=filena,status='new',form='unformatted')
      endif
      if (junit.eq.qb_unit) then
        filena = output_file_prefix
        filena(namelen+1:namelen+4) = 'b.q'
        open(unit=qb_unit,file=filena,status='new',form='unformatted')
      endif
     
      write(junit) jmax,kmax
      write(junit) fsmach,alpha,re*fsmach,totime                        
      write(junit) (((q(j,k,n)*xyj(j,k),j=1,jmax) ,k=1,kmax) ,n=1,4)  
      write(junit) jtail1, numiter
     
      if (itmodel.eq.2) then
        write(junit) ((turre(j,k), j=1,jmax),k=1,kmax)
        write(junit) ((turmu(j,k), j=1,jmax) ,k=1,kmax)
      endif
     
      close(junit)
      goto 10000
c     *****************************************************************
 23   continue
     
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+3) = '.q'
      open (unit=rq2_unit,file=filena,status='old',form='unformatted')
     
      read(rq2_unit) jmax2,kmax2 
      read(rq2_unit) stuff,stuff,stuff,stuff
      read(rq2_unit) ( ( ( q(j,k,n),j=1,jmax2) ,k=1,kmax2) ,n=1,4)             
      read(rq2_unit,IOSTAT=KODE) jstuff, istart,dtold
      istart=istart+1
     
      do n=1,4
        do k=1,kmax2
          do j=1,jmax2
            q(j,k,n)=q(j,k,n)/xyj(j,k)
          end do
        end do
      end do

      if (itmodel.eq.2) then
        read(rq2_unit,end=2310) ((turre(j,k), j=1,jmax2),k=1,kmax2) 
        read(rq2_unit,end=2310) ((turmu(j,k), j=1,jmax2),k=1,kmax2)
      endif

      goto 2320

 2310 do k=1,kmax2
        do j=1,jmax2
          turre(j,k)=retinf
          turmu(j,k)=0.d0
        enddo
      enddo
      write (out_unit,*) 'WARNING!!!'
      write (out_unit,*)
     &      'Restart data for turbulence model not available.'
      write (out_unit,*) 'Execution continuing ...'

 2320 close(rq2_unit)

      goto 10000
c     *****************************************************************
 24   continue

c     -- remove dt scaling in budget --
      do n=1,3
        do k=kbegin,kend
          do j=jbegin,jend
            budget(j,k,n)=budget(j,k,n)/dt
          end do
        end do
      end do
c     
c     -flux budget balance in tecplot format --
c     
c     write (80,*) 'variables="x","y","Convective Flux",
c     $"Viscous Flux","Artificial Dissipation"'
c     write (80,*) 'zone f=point,i=',jmax-2,', j=',kmax-2
c     do k=2,kmax-1
c     do j=2,jmax-1
c     write (80,221) x(j,k),y(j,k),budget(j,k,1),budget(j,k,3),
c     $           budget(j,k,2)
c     end do
c     end do
c     
c     -- flux budget balance in gnuplot format
c        also includes boundary layer profile --
      j = jtail2
      do while (x(j,1) .gt. pchord)
        j = j - 1
      end do
      jm = j
c     
      fsmachinv=1.d0/fsmach
      do k = klow,kend
c     - calculate vel for boundary layer profile
        rhoinv= 1.d0/q(jm,k,1)
        uu    = q(jm,k,2)*rhoinv
        vv    = q(jm,k,3)*rhoinv
        vel   = dsqrt(uu**2+vv**2)*fsmachinv
        yy    = y(jm,k)-y(jm,1)
        write (bud_unit,2400) yy, budget(jm,k,1), budget(jm,k,2),
     &        budget(jm,k,3), vel
c     
c     bud1 = convective flux
c     bud2 = dissipation
c     bud3 = viscous flux
c     
      end do
 
 2400 format (e15.7,4e16.8)
      write(out_unit,2410) pchord*100.,jm
 2410 format(1x,41HFlux budget computed on upper surface at ,f4.1,
     *      16H% chord with j= ,I3)
      rewind bud_unit
      go to 10000
c     
c     *****************************************************************
 25   continue

c     -- writing turbulent variables to turb_unit in plot3d format --

      write(turb_unit) jmaxold,kmax
      write(turb_unit) fsmach,alpha,re*fsmach,totime
      write(turb_unit) ((fmu(j,k)  ,j=1,jmaxold),k=1,kmax),
     &      ((turre(j,k),j=1,jmaxold),k=1,kmax),
     &      ((turmu(j,k),j=1,jmaxold),k=1,kmax),
     &      ((vort(j,k) ,j=1,jmaxold),k=1,kmax)
      write(turb_unit) jtail1, numiter
      rewind turb_unit
      goto 10000
c     *****************************************************************
 26   continue

c     -- write skinfriction data to unit 17 --
      call skinfrct(jdim,kdim,q,x,y,xy,work2)
      rewind cf_unit
c     
      goto 10000
c     *****************************************************************
 27   continue

c     ----------------------------------------------------
c     -- write boundary layer properties to output file --
c     -- Ramy Rashad (Oct. 2010)                        --
c     ----------------------------------------------------

c     -- fixed chord positions for which to output BL properties
      pchordtemp(1) = 0.01
      pchordtemp(2) = 0.05
      pchordtemp(3) = 0.1
      pchordtemp(4) = 0.2
      pchordtemp(5) = 0.4
      pchordtemp(6) = 0.6
      pchordtemp(7) = 0.8
c     -- Store inverse of Mach number used for dimensionality
c     -- (i.e. to obtain vel/vel_inf)
      fsmachinv=1.d0/fsmach

c     -- Tecplot header
      write(blprop_unit,*) 'TITLE="blproperties"'
      write(blprop_unit,*) 'VARIABLES="j","k","yy","vel","uu","vv","p"',
     &     ',"rho","a","M","vort","fmu","turmu"'

c     -- At each chordwise station (top surface) output BL properties
      do jbl=1,7

         write (blprop_unit,2710) pchordtemp(jbl)
 2710    format('zone T="',f3.2,' chord"')

         j = jtail2
         do while (x(j,1) .gt. pchordtemp(jbl))
            j = j - 1
         end do
         jbltemp(jbl) = j
         jj = j
!        -- Initialize off wall distance 
         yy = 0
c        -- Calculate the velocity at each node
         do k = klow,kend

!           -- WRONG q: since not rescaled by jacobian of geometric
!           -- transformation
!            yy  = y(jj,k)-y(jj,1)
!            rho = q(jj,k,1)
!            uu  = q(jj,k,2)/rho
!            vv  = q(jj,k,3)/rho
!            vel = dsqrt(uu**2+vv**2)*fsmachinv
!            p   = 0.4*(q(jj,k,4)-0.5*q(j,k,1)*(uu**2 + vv**2))
!            a = sqrt(1.4*p/q(j,k,1))
!            M = sqrt(uu**2 + vv**2)/a
!            write (blprop_unit,2400) yy, vel, uu, vv, p, rho, a, M,
!     &       vort(jj,k), fmu(jj,k), turmu(jj,k)

!           -- CORRECT q: rescaling using jacobian of geometric
!           -- transformation
!           -- Wall distance computation taken from SA model (spalart.f)
            yy  = dsqrt((x(jj,k)-x(jj,1))**2+(y(jj,k)-y(jj,1))**2)
            rho = q(jj,k,1)*xyj(jj,k)
            uu  = q(jj,k,2)/q(jj,k,1)
            vv  = q(jj,k,3)/q(jj,k,1)
            p = 0.4*(q(jj,k,4)*xyj(jj,k)
     &          - 0.5*q(j,k,1)*xyj(jj,k)*(uu*uu + vv*vv))
            a = dsqrt(1.4*p/(q(j,k,1)*xyj(jj,k)))
            M = dsqrt(uu*uu + vv*vv)/a
!           -- The following gives local velocity magnitude normalized
!           -- by freestream velocity (as opposed to freestream sound
!           -- speed)
            vel = dsqrt(uu*uu+vv*vv)*fsmachinv

!           -- Output to file
            write (blprop_unit,2400) jj, k, yy, vel, uu, vv, p, rho, a,
     &           M, vort(jj,k), fmu(jj,k), turmu(jj,k)

         end do ! k
      end do ! jbl

      rewind blprop_unit
      goto 10000
c     *****************************************************************
 28   continue

c     -------------------------------------------------------
c     -- Unused but reserved for GNUplot output (using ioall)  
c     -- Ramy Rashad (Dec. 2010)                        
c     -------------------------------------------------------

c     -- GNUplot header
      write(gnuplot_res_unit,*) '# GNUplot Flow Residual Data \n'
      write(gnuplot_res_unit,*) '# Interation, Residual \n' 

      rewind gnuplot_res_unit
      goto 10000
c     *****************************************************************
 29   continue

c     -------------------------------------------------------
c     -- Unused but reserved for blvel output (using ioall)  
c     -- Ramy Rashad (Dec. 2010)                        
c     -------------------------------------------------------

c     -- GNUplot header
      write(blvel_unit,*) 'boundary layer velocity data'

      rewind blvel_unit
      goto 10000
c     *****************************************************************
 30   continue

c     -------------------------------------------------------
c     -- Unused but reserved for blvel output (using ioall)  
c     -- Ramy Rashad (Dec. 2010)                        
c     -------------------------------------------------------

c     -- GNUplot header
      write(tp_unit,*) 'transition prediction data'

      rewind tp_unit
      goto 10000
c     *****************************************************************
 31   continue 
      
c     -- output aerodynamic coefficients and transition points tecplot format --
      write (op_unit,71) junit, re_in, fsmach, cdt, clt, cmt, alpha,
     &     resid, gradmag, xTransPredictNoShift(1), xTransPredict(1),
     &     xTransPredictNoShift(2), xTransPredict(2) 
      call flush(op_unit)
 71   format (i3, 12e15.7)

      go to 10000                                                      
c     *****************************************************************
10000 return                                                            
      end                                                               
