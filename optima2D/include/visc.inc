c         prlam   =  laminar prandtl number  = .72                      
c         prturb  =  turbulent prandtl number = .90                     
c         prlinv  =  1./(laminar prandtl number)                        
c         prtinv  =  1./(turbulent prandtl number)                      
c         f13     =  1/3                                                
c         f43     =  4/3
c         f23     =  2/3       

      double precision prlam, prturb, prlinv, prtinv,f43,f13,f23

      parameter ( prlam = .72d0 , prturb = 0.9d0 )                            
      parameter ( prlinv = 1.d0/prlam , prtinv = 1.d0/prturb )       
      parameter ( f43=4.d0/3.d0,f13 = 1.d0/3.d0,f23 =2.d0/3.d0)             
