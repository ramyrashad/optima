####################################################################
#  LAPACK make include file.                                       #
#  LAPACK, Version 3.0                                             #
#  June 30, 1999                                                  #
####################################################################
#
SHELL = /bin/sh
#
#  The machine (platform) identifier to append to the library names
#
OS:=$(shell uname)
PLAT:=$(shell uname -m)
#  
#  Modify the FORTRAN and OPTS definitions to refer to the
#  compiler and desired compiler options for your machine.  NOOPT
#  refers to the compiler options desired when NO OPTIMIZATION is
#  selected.  Define LOADER and LOADOPTS to refer to the loader and 
#  desired load options for your machine.
#
FORTRAN  = ifort#g77 
OPTS     = -O2#-funroll-all-loops -fno-f2c -O3
DRVOPTS  = $(OPTS)
NOOPT    =
LOADER   = ifort#g77
LOADOPTS =
#
#  The archiver and the flag(s) to use when building archive (library)
#  If you system has no ranlib, set RANLIB = echo.
#
ARCH     = ar
ARCHFLAGS= cr
RANLIB   = ranlib
#
#  The location of the libraries to which you will link.  (The 
#  machine-specific, optimized BLAS library should be used whenever
#  possible.)
#
BLASLIB      = ../../blas_$(PLAT).a
LAPACKLIB    = lapack_$(PLAT).a
TMGLIB       = tmglib_$(PLAT).a
EIGSRCLIB    = eigsrc_$(PLAT).a
LINSRCLIB    = linsrc_$(PLAT).a
