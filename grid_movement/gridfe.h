
#ifndef GRIDFE_H
#define GRIDFE_H

#include "Face.h"
#include "grid.h"
#include "stiff.h"
#include "stiffTri.h"
#include "tagSort.h"

#define dxTEtol 5.0e-10  // if the x-coordinate of the TE changes more than
// this, the nodes on the TE line will be perturbed

void read_grid (char *filename, vector <Node>& node_loc,
		int &moving_bnd_nodes, vector<Node*>& bnd_nodes, 
		const int &jtail1, const int& jtail2, vector <Face>& faces);

void read_gridTE (char *filename, vector <Node>& node_loc,
		int &moving_bnd_nodes, vector<Node*>& bnd_nodes, 
		const int &jtail1, const int& jtail2, vector <Face>& faces);

void form_faces (vector<Node>& node, vector <Face>& faces, 
		 const int& jmax, const int& kmax);

// form faces for multiblock grid
void form_faces (vector<Node>& node, vector <Face>& faces, 
		 const int& nblk, const int* jmax, const int* kmax);

bool arrange_counter_clockwise (Node** corners);


/*void tagSort(vector <int>& dataArr, vector <int>& tagArr, const int& 
size);
void tagSort(vector <int>& dataArr, int* tagArr, const int* size);
void swap4 ( int* x, int* y );
*/

// assembles the global stiffness matrix
void get_nonzerosfe (vector <Node>& node, int& num_inn_nodes, 
	int& nonzeros);

void get_nonzerosfe_symm (vector <Node> &node, int& num_inn_nodes, 
		       int& nonzeros);

void construct_stiffness_matrix (int* colind, int* rowptr, 
                 const vector <Node>& node, const int& dof);

void construct_stiff_matrix_symm (int* colind, int* rowptr, 
				  const vector <Node>& node);

void form_stiff (CompRow_Mat_double& alpha, const vector <Node>& node, 
		 const vector <Face>& face, VECTOR_double &F,
		const double* elemQo, double& meshQ);

void form_stiff (int*& rowptr, int*& colind, double*& stiff_val, 
		 const vector <Node>& node, 
		 const vector <Face>& face, VECTOR_double &F, const
		 double *elemQo, double& meshQ);

void ini_stiff (CompRow_Mat_double& alpha, const vector <Node>& node, 
		 const vector <Face>& face);

// initial quality of mesh
void get_ini_quality (const vector <Face>& faces, const
	vector <Node>& nodes, double* elemQo, double& Qomax);

void get_new_boundaryMB (char* filename, const vector 
	<Node*>& bnd_nodes, double** deltaxy, const int& len,
	const double& f);
void form_TE_bnd(vector<Node>&nodes, const int& jtail1,
		 const int& jtail2);
void modify_TE_FF (vector <Node>& nodes, const int& jtail1, 
		   const double& factor);


// updating functions
void update (vector <Node>& node, const VECTOR_double& du);
void update_xyz (vector <Node>& node, const VECTOR_double& du);
void update_xyz (vector <Node>& node, const VECTOR_double& du,
		 double* dx, double* dy, const int& incr);

void add_val_csr ( const int& row, const int& col, int*& rowptr,
		   int*& colind, double*& val_array, const double& val);

void init_array(double*& array, const double& size);
void init_array(VECTOR_double& array, const double& size);


#endif  // GRIDFE_H		







