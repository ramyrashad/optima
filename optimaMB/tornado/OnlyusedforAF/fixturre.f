      subroutine fixturre(iblk,jdim,kdim,turre)
c     -this subroutine zero's any turb. eddy viscosity values
c      in regions before transition points.  Finite values
c      may have been placed at these points after prolonging turre 
c      in prlng_turre.f
c
#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/mbcom.inc"
#include "../include/trip.inc"
c
      dimension turre(jdim,kdim)
c
c     ********************************************************************
c     - trip_present(blk#) => set to true if block contains trip point
                              
c     - multi_trans(blk #) => set to true it there are more than one
c                             trip points set for the same side of a block
                              
c     - itrans(blk #)      => number of trip points for given block
                              
c     - itranside(blk #,1) => side that trip belongs to for lower
c                             surface trip point
                              
c     - itranside(blk #,2) => side that trip belongs to for upper
c                             surface trip point
c     ********************************************************************



c     -from here up until line 777, is the same as in spalart_allmaras.f
      multi_trans(iblk)=.false.
      if (itrans(iblk).gt.0) then
c       -there is at least one transition point
        jtran1=itranj(iblk,1)
        jtran2=itranj(iblk,2)
        if (jtran1.gt.0) then
c         -the trans. point is on lower surface
          jtranlo1=jtran1
        else
          jtranlo1=-99
        endif
        if (jtran2.gt.0) then
c         -the trans. point is on upper surface
          jtranup1=jtran2
        else
          jtranup1=-99
        endif
c         -checking to see if there are multiple transition points per
c          block is not as simple as checking if itrans(iblk).gt.1.
c         -what about the case where a block has both sides 1 & 3 touching
c          an airfoil surface (ie. side3 touching slat and side1 touching 
c          main element and both sides having a trip point)
        if (itrans(iblk).gt.1) then
          if (bnd(1,iblk).and.bnd(3,iblk)) then
            multi_trans(iblk)=.false.
          else
            multi_trans(iblk)=.true.
          endif
        endif
      else
        return
c        trip_present(iblk)=.false.
c        jtranup1=-99
c        jtranlo1=-99
      endif
      jtlo=jtranlo1
      jtup=jtranup1
 777  continue
c
c     -if we get here, there must be trip present in this block
c     -if multi_trans=true, then side 1 always touches airfoil
c
      if (multi_trans(iblk)) then
        do k=1,kdim
          do j=jtlo+1,jtup-1
            turre(j,k)=.01*retinf
          enddo
        enddo
      else
c       -note: should assumtions mentioned below be wrong,
c               you must take into account direction of
c               block (i.e. ibcdir(iblk)) when determining
c               j loop indecies ... not done here.
c 
        if (jtlo.gt.0) then
c         lower surface (I am assuming side 3 touches body)
          do k=1,kdim
            do j=1,jtlo-1
              turre(j,k)=.01*retinf
            enddo
          enddo
        elseif (jtup.gt.0) then
c         upper surface (I am assuming side 1 touches body)
          do k=1,kdim
            do j=1,jtup-1
              turre(j,k)=.01*retinf
            enddo
          enddo
        endif
      endif
c
      return
      end
