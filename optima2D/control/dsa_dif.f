c----------------------------------------------------------------------
c     -- differentiation of diffusion terms for the S-A model --
c     written by: marian nemec
c     date: jan. 2001
c----------------------------------------------------------------------
      
      subroutine dsa_dif (jdim, kdim, q, xy, xyj, fmu, dmul, lpt, wk,
     &      dnu, bm2, bm1,db, bp1, bp2)

#include "../include/arcom.inc"
#include "../include/sam.inc"

      integer jdim, kdim

      double precision q(jdim,kdim,5), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision bm2(5,jdim,kdim), bm1(5,jdim,kdim)
      double precision db(5,jdim,kdim), bp1(5,jdim,kdim)
      double precision bp2(5,jdim,kdim), fmu(jdim,kdim)
      double precision dmul(5,jdim,kdim), lpt(jdim,kdim), wk(jdim,kdim)
      double precision dnu(5,jdim,kdim)

      resiginv= sigmainv/re
      difa = (1.0+cb2)*resiginv
      difb = cb2*resiginv

c     -- lpt = nu_laminar + nu_turbulent --
      do k = kbegin,kend
        do j = jbegin,jend
          tnu = q(j,k,5)*xyj(j,k)
          lpt(j,k) = fmu(j,k)/(q(j,k,1)*xyj(j,k)) + tnu
        end do
      end do

c     -- dnu = d( nu + nu_tilde )/d(Q^) --
      do k = kbegin,kend
        do j = jbegin,jend
          ri = 1.d0/(q(j,k,1)*xyj(j,k))
          dnu(1,j,k) = - fmu(j,k)*ri*ri*xyj(j,k) + ri*dmul(1,j,k)
          dnu(2,j,k) = ri*dmul(2,j,k)
          dnu(3,j,k) = ri*dmul(3,j,k)
          dnu(4,j,k) = ri*dmul(4,j,k)
          dnu(5,j,k) = xyj(j,k)
        end do
      end do

c     -- diffusion terms in xi direction --
      do k = klow,kup
        do j = jbegin,jup
          jp = jplus(j)
          wk(j,k) = q(jp,k,5)*xyj(jp,k) - q(j,k,5)*xyj(j,k)
        end do
      end do

      do k = klow,kup
        do j = jlow,jup
          jp = jplus(j)
          jm = jminus(j)
          xy1p = 0.5d0*(xy(j,k,1)+xy(jp,k,1))
          xy2p = 0.5d0*(xy(j,k,2)+xy(jp,k,2))
          ttp = (xy1p*xy(j,k,1)+xy2p*xy(j,k,2))
          
          xy1m = 0.5d0*(xy(j,k,1)+xy(jm,k,1))
          xy2m = 0.5d0*(xy(j,k,2)+xy(jm,k,2))
          ttm =  (xy1m*xy(j,k,1)+xy2m*xy(j,k,2))

c     -- contribution by holding ( nu + nu_tilde ) constant --
          trem = 0.5d0*(lpt(jm,k)+lpt(j,k))
          trep = 0.5d0*(lpt(j,k)+lpt(jp,k))

          cap = ttp*trep*difa
          cam = ttm*trem*difa

          cnub = difb*lpt(j,k)
          cbp = ttp*cnub
          cbm = ttm*cnub 

          bm2(5,j,k) = bm2(5,j,k) + (cbm-cam)/xyj(j,k)*xyj(jm,k)
          db(5,j,k)  = db(5,j,k)  - cbp + cap - cbm + cam
          bp2(5,j,k) = bp2(5,j,k) + (cbp-cap)/xyj(j,k)*xyj(jp,k)

c     -- contribution from ( nu + nu_tilde) term --
          cnua = difa/xyj(j,k)
          cap = ttp*cnua*wk(j,k)
          cam = ttm*cnua*wk(jm,k)

          cnub = difb/xyj(j,k)
          cbp = ttp*cnub*wk(j,k)
          cbm = ttm*cnub*wk(jm,k)               

          do n = 1,5
            bm2(n,j,k) = bm2(n,j,k) + dnu(n,jm,k)*0.5d0*cam
            db(n,j,k)  = db(n,j,k)  + dnu(n,j,k)*( 0.5d0*( - cap + cam )
     &            + cbp - cbm )
            bp2(n,j,k) = bp2(n,j,k) - dnu(n,jp,k)*0.5d0*cap
          end do
        end do
      end do

c     -- diffusion terms in eta direction --
      do k = kbegin,kup
        kp = k + 1
        do j = jlow,jup
          wk(j,k) = q(j,kp,5)*xyj(j,kp) - q(j,k,5)*xyj(j,k)
        end do
      end do

      do k = klow,kup
        kp = k+1
        km = k-1
        do j = jlow,jup
          xy3p = 0.5d0*(xy(j,k,3)+xy(j,kp,3))
          xy4p = 0.5d0*(xy(j,k,4)+xy(j,kp,4))
          ttp  =  (xy3p*xy(j,k,3)+xy4p*xy(j,k,4))
          
          xy3m = 0.5d0*(xy(j,k,3)+xy(j,km,3))
          xy4m = 0.5d0*(xy(j,k,4)+xy(j,km,4))
          ttm  =  (xy3m*xy(j,k,3)+xy4m*xy(j,k,4))
          
c     -- contribution by holding ( nu + nu_tilde ) constant --
          trem = 0.5d0*(lpt(j,km) + lpt(j,k))
          trep = 0.5d0*(lpt(j,k) + lpt(j,kp))
          
          cap  = ttp*trep*difa
          cam  = ttm*trem*difa

          cnub = difb*lpt(j,k)
          cbp  = ttp*cnub
          cbm  = ttm*cnub
          
          bm1(5,j,k) = bm1(5,j,k) + (cbm-cam)/xyj(j,k)*xyj(j,km)
          db(5,j,k)  = db(5,j,k)  - cbp+cap-cbm+cam
          bp1(5,j,k) = bp1(5,j,k) + (cbp-cap)/xyj(j,k)*xyj(j,kp)

c     -- contribution from ( nu + nu_tilde) term --
          cnua = difa/xyj(j,k)
          cap  = ttp*cnua*wk(j,k)
          cam  = ttm*cnua*wk(j,km)
          
          cnub = difb/xyj(j,k)
          cbp  = ttp*cnub*wk(j,k)
          cbm  = ttm*cnub*wk(j,km)
          
          do n = 1,5
            bm1(n,j,k) = bm1(n,j,k) + dnu(n,j,km)*0.5d0*cam
            db(n,j,k)  = db(n,j,k)  + dnu(n,j,k)*( 0.5d0*( - cap + cam )
     &            + cbp - cbm)
            bp1(n,j,k) = bp1(n,j,k) - dnu(n,j,kp)*0.5d0*cap 
          end do
          
        end do
      end do
      
      return
      end                       !dsa_dif
