#ifndef CUTERPROBLEM
#define CUTERPROBLEM

#include "snoptProblem.hh"

#ifndef F2C_INCLUDE
#include "f2c.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif
  /** Set up data structures for subsequent computations.  @ingroup CuterSupport */
  void csetup_(integer *   input,     integer *   iout,     integer * n,      integer * m,
	       double  x[],       double  bl[],     double bu[],  integer * nmax,
	       integer     equatn[],  integer     linear[],
	       double  v[],       double  cl[],     double cu[],
	       integer *   mmax,      integer *   efirst,   integer *lfirst,  integer *nvfrst );

  /** Evaluate the objective and general constraint function values.
   *  @ingroup CuterSupport */
  void cfn_ (integer *, integer *, double *, double *, integer *, double *);

  /** Evaluate the gradients of the general constraint functions.
   *  @ingroup CuterSupport */
  void cgr_ (integer *, integer *, double *, integer *, integer *, double *,
	     double *, integer *, integer *, integer *, double *);

  /** Evaluate the Hessian matrix of the Lagrangian ( when stored as a dense
   *  matrix).  @ingroup CuterSupport */
  void cdh_ (integer *, integer *, double *, integer *, double *, integer *, double *);

  /** Evaluate the gradients of the general (sparse) constraint functions.
   *  @ingroup CuterSupport */
  void csgr_( integer *n, integer *m, integer *grlagf, integer *lv, double v[],
	      double x[], integer *nnzj, integer *lcjac, double cjac[],
	      integer indvar[], integer indfun[] );

  /** Evaluate the Hessian matrix of the Lagrangian ( when stored as a sparse
   *  matrix).  @ingroup CuterSupport */
  void csh_( integer *n, integer *m, double x[], integer *lv, double v[], integer *nnzh,
	     integer *lh, double H[], integer irnh[], integer icnh[] );

  /** Find number of nonzeros in sparse Jacobian */
  void cdimsj_(integer *);

  /** Find number of nonzeros in sparse Hessian */
  void cdimsh_(integer *);

  void cnames_( integer *n, integer *m, char *pname, char *vname, char *gname,
		ftnlen pname_len, ftnlen vname_len, ftnlen gname_len );


  void cdimen_( integer *input, integer *n, integer *m );

  int cuterusrfg_( integer    *Status, integer *n,    doublereal x[],
		   integer    *needF,  integer *neF,  doublereal F[],
		   integer    *needG,  integer *neG,  doublereal G[],
		   char       *cu,     integer *lencu,
		   integer    iu[],    integer *leniu,
		   doublereal ru[],    integer *lenru);

#ifdef __cplusplus
}
#endif

/* class snoptProblem performs problem set up, initialization,  */
/* and problem specific variable storage. */
class cuterProblem : public snoptProblem {
protected:
  integer nx, mc;
  integer *equatn, *linear;
  void makePrintName();
public:
  cuterProblem();
  ~cuterProblem();
  void alloc( integer n, integer neF );
  virtual char *pname() { return printname; }
  virtual void initialize();
};


#endif
