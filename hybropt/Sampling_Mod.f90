!
! ===================================================================
!
module Sampling_Mod
!
! ------------------------------------------------------------------- 
! Purpose: Contains neccessary subroutines to perform
!          sampling of the design space
!
! Author:  Oleg Chernukhin, May 2010
! -------------------------------------------------------------------

use CommonH

implicit none

! _________________
! module variables

   integer, parameter :: dnunit = 251

   integer, allocatable :: dirnums(:,:)
   integer, allocatable :: dnsize(:)
   integer, allocatable :: aval(:)

   integer :: scal = 31 ! less likely to run into problems than 32

! ___________________
! module subroutines
contains
!
! =====================================================================
!
subroutine getSobolPt(n, d, point)
!
! ---------------------------------------------------------------------
!
! Purpose: 
!    Calculates 'n'-th point in the Sobol sequence in 
!    'd'-dimensional space and stores it in the 'point' array
!
! Post:
!    'point' now contains the scaled coordinates of the sample pt
!
! ---------------------------------------------------------------------
! ______________________
! variable specification

   !-- subroutine arguments
   integer :: n, d
   real(kind=dp), dimension(d) :: point 

   !-- local variables
   integer :: value
   integer :: ii, jj, kk, L
   integer :: C(n)
   integer, allocatable :: V(:)
   integer, allocatable :: X(:)

! _____________________
! begin main execution

   ! L is max number of bits needed
   L = ceiling(log(dble(n)) / log(2.0d0))

   ! C(i) is index from the right of the first zero bit of i
   C(1) = 1
   do ii = 2, n
      C(ii) = 1
      value = ii-1
      do while (iand(value, 1) == 1)
         value = ishft(value, -1)
         C(ii) = C(ii) + 1
      end do
   end do

   ! --- Compute the first dimension ---

   ! Compute direction numbers V(1) to V(L), scaled
   
   allocate(V(L))

   do ii = 1, L
      V(ii) = ishft(1, scal-ii) ! all m's = 1
   end do

   ! Evalulate X(1) to X(N), scaled
   allocate(X(n))
   X(1) = 0
   do ii = 2, n
      X(ii) = ieor(X(ii-1), V(C(ii-1)))
   end do

   point(1) = dble(X(n) / (2.0d0**scal))

   ! --- Compute the remaining dimensions --- 
   do jj = 2, d

      ! Compute direction numbers V[1] to V[L], scaled
      if (L <= dnsize(jj)) then  
         do ii = 1, L
            V(ii) = ishft(dirnums(jj,ii), scal-ii)
         end do
      else
         do ii = 1, dnsize(jj)
            V(ii) = ishft(dirnums(jj,ii), scal-ii)
         end do
         do ii = dnsize(jj)+1, L
            V(ii) = ieor(V(ii-dnsize(jj)), ishft(V(ii-dnsize(jj)), -dnsize(jj)))
            do kk = 1, dnsize(jj) - 1
               V(ii) = ieor(V(ii), iand(ishft(aval(jj), -(dnsize(jj)-1-kk)), 1) * V(ii-kk))
            end do
         end do
      endif

      X(1) = 0
      do ii = 2, n
         X(ii) = ieor(X(ii-1), V(C(ii-1)))
      end do

      point(jj) = dble(X(n) / (2.0d0**scal))
      
   end do

end subroutine
!
! =====================================================================
!
subroutine readDirNums(filename, dims)
!
! ---------------------------------------------------------------------
! 
! Purpose: Opens file 'filname' containing the sets of directional 
!          numbers, and reads in the directional numbers
!
! Post:    Directional numbers array dirnums read in and Sobol sequence 
!          is ready to be generated using getSobolPt() subroutine
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   character(128) :: filename
   integer :: dims

   !-- local variables
   integer :: ii, jj, pos1, pos2
   character(128) :: line
   integer :: dcheck, dirnumsize

! _____________________
! begin main execution
  
   ! allocate arrays
   allocate(dirnums(dims,18))
   allocate(dnsize(dims))
   allocate(aval(dims))

   ! may want to check if this unit is already open elsewhere...   
   open(unit=dnunit, &
        file=filename(1:len_trim(filename)), &
        status='unknown', &
        form='formatted', &
        iostat=ierr)
   if (ierr /= 0) then
      write(*,*) 'Error in readDirNums :: open failed'
   endif
   
   ! read from the file
   read (dnunit, *) ! skip first line
   do ii = 2, dims
      read (unit=dnunit, fmt='(i8, i8, i8, a128)', iostat=ierr) &
           dcheck, dnsize(ii), aval(ii), line

      ! tokenize the line
      pos1 = 1
      do jj = 1, dnsize(ii)
         pos2 = index(line(pos1:), " ") + pos1 - 1
         read(unit=line(pos1:pos2), fmt='(i8)') dirnums(ii,jj)
         pos1 = pos2 + 1 
      end do
      
   end do
 
   close(dnunit)
 
end subroutine
!
! ===================================================================
!
end module

