#ifndef SNOPTCMEX
#define SNOPTCMEX

#pragma once

#ifndef mex_h
#include "mex.h"
#endif

#ifndef F2C_INCLUDE
#include "f2c.h"
#endif

/* sncmxInit() is called on first entry to the mex gateway.   */
/* It sets all optional parameters as "undefined" and         */
/* hence must be called prior to setting SNOPT options.       */
/* Must NEVER be called more than once.                       */
void     sncmxInit ();

/* sncmxAlloc() allocates memory once the problem dimensions  */
/* are known.                                                 */
void     sncmxAlloc( integer neF, integer n, integer nxname,
		     integer nfname, integer neA, integer neG );

/* Functions for setting and getting options */
integer  sncmxGeti ( char option[] );
integer  sncmxGet  ( char option[] );
integer  sncmxSet  ( char option[] );
integer  sncmxSeti ( char option[], integer iopt );
integer  sncmxSetr ( char option[], double  ropt );
integer  sncmxSpecs( char specsfile[] );

void     sncmxJac  ( int nlhs, mxArray *plhs[], int nrhs,
		     const mxArray *prhs[] );
void     sncmxSnopt( int nlhs, mxArray *plhs[], int nrhs,
		     const mxArray *prhs[], integer advancedCallbacks );
void     sncmxFree ( );

integer  sncmxGetksnoptValue ( char *ksnoptname );
void     sncmxSingleStringArg( int nlhs, mxArray *plhs[], int nrhs,
                               const mxArray *prhs[], int what );

extern FILE   *snPrintFile, *snSummaryFile, *snSpecsFile;

extern integer snSpecsUnit, snPrintUnit, snSummaryUnit;

extern int     callType;
extern int     printFileIsOpen, summaryFileIsOpen, screenIsOn;

extern int     snlog_  ( );
extern int     snlog2_ ( );
extern int     sqlog_  ( );
extern int     snabort_( );
#endif
