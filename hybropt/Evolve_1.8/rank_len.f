
      subroutine rank_len(nchrom,nobfun,fit,irank,i_mnx)
      
      dimension fit(nobfun,nchrom)
      dimension irank(nchrom),ir(nchrom)

      call setv_i(nchrom,1,ir)

      icnt = 1

      do nc = 1,nchrom-1
         do ic = nc+1,nchrom
            info1 = 0
            info2 = 0
            do no = 1,nobfun
               if (fit(no,ic) .le. fit(no,nc) .and. i_mnx.eq.1)then
                  info1 = info1+1 
               elseif (fit(no,ic) .ge. fit(no,nc) .and. i_mnx.eq.0)then
                  info1 = info1+1 
               endif
               if (fit(no,ic) .lt.  fit(no,nc) .and. i_mnx.eq.1) then
                  info2 = 1
               elseif (fit(no,ic) .gt. fit(no,nc) .and. i_mnx.eq.0) then
                  info2 = 1
               endif
            enddo
            if (info1 .eq. nobfun .and. info2 .eq. 1 )then
               if (ir(ic) .eq. ir(nc))then
                  ir(ic) = ir(nc)+1
               endif
            endif
         enddo
      enddo

      call cpyv_i(nchrom,ir,irank)

      return
      end
