c-----------------------------------------------------------------------
c     -- residuals at boundary interface in normal direction --
c     -- average boundary conditions --      
c     -- m. nemec,  july 2001 --
c-----------------------------------------------------------------------
      subroutine bcravg( is1, is2, jmax1, kmax1, q1, xyj1, press1, s,
     &     js, je, kbegin1, kend1, jmax2, kmax2, q2, xyj2, press2,
     &     kbegin2, kend2, turre1, turre2, rsa, turbulnt)

      implicit none

      logical turbulnt

      integer is1,is2,jmax1,kmax1,js,je,kbegin1,kend1,jmax2,kmax2
      integer kbegin2,kend2,k1,k2,k3,if1,if2,j,n,j2
      double precision q1(jmax1,kmax1,4),   xyj1(jmax1,kmax1)
      double precision q2(jmax2,kmax2,4),   xyj2(jmax2,kmax2)
      double precision press1(jmax1,kmax1), press2(jmax2,kmax2)
      double precision turre1(jmax1,kmax1), turre2(jmax2,kmax2)
      double precision s(jmax1,kmax1,4),    rsa(jmax1,kmax1)

      if (is1.eq.1) then
         k1 = kbegin1
         k2 = kbegin1 + 1
      else
         k1 = kend1 
         k2 = kend1 - 1
      end if

      if (is2.eq.1) then
         k3 = kbegin2 + 1
      else
         k3 = kend2 - 1
      endif

      if (is1.eq.is2) then
         if1 = 1
         if2 = -1
      else
         if1 = 0
         if2 = 1
      endif

      do j = js,je
         j2  = if1*(jmax2 + 1) + if2*j

         do n = 1,3
            s(j,k1,n) = -( xyj1(j,k1)*q1(j,k1,n) - 0.5*( xyj1(j,k2)
     &           *q1(j,k2,n) + xyj2(j2,k3)*q2(j2,k3,n) ))
         end do

         s(j,k1,4) = -( press1(j,k1)*xyj1(j,k1) - 0.5*(press1(j,k2)
     &        *xyj1(j,k2) + press2(j2,k3)*xyj2(j2,k3) ))
      end do

c     -- s-a model: nu average  --
      if (turbulnt) then
         do j = js,je
            j2  = if1*(jmax2 + 1) + if2*j
            rsa(j,k1) = -( turre1(j,k1) - 0.5*(turre1(j,k2) +
     &           turre2(j2,k3) ) )  
         end do
      end if
      
      return
      end                       !bcravg
