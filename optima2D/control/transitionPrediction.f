      ! ================================================================
      ! ================================================================
      ! 
      subroutine transitionPrediction(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     xTransPredict,xConvergeTP,jConvergeTP)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will compute the boundary layer
      !   properties and transition criteria necessary to carry out
      !   laminar-turbulent transition prediction on both the upper and
      !   lower surfaces of a single-element airfoil.
      !
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif



      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"


      ! _______________________
      ! declare input variables
      integer
     |     jdim,
     |     kdim,
     |     ndim,
     |     jConvergeTP(2)
      double precision
     |     q(jdim,kdim,ndim),
     |     xy(jdim,kdim,4),
     |     xyj(jdim,kdim),
     |     x(jdim,kdim),
     |     y(jdim,kdim),
     |     fmu(jdim,kdim),
     |     vort(jdim,kdim),
     |     turmu(jdim,kdim),
     |     xTransPredict(2),
     |     xConvergeTP(2)

      ! ______________________________
      ! declare input/output variables
      ! -- none (changes made to global variables)

      ! _____________________________________________
      ! declare blProperties() input/output variables
      double precision 
     |     dstar(jdim),
     |     theta(jdim),
     |     Hfactor(jdim),
     |     idstar(jdim),
     |     itheta(jdim),
     |     iHfactor(jdim),
     |     Re_x(jdim),
     |     Re_dstar(jdim),
     |     Re_theta(jdim),
     |     Pohlhausen(jdim)

      ! --------------------------------
      ! MAIN TRANSITION PREDICTION BEGIN
      ! --------------------------------

      ! ______________________________________
      ! Do for each surface (upper=1, lower=2)
      do surfaceBL = 1,2 

         ! ___________________________________________________________
         ! step 1) initialize global variables (flags & BL properties)
         call initializeTP(
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu)

         ! ________________________________
         ! step 2) find boundary layer edge
         if ( blEdgeMethod.eq.0 ) then ! 0 = cycle through methods
            do blEdgeMethod = 1,3
               call blEdge(
     &           jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu)
            end do
         else
            call blEdge(
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu)

         end if ! blEdgeMethod
         ! _________________________________________
         ! step 3) compute boundary layer properties
         call blProperties(
              !-- input
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
              !-- in/out
     &        dstar, theta, Hfactor, idstar, itheta, iHfactor,
     &        Re_x, Re_dstar, Re_theta, Pohlhausen)

         ! _______________________________________________
         ! step 4) compute and assess transition criteria
         call transitionCriteria(
              !-- input
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &        dstar, theta, Hfactor, iHfactor, Re_x, Re_dstar, Re_theta,
     &        Pohlhausen, 
              !-- in/out
     &        xTransPredict,xConvergeTP,jConvergeTP)

      end do

      ! ------------------------------
      ! MAIN TRANSITION PREDICTION END
      ! ------------------------------

      return
      end subroutine transitionPrediction

      

      ! ================================================================
      ! ================================================================
      ! 
      subroutine transitionCriteria(
           !-- input
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     dstar, theta, Hfactor, iHfactor, Re_x, Re_dstar, Re_theta,
     &     Pohlhausen,
           !-- in/out
     &      xTransPredict,xConvergeTP,jConvergeTP)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will compute and assess the various
      ! transition criteria Note that
      !   the transition points are moved/updated in the subroutine
      !   transitionUpdatePoints() called in flow_solve.
      !
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! declare input variables
      integer
     |     jdim,
     |     kdim,
     |     ndim,
     |     jConvergeTP(2)
      double precision
     |     q(jdim,kdim,ndim),
     |     xy(jdim,kdim,4),
     |     xyj(jdim,kdim),
     |     x(jdim,kdim),
     |     y(jdim,kdim),
     |     fmu(jdim,kdim),
     |     vort(jdim,kdim),
     |     turmu(jdim,kdim),
     |     dstar(jdim),
     |     theta(jdim),
     |     Hfactor(jdim),
     |     iHfactor(jdim),
     |     Re_x(jdim),
     |     Re_dstar(jdim),
     |     Re_theta(jdim),
     |     Pohlhausen(jdim),
     |     xTransPredict(2),
     |     xConvergeTP(2)

      ! _____________________________________________________
      ! declare output variables from getCurvilinearVariables
      double precision
     |     vel(jdim,kdim),
     |     uu(jdim,kdim),
     |     vv(jdim,kdim),
     |     pp(jdim,kdim),
     |     rho(jdim,kdim),
     |     aa(jdim,kdim),
     |     MM(jdim,kdim),
     |     dyy(jdim,kdim)


      ! _____________________
      ! Function declarations
      double precision trapezoidalIntegration
      double precision linearInterp

      ! _______________________
      ! declare local variables
      logical
     |     foundTRShift,
     |     checkFlowSep,
     |     checkEddyVisc,
     |     foundFlowSep,
     |     foundEddyVisc
      integer
     |     j,
     |     k,
     |     sf,
     |     kd,
     |     jm1,
     |     jp1,
     |     jCR,
     |     jCRm1,
     |     jTR,
     |     jTRm1,
     |     jPt,
     |     jPtm,
     |     jTRS,
     |     jTRSm1,
     |     jFS,
     |     jFSm1,
     |     jEV,
     |     jEVm1,
     |     jMonotone,
     |     jpmin
      double precision
     |     ds(jdim),
     |     s_arc(jdim),
     |     delx,
     |     dely,
     |     dpdx,
     |     Hi(jdim),
     |     Ph,
     |     polyPh(jdim),
     |     sumPh(jdim),
     |     MeanPh(jdim),
     |     Re_theta_CR(jdim),
     |     Re_theta_CR_found,
     |     m1,
     |     m2,
     |     b1,
     |     b2,
     |     x_ratio,
     |     PhCR,
     |     xCR,
     |     sCR,
     |     Re_theta_TR(jdim),
     |     Re_theta_TR_found,
     |     Re_theta_TR_project,
     |     Re_theta_shift,
     |     xTR,
     |     sTR,
     |     numerPh,
     |     denomPh,
     |     Re_th_TRSHift,
     |     Re_ratio,
     |     xTRS,
     |     sTRs,
     |     cf_check(jdim),
     |     cf_ratio,
     |     ev_ratio,
     |     xFlowSep,
     |     sFlowSep,
     |     xEddyVisc,
     |     sEddyVisc,
     |     avg_turmu(jdim),
     |     m_tmp,
     |     Re_theta_shift_tmp,
     |     xTRS_tmp,
     |     pmin

      ! ___________________________________
      ! declare output variables from MATTC

      double precision
     &     Nts(jdim)

      ! _________________________
      ! initialize values to zero
      select case (surfaceBL)
      case (1) ! upper surface
         sf = surfaceBL
         do j=1,jdim
            ds(j)          = 0d0
            s_arc(j)       = 0d0
            Hi(j)          = 0d0
            polyPh(j)      = 0d0
            sumPh(j)       = 0d0
            MeanPh(j)      = 0d0
            Re_theta_CR(j) = 0d0
            Re_theta_TR(j) = 0d0
            Nts(j)         = 0d0
         end do
      case (2) ! lower surface
         sf = surfaceBL
         do j=jblSP,jtail1, jblinc
            ds(j)          = 0d0
            s_arc(j)       = 0d0
            Hi(j)          = 0d0
            polyPh(j)      = 0d0
            sumPh(j)       = 0d0
            MeanPh(j)      = 0d0
            Re_theta_CR(j) = 0d0
            Re_theta_TR(j) = 0d0
            Nts(j)         = 0d0
         end do
      case default
         stop ' transitionCriterion: surfaceBL assigned wrong value '
      end select ! surfaceBL
      Re_theta_CR_found = 0d0
      Re_theta_TR_found = 0d0
      jCR  = -1
      xCR  = -1
      sCR  = -1
      jTR  = -1
      xTR  = -1
      sTR  = -1
      jTRS = -1
      xTRS = -1
      sTRS = -1
      jFS  = -1
      jEV  = -1
      foundCR       = .false.
      foundTR       = .false.
      foundFlowSep  = .false.
      foundEddyVisc = .false.
      !RR: add to input parameters
      !if (transPredictMethod .eq. 7) then
      !   checkFlowSep = .false.
      !else
      !   checkFlowSep = .true.
      !end if
      checkFlowSep  = .true.
      checkEddyVisc = .true.

      s_arc(jblSP) = 0d0
      ds(jblLE)    = 0d0

      ! _________________
      ! Compute arclength
      ! -- Using stag pt as starting point to calc arclength
      do j=jblSP+jblinc,jblEnd,jblinc 

         jm1 = j - jblinc
         delx = x(j,1)-x(jm1,1)
         dely = y(j,1)-y(jm1,1)
         ds(j) = dsqrt(delx*delx + dely*dely)
         s_arc(j) = s_arc(jm1) + ds(j)

      end do ! j

      ! _____________________________________
      ! Compute Cf for flow separation checks
      call skinfrct(jdim,kdim,q,x,y,xy,cf_check)

      if ( checkFlowSep .or. checkEddyVisc ) then

         do j = jblStart, jblEnd, jblinc
    
            ! ____________________________________________________
            ! Check for flow separation and store separation point
            ! RR: fix this: place inside other do loop (later)
            if (checkFlowSep) then
               if ( j.gt.jblStart ) then
                  if ( cf_check(j).lt.0d0 .and. .not.foundFlowSep ) then
                     foundFlowSep = .true.
                     jFS = j
                  end if 
                  ! RR: fix this later x > 0.02 
               else if (j.lt.jblStart .and.  x(j,1) .gt. 0.02 ) then
                  if ( cf_check(j).gt.0d0 .and. .not.foundFlowSep ) then
                     foundFlowSep = .true.
                     jFS = j
                  end if
               end if 
            end if
    
            ! ______________________________________________
            ! Check for eddy viscosity in the boundary layer 
            if (checkEddyVisc) then
               avg_turmu(j) = 0d0
               if ( foundBLEdge(j) ) then
                  do k = 1, k_deltaBL(j)
                     avg_turmu(j) = avg_turmu(j) + turmu(j,k)
                  end do
               else 
                  do k = 1, kblEnd
                     avg_turmu(j) = avg_turmu(j) + turmu(j,k)
                  end do
               end if
               avg_turmu(j) = 2*avg_turmu(j)/k_deltaBL(j)
               if (avg_turmu(j).gt.0.1 .and. .not.foundEddyVisc) then
                  foundEddyVisc = .true. 
                  jEV = j
                  write (34, *) 'avg_turmu(',j,') = ', avg_turmu(j)
               end if
            end if

         end do ! j

      end if ! checkFlowSep .and. checkEddyVisc

      if (foundFlowSep) then
         ! Linearly interpolate for exact separation point
         jFSm1 = jFS - jblinc
         cf_ratio = -cf_check(jFSm1) / (cf_check(jFS)-cf_check(jFSm1))
         xFlowSep = x(jFSm1,1)
     &        + cf_ratio*(x(jFS,1)-x(jFSm1,1))
         sFlowSep = s_arc(jFSm1)
     &        + cf_ratio*(s_arc(jFS)-s_arc(jFSm1))
         write (34, *) 'sf=',sf, 'xFlowSep=',xFlowSep
         
      end if 
      
      if (foundEddyVisc) then
!         ! Linearly interpolate for exact eddy viscosity point
!         jEVm1 = jEV - jblinc
!         ev_ratio = (0.1-turmu(jEVm1,60))
!     &        / (turmu(jEV,60)-turmu(jEVm1,60))
!         xEddyVisc = x(jEVm1,1)
!     &        + ev_ratio*(x(jEV,1)-x(jEVm1,1))
!         sEddyVisc = s_arc(jEVm1)
!     &        + ev_ratio*(s_arc(jEV)-s_arc(jEVm1))
         xEddyVisc = x(jEV,1)
         sEddyVisc = s_arc(jEV)
         write (34, *) 'sf=',sf, 'xEddyVis=',xEddyVisc
         
      end if 






      select case (transPredictMethod)

      case (1:3)

         call AHD_Granville(
              !-- input
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &        dstar, theta, Hfactor, iHfactor, Re_x, Re_dstar, Re_theta,
     &        Pohlhausen, s_arc,
              !-- in/out
     &        Hi, polyPh, Re_theta_CR, jCR, xCR, sCR, MeanPh,
     &        Re_theta_TR, jTR)

      case (4:5)

         call Michel_HRx(
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &        dstar, theta, Hfactor, Re_x, Re_dstar, Re_theta,
     &        Pohlhausen, s_arc)

      case(6)

         call MATTC(
           ! input
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     dstar, theta, Hfactor, Re_x, Re_dstar, Re_theta,
     &     Pohlhausen, s_arc,
           ! in/out
     &     Nts, jTR)

      case(7)

         call eN_Drela(
           ! input
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     dstar, theta, Hfactor, iHfactor, Re_x, Re_dstar, Re_theta,
     &     Pohlhausen, s_arc,
           ! in/out
     &     Hi, polyPh, Re_theta_CR, jCR, xCR, sCR, MeanPh,
     &     Nts, jTR)

!         do j =jblSP, jblEnd, jblinc
!            write (*,*) j, x(j,1), Nts(j), jTR
!         end do


      case default
         stop ' transPredictMethod assigned wrong value '
      end select ! transPredictMethod
      



      if (foundTR) then

         ! set error flag: transition point successfully found
         errorTrackerTP = 0
         jTRm1 = jTR - jblinc

         select case (transPredictMethod)
         case (1:3)

            ! Find intersection of Re_theta and Re_theta_TR curves occuring
            ! between jTR and jTR minus one.
            ! Line Re_theta
            m1 = (Re_theta(jTR)-Re_theta(jTRm1))/(x(jTR,1)-x(jTRm1,1)) ! slope
            b1 = Re_theta(jTRm1)-m1*x(JTRm1,1) ! y-intercept
            ! Line Re_theta_TR
            m2 =(Re_theta_TR(jTR)-Re_theta_TR(jTRm1)) /
     &           (x(jTR,1)-x(jTRm1,1)) ! slope
            b2 = Re_theta_TR(jTRm1)-m2*x(JTRm1,1) ! y-intercept
            ! Intersection pont
            xTR = (b2 - b1) / (m1 - m2)
            Re_theta_TR_found = m2*xTR + b2

            ! Linearly interpolate for sTR using xTR
            x_ratio = (xTR - x(jTRm1,1)) / (x(jTR,1) - x(jTRm1,1))
            sTR = s_arc(jTRm1)
     &           + x_ratio*(s_arc(jTR)-s_arc(jTRm1))

         case (4:5)
            ! nothing yet
            
         case (6:7)
            xTR = linearInterp(
     &           Nts(jTRm1),x(jTRm1,1),
     &           Nts(jTR),x(jTR,1),
     &           Ncrit)
            
            sTR = linearInterp(
     &           Nts(jTRm1),s_arc(jTRm1),
     &           Nts(jTR),s_arc(jTR),
     &           Ncrit)
         case default
            stop ' transPredictMethod assigned wrong value '
         end select ! transPredictMethod
         

         ! _______________________________________
         ! Tansition Point Shift based on Re_theta
         !
         ! The transition point predicted by the given criterion may be
         ! shifted downstream by a specified percentage of Re_theta_TR_found
         ! 
         ! Important Notes:
         ! 1. A linear extrapolation of the laminar BL's Re_theta is used
         ! 2. Re_theta tends to oscillate up and down near the transition point,
         !    which misguide the linear extrapolation
         ! 3. As such, an upstream search (moving fwd from the TP) is carried out
         !    to find the nearest monotone (i.e. smoothly increasing)
         !    segment of Re_theta from which we can linearly extrapolate
         !
         if (applyTPShift) then
            ! find nearest monotonically increasing Re_theta region
            do j = jTR-jblinc, jBLStart, -jblinc
               if ((Re_theta(j).gt.Re_theta(j-jblinc)) .and.
     &             (Re_theta(j-jblinc).gt.Re_theta(j-2*jblinc)) .and.
     &             (Re_theta(j-2*jblinc).gt.Re_theta(j-3*jblinc)) ) then
                  jMonotone = j
                  exit
               end if 
            end do ! j

            ! project Re_theta_TR_found onto extrapolation
            ! line of the laminar Re_theta using xTR
            jPt  = jMonotone
            jPtm = jMonotone - 2*jblinc
            Re_theta_TR_project = linearInterp(
     &              x(jPtm,1),Re_theta(jPTm),
     &              x(jPt,1),Re_theta(jPt),
     &              xTR)

            ! use input variable percentTPShift to compute new target
            ! Re_theta value for linear extrapolation 
            Re_theta_shift = (1d0+percentTPShift/100d0) * 
     &              Re_theta_TR_project
            ! compute new transition location based on Re_theta_shift
            xTRS = linearInterp(
     &              Re_theta(jPtm),x(jPtm,1),
     &              Re_theta(jPt),x(jPt,1),
     &              Re_theta_shift)
            sTRS = linearInterp(
     &              Re_theta(jPTm),s_arc(jPtm),
     &              Re_theta(jPt),s_arc(jPt),
     &              Re_theta_shift)

            m_tmp = (Re_theta(jPT) - Re_theta(jPTm)) /
     &               (x(jPt,1) - x(jPTm,1))
            Re_theta_shift_tmp = (1d0+25d0/100d0) * 
     &              Re_theta_TR_project
            xTRS_tmp = linearInterp(
     &              Re_theta(jPtm),x(jPtm,1),
     &              Re_theta(jPt),x(jPt,1),
     &              Re_theta_shift_tmp)

            do j = jTR, jblEnd, jblinc
               if (x(j,1).ge.xTRS) then
                  jTRS = j
                  exit
               end if
            end do

         else 
            ! do not shift the TPs 
            jTRS = jTR
            xTRS = xTR
            sTRS = sTR
         end if ! applyTPShift

         ! distance between previous (forced) and current (predicted) TP
         jConvergeTP(sf) = jblinc*(jTransForced(sf)-jTRS)
         xConvergeTP(sf) = xTransForced(sf)-xTRS

         ! RR: debug write statements
         write (34,*)
         write (34,*)
         write (34,*) 'Before final Checks'
         write (34,*) '-----------------'
         write (34,*) 'surface         =', sf
         write (34,*) '-----------------'
         write (34,*) 'jTransForced    =', jTransForced(sf)
         write (34,*) 'jTR             =', jTR
         write (34,*) 'jTRS            =', jTRS
         write (34,*) 'jConvergeTP     =', jConvergeTP(sf)
         write (34,*) '-----------------', sf
         write (34,*) 'xTransForced    =', xTransForced(sf)
         write (34,*) 'xTR             =', xTR
         write (34,*) 'xTRS            =', xTRS
         write (34,*) 'xConvergeTP     =', xConvergeTP(sf)
         write (34,*)
         write (34,*)

      else if (foundCR .and. .not.foundTR) then
         ! set error flag: transition point not found
         errorTrackerTP = 3

         write(34,*)
         write(34,*)
         write(34,*) 'WARNING: TRANSITION POINT NOT FOUND ON SURFACE =',
     &        sf
         write(34,*)
         write(34,*)

      end if ! foundTR


      ! __________________
      ! BEGIN FINAL CHECKS

      ! The order of the following checks is critical and requires
      ! special attention.

      ! ____________________________________________________________
      ! CHECK #1: Check if predicted point is downstream of previous
      ! xTransForced point, and set errorTrackerTP flag accordingly.
      if (foundTR .and. (xConvergeTP(sf).lt.0) ) then
         if (checkForVanishingBubble(sf)) then
            ! If we're here it means that the separation bubble has
            ! vanished by moving the TP forward to the separation point
            ! and, on this subsequent iteration, no TP has been detected
            ! UPSTREAM of the previous iteration's separation point.
            errorTrackerTP = 5
         else 
            errorTrackerTP = 1
         end if
      end if

      ! __________________________________________________________
      ! CHECK #2: Check if eddy viscosity exists in BL upstream of
      ! predicted or critical points
      if (foundEddyVisc .and. foundTR) then
         ! RR: fix this later: 0.1
         if ( (xEddyVisc .lt. (xTR-0.1)) .and. (iterTP.ne.1) ) then
            ! set error flag: eddy viscosity found
            errorTrackerTP = 6
            jTR  = jEV
            xTR  = xEddyVisc
            sTR  = sEddyVisc
            jTRS = jEV
            xTRS = xEddyVisc
            sTRS = sEddyVisc
         end if 
      else if (foundEddyVisc .and. foundCR) then
         if (xEddyVisc .lt. xCR) then
            ! set error flag: eddy viscosity found
            errorTrackerTP = 6
            jTR  = jEV
            xTR  = xEddyVisc
            sTR  = sEddyVisc
            jTRS = jEV
            xTRS = xEddyVisc
            sTRS = sEddyVisc
         end if
      else if ( (foundEddyVisc .and. .not.foundCR) .or.
     &          (foundEddyVisc .and. .not.foundTR) ) then
         if (xEddyVisc .lt. xCR) then
            ! set error flag: eddy viscosity found
            errorTrackerTP = 6
            jTR  = jEV
            xTR  = xEddyVisc
            sTR  = sEddyVisc
            jTRS = jEV
            xTRS = xEddyVisc
            sTRS = sEddyVisc
         end if
      end if

      ! _____________________________________________________________
      ! CHECK #3: Check if flow separated upstream of predicted point
      if (foundFlowSep .and. foundTR) then
         if (xFlowSep .lt. xTR) then
            if (checkForVanishingBubble(sf)) then
               ! If we're here, it means that this is the second
               ! consecutive iteration in which laminar separation
               ! has been detected upstream of the predicted TP.
               errorTrackerTP = 4
            else
               ! set error flag: laminar flow separation found. This
               ! would be the first iteration in which a separation
               ! bubble has been detected. Set transition point to
               ! separation point.
               errorTrackerTP = 4
               jTR = jFS
               xTR = xFlowSep
               sTR = sFlowSep
               jTRS = jFS
               xTRS = xFlowSep
               sTRS = sFlowSep
            end if ! checkForVanishingBubble
         end if ! xFlowSep .lt. xTR
      else if (foundFlowSep .and. foundCR) then
         if (xFlowSep .lt. xCR) then
            ! set error flag: laminar flow separation found
            errorTrackerTP = 4
            jTR = jFS
            xTR = xFlowSep
            sTR = sFlowSep
            jTRS = jFS
            xTRS = xFlowSep
            sTRS = sFlowSep
         end if ! xFlowSep .lt. xCR
      else if ( (foundFlowSep .and. .not.foundCR) .or.
     &          (foundFlowSep .and. .not.foundTR) ) then
            errorTrackerTP = 4
            jTR = jFS
            xTR = xFlowSep
            sTR = sFlowSep
            jTRS = jFS
            xTRS = xFlowSep
            sTRS = sFlowSep
      end if ! foundFlowSep .and. foundTR

      ! ______________________________________
      ! CHECK #4. Check for fully laminar flow
      if (checkForFullyLam(sf)) then
         if (.not.foundTR .or. xTRS.gt.aftLimitTP) then
            ! Set flag for fully laminar flow on given surface
            errorTrackerTP = 7
         end if
      end if

      ! ________________
      ! END FINAL CHECKS











      ! ______________________________________________________
      ! Store information in global variables for each surface
      errorFlagTP(sf) = errorTrackerTP
      jTransPredict(sf) = jTRS
      xTransPredict(sf) = xTRS
      xTransPredictNoshift(sf) = xTR

      ! __________________
      ! Write data to file
      if (write_TP) then
         call write_transition(
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &        dstar, theta, iHfactor, Re_dstar, Re_theta, Pohlhausen,
     &        polyPh, Hi, Re_theta_CR, MeanPh, Re_theta_TR, jCR, xCR,
     &        sCR, jTR, xTR, sTR, jTRS, xTRS, sTRS) 
      end if

      ! _________________________________________
      ! Find and report minimum pressure location
      call getCurvilinearVariables(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     vel, uu, vv, pp, rho, aa, MM, dyy)

      pmin = 100.d0
      do j = jblSP, jblEnd, jblinc
         if ( pp(j,1) .lt. pmin) then
            pmin = pp(j,1)
            jpmin = j
         end if
      end do ! j
      ! write statmentes
      write (34,*)
      write (34,*) 'Min Pressure at:'
      write (34,*) 'sf    = ', surfaceBL
      write (34,*) 'jpmin = ', jpmin
      write (34,*) 'xpmin = ', x(jpmin,1)
      write (34,*) 'pmin  = ', pmin
      write (34,*) 

      
      


      return
      end subroutine transitionCriteria
