c-----------------------------------------------------------------------
c     -- implicit thin-layer viscous terms in y-direction --
c     -- differentiation of the viscous flux vector w.r.t. Q^ --
c     -- m. nemec, sept. 2001 --
c     -- heavily modified by markus rumpfkeil -
c     -- date: may. 2006
c     Note: we need -Re^(-1)*d_eta Fhat
c-----------------------------------------------------------------------
      subroutine impvis(jmax, kmax, nmax, ibc1,ibc2,ibc3,ibc4, kminbnd,
     &     kmaxbnd,jminbnd,jmaxbnd,jbegin,jend,jlow,jup,k1,k2,xy,xyj,
     &     q, turre, fmu, turmu, press, sndsp, chi, dchi, fv1, dfv1,
     &     dmul, indx, as, pa, icol, precmat, jacmat, re, gamma, gami,
     &     tinf)

      implicit none

#include "../include/parms.inc"
#include "../include/visc.inc"
#include "../include/sam.inc"

      integer jmax, kmax, nmax, ibc1,ibc2,ibc3,ibc4, kminbnd
      integer kmaxbnd,jminbnd,jmaxbnd,jbegin,jend,jlow,jup,k1,k2
      integer kbegin,kend,kup,k,j,kp,n,js,je,ks,ke,m,ii,ij,nm1,km1,itmp
      double precision q(jmax,kmax,4),xy(jmax,kmax,4),xyj(jmax,kmax)
      double precision fmu(jmax,kmax),turmu(jmax,kmax) 
      double precision press(jmax,kmax),sndsp(jmax,kmax)
      double precision pa(*),as(*),turre(jmax,kmax),dmul(jacb,jmax,kmax)
      integer indx(jmax,kmax),icol(*)
      logical precmat,jacmat

      double precision re,gamma,gami,tinf,hre,t1,t2,t3,r1,hfmu,ah1,ah2
      double precision ah3,ah4,ueta,veta,uavg,vavg,S1,S2,g1,g2,dmuda
      double precision dadq1,dadq2,dadq3,dadq4,c2b,c2bp,t4,t5,tnu,rho,t0
      double precision chi3,rp1,up1,u1,vp1,v1,c2eta,vflux1,vflux2,vflux3

      double precision dj(jacb,3,maxjb,maxkb), dpj(jacb,3,maxjb,maxkb)
      double precision q2(maxjb,maxkb,4),      a1(maxjb,maxkb)
      double precision a2(maxjb,maxkb),        a3(maxjb,maxkb)
      double precision a4(maxjb,maxkb),        dmut(jacb,maxjb,maxkb)

      double precision chi(jmax,kmax), dchi(5,jmax,kmax)
      double precision fv1(jmax,kmax), dfv1(jmax,kmax)

c     prlam   =  laminar prandtl number  = .72                      
c     prturb  =  turbulent prandtl number = .90                     
c     prlinv  =  1./(laminar prandtl number)                        
c     prtinv  =  1./(turbulent prandtl number)                      
c     f13     =  1/3                                                
c     f43     =  4/3                                                
c     hre     =  1/2 * reynolds number                           
c     fmu     =  laminar viscosity
c     turmu   =  turbulent viscosity      

      kbegin = kminbnd
      kend   = kmaxbnd
      kup    = kmaxbnd-1

      hre = 1.0/(2.0*re)
      
c     --  ^  ^ --
c     -- dS/dQ -- 
c     -- lam. and turb. viscosities are assumed to be constant --

c     -- calculate alfas and non-conserv. variables --
      do k = kbegin,kend                                              
         do j = jbegin,jend 
c     t1 = eta_x **2
c     t2 = eta_x*eta_y
c     t3 = eta_y **2          
            t1      = xy(j,k,3)*xy(j,k,3)                               
            t2      = xy(j,k,3)*xy(j,k,4)                               
            t3      = xy(j,k,4)*xy(j,k,4)                               
            r1      = hre/xyj(j,k)
            
            a1(j,k) = r1*( f43*t1 + t3    ) 
            a2(j,k) = r1*(     t2 * f13   ) 
            a3(j,k) = r1*( t1     + f43*t3)                        
            a4(j,k) = r1*( t1     + t3    )

            r1   = 1.0/q(j,k,1)                                        
            q2(j,k,1) = r1                                           
            q2(j,k,2) = r1*q(j,k,2)                                  
            q2(j,k,3) = r1*q(j,k,3)                                  
            q2(j,k,4) = r1*q(j,k,4) - (q2(j,k,2)**2 +q2(j,k,3)**2)
         end do
      end do

c     -- entries for viscous Jacobian, treating mu as constant -- 
      do k = kbegin,kup
         kp = k + 1
         do j = jlow,jup

c     -- extrapolate fmu to get it at 1/2 nodes --
            hfmu = 0.5*(fmu(j,k)+fmu(j,kp))
c     -- laminar + turbulent viscosity at 1/2 nodes --
            t1 = hfmu + turmu(j,k)
c     -- mu_l/Pr_l + mu_t/Pr_t --
            t2 = hfmu*prlinv + turmu(j,k)*prtinv

c     -- evaluate alfas at k+1/2 factor 1/2 already in hre--
            ah1 = ( a1(j,k) + a1(j,kp) )*t1
            ah2 = ( a2(j,k) + a2(j,kp) )*t1
            ah3 = ( a3(j,k) + a3(j,kp) )*t1
            ah4 = ( a4(j,k) + a4(j,kp) )*t2*gamma

            ueta  = q2(j,kp,2)-q2(j,k,2)  
            veta  = q2(j,kp,3)-q2(j,k,3)   
            uavg  = 0.5d0*(q2(j,kp,2)+q2(j,k,2))
            vavg  = 0.5d0*(q2(j,kp,3)+q2(j,k,3))

            dj(1,1,j,k) = -( ah1*q2(j,k,2)  + ah2*q2(j,k,3)) *q2(j,k,1)
            dpj(1,1,j,k)=  ( ah1*q2(j,kp,2) + ah2*q2(j,kp,3))*q2(j,kp,1)
            dj(2,1,j,k) = ah1*q2(j,k,1)                              
            dpj(2,1,j,k)=-ah1*q2(j,kp,1)
            dj(3,1,j,k) = ah2*q2(j,k,1)
            dpj(3,1,j,k)=-ah2*q2(j,kp,1)
            dj(4,1,j,k) = 0.0d0
            dpj(4,1,j,k)= 0.0d0
            dj(1,2,j,k) = -( ah2*q2(j,k,2) + ah3*q2(j,k,3))  *q2(j,k,1)
            dpj(1,2,j,k)=  ( ah2*q2(j,kp,2) + ah3*q2(j,kp,3))*q2(j,kp,1)
            dj(2,2,j,k) = ah2*q2(j,k,1) 
            dpj(2,2,j,k)=-ah2*q2(j,kp,1)                            
            dj(3,2,j,k) = ah3*q2(j,k,1)
            dpj(3,2,j,k)=-ah3*q2(j,kp,1)                            
            dj(4,2,j,k) = 0.0d0 
            dpj(4,2,j,k)= 0.0d0

            S1=ah1*ueta+ah2*veta
            S2=ah2*ueta+ah3*veta
                                         
            dj(1,3,j,k) = ( -ah4*q2(j,k,4)+0.5d0*S1*q2(j,k,2)  
     &         +0.5d0*S2*q2(j,k,3) )*q2(j,k,1)
     &         + uavg*dj(1,1,j,k) + vavg*dj(1,2,j,k)

            dpj(1,3,j,k) =( ah4*q2(j,kp,4)+0.5d0*S1*q2(j,kp,2)  
     &         +0.5d0*S2*q2(j,kp,3) )*q2(j,kp,1) 
     &         + uavg*dpj(1,1,j,k) + vavg*dpj(1,2,j,k)    

            dj(2,3,j,k) = -ah4*q2(j,k,2)*q2(j,k,1)+uavg*dj(2,1,j,k)+
     &         vavg*dj(2,2,j,k)  - 0.5d0*S1 * q2(j,k,1)         
            dpj(2,3,j,k) =ah4*q2(j,kp,2)*q2(j,kp,1)+uavg*dpj(2,1,j,k)+
     &         vavg*dpj(2,2,j,k) - 0.5d0*S1 * q2(j,kp,1)
     
            dj(3,3,j,k) = -ah4*q2(j,k,3)*q2(j,k,1)+uavg*dj(3,1,j,k)+
     &         vavg*dj(3,2,j,k) - 0.5d0*S2 * q2(j,k,1) 
            dpj(3,3,j,k) = ah4*q2(j,kp,3)*q2(j,kp,1)+uavg*dpj(3,1,j,k)+
     &         vavg*dpj(3,2,j,k)- 0.5d0*S2 * q2(j,kp,1) 
     
            dj(4,3,j,k) =  ah4*q2(j,k,1)                              
            dpj(4,3,j,k) = -ah4*q2(j,kp,1)                           
         end do
      end do

      if (nmax.eq.5) then
         do k = kbegin,kup
            do j = jlow,jup
               do n = 1,3              
                  dj(5,n,j,k)  = 0.0d0
                  dpj(5,n,j,k) = 0.0d0
               end do
            end do
         end do
      end if

c     -- contribution from lam. and turb. viscosities --
      g1  = 1.0/gami
      g2  = gamma*gami

      do k = kbegin,kend
         do j = jbegin,jend  
            do n = 1,nmax
               dmut(n,j,k) = 0.0
            end do
         end do
      end do

c      write (*,*) 'starting dmul'
c     -- compute laminar d(fmu)/dQ vector --
      c2b = 198.6/tinf                                                  
      c2bp = c2b + 1.0
      do k = kbegin,kend
         do j = jbegin,jend

            t1 = sndsp(j,k)*sndsp(j,k)
c            write (*,*) j,k,sndsp(j,k)
            t2 = t1 + 3.0*c2b
            t3 = 1.0/(t1 + c2b)
            dmuda = c2bp*t1*t2*t3*t3

            t1 = 0.5/sndsp(j,k)
            t2 = q(j,k,2)*q(j,k,2)
            t3 = q(j,k,3)*q(j,k,3)
            t4 = 1.0/q(j,k,1)
            t5 = t4*t4
            dadq1 = g2*t5*( (t2+t3)*t4 - q(j,k,4) )
            dadq2 = - g2*q(j,k,2)*t5
            dadq3 = - g2*q(j,k,3)*t5
            dadq4 = g2*t4

            dmul(1,j,k) = dmuda*dadq1*t1
            dmul(2,j,k) = dmuda*dadq2*t1
            dmul(3,j,k) = dmuda*dadq3*t1
            dmul(4,j,k) = dmuda*dadq4*t1
         end do
      end do

      if (nmax.eq.5) then
         do k = kbegin,kend
            do j = jbegin,jend
               dmul(5,j,k) = 0.0
            end do
         end do
      end if

c     -- compute turbulent d(mut)/dQ vector --
      if (nmax.eq.5) then
         do k = kbegin,kend
            do j = jbegin,jend

               tnu = turre(j,k)
               rho = q(j,k,1)*xyj(j,k)
               t0 = rho*tnu
c     -- t1 = 1/mul --
               t1 = 1.0/fmu(j,k)
c     -- chi = rho*tnu/fmu  --
               chi(j,k) = t0*t1
c     -- chi3 = chi**3 --
               chi3 = chi(j,k)*chi(j,k)*chi(j,k)
c     -- t2 = 1/(chi**3+cv1**3) --
               t2 = 1.0/(chi3 + cv1_3)
               fv1(j,k) = chi3*t2
               dfv1(j,k) = 3.0*cv1_3*chi(j,k)*chi(j,k)*t2*t2

c     -- d(chi)/d(Qm) --
               dchi(1,j,k) = -t0*t1*t1*dmul(1,j,k) + tnu*t1*xyj(j,k)
               dchi(2,j,k) = -t0*t1*t1*dmul(2,j,k)
               dchi(3,j,k) = -t0*t1*t1*dmul(3,j,k)
               dchi(4,j,k) = -t0*t1*t1*dmul(4,j,k)
               dchi(5,j,k) =  rho*t1*xyj(j,k)

               dmut(1,j,k) = t0*dfv1(j,k)*dchi(1,j,k) + fv1(j,k)*tnu
     &              *xyj(j,k)
               dmut(2,j,k) = t0*dfv1(j,k)*dchi(2,j,k)
               dmut(3,j,k) = t0*dfv1(j,k)*dchi(3,j,k)
               dmut(4,j,k) = t0*dfv1(j,k)*dchi(4,j,k)
               dmut(5,j,k) = t0*dfv1(j,k)*dchi(5,j,k) + fv1(j,k)*rho
     &              *xyj(j,k)
            end do
         end do
      end if

c     -- compute derivative terms due to laminar and turbulent (sa)
c     viscosity: dmu is negative to be consistent with inviscid terms --
      do k = kbegin,kup
         kp = k + 1
         do j = jlow,jup

c     -- calculate viscous flux vector without viscosity terms --
            r1    = 1.0/q(j,k,1) 
            rp1   = 1.0/q(j,kp,1)                                       
            up1   = q(j,kp,2)*rp1                                     
            u1    = q(j,k,2)*r1                                      
            vp1   = q(j,kp,3)*rp1                                     
            v1    = q(j,k,3)*r1                                      
            ueta  = up1 - u1                                            
            veta  = vp1 - v1                                            
            c2eta = gamma*(press(j,kp)*rp1 - press(j,k)*r1)*g1

            ah1 = a1(j,kp) + a1(j,k)
            ah2 = a2(j,kp) + a2(j,k)
            ah3 = a3(j,kp) + a3(j,k)
            ah4 = a4(j,kp) + a4(j,k)

            vflux1 = ah1*ueta + ah2*veta
            vflux2 = ah2*ueta + ah3*veta
            vflux3 = 0.5*(up1 + u1)*vflux1 + 0.5*(vp1 +v1)*vflux2 

c     -- dmu/dQ terms factor 0.5d0 from averaging of mu--
            do n = 1,nmax
               dj(n,1,j,k) = dj(n,1,j,k) - 0.5d0*vflux1*(dmul(n,j,k) +
     &              dmut(n,j,k))  
               dj(n,2,j,k) = dj(n,2,j,k) - 0.5d0*vflux2*(dmul(n,j,k) +
     &              dmut(n,j,k)) 
               dj(n,3,j,k) = dj(n,3,j,k) - 0.5d0*( vflux3*(dmul(n,j,k) +
     &              dmut(n,j,k)) + ah4*c2eta*(dmul(n,j,k)*prlinv +
     &              dmut(n,j,k)*prtinv) )  

               dpj(n,1,j,k) = dpj(n,1,j,k) - 0.5d0*vflux1*(dmul(n,j,kp)
     &              +dmut(n,j,kp)) 
               dpj(n,2,j,k) = dpj(n,2,j,k) - 0.5d0*vflux2*(dmul(n,j,kp)
     &              +dmut(n,j,kp)) 
               dpj(n,3,j,k) = dpj(n,3,j,k) - 0.5d0*(vflux3*(dmul(n,j,kp)
     &              + dmut(n,j,kp)) + ah4*c2eta*(dmul(n,j,kp)*prlinv
     &              + dmut(n,j,kp)*prtinv))

               if (n.eq.5) then
                  if ( abs(dj(5,3,j,k)) .lt. 5.e-16 ) then
                     dj(5,1,j,k) = 0.0d0
                     dj(5,2,j,k) = 0.0d0
                     dj(5,3,j,k) = 0.0d0
                  end if
                  if ( abs(dpj(5,3,j,k)) .lt. 5.e-16 ) then
                     dpj(5,1,j,k) = 0.0d0
                     dpj(5,2,j,k) = 0.0d0
                     dpj(5,3,j,k) = 0.0d0
                  end if
               end if
            end do
         end do
      end do

c     -- fill sparse matrix --

c     -- preconditioner --
      if ( precmat ) then

         js = jlow
         je = jup
         ks = k1
         ke = k2
cmt ~~~
         do k = ks,ke
            km1 = k-1
            do j = js,je
               itmp = ( indx(j,k) - 1 )*nmax
               do n  = 2,4
                  nm1 = n-1 
                  ij  = itmp + n
                  ii  = ( ij - 1 )*icol(5)
                  do m = 1,nmax
                     pa(ii+icol(1)+m) = pa(ii+icol(1)+m) - 
     &                    dj(m,nm1,j,km1)
                     pa(ii+icol(2)+m) = pa(ii+icol(2)+m) + dj(m,nm1,j,k)
     &                    - dpj(m,nm1,j,km1)
                     pa(ii+icol(3)+m) = pa(ii+icol(3)+m) +
     &                    dpj(m,nm1,j,k)
                  end do
               end do
            end do
         end do
      end if

c     -- second order jacobian --
c     -- interior points --
      if ( jacmat ) then

         if (ibc2.EQ.0) then
            js = jminbnd
         else
            js = jminbnd + 2
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd - 2
         end if          
         ks = kminbnd + 2
         ke = kmaxbnd - 2

         do k = ks,ke
            km1 = k-1
            do j = js,je
               itmp = ( indx(j,k) - 1 )*nmax
               do n  = 2,4
                  nm1 = n-1 
                  ij  = itmp + n
                  ii  = ( ij - 1 )*icol(9)
                  do m =1,nmax
                     as(ii+icol(3)+m) = as(ii+icol(3)+m) -
     &                    dj(m,nm1,j,km1)
                     as(ii+icol(4)+m) = as(ii+icol(4)+m) + dj(m,nm1,j,k)
     &                    - dpj(m,nm1,j,km1)
                     as(ii+icol(5)+m) = as(ii+icol(5)+m) +
     &                    dpj(m,nm1,j,k)
                  end do
               end do
            end do
         end do

c     -- first interior boundary nodes --
         if (ibc2.EQ.0) then
            js = jminbnd
         else
            js = jminbnd + 2
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd - 2
         end if
         k = kminbnd+1
         km1 = k-1

         do j = js,je
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 2,4
               nm1 = n-1 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,nmax
                  as(ii+icol(2)+m) = as(ii+icol(2)+m) - dj(m,nm1,j,km1)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + dj(m,nm1,j,k)
     &                 - dpj(m,nm1,j,km1)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + dpj(m,nm1,j,k)
               end do
            end do
         end do

         k = kmaxbnd-1
         km1 = k-1
         do j = js,je
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 2,4
               nm1 = n-1 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,nmax
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) - dj(m,nm1,j,km1)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + dj(m,nm1,j,k)
     &                 - dpj(m,nm1,j,km1)
                  as(ii+icol(5)+m) = as(ii+icol(5)+m) + dpj(m,nm1,j,k)
               end do
            end do
         end do

         if (ibc2.NE.0) then
            j   = jminbnd + 1
            ks  = kminbnd + 2
            ke  = kmaxbnd - 2

            do k = ks,ke
               km1 = k-1
               itmp = ( indx(j,k) - 1 )*nmax
               do n  = 2,4
                  nm1 = n-1 
                  ij  = itmp + n
                  ii  = ( ij - 1 )*icol(9)
                  do m =1,nmax
                     as(ii+icol(2)+m) = as(ii+icol(2)+m) -
     &                    dj(m,nm1,j,km1)
                     as(ii+icol(3)+m) = as(ii+icol(3)+m) + dj(m,nm1,j,k)
     &                    - dpj(m,nm1,j,km1)
                     as(ii+icol(4)+m) = as(ii+icol(4)+m) +
     &                    dpj(m,nm1,j,k)
                  end do
               end do
            end do

            k = kminbnd + 1
            km1 = k-1
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 2,4
               nm1 = n-1 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,nmax
                  as(ii+icol(1)+m) = as(ii+icol(1)+m) - dj(m,nm1,j,km1)
                  as(ii+icol(2)+m) = as(ii+icol(2)+m) + dj(m,nm1,j,k)
     &                 - dpj(m,nm1,j,km1)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + dpj(m,nm1,j,k)
               end do
            end do

            k = kmaxbnd - 1
            km1 = k-1
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 2,4 
               nm1 = n-1
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,nmax
                  as(ii+icol(2)+m) = as(ii+icol(2)+m) - dj(m,nm1,j,km1)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + dj(m,nm1,j,k)
     &                 - dpj(m,nm1,j,km1)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + dpj(m,nm1,j,k)
               end do
            end do
         end if

         if (ibc4.NE.0) then
            j   = jmaxbnd - 1
            ks  = kminbnd + 2
            ke  = kmaxbnd - 2

            do k = ks,ke
               km1 = k-1
               itmp = ( indx(j,k) - 1 )*nmax
               do n  = 2,4
                  nm1 = n-1
                  ij  = itmp + n
                  ii  = ( ij - 1 )*icol(9)
                  do m =1,nmax
                     as(ii+icol(3)+m) = as(ii+icol(3)+m) -
     &                    dj(m,nm1,j,km1)
                     as(ii+icol(4)+m) = as(ii+icol(4)+m) + dj(m,nm1,j,k)
     &                    - dpj(m,nm1,j,km1)
                     as(ii+icol(5)+m) = as(ii+icol(5)+m) +
     &                    dpj(m,nm1,j,k)
                  end do
               end do
            end do

            k = kminbnd + 1
            km1 = k-1
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 2,4 
               nm1 = n-1
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,nmax
                  as(ii+icol(2)+m) = as(ii+icol(2)+m) - dj(m,nm1,j,km1)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + dj(m,nm1,j,k)
     &                 - dpj(m,nm1,j,km1)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + dpj(m,nm1,j,k)
               end do
            end do
            
            k = kmaxbnd - 1
            km1 = k-1
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 2,4 
               nm1 = n-1
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,nmax
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) - dj(m,nm1,j,km1)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + dj(m,nm1,j,k)
     &                 - dpj(m,nm1,j,km1)
                  as(ii+icol(5)+m) = as(ii+icol(5)+m) + dpj(m,nm1,j,k)
               end do
            end do      
         end if
      end if

      return                                                            
      end                       !impvis
