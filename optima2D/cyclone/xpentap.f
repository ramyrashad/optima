        subroutine xpentap(jdim,kdim,a,b,c,d,e,u1,u2,u3,u4,f,           
     *                     jl,ju,kl,ku)                                 
c       parameter (maxj=497)                                            
#include "../include/parm.inc"
        double precision ld,ld1,ld2,ldi,ldd,lu                                      
        dimension a(jdim,kdim),b(jdim,kdim),c(jdim,kdim),d(jdim,kdim)   
        dimension e(jdim,kdim),f(jdim,kdim),u1(jdim,kdim)               
        dimension u2(jdim,kdim),u3(jdim,kdim),u4(jdim,kdim)             
c                                                                       
        common/worksp/lda(maxj),ldb(maxj),lus(maxj),                    
     *         lds(maxj),work(maxj,26)                                  
        double precision lda,ldb,lus,lds                                            
c                                                                       
c        ! start forward generation process and sweep                   
c                                                                       
        i = jl                                                          
        do 10 k = kl,ku                                                 
        ld = c(i,k)                                                     
        ldi = 1./ld                                                     
        u1(i,k) = d(i,k)*ldi                                            
        u2(i,k) = e(i,k)*ldi                                            
        u3(i,k) = a(i,k)*ldi                                            
        u4(i,k) = b(i,k)*ldi                                            
        f(i,k) = f(i,k)*ldi                                             
10      continue                                                        
c                                                                       
        i = jl+1                                                        
        do 20 k = kl,ku                                                 
        ld1 = b(i,k)                                                    
        ld = c(i,k) - ld1*u1(i-1,k)                                     
        ldi = 1./ld                                                     
        u1(i,k) = (d(i,k) - ld1*u2(i-1,k))*ldi                          
        u2(i,k) = e(i,k)*ldi                                            
        u3(i,k) = -ld1*u3(i-1,k)*ldi                                    
        u4(i,k) = (a(i,k) -ld1*u4(i-1,k))*ldi                           
        f(i,k) = (f(i,k) - ld1*f(i-1,k))*ldi                            
20      continue                                                        
c                                                                       
          do 1 i = jl+2,ju-4                                            
                do 11 k = kl,ku                                         
                ld2 = a(i,k)                                            
                ld1 = b(i,k) - ld2*u1(i-2,k)                            
                ld = c(i,k) - (ld2*u2(i-2,k) + ld1*u1(i-1,k))           
                ldi = 1./ld                                             
                u1(i,k) = (d(i,k) - ld1*u2(i-1,k))*ldi                  
                u2(i,k) = e(i,k)*ldi                                    
                u3(i,k) = (-ld2*u3(i-2,k) - ld1*u3(i-1,k))*ldi          
                u4(i,k) = (-ld2*u4(i-2,k) - ld1*u4(i-1,k))*ldi          
                f(i,k) = (f(i,k) - ld2*f(i-2,k) - ld1*f(i-1,k))*ldi     
11              continue                                                
1           continue                                                    
c                                                                       
                i = ju-3                                                
                do 12 k = kl,ku                                         
                ld2 = a(i,k)                                            
                ld1 = b(i,k) - ld2*u1(i-2,k)                            
                ld = c(i,k) - (ld2*u2(i-2,k) + ld1*u1(i-1,k))           
                ldi = 1./ld                                             
                u1(i,k) = (d(i,k) - ld1*u2(i-1,k))*ldi                  
                u3(i,k) = (e(i,k) - ld2*u3(i-2,k) - ld1*u3(i-1,k))*ldi  
                u4(i,k) = (-ld2*u4(i-2,k) - ld1*u4(i-1,k))*ldi          
                f(i,k) = (f(i,k) - ld2*f(i-2,k) - ld1*f(i-1,k))*ldi     
12              continue                                                
c                                                                       
        i = ju-2                                                        
        do 22 k = kl,ku                                                 
        ld2 = a(i,k)                                                    
        ld1 = b(i,k) - ld2*u1(i-2,k)                                    
        ld = c(i,k) - (ld2*u2(i-2,k) + ld1*u1(i-1,k))                   
        ldi = 1./ld                                                     
        u3(i,k) = (d(i,k) - ld2*u3(i-2,k) - ld1*u3(i-1,k))*ldi          
        u4(i,k) = (e(i,k) - ld2*u4(i-2,k) - ld1*u4(i-1,k))*ldi          
        f(i,k) = (f(i,k) - ld2*f(i-2,k) - ld1*f(i-1,k))*ldi             
22      continue                                                        
c                                                                       
        i = ju-1                                                        
        do 32 k = kl,ku                                                 
        lda(k) = e(i,k)                                                 
        ldb(k) = -lda(k)*u1(jl,k)                                       
        f(i,k) = f(i,k) - lda(k)*f(jl,k) - ldb(k)*f(jl+1,k)             
        lds(k) = lda(k)*u3(jl,k) + ldb(k)*u3(jl+1,k)                    
        u4(i,k) = lda(k)*u4(jl,k) + ldb(k)*u4(jl+1,k)                   
32      continue                                                        
                                                                        
        do 3 m = jl+2,ju-4                                              
           do 14 k = kl,ku                                              
           ldd = -lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)                   
           lda(k) = ldb(k)                                              
           ldb(k) = ldd                                                 
           lds(k) = lds(k) + ldd*u3(m,k)                                
           u4(i,k) = u4(i,k) + ldd*u4(m,k)                              
           f(i,k) = f(i,k) - ldd*f(m,k)                                 
14         continue                                                     
3       continue                                                        
c                                                                       
        m = ju-3                                                        
        do 15 k = kl,ku                                                 
        ldd = a(i,k) - lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)              
        lda(k) = ldb(k)                                                 
        ldb(k) = ldd                                                    
        lds(k) = lds(k) + ldd*u3(m,k)                                   
        u4(i,k) = u4(i,k) + ldd*u4(m,k)                                 
        f(i,k) = f(i,k) - ldd*f(m,k)                                    
15      continue                                                        
        m = ju-2                                                        
        do 25 k = kl,ku                                                 
        ldd = b(i,k) - lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)              
        lda(k) = ldb(k)                                                 
        ldb(k) = ldd                                                    
        lds(k) = lds(k)+ ldd*u3(m,k)                                    
        u4(i,k) = u4(i,k) + ldd*u4(m,k)                                 
        f(i,k) = f(i,k) - ldd*f(m,k)                                    
25      continue                                                        
c                                                                       
        do 35 k = kl,ku                                                 
        lds(k) = c(i,k) - lds(k)                                        
        ldi = 1./lds(k)                                                 
        u4(i,k) = (d(i,k) - u4(i,k))*ldi                                
        f(i,k) = f(i,k)*ldi                                             
35      continue                                                        
c                                                                       
c    this breakup of k loops is very important                          
c                                                                       
        i = ju                                                          
        do 16 k = kl,ku                                                 
        lda(k) = d(i,k)                                                 
        ldb(k) = e(i,k) - lda(k)*u1(jl,k)                               
        lus(k) = lda(k)*u3(jl,k) + ldb(k)*u3(jl+1,k)                    
        lds(k) = lda(k)*u4(jl,k) + ldb(k)*u4(jl+1,k)                    
        f(i,k) = f(i,k) - lda(k)*f(jl,k) - ldb(k)*f(jl+1,k)             
c                                                                       
16      continue                                                        
c                                                                       
        do 4 m = jl+2,ju-3                                              
           do 17 k = kl,ku                                              
           ldd = -lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)                   
           lda(k) = ldb(k)                                              
           ldb(k) = ldd                                                 
           lus(k) = lus(k) + ldd*u3(m,k)                                
           lds(k) = lds(k) + ldd*u4(m,k)                                
           f(i,k) = f(i,k) - ldd*f(m,k)                                 
17         continue                                                     
4       continue                                                        
c                                                                       
        m = ju-2                                                        
        do 18 k = kl,ku                                                 
        ldd = a(i,k) - lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)              
        lda(k) = ldb(k)                                                 
        ldb(k) = ldd                                                    
        lus(k) = lus(k) + ldd*u3(m,k)                                   
        lds(k) = lds(k) + ldd*u4(m,k)                                   
        f(i,k) = f(i,k) - ldd*f(m,k)                                    
18      continue                                                        
        m = ju-1                                                        
        do 28 k = kl,ku                                                 
        ldd = b(i,k) - lus(k)                                           
        f(i,k) = f(i,k) - ldd*f(m,k)                                    
        lds(k) = c(i,k) - ldd*u4(m,k) - lds(k)                          
        ldi = 1./lds(k)                                                 
        f(i,k) = f(i,k)*ldi                                             
28      continue                                                        
c                                                                       
c        !  back sweep solution                                         
c                                                                       
      do 19 k = kl,ku                                                   
      f(ju,k) = f(ju,k)                                                 
      f(ju-1,k) = f(ju-1,k) - u4(ju-1,k)*f(ju,k)                        
      f(ju-2,k) = f(ju-2,k) - u4(ju-2,k)*f(ju,k) - u3(ju-2,k)*f(ju-1,k) 
      f(ju-3,k) = f(ju-3,k) - u4(ju-3,k)*f(ju,k) - u3(ju-3,k)*f(ju-1,k) 
     *                - u1(ju-3,k)*f(ju-2,k)                            
19      continue                                                        
c                                                                       
        do 2 i = 4,ju-jl                                                
        ix = ju-i                                                       
          do 50 k = kl,ku                                               
          f(ix,k) = f(ix,k) - u4(ix,k)*f(ju,k) - u3(ix,k)*f(ju-1,k)     
     *              - u1(ix,k)*f(ix+1,k) - u2(ix,k)*f(ix+2,k)           
50        continue                                                      
2       continue                                                        
c                                                                       
        return                                                          
        end                                                             
