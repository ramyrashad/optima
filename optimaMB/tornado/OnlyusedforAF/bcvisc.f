CC ********************* BOUNDARY CONDITIONS **************************
c ** Application of solid wall boundary conditions on side 2 or 4    **
c
c     calling routine:  bcupdate
c     last revision: SDR 22/05/00
c
      subroutine bcvisc(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,f,
     &                  k1,k2,jbc,idir)
c
c     Notes:   the j range for the bc's is j1 to j2
c              the line for application of bc is k = 1 
c              metrics for corresponding pts are at k=kbegin
c              q has been loaded with data in k=1,2
c
c
C                                                       
#include "../include/parms.inc"
#include "../include/common.inc"
c                                                      
      dimension q(jdim,kdim,4),press(jdim,kdim)     
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)       
      dimension xit(jdim,kdim),ett(jdim,kdim)       
      dimension x(jdim,kdim),y(jdim,kdim),f(jdim)
c
      jadd = sign(1,idir)
c
c     -boundary conditions for surface boundary at j=jbegin or jend
c                                       i.e. blunt trailing edge
c
c
c     -This scaling is used if the calculation is not being restarted
c       It is used to bring the calculation up slowly, rather than   
c       impulsively.  It uses a sixth degree polynomial f            
c       for a smooth transition from scal slightly bigger than 0. at
c       to first iteration, to scal = 1. after the strtit'th iteration.
c       The scaling is turned off by restart if this is a restart run.
c                                                                       
      t = float(numiter - 1)/ strtit
      if(t.gt.1.) t = 1.                                              
      scal = (10. -15.*t + 6.*t*t) *t**3                                
c                                                                       
c     -no scaling if this is a restart                        
      if(restart) scal=1.                                             
c
c      -viscous surface bc on side 1
      jq  = jbc
      jq2 = jq + jadd
      jq3 = jq + 2*jadd
      jq4 = jq + 3*jadd
c
      do 35 k=k1,k2
c       cap u and cap v = 0.0
        u1 = (xy(jq,k,4)*(-xit(jq,k)) + xy(jq,k,2)*ett(jq,k))
     &        /xyj(jq,k)
        v1 = (- xy(jq,k,3)*(-xit(jq,k)) -xy(jq,k,1)*ett(jq,k))
     &        /xyj(jq,k)
        u1 = (1. - scal)*q(jq,k,2)/q(jq,k,1) + scal*u1               
        v1 = (1. - scal)*q(jq,k,3)/q(jq,k,1) + scal*v1               
c       -q2 and q3 now satify tangency
c       -note: q2 and q3 contain just velocity values and not
c                not momentum yet ... also have no jacobian
        q(jq,k,2) = u1
        q(jq,k,3) = v1
 35   continue                                                       

c     -surface condition on pressure  
c      -dp/dn = 0.0 
c      -extrap p
c      -note: f(k) doesn't have jacobian in it

      if (iord.eq.2) then
        do 40 k =k1,k2  
          r2 = q(jq2,k,1)*xyj(jq2,k)
          p2=press(jq2,k)*xyj(jq2,k)
c     
          f(k)=p2                   !pressure
          q(jq,k,1) = r2/xyj(jq,k)  !density 
 40     continue
      else
c       *****  higher-order  *****
        do 45 k =k1,k2
c         -pressure at the boundary            
          p2 = press(jq2,k)*xyj(jq2,k)
          p3 = press(jq3,k)*xyj(jq3,k)
          p4 = press(jq4,k)*xyj(jq4,k)
c     
c         force dp/dn=0 on surface to ...
c         -second-order
c          f(j) = (4.d0*p2 - p3)/3.d0
c         -third-order
          f(k) = (18.d0*p2 - 9.d0*p3 + 2.d0*p4)/1.1d1
c     
c     
c         -density at the boundary
          r2 = q(jq2,k,1)*xyj(jq2,k)
          r3 = q(jq3,k,1)*xyj(jq3,k)
          r4 = q(jq4,k,1)*xyj(jq4,k)
c     
c         -set d/dn (rho) = 0 (second-order)                 
c          q(j,k,1) = (4.d0*r2 - r3)*rj/3.d0
c         -set d/dn (rho) = 0 (third-order)                 
          q(jq,k,1) = (18.d0*r2 - 9.d0*r3 + 2.d0*r4)/(xyj(jq,k)*11.d0)
 45     continue
      endif
c                                                                       
      if (wtrat.ne.0.0) then                                    
        do 50 k=k1,k2
c         -fix wall temp   tim  barth 10/10/84                
c         -twall is the ratio of twall/tinf                            
          prs = f(k)/xyj(jq,k)                                           
          twall = wtrat/gamma                                     
          q(jq,k,1) = prs/twall                                    
 50     continue
      endif                                                   
c
c      -now that density is updated, convert q2 and q3 to momentum
      do 55 k=k1,k2
        q(jq,k,2) = q(jq,k,2)*q(jq,k,1)                                       
        q(jq,k,3) = q(jq,k,3)*q(jq,k,1)                                       
        q(jq,k,4) = f(k)/(gami*xyj(jq,k)) +   
     &        .5 * (q(jq,k,2)**2+q(jq,k,3)**2) /q(jq,k,1)
 55   continue                                                       
c
      return                                                           
      end                                                              
