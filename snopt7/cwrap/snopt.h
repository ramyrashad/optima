#ifndef _SNOPT_H
#define _SNOPT_H

#include <time.h>
#include "iter.h"

typedef int /* Unknown procedure type */ (*U_fp)();

typedef void (*snObjectiveFunc)
(
 int    *mode,
 int    *nnObj,
 double *x,
 double *fObj,
 double *gObj,
 int    *nState,
 char   *cu,
 int    *lencu,
 double *iu,
 int    *leniu,
 double *ru,
 int    *lenru
);

typedef void (*snConstraintFunc)
(
 int    *mode,
 int    *nnCon,
 int    *nnJac,
 int    *negCon,
 double *x,
 double *fCon,
 double *gCon,
 int    *nState,
 char   *cu,
 int    *lencu,
 double *iu,
 int    *leniu,
 double *ru,
 int    *lenru
);

typedef struct {
  int    n;
  int    m;
  int    ne;
  int    negCon;
  int    nnCon;
  int    nnObj;
  int    nnJac;

} snDimension;

typedef struct {
  /* Printing */
  int     major_print_level;
  int     minor_print_level;
  char*   print_file_name;
  char*   sumry_file_name;
  int     print_frequency;
  int     summary_frequency;
  int     solution_printed;
  int     options_print_mode;

  /* Convergence Tolerances */
  double  major_feasibility_tolerance;
  double  major_optimality_tolerance;
  double  minor_feasibility_tolerance;
  double  minor_optimality_tolerance;

  /* Derivative checking */
  int     verify_level;
  int     start_obj_check_column;
  int     stop_obj_check_column;
  int     start_con_check_column;
  int     stop_con_check_column;

  /* Scaling */
  int     scale;
  double  scale_tolerance;
  int     scale_print;

  /* Other Tolerances */
  double  crash_tolerance;
  double  linesearch_tolerance;
  double  LU_factor_tolerance;
  double  LU_update_tolerance;
  double  LU_singularity_tolerance;
  double  pivot_tolerance;

  /* QP subproblems */
  int     crash_mode;
  double  elastic_weight;
  int     iterations_limit;
  int     partial_price;

  /* SQP method */
  int     major_iterations_limit;
  int     minor_iterations_limit;
  double  major_step_limit;
  int     superbasics_limit;
  int     reduced_Hessian_dimension;
  int     derivative_level;
  int     linesearch_mode;
  double  function_precision;
  double  difference_interval;
  double  central_difference_interval;
  double  violation_limit;
  double  unbounded_step_size;
  double  unbounded_objective;

  /* Hessian approximation */
  int     hessian_memory_mode;
  int     hessian_frequency;
  int     hessian_updates;
  int     hessian_flush;

  /* Frequencies */
  int     check_frequency;
  int     expand_frequency;
  int     factorization_frequency;
  int     save_frequency;

  /* Stuff        */

  double  objective_add;
  int     start_mode;
  double  infinity;
  char   *ProblemName;

} snOption;

typedef struct {

  int     num_major_iterations;
  int     num_minor_iterations;
  int     num_constraint_function_evals;
  int     num_objective_function_evals;
  int     num_superbasics;
  int     num_degenerate_steps;
  int     num_infeasibilities;

  double  max_primal_infeasibility;
  int     i_max_primal_infeasibility;
  double  max_dual_infeasibility;
  int     i_max_dual_infeasibility;
  double  max_scaled_primal_infeasibility;
  int     i_max_scaled_primal_infeasibility;
  double  max_scaled_dual_infeasibility;
  double  i_max_scaled_dual_infeasibility;

  double  norm_scaled_solution;
  double  norm_solution;
  double  norm_scaled_multipliers;
  double  norm_multipliers;

  double  penalty;
  double  objective_value;
  double  linear_objective_value;
  double  nonlinear_objective_value;
  double  sum_infeasibilities;
  double  scaled_merit;
  double  elastic_weight;
  double  max_nonlinear_violation;

} snStats;

typedef struct {
  int     print_file_unit;
  int     sumry_file_unit;

  int     length_rw;
  double *rw;

  int     length_iw;
  int    *iw;

  int     length_cw;
  char   *cw;

  double *Jcol;
  int    *indJ;
  int    *locJ;

  double *bl;
  double *bu;
  int    *hs;
  double *xs;
  double *pi;
  double *rc;

  snDimension dimension;
  snOption    option;
  snStats     stats;

  int         linear_objective_row;
  int         solve_mode;
  int         exit_feasibility_mode;

  snConstraintFunc  constraint_func;
  snObjectiveFunc   objective_func;
  void*             func_data;

  int         solve_status;
  int         set_flags;

  /* error handling */
  int     error;
  char*   error_msg;
  int     error_msg_len;   /* length of message alloc'd (include \0 ) */

} snProblem;

extern int snlog_();
extern int snlog2_();
extern int sqlog_ ();
extern int snstop_();

/* Set functions */
int    snSetObjectiveFunction        ( snProblem* prob, snObjectiveFunc  func );
int    snSetConstraintFunction       ( snProblem* prob, snConstraintFunc func );
int    snSetObjectiveAdd             ( snProblem* prob, double objective_add );
int    snSetPrintFileName            ( snProblem* prob, char*  print_file_name );
int    snSetSummaryFileName          ( snProblem* prob, char*  sumry_file_name );

/* Set functions assigning numeric arrays */
int    snSetNonzero             ( snProblem* prob, int nz , double Jcol , int    indJ  );
int    snSetNonzeroColumnPointer( snProblem* prob, int nz , int    locJ                );
int    snSetVariable            ( snProblem* prob, int var, int    hs   , double xs    );
int    snSetConstraint          ( snProblem* prob, int con, int    hs   , double xs    );
int    snSetVariableBounds      ( snProblem* prob, int var, double lower, double upper );
int    snSetConstraintBounds    ( snProblem* prob, int con, double lower, double upper );
int    snSetMultiplier          ( snProblem* prob, int con, double pi                  );

/* Get functions returning scalars */
double snGetMajorOptimalityTolerance      ( snProblem* prob );
double snGetMajorFeasibilityTolerance     ( snProblem* prob );
double snGetObjectiveAdd                  ( snProblem* prob );
int    snGetNumNonlinearConstraints       ( snProblem* prob );
int    snGetNumNonzeros                   ( snProblem* prob );
int    snGetNumVariables                  ( snProblem* prob );
int    snGetNumConstraints                ( snProblem* prob );
int    snGetNumNonlinearObjectiveVariables( snProblem* prob );
int    snGetNumNonlinearJacobianVariables ( snProblem* prob );
int    snGetSolveStatus                   ( snProblem* prob );

/* Get functions returning pointers */
double *snGetNonzeroValues               ( snProblem* prob );
int    *snGetNonzeroRowIndices           ( snProblem* prob );
int    *snGetNonzeroColumnPointers       ( snProblem* prob );
double *snGetVariables                   ( snProblem* prob );
double *snGetConstraints                 ( snProblem* prob );
double *snGetConstraintUpperBounds       ( snProblem* prob );
double *snGetConstraintLowerBounds       ( snProblem* prob );
double *snGetVariableUpperBounds         ( snProblem* prob );
double *snGetVariableLowerBounds         ( snProblem* prob );
double *snGetMultipliers                 ( snProblem* prob );
int    *snGetVariableBasisEligibilities  ( snProblem* prob );
int    *snGetConstraintBasisEligibilities( snProblem* prob );

/* Get functions returning elements of arrays */
double  snGetNonzeroValue              ( snProblem* prob, int nz  );
int     snGetNonzeroRowIndex           ( snProblem* prob, int nz  );
int     snGetNonzeroColumnPointer      ( snProblem* prob, int col );
double  snGetVariable                  ( snProblem* prob, int var );
double  snGetConstraint                ( snProblem* prob, int con );
double  snGetConstaintUpperBound       ( snProblem* prob, int con );
double  snGetConstaintLowerBound       ( snProblem* prob, int con );
double  snGetVariableUpperBound        ( snProblem* prob, int var );
double  snGetVariableLowerBound        ( snProblem* prob, int var );
double  snGetMultiplier                ( snProblem* prob, int con );
int     snGetVariableBasisEligibility  ( snProblem* prob, int var );
int     snGetConstraintBasisEligibility( snProblem* prob, int con );

/* Get function returning times */
float   snGetCpuTime                   ( void );

/* Get functions returning integer solution statistics */
int     snGetNumMinorIterations           ( snProblem* prob );
int     snGetNumMajorIterations           ( snProblem* prob );
int     snGetNumObjectiveFunctionEvals    ( snProblem* prob );
int     snGetNumConstraintFunctionEvals   ( snProblem* prob );
int     snGetNumSuperBasicVariables       ( snProblem* prob );
int     snGetNumDegenerateSteps           ( snProblem* prob );
int     snGetNumInfeasibleConstraints     ( snProblem* prob );
int     snGetIMaxScaledPrimalInfeasibility( snProblem* prob );
int     snGetIMaxScaledDualInfeasibility  ( snProblem* prob );
int     snGetIMaxPrimalInfeasibility      ( snProblem* prob );
int     snGetIMaxDualInfeasibility        ( snProblem* prob );

/* Get functions returning double solution statistics */
double  snGetNormScaledSolution          ( snProblem* prob );
double  snGetNormSolution                ( snProblem* prob );
double  snGetNormScaledMultipliers       ( snProblem* prob );
double  snGetNormMultipliers             ( snProblem* prob );
double  snGetPenalty                     ( snProblem* prob );
double  snGetObjectiveValue              ( snProblem* prob );
double  snGetLinearObjectiveValue        ( snProblem* prob );
double  snGetNonlinearObjectiveValue     ( snProblem* prob );
double  snGetSumInfeasibilities          ( snProblem* prob );
double  snGetMaxScaledPrimalInfeasibility( snProblem* prob );
double  snGetMaxScaledDualInfeasibility  ( snProblem* prob );
double  snGetMaxPrimalInfeasibility      ( snProblem* prob );
double  snGetMaxDualInfeasibility        ( snProblem* prob );

/* error handling */
int     snGetError      ( snProblem* prob , char** error_msg );
void    snClearLastError( snProblem* prob );
void    snSetError      ( snProblem* prob , int type , const char* error_msg );
int     snCheck         ( snProblem* prob );

/* Principal routines */
int snSolve ( snProblem* prob );
int snDelete( snProblem* prob );
int snInit  ( snProblem* prob ,
	      int        variables,
	      int        constraints,
	      int        nonzeros,
	      int        jacobian_nonzeros,
	      int        nonlinear_constraints,
	      int        nonlinear_objective_variables,
	      int        nonlinear_jacobian_variables ,
	      int        linear_objective_row,
              char      *print_file_name,
              char      *summary_file_name );

/* boolean */
#define SN_TRUE  1
#define SN_FALSE 0

/* error codes */
#define SN_ERROR                 0
#define SN_OK                    1
#define SN_FILE_ERROR            2  /* file error */
#define SN_PROBLEM_UNINITIALIZED 3
#define SN_PROBLEM_UNSOLVED      4

/* basis eligibility (hs) values */
#define SN_BASIS_COLD_ELIGIBLE_0   0
#define SN_BASIS_COLD_ELIGIBLE_1   1
#define SN_BASIS_IGNORE            2
#define SN_BASIS_COLD_ELIGIBLE_3   3
#define SN_BASIS_LOWER_BOUND       4
#define SN_BASIS_UPPER_BOUND       5

/* bits for set_flags */
#define SN_BIT_MAJOR_FEASIBILITY_TOLERANCE (1 << 0)
#define SN_BIT_MINOR_FEASIBILITY_TOLERANCE (1 << 1)
#define SN_BIT_MAJOR_OPTIMALITY_TOLERANCE  (1 << 2)
#define SN_BIT_MINOR_OPTIMALITY_TOLERANCE  (1 << 3)
#define SN_BIT_SOLVED                      (1 << 4)

/* "Normal" result conditions for snSolve() */
#define SN_SOLUTION_FOUND                      1
#define SN_INFEASIBLE                          2
#define SN_UNBOUNDED                           3
#define SN_VIOLATION_LIMIT_EXCEEDED            4
#define SN_MINOR_ITERATION_LIMIT_EXCEEDED      5
#define SN_MAJOR_ITERATION_LIMIT_EXCEEDED      6
#define SN_ACCURACY_NOT_ACHIEVED               7
#define SN_SUPERBASICS_LIMIT_EXCEEDED          8
#define SN_POINT_CANNOT_BE_IMPROVED            9
#define SN_CANNOT_SATISFY_GENERAL_CONSTRAINTS 10
#define SN_SINGULAR_BASIS                     11

/* "Error" result conditions for snSolve() */

#ifndef NULL
#define NULL 0L
#endif

#endif
