      ! ================================================================
      ! ================================================================
      !     
      subroutine blEdge(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu)
      ! 
      ! ================================================================
      ! ================================================================
      !     
      ! Purpose: This subroutine will attempt to find the boundary layer
      !   edge. The following methods for doing so have been
      !   implemented:
      !   1) compressible Bernouilli's equation method
      !   2) diagnostic function (Baldwin-Lomax model) method
      !   3) vorticity and shear stress method
      !
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"


      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim,
     &     ndim
      double precision
     &     q(jdim,kdim,ndim),
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim)


      ! _______________________
      ! declare local variables
      integer
     &     j,
     &     k,
     &     k_cutoff

      
      ! _____________________________________________________
      ! declare output variables from getCurvilinearVariables
      double precision
     &     vel(jdim,kdim),
     &     uu(jdim,kdim),
     &     vv(jdim,kdim),
     &     p(jdim,kdim),
     &     rho(jdim,kdim),
     &     a(jdim,kdim),
     &     M(jdim,kdim),
     &     dy(jdim,kdim)


      ! __________________________________________________
      ! Compute variables in curvilinear coordinate system
      call getCurvilinearVariables(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     vel, uu, vv, p, rho, a, M, dy)

      ! _____________________________________________
      ! Execute subroutine(s) used to compute BL edge
      select case (blEdgeMethod)

      case (1)

         call blEdge_Bernouilli(
     &        jdim, kdim, xy, xyj, x, y, fmu, vort, turmu,
     &        vel, uu, vv, p, rho, a, M, dy)
         
      case (2)

         call blEdge_DiagnosticFcn(
     &        jdim, kdim, xy, xyj, x, y, fmu, vort, turmu,
     &        vel, uu, vv, p, rho, a, M, dy)
         
      case (3)

         call blEdge_VorticityMethod(
     &        jdim, kdim, xy, xyj, x, y, fmu, vort, turmu,
     &        vel, uu, vv, p, rho, a, M, dy)

      end select ! blEdgeMethod

      return
      end subroutine blEdge

      ! ================================================================
      ! ================================================================
      !     
      subroutine blEdge_Bernouilli(
     &     jdim, kdim, xy, xyj, x, y, fmu, vort, turmu,
     &     vel, uu, vv, p, rho, a, M, dy)
      ! 
      ! ================================================================
      ! ================================================================
      ! 
      ! Purpose: This subroutine will find the boundary layer edge using
      !   the compressible form of the bernouilli equation to find the
      !   edge velocity using the known wall pressure
      !
      ! On completion, the following global variables will be updated:
      ! foundBLEdge, k_deltaBL, delatBL, and velBLEdge 
      !
      ! ----------------------------------------------------------------
#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim
      double precision
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim),
     &     vel(jdim,kdim),
     &     uu(jdim,kdim),
     &     vv(jdim,kdim),
     &     p(jdim,kdim),
     &     rho(jdim,kdim),
     &     a(jdim,kdim),
     &     M(jdim,kdim),
     &     dy(jdim,kdim)

      ! ______________________________
      ! declare input/output variables
      ! -- none

      ! _______________________
      ! declare local variables
      integer
     &     j,
     &     k,
     &     kd,
     &     kdm1,
     &     k_edge,
     &     k_velMax
      double precision
     &     u_deltaBL,
     &     vel_max,
     &     p_w

      write(36,*)
      write(36,*) 'Bernouilli Method Output:'
      write(36,*) '---------------------------'


      do j=jblStart,jblEnd,jblinc

         p_w = p(j,1)           ! wall pressure

         ! compute edge velocity using compressible bernouilli's eqn
         u_deltaBL = dsqrt(uinf*uinf - 2d0*gamma*pinf
     &        /(gamma-1d0)/rhoinf*((p_w/pinf)**((gamma-1d0)/gamma) -
     &        1d0))

         ! search along off-wall direction for u = 0.99*velBLEdge(j)
         u_deltaBL = 0.99d0*u_deltaBL 
         do k=kblStart,kblEnd
            if (.not.foundBLEdge(j)) then
               if (vel(j,k).ge.u_deltaBL) then
                  ! BLEdge has been found, set flag:
                  foundBLEdge(j) = .true.
                  k_deltaBL(j) = k
                  ! linear interpolation based on velocity
                  velBLEdge(j) = u_deltaBL
                  kd = k_deltaBL(j)
                  kdm1 = k_deltaBL(j)-1
                  deltaBL(j) = dy(j,kdm1)
     &                 + (dy(j,kd)-dy(j,kdm1))
     &                 * (u_deltaBL-vel(j,kdm1))
     &                 / (vel(j,kd)-vel(j,kdm1))
               end if
            end if 
         end do


         ! ____________
         ! Backup Plan: 
         ! If 0.99*velBLEdge not reached then take the point with max
         ! velocity to be the boundary layer edge
         if (.not.foundBLEdge(j)) then
            vel_max = 0d0
            k_velMax = 0
            ! -- find the max velocity in the boundary layer
            do k=kblStart,kblEnd
               if (vel(j,k).ge.vel_max) then
                  vel_max = vel(j,k)
                  k_velMax = k
                  foundBLEdge(j) = .true.
               end if
            end do 
            ! -- if max velocity was found set the BL edge
            if (foundBLEdge(j)) then
               deltaBL(j) = dy(j,k_velMax)
               k_deltaBL(j) = k_velMax
               velBLEdge(j) = vel(j,k_deltaBL(j))
               write(36,130) j, velBLEdge(j)
 130           format('M1: Using max velocity, for j = ', I3, 
     &              ' with a Max Vel = ', 1p, e14.7)
            else
               foundBLEdge(j) = .false.
               write(36,*) 'BLEdge method 1 failed: k_velMax is zero!'
            end if
         end if

      end do ! j-loop

      return
      end subroutine blEdge_Bernouilli


      ! ================================================================
      ! ================================================================
      ! 
      !
      subroutine blEdge_DiagnosticFcn(
     &     jdim, kdim, xy, xyj, x, y, fmu, vort, turmu,
     &     vel, uu, vv, p, rho, a, M, dy)
      ! 
      ! ================================================================
      ! ================================================================
      ! 
      ! Purpose: This subroutine will find the boundary layer edge using
      !   the diagnostic function, analogous to the that used in the
      !   Baldwin-Lomax turbulence model. Reference: Nebel(2003).
      !
      ! On completion, the following global variables will be updated:
      ! foundBLEdge, k_deltaBL, delatBL, and velBLEdge 
      !
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim
      double precision
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim),
     &     vel(jdim,kdim),
     &     uu(jdim,kdim),
     &     vv(jdim,kdim),
     &     p(jdim,kdim),
     &     rho(jdim,kdim),
     &     a(jdim,kdim),
     &     M(jdim,kdim),
     &     dy(jdim,kdim)

      ! ______________________________
      ! declare input/output variables
      ! -- none

      ! _______________________
      ! declare local variables
      logical
     &     flowLaminarState     !< flag used to set the constants that
                                !! are used in the calculation of F
      integer
     &     j,
     &     k,
     &     k_Fmax,              !< index of max diagnostic function value
     &     k_d,                 !< index of BLEdge
     &     k_dm,                !< index of BLEdge minus 1
     &     jp1,                 !< index j plus 1
     &     jm1,                 !< index j minus 1
     &     kp1,                 !< index k plus 1
     &     km1                  !< index k minus 1

      double precision
     &     const_a,             !< constant a used in calc of F
     &     const_b,             !< constant b used in calc of F
     &     const_eps,           !< constant epsilon used in calc of F
     &     Fmax,                !< max diagnostic function value
     &     y_Fmax,              !< off-wall distance of max diagnostic function value
     &     F(jdim,kdim),        !< diagnostic function
     &     dveldx(jdim,kdim),   !< derivative of magnitude of velocity wrt x
     &     dveldy(jdim,kdim),   !< derivative of magnitude of velocity wrt y
     &     dveldn(jdim,kdim),   !< derivative of velocity wrt off-wall distance
     &     dveldn_1(jdim,kdim), !< derivative of velocity wrt off-wall distance
     &     dveldn_2(jdim,kdim), !< derivative of velocity wrt off-wall distance
     &     dveldn_3(jdim,kdim), !< derivative of velocity wrt off-wall distance
     &     dveldxi(jdim,kdim),
     &     dveldeta(jdim,kdim),
     &     velt(jdim,kdim),   
     &     dveltdxi(jdim,kdim), 
     &     dveltdeta(jdim,kdim),
     &     vel_inv,             !< inverse of velocity (1d0/vel)
     &     dudxi,               !< derivative of velocity u wrt xi
     &     dudeta,              !< derivative of velocity u wrt eta
     &     dvdxi,               !< derivative of velocity v wrt xi
     &     dvdeta,              !< derivative of velocity v wrt eta
     &     denom

      ! variables for cubic spline interpolation
      logical
     &     ramy_flag
      integer
     &     length_cs,
     &     k_csmid,
     &     k_csm1,
     &     k_cs,
     &     numSubGridPts,
     &     indx
         
      double precision
     &     f_cs(3),
     &     y_cs(3),
     &     b(2),
     &     c(2),
     &     d(2),
     &     stepy,
     &     deltay,
     &     splinePt

      write(36,*)
      write(36,*) 'Diagnostic Function Output:'
      write(36,*) '---------------------------'

      do j=1,jdim
         do k=1,kdim
            F(j,k) = 0d0
            denom = dsqrt(xy(j,k,3)*xy(j,k,3) + xy(j,k,4)*xy(j,k,4))
            velt(j,k) = (xy(j,k,4)*uu(j,k) - xy(j,k,3)*vv(j,k)) / denom
         end do
      end do
         

      do j=jblStart,jblEnd,jblinc
         
         jp1 = j+jblinc
         jm1 = j-jblinc

         Fmax = -1.e30 ! initially set to a huge negative number

         ! Set constants based on Laminar or Turbulent flow state. This is
         ! not strictly accurate since the SA model transitions the flow
         ! over a region.
         !flowlaminarstate = .true.
         if (j.gt.jtranlo .or. j.lt.jtranup) then
            !if (flowlaminarstate) then
            ! constants for laminar flow
            const_a = 3.9d0
            const_b = 1.0d0
            const_eps = 1.294d0
         else
            ! constant for turbulent flow
            const_a = 1.0d0
            const_b = 1.0d0
            const_eps = 1.936d0
         end if

         ! ______________________________________________________
         ! Loop to find max diagnostic function in boundary layer

         do k=kblStart,kblEnd 

            kp1 = k+1
            km1 = k-1

            ! _______________________________________________________
            ! Compute gradient of tangential velocity in the off-wall
            ! normal direction
            ! Investigating 3 options here:
            ! Option 1: Compute tangential component of the velocity
            !           using dot product between the cartesian velocity
            !           vector and the covariant basis vector, e1_hat. Then
            !           use dot product between gradient of the tangential
            !           velocity and the covariant basis vector, e1_hat.
            ! Option 2: Use dot product between gradient of magnitude of
            !           velocity and the covariant basis vector, e1_hat
            ! Option 3: finite difference the edge velocity by taking
            !           the average of the forward and backward difference.

            ! Option 1         
            dveltdxi(j,k) = 0.5*(velt(jp1,k)-velt(jm1,k))
            if (k.eq.1) then 
               dveltdeta(j,k) = (velt(j,kp1)-velt(j,k))
            else if (k.eq.kdim) then
               dveltdeta(j,k) = (velt(j,k)-velt(j,km1))
            else 
               dveltdeta(j,k) = 0.5*(velt(j,kp1)-velt(j,km1))
            end if
            denom = dsqrt(xy(j,k,3)*xy(j,k,3) + xy(j,k,4)*xy(j,k,4))
            dveldn_1(j,k) =
     &           ((xy(j,k,3)*xy(j,k,1)+xy(j,k,3)*xy(j,k,2))
     &           *dveltdxi(j,k)
     &           +(xy(j,k,3)*xy(j,k,3)+xy(j,k,4)*xy(j,k,4))
     &           *dveltdeta(j,k))
     &           / denom

            ! Option 2
            dveldxi(j,k) = 0.5*(vel(jp1,k)-vel(jm1,k))
            if (k.eq.1) then 
               dveldeta(j,k) = (vel(j,kp1)-vel(j,k))
            else if (k.eq.kdim) then
               dveldeta(j,k) = (vel(j,k)-vel(j,km1))
            else 
               dveldeta(j,k) = 0.5*(vel(j,kp1)-vel(j,km1))
            end if
            denom = dsqrt(xy(j,k,3)*xy(j,k,3) + xy(j,k,4)*xy(j,k,4))
            dveldn_2(j,k) =
     &           ((xy(j,k,3)*xy(j,k,1)+xy(j,k,3)*xy(j,k,2))
     &           *dveldxi(j,k)
     &           +(xy(j,k,3)*xy(j,k,3)+xy(j,k,4)*xy(j,k,4))
     &           *dveldeta(j,k))
     &           / denom

            ! Option 3
            if (k.eq.1) then
               dveldn_3(j,k) =
     &              (vel(j,kp1) - vel(j,k)) / (dy(j,kp1)-dy(j,k))
            else if (k.eq.kdim) then
               dveldn_3(j,k) =
     &              (vel(j,k) - vel(j,km1)) / (dy(j,k)-dy(j,km1))
            else
               dveldn_3(j,k) =
     &              0.5*((vel(j,kp1) - vel(j,k)) / (dy(j,kp1)-dy(j,k))
     &              +    (vel(j,k) - vel(j,km1)) / (dy(j,k)-dy(j,km1)))
            end if

            ! ___________________________
            ! Compute diagnostic function
            dveldn(j,k) = dveldn_2(j,k) ! use Option 2
            F(j,k)  = dy(j,k)**const_a * dveldn(j,k)**const_b

            ! -- Store max diagnostic funciton value in boundary layer
            if (F(j,k).ge.Fmax) then
               Fmax = F(j,k)
               k_Fmax = k
               y_Fmax = dy(j,k_Fmax) ! note: value improved by interpolation (below)
            end if

         end do ! k

         ! _________________________________________
         ! Set flag if boundary layer edge was found
         if (k_Fmax.ge.2 .and. k_Fmax.lt.kblEnd) then 
            foundBLEdge(j) = .true.
         else
            foundBLEdge(j) = .false.
            write(36,140) j
 140        format(
     &           'M2: BL DiagnosticFcn Error: k_Fmax not found for j = ',
     &           I3)
         end if 

         if (foundBLEdge(j)) then

            ! Option to set y_Fmax to the height of the node k_Fmax
            !y_Fmax = dy(j,k_Fmax)

            ! Better option is to construct cubic splines and then
            ! find the maximum value in the span k_Fmax-1 < k < k_Fmax+1
            ! __________________________________________________
            ! Step 1 of 2) Construct the piecewise cubic splines
            !              used to interpolate the data points
            ramy_flag = .false.
            length_cs = 3 ! must be an odd number
            k_cs = 1
            do k=k_Fmax-(length_cs-1)/2,k_Fmax+(length_cs-1)/2
               f_cs(k_cs) = F(j,k)
               y_cs(k_cs) = dy(j,k)
               if (k.eq.k_Fmax) k_csmid = k_cs
               k_cs = k_cs + 1
            end do
            k_cs = k_cs -1

            call naturalCubicSpline(
     &           length_cs,y_cs,f_cs,1,length_cs,1,
     &           b,c,d)


            ! Step 2 of 2) find the maximum value in the span
            !              k_Fmax-1 < k < k_Fmax+1
            ! --> This requires the evaluation of the two splines
            !     on either side of the previously found k_Fmax
            numSubGridPts = 1000
            do k_cs = k_csmid-1,k_csmid
               stepy = (y_cs(k_cs+1)-y_cs(k_cs)) / (numSubGridPts-1)
               do indx = 1,numSubGridPts
                  deltay = (indx-1)*stepy
                  ! evaluate spline at sub-grid point
                  splinePt =  f_cs(k_cs)
     &                       + b(k_cs)*deltay
     &                       + c(k_cs)*deltay**2
     &                       + d(k_cs)*deltay**3
!                  if (j.eq.310) then
!                     write(36,*) 'k_cs, indx, SplinePt = ',
!     &                    k_cs, indx, SplinePt
!                  end if
                  ! find max value
                  if (splinePt.gt.Fmax) then
                     Fmax = splinePt
                     y_Fmax = y_cs(k_cs)+deltay
                     ramy_flag = .true.
                  end if
               end do 
            end do
            
            if (.not.ramy_flag) then
               write(36,*) 'Not improved for j = ', j, j-jblStart+1
            end if

            ! _______________________________________________________________
            ! Compute boundary layer thickness using the y-coordinate of Fmax
            deltaBL(j) = const_eps*y_Fmax


            ! -- interpolation of velBLEdge(j) based on deltaBL and off-wall distance
            do k=kblStart,kblEnd
               if (dy(j,k).ge.deltaBL(j)) then
                  k_deltaBL(j) = k
                  k_d = k
                  k_dm = k-1
                  velBLEdge(j) = vel(j,k_dm) + 
     &                 (vel(j,k_d) - vel(j,k_dm))
     &                 * (deltaBL(j)-dy(j,k_dm))
     &                 / (dy(j,k_d)-dy(j,k_dm))
                  go to 541
               end if
            end do

            ! If dy > deltaBL is not found:
            deltaBL(j) = 0d0
            foundBLEdge(j) = .false.

            write(36,141) j
 141        format(
     &           'BLEdge DiagnosticFcn Error: dy = deltaBL not',
     &            ' found for j = ',I3)
            write(36,142) k_Fmax, dy(j,k_Fmax), deltaBL(j), Fmax
 142        format(1p,
     &           'k_Fmax = ',I3,' dy = ', e14.7,
     &           ' deltaBL = ',e14.7, ' Fmax = ',e14.7)
            
 541        continue

         end if ! (foundBLEdge(j))

      end do ! j-loop

      return
      end subroutine blEdge_DiagnosticFcn


      ! ================================================================
      ! ================================================================
      ! 
      subroutine blEdge_VorticityMethod(
     &     jdim, kdim, xy, xyj, x, y, fmu, vort, turmu,
     &     vel, uu, vv, p, rho, a, M, dy)
      ! 
      ! ================================================================
      ! ================================================================
      ! 
      ! Purpose: This subroutine will find the boundary layer edge
      !   using the vorticyt and shear stress method outlined by Cliquet
      !   and Arnal (2008) Baldwin-Lomax turbulence model. Reference:
      !   Nebel(2003).
      !
      ! On completion, the following global variables will be updated:
      ! foundBLEdge, k_deltaBL, delatBL, and velBLEdge 
      !
      ! ----------------------------------------------------------------
#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim
      double precision
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim),
     &     vel(jdim,kdim),
     &     uu(jdim,kdim),
     &     vv(jdim,kdim),
     &     p(jdim,kdim),
     &     rho(jdim,kdim),
     &     a(jdim,kdim),
     &     M(jdim,kdim),
     &     dy(jdim,kdim)

      ! ______________________________
      ! declare input/output variables
      ! -- none

      ! _______________________
      ! declare local variables
      integer
     &     j,
     &     k,
     &     k_deltaBLVort,
     &     k_deltaBLTauTotal,
     &     kdm1,
     &     k_velMax
      double precision
     &     tauLaminar(jdim,kdim),
     &     tauTurbulent(jdim,kdim),
     &     tauTotal(jdim,kdim),
     &     vortMax,
     &     tauTotalMax,
     &     vortMaxScaled,
     &     tauTotalMaxScaled,
     &     epsilonVorticity,
     &     epsilonTau,
     &     deltaBLVort,
     &     deltaBLTauTotal,
     &     vel_max

      write(36,*)
      write(36,*) 'Vorticity Method Output:'
      write(36,*) '---------------------------'

      do j=jblStart,jblEnd,jblinc

         ! -- Initialize variables
         vortMax = 0d0
         tauTotalMax = 0d0
         epsilonVorticity = 0.001d0
         epsilonTau = 0.015d0

         do k=1,kdim

            ! -- compute tauTotal (approximate total shear stress)
            tauLaminar(j,k) = fmu(j,k)*abs(vort(j,k))
            tauTurbulent(j,k)  = turmu(j,k)*abs(vort(j,k))
            tauTotal(j,k)  = tauLaminar(j,k) + tauTurbulent(j,k)

            ! -- find max vorticity
            if (vort(j,k).ge.vortMax) then
               vortMax = vort(j,k)
            endif

            ! -- find max total shear stress
            if (tauTotal(j,k).ge.tauTotalMax) then
               tauTotalMax = tauTotal(j,k)
            endif
            
         end do ! k

         ! -- find boundary layer thickness
         vortMaxScaled = abs(vortMax)*epsilonVorticity
         tauTotalMaxScaled = abs(tauTotalMax)*epsilonTau
         k_deltaBLVort = 0d0
         k_deltaBLTauTotal = 0d0
         do k=kblStart,kblEnd
            if (abs(vort(j,k)).le.vortMaxScaled) then
               deltaBLVort = dy(j,k)
               k_deltaBLVort = k
               go to 552 ! exit
            end if
         end do
 552     continue
         do k=kblStart,kblEnd
            if (abs(tauTotal(j,k)).le.tauTotalMaxScaled) then
               deltaBLTauTotal = dy(j,k)
               k_deltaBLTauTotal = k
               go to 553 ! exit 
            end if
         end do
 553     continue

         ! -- Take the minimum between deltaBLVort and deltaBLTauTotal
         if (deltaBLVort.le.deltaBLTauTotal) then
            k_deltaBL(j) = k_deltaBLVort
         else 
            k_deltaBL(j) = k_deltaBLTauTotal
         end if

         kdm1 = k_deltaBL(j) -1 

         if (k_deltaBL(j).ge.2) then
            ! Vorticity Method was successful, set flag
            foundBLEdge(j) = .true.

            if (deltaBLVort.le.deltaBLTauTotal) then
               ! -- Interpolate for deltaBL using off wall distance and
               ! -- vorticity
               deltaBL(j) = dy(j,kdm1)
     &              + (dy(j,k_deltaBL(j))-dy(j ,kdm1))
     &              * (vortMaxScaled-vort(j,k_deltaBL(j) -1))
     &              / (vort(j,k_deltaBL(j))-vort(j,kdm1))

            else 
               ! -- Interpolate for deltaBL using off wall distance and
               ! -- total shear stress
               deltaBL(j) = dy(j,kdm1)
     &              + (dy(j,k_deltaBL(j))-dy(j ,kdm1))
     &              * (tauTotalMaxScaled-tauTotal(j,k_deltaBL(j) -1))
     &              / (tauTotal(j,k_deltaBL(j))-tauTotal(j ,kdm1))

            end if 

            ! -- interpolation of velBLEdge(j) based on y (off wall distance)
            velBLEdge(j) = vel(j,kdm1)
     &           + (vel(j,k_deltaBL(j)) - vel(j ,kdm1))
     &           * (deltaBL(j)-dy(j,kdm1))
     &           / (dy(j,k_deltaBL(j)) -dy(j,kdm1))
         end if

         ! ___________
         ! Backup Plan:
         ! if vorticity method failed, use max velocity instead
         if (.not.foundBLEdge(j)) then

            write(36,151) j
 151        format('M3: Vorticity Method failed:',
     &           'k_deltaBL not found for j=', I3,
     &           '--> using max velocity instead')
            ! -- find the max velocity in the boundary layer
            vel_max = 0d0
            k_velMax = 0
            do k=kblStart,kblEnd
               if (vel(j,k).ge.vel_max) then
                  vel_max = vel(j,k)
                  k_velMax = k
               end if
            end do
            if (k_velMax.le.0) then
               foundBLEdge(j) = .false.
               stop 'method 3: k_velMax is zero!'
            else
               foundBLEdge(j) = .true.
               deltaBL(j) = dy(j,k_velMax)
               k_deltaBL(j) = k_velMax
               velBLEdge(j) = vel(j,k_deltaBL(j))
               write(36,152) velBLEdge(j),j
 152           format('M3: Max velocity = ', f10.8
     &              ,' at k_velMax = ',I3)
            end if

         end if

      end do ! j-loop

      return
      end subroutine blEdge_VorticityMethod

      
      ! ================================================================
      ! ================================================================
      ! 
      subroutine blProperties(
           !-- input
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
           !-- in/out
     &     dstar, theta, Hfactor, idstar, itheta, iHfactor,
     &     Re_x, Re_dstar, Re_theta, Pohlhausen)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will use the computed boundary layer
      !   edge to compute the various boundary layer properties
      !   necessary for transition prediction.
      !
      ! ----------------------------------------------------------------
#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim,
     &     ndim
      double precision
     &     q(jdim,kdim,ndim),
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim)

      ! ______________________________
      ! declare input/output variables
      double precision
     &     dstar(jdim),
     &     theta(jdim),
     &     Hfactor(jdim),
     &     idstar(jdim),
     &     itheta(jdim),
     &     iHfactor(jdim),
     &     Re_x(jdim),
     &     Re_dstar(jdim),
     &     Re_theta(jdim),
     &     Pohlhausen(jdim)

      ! _____________________________________________________
      ! declare output variables from getCurvilinearVariables
      double precision
     &     vel(jdim,kdim),
     &     uu(jdim,kdim),
     &     vv(jdim,kdim),
     &     p(jdim,kdim),
     &     rho(jdim,kdim),
     &     a(jdim,kdim),
     &     M(jdim,kdim),
     &     dy(jdim,kdim)

      ! _______________________
      ! declare local variables
      integer
     &     j,
     &     k,
     &     klimit,
     &     kdj,
     &     kdjm,
     &     kdt,
     &     kdtm1,
     &     jp1,
     &     jm1
      double precision
     &     vel_ratio(jdim,kdim),
     &     tmp,
     &     itmp,
     &     yj(kdim),
     &     f_dstar(kdim),
     &     f_theta(kdim),
     &     f_idstar(kdim),
     &     f_itheta(kdim),
     &     dtheory(jdim),
     &     dvelds(jdim),
     &     dvelds_1(jdim),
     &     dvelds_2(jdim),
     &     dvelds_3(jdim),
     &     dvelds_4(jdim),
     &     delx,
     &     dely,
     &     ds(jdim),
     &     velt(jdim,kdim),
     &     veltp1,
     &     veltm1,
     &     vel_inv,
     &     dveldxi,
     &     dveltdxi,
     &     dpds(jdim),
     &     dpdxi,
     &     denom,
     &     rhoBLEdge,
     &     fmuBLEdge
      ! _____________________
      ! Function declarations
      double precision trapezoidalIntegration
      double precision linearInterp

      ! __________________________________________________
      ! Compute variables in curvilinear coordinate system
      call getCurvilinearVariables(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     vel, uu, vv, p, rho, a, M, dy)

      ! Initialize properties
      select case (surfaceBL)
      case (1)
         do j=1,jdim
            dstar(j) = 0d0
            idstar(j) = 0d0
            theta(j) = 0d0
            itheta(j) = 0d0
            Hfactor(j) = 0d0
            iHfactor(j) = 0d0
            Re_x(j) = 0d0
            Re_dstar(j) = 0d0
            Re_theta(j) = 0d0
            dvelds(j) = 0d0
            Pohlhausen(j) = 0d0
         end do
      case (2)
         do j=jblSP,jtail1,jblinc
            dstar(j) = 0d0
            idstar(j) = 0d0
            theta(j) = 0d0
            itheta(j) = 0d0
            Hfactor(j) = 0d0
            iHfactor(j) = 0d0
            Re_x(j) = 0d0
            Re_dstar(j) = 0d0
            Re_theta(j) = 0d0
            dvelds(j) = 0d0
            Pohlhausen(j) = 0d0
         end do
      case default
         stop ' blProperties: surfaceBL assigned wrong value '
      end select ! surfaceBL

      ! RR: tangential velocity - currently unused
      do j=1,jdim
         do k=1,kdim
            denom = dsqrt(xy(j,k,3)*xy(j,k,3) + xy(j,k,4)*xy(j,k,4))
            velt(j,k) = (xy(j,k,4)*uu(j,k) - xy(j,k,3)*vv(j,k)) / denom
         end do
      end do

      ! ____________
      ! j-loop begin
      do j=jblStart,jblEnd,jblinc

         ! ________________________
         ! Preliminaries for each j
         jp1 = j+jblinc
         jm1 = j-jblinc
         delx = x(jp1,1)-x(j,1)
         dely = y(jp1,1)-y(j,1)
         ds(j) = dsqrt(delx*delx + dely*dely)
         
         ! ____________________________________________________________
         ! Compute integrands for both the compressible and
         ! incompressible displacement and momentum thicknesses.
         ! Note that although deltaBL lies somewhere in between node
         ! k_deltaBL and k_deltaBL -1, we do not include this edge point
         ! in the integration, since by definition: vel/velBLEdge = 1.
         do k=kblStart,kblEnd
            yj(k) = 0
            f_dstar(k) = 0
            f_theta(k) = 0
         end do

         if (foundBLEdge(j)) then

            kdj = k_deltaBL(j)
            kdjm = kdj -1
            klimit = kdjm
            itmp = 0
            tmp = 0

            ! interpolate the density and fluid viscosity at BL edge
            rhoBLedge = linearInterp(
     &           dy(j,kdjm),rho(j,kdjm),
     &           dy(j,kdj),rho(j,kdj),
     &           deltaBL(j))
            fmubledge = linearInterp(
     &           dy(j,kdjm),fmu(j,kdjm),
     &           dy(j,kdj),fmu(j,kdj),
     &           deltaBL(j))

            do k=kblStart,klimit
               yj(k) = dy(j,k)
               itmp = vel(j,k)/velBLEdge(j)
               tmp  = itmp*rho(j,k)/rhoBLEdge
               ! -- idstar = int{1 - u/velBLEdge}dy (from 0-->deltaBL)
               ! -- dstar  = int{1 - (rho*u)/(rhoBLEdge*velBLEdge)}dy (from 0-->deltaBL)
               f_idstar(k) = 1d0 - itmp
               f_dstar(k)  = 1d0 - tmp
               ! -- itheta = int{u/velBLEdge*(1 - u/velBLEdge)}dy (from 0-->deltaBL)
               ! -- itheta = int{(rho*u)/(rhoBLEdge*velBLEdge)*(1 - u/velBLEdge)}dy (from 0-->deltaBL)
               f_itheta(k) = itmp*(1d0 - itmp) 
               f_theta(k)  = tmp*(1d0 - itmp) 
               ! -- store ratio (for debugging) 
               vel_ratio(j,k) = tmp
               ! -- print out warning 
               if (itmp.gt.1d0) then
                  write(36,*)
     &                 'INT: warning vel/velBLEdge > 1.0, method = '
     &                 ,blEdgeMethod, 'vel/velBLEdge = ', tmp, 'j = ', j
     &                 , 'k = ',k
               end if 
            end do

!               if (j.eq.270) then
!               write(36,908)
! 908           format(
!     &              'j',T15   
!     &              'k',T30,
!     &              'uu', T45,
!     &              'vv',T60,
!     &              'vel', T75,
!     &              'velBLEdge',T90,
!     &              'vel_ratio')
!               do k=kblStart,klimit
!                  write(36,909) j, k, uu(j,k), vv(j,k), vel(j,k), 
!     &                 velBLEdge(j), vel_ratio(j,k)
!               end do
! 909           format(1p,
!     &              I3,T15,
!     &              I3,T30,
!     &              e14.7,T45,
!     &              e14.7,T60,
!     &              e14.7,T75,
!     &              e14.7,T90,
!     &              e14.7)
!               write(36,*)
!               end if
            

            ! ___________________________________________________
            ! Integrate for Incompressible Displacement Thickness
            idstar(j) = trapezoidalIntegration(
     &           kdim,yj,f_idstar,kblStart,klimit,1)
            ! _______________________________________________
            ! Integrate for Incompressible Momentum Thickness
            itheta(j) = trapezoidalIntegration(
     &           kdim,yj,f_itheta,kblStart,klimit,1)
            ! ___________________________
            ! Incompressible Shape Factor
            iHfactor(j) = idstar(j)/itheta(j)


            ! _________________________________________________
            ! Integrate for Compressible Displacement Thickness
            dstar(j) = trapezoidalIntegration(
     &           kdim,yj,f_dstar,kblStart,klimit,1)
            ! _____________________________________________
            ! Integrate for Compressible Momentum Thickness
            theta(j) = trapezoidalIntegration(
     &           kdim,yj,f_itheta,kblStart,klimit,1)
            ! _________________________
            ! Compressible Shape Factor
            Hfactor(j) = dstar(j)/theta(j)


            ! ____________________________________
            ! Reynolds number based chord position
            !Re_x(j) = rho(j,kdj)/fmu(j,kdj)*velBLEdge(j)*x(j,kdj)
            Re_x(j) = rhoBLEdge/fmuBLEdge*velBLEdge(j)*x(j,1)
            Re_x(j) = re*Re_x(j)
            ! _______________________________________________
            ! Reynolds number based on displacement thickness
!            Re_dstar(j) = rho(j,kdj)*velBLEdge(j)*dstar(j) /
!     &           fmu(j,kdj)        
            Re_dstar(j) = rhoBLEdge*velBLEdge(j)*dstar(j) /
     &           fmuBLEdge
            Re_dstar(j) = re*Re_dstar(j)
            ! _______________________________________________
            ! Reynolds number based on momentum thickness and
            ! free stream conditions (Re_theta)
            Re_theta(j) = rhoBLEdge*velBLEdge(j)*theta(j) /
     &           fmuBLEdge
            Re_theta(j) = re*Re_theta(j)
               

            ! ______________________________
            ! Pohlhausen Number (Pohlhausen)

            ! Step 1)
            ! -------
            ! Compute the tangential velocity gradient in the streamwise
            ! direction, dvelds

            ! Investigating 4 options here:
            ! Option 1: compute arc length along airfoil surface, ds, and
            !           finite difference the edge velocity by taking
            !           the average of the forward and backward difference.
            ! Option 2: Use dot product between gradient of edge velocity
            !           and the covariant basis vector, e1_hat
            ! Option 3: Compute tangential component of the edge velocity
            !           using dot product between the cartesian velocity
            !           vector and the covariant basis vector, e1_hat. Then
            !           use dot product between gradient of the tangential
            !           component of the edge velocity and the covariant basis
            !           vector, e1_hat.
            ! Option 4: Use first-order boundary layer theory to get dvelds from
            !           dpds at the wall. Compute dpds using the dot product
            !           between the gradient of the pressure at the wall and the
            !           covariant basis vector, e1_hat.

            ! Option 1
            if (.not.foundBLEdge(jm1)) then
               dvelds_1(j) = velBLEdge(jp1)-velBLEdge(j) / ds(j)
            else if (.not.foundBLEdge(jp1)) then
               dvelds_1(j) = velBLEdge(j)-velBLEdge(jm1) / ds(jm1)
            else
               dvelds_1(j) =
     &              0.5 * ( (velBLEdge(jp1) - velBLEdge(j))   / ds(j)
     &                     +(velBLEdge(j)   - velBLEdge(jm1)) / ds(jm1))
               end if

            ! Option 2
            dveldxi = 0.5*(velBLEdge(jp1)-velBLEdge(jm1))
            if (.not.foundBLEdge(jm1)) then
               dveldxi = velBLEdge(jp1)-velBLEdge(j)
            else if (.not.foundBLEdge(jp1)) then
               dveldxi = velBLEdge(j)-velBLEdge(jm1)
            end if
            denom = dsqrt(  xy(j,kdj,3)*xy(j,kdj,3)
     &                    + xy(j,kdj,4)*xy(j,kdj,4) )
            dvelds_2(j) =
     &           (xy(j,kdj,4)*xy(j,kdj,1)-xy(j,kdj,3)*xy(j,kdj,2))
     &           *dveldxi/denom
!
            ! Option 3
            if (.not.foundBLEdge(jm1)) then
               kdt = k_deltaBL(j)
               kdtm1 = kdt -1
               veltm1 = linearInterp(
     &              dy(j,kdtm1),velt(j,kdtm1),
     &              dy(j,kdt),velt(j,kdt),
     &              deltaBL(j))
               kdt = k_deltaBL(jp1)
               kdtm1 = kdt -1
               veltp1 = linearInterp(
     &              dy(jp1,kdtm1),velt(jp1,kdtm1),
     &              dy(jp1,kdt),velt(jp1,kdt),
     &              deltaBL(jp1))
               ! forward difference
               dveltdxi = jblinc*(veltp1 - veltm1)
            else if (.not.foundBLEdge(jp1)) then
               kdt = k_deltaBL(jm1)
               kdtm1 = kdt -1
               veltm1 = linearInterp(
     &              dy(jm1,kdtm1),velt(jm1,kdtm1),
     &              dy(jm1,kdt),velt(jm1,kdt),
     &              deltaBL(jm1))
               kdt = k_deltaBL(j)
               kdtm1 = kdt -1
               veltp1 = linearInterp(
     &              dy(j,kdtm1),velt(j,kdtm1),
     &              dy(j,kdt),velt(j,kdt),
     &              deltaBL(j))
               ! backward difference
               dveltdxi = jblinc*(veltp1 - veltm1)
            else
               kdt = k_deltaBL(jm1)
               kdtm1 = kdt -1
               veltm1 = linearInterp(
     &              dy(jm1,kdtm1),velt(jm1,kdtm1),
     &              dy(jm1,kdt),velt(jm1,kdt),
     &              deltaBL(jm1))
               kdt = k_deltaBL(jp1)
               kdtm1 = kdt -1
               veltp1 = linearInterp(
     &              dy(jp1,kdtm1),velt(jp1,kdtm1),
     &              dy(jp1,kdt),velt(jp1,kdt),
     &              deltaBL(jp1))
               ! centered-difference
               dveltdxi = 0.5*jblinc*(veltp1 - veltm1)
            end if
!            dveltdxi = 0.5*(velt(jp1,k_deltaBL(jp1))
!     &           -velt(jm1,k_deltaBL(jm1)))
            denom = dsqrt(  xy(j,kdj,3)*xy(j,kdj,3)
     &                    + xy(j,kdj,4)*xy(j,kdj,4) )
            dvelds_3(j) =
     &           (xy(j,kdj,4)*xy(j,kdj,1)-xy(j,kdj,3)*xy(j,kdj,2))
     &           *dveltdxi/denom

!            ! Option 4
!            dpdxi = 1d0/12d0*(p(j-2,1)-8*p(j-1,1)
!     &           + 8*p(j+1,1)-p(j+2,1))
            dpdxi = 0.5*(p(jp1,1)-p(jm1,1))
            denom = dsqrt(  xy(j,1,3)*xy(j,1,3)
     &                    + xy(j,1,4)*xy(j,1,4) )
            dpds(j) = (xy(j,1,4)*xy(j,1,1)-xy(j,1,3)*xy(j,1,2))*dpdxi
     &           /denom                                          
            dvelds_4(j) = -dpds(j) / (rhoBLEdge*velBLEdge(j))


            ! Step 2) 
            ! -------
            ! Compute the Pohlhausen number based on dvelds
            
            ! Using option 4
            dvelds(j) = dvelds_4(j)
!            Pohlhausen(j) = rho(j,kdj)/fmu(j,kdj)*theta(j)*theta(j)
!     &           *dvelds(j)
            Pohlhausen(j) = rhoBLEdge/fmuBLEdge*theta(j)*theta(j)
     &           *dvelds(j)
            
            Pohlhausen(j) = re*Pohlhausen(j)

         end if ! (foundBLEdge(j)) 

      end do ! j-loop

! RR: debug write statements
!      write(36,161)
! 161  format(
!     &     'j',T15   
!     &     'velBLEdge',T30,
!     &     'velt', T45,
!     &     'dvelds_1',T60,
!     &     'dvelds_2', T75,
!     &     'dvelds_3',T90,
!     &     'dvelds_4')
!      do j=jblStart,jblEnd,jblinc
!         write(36,162) j, velBLEdge(j), velt(j,k_deltaBL(j)),
!     &        dvelds_1(j), dvelds_2(j), dvelds_3(j), dvelds_4(j)
!      end do
! 162  format(1p,
!     &     I3,T15,
!     &     e14.7,T30,
!     &     e14.7,T45,
!     &     e14.7,T60,
!     &     e14.7,T75,
!     &     e14.7,T90,
!     &     e14.7)
!      write(36,*)

      return
      end subroutine blProperties

