c ********************************************************************
c *************  compute eigenvalues *********************************
c ********************************************************************
      subroutine eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,tmet,uu,cc,
     &           jbegin,jend,kbegin,kend)

      implicit none
c                                                                       
c     ix = 1  iy = 2 for xi   direction  metrics                           
c     ix = 3  iy = 4 for eta  direction  metrics                           
c                                                                       
#include "../include/common.inc"
c 
      integer ix,iy,jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)    
      double precision xy(jdim,kdim,4),tmet(jdim,kdim),xyj(jdim,kdim)
      double precision uu(jdim,kdim),cc(jdim,kdim),ri,u,v                    
c                                                                       
c     compute spectral radius scaling                                     
c                                                                       
      do 100 k=kbegin,kend                                              
      do 100 j=jbegin,jend                                              
c       ri is j/rho                                                    
        ri = 1./q(j,k,1)                                               
        u = q(j,k,2)*ri                                                
        v = q(j,k,3)*ri                                                
c                                                                       
c       note: time metrics put in (sloppy but they work)      
c     
        uu(j,k) = tmet(j,k) + u*xy(j,k,ix)+v*xy(j,k,iy)                
        cc(j,k) = sndsp(j,k)*sqrt(xy(j,k,ix)**2+xy(j,k,iy)**2)
  100 continue
c                                                                       
      return                                                  
      end                                                     
