      subroutine specty(jdim,kdim,vv,ccy,xyj,xy,spect,precon)   
c                                                                       
c     For ispec = -1   spect = 64./xyj                                  
c     For ispec = 0    spect = 1./xyj                                   
c     For ispec = 1  form  spect independently                           
c         spect = (abs(vv) + ccy)/xyj                                    
c     For ispec = 3  form  spect independently with limits
c         spect = (abs(vv) + ccy)/xyj                                    
c     For ispec = 4                                                     
c         spect = (abs(xy(3)+abs(xy(4)) /xyj                             
c                                                                       
#include "../include/arcom.inc"
c                                                               
      dimension xyj(jdim,kdim),xy(jdim,kdim,4),precon(jdim,kdim,6)      
      dimension spect(jdim,kdim),vv(jdim,kdim),ccy(jdim,kdim)  
       
c                                                                       
c     compute dissipation scaling                                     
c                                                                       
      if(ispec.eq.0)then                                                
c       -constant                                                        
        do 50 k = kbegin,kend
        do 50 j = jbegin,jend
          rj = 1./xyj(j,k)
          spect(j,k) = rj
 50     continue
c                                                                       
      elseif(ispec.eq.1)then                                       
c       -spectral radius                                                
        if (prec.eq.0) then
          do 100 k=kbegin,kend
          do 100 j=jbegin,jend
            sigy = abs(vv(j,k)) + ccy(j,k)
            rj = 1./xyj(j,k)           
            spect(j,k) = sigy*rj
 100      continue
        else
          do 1000 k=kbegin,kend
          do 1000 j=jbegin,jend
            sigy=abs(precon(j,k,5))+precon(j,k,6)
            rj=1.0/xyj(j,k)
            spect(j,k)=sigy*rj
 1000     continue
        endif
c     
      elseif(ispec.eq.3)then                                         
c       -ispec = 3 use limit specral radii                                    
        do 200 k=kbegin,kend                                         
          do 250 j=jbegin,jend
            sigy = abs(vv(j,k)) + ccy(j,k)
            rj = 1./xyj(j,k)
            spect(j,k) = sigy
 250      continue
c     
          do 260 j=jbegin,jend
            if(spect(j,k).lt.21.)spect(j,k)=21.
            if(spect(j,k).gt.1000.)spect(j,k)=1000.
 260      continue
c     
          do 270 j=jbegin,jend
            rj = 1./xyj(j,k)
            spect(j,k) = spect(j,k)*rj
 270      continue
 200    continue                                                     
c                                                                       
c                                                                       
      elseif(ispec.eq.4)then                                       
c                                                                       
c       -spectral radius                                                      
        do 400 k=kbegin,kend                                         
        do 400 j=jbegin,jend                                         
          sigy = abs(xy(j,k,3)) + abs(xy(j,k,4))                       
          rj = 1./xyj(j,k)                                             
          spect(j,k) = sigy*rj                                         
 400    continue                                                     
c                                                                       
      elseif(ispec.eq.-1)then                          
c                                                                       
        do 500 k=kbegin,kend                             
        do 500 j=jbegin,jend                             
          spect(j,k) = 64./xyj(j,k)                        
 500    continue                                         
c                                                                       
      endif                                                           
c                                                                       
      return                                                  
      end                                                     
