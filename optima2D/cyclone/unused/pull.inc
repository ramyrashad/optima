         do k=2,kdim-1
            do j=2,jdim-1
c
                d_p_bar(j,k)=(max(d1(j,k)-p1(j,k),0.0)+
     $               max(d_prime(j,k)-p_prime(j,k),0.0)*
     $               turre(j,k))
c
                denom(j,k)=(1.0+dtm*2.*(cx(k,j)+cy(j,k)+d_p_bar(j,k))
     >                     /3.)
c                denom(j,k)=(1.0+dtm*(cx(k,j)+cy(j,k)+d_p_bar(j,k)))
                bx(k,j) = bx(k,j)*dtm*2./3./denom(j,k)
c                bx(k,j) = bx(k,j)*dtm/denom(j,k)
                cx(k,j) = 1.0
                dx(k,j) = dx(k,j)*dtm*2./3./denom(j,k)
                by(j,k) = by(j,k)*dtm*2./3./denom(j,k)
c                dx(k,j) = dx(k,j)*dtm/denom(j,k)
c                by(j,k) = by(j,k)*dtm/denom(j,k)
                cy(j,k) = 1.0
                dy(j,k) = dy(j,k)*dtm*2./3./denom(j,k)
c                dy(j,k) = dy(j,k)*dtm/denom(j,k)
c
              enddo
            enddo
c         
c            do k=1,kdim
c               do j=1,jdim
c                  turre2(j,k)=turre(j,k)
c               enddo
c            enddo
c
c            do j=1,jdim
c               do k=1,kdim
c                 ffy(j,k)=0.0
c                 ffx(k,j)=0.0
c                 fx(k,j)=0.0
c               enddo
c            enddo
c
c         resid_max=1.e-6
c         residual=100.
cc         negn=100
c         isub=1
c
         if (isub .eq. 1) then
      	    print *,' '
332   	    format(8Hnumiter=,I7,2x,7Hjsubit=,I3)
      	    write(6,332) numiter,jsubit
         endif
c         do 501 while ((residual.gt.resid_max).and.(isub.le.nnit))
            do k=2,kdim-1
              do j=2,jdim-1
                ttt=turre(j,k)+(-4.*turre1(j,k)+turold(j,k))/3.d0
                tt=p(j,k)-d(j,k)+trip(j,k)
c
                fby(j,k) = ((fy(j,k)+tt)*(2./3.)*dtm - ttt)/denom(j,k)
              enddo
            enddo
c
            tot_res=0.0
            do k=2,kdim-1
            do j=2,jdim-1
               tot_res=tot_res+fby(j,k)**2               
            enddo
            enddo
c
            call triv(maxj,maxk,2,jdim-1,2,kdim-1,work,by,cy,dy,fby)
c
            do k=2,kdim-1
              do j=2,jdim-1
                 fx(k,j) = fby(j,k)
c                 ffy(j,k) = fby(j,k)
              enddo
           enddo
c
           call triv(maxk,maxj,2,kdim-1,2,jdim-1,work,bx,cx,dx,fx)
c
           negn = 0
c           resid_term1=0.0
c           resid_term2=0.0
c           tot_res=0.0
c
           do k=2,kdim-1
              do j=2,jdim-1
                 turre(j,k) = turre(j,k) + fx(k,j)
c                 turre(j,k) = turre2(j,k) + fx(k,j)
                 if(turre(j,k).lt. 0.0)then
                    negn        =   negn + 1
                    turre(j,k)  =    0.0
                    fx(k,j)     =    0.0
                 endif
c                 tot_res=tot_res+(fx(k,j))**2
c                 resid_term1=resid_term1+(fx(k,j)-ffx(k,j))**2
c                 resid_term2=resid_term2+(fx(k,j))**2
c                 ffx(k,j)=fx(k,j)
              enddo
           enddo
           if (negn .ne. 0) print *,'negn=',numiter,jsubit,negn
           tot_res=sqrt(tot_res/float((jdim-1)*(kdim-1)))
           tot_res=tot_res/dtm
           if(isub.eq.1) tot_res2=tot_res
c
c           residual1=sqrt(resid_term1)
c           residual2=sqrt(resid_term2)
c           residual=residual1/max(residual2,1.e-10)
c
c
c           if (residual .le. resid_max) then
c           print *,'isub and residual=',isub,residual
c           endif
c           isub=isub+1
c
c 501  continue
        
