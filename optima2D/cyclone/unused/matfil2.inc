      jp4=j+4
      jp3=j+3
      jp2=j+2
      jp1=j+1
      jm1=j-1
      jm2=j-2
      jm3=j-3
      jm4=j-4
      rho = q(j,k,1)*xyj(j,k)                                        
      uu = xy(j,k,1)*u(j,k) + xy(j,k,2)*v(j,k) + xit(j,k)
c
      if (j.eq.ja) then
         uxi= (-25.*u(j,k)+48.*u(jp1,k)-36.*u(jp2,k)+16.*u(jp3,k)-
     &                        3.*u(jp4,k))*tp
         vxi= (-25.*v(j,k)+48.*v(jp1,k)-36.*v(jp2,k)+16.*v(jp3,k)-
     &                        3.*v(jp4,k))*tp
      elseif(j.eq.ja+1) then
         uxi= (-6.*u(jm1,k)-20.*u(j,k)+36.*u(jp1,k)-12.*u(jp2,k)+
     &                        2.*u(jp3,k))/24.
         vxi= (-6.*v(jm1,k)-20.*v(j,k)+36.*v(jp1,k)-12.*v(jp2,k)+
     &                        2.*v(jp3,k))/24.
      elseif(j.eq.jb-1) then
         uxi= (-2.*u(jm3,k)+12.*u(jm2,k)-36.*u(jm1,k)+20.*u(j,k)+
     &                        6.*u(jp1,k))/24.
         vxi= (-2.*v(jm3,k)+12.*v(jm2,k)-36.*v(jm1,k)+20.*v(j,k)+
     &                        6.*v(jp1,k))/24.
      elseif(j.eq.jb) then
         uxi= (25.*u(j,k)-48.*u(jm1,k)+36.*u(jm2,k)-16.*u(jm3,k)+
     &                        3.*u(jm4,k))*tp
         vxi= (25.*v(j,k)-48.*v(jm1,k)+36.*v(jm2,k)-16.*v(jm3,k)+
     &                        3.*v(jm4,k))*tp
      endif
c     
      ff = rho*uu*( xy(j,k,3)*uxi+xy(j,k,4)*vxi)                     
      c1 = xy(j,k,3)*xy(j,k,1) + xy(j,k,2)*xy(j,k,4)                 
      c2 = xy(j,k,3)**2 + xy(j,k,4)**2                               
      p2 = press(j,2)*xyj(j,2)
      p3 = press(j,3)*xyj(j,3)
      p4 = press(j,4)*xyj(j,4)
      p5 = press(j,5)*xyj(j,5)
c     For (dp/dn = 0) use following two lines
c      f(j)=-ff-c2*p2
c      c(j)=-c2
c
c     Otherwise use 3rd order extrapolation of pressure.
      f(j)=-ff-c2*tp*(48.*p2 - 36.*p3 + 16*p4 - 3.*p5)
      c(j)=-25.*tp*c2
c
      a(j)=c1*tp                                                  
      b(j)=-8.*tp*c1                                        
      d(j)=-b(j)
      e(j)=-a(j)
