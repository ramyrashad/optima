c        ***********************
c        F_eta_eta Viscous Terms
c        ***********************
c        
         do k=klow,kup
            kp1 = k+1
            km1 = k-1
            do j=jlow,jup
               xy3p     = .5*(xy(j,k,3)+xy(j,kp1,3))
               xy4p     = .5*(xy(j,k,4)+xy(j,kp1,4))
               ttp      =  (xy3p*xy(j,k,3)+xy4p*xy(j,k,4))
               
               xy3m     = .5*(xy(j,k,3)+xy(j,km1,3))
               xy4m     = .5*(xy(j,k,4)+xy(j,km1,4))
               ttm      =  (xy3m*xy(j,k,3)+xy4m*xy(j,k,4))
               
               cnud=cb2*resiginv*work(j,k)
               
               cdp       =    ttp*cnud
               cdm       =    ttm*cnud
               
               trem =.5*(work(j,km1)+work(j,k))
               trep =.5*(work(j,k)+work(j,kp1))
         
               cap  =  ttp*trep*(1.0+cb2)*resiginv
               cam  =  ttm*trem*(1.0+cb2)*resiginv
               
               by(j,k)   = cdm-cam
               cy(j,k)   = -cdp+cap-cdm+cam
               dy(j,k)   = cdp-cap
               
               fy(j,k)   = - by(j,k)*turre(j,km1)
     &              - cy(j,k)*turre(j,k  )
     &              - dy(j,k)*turre(j,kp1)
               
            enddo
         enddo
c     
