c     ------------------------------------------------------------------
c     -- display jacobian values --
c     -- m. nemec, sept. 2001 --
c     ------------------------------------------------------------------
      subroutine fd_vs_a( jvn, kvn, jmax, kmax, nmax, indx, pf, jpf,
     &     ipf, ipa, jpa, pa, js, je, ks, ke)

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/units.inc"

      integer strlen

      dimension indx(jmax,kmax)
      dimension ipf(*), jpf(*), pf(*)
      dimension ipa(*), jpa(*), pa(*)
      
      i1 = ( indx(jvn,kvn) - 1 )*nmax + 1
      i2 = ( indx(jvn,kvn) - 1 )*nmax + nmax

      write (*,*) 'node: ',jvn,kvn,i1,i2

      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+6) = '.jaca'
      open(unit=n_all,file=filena,status='unknown')
      do i=i1, i2
         write(n_all,200) i
         k1 = ipa(i)
         k2 = ipa(i+1)-1
         write (n_all,201) (jpa(k),pa(k),k=k1,k2)
      end do
      close(n_all)

      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+7) = '.jacfd'
      open(unit=n_all,file=filena,status='unknown')
      do i=i1, i2
         write(n_all,200) i
         k1 = ipf(i)
         k2 = ipf(i+1)-1
         write (*,*) 'k1 k2',k1,k2
         write (n_all,201) (jpf(k),pf(k),k=k1,k2)
      end do
      close(n_all)
      
      do j=js,je
         do k=ks,ke
            i1 = ( indx(j,k) - 1 )*nmax + 1
            i2 = ( indx(j,k) - 1 )*nmax + nmax
            do i=i1, i2

               ka1 = ipa(i)
               ka2 = ipa(i+1)-1

               kf1 = ipf(i)
               kf2 = ipf(i+1)-1
               
               do m = ka1,ka2
                  if (abs(pa(m)).gt.1.e-10) then
                     do n = kf1,kf2
                        if (jpf(n).eq.jpa(m)) then
                           if ( abs(pf(n)-pa(m))/pf(n) .gt. 1.e-1 ) then
                              write (*,*) 'problem',j,k,jpa(m)
                           end if
                           goto 25
                        end if
                     end do
                  else
                     do n = kf1,kf2
                        if (jpf(n).eq.jpa(m) .and. abs(pf(n)).gt.1.e-10)
     &                       write (*,300) j, k, jpa(m), pf(n) 
                     end do
                  end if
 25               continue
               end do
            end do
         end do
      end do

 300  format('fd has a value, but no value in as',3i5,e12.5)

 200  format (1h ,34(1h-),' row',i5,1x,34(1h-),/
     *     3('  columns :     values   * ') )
c     -- xiiiiiihhhhhhdddddddddddd-*- --
 201  format(3(1h ,i6,5h   : ,D12.5,3h * ) ) 

      

      stop

      return
      end                       !fd_vs_a
