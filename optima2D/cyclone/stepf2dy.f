      subroutine stepf2dy(jdim,kdim,q,turmu,fmu,s,press,sndsp,xy,xyj,x,
     &      y,ds,coef2,coef4,vv,cc,work,spect,precon,akk)        
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension ds(jdim,kdim),turmu(jdim,kdim),fmu(jdim,kdim)           
c                                                                       
      dimension spect(jdim,kdim,3),precon(jdim,kdim,6)
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension vv(jdim,kdim),cc(jdim,kdim)
c
      dimension work(jdim,kdim,10)          
      logical addvisc
      parameter (prlam = .72d0)                            
c
c     CUSP dissipation variables:
      integer en, nlow
      double precision swp1,swm1,cp1,cm1,javep1,javem1
      double precision akk
c
      addvisc=.false.
      dtd   = dt * thetadt/(1. + phidt)                                 
      hd    = 0.5*dtd                                                   
c   
c     eta inversion part of diagonal scheme                            
c
      if (idmodel.eq.2) then
        if ( iord.eq.4 .and. visceig.eq.1.0 ) then
c         -increase limiters in matrix dissipation to improve robustness
          if (vleta.lt.0.3) then
            vltmp=vleta
            vleta=0.3
            call mspecty(jdim,kdim,q,vv,cc,xyj,xy,spect,sndsp)
            vleta=vltmp
          endif 
        else if (idmodel.eq.2 .and. cmesh) then
          call mspecty(jdim,kdim,q,vv,cc,xyj,xy,spect,sndsp)
        endif
      endif
c     
c     -if CUSP dissipation and full limiters -> must loop over all
c      state variables
      if (idmodel.eq.4 .and. limiter.gt.3) then
        nlow = 1
      else 
        nlow = 2                !do n = 1 and 2 together  
      endif
c     
      if(viscous .and. viseta) then
        addvisc=.true.
c       -hd/re is original
        hre = dtd*visceig/re
c       -increase viscous eigenvalue          
c        hre = dtd/re*gamma/0.72
        do 4000 k = kbegin,kend
        do 4000 j = jlow,jup
          rinv = 1./q(j,k,1)
c         -eta_x**2 + eta_y**2                                     
          etamet = xy(j,k,3)**2 + xy(j,k,4)**2 
          djac = hre/xyj(j,k)
          work(j,k,8) = djac*etamet
c         -xyj/rho                                                  
          work(j,k,9) = rinv
 4000   continue
c
        do 4100 k = kbegin,kup
          kp1=k+1
          do 4050 j = jlow,jup
            vnum = 0.5*(fmu(j,k)+fmu(j,kp1)) + turmu(j,k)
            work(j,k,10) = 0.5*(work(j,kp1,8)+work(j,k,8))*vnum
 4050     continue
 4100   continue
      endif
c                                                                       
c
      do n = nlow,4 
        if ( n .eq. 1) sn = 0. 
        if ( n .eq. 2) sn = 0.d0                                 
        if ( n .eq. 3) sn = 1.d0                                
        if ( n .eq. 4) sn = -1.d0
        nm=n-1
c     
        if (idmodel.eq.1) then
c         ***************************************************************
c         **                       JST diss model                      **
c         ***************************************************************
c         -boundaries first
          k   = klow                                               
          km1 = k-1                                                 
          do j = jlow,jup                                      
            c2m = coef2(j,km1)*dtd                                  
            c4m = coef4(j,km1)*dtd                                  
            c2 = coef2(j,k)*dtd                                  
            c4 = coef4(j,k)*dtd                               
            work(j,k,1) =  0.d0
            work(j,k,2) = -(c2m + c4m + c4)*xyj(j,k-1)               
            work(j,k,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)             
            work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)             
            work(j,k,5) = xyj(j,k+2)*c4                             
          enddo
c     
          k   = kup                                                 
          km1 = k-1                                                 
          do j = jlow,jup                                       
            c2m = coef2(j,km1)*dtd                                  
            c4m = coef4(j,km1)*dtd                                  
            c2 = coef2(j,k)*dtd                                  
            c4 = coef4(j,k)*dtd                                   
            work(j,k,1) =  xyj(j,k-2)*c4m                           
            work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,k-1)          
            work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)           
            work(j,k,4)  = -(c2 + c4 + c4m)*xyj(j,k+1)             
            work(j,k,5)  = 0.                                        
          enddo
c     
          do k = klow+1,kup-1                                    
            km1 = k-1                                                
            do j = jlow,jup                                      
              c2m = coef2(j,km1)*dtd                                 
              c4m = coef4(j,km1)*dtd                                
              c2 = coef2(j,k)*dtd                                  
              c4 = coef4(j,k)*dtd                                 
              work(j,k,1)  =  xyj(j,k-2)*c4m                        
              work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,k-1)        
              work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)         
              work(j,k,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)          
              work(j,k,5)  = xyj(j,k+2)*c4                          
            enddo
          enddo
        else if (idmodel.eq.2) then
c         **************************************************************
c         **                    matrix diss model                     **
c         **************************************************************
c         -dissipation at first and last interior nodes different
c          for ord=2 and ord=4 code.
          do k=kbegin,kup
            kp1=k+1
            do j=jbegin,jend
              work(j,k,7)=dtd*(coef2(j,k)*spect(j,k,nm)
     &                       + coef2(j,kp1)*spect(j,kp1,nm))
              work(j,k,8)=dtd*(coef4(j,k)*spect(j,k,nm)
     &                       + coef4(j,kp1)*spect(j,kp1,nm))
            enddo
          enddo

          if (iord.eq.2) then
            k   = klow                                         
            km1 = k-1                                             
            kp1 = k+1
            do j = jlow,jup                                   
              c2m = work(j,km1,7) 
              c2 = work(j,k,7)
              c4m = work(j,km1,8)
              c4 = work(j,k,8)
c     
              work(j,k,1) =  0.                                   
              work(j,k,2) = -(c2m + c4m + c4)*xyj(j,km1)       
              work(j,k,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)      
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(j,kp1)      
              work(j,k,5) = xyj(j,k+2)*c4                   
            enddo
c     
            k   = kup                                               
            km1 = k-1                                     
            kp1 = k+1
            do j = jlow,jup                                    
              c2m = work(j,km1,7) 
              c2 = work(j,k,7)
              c4m = work(j,km1,8)
              c4 = work(j,k,8)
c     
              work(j,k,1) =  xyj(j,k-2)*c4m
              work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)   
              work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)    
              work(j,k,4)  = -(c2 + c4 + c4m)*xyj(j,kp1)
              work(j,k,5)  = 0.d0                               
            enddo
          else                  !4th-order
            k   = klow                                            
            km1 = k-1                                              
            kp1 = k+1
            do j = jlow,jup                                     
              c2m = work(j,km1,7) 
              c2 = work(j,k,7)
              c4m = work(j,km1,8)
              c4 = work(j,k,8)
c
              work(j,k,1) =  0.d0                                 
              work(j,k,2) = -(c2m + c4)*xyj(j,km1)             
              work(j,k,3) = xyj(j,k)*(c2m+c4m+c2+2.d0*c4)      
              work(j,k,4) = -(c2 + c4 + 2.d0*c4m)*xyj(j,kp1)    
              work(j,k,5) = xyj(j,k+2)*c4m                     
            enddo
c     
            k   = kup                                              
            km1 = k-1                                     
            kp1 = k+1
            do j = jlow,jup                                    
              c2m = work(j,km1,7) 
              c2 = work(j,k,7)
              c4m = work(j,km1,8)
              c4 = work(j,k,8)
c
              work(j,k,1) =  xyj(j,k-2)*c4m
              work(j,k,2)  = -(c2m + 2.*c4m + c4)*xyj(j,km1)
              work(j,k,3)  = xyj(j,k)*(c2m+c4m+c2+2.d0*c4)      
              work(j,k,4)  = -(c2 + c4)*xyj(j,kp1)             
              work(j,k,5)  = 0.                                  
            enddo
          endif                 !(iord.eq.2)
c     
c         *** fourth-difference in interior for both order 2 & 4 ***
          do k = klow+1,kup-1                                   
            km1 = k-1
            kp1 = k+1
            do j = jlow,jup                                      
              c2m = work(j,km1,7) 
              c2 = work(j,k,7)
              c4m = work(j,km1,8)
              c4 = work(j,k,8)
c     
              work(j,k,1)  =  xyj(j,k-2)*c4m                        
              work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)        
              work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)          
              work(j,k,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,kp1)          
              work(j,k,5)  = xyj(j,k+2)*c4                          
            enddo
          enddo
        else if (idmodel.eq.4) then 
c         **************************************************************
c         **                     cusp diss model                      **
c         **************************************************************
c         -dissipation for interior nodes
          if ( n .ge. 2 ) then
            en = nm
          else
            en = n
          endif

c         -for full limiting, must recalculate the limiter values.
c         -this is a speed penalty, but saves memory.
c         -the full limiters are slow anyway, using precon for temp
c          storage.
          if (limiter.gt.3 .and. prec.eq.0) 
     &       call dclimy (jdim,kdim,q,xyj,x,y,precon,press,(n),coef2)

          do k = klow+1,kup-1
          do j = jlow,jup
            kp1 = k + 1
            km1 = k - 1
            swp1 = coef2(j,k)
            swm1 = coef2(j,km1)
            cp1 = spect(j,k,en)
            cm1 = spect(j,km1,en)
            javep1 = (xyj(j,kp1)+xyj(j,k))*0.25d0*dtd
            javem1 = (xyj(j,km1)+xyj(j,k))*0.25d0*dtd
            work(j,k,1) = 0.5d0*swm1*cm1*javem1
            work(j,k,2) = - ( 0.5d0*cp1*swp1*javep1 + 
     &            ( 1.d0+ 0.5d0*swm1 )*cm1*javem1 )
            work(j,k,3) = ( 1.d0 + 0.5d0*swp1 )*javep1*cp1 +
     &            ( 1.d0 + 0.5d0*swm1 )*javem1*cm1
            work(j,k,4) = - ( javem1*0.5d0*cm1*swm1 +
     $            javep1*( 1.d0 + 0.5d0*swp1 )*cp1 )
            work(j,k,5) = 0.5d0*swp1*cp1*javep1
          enddo
          enddo
c
c         -boundary conditions
          k = klow
          kp1 = k + 1
          km1 = k - 1
          do j = jlow,jup
            swp1 = coef2(j,k)
            swm1 = coef2(j,km1)
            cp1 = spect(j,k,en)
            cm1 = spect(j,km1,en)
            javep1 = (xyj(j,kp1)+xyj(j,k))*0.25d0*dtd
            javem1 = (xyj(j,km1)+xyj(j,k))*0.25d0*dtd
            work(j,k,1) =  0.d0
            work(j,k,2) = - ( 0.5d0*swp1*cp1*javep1
     $            + cm1*javem1*( 1.d0 - 0.5d0*swm1 ) )
            work(j,k,3) = ( 1.d0 + 0.5d0*swp1 )*cp1*javep1 +
     &            cm1*javem1
            work(j,k,4) = - ( ( 1.d0 + 0.5d0*swp1 )*cp1*javep1 +
     &            0.5d0*swm1*cm1*javem1 )
            work(j,k,5) = 0.5d0*swp1*cp1*javep1
          enddo
c     
          k = kup
          kp1 = k + 1
          km1 = k - 1
          do j = jlow,jup                                         
            swp1 = coef2(j,k)
            swm1 = coef2(j,km1)
            cp1 = spect(j,k,en)
            cm1 = spect(j,km1,en)
            javep1 = (xyj(j,kp1)+xyj(j,k))*0.25d0*dtd
            javem1 = (xyj(j,km1)+xyj(j,k))*0.25d0*dtd
            work(j,k,1) =  0.5d0*swm1*cm1*javem1
            work(j,k,2) = - ( 0.5d0*swp1*cp1*javep1 + cm1*javem1
     $            *( 1.d0 + 0.5d0*swm1 ) )
            work(j,k,3) = cp1*javep1  + cm1*javem1*( 1.d0 +
     &            0.5d0*swm1 ) 
            work(j,k,4) = - ( cp1*javep1*( 1.d0 - 0.5d0*swp1 ) +
     &            0.5d0*swm1*cm1*javem1 )
            work(j,k,5) = 0.d0
          enddo
        endif                   !idmodel
c     
c       ***************************************
c       **  Add approx. viscous eigenvalues  **
c       ***************************************
        if (addvisc) then
cdu       -apply a linearization of the preconditioner to the
cdu        viscous terms
c
          if (prec.gt.0) then
            if (n.eq.3) then
              do 1900 k=kbegin,kup
              do 1900 j=jlow,jup
                duterm=((precon(j,k,1)-1.0)*
     +                (precon(j,k,5)-vv(j,k))/
     +                precon(j,k,6)+precon(j,k,1)+1.0)/2.0
                work(j,k,9)=work(j,k,9)*duterm
 1900         continue
            elseif (n.eq.4) then
              do 2900 k=kbegin,kup
              do 2900 j=jlow,jup
                duterm=((-precon(j,k,1)+1.0)*
     +                (precon(j,k,5)-vv(j,k))/
     +                precon(j,k,6)+precon(j,k,1)+1.0)/2.0
                work(j,k,9)=work(j,k,9)*duterm
 2900         continue
            endif
          endif
c     
          do 3200 k = klow,kup
            km1=k-1
            kp1=k+1
            do 3100 j = jlow,jup
c           -turb. viscosity                                       
              ctm1 = work(j,km1,10)
              ctp1 = work(j,k,10)
              cttt = ctm1 + ctp1                                    
              work(j,k,2) = work(j,k,2) - work(j,km1,9)*ctm1        
              work(j,k,3) = work(j,k,3) + work(j,k,9)*cttt          
              work(j,k,4) = work(j,k,4) - work(j,kp1,9)*ctp1        
 3100       continue
 3200     continue
        endif                   !viscous 
c     
        fct=1.d0
csi     add terms into lhs for time
c       -- add this factor now so its gets scaled by ds(j,k) --
        if (ismodel.gt.1 .and. meth.eq.1) then
           if (jesdirk.eq.4 .or. jesdirk.eq.3)then
              fct=1.0*dt/(dt2*akk)
           else
              fct=1.5*dt/dt2
           endif
           do 530 k=klow,kup
           do 530 j=jlow,jup
              work(j,k,3) = work(j,k,3) + fct
 530       continue
c     -- get fct ready for unscaled contribution
           fct=1.d0
           if (ismodel.eq.2) then
              if (jesdirk .eq.4 .or. jesdirk .eq. 3) then
                 fct=1.0d0/(akk*dt2)
              else
                 fct=1.5d0/dt2
              endif
           endif
        endif
c     
c       -add in flux jacobians, variable dt and identity
        if ( (prec.eq.0) .or. (n.eq.2) ) then
          if (iord.eq.4) then
            thd=dtd/12.d0
            do 531 k = klow+1,kup-1
            do 531 j = jlow,jup
              work(j,k,1)= work(j,k,1) + (vv(j,k-2)+sn*cc(j,k-2))*thd
              work(j,k,2)= work(j,k,2)-8.*(vv(j,k-1)+sn*cc(j,k-1))*thd
              work(j,k,4)= work(j,k,4)+8.*(vv(j,k+1)+sn*cc(j,k+1))*thd
              work(j,k,5)= work(j,k,5) - (vv(j,k+2)+sn*cc(j,k+2))*thd 
              work(j,k,1)= work(j,k,1)*ds(j,k)                        
              work(j,k,2)= work(j,k,2)*ds(j,k)                       
              work(j,k,3)= work(j,k,3)*ds(j,k)                       
              work(j,k,3)= work(j,k,3) + fct
              work(j,k,4)= work(j,k,4)*ds(j,k)                       
              work(j,k,5)= work(j,k,5)*ds(j,k)                        
 531        continue
c
c           using third order for boundary
            thd=dtd/6.d0
            do 532 j=jlow,jup
              k=klow
              work(j,k,2)= work(j,k,2)-2.*(vv(j,k-1)+sn*cc(j,k-1))*thd
              work(j,k,3)= work(j,k,3) -   3.*(vv(j,k)+sn*cc(j,k))*thd
              work(j,k,4)= work(j,k,4)+6.*(vv(j,k+1)+sn*cc(j,k+1))*thd 
              work(j,k,5)= work(j,k,5) -  (vv(j,k+2)+sn*cc(j,k+2))*thd
              work(j,k,1)= work(j,k,1)*ds(j,k)                       
              work(j,k,2)= work(j,k,2)*ds(j,k)                       
              work(j,k,3)= work(j,k,3)*ds(j,k)                        
              work(j,k,3)= work(j,k,3) + fct
              work(j,k,4)= work(j,k,4)*ds(j,k)                        
              work(j,k,5)= work(j,k,5)*ds(j,k)                       
c               
              k=kup
              work(j,k,1)= work(j,k,1) +  (vv(j,k-2)+sn*cc(j,k-2))*thd
              work(j,k,2)= work(j,k,2)-6.*(vv(j,k-1)+sn*cc(j,k-1))*thd 
              work(j,k,3)= work(j,k,3) +   3.*(vv(j,k)+sn*cc(j,k))*thd
              work(j,k,4)= work(j,k,4)+2.*(vv(j,k+1)+sn*cc(j,k+1))*thd
              work(j,k,1)= work(j,k,1)*ds(j,k)                     
              work(j,k,2)= work(j,k,2)*ds(j,k)                      
              work(j,k,3)= work(j,k,3)*ds(j,k)
              work(j,k,3)= work(j,k,3) + fct
              work(j,k,4)= work(j,k,4)*ds(j,k)                         
              work(j,k,5)= work(j,k,5)*ds(j,k)                        
 532        continue
          else
            do 535 k = klow,kup
            do 535 j = jlow,jup
              work(j,k,2) = work(j,k,2) - (vv(j,k-1)+sn*cc(j,k-1))*hd
              work(j,k,4) = work(j,k,4) + (vv(j,k+1)+sn*cc(j,k+1))*hd
              work(j,k,1) = work(j,k,1)*ds(j,k)                       
              work(j,k,2) = work(j,k,2)*ds(j,k)                       
              work(j,k,3) = work(j,k,3)*ds(j,k)                       
              work(j,k,3) = work(j,k,3) + fct
              work(j,k,4) = work(j,k,4)*ds(j,k)                       
              work(j,k,5) = work(j,k,5)*ds(j,k)                      
 535        continue
          endif
        else
c     
c         -use preconditioned 3rd and 4th eigenvalues
          do 536 k = klow,kup                                      
          do 536 j = jlow,jup
            work(j,k,2) = work(j,k,2) - 
     +            (precon(j,k-1,5)+sn*precon(j,k-1,6))*hd       
            work(j,k,4) = work(j,k,4) + 
     +            (precon(j,k+1,5)+sn*precon(j,k+1,6))*hd       
            work(j,k,1) = work(j,k,1)*ds(j,k)            
            work(j,k,2) = work(j,k,2)*ds(j,k)                  
            work(j,k,3) = work(j,k,3)*ds(j,k)      
            work(j,k,4) = work(j,k,4)*ds(j,k)            
            work(j,k,5) = work(j,k,5)*ds(j,k) 
            work(j,k,3) = work(j,k,3) + fct
 536      continue                                                    
        endif
c     
c       -if CUSP dissipation - cannot invert first two s elements 
c        together when full limiters are selected
        if (idmodel.eq.4 .and. limiter.gt.3) then 
          call ypenta(jdim,kdim,work(1,1,1),work(1,1,2),
     $          work(1,1,3),work(1,1,4),work(1,1,5),work(1,1,6),
     $          work(1,1,7),s(1,1,n),klow,kup,jlow,jup)
        else                                                      
c         -combine n=1 and 2
c         -invert on first two s elements
          if(n.eq.2)then                                              
            call ypenta2(jdim,kdim,work(1,1,1),work(1,1,2),work(1,1,3),
     *            work(1,1,4),work(1,1,5),work(1,1,6),work(1,1,7),   
     *            s(1,1,1),klow,kup,jlow,jup)                        
          else                                                       
c           -do n=3 and 4
            call ypenta(jdim,kdim,work(1,1,1),work(1,1,2),work(1,1,3),
     *            work(1,1,4),work(1,1,5),work(1,1,6),work(1,1,7),    
     *            s(1,1,n),klow,kup,jlow,jup)                          
          endif                                    
        endif                   !(idmodel.eq.4) .and. (limiter.gt.3)
      enddo                     !n = nlow,4 
c     
      return
      end                       !stepf2dy

