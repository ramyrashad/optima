      subroutine ratio(iblk,jdim,kdim,jl,ju,kl,ku,
     &                 nhalo,nodetot,jmxar,kmxar,iblkmx,
     &                 asrmax,asravg,x,y)

      implicit none
      integer iblk,jdim,kdim,jl,ju,kl,ku,nhalo,nodetot,jmxar,kmxar
      integer iblkmx,j,k
      double precision asrmax,asravg,xl1,xl2,xl,yl1,yl2,yl,asr
      double precision x(jdim,kdim),y(jdim,kdim)
c
      do 10 k=kl,ku-1                                            
      do 10 j=jl,ju-1                                          
        xl1 = sqrt ((x(j+1,k)-x(j,k))**2+(y(j+1,k)-y(j,k))**2)
        xl2 = sqrt ((x(j+1,k+1)-x(j,k+1))**2+(y(j+1,k+1)-y(j,k+1))**2)
        xl = .5d0*(xl1+xl2)
        yl1 = sqrt ((x(j,k+1)-x(j,k))**2+(y(j,k+1)-y(j,k))**2)
        yl2 = sqrt ((x(j+1,k+1)-x(j+1,k))**2+(y(j+1,k+1)-y(j+1,k))**2)
        yl = .5d0*(yl1+yl2)
        asr = abs(xl)/abs(yl)
        if (asr.lt.1.d0) asr = 1.d0/asr
        asravg = asravg + asr
        if (asrmax.lt.asr) then
          asrmax = asr
          jmxar = j-nhalo
          kmxar = k-nhalo
          iblkmx= iblk
        endif
        nodetot=nodetot+1
 10   continue
c
      return
      end
