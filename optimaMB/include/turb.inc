c
c     workspace for Baldwin-Barth and Spalart-Allmaras turbulence model
c
      common/turbmod/
     r zdamp1m,  aa,       aan,  smin,
     i jtranloc, jtranupc, jtlo, jtup, kwall, jmid
c
      double precision zdamp1m(maxjk),smin(maxjk)
      double precision aa(maxjkaa),aan(maxjk)
c
      integer jtranloc(maxjk),jtranupc(maxjk)
      integer jtlo(maxblk), jtup(maxblk)
      integer kwall(maxjk), jmid(maxblk)
