! ===================================================================
module Database_Mod

! ------------------------------------------------------------------- 
! Purpose: Contains neccessary subroutines to store new function
!          evaluations and retrieve previous ones
!
! Author:  Oleg Chernukhin, Aug 2010
! -------------------------------------------------------------------

use CommonH

implicit none
public

! _________________________________________
! -- Module variables, constants and structures

integer :: entrytype
logical :: updated, globaldb
character(len=128) :: dbfilename
integer :: DBCOMM, dbrootrank, dbhandle


!-- Defines database entry type
!-- The storage is comprised of eight pieces of information:
!-- First, an integer indicating the number of design variables
!-- Second, an array of design variables (double precision)
!-- Third, an integer indicating the size of Fnc array
!-- Fourth, the Fnc array itself (double prec.)
!-- Fifth, a boolean indicating whether the gradients are given
!-- Sixth, an integer indicating the size of G array
!-- Seventh, an array of gradients (double precision)
!-- Eighth, a boolean indicating whether this ia a 'good' entry
type dbEntry
   sequence
   integer :: nX
   real(kind=dp), allocatable :: X(:)
   integer :: nFnc
   real(kind=dp), allocatable :: Fnc(:)
   logical :: haveG
   integer :: nG
   real(kind=dp), allocatable :: G(:)
   logical :: valid
end type dbEntry

type(dbEntry), allocatable :: allentries(:)

! _________________________________
! -- Module subroutines
contains

! ===================================================================

subroutine initdb(glob)

! -------------------------------------------------------------------
! Purpose: Initializes the database by defining a custom MPI datatype
!          and opening the database file
!
! -------------------------------------------------------------------
        
   ! -- Input parameters
   logical :: glob

   !-- Local variables
   integer :: intsize, dblsize, lgcsize, d100, d10, d1
   integer :: numparts
   integer :: blocklen(8)
   integer(kind=MPI_ADDRESS_KIND) :: disp(8)
   integer :: typearr(8)
   integer(kind=MPI_OFFSET_KIND) :: offs
   integer :: buf
   logical :: file_exists

   !-- Begin execution
   globaldb = glob

   !-- Set appropriate DBCOMM and database file name
   if (.not. globaldb) then

      DBCOMM = comm(mycid)
      dbrootrank = myrank_loc
    
      dbfilename = 'database'

      d100 = (mycid - 1) / 100
      d10  = (mycid - 1) / 10 - (d100 * 10)
      d1   = (mycid - 1) / 1  - (d100 * 100) - (d10 * 10)

      dbfilename(len_trim(dbfilename)+1:len_trim(dbfilename)+8) &
         = '-'//achar(d100+48)//achar(d10+48)//achar(d1+48)//'.dat'
   else
      DBCOMM = HYBROPT_COMM
      dbrootrank = myrank_glob
      dbfilename = 'database.dat'
   endif

   !-- Get extents of the elementary types 
   call mpi_type_size(MPI_INTEGER, intsize, ierr)
   call mpi_type_size(MPI_DOUBLE_PRECISION, dblsize, ierr)
   call mpi_type_size(MPI_LOGICAL, lgcsize, ierr)

   !-- There are eight pieces of information
   numparts = 8

   !-- First is an integer nX
   blocklen(1) = 1
   disp(1) = 0
   typearr(1) = MPI_INTEGER

   !-- Second is an array X(nX)
   blocklen(2) = nX
   disp(2) = intsize
   typearr(2) = MPI_DOUBLE_PRECISION

   !-- Third is an integer nFnc
   blocklen(3) = 1
   disp(3) = intsize + nX*dblsize
   typearr(3) = MPI_INTEGER

   !-- Fourth is an array Fnc(nFnc)
   blocklen(4) = nFnc
   disp(4) = intsize + nX*dblsize + intsize
   typearr(4) = MPI_DOUBLE_PRECISION

   !-- Fifth is a logical haveG
   blocklen(5) = 1
   disp(5) = intsize + nX*dblsize + intsize + nFnc*dblsize
   typearr(5) = MPI_LOGICAL

   !-- Sixth is an integer nnzGrad
   blocklen(6) = 1
   disp(6) = intsize + nX*dblsize + intsize + nFnc*dblsize + lgcsize
   typearr(6) = MPI_INTEGER

   !-- Seventh is an array G(nnzGrad)
   blocklen(7) = nnzGrad
   disp(7) = intsize + nX*dblsize + intsize + nFnc*dblsize + lgcsize + &
             intsize
   typearr(7) = MPI_DOUBLE_PRECISION

   !-- Eighth is a logical valid
   blocklen(8) = 1
   disp(8) = intsize + nX*dblsize + intsize + nFnc*dblsize + lgcsize + &
             intsize + nnzGrad*dblsize
   typearr(8) = MPI_LOGICAL
  
   !-- Commit this datatype
!   call mpi_type_create_struct(numparts, blocklen, disp, &
!                               typearr, entrytype, ierr)
!
!   call mpi_type_commit(entrytype, ierr)

   !-- Check if there is an existing database file before we call
   !-- MPI_FILE_OPEN
   if (dbrootrank == 0) inquire(file=dbfilename, exist=file_exists)

   call mpi_bcast(file_exists, 1, MPI_LOGICAL, 0, DBCOMM, ierr)

   !-- Open the database file
   call mpi_file_open(DBCOMM, dbfilename,&
                      MPI_MODE_CREATE+MPI_MODE_RDWR, &
                      MPI_INFO_NULL, dbhandle, ierr)

   !-- If file did not already exist, write 0 at the very beginning
   if (.not. file_exists) then
      offs = 0
      call mpi_file_set_view(dbhandle, offs, MPI_INTEGER, MPI_INTEGER, &
                             'native', MPI_INFO_NULL, ierr)

      if (dbrootrank == 0) then
         buf = 0
         call mpi_file_write(dbhandle, 0, 1, MPI_INTEGER, &
                             MPI_STATUS_IGNORE, ierr)
      endif
   endif

   !-- Set 'updated' flag to true
   updated = .true.

end subroutine

! ===================================================================

subroutine closedb()
! -------------------------------------------------------------------
!
! Purpose: Closes the database and free the derived entry datatype
!
! -------------------------------------------------------------------
 
   !-- Begin execution
!   call mpi_type_free(entrytype, ierr)   
   call mpi_file_close(dbhandle, ierr)

end subroutine

! ===================================================================

subroutine writedb(nX, X, nF, F, haveG, nG, G, stat, existent)

! -------------------------------------------------------------------
! Purpose: 
!    Stores provided information about the performed flowsolve into
!    the database file. Note that this a collective operation and
!    all processes must call this subroutine. Whether the actual
!    write is performed or not is indicated by 'existent' parameter
!
! Pre:
!    Database file is opened, entrytype is committed
!
! Post:
!
! -------------------------------------------------------------------

   !-- Input parameters
   integer :: nX, nF, nG, stat
   logical :: haveG, existent
   real(kind=dp) :: X(nX), F(nF), G(nG)

   !-- Local variables
   integer :: entrylen, intlen, ii, numentries, tag, newvals, currdisp
   integer :: source, tmpd
   integer :: disp(popsize)
   integer(kind=MPI_OFFSET_KIND) :: adisp
   logical :: valid, aex(popsize), dowr(popsize)
   type(dbEntry) :: currentry
   character(len=MPI_MAX_ERROR_STRING) :: errorstr

   !-- Begin execution

   aex(:) = .true.
   aex(mycid) = existent
   dowr(:) = .false. 
   disp(:) = 0
   newvals = 0
   currdisp = 0
   tag = 1
   call mpi_type_size(MPI_INTEGER, intlen, ierr)
!   call mpi_type_size(entrytype, entrylen, ierr)

   !-- Send existent status to root, if global database
   if (myrank_loc == 0 .and. globaldb) then
      if (myrank_glob .ne. 0) then
         call mpi_send(existent, 1, MPI_LOGICAL, 0, tag, &
                       DBCOMM, ierr)
      else
         do ii = 2, popsize
            source = (ii-1) * ppc
            call mpi_recv(aex(ii), 1, MPI_LOGICAL, source, tag, &
                          DBCOMM, MPI_STATUS_IGNORE, ierr)
         end do
      endif
   endif

   !-- Calculate dowr and disp arrrays and broadcast them to all procs
   !-- This logic should take care of both database cases (local/global)
   if (dbrootrank == 0) then
      do ii = 1, popsize
         disp(ii) = currdisp
         if (.not. aex(ii)) then
            newvals = newvals + 1
            dowr(ii) = .true.
            currdisp = currdisp + entrylen
         else
            dowr(ii) = .false.
         endif
      end do
   endif
   call mpi_bcast(newvals, 1, MPI_INTEGER, 0, DBCOMM, ierr)
   call mpi_bcast(dowr(:), popsize, MPI_LOGICAL, 0, DBCOMM, ierr)
   call mpi_bcast(disp(:), popsize, MPI_INTEGER, 0, DBCOMM, ierr)

   !-- Transfer data to dbEntry structure

   !-- Allocate the neccessary arrays in currentry
   allocate(currentry%X(nX))
   allocate(currentry%Fnc(nF))
   allocate(currentry%G(nG))

   !-- Set all scalar and logical fields in currentry
   currentry%nX = nX
   currentry%nFnc = nF
   currentry%haveG = haveG
   currentry%nG = nG
   if (stat .ne. -1) then
      currentry%valid = .true.
   else
      currentry%valid = .false.
   endif   

   !-- Fill in X in currentry
   do ii = 1, nX
      currentry%X(ii) = X(ii)
   end do   

   !-- Fill in Fnc in currentry
   do ii = 1, nF
      currentry%Fnc(ii) = F(ii)
   end do   

   !-- Fill in G in currentry
   if (haveG) then
      do ii = 1, nG
         currentry%G(ii) = G(ii)
      end do
   else
      do ii = 1, nG
         currentry%G(ii) = 0.0d0
      end do
   endif

   !-- Set initial offsets
   call getNumDbEntries(numentries)
   adisp = intlen + entrylen*numentries + disp(mycid)

   !-- Call subroutine to write the entry to file
   !-- NOTE: I originally tried to call a single write with
   !-- entrytype datatype, but couldn't get it to work. 
   !-- Hence, I created writeEntry subroutine that writes
   !-- individual fields in dbEntry one-by-one.
   !-- There should be a (much) more efficient way of doing 
   !-- this operation.
   call writeEntry(currentry, adisp, dowr(mycid))

   !-- Update the first field in database file, which indicates
   !-- the number of entries in the database
   adisp = 0
   numentries = numentries + newvals

   call mpi_file_set_view(dbhandle, adisp, MPI_INTEGER, &
                          MPI_INTEGER, 'native', MPI_INFO_NULL, &
                          ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in writedb'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
   if (dbrootrank == 0 .and. newvals > 0) then      
      call mpi_file_write(dbhandle, numentries, 1, MPI_INTEGER, &
                          MPI_STATUS_IGNORE, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_FILE_WRITE in writedb, numentr write'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif

      updated = .true.
   else
      updated = .false.
   endif

   !-- Sync 'updated' flag
   call mpi_bcast(updated, 1, MPI_LOGICAL, 0, DBCOMM, ierr)

   call mpi_file_set_view(dbhandle, adisp, MPI_INTEGER, &
                          MPI_INTEGER, 'native', MPI_INFO_NULL, &
                          ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_FILE_SET_VIEW in writedb, reset 0, last'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
   
   if (allocated(currentry%X)) deallocate(currentry%X)
   if (allocated(currentry%Fnc)) deallocate(currentry%Fnc)
   if (allocated(currentry%G)) deallocate(currentry%G)

end subroutine

! ===================================================================

subroutine writeEntry(entryc, disp, dowr)
! -------------------------------------------------------------------
!
! Purpose: 
!    Write the individual fields in dbEntry datatype into the 
!    database file. I tried a single write operation:
!   
!    call mpi_file_write(dbhandle, entryc, 1, entrytype, entrytype, &
!                        'native', MPI_INFO_NULL, ierr)
!
!    However, this does not work, presumably because the dbEntry is
!    not stored in memory the same way as entrytype. There might be
!    a better workaround for this, but this works for now.
!
!    This is a collective operation and all processes from the 
!    appropriate communicator (DBCOMM) must call
!    this subroutine. Otherwise, we'll get a deadlock.
!
! Pre:
!    database file is opened
!    entryc contains the correct information to be written
!    disp is set to the correct offset
!    dowr is set to T/F, depending on whether a write is needed
!
! -------------------------------------------------------------------

   !________________________________
   !-- Input parameters
   type(dbEntry) :: entryc
   integer(kind=MPI_OFFSET_KIND) :: disp
   logical :: dowr

   !-- Local variables
   integer :: intlen, dbllen, lgclen, ii

   !________________________________
   !-- Begin execution

   !-- Calculate extents of elementary datatypes
   call mpi_type_size(MPI_INTEGER, intlen, ierr)
   call mpi_type_size(MPI_DOUBLE_PRECISION, dbllen, ierr)
   call mpi_type_size(MPI_LOGICAL, lgclen, ierr)

   !-- Write nX
   call mpi_file_set_view(dbhandle, disp, MPI_INTEGER, MPI_INTEGER, &
                          'native', MPI_INFO_NULL, ierr)
   if (myrank_loc == 0 .and. dowr) then
      call mpi_file_write(dbhandle, entryc%nX, 1, MPI_INTEGER, &
                          MPI_STATUS_IGNORE, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_SET_FILE_VIEW in writeEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
   endif
   disp = disp + intlen

   !-- Write X(nX)
   do ii = 1, entryc%nX
      call mpi_file_set_view(dbhandle, disp, MPI_DOUBLE_PRECISION, &
                             MPI_DOUBLE_PRECISION, &
                             'native', MPI_INFO_NULL, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_FILE_SET_VIEW in writeEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
      if (myrank_loc == 0 .and. dowr) then
         call mpi_file_write(dbhandle, entryc%X(ii), 1, &
                             MPI_DOUBLE_PRECISION, &
                             MPI_STATUS_IGNORE, ierr)
         if (ierr .ne. MPI_SUCCESS) then
            write(*,*) 'ERROR: MPI_FILE_WRITE in writeEntry'
            call mpi_abort(HYBROPT_COMM, 0, ierr)
         endif
      endif
      disp = disp + dbllen
   end do

   !-- Write nFnc
   call mpi_file_set_view(dbhandle, disp, MPI_INTEGER, MPI_INTEGER, &
                          'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_FILE_SET_VIEW in writeEntry'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif      
   if (myrank_loc == 0 .and. dowr) then
      call mpi_file_write(dbhandle, entryc%nFnc, 1, MPI_INTEGER, &
                          MPI_STATUS_IGNORE, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_FILE_WRITE in writeEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif      
   endif
   disp = disp + intlen

   !-- Write Fnc(nFnc)
   do ii = 1, entryc%nFnc
      call mpi_file_set_view(dbhandle, disp, MPI_DOUBLE_PRECISION, &
                             MPI_DOUBLE_PRECISION, &
                             'native', MPI_INFO_NULL, ierr)
      if (myrank_loc == 0 .and. dowr) then
         call mpi_file_write(dbhandle, entryc%Fnc(ii), 1, &
                             MPI_DOUBLE_PRECISION, &
                             MPI_STATUS_IGNORE, ierr)
         ! ERROR handling... TBI
      endif
      disp = disp + dbllen
   end do

   !-- Write haveG
   call mpi_file_set_view(dbhandle, disp, MPI_LOGICAL, MPI_LOGICAL, &
                          'native', MPI_INFO_NULL, ierr)
   if (myrank_loc == 0 .and. dowr) then
      call mpi_file_write(dbhandle, entryc%haveG, 1, MPI_LOGICAL, &
                          MPI_STATUS_IGNORE, ierr)
      ! ERROR handling... TBI
   endif
   disp = disp + lgclen

   !-- Write nG
   call mpi_file_set_view(dbhandle, disp, MPI_INTEGER, MPI_INTEGER, &
                          'native', MPI_INFO_NULL, ierr)
   if (myrank_loc == 0 .and. dowr) then
      call mpi_file_write(dbhandle, entryc%nG, 1, MPI_INTEGER, &
                          MPI_STATUS_IGNORE, ierr)
      ! ERROR handling... TBI
   endif
   disp = disp + intlen

   !-- Write G(nG)
   do ii = 1, entryc%nG
      call mpi_file_set_view(dbhandle, disp, MPI_DOUBLE_PRECISION, &
                             MPI_DOUBLE_PRECISION, &
                             'native', MPI_INFO_NULL, ierr)
      if (myrank_loc == 0 .and. dowr) then
         call mpi_file_write(dbhandle, entryc%G(ii), 1, &
                             MPI_DOUBLE_PRECISION, &
                             MPI_STATUS_IGNORE, ierr)
         ! ERROR handling... TBI
      endif
      disp = disp + dbllen
   end do

   !-- Write valid
   call mpi_file_set_view(dbhandle, disp, MPI_LOGICAL, MPI_LOGICAL, &
                          'native', MPI_INFO_NULL, ierr)
   if (myrank_loc == 0 .and. dowr) then
      call mpi_file_write(dbhandle, entryc%valid, 1, MPI_LOGICAL, &
                          MPI_STATUS_IGNORE, ierr)
      ! ERROR handling... TBI
   endif
   disp = disp + lgclen

end subroutine

subroutine readdb(nX, nFnc, nG)

! -------------------------------------------------------------------
! Purpose: 
!    Reads the entire database and stores it in memory.
!
! Pre:
!    Database file is opened, entrytype is committed
!
! Post:
!    Array of dbEntries is allocated and filled
!
! -------------------------------------------------------------------

   !-- Input parameters
   integer :: nX, nFnc, nG

   !-- Local variables
   integer :: num, entrylen, intlen, ii, jj
   integer(kind=MPI_OFFSET_KIND) :: disp
   logical :: stop_exists

   !-- Begin execution

   !-- First, check if the database has been updated
   call mpi_bcast(updated, 1, MPI_LOGICAL, 0, DBCOMM, ierr)

   if (.not. updated) then
      !-- The data in allentries is up-to-date
      return
   else
      !-- Set 'updated' flag to false
      updated = .false.
      call mpi_bcast(updated, 1, MPI_LOGICAL, 0, DBCOMM, ierr)
   endif  

   !-- If we get to here, need to re-read the database file

   call getNumDbEntries(num)

   !-- Deallocate first
   if (allocated(allentries)) then
      do ii = 1, size(allentries)
         if (allocated(allentries(ii)%X)) deallocate(allentries(ii)%X)
         if (allocated(allentries(ii)%Fnc)) &
            deallocate(allentries(ii)%Fnc)
         if (allocated(allentries(ii)%G)) deallocate(allentries(ii)%G)
      end do
      deallocate(allentries)
   endif

   !-- Allocate space for entries
   allocate(allentries(num))
   do ii = 1, num
      allocate(allentries(ii)%X(nX))
      allocate(allentries(ii)%Fnc(nFnc))
      allocate(allentries(ii)%G(nG))
   end do

   !-- Calculate data type extents
!   call mpi_type_size(entrytype, entrylen, ierr)
   call mpi_type_size(MPI_INTEGER, intlen, ierr)

   !-- Read data from file into allentries
   do ii = 1, num

      !-- Set file view to appropriate entry
      disp = intlen + entrylen * (ii-1)

      !-- Read-in the entry into allentries(ii)
      call readEntry(allentries(ii), disp, nX, nFnc, nG)

   end do

   !-- Reset file view to the beginning of the file
   disp = 0
   call mpi_file_set_view(dbhandle, disp, MPI_INTEGER, MPI_INTEGER, &
                          'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readdb'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif

#if 0
   !-- Write out the entire database for debugging
   if (myrank_glob == 0) then
      do ii = 1, num
         write(*,*) '=====', ii, '====='
         write(*,*) allentries(ii)%nX
         do jj = 1, allentries(ii)%nX
            write(*,*) jj, allentries(ii)% X(jj)
         end do
         write(*,*) allentries(ii)%nFnc
         do jj = 1, allentries(ii)%nFnc
            write(*,*) jj, allentries(ii)% Fnc(jj)
         end do
         write(*,*) allentries(ii)%haveG
         write(*,*) allentries(ii)%nG
         do jj = 1, allentries(ii)%nG
            write(*,*) jj, allentries(ii)% G(jj)
         end do
         write(*,*) allentries(ii)%valid
         write(*,*) '-------------------'
      end do
      stop
   endif
#endif
   
   return

end subroutine

! ===================================================================

subroutine readEntry(entryc, disp, nX, nFnc, nG)
! -------------------------------------------------------------------
!
! Purpose:
!    Reads-in individual fields from the database and stores them
!    in entryc. See writeEntry for more info.
!
! -------------------------------------------------------------------

   !___________________________________
   !-- Input parameters
   type(dbEntry) :: entryc
   integer(kind=MPI_OFFSET_KIND) :: disp
   integer :: nX, nFnc, nG

   !-- Local variables
   integer :: intlen, dbllen, lgclen, ii

   !___________________________________
   !-- Begin execution

   !-- Calculate extents of elementary datatypes
   call mpi_type_size(MPI_INTEGER, intlen, ierr)
   call mpi_type_size(MPI_DOUBLE_PRECISION, dbllen, ierr)
   call mpi_type_size(MPI_LOGICAL, lgclen, ierr)

   !-- Read nX
   call mpi_file_set_view(dbhandle, disp, &
                          MPI_INTEGER, MPI_INTEGER, &
                          'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readEntry'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
   if (myrank_loc == 0) then
      call mpi_file_read(dbhandle, entryc%nX, 1, MPI_INTEGER, &
                         MPI_STATUS_IGNORE, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_FILE_READ in readEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
   endif
   disp = disp + intlen

   !-- Read X
   do ii = 1, nX
      call mpi_file_set_view(dbhandle, disp, &
                             MPI_DOUBLE_PRECISION,MPI_DOUBLE_PRECISION,&
                             'native', MPI_INFO_NULL, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
      if (myrank_loc == 0) then
         call mpi_file_read(dbhandle, entryc%X(ii), 1, &
                            MPI_DOUBLE_PRECISION, &
                            MPI_STATUS_IGNORE, ierr)
         if (ierr .ne. MPI_SUCCESS) then
            write(*,*) 'ERROR: MPI_FILE_READ in readEntry'
            call mpi_abort(HYBROPT_COMM, 0, ierr)
         endif
      endif
      disp = disp + dbllen
   end do

   !-- Read nFnc
   call mpi_file_set_view(dbhandle, disp, &
                          MPI_INTEGER, MPI_INTEGER, &
                          'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readEntry'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
   if (myrank_loc == 0) then
      call mpi_file_read(dbhandle, entryc%nFnc, 1, MPI_INTEGER, &
                         MPI_STATUS_IGNORE, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_FILE_READ in readEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
   endif
   disp = disp + intlen

   !-- Read Fnc
   do ii = 1, nFnc
      call mpi_file_set_view(dbhandle, disp, &
                             MPI_DOUBLE_PRECISION,MPI_DOUBLE_PRECISION,&
                             'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readEntry'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
      if (myrank_loc == 0) then
         call mpi_file_read(dbhandle, entryc%Fnc(ii), 1, &
                            MPI_DOUBLE_PRECISION, &
                            MPI_STATUS_IGNORE, ierr)
         if (ierr .ne. MPI_SUCCESS) then
            write(*,*) 'ERROR: MPI_FILE_READ in readEntry'
            call mpi_abort(HYBROPT_COMM, 0, ierr)
         endif
      endif
      disp = disp + dbllen
   end do

   !-- Read haveG
   call mpi_file_set_view(dbhandle, disp, &
                          MPI_LOGICAL, MPI_LOGICAL, &
                          'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readEntry'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
   if (myrank_loc == 0) then
      call mpi_file_read(dbhandle, entryc%haveG, 1, MPI_LOGICAL, &
                         MPI_STATUS_IGNORE, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_FILE_READ in readEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif      
   endif
   disp = disp + lgclen

   !-- Read nG
   call mpi_file_set_view(dbhandle, disp, &
                          MPI_INTEGER, MPI_INTEGER, &
                          'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readEntry'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
   if (myrank_loc == 0) then
      call mpi_file_read(dbhandle, entryc%nG, 1, MPI_INTEGER, &
                         MPI_STATUS_IGNORE, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_FILE_READ in readEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
   endif
   disp = disp + intlen

   !-- Read G
   do ii = 1, nG
      call mpi_file_set_view(dbhandle, disp, &
                             MPI_DOUBLE_PRECISION,MPI_DOUBLE_PRECISION,&
                             'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readEntry'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
      if (myrank_loc == 0) then
         call mpi_file_read(dbhandle, entryc%G(ii), 1, &
                            MPI_DOUBLE_PRECISION, &
                            MPI_STATUS_IGNORE, ierr)
         if (ierr .ne. MPI_SUCCESS) then
            write(*,*) 'ERROR: MPI_FILE_READ in readEntry'
            call mpi_abort(HYBROPT_COMM, 0, ierr)
         endif
      endif
      disp = disp + dbllen
   end do

   !-- Read valid
   call mpi_file_set_view(dbhandle, disp, &
                          MPI_LOGICAL, MPI_LOGICAL, &
                          'native', MPI_INFO_NULL, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in readEntry'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif
   if (myrank_loc == 0) then
      call mpi_file_read(dbhandle, entryc%valid, 1, MPI_LOGICAL, &
                         MPI_STATUS_IGNORE, ierr)
      if (ierr .ne. MPI_SUCCESS) then
         write(*,*) 'ERROR: MPI_FILE_READ in readEntry'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
   endif
   disp = disp + lgclen

end subroutine

! ===================================================================

subroutine getNumDbEntries(numentries)
! -------------------------------------------------------------------
!
! Purpose:
!    Reads and returns the total number of entries in the database
!
! -------------------------------------------------------------------

   !___________________________________
   !-- Input parameters
   integer :: numentries

   !-- Local variables
   integer(kind=MPI_OFFSET_KIND) :: disp

   !___________________________________
   !-- Begin execution

   numentries = 0
   disp = 0

   !-- Set view to the beginning of file  
   call mpi_file_set_view(dbhandle, disp, MPI_INTEGER, &
                          MPI_INTEGER, 'native', MPI_INFO_NULL, &
                          ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in getNumDbEntries'
   endif

   call mpi_file_read(dbhandle, numentries, 1, MPI_INTEGER, &
                      MPI_STATUS_IGNORE, ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_FILE_READ in getNumDbEntries'
   endif

   !-- Reset view to the beginning of file
   call mpi_file_set_view(dbhandle, disp, MPI_INTEGER, &
                          MPI_INTEGER, 'native', MPI_INFO_NULL, &
                          ierr)
   if (ierr .ne. MPI_SUCCESS) then
      write(*,*) 'ERROR: MPI_SET_FILE_VIEW in getNumDbEntries'
   endif

end subroutine

! ===================================================================

subroutine checkdbentry(nX, X, nFnc, Fnc, needG, nG, G, stat, stored)
! -------------------------------------------------------------------
!
! Purpose:
!    Checks whether the provided design point has been evaluated 
!
! -------------------------------------------------------------------
   !___________________________________
   !-- Input parameters
   integer :: nX, nFnc, nG, stat
   real(kind=dp) :: X(nX), Fnc(nFnc), G(nG)
   logical :: needG, stored

   !-- Local variables
   integer :: ii, jj, numentries
   real(kind=dp) :: X2(nX), delX(nX), Fnc2(nFnc), G2(nG)
   real(kind=dp) :: delta, tolerance
   logical :: equal

   !___________________________________
   !-- Begin execution

   !-- All non-root processes can return
   if (myrank_loc .ne. 0) return

   stored = .false.
   delta = 0.0d0
   tolerance = 1.0d-13

   !-- Iterate through all entries, exit if entry is found
   do ii = 1, size(allentries)
      !-- Equality test
      delX(:) = X(:) - allentries(ii)%X(:)
#if 1
      if (needG .and. (.not.allentries(ii)%haveG)) then
         equal = .false.
      else
         equal = .true.
         do jj = 1, nX
            if (abs(delX(jj)) < tolerance .and. equal) then
               equal = .true.
            else
               equal = .false.
            endif
         end do
      endif
#endif
#if 0
      delta = 0.0d0
      do jj = 1, nX
         delta = delta + abs(delX(jj))
      end do
      equal = .false.
      if (delta < tolerance) then
         if (needG .and. (.not.allentries(ii)%haveG)) then
            equal = .false.
         else
            equal = .true.
do jj = 1, nX
print *, 'DEBUG:eq', jj, X(jj), allentries(ii)%X(jj), delX(jj)
end do
         endif
      endif
#endif

      !-- If entry is found, copy values
      if (equal) then
         stored = .true.
         Fnc(:) = allentries(ii)%Fnc(:)
         G(:) = allentries(ii)%G(:)
         needG = allentries(ii)%haveG
         if (allentries(ii)%valid) then
            stat = 0
         else
            stat = -1
         endif
         exit
      endif

   end do

end subroutine

! ===================================================================

end module

