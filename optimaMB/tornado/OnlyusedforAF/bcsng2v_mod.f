C ********************************************************************
C ********************* BOUNDARY CONDITIONS **************************
c ** Application of solid wall boundary conditions on at sing pt    **
c ** Version 2 for viscous flows                                    **
C ********************************************************************
c calling routine:  integrat
c
      subroutine bcsng2v_mod(jdim1,kdim1,q1,xyj1,js1,ks1,j1,k1,
     &                   jdim2,kdim2,q2,xyj2,js2,ks2,j2,k2) 
c
      dimension q1(jdim1,kdim1,4),xyj1(jdim1,kdim1)
      dimension q2(jdim2,kdim2,4),xyj2(jdim2,kdim2)
#include "../include/common.inc"
C ....................................................................
c                                                                       
csd      t = ( numiter - 1.)/ strtit                                       
csd      if( t .gt.1.) t = 1.                                              
csd      scal = (10. -15.*t + 6.*t*t) *t**3                                
csdc                                                                       
csdc                no scaling if this is a restart                        
csdc                                                                       
csd      if(restart) scal=1.                                             
csdc
csdc     -viscous surface bc 
csdc
csdc     -viscous velocity bc at body
csdc      -cap u and cap v = 0.0                                                
csdc      u1=(xy(js1,ks1,4)*(-xit(js1,ks1))+xy(js1,ks1,2)*ett(js1,ks1))
csdc    &      /xyj(js1,ks1)
csdc      v1=(- xy(js1,ks1,3)*(-xit(js1,ks1))-xy(js1,ks1,1)*ett(js1,ks1))
csdc    &      /xyj(js1,ks1)
csdc      u2=(xy(js2,ks2,4)*(-xit(js2,ks2))+xy(js2,ks2,2)*ett(js2,ks2))
csdc    &      /xyj(js2,ks2)
csdc      v2=(- xy(js2,ks2,3)*(-xit(js2,ks2))-xy(js2,ks2,1)*ett(js2,ks2))
csdc    &      /xyj(js2,ks2)
csdc
csdc     -leave out time metrics for now
csd      u1=0.
csd      v1=0.
csd      u2=0.
csd      v2=0.
csdc
csd      u1=(1. - scal)*q1(js1,ks1,2)/q1(js1,ks1,1) + scal*u1
csd      v1=(1. - scal)*q1(js1,ks1,3)/q1(js1,ks1,1) + scal*v1
csd      u2=(1. - scal)*q2(js2,ks2,2)/q2(js2,ks2,1) + scal*u1
csd      v2=(1. - scal)*q2(js2,ks2,3)/q2(js2,ks2,1) + scal*v1
csdc
csdc     -(u1,v1) and (u2,v2) do not contain j so they may be averaged
csdc
csd      u = 0.5*(u1+u2)
csd      v = 0.5*(v1+v2)
csdc
csdc     -q2 and q3 now satify tangency... independently of whether
csdc      density is lagged                                      
csd      q1(js1,ks1,2) = u*q1(js1,ks1,1)
csd      q1(js1,ks1,3) = v*q1(js1,ks1,1)
csd      q2(js2,ks2,2) = u*q2(js2,ks2,1)
csd      q2(js2,ks2,3) = v*q2(js2,ks2,1)
c
c
c     surface condition on pressure
c     
c     dp/dn = 0.0 
c     extrap p low order extrapolation only!!
c     note: f doesn't have jacobian in it
c
c      p1=gami*(q1(j1,k1,4)-.5*(q1(j1,k1,2)**2+q1(j1,k1,3)**2)
c     &      /q1(j1,k1,1))*xyj1(j1,k1)
c      p2=gami*(q2(j2,k2,4)-.5*(q2(j2,k2,2)**2+q2(j2,k2,3)**2)
c     &      /q2(j2,k2,1))*xyj2(j2,k2)
      p1=gami*(q1(js1,ks1,4)-.5*(q1(js1,ks1,2)**2+q1(js1,ks1,3)**2)
     &      /q1(js1,ks1,1))*xyj1(js1,ks1)
      p2=gami*(q2(js2,ks2,4)-.5*(q2(js2,ks2,2)**2+q2(js2,ks2,3)**2)
     &      /q2(js2,ks2,1))*xyj2(js2,ks2)
      f = 0.5*(p1+p2)
c                                                                       
c     -first order of density extrapolation seems to      
c      be unstable for viscous calculations ... zero'th order is used.
c      - or constant wall temperature                       
c                                                                       
      rinver1 = 1./q1(js1,ks1,1)
      rinver2 = 1./q2(js2,ks2,1)
      rj1 = 1./xyj1(js1,ks1)
      rj2 = 1./xyj2(js2,ks2)
      if(wtrat.ne.0.0)then                                    
c       fix wall temp   --  twall is the ratio of twall/tinf
        prs1 = f*rj1
        prs2 = f*rj2
        twall = wtrat/gamma                                     
        q1(js1,ks1,1) = prs1/twall
        q2(js2,ks2,1) = prs2/twall
      else                                                    
c        rhoavg=.5d0*(q1(j1,k1,1)*xyj1(j1,k1)+q2(j2,k2,1)*xyj2(j2,k2))
        rhoavg=.5d0*(q1(js1,ks1,1)*xyj1(js1,ks1)+
     &               q2(js2,ks2,1)*xyj2(js2,ks2))
        q1(js1,ks1,1) = rhoavg*rj1
        q2(js2,ks2,1) = rhoavg*rj2
c
c       -original code did not average density
c       q1(js1,ks1,1) = q1(j1,k1,1)*xyj1(j1,k1)*rj1
c       q2(js2,ks2,1) = q2(j2,k2,1)*xyj2(j2,k2)*rj2
      endif                                                   
      u1 = q1(js1,ks1,2)*rinver1
      v1 = q1(js1,ks1,3)*rinver1
      q1(js1,ks1,2) = u1*q1(js1,ks1,1)
      q1(js1,ks1,3) = v1*q1(js1,ks1,1)
      q1(js1,ks1,4) = f/gami*rj1 + 
     %      .5*(q1(js1,ks1,2)**2+q1(js1,ks1,3)**2)/q1(js1,ks1,1)
      u2 = q2(js2,ks2,2)*rinver2
      v2 = q2(js2,ks2,3)*rinver2
      q2(js2,ks2,2) = u2*q2(js2,ks2,1)
      q2(js2,ks2,3) = v2*q2(js2,ks2,1)
      q2(js2,ks2,4) = f/gami*rj2 + 
     %      .5*(q2(js2,ks2,2)**2+q2(js2,ks2,3)**2)/q2(js2,ks2,1)
c
      return                                                           
      end                                                              
