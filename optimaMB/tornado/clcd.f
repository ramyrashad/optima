C ********************************************************************
C ********************* LIFT AND DRAG CALCULATIONS *******************
C ********************************************************************
      subroutine clcd(jdim,kdim,iside,idir,q,press,x,y,xy,xyj,
     &      nscal,cl,cd,cm,clvv,cdvv,cmvv,jstart,jstop,kstart,kstop)

      implicit none
c
#include "../include/parms.inc"
#include "../include/common.inc"
c
      integer jdim,kdim,iside,idir,jstart,jstop,kstart,kstop,nscal,j,k
      integer k1,k2,ktp,j1,j2,jtp,jm1,km1,jadd,kadd,k3,k4,k5,j3,j4,j5
      double precision q(jdim,kdim,4),press(jdim,kdim)
      double precision xy(jdim,kdim,4),xyj(jdim,kdim)
      double precision x(jdim,kdim),y(jdim,kdim)
      double precision cl,cd,cm,clvv,cdvv,cmvv,alngth,amu,uinf2
      double precision sgn,cpc,pp,cn,cc,cmle,cpav,sn,cs,cmqc,uxi,vxi,ccv
      double precision r,rp1,rp2,u,v,up1,up2,vp1,vp2,ueta,veta,xix,xiy
      double precision etax,etay,tauw,tmp,rp3,rp4,up3,up4,vp3,vp4,cnv
      double precision cmlev,cfav,cmqcv
      logical   oddside
c
      double precision z(maxj),work(maxj,4)
      common/worksp/z,work
c
c     -if idir=0 don't integrat the pressure (used for inviscid walls
c      in viscous problems)
c
      if (idir.eq.0) return
c
c     -the parameter nscal determines whether the q variables are scaled
c      by the metric jacobians xyj.
c
c     -this routine supplies force and moment coeffs.  on airfoil surface
c
      sgn = float(sign(1,idir))
      cpc = 2.d0/(gamma*fsmach**2)

      cl = 0.d0
      cd = 0.d0
      cm = 0.d0
      clvv = 0.d0
      cdvv = 0.d0
      cmvv = 0.d0
c
c     -set limits of integration
      k1  = kstart
      k2  = kstop
      ktp = kstart + 1
      j1  = jstart
      j2  = jstop
      jtp = jstart + 1
      if (iside.eq.1) k1 = kstart
      if (iside.eq.2) j1 = jstart
      if (iside.eq.3) k1 = kstop
      if (iside.eq.4) j1 = jstop

c     -set side type
      if ((iside.eq.1).or.(iside.eq.3)) then
        oddside = .true.
      else
        oddside = .false.
      endif

c
c     -compute cp at grid points and store in z array
      if (oddside) then
        do 10 j=j1,j2
          pp = press(j,k1)
          if (nscal.eq.0) pp = pp*xyj(j,k1)
          z(j) = (pp*gamma -1.d0)*cpc
   10   continue
      else
        do 11 k=k1,k2
          pp = press(j1,k)
          if (nscal.eq.0) pp = pp*xyj(j1,k)
          z(k) = (pp*gamma -1.d0)*cpc
   11   continue
      endif
c
c     -compute normal force coefficient and chord directed force coeff
c     -chord taken as one in all cases
c
      cn = 0.d0
      cc = 0.d0
      cmle = 0.d0

      if (oddside) then

        if (matcharc2d) then 
          do 12 j=jtp,j2
            jm1 = j-1
            cpav = (z(j) + z(jm1))*0.5d0
            cn = cn - cpav*(x(j,k1) - x(jm1,k1))
            cc = cc + cpav*(y(j,k1) - y(jm1,k1))
            cmle=cmle+cpav*(x(j,k1)+x(jm1,k1))*.5d0*(x(j,k1)-x(jm1,k1))
 12       continue
        else
          do 13 j=jtp,j2
            jm1 = j-1
            cpav = (z(j) + z(jm1))*0.5d0
            cn = cn - cpav*(x(j,k1) - x(jm1,k1))
            cc = cc + cpav*(y(j,k1) - y(jm1,k1))
            cmle=cmle+cpav*((x(j,k1)+x(jm1,k1))*.5d0*(x(j,k1)-x(jm1,k1))
     &                    +(y(j,k1)+y(jm1,k1))*.5d0*(y(j,k1)-y(jm1,k1)))
 13       continue
        endif

      else  !.not. oddside

        if (matcharc2d) then
          do 14 k=ktp,k2
            km1=k-1
            cpav = (z(k) + z(km1))*0.5d0
            cn = cn - cpav*(x(j1,k) - x(j1,km1))
            cc = cc + cpav*(y(j1,k) - y(j1,km1))
            cmle=cmle+cpav*(x(j1,k)+x(j1,km1))*.5d0*(x(j1,k)-x(j1,km1))
 14       continue
        else
          do 15 k=ktp,k2
            km1=k-1
            cpav = (z(k) + z(km1))*0.5d0
            cn = cn - cpav*(x(j1,k) - x(j1,km1))
            cc = cc + cpav*(y(j1,k) - y(j1,km1))
            cmle=cmle+cpav*((x(j1,k)+x(j1,km1))*.5d0*(x(j1,k)-x(j1,km1))
     &                    +(y(j1,k)+y(j1,km1))*.5d0*(y(j1,k)-y(j1,km1)))
 15       continue
        endif

      endif  !oddside?
c
c     **************************************
c     correct for curves going the wrong way
c     **************************************
      cn   = sgn*cn
      cc   = sgn*cc
      cmle = sgn*cmle
c
      sn=sin(alpha*pi/180.d0)
      cs=cos(alpha*pi/180.d0)
      cl = cn*cs - cc*sn
      cd = cn*sn + cc*cs

c     write (*,*) 'cl,cd',cl,cd,nscal

      cmqc = cmle +.25d0*cn
c
c     note: cmle is moment about (x,y)=(0,0) 
c     but cmqc is moment about (0.25,0)

      cm = cmqc











c
c     *********************************************
c      Viscous Coefficient of Friction Calculation
c     *********************************************

      if (viscous) then
c
c
c     -calculation taken from p. buning
c
c      calculate the skin friction coefficient                             
c                                                                       
c      c  = tau   /            2                                        
c       f      w / 1/2*rho   *u                                         
c                         inf  inf                                      
c                                                                       
c      tau  = mu*(du/dy-dv/dx)                                          
c         w                   w                                         
c                                                                       
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc., 
c     york, 1974, p. 50, but use freestream values instead of edge values.
c                                                                       
c                                                                       
c     -for calculating cf, we need the coefficient of viscosity, mu.  use  
c     re = (rhoinf*ainf*length scale)/mu. also assume length scale=1.  

        if(iside.eq.1) then
          kadd = 1
        elseif(iside.eq.3) then
          kadd = -1
        elseif(iside.eq.2) then
          jadd = 1
        else
          jadd = -1
        endif

        if (oddside) then
           k  = k1
           k2 = k1 + kadd
           k3 = k2 + kadd
           k4 = k3 + kadd
           k5 = k4 + kadd
        else
           j  = j1
           j2 = j1 + jadd
           j3 = j2 + jadd
           j4 = j3 + jadd
           j5 = j4 + jadd
        end if
c
        alngth= 1.d0
        amu   = rhoinf*alngth/re
        uinf2 = fsmach**2
c
        if (iord.eq.2) then

           if (oddside) then

              do j = j1,j2

c             -- xi direction

cmpr            uxi = 0.d0
cmpr            vxi = 0.d0

                 if (j.ne.j1 .and. j.ne.j2) then !central stencil
                    uxi = 0.5d0*(q(j+1,k,2)/q(j+1,k,1)-
     &                   q(j-1,k,2)/q(j-1,k,1))      
                    vxi = 0.5d0*(q(j+1,k,3)/q(j+1,k,1)-
     &                   q(j-1,k,3)/q(j-1,k,1))
                 else if (j.eq.j1) then !first-order biased
                    uxi= q(j+1,k,2)/q(j+1,k,1)-q(j,k,2)/q(j,k,1)                
                    vxi= q(j+1,k,3)/q(j+1,k,1)-q(j,k,3)/q(j,k,1)       
                 else if (j.eq.j2) then !first-order biased
                    uxi= q(j,k,2)/q(j,k,1)-q(j-1,k,2)/q(j-1,k,1)         
                    vxi= q(j,k,3)/q(j,k,1)-q(j-1,k,3)/q(j-1,k,1)       
                 end if
     
c             -- eta direction

                 r=1.d0/q(j,k,1)
                 rp1=1.d0/q(j,k2,1)
                 rp2=1.d0/q(j,k3,1)
                 u  =q(j,k ,2)*r
                 up1=q(j,k2,2)*rp1
                 up2=q(j,k3,2)*rp2
                 v  =q(j,k ,3)*r
                 vp1=q(j,k2,3)*rp1
                 vp2=q(j,k3,3)*rp2
                 
                 ueta= float(kadd)*(-3.d0*u + 4.d0*up1 - up2)*0.5d0
                 veta= float(kadd)*(-3.d0*v + 4.d0*vp1 - vp2)*0.5d0

c     
                 xix = xy(j,k,1)
                 xiy = xy(j,k,2)
                 etax = xy(j,k,3)
                 etay = xy(j,k,4)
                 tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
                 z(j)= tauw/(0.5d0*rhoinf*uinf2)

              end do

           else  ! not oddside

              do k = k1,k2

c             -- eta direction

cmpr                 ueta = 0.d0
cmpr                 veta = 0.d0

                 if (k.ne.k1 .and. k.ne.k2) then !central stencil
                    ueta = 0.5d0*(q(j,k+1,2)/q(j,k+1,1)-
     &                   q(j,k-1,2)/q(j,k-1,1))      
                    veta = 0.5d0*(q(j,k+1,3)/q(j,k+1,1)-
     &                   q(j,k-1,3)/q(j,k-1,1))
                 else if (k.eq.k1) then !first-order biased
                    ueta= q(j,k+1,2)/q(j,k+1,1)-q(j,k,2)/q(j,k,1)                
                    veta= q(j,k+1,3)/q(j,k+1,1)-q(j,k,3)/q(j,k,1)       
                 else if (k.eq.k2) then !first-order biased
                    ueta= q(j,k,2)/q(j,k,1)-q(j,k-1,2)/q(j,k-1,1)         
                    veta= q(j,k,3)/q(j,k,1)-q(j,k-1,3)/q(j,k-1,1)       
                 end if
     
c             -- xi direction

                 r=1.d0/q(j1,k,1)
                 rp1=1.d0/q(j2,k,1)
                 rp2=1.d0/q(j3,k,1)
                 u  =q(j1,k,2)*r
                 up1=q(j2,k,2)*rp1
                 up2=q(j3,k,2)*rp2
                 v  =q(j1,k,3)*r
                 vp1=q(j2,k,3)*rp1
                 vp2=q(j3,k,3)*rp2
                 
                 uxi= float(jadd)*(-3.d0*u + 4.d0*up1 - up2)*0.5d0
                 vxi= float(jadd)*(-3.d0*v + 4.d0*vp1 - vp2)*0.5d0

c     
                 xix = xy(j,k,1)
                 xiy = xy(j,k,2)
                 etax = xy(j,k,3)
                 etay = xy(j,k,4)
                 tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
                 z(k)= tauw/(0.5d0*rhoinf*uinf2)

              end do

           end if   !oddside?

        else     !higher order

           if (oddside) then

              tmp=float(kadd)/12.d0
              do j = j1,j2

c             -- xi direction

                 uxi = 0.d0
                 vxi = 0.d0
c
c             -- eta direction

                 r=1.d0/q(j,k1,1)
                 rp1=1.d0/q(j,k2,1)
                 rp2=1.d0/q(j,k3,1)
                 rp3=1.d0/q(j,k4,1)
                 rp4=1.d0/q(j,k5,1)
c           
                 u  =q(j,k1,2)*r
                 up1=q(j,k2,2)*rp1
                 up2=q(j,k3,2)*rp2
                 up3=q(j,k4,2)*rp3
                 up4=q(j,k5,2)*rp4
c           
                 v  =q(j,k1,3)*r
                 vp1=q(j,k2,3)*rp1
                 vp2=q(j,k3,3)*rp2
                 vp3=q(j,k4,3)*rp3
                 vp4=q(j,k5,3)*rp4
c           
                 ueta= tmp*(-25.d0*u +48.d0*up1-36.d0*up2+
     &                16.d0*up3-3.d0*up4) 
                 veta= tmp*(-25.d0*v +48.d0*vp1-36.d0*vp2+
     &                16.d0*vp3-3.d0*vp4) 
c           
                 xix = xy(j,k,1)
                 xiy = xy(j,k,2)
                 etax = xy(j,k,3)
                 etay = xy(j,k,4)
                 tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
                 z(j)= tauw/(0.5d0*rhoinf*uinf2)
              end do

           else   ! not oddside

              tmp=float(jadd)/12.d0
              do k = k1,k2

c             -- eta direction

                 ueta = 0.d0
                 veta = 0.d0
c
c             -- xi direction

                 r=1.d0/q(j1,k,1)
                 rp1=1.d0/q(j2,k,1)
                 rp2=1.d0/q(j3,k,1)
                 rp3=1.d0/q(j4,k,1)
                 rp4=1.d0/q(j5,k,1)
c           
                 u  =q(j1,k,2)*r
                 up1=q(j2,k,2)*rp1
                 up2=q(j3,k,2)*rp2
                 up3=q(j4,k,2)*rp3
                 up4=q(j5,k,2)*rp4
c           
                 v  =q(j1,k,3)*r
                 vp1=q(j2,k,3)*rp1
                 vp2=q(j3,k,3)*rp2
                 vp3=q(j4,k,3)*rp3
                 vp4=q(j5,k,3)*rp4
c           
                 uxi= tmp*(-25.d0*u +48.d0*up1-36.d0*up2+
     &                16.d0*up3-3.d0*up4) 
                 vxi= tmp*(-25.d0*v +48.d0*vp1-36.d0*vp2+
     &                16.d0*vp3-3.d0*vp4) 
c           
                 xix = xy(j,k,1)
                 xiy = xy(j,k,2)
                 etax = xy(j,k,3)
                 etay = xy(j,k,4)
                 tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
                 z(k)= tauw/(0.5d0*rhoinf*uinf2)
              end do

           end if  !oddside? 

        endif   !higher order?

        if (matcharc2d .and. sngvalte) then
c         -average trailing edge value
          z(j1)=0.5d0*(z(j1)+z(j2))
          z(j2)=z(j1)
        endif
c
c       -compute viscous normal and axial forces
c       -compute normal force coefficient and chord directed force coeff
c        chord taken as one in all cases
c
        cnv = 0.d0
        ccv = 0.d0
        cmlev = 0.d0

        if (oddside) then
           
           do j=jtp,j2
              jm1 = j-1
              cfav = (z(j) + z(jm1))*0.5d0
              ccv = ccv + cfav*(x(j,k) - x(jm1,k))
              cnv = cnv + cfav*(y(j,k) - y(jm1,k))
              cmlev = cmlev + cfav*(
     &             (x(j,k)+x(jm1,k))*0.5d0*(y(j,k) -y(jm1,k)) -
     &             (y(j,k)+y(jm1,k))*0.5d0*(x(j,k) -x(jm1,k))  )
           end do

        else

           do k=ktp,k2
              km1 = k-1
              cfav = (z(k) + z(km1))*0.5d0
              ccv = ccv + cfav*(x(j,k) - x(j,km1))
              cnv = cnv + cfav*(y(j,k) - y(j,km1))
              cmlev = cmlev + cfav*(
     &             (x(j,k)+x(j,km1))*0.5d0*(y(j,k) -y(j,km1)) -
     &             (y(j,k)+y(j,km1))*0.5d0*(x(j,k) -x(j,km1))  )
           end do

        end if
c
c       **************************************
c       correct for curves going the wrong way
c       **************************************
        cnv   = sgn*cnv
        ccv   = sgn*ccv
        cmlev = sgn*cmlev
c
        sn=sin(alpha*pi/180.d0)
        cs=cos(alpha*pi/180.d0)
        clvv = cnv*cs - ccv*sn
        cdvv = cnv*sn + ccv*cs
        cmqcv = cmlev +0.25d0*cnv
        cmvv = cmqcv
c
      endif
c
      return
      end
