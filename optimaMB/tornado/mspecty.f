      subroutine mspecty(jdim,kdim,vv,ccy,xyj,xy,spect,                  
     &                   jbegin,jend,kbegin,kend)

      implicit none
c
#include "../include/common.inc"
c
      integer jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision xyj(jdim,kdim),xy(jdim,kdim,4)                          
      double precision spect(jdim,kdim,3),vv(jdim,kdim),ccy(jdim,kdim)
      double precision temp,rj
c                                                                       
c     jacobian matrix spectral values
c                                                                       
      do 100 k=kbegin,kend                                         
      do 100 j=jbegin,jend                                        
        rj =1.d0/xyj(j,k)
        temp=(abs(vv(j,k))+ccy(j,k))*rj
        spect(j,k,1)=max(abs(vv(j,k))*rj,vleta*temp)
        spect(j,k,2)=max(abs(vv(j,k)+ccy(j,k))*rj,vneta*temp)
        spect(j,k,3)=max(abs(vv(j,k)-ccy(j,k))*rj,vneta*temp)
 100  continue       
c
      return                                                  
      end                                                     


