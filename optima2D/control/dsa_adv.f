c----------------------------------------------------------------------
c     -- differentiation of advective terms for the S-A model --
c     written by: marian nemec
c     date: jan. 2001
c----------------------------------------------------------------------

      subroutine dsa_adv(jdim, kdim, q, xy, xyj, uu, vv, bm2, bm1, db,
     &      bp1, bp2, wk1, wk2)

      use disscon_vars

#include "../include/arcom.inc"

      integer jdim, kdim

      double precision q(jdim,kdim,5), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision bm2(5,jdim,kdim), bm1(5,jdim,kdim)
      double precision bp1(5,jdim,kdim), bp2(5,jdim,kdim)
      double precision db(5,jdim,kdim), wk3(jdim,kdim)
      double precision wk1(jdim,kdim), wk2(jdim,kdim), wk4(jdim,kdim)
      double precision duudq(3), dvvdq(3)

c     -- d(advective terms in xi direction)/d(q5^) --
      do k = klow,kup
         do j = jlow,jup
            sgnu = sign(1.d0,uu(j,k))
            app  = 0.5d0*(1.d0 + sgnu)
            apm  = 0.5d0*(1.d0 - sgnu)
            if (dissCon) then
               bm2(5,j,k) =   
     &              (- uu(j,k)*app/xyj(j,k)*xyj(jminus(j),k))
     &              + lamDissT*
     &              0.5*dabs(uu(j,k))/xyj(j,k)*(-xyj(jminus(j),k))
               bp2(5,j,k) =   
     &              (+ uu(j,k)*apm/xyj(j,k)*xyj(jplus(j),k))
     &              + lamDissT*
     &              0.5*dabs(uu(j,k))/xyj(j,k)*(-xyj(jplus(j),k))
               db(5,j,k)  =   
     &              (+ uu(j,k)*(app-apm))
     &              + lamDissT*dabs(uu(j,k))  

            else
               bm2(5,j,k) =  - uu(j,k)*app/xyj(j,k)*xyj(jminus(j),k)
               bp2(5,j,k) =  + uu(j,k)*apm/xyj(j,k)*xyj(jplus(j),k)
               db(5,j,k)  =  + uu(j,k)*(app-apm)
            end if
         end do
      end do

c     -- d(advective terms in eta direction)/d(q5^) --
      do k = klow,kup
         do j = jlow,jup
            sgnu = sign(1.d0,vv(j,k))
            app  = 0.5d0*(1.d0 + sgnu)
            apm  = 0.5d0*(1.d0 - sgnu)
            if (dissCon) then
               bm1(5,j,k) =   
     &              (- vv(j,k)*app/xyj(j,k)*xyj(j,k-1))
     &              + lamDissT*
     &              0.5*dabs(vv(j,k))/xyj(j,k)*(-xyj(j,k-1))
               bp1(5,j,k) =   
     &              (+ vv(j,k)*apm/xyj(j,k)*xyj(j,k+1))
     &              + lamDissT*
     &              0.5*dabs(vv(j,k))/xyj(j,k)*(-xyj(j,k+1))
               db(5,j,k)  =  db(5,j,k) +  
     &              (+ vv(j,k)*(app-apm))
     &              + lamDissT*dabs(vv(j,k))
            else
               bm1(5,j,k) =  - vv(j,k)*app/xyj(j,k)*xyj(j,k-1)
               bp1(5,j,k) =  + vv(j,k)*apm/xyj(j,k)*xyj(j,k+1)
               db(5,j,k)  =  db(5,j,k) + vv(j,k)*(app-apm)
            end if
         end do
      end do

c     -- d(advective terms)/d(Qm^) where Qm => mean flow variables --
      do k = kbegin,kup
        kp = k + 1
        do j = jbegin,jup
          jp = jplus(j)
          tmp = q(j,k,5)*xyj(j,k)
          wk1(j,k) = q(jp,k,5)*xyj(jp,k) - tmp
          wk2(j,k) = q(j,kp,5)*xyj(j,kp) - tmp
        end do
      end do

      do k = klow,kup
        kp = k + 1
        km = k - 1
        do j = jlow,jup
          jp = jplus(j)
          jm = j - 1
          tmp = q(j,k,5)*xyj(j,k)
          wk3(j,k) = -q(jm,k,5)*xyj(jm,k) + 2*tmp -q(jp,k,5)*xyj(jp,k)
          wk4(j,k) = -q(j,km,5)*xyj(j,km) + 2*tmp -q(j,kp,5)*xyj(j,kp)
        end do
      end do

      if (.not. frozen .or. ( frozen .and. ifrz_what.eq.2 ) ) then
         do k = klow,kup
            do j = jlow,jup

               ri  = 1.d0/q(j,k,1)
               ri2 = ri*ri/xyj(j,k)
               ri = ri/xyj(j,k)

               duudq(1) = - ( q(j,k,2)*ri2*xy(j,k,1) + q(j,k,3)*ri2
     &              *xy(j,k,2) )
               duudq(2) = ri*xy(j,k,1) 
               duudq(3) = ri*xy(j,k,2)

               dvvdq(1) = - ( q(j,k,2)*ri2*xy(j,k,3) + q(j,k,3)*ri2
     &              *xy(j,k,4) )
               dvvdq(2) = ri*xy(j,k,3) 
               dvvdq(3) = ri*xy(j,k,4)

c     -- code to evaluate dUUdQ and dVVdQ by finite differences --
c     -- results are not much different from analytic derivation -- 
c     uum = (q(j,k,2)*xy(j,k,1)+q(j,k,3)*xy(j,k,2))/q(j,k,1) 
c     vvm = (q(j,k,2)*xy(j,k,3)+q(j,k,3)*xy(j,k,4))/q(j,k,1) 
c     
c     do n = 1,3
c     tmp = q(j,k,n)
c     stepsize = 1.d-4*q(j,k,n)
c     if ( dabs(stepsize) .lt. 1.d-5 ) then
c     stepsize = 1.d-5*dsign(1.d0,stepsize)
c     end if
c     
c     c     -- evaluate (+) state --
c     q(j,k,n) = q(j,k,n) + stepsize
c     stepsize = q(j,k,n) - tmp
c     
c     uup = (q(j,k,2)*xy(j,k,1)+q(j,k,3)*xy(j,k,2))/q(j,k,1) 
c     vvp = (q(j,k,2)*xy(j,k,3)+q(j,k,3)*xy(j,k,4))/q(j,k,1)
c     
c     duudq(n) = (uup-uum)/stepsize/xyj(j,k)
c     dvvdq(n) = (vvp-vvm)/stepsize/xyj(j,k)
c     
c     q(j,k,n) = tmp
c     end do
c     -------------------------------------------------------------

               if ( uu(j,k) .ge. 0.d0 ) then
                  do n = 1,3
                     if (dissCon) then
                        db(n,j,k) = db(n,j,k) +  
     &                       (wk1(j-1,k)*duudq(n))
     &                       + lamDissT*0.5*wk3(j,k)*duudq(n)
                     else
                        db(n,j,k) = db(n,j,k) + wk1(j-1,k)*duudq(n)
                     end if
                  end do
               else
                  do n = 1,3
                     if (dissCon) then
                        db(n,j,k) = db(n,j,k) +  
     &                       (wk1(j,k)*duudq(n))
     &                       - lamDissT*0.5*wk3(j,k)*duudq(n)
                     else
                        db(n,j,k) = db(n,j,k) + wk1(j,k)*duudq(n)
                     end if
                  end do
               end if

               if ( vv(j,k) .ge. 0.d0 ) then
                  do n = 1,3
                     if (dissCon) then
                        db(n,j,k) = db(n,j,k) +  
     &                       (wk2(j,k-1)*dvvdq(n))
     &                       + lamDissT*0.5*wk4(j,k)*dvvdq(n)
                     else
                        db(n,j,k) = db(n,j,k) + wk2(j,k-1)*dvvdq(n)
                     end if
                  end do
               else
                  do n = 1,3
                     if (dissCon) then
                        db(n,j,k) = db(n,j,k) +  
     &                       (wk2(j,k)*dvvdq(n))
     &                       - lamDissT*0.5*wk4(j,k)*dvvdq(n)
                     else
                        db(n,j,k) = db(n,j,k) + wk2(j,k)*dvvdq(n)
                     end if
                  end do
               end if

            end do
         end do      
      end if

      return
      end                       !dsa_adv
