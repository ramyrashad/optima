c----------------------------------------------------------------------
c     -- find the farthest point away from the trailing edge mid-point
c     to find the leading edge --   
c     -- m. nemec --
c----------------------------------------------------------------------
      subroutine findLE (ips, bap, ibody, xle, yle, ile)
      
      implicit none

      integer i, ips, ile, ibody
      double precision bap(ibody,2), xle, yle, xmid, ymid, dist, dist2

      xmid = 0.5d0*(bap(1,1) + bap(ips,1))
      ymid = 0.5d0*(bap(1,2) + bap(ips,2))
      dist = 0.0d0
      do i=2,ips-1
         dist2 = sqrt( (bap(i,1)-xmid)**2 + (bap(i,2)
     &        -ymid)**2 )
         if (dist2.gt.dist) then
            dist = dist2
            xle = bap(i,1)
            yle = bap(i,2)
            ile = i
         endif
      end do
      
      return
      end                       ! findLE
