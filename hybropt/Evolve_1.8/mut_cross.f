      subroutine mut_cross(N,nchrom,ngene,nobfun,pop,fit,nfuneval,beta,
     $     beta_sin,fit_init,genemin,genemax,debug,io_34,
     $     beta_min,beta_max,n_beta_s,n_beta_i,
     $     box_over, prob_pert, prob_mut, prob_avg, i_avg_cho,
     $     XBEST,      XAVG,       XPERT,      XMUT,
     $     NBEST,      NAVG,       NPERT,      NMUT,
     $     compute_fit,cbest)
C
      SAVE
      logical box_over,beta_sin,debug,io_34,cbest
      logical compute_fit(nchrom)
      real*8 rand
C
      dimension pop(ngene,nchrom),fit(nobfun,nchrom)
      dimension zzz(ngene,nchrom),zzzfit(nobfun,nchrom)
      dimension genemin(ngene), genemax(ngene)

C-----------------------------------------------------------------------
C          *** COMPUTE THE INTEGER PARAMETERS THAT WILL
C              CONTROL THE SELECTION PROCESS. THEY ARE
C              DEFINED AS FOLLOWS:
C              XBEST....PERCENT OF THOSE INDIVIDUALS IN THE 
C                       CURRENT POPULATION THAT ARE PASSED
C                       THROUGHT TO THE NEXT GENERATION WITHOUT
C                       CROSSOVER OF MUTATION (REAL NUMBER 
C                       BOUNDED BY 0.0 AND 100.0) ***
C              XAVG.....PERCENT OF THOSE INDIVIDUALS IN THE 
C                       NEW POPULATION THAT ARE DETERMINED 
C                       USING THE RANDOM AVERAGE CROSSOVER 
C                       OPERATOR (REAL NUMBER BOUNDED BY 0.0 
C                       AND 100.0) ***
C              XPERT....PERCENT OF THOSE INDIVIDUALS IN THE 
C                       NEW POPULATION THAT ARE DETERMINED 
C                       USING THE PERTURBATION MUTATION 
C                       OPERATOR (REAL NUMBER BOUNDED BY 0.0 
C                       AND 100.0) ***
C              XMUT.....PERCENT OF THOSE INDIVIDUALS IN THE 
C                       NEW POPULATION THAT ARE DETERMINED 
C                       USING THE RANDOM MUTATION OPERATOR 
C                       (REAL NUMBER BOUNDED BY 0.0 AND
C                       100.0) ***
C-----------------------------------------------------------------------
C          *** MAKE SURE THAT NBEST+NAVG+NPERT+NMUT=NCHROM.
C              OTHERWISE MODIFY THE VALUES UNTIL CONDITION
C              IS SATISFIED ***
C
      if(debug)write(49,*)' Call to mut_cross'

      if(debug)then
         do nc = 1,nchrom
            write(49,*)'fit(',nc,') = ',(fit(no,nc),no=1,nobfun)
         enddo
      endif

      pi = 4.0*atan(1.0)
      NBEST = XBEST*NCHROM/100
      NAVG  = XAVG*NCHROM/100
      NPERT = XPERT*NCHROM/100
      NMUT  = XMUT*NCHROM/100
C
      NTOT  = NBEST+NAVG+NPERT+NMUT
      IF (NTOT.NE.NCHROM) NBEST = NBEST+1
      NTOT  = NBEST+NAVG+NPERT+NMUT
      IF (NTOT.NE.NCHROM) NAVG = NAVG+1
      NTOT  = NBEST+NAVG+NPERT+NMUT
      IF (NTOT.NE.NCHROM) NPERT = NPERT+1
      NTOT  = NBEST+NAVG+NPERT+NMUT
      IF (NTOT.NE.NCHROM) NMUT = NMUT+1
C
      mod_count = 0
      if(debug)write(49,401)NBEST,NAVG, NPERT,NMUT
 401  format('>>>>  P Vector indecies: NBEST = ',i3,' NAVG = ',i3,
     $     ' NPERT = ',i3,' NMUT = ',i3,' <<<<')
C-----------------------------------------------------------------------
C Load selection into zzz and zzzfit (note fitness valid here, but not ofter operations)
      do nc = 1,nchrom
         do ng = 1,ngene
            zzz(ng,nc) = pop(ng,nc)
         enddo
         do no = 1,nobfun
            zzzfit(no,nc) = fit(no,nc)
         enddo
      enddo
         
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** THIS SECTION OF CODE USES SEVERAL DIFFERENT
C              OPERATIONS TO DETERMINE HOW THE NEWLY 
C              SELECTED POPULATION DISTRIBUTION (ZZZ) WILL
C              BE MODIFIED. THESE OPERATORS ARE LISTED AS
C              FOLLOWS: 
C              (1) PASS THROUGH
C              (2) RANDOM AVERAGE CROSSOVER
C              (3) PERTURBATION MUTATION
C              (4) RANDOM MUTATION
C-----------------------------------------------------------------------
C          *** PASS THROUGH OPERATOR *** 
C              THE NBEST INDIVIDUALS WITH THE HIGHEST VALUES 
C              OF FITNESS ARE SIMPLY PASSED TRHOUGH TO THE 
C              NEXT GENERATION WITHOUT BEING ALTERED. THE
C              FITNESS VALUES (FIT) FOR THESE INDIVIDUALS
C              CAN ALSO BE SAVED REDUCING THE NUMBER OF
C              FUNCATION EVALUATIONS THAT ARE REQUIRED ***
C
      N1 = NBEST
      DO NC=1,N1
         DO NG=1,NGENE
            POP(NG,NC) = ZZZ(NG,NC)
         ENDDO
         DO NO=1,NOBFUN
            FIT(NO,NC) = ZZZFIT(NO,NC)
         ENDDO
         compute_fit(nc) = cbest
      ENDDO
      DO NC=1,N1
         DO NG=1,NGENE
            IF (POP(NG,NC).LT.GENEMIN(NG))then
               POP(NG,NC) = GENEMIN(NG)
               compute_fit(nc) = .true.
            endif
            IF (POP(NG,NC).GT.GENEMAX(NG))then
               POP(NG,NC) = GENEMAX(NG)
               compute_fit(nc) = .true.
            endif
         enddo
      enddo

C-----------------------------------------------------------------------
C          *** RANDOM AVERAGE CROSSOVER OPERATOR: 
C              THIS SECTION OF CODE USES A SPECIAL FORM  
C              OF CROSSOVER IN CONJUNCTION WITH A RANDOM
C              NUMBER (RAN3) GENERATOR TO CONSTRUCT NEW 
C              CHROMOSOMES. THE GENERATION NUMBER (N) IS 
C              THE "SEED" FOR THE RANDOM NUMBER GENERATOR 
C              AND INSURES THAT EACH GENERATION WILL HAVE
C              A RANDOM SEQUENCE OF NUMBERS. THE NEW 
C              POPULATION IS HEAVILY BIASED TOWARD THE 
C              MORE FIT INDIVIDUALS AND THUS THEIR GENES 
C              WILL BE SELECTED MORE OFTEN THAN LESS FIT 
C              INDIVIDUALS ***
C
      N2 = NBEST+1
      N3 = NBEST+NAVG
      DO NC=N2,N3
         if(i_avg_cho.eq.0)then
            DO NG=1,NGENE
               CALL RAN3(N,RAND)
               if(rand.le.prob_avg) then
                  IR1        =  INT(RAND*FLOAT(NCHROM))+1
                  CALL RAN3(N,RAND)
                  IR2        =  INT(RAND*FLOAT(NCHROM))+1
                  c1 = 0.5
                  c2 = 0.5
                  if(box_over)then
                     c1 = -0.5 + 2.0*rand
                     c2 = 1.0 - c1
                  endif
                  POP(NG,NC) = c1*ZZZ(NG,IR1)+c2*ZZZ(NG,IR2)
                  if(debug)then
                     mod_count= mod_count + 1
                     write(49,402)ir1,ir2,ng,c1,c2,nc
                  endif
                  IF (POP(NG,NC).LT.GENEMIN(NG))POP(NG,NC) = GENEMIN(NG)
                  IF (POP(NG,NC).GT.GENEMAX(NG))POP(NG,NC) = GENEMAX(NG)
               endif
            ENDDO
         elseif(i_avg_cho.eq.1)then
           CALL RAN3(N,RAND)
            IR1        =  INT(RAND*FLOAT(NCHROM))+1
            CALL RAN3(N,RAND)
            IR2        =  INT(RAND*FLOAT(NCHROM))+1
            DO NG=1,NGENE
               CALL RAN3(N,RAND)
               if(rand.le.prob_avg) then
                  c1 = 0.5
                  c2 = 0.5
                  if(box_over)then
                     CALL RAN3(N,RAND)
                     c1 = -0.5 + 2.0*rand
                     c2 = 1.0 - c1
                  endif
                  POP(NG,NC) = c1*ZZZ(NG,IR1)+c2*ZZZ(NG,IR2)
                  if(debug)then
                     mod_count= mod_count + 1
                     write(49,402)ir1,ir2,ng,c1,c2,nc
 402                 format(' Chromosomes',i3,2x,i3,'  Gene # ',i3,  
     $                    '  Using Cross_avg values ',f8.4,2x,f8.5,
     $                    '  to produce Chromosome #',i3)
                  endif
                  IF (POP(NG,NC).LT.GENEMIN(NG))POP(NG,NC) = GENEMIN(NG)
                  IF (POP(NG,NC).GT.GENEMAX(NG))POP(NG,NC) = GENEMAX(NG)

               endif
            ENDDO

         endif
c
c  check if chromo changed
c
         do ng = 1,ngene
            if(pop(ng,nc) .ne. zzz(ng,nc))go to 200
         enddo
         if(.not.cbest)then
            compute_fit(nc) = cbest
            do no = 1,nobfun
               fit(no,nc) = zzzfit(no,nc)
            enddo
         endif
         go to 201
 200     continue
c
c  load default fitness since this chromosome has changed
         DO NO=1,NOBFUN
            FIT(NO,NC) = fit_init
         ENDDO
 201     continue
      ENDDO
C
C-----------------------------------------------------------------------
C          *** PERTURBATION MUTATION OPERATOR:
C              THIS SECTION OF CODE USES SMALL PERTURBATION
C              MUTATIONS TO ESTABLISH THE NEXT SET OF 
C              INDIVIDUALS IN THE NEW POPULATION 
C              DISTRIBUTION ***
C
      N4   = NBEST+NAVG+1
      N5   = NBEST+NAVG+NPERT
      BET  =  ABS(BETA)
      BET1 = BET
      BET2 = BET*0.2
      if(nfuneval.ne.0)then
         IF (BETA.LT.0.0.AND.NFUNEVAL.GT.300) THEN
            FACT = (FLOAT(NFUNEVAL)-300.0)/300.0
            IF (FACT.GT.1.0) FACT = 1.0
            BET  =  BET1*(1.0-FACT)+BET2*FACT
         ENDIF
         if (beta_min .ne. 0.0 .and. nfuneval.ge.n_beta_s)then
            if(.not.beta_sin)then
               FACT = (FLOAT(NFUNEVAL)-float(n_beta_s))/float(n_beta_i)
               IF (FACT.GT.1.0) FACT = 1.0
               BET  =  beta_max*(1.0-FACT)+beta_min*FACT
            elseif(beta_sin)then
               bet = beta_min + abs(sin(nfuneval*pi/n_beta_i))*beta_max
            endif
         endif
      endif
      if(io_34)write(34,2000)bet,nfuneval
      if(debug)write(49,2000)bet,nfuneval
 2000 format('  Value of Beta used in pert = ',e12.6,' With ',i8,
     $     ' Functions Evaluations')

      DO NC=N4,N5
         CALL RAN3(N,RAND)
         IR         =  INT(RAND*FLOAT(NCHROM))+1
         DO NG=1,NGENE
            POP(NG,NC) = ZZZ(NG,IR)
         ENDDO
         if(debug)write(49,405)ir,nc
 405     format('Choose Chromosome #',i3,' for chromo # ', i3)
         pop_change = 0
         DO NG=1,NGENE
            CALL RAN3(N,RAND)
            IF (RAND.LE.prob_pert) THEN
               pop_change = pop_change + 1
               CALL RAN3(N,RAND)
               FACT       = (GENEMAX(NG)-GENEMIN(NG))*(RAND-0.5)*BET
               POP(NG,NC) =  POP(NG,NC)+FACT
               IF (POP(NG,NC).LT.GENEMIN(NG)) POP(NG,NC) = GENEMIN(NG)
               IF (POP(NG,NC).GT.GENEMAX(NG)) POP(NG,NC) = GENEMAX(NG)
               if(debug)then
                  mod_count= mod_count + 1
                  write(49,403)nc,ng,fact
 403              format(' Chromosome #',i3,'  Gene # ',i3,  
     $                 '  pert-mutated with value ',e14.7)

               endif
            ENDIF
         ENDDO

         if(.not.cbest .and. pop_change .eq. 0)then
            compute_fit(nc) = cbest
            do no = 1,nobfun
               fit(no,nc) = zzzfit(no,ir) ! note this if fit of ir not nc
            enddo
         else
c
c  load default fitness since this chromosome has changed
            DO NO=1,NOBFUN
               FIT(NO,NC) = fit_init
            ENDDO

         endif

      ENDDO
C
C-----------------------------------------------------------------------
C          *** RANDOM MUTATION OPERATOR:
C              THIS SECTION OF CODE USES MUTATION TO 
C              ESTABLISH THE LAST SET OF INDIVIDUALS IN 
C              THE NEW POPULATION DISTRIBUTION ***
C
      N6 = NBEST+NAVG+NPERT+1
      DO NC=N6,NCHROM
         CALL RAN3(N,RAND)
         IR = INT(RAND*FLOAT(NCHROM))+1
         if(debug)write(49,405)ir,nc
         DO NG=1,NGENE
            POP(NG,NC) = ZZZ(NG,IR)
         ENDDO
         pop_change = 0
         DO NG=1,NGENE
            CALL RAN3(N,RAND)
            IF (RAND.LE.prob_mut) THEN
               pop_change = pop_change + 1
               CALL RAN3(N,RAND)
               POP(NG,NC) = RAND*(GENEMAX(NG)-GENEMIN(NG))+GENEMIN(NG)
               if(debug)then
                  mod_count= mod_count + 1
                  write(49,404)nc,ng,rand
 404              format(' Chromosome #',i3,'  Gene #',i3,  
     $                 '  Rand-Mutated with Min/max range value ',f10.4)
               endif
            ENDIF
            IF (POP(NG,NC).LT.GENEMIN(NG)) POP(NG,NC) = GENEMIN(NG)
            IF (POP(NG,NC).GT.GENEMAX(NG)) POP(NG,NC) = GENEMAX(NG)
         ENDDO

         if(.not.cbest .and. pop_change .eq. 0)then
            compute_fit(nc) = cbest
            do no = 1,nobfun
               fit(no,nc) = zzzfit(no,ir) ! note this if fit of ir not nc
            enddo
         else
c
c  load default fitness since this chromosome has changed
            DO NO=1,NOBFUN
               FIT(NO,NC) = fit_init
            ENDDO

         endif

      ENDDO
C
C          *** SELECTION OPERATION IS COMPLETE ***
C-----------------------------------------------------------------------
      if(debug)write(49,*)' # of Mods ',mod_count
      if(debug)write(49,*)' end of mut_cross'

      if(debug)then
         do nc = 1,nchrom
            write(49,*)'fit(',nc,') = ',(fit(no,nc),no=1,nobfun)
         enddo
      endif
C-----------------------------------------------------------------------
      if(debug)then
         write(49,*)'nc compute_fit'
         do nc = 1,nchrom
            write(49,'(i5,L4)') nc,compute_fit(nc)
         enddo
      endif
C-----------------------------------------------------------------------
C
      RETURN
      END
