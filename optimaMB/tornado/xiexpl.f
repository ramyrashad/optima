      SUBROUTINE xiexpl(jdim,kdim,q,s,press,sndsp,turmu,fmu,vort,
     &     turre,vk,ve,x,y,xy,xyj,xit,ds,uu,ccx,coef2x,coef4x,spect,
     &     precon,tmp)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer jdim,kdim,i,n,lq,lg,ii,ix,iy,ixy

      double precision q(jdim*kdim*4),tmp(jdim*kdim*4)
      double precision spect(jdim*kdim*3),precon(jdim*kdim*8)
      double precision press(jdim*kdim),sndsp(jdim*kdim)
      double precision s(jdim*kdim*4),xy(jdim*kdim*4),xyj(jdim*kdim)
      double precision xit(jdim*kdim),ds(jdim*kdim)
      double precision x(jdim*kdim),y(jdim*kdim),turmu(jdim*kdim)
      double precision vort(jdim*kdim),turre(jdim*kdim),fmu(jdim*kdim)
      double precision vk(jdim*kdim),ve(jdim*kdim)
      double precision uu(jdim*kdim),ccx(jdim*kdim)
      double precision coef2x(jdim*kdim),coef4x(jdim*kdim)

#include "../include/scratch.inc"

      logical frozen
      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

c ********************************************************************
c **                      explicit operators                        **
c ********************************************************************

cmt ~~~ remember the NK version doesn't work with higher order methods
cmt ~~~ iord=4 needs to be implemented later

c      IF (iord.EQ.4) THEN
c         DO i=nblkstart,nblkend
c         lq=lqptr(i)
c         lg=lgptr(i)
c            CALL rhsx(jmax(i),kmax(i),q(lq),s(lq),
c     &           press(lg),xy(lq),xyj(lg),xit(lg),d,
c     &           jbegin2(i),jend2(i),jlow(i)-nsd2(i),jup(i)+nsd4(i),
c     &           kminbnd(i),kmaxbnd(i))
c         ENDDO
c      ELSE
         DO i=nblkstart,nblkend
            lq=lqptr(i)
            lg=lgptr(i)
            CALL rhsx(jmax(i),kmax(i),q(lq),s(lq),
     &           press(lg),xy(lq),xyj(lg),xit(lg),d,
     &           jbegin(i),jend(i),jlow(i),jup(i),
     &           kminbnd(i),kmaxbnd(i))
         ENDDO
c      ENDIF

c     -range for s is low/up so no halo copy is required


c     -viscous rhs
      if (visxi) then
         do 60 ii=nblkstart,nblkend
            call visrhsnx(jmax(ii),kmax(ii),q(lqptr(ii)),
     &           press(lgptr(ii)),s(lqptr(ii)),turmu(lgptr(ii)),
     &           fmu(lgptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &           d,d(1,2),d(1,3),d(1,4),d(1,5), 
     &           d(1,6),d(1,7),d(1,8),d(1,9),d(1,10),
     &           jbegin(ii),jend(ii),jlow(ii),jup(ii),
     &           kminbnd(ii),kmaxbnd(ii))
 60      continue
      endif      

c     -calculate eigenvalues for xi sweep
      ix = 1
      iy = 2
      DO i=nblkstart,nblkend
         lg=lgptr(i)
         lq=lqptr(i)
         CALL eigval(ix,iy,jmax(i),kmax(i),q(lq),
     &        press(lg),sndsp(lg),xyj(lg),
     &        xy(lq),xit(lg),uu(lg),ccx(lg),
     &        jbegin2(i),jend2(i),kminbnd(i),kmaxbnd(i))
      ENDDO
      
c     -- optimization debug: freezing absolute values --
      if (frozen .and. ifrzw.eq.1) then
         do ii=nblkstart,nblkend
            lg = lgptr(ii)
            call copy_array( jmax(ii), kmax(ii), 1, fra1(lg),
     &           uu(lg))
         end do
      end if

c   ****************************************************************
c   *                                                              *
c   *        matrix artificial dissipation in xi direction         *
c   *                                                              *
c   ****************************************************************

      IF (idmodel.EQ.2) THEN
         do 105 ii=nblkstart,nblkend
            call mspectx(jmax(ii),kmax(ii),uu(lgptr(ii)),ccx(lgptr(ii)),
     &           xyj(lgptr(ii)),xy(lqptr(ii)),spect(lspptr(ii)),
     &           jbegin2(ii),jend2(ii),
     &           kminbnd(ii),kmaxbnd(ii))
 105     continue

cmt ~~~ dis2x=0 does not work, stops at drdq.f! 
cmt ~~~ that's why this part is commented out. 

c       -calculate the gradient coef in x & store it in array coef2x
c       -bypass logic if dis2x = 0.0                                         
c         if (dis2x.ne.0.d0) then
c            ixy=1
c            do 106 ii=nblkstart,nblkend
c               call gradcoef(ixy,jmax(ii),kmax(ii),press(lgptr(ii)),
c     &              xyj(lgptr(ii)),coef2x(lgptr(ii)),work1(lgptr(ii)),
c     &              jbegin2(ii),jend2(ii),
c     &              kminbnd(ii),kmaxbnd(ii),
c     &              .true.,.true.,eta1mesh(ii),eta2mesh(ii))
c 106        continue
c         endif
     
c       -compute the second and fourth order dissipation coefficients
         do 120 ii=nblkstart,nblkend
            call mcoef24x(jmax(ii),kmax(ii),
     &           coef2x(lgptr(ii)),coef4x(lgptr(ii)),
     &           jbegin2(ii),jend2(ii),
     &           kminbnd(ii),kmaxbnd(ii))
 120     continue

c       -compute the second and fourth difference dissipation
         if (psiavg) then
            do 130  ii=nblkstart,nblkend
               call expmatx(jmax(ii),kmax(ii),q(lqptr(ii)),
     &              coef2x(lgptr(ii)),
     &              coef4x(lgptr(ii)),sndsp(lgptr(ii)),s(lqptr(ii)),
     &              spect(lspptr(ii)),xyj(lgptr(ii)),press(lgptr(ii)),
     &              ccx(lgptr(ii)),uu(lgptr(ii)),xy(lqptr(ii)),
     &              d(1,1),d(1,5),d(1,9),
     &              c(1,1),c(1,2),c(1,3),c(1,4),c(1,5),c(1,6),
     &              jbegin2(ii),jend2(ii),
     &              kminbnd(ii)+1,kmaxbnd(ii)-1)
 130        continue
         else
c       -you can use only this call if you'd like, but one can reduce
c        number of operations by using the call above for psiavg=true
            do 131  ii=nblkstart,nblkend
               call expmatx(jmax(ii),kmax(ii),q(lqptr(ii)),
     &              coef2x(lgptr(ii)),
     &              coef4x(lgptr(ii)),sndsp(lgptr(ii)),s(lqptr(ii)),
     &              spect(lspptr(ii)),xyj(lgptr(ii)),press(lgptr(ii)),
     &              ccx(lgptr(ii)),uu(lgptr(ii)),xy(lqptr(ii)),
     &              d(1,1),d(1,5),d(1,9),
     &              c(1,1),c(1,2),c(1,3),c(1,4),c(1,5),c(1,6),
     &              jbegin2(ii),jend2(ii),
     &              kminbnd(ii),kmaxbnd(ii))
 131        continue
         endif

      ELSE

c   ****************************************************************
c   *                                                              *
c   *        scalar artificial dissipation in xi direction         *
c   *                                                              *
c   ****************************************************************

c       -form spectral radii for dissipation model
         DO i=nblkstart,nblkend
            lq=lqptr(i)
            lg=lgptr(i)
            CALL spectx(jmax(i),kmax(i),uu(lg),ccx(lg),
     &           xyj(lg),xy(lq),coef4x(lg),precon(lprecptr(i)),
     &           jbegin2(i),jend2(i),klow(i),kup(i))
         ENDDO

c       **************************************************************
c         Dissipation terms : meth = 2, 3                        
c              -set up coefficients and variables for meth = 2,3 
c              -form pressure coefficients                     
c              -artificial dissipation coefficients            
c              -artificial dissipation                           
c       **************************************************************

cmt ~~~ dis2x=0 does not work, stops at drdq.f! 
cmt ~~~ that's why this part is commented out. 

c         IF (dis2x.NE.0.d0) THEN
c            ixy = 1
c         -calculate the gradient coef in x & store it in array coef2x
cmt ~~~ used to be gradcoef1, gradcoef2 and gradcoef3 ~~~
c            DO i=nblkstart,nblkend
c               lq=lqptr(i)
c               lg=lgptr(i)
c               CALL gradcoef(ixy,jmax(i),kmax(i),press(lg),
c     &              xyj(lg),coef2x(lg),work1(lg),
c     &              jbegin2(i),jend2(i),kminbnd(i),kmaxbnd(i),
c     &              .true.,.true.,eta1mesh(i),eta2mesh(i))
c            ENDDO
c         ENDIF

c       -compute the second and fourth order dissipation coefficients
         DO i=nblkstart,nblkend
            lq=lqptr(i)
            lg=lgptr(i)
            CALL coef24x(jmax(i),kmax(i),coef2x(lg),coef4x(lg),
     &           d,d(1,2),jbegin2(i),jend2(i),klow(i),kup(i))
         ENDDO

c       -do not copy coef2x and coef4x to neighbors --- it isn't required
cmt ~~~ used to be filterx1, filterx2, filterx3 and filterxs ~~~
         DO n=1,4
            DO i=nblkstart,nblkend
               lq=lqptr(i)
               lg=lgptr(i)
               CALL filterx(n,jmax(i),kmax(i),q(lq),xyj(lg),
     &              coef2x(lg),coef4x(lg),work1(lwf1ptr(i)),tmp(lq),
     &              jbegin2(i),jend2(i),klow(i),kup(i))
            ENDDO
         ENDDO

         DO i=nblkstart,nblkend
            lq=lqptr(i)
            lg=lgptr(i)
            CALL predis(jmax(i),kmax(i),q(lq),s(lq),xyj(lg),
     &           sndsp(lg),precon(lprecptr(i)),tmp(lq),
     &           jlow(i),jup(i),klow(i),kup(i))
         ENDDO

      ENDIF

      RETURN
      END
