c ********************************************************************
c **       Routine to copy data from the interior of block2         **
c **       to the halo points of block 1                            **
c **       For arrays dimensioned (jdim,kdim,4)                     **
c **       Includes rescaling by jacobian                           **
c ********************************************************************
c calling routine: integrat
c
c When copying data, the sign does not change.
c Note: the range of copying is  low to up.  Points must match by 
c index in opposing blocks but low and up may differ so min/max 
c must be used!
c
c
      subroutine halo4j(iblk,nhalo,nside1,nside2,
     &  jdim1,kdim1,arr1,xyj1,
     &  jbegin1,jend1,kbegin1,kend1,
     &  jdim2,kdim2,arr2,xyj2,
     &  jbegin2,jend2,kbegin2,kend2)
c
      dimension arr1(jdim1,kdim1,4), xyj1(jdim1,kdim1)
      dimension arr2(jdim2,kdim2,4), xyj2(jdim2,kdim2)
c
c
c     -indices for begining and end of blocks
c      -note jbegin and jend extend full scope of block including nhalo
      jb1=jbegin1+nhalo
      je1=jend1-nhalo
      jb2=jbegin2+nhalo
      je2=jend2-nhalo
c      -note kbegin and kend extend full scope of block including nhalo
c       only when copying halo rows, during halo column copy ... not so
      kb1=kbegin1+nhalo
      ke1=kend1-nhalo
      kb2=kbegin2+nhalo
      ke2=kend2-nhalo
c
      do 99 ii=0,nhalo-1
c ********************************************************************
c **  block 1   :      side 1 or 3                                  **
c ********************************************************************
      if(nside1.eq.1.or.nside1.eq.3) then
        if (nside1.eq.1) then
          k1h = kbegin1 + ii
        else
          k1h = kend1 - ii
        endif
        if (nside2.eq.1.or.nside2.eq.3) then
c         -block 1 - side 1 or 3 connected to block 2 - side 1 or 3 
          if(nside2.eq.1) then
            k2i = kb2 + nhalo - ii
          else
            k2i = ke2 - nhalo + ii
          endif
          if(nside1.eq.nside2) then
            j1 = max(jbegin1,jdim2+1-jend2)
            j2 = min(jend1,jdim2+1-jbegin2)
            do 111 jj=j1,j2
              j1h = jj
              j2i = jdim2 + 1 - jj
              rr1 = 1./xyj1(j1h,k1h)
              do 110 n=1,4
                arr1(j1h,k1h,n) = arr2(j2i,k2i,n)*xyj2(j2i,k2i)*rr1
 110          continue
 111        continue
          else
            j1 = max(jbegin1,jbegin2)
            j2 = min(jend1,jend2)
            do 116 jj=j1,j2 
              j1h = jj
              j2i = jj
              rr1 = 1./xyj1(j1h,k1h)
              do 115 n=1,4
                arr1(j1h,k1h,n) = arr2(j2i,k2i,n)*xyj2(j2i,k2i)*rr1
 115          continue
 116        continue
          endif
        elseif(nside2.eq.2.or.nside2.eq.4)then
c         -block 1 - side 1 or 3 connected to block 2 - side 2 or 4 
          stop ' halo4j error'
        endif
c ********************************************************************
c **  block 1   :      side 2 or 4                                  **
c ********************************************************************
      elseif(nside1.eq.2.or.nside1.eq.4) then
        if(nside1.eq.2) then
          j1h = jbegin1 + ii
        else 
          j1h = jend1 - ii
        endif 
        if(nside2.eq.2.or.nside2.eq.4)then
csdr      -if nhalo=2 then second halo column is done first
          if(nside2.eq.2) then
            j2i = jb2 + nhalo - ii
          else
            j2i = je2 - nhalo + ii
          endif
          if(nside1.eq.nside2) then
            k1 = max(kbegin1,kdim2+1-kend2)
            k2 = min(kend1,kdim2+1-kbegin2)
            do 211 kk=k1,k2
              k1h = kk
              k2i = kdim2 + 1 - kk
              rr1 = 1./xyj1(j1h,k1h)
              do 210 n=1,4
                arr1(j1h,k1h,n) = arr2(j2i,k2i,n)*xyj2(j2i,k2i)*rr1
 210          continue
 211        continue
          else
            k1 = max(kbegin1,kbegin2)
            k2 = min(kend1,kend2)
              do 217 kk=k1,k2
                k1h = kk
                k2i = kk
                rr1 = 1./xyj1(j1h,k1h)
                do 215 n=1,4
                  arr1(j1h,k1h,n) = arr2(j2i,k2i,n)*xyj2(j2i,k2i)*rr1
c                  write (*,*) xyj2(j2i,k2i), xyj1(j1h,k1h)
 215            continue
 217          continue
          endif
        elseif(nside2.eq.1.or.nside2.eq.3)then
          stop ' halo4j error'
        endif
      else
        stop ' error in halo4j, nside1'
      endif

 99   continue
      return
      end
