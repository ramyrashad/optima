c----------------------------------------------------------------------
c-- regrid: grid perturbation --

c written by: marian nemec
c date: march, 2000
c modifications:
c   Anh Truong: use elasticity (finite element) method to deform grid.
c   Chad Oldfield: always revert to original grid before perturbing.
c   Markus Rumpfkeil: grid output option, klineconst parameter
c----------------------------------------------------------------------

      subroutine regrid ( i, jdim, kdim, dvs, idv, xgrid, ygrid, bap,
     &      bcp, bt, bknot,xOrig,yOrig,dx,dy,gridout)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

c Arguments (Chad Oldfield)
c i: index of the design variable that has been changed.  If all design
c   variables have be changed, then i = -1
c jdim, kdim: size of the grid in the j & k directions
c dvs: design variables.  The first ngdv design variables are geometric
c   design variables.  That is, they are y-coordinates of B-spline
c   control points.
c idv: design variable indices.  Design variable i corresponds to
c   the y-component of the B-spline control point idv(i).
c xgrid, ygrid: x- and y-coordinates of the grid node locations.  These
c   are for the grid that is returned. 
c xOrig, yOrig: x- and y-coordinates of the grid node locations, for the
c   original grid.
c bap:
c bcp: B-spline control point x- and y- coordinates.
c bt: B-spline parameter values (for each airfoil surface node)
c bknot: B-spline knot vector
c gridout: flag to indicate whether the grid should be outputted
c dx,dy: The increments in the grid, as computed by the elasticity
c   grid perturbation scheme.

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer h, i, j, k, ii, idv(nc+mpopt), jdim, kdim, update_dxy

      double precision xgrid(jdim,kdim), ygrid(jdim,kdim)
      double precision xOrig(jdim,kdim), yOrig(jdim,kdim) !original grid
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision dvs(nc+mpopt), bap(jbody,2), bcp(nc,2), bt(jbody)
      double precision bknot(jbsord+nc)

      double precision dely, tmp, bcp_tmp

      logical gridout

c     --  added by Anh Truong June 2005  --
      character command*60
      integer namelen, strlen
      external movegridfe
c     ---------------

c     -- work arrays --
c     -- compatible with cyclone --
      double precision segment(maxj*30)
      common/worksp/ segment 


c     Restore original grid before perturbing (Chad Oldfield)
      if (original_grid) then
         do k = kbegin,kend
            do j = jbegin,jend
               xgrid(j,k) = xOrig(j,k)
               ygrid(j,k) = yOrig(j,k)
            enddo
         enddo
      endif

      if ( i.eq.-1 ) then
c     -- regrid the whole domain --
        do ii = 1,ngdv
          bcp(idv(ii),2) = dvs(ii)
        end do
      else
c     -- regrid for selected design variable --
        bcp_tmp = bcp(idv(i),2)
        bcp(idv(i),2) = dvs(i)
      end if

c     -- calculate new airfoil profile --
      call new_ybody ( bknot, bt, bap(1,2), bcp )

c     -- adjust grid --
      if (incr == 0) then
        !-- Perturb grid around airfoil
        do j = jtail1,jtail2
          dely = bap(j-jtail1+1,2) - yOrig(j,1)

          if ( dabs(bap(j-jtail1+1,2) - ygrid(j,1)) .gt. 5.d-15 ) then
c       -- calculate arclength for k-line at location j --
            segment(1) = 0.d0
            do k = 2, kend
              tmp = dsqrt( (xOrig(j,k) - xOrig(j,k-1) )**2 +
     &              ( yOrig(j,k) - yOrig(j,k-1) )**2 )
              segment(k) = segment(k-1) + tmp
            end do

            do k = 1, kup
              ygrid(j,k) = yOrig(j,k) + dely*
     &              ( 1.d0 - segment(k)/segment(kmax) )
            end do
          end if
        end do

        !-- Perturb grid in the wake - perturb wake-cut:

        dely = bap(jtail2-jtail1+1,2) - yOrig(jtail2,1)

        !-- Calculate arclength for wake-cut at location j
        segment(1) = 0.d0
        h=1
        do j = jtail2+1, jend  
          h=h+1            
          tmp = dsqrt( (xOrig(j,1) - xOrig(j-1,1) )**2 +
     &              ( yOrig(j,k) - yOrig(j-1,k) )**2 )
          segment(h) = segment(h-1) + tmp

        end do
 
        ii=1
        do j = jtail2+1, jup !-- Upper wake-cut nodes
          ii=ii+1
          ygrid(j,1) = yOrig(j,1) + dely*
     &              ( 1.d0 - segment(ii)/segment(h) )

        end do

        ii=1
        do j = jtail1-1, jlow, -1 !-- Lower wake-cut nodes
          ii=ii+1
          ygrid(j,1) = yOrig(j,1) + dely*
     &              ( 1.d0 - segment(ii)/segment(h) )

        end do

        !-- Perturb k-lines in the wake:

        do j = jtail2+1,jend !-- Upper half of wake k-lines
          dely = ygrid(j,1) - yOrig(j,1)

c       -- calculate arclength for k-line at location j --
          segment(1) = 0.d0
          do k = 2, kend
            tmp = dsqrt( (xOrig(j,k) - xOrig(j,k-1) )**2 +
     &              ( yOrig(j,k) - yOrig(j,k-1) )**2 )
            segment(k) = segment(k-1) + tmp
          end do

          do k = 1, kup
            ygrid(j,k) = yOrig(j,k) + dely*
     &              ( 1.d0 - segment(k)/segment(kmax) )
          end do
 
        end do

        do j = jtail1-1, 1, -1 !-- Lower half of wake k-lines
          dely = ygrid(j,1) - yOrig(j,1)

c       -- calculate arclength for k-line at location j --
          segment(1) = 0.d0
          do k = 2, kend
            tmp = dsqrt( (xOrig(j,k) - xOrig(j,k-1) )**2 +
     &              ( yOrig(j,k) - yOrig(j,k-1) )**2 )
            segment(k) = segment(k-1) + tmp
          end do

          do k = 1, kup
            ygrid(j,k) = yOrig(j,k) + dely*
     &              ( 1.d0 - segment(k)/segment(kmax) )
          end do
 
        end do

        !-- Write perturbed grid to file

        call write_grid(xgrid, ygrid, jdim, kdim)

      else
c       Added by Anh Truong June 2005
c       Use the elasticity (finite element) method to perturb the grid
        filena = grid_file_prefix
        namelen = strlen(grid_file_prefix)
        filena(namelen+1:namelen+3) = '.g'
        namelen = namelen+2

        update_dxy = 0
        if (i == -1) update_dxy=1

c       Perturb the grid        
        call movegridfe (filena, namelen, jtail1, jtail2, xgrid, ygrid,
     &       bap(1,2), dx, dy, incr, moveTE, update_dxy, gptol, gpmaxit)
       
        !-- Write perturbed grid to file

        call write_grid(xgrid, ygrid, jdim, kdim)

      endif  !incr == 0


      if ( i.ne.-1 ) bcp(idv(i),2) = bcp_tmp

      !-- All processors to read in the newly perturbed grid file
    
      call read_grid(xgrid, ygrid, jdim, kdim)   
       
      return
      end                       !regrid
c----------------------------------------------------------------------

c----------------------------------------------------------------------
c     -- generate new y-coordinates for airfoil --

c     written by: marian nemec
c     date: march, 2000
c----------------------------------------------------------------------

      subroutine new_ybody ( bknot, bt, y, bcp )

#include "../include/optcom.inc"

      integer i,j,left

      double precision bt(jbody), bknot(jbsord+nc), y(jbody)
      double precision bcp(nc,2), tmp, BIatT(ibsord)

      tmp = -1.d0
      do j = 1,jbody
        do left = jbsord,nc
          if ( bknot(left).le.bt(j) .and. bt(j).le.bknot(left+1)
     &          .and. bt(j).ne.tmp ) then 
            tmp = bt(j)
            call Bval ( bknot, bt(j), left, BIatT )

            y(j) = 0.d0
            do i=1,jbsord
              y(j) = y(j) + bcp(i+left-jbsord,2)*BIatT(i)
            end do

          end if

        end do
      end do    

      return
      end                       !new_ybody
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c     calc. the value of all possibly nonzero b-spline basis functions
c     based on: carl de boor, 'practical guide to splines' pg. 134

c     written by: marian nemec
c     date: Feb. 16, 2000
c-----------------------------------------------------------------------

      subroutine Bval ( knot, t, left, BIatT)

#include "../include/optcom.inc"

      integer i, k, kp1, left
      
      double precision knot(jbsord+nc), t, BIatT(ibsord), tmp 
      double precision deltaR(ibsord), deltaL(ibsord), saved

      k = 1
      BIatT(1) = 1.d0
      if (k.ge.jbsord) return

      do while (k.lt.jbsord)
        kp1 = k+1
        deltaR(k) = knot(left+k) - t
        deltaL(k) = t - knot(left+1-k)
        saved = 0.d0

        do i = 1,k
          tmp = BIatT(i)/(deltaR(i) + deltaL(kp1-i))
          BIatT(i) = saved + deltaR(i)*tmp
          saved = deltaL(kp1-i)*tmp
        end do
        BIatT(kp1) = saved
        k = kp1
      end do

      return 
      end                       !Bval
c-----------------------------------------------------------------------
c     Program name: write_grid
c     
c     This subroutine writes the grid to file after it is perturbed
c
c-----------------------------------------------------------------------

      subroutine write_grid(x, y, jdim, kdim)

#ifdef _MPI_VERSION
      use mpi
#endif


      !-- Declare variables

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer
     &     namelen, strlen, jdim, kdim, j, k, d1, d10, d100

      double precision
     &     x(jdim,kdim), y(jdim,kdim)

      character
     &     command*60, filename*60

      !-- Open a scratch file for temporary storage of the grid

#ifdef _MPI_VERSION

      if (opt_meth .eq. 11) then
         d100 = ((mycommid - 1) * procs_per_chromo + rank) / 100
         d10  = ((mycommid - 1)*  procs_per_chromo + rank) / 10 
     &          - (d100*10)
         d1  =  ((mycommid - 1) * procs_per_chromo + rank) / 1
     &          - (d10*10) - (d100*100)
         filename = 'gridtmp'
         namelen =  len_trim(filename)

         if (size_glob .gt. 99) then
            filename(namelen+1:namelen+3) 
     &         = char(d100+48)//char(d10+48)//char(d1+48)
          else
            filename(namelen+1:namelen+2) 
     &         = char(d10+48)//char(d1+48)
         endif

      else
         d10  = rank / 10
         d1   = rank - (d10*10)
         filename = 'gridtmp'
         namelen =  len_trim(filename)
         filename(namelen+1:namelen+2)=char(d10+48)//char(d1+48)
      endif

#else

      d10  = 0 / 10
      d1   = 0 - (d10*10)
      filename = 'gridtmp'
      namelen =  len_trim(filename)
      filename(namelen+1:namelen+2)=char(d10+48)//char(d1+48)

#endif

      if (iread.eq.1) then
         open(unit=98,file=filename,status='unknown',
     &        form='formatted')
         rewind(98)
         write(98,*) jdim,kdim
         write(98,*) ((x(j,k),j=1,jdim),k=1,kdim),
     &     ((y(j,k),j=1,jdim),k=1,kdim)

      endif

      if (iread.eq.2) then
         open(unit=98,file=filename,status='unknown',
     &        form='unformatted')
         rewind(98)
         write(98) jdim,kdim
         write(98) ((x(j,k),j=1,jdim),k=1,kdim),
     &     ((y(j,k),j=1,jdim),k=1,kdim)

      endif

      close(98)
      call flush(98)

      return

      end


c-----------------------------------------------------------------------
c     Program name: read_grid
c     
c     This subroutine reads the grid from file after it is perturbed
c
c-----------------------------------------------------------------------

      subroutine read_grid(x, y, jdim, kdim)

#ifdef _MPI_VERSION
      use mpi
#endif


      !-- Declare variables

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer
     &     namelen, strlen, jdim, kdim, j, k, d1, d10, d100

      double precision
     &     x(jdim,kdim), y(jdim,kdim)

      character
     &     command*60, filename*60

      !-- Open a scratch file for temporary storage of the grid

#ifdef _MPI_VERSION

      if (opt_meth .eq. 11) then
         d100 = ((mycommid - 1) * procs_per_chromo + rank) / 100
         d10  = ((mycommid - 1)*  procs_per_chromo + rank) / 10 
     &          - (d100*10)
         d1  =  ((mycommid - 1) * procs_per_chromo + rank) / 1
     &          - (d10*10) - (d100*100)
         filename = 'gridtmp'
         namelen =  len_trim(filename)

         if (size_glob .gt. 99) then
            filename(namelen+1:namelen+3) 
     &         = char(d100+48)//char(d10+48)//char(d1+48)
          else
            filename(namelen+1:namelen+2) 
     &         = char(d10+48)//char(d1+48)
         endif

      else
         d10  = rank / 10
         d1   = rank - (d10*10)
         filename = 'gridtmp'
         namelen =  len_trim(filename)
         filename(namelen+1:namelen+2)=char(d10+48)//char(d1+48)
      endif
      
#else

      d10  = 0 / 10
      d1   = 0 - (d10*10)
      filename = 'gridtmp'
      namelen =  len_trim(filename)
      filename(namelen+1:namelen+2)=char(d10+48)//char(d1+48)

#endif
      
      if ( iread.eq.1 ) then  

        open(unit=98,file=filename,status='old',
     &        form='formatted')
        rewind(98)
        read(98,*) jdim, kdim
        read(98,*)  ((x(j,k),j=1,jdim) ,k=1,kdim), 
     &               ((y(j,k),j=1,jdim) ,k=1,kdim) 

      else if ( iread.eq.2 ) then 

        open(unit=98,file=filename,status='old',
     &        form='unformatted')
        rewind(98)
        read(98) jdim, kdim              
        read(98)  ((x(j,k),j=1,jdim) ,k=1,kdim),
     &           ((y(j,k),j=1,jdim) ,k=1,kdim)
      endif 

      close(98)

      return

      end
