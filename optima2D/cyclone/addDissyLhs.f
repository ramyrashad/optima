************************************************************************
      !-- Program name: addDissxLhs
      !-- Written by: Howard Buckley
      !-- Date: April 2010
      !-- 
      !-- If dissipation-based continuation is to be used as a
      !-- globalization method for N-K, then add additional 
      !-- dissipation to 1st order preconditioner for flow Jacobian
      !-- matrix.
************************************************************************

      subroutine  addDissyLhs(jdim,kdim,ndim,xyj,indx,icol,pa)

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      
      !-- Declare variables

      integer
     &     j, jdim, jpl, jmi, k, kdim, ii, jj, n, ndim, indx(jdim,kdim),
     &     icol(9), kmi, kpl

      double precision
     &     cf2ydc(jdim,kdim), c2, c2m,
     &     work(jdim,kdim,3), diag1, diag2, diag3,
     &     pa(jdim*kdim*ndim*ndim*5+1), xyj(jdim,kdim)

      !-- Calculate a scalar dissipation coefficient
      !-- governed by the continuation parameter 'lamDiss'

      do k = kbegin,kup
        kpl = k+1
        do j = jbegin,jend
          c2 = lamDiss*(specty_dc(j,kpl)+specty_dc(j,k))
          cf2ydc(j,k) = c2
        end do
      end do

      !-- Add aditional dissipation to 1st order preconditioner

      do j = jlow,jup
        do k = klow,kup
          kpl = k+1
          kmi = k-1
          c2 = cf2ydc(j,k)
          c2m = cf2ydc(j,kmi)

          diag1 = xyj(j,kmi)*c2m
          diag2 = xyj(j,k)  *(c2m + c2)
          diag3 = xyj(j,kpl)*c2
            
          do n =1,4
            ii = ( indx(j,k) - 1 )*ndim + n
            jj = ( ii - 1 )*icol(5)
            pa(jj+icol(1)+n) = pa(jj+icol(1)+n) - diag1
            pa(jj+icol(2)+n) = pa(jj+icol(2)+n) + diag2
            pa(jj+icol(3)+n) = pa(jj+icol(3)+n) - diag3
          end do                                         
        end do
      end do

      return
      end
