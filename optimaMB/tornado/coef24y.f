c     ------------------------------------------------------------------
c     -- compute nonlinear 2nd and 4th difference COEFficients --
c     ------------------------------------------------------------------
      subroutine coef24y( jdim, kdim, coef2y, coef4y, pcoef, spect,
     &     jbegin, jend, kbegin, kend)

      implicit none

#include "../include/common.inc"

c*****************************************************************
c   coef2y comes in as pressure gradient coefficient         
c   coef4y comes in as specy (spectral radius)                          
c*****************************************************************

      INTEGER jdim,kdim,jbegin,jend,kbegin,kend,j,k
      DOUBLE PRECISION coef2y(jdim,kdim),coef4y(jdim,kdim)
      DOUBLE PRECISION pcoef(jdim,kdim),spect(jdim,kdim),c2,c4

      do k = kbegin,kend                                              
         do j = jbegin,jend
            pcoef(j,k) = coef2y(j,k)
            spect(j,k) = coef4y(j,k)
         end do
      end do

      do k = kbegin,kend-1
         do j = jbegin,jend                                            
            c2 = dis2y*(pcoef(j,k+1)*spect(j,k+1)+pcoef(j,k)*spect(j,k))
            c4 = dis4y*(spect(j,k+1) + spect(j,k))
            c4 = c4 - min(c4,c2)
            coef2y(j,k) = c2
            coef4y(j,k) = c4
         end do
      end do

      return
      end
