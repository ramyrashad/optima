      subroutine fixte(jmax1,kmax1,j1,k1,q1,xyj1,press1,
     &                 sndsp1,turre1,vk1,ve1,
     &                 jmax2,kmax2,j2,k2,q2,xyj2,press2,
     &                 sndsp2,turre2,vk2,ve2,meanflow)
c
      dimension q1(jmax1,kmax1,4),xyj1(jmax1,kmax1)
      dimension press1(jmax1,kmax1),sndsp1(jmax1,kmax1)
      dimension q2(jmax2,kmax2,4),xyj2(jmax2,kmax2)
      dimension press2(jmax2,kmax2),sndsp2(jmax2,kmax2)
      dimension turre1(jmax1,kmax1), turre2(jmax2,kmax2)
      dimension vk1(jmax1,kmax1),ve1(jmax1,kmax1)
      dimension vk2(jmax2,kmax2),ve2(jmax2,kmax2)
      logical meanflow
c
#include "../include/common.inc"
c
c     The jacobian may change so rescale quantities
c     pressure contains jacobian, sound speed does not
c
c      write(6,*) 'j1,k1,q1',j1,k1,q1(j1,k1,1)
c      write(6,*) 'j2,k2,q2',j2,k2,q2(j2,k2,1)
c
      if (meanflow) then
        rr2 = 1./xyj2(j2,k2)
        pp1 = gami*(q1(j1,k1,4)-0.5*
     &        (q1(j1,k1,2)**2+q1(j1,k1,3)**2)/q1(j1,k1,1))*xyj1(j1,k1)
        q2(j2,k2,1)  = q1(j1,k1,1)*xyj1(j1,k1)*rr2
        q2(j2,k2,2)  = q1(j1,k1,2)*xyj1(j1,k1)*rr2
        q2(j2,k2,3)  = q1(j1,k1,3)*xyj1(j1,k1)*rr2
        q2(j2,k2,4)  = pp1/gami*rr2 +
     *        0.5*(q2(j2,k2,2)**2+q2(j2,k2,3)**2)/q2(j2,k2,1)
c        
        press2(j2,k2)= press1(j1,k1)*xyj1(j1,k1)*rr2
        sndsp2(j2,k2)= sndsp1(j1,k1)
      endif
c      
      turre2(j2,k2) = turre1(j1,k1)
      vk2(j2,k2)=vk1(j1,k1)
      ve2(j2,k2)=ve1(j1,k1)
c
 999  return
      end
