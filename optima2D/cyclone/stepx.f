c ########################
c ##                    ##
c ##  subroutine stepx  ##
c ##                    ##
c ########################
c
      subroutine stepx(jdim,kdim,q,s,press,sndsp,turmu,fmu,ds,xy,xyj,   
     >                 xit,coef2,coef4,a,b,c,d,e,dj,ddj)                
c
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),xit(jdim,kdim),ds(jdim,kdim)             
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension press(jdim,kdim),sndsp(jdim,kdim)                       
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension turmu(jdim,kdim),fmu(jdim,kdim)                         
c                                                                       
      dimension a(jdim*kdim*4),b(jdim*kdim*16),c(jdim*kdim*16),         
     >          d(jdim*kdim*16),e(jdim*kdim*4)                          
      dimension dj(jdim*kdim*12),ddj(jdim*kdim*12)                      
c                                                                       
c
c
c
c                         block tridiagonal
c======================================================================
c old stuff =>           if (meth .eq. 1) then                  
c
c
c---- flux jacobians a ----
c     : we pass d_xi/dx, d_xi/dy & d_xi/dt to fillmat.f
c      call fillmat(jdim,kdim,q,xy(1,1,1),xy(1,1,2),xit,d)            
c     : s  must be zero on b.c.                                            
c
c
c---- solving a block tridiagonal by lu decomp. ----
c      if (.not.periodic) then                       
c         call btrix (jdim,kdim,xyj,xy,ds,coef4,a,b,c,d,s,jlow,jup,
c     >               klow,kup) 
c      else      
c         call btripx (jdim,kdim,xyj,xy,ds,                                  
c     >                coef4,a,b,c,d,e,s,jlow,jup,klow,kup)         
c      endif                                                             
c
c
c
c
c                         block pentadiagonal
c======================================================================
c      if (meth.eq.2) then
c
c                                                                       
c---- form block penta ----
      call filpnx(jdim,kdim,q,press,sndsp,coef2,coef4,                  
     >            xit,xy,xyj,turmu,fmu,ds,a,b,c,d,e,dj,ddj,s)
c
c
c---- solve block penta --**** ONLY FOR NON-PERIODIC MESHES ****--
      if (ibc) then
         call bpentx(jdim,kdim,jbegin,jend,klow,kup,a,b,c,d,e,s)              
      else
         call bpentx(jdim,kdim,jlow,jup,klow,kup,a,b,c,d,e,s)              
      endif                                                             
c                                                                       
      return                                                          
      end                                                             
