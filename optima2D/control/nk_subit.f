      subroutine nk_subit(jdim,kdim,ndim,qp,q,qold,s,akk)
      
      implicit none
                                                                      
#include "../include/arcom.inc"

      integer n,k,j, jdim, kdim,ndim
      double precision q(jdim,kdim,ndim),s(jdim,kdim,ndim)
      double precision qold(jdim,kdim,ndim),qp(jdim,kdim,ndim)      
      double precision akk,mprfac1,mprfac2,mprfac3,dttmp
      
      if (jesdirk.eq.4 .or. jesdirk.eq.3 )then 

            do 500 n=1,ndim
            do 500 k=klow,kup
            do 500 j=jlow,jup
               s(j,k,n)=s(j,k,n)-(qp(j,k,n)-q(j,k,n))/(akk*dt2)
 500        continue

      else !BDF2 time marching

         if (numiter-istart .ne. 1) then
c        Not first time step

            if (.not. errest) then

               if (numiter .ne. iend+nk_skip+1 .or. nk_skip .eq.0 ) then
c              normal BDF2 with constant time steps
                  mprfac1=3.d0/(2.d0*dt2)
                  mprfac2=-4.d0/(2.d0*dt2)
                  mprfac3=1.d0/(2.d0*dt2)
               else
c              special BDF2 at time step size change
                  mprfac1=(2.d0*dtsmall+dtbig)/(dtsmall*(dtsmall+dtbig))
                  mprfac2=-(dtsmall+dtbig)/(dtsmall*dtbig)
                  mprfac3=dtsmall/(dtbig*(dtsmall+dtbig))
               end if
               
            else
c           special BDF2 with variable time steps
               dttmp=dta(numiter-iend-1)
               mprfac1=(2.d0*dt2+dttmp)/(dt2*(dt2+dttmp))
               mprfac2=-(dt2+dttmp)/(dt2*dttmp)
               mprfac3=dt2/(dttmp*(dt2+dttmp))
               
            end if
               
         else
c        First time step
            
            if (.not. sbbc) then
c           special BDF2 at first time step
               mprfac1=(2.d0*dt2+dtold)/(dt2*(dt2+dtold))
               mprfac2=-(dt2+dtold)/(dt2*dtold)
               mprfac3=dt2/(dtold*(dt2+dtold))
            else
c           Implicit Euler at first iteration for suction/blowing BC
               mprfac1=1.d0/dt2
               mprfac2=-1.d0/dt2
               mprfac3=0.d0
            end if

         end if                 !First time step?

         do 700 n = 1,ndim                                                 
         do 700 k = klow,kup
         do 700 j = jlow,jup
                   
            s(j,k,n)=s(j,k,n)-( mprfac1*qp(j,k,n)+mprfac2*q(j,k,n)+
     &           mprfac3*qold(j,k,n) )

 700     continue

      endif !jesdirk?

      return
      end                                                       
                                                       
