c     ------------------------------------------------------------------
c     -- adjoint gradient calculation --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine adjoint (q, press, sndsp, dRdX, xy, xyj, xit, ett, ds,
     &     x, y, turmu, vort, turre, fmu, vk, ve, tmp, uu, vv,ccx, ccy,
     &     coef2x, coef2y, coef4x, coef4y, precon, spectxi, specteta,
     &     dRsadX, indx, icol, ia, ja, ipa, jpa, ntvar, nmax, xorig,
     &     yorig, xs, ys, xyjs, xys, wk)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"
#include "../include/turb.inc"
#include "../include/scratch.inc"

      double precision q(*),         press(*),     sndsp(*)
      double precision dRdX(*),      xy(*),        xyj(*)
      double precision xit(*),       ett(*),       ds(*)
      double precision x(*),         y(*),         turmu(*)
      double precision vort(*),      turre(*),     fmu(*)
      double precision vk(*),        ve(*),        tmp(*)
      double precision uu(*),        vv(*),        ccx(*)
      double precision ccy(*),       coef2x(*),    coef2y(*)
      double precision coef4x(*),    coef4y(*),    precon(*)
      double precision spectxi(*),   specteta(*),  dRsadX(*)
      integer indx(*),icol(*),ntvar,nmax

c     -- ntvar: total number of unknows --
c     -- as, ja, ia: second order flux jacobian --
c     -- pa, jpa, ipa: preconditioner --
c     -- wa, jwa, iwa: sparse work array --
      integer ia(*),             ja(*)
      integer ipa(*),            jpa(*)
      double precision, dimension(:),allocatable :: as,pa


c     -- arrays for grid storage --
      double precision xys(maxjkq), xyjs(maxjk), xorig(*), yorig(*)
      double precision xs(maxjk), ys(maxjk)

c     -- work array --
      double precision wk(maxjk*jacb)

c     -- local declarations --
c     -- gmres arrays --
      double precision rhs(maxjk*jacb), sol(maxjk*jacb)
c     -- adjoint variables --
      double precision psi(maxjkq), psit(maxjk)

c     -- shuffle arrays required to keep track of row exchanges,
c     necessary for the adjoint problem but also used in the n-k flow
c     solver --
c     -- iex: integer array of row exchanges --
c     -- bcf: boundary condition factors --
      integer iex(maxjb,4,4,mxbfine)
      double precision bcf(maxjb,4,4,mxbfine)      

c     -- local temp. sparse adjoint arrays --
      integer,dimension(:),allocatable :: iatmp,ipatmp,jatmp,jpatmp
      double precision,dimension(:),allocatable :: astmp,patmp

c     -- ILU arrays --
      integer iwk
      parameter (iwk = maxjk*jacb2*5*5)
      integer,dimension(:),allocatable :: jlu,ju,levs,jw
      double precision,dimension(:),allocatable :: alu 

c     -- gmres control --
      integer ipar(16)
      double precision fpar(16)

c     -- gmres work array --
      integer iwk_gmres
      parameter (iwk_gmres=maxjk*jacb*105)
      double precision,allocatable :: wk_gmres(:)

      logical precmat, jacmat, imps

      integer ib,lq,lg,is,ierr,lfil,j,ifirst,ifoil,ic,iseg,in
      double precision cosang,sinang,dOdX

c     --arrays needed for run_gmres--
      double precision exprhs(maxjkq),dtvc(5*maxjk)
      double precision exprsa(maxjk),dhatt(6*maxjk)
      double precision,allocatable :: dhatq(:)

c     -- timing variables --
      real*4 etime, time, t
      external etime


cmpr  Allocate some of the big arrays dynamically
      allocate(as(maxjk*jacb2*9),pa(maxjk*jacb2*5))
      allocate(iatmp(maxjk*jacb+1),jatmp(maxjk*jacb2*9))
      allocate(ipatmp(maxjk*jacb+1),jpatmp(maxjk*jacb2*5))
      allocate(astmp(maxjk*jacb2*9),patmp(maxjk*jacb2*5))
      allocate(jlu(iwk),ju(maxjk*jacb),levs(iwk),jw(3*maxjk*jacb))
      allocate(alu(iwk),wk_gmres(iwk_gmres),dhatq(6*maxjkq))
     

      write (n_out,10)
 10   format (/3x,'<< Solving adjoint equation to compute gradient. >>')
      call flush(n_out)

      jacmat  = .true.
      precmat = .true.

      call copmat (ntvar, as, ja,  ia,  astmp, jatmp,  iatmp, 1,  0) 
      call copmat (ntvar, pa, jpa, ipa, patmp, jpatmp, ipatmp, 1, 0) 

c     -- put in jacobian --
      do ib=1,nblks
         call scjac(ib, jmax(ib), kmax(ib), q(lqptr(ib)), 
     &        xyj(lgptr(ib)), jbegin(ib)-nsd2(ib),jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
      end do

c     -- calculate pressure and sound speed --
      do ib=1,nblks
         lq = lqptr(ib)
         lg = lgptr(ib)
         call calcps(ib, jmax(ib), kmax(ib), q(lq), press(lg),
     &        sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
     &        jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
      end do

c     -- adjoint lhs: dR/dQ --
      imps=.true.
      call get_lhs ( nmax, q, press, sndsp, xy, xyj, precon, xit, ett,
     &     fmu, x, y, turmu, vort, turre, vk, ve, coef4x, coef4y, uu,
     &     vv, ccx, ccy, coef2x, coef2y, indx, pdcg, pa, ipa, as, ia,
     &     icol, iex, bcf, ntvar, precmat, jacmat, imps)
c     write (n_out,*) 'done LHS'
c     call flush (n_out)

c     -- transpose jacobian array --
      call csrcsc (ntvar, 1, 1, as, ja, ia, alu, jlu, jw)
      call copmat (ntvar, alu, jlu, jw, as, ja, ia, 1, 1) 
c     if (icg.eq.0) write (opt_unit,20) ia(na+1) - ia(1)
c     20   format (3x,'Non-zeros in Jacobian matrix:',i8)

c     -- transpose preconditioner array --
      call csrcsc (ntvar, 1, 1, pa, jpa, ipa, alu, jlu, jw)
      call copmat (ntvar, alu, jlu, jw, pa, jpa, ipa, 1, 1)
c     if (icg.eq.0) write (opt_unit,30) ipa(na+1) - ipa(1)
c     30   format (3x,'Non-zeros in preconditioner:',i8)

c     -- remove jacobian scaling from q vector --
      do ib = 1,nblks
         call qmuj( jmax(ib), kmax(ib), q(lqptr(ib)), xyj(lgptr(ib)),
     &        jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
      end do

c     -- form adjoint rhs: dObj/dQ, stored in array psi --
c     -- note for adjoint: do not row shuffle dObj/dQ  --
c     -- pressure has no jacobian -- 
      do ib=1,nblks
         lq = lqptr(ib)
         lg = lgptr(ib)
         call calcps(ib, jmax(ib), kmax(ib), q(lq), press(lg),
     &        sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
     &        jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
      end do    

      call dOBJdQ ( x, y, xy, xyj, q, psi, press)

c     -- diagonal scaling for all b.c. equations --
c     -- this operation is performed after taking the transpose,
c     special case for the adjoint problem --
      do ib=1,nblks
         do is=1,4
            if (ibctype(ib,is).gt.0 .and. ibctype(ib,is).lt.5) 
     &           call sadjoint (ib, is, jmax(ib), kmax(ib), nmax,
     &           kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib),
     &           bcf, indx(lgptr(ib)), psi(lqptr(ib)), pa, as, ipa, ia)
         end do
      end do

c     -- sparskit iluk(p) preconditioner --
      ierr = 0
      lfil = lfilg
      t = etime(tarray)
      time = tarray(1)
      call iluk (ntvar, pa, jpa, ipa, lfil, alu, jlu, ju, levs, iwk,
     &     wk, jw, ierr)
      t = etime(tarray)
      time = tarray(1) - time
      write (n_out,40) lfil,time
 40   format (3x,'ILUK(',i2,') CPU time (s):',f10.3)
      call flush(n_out)

c     nz=ju(ntvar) - 2
      if (ierr.ne.0) then
         write (n_out,*) 'nksolve: ilu problem, ierr=',ierr
         stop
      end if

c     -- gmres --
      ipar(1) = 0
      ipar(2) = 2
      ipar(3) = 1
      ipar(4) = iwk_gmres
      ipar(5) = imgmr
      ipar(6) = itgmr

c     -- gmres convergence tolerance -- 
      fpar(1) = tolgmr
      fpar(2) = 0.0
      fpar(11) = 0.0

c     -- setup rhs gmres --
      do j = 1,maxjk*jacb
         rhs(j) = 0.0
      end do

      do ib = 1,nblks
         call setrhs( jmax(ib), kmax(ib), nmax, kminbnd(ib),
     &        kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), indx(lgptr(ib)),
     &        psi(lqptr(ib)), dRsadX(lgptr(ib)), rhs, .false.)
      end do

      write (n_out,55) ipar(5)
 55   format (/3x,'Starting GMRES(',i3,') ...')
      call flush(n_out)

      ifirst = 0
      ierr   = 0
      t      = etime(tarray)
      time   = tarray(1)
c     -- note: matrix-present gmres is used so most of the arrays below
c     are dummy variables --
      call run_gmres( ifirst, nmax, ntvar, jacmat, q,q,q, press, sndsp,
     &     dRdX, dRsadX, xy, xyj, xit, ett, ds, x, y, turmu, vort,
     &     turre,turre,turre,fmu,vk,ve,tmp,uu,vv,ccx,ccy,coef2x, coef2y,
     &     coef4x, coef4y, precon, spectxi, specteta, indx, wk_gmres,
     &     alu, jlu, ju, as, ja, ia, rhs, sol,iex, bcf, ipar, fpar, 0,
     &     n_gmres,exprhs,dhatq,exprsa,dhatt,dtvc)
      t=etime(tarray)
      time=tarray(1)-time

      write (n_out,60) imgmr, ipar(1), time
      write (n_out,61) ipar(7), fpar(5)
      call flush(n_out)

 60   format (3x,'GMRES(',i3,') return code:',i4,2x,'Run-time:',
     &      f7.2,'s')
 61   format (3x,'Number of inner iterations',i5,' and residual',e12.4)

      if ( ipar(1).lt.0 ) then 
        write (n_out,*) '!!ADJOINT: gmres convergence problems!!'
      end if

c     -- calculate adjoint variable psi --
      do ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)
         call calcpsi( ib, jmax(ib), kmax(ib), nmax, jminbnd(ib),
     &        jmaxbnd(ib), jlow(ib), jup(ib), kminbnd(ib), kmaxbnd(ib),
     &        indx(lg), psi(lq), psit(lg), iex, sol, ibctype, .true.)
      end do

c     -- for viscous flow, zero out the psi vector at the wall boundary
c     for the momentum equation --
      if (viscous) then
         do ifoil = 1,nfoils
            ic = 1
            do iseg = 1,nsegments(ifoil)
               ib = isegblk(iseg,ifoil)
               is = isegside(iseg,ifoil)
               lq = lqptr(ib)
               lg = lgptr(ib)

               call zrpsi( is, nmax, jminbnd(ib), jmaxbnd(ib),
     &              kminbnd(ib), kmaxbnd(ib), jmax(ib), kmax(ib),
     &              psi(lq), psit(lg))
            end do
         end do
      end if

c     -- storing grid and metrics: done for speed --
      do ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)
         call copy_array( jmax(ib), kmax(ib), 4, xy(lq),  xys(lq))
         call copy_array( jmax(ib), kmax(ib), 1, x(lg),   xs(lg))
         call copy_array( jmax(ib), kmax(ib), 1, y(lg),   ys(lg))
         call copy_array( jmax(ib), kmax(ib), 1, xyj(lg), xyjs(lg))
      end do

c     -- calculate the sensitivities of the flow residuals -- 
c     -- arrays rhs and sol used for temp. storage --
      do in = 1,ndv
         
         if ( in.le.ngdv ) then
c     -- copy original grid into x and y --
            do ib = 1,nblks
               lg = lgptr(ib)
c               lq = lqptr(ib)
               call copy_array( jmax(ib), kmax(ib), 1, xorig(lg), x(lg))
               call copy_array( jmax(ib), kmax(ib), 1, yorig(lg), y(lg))
            end do 
         else                   ! alpha is a design variable
            do ib = 1,nblks
               lg = lgptr(ib)
               lq = lqptr(ib)
               call copy_array( jmax(ib), kmax(ib), 1, xs(lg),  x(lg))
               call copy_array( jmax(ib), kmax(ib), 1, ys(lg),  y(lg))
               call copy_array( jmax(ib), kmax(ib), 1, xyjs(lg),xyj(lg))
               call copy_array( jmax(ib), kmax(ib), 4, xys(lq), xy(lq))
            end do
         end if

         call dResdX( in, q, press, sndsp, dRdX, xy, xyj, xit, ett, ds,
     &        x, y, turmu, vort, turre, fmu, vk, ve, tmp, uu, vv, ccx,
     &        ccy, coef2x, coef2y, coef4x, coef4y, precon, spectxi,
     &        specteta, dRsadX, xorig, yorig, rhs, sol)

c     -- product of adjoint variable with residual sensitivity -- 
         grad(in) = 0.0
         do ib = 1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            call vdotv( in, grad, nmax, jmax(ib), kmax(ib), jminbnd(ib),
     &           jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), psi(lq),
     &           dRdX(lq), psit(lg), dRsadX(lg) )
         end do

c     -- dJ/dX --
         do ib=1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call calcps(ib, jmax(ib), kmax(ib), q(lq), press(lg),
     &           sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
     &           jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &           kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
         end do      

         if ( in.le.ngdv ) then
c     -- copy original grid into x and y --
            do ib = 1,nblks
               lg = lgptr(ib)
               lq = lqptr(ib)
               call copy_array( jmax(ib), kmax(ib), 1, xorig(lg), x(lg))
               call copy_array( jmax(ib), kmax(ib), 1, yorig(lg), y(lg))
            end do 
         end if

         call dObjdX(in, dOdX, x, y, xy, xyj, xorig, yorig, xit, ett, q,
     &        press)

c     -- gradient vector --
         grad(in) = grad(in) + dOdX
      end do

c     -- restore grid and solver parameters --
      do ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)
         call copy_array( jmax(ib), kmax(ib), 1, xs(lg),  x(lg))
         call copy_array( jmax(ib), kmax(ib), 1, ys(lg),  y(lg))
         call copy_array( jmax(ib), kmax(ib), 1, xyjs(lg),xyj(lg))
         call copy_array( jmax(ib), kmax(ib), 4, xys(lq), xy(lq))
      end do

      if ( viscous .and. turbulnt ) then
c     -- update distances for the s-a model (array smin) --
         call autostan( 1, 1, maxj, maxk, x, y, d, d(1,2), .false.)
         do ib=1,nblks
            lg = lgptr(ib)
            call caldist(jmax(ib), kmax(ib), x(lg), y(lg), smin(lg),
     &           jlow(ib), jup(ib), klow(ib), kup(ib), icoord, xcoord,
     &           ycoord)
         end do
      end if
      
      if (ndv.gt.ngdv) then
         alpha = dvs(ndv)
         cosang = cos(pi*alpha/180.0)                               
         sinang = sin(pi*alpha/180.0)
         uinf   = fsmach*cosang
         vinf   = fsmach*sinang
      end if

      call copmat (ntvar, astmp, jatmp,  iatmp,  as, ja,  ia,  1, 0) 
      call copmat (ntvar, patmp, jpatmp, ipatmp, pa, jpa, ipa, 1, 0) 


cmpr  Deallocate the big arrays
      deallocate(as,pa,iatmp,jatmp,ipatmp,jpatmp,astmp,patmp,jlu,ju)
      deallocate(levs,jw,alu,wk_gmres,dhatq)

      return 
      end                       !adjoint
