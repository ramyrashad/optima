c----------------------------------------------------------------------
c     -- differentiation of spectral radius for JST dissipation --
c     -- m. nemec, sept. 2001 --
c----------------------------------------------------------------------
      subroutine dspecty(jmax, kmax, nmax, indx, icol, q, xy, xyj, vv,
     &     sndsp, as, coef2, coef4, ibc1, ibc2, ibc3, ibc4, 
     &     jminbnd, jmaxbnd, kminbnd, kmaxbnd, 
     &     dis2y, dis4y, g2)

      implicit none

#include "../include/parms.inc"

      integer ibc1, ibc2, ibc3, ibc4
      integer jmax,kmax,indx(jmax,kmax),icol(*),jbegin,jend,kbegin,kend
      integer jminbnd, jmaxbnd,kminbnd,kmaxbnd,jlow,jup,klow,kup,j,k,n,m
      integer kp,km,kb,js,je,ks,ke,ii,ij,itmp,nmax
      double precision q(jmax,kmax,4),xy(jmax,kmax,4),xyj(jmax,kmax)
      double precision sndsp(jmax,kmax),vv(jmax,kmax),coef2(jmax,kmax)
      double precision coef4(jmax,kmax),as(*)

c     -- local variables --
      double precision dspdq(4,maxjb,maxkb), work(maxjb,maxkb,2,4) 
      double precision pcoef(maxjb,maxkb),   spect(maxjb,maxkb)
      double precision c2(maxjb,maxkb),      c4(maxjb,maxkb)
      double precision db(4,4,maxjb,maxkb),  bm1(4,4,maxjb,maxkb)
      double precision bp1(4,4,maxjb,maxkb), dvvdq(3)
      double precision ri,ri2,t1,t2,t3,dadq1,dadq2,dadq3,dadq4,sqm,g2
      double precision d4m,d4,d4p,dis2y,dis4y,pcm,pc,pcp

      logical frozen
      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

      kbegin = kminbnd
      kend   = kmaxbnd
      klow   = kminbnd + 1
      kup    = kmaxbnd - 1
      jbegin = jminbnd
      jend   = jmaxbnd
      jlow   = jminbnd + 1
      jup    = jmaxbnd - 1

c     -- form derivative of spectral radius at node j,k --
      do k = kbegin,kend
         do j = jbegin,jend

            ri  = 1.0/q(j,k,1)
            ri2 = ri*ri

            t1 = 0.5/sndsp(j,k)
            t2 = q(j,k,2)*q(j,k,2)
            t3 = q(j,k,3)*q(j,k,3)

            dadq1 = g2*ri2*( (t2+t3)*ri - q(j,k,4) )
            dadq2 = - g2*q(j,k,2)*ri2
            dadq3 = - g2*q(j,k,3)*ri2
            dadq4 = g2*ri

            sqm = sqrt( xy(j,k,3)**2+xy(j,k,4)**2 )*t1

            dspdq(1,j,k) = dadq1*sqm
            dspdq(2,j,k) = dadq2*sqm
            dspdq(3,j,k) = dadq3*sqm
            dspdq(4,j,k) = dadq4*sqm
         end do
      end do      

      if (.not. frozen) then
         do k = kbegin,kend
            do j = jbegin,jend

               ri  = 1.0/q(j,k,1)
               ri2 = ri*ri
               dvvdq(1) = - ri2*(q(j,k,2)*xy(j,k,3) +q(j,k,3)*xy(j,k,4))
               dvvdq(2) = ri*xy(j,k,3) 
               dvvdq(3) = ri*xy(j,k,4)
               
               if ( vv(j,k) .lt. 0.d0 ) then
                  do n = 1,3
                     dvvdq(n) = - dvvdq(n)
                  end do
               end if

               dspdq(1,j,k) = dvvdq(1) + dspdq(1,j,k)
               dspdq(2,j,k) = dvvdq(2) + dspdq(2,j,k)
               dspdq(3,j,k) = dvvdq(3) + dspdq(3,j,k)
            end do
         end do
      end if

c     -- form dissipation differences --

      do n = 1,4                                                     
c     -- 1st-order forward difference --
         do k = kbegin,kup
            kp = k+1
c            do j = jlow,jup
               do j =jbegin,jend
               work(j,k,1,n) = q(j,kp,n)*xyj(j,kp) - q(j,k,n)*xyj(j,k)
            end do
         end do

c     -- apply cent-dif to 1st-order forward --
         do k =klow,kup-1
            kp = k+1
            km = k-1
c            do j =jlow,jup
            do j =jbegin,jend
              work(j,k,2,n) = work(j,kp,1,n) - 2.0*work(j,k,1,n) +
     &              work(j,km,1,n) 
            end do
         end do

c     -- points on boundaries: rows 2 & kmax-1 --
         kb = kbegin
c         do j = jlow,jup
            do j =jbegin,jend
            work(j,kb,2,n) = q(j,kb+2,n)*xyj(j,kb+2) - 2.0
     &           *q(j,kb+1,n)*xyj(j,kb+1) + q(j,kb,n)*xyj(j,kb)
            work(j,kup,2,n) = work(j,kup-1,1,n) - work(j,kup,1,n)
         end do
      end do

c     -- taken from coef24y.f --

      do k = kbegin,kend                                              
         do j = jbegin,jend
            pcoef(j,k) = coef2(j,k)
            spect(j,k) = coef4(j,k)
         end do
      end do

      do k = kbegin,kup                                          
         kp = k+1
         do j = jbegin,jend                                         
            c2(j,k) = dis2y*(pcoef(j,kp)*spect(j,kp) +
     &           pcoef(j,k)*spect(j,k))
            coef2(j,k) = c2(j,k)
            c4(j,k) = dis4y*(spect(j,kp) + spect(j,k))
            coef4(j,k) = c4(j,k) - min( c4(j,k),c2(j,k) )
         end do
      end do

c     -- linearize c4 = c4 - min(c4,c2) --
c     -- pressure switch (pcoef) is treated as constant --
      do k = klow,kup                                            
         km = k-1
         kp = k+1
c         do j = jlow,jup
            do j =jbegin,jend
            pcm = dis2y*pcoef(j,km)/xyj(j,km)
            pc  = dis2y*pcoef(j,k)/xyj(j,k)
            pcp = dis2y*pcoef(j,kp)/xyj(j,kp)

            d4m = dis4y/xyj(j,km)
            d4  = dis4y/xyj(j,k)
            d4p = dis4y/xyj(j,kp)

            if ( c4(j,k) .le. c2(j,k) ) then
c     -- no O(3) dissipation: c4 = 0 --
               do n = 1,4
                  do m = 1,4
                     bm1(m,n,j,k) = pcm*dspdq(m,j,km)*work(j,km,1,n)
                     db(m,n,j,k) = - pc*dspdq(m,j,k)*(work(j,k,1,n) -
     &                    work(j,km,1,n))
                     bp1(m,n,j,k) = - pcp*dspdq(m,j,kp)*work(j,k,1,n)
                  end do
               end do
            else
c     -- mostly O(3) dissipation: c4 = c4 - c2 --
               do n = 1,4
                  do m = 1,4
                     bm1(m,n,j,k) = - d4m*dspdq(m,j,km)*work(j,km,2,n) +
     &                    pcm*dspdq(m,j,km)*work(j,km,1,n)  
                     db(m,n,j,k) = d4*dspdq(m,j,k)*(work(j,k,2,n) -
     &                    work(j,km,2,n) ) - pc*dspdq(m,j,k)*( work(j,k
     &                    ,1,n)-work(j,km,1,n) ) 
                     bp1(m,n,j,k) = d4p*dspdq(m,j,kp)*work(j,k,2,n) -
     &                    pcp*dspdq(m,j,kp)*work(j,k,1,n) 
                  end do
               end do
            end if
         end do
      end do

c     -- add to jacobian, follows routine fillay.f --
      if (ibc2.EQ.0) then
         js = jminbnd
      else
         js = jminbnd + 2
      end if
      if (ibc4.EQ.0) then
         je = jmaxbnd
      else
         je = jmaxbnd -2
      end if 
      ks = kminbnd + 2
      ke = kmaxbnd - 2

      do k = ks,ke
         do j = js,je
            itmp = ( indx(j,k) - 1 )*nmax
            do n = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,n,j,k)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
                  as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
               end do
            end do
         end do
      end do        

      if (ibc2.EQ.0) then
         js = jminbnd
      else
         js = jminbnd + 2
      end if
      if (ibc4.EQ.0) then
         je = jmaxbnd
      else
         je = jmaxbnd - 2
      end if
      k = kminbnd+1

      do j = js,je
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,n,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,n,j,k)
            end do
         end do
      end do

      k = kmaxbnd-1
      do j = js,je
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
            end do
         end do
      end do 

      if (ibc2.NE.0) then
         j   = jminbnd + 1
         ks  = kminbnd + 2
         ke  = kmaxbnd - 2

         do k = ks,ke
            itmp = ( indx(j,k) - 1 )*nmax
            do n = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,n,j,k)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,n,j,k) 
               end do
            end do
         end do

         k = kminbnd + 1
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + db(m,n,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + bp1(m,n,j,k)
            end do
         end do

         k = kmaxbnd - 1
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,n,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,n,j,k)
            end do
         end do
      end if

      if (ibc4.NE.0) then
         j   = jmaxbnd - 1
         ks  = kminbnd + 2
         ke  = kmaxbnd - 2

         do k = ks,ke
            itmp = ( indx(j,k) - 1 )*nmax
            do n = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,n,j,k)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
                  as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k) 
               end do
            end do
         end do

         k = kminbnd + 1
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,n,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,n,j,k)
            end do
         end do

         k = kmaxbnd - 1
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
            end do
         end do
      end if

      return
      end                       !dspecty
