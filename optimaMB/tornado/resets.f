c     ******************************************************************
c     ** routine to reset S, called from subroutine setup
c     ******************************************************************

      subroutine resets(jdim,kdim,s)

      implicit none
      integer jdim,kdim ,j,k,n   
      double precision s(jdim,kdim,4)   

      do n=1,4                                                        
         do k=1,kdim
            do j=1,jdim
               s(j,k,n) = 0.0d0
            end do
         end do
      end do

      return
      end                       !resets
