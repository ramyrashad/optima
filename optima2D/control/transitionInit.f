      ! ================================================================
      ! ================================================================
      ! 
      subroutine initializeTP(
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine initializes the various boundary layer
      !   flags and properties as required for transition prediction.
      !
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"


      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim,
     &     ndim
      double precision
     &     q(jdim,kdim,ndim),
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim)

      ! _______________________
      ! declare local variables
      integer
     &     j,k, 
     &     jte,                 !< j index of trailing edge
     &     jSPBuffer,           !< region around stagnation point node with no transition prediction
     &     kBLlimit,            !< sets off-wall limit based on distance to far field boundary
     &     tmp                  !< temporary variable
      double precision
     &     xte, yte,            !< x,y coordinates of trailing edge node
     &     xSPBuffer,           !< region around stagnation point node with no transition prediction
     &     yBLlimit,            !< sets off-wall limit based on distance to far field boundary
     &     dist,                !< max distance from trailing edge (TE) node
     &     dist2,               !< distance from TE node to a given surface node
     &     distx,               !< x-distance from TE node to a given node
     &     disty,               !< y-distance from TE node to a given node
     &     stagPress,           !< stagnation pressure
     &     dx !< RR: debug

      ! _______________________________________________________
      ! declare output variables from getCurvilinearVariables()
      double precision
     &     vel(jdim,kdim),
     &     uu(jdim,kdim),
     &     vv(jdim,kdim),
     &     p(jdim,kdim),
     &     rho(jdim,kdim),
     &     a(jdim,kdim),
     &     M(jdim,kdim),
     &     dy(jdim,kdim)

      ! ______________________________________
      ! top and bottom surface index increment
      select case (surfaceBL)
      case (1)
         jblinc = 1
      case (2)
         jblinc = -1
      case default
         stop ' blInitialize: surfaceBL assigned wrong value '
      end select ! surfaceBL
      
      call getCurvilinearVariables(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     vel, uu, vv, p, rho, a, M, dy)

      jte=jtail1
      xte=x(jte,1)
      yte=y(jte,1)
      dist=0.d0
      k=kbegin
      stagPress = 0.d0
      do j=jtail1,jtail2
         ! ______________________
         ! find leading edge node
         distx=x(j,k)-xte
         disty=y(j,k)-yte
         dist2=sqrt(distx**2+disty**2)
         if (dist2.gt.dist) then
            dist=dist2
            jblLE=j
         endif
         ! __________________________
         ! find stagnation point node
         if (p(j,1) .gt. stagPress) then
            jblSP = j
            stagPress = p(j,1)
         end if
      enddo

      ! ____________________
      ! initialize variables
      foundCR = .false.
      foundTR = .false.
      jTransPredict(surfaceBL) = -1
      errorFlagTP(surfaceBL) = -1
      if (.not.convergedTPOld(surfaceBL)) then
         convergedTP(surfaceBL) = .false.
      end if 
      xTransPredictNoShift(surfaceBL) = -1d00
      if (surfaceBL.eq.1) then
         errorTrackerTP = -1 
         do j=1,jdim
            foundBLEdge(j) = .false.
            k_deltaBL(j) = 0
            deltaBL(j) = 0d0
            velBLEdge(j) = 0d0
         end do
      else if (surfaceBL.eq.2) then
         errorTrackerTP = -1
         do j=jblSP,jtail1,jblinc
            foundBLEdge(j) = .false.
            k_deltaBL(j) = 0
            deltaBL(j) = 0d0
            velBLEdge(j) = 0d0
         end do
      end if


      ! _______________________________________
      ! set-up streamwise start and end indices
!      xleBuffer = 0.005 ! = 0.5% chord
!      if (x(jblSP,1).le.xleBuffer) then
!         tmp = 0
!         do j = jblSP,jtail2
!            tmp = tmp + 1
!            if (x(j,1) .ge. xleBuffer) then
!               jlebuffer = tmp
!               exit
!            end if
!         end do
!      else
!         jlebuffer = 0
!      end if 
!      select case (surfaceBL)
!      case (1)
!         ! Top (upper) surface of airfoil
!         jblStart = jblSP+jleBuffer
!         jblEnd   = jtail2-1
!      case (2)
!         ! Bottom (lower) surface of airfoil
!         jblStart = jblSP-jleBuffer
!         jblEnd   = jtail1+1
!      case default
!         stop ' blInitialize: surfaceBL assigned wrong value '
!      end select ! surfaceBL
      

      ! __________________________________________________________
      ! define buffer region surrounding stagnation point where no
      ! transition prediction will be performed
      xSPBuffer = 0.005 ! = 0.5% chord
!      do j = jblSP+1,jtail2-1
!         tmp = tmp + 1
!         if ( (x(j,1)-x(jblSP,1)) .ge. xSPBuffer) then
!            jlebuffer = tmp
!            exit
!         end if
!      end do
      j = jblSP + jblinc
      jSPBuffer = 1
      do while ( (x(j,1) - x(jblSP,1)) .le. xSPBuffer )
         j = j + jblinc
         jSPBuffer = jSPBuffer + 1 
      end do
      ! N.B. it only makes sense to apply the jSPbuffer to a certain
      ! side of the airfoil (top/bottom) as described in the following
      ! set-up of streamwise indices

      ! _______________________________________
      ! set-up streamwise start and end indices
      if (jblSP .lt. jblLE) then ! positive angle of attack

         select case (surfaceBL)
         case (1) ! Top (upper) surface of airfoil
            jblStart = jblLE
            jblEnd   = jtail2 - 1
         case (2) ! Bottom (lower) surface of airfoil
            jblStart = jblSP - jSPBuffer
            jblEnd   = jtail1 + 1
         end select ! surfaceBL

      else if (jblSP .gt. jblLE) then ! negative angle of attack

         select case (surfaceBL)
         case (1) ! Top (upper) surface of airfoil
            jblStart = jblSP + jSPBuffer
            jblEnd   = jtail2 - jblinc
         case (2) ! Bottom (lower) surface of airfoil
            jblStart = jblLE
            jblEnd   = jtail1 - jblinc
         end select ! surfaceBL

      else ! jblSP = jblLE

         select case (surfaceBL)
         case (1) ! Top (upper) surface of airfoil
            jblStart = jblSP + jSPBuffer
            jblEnd   = jtail2 - jblinc
         case (2) ! Bottom (lower) surface of airfoil
            jblStart = jblSP - jSPBuffer
            jblEnd   = jtail1 - jblinc
         end select ! surfaceBL

      end if

      ! _____________________________________
      ! set-up off-wall start and end indices
      yBLlimit = 0.1d0 ! = sets y/c off-wall search limit
      do k=1,kdim
         ! only valid for C-grids
         if (abs(y(jtail2,k)).ge.yblLimit) then
            kBLlimit = k
            exit
         end if 
!         write(36,*) 'kBLlimit info, k = ', k, 'y = ', y(jend,k),
!     &        'ye = ', y(jdim,kdim)
      end do
!      write(36,*) 'kBLlimit final = ', kBLlimit
      kblStart = 2 ! important to keep at 2 for vorticity method
      kblEnd   = kBLlimit
      
      ! _____________________________________________________________
      ! initialize the container for Re_theta_laminar used to compute
      ! the "transShift" based on an extrapolated laminar BL.
      if (iterTP.eq.1) then
         select case (surfaceBL)
         case (1)
            do j=1,jdim
               Re_theta_laminar(j) = 0d0
            end do
         case (2)
            do j=jblSP,jtail1,jblinc
               Re_theta_laminar(j) = 0d0
            end do
         case default
            stop ' blInitialize: surfaceBL assigned wrong value '
         end select ! surfaceBL
      end if ! iterTP

      return
      end subroutine initializeTP

