c---- Routine used to add the forcing function of the fine level
c     to the residual before its restriction
c
c     Calling subroutine: mk_pk.f
c

      subroutine add_fine_pk(jdim,kdim,s,pk)

      dimension s(jdim,kdim,4),pk(jdim,kdim,4)

      do 5 n=1,4
      do 5 k=1,kdim
      do 5 j=1,jdim
        s(j,k,n)=-s(j,k,n)-pk(j,k,n)
 5    continue
c
      return
      end
