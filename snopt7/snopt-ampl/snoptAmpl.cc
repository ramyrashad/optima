#include <stdio.h>
#include <string.h>
#include "snopt.hh"
#include "snoptProblem.hh"
#include "amplProblem.hh"


int main( int argc, char **argv)
{
  integer Cold = 0;

  amplProblem prob( argc, argv );
  prob.initialize ();
  prob.setSpecFile( "snampl.spc" );
  prob.solve      ( Cold );
}
