c----------------------------------------------------------------------
c     -- check if the current x grid coord. matches xcp coord. --
c     -- if not, the cp should be interpolated --
c     -- m. nemec, june 2001 --
c----------------------------------------------------------------------
c     calling routine: ioall

      subroutine checkxcp(ibn, jdim, kdim, jbegin, jend, kbegin, kend,
     &     ifoil, iside, x)

      implicit none

#include "../include/parms.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer ibn,jdim,kdim,jbegin,jend,kbegin,kend,ifoil,iside,j,k
      double precision x(jdim,kdim)

c     write (*,*) 'checkxcp,iside',iside
c     write (*,*) 'jdim,kdim',jdim,kdim
c     write (*,*) 'jbegin,jend,kbegin,kend',jbegin,jend,kbegin,kend

      if (iside.eq.1) then
         k = kbegin
         do j = jbegin,jend
            if ( abs(xcp_tar(ibn,ifoil) - x(j,k)) .gt. 1.e-15 ) then
               write (*,100)
c     stop
            end if
            ibn = ibn+1
         end do

      elseif (iside.eq.2) then
         j = jbegin
         do k= kend, kbegin, -1
            if ( abs(xcp_tar(ibn,ifoil) - x(j,k)) .gt. 1.e-15 ) then
               write (*,100)
c     stop
            end if
            ibn = ibn+1
         end do

      elseif (iside.eq.3) then
         k = kend
         do j = jend,jbegin,-1
            if ( abs(xcp_tar(ibn,ifoil) - x(j,k)) .gt. 1.e-15 ) then
               write (*,100)
c     stop
            end if
            ibn = ibn+1
         end do

      elseif (iside.eq.4) then
         j = jend
         do k = kbegin,kend
            if ( abs(xcp_tar(ibn,ifoil) - x(j,k)) .gt. 1.e-15 ) then
               write (*,100)
c     stop
            end if
         end do
      endif

 100  format(/3x,'! checkxcp: target cp and current grid mismatch !') 

      return
      end                       !checkxcp
