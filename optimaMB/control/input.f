c     *****************************************
c     **** read in data and set parameters ****
c     *****************************************
      subroutine input(isequal,mglev)

      implicit none
c     
#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer strlen,i,mglev,isequal,namelen,mp,ii,jj,lentmp
      double precision objwt
      character command*80
      character filena_restart*40

      namelist/optima/ ioptm, iobjf, igrad, inord, ireord, fd_eta, wfl,
     &     wfd, wtc, wgo, cl_tar, cd_tar, optol, bfstep, pdcg, mxfun,
     &     itgmr, imgmr, tolgmr, lfilg, ifrzw, wstart, dvalfa,
     &     remxmin,remxmax,remymin,remymax,remdx,remdy,opt_restart

      namelist/tornado/
     &     alpha,autowrite,bcairf,bcfarf,circul,clalpha,clinput,cltol,
     &     dis2x,dis4x,dis2y,dis4y,dtrate,dswall,
     &     fsmach,gseq,grdseq_rest,
     &     ispec,ivis,iord,iramp,idmodel,iread,iclfreq,iclstrt,
     &     integ,iturb,jacarea,lomet,
     &     fsres,afres,mg,mglev,matcharc2d,
cmt ~~~
     &     dualres,outtime,noutevery,nouteverybig,dtbig,killed,
cmt ~~~
     &     nhalo,nq,ncp,nres,nnit_sp,orderxy,
     &     psiavg,poutflow,phidt,prec,prphi,prxi,
     &     restart,re,retinf,relaxcl,
     &     smu,smuim,strtit,sobmax,store,sngvalte,
     &     tinf,thetadt,translo,transup,turbulnt,fturb,timing,
     &     viscous,visxi,viseta,viscross,
     &     vlxi,vleta,vnxi,vneta,visceig,viscoutflow,
     &     writemets,writedisp,writeresid,windtunnel,
     &     w_cycle,wtrat,zerostart,zeroturre,
c     -two following lines are for menter model
     &     t_step_kw,vk_inf,ve_inf,turnu_inf,
     &     itertotal,eps_ke,delta_ke,k2_l,restart_ke,
cmt ~~~
     &     unsteady,imarch,klineconst,omegaa,omegaf,sbbc,sbeta,
cmt ~~~
     &     jobs,kobs,blobs,Nobs,xobs,yobs,outFWH

      namelist/nks/ pdcnk, nkits, nklfil, nkpfrz, nkimg, nkitg,
     &     nkiends, nksubits, nkskip
      
      namelist/extra/
     &     grid_file_prefix,restart_file_prefix,output_file_prefix

      data transup/maxfoil*0./,translo/maxfoil*0./

c     -- units taken  by Tornado --
c     -- if you need a new file use the units.inc common block --
c     -- for one time read in or write out files use n_all, remember to
c     close it when finished --
      data n_all, n_cp, n_cf, n_ld, n_eld, n_q /10, 11, 12, 13, 14, 15/
      data n_out, n_t, n_his, n_this, n_time /16, 17, 18, 19, 20/
      data n_polar, n_q2, n_t2 /21, 22, 23/
      data n_qk,n_q2k,n_tk,n_t2k /24,25,26,27/

c     -- units taken by OPTIMA start at 50 --
      data n_ds, n_ohis, n_ghis / 53, 54, 55 / 
      data n_dvhis, n_gvhis, n_fhis, n_ac, n_cphis /56, 57, 58, 59, 60/
      data n_gmres, n_dvr, n_con, n_cfhis / 61, 62, 63, 64 /
c     data gmres_unit, ac_unit, bc_unit, dvhis_unit / 51, 52, 53,54 /
c     data gvhis_unit, gopt_unit, bspr_unit, dvsr_unit / 55, 56, 57, 58/

c     -- multi-point optimization output files --
      data n_mpo  /70, 71, 72, 73, 74, 75, 76, 77, 78/ 
      data n_mpcp /79, 80, 81, 82, 83, 84, 85, 86, 87/ 

      pi = 4.0*atan(1.0)

c     -set parameter defaults
      nq         = 100
      ncp        = 100  
      nres       = 1
      iread      = 2
      iprint     = 1
      restart    = .false.
      store      = .true.
c     jacdt      = 1
      dtrate     = 1.0
      thetadt    = 1.
      phidt      = 0.
      periodic   = .false.
      fsmach     = .397
      alpha      = 0.
      smu        = 0.6
      smuim      = 0.6
      meth       = 3
      ispec      = 1
      dis2x      = 1.0
      dis4x      = 0.01
      dis2y      = 1.0
      dis4y      = 0.01
      strtit     = 12.
      dswall     = -.005
      sobmax     = 12.
      circul     = .true.
      sharp      = .false.
      cusp       = .false.
      re_input   = 0.0
      viscous    = .true. 
      ivis       = 2
      turbulnt   = .true.
      fturb      = .true.
      visxi      = .false.
      viseta     = .true.
      viscross   = .false.
      clalpha    = .false.
      relaxcl    = 1.0
      clinput    = 0.0
      iclfreq    = 10000
      iclstrt    = 10000
      cltol      = 1.0e-4
      bcairf     = .true.
      bcfarf     = .true.
      wtrat      = 0.0
      tinf       = 460.
      iscray     = .false.
      orderxy    = .true.
      neudich    = .false.
      conmin     = 0.0
      conmax     = 2.0
      coninc     = 0.05
      iturb      = 3
      mg         = .false.
      gseq       = .false.
      mglev      = 3
      lomet      = .false.
      grdseq_rest= .false.
      visceig    = 1.0
      wake       = .false.
c     
c     note: iturb    2: Baldwin-Barth
c     3: Spalart-Allmaras
c     4: Menter
      retinf     = 0.001
      writemets  = .false.
      writedisp  = .false.
      writeresid = .false.
      nhalo      = 2
      psiavg     = .true.
      poutflow   = .false.
      windtunnel = .false.
      autowrite  = .false.
      sngvalte   = .false.
      timing     = .false.
      nnit_sp    = 3
      zeroturre  = .true.
      iord       = 2
      integ      = 2
      gseq       = .false.
      mg         = .false.
      w_cycle    = .false.
      jacarea    = .false.
      viscoutflow= .false.
      fsres      = 1.e-14
      afres      = 1.e-8
c     
      zerostart  = .false.
      matcharc2d = .false.
      grid_file_prefix = 'grid'
      restart_file_prefix = 'input'
      output_file_prefix = 'output'
c     
c     
c     -matrix dissipation variables 
      vlxi       = 0.0
      vleta      = 0.0
      vnxi       = 0.0
      vneta      = 0.0
cmt -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
      dualres   = 1.e-12
      imarch    = 2
      unsteady  = .false.
      outtime    = .false.
      killed     = .false.
      noutevery  = 1
      nouteverybig  = 1
cmt -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~

cmpr  suction/blowing BC
      omegaa     = 0.d0
      omegaf     = 0.d0 
      sbeta      = 90.d0
      sbbc       = .false.
cmpr

cmpr  FWH observer information
      jobs=1
      kobs=1
      blobs=1
      Nobs=1
      xobs=0.d0
      yobs=0.d0 
      outFWH=0
cmpr
c     
c     -note: idmodel  1: scalar diss.
c     2: matrix diss.
      idmodel    = 1
c     
c     
c     -preconditioning variables
      prec       = 0
      prxi       = 0.00
      prphi      = 0.50
c     
c     -ramp change in alpha
      iramp      = 50
c     
c     -mentor model variables
      t_step_kw=5.0
      ve_inf=0.185              ! beta*fsmach (1 <= beta <= 10)
      turnu_inf=.398e-9         ! 1e-3*Re
      vk_inf=7.37e-10           ! turnu_inf*ve_inf
      itertotal=1
      eps_ke=1.0e-30
      delta_ke=5.0
      k2_l=20
      restart_ke=.false.

c     -- newton-krylov variables --
      pdcnk  = 6.0
      nkits  = 20
      nklfil = 2
      nkpfrz = 1
      nkimg  = 40
      nkitg  = 40
cmt -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
      nkiends  = 0
      nksubits = 6
      nkskip = 0
      dtbig = 0.1d0
cmt -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~

c     -- optimization variables --
      inord   = 2
      ireord  = 2
      ioptm   = 1
      iobjf   = 2
      igrad   = 1
      fd_eta  = 1.d-6
      opt_tol = 1.d-8
      wfl     = 1.0
      wfd     = 0.1
      wtc     = 1.0
      wgo     = 0.05
      cd_tar  = 0.1
      cl_tar  = 0.5
      mxfun   = 20
      optol   = 1.e-5
      bfstep  = 0.2
      itgmr   = 600
      imgmr   = 100
      tolgmr  = 1.e-5
      lfilg   = 6
      wstart  = .true.
      opt_restart = .false.
      tdelx   = 0.0
      tdely   = 0.0
      ndv     = 0
      nbdv    = 0
      ntdv    = 0
      pdcg    = 6.0
      ifrzw   = 0
      do i =1,ndpts
         dvalfa(i)  = .false.
      end do
c     -- remote inverse design control L --
      remxmin = -5.d0
      remxmax =  5.d0
      remymin = -5.d0
      remymax =  5.d0
      remdx = 0.1d0
      remdy = 0.1d0
      klineconst = 0
c     -- muti-point --
      mpopt   = 1
      mpit    = 1

c     -- detection of negative jacobian --
      badjac = .false.

c     -- read input file --
      read(*,*)
      read(*,*) mpopt
      if (mpopt.lt.1 .or. mpopt.gt.ndpts) then
         stop 'INPUT.f: Problem with mpopt!!'
      end if
      read(*,*)
      do i = 1, mpopt
         read(*,*) wmpo(i), fsmachs(i), cltars(i), wfls(i),
     &        cdtars(i), reno(i), dvalfa(i), alphas(i)
      end do

      read(*,nml=optima)
      read(*,nml=tornado)
      read(*,nml=nks)
      read(*,nml=extra)
      read(*,*)
      read(*,*)isequal
      read(*,*)

c     -- save re for optimization runs and initialize variables --
      re_input = reno(1)
      re       = re_input
      fsmach   = fsmachs(1)
      cl_tar   = cltars(1)
      alpha    = alphas(1)
      cd_tar   = cdtars(1)

c     -- open files: get length --
      namelen =  strlen(output_file_prefix)
c     
c     -write namelist parameters to file         
c     filena = output_file_prefix
c     filena(namelen+1:namelen+5) = '.nls'
c     open(unit=99,file=filena,status='new',form='formatted')
c     write(99,inputs)
c     write(99,extra)
c     close(99)

c     -- check memory for iex and bcf arrays --
c     -- these arrays are used for n-k and optimization runs --
      if (maxjb.lt.maxkb) then
         write (*,*) '!!Not enough memory for IEX and BCF arrays!!'
         write (*,*) '!!Check sparse.inc!!'
         write (*,*) '!!Replace maxjb with maxkb in declaration!!'
         stop
      end if

c     -- general output file --
      filena = output_file_prefix
      filena(namelen+1:namelen+6) = '.out'
      open(unit=n_out,file=filena,status='new',form='formatted')

      write(n_out,820)
      write(n_out,*)
     $   '------------------------------------------------------------'
      write(n_out,*)
     $   '|                                                          |'
      write(n_out,*)
     *   '| TORNADO: A multi-block implicit finite difference code   |'
      write(n_out,*)
     $   '|   for multiple element geometries with general grids     |'
      write(n_out,*)
     *   '|                                                          |'
      write(n_out,*)
     *   '|   For further details see self documentation or contact  |'
      write(n_out,*)
     *   '|             Stan De Rango  or  Tom Nelson                |'
      write(n_out,*)
     *   '|                     UTIAS  2000                          |'
      write(n_out,*)
     *   '|                                                          |'
      write(n_out,*)
     *   '| OPTIMA_MB: Aerodynamic shape optimization code based on  |'
      write(n_out,*)
     *   '|        TORNADO and using a Newton-Krylov approach        |'
      write(n_out,*)
     *   '|                                                          |'
      write(n_out,*)
     *   '|                     Marian Nemec                         |'
      write(n_out,*)
     *   '|                     UTIAS  2002                          |'
      write(n_out,*)
     $   '------------------------------------------------------------'
      write(n_out,820)

      if (ioptm .gt. 2) then
c     -- optimization files --

         if (ioptm .ne. 6) then

c     -- open objective function convergence history file --
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         open(unit=n_ohis,file=filena,status='unknown',
     &        access='append')
         if (mpopt.gt.1) then
            do mp = 1,mpopt
c     -- char function works with ASCII code --
c     -- numbers 1 to 9 correspond to decimals 49 to 57 --
               filena = output_file_prefix
               filena(namelen+1:namelen+8) = '.ohis'//char(48+mp)
               open(unit=n_mpo(mp),file=filena,status='unknown',
     &              access='append')
               filena = output_file_prefix
               filena(namelen+1:namelen+7) = '.cph'//char(48+mp)
               open(unit=n_mpcp(mp),file=filena,status='unknown',
     &              access='append')
            end do
         endif

c     -- open |gradient| convergence history file --
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ghis'
         open(unit=n_ghis,file=filena,status='new',form='formatted')

c     -- open design variable history file --        
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+7) = '.dvhis'
         open(unit=n_dvhis,file=filena,status='unknown',
     &        access='append')

c     -- open gradient convergence history file --
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+7) = '.gvhis'
         open(unit=n_gvhis,file=filena,status='unknown',
     &        access='append')

c     -- open airfoil geometry file --
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+4) = '.ac'
         open(unit=n_ac,file=filena,status='new')
         
c     -- open airfoil geometry file --
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+5) = '.dvs'
         open(unit=n_dvr,file=filena,status='new')

c     -- open gradient gmres residual file --
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.gres'
         open(unit=n_gmres,file=filena,status='unknown',
     &        access='append')

         write (n_out,10)
 10      format(//10x, 'OPTIMA-MB Parameters:'/)
         
         write (n_out,501) mpopt
 501     format(/3x,'Number of design points:',i4)
         objwt=0.0
         do i=1,mpopt
            objwt = objwt + wmpo(i)
         end do
c     -- normalize weights --
         do i=1,mpopt
            wmpo(i) = wmpo(i)/objwt 
         end do
         write (n_out,*)
         write (n_out,502)
 502     format(3x,'Design Point',5x,'Weight',5x,'Mach Number',5x
     &        ,'Cl*',6x,'Cd*',9x,'Re',8x,'ALPHA')
         do i=1,mpopt
            write(n_out,503) i, wmpo(i), fsmachs(i), cltars(i),
     &           cdtars(i), reno(i), alphas(i)
         end do
 503     format(7x,i4,7x,f8.4,5x,f8.3,4x,f8.4,1x,e11.4,1x,e11.4,f9.4)
         write(n_out,820)

         end if !ioptm .ne. 6
         
         if ( iobjf.eq.1 ) then
            write (n_out,12)
 12         format(3x, 'Objective function: inverse design'/)
         else if ( iobjf.eq.2 ) then
            write (n_out,14) cd_tar, wfd, wtc, wgo
 14         format(3x,
     &           'Objective function: wfd*(1-Cd/Cd*)^2 + ',
     &           'wfl*(1-Cl/Cl*)^2',
     &           /25x,' + wft*(1-t/t*)^2 + wgo*(1- ... ',
     &           /3x,'Cd*=',e12.5,/3x, 
     &           ' WFD =',e9.2,' WTC =',e9.2, ' WGO =',e9.2/)
         else if ( iobjf.eq.3 ) then
            wfd = 0.0
            write (n_out,16)
            write (n_out,17) cl_tar, wfd, wtc, wgo
 16         format(3x,'Objective function: maximize Cl/Cd')
 17         format(3x,'Cl* denotes target Cl/Cd ratio =', e12.5, /3x,
     &           'Cd* is ignored by forcing WFD= 0', /3x,
     &           ' WFD =', e9.2,' WTC =', e9.2,' WGO =',e9.2/)
         else if ( iobjf.eq.0 ) then
            write (n_out,666)
 666        format(3x,'Objective function: minimize mean Cd')       
         else if ( iobjf.eq.5 ) then
            write (n_out,667)
 667        format(3x,'Objective function: minimize mean Cd/Cl')
         else if ( iobjf.eq.8 ) then
            write (n_out,668)
 668        format(3x, 'Objective function: remote inverse design'/) 
         else if ( iobjf.eq.10 ) then
            write (n_out,669)
 669        format(3x,'Objective function: inverse remote design FWH.'/) 
         else if ( iobjf.eq.11 ) then
            write (n_out,671)
 671        format(3x,'Objective function: 
     & minimize far-field pressure fluctutation.'/) 
         else
            write (*,*) 'INPUT: iobjf wrong value!!', iobjf
            stop  
         end if
         
         if ( ioptm.eq.3 ) then 
            write (n_out,20)
 20         format(3x,'Unconstrained BFGS optimization')
         else if ( ioptm.eq.4 ) then 
            write (n_out,25)
 25         format(3x,'Design Space Map')
         else if ( ioptm.eq.6 ) then 
            write (n_out,26)
 26         format(3x,'Unsteady Airfoil Optimization')
         else
            write (n_out,*) '** Error in ioptm input!!! **'
            stop
         end if

         if ( igrad.eq.0 ) then
            write (n_out,30) fd_eta
 30         format (/3x,
     &           "Centered-diff's are used to calculate gradient:",
     &           /3x,'Stepsize:',e12.4)
         else if ( igrad.eq.1 ) then
            write (n_out,32) lfilg, pdcg, imgmr, itgmr, fd_eta,
     &           tolgmr 
 32         format (/3x,
     &           'Adjoint method is used to calculate gradient:',/3x,
     &           'Fill level:',i3,/3x,
     &           'Sigma (pdcg):',e10.2,/3x,
     &           'Krylov search space:',i5,/3x,
     &           'Maximum number of GMRES iterations:',i5,/3x,
     &           'Stepsize:',e10.2,/3x,
     &           'Gradient tolerance:',e10.2)
            
         else if ( igrad.eq.2 ) then
            write (n_out,34) lfilg, pdcg, imgmr, itgmr, fd_eta,
     &           tolgmr
 34         format (/3x,
     &           'Matrix-free sensitivity gradient calculation:',/3x,
     &           'Fill level:',i3,/3x,
     &           'Sigma (pdcg):',e10.2,/3x,
     &           'Krylov search space:',i5,/3x,
     &           'Maximum number of GMRES iterations:',i5,/3x,
     &           'Stepsize:',e10.2,/3x,
     &           'Gradient tolerance:',e10.2)
         else 
            write (n_out,*) '** input.f: error in gradient!!! **'
            stop
         end if
      end if                    !(ioptm)

      if (ioptm.gt.1 .and. mpopt.eq.1) then
c     -- open cp history file --
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+7) = '.cphis'
         open(unit=n_cphis,file=filena,status='new')  
c     -- open cf history file --
         filena = output_file_prefix
         filena(namelen+1:namelen+7) = '.cfhis'
         open(unit=n_cfhis,file=filena,status='new')  
      end if

      if (ioptm.eq.2) then
c     -- open polar history file --
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+5) = '.pol'
         open(unit=n_polar,file=filena,status='new')  
      end if

      filena = output_file_prefix
      filena(namelen+1:namelen+3) = '.q'
      open(unit=n_q,file=filena,status='new',form='unformatted')

      if (unsteady) then 
         filena = output_file_prefix
         filena(namelen+1:namelen+4) = '.q2'
         open(unit=n_q2,file=filena,status='new',form='unformatted')
      end if

      if (turbulnt) then         
         filena = output_file_prefix
         filena(namelen+1:namelen+3) = '.t'
         open(unit=n_t,file=filena,status='unknown',form
     &        ='unformatted')
         if (unsteady) then
            filena = output_file_prefix
            filena(namelen+1:namelen+4) = '.t2'
            open(unit=n_t2,file=filena,status='unknown',form
     &           ='unformatted')
         end if
      end if

      filena = output_file_prefix
      filena(namelen+1:namelen+4) = '.ld'
      open(unit=n_ld, file=filena, status='new', form='formatted')

      filena = output_file_prefix
      filena(namelen+1:namelen+5) = '.eld'
      open(unit=n_eld, file=filena, status='new', form='formatted')

      filena = output_file_prefix
      filena(namelen+1:namelen+4) = '.cp'
      open(unit=n_cp,file=filena,status='new')

      if (viscous) then
         filena = output_file_prefix
         filena(namelen+1:namelen+4) = '.cf'
         open(unit=n_cf,file=filena,status='new',form='formatted')
      endif

      if (timing) then
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.time'
         open(unit=n_time,file=filena)
      endif

c     -- header for history file --
      if (restart) then
         namelen =  strlen(restart_file_prefix)
         filena_restart = restart_file_prefix
         filena_restart(namelen+1:namelen+5) = '.his'
c     
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+5) = '.his'
c     
         command = 'cp '
         namelen = strlen(filena_restart)
         command(4:3+namelen) = filena_restart
c     -length of 'cp ' is 3, length of filena_restart is namelen
c     -add 1 for blank space at end of 'cp filena_restart' 
         lentmp = namelen+3+1
         namelen = strlen(filena)
         command(lentmp+1:lentmp+namelen) = filena
         call system(command)
      else
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+5) = '.his'
      endif
      open(unit=n_his,file=filena,status='unknown',access='append')
c     
c     -- turbulence model convergence history --
      if (turbulnt) then
         if (restart) then
            namelen =  strlen(restart_file_prefix)
            filena_restart = restart_file_prefix
            filena_restart(namelen+1:namelen+6) = '.this'
c     
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+6) = '.this'
c     
            command = 'cp '
            namelen = strlen(filena_restart)
            command(4:3+namelen) = filena_restart
            lentmp = namelen+3+1
            namelen = strlen(filena)
            command(lentmp+1:lentmp+namelen) = filena
            call system(command)
         else
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+5) = '.this'
         endif
         open(unit=n_this,file=filena,status='unknown',access
     &        ='append')
      end if

      if (matcharc2d) then
         retinf=0.001
         nnit_sp=1
      endif
c     
      if(windtunnel) poutflow=.true.
c
      write(n_out,*)
     &     '------------------------------------------------------'  
      write(n_out,*)
     &     '|                                                    |'  
      if (circul) then                                                        
         write(n_out,*)
     &        '|         Far field circulation model used.          |'  
      else
         write(n_out,*)
     &        '|       Far field circulation model not used.        |'  
      endif
      write(n_out,*)
     &     '|                                                    |'  
      write(n_out,*)
     &     '------------------------------------------------------'  

c     -- set target alpha --

      targetalpha = alpha

      if ( viscous ) then

c     -- set multiplier for S-A trip terms --
         if (fturb) then
            ftfac = 0.0
         else
            ftfac = 1.0
         end if

         if (.not. fturb .and. nkits.gt.0) then
            write (n_out,90)
 90         format (3x,'Newton-Krylov solver cannot be used with',
     &           'trip terms.',/3x,'Set FTURB to true for fully',
     &           'turbulent flow or NKITS to zero for transition.')
            stop
         end if
         if (.not. fturb .and. ioptm.gt.2) then
            write (n_out,92)
 92         format (3x,'Optimization only with fully turbulent flow!')
            stop
         end if

         jj = 0
         do 100 ii=1,maxfoil
            jj = jj + int(100.0*(transup(ii) + translo(ii)))
 100     continue
         if (jj.gt.0) then
            write(n_out,*)'    '
            write(n_out,*)'    '
            write(n_out,*)
     &        '------------------------------------------------------'
            write(n_out,*)
     &        '|          Non-zero Transition Locations             |'
            write(n_out,*)
     &        '------------------------------------------------------'
            write(n_out,*)
     &        '|                                                    |'
            do 110 ii=1,maxfoil
               if (abs(transup(ii)).gt.1.0e-09)
     &              write(n_out,850) transup(ii)*100.0,ii
               if (abs(translo(ii)).gt.1.0e-09)
     &              write(n_out,860) translo(ii)*100.0,ii
 110        continue
 850        format (' |    u/s transition @',f6.2,' % on element # '
     &        ,i2,'        |')
 860        format (' |    l/s transition @',f6.2,' % on element # '
     &        ,i2,'        |')
            write(n_out,*)
     &     '|                                                    |'
            if ( iturb.eq.3 .and. fturb ) write(n_out,715)
 715        format(1x,1h|,14x,21h FULLY TURBULENT FLOW,17x, 1h|/,1x,1h|, 
     &           10x,27h NO TRIP TERMS IN S-A MODEL, 15x, 1h|)
            write(n_out,*)
     &       '|                                                    |'
            write(n_out,*)
     &       '------------------------------------------------------'
         endif
c
         write(n_out,*)'    '
         write(n_out,*)'    '
         write(n_out,*)
     &        '------------------------------------------------------'  
         write(n_out,*)
     &        '|               Grid Specifications                  |'  
         write(n_out,*)
     &        '------------------------------------------------------'  
         if(iread.eq.1)then                                            
            write(n_out,*)
     &         '|                                                    |'  
            write(n_out,*)
     &         '|                 Grid file is formatted             |'  
         elseif(iread.eq.2)then                                            
            write(n_out,*)
     &         '|                                                    |'  
            write(n_out,*)
     &         '|                 Grid file is unformatted           |'  
         endif
         if (jacarea) then
            write(n_out,*)
     &         '|    with Jacobian calculated using cell areas.      |'
         else
            write(n_out,*)
     &         '|    with Jacobian calculated using grid metrics.    |'
         endif
         write(n_out,*)
     &         '|                                                    |'
         write(n_out,*)
     &         '------------------------------------------------------'

         write(n_out,*)'    '
         write(n_out,*)'    '
         write(n_out,*)
     &         '------------------------------------------------------'  
         write(n_out,*)
     &         '|                Viscous Parameters                  |'  
         write(n_out,*)
     &         '------------------------------------------------------'  
         write(n_out,*)
     &         '|                                                    |'  
         write(n_out,70) re_input,tinf
 70      format(17H |Reynolds No. = ,e12.6,13H      Tinf = ,f7.3,5x,1H|)
 71      format(17H |     Translo = ,f12.3,13H   Transup = ,f7.3,5x,1H|)
         if(wtrat.ne.0.0)then                                              
            write(n_out,*)
     &         '|                                                    |'  
            write(n_out,*)
     &         '|   Wall temperature ratio used in bc for density    |'  
            write(n_out,*)'|   Wall temperature ratio = ',wtrat                     
         endif                                                             
         write(n_out,*)
     &         '|                                                    |'  
         if(visxi)                                                         
     >        write(n_out,*)
     &         '|   Explicit viscous terms in xi used                |'  
         if(viseta)                                                        
     >        write(n_out,*)
     &         '|   Explicit viscous terms in eta used               |'  
         if(viscross)                                                      
     >        write(n_out,*)
     &         '|   Explicit viscous cross terms used                |'  
         write(n_out,*)
     &         '|                                                    |'

         if(turbulnt)then
            if(iturb.eq.2)
     &           write(n_out,*)
     &         '|   Baldwin-Barth tubulence model is being used      |'
            if(iturb.eq.3) then
               write(n_out,*)
     &         '|   Spalart-Allmaras tubulence model is being used   |'
               write(n_out,19)nnit_sp,retinf
 19            format(2h |,9x,'with nnit_sp=',I2,3x,1h&,3x,7hretinf=,
     &              f6.4,8x,1h|)
            endif
            if(iturb.eq.4)
     &           write(n_out,*)
     &         '|   Menter SST tubulence model is being used         |'
         else
            write(n_out,*)
     &         '|       <<<<<<<< laminar flow assumed >>>>>>>>>>     |'
         endif
c
         write(n_out,*)
     &         '|                                                    |'
         write(n_out,890) ivis
 890     format(' |                ivis = ',i2
     &      ,'                           |')
         if(ivis.eq.1)write(n_out,*)
     >         '|       Implicit block in eta with viscous terms     |'
         if(ivis.eq.2)write(n_out,*)
     >         '|    Penta diag in xi and eta : imp. vis. eig. val.  |'
         if (idmodel.ne.2) visceig=1.d0
         write(n_out,78) visceig
 78      format(2h |,31x,'visceig=',f5.2,8x,1h|)
         write(n_out,*)
     &         '|                                                    |'
         write(n_out,*)
     &         '------------------------------------------------------'
      else
         write(n_out,*)
     &        '|                                                    |'
         write(n_out,*)
     &     '|  <<<<<<<<<  inviscid flow assumed >>>>>>>>>>>      |'
         write(n_out,*)
     &     '|                                                    |'
         write(n_out,*)
     &     '------------------------------------------------------'
         viscoutflow=.false.
      endif
c
      write(n_out,*)'    '
      write(n_out,*)'    '
      write(n_out,*)
     &     '------------------------------------------------------'  
      write(n_out,*)
     &     '|               Algorithm Parameters                 |'  
      write(n_out,*)
     &     '------------------------------------------------------'  
      write(n_out,175)
      write(n_out,*)
     &     '|  Time accuracy ::                                  |'  
      write(n_out,175)
      write(n_out,73) thetadt,phidt
 73   format(14H |  thetadt = ,f7.2,11H   phidt = ,f7.2,15x,1H|)
      if(thetadt.eq.1.0 .and. phidt.eq.0.0)                             
     &     write(n_out,*)
     &     '|  Euler implicit 1st order accurate                 |'  
      if(thetadt.eq.0.5 .and. phidt.eq.0.0)                             
     &     write(n_out,*)
     &     '|  Trap. implicit 2nd order accurate                 |'  
      if(thetadt.eq.1.0 .and. phidt.eq.0.5)                             
     &     write(n_out,*)
     &     '|  3pt Backwards Implicit 2nd order accurate.        |'  
      write(n_out,175)
      write(n_out,175)
      write(n_out,*)
     &     '|  Variable time step parameters  ::                 |'  
      write(n_out,74) dtrate
 74   format(2H |,25x,9hdtrate = ,f7.3,11x,1H|)
 173  format(2H |,6x,40h** Grid Sequencing Option is <active> **,6x,1H|)
 174  format(2H |,9x,35h** Multi-Grid Option is <active> **,8x,1H|)
 175  format(2h |,52x,1h|)
 176  format(2H |,9x,35h         < W Cycle Used >          ,8x,1H|)
 177  format(2H |,18x,i3,1x,11hLevels Used,19x,1h|)
 178  format(2H |,9x,i3,1x,11hLevels Used,2x,3h...,2x,12hLOMET=.true.,
     &     9x,1h|)
 179  format(2H |,5x,43h** Restart from Grid Sequencing Solution **,
     &     4x,1H|)
 180  format(2h |,5x,42h  -Using solution from child/coarser grid-,
     &     5x,1h|)
      write(n_out,175)
c     if(jacdt.eq.0)                                                    
c     &write(n_out,*)'|  <<<<<<<<<<<<<  Time accurate  >>>>>>>>>>>>>>>
c     |'  
c     if(jacdt.eq.1)                                                    
c     &write(n_out,*)'|  <<<<<<<<  Variable dt = 1./(1+sqrt(j))
c     >>>>>>>>>  |'  
c     if(jacdt.eq.2)                                                    
c     &write(n_out,*)'|  <<<<<<<<  Variable dt : min. eigen.
c     >>>>>>>>>>>>  |'  
c     if(jacdt.eq.3)                                                    
c     &write(n_out,*)'|    <<<<<<  Variable dt : const. cfl
c     >>>>>>>>>>>>  |'  
c     write(n_out,*)'|
c     |'  
      if (mg .and. strtit.gt.6) strtit=6
      write(n_out,77) strtit
 77   format(36H |  slow start time  ::    strtit = ,f5.1,13x,1H|)
      write(n_out,175)
      write(n_out,175)
      if (mg) then
         write(n_out,174)
         if (lomet) then
            write(n_out,178) mglev
         else
            write(n_out,177) mglev
         endif
      endif
      if (mg.and.w_cycle) write(n_out,176)
      if (gseq) then
         write(n_out,173)
         write(n_out,177) mglev
      endif
      if (grdseq_rest) then
c     *****  Grid-sequencing restart from a coarse grid *****
c     -note that restart must be set to true so that bcbody_ho
c     does not ramp in viscous bc effect and mutur does not change turre
c     .
         gseq=.true.
         restart=.true.
         write(n_out,179)
         write(n_out,180)
         write(n_out,177) mglev
      endif
      if(orderxy) then                                            
         write(n_out,*)
     &        '|       ** x-y order of implicit integration **      |'  
      else
         write(n_out,*)
     &        '|       ** y-x order of implicit integration **      |'  
      endif
      write(n_out,175)
      if (matcharc2d) then
         write(n_out,*)
     &        '|           Match arc2d option is <active>           |'
         write(n_out,175)
      endif
      write(n_out,*)
     &     '------------------------------------------------------'

      write(n_out,*)'    '
      write(n_out,*)'    '
      write(n_out,*)
     &     '------------------------------------------------------'
      write(n_out,*)
     &     '|        Artificial Dissipation Parameters           |'
      write(n_out,*)
     &     '------------------------------------------------------'
      write(n_out,*)
     &     '|                                                    |'
      write(n_out,*)
     &     '|  Implicit 2nd-4th diff. dissipation used on left   |'  
      write(n_out,*)
     &     '|  Explicit 2nd-4th diff. dissipation used on right. |'  
      write(n_out,*)
     &     '|                                                    |'  
      write(n_out,*)
     &     '|  Dissipation coeficients ::                        |'  
      write(n_out,80) dis2x,dis4x
      write(n_out,81) dis2y,dis4y
 80   format(13H |   dis2x = ,f7.3,12H    dis4x = ,f7.3,15x,1H|)
 81   format(13H |   dis2y = ,f7.3,12H    dis4y = ,f7.3,15x,1H|)
      write(n_out,*)
     &     '|                                                    |'  
      if (idmodel.eq.2) then
         write(n_out,*)
     &        '|  <<<<<<<<<   Matrix Dissipation Used   >>>>>>>>>>  |'
c     write(n_out,101) vlxi,vleta
c     write(n_out,102) vnxi,vneta
c     101  format(13H |    vlxi = ,f7.3,12H    vleta = ,f7.3,15x,1H|)
c     102  format(13H |    vnxi = ,f7.3,12H    vneta = ,f7.3,15x,1H|)
         write(n_out,*)
     &        '|                                                    |'  
      elseif (idmodel.eq.1) then
         write(n_out,*)
     &        '|  <<<<<<<<<   Scalar Dissipation Used   >>>>>>>>>>  |'
         write(n_out,*)
     &        '|                                                    |'
      endif
c     
      if (iord.eq.2) then
         write(n_out,*)
     &        '|  <<<<   Second-Order Spatial Accuracy Used   >>>>  |'
      elseif(iord.eq.4) then
         write(n_out,*)
     &        '|  <<<<   Fourth-Order Spatial Accuracy Used   >>>>  |'
      else
         write(n_out,*)
     &        '|  <<<<      Unknown Spatial Accuracy Used     >>>>  |'
         write(n_out,*)
     &        '             IORD should be set to 2 or 4.            '
         stop
      endif
      if (integ.eq.2) then
         write(n_out,*)
     &        '|         With Second-Order Force Integration        |'
      elseif(integ.eq.3) then
         write(n_out,*)
     &        '|         With Third-Order Force Integration         |'
      else
         write(n_out,*)'Error in input.'
         write(n_out,*)'Value for INTEG should be 2 or 3'
         stop
      endif
      write(n_out,*)
     &     '|                                                    |'  
c     
      if(ispec.eq.0)then                                                
         write(n_out,*)
     &        '|            spectx = specty = 1./xyj                |'  
      elseif(ispec.eq.1)then                                            
         write(n_out,*)
     &        '|     Form spectx and specty independently           |'  
         write(n_out,*)
     &        '|         spectx = (abs(uu) + ccx)/xyj               |'  
         write(n_out,*)
     &        '|         specty = (abs(vv) + ccy)/xyj               |'  
      elseif(ispec.eq.3)then                                            
         write(n_out,*)
     &        '|   Form spectx and specty independently with limits |'  
         write(n_out,*)
     &        '|         spectx = (abs(uu) + ccx)/xyj               |'  
         write(n_out,*)
     &        '|         specty = (abs(vv) + ccy)/xyj               |'  
      elseif(ispec.eq.4)then                                            
         write(n_out,*)
     &        '|     Form spectx and specty independently           |'  
         write(n_out,*)
     &        '|         spectx = [ abs( xi_x) + abs( xi_y)]/xyj    |'  
         write(n_out,*)
     &        '|         specty = [ abs(eta_x) + abs(eta_y)]/xyj    |'  
      elseif(ispec.eq.-1)then                                           
         write(n_out,*)
     &        '|         spectx=specty=64./xyj                      |'  
      endif                                                             
c     
      write(n_out,*)
     &     '|                                                    |'
      write(n_out,*)
     &     '------------------------------------------------------'

      if(clalpha)then
         write(n_out,*)
     &        '------------------------------------------------------'
         write(n_out,*)
     &        '|               Cl vrs Alpha Logic                   |'
         write(n_out,*)
     &        '------------------------------------------------------'
         write(n_out,*)
     &        '|                                                    |'
         write(n_out,*)
     &        '| Option to compute alpha for a given Cl             |'
         write(n_out,960) clinput
 960     format(' |     Cl input = ',f9.4,'                           |'
     &        )
         write(n_out,970) iclstrt
 970     format(' |     Ctart alpha mod after ',i5
     &        ,' iterations         |')
         write(n_out,980) iclfreq
 980     format(' |     Update alpha every ',i5
     &        ,' steps                 |')
         write(n_out,*)'|      using the formula : '
         write(n_out,*)
     &        '|                delta alpha = -relaxcl * delta cl   |'
         write(n_out,*)
     &        '|                delta cl = clinput - cl(claculated) |'
         write(n_out,*)'|                relaxcl = ',relaxcl
         write(n_out,*)
     &        '|                                                    |'
         write(n_out,*)
     &        '| Update of alpha stopped when abs(delta cl) reaches |'
         write(n_out,990) cltol
 990     format(' | tolerance for delta cl = ',f9.4,'                 |'
     &        )
         write(n_out,*)
     &        '|                                                    |'
         isw = 1
         write(n_out,*)
     &        '------------------------------------------------------'
      endif

      write(n_out,820)
      write(n_out,*)
     &     '------------------------------------------------------'
      write(n_out,*)
     &     '|             Boundary Condition Type                |'
      write(n_out,*)
     &     '------------------------------------------------------'
      write(n_out,*)
     &     '|                                                    |'
      if(bcairf)then
         write(n_out,*)
     &        '|         Boundary conditions for airfoil used       |'
      elseif(.not.bcairf)then
         write(n_out,*)
     &        '|       Boundary conditions for flat plate used      |'
      endif
      if(psiavg) then
         write(n_out,*)
     &        '|                                                    |'
         write(n_out,*)
     &        '|         Average psi b.c. option <active>           |'
      endif
      write(n_out,*)
     &     '|                                                    |'
      if(sngvalte) then
         write(n_out,*) 
     &        '|         Single-Valued Trailing Edge Used           |'
      else
         if (viscous) then
            write(n_out,*)
     &          '|         Triple-Valued Trailing Edge Used           |'
         else
            write(n_out,*) 
     &          '|         Dual-Valued Trailing Edge Used             |'
         endif
      endif
      write(n_out,*) 
     &     '|                                                    |'
      if (matcharc2d.and.viscous) viscoutflow=.true.
      if (viscoutflow) then
         write(n_out,*) 
     &        '|  Extrapolation used along x=xmax farfield outflow. |'
      else
         write(n_out,*) 
     &        '| Characteristics used along x=xmax farfield outflow.|'
      endif
      write(n_out,*)
     &     '|                                                    |'
      write(n_out,*)
     &     '------------------------------------------------------'

      write(n_out,820)
      write(n_out,*)
     &     '------------------------------------------------------'
      write(n_out,*)
     &     '|                 I/O Specifications                 |'
      write(n_out,*)
     &     '------------------------------------------------------'
      write(n_out,*)
     &     '|                                                    |'
      if(restart)
     >     write(n_out,*)
     &     '|       This is a restart run using unit 3           |'
      if(store)
     >     write(n_out,*)
     &     '| Data set will stored on unit 4 upon completion     |'
      write(n_out,*)
     &     '|                                                    |'
      write(n_out,*)
     &     '| Output frequency parameters:                       |'
c     write(n_out,701) iprint
      write(n_out,702) nq
      write(n_out,703) ncp
 701  format(' | printed convergence data :: iprint = ',i5,'         |')
 702  format(' | stored solution file     :: nq     = ',i5,'         |')
 703  format(' | stored Cp                :: ncp    = ',i5,'         |')
      write(n_out,*)
     &     '|                                                    |'
      write(n_out,*)
     &     '| Logicals:                                          |'
      if (autowrite)
     >     write(n_out,*)
     &     '| Write auto files option   : <active>               |'
      if (writemets)
     >     write(n_out,*)
     &     '| Write metrics option      : <active>               |'
      if (writedisp)
     >     write(n_out,*)
     &     '| Write dissipation option  : <active>               |'
      if (writeresid)
     >     write(n_out,*)
     &     '| Write residual option     : <active>               |'
      write(n_out,*)
     &     '|                                                    |'
      write(n_out,*)
     &     '------------------------------------------------------'

c     -check maxpen against maxjk
c      maxpen can be either maxjk which allows use of meth = 1,2,5,7
c      or maxpen = maxj so that larger meshes can be used for
c      meth 3,4,6,8

      if(maxpen.ne.maxjk)then
         if(meth.eq.1 .or. meth.eq.2 .or. meth.eq.5 .or. meth.eq.7)then
            if(meth.eq.3 .and. viscous .and. ivis.eq.1)then
               write(n_out,*)
     &              ' dimensions not correct for these methods'
               write(n_out,*)'  change maxpen to be equal to maxjk'
            endif
            stop
         endif
      endif

      if (meth.ne.3 .and. idmodel.eq.2) then
         write(n_out,*)'Matrix dissipation set up for meth=3 only.'
         stop
      endif

      if (nhalo.lt.2 .and. idmodel.gt.1) then
         write(n_out,*)'Warning!'
         write(n_out,*)'Matrix diss. routines can work with nhalo=1 but'
         write(n_out,*
     &        )'NHALO should be set to 2 for this dissipation scheme.'
         write(n_out,*
     &        )'Uncomment stop command in input.f if you want to'
         write(n_out,*)'proceed with nhalo=1.'
         stop
      endif
        
      if (mg.or.gseq) then
         if (mglev.lt.2) then
            write(n_out,*)
     &           'Multi-grid or Grid Sequencing option is active.'
            write(n_out,*)'MGLEV must be greater than 1.'
            write(n_out,*)'Reset MGLEV in input file and try again.'
            stop
         endif
         if (isequal.lt.2) then
            write(n_out,*)
     &           'Multi-grid or Grid Sequencing option is active.'
            write(n_out,*)'ISEQUAL must be greater than 1.'
            write(n_out,*)'Reset ISEQUAL in input file and try again.'
            stop
         endif
         if (iturb.ne.3) then
            write(n_out,*)
     &           'Multi-grid or Grid Sequencing option is currently'
            write(n_out,*)
     &           'available for use with Spalart-Allmaras model only.'
            stop
         endif
      endif

      if (mg) then
         if (gseq) then
            write(n_out,*)'Multi-grid option and Grid Sequencing option'
            write(n_out,*)'cannot both be active at the same time.'
            stop
         endif
         if (restart) then
            write(n_out,*)'Restart option with Multi-grid is not'
            write(n_out,*)'available yet.'
            stop
         endif
         if (viscous .and. .not.sngvalte) then
            write(n_out,*)'SNGVALTE set to false in input file.'
            write(n_out,*)'Multi-grid option for viscous cases requires'
            write(n_out,*)'SNGVALTE=true.'
            stop
         endif
      endif

      if (.not.viscous .and. turbulnt) then
         write(n_out,*)'Viscous set to false and Turbulnt set to true'
         write(n_out,*)'in input file.  Invalid combination.'
         write(n_out,*)'Setting turbulnt to false and proceeding.'
         turbulnt=.false.
      endif
      
      if (prec.gt.0) then
         write(n_out,*)'  '
         write(n_out,*)
     &        '------------------------------------------------------'
         write(n_out,*)
     &        '|                                                    |'
         write(n_out,*)
     &        '|           Preconditioning will be used.            |'
         write(n_out,*)
     &        '|                                                    |'
         if (prec.eq.1) then
            write(n_out,*)
     &          '|           Weiss-Smith,   Epsilon = 1               |'
         elseif (prec.eq.2) then
            write(n_out,*)
     &          '|           Weiss-Smith,   Epsilon = Mr              |'
         elseif (prec.eq.3) then
            write(n_out,*)
     &          '|           Weiss-Smith,   Epsilon = Mr^2            |'
         else
            write(n_out,*)
     &          '|           INCORECT VALUE OF DUPREC!!!!!!           |'
            stop
         endif
         write(n_out,*)
     &        '|                                                    |'
         write(n_out,103) prxi,prphi
 103     format(20H |           prxi = ,f5.2,12H    prphi = ,f5.2,12x
     &        ,1H|)
         write(n_out,*)
     &        '|                                                    |'
         write(n_out,*)
     &        '------------------------------------------------------'
      endif

      write(n_out,820)
 801  format(a72)
 820  format(/)

      return
      end                       !input
