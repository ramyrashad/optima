c
c  diffusion terms in the k-equation for all blocks (explicit and implicit)
c  on rhs of equation
c
      do k=klow,kup
         do j=jlow,jup
c
            g11_p=0.5*(xy(j,k,1)+xy(j+1,k,1))*xy(j,k,1)
     $                +0.5*(xy(j,k,2)+xy(j+1,k,2))*xy(j,k,2)
            g12_p=xy(j+1,k,3)*xy(j,k,1)+xy(j+1,k,4)*xy(j,k,2)
            g11_m=0.5*(xy(j,k,1)+xy(j-1,k,1))*xy(j,k,1)
     $                +0.5*(xy(j,k,2)+xy(j-1,k,2))*xy(j,k,2)
            g12_m=xy(j-1,k,3)*xy(j,k,1)+xy(j-1,k,4)*xy(j,k,2)
c
            g1_p2=xy(j,k+1,1)*xy(j,k,3)+xy(j,k+1,2)*xy(j,k,4)
            g2_p2=0.5*(xy(j,k,3)+xy(j,k+1,3))*xy(j,k,3)
     $                +0.5*(xy(j,k,4)+xy(j,k+1,4))*xy(j,k,4)
            g1_m2=xy(j,k-1,1)*xy(j,k,3)+xy(j,k-1,2)*xy(j,k,4)
            g2_m2=0.5*(xy(j,k,3)+xy(j,k-1,3))*xy(j,k,3)
     $                +0.5*(xy(j,k,4)+xy(j,k-1,4))*xy(j,k,4)
c
            di_plk=0.5*((fnu(j,k)+turnu(j,k)*sigma_k(j,k))*q(j,k,1)
     $            +(fnu(j+1,k)+turnu(j+1,k)*sigma_k(j+1,k))*q(j+1,k,1))
     $            *g11_p
            di_pxk=(fnu(j+1,k)+turnu(j+1,k)*sigma_k(j+1,k))*q(j+1,k,1)
     $            *g12_p
            di_mlk=0.5*((fnu(j,k)+turnu(j,k)*sigma_k(j,k))*q(j,k,1)
     $            +(fnu(j-1,k)+turnu(j-1,k)*sigma_k(j-1,k))*q(j-1,k,1))
     $            *g11_m
            di_mxk=(fnu(j-1,k)+turnu(j-1,k)*sigma_k(j-1,k))*q(j-1,k,1)
     $            *g12_m
c
            di_ple=0.5*((fnu(j,k)+turnu(j,k)*sigma_e(j,k))*q(j,k,1)
     $            +(fnu(j+1,k)+turnu(j+1,k)*sigma_e(j+1,k))*q(j+1,k,1))
     $            *g11_p
            di_pxe=(fnu(j+1,k)+turnu(j+1,k)*sigma_e(j+1,k))*q(j+1,k,1)
     $            *g12_p
            di_mle=0.5*((fnu(j,k)+turnu(j,k)*sigma_e(j,k))*q(j,k,1)
     $            +(fnu(j-1,k)+turnu(j-1,k)*sigma_e(j-1,k))*q(j-1,k,1))
     $            *g11_m
            di_mxe=(fnu(j-1,k)+turnu(j-1,k)*sigma_e(j-1,k))*q(j-1,k,1)
     $            *g12_m
c
            dj_plk=0.5*((fnu(j,k)+turnu(j,k)*sigma_k(j,k))*q(j,k,1)
     $            +(fnu(j,k+1)+turnu(j,k+1)*sigma_k(j,k+1))*q(j,k+1,1))
     $            *g2_p2
            dj_pxk=(fnu(j,k+1)+turnu(j,k+1)*sigma_k(j,k+1))*q(j,k+1,1)
     $            *g1_p2
            dj_mlk=0.5*((fnu(j,k)+turnu(j,k)*sigma_k(j,k))*q(j,k,1)
     $            +(fnu(j,k-1)+turnu(j,k-1)*sigma_k(j,k-1))*q(j,k-1,1))
     $            *g2_m2
            dj_mxk=(fnu(j,k-1)+turnu(j,k-1)*sigma_k(j,k-1))*q(j,k-1,1)
     $            *g1_m2
c
            dj_ple=0.5*((fnu(j,k)+turnu(j,k)*sigma_e(j,k))*q(j,k,1)
     $            +(fnu(j,k+1)+turnu(j,k+1)*sigma_e(j,k+1))*q(j,k+1,1))
     $            *g2_p2
            dj_pxe=(fnu(j,k+1)+turnu(j,k+1)*sigma_e(j,k+1))*q(j,k+1,1)
     $            *g1_p2
            dj_mle=0.5*((fnu(j,k)+turnu(j,k)*sigma_e(j,k))*q(j,k,1)
     $            +(fnu(j,k-1)+turnu(j,k-1)*sigma_e(j,k-1))*q(j,k-1,1))
     $            *g2_m2
            dj_mxe=(fnu(j,k-1)+turnu(j,k-1)*sigma_e(j,k-1))*q(j,k-1,1)
     $            *g1_m2
c
            vkpp=vk(j+1,k+1)
            vkpm=vk(j+1,k-1)
            vkmp=vk(j-1,k+1)
            vkmm=vk(j-1,k-1)
c
            vepp=ve(j+1,k+1)
            vepm=ve(j+1,k-1)
            vemp=ve(j-1,k+1)
            vemm=ve(j-1,k-1)
c
c           xi direction
c
            bx_k(j,k)=bx_k(j,k)+di_mlk
            cx_k(j,k)=cx_k(j,k)-di_mlk-di_plk
            dx_k(j,k)=dx_k(j,k)+di_plk
            rhs_k(j,k)=rhs_k(j,k)+di_mlk*vk(j-1,k)
     $               -(di_mlk+di_plk)*vk(j,k)
     $               +di_plk*vk(j+1,k)
c     $               +0.25*di_pxk*(vkpp-vkpm)
c     $               -0.25*di_mxk*(vkmp-vkmm)
c
            bx_e(j,k)=bx_e(j,k)+di_mle
            cx_e(j,k)=cx_e(j,k)-di_mle-di_ple
            dx_e(j,k)=dx_e(j,k)+di_ple
            rhs_e(j,k)=rhs_e(j,k)+di_mle*ve(j-1,k)
     $               -(di_mle+di_ple)*ve(j,k)
     $               +di_ple*ve(j+1,k)
c     $               +0.25*di_pxe*(vepp-vepm)
c     $               -0.25*di_mxe*(vemp-vemm)
c
            by_k(j,k)=by_k(j,k)+dj_mlk
            cy_k(j,k)=cy_k(j,k)-dj_mlk-dj_plk
            dy_k(j,k)=dy_k(j,k)+dj_plk
            rhs_k(j,k)=rhs_k(j,k)+dj_mlk*vk(j,k-1)
     $               -(dj_mlk+dj_plk)*vk(j,k)
     $               +dj_plk*vk(j,k+1)
c     $               +0.25*dj_pxk*(vkpp-vkmp)
c     $               -0.25*dj_mxk*(vkpm-vkmm)
c
            by_e(j,k)=by_e(j,k)+dj_mle
            cy_e(j,k)=cy_e(j,k)-dj_mle-dj_ple
            dy_e(j,k)=dy_e(j,k)+dj_ple
            rhs_e(j,k)=rhs_e(j,k)+dj_mle*ve(j,k-1)
     $               -(dj_mle+dj_ple)*ve(j,k)
     $               +dj_ple*ve(j,k+1)
c     $               +0.25*dj_pxe*(vepp-vemp)
c     $               -0.25*dj_mxe*(vepm-vemm)
c
         enddo
      enddo
