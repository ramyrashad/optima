c     ########################
c     ##                    ##
c     ##  subroutine input  ##
c     ##                    ##
c     ########################
c
c     modified by marian nemec for optima2D
c     date: 4/12/00

      subroutine input

#ifdef _MPI_VERSION
      use mpi
#endif


      use disscon_vars

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/graph.inc"
#include "../include/mpi_info.inc"


      double precision
     &     wmpo_tmp(nopc), c_upp1_tmp(nopc),
     &     c_low1_tmp(nopc), fsmachs_tmp(nopc), cltars_tmp(nopc),
     &     wfls_tmp(nopc), cdtars_tmp(nopc), reno_tmp(nopc), 
     &     alphas_tmp(nopc), 
     &     wt_int_m(nopc), wt_int_cl(nopc), int_wts(nopc),
     &     delm, delwt, delalt, wt_int_re(nopc), int_wts2(nopc),
     &     wmpo2_tmp(nopc), objwt, objwt2

      integer 
     &     d100, d10, d1, strlen, numberlen, j, i, ondpTotal, 
     &     datevals(3), objfuncs_tmp(nopc), ondpTotal2

      logical 
     &     duerr, dvalfa_tmp(nopc), off_flags_tmp(nopc), 
     &     dvmach_tmp(nopc),
     &     clalphas_tmp(nopc), clalpha2s_tmp(nopc), clopts_tmp(nopc)

      character 
     &     filena_restart*40,command*60,number*4,filename*40

C     START CHANGES MADE BY LASLO

      namelist/optima/opt_meth,obj_func,opt_tol,opt_iter,gradient,
     &     opt_restart,cdf,fd_eta,coef_frz,ifrz_what,wfactor,cd_tar,
     &     obj_restart,erradap,erradaptol,sc_method, tol_gmres2,
     &     cl_tar,inord,ireord,ilu_meth,lfil,pdc,im_gmres,eps_gmres,
     &     iter_gmres,ntcon,wfl,wfd,wtc, mpopt,bstep,tol_gmres1,  
     &     original_grid, incr, gptol, gpmaxit, gatol, scale_obj,
     &     gamaxit,wac, areafac, arearef, moveTE, wt_update_flag,
     &     nrtcon,crtxl,crtxt,crtxn,crthtar ,ctx,cty_tar, scale_con,
     &     nrc, xrc_tar, rc_tar, wrc_con, adptKS, 
     &     gdv_scale, aoadv_scale, mdv_scale, mdv_low, mdv_upp,
     &     auto_restart, num_restarts, warm_start, warm1st,
     &     hessian,hesgrad,hessian_eta,inithes,reset_hessian,
     &     new_search, scaling, warmset, 
     &     sol_his, grid_his, flo_his, cpp_his, cf_his,
     &     remxmin,remxmax,remymin,remymax,remnx,remny, spect_frz,
     &     use_quad_penalty_meth, jmstart, jmend, kmstart, kmend, jminc,
     &     kminc, rhoKS, wt_cycle_iter, wt_aoa, wt_exp, cllow, clup,
     &     mlow, mup, ncl, nm, Mstar, wt_int, wtlow, wtup, nwt, omga,
     &     altlow, altup, nalt, wspan, warea, wt_func, sweep, ref_chord,
     &     spect_frz, dvmach, dvmach0, ndvmach, dl_int, exp_wt,
     &     v_profile, v_profile_upper, v_profile_x, vp_dist, vp_points,
     &     aoaout, fgtout, gmresout, ldout, o2sout, thisout,
     &     acout, bcout, bestout, bspout, dvsout, hestout,
     &     ohisout, cpout, hisout, mout, gvhisout
     
C     END CHANGES MADE BY LASLO

      namelist/cyclone/jmax,kmax,meth,iread,restart,store,jtail1,jtail2,
     &      jacdt,dtrate,periodic,fsmach,alpha,smu,smuim,ispec,wtrat,
     &      tinf,dis2x,dis4x,dis2y,dis4y,foso,phidt,thetadt,strtit,
     &      ismodel,idmodel,ispmod,dswall,sobmax,circul,itmodel,re,
     &      translo,transup,viscous,ivis,turbulnt,orderxy,
     &      visxi,viseta,viscross,sharp,cusp,bcairf,cray,cmesh,clalpha,
     &      clinput,clalpha2,clopt,originalSA,
     &      iclfreq,iclstrt,relaxcl,cltol,jsubmx,vnxi,vlxi,vneta,vleta,
     &      prec,prxi,prphi,unsted,ibc,iord,writeresid,integ,sv_grdseq,
     &      grdseq_rest,flbud,pchord,timing,retinf,writeturb,jacarea,
     &      ramptran,nnit,visceig,sngvalte,mscal,zeroturre,zerote,
     &      scaledq,ncp,nq,sknfrc,ispbc,limiter,epz,epv,
     &      jesdirk,outtime,noutevery,nouteverybig,omegaa,omegaf,dtbig,
     &      klineconst,sbbc,sbeta,jobs,kobs,Nobs,outFWH,xobs,yobs,
     &      jsls,jsle,dissCon,lamDissMax,dcTol, ptest, lamKill,
     &      lamDissMaxT,lamRed,lamTRed,lamLockstep,
     &      gnuplot_res, postProcessOnly, transitionalFlow,
     &      freeTransition, TPGuessOveride, bypassFinalTPRun,
     &      write_blprop, write_blvel, write_tp,
     &      blEdgeMethod, transPredictMethod, underRelaxTP, maxIterTP,
     &      Tu, Ncrit, earlyTP, earlyResidTP, applyTPShift,
     &      percentTPShift, aftLimitTP, xConvTolTP, jConvTolTP
     
      namelist/probe/nk_its, nk_ilu, nk_lfil, nk_pdc, nk_pfrz, nk_imgmr,
     &      nk_itgmr,nk_iends,nk_skip

c     --start changes by huafei
      namelist/graph/graphout,graph_mode,interval,num_graph,graph_pt
c     --end changes by huafei
      namelist/extra/
     %      output_file_prefix,grid_file_prefix,restart_file_prefix
c     
      pi=4.d0*datan(1.d0)

c     --------- Hybrid optimizer options -------                                          
#ifdef _MPI_VERSION
      namelist/hybropt/popsize, procs_per_chromo, hybr_meth, ranseed,
     &   selmethod, maxgen, sn_maj_lim, flow_sol_lim, sample_offset,
     &   optf_output, prob_pert, prob_mut, prob_avg, pen, xbest, xavg, 
     &   xpert, xmut, load_bal_tol, sobol_file, cig, use_db, mingen
#endif

c     -------------------------------------------
c     -- assign input/output file units --
c     -- note: this is the only place in the code where unit
c        numbers are assigned --
c     -- cyclone files --
      data grid_unit, cylgrd_unit, res_unit / 10, 11, 12 / 
      data turb_unit, bud_unit, time_unit, cf_unit / 13, 14, 15, 16 /
      data q2_unit, q_unit, out_unit, cp_unit / 17, 18, 19, 20 /
      data ld_unit, rq_unit, rq2_unit, his_unit / 21, 22, 23, 24 /
      data turbhis_unit, subit_unit, qc_unit, qb_unit / 25, 26, 27, 28 /
      data n_cphis, n_cfhis, rqo_unit, blprop_unit /29, 30, 31, 32/
      data gnuplot_res_unit, tp_unit, blvel_unit / 33, 34, 35 /
      data dettp_unit / 36 /

c     -- optimizer files --
      data bdef_unit, ohis_unit, ghis_unit, opt_unit / 40, 41, 42, 43 / 
      data cptar_unit, dvs_unit, ksopt_unit, tcon_unit/ 44, 48, 49, 50 /
      data gmres_unit, ac_unit, bc_unit, dvhis_unit / 51, 52, 53, 54 /
      data gvhis_unit, bspr_unit, dvsr_unit / 55, 56, 57 /
      data ip_unit, op_unit, par_unit, n_best / 58, 59, 60, 61 /
C     CHANGES MADE BY LASLO, laura
      data hes_unit, hest_unit, obj_unit / 62, 63, 64 /
C     END CHANGES MADE BY LASLO, laura
      data fgt_unit, rccon_unit / 65, 66 /
      data svhis_unit, input_unit, mach_unit / 67, 68, 69/ 
c         time, and radius of curvature constraint (Chad Oldfield)
c     -- multi-point optimization output files --
c     -- make sure all file units are below 80 or above 80+2x(nopc) --
      data n_mpo, n_mpcp, scr_unit, rest_unit /70, 79, 71, 72/
      data prefix_unit, aoa_unit, snopt_unit, iPrint / 73, 74, 75, 76 /
      data snfg_unit, dbc_unit, iSumm / 77, 78, 154 /
      data wtint_unit / 79/
      data opt_def_unit, dpgrad_unit, blvp_unit / 153, 155, 156 /

c========= BEGIN EXECUTION ===========================================


#ifdef _MPI_VERSION
      !-- Store global size (used for file prefixes)
      call mpi_comm_size(MPI_COMM_WORLD, size_glob, ierr)
#endif

      ! force the input file to be read in once
      if ((opt_cycle.eq.1).and.(.not.wt_update)) then 

c     -- set parameter defaults --
         jmax       = 193
         kmax       = 33
         jsubmx     = 2
         ismodel    = 2
         idmodel    = 1
         ispmod     = 1
         itmodel    = 2 
         retinf     = 0.001
         unsted     = .false.
         ibc        = .false.
         iord       = 2
         ispbc      = 0
         integ      = 2
         vlxi       = 0.1d0
         vleta      = 0.1d0
         vnxi       = 0.1d0
         vneta      = 0.1d0
         prec       = 0
         prxi       = 0.d0
         prphi      = 1.d0
         min_res    = 1.d-13
         spmin_res  = 1.d-2
         ramptran    = 0.d0
         foso       = 0.d0
         writeturb  = .false.
         writeresid = .false.
         sv_grdseq  = .false.
         grdseq_rest= .false.
         flbud      = .false.
         pchord     = 0.5
         timing     = .false.
         jacarea    = .false.
         sngvalte   = .false.
         mscal      = .false.
         zeroturre  = .false.
         zerote     = .false.
         sknfrc     = .false.
         scaledq    = 1.d0
         nnit       = 1
         visceig    = 1.d0
         iread      = 0
         restart    = .false.
         store      = .true.
         jtail1     = 1
         jtail2     = 193
         jacdt      = 1            
         dtrate     = 1.d0
         thetadt    = 1.d0
         phidt      = 0.d0
         periodic   = .false.
         fsmach     = .397
         alpha      = 0.0
         smu        = 0.2d0
         smuim      = 0.2d0
         meth       = 3
         ispec      = 1
         dis2x      = 1.d0
         dis4x      = 1.d-2
         dis2y      = 1.d0
         dis4y      = 1.d-2
         strtit     = 12
         dswall     = 1.d-5
         sobmax     = 16.d0
         circul     = .true.
         sharp      = .false.
         cusp       = .false.
         re         = 0.d0
         viscous    = .false.
         ivis       = 2
         turbulnt   = .false.
         dissCon    = .false.
         lamDissMax = 0.01
         lamDissMaxT= 1.0
         lamKill    = 0.1e-5
         lamRed     = 0.5
         lamTRed    = 0.5
         lamLockstep= .false.
         dcTol      = 0.1
         ptest      = .false.
         originalSA = .true.
         visxi      = .false.
         viseta     = .true.
         viscross   = .false.
         clalpha    = .false.
         off_flag   = .false.
         relaxcl    = 2.0d0
         clinput    = 0.d0
         iclfreq    = 50
         iclstrt    = 400
         cltol      = 1.0d-5
         bcairf     = .true.
         wtrat      = 0.d0
         tinf       = 460.d0
         cmesh      = .false.
         orderxy    = .true.
         ncp        = 100
         nq         = 100
         limiter    = 3
         epz        = 1.d-3
         epv        = 5.d0
         wake       = .false.  
         res_spl    = 0.d0
         res_spl2   = 0.d0
         outtime    = .false.
         noutevery  = 1
         nouteverybig = 1
         omegaa     = 0.d0
         omegaf     = 0.d0
         dtbig      = 0.1d0
         klineconst = 10000
         jobs       = 1
         kobs       = 1
         Nobs       = 1
         outFWH     = 0
         xobs       = 0.d0
         yobs       = 0.d0
         sbbc       = .false.
         sbeta      = 90.d0
         jsls       = 0
         jsle       = 0
         erradap    = .false.
         erradaptol = 1.d-6
         jesdirk    = 1                 !< esdirk time marching (1 = BDF2)
         gnuplot_res        = .false.   !< output residual info for gnuplot plotting
         postProcessOnly    = .false.   !< skips calls to the AF and NK flow solvers
                             
c     -- transitional flow default values (sets flow to fully turbulent)
         transup            = 0.d0      !< upper surface transition location (x/c)
         translo            = 0.d0      !< lower surface transition location (x/c)
         transitionalFlow   = .false.   !< false = fully turbulent flow
         freeTransition     = .false.   !< false = fixed transition points (no prediction)
         TPGuessOveride     = .true.    !< false = allow algorithm to pick initial guess for you
         bypassFinalTPRun   = .false.   !< true = skip the final flow solve with final predicted transition points
         transPredictMethod = 1         !< 1 = incompressible AHD transition criterion
                                        !< 2 = compressible AHD transition criterion
                                        !< 3 = Granville's criterion
                                        !< 4 = Michel's criterion (not implemented)
                                        !< 5 = H-Rx criterion (not implemented)
                                        !< 6 = MATTC transition (N-factor) prediction
                                        !< 7 = Drela's e^N envelope method
         blEdgeMethod       = 3         !< BL edge finding method:
                                        !< 1 = compressible Bernouilli equation
                                        !< 2 = Baldwin-Lomax diagnostic function
                                        !< 3 = vorticity/shear-stress method
         underRelaxTP       = 0.9       !< under-relaxation factor for TP movement
         maxIterTP          = 5         !< max number of iterations for transition prediciton
         Tu                 = 0.0007014 !< free-stream turbulence intensity (ex: for Tu = 0.1% enter 0.001)
         Ncrit              = 9.0       !< critical N-factor for e^N method transition prediction (MATTC)
         earlyTP            = .false.   !< flag TRUE if only a partial AF convergence req'd for TP
         earlyResidTP       = 5.0E-5    !< AF residual for early transition prediction (see earlyTP)
         applyTPShift       = .true.    !< flag TRUE if shifting predicted TP downstream by a % of Re_theta 
         percentTPShift     = 15.d0     !< percentage of Re_theta by which predicted points are shifted downstream
         aftLimitTP         = 0.95d0    !< furthest aft chord position allowed for transition points
         xConvTolTP         = 0.030     !< convergence tolerance based on x/c distance
         jConvTolTP         = 5         !< convergence tolerance based on number of streamwise nodes
         write_blprop       = .false.   !< output BL properterties to file
         write_blvel        = .false.   !< output BL velocity profiles to file
         write_tp           = .false.   !< output transition prediction info to file

c     -- hybropt_parameters

#ifdef _MPI_VERSION
         popsize = 1
         procs_per_chromo = 1
         hybr_meth = 1
         call itime(datevals)
         ranseed = datevals(1) * 10000 + datevals(2) * 100 + datevals(3)
         selmethod = 1
         mingen = 1
         maxgen = 1000
         sn_maj_lim = 10000
         flow_sol_lim = 10000
         sample_offset = 0
         optf_output = 131

         prob_pert = 0.2d0
         prob_mut = 0.2d0
         prob_avg = 1.0d0
         pen = 10.0d0
         xbest = 10.0d0
         xavg  = 30.0d0
         xpert = 30.0d0
         xmut  = 30.0d0
         load_bal_tol = 1.0d0

         sobol_file = 'sobolfile'

         cig = .false.
         use_db = .false.
#endif

         optdef = .false.
         aoaout = .true.
         fgtout = .true. 
         gmresout = .true.
         ldout = .true.
         o2sout = .true.
         thisout = .true.
         acout = .true.
         bcout = .true. 
         bestout = .true.
         bspout = .true.
         dvsout = .true.
         hestout = .true.
         ohisout = .true.
         cpout = .true.
         hisout = .true.
         mout = .true.
         gvhisout = .true.
c     -- optimization parameters --
         opt_meth   = 1
         flowAnalysisOnly = .true.
         obj_func   = 1
         opt_tol    = 1.d-8
         opt_iter   = 1
         gradient   = 1 
         sc_method  = 3
         opt_restart= .false.
         obj_restart= .false.
         cdf        = .true.
         scale_obj  = .false.     ! (Chad Oldfield, Laura Billing)
         scale_con  = .false.
         wt_update_flag = .false.! (H.Buckley)
         wt_update = .false.
         wt_aoa = .false.
         gdv_scale = 0.01
         aoadv_scale = 1.0
         mdv_scale = 0.65
         mdv_low = 0.35
         mdv_upp = 0.78
         spect_frz = .false.         
         wt_exp = 4
         wt_cycle_iter = 1000
         cllow = 0.2
         clup  = 0.3
         mlow  = 0.78
         mup   = 0.88
         wtlow = 27216
         wtup  = 45259
         altlow= 8839
         altup = 11887
         nm   = 2
         ncl  = 2
         nwt  = 2
         nalt = 2
         wspan = 28.75
         warea = 92.90
         sweep = 35.0
         ref_chord = 3.93
         wt_func = 0
         omga    = 1.0
         exp_wt = 20.d0
         wt_int = .false.
         dl_int = .true.
         clmax_flag = .false.
         warm_start = .true.     ! (H.Buckley)
         warm1st = .false.       ! (H.Buckley)
         sol_his = .false.       ! (H.Buckley)
         grid_his = .false.      ! (H.Buckley)
         flo_his = .false.       ! (H.Buckley)
         cpp_his = .false.        ! (R.Rashad)
         cf_his = .false.        ! (R.Rashad)
         adptKS = .false.        ! (H.Buckley)
         original_grid = .true.  ! (Chad Oldfield)
         moveTE     = .false.
         incr       = 0          ! (Chad Oldfield)
         gptol      = 1.d-15     ! (Chad Oldfield)
         gpmaxit    = 5000       ! (Chad Oldfield)
         gatol      = 1.d-15     ! (Chad Oldfield)
         gamaxit    = 5000       ! (Chad Oldfield)
         areafac    = 1.0d0
         arearef    = 0.0d0
         wac        = 0.0d0
     	   v_profile  = false 
         v_profile_upper = true 
         v_profile_x = 0.5 
         vp_dist = 1.0 
         vp_points = 100
c        Radius of curvature constraint parameters (Chad Oldfield)
         nrc        = 2
         wrc_con    = 0.d0
         xrc_tar(1) = 0.d0
         xrc_tar(2) = 1.d0
         rc_tar(1)  = 0.d0
         rc_tar(2)  = 0.d0

         auto_restart = .false.
         num_restarts = 0

         fd_eta     = 1.d-4
         coef_frz   = .false.
         ifrz_what  = 1
         wfactor    = 1.0
         cd_tar     = 0.d0
         cl_tar     = 0.d0
         inord      = 2
         ireord     = 2
         ilu_meth   = 2
         lfil       = 5
         pdc        = 3.d0
         im_gmres   = 60
         eps_gmres  = 1.d-5
         iter_gmres = 500
         tol_gmres1 = 1e-1
         tol_gmres2 = 1e-1
         bstep      = 1.0
         do i=1,nopc
            dvalfa(i) = .false.
         end do
         do i=1,nopc
            dvmach(i) = .false.
         end do
         dvmach0 = 0.50
         ndvmach = 0
         use_quad_penalty_meth = .false. 
         jmstart = 1
         jmend = 200
         kmstart = 1
         kmend = 40
         jminc = 1
         kminc = 1
         rhoKS = 40.0
         Mstar = 1.35

c     -- remote inverse design control box--
      remxmin = -5.d0
      remxmax =  5.d0
      remymin = -5.d0
      remymax =  5.d0
      remnx = 100
      remny = 100

C     START CHANGES MADE BY LASLO
      hessian    = .false.
      hessian_eta     = 1.d-4
      hesgrad    = 1
      inithes    = 0  ! Modified by Chad Oldfield (was 2)
      reset_hessian = .false.
      new_search = .false.
      scaling    = 0
      warmset    = 1

C     END CHANGES MADE BY LASLO

c     -- N-K solver --
      nk_its   = 20
      nk_ilu   = 2
      nk_lfil  = 2
      nk_pfrz  = 1
      nk_pdc   = 6.d0
      nk_imgmr = 40
      nk_itgmr = 40
      nk_iends   = 0
      nk_skip   = 0

      af_minr   = 1.d-7  
      min_res   = 1.d-15
      spmin_res = 1.d-2
      simin_res = 1.d-12

c     --start changes by huafei
         graphout=.false.
         graph_mode=1
         interval=0
         num_graph=0
         do i=1,4
            graph_pt(i)=0
         end do
c     --end changes by huafei

c     -- Open the input file 'opt.inp'

         open(unit=input_unit,file='opt.inp',status='old',
     &         form='formatted')
c     -- read in the data for the run --

         read(input_unit,*)         
         read(input_unit,*) mpopt

         if (mpopt.lt.1) then
            stop 'INPUT.f: MPOPT should be greater than ZERO!'
         end if
         if (mpopt.gt.nopc) then
            stop 'INPUT.f: MPOPT greater than NOPC, check optcom.inc!'
         end if

#ifdef _MPI_VERSION
         continue
#else
         if (mpopt.gt.1) then
            write(*,*) 'INPUT.f: MPOPT greater than 1...'
            write(*,*) 'Cannot run multipoint optimizations on this
     & computer...'
            write(*,*) 'Terminating optimization'
            stop
         end if
#endif
         read(input_unit,*)
         
         do i = 1, mpopt
            read(input_unit,*) wmpo(i), off_flags(i),c_upp1(i),
     &           c_low1(i), fsmachs(i), dvmach(i),
     &           cltars(i),wfls(i),cdtars(i),
     &           reno(i),dvalfa(i),alphas(i), objfuncs(i), clalphas(i),
     &           clalpha2s(i), clopts(i)
         end do

#ifdef _MPI_VERSION
         read(input_unit,nml=hybropt)
#endif
         read(input_unit,nml=optima)
         read(input_unit,nml=cyclone)
         read(input_unit,nml=probe)
         read(input_unit,nml=extra)
c     --start changes by huafei
         read(input_unit,nml=graph)
         if(graphout.eq..true.)then
            if(num_graph.gt.4.or.num_graph.gt.mpopt)then
               stop 'maximum graph to output is 4 or mpopt'
            end if
            if(graph_mode.ne.1.and.graph_mode.ne.2)
     &           then
               stop 'graph mode could be only 1 or 2'
            end if
            i=1
            do while(graph_pt(i).ne.0)
               if(graph_pt(i).gt.mpopt)then
                  stop 'check graph_pt'
               end if
               i=i+1
            end do
            if(i.ne.num_graph+1)then
               stop 'num_graph does not match the real num of graph'
            end if
         end if
c     --end changes by huafei

c     -- other inputs --
         read(input_unit,*) af_minr
         read(input_unit,*) min_res
         read(input_unit,*) spmin_res
         read(input_unit,*) simin_res


         !-- Save the initial guess of upper and lower transition points
         transup_initGuess = transup
         translo_initGuess = translo


        !-- Count the number of off-design points specified

         noffcon = 0

         do i=1,mpopt

            if (off_flags(i)) noffcon = noffcon+1

         end do

         !-- Collect the upper and lower bounds for each off-design
         !-- constraint and store in arrays 'c_upp2' and c_low2'

         j=1

         do i=1,mpopt
            
            if (off_flags(i)) then

               c_upp2(j) = c_upp1(i)
               c_low2(j) = c_low1(i)

               j=j+1

            end if

         end do

         !-- Count the number of points with lift-constrained-drag-min
         !-- objective OBJ_FUNC=19

         nlcdm = 0

         do i=1,mpopt

            if (objfuncs(i).eq.19) nlcdm = nlcdm+1

         end do

         !-- Collect the lift constraint targets for each design point
         !-- with OBJ_FUNC=19 and store in array 'cltars2'

         j=1

         do i=1,mpopt

            if (objfuncs(i).eq.19) then

               cltars2(j) = cltars(i)

               j=j+1

            end if

         end do

         !-- If number of design points in a multipoint optimization
         !-- is greater than the number of processors, then terminate
         !-- this optimization

#ifdef _MPI_VERSION

         if (mpopt.gt.size) then


            if (rank==0) then

               write(scr_unit,*) 
     &         'STOP: No. proc. must be greater than No. Op. Pt'

            endif

            stop

         end if ! (mpopt.gt.size)

#endif

      endif                     !endif opt_cycle.eq.1

      !------------ Group communicators if needed -------------
      mycommid = 1
#ifdef _MPI_VERSION
      if (opt_meth .eq. 11) then
         call groupComms()
      endif
#endif
      !--------------------------------------------------------
      !-- Open the file containing file prefixes 'file_prefixes'
      open(unit=prefix_unit,file='file_prefixes',status='old',
     &         form='formatted')

      !-- Set file prefixes for a restart
      !-- For the inital run before any restarts, the output and restart
      !-- file prefixes are 'rest00' and rest00'

      rewind(prefix_unit)
      read(prefix_unit,*)output_file_prefix
      read(prefix_unit,*)restart_file_prefix
      read(prefix_unit,*)opt_restart
      read(prefix_unit,*)obj_restart

#ifdef _MPI_VERSION

      !-- Assign design point operating conditions according to processor
      !-- rank number.

      re_in = reno(rank+1)
      re = re_in
      fsmach = fsmachs(rank+1)
      cl_tar = cltars(rank+1)
      alpha  = alphas(rank+1)
      cd_tar = cdtars(rank+1)
      obj_func = objfuncs(rank+1)
      clalpha = clalphas(rank+1)
      clalpha2 = clalpha2s(rank+1)
      clopt = clopts(rank+1)
      off_flag = off_flags(rank+1)

#else

      re_in = reno(1)
      re = re_in
      fsmach = fsmachs(1)
      cl_tar = cltars(1)
      alpha  = alphas(1)
      cd_tar = cdtars(1)
      obj_func = objfuncs(1)
      clalpha = clalphas(1)
      clalpha2 = clalpha2s(1)
      clopt = clopts(1)
      off_flag = off_flags(1)

#endif

      !-- Slightly adjust the output and restart file names so that each
      !-- processor can write to or read from a different file without 
      !-- clashing

      if (opt_cycle.eq.1) then

         !-- 'output_file_prefix0' is reserved for  read/write 
         !-- operations that are assigned exclusively to processor 0
         output_file_prefix0=output_file_prefix
         restart_file_prefix0=restart_file_prefix

#ifdef _MPI_VERSION

         d10  = rank / 10
         d1   = rank - (d10*10)

#else

         d10  = 0 / 10
         d1   = 0 - (d10*10)

#endif

         !-- 'output_file_prefix0' is reserved for  read/write operations
         !-- that are assigned exclusively to processor 0
      
         output_file_prefix0=output_file_prefix
         restart_file_prefix0=restart_file_prefix

#ifdef _MPI_VERSION         
         if (opt_meth .eq. 11) then
            d100 = ((mycommid - 1) * procs_per_chromo + rank) / 100
            d10  = ((mycommid - 1) * procs_per_chromo + rank) / 10 
     &             - (d100*10)
            d1   = ((mycommid - 1) * procs_per_chromo + rank) / 1 
     &             - (d10*10) - (d100*100)
         else
            d100 = rank / 100
            d10  = rank / 10 - (d100*10)
            d1   = rank / 1 - (d10*10) - (d100*100)
         endif
#endif

         !-- Create a unique output file prefix for each processor 
         !-- for example, if output_file_prefix is 'rest##', then 
         !-- processors with ranks 0, 1, 2,... will have 
         !-- output_file_prefix = rest##-00, rest##-01, rest##-02,...

         namelen =  strlen(output_file_prefix)

         if (size_glob .gt. 99) then
            output_file_prefix(namelen+1:namelen+4)
     &         = '-'//char(d100+48)//char(d10+48)//char(d1+48)
         else
         output_file_prefix(namelen+1:namelen+3)
     &         = '-'//char(d10+48)//char(d1+48)
         endif      

         !-- Save output file prefix for use in the weight update 
         !-- procedure

         output_file_prefix_backup = output_file_prefix

         !-- Create a unique restart file prefix for each processor

         namelen =  strlen(restart_file_prefix)

         if (size_glob .gt. 99) then
            restart_file_prefix(namelen+1:namelen+4)
     &         = '-'//char(d100+48)//char(d10+48)//char(d1+48)
         else
         restart_file_prefix(namelen+1:namelen+3)
     &         = '-'//char(d10+48)//char(d1+48)
         endif       

      end if
 
      !-- Storing output file prefixes to be used in auto_restart
      if((opt_cycle.eq.1).and.(auto_restart)) then
         output_file_prefix_old = output_file_prefix
         output_file_prefix0_old = output_file_prefix0
      endif

      !-- Create output file prefixes for optimization auto-restart
      !-- cycles.

      if ((auto_restart).and.(opt_cycle.gt.1).and.(.not.wt_update))then
         
         grid_file_prefix = output_file_prefix0

         !-- 'output_file_prefix' for auto-restart

         namelen =  strlen(output_file_prefix_old)

         filena = output_file_prefix_old

         !-- Convert the integer value of 'opt_cycle' to a string to be 
         !-- concatenated onto the end of the output_file prefix. 
         !-- Subroutine 'i_to_s' does this.

         call i_to_s(opt_cycle, number)
         
         numberlen = len(number)

         filena(namelen+1:namelen+numberlen+1)=
     &        '-'//number

         output_file_prefix = filena 

         !-- 'output_file_prefix0' for auto-restart

         namelen =  strlen(output_file_prefix0_old)

         filena = output_file_prefix0_old

         !-- Convert the integer value of 'opt_cycle' to a string to be 
         !-- concatenated onto the end of the output_file prefix. 
         !-- Subroutine 'i_to_s' does this.

         call i_to_s(opt_cycle, number)
         
         numberlen = len(number)

         filena(namelen+1:namelen+numberlen+1)=
     &        '-'//number

         output_file_prefix0 = filena 

      endif

      !-- Open a scratch file for reading and writting restart 
      !-- information

#ifdef _MPI_VERSION

      d10  = rank / 10
      d1   = rank - (d10*10)

#else

      d10  = 0 / 10
      d1   = 0 - (d10*10)

#endif


#ifdef _MPI_VERSION
      if (opt_meth .eq. 11) then
         d100 = ((mycommid - 1) * procs_per_chromo + rank) / 100
         d10  = ((mycommid - 1) * procs_per_chromo + rank) / 10 
     &          - (d100*10)
         d1   = ((mycommid - 1) * procs_per_chromo + rank) / 1
     &          - (d10*10) - (d100*100)
      else
         d100 = rank / 100
         d10  = rank / 10 - (d100*10)
         d1   = rank / 1 - (d10*10) - (d100*100)
      endif
#endif      

      filename = 'restart'
      namelen =  len_trim(filename)
      if (size_glob .gt. 99) then
         filename(namelen+1:namelen+3) = 
     &      char(d100+48)//char(d10+48)//char(d1+48) 
      else
      filename(namelen+1:namelen+2)=char(d10+48)//char(d1+48)
      endif
      
      open(unit=rest_unit,file=filename,status='unknown',
     &       form='unformatted')

      !-- Open file for writing general info about optimimization 
      !-- running on the current processor. There will be a separate 
      !-- file created for each processor running in a multipoint 
      !-- optimization. In the serial version of optima2D, all of 
      !-- this general info is written to standard output, 
      !-- i.e. the screen

      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+6) = '.scr'
      open(unit=scr_unit,file=filena,status='new')

#ifdef _MPI_VERSION

      write(scr_unit,874) rank
 874  format
     & (/3x,'Standard output statements from processor [',i3,' ]:',/)

#else

      write(scr_unit,874) 
 874  format
     & (/3x,'Standard output statements from processor [ 1 ]:',/)

#endif

      !-- Generate design points for a weighted integral objective
      !-- function
      if (wt_int) then

         call wt_int_dps(int_wts, wt_int_m, wt_int_cl, ondpTotal,
     &        delm, delwt, delalt, wt_int_re, int_wts2)

         !-- Update the total number design points
         mpopt = ondpTotal + noffcon

         !-- Store arrays containing the current set of design point
         !-- operating conditions
         wmpo_tmp = wmpo
         wmpo2_tmp = wmpo
         off_flags_tmp = off_flags
         c_upp1_tmp = c_upp1
         c_low1_tmp = c_low1
         fsmachs_tmp = fsmachs
         dvmach_tmp  = dvmach
         cltars_tmp = cltars
         wfls_tmp = wfls
         cdtars_tmp = cdtars
         reno_tmp = reno
         dvalfa_tmp = dvalfa
         alphas_tmp = alphas
         objfuncs_tmp = objfuncs
         clalphas_tmp = clalphas
         clalpha2s_tmp = clalpha2s
         clopts_tmp = clopts

#ifdef _MPI_VERSION

         !-- If doing special case with multiple weighted integrals
         if (ndvmach.eq.2) mpopt = 2*ondpTotal+noffcon
         if (ndvmach.eq.2.and.dvalfa(3)) mpopt = 2*ondpTotal+noffcon+1

         !-- Update arrays with new design point operating conditions
         do i = 1,ondpTotal
            wmpo(i) = int_wts(i)
            wmpo2(i) = int_wts2(i)
            fsmachs(i) = wt_int_m(i)
            dvmach(i)  = dvmach(1)
            cltars(i) = wt_int_cl(i)
            off_flags(i) = off_flags(1)
            c_upp1(i) = c_upp1_tmp(1)
            c_low1(i) = c_low1_tmp(1)
            wfls(i) = wfls_tmp(1)
            cdtars(i) = cdtars_tmp(1)
            reno(i) = wt_int_re(i)
            dvalfa(i) = dvalfa_tmp(1)
            alphas(i) = alphas_tmp(1)
            objfuncs(i) = objfuncs_tmp(1)
            clalphas(i) = clalphas_tmp(1)
            clalpha2s(i) = clalpha2s_tmp(1)
            clopts(i) = clopts_tmp(1)
         end do

         j = 1
         do i = ondpTotal+1,mpopt
            wmpo(i) = wmpo_tmp(j+1)
            wmpo2(i) = wmpo2_tmp(j+1)
            fsmachs(i) = fsmachs_tmp(j+1)
            dvmach(i)  = dvmach(j+1)
            cltars(i) = cltars_tmp(j+1)
            off_flags(i) = off_flags_tmp(j+1)
            c_upp1(i) = c_upp1_tmp(j+1)
            c_low1(i) = c_low1_tmp(j+1)
            wfls(i) = wfls_tmp(j+1)
            cdtars(i) = cdtars_tmp(j+1)
            reno(i) = reno_tmp(j+1)
            dvalfa(i) = dvalfa_tmp(j+1)
            alphas(i) = alphas_tmp(j+1)
            objfuncs(i) = objfuncs_tmp(j+1)
            clalphas(i) = clalphas_tmp(j+1)
            clalpha2s(i) = clalpha2s_tmp(j+1)
            clopts(i) = clopts_tmp(j+1)
            j = j+1
         end do

         if (ndvmach.eq.2.and.(.not.dvalfa_tmp(3))) then

            do i = 1,ondpTotal
               wmpo(i) = wmpo_tmp(1)*int_wts(i)
               wmpo2(i) = wmpo_tmp(1)*int_wts2(i)
               fsmachs(i) = wt_int_m(i)
               dvmach(i)  = dvmach(1)
               cltars(i) = wt_int_cl(i)
               off_flags(i) = off_flags(1)
               c_upp1(i) = c_upp1_tmp(1)
               c_low1(i) = c_low1_tmp(1)
               wfls(i) = wfls_tmp(1)
               cdtars(i) = cdtars_tmp(1)
               reno(i) = wt_int_re(i)
               dvalfa(i) = dvalfa_tmp(1)
               alphas(i) = alphas_tmp(1)
               objfuncs(i) = objfuncs_tmp(1)
               clalphas(i) = clalphas_tmp(1)
               clalpha2s(i) = clalpha2s_tmp(1)
               clopts(i) = clopts_tmp(1)
            end do

            do i = 1, ondpTotal
               wmpo(i+ondpTotal) = wmpo_tmp(2)*int_wts(i)
               wmpo2(i+ondpTotal) = wmpo_tmp(2)*int_wts2(i)
               fsmachs(i+ondpTotal) = wt_int_m(i)
               dvmach(i)  = dvmach(2)
               cltars(i+ondpTotal) = wt_int_cl(i)
               off_flags(i+ondpTotal) = off_flags(2)
               c_upp1(i+ondpTotal) = c_upp1_tmp(2)
               c_low1(i+ondpTotal) = c_low1_tmp(2)
               wfls(i+ondpTotal) = wfls_tmp(2)
               cdtars(i+ondpTotal) = cdtars_tmp(2)
               reno(i+ondpTotal) = wt_int_re(i)
               dvalfa(i+ondpTotal) = dvalfa_tmp(2)
               alphas(i+ondpTotal) = alphas_tmp(2)
               objfuncs(i+ondpTotal) = objfuncs_tmp(2)
               clalphas(i+ondpTotal) = clalphas_tmp(2)
               clalpha2s(i+ondpTotal) = clalpha2s_tmp(2)
               clopts(i+ondpTotal) = clopts_tmp(2)
            end do

            j = 1
            do i = ondpTotal*2+1,mpopt
               wmpo(i) = wmpo_tmp(j+2)
               wmpo2(i) = wmpo2_tmp(j+2)
               fsmachs(i) = fsmachs_tmp(j+2)
               dvmach(i)  = dvmach(j+2)
               cltars(i) = cltars_tmp(j+2)
               off_flags(i) = off_flags_tmp(j+2)
               c_upp1(i) = c_upp1_tmp(j+2)
               c_low1(i) = c_low1_tmp(j+2)
               wfls(i) = wfls_tmp(j+2)
               cdtars(i) = cdtars_tmp(j+2)
               reno(i) = reno_tmp(j+2)
               dvalfa(i) = dvalfa_tmp(j+2)
               alphas(i) = alphas_tmp(j+2)
               objfuncs(i) = objfuncs_tmp(j+2)
               clalphas(i) = clalphas_tmp(j+2)
               clalpha2s(i) = clalpha2s_tmp(j+2)
               clopts(i) = clopts_tmp(j+2)
               j = j+1
            end do

         end if

         if (ndvmach.eq.2.and.dvalfa_tmp(3)) then

            do i = 1,ondpTotal
               wmpo(i) = wmpo_tmp(1)*int_wts(i)
               wmpo2(i) = wmpo_tmp(1)*int_wts2(i)
               fsmachs(i) = wt_int_m(i)
               dvmach(i)  = dvmach_tmp(1)
               cltars(i) = wt_int_cl(i)
               off_flags(i) = off_flags(1)
               c_upp1(i) = c_upp1_tmp(1)
               c_low1(i) = c_low1_tmp(1)
               wfls(i) = wfls_tmp(1)
               cdtars(i) = cdtars_tmp(1)
               reno(i) = wt_int_re(i)
               dvalfa(i) = dvalfa_tmp(1)
               alphas(i) = alphas_tmp(1)
               objfuncs(i) = objfuncs_tmp(1)
               clalphas(i) = clalphas_tmp(1)
               clalpha2s(i) = clalpha2s_tmp(1)
               clopts(i) = clopts_tmp(1)
            end do

            do i = 1, ondpTotal
               wmpo(i+ondpTotal) = wmpo_tmp(2)*int_wts(i)
               wmpo2(i+ondpTotal) = wmpo_tmp(2)*int_wts2(i)
               fsmachs(i+ondpTotal) = wt_int_m(i)
               dvmach(i+ondpTotal)  = dvmach_tmp(2)
               cltars(i+ondpTotal) = wt_int_cl(i)
               off_flags(i+ondpTotal) = off_flags(2)
               c_upp1(i+ondpTotal) = c_upp1_tmp(2)
               c_low1(i+ondpTotal) = c_low1_tmp(2)
               wfls(i+ondpTotal) = wfls_tmp(2)
               cdtars(i+ondpTotal) = cdtars_tmp(2)
               reno(i+ondpTotal) = wt_int_re(i)
               dvalfa(i+ondpTotal) = dvalfa_tmp(2)
               alphas(i+ondpTotal) = alphas_tmp(2)
               objfuncs(i+ondpTotal) = objfuncs_tmp(2)
               clalphas(i+ondpTotal) = clalphas_tmp(2)
               clalpha2s(i+ondpTotal) = clalpha2s_tmp(2)
               clopts(i+ondpTotal) = clopts_tmp(2)
            end do
            
            do i = ondpTotal*2+1,ondpTotal*2+1
               wmpo(i) = wmpo_tmp(3)
               wmpo2(ix) = wmpo_tmp(3)
               fsmachs(i) = fsmachs_tmp(3)
               dvmach(i)  = dvmach_tmp(3)
               cltars(i) = cltars_tmp(3)
               off_flags(i) = off_flags(3)
               c_upp1(i) = c_upp1_tmp(3)
               c_low1(i) = c_low1_tmp(3)
               wfls(i) = wfls_tmp(3)
               cdtars(i) = cdtars_tmp(3)
               reno(i) = reno_tmp(3)
               dvalfa(i) = dvalfa_tmp(3)
               alphas(i) = alphas_tmp(3)
               objfuncs(i) = objfuncs_tmp(3)
               clalphas(i) = clalphas_tmp(3)
               clalpha2s(i) = clalpha2s_tmp(3)
               clopts(i) = clopts_tmp(3)
            end do

            j = 1
            do i = ondpTotal*2+2,mpopt
               wmpo(i) = wmpo_tmp(j+3)
               wmpo2(i) = wmpo2_tmp(j+3)
               fsmachs(i) = fsmachs_tmp(j+3)
               dvmach(i)  = dvmach(j+3)
               cltars(i) = cltars_tmp(j+3)
               off_flags(i) = off_flags_tmp(j+3)
               c_upp1(i) = c_upp1_tmp(j+3)
               c_low1(i) = c_low1_tmp(j+3)
               wfls(i) = wfls_tmp(j+3)
               cdtars(i) = cdtars_tmp(j+3)
               reno(i) = reno_tmp(j+3)
               dvalfa(i) = dvalfa_tmp(j+3)
               alphas(i) = alphas_tmp(j+3)
               objfuncs(i) = objfuncs_tmp(j+3)
               clalphas(i) = clalphas_tmp(j+3)
               clalpha2s(i) = clalpha2s_tmp(j+3)
               clopts(i) = clopts_tmp(j+3)
               j = j+1
            end do

         end if

#endif


      end if

#ifdef _MPI_VERSION

      !-- Assign design point operating conditions according to 
      !-- processor rank number.
      re_in = reno(rank+1)
      re = re_in
      fsmach = fsmachs(rank+1)
      cl_tar = cltars(rank+1)
      alpha  = alphas(rank+1)
      cd_tar = cdtars(rank+1)
      obj_func = objfuncs(rank+1)
      clalpha = clalphas(rank+1)
      clalpha2 = clalpha2s(rank+1)
      clopt = clopts(rank+1)
      off_flag = off_flags(rank+1)

#else

      re_in = reno(1)
      re = re_in
      fsmach = fsmachs(1)
      cl_tar = cltars(1)
      alpha  = alphas(1)
      cd_tar = cdtars(1)
      obj_func = objfuncs(1)
      clalpha = clalphas(1)
      clalpha2 = clalpha2s(1)
      clopt = clopts(1)
      off_flag = off_flags(1)

#endif

      if (auto_restart .and. opt_meth.eq.3) then
        if (opt_cycle.eq.1) then
            write(scr_unit,875) 
        endif
        write(scr_unit,*)'grid_file_prefix     = ',grid_file_prefix
        write(scr_unit,*)'output_file_prefix   = ',output_file_prefix
        write(scr_unit,876) opt_cycle, num_restarts
      endif

      if((opt_restart).and.(opt_meth.eq.3)) then
         rewind(72)
         read(72) wt_cycle
      end if

      if (opt_meth.eq.3) write(scr_unit,877) wt_cycle

 875  format(/3x,'Automatic Restart enabled for BFGS')
 876  format(/3x,'Auto-Restart Cycle ',i3,' of ',i3)
 877  format(/3x,'Weight Update Cycle [',i3,']')

      !----------------------------------------------------
      !------- Open SNOPT Output Files --------------------
      !----------------------------------------------------

      if (opt_meth.eq.8 .or. opt_meth.eq.11) then

         !-- Open file for transfering optima2D variables to SNOPT
      
         if (opt_restart) then
           command = 'cp '      
           namelen =  strlen(restart_file_prefix)
           filena = restart_file_prefix
           filena(namelen+1:namelen+7) = '.o2s'
           command(4:3+namelen+4) = filena
           lentmp = 3+namelen+4+1  
           namelen =  strlen(output_file_prefix)
           filena = output_file_prefix
           filena(namelen+1:namelen+7) = '.o2s'
           command(lentmp+1:lentmp+namelen+5) = filena
           write(scr_unit,*)command
           call system(command)         
          
         end if

         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.o2s'

         if (o2sout) open(unit=snopt_unit,file=filena,status='unknown', 

     &   form='formatted')

         !-- Open file for storing function and gradient evaluations
         !-- used for SNOPT restarts

         if (opt_restart) then
            command = 'cp '      
            namelen =  strlen(restart_file_prefix)
            filena = restart_file_prefix
            filena(namelen+1:namelen+7) = '.snfg'
            command(4:3+namelen+5) = filena
            lentmp = 3+namelen+5+1  
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+7) = '.snfg'
            command(lentmp+1:lentmp+namelen+5) = filena
            call system(command)           
         end if

         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+7) = '.snfg'
         open(unit=snfg_unit,file=filena,status='unknown',
     &    form='unformatted')
      
      end if

c--------------------------------------------
c     Optimization files
c--------------------------------------------

      if ((opt_meth .ge. 2 .and. opt_meth .le. 4 )

     &  .or. (opt_meth.eq.8) .or. (opt_meth.eq.11)) then

      !-- Open optimization output file --
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.opt'
         open(unit=opt_unit,file=filena,status='new')

         if (.not.periodic .and. .not. sbbc) then

            !-- read b-spline info file -- 

            namelen =  strlen(grid_file_prefix)
            filena = grid_file_prefix
            filena(namelen+1:namelen+6) = '.bsp'

            if (wt_update) then
      
               namelen =  strlen(dirname)
               filena = dirname
               filena(namelen+1:namelen+6) = '.bsp'
            
            end if

            open( unit=bdef_unit, file=filena, iostat=ios, err=1000,
     &       status='old' )

            read (bdef_unit,*) jbsord
            read (bdef_unit,*) nc
            read (bdef_unit,*) jbody2
            jbody = jtail2 - jtail1 + 1

#ifdef _MPI_VERSION        
            if (rank.eq.0) then
#endif    
               if (jbody .gt. ibody .or. jbody2 .gt. ibody) then
                  write (scr_unit,*) 
     &            '** Check OPTCOM.INC: ibody too small!! **'
                  stop
               end if
               if (jbody.ne.jbody2) then
                  write (scr_unit,*) '** Problem with input!! **'
                  stop
               end if
#ifdef _MPI_VERSION               
            end if               
#endif

            !-- Read design variables file --

            namelen =  strlen(grid_file_prefix)
            filena = grid_file_prefix
            filena(namelen+1:namelen+5) = '.dvs'

            if (wt_update) then
    
               namelen =  strlen(dirname)
               filena = dirname
               filena(namelen+1:namelen+5) = '.dvs'

            end if

            open ( unit=dvs_unit, file=filena, iostat=ios, err=1000,
     &       status='old' )

 1000       continue
            ckfile = .false.
            if (ios.ne.0) then
               write(opt_unit,*) 'files ".dvs or .bsp" not found!'
               if (opt_meth.eq.4) then
                  ckfile = .true.
               else
                  stop
               end if
            endif

         end if !.not.periodic .and. .not. sbbc


c     -- pressure distribution 'cp_tar' file if necessary--
         if (obj_func.eq.1 .or. obj_func.eq.10 ) then
            filena = grid_file_prefix
            filena(namelen+1:namelen+7) = '.cptar'
            open ( unit=cptar_unit, file=filena, status='unknown' )
         end if

         if (opt_meth.gt.2) then 

            !-- Open the design point objective function convergence 
            !-- history file. Each processor will have its own .ohis file
            !-- e.g. rest##-00.ohis, rest##-01.ohis, rest##-02.ohis for 
            !-- processors of rank 0,1,2, etc...

            if (mpopt.gt.1) then

              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix)
                 filena = restart_file_prefix
                 filena(namelen+1:namelen+6) = '.ohis'
                 command(4:3+namelen+5) = filena
                 lentmp = 3+namelen+5+1  
                 namelen =  strlen(output_file_prefix)
                 filena = output_file_prefix
                 filena(namelen+1:namelen+6) = '.ohis'
                 command(lentmp+1:lentmp+namelen+6) = filena

                 call system(command)           
              end if

              namelen = strlen(output_file_prefix)
              filena = output_file_prefix
              filena(namelen+1:namelen+8) = '.ohis'
              if (ohisout) open(unit=n_mpo,file=filena,status='unknown',
     &        access='append')

            end if ! (mpopt.gt.1)

c       *********AOA Sweep History From Weight Update Procedure *********

            if (opt_restart) then
               command = 'cp '      
               namelen =  strlen(restart_file_prefix)
               filena = restart_file_prefix
               filena(namelen+1:namelen+4) = '.aoa'
               command(4:3+namelen+4) = filena
               lentmp = 3+namelen+4+1  
               namelen =  strlen(output_file_prefix)
               filena = output_file_prefix
               filena(namelen+1:namelen+4) = '.aoa'
               command(lentmp+1:lentmp+namelen+6) = filena
               call system(command)           
            end if

            namelen = strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+4) = '.aoa'

            if (aoaout) open(unit=aoa_unit,file=filena,status='unknown')

            !-- Open function & gradient evaluation time output file
            !-- (Chad Oldfield)
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+6) = '.fgt'
            if (fgtout) open(unit=fgt_unit,file=filena,status='new')
            !-----------------------------------------------------------
            !----- Open output files common to all design points -------
            !-----------------------------------------------------------

#ifdef _MPI_VERSION
            if (rank.eq.0) then
#endif

               !-- Open the composite objective function convergence 
               !-- history file           

               if (opt_restart) then
                  command = 'cp '      
                  namelen =  strlen(restart_file_prefix0)
                  filena = restart_file_prefix0
                  filena(namelen+1:namelen+6) = '.ohis'
                  command(4:3+namelen+5) = filena
                  lentmp = 3+namelen+5+1  
                  namelen =  strlen(output_file_prefix0)
                  filena = output_file_prefix0
                  filena(namelen+1:namelen+6) = '.ohis'
                  command(lentmp+1:lentmp+namelen+6) = filena

                  call system(command)           
               end if

               if (opt_meth .eq. 11) then
                  output_file_prefix0 = output_file_prefix
               endif

               namelen = strlen(output_file_prefix0)
               filena = output_file_prefix0
               filena(namelen+1:namelen+6) = '.ohisc' !-- note ohisC
               open(unit=ohis_unit,file=filena,
     &            status='unknown', access='append')

               !-- Open the weighted integral objective function 
               !-- history file           
               if (wt_int) then
                  if (opt_restart) then
                     command = 'cp '      
                     namelen =  strlen(restart_file_prefix0)
                     filena = restart_file_prefix0
                     filena(namelen+1:namelen+6) = '.wtint'
                     command(4:3+namelen+5) = filena
                     lentmp = 3+namelen+5+1  
                     namelen =  strlen(output_file_prefix0)
                     filena = output_file_prefix0
                     filena(namelen+1:namelen+6) = '.wtint'
                     command(lentmp+1:lentmp+namelen+6) = filena
                     
                     call system(command)           
                  end if
                  
                  if (opt_meth .eq. 11) then
                     output_file_prefix0 = output_file_prefix
                  endif
                  
                  namelen = strlen(output_file_prefix0)
                  filena = output_file_prefix0
                  filena(namelen+1:namelen+6) = '.wtint'
                  open(unit=wtint_unit,file=filena,
     &                 status='unknown', access='append')
               end if
        
               !-- Open radius of curvature constraint output file 
               !-- (Chad Oldfield)

               if (opt_restart) then
                  command = 'cp '      
                  namelen =  strlen(restart_file_prefix0)
                  filena = restart_file_prefix0
                  filena(namelen+1:namelen+6) = '.rccon'
                  command(4:3+namelen+6) = filena
                  lentmp = 3+namelen+6+1  
                  namelen =  strlen(output_file_prefix0)
                  filena = output_file_prefix0
                  filena(namelen+1:namelen+6) = '.rccon'
                  command(lentmp+1:lentmp+namelen+6) = filena

                  call system(command)           
               end if
               namelen =  strlen(output_file_prefix0)
               filena = output_file_prefix0
               filena(namelen+1:namelen+6) = '.rccon'
               open(unit=rccon_unit,file=filena,status='unknown',
     &         access='append')

c     -- open history file for L2 norm of composite gradient vector --
               if (opt_restart) then
                  command = 'cp '      
                  namelen =  strlen(restart_file_prefix0)
                  filena = restart_file_prefix0
                  filena(namelen+1:namelen+6) = '.ghis'
                  command(4:3+namelen+5) = filena
                  lentmp = 3+namelen+5+1  
                  namelen =  strlen(output_file_prefix0)
                  filena = output_file_prefix0
                  filena(namelen+1:namelen+6) = '.ghis'
                  command(lentmp+1:lentmp+namelen+6) = filena

                  call system(command)           
              end if
              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+6) = '.ghis'
              open(unit=ghis_unit,file=filena,status='unknown',
     &          access='append')

c     -- open design variable history file --   
              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+7) = '.dvhis'
                 command(4:3+namelen+6) = filena
                 lentmp = 3+namelen+6+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+7) = '.dvhis'
                 command(lentmp+1:lentmp+namelen+7) = filena

                 call system(command)           
              end if
              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+6) = '.dvhis'
              open(unit=dvhis_unit,file=filena,status='unknown',
     &        access='append')

c     -- open history file for composite gradient vector --

              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+7) = '.gvhis'
                 command(4:3+namelen+6) = filena
                 lentmp = 3+namelen+6+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+7) = '.gvhis'
                 command(lentmp+1:lentmp+namelen+7) = filena

                 call system(command)           
              end if
              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+7) = '.gvhis'
              open(unit=gvhis_unit,file=filena,status='unknown',
     &        access='append')

c     -- open initial function value file --

              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+5) = '.obj'
                 command(4:3+namelen+4) = filena
                 lentmp = 3+namelen+4+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+5) = '.obj'
                 command(lentmp+1:lentmp+namelen+5) = filena

                 call system(command)           
              end if
              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+5) = '.obj'
              open(unit=obj_unit,file=filena,status='unknown',
     &        access='append')

c     -- open airfoil geometry file --

              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+5) = '.ac'
                 command(4:3+namelen+4) = filena
                 lentmp = 3+namelen+4+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+5) = '.ac'
                 command(lentmp+1:lentmp+namelen+5) = filena

                 call system(command)           
              end if
              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+5) = '.ac'
              if (acout) open(unit=ac_unit,file=filena,status='unknown',
     &        access='append')

c     -- open best geometry file, Amber2d format --

              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+7) = '.best'
                 command(4:3+namelen+5) = filena
                 lentmp = 3+namelen+5+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+7) = '.best'
                 command(lentmp+1:lentmp+namelen+5) = filena

                 call system(command)           
              end if

              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+7) = '.best'

              if (bestout)open(unit=n_best,file=filena,status='unknown',
     &        access='append')

c     -- open b-spline control points file --

              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+5) = '.bcp'
                 command(4:3+namelen+4) = filena
                 lentmp = 3+namelen+4+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+5) = '.bcp'
                 command(lentmp+1:lentmp+namelen+5) = filena

                 call system(command)           
              end if

              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+6) = '.bcp'

              if (bcout) open(unit=bc_unit,file=filena,status='unknown',
     &        access='append')  

c     -- open search vector history file (H.Buckley) --

              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+7) = '.svhis'
                 command(4:3+namelen+6) = filena
                 lentmp = 3+namelen+6+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+7) = '.svhis'
                 command(lentmp+1:lentmp+namelen+5) = filena

                 call system(command)           
              end if

              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+7) = '.svhis'
              open(unit=svhis_unit,file=filena,status='unknown',
     &        access='append')

c     -- open finite difference hessian file --

              if(hessian) then
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+6) = '.hes'
                 open(unit=hes_unit,file=filena,status='new')      
              endif

c     -- open estimated hessian file --

c     This should be uncommented to leave if hessian, however
c     This will ensure that the BFGS estimate of the Hessian
C     Is alway printed

              if(.true.) then
c             if(hessian) then
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+7) = '.hest'

                 if (hestout) open(unit=hest_unit,file=filena,
     &              status='new') 
              endif

c     -- open design variables output file

              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+5) = '.dvs'
                 command(4:3+namelen+4) = filena
                 lentmp = 3+namelen+4+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+5) = '.dvs'
                 command(lentmp+1:lentmp+namelen+5) = filena

                 call system(command)           
              end if

              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+6) = '.dvs'

              if (dvsout) open(unit=dvsr_unit,file=filena,
     &           status='unknown', access='append')

c     -- open b-spline control points output file
              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+5) = '.bsp'
                 command(4:3+namelen+4) = filena
                 lentmp = 3+namelen+4+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+5) = '.bsp'
                 command(lentmp+1:lentmp+namelen+5) = filena

                 call system(command)           
              end if

              namelen =  strlen(output_file_prefix0)
              filena = output_file_prefix0
              filena(namelen+1:namelen+6) = '.bsp'

              if (bspout) open(unit=bspr_unit,file=filena,
     &           status='unknown', access='append')

c     -- output for thickness constraints --
              if (opt_restart) then
                 command = 'cp '      
                 namelen =  strlen(restart_file_prefix0)
                 filena = restart_file_prefix0
                 filena(namelen+1:namelen+6) = '.tcon'
                 command(4:3+namelen+5) = filena
                 lentmp = 3+namelen+5+1  
                 namelen =  strlen(output_file_prefix0)
                 filena = output_file_prefix0
                 filena(namelen+1:namelen+6) = '.tcon'
                 command(lentmp+1:lentmp+namelen+6) = filena

                 call system(command)           
               end if

               namelen =  strlen(output_file_prefix0)
               filena = output_file_prefix0
               filena(namelen+1:namelen+7) = '.tcon'
               open(unit=tcon_unit,file=filena,status='unknown',
     &          access='append')


c     -- output pareto information --
               if (obj_func.eq.6) then
                  if (opt_restart) then
                     command = 'cp '      
                     namelen =  strlen(restart_file_prefix0)
                     filena = restart_file_prefix0
                     filena(namelen+1:namelen+5) = '.par'
                     command(4:3+namelen+4) = filena
                     lentmp = 3+namelen+4+1  
                     namelen =  strlen(output_file_prefix0)
                     filena = output_file_prefix0
                     filena(namelen+1:namelen+5) = '.par'
                     command(lentmp+1:lentmp+namelen+5) = filena

                     call system(command)           
                  end if

                  namelen =  strlen(output_file_prefix0)
                  filena = output_file_prefix0
                  filena(namelen+1:namelen+7) = '.par'
                  open(unit=par_unit,file=filena,status='unknown',
     &             access='append')
               end if

#ifdef _MPI_VERSION
            end if ! (rank.eq.0)
#endif

            !-- Open gmres output file --
            filena = output_file_prefix
            filena(namelen+1:namelen+7) = '.gmres'

            if (gmresout) open(unit=gmres_unit,file=filena,
     &         status='unknown', access='append') 

            !-- output pareto information --
            if (opt_meth.eq.4) then
              obj_func = 6
              wfl = 1.0
              wfd = 0.0
              wtc = 0.0
            end if   

            !-- normalize weights --

            objwt=0.0
            
            do i=1,mpopt
              objwt = objwt + wmpo(i)
            end do

            do i=1,mpopt
               wmpo1(i) = wmpo(i)/1.d0 ! only used for wt_int output file
               wmpo2(i) = wmpo2(i)/1.d0! only used for wt_int output file
               wmpo(i) = wmpo(i)/objwt ! Actual weights used in composite
                                       ! objective function  
            end do

            write (opt_unit,501) mpopt
 501        format(3x,'Number of design points:',i4)
           
            write (opt_unit,*)
            write (opt_unit,502)
 502        format(3x,'Design Point',5x,'Weight',5x,'Mach Number',5x
     &          ,'Cl*',6x,'Cd*',9x,'Re',8x,'ALPHA')
            do i=1,mpopt
               write(opt_unit,503) i, wmpo(i), fsmachs(i), cltars(i),
     &             cdtars(i), reno(i), alphas(i)
            end do

 503        format(7x,i4,7x,f8.4,5x,f8.3,4x,f8.4,1x,e11.4,1x,e11.4
     &               ,f9.4)

         end if !opt_meth .gt.2
         
         !-- Write information about the optimization run to the .opt
         !-- output file

         write (opt_unit,5)
 5       format(//10x, 'Optimization Parameters:',/)


         if (warm_start) then
         
            write(opt_unit,401)
 401        format(3x, 'Flow solver warm-starts enabled',/)

         else

            write(opt_unit,402)
 402        format(3x, 'Flow solver warm-starts disabled',/)

         end if

         if (sol_his) then
         
            write(opt_unit,403)
 403        format(3x, 'Flow solution logging enabled',/)

         else

            write(opt_unit,404)
 404        format(3x, 'Flow solution logging disabled',/)

         end if

         if (grid_his) then
          
            write(opt_unit,405)
 405        format(3x, 'Grid logging enabled',/)
         else

            write(opt_unit,406)
 406        format(3x, 'Grid logging disabled',/)

         end if

         if (flo_his) then
          
            write(opt_unit,407)
 407        format
     &      (3x, 'Flow solver convergence history logging enabled',/)
         else

            write(opt_unit,408)
 408        format
     &      (3x, 'Flow solver convergence history logging disabled',/)

         end if

         if (cpp_his) then
          
            write(opt_unit,409)
 409        format
     &      (3x, 'Pressure coefficient history logging enabled',/)
         else

            write(opt_unit,410)
 410        format
     &      (3x, 'Pressure coefficient history logging disabled',/)

         end if

         if (cf_his) then
          
            write(opt_unit,411)
 411        format
     &      (3x, 'Skin Friction coefficient history logging enabled',/)
         else

            write(opt_unit,412)
 412        format
     &      (3x, 'Skin Friction coefficient history logging disabled',/)

         end if

         if ( obj_func.eq.0 ) then
           write (opt_unit,6)
 6         format(3x, 'Objective function: mean drag minimization.'/)

         else if ( obj_func.eq.1 ) then
           write (opt_unit,7)
 7         format(3x, 'Objective function: inverse design.'/)

         else if ( obj_func.eq.2 ) then
           write (opt_unit,8)
 8         format(3x, 'Objective function: drag minimization.', /3x,
     &          'Cl is constrained by CYCLONE.')
           if (clalpha) then
             write (opt_unit,8005) clinput, cltol
 8005        format (3x,'Constrained Cl:',e18.10,/3x,'Cl tol.:',e12.2)
           else
             write(scr_unit,8010) 
 8010        format (3x,'CLALPHA must be turned on!!')
             stop
           end if

         else if ( obj_func.eq.3 ) then
           write (opt_unit,9) cd_tar, cl_tar
 9         format(3x,
     &          'Objective function: wfd*(Cd-Cd*)**2 + wfl*(Cl-Cl*)**2',
     &          /3x,'Target Cd:',f6.3,/3x,'Target Cl:',f9.6/)  

         else if ( obj_func.eq.4 ) then
           write (opt_unit,905)
 905       format(3x,'Objective function: maximize Cl')

         else if ( obj_func.eq.5 ) then
           write (opt_unit,910)
 910       format(3x,'Objective function: maximize Cl/Cd')

         else if ( obj_func.eq.6 ) then

           write (opt_unit,915) cd_tar, cl_tar
 915       format(3x,
     &          'Objective function: w1*(1-Cd/Cd*)^2 + w2*(1-Cl/Cl*)^2',
     &          /25x,' + w3*(1-t/t*)^2 + ... ',//3x,'Cd*=',e12.5,
     &          ' and Cl*=',e12.5/)

         else if ( obj_func.eq.7 ) then

           write (opt_unit,916) cd_tar, cl_tar
 916       format(3x,
     &          'Objective function: w1*(1-Cm/Cm*)^2 + w2*(1-Cl/Cl*)^2',
     &          /25x,' + w3*(1-t/t*)^2 + ... ',//3x,'Cm*=',e12.5,
     &          ' and Cl*=',e12.5/)

         else if ( obj_func.eq.8 ) then
            if (wt_int) then
               write (opt_unit,9172)
 9172          format
     &         (3x,'Objective function: Integral of Cd over range of',
     &         /3x,'operating conditions')
               write(opt_unit,9173) wtlow, wtup
 9173          format(/6x, 'Aircraft Weight Range: ',f8.1,' - ',f8.1)
               write(opt_unit,9174) delwt
 9174          format(6x, 'Aircraft Weight Interval Spacing: ',f8.2)
               write(opt_unit,9175) mlow, mup
 9175          format(/6x, 'Cruise Mach # Range: ',f4.2,' - ',f4.2)
               write(opt_unit,9176) delm
 9176          format(6x, 'Cruise Mach # Interval Spacing: ',f6.4)
               write(opt_unit,9177) altlow, altup
 9177          format(/6x, 'Cruise Altitude Range: ',f8.1,' - ',f8.1)
               write(opt_unit,9178) delalt
 9178          format(6x, 'Cruise Altitude Interval Spacing: ',f8.2,/)
            else
               write (opt_unit,917)
 917           format
     &     (3x,'Objective function: Lift-constrained drag minimization')
               write (opt_unit,9171)
 9171          format
     &              (3x,'Lift target: cl_tar',/)
            end if

         else if ( obj_func.eq.9 ) then
           write (opt_unit,*)'  Objective function: inverse' 
     &         // ' remote design gridshape.'

         else if ( obj_func.eq.10 ) then
           write (opt_unit,*) '  Objective function: inverse remote' 
     &         // ' design FWH.' 

         else if ( obj_func.eq.11 ) then
           write (opt_unit,*) '  Objective function: total radiated' 
     &     // ' acoustic power.' 

         else if ( obj_func.eq.12 ) then
           write (opt_unit,*) '  Objective function: inverse remote' 
     &     // ' design boxshape.'

         else if ( obj_func.eq.13 ) then
           write (opt_unit,913)
 913       format(3x, 'Objective function: vorticity minimization.') 

         else if ( obj_func.eq.14 ) then
           write (opt_unit,*) '  Objective function:' 
     &    // ' minimize far-field pressure fluctutation.'

         else if ( obj_func.eq.15 ) then

#ifdef _MPI_VERSION
           write (opt_unit,918) c_low1(rank+1)
#else
           write (opt_unit,918) c_low1(1)
#endif

 918       format(3x,'Off-design constraint function:',
     &          /3x,'Lift >= ',f9.6/ )

         else if ( obj_func.eq.16 ) then

#ifdef _MPI_VERSION
            write (opt_unit,920) c_upp1(rank+1)
#else
            write (opt_unit,920) c_upp1(1)
#endif

 920        format(3x,'Off-design constraint function:',
     &              /3x,'Max Mach Number <= ',f9.6/)

         else if ( obj_func.eq.17 ) then
#ifdef _MPI_VERSION
            write (opt_unit,921) c_upp1(rank+1)
#else
            write (opt_unit,921) c_upp1(1)
#endif

            write (opt_unit,922) Mstar

 921        format(3x,'Off-design constraint function:',
     &              /3x,'KS Max Mach Number <= ',f9.6/)
 922        format(3x,'Actual Max Mach Number <= ',f9.6/)

         else if ( obj_func.eq.18 ) then
            write (opt_unit,923)
 923        format(3x,'Objective function:',
     &              /3x,'Lift maximization',/)

         else if ( obj_func.eq.19 ) then
            write (opt_unit,924)
 924        format(3x,'Objective function: Lift-constrained drag min',/)

         else if ( obj_func.eq.20 ) then
            write (opt_unit,925)
 925        format
     &      (3x,'Objective function: Inverse aircraft range factor',/)

         else if ( obj_func.eq.21 ) then
            write (opt_unit,926)
 926        format
     &    (3x,'Objective function: Inverse aircraft endurance factor',/)

         else
           write (scr_unit,*) 'INPUT: obj_func wrong value!!', obj_func
           stop  
         end if  !output objective function

c     error check for constraints

        if ( ( opt_meth.gt.2.and. obj_func.gt.4) .or.
     &       (opt_meth==2 .and. .not.periodic .and. .not.sbbc) ) then

           if (ntcon .lt. 0) then
              write(scr_unit,*) 
     &        'INPUT.f: Negative # of thickness constraints encountered'
              stop
           else if (ntcon .gt. maxtcon ) then
              write(scr_unit,*) 
     &        'INPUT.f: Too many constraints, check maxtcon'
              stop
           endif

           if (nrtcon .lt. 0) then
              write(scr_unit,*) 'INPUT.f: Negative # of range thickness
     &             constraints encountered'
              stop
           else if (nrtcon .gt. nthc ) then
              write(scr_unit,*) 
     &        'INPUT.f: Too many constraints, check nthc'
              stop
           endif

c          Radius of curvature constraints (Chad Oldfield)
           if (nrc .lt. 2) then
              write(scr_unit,*) 'INPUT.f: nrc must be >= 2'
              stop
           else if (nrc .gt. nrc_max) then
              write(scr_unit,*) 'INPUT.f: nrc must be <= ', nrc_max
              stop
           endif
           do i = 1, nrc-1
             if (xrc_tar(i) > xrc_tar(i+1)) then
               write(scr_unit,*) 'INPUT.f: xrc_tar must be sorted in',
     |            ' increasing order.'
               stop
             endif
           enddo

        end if  !constraint error check
    

c     Optimization method output and error checking

        if ( opt_meth.eq.2 .and. .not. periodic) then
           write (opt_unit,10)
 10        format(3x,'Unsteady BFGS Optimization for Airfoils.')

        else if ( opt_meth.eq.2 .and. periodic) then
           write (opt_unit,115)
 115       format(3x,'Unsteady BFGS Optimization for Cylinders.')
           
        else if ( opt_meth.eq.3 ) then 
           write (opt_unit,11)
 11        format(3x,'Unconstrained BFGS optimization.')
           write(opt_unit,111)
 111       format(/5x,'Flow Solver Tolerance',5x,'Gradient Tolerance',
     &          5x,'Max. Iter.')
           write(opt_unit,112) min_res,eps_gmres,opt_iter
 112       format(6x,e12.3,12x,e12.3,14x,i4)

        else if ( opt_meth.eq.4 ) then 
           write (opt_unit,113)
 113       format(3x,'M vs. Cd sweep with BFGS.')

        else if ( opt_meth.eq.5 ) then 
           write (opt_unit,123)
 123       format(3x,'Angle of attack sweep.')
        else if ( opt_meth.eq.6 ) then 
           write (opt_unit,114)
 114       format(3x,'M vs. Cd sweep with aoa variable in flow solver.')
        else if ( opt_meth.eq.7 ) then 
           write (opt_unit,154)
 154       format(3x,'Error estimation and time step adaptation.')
        else if ( opt_meth.eq.8 ) then 
           write (opt_unit,155)
 155       format(3x,'Constrained optimization using SNOPT')
        end if
        
        if ( gradient.eq.0 ) then
          write (opt_unit,12) 
 12       format (/3x,"Finite diff's are used to calculate gradient.")
        else if ( gradient.eq.1 ) then
          write (opt_unit,14)
 14       format (/3x,'Adjoint method is used to calculate gradient.')
        else if ( gradient.eq.2 ) then
          write (opt_unit,16)
 16       format (/3x,
     &          'Matrix sensitivity method is used to',/3x,
     &          'calculate gradient.')
        else if ( gradient.eq.3 ) then
          write (opt_unit,18)
 18       format (/3x,
     &          'Matrix-free sensitivity method is used to',/3x,
     &          'calculate gradient.')
        else if ( gradient.eq.4 ) then
          write (opt_unit,20)
 20       format (/3x,
     |        'Augmented adjoint method is used to calculate gradient.') 
        else 
          write (opt_unit,*) '** input.f: error in gradient!!! **'
          stop
        end if

c       (Chad Oldfield)
        if (opt_meth == 3 .or. 
     &       (opt_meth==2 .and. .not.periodic .and. .not.sbbc)) then
           if (incr == 0) then
              write (opt_unit,'(/3x,A)') 'Algebraic grid perturbation'
           else
              write (opt_unit,'(/3x,A)') 'Elasticity grid perturbation'
              if (incr <= 0) then
                 write(scr_unit,*) 'INPUT.f: incr must be > 0'
                 stop
              elseif (incr > maxincr) then
                 write(scr_unit,*) 'INPUT.f: incr must be <= ', maxincr
                 stop
              endif
              if (gpmaxit <= 0) then
                 write(scr_unit,*) 'INPUT.f: gpmaxit must be > 0'
                 stop
              endif
              if (gamaxit <= 0) then
                 write(scr_unit,*) 'INPUT.f: gamaxit must be > 0'
                 stop
              endif
           endif
        endif  !opt_meth == 3 .or. opt_meth == 2

        if (obj_func.eq.2 .and. gradient.ne.0) then
          write(scr_unit,21)
 21       format (3x,'For drag minimization at fixed lift',/3x,
     &          'you must use finite-difference gradients!!!')
          stop
        end if

        if ((opt_meth.eq.2 .or. opt_meth.eq.7) .and. .not. outtime) then
           write(scr_unit,*) 'For unsteady optimization you must set'
           write(scr_unit,*) '            OUTTIME = TRUE'
           stop
        end if

        if (cdf) then
          write (opt_unit,22) fd_eta
 22       format (3x,'Centered-differences are used with stepsize:',
     &          e12.2)
        else 
          write (opt_unit,24) fd_eta
 24       format (3x,'Forward-differences are used with stepsize:',
     &          e12.2)
        end if

        if ( gradient.ne.0 ) then
          write (opt_unit,26)
          write (opt_unit,28) pdc
 26       format (3x,'First-order Jacobian used as preconditioner.')
 28       format (3x,'Dissipation constant pdc:', f6.2)
        end if

        if ( inord.gt.2 ) then
          write (opt_unit,*) '** Error in inord input!!! **'
          stop
        end if

        if ( ireord.lt.0 .or. ireord.gt.2) then
          write (opt_unit,*) '** Error in ireord input!!! **'
          stop
        end if

        if (coef_frz) then
          write (opt_unit,34) 
 34       format (/3x,'FROZEN COEFFICIENTS'/)
        end if
      
      end if ! ((opt_meth .ge. 2 .and. opt_meth .le. 4 )

c--------------------------------------
c     Flow solver files
c--------------------------------------

      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+3) = '.g'
      namelen = namelen+2

      if (iread.eq.1) then

        if (wt_update) then
      
           namelen =  strlen(dirname)
           filena = dirname
           filena(namelen+1:namelen+3) = '.g'
           
        end if

        open(unit=grid_unit,file=filena,status='old',form='formatted')

      endif

      if (iread.eq.2) then

        if (wt_update) then
      
           namelen =  strlen(dirname)
           filena = dirname
           filena(namelen+1:namelen+3) = '.g'

        end if

        open(unit=grid_unit,file=filena,status='old',form='unformatted')

      endif
c
      namelen =  strlen(output_file_prefix)
      if (iread.eq.0) then
        filena = output_file_prefix
        filena(namelen+1:namelen+8) = '.cylgrd'
        open(unit=cylgrd_unit,file=filena,form='unformatted')
      endif
      if (writeresid) then
        filena = output_file_prefix
        filena(namelen+1:namelen+5) = '.res'
        open(unit=res_unit,file=filena,status='new',form='unformatted')
      endif
      if (writeturb) then
        filena = output_file_prefix
        filena(namelen+1:namelen+3) = '.t'
        open(unit=turb_unit,file=filena,status='new',form='unformatted')
      endif
      if (flbud) then
        filena = output_file_prefix
        filena(namelen+1:namelen+5) = '.bud'
        open(unit=bud_unit,file=filena,status='new')
      endif
      if (timing) then
        filena = output_file_prefix
        filena(namelen+1:namelen+6) = '.time'
        open(unit=time_unit,file=filena,status='new')
      endif
      if (sknfrc) then
        filena = output_file_prefix
        filena(namelen+1:namelen+3) = '.cf'
        open(unit=cf_unit,file=filena,status='new')
      endif
      if (unsted) then
        filena = output_file_prefix
        filena(namelen+1:namelen+4) = '.q2'
        open(unit=q2_unit,file=filena,status='new',form='unformatted')
      endif
      if (write_blprop) then
        filena = output_file_prefix
        filena(namelen+1:namelen+9) = '.blprop'
        open(unit=blprop_unit,file=filena,status='new')
      endif
      if (write_blvel) then
        filena = output_file_prefix
        filena(namelen+1:namelen+8) = '.blvel'
        open(unit=blvel_unit,file=filena,status='new')
      endif
      if (write_tp) then
        filena = output_file_prefix
        filena(namelen+1:namelen+4) = '.tp'
        open(unit=tp_unit,file=filena,status='new')
        filena = output_file_prefix
        filena(namelen+1:namelen+7) = '.dettp'
        open(unit=dettp_unit,file=filena,status='new')
      endif
      if (gnuplot_res) then
        filena = output_file_prefix
        filena(namelen+1:namelen+5) = '.gnu'
        open(unit=gnuplot_res_unit,file=filena,status='unknown',access
     &       ='append')
      endif

      filena = output_file_prefix
      filena(namelen+1:namelen+3) = '.q'
      open(unit=q_unit,file=filena,status='new',form='unformatted')

      filena = output_file_prefix
      filena(namelen+1:namelen+3) = '.m'
      if (mout) open(unit=mach_unit,file=filena,status='new',
     &   form='formatted')
      filena = output_file_prefix
      filena(namelen+1:namelen+6) = '.solv'
      open(unit=out_unit,file=filena,status='new',form='formatted')

      filena = output_file_prefix
      filena(namelen+1:namelen+4) = '.cp'
      if (cpout) open(unit=cp_unit,file=filena,status='new')

      filena = output_file_prefix
      filena(namelen+1:namelen+4) = '.ld'
      if (ldout) open(unit=ld_unit,file=filena,status='new')

c     -- input and output for M vs. Cd runs, alpha polars, or cl polars  --
      if ((opt_meth.ge.4 .and. opt_meth.le.6) .or. opt_meth.eq.10
     &     .or. opt_meth.eq.11) then
#ifdef _MPI_VERSION
         if(rank.eq.0) then
#endif
            namelen =  strlen(output_file_prefix0)
            filena = output_file_prefix0
            filena(namelen+1:namelen+5) = '.pol'
            open(unit=op_unit,file=filena,status='unknown',
     &        access='append')

            namelen =  strlen(output_file_prefix0)
            filena = output_file_prefix0
            filena(namelen+1:namelen+7) = '.cphis'
            open(unit=n_cphis,file=filena,status='new',form='formatted')
         
            namelen =  strlen(output_file_prefix0)
            filena = output_file_prefix0
            filena(namelen+1:namelen+7) = '.cfhis'
            open(unit=n_cfhis,file=filena,status='new',form='formatted')
         
            namelen =  strlen(grid_file_prefix)
            filena = grid_file_prefix
            filena(namelen+1:namelen+6) = '.pol'
            open( unit=ip_unit, file=filena, status='unknown' )
#ifdef _MPI_VERSION
         end if ! (rank.eq.0)
#endif

      end if !(opt_meth.ge.4 .and. opt_meth.le.6)

#ifdef _MPI_VERSION
      if(rank.eq.0) then
#endif
         if (grdseq_rest .and. restart) then
            write(out_unit,*) ' Error in Input'
            write(out_unit,*)
     &         ' GRDSEQ_REST and RESTART cannot both be TRUE'
            stop
         endif
#ifdef _MPI_VERSION
      end if !(rank.eq.0)
#endif

c---------------------------------------------
c     Copy history files for restarts
c---------------------------------------------
      if (restart .or. opt_restart .or. warm1st .or. postProcessOnly)
     &     then
        namelen =  strlen(restart_file_prefix)
        filena = restart_file_prefix
        filena(namelen+1:namelen+3) = '.q'
        open(unit=rq_unit,file=filena,status='old',form='unformatted')

      end if

      if (restart .or. opt_restart) then

        namelen =  strlen(restart_file_prefix)

        if (unsted) then
          filena = restart_file_prefix
          filena(namelen+1:namelen+4) = '.q2'
          open(unit=rq2_unit,file=filena,status='old',
     &          form='unformatted')
        endif
c
c       *********Convergence History **********
        filena_restart = restart_file_prefix
        filena_restart(namelen+1:namelen+5) = '.his'
c
        namelen =  strlen(output_file_prefix)
        filena = output_file_prefix
        filena(namelen+1:namelen+5) = '.his'
c
        command = 'cp '
        namelen = strlen(filena_restart)
        command(4:3+namelen) = filena_restart
c       -length of 'cp ' is 3, length of filena_restart is namelen
c       -add 1 for blank space at end of 'cp filena_restart' 
        lentmp = namelen+3+1
        namelen = strlen(filena)
        command(lentmp+1:lentmp+namelen) = filena
        call system(command)
        open(unit=his_unit,file=filena,status='unknown',access='append')

c       ********* Design Point Gradient History **********
        filena_restart = restart_file_prefix
        filena_restart(namelen+1:namelen+7) = '.gvhis'
c
        namelen =  strlen(output_file_prefix)
        filena = output_file_prefix
        filena(namelen+1:namelen+7) = '.gvhis'
c
        command = 'cp '
        namelen = strlen(filena_restart)
        command(4:3+namelen) = filena_restart
c       -length of 'cp ' is 3, length of filena_restart is namelen
c       -add 1 for blank space at end of 'cp filena_restart' 
        lentmp = namelen+3+1
        namelen = strlen(filena)
        command(lentmp+1:lentmp+namelen) = filena
        write (*, *) command !RR: debug
        
        call system(command)
        open(unit=dpgrad_unit,file=filena,status='unknown'
     &       ,access='append')

c       ****** Dissipation-based Continuation output *******
        if (dissCon) then
          filena_restart = restart_file_prefix
          filena_restart(namelen+1:namelen+5) = '.dbc'
c
          namelen =  strlen(output_file_prefix)
          filena = output_file_prefix
          filena(namelen+1:namelen+5) = '.dbc'

          command = 'cp '
          namelen = strlen(filena_restart)
          command(4:3+namelen) = filena_restart
c       -length of 'cp ' is 3, length of filena_restart is namelen
c       -add 1 for blank space at end of 'cp filena_restart' 
          lentmp = namelen+3+1
          namelen = strlen(filena)
          command(lentmp+1:lentmp+namelen) = filena
          call system(command)
          open
     &    (unit=dbc_unit,file=filena,status='unknown',access='append')
        end if
c
c       **********Trubulence History***********        
        if (viscous .and. itmodel.eq.2) then
          namelen = strlen(restart_file_prefix)
          filena_restart = restart_file_prefix
          filena_restart(namelen+1:namelen+6) = '.this'
c     
          namelen =  strlen(output_file_prefix)
          filena = output_file_prefix
          filena(namelen+1:namelen+6) = '.this'
c
          command = 'cp '
          namelen = strlen(filena_restart)
          command(4:3+namelen) = filena_restart
          lentmp = namelen+3+1
          namelen = strlen(filena)
          command(lentmp+1:lentmp+namelen) = filena
          call system(command)

          if (thisout) open(unit=turbhis_unit,file=filena,
     &       status='unknown', access='append')
        endif
c
c       **********Subiteration History***********        
        if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5) then
          namelen = strlen(restart_file_prefix)
          filena_restart = restart_file_prefix
          filena_restart(namelen+1:namelen+4) = '.si'
c     
          namelen =  strlen(output_file_prefix)
          filena = output_file_prefix
          filena(namelen+1:namelen+4) = '.si'
c
          command = 'cp '
          namelen = strlen(filena_restart)
          command(4:3+namelen) = filena_restart
          lentmp = namelen+3+1
          namelen = strlen(filena)
          command(lentmp+1:lentmp+namelen) = filena
          call system(command)
          open(unit=subit_unit,file=filena,status='unknown',
     &          access='append')
        endif

      else  !no restart

        namelen =  strlen(output_file_prefix)
        filena = output_file_prefix
        filena(namelen+1:namelen+5) = '.his'

        if (hisout) open(unit=his_unit,file=filena,status='unknown',
     &     access='append')
c
        if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5) then
          filena = output_file_prefix
          filena(namelen+1:namelen+4) = '.si'
          open(unit=subit_unit,file=filena,status='unknown',
     &          access='append')
        endif

c       ****** Dissipation-based Continuation output *******
        if (dissCon) then

          namelen =  strlen(output_file_prefix)
          filena = output_file_prefix
          filena(namelen+1:namelen+5) = '.dbc'
          open
     &    (unit=dbc_unit,file=filena,status='unknown',access='append')

        end if

c       ****** Boundary-layer velocity profile output *******
        if (v_profile) then

          namelen =  strlen(output_file_prefix)
          filena = output_file_prefix
          filena(namelen+1:namelen+6) = '.blvp'
          open
     &    (unit=blvp_unit,file=filena,status='unknown',access='append')

        end if

c       ********* Design Point Gradient History **********
        namelen =  strlen(output_file_prefix)
        filena = output_file_prefix
        filena(namelen+1:namelen+7) = '.gvhis'

        if (gvhisout) open(unit=dpgrad_unit,file=filena,
     &       status='unknown', access='append')

c
        if (itmodel.eq.2) then
          filena = output_file_prefix
          filena(namelen+1:namelen+6) = '.this'
          if (thisout) open(unit=turbhis_unit,file=filena,
     &       status='unknown', access='append')
        endif

      endif  !restart?

      if (obj_restart) then
         namelen =  strlen(restart_file_prefix0)
         filena = restart_file_prefix0
         filena(namelen+1:namelen+5) = '.obj'

         if (wt_update) then
             namelen =  strlen(dirname)
             filena = dirname
             filena(namelen+1:namelen+5) = '.obj'
         end if

         open(unit=rqo_unit,file=filena,status='old',form='formatted')
      end if

c--------------------------------------------------------------
c     Checking and output
c--------------------------------------------------------------

c     -- check mesh sizes --
      if ( jmax .gt. maxj ) then                                      
        write(out_unit,36) jmax, maxj
     &        
 36     format( 1x, 'jmax is ', i5, 5x, 'only dimensioned for ', i5)   
        stop 'error 1 in ioall - see .solv file for details'                                        
      endif                                                             

      if ( jmax*kmax .gt. maxjk ) then                                
        write(out_unit,37) jmax*kmax, maxjk
     &        
 37     format( 1x,'jmax*kmax is ',i10,5x,'only dimensioned for ',i10)
        stop 'error 1 in ioall - see .solv file for details'                                        
      endif                                                             

      if ( jmax .gt. 1999 .or. kmax .gt. 1999 ) then                    
        write(out_unit,38) jmax,kmax
     &        
 38     format( 1x, 'jmax,kmax are ', 2i5, 5x,                         
     *        'bigger than 1999 ', i5)
     &        
        stop 'error 1 in ioall - see .solv file for details'                                        
      endif                                                             
      
      ! ________________________________
      ! check turublent input parameters
      if (turbulnt .and. .not.viscous) then
         write(out_unit,*) 'CONFLICT between input parameters TURBULNT',
     &        ' and VISCOUS: a turbulent flow must also be specified',
     &        ' as viscous.'
         stop 'error 1 in ioall - see .solv file for details'
      endif
      ! ___________________________________
      ! check transitional input parameters
      if (transitionalFlow .and. .not.turbulnt) then
         write(out_unit,*) 'CONFLICT between input parameters',
     &        ' TRANSITIONALFLOW and TURBULNT: a transitional flow',
     &        ' must also be specified as turbulent.'
         stop 'error 1 in ioall - see .solv file for details'
      endif
      if (freeTransition .and. .not.transitionalFlow) then
         write(out_unit,*) 'CONFLICT between input parameters',
     &        ' FREETRANSITION and TRANSITIONALFLOW:',
     &        ' for free transition (i.e. transition prediction)',
     &        ' the flow must also be specified as transitional.'
         stop 'error 1 in ioall - see .solv file for details'
      endif
      if (freeTransition .and. unsted) then
         write(out_unit,*) 'CONFLICT between input parameters',
     &        ' FREETRANSITION and UNSTED:',
     &        ' currently free transition (i.e. transition prediction)',
     &        ' is not set-up to work with unsteady flow solves.'
         stop 'error 1 in ioall - see .solv file for details'
      endif
      if (.not. transitionalFlow .and.
     &     (transup.gt.0d0 .or. translo.gt.0d0)) then
         write(out_unit,*) 'CONFLICT between input parameters',
     &        ' TRANSITIONALFLOW and TRANSUP or TRANSLO:',
     &        ' for fully-turbulent flow, please set both transup', 
     &        ' and translo to 0.00'
         stop 'error 1 in ioall - see .solv file for details'
      endif

      if (periodic .and. sngvalte) sngvalte=.false.
      if (ibc) circul=.false.

      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|              Flow field conditions                 |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,61) fsmach,alpha
 61   format(18H |       fsmach = ,f7.3,12H    alpha = ,f7.3,10x,1H|)
      write (out_unit,*)
     &      '|                                                    |'  

      if (circul) then                                                  
        write (out_unit,*)
     &        '|         Far field circulation model used.          |'  
      else
        write (out_unit,*)
     &        '|       Far field circulation model not used.        |'  
      endif

      if (cmesh) then
        write (out_unit,*)
     &        '|             Cmesh logic set to true.               |'
      else
        write (out_unit,*)
     &        '|             Cmesh logic set to false.              |'
      endif

      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  

      if(viscous)then                                                   
        write (out_unit,*)
        write (out_unit,*)
     &        '------------------------------------------------------'  
        write (out_unit,*)
     &        '|                Viscous Parameters                  |'  
        write (out_unit,*)
     &        '------------------------------------------------------'  
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,70) re, tinf
        if (transitionalFlow) write(out_unit,71) translo, transup
        write (out_unit,*)
     &        '|                                                    |'
        if (turbulnt .and. itmodel .eq.2 ) then 
           if (transitionalFlow) then
              write(out_unit,*)
     &        '|        LAMINAR-TURBULENT TRANSITIONAL FLOW         |'
              write(out_unit,*)
     &        '|           USING TRIP TERMS IN S-A MODEL            |'
           else
              write(out_unit,715)
           end if
           if (originalSA) then 
              write(out_unit,*)
     &        '|            Original S-A MODEL is used              |'  
           else 
              write(out_unit,*)
     &        '|        S-A MODEL with Ashford fix is used          |'  
           end if
        end if
 70     format(19H |  Reynolds No. = ,f12.3,13H      Tinf = ,f7.3,3x,
     &        1H|)
 71     format(19H |       Translo = ,f12.3,13H   Transup = ,f7.3,3x, 
     &        1H|)
 715    format(1x,1h|,14x,21h FULLY TURBULENT FLOW,17x, 1h|/, 1x, 1h|, 
     &        10x,27h NO TRIP TERMS IN S-A MODEL, 15x, 1h|)


        if (wtrat.ne.0.0) then
          write (out_unit,*)
     &          '|                                                    |'
          write (out_unit,*)
     &          '|  Wall temperature ratio used in bc for density     |'
          write (out_unit,*) '|  Wall temperature ratio = ',wtrat 
        endif
        
        write (out_unit,*)
     &        '|                                                    |'  
        if (visxi) write (out_unit,*)
     &        '|  Explicit viscous terms in xi used                 |'  
        if (viseta) write (out_unit,*)
     &        '|  Explicit viscous terms in eta used                |'  
        if (viscross) write (out_unit,*)
     &        '|  Explicit viscous cross terms used                 |'  
        if (cmesh) then      
          if (viscross) then                                          
            write (out_unit,*) ' Error in input '
            write (out_unit,*)
     &            ' Cmesh for cross terms not implemented.'
            write (out_unit,*) ' Try viscross = false'
            stop                                                      
          endif                                                     
        endif                                                             
        write (out_unit,*)
     &        '|                                                    |'  
        if (turbulnt) then
          if (itmodel.eq.1) then
            write (out_unit,210) 
          elseif (itmodel.eq.2) then
            write (out_unit,220)
            if (iord.eq.2) ispbc=0
            write (out_unit,190) ispmod, retinf
            write (out_unit,200) nnit, ispbc
          endif
        else                                                              
          write (out_unit,230)
        endif                                                             
        write (out_unit,240)
 190    format(2h |,9x,12hwith ispmod=,I2,3x,1h&,3x,7hretinf=,f8.4,
     &        7x,1h|)
 200    format(2h |,15x,6hnnit =,I2,3x,1h&,3x,7hispbc =,I3,12x,1h|)
 210    format(2h |,2x,44hBaldwin-Lomax tubulence model is being used.,
     &        6x,1h|)
 220    format(2h |,2x,34hS-A tubulence model is being used.,16x,1h|)
 230    format(2h |,5x,40h<<<<<<<< Laminar flow assumed >>>>>>>>>>,7x
     &        ,1h|)
 240    format(2h |,52x,1h|)

        if (meth.eq.1 .or. meth.eq.3 .or. meth.eq.4 .or. meth.eq.5) then
          if (ivis.eq.1) then
            write (out_unit,*)
     &          '|       Implicit block in eta with viscous terms     |'
            write (out_unit,75) ivis
 75         format(17H |        Ivis = ,i1,36x,1H|)
          endif
          if (ivis.eq.2) then 
            write (out_unit,*)
     &          '|  Penta diag in xi and eta : imp. vis. eig. val.    |'
            write (out_unit,72) ivis,visceig
 72         format(17H |        Ivis = ,i1,11x,13H   Visceig = ,f6.2, 
     &            6x,1H|)
          endif
        endif                                                       

        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '------------------------------------------------------'  
      else                                                              
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '|  <<<<<<<<<  Inviscid flow assumed >>>>>>>>>>>      |'  
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '------------------------------------------------------'  
      endif                                                             
      
      write (out_unit,*)
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|               Algorithm Parameters                 |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,*)
     &      '|  Time accuracy ::                                  |'  
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,73) thetadt,phidt
      write (out_unit,76) scaledq
 73   format(14H |  thetadt = ,f7.2,11H   phidt = ,f7.2,15x,1H|)
 76   format(14H |  scaledq = ,f7.2,33x,1H|)
      if (thetadt.eq.1.0 .and. phidt.eq.0.0) write (out_unit,*)
     &      '|  Euler implicit 1st order accurate                 |'  
      if (thetadt.eq.0.5 .and. phidt.eq.0.0) write (out_unit,*)
     &      '|  Trap. implicit 2nd order accurate                 |'  
      if (thetadt.eq.1.0 .and. phidt.eq.0.5) write(out_unit,*)
     &      '|  3pt Backwards Implicit 2nd order accurate.        |'  
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,*)
     &      '|  Variable time step parameters  ::                 |'  
      write (out_unit,74) jacdt,dtrate
 74   format(17H |       jacdt = ,I3,12H   dtrate = ,f7.3,15x,1H|)
      write (out_unit,*)
     &      '|                                                    |'
      if(jacdt.eq.0) write (out_unit,*)
     &      '|  <<<<<<<<<<<<<  Time accurate  >>>>>>>>>>>>>>>     |'  
      if(jacdt.eq.1) write (out_unit,*)
     &      '|  <<<<<<<<  Variable dt = 1./(1+sqrt(j)) >>>>>>>>>  |'  
      if(jacdt.eq.2) write (out_unit,*)
     &      '|  <<<<<<<<  Variable dt : min. eigen. >>>>>>>>>>>>  |'  
      if(jacdt.eq.3) write (out_unit,*)
     &      '|    <<<<<<  Variable dt : const. cfl  >>>>>>>>>>>>  |'  
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,77) strtit
 77   format(40H |  slow start time  ::        strtit = ,f5.1,9x,1H|)
      write (out_unit,*)
     &      '|                                                    |'  
      if (cmesh)write (out_unit,*)
     &      '|   <<<<<<<<<<<<<  C mesh logic >>>>>>>>>>>>>>>>>    |'  
      write (out_unit,*)
     &      '|                                                    |'  
      if (orderxy .or. cmesh) write (out_unit,*)
     &      '|       ** x-y order of implicit integration **      |'  
      if (.not.orderxy .and. .not.cmesh)write (out_unit,*)
     &      '|       ** y-x order of implicit integration **      |'  
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
      
      write (out_unit,*)
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|                  Method Choice                     |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|                                                    |'  
      if (meth.eq.2) then                                             
        write (out_unit,*)
     &        '|       Block pentadiagonal algorithm  meth = 2      |'

        if (ibc) then
          write (out_unit,*)
     &          '|          Implicit Boundary Conditions Used         |'
        else      
          write (out_unit,*)
     &          '|          Explicit Boundary Conditions Used         |'
        endif
        write (out_unit,*)
     &        '|                                                    |'  

        if (idmodel.ne.3) then
          write (out_unit,*)
     &          '|  Implicit 2nd-4th diff. dissipation used on left   |'
          write (out_unit,*)
     &          '|     filter 2nd-4th dissipation used on right.      |'
          write (out_unit,*)
     &          '|                                                    |'
          write (out_unit,*)
     &          '|  Dissipation coeficients ::                        |'
          write (out_unit,80) dis2x,dis4x
          write (out_unit,81) dis2y,dis4y
        else
          write (out_unit,*)
     &          '|   Imp. 4th diff. const. coef. diss. used on left   |'
          write (out_unit,*)
     &          '|     filter 4th const. coef. diss. used on right.   |'
          write (out_unit,*)
     &          '|                                                    |'
          write (out_unit,*)
     &          '|  Dissipation coeficients ::                        |'
          write (out_unit,84) smu,smuim
        endif

 84     format(13H |     smu = ,f7.2,12H    smuim = ,f7.2,15x,1H|)
        if(periodic)then                                                
          write (out_unit,*) '  Periodic penta option not availiable '
          write (out_unit,*)'  for meth = 2           abort !!'
          stop                                                          
        endif                                                           

      else if (meth.eq.1 .or. meth.eq.3 .or. meth.eq.4 .or. meth.eq.5)
     &        then
        if (meth.eq.3) write (out_unit,*)
     &        '|       Pentadiagonal algorithm  meth = 3            |'  
        if (meth.eq.1) write (out_unit,*)
     &        '|       Pentadi subiteration alg. meth = 1           |'  
        if (meth.eq.4) write (out_unit,*)
     &        '|       Tridi ludecmp algorithm  meth = 4            |'  
        if (meth.eq.5) write (out_unit,*)
     &        '|       Pentadi ludecmp algorithm  meth = 5          |'  
        if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5)
     &        write (out_unit,79)jsubmx
 79     format(2h |,15x,13hwith jsubmx =,I3,21x,1h|)
        write (out_unit,*)
     &        '|                                                    |'  

        if (idmodel.eq.4) then
          write (out_unit,*)
     &          '|  <<<<<<<<<<   CUSP Dissipation Used  > >>>>>>>>>>  |'
          write (out_unit,104) limiter
 104      format (19H |       Limiter = ,i3,32x,1H|)
          write (out_unit,105) epz,epv
 105      format (15H |       epz = ,d8.2,10H    epv = ,d8.2,13x,1H|)
          write (out_unit,*)
     &          '|                                                    |'
        else if (idmodel.eq.3) then
          write (out_unit,*)
     &          '|   Imp. 4th diff. const. coef. diss. used on left   |'
          write (out_unit,*)
     &          '|     filter 4th const. coef. diss. used on right.   |'
          write (out_unit,*)
     &          '|                                                    |'
          write (out_unit,*)
     &          '|   Dissipation coeficients ::                       |'
          write (out_unit,84) smu,smuim
        else
          write (out_unit,*)
     &          '|  Implicit 2nd-4th diff. dissipation used on left   |'
          write (out_unit,*)
     &          '|     filter 2nd-4th dissipation used on right.      |'
          write (out_unit,*)
     &          '|                                                    |'
          write (out_unit,*)
     &          '|  Dissipation coeficients ::                        |'
          write (out_unit,80) dis2x,dis4x
          write (out_unit,81) dis2y,dis4y
 80       format (13H |   dis2x = ,f8.4,12H    dis4x = ,f8.4,13x,1H|)
 81       format (13H |   dis2y = ,f8.4,12H    dis4y = ,f8.4,13x,1H|)
          write (out_unit,*)
     &          '|                                                    |'
          if (idmodel.eq.2) then
            write (out_unit,*)
     &           '|  <<<<<<<<<   Matrix Dissipation Used   >>>>>>>>>  |'
            write (out_unit,101) vlxi,vleta
            write (out_unit,102) vnxi,vneta
 101        format (13H |    vlxi = ,f8.4,12H    vleta = ,f8.4,13x,1H|)
 102        format (13H |    vnxi = ,f8.4,12H    vneta = ,f8.4,13x,1H|)
            write (out_unit,*)
     &           '|                                                   |' 
          endif
        endif

        if (iord.eq.2) then
          write (out_unit,*)
     &          '|  <<<<   Second-Order Spatial Accuracy Used   >>>>  |'
        elseif (iord.eq.4) then
          write (out_unit,*)
     &          '|  <<<<   Fourth-Order Spatial Accuracy Used   >>>>  |'
        else
          write (out_unit,*)
     &          '|  <<<<      Unknown Spatial Accuracy Used     >>>>  |'
          write (out_unit,*)
     &          '             IORD should be set to 2 or 4.            '
          stop
        endif
        if (integ.eq.2) then
          write (out_unit,*)
     &          '|         With Second-Order Force Integration        |'
        elseif (integ.eq.3) then
          write (out_unit,*)
     &          '|         With Third-Order Force Integration         |'
        else
          write (out_unit,*) 'Error in input.'
          write (out_unit,*) 'Value for INTEG should be 2 or 3'
          stop
        endif
        write (out_unit,*)
     &        '|                                                    |'  
      endif                                                             
c     
c     idmodel=3 => constant coefficient dissipation
      if (idmodel.eq.3) ispec=0
      if (meth.le.5) then
        if (ispec.eq.0) then
          write (out_unit,*)
     &          '|            spectx = specty = 1./xyj                |'
        elseif(ispec.eq.1)then
          write (out_unit,*)
     &          '|     Form spectx and specty independently           |'
          write (out_unit,*)
     &          '|         spectx = (abs(uu) + ccx)/xyj               |'
          write (out_unit,*)
     &          '|         specty = (abs(vv) + ccy)/xyj               |'
        elseif (ispec.eq.3) then
          write (out_unit,*)
     &          '|   Form spectx and specty independently with limits |'
          write (out_unit,*)
     &          '|         spectx = (abs(uu) + ccx)/xyj               |'
          write (out_unit,*)
     &          '|         specty = (abs(vv) + ccy)/xyj               |'
        elseif (ispec.eq.4) then
          write (out_unit,*)
     &          '|     Form spectx and specty independently           |'
          write (out_unit,*)
     &          '|         spectx = [ abs( xi_x) + abs( xi_y)]/xyj    |'
          write (out_unit,*)
     &          '|         specty = [ abs(eta_x) + abs(eta_y)]/xyj    |'
        elseif (ispec.eq.-1) then
          write (out_unit,*)
     &          '|         spectx=specty=64./xyj                      |'
        endif                                                             
      endif                                                             
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
     
      write(out_unit,*)'    '
      
      if(clalpha)then                                                   
        write(out_unit,*)
     &        '------------------------------------------------------'
        write(out_unit,*)
     &        '|                  Cl vrs Alpha Logic                |'  
        write(out_unit,*)
     &        '------------------------------------------------------'  
        write(out_unit,*)
     &        '|                                                    |'  
        write(out_unit,*)
     &        '|     Option to compute alpha for a given cl         |'  
        write(out_unit,*)
     &        '|     Cl input = ',clinput,'            |'
        write(out_unit,*)
     &        '|     Start alpha mod after ',iclstrt,' iterations  |'
        write(out_unit,*)
     &        '|     update alpha every ',iclfreq,' steps          |'
      
        write(out_unit,*) 
     &        '|     using the formula :                            |'
        write(out_unit,*)
     &        '|     delta alpha = -relaxcl * deltacl               |'  
        write(out_unit,*) 
     &        '|     delta cl = clinput - cl(calculated)            |'  
      write(out_unit,*)'|                relaxcl = ',relaxcl,'  |'
      write(out_unit,*)
     &      '|                                                    |'  
      write(out_unit,*)
     &      '| update of alpha stopped when abs(delta cl) reaches |'  
      write(out_unit,*)'| tolerance for delta cl = ',cltol,'  |'
      write(out_unit,*)
     &      '|                                                    |'  
      isw = 1                                                           
      write(out_unit,*)
     &      '------------------------------------------------------'
      endif                                                             
     
      write (out_unit,*)
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|               Grid Specifications                  |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
      if (iread.eq.0) then
c     cylinder default case                            
        if (.not.periodic) then                                         
          write (out_unit,*) 'Error in inputs periodic = f' 
          write (out_unit,*) 'Try periodic = true' 
          stop                                                         
        endif                                                         
        write (out_unit,*)
     &        '|                                                    |'
        write (out_unit,*)
     &        '|     Cylinder grid is being internally generated    |'
        write (out_unit,*)
     &        '|                                                    |'
        write (out_unit,*)
     &        '|      Minimum spacing at body for cylinder grid     |'
        write(out_unit,345) dswall
 345    format(1x,1h|,17x,8hdswall =,e11.3,16x,1h|)
 346    format(1x,1h|,17x,8hsobmax =,f6.2,21x,1h|)
        if (dswall.eq.0.0) then
          write (out_unit,*)
     &          '|           Produces uniform circle grid             |'
        elseif (dswall.gt.0.0) then
          write (out_unit,*)
     &          '|    Produces stretched grid with dswall minimum     |'
          write (out_unit,*)
     &          '|    spacing in eta and cosine clustering to front   |'
          write (out_unit,*)
     &          '|    and back.                                       |'
        elseif (dswall.lt.0.0) then
          write (out_unit,*)
     &          '|  Produces uniform grid in xi and stretched in eta. |'
        endif                                                             
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '|  Far-field extent in diameters (d=1) for cyl. grid |'  
        write (out_unit,346) sobmax 
      elseif (iread.eq.1) then
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '|    Formatted grid file is being read from unit 8   |'  
      elseif (iread.eq.2) then
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '|   Unformatted grid file is being read from unit 8  |'  
      else                                                              
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '|          Error in inputs iread .ne. 0,1,2          |'  
        stop                                     
      endif                                                             
      if (periodic) then
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '|    This is a periodic grid ...                     |'  
        if (jacarea) then
          write (out_unit,*)
     &          '|    with Jacobian calculated using cell areas.      |'
        else
          write (out_unit,*)
     &          '|    with Jacobian calculated using grid metrics.    |'
        endif
        write (out_unit,*)
     &        '|                                                    |'  
        write(out_unit,82) jmax,kmax
        write(out_unit,83) jtail1,jtail2
      else                                                              
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '|    This is a nonperiodic grid ...                  |'  
        if (jacarea) then
          write (out_unit,*)
     &          '|    with Jacobian calculated using cell areas.      |'
        else
          write (out_unit,*)
     &          '|    with Jacobian calculated using grid metrics.    |'
        endif
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,82) jmax,kmax
        write (out_unit,83) jtail1,jtail2
 82     format (17H |         jmax = ,I5,12H     kmax = ,I5,15x,1H|)
 83     format (17H |       jtail1 = ,I5,12H   jtail2 = ,I5,15x,1H|)
        write (out_unit,*)
     &        '|                                                    |'  
      endif                                                             
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|             Boundary Condition Type                |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
      write (out_unit,*)
     &      '|      Body:                                         |'  
      if (iord.eq.2) then
        if (.not.viscous) then
          write (out_unit,*)
     &          '|      Pressure extrapolation is first-order         |'
          write (out_unit,*)
     &          '|        U-tang extrapolation is first-order         |'
        endif

        if (viscous) then
          if (foso.eq.0) then
            write (out_unit,*)
     &            '|      Pressure extrapolation is zeroeth-order     |'
          elseif (foso.eq.1) then
            write (out_unit,*)
     &            '|      Pressure extrapolation is first-order       |'  
          endif
        endif

      elseif (iord.eq.4) then
        if (.not.viscous) then
          write (out_unit,*)
     &          '|      Pressure extrapolation is third-order         |'
          write (out_unit,*)
     &          '|      U-tang extrapolation is second-order          |'
        else
          write (out_unit,*)
     &          '|                dp/dn is third-order                |'
          write (out_unit,*)
     &          '|                d(rho)/dn is third-order            |'
        endif
      endif
      if (.not.periodic) then
        write (out_unit,*)
     &        '|                                                    |'  
        write (out_unit,*)
     &        '|      Trailing Edge:                                |'
        if (sngvalte) then
          write (out_unit,*)
     &          '|      Single-Valued Trailing Edge.                  |'
        else
          write (out_unit,*)
     &          '|      Dual-Valued Trailing Edge.                    |'
        endif
      endif
      if (dis2x.gt.0 .or. dis2y.gt.0.) then
        if (zerote) then
          write (out_unit,*)
     &          '|      Second-Difference Dissipation Zeroed.         |'
        endif
      endif
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,*)
     &      '------------------------------------------------------'  
    
      if (prec.gt.0) then
        write (out_unit,*)
        write (out_unit,*)
     &        '------------------------------------------------------'
        write (out_unit,*)
     &        '|                                                    |'
        write (out_unit,*)
     &        '|           Preconditioning will be used.            |'
        write (out_unit,*)
     &        '|                                                    |'
        if (prec.eq.1) then
          write (out_unit,*)
     &          '|           Weiss-Smith,   Epsilon = 1               |'
        elseif (prec.eq.2) then
          write (out_unit,*)
     &          '|           Weiss-Smith,   Epsilon = Mr              |'
        elseif (prec.eq.3) then
          write (out_unit,*)
     &          '|           Weiss-Smith,   Epsilon = Mr^2            |'
        else
          write (out_unit,*)
     &          '|           INCORECT VALUE OF PREC!!!!!!           |'
          stop
        endif
        write (out_unit,*)
     &        '|                                                    |'
        write (out_unit,103) prxi,prphi
 103    format (20H |           prxi = ,f5.2,12H    prphi = ,f5.2,12x
     &        ,1H|)
        write (out_unit,*)
     &        '|                                                    |'
        write (out_unit,*)
     &        '------------------------------------------------------'
      endif
 
c     
c     check maxpen against maxjk
c     
c     maxpen can be either maxjk which allows use of meth =2
c     or maxpen = maxj so that larger meshes can be used for           
c     meth 1,3,4,5 
c     
      if (maxpen.ne.maxjk) then
        if (meth.eq.2) then  
          write (out_unit,*) 'Dimensions not correct for meth=2.'
          write (out_unit,*) 'Change maxpen to be equal to maxjk.'
          stop                                                        
        endif                                                           
      endif

      if (ibc) then
        if (meth.eq.2) then
          if (.not.viscous) then
            write (out_unit,*) 'VISCOUS=FALSE & IBC=TRUE'
            write (out_unit,*)
     &            'Implicit boundary conditions are implemented for'
            write (out_unit,*) 'viscous cases only.'
            stop
          endif
        else
          write (out_unit,*) 'METH=',meth
          write (out_unit,*)
     &          'Implicit boundary conditions implemented for'
          write (out_unit,*) 'Block-Pentadiagonal scheme ... Meth=2'
          stop
        endif
      endif

      if (.not.unsted .and.(meth.eq.1 .or. meth.eq.4 .or. meth.eq.5))
     &      then
        write (out_unit,*)
     &        '***************************************************'
        write (out_unit,*)
     &        '***************************************************'
        write (out_unit,*)
     &        'For methods 1,4,5 the variable UNSTED must be set'
        write (out_unit,*)
     &        'to true.  It was set to false by user and reset to'
        write (out_unit,*) 'true by SC1'
        write (out_unit,*)
     &        '***************************************************'
        write (out_unit,*)
     &        '***************************************************'
        unsted=.true.
      endif

      if (unsted .and. jacdt.ne.0) then
        write (out_unit,*) 'UNSTED=TRUE and JACDT.NE.0'
        write (out_unit,*) 'For unsteady run you must set jacdt=0.'
        stop
      endif

      if (unsted .and. (CLALPHA.or.CLALPHA2.or.CLOPT)) then
       write (out_unit,*) 'For unsteady run you must set CLALPHA=FALSE,'
       write (out_unit,*) 'CLALPHA2=FALSE, and CLOPT=FALSE'
       stop
      endif

      if (itmodel.eq.2 .and. cmesh) then
        write (out_unit,*)
     &        'The Spalart-Allmaras model is not set up to handle'
        write (out_unit,*) 'Cmesh logic.  Reset Cmesh to false.'
        stop
      endif

      if (.not.viscous .and. turbulnt) then
        write (out_unit,*) 'VISCOUS=FALSE ... TURBULNT=TRUE'
        write (out_unit,*)
     &        '-incompatible ... if inviscid set turbulnt=false'
        stop
      endif

      if (ramptran.gt.0. .and. cmesh) then
        write (out_unit,*) 'WARNING!!'
        write (out_unit,*)
     &        'Transition ramp array not shuffled properly'
        write (out_unit,*)
     &        'for cmesh=true ... reseting ramptran to zero'
        write (out_unit,*)
        ramptran=0.d0
      endif

      if ( (idmodel.eq.2 .or. idmodel.eq.4) .and. meth.eq.2) then
        write (out_unit,*) 'Matrix and CUSP dissipation are NOT'
        write (out_unit,*) 'set up for  method 2.'
        stop
      else if (meth.ne.2 .and. idmodel.eq.3) then
        write (out_unit,*) 'METH=',meth
        write (out_unit,*)
     &        'Constant coefficient diss. set up for meth=2 only.'
        stop
      endif
c     
      if (idmodel.eq.4 .and. periodic) then
        write (out_unit,*)
     &        'CUSP dissipation not set up for periodic grids!!'
        stop
      end if
c     
      if (ismodel.gt.1 .and. phidt.ne.0. .and. unsted) then
        write (out_unit,*)
     &        'Must use implicit Euler time-marching for ismodel >1'
        write (out_unit,*) 'Reset phidt to 0.'
        stop
      endif
c     
      if (ismodel.eq.1 .and. phidt.ne.0.5 .and. unsted) then
        write (out_unit,*)
     &        'Must use Second-Order Backwards time-marching'
        write (out_unit,*) 'for ismodel = 1.  Reset phidt to -0.5- .'
        stop
      endif
c     
      if (iord.ne.2 .and. iord.ne.4) then
        write (out_unit,*) 'Error in Input'
        write (out_unit,*) 'iord can only be equal to 2 or 4'
        stop
      endif
      
      if (ispbc.gt.2) then
        write (out_unit,*) 'Error in Input'
        write (out_unit,*) 'ISPBC cannot be greater than 2'
        stop
      endif

c     check some other parameters if preconditionning is on
      if (prec.ne.0) then
        duerr=.false.
        if (meth.ne.3) then
          write (out_unit,*)
     &          'Only METH=3 is supported with preconditioning'
          duerr=.true.
        endif
        if (ispec.ne.1) then
          write (out_unit,*)
     &          'Only ISPEC=1 is supported with preconditioning'
          duerr=.true.
        endif
        if (jacdt.gt.2) then
          write (out_unit,*)
     &          'JACDT >2 is not supported with preconditioning'
          duerr=.true.
        endif
        if (IVIS.ne.2) then
          write (out_unit,*)
     &          'Only IVIS=2 is supported with preconditioning'
          duerr=.true.
        endif
     
        if (duerr) then
          write (out_unit,*)
     &          'Errors appeared in the Input, TERMINATING!'
          stop
        endif
      endif
      write(out_unit,300)
     &      
 300  format(/)                                                         

      call flush(out_unit)
    
      return                                                            
      end                                                               


c$$$chb  Added Markus' subroutine for converting integers to strings for use
c$$$chb  in naming output files from Optima2D.
c$$$
c$$$      subroutine i_to_s(intval,s)
c$$$
c$$$      integer idig,intval,ipos,ival,i
c$$$      character ( len = * ) s
c$$$
c$$$      s = ' '
c$$$  
c$$$      ival = intval
c$$$
c$$$c  Working from right to left, strip off the digits of the integer
c$$$c  and place them into S(1:len ( s )).
c$$$c
c$$$      ipos = len(s) 
c$$$
c$$$      do while ( ival /= 0 )
c$$$
c$$$         idig = mod( ival, 10 )
c$$$         ival = ival / 10
c$$$         s(ipos:ipos) = char(idig + 48 )
c$$$         ipos = ipos - 1
c$$$
c$$$      end do
c$$$c
c$$$c  Fill the empties with zeroes.
c$$$c
c$$$      do i = 1, ipos
c$$$
c$$$         s(i:i) = '0'
c$$$      end do
c$$$ 
c$$$      return
c$$$      end      
