c     *****************************************************************
c     ****       subroutine to ouput pressure distributions        ****
c     *****************************************************************
c     calling routine: ioall

      subroutine writecp(nhalo, jdim, kdim, iside, press, x,
     &     y, xyj)

      implicit none

#include "../include/common.inc"
#include "../include/units.inc"

      integer nhalo,jdim,kdim,iside,j,k
      double precision CPC,PP,CP
      double precision  press(jdim+2*nhalo,kdim+2*nhalo)
      double precision  x(jdim+2*nhalo,kdim+2*nhalo)
      double precision  y(jdim+2*nhalo,kdim+2*nhalo) 
      double precision  xyj(jdim+2*nhalo,kdim+2*nhalo)

      CPC = 2./(GAMMA*FSMACH*FSMACH)

c     side # 1
      if (iside.eq.1) then
         k = nhalo + 1
         do j=nhalo+1,nhalo + jdim
            PP = PRESS(J,k)*XYJ(J,k)
            CP = (PP*GAMMA -1.)*CPC
            write(n_cp,100) x(j,k), cp, y(j,k)
         end do

c     side # 2
      elseif (iside.eq.2) then
         j = nhalo + 1
         do k=nhalo + kdim,nhalo + 1,-1
            PP = PRESS(J,k)*XYJ(J,k)
            CP = (PP*GAMMA -1.)*CPC
            write(n_cp,100) x(j,k), cp, y(j,k)
         end do

c     side # 3
      elseif (iside.eq.3) then
         k = nhalo + kdim
         do j=nhalo + jdim,nhalo + 1,-1
            PP = PRESS(J,k)*XYJ(J,k)
            CP = (PP*GAMMA -1.)*CPC
            write(n_cp,100) x(j,k), cp, y(j,k)
         end do

c     side # 4
      elseif (iside.eq.4) then
         j = nhalo + jdim
         do k=1 + nhalo,nhalo + kdim 
            PP = PRESS(J,k)*XYJ(J,k)
            CP = (PP*GAMMA -1.)*CPC
            write(n_cp,100) x(j,k), cp, y(j,k)
         end do
      endif

c     That's all folks!

 100  format(f14.7,5x,f14.7,5x,f14.7)

      return
      end
