C ********************************************************************
C ********************* DIFFERENCE X, Y  IN XI ***********************
C ********************************************************************
C Note: For now backward difference at boundaries.....
c calling routine: xymets
c
      subroutine etadif(iblk,nblks,jdim,kdim,x,y,xy,             
     &                 jbegin,jend,kbegin,kend)

      implicit none
c                 
#include "../include/common.inc"
c
      integer iblk,nblks,jdim,kdim,jbegin,jend,kbegin,kend,j,k,n,level
      double precision xy(jdim,kdim,4),tmp,tmp2    
      double precision x(jdim,kdim),y(jdim,kdim)             
      logical snglgrd,mgrds,condition
c
c     - xy2 = xeta, xy1 = yeta
c                 
cmt ~~~ I prefer to initialize the xy1,xy2 for each point including the corners (halo points)
cmt ~~~ so we can avoid having zeros or random numbers on the block corners
      DO n=1,2
         DO k=1,kdim
            DO j=1,jdim
               xy(j,k,n)=1.d0
            ENDDO
         ENDDO
      ENDDO
cmt
      if (iord.eq.4) then
        tmp=1.d0/12.d0
        tmp2=2.d0*tmp
        do 11 j=jbegin,jend
          do 10 k=kbegin+2,kend-2
            xy(j,k,2)=tmp*(-x(j,k+2)+8.d0*(x(j,k+1)-x(j,k-1))+x(j,k-2))
            xy(j,k,1)=tmp*(-y(j,k+2)+8.d0*(y(j,k+1)-y(j,k-1))+y(j,k-2))
c            if (iblk.eq.11) then
c              write(99,777) j-nhalo,k-nhalo,y(j,k+2),y(j,k+1),y(j,k-1)
c     &              ,y(j,k-2),xy(j,k,1)
c     &              ,y(j,k-2),(y(j,k+1) - y(j,k-1))*.5
c            endif
 10       continue
 777      format(2i3,4f15.7,e13.4)
c
c         **********************************************
c         ***          First interior node           ***          
c         **********************************************
          k = kbegin+1
c         -second order 
c          xy(j,k,2) = ( x(j,k+1) - x(j,k-1))*.5d0                       
c          xy(j,k,1) = ( y(j,k+1) - y(j,k-1))*.5d0                       
c         -third order
          xy(j,k,2)=tmp2*(-2.d0*x(j,k-1) - 3.d0*x(j,k) + 
     &          6.d0*x(j,k+1) - x(j,k+2))     
          xy(j,k,1)=tmp2*(-2.d0*y(j,k-1) - 3.d0*y(j,k) + 
     &          6.d0*y(j,k+1) - y(j,k+2))  
c
          k = kend-1                                                    
c         -second order
c          xy(j,k,2) = ( x(j,k+1) - x(j,k-1))*.5d0                       
c          xy(j,k,1) = ( y(j,k+1) - y(j,k-1))*.5d0                       
c         -third order
          xy(j,k,2)=tmp2*(x(j,k-2) - 6.d0*x(j,k-1)
     &          + 3.d0*x(j,k) + 2.d0*x(j,k+1))
          xy(j,k,1)=tmp2*(y(j,k-2) - 6.d0*y(j,k-1)
     &          + 3.d0*y(j,k) + 2.d0*y(j,k+1))
c     
c         **********************************************
c         ***             Boundary node              ***          
c         **********************************************
c          if (matcharc2d) then
c           -third order
            k = kbegin
            xy(j,k,2) =tmp2*(-11.d0*x(j,k) + 18.d0*x(j,k+1) - 
     &            9.d0*x(j,k+2) + 2.d0*x(j,k+3))   
            xy(j,k,1) =tmp2*(-11.d0*y(j,k) + 18.d0*y(j,k+1) -
     &            9.d0*y(j,k+2) + 2.d0*y(j,k+3))   
            k = kend
            xy(j,k,2) =tmp2*(11.d0*x(j,k) - 18.d0*x(j,k-1) + 
     &            9.d0*x(j,k-2) - 2.d0*x(j,k-3))   
            xy(j,k,1) =tmp2*(11.d0*y(j,k) - 18.d0*y(j,k-1) +
     &            9.d0*y(j,k-2) - 2.d0*y(j,k-3))   
c          else
c           -second order
c            k = kbegin
c            xy(j,k,2) = ( -3.d0*x(j,k) +4.d0*x(j,k+1) - x(j,k+2))*.5d0
c            xy(j,k,1) = ( -3.d0*y(j,k) +4.d0*y(j,k+1) -y(j,k+2))*.5d0
c            k = kend
c            xy(j,k,2) = ( 3.d0*x(j,k) -4.d0*x(j,k-1) + x(j,k-2))*.5d0
c            xy(j,k,1) = ( 3.d0*y(j,k) -4.d0*y(j,k-1) + y(j,k-2))*.5d0
c          endif
 11     continue
c
c
c
c
      else
c       -figure out which grid you are on ... needed if mg=true
c       -integer division always rounds down to the nearest integer
        level=(iblk+nblks)/nblks
        if (mod(iblk,nblks).eq.0) level=level-1
c
c       -interior nodes
        do 20 j=jbegin,jend
        do 20 k=kbegin+1,kend-1
          xy(j,k,2) = (x(j,k+1) - x(j,k-1))*.5
          xy(j,k,1) = (y(j,k+1) - y(j,k-1))*.5
 20     continue
c
        snglgrd=.false.
        mgrds=.false.
        condition=.false.
        if (.not.mg .and. .not.gseq) snglgrd=.true.
        if (mg .or. gseq) mgrds=.true.
        if (snglgrd .or. (mgrds .and. level.ge.2)) condition=.true.
c
c       -boundary nodes
        if (lomet .and. condition) then
c         -first-order (ie. Lower-Ordered METrics)
c          -used to avoid negative jacobian on grid level 2 and 3
          do 21 j=jbegin,jend
            k = kbegin
            xy(j,k,2) = x(j,k+1) - x(j,k)
            xy(j,k,1) = y(j,k+1) - y(j,k)
c     
            k = kend 
            xy(j,k,2) = x(j,k) - x(j,k-1)
            xy(j,k,1) = y(j,k) - y(j,k-1)
 21       continue
        else
c         -second-order biased
          do 22 j=jbegin,jend
            k = kbegin
            xy(j,k,2) = ( -3.*x(j,k) +4.*x(j,k+1) -x(j,k+2) )*.5
            xy(j,k,1) = ( -3.*y(j,k) +4.*y(j,k+1) -y(j,k+2) )*.5
c     
            k = kend 
            xy(j,k,2) = ( 3.*x(j,k) -4.*x(j,k-1) + x(j,k-2) )*.5
            xy(j,k,1) = ( 3.*y(j,k) -4.*y(j,k-1) + y(j,k-2) )*.5
 22       continue
        endif
      endif
c

      return   
      end      
