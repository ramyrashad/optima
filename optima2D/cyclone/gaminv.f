      subroutine gaminv(jdim,kdim,q,xyj,sndsp,precon,s)
cdu
cdu  this routine multiplies the vector inverse transfer matrix
cdu  and inverse preconditioner
cdu
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),xyj(jdim,kdim),sndsp(jdim,kdim)
      dimension s(jdim,kdim,4),precon(jdim,kdim,6)
c
c     note gami=gamma - 1
c
      do 1000 k=klow,kup
         do 1000 j=jlow,jup
c
            rho=q(j,k,1)*xyj(j,k)
            rhoinv=1.d0/rho
            u=q(j,k,2)/q(j,k,1)
            v=q(j,k,3)/q(j,k,1)
            c=sndsp(j,k)
c
c     multiply by M inverse
c
            t4=gami*(precon(j,k,2)*s(j,k,1)-u*s(j,k,2)-
     +           v*s(j,k,3)+s(j,k,4))
            t1=t4*rhoinv/c
            t4=t4-sndsp(j,k)**2*s(j,k,1)
            t2=(-u*s(j,k,1)+s(j,k,2))*rhoinv
            t3=(-v*s(j,k,1)+s(j,k,3))*rhoinv
c
c     multiply by Gamma Inverse
c
            s(j,k,1)=t1*precon(j,k,1)
            s(j,k,2)=t2
            s(j,k,3)=t3
            s(j,k,4)=t4
c
 1000 continue
      return
      end
