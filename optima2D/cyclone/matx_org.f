c ##########################
c ##                      ##
c ##  subroutine matx     ##
c ##                      ##
c ##########################
c
      subroutine matx_org (jdim,kdim,q,s,xy,xyj,gam,sndsp,uu,ccx,
     &                           coef2,coef4,work,tmp,tmp2)            
c
c******************************************************************
c   this subroutine follows very faithfully t. pulliam notes, p.31
c
c   fourth order smoothing, added explicitly to rhs                     
c   second order near shocks with pressure grd coeff.                   
c                                                                       
c  xi direction                                                         
c                                                                       
c   start differences each variable separately                          
c******************************************************************
c                                                                       
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)          
      dimension work(jdim,kdim,4),uu(jdim,kdim),ccx(jdim,kdim)
      dimension tmp(4,jdim,kdim),tmp2(4,jdim,kdim),gam(4,4,jdim,kdim)
      dimension vec(4),tinv(4,4),t(4,4),xy(jdim,kdim,4)
c
      do 10 n = 1,4                                                     
c----    1st-order forward difference ----
         do 15 k =klow,kup                                                
         do 15 j =jlow,jup                                                
            tmp(n,j,k) = q(j+1,k,n)*xyj(j+1,k) - q(j,k,n)*xyj(j,k)           
 15      continue
c  --    c mesh bc --
         if (.not.periodic) then                                          
            j1 = jbegin                                                    
            j2 = jend                                                      
            do 16 k = klow,kup                                             
               tmp(n,j1,k) = q(j1+1,k,n)*xyj(j1+1,k) -                       
     >                        q(j1,k,n)*xyj(j1,k)                           
               tmp(n,j2,k) = tmp(n,j2-1,k)                                  
 16         continue                                                       
         endif                                                          
c        
c        
c----    apply cent-dif to 1st-order forward ----
         do 17 k =klow,kup                                                
         do 17 j =jlow,jup                                                
            tmp2(n,j,k) = tmp(n,j+1,k)-2.d0*tmp(n,j,k)+tmp(n,j-1,k)     
 17      continue                                                          
c  --    c mesh bc --
         if (.not.periodic) then                                          
            j1 = jbegin                                                    
            j2 = jend                                                      
            do 18 k =klow,kup                                             
               tmp2(n,j1,k) = q(j1+2,k,n)*xyj(j1+2,k) -                   
     >              2.d0*q(j1+1,k,n)*xyj(j1+1,k) + q(j1,k,n)*xyj(j1,k)    
               tmp2(n,j2,k) = 0.                                   
 18         continue
     $              
         endif                                                          
 10   continue                                                          
c
c
c
      do 19 k = kbegin,kend                                            
      do 19 j = jbegin,jend       
c
c        -need to compute  J^{-1} * T * |eigenvalue matrix| * T^{-1}
c
c        -compute magnitude of eigenvalues
c         vec(1)=2
c         vec(2)=2
c         vec(3)=2
c         vec(4)=2
         rj =1.d0/xyj(j,k)
         vec(1)=abs(uu(j,k))*rj
         vec(2)=vec(1)
         vec(3)=abs(uu(j,k)+ccx(j,k))*rj
         vec(4)=abs(uu(j,k)-ccx(j,k))*rj
c
c        -compute |eig. matrix| * T^{-1}
         rho=q(j,k,1)*xyj(j,k)
         rhoinv=1.d0/q(j,k,1)
         u=q(j,k,2)*rhoinv
         v=q(j,k,3)*rhoinv
         rhoinv=1.d0/rho
         bt=1.d0/sqrt(2.d0)
         vtot2=0.5d0*(u**2+v**2)
         c=sndsp(j,k)
         snr=1.d0/c
         snr2=snr**2
         beta=bt*rhoinv*snr
         phi2=vtot2*gami
         alp=bt*rho*snr
c
         rx=xy(j,k,1)
         ry=xy(j,k,2)
         rxy2=1.d0/sqrt(rx**2+ry**2)
         rx=rx*rxy2
         ry=ry*rxy2
         ryu=ry*u
         rxu=rx*u
         ryv=ry*v
         rxv=rx*v
         theta=rxu+ryv
c
c        -compute T inverse and store in tinv
         tinv(1,1)=1.d0-phi2*snr2
         tinv(2,1)=-(ryu-rxv)*rhoinv
         tinv(3,1)=beta*(phi2-c*theta)
         tinv(4,1)=beta*(phi2+c*theta)
c
         tinv(1,2)=gami*u*snr2
         tinv(2,2)=ry*rhoinv
         tinv(3,2)=beta*(rx*c-gami*u)
         tinv(4,2)=-beta*(rx*c+gami*u)
c
         tinv(1,3)=gami*v*snr2
         tinv(2,3)=-rx*rhoinv
         tinv(3,3)=beta*(ry*c-gami*v)
         tinv(4,3)=-beta*(ry*c+gami*v)
c
         tinv(1,4)=-gami*snr2
         tinv(2,4)=0.d0
         tinv(3,4)=beta*gami
         tinv(4,4)=tinv(3,4)
c
c        -multiply eig. matrix * T^{-1}
         do n=1,4
         do i=1,4
           tinv(i,n)=vec(i)*tinv(i,n)
         enddo
         enddo
c
c        -now compute T and store in t
         t(1,1)=1.d0
         t(2,1)=u
         t(3,1)=v
         t(4,1)=phi2/gami
c         
         t(1,2)=0.d0
         t(2,2)=ry*rho
         t(3,2)=-rx*rho
         t(4,2)=rho*(ryu-rxv)
c         
         t(1,3)=alp
         t(2,3)=alp*(u+rx*c)
         t(3,3)=alp*(v+ry*c)
         t(4,3)=alp*((phi2+c**2)/gami + c*theta)
c         
         t(1,4)=alp
         t(2,4)=alp*(u-rx*c)
         t(3,4)=alp*(v-ry*c)
         t(4,4)=alp*((phi2+c**2)/gami - c*theta)
c
c         do n=1,4
c         do nn=1,4
c           tinv(n,nn)=dble(n)
c           t(n,nn)=1.d0
c         enddo
c         enddo
c
c        -multiply T * (matrix stored in tinv)
         do nn=1,4
           do n=1,4
             sum=0.
             do i=1,4
               sum=sum+t(n,i)*tinv(i,nn)
             enddo
             gam(n,nn,j,k)=sum
           enddo
         enddo
c
c         do n=1,4
c           write(6,*) (nint(gam(n,nn,j,k)),nn=1,4)
c         enddo
c         stop
c
 19   continue
c
      do 50 k=klow,kup
      do 50 j=jbegin,jup
         jj=jplus(j)
         do 25 n=1,4
            sum=0.
            do 20 i=1,4
               sum=sum+(coef2(j,k)*gam(n,i,j,k)+
     &                           coef2(jj,k)*gam(n,i,jj,k))*tmp(i,j,k)
 20         continue
c        
            do 21 i=1,4
               sum=sum-(coef4(j,k)*gam(n,i,j,k)+
     &                          coef4(jj,k)*gam(n,i,jj,k))*tmp2(i,j,k)
 21         continue
            work(j,k,n)=sum
 25      continue
 50   continue
c
c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
         s(j,k,n)=s(j,k,n) + (work(j,k,n) - work(j-1,k,n))*dtd
 200  continue
c
      return                                                            
      end                                                               
