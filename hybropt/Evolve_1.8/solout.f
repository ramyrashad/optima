C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C              SSSS    OOOO   L       OOOO   U    U  TTTTTTT           C
C             S       O    O  L      O    O  U    U     T              C
C              SSSS   O    O  L      O    O  U    U     T              C
C                  S  O    O  L      O    O  U    U     T              C
C              SSSS    OOOO   LLLLL   OOOO    UUUU      T              C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE SOLOUT(IPOP,nchrom,ngene,nobfun,pop,fit,irank,nout,
     $     i_mnx,io_34,ga_file_prefix,cputime,fitmin,fitmax,nfuneval,
     $     arc2d_io)
      logical io_34,arc2d_io
      character*80 ga_file_prefix
      integer strlen
      character*80 filena
      SAVE
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTINE CONTAINS SOLUTION OUTPUT FOR THE       C
C               GENETIC ALGORITHM ***                                  C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      dimension pop(ngene,nchrom),fit(nobfun,nchrom),irank(nchrom)
      dimension fitmin(nobfun),fitmax(nobfun)
C
      DIMENSION         FITSAV(nobfun,nchrom)
      DIMENSION         ISAV(2,nchrom*10)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C           *** INITIALIZE PARAMETERS ***
C
      IF (IPOP.EQ.0) THEN
         DO NC=1,NCHROM
            DO NO=1,NOBFUN
               FITSAV(NO,NC) = -99.0
            ENDDO
         ENDDO
      ENDIF
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** WRITE GENETIC ALGORITHM CONVERGENCE HISTORY
C              TO UNIT 33 (SHORT CONVERGENCE HISTORY IN
C              KALEIDAGRAPH FORMAT) (ALSO TO SCREEN UNIT 6) ***
C-----------------------------------------------------------------------
C          *** WRITE CONVERGENCE HISTORY LABELS AND INITIAL
C              LINE OF CONVERGENCE HISTORY CORRESPONDING TO IPOP=0 ***
C
C
C-----------------------------------------------------------------------
C          *** WRITE NEXT LINE OF CONFERGENCE HISTORY FOR
C              ALL VALUES OF IPOP>0 ***
C
      IF (IPOP.GE.1.AND.NOUT.GE.1) THEN
         if(i_mnx.eq.1)then
            if(arc2d_io)WRITE(33,205) 
     $           IPOP,NFUNEVAL,CPUTIME,(FITMAX(NO),NO=1,NOBFUN)
            if(.not.arc2d_io)WRITE(33,215) 
     $           IPOP,(FITMAX(NO),NO=1,NOBFUN)
         elseif(i_mnx.eq.0)then
            if(arc2d_io)WRITE(33,205) 
     $           IPOP,NFUNEVAL,CPUTIME,(FITMIN(NO),NO=1,NOBFUN)
            if(.not.arc2d_io)WRITE(33,215) 
     $           IPOP,(FITMIN(NO),NO=1,NOBFUN)
         ENDIF
      ENDIF
 205  FORMAT (I6,2X,I8,3X,E16.8,3X,10E16.8)
 215  FORMAT (I6,2X,10E16.8)
C
C          *** OUTPUT TO UNIT 33 IS COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** WRITE OUT CURRENT DETAILED CONVERGENCE HISTORY 
C              INFORMATION TO UNIT 34 INCLUDING CHROMOZONES AND
C              GENES (ALL VALUES OF IPOP) ***
C
      IF (IPOP.GT.0.AND.NOUT.GE.1.and.io_34) THEN
         WRITE(34,206) 
  206    FORMAT (/'   IPOP NFUNEVAL  CPU(HRS)  FITMAX(NOBFUN) ')
         WRITE(34,207) IPOP,NFUNEVAL,CPUTIME,
     $        (FITMAX(NO),NO=1,NOBFUN)
  207    FORMAT (I6,2X,I6,3X,F12.2,3X,10E16.8)
         WRITE(34,210)
  210    FORMAT ('   NC RANK  FITNESS/GENES')
         DO NC=1,NCHROM
            WRITE(34,211) NC,IRANK(NC),(FIT(NO,NC),NO=1,NOBFUN)
            WRITE(34,212)(POP(NG,NC),NG=1,NGENE)
  211       FORMAT (2I5,10E16.8)
  212       FORMAT (10X,10E14.6)
         ENDDO
         WRITE(34,213)
  213    FORMAT (/)
      ENDIF
C
C          *** OUTPUT TO UNIT 34 IS COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** WRITE OUT FITNESS VALUES ASSOCIATED WITH 
C              THE PARETO FRONT TO UNIT 35 (ACTIVE FILE)
C              AND TO ACCUMULATED FILE. THE
C              ACCUMULATED FILE IS WRITTEN ONLY IF IACCUM=1.
C              IN DEFINING THE PARETO FRONT ONLY FITNESS
C              VALUES FOR THOSE CHROMOSOMES THAT ARE NOT
C              DOMINATED ARE USED, I.E., THEY HAVE TO HAVE 
C              IRANK=1. SUBROUTINE SORT SORTS THE FITNESS 
C              VALUES BY THE FITNESS IN POSITION ONE (NO=1).
C              THIS LOGIC IS EXECUTED ONLY IF NOBFUN>1 ***
C
      IF (NOUT.GE.1.AND.NOBFUN.GT.1.AND.IPOP.GE.1) THEN
         ICCOUNT = 0
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               ICCOUNT = ICCOUNT+1
               DO NO=1,NOBFUN
                  FITSAV(NO,ICCOUNT) = FIT(NO,NC)
               ENDDO
               ISAV(1,ICCOUNT) = IPOP
               ISAV(2,ICCOUNT) = NC
            ENDIF
         ENDDO
C
C-----------------------------------------------------------------------
C           *** SORT THE ACTIVE FILE PARETO FRONT USING
C               THE FIRST FITNESS VALUE ***
C
         CALL SORT(NOBFUN,ICCOUNT,FITSAV,ISAV)
C
C-----------------------------------------------------------------------
C           *** REMOVE DUPLICATE VALUES FORM THE ACTIVE FILE 
C               PARETO FRONT AS WELL AS ANY ENTRIES THAT 
C               CONTAIN -99.0 (THE VALUES USED FOR 
C               INITIALIZATION ***
C
         NCCOUNT = ICCOUNT
         CALL SORT2(NOBFUN,NCCOUNT,ICCOUNT,FITSAV,ISAV)
C-----------------------------------------------------------------------
C           *** WRITE THE ACTIVE FILE TO active.dat ***
C
      namelen =  strlen(ga_file_prefix)
      filena = ga_file_prefix
      filena(namelen+1:namelen+7) = '.active'
         OPEN (4,FILE=filena,STATUS='UNKNOWN',FORM='FORMATTED')
         WRITE (4,'(2(8x,i6))') IPOP,ICCOUNT
         DO NC=1,ICCOUNT
            WRITE (4,'(2(8x,i6),(3((2x,e24.16))))') 
     $           ISAV(1,NC),ISAV(2,NC),
     $           (FITSAV(NO,NC),NO=1,NOBFUN)
         ENDDO
         CLOSE (4)
      ENDIF
C
C          *** SOLUTION WRITE COMPLETED ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
