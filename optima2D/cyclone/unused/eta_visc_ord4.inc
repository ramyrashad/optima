c        ***********************
c        F_eta_eta Viscous Terms
c        ***********************
c        
         xx=1.d0/8.d0
         yy=1.d0/24.d0
         zz=1.d0/16.d0
         do k=klow,kup-1
            km1 = k-1
            kp1 = k+1
            kp2 = k+2
            do j=jlow,jup
               xy3m1=xy(j,km1,3)
               xy3  =xy(j,k,3)
               xy3p1=xy(j,kp1,3)
               xy3p2=xy(j,kp2,3)
c
               xy4m1=xy(j,km1,4)
               xy4  =xy(j,k,4)
               xy4p1=xy(j,kp1,4)
               xy4p2=xy(j,kp2,4)
c
c              -h stands for half
c              -interpolate metrics to +1/2 (ph) nodes
               xy3ph    = zz*(-xy3m1+9.d0*(xy3+xy3p1)-xy3p2)
               xy4ph    = zz*(-xy4m1+9.d0*(xy4+xy4p1)-xy4p2)
c         
c              note: work(j,k) = fnu(j,k) + turre(j,k)
c              - interpolate work to +1/2 node
               workph(j,k)=zz*(-work(j,km1)+9.d0*(work(j,k)+work(j,kp1))
     &              - work(j,kp2))

c              -differentiate turre w.r.t eta
               dvph=(turre(j,km1)+27.d0*(turre(j,kp1)-turre(j,k))-
     &                                                 turre(j,kp2))*yy
c
               tmpx(j,k)=xy3ph*dvph
               tmpy(j,k)=xy4ph*dvph
            enddo
         enddo
c
c
         k=kbegin
         kp1 = k+1
         kp2 = k+2
         kp3 = k+3
         do j=jlow,jup
           xy3  =xy(j,k,3)
           xy3p1=xy(j,kp1,3)
           xy3p2=xy(j,kp2,3)
c     
           xy4  =xy(j,k,4)
           xy4p1=xy(j,kp1,4)
           xy4p2=xy(j,kp2,4)
c     
           tur  =turre(j,k)
           turp1=turre(j,kp1)
           turp2=turre(j,kp2)
           turp3=turre(j,kp3)
c     
           xy3ph    = xx*(3.d0*xy3 + 6.d0*xy3p1 - xy3p2)
           xy4ph    = xx*(3.d0*xy4 + 6.d0*xy4p1 - xy4p2)
c     
           workph(j,k)=xx*(3.d0*work(j,k)+6.d0*work(j,kp1)-work(j,kp2))
c
           dvph=yy*(-turp3 + 3.d0*turp2 + 21.d0*turp1 -23.d0*tur)
           
           tmpx(j,k)=xy3ph*dvph
           tmpy(j,k)=xy4ph*dvph
         enddo
c     
c        Outflow Boundary
c        -second-order
c         k=kup
c         kp1 = k+1
c         km1 = k-1
c         do j=jlow,jup
c           xy3  =xy(j,k,3)
c           xy3p1=xy(j,kp1,3)
cc     
c           xy4  =xy(j,k,4)
c           xy4p1=xy(j,kp1,4)
cc     
c           tur  =turre(j,k)
c           turp1=turre(j,kp1)
cc     
c           xy3ph    = 0.5d0*(xy3 + xy3p1)
c           xy4ph    = 0.5d0*(xy4 + xy4p1)
cc     
c           workph(j,k)=0.5d0*(work(j,kp1)+work(j,k))
cc           
c           dvph=(turp1-tur)
c           
c           tmpx(j,k)=xy3ph*dvph
c           tmpy(j,k)=xy4ph*dvph
c         enddo
c        -third-order
         k=kup
         kp1 = k+1
         km1 = k-1
         km2 = k-2
         do j=jlow,jup
           xy3m1=xy(j,km1,3)
           xy3  =xy(j,k,3)
           xy3p1=xy(j,kp1,3)
c     
           xy4m1=xy(j,km1,4)
           xy4  =xy(j,k,4)
           xy4p1=xy(j,kp1,4)
c     
           turm2=turre(j,km2)
           turm1=turre(j,km1)
           tur  =turre(j,k)
           turp1=turre(j,kp1)
c     
           xy3ph    = xx*(-xy3m1 + 6.d0*xy3 + 3.d0*xy3p1)
           xy4ph    = xx*(-xy4m1 + 6.d0*xy4 + 3.d0*xy4p1)
c     
           workph(j,k)=xx*(-work(j,km1)+6.d0*work(j,k)+3.d0*work(j,kp1))
c           
           dvph=yy*(turm2 - 3.d0*turm1 - 21.d0*tur + 23.d0*turp1)
           
           tmpx(j,k)=xy3ph*dvph
           tmpy(j,k)=xy4ph*dvph
         enddo
c
c
c        ***************************************************************
c        ***************************************************************

         do k=klow+1,kup-1
           km2=k-2
           km1=k-1
           kp1=k+1
           do j=jlow,jup
             txm2= tmpx(j,km2)
             txm1= tmpx(j,km1)
             tx  = tmpx(j,k)
             txp1= tmpx(j,kp1)
c
             tym2= tmpy(j,km2)
             tym1= tmpy(j,km1)
             ty  = tmpy(j,k)
             typ1= tmpy(j,kp1)
c
             wrkm2= workph(j,km2)
             wrkm1= workph(j,km1)
             wrk  = workph(j,k)
             wrkp1= workph(j,kp1)
c
             ztmp=(1.0+cb2)*resiginv
c
             fy(j,k) = (xy(j,k,3)*(wrkm2*txm2 -
     &           27.d0*(wrkm1*txm1 - wrk*tx) - wrkp1*txp1)
     &               + xy(j,k,4)*(wrkm2*tym2 -
     &           27.d0*(wrkm1*tym1 - wrk*ty) - wrkp1*typ1))*yy*ztmp
c
             ztmp=cb2*resiginv*work(j,k)
c
             fy(j,k) = fy(j,k) - (xy(j,k,3)*(txm2 -
     &           27.d0*(txm1 - tx) - txp1)
     &               + xy(j,k,4)*(tym2 -
     &           27.d0*(tym1 - ty) - typ1))*yy*ztmp
           enddo
         enddo
c
         k=klow
         km1=k-1
         kp1=k+1
         kp2=k+2
         do j=jlow,jup
           txm1= tmpx(j,km1)
           tx  = tmpx(j,k)
           txp1= tmpx(j,kp1)
           txp2= tmpx(j,kp2)
c     
           tym1= tmpy(j,km1)
           ty  = tmpy(j,k)
           typ1= tmpy(j,kp1)
           typ2= tmpy(j,kp2)
c     
           wrkm1= workph(j,km1)
           wrk  = workph(j,k)
           wrkp1= workph(j,kp1)
           wrkp2= workph(j,kp2)
c     
           ztmp=(1.0+cb2)*resiginv
c     
           fy(j,k) = (xy(j,k,3)*(-23.d0*wrkm1*txm1 
     &      + 21.d0*wrk*tx + 3.d0*wrkp1*txp1 - wrkp2*txp2)
     &               + xy(j,k,4)*(-23.d0*wrkm1*tym1 
     &      + 21.d0*wrk*ty + 3.d0*wrkp1*typ1 - wrkp2*typ2))*yy*ztmp
c     
           ztmp=cb2*resiginv*work(j,k)
c
           fy(j,k) = fy(j,k) - (xy(j,k,3)*(-23.d0*txm1 
     &         + 21.d0*tx + 3.d0*txp1 - txp2)
     &               + xy(j,k,4)*(-23.d0*tym1 
     &         + 21.d0*ty + 3.d0*typ1 - typ2))*yy*ztmp
         enddo
c
c        Outflow Boundary
c        -second-order         
         k=kup
         km1=k-1
         kp1=k+1
         do j=jlow,jup
           txm1= tmpx(j,km1)
           tx  = tmpx(j,k)
c     
           tym1= tmpy(j,km1)
           ty  = tmpy(j,k)
c     
           wrkm1= workph(j,km1)
           wrk  = workph(j,k)
c     
           ztmp=(1.0+cb2)*resiginv
c     
           fy(j,k) =  (xy(j,k,3)*(wrk*tx - wrkm1*txm1)
     &             +  xy(j,k,4)*(wrk*ty - wrkm1*tym1))*ztmp

c     
           ztmp=cb2*resiginv*work(j,k)
c
           fy(j,k) = fy(j,k) - (xy(j,k,3)*(tx-txm1)
     &                       +  xy(j,k,4)*(ty-tym1))*ztmp
         enddo
c
c        -third-order
c         k=kup
c         km1=k-1
c         km2=k-2
c         km3=k-3
c         do j=jlow,jup
c           txm3= tmpx(j,km3)
c           txm2= tmpx(j,km2)
c           txm1= tmpx(j,km1)
c           tx  = tmpx(j,k)
cc     
c           tym3= tmpy(j,km3)
c           tym2= tmpy(j,km2)
c           tym1= tmpy(j,km1)
c           ty  = tmpy(j,k)
cc     
c           wrkm3= workph(j,km3)
c           wrkm2= workph(j,km2)
c           wrkm1= workph(j,km1)
c           wrk  = workph(j,k)
cc     
c           ztmp=(1.0+cb2)*resiginv
cc     
c           fy(j,k) =  fy(j,k) + yy*ztmp*(xy(j,k,3)*
c     & (23.d0*wrk*tx - 21.d0*wrkm1*txm1 - 3.d0*wrkm2*txm2 + wrkm3*txm3)
c     &                        +  xy(j,k,4)*
c     & (23.d0*wrk*ty - 21.d0*wrkm1*tym1 - 3.d0*wrkm2*tym2 + wrkm3*tym3))
cc     
c           ztmp=cb2*resiginv*work(j,k)
cc
c           fy(j,k) =  fy(j,k) - yy*ztmp*(xy(j,k,3)*
c     &           (23.d0*tx - 21.d0*txm1 - 3.d0*txm2 + txm3)
c     &                        +  xy(j,k,4)*
c     &           (23.d0*ty - 21.d0*tym1 - 3.d0*tym2 + tym3))
c         enddo
c
c        ***************************************************************
c        ***************************************************************
         do k=klow,kup
           kp1=k+1
           km1=k-1
           do j=jlow,jup
c
c            ************************************
c            -use second-order for implicit side
             xy3p     = .5*(xy(j,k,3)+xy(j,kp1,3))
             xy4p     = .5*(xy(j,k,4)+xy(j,kp1,4))
             ttp      =  (xy3p*xy(j,k,3)+xy4p*xy(j,k,4))
             
             xy3m     = .5*(xy(j,k,3)+xy(j,km1,3))
             xy4m     = .5*(xy(j,k,4)+xy(j,km1,4))
             ttm      =  (xy3m*xy(j,k,3)+xy4m*xy(j,k,4))
             
             cnud=cb2*resiginv*work(j,k)
             
             cdp       =    ttp*cnud
             cdm       =    ttm*cnud
             
             trem =.5*(work(j,km1)+work(j,k))
             trep =.5*(work(j,k)+work(j,kp1))
             
             cap  =  ttp*trep*(1.0+cb2)*resiginv
             cam  =  ttm*trem*(1.0+cb2)*resiginv
             
             by(j,k)   = cdm-cam
             cy(j,k)   = -cdp+cap-cdm+cam
             dy(j,k)   = cdp-cap
c
c             by(j,k)   = 0.d0
c             cy(j,k)   = 0.d0
c             dy(j,k)   = 0.d0
c            ************************************
           enddo
         enddo
         
