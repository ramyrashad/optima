#!/bin/csh
#
echo "Split the file."
#
mkdir temp
cd temp
\rm *
../f90split ../latin_random.f90
#
echo "Compile the routines."
#
foreach FILE (*.f90)
  G95_compiler -c $FILE >& compiler.out
  if ( $status != 0 ) then
    echo "Errors compiling " $FILE
    exit
  endif
  \rm compiler.out
end
\rm *.f90
#
echo "Create the archive."
ar qc liblatin_random.a *.o
\rm *.o
#
echo "Store the archive."
mv liblatin_random.a ../.
cd ..
\rm -r temp
#
echo "A new version of latin_random has been created."
