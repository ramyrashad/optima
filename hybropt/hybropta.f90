!********************************************************************
!
! Purpose:
!     This is an interface between HYBROPT optimizer and
!     Diablo or OPTIMA2D. The only operation here is to call
!     the optimize() subroutine in Hybropt_Mod which coordinates
!     all the optimization work.
!
! Author: 
!     Oleg Chernukhin, May 2010
!
!********************************************************************
subroutine hybrOptA(husrfun, &
                    nchr, &
                    nppc, &
                    carr, &
                    cid, &
                    hybr_meth, &
                    sobol_file, &
                    usedb, &
                    mingen, &
                    maxgen, &
                    ranseed, &
                    selmethod, &
                    prob_pert, &
                    prob_mut, &
                    prob_avg, &
                    pen, &
                    xbest, &
                    xavg, &
                    xpert, &
                    xmut, &
                    sn_maj_lim, &
                    flow_sol_lim, &
                    sample_offset, &
                    cig, &
                    optf_output, &
                    opt_info_unit, &
                    opt_his_unit, &
                    opt_def_unit, &
                    dpfile_unit, &
                    glob_comm, &
                    lb_tol, &
                    sampl_seq_unit, &
                    csampl)

   use Hybropt_Mod

   implicit none

   !___________________________________
   !-- Input parameters

   external :: husrfun !-- no interface, since it is just passed along
   integer :: opt_info_unit, opt_his_unit, nppc, nchr, cid, hybr_meth,&
              maxgen, ranseed, selmethod, sn_maj_lim, flow_sol_lim, &
              sample_offset, opt_def_unit, optf_output, dpfile_unit, &
              glob_comm, sampl_seq_unit, mingen
   integer :: carr(nchr)
   real(kind=dp) :: prob_pert, prob_mut, &
                    prob_avg, xbest, xavg, xpert, xmut, pen, lb_tol
   character(128) :: sobol_file
   logical :: cig, usedb, csampl
   
   !___________________________________
   !-- Begin execution

   call optimize(husrfun, nchr, nppc, carr, cid, hybr_meth, sobol_file, &
                 usedb, mingen, maxgen, ranseed, selmethod, prob_pert, &
                 prob_mut, prob_avg, pen, xbest, xavg, xpert, xmut, &
                 sn_maj_lim, flow_sol_lim, sample_offset, cig, &
                 optf_output, opt_info_unit, opt_his_unit, &
                 opt_def_unit, dpfile_unit, glob_comm, lb_tol, &
                 sampl_seq_unit, csampl)

end subroutine
