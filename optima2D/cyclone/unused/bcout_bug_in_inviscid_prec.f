      subroutine bcout(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,k1,k2,
     &                  jbc,idir)              
c
      include '../include/arcom.inc'
c                                                                       
      dimension duqinf(4),duqext(4),duqbar(4),duqdif(4)
      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c
      dimension vinff(4),vint(4),vinfp(4),vintp(4),qbar(4),qdif(4)
      dimension vec(4),xx(4,4),tmp(4)
c     -far field bc for o mesh outer boundary and c mesh k = kmax          
c                                                                       
c   .....................................................................  
c     Far field circulation based on potential vortex added to reduce      
c     the dependency on outer boundary location.  works well.  for         
c     instance, a naca0012 at m=0.63 a=2. shows no dependency on ob from   
c     4 - 96 chords.                                                       
c   .....................................................................  
c                                                                       
      gi = 1.d0/gamma                                                 
      gm1i = 1.d0/gami                                                
c                                                                    
      alphar = alpha*pi/180.d0
      cosang = cos( alphar )                                         
      sinang = sin( alphar )                                         
      ainf = sqrt(gamma*pinf/rhoinf)                                 
      hstfs = 1.d0/gami + 0.5d0*fsmach**2                                
c                                                                       
c                                                                       
      j = jbc
      jq= jbc
c     The normal points in the direction of increasing xi location ...
c     So Vn is positive for outflow at j=jmax and negative at j=1.
      sgn= -sign(1.d0,float(idir))
      jadd = sign(1,idir)
c
      if(.not.periodic .and. .not.viscous)then                          
         if (prec.eq.0) then
            do 60 k = k1,k2                                                
c                                                                   
c              -reset free stream values with circulation correction       
c              -this if statement is here so that when I test for uniform
c               flow convergence I don't get a floating point exception when
c               using this routine for the body too.
c              -circul should be set to false when testing uniform flow.
               if (circul) then
                  xa = x(j,k) - chord/4.d0
                  ya = y(j,k)                                            
                  radius = sqrt(xa**2+ya**2)                               
                  angl = atan2(ya,xa)                                      
                  cjam = cos(angl)                                         
                  sjam = sin(angl)                                        
                  qcirc=circb
     &                     /(radius*(1.d0-(fsmach*sin(angl-alphar))**2))  
               else
                  qcirc=0.d0
                  sjam=0.d0
                  cjam=0.d0
               endif
               uf = uinf + qcirc*sjam                                    
               vf = vinf - qcirc*cjam                                     
               af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))         
               af = sqrt(af2)        
c                                                                       
c              -metrics for normals
               rx=xy(j,k,1)
               ry=xy(j,k,2)
               rxy2=1.d0/sqrt(rx**2+ry**2)
c               print *,'rxy2=',j,rxy2
               rx=rx*rxy2*sgn
               ry=ry*rxy2*sgn
c        
c              -get extrapolated variables                                
               if (iord.eq.4) then
                  jq=j+jadd
                  xyj2 = xyj(jq,k)                                         
                  rho2 = q(jq,k,1)*xyj2                                   
                  rinv = 1.d0/q(jq,k,1)                                    
                  u2 = q(jq,k,2)*rinv                                     
                  v2 = q(jq,k,3)*rinv
                  e2 = q(jq,k,4)*xyj2
                  p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c        
                  jq=j+2*jadd
                  xyj3 = xyj(jq,k)                                       
                  rho3 = q(jq,k,1)*xyj3
                  rinv = 1.d0/q(jq,k,1)                                   
                  u3 = q(jq,k,2)*rinv                                     
                  v3 = q(jq,k,3)*rinv                                     
                  e3 = q(jq,k,4)*xyj3
                  p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c                 
                  jq=j+3*jadd
                  xyj4 = xyj(jq,k)                                       
                  rho4 = q(jq,k,1)*xyj4
                  rinv = 1.d0/q(jq,k,1)                                   
                  u4 = q(jq,k,2)*rinv                                     
                  v4 = q(jq,k,3)*rinv                                     
                  e4 = q(jq,k,4)*xyj4
                  p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c                 
c                  jq=j+4*jadd
c                  xyj5 = xyj(jq,k)                                       
c                  rho5 = q(jq,k,1)*xyj5
c                  rinv = 1.d0/q(jq,k,1                                   
c                  u5 = q(jq,k,2)*rinv                                   
c                  v5 = q(jq,k,3)*rinv                                     
c                  e5 = q(jq,k,4)*xyj5
c                  p5 = gami*(e5 - 0.5d0*rho5*(u5**2+v5**2))
c                 
c                 -3rd order extrapolation
c                  rhoext= 4.*(rho2+rho4) - 6.*rho3 -rho5
c                  uext= 4.*(u2+u4) -6.*u3 -u5
c                  vext= 4.*(v2+v4) -6.*v3 -v5
c                  pext= 4.*(p2+p4) -6.*p3 -p5
c                 -2nd order extrapolation
                  rhoext= 3.*(rho2 - rho3) + rho4
                  uext= 3.*(u2 - u3) + u4
                  vext= 3.*(v2 - v3) + v4
                  pext= 3.*(p2 - p3) + p4
c                 -1st order extrapolation
c                  rhoext= 2.*rho2 - rho3
c                  uext= 2.*u2 - u3
c                  vext= 2.*v2 - v3
c                  pext= 2.*p2 - p3
c                 -Zeroth order
c                  rhoext= rho2
c                  uext= u2
c                  vext= v2
c                  pext= p2               
               else
                  jq=j+jadd
                  xyj2 = xyj(jq,k)                                         
                  rho2 = q(jq,k,1)*xyj2                                   
                  rinv = 1.d0/q(jq,k,1)                               
                  u2 = q(jq,k,2)*rinv                                      
                  v2 = q(jq,k,3)*rinv
                  e2 = q(jq,k,4)*xyj2
                  p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c        
c                  jq=j+2*jadd
c                  xyj3 = xyj(jq,k)                                       
c                  rho3 = q(jq,k,1)*xyj3
c                  rinv = 1.d0/q(jq,k,1)                                   
c                  u3 = q(jq,k,2)*rinv                         
c                  v3 = q(jq,k,3)*rinv                                    
c                  e3 = q(jq,k,4)*xyj3
c                  p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c
c                 -1st order extrapolation
c                  rhoext= 2.*rho2 - rho3
c                  uext= 2.*u2 - u3
c                  vext= 2.*v2 - v3
c                  pext= 2.*p2 - p3
c                 -Zeroth order
                  rhoext= rho2
                  uext= u2
                  vext= v2
                  pext= p2               
               endif
               entext=pext/rhoext**gamma
               hext= gamma*pext/rhoext/gami + 0.5d0*(uext**2+vext**2)
               a2ext= gamma*pext/rhoext
c                                                        
c              -calc conserved and primitive variables for both
c               infinity and interior values
c        
c              -conserved variables at inf
c              -note that entinf=1/gamma so  
c                          rhoinf=(ainf**2/(gamma*entinf))**gm1i
               vinff(1)= af2**gm1i
               vinff(2)= vinff(1)*uf
               vinff(3)= vinff(1)*vf
               pf     = vinff(1)*af2*gi
               vinff(4)=pf*gm1i+0.5d0*(uf**2+vf**2)*vinff(1)
c        
c              -primitive variables at inf
c               vinfp(1)= vinff(1)
c               vinfp(2)= uf
c               vinfp(3)= vf
c               vinfp(4)= pf
c        
c              -conserved variables extrapolated from interior
c              -formula used for vint(1) here is equivalent to rhoext
c                  vint(1)= (a2ext/(gamma*entext))**gm1i
c                  vint(1)= a2ext**gm1i
               vint(1)= rhoext
               vint(2)= vint(1)*uext
               vint(3)= vint(1)*vext
               vint(4)= pext*gm1i +0.5d0*(uext**2+vext**2)*vint(1)
c        
c              -primitve variables extrapolated from interior
c               vintp(1)= vint(1)
c               vintp(2)= uext
c               vintp(3)= vext
c               vintp(4)= pext
c        
c        
c              -form simple average (conservative variables)
               qbar(1)= 0.5d0*(vinff(1) + vint(1))
               qbar(2)= 0.5d0*(vinff(2) + vint(2))
               qbar(3)= 0.5d0*(vinff(3) + vint(3))
               qbar(4)= 0.5d0*(vinff(4) + vint(4))
c        
c              -form difference (conservative variables)
               qdif(1)= vinff(1) - vint(1)
               qdif(2)= vinff(2) - vint(2)
               qdif(3)= vinff(3) - vint(3)
               qdif(4)= vinff(4) - vint(4)
c        
               rho=qbar(1)
               rhoinv=1.d0/rho
               u=qbar(2)*rhoinv
               v=qbar(3)*rhoinv
               e=qbar(4)
               p=gami*(e-0.5d0*rho*(u**2+v**2))
               a2bar=gamma*p*rhoinv
               ca=dsqrt(a2bar)
               snr=1.d0/ca
c        
c              *******************************************************
c              -multiply by Tkinv
               bt=1.d0/sqrt(2.d0)
               ucap=xy(j,k,1)*u + xy(j,k,2)*v
c               ucap=rx*u + ry*v
               vtot2=0.5d0*(u**2+v**2)
               beta=bt*rhoinv*snr
               ryu=ry*u
               rxu=rx*u
               ryv=ry*v
               rxv=rx*v
               
               t1= gami*(u*qdif(2) + v*qdif(3) - qdif(4)-vtot2*qdif(1))
               
               vec(1) =qdif(1) + t1*snr**2
               vec(2) =((-ryu+rxv)*qdif(1)+ry*qdif(2)-rx*qdif(3))*rhoinv
               
               t1= t1*beta
               t2= ((rxu+ryv)*qdif(1) - rx*qdif(2)-ry*qdif(3))*bt*rhoinv
               
               vec(3) = -(t1+t2)
               vec(4) = -(t1-t2)
c              *******************************************************
c              
c              -multipy by sign of eigenvalue matrix
               t1=sign(1.d0,ucap)
               vec(1) = t1*vec(1)
               vec(2) = t1*vec(2)
               vec(3) = sign(1.d0,ucap+ca)*vec(3)
               vec(4) = sign(1.d0,ucap-ca)*vec(4)
c              
c              *******************************************************
c              -multiply by Tk
               alp=rho*bt*snr
               t1= alp*(vec(3)+vec(4))
               t2= rho*bt*(vec(3)-vec(4))
               t3= rho*vec(2)
               t4= alp*t1
               tmp(1)= vec(1) + t1
               tmp(2)= vec(1)*u + ry*t3 + u*t4 + rx*t2
               tmp(3)= vec(1)*v - rx*t3 + v*t4 + ry*t2
               tmp(4)= vtot2*vec(1) + (ryu - rxv)*t3 +
     &                  (vtot2+ca**2*gm1i)*t1 + (rxu + ryv)*t2
c               tmp(2)= vec(1)*u + rho*ry*vec(2) + alp*u*t1 + rx*t2
c               tmp(3)= vec(1)*v - rho*rx*vec(2) + alp*v*t1 + ry*t2
c               tmp(4)= vtot2*vec(1) + rho*(ry*u - rx*v)*vec(2) +
c     &                  (vtot2+ca**2*gm1i)*t1 + (rx*u + ry*v)*t2
c              
c              *******************************************************
c              
               tmp(1)=qbar(1)-0.5d0*tmp(1)
               tmp(2)=qbar(2)-0.5d0*tmp(2)
               tmp(3)=qbar(3)-0.5d0*tmp(3)
               tmp(4)=qbar(4)-0.5d0*tmp(4)
c
c              -add jacobian                                             
               rjj = 1.d0/xyj(j,k)                                       
               q(j,k,1) = tmp(1)*rjj
               q(j,k,2) = tmp(2)*rjj                             
               q(j,k,3) = tmp(3)*rjj
               q(j,k,4) = tmp(4)*rjj
60          continue                                                  
         else
c        
            if (prec.ne.3) then
               print *,'BCFAR only written for prec=3'
               stop
            endif
c        
c        
cdu         -use the preconditioned characteristics to solve the bc's
c        
            dulimit=prphi*fsmach**2
c        
            do 1000 k=k1,k2
c        
c             -do circulation correction
               xa = x(j,k) - chord/4.                                   
               ya = y(j,k)                                                 
               radius = sqrt(xa**2+ya**2)                                  
               angl = atan2(ya,xa)                                         
               cjam = cos(angl)                                            
               sjam = sin(angl)                                            
               qcirc =circb/(radius*(1.d0-(fsmach*SIN(ANGL-ALPHAR))**2))  
               uf = uinf + qcirc*sjam                                      
               vf = vinf - qcirc*cjam                                     
               af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))                    
               af = sqrt(af2)                                             
c        
c             -calculate values for duqinf
               duqinf(1)=af2**gm1i
               duqinf(2)=duqinf(1)*uf
               duqinf(3)=duqinf(1)*vf
               dupinf=duqinf(1)*af2*gi
               duqinf(4)=dupinf*gm1i+0.5d0*(uf**2+vf**2)*duqinf(1)
c        
c          -calculate values for duqext (no Jacobian in these)
c            if ((j.lt.(j2-3)).and.(j.gt.(j1+3))) then
c               duqext(1)=2.0*q(j,kq-1,1)*xyj(j,k-1)-q(j,kq-2,1)*xyj(j,k-2)
c               duqext(2)=2.0*q(j,kq-1,2)*xyj(j,k-1)-q(j,kq-2,2)*xyj(j,k-2)
c               duqext(3)=2.0*q(j,kq-1,3)*xyj(j,k-1)-q(j,kq-2,3)*xyj(j,k-2)
c               duqext(4)=2.0*q(j,kq-1,4)*xyj(j,k-1)-q(j,kq-2,4)*xyj(j,k-2)
c            else
               duqext(1)=q(j+jadd,k,1)*xyj(j+jadd,k)
               duqext(2)=q(j+jadd,k,2)*xyj(j+jadd,k)
               duqext(3)=q(j+jadd,k,3)*xyj(j+jadd,k)
               duqext(4)=q(j+jadd,k,4)*xyj(j+jadd,k)
c            endif
c        
c             -form simple average
               duqbar(1)=0.5d0*(duqinf(1)+duqext(1))
               duqbar(2)=0.5d0*(duqinf(2)+duqext(2))
               duqbar(3)=0.5d0*(duqinf(3)+duqext(3))
               duqbar(4)=0.5d0*(duqinf(4)+duqext(4))
c        
c             -form difference
               duqdif(1)=0.5d0*(duqinf(1)-duqext(1))
               duqdif(2)=0.5d0*(duqinf(2)-duqext(2))
               duqdif(3)=0.5d0*(duqinf(3)-duqext(3))
               duqdif(4)=0.5d0*(duqinf(4)-duqext(4))
c        
c             -calculate properties at mean state
               durho=duqbar(1)
               duu=duqbar(2)/durho
               duv=duqbar(3)/durho
               duuv2=duu**2+duv**2
               dup=gami*(duqbar(4)-0.5d0*(duuv2)*durho)
               duc2=gamma*dup/durho
               duc=sqrt(duc2)
               duma2=duuv2/duc2
c        
               duw=xy(j,k,1)**2+xy(j,k,2)**2
               dul=xy(j,k,3)**2+xy(j,k,4)**2
c        
               dulim=max(dulimit,prxi*rhoinf*sqrt(max(duw,dul))
     +              /re/durho/fsmach)
               due=min(1.d0,max(duma2,dulim))
c               due=1.d0
c        
c  used to be
               dukx=xy(j,k,3)
               duky=xy(j,k,4)
c               dukx=xy(j,k,1)*sgn
c               duky=xy(j,k,2)*sgn
               duuk=dukx*duu+duky*duv
c        
               dua=(1.d0+due)/2.0*duuk
               dub=sqrt(((1.d0-due)*duuk/2.0)**2+due*dul*duc2)
c        
c               dul=sqrt(duw)
c               dukx=dukx/duw
c               duky=duky/duw
               dul=sqrt(dul)
               dukx=dukx/dul
               duky=duky/dul
c        
c             -multiply by Minv
               du4=gami*(duuv2/2.0*duqdif(1)-duu*duqdif(2)-
     +              duv*duqdif(3)+duqdif(4))
               du1=du4/durho/duc
               du4=du4-duc2*duqdif(1)
               du2=(duqdif(2)-duu*duqdif(1))/durho
               du3=(duqdif(3)-duv*duqdif(1))/durho
c        
c             -multiply by Tkinv
               dut1=du4
               dut2=duky*du2-dukx*du3
               dut3=(dul*duc*du1+(duuk-dua+dub)*
     +              (dukx*du2+duky*du3))/dub/2.0
               dut4=(dul*duc*du1+(duuk-dua-dub)*
     +              (dukx*du2+duky*du3))/dub/2.0
c        
c             -multiply by sign(Lambda)
               du1=sign(1.d0,duuk)*dut1
               du2=sign(1.d0,duuk)*dut2
               du3=sign(1.d0,dua+dub)*dut3
               du4=sign(1.d0,dua-dub)*dut4
c        
c             -multiply by Tk
               dut1=((dua+dub-duuk)*du3+(duuk-dua+dub)*du4)/dul/duc
               dut2=duky*du2+dukx*(du3-du4)
               dut3=-dukx*du2+duky*(du3-du4)
               dut4=du1
c        
c             -multiply by M
               du1=(durho*dut1-dut4/duc)/duc
               du2=duu*du1+durho*dut2
               du3=duv*du1+durho*dut3
               du4=durho*((duuv2/2.0/duc+duc/gami)*dut1+
     +              duu*dut2+duv*dut3)-duuv2/2.0/duc2*dut4
c        
c             -calculate the boundry Q
               dut1=duqbar(1)-du1
               dut2=duqbar(2)-du2
               dut3=duqbar(3)-du3
               dut4=duqbar(4)-du4
c        
c             -apply
               q(j,k,1)=dut1/xyj(j,k)
               q(j,k,2)=dut2/xyj(j,k)
               q(j,k,3)=dut3/xyj(j,k)
               q(j,k,4)=dut4/xyj(j,k)
 1000       continue
         endif
      elseif(.not.periodic .and. viscous)then                           
c                                                                       
c        extrapolation on viscous outflow with pressure extrapolated      
c                                                                       
c        loop from kbegin to kend
         if (iord.eq.2) then
            do 250 k = k1,k2                                        
               jq=j+jadd
               rrj = xyj(jq,k)/xyj(j,k)                                  
               q(j,k,1) = q(jq,k,1)*rrj                                
               q(j,k,2) = q(jq,k,2)*rrj                                
               q(j,k,3) = q(jq,k,3)*rrj                                
               ppp = gami*(q(jq,k,4) -                                  
     *                0.5d0*(q(jq,k,2)**2+q(jq,k,3)**2)/q(jq,k,1) )   
               q(j,k,4) = ppp*gm1i*rrj +                                 
     *                   0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)        
250         continue
         else
            do 255 k = k1,k2                                        
c
               jq=j+jadd
               xyj2 = xyj(jq,k)                                           
               rho2 = q(jq,k,1)*xyj2                                     
               rinv = 1.d0/q(jq,k,1)                                      
               u2 = q(jq,k,2)*rinv                                        
               v2 = q(jq,k,3)*rinv
               e2 = q(jq,k,4)*xyj2
               p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c              
               jq=j+2*jadd
               xyj3 = xyj(jq,k)                                           
               rho3 = q(jq,k,1)*xyj3
               rinv = 1.d0/q(jq,k,1)                                
               u3 = q(jq,k,2)*rinv                                        
               v3 = q(jq,k,3)*rinv                                       
               e3 = q(jq,k,4)*xyj3
               p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c              
               jq=j+3*jadd
               xyj4 = xyj(jq,k)                                          
               rho4 = q(jq,k,1)*xyj4
               rinv = 1.d0/q(jq,k,1)                                       
               u4 = q(jq,k,2)*rinv                                        
               v4 = q(jq,k,3)*rinv                                       
               e4 = q(jq,k,4)*xyj4
               p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c
c              -zeroeth-order extrapolation
c               or setting d(var)/dn=0 to first-order
c                rhoext=rho2
c                uext=  u2
c                vext=  v2
c                pext=  p2
c              -first-order extraplation
c               rhoext=2.d0*rho2-rho3
c               uext=  2.d0*u2-u3    
c               vext=  2.d0*v2-v3    
c               pext=  2.d0*p2-p3
c              -second-order 
c               rhoext= 3.*(rho2 - rho3) + rho4
c               uext= 3.*(u2 - u3) + u4
c               vext= 3.*(v2 - v3) + v4
c               pext= 3.*(p2 - p3) + p4
c
c              -set d(var)/dn =0 to second-order
               third=1.d0/3.d0
               rhoext=(4.d0*rho2-rho3)*third
               uext=  (4.d0*u2 - u3)*third
               vext=  (4.d0*v2 - v3)*third
               pext=  (4.d0*p2 - p3)*third
c                       
               rrj=1.d0/xyj(j,k)
               q(j,k,1) = rhoext*rrj                                
               q(j,k,2) = uext*q(j,k,1)
               q(j,k,3) = vext*q(j,k,1)
               ppp = pext*rrj
               q(j,k,4) = ppp*gm1i +                                 
     *                   0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)        
 255        continue
         endif                                                      
      endif
c
      return                                                            
      end                                                               
