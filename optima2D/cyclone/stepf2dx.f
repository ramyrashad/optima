      subroutine stepf2dx(jdim,kdim,q,turmu,fmu,s,press,sndsp,xy,xyj,x,
     &      y,ds,coef2,coef4,uu,cc,work,spect,precon,akk)     
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension ds(jdim,kdim), turmu(jdim, kdim), fmu(jdim,kdim)        
c                                                                       
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension uu(jdim,kdim),cc(jdim,kdim),spect(jdim,kdim,3)    
c                                                                       
      dimension work(jdim,kdim,10),precon(jdim,kdim,6)
c                                                                       
c     CUSP dissipation variables:
      integer en, nlow
      double precision swp1,swm1,cp1,cm1,javep1,javem1
      double precision akk
c
c     note : d used for vectorization                                  
c                                                                       
      dtd   = dt * thetadt/(1.d0 + phidt)                              
      hd    = 0.5d0*dtd                                                
c                                                                       
c     dissipation coefficients for implicit side                        
c                                                                       
c                                                                       
c     nonlinear coef's contain dt                                      
c     need to add theta scaling                                        
c                                                                       
      smudt = smuim*dt* thetadt/(1.d0 + phidt)                           
c                                                                 
c     xi direction                                                      
c                                                                       
c     -fill matrix for inversion for diagonal in xi                      
c     -set switches for eigenvalues                                      
c     -do first two at the same time since they have the
c      same coefficients  
c
      if (idmodel.eq.2 .and. cmesh) then
         call mspectx(jdim,kdim,q,uu,cc,xyj,xy,spect,sndsp)
      end if
c
c     -if CUSP dissipation and full limiters -> must loop over all
c      state variables. 
      if ( (idmodel.eq.4) .and. (limiter.gt.3) ) then
         nlow = 1
      else 
         nlow = 2
      endif
c
      do 300 n = nlow,4 
        if ( n .eq. 1) sn = 0.
        if ( n .eq. 2) sn = 0.                                      
        if ( n .eq. 3) sn = 1.                                      
        if ( n .eq. 4) sn = -1.
        nm=n-1

      ! ________________________________________________________________
      ! Dissipation Models

        if (idmodel .eq. 1) then
c         **********************************************************
c         **                   JST diss model                     **
c         **********************************************************
c     
c         Interior Nodes
c
          do k = klow,kup                                         
          do j = jlow,jup                                         
            jp1 = jplus(j)                                            
            jm1 = jminus(j)                                          
            jp2 = jplus(jp1)                                         
            jm2 = jminus(jm1)                                         
            c2m = coef2(jm1,k)*dtd                                   
            c4m = coef4(jm1,k)*dtd                                   
            c2 = coef2(j,k)*dtd                                       
            c4 = coef4(j,k)*dtd                                       
            work(j,k,1) =  xyj(jm2,k)*c4m                           
            work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)           
            work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)             
            work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)             
            work(j,k,5) = xyj(jp2,k)*c4                              
          enddo
          enddo
c     
c         Boundary
c
          if ( .not.periodic ) then
            j = jlow                                                 
            jp1 = jplus(j)                                           
            jm1 = jminus(j)                                          
            jp2 = jplus(jp1)                                         
            do k = klow,kup                                   
              c2m = coef2(jm1,k)*dtd                                 
              c4m = coef4(jm1,k)*dtd                              
              c2 = coef2(j,k)*dtd                                 
              c4 = coef4(j,k)*dtd                                 
              work(j,k,1) =  0.                                     
              work(j,k,2) = -(c2m + c4m + c4)*xyj(jm1,k)            
              work(j,k,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)           
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)        
              work(j,k,5) = xyj(jp2,k)*c4                          
            enddo
c     
c         Boundary
c
            j = jup                                                  
            jp1 = jplus(j)                                           
            jm1 = jminus(j)                                          
            jm2 = jminus(jm1)                                        
            do k = klow,kup                                   
              c2m = coef2(jm1,k)*dtd                                
              c4m = coef4(jm1,k)*dtd                                
              c2 = coef2(j,k)*dtd                                  
              c4 = coef4(j,k)*dtd                                  
              work(j,k,1) =  xyj(jm2,k)*c4m                         
              work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)          
              work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)         
              work(j,k,4) = -(c2 + c4 + c4m)*xyj(jp1,k)             
              work(j,k,5) = 0.                                      
            enddo
          endif                 !(not periodic) 
        elseif (idmodel.eq.2) then
c         **********************************************************
c         **                  matrix diss model                   **
c         **********************************************************
          do k = klow,kup
          do j = jbegin,jup
            jp1=j+1
            work(j,k,7)=dtd*(coef2(j,k)*spect(j,k,nm)
     &                     + coef2(jp1,k)*spect(jp1,k,nm))
            work(j,k,8)=dtd*(coef4(j,k)*spect(j,k,nm)
     &                     + coef4(jp1,k)*spect(jp1,k,nm))
          enddo
          enddo

          do k = klow,kup     
          do j = jlow,jup
            jp1 = jplus(j)
            jm1 = jminus(j)             
            jp2 = jplus(jp1)   
            jm2 = jminus(jm1)          

            c2m = work(jm1,k,7)
            c2 = work(j,k,7)
            c4m = work(jm1,k,8)
            c4 = work(j,k,8)
            work(j,k,1) =  xyj(jm2,k)*c4m   
            work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k) 
            work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4) 
            work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
            work(j,k,5) = xyj(jp2,k)*c4   
          enddo
          enddo
c
          if ( .not.periodic ) then
            if (iord.eq.2) then
              j = jlow                                                  
              jp1 = jplus(j)                                            
              jm1 = jminus(j)                                           
              jp2 = jplus(jp1)                                          
              do k = klow,kup                                     
                c2m = work(jm1,k,7)
                c2 = work(j,k,7)
                c4m = work(jm1,k,8)
                c4 = work(j,k,8)
c     
                work(j,k,1) =  0.                                 
                work(j,k,2) = -(c2m + c4m + c4)*xyj(jm1,k)      
                work(j,k,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)     
                work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)     
                work(j,k,5) = xyj(jp2,k)*c4
              enddo
              j = jup                                                 
              jp1 = jplus(j)                                         
              jm1 = jminus(j)                                       
              jm2 = jminus(jm1)                                      
              do k = klow,kup                                     
                c2m = work(jm1,k,7)
                c2 = work(j,k,7)
                c4m = work(jm1,k,8)
                c4 = work(j,k,8)
c
                work(j,k,1) =  xyj(jm2,k)*c4m                     
                work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)    
                work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)      
                work(j,k,4) = -(c2 + c4 + c4m)*xyj(jp1,k)
                work(j,k,5) = 0.                                   
              enddo
            else                ! 4th-order
              j = jlow                                                  
              jp1 = jplus(j)                                            
              jm1 = jminus(j)                                           
              jp2 = jplus(jp1)                                          
              do k = klow,kup                                    
                c2m = work(jm1,k,7)
                c2 = work(j,k,7)
                c4m = work(jm1,k,8)
                c4 = work(j,k,8)
c     
                work(j,k,1) =  0.                                   
                work(j,k,2) = -(c2m + c4)*xyj(jm1,k)              
                work(j,k,3) = xyj(j,k)*(c2m+c4m+c2+2.d0*c4)     
                work(j,k,4) = -(c2 + c4 + 2.d0*c4m)*xyj(jp1,k)
                work(j,k,5) = xyj(jp2,k)*c4m                     
              enddo
              j = jup                                               
              jp1 = jplus(j)                                          
              jm1 = jminus(j)                                         
              jm2 = jminus(jm1)                                       
              do k = klow,kup                                     
                c2m = work(jm1,k,7)
                c2 = work(j,k,7)
                c4m = work(jm1,k,8)
                c4 = work(j,k,8)
c
                work(j,k,1) =  xyj(jm2,k)*c4m                   
                work(j,k,2) = -(c2m + 2.*c4m + c4)*xyj(jm1,k)    
                work(j,k,3) = xyj(j,k)*(c2m+c4m+c2+2.d0*c4)     
                work(j,k,4) = -(c2 + c4)*xyj(jp1,k)
                work(j,k,5) = 0.d0                                  
              enddo
            endif               !order
          endif                 !(not periodic)
        elseif (idmodel.eq.4) then
c         **********************************************************
c         **                   cusp diss model                    **
c         **********************************************************
          if ( n .ge. 2 ) then
            en = nm
          else
            en = n
          endif

c         -for full limiting, must recalculate the limiter values.
c         -this is a speed penalty, but saves memory.
c         -the full limiters are slow anyway, using precon for temp
c          storage.
          if (limiter.gt.3 .and. prec.eq.0) 
     &      call dclimx (jdim,kdim,q,xyj,x,y,precon,press,(n),coef2)

          do k = klow,kup                                         
          do j = jlow,jup
            jp1 = jplus(j)
            jm1 = jminus(j)
            jp2 = jplus(jp1)
            jm2 = jminus(jm1)
            swp1 = coef2(j,k)
            swm1 = coef2(jm1,k)
            cp1 = spect(j,k,en)
            cm1 = spect(jm1,k,en)
            javep1 = (xyj(jp1,k)+xyj(j,k))*0.25d0*dtd
            javem1 = (xyj(jm1,k)+xyj(j,k))*0.25d0*dtd
c     
            work(j,k,1) = 0.5d0*swm1*cm1*javem1
            work(j,k,2) = - ( 0.5d0*cp1*swp1*javep1 + (
     $            1.d0 + 0.5d0*swm1 )*cm1*javem1 )
            work(j,k,3) = ( 1.d0 + 0.5d0*swp1 )*javep1*cp1 +
     &            ( 1.d0 + 0.5d0*swm1 )*javem1*cm1
            work(j,k,4) = - ( javem1*0.5d0*cm1*swm1 +
     $            javep1*( 1.d0 + 0.5d0*swp1 )*cp1 )
            work(j,k,5) = 0.5d0*swp1*cp1*javep1
          enddo
          enddo
c
c         - boundary conditions
          if ( .not.periodic ) then
            j = jlow
            jp1 = jplus(j)
            jm1 = jminus(j)
            jp2 = jplus(jp1)
            do k = klow,kup
              swp1 = coef2(j,k)
              swm1 = coef2(jm1,k)
              cp1 = spect(j,k,en)
              cm1 = spect(jm1,k,en)
              javep1 = (xyj(jp1,k)+xyj(j,k))*0.25d0*dtd
              javem1 = (xyj(jm1,k)+xyj(j,k))*0.25d0*dtd
c     
              work(j,k,1) =  0.d0
              work(j,k,2) = - ( 0.5d0*swp1*cp1*javep1
     $              + cm1*javem1*( 1.d0 - 0.5d0*swm1 ) )
              work(j,k,3) = ( 1.d0 + 0.5d0*swp1 )*cp1*javep1 +
     &              cm1*javem1
              work(j,k,4) = - ( ( 1.d0 + 0.5d0*swp1 )*cp1*javep1 +
     &              0.5d0*swm1*cm1*javem1 )
              work(j,k,5) = 0.5d0*swp1*cp1*javep1
            enddo
c     
            j = jup
            jp1 = jplus(j)
            jm1 = jminus(j)
            jm2 = jminus(jm1)
            do k = klow,kup                       
              swp1 = coef2(j,k)
              swm1 = coef2(jm1,k)
              cp1 = spect(j,k,en)
              cm1 = spect(jm1,k,en)
              javep1 = (xyj(jp1,k)+xyj(j,k))*0.25d0*dtd
              javem1 = (xyj(jm1,k)+xyj(j,k))*0.25d0*dtd
c     
              work(j,k,1) =  0.5d0*swm1*cm1*javem1
              work(j,k,2) = - ( 0.5d0*swp1*cp1*javep1 + cm1*javem1
     $              *( 1.d0 + 0.5d0*swm1 ) )
              work(j,k,3) = cp1*javep1 + cm1*javem1*( 1.d0 +
     &              0.5d0*swm1 ) 
              work(j,k,4) = - ( cp1*javep1*( 1.d0 - 0.5d0*swp1 ) +
     &              0.5d0*swm1*cm1*javem1 )
              work(j,k,5) = 0.d0
            enddo
          endif !(not periodic)
        endif !idmodel


        ! ______________________________________________________________
        ! Flux Jacobians
c     
c       -add in flux jacobians, variable dt and identity                   
        if (viscous .and. visxi) then
          hre = dtd/re
c     
c         Sutherland equation                                           
c     
          do 4000 k = klow,kup                                         
          do 4000 j = jbegin,jend                                      
            rinv = 1./q(j,k,1)                                       
c           -xi_x**2 + xi_y**2                                           
            ximet = xy(j,k,1)**2 + xy(j,k,2)**2                      
c           -turb. viscosity                                             
            djac = hre/xyj(j,k)                                      
            work(j,k,6) = djac*ximet                                 
c           -xyj/rho                                                     
            work(j,k,7) = rinv                                       
 4000     continue                                                 
c                                                                     
          do 4401 k = klow,kup                                         
          do 4401 j = jlow,jup
            jm1 = jminus(j)
            jp1 = jplus(j)
c           -thp-4/29/87
c           for now laminar flow in xi viscous cases only
c           turm = turmu(j,k)                            
c
            turm = 0.d0                                               
            vnum1 = 0.5d0*(fmu(j,k)+fmu(jm1,k)) + turm                 
            vnum  = 0.5d0*(fmu(j,k)+fmu(jp1,k)) + turm                 
            ctm1 = 0.5d0*(work(jm1,k,6)+work(j,k,6))*vnum1            
            ctp1 = 0.5d0*(work(jp1,k,6)+work(j,k,6))*vnum             
            cttt = ctm1 + ctp1                                       
            work(j,k,2) = work(j,k,2) - work(jm1,k,7)*ctm1           
            work(j,k,3) = work(j,k,3) + work(j,k,7)*cttt             
            work(j,k,4) = work(j,k,4) - work(jp1,k,7)*ctp1           
 4401     continue
        endif !viscous
c     
        fct=1.d0
csi     add terms to lhs for time terms
c       -add this factor now so its gets scaled by ds(j,k) --
        if (ismodel.gt.1 .and. meth.eq.1) then
           if (jesdirk.eq.4 .or. jesdirk.eq.3)then
              fct=1.0d0*dt/(dt2*akk)
           else
              fct=1.5d0*dt/dt2
           endif
           do 230 k=klow,kup
           do 230 j=jlow,jup
              work(j,k,3) = work(j,k,3) + fct
 230       continue
c         -get fct ready for unscaled contribution
          fct=1.d0
          if (ismodel.eq.2) then
             if(jesdirk.eq.4 .or. jesdirk.eq.3)then 
                fct=1.0d0/(akk*dt2)
             else 
                fct=1.5d0/dt2
             endif 
          endif
        endif
c     
c       -add in flux jacobians and variable dt and identity               
        if ((prec.eq.0).or.(n.eq.2)) then

          ! ____________
          ! Fourth-Order 
          if (iord.eq.4) then
          if (.not.periodic) then
             thd=dtd/12.d0  
             do 231 k = klow,kup                                       
             do 231 j = jlow+1,jup-1                            
                work(j,k,1)= work(j,k,1) + (uu(j-2,k)+sn*cc(j-2,k))*thd
                work(j,k,2)= work(j,k,2)-8.*(uu(j-1,k)+sn*cc(j-1,k))*thd
                work(j,k,4)= work(j,k,4)+8.*(uu(j+1,k)+sn*cc(j+1,k))*thd
                work(j,k,5)= work(j,k,5) - (uu(j+2,k)+sn*cc(j+2,k))*thd
                work(j,k,1)= work(j,k,1)*ds(j,k)                     
                work(j,k,2)= work(j,k,2)*ds(j,k)                      
                work(j,k,3)= work(j,k,3)*ds(j,k)                       
                work(j,k,3)= work(j,k,3) + fct
                work(j,k,4)= work(j,k,4)*ds(j,k)                       
                work(j,k,5)= work(j,k,5)*ds(j,k)                    
 231         continue
c               
c            using third order for boundary
             thd=dtd/6.d0
             do 232 k=klow,kup
                j=jlow
                work(j,k,2)= work(j,k,2)-2.*(uu(j-1,k)+sn*cc(j-1,k))*thd
                work(j,k,3)= work(j,k,3) -   3.*(uu(j,k)+sn*cc(j,k))*thd
                work(j,k,4)= work(j,k,4)+6.*(uu(j+1,k)+sn*cc(j+1,k))*thd
                work(j,k,5)= work(j,k,5) -  (uu(j+2,k)+sn*cc(j+2,k))*thd
                work(j,k,1)= work(j,k,1)*ds(j,k)                      
                work(j,k,2)= work(j,k,2)*ds(j,k)                      
                work(j,k,3)= work(j,k,3)*ds(j,k)                       
                work(j,k,3)= work(j,k,3) + fct
                work(j,k,4)= work(j,k,4)*ds(j,k)                       
                work(j,k,5)= work(j,k,5)*ds(j,k)                       
c               
                j=jup
                work(j,k,1)= work(j,k,1) +  (uu(j-2,k)+sn*cc(j-2,k))*thd
                work(j,k,2)= work(j,k,2)-6.*(uu(j-1,k)+sn*cc(j-1,k))*thd
                work(j,k,3)= work(j,k,3) +   3.*(uu(j,k)+sn*cc(j,k))*thd
                work(j,k,4)= work(j,k,4)+2.*(uu(j+1,k)+sn*cc(j+1,k))*thd
                work(j,k,1)= work(j,k,1)*ds(j,k)                     
                work(j,k,2)= work(j,k,2)*ds(j,k)                        
                work(j,k,3)= work(j,k,3)*ds(j,k)                        
                work(j,k,3)= work(j,k,3) + fct
                work(j,k,4)= work(j,k,4)*ds(j,k)                        
                work(j,k,5)= work(j,k,5)*ds(j,k)                       
 232         continue
          else ! periodic
c          for periodic meshes 4th-order
c          thd=dtd/12.d0  
           do 233 k = klow,kup                                      
           do 233 j = jlow,jup                                       
               jp1=jplus(j)
               jp2=jplus(jp1)
               jm1=jminus(j)
               jm2=jminus(jm1)
               work(j,k,1)= work(j,k,1) + (uu(jm2,k)+sn*cc(jm2,k))*thd
               work(j,k,2)= work(j,k,2)-8.*(uu(jm1,k)+sn*cc(jm1,k))*thd
               work(j,k,4)= work(j,k,4)+8.*(uu(jp1,k)+sn*cc(jp1,k))*thd 
               work(j,k,5)= work(j,k,5) - (uu(jp2,k)+sn*cc(jp2,k))*thd 
               work(j,k,1)= work(j,k,1)*ds(j,k)                      
               work(j,k,2)= work(j,k,2)*ds(j,k)                       
               work(j,k,3)= work(j,k,3)*ds(j,k)                       
               work(j,k,3)= work(j,k,3) + fct
               work(j,k,4)= work(j,k,4)*ds(j,k)                         
               work(j,k,5)= work(j,k,5)*ds(j,k)                        
 233       continue
          endif ! (.not. periodic)
             else
c             for 2nd-order code             
              do 234 k = klow,kup                                         
              do 234 j = jlow,jup                                        
                 jp1=jplus(j)
                 jm1=jminus(j)
                 work(j,k,2) = work(j,k,2) - (uu(jm1,k)+sn*cc(jm1,k))*hd  
                 work(j,k,4) = work(j,k,4) + (uu(jp1,k)+sn*cc(jp1,k))*hd   
                 work(j,k,1) = work(j,k,1)*ds(j,k)                          
                 work(j,k,2) = work(j,k,2)*ds(j,k)                         
                 work(j,k,3) = work(j,k,3)*ds(j,k)                         
                 work(j,k,3) = work(j,k,3) + fct
                 work(j,k,4) = work(j,k,4)*ds(j,k)                         
                 work(j,k,5) = work(j,k,5)*ds(j,k)                         
 234          continue
                endif
        else ! belonging to: if ((prec.eq.0).or.(n.eq.2)) then
c        -use the preconditioned 3rd and 4th eigenvalues
c        -2nd-order
         do 235 k = klow,kup                                         
         do 235 j = jlow,jup
            jp1=jplus(j)
            jm1=jminus(j)
            work(j,k,2) = work(j,k,2) - 
     +           (precon(jm1,k,3)+sn*precon(jm1,k,4))*hd     
            work(j,k,4) = work(j,k,4) + 
     +           (precon(jp1,k,3)+sn*precon(jp1,k,4))*hd     
            work(j,k,1) = work(j,k,1)*ds(j,k)                           
            work(j,k,2) = work(j,k,2)*ds(j,k)                           
            work(j,k,3) = work(j,k,3)*ds(j,k)                           
            work(j,k,3) = work(j,k,3) + fct                              
            work(j,k,4) = work(j,k,4)*ds(j,k)                           
            work(j,k,5) = work(j,k,5)*ds(j,k)                           
 235     continue
        endif ! ((prec.eq.0).or.(n.eq.2))
c     
c      -if CUSP dissipation - cannot invert first two s elements 
c       together when full limiters are selected.
        if ( (idmodel.eq.4) .and. (limiter.gt.3) ) then 
          call xpenta(jdim,kdim,work(1,1,1),work(1,1,2),
     &          work(1,1,3),work(1,1,4),work(1,1,5),work(1,1,7),
     &          work(1,1,8),s(1,1,n),jlow,jup,klow,kup)
        else
c     
c         -for n=1 and 2 do both together                                 
c         -invert on first two s elements                                 
          if (n.eq.2) then
            if(.not.periodic)then                                       
              call xpenta2(jdim,kdim,work(1,1,1),work(1,1,2),
     &              work(1,1,3),work(1,1,4),work(1,1,5),
     &              work(1,1,7),work(1,1,8),s(1,1,1),jlow,jup,klow,kup)
           else !periodic
              call xpentap2(jdim,kdim,work(1,1,1),work(1,1,2),
     &              work(1,1,3),work(1,1,4),work(1,1,5),work(1,1,7)
     &              ,work(1,1,8),work(1,1,9),work(1,1,10),s(1,1,1),
     &              jlow,jup,klow,kup)
            endif                                                    
          else !n = 3 or 4
            if (.not.periodic) then                                    
              call xpenta(jdim,kdim,work(1,1,1),work(1,1,2),
     &              work(1,1,3),work(1,1,4),work(1,1,5),
     &              work(1,1,7),work(1,1,8),s(1,1,n),jlow,jup,klow,kup)
            else !periodic
              call xpentap(jdim,kdim,work(1,1,1),work(1,1,2),
     &              work(1,1,3),work(1,1,4),work(1,1,5),work(1,1,7),
     &              work(1,1,8),work(1,1,9),work(1,1,10),s(1,1,n),
     &              jlow,jup,klow,kup)
            endif                                                    
          endif                                                           
        endif !(idmodel.eq.4) .and. (limiter.gt.3)
c     
 300  continue
c     
      return                                                          
      end     
