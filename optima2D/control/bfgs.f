C     
C     PURPOSE: MINIMIZE AN UNCONSTRAINED NONLINEAR SCALAR VALUED
C     FUNCTION OF A VECTOR VARIABLE X BY THE BFGS VARIABLE 
C     METRIC ALGORITHM.
C     
C     USAGE:      CALL CONMIN(N,X,F,G,IFUN,ITER,EPS,NFLAG,MXFUN,W,
C     IOUT,MDIM,IDEV,ACC,NMETH)
C     
C     PARAMETERS: N      THE NUMBER OF VARIABLES IN THE FUNCTION TO
C     BE MINIMIZED.
C     X      THE VECTOR CONTAINING THE CURRENT ESTIMATE TO
C     THE MINIMIZER. ON ENTRY TO CONMIN,X MUST CONTAIN
C     AN INITIAL ESTIMATE SUPPLIED BY THE USER.
C     ON EXITING,X WILL HOLD THE BEST ESTIMATE TO THE
C     MINIMIZER OBTAINED BY CONMIN. X MUST BE DOUBLE
C     PRECISIONED AND DIMENSIONED N.
C     F      ON EXITING FROM CONMIN,F WILL CONTAIN THE LOWEST
C     VALUE OF THE OBJECT FUNCTION OBTAINED.
C     F IS DOUBLE PRECISIONED.
C     G      ON EXITING FROM CONMIN,G WILL CONTAIN THE
C     ELEMENTS OF THE GRADIENT OF F EVALUATED AT THE
C     POINT CONTAINED IN X. G MUST BE DOUBLE
C     PRECISIONED AND DIMENSIONED N.
C     IFUN   UPON EXITING FROM CONMIN,IFUN CONTAINS THE
C     NUMBER OF TIMES THE FUNCTION AND GRADIENT
C     HAVE BEEN EVALUATED.
C     ITER   UPON EXITING FROM CONMIN,ITER CONTAINS THE
C     TOTAL NUMBER OF SEARCH DIRECTIONS CALCULATED
C     TO OBTAIN THE CURRENT ESTIMATE TO THE MINIMIZER.
C     EPS    EPS IS THE USER SUPPLIED CONVERGENCE PARAMETER.
C     CONVERGENCE OCCURS WHEN THE NORM OF THE GRADIENT
C     IS LESS THAN OR EQUAL TO EPS TIMES THE MAXIMUM
C     OF ONE AND THE NORM OF THE VECTOR X. EPS
C     MUST BE DOUBLE PRECISIONED.
C     NFLAG  UPON EXITING FROM CONMIN,NFLAG STATES WHICH
C     CONDITION CAUSED THE EXIT.
C     IF NFLAG=0, THE ALGORITHM HAS CONVERGED.
C     IF NFLAG=1, THE MAXIMUM NUMBER OF FUNCTION
C     EVALUATIONS HAVE BEEN USED.
C     IF NFLAG=2, THE LINEAR SEARCH HAS FAILED TO
C     IMPROVE THE FUNCTION VALUE. THIS IS THE
C     USUAL EXIT IF EITHER THE FUNCTION OR THE
C     GRADIENT IS INCORRECTLY CODED.
C     IF NFLAG=3, THE SEARCH VECTOR WAS NOT
C     A DESCENT DIRECTION. THIS CAN ONLY BE CAUSED
C     BY ROUNDOFF,AND MAY SUGGEST THAT THE
C     CONVERGENCE CRITERION IS TOO STRICT.
C     MXFUN  MXFUN IS THE USER SUPPLIED MAXIMUM NUMBER OF
C     FUNCTION AND GRADIENT CALLS THAT CONMIN WILL
C     BE ALLOWED TO MAKE.
C     W      W IS A VECTOR OF WORKING STORAGE.IF NMETH=0,
C     W MUST BE DIMENSIONED 5*N+2. IF NMETH=1,
C     W MUST BE DIMENSIONED N*(N+7)/2. IN BOTH CASES,
C     W MUST BE DOUBLE PRECISIONED.
C     IOUT   IOUT IS A USER  SUPPLIED OUTPUT PARAMETER.
C     IF IOUT = 0, THERE IS NO PRINTED OUTPUT FROM
C     CONMIN. IF IOUT > 0,THE VALUE OF F AND THE
C     NORM OF THE GRADIENT SQUARED,AS WELL AS ITER
C     AND IFUN,ARE WRITTEN EVERY IOUT ITERATIONS.
C     MDIM   MDIM IS THE USER SUPPLIED DIMENSION OF THE
C     VECTOR W. IF NMETH=0,MDIM=5*N+2. IF NMETH=1,
C     MDIM=N*(N+7)/2.
C     IDEV   IDEV IS THE USER SUPPLIED NUMBER OF THE OUTPUT
C     DEVICE ON WHICH OUTPUT IS TO BE WRITTEN WHEN
C     IOUT>0.
C     ACC    ACC IS A USER SUPPLIED ESTIMATE OF MACHINE
C     ACCURACY. A LINEAR SEARCH IS UNSUCCESSFULLY
C     TERMINATED WHEN THE NORM OF THE STEP SIZE
C     BECOMES SMALLER THAN ACC. IN PRACTICE,
C     ACC=10.D-20 HAS PROVED SATISFACTORY. ACC IS
C     DOUBLE PRECISIONED.

      subroutine bfgs( jdim, kdim, ndim, ifirst, indx, icol, iex, q, cp,
     &    xy, xyj, x, y, dx, dy, lambda, fmu, vort, turmu, cp_tar, dvs,
     &    idv, bap, bcp, bt, bknot, work1, ia, ja, as, ipa, jpa, pa,
     &    iat, jat, ast, ipt, jpt, pat, cp_his, ac_his, opwk, ifun,
     &    iter, eps, nflag, mxfun, iout, acc, xsave, ysave, qtmp,
     &    itfirst, obj0s )

#ifdef _MPI_VERSION
      use mpi
#endif

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/graph.inc"
#include "../include/mpi_info.inc"

c     -- solver arrays --
      integer jdim, kdim, ndim, ifirst, itfirst, ifun, iex(jdim,kdim,4)
      double precision q(jdim,kdim,ndim), cp(jdim,kdim), y(jdim,kdim)
      double precision xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim)
      double precision x_r(jdim,kdim), y_r(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision lambda(2*jdim*kdim*incr), obj1
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)

c     -- optimization and b-spline arrays --
      integer idv(nc+mpopt), indx(jdim,kdim), icol(9), ifun_tmp
      double precision cp_tar(jbody,2), bap(jbody,2), bcp(nc,2)
      double precision bt(jbody), bknot(jbsord+nc), dvs(nc+mpopt)
      double precision cp_his(jbody,300,mpopt), ac_his(jbody,300)
      double precision s_vector(ndv), resids(mpopt), psi, psi_star
      double precision alphamax, clmax, MaxMach_r, cdt_r, clt_r
      double precision wt_old, wt_new

c     -- sparse adjoint arrays --

      integer ia(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer iat(jdim*kdim*ndim+2)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)


c     -- grid storage --
      double precision  xsave(jdim,kdim), ysave(jdim,kdim)

c     -- common block used for out of pareto info --
      common/pareto/ ftc, fcl, fcd

c     -- work arrays --
      double precision work1(jdim*kdim,100), opwk(jdim*kdim,8)

      Double Precision grad(ibsnc), WBFGS(ibsnc*(ibsnc+7)/2)
      Double Precision obj, obj0, FP, FMIN, ALFA, AT, AP, GSQ, DG, DG1
      Double Precision DP, STEP, ACC, DAL, U1, U2, U3, U4, EPS, XSQ
      double precision dv_scal(ibsnc), test, dv_scalMag, wobj
      double precision sdMag, numer, denom, compObj, compGrad(ibsnc)
      double precision wgrad(ibsnc), lcon
      Logical Rsw, badjacs(mpopt)

c     START CHANGES MADE BY LASLO

      double Precision tmp, stmp, gtmp(ibsnc), gtemp(ibsnc), fd_temp
      double precision alphawarm, walfaGrad, U, V, P, a, Mach, MaxMach
      integer gradtemp, sendProc, jMaxMach, kMaxMach
      double precision qs(jdim,kdim,4), xs(jdim,kdim), ys(jdim,kdim)
      double precision aleft, dleft, fleft, aright, dright, fright       
      double precision garbage, dvs_avg, gmin(ibsnc), dgTest, gminMag
      dimension qwarm(warmset,jdim,kdim,ndim,mpopt)      
      dimension q_init(warmset,jdim,kdim,ndim,mpopt)      
      character command*60, filename*40
      integer d100, d10, d1, badfs

C     END CHANGES MADE BY LASLO

      
      dimension grads(mpopt,ibsnc), objs(mpopt), obj0s(mpopt)

c     start changes by huafei
      double precision cd(mpopt),cl(mpopt),cm(mpopt)

c     -- temp solution array for warm starts --
      dimension qtmp(jdim,kdim,ndim,mpopt)

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c  objs(mp) - vector of all current objective function values.  Size is
c    the number of operating points in the optimization.
c  obj0s(mp) - vector of all initial objective function values.  Size is 
c    the number of operating points in the optimization.  Used for 
c    normalization of objective functions.
c  clalphas(mp), clalpha2s(mp), clopts(mp) are vectors of logical values
c    that control whether the angle of attack varies in the AF, NK, and 
c    gradient calculation respectively.  Size is number of operating
c    points in the optimization.  clalpha, clalpha2, clopt are the same
c    logical values, but specific to point currently being studied.
c  reno(mp), cltars(mp), cdtars(mp), wfls(mp), and objfuncs(mp), similarly,
c    are vectors containing the reynolds numbers, target coefficients of 
c    lift, target coefficients of drag, weights given to lift component 
c    (for obj_func = 6) of objective function, and which objective functions
c    are to be used for which design points.
c  grad(dv) contains the gradient vector for the current design point.  
c    size is the number of design variables.
c  grads(mp,dv) contains the gradient vectors for all design points.  Size is
c    number of operating points by number of design variables.
c  itfirst is a variable that controls whether or not the current objective
c    function value is the initial objective function value.  It is set to 
c    1 for the first objective function calculations for all design points, 
c    and then remains 0 for all subsequent steps.  It remains 0 even if the
c    optimization is restarted from the steepest descent method.

C     INITIALIZE ITER,IFUN,NFLAG,AND IOUTK,WHICH COUNTS OUTPUT
c     ITERATIONS.
    
      if (opt_restart) goto 211

c     --start change made by huafei
      if(graphout.eq..true.) then
          call initial_graph_out(x,y,jdim,kdim)
      end if
c     --end change made by huafei
      wt_iter=0
      iter=0
      ifun=0
      ioutk=0
      nflag=0
c     START CHANGES MADE BY LASLO

c     -- scaling info --
c     If scaling = 0 set dv_scal to be the initial design variables
c     ie scale all design variables to 1.

      if(scaling.eq.0) then
         do i = 1,ndv
            dv_scal(i) = dvs(i)
         end do

c     If scaling =1 then scale alpha by it's initial value, while scale
c     all other design variables by the average of their initial values
c
      else if(scaling.eq.1) then
         dv_scal(1) = 0.D0

         do mp=1,mpopt
            if(dvalfa(mp))  ndv = ngdv
         enddo
         
         dvs_avg = 0.D0
         do i = 1,ndv
            dvs_avg = dvs_avg + abs(dvs(i))
         enddo
         dvs_avg = dvs_avg/ndv
         do i = 1,ndv
            dv_scal(i) = dvs(i)/abs(dvs(i))*dvs_avg
         enddo

         do mp=1,mpopt
            if(dvalfa(mp)) then
               ndv = ngdv + 1
               dv_scal(ndv) = dvs(ndv)
            endif
         enddo

C     If scaling =2 then scale by squre root of the design variable
c     times the sign of the design variable
C
      else if(scaling.eq.2) then
         do i = 1,ndv
            dv_scal(i) = dvs(i)/abs(dvs(i)) * sqrt(abs(dvs(i)))
         end do


c     If scaling =3 scale by 1 times the sign of the design variable
      else if(scaling.eq.3) then
         do i=1,ndv
            dv_scal(i) = 1.d0*dvs(i)/abs(dvs(i))
         end do
      endif



C     END CHANGES MADE BY LASLO


      if ((opt_cycle.eq.1).and.(.not.wt_update)) then

         !-- save initial grid --
         do k = 1,kdim
            do j = 1,jdim
               xsave(j,k) = x(j,k)
               ysave(j,k) = y(j,k)
            end do
         end do
         
         !-- save initial angle of attack values(H.Buckley)

         alphas_init = alphas

      end if

      !-- If warm starts are enabled and this optimization cycle is an 
      !-- auto-restart, then set values for the 1st iteration of
      !-- the restart.
      !--  OR
      !-- If warm starts are enabled and this is the beginning of a new 
      !-- weight update cycle, then set values for the 1st iteration of
      !-- the new weight update cycle.

      if (((opt_cycle.gt.1).and.(warm_start)).or.
     &   ((wt_update).and.(warm_start))) then
         
         write(scr_unit,9)
 9       format(/3x,'Reading warm-start solution from file')

         !-- Set the initial solution guess for the 1st iteration of
         !-- the restart to the initial solution guess from the last
         !-- successful design iteration before the restart.

#ifdef _MPI_VERSION
         d10  = rank / 10
         d1   = rank - (d10*10)
#else
         d10  = 0 / 10
         d1   = 0 - (d10*10)
#endif

         !-- Create a unique 'initial solution guess' filename
         !-- for each processor

         filename = 'q_restart'
         namelen =  len_trim(filename)
         filename(namelen+1:namelen+2)=char(d10+48)//char(d1+48)

         open(unit=456,file=filename,status='old',form='unformatted')

         read(456) qwarm

         close(456)

         !-- Force 1st iteration of a restart to be warm started

         ifirst = 0 

      end if


c     -- store total number of design variables --
      ndvtot = ndv

C     SET PARAMETERS TO EXTRACT VECTORS FROM W.
C     WBFGS(1..n) HOLDS THE SEARCH VECTOR,WBFGS(n+1..2n) HOLDS THE BEST
c     CURRENT
C     ESTIMATE TO THE MINIMIZER,AND WBFGS(2n+1..3n) HOLDS THE GRADIENT
C     AT THE BEST CURRENT ESTIMATE.
C     
      nx=ndv
      ng=nx+ndv

C     IF NMETH=1,WBFGS(3n..) HOLDS THE APPROXIMATE INVERSE HESSIAN.
C     
 10   ncons=3*ndv

C     CALCULATE THE FUNCTION AND GRADIENT AT THE INITIAL
C     POINT AND INITIALIZE NRST,WHICH IS USED TO DETERMINE
C     WHETHER A BEALE RESTART IS BEING DONE. NRST=N MEANS THAT THIS
C     ITERATION IS A RESTART ITERATION. INITIALIZE RSW,WHICH INDICATES
C     THAT THE CURRENT SEARCH DIRECTION IS A GRADIENT DIRECTION.

c     -- compute objective function --
 20   continue

c     -- adjust grid --
      if (ngdv.ne.0) then
c     -- restore original grid --
         do k = 1,kdim
            do j = 1,jdim
               x(j,k) = xsave(j,k)
               y(j,k) = ysave(j,k)
            end do
         end do

         ifun_tmp = ifun+1

         call regrid( -1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

         !-- Store perturbed grid coordinates

         x_r = x
         y_r = y

      end if  

c     -- write current airfoil geometry and control points to file --
c     -- Only done by processor 0

#ifdef _MPI_VERSION
      if(rank.eq.0) then
#endif
         rewind (ac_unit)
         rewind (bc_unit)
         do j = jtail1,jtail2
            write (ac_unit,600) x(j,1), y(j,1)
         end do
         call flush(ac_unit)
         do i = 1,nc
            write (bc_unit,600) bcp(i,1), bcp(i,2)
         end do
         call flush(bc_unit)
 600     format (3e24.16)
#ifdef _MPI_VERSION
      end if
#endif

c     -- muti-point optimization loop --
c     -- if mpopt = 1 => single point design --
c     -- routines calcobj and calcgrad require ndv = ngdv+1 with the
c     aoa as the last design variable --

      ifun=ifun+1

      !-- Write information to restart file
      
      write(scr_unit,213) ifun

      rewind(72)
      write(72) wt_cycle
      write(72) nc,mpopt,mp
      write(72) (dvs(i),i = 1,nc+mpopt)
      write(72) (dv_scal(i),i = 1,ibsnc)
      write(72) pobj,obj, clt, cdt, cmt
      write(72) alpha, resid, slope,rhoinf,pinf
      write(72) (grad(i),i = 1,ibsnc)
      write(72) (alphas(i),i = 1,nopc) 
      write(72) (WBFGS(i),i = 1,ibsnc*(ibsnc+7)/2)
      write(72) ifsd,ifun,nflag,iter,ioutk,ndvtot,nx,ng,ncons,badfs
      write(72) ico,icg,itfirst,nxpi,ngpi,ifirst,warmset,ngdv
      write(72) alfa,dg,step,fmin,dg1,bstep,ap
      write(72) dleft,fleft,aleft,dright,fright,aright,fp,dp      
      write(72)rsw,new_search,restart
      write(72) (obj0s(i),i = 1,mpopt)
      write(72) (objs(i),i = 1,nopc)
      write(72) (wmpo(i),i = 1,nopc) 
      write(72) ((grads(i,k),i = 1,20),k=1,ibsnc)
      write(72) (((((qwarm(i,j,k,l,m),i=1,warmset),j=1,jdim),
     &        k=1,kdim),l=1,ndim),m=1,mpopt)    
      write(72) ((((qtmp(j,k,l,m),j=1,jdim),
     &        k=1,kdim),l=1,ndim),m=1,mpopt)
      write(72) (((q(j,k,l),j=1,jdim),k=1,kdim),l=1,ndim)
      write(72) (cl(i),i = 1,mpopt)
      write(72) (cd(i),i = 1,mpopt)
      write(72) (cm(i),i = 1,mpopt)
      write(72) ((xsave(j,k),j=1,jdim),k=1,kdim)
      write(72) ((ysave(j,k),j=1,jdim),k=1,kdim)
      write(72) wt_iter, rest_iter
      write(72) MaxMach_r, cdt_r, clt_r
      write(72) ((x_r(j,k),j=1,jdim),k=1,kdim)
      write(72) ((y_r(j,k),j=1,jdim),k=1,kdim)
      write(72) (alphas_init(i),i = 1,nopc)
      write(72) (alphas_restart(i),i = 1,nopc)

      !-- Set file prefixes for a restart
      !-- For the inital run before any restarts, the output and restart
      !-- file prefixes are 'rest00' and rest00'
#ifdef _MPI_VERSION
      if (rank==0) then
#endif
         !-- Update the output file prefix to reflect the current value
         !-- of the restart counter

         d10  = rest_iter / 10
         d1   = rest_iter - (d10*10)

         output_file_prefix = 'rest'
         namelen =  len_trim(output_file_prefix)
         output_file_prefix(namelen+1:namelen+2)
     &          = char(d10+48)//char(d1+48)

         !-- Update the restart file prefix to reflect the previous value
         !-- of the restart counter

         d10  = (rest_iter-1) / 10
         d1   = (rest_iter-1) - (d10*10)

         restart_file_prefix = 'rest'
         namelen =  len_trim(restart_file_prefix)
         restart_file_prefix(namelen+1:namelen+2)
     &          = char(d10+48)//char(d1+48)

         opt_restart = .true.
         obj_restart = .true.

         rewind(prefix_unit)
         write(prefix_unit,*)output_file_prefix
         write(prefix_unit,*)restart_file_prefix
         write(prefix_unit,*)opt_restart
         write(prefix_unit,*)obj_restart
         
         opt_restart = .false.
         obj_restart = .false.

#ifdef _MPI_VERSION
      end if
#endif

c     -- store airfoil shape --
      i = 1
      do j = jtail1, jtail2
c     ifun=1 here       
         ac_his(i,ifun)   = x(j,1)
         ac_his(i,ifun+1) = y(j,1)
         i = i + 1
      end do
 
c     -- write current design variables --
c     -- Only done by processor 0
#ifdef _MPI_VERSION
      if(rank.eq.0) then
#endif
         write (dvhis_unit,720) ifun, (dvs(j),j=1,ndv)
         call flush(dvhis_unit)

c     -- write initial airfoil coordinates --
c     -- Only done by processor 0

         write (n_best,890)
         do j = jtail1, jtail2
            write (n_best,896) x(j,1), y(j,1)
         end do
         write (n_best,895)
         call flush(n_best)

#ifdef _MPI_VERSION
      end if ! (rank.eq.0)
#endif

      !-- Set design point operating conditions according to processor
      !-- rank. i.e. the processor with rank N will handle computation
      !-- of design point N+1
#ifdef _MPI_VERSION
      mp = rank+1
#else
      mp = 1
#endif

      !-- define number of design variables at design point --

      if ( dvalfa(mp) ) then
         ndv = ngdv+1
      else
         ndv = ngdv
      end if
         
      !-- Set operating conditions for the design point

      if (opt_meth.ne.4) fsmach = fsmachs(mp)
      re_in = reno(mp)
      cl_tar = cltars(mp)
      cd_tar = cdtars(mp)
      wfl = wfls(mp)
      obj_func = objfuncs(mp)
      clalpha = clalphas(mp)
      clalpha2 = clalpha2s(mp)
      clopt = clopts(mp)
      obj0 = obj0s(mp)

      !-- If warm starts are enabled and this is the 1st iteration after
      !-- a restart, then alpha will be set to the initial value of alpha
      !-- from the last successful design iteration before the restart. 
      !-- Otherwise, alpha will be set to the original AOA value from the
      !-- input file (H.Buckley)

      !-- adjust angle of attack --

      if (.not. clopt) then
         if ( dvalfa(mp) ) then
            atmp = dvs(ngdv+1)
            dvs(ngdv+1) = dvs(idv(ngdv+mp))
            alpha = dvs(ngdv+1)
         else
            if ((opt_cycle.gt.1).and.(.not.warm_start)) then
               alpha = alphas_init(mp)
            else
               alpha = alphas(mp)
            end if
         end if
      else
         if ((opt_cycle.gt.1).and.(.not.warm_start)) then
            alpha = alphas_init(mp)
         else
            alpha = alphas(mp)
         end if
      end if

      frozen = .false.       !flag used for freezing dissipation

      !-- If warm starts are enabled and this is the 1st iteration after
      !-- a restart, then the initial solution guess will be set to the 
      !-- initial solution guess from the last successful design iteration 
      !-- before the restart. Otherwise, the initial solution guess will 
      !-- be set to free-stream conditions (H.Buckley)

c     -- restore last converged solution --
      if (((ifirst.eq.0).and.(opt_cycle.gt.1)).or.
     &   ((wt_update).and.(warm_start))) then

         do n=1,4
            do k=1,kdim
               do j=1,jdim
                  q(j,k,n)=qwarm(1,j,k,n,mp)
               end do
            end do
         end do      
c     -- update turbulent viscosity --
         if (viscous .and. turbulnt .and. itmodel.eq.2) then
            do k = 1,kdim
               do j = 1,jdim
                  turre(j,k) = qwarm(1,j,k,5,mp)
               end do
            end do
         end if
      end if
       
c     -- compute obj. function --      

      call CalcObj (obj, jdim, kdim, ndim, ifirst, indx, icol, iex,
     &        q, cp, xy, xyj, x, y, cp_tar, fmu, vort, turmu, work1,
     &        ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat, 
     &        itfirst, obj0, mp, ifun, obj1, lcon )

      !-- Save lift value from 1st iteration

      clt_r = clt
     
      !-- Force all processors to pause here until the objective
      !-- function at every design point has been calculated

#ifdef _MPI_VERSION
      call MPI_BARRIER(COMM_CURRENT, ierr)
#endif    
      !-- Terminate the optimization if we encounter flow convergence
      !-- problems at this early stage

      if (resid .gt. 1.d-8 ) then
         write (scr_unit,*)
     &        'Flow convergence problems on first design iteration!!'
         write (scr_unit,*)
     &        'Design Point: ', mp
         stop
      end if
      
      !-- Send final angle of attack from this iteration to array 
      !-- 'alphas' on processor 0 for warm-starting solution of next 
      !-- iteration

#ifdef _MPI_VERSION

      if (rank==0) then

         if (clopt) then
         
            alphas(1) = alpha

         end if

         do i = 2, mpopt

            if (clopts(i)) then

               sendProc = i-1

               call MPI_RECV(alpha, 1, MPI_DOUBLE_PRECISION,  
     &            sendProc, 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)
               
               alphas(i) = alpha
               
            end if

         end do
            
      else

         if (clopt) then
            
            call MPI_SEND(alpha, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                    COMM_CURRENT, ierr)            
            
         end if 

      end if ! (rank==0)

      !-- Broadcast 'alphas' to all processors

      call MPI_BCAST(alphas,mpopt,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)    

#else

      if (clopt) then
         
         alphas(1) = alpha

      end if  

#endif

      !-- Set the AOA restart array with AOA values from the first
      !-- iteration

      alphas_restart = alphas

      !-- The storage of AOA's from all processors done on processor
      !-- zero overwrites the AOA for processor zero, so restore the
      !-- correct AOA on processor zero
#ifdef _MPI_VERSION
      if ((rank.eq.0).and.(clopt)) alpha = alphas(1)
#else
      if (clopt) alpha = alphas(1)
#endif

      write(scr_unit,21) 
 21   format(/3x,'Set the AOA restart values to the initial AOA',/3x,
     &           'values from the first iteration',/)

      !-- Apply the design point weighting to the computed objective
      !-- function

      wobj = wmpo(mp)*obj

      !-- Use MPI_REDUCE to compute the composite objective function
      !-- and store on processor 0
#ifdef _MPI_VERSION
      call MPI_REDUCE(wobj,compObj,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,
     &     COMM_CURRENT, ierr)

      !-- Broadcast the composite objective function value to all
      !-- processors

      call MPI_BCAST(compObj,1,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)
#else
      compObj = wobj
#endif
      
      !-- If logging of grid or solution history is enabled, then
      !-- write these values to file now (H.Buckley).

      if((grid_his).or.(sol_his)) then

         !-- Add jacobian scaling...don't ask why!!!!           
         do n = 1,ndim
            do j = 1,jmax
               do k = 1,kmax
                  q(j,k,n) = (q(j,k,n))/(xyj(j,k))
               end do
            end do
         end do

         call history(x, y, q, q, ifun, turmu, xy, xyj, jdim, 
     &                      kdim, mp )

         !-- Remove jacobian scaling...pretend it was never there!!!!
            
         do n = 1,ndim
            do j = 1,jmax
               do k = 1,kmax
                  q(j,k,n) = (q(j,k,n))*(xyj(j,k))
               end do
            end do
         end do

      endif

      !-- Store initial objective function values in 'obj0s' array

      if (itfirst == 1) then

#ifdef _MPI_VERSION
         if (rank==0) then

            obj0s(1) = obj0
            
            do i = 2, mpopt

               sendProc = i-1

               call MPI_RECV(obj0, 1, MPI_DOUBLE_PRECISION,  
     &            sendProc, 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)
               
               obj0s(i) = obj0
                           
               !-- Reset the initial objective function value for proc 0
               !-- This step required because 'obj0' on proc 0 is 
               !-- repeatedly overwritten with the values of obj0 from
               !-- the sending processors.

               obj0 = obj0s(1)

            end do

            !-- After all initial objective function values have been
            !-- stored in 'obj0s' write 'obj0s' to .obj file
            if (rank.eq.0) then
               do i = 1, mpopt
                  write(obj_unit,401) obj0s(i)
 401              format(e40.34)
               end do
            end if
            
         else
            
            call MPI_SEND(obj0, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                    COMM_CURRENT, ierr)            
            
         end if ! (rank==0)

         !-- Broadcast 'obj0s' to all processors

         call MPI_BCAST(obj0s,mpopt,MPI_DOUBLE_PRECISION,0,
     &        COMM_CURRENT,ierr)   
#else
         obj0s(1) = obj0

         do i = 1, mpopt
            write(obj_unit,401) obj0s(i)
 401        format(e40.34)
         end do

#endif

      end if ! (itfirst == 1)

c     -- save converged solution --
      do n=1,ndim
         do k=1,kdim
            do j=1,jdim
               if(warmset.eq.1) then
                  qwarm(1,j,k,n,mp)=q(j,k,n)
               else
                  qtmp(j,k,n,mp)=q(j,k,n)
               endif
            end do
         end do
      end do


c     -- pareto front information --
c     -- Only done on processor 0
#ifdef _MPI_VERSION
      if(rank.eq.0)then
#endif
         if (obj_func.eq.6 .and. mpopt.eq.1) then
            write (par_unit,550) ico+1, fcl, fcd, ftc, fcl+ftc, fcd+ftc
            call flush(par_unit)
         end if
 550     format(i4,5e16.8)
#ifdef _MPI_VERSION
      end if
#endif

c     -- check T.E. slope --
      theta1 = atan2d( y(jtail2,1)-y(jtail2-1,1), x(jtail2,1) -
     &        x(jtail2-1,1) )
         
      theta2 = atan2d( y(jtail1,1)-y(jtail1+1,1), x(jtail1,1) -
     &        x(jtail1+1,1))
c     slope = tand( .5d0*(theta1+theta2) )  
      slope = 0.5d0*(theta1+theta2)

c     -- output info --
      pobj = obj
      if ( obj_func.eq.5 ) pobj = 1.d0/pobj
      if ( obj_func.eq.4 ) pobj = - pobj

      !-- Write thickenss constraint info to file
      !-- Only done on processor 0
#ifdef _MPI_VERSION
      if (rank.eq.0)then
#endif
         if (mp.eq.1 .and.
     &        (ntcon.gt.0 .or. nrtcon.gt.0 .or. wac.gt.0)) then
            write (tcon_unit,720) ifun,(cty(i),i=1,ntcon), area,
     &           (crth(i),i=1,nrtcon), (crthx(i),i=1,nrtcon)
            call flush(tcon_unit)
         endif
#ifdef _MPI_VERSION
      end if
#endif
         
c     -- store pressure distribution --
      if (mpopt.gt.1) rewind (n_mpcp)
      if (ifun .lt. 199) then
         i = 1
         do j = jtail1, jtail2
            cp_his(i,ifun,mp) = cp(j,1)
            if (mpopt.gt.1) write (n_mpcp,900) x(j,1),cp(j,1)
            i = i + 1
         end do
      end if
         
c     -- compute obj. func. gradient --
      if (coef_frz) frozen = .true.

      call CalcGrad (obj, grad, jdim, kdim, ndim, ifirst, q, cp, xy,
     &        xyj, x, y, xsave, ysave, dx, dy, lambda, fmu, vort, turmu,
     &        cp_tar, bap, bcp, bt, bknot,
     &        dvs, idv, indx, icol, iex, work1, ia, ja, ipa,jpa, iat,
     &        jat, ipt, jpt, as, ast, pa, pat, opwk, obj0, mp, ifun)

      !-- Calculate the L2 norm fo the gradient associated with design
      !-- point assigned to this processor

      gradmag = 0.0
      do i=1,ndv
         gradmag = gradmag + grad(i)*grad(i)
      end do
      gradmag = dsqrt(gradmag)

#ifdef _MPI_VERSION
      !-- Force all processors to pause here until the objective
      !-- function gradient at every design point has been calculated

      call MPI_BARRIER(COMM_CURRENT, ierr)
#endif
    
      !-- Reset ndv to total number of design variables 

      ndv = ndvtot

      !-- Initialize weighted gradient to zero

      do i=1,ndv
         wgrad(i)=0.0
      end do

      !-- Apply the design point weighting to the geometric design
      !-- variable components of the gradient vector

      do i=1,ngdv
         wgrad(i)= wmpo(mp)*grad(i)
      end do

#ifdef _MPI_VERSION

      !-- Use MPI_REDUCE to sum the weighted geometric design
      !-- variable components of the gradient vector from each processor
      !-- and store the result on processor 0

      call MPI_REDUCE(wgrad,compGrad,50,MPI_DOUBLE_PRECISION,MPI_SUM,
     &    0, COMM_CURRENT, ierr)

      !-- Add angle-of-attack gradient components to composite gradient
      !-- vector
   
      if (dvalfa(mp)) then

         walfaGrad =  wmpo(mp)*grad(ngdv+1)

         if (rank==0) then

            compGrad(idv(ngdv+mp)) = walfaGrad

         else
            
            call MPI_SEND(walfaGrad, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                    COMM_CURRENT, ierr)            

         end if

      end if

      if (rank==0) then

         do mp = 2, mpopt

            sendProc = mp-1

            if (dvalfa(mp)) then

              call MPI_RECV(walfaGrad, 1, MPI_DOUBLE_PRECISION, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

              compGrad(idv(ngdv+mp)) = walfaGrad

            end if

         end do
         
         mp = 1

      end if
    
      !-- Broadcast the composite gradient vector to all processors

      call MPI_BCAST(compGrad,50,MPI_DOUBLE_PRECISION,0,
     &                COMM_CURRENT, ierr)
#else

      do i=1,ndv
         compGrad(i) = wgrad(i)
      end do

      if (dvalfa(mp)) then

         walfaGrad =  wmpo(mp)*grad(ngdv+1)

         compGrad(idv(ngdv+mp)) = walfaGrad

      end if

#endif


      !-- Calculate the L2 norm fo the composite gradient
      !-- Only done on processor 0

#ifdef _MPI_VERSION

      if(rank.eq.0) then

         cgradmag = 0.0
         do i=1,ndv
            cgradmag = cgradmag + compGrad(i)*compGrad(i)
         end do
         cgradmag = dsqrt(cgradmag)

      end if

      !-- Broadcast the L2 norm fo the composite gradient to all
      !-- processors
      
      call MPI_BCAST(cgradmag,1,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)

#else

         cgradmag = 0.0
         do i=1,ndv
            cgradmag = cgradmag + compGrad(i)*compGrad(i)
         end do
         cgradmag = dsqrt(cgradmag)

#endif      

      ico = ico+1

      !-- re-adjust design variable vector --
      if ( dvalfa(mp) ) then
         dvs(idv(ngdv+mp)) = dvs(ngdv+1)
         dvs(ngdv+1) = atmp
      end if

      !-- Write design point objective function and gradient results 
      !-- to the repsective design point .ohis file

      if (mpopt.gt.1) then
c     start change by huafei
         cd(mp)=cdt
         cl(mp)=clt
         cm(mp)=cmt
c     end change by huafei
         if(ohisout) write(n_mpo,700) ifun,iter,pobj, gradmag, clt, cdt,
     &           cmt, alpha, resid
         if(ohisout) call flush(n_mpo)
      end if

#ifdef _MPI_VERSION

      !-- Pause until all processors are caught up

      call MPI_BARRIER(COMM_CURRENT,ierr)

#endif

      !-- Write design point objective function and gradient results 
      !-- to standard output file
         
      if (mpopt.eq.1) then
         write (scr_unit,710) ico, iter, obj, gradmag
      else
         write (scr_unit,711) ico, iter, obj, gradmag, mp
      end if

      write(scr_unit,1002)
 1002 format(/3x, 
     &'===========================================================',/)
      call flush(6)

      !-- Write composite objective function and gradient results 
      !-- to the main .ohis file
      !-- Only done by processor 0

      if (mpopt.gt.1) then
#ifdef _MPI_VERSION
         if(rank.eq.0) then
#endif
            write (ohis_unit,705)  
     &                ifun, ico, iter, compObj, cgradmag, slope       
            call flush(ohis_unit)
            if(graphout.eq..true.)then
               call graph_out(ac_his,cp_his,jdim,kdim,ifun,cd,cl,cm)
            end if
#ifdef _MPI_VERSION
         end if
#endif
      else
         cl(1)=clt
         cd(1)=cdt
         cm(1)=cmt
#ifdef _MPI_VERSION
         if(rank==0) then
#endif
            write (ohis_unit,702) ico, iter, pobj, gradmag, clt, cdt, 
     &              cmt, alpha, resid, slope
            call flush(ohis_unit)
#ifdef _MPI_VERSION
         end if
#endif
         if(graphout.eq..true.) then
            call graph_out(ac_his,cp_his,jdim,kdim,ifun,cd,cl,cm)
         end if
      end if

      !-- Write the composite gradient vector to the .gvhis file
      !-- Only done by processor 0
#ifdef _MPI_VERSION
      if(rank.eq.0)then
#endif
         write (gvhis_unit,810) ifun, (compGrad(i),i=1,ndv)
         call flush(gvhis_unit)
#ifdef _MPI_VERSION
      end if
#endif

      !-- Calculate the maximum mach number in the flow solution from
      !-- the current design iteration. 

      MaxMach = 0.0

      do j = 1, jdim

         do k = 1, kdim

            !-- Calculate velocities in x and y directions
            
            U = q(j,k,2)/q(j,k,1)
            V = q(j,k,3)/q(j,k,1)

            !-- Calculate pressure

            P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))

            !-- Calculate sound speed
            
            a = sqrt(1.4*P/q(j,k,1))

            !-- Calculate Mach number

            Mach = sqrt(U**2 + V**2)/a

            if (Mach.gt.MaxMach) then

               MaxMach = Mach
               jMaxMach = j
               kMaxMach = k

            end if

         end do

      end do

      !-- Store the maximum Mach number

      MaxMach_r = MaxMach

      !-- Save max Mach number to file

      if (mout) write(mach_unit,131) ifun,iter,MaxMach,jMaxMach,kMaxMach

      !-- Set 'itfirst' to zero to indicate that the first design 
      !-- iteration has been completed
      
      itfirst = 0

 700  format (2i4, e16.8, 4e15.6, e12.4, e10.2)
 702  format (2i4, e24.16, 4e15.6, e12.4, e10.2, f7.1) 
 705  format (3i4, 2e24.16, f7.1)
 710  format ( /3x, 'ICO:', i4, 3x, 'S.D.#:', i4, 3x, 'OBJ.:',
     &     e12.4, 3x, 'GRAD:', e12.4)
 711  format ( /3x, 'ICO:', i4, 3x, 'S.D.#:', i4, 3x, 'OBJ.:',
     &     e12.4, 3x, 'GRAD:', e12.4, 3x, 'PT:',i4)
 712  format ( /1x, 'Design Iteration:', i4, 3x, 'Search Direction #:',  
     &        i4, 3x, 'Step Size alfa:', e16.8,
     &        /, 4x, 'Search Vector:', 40e16.8, //) 
 720  format (i4, 40e16.8)   
 800  format(2i4, 2e16.8)
 810  format(i4,40e25.16)

      !-- Now that we have completed the 1st iteration, turn off
      !-- the '1st iteration' flag
 
      ifirst = 0  

      rsw=.true.

C     CALCULATE THE INITIAL SEARCH DIRECTION, THE NORM OF X SQUARED,
C     AND THE NORM OF G SQUARED. DG1 IS THE CURRENT DIRECTIONAL
C     DERIVATIVE,WHILE XSQ AND GSQ ARE THE SQUARED NORMS.


      dg1=0.d0
      xsq=0.d0
      do i=1,ndv        
         wbfgs(i) =    -compGrad(i)*dv_scal(i)*dv_scal(i)
         xsq  = xsq+dvs(i)*dvs(i)/dv_scal(i)/dv_scal(i)
         dg1  = dg1-compGrad(i)*compGrad(i)*dv_scal(i)*dv_scal(i)
      enddo
      gsq=-dg1

C     WRITE INITIAL SEARCH VECTOR TO OUTPUT FILE 'xxxx.svhis' 
c     -- Only done by processor 0
#ifdef _MPI_VERSION

      if (rank.eq.0) then
         write (svhis_unit,712) ifun, iter, alfa, (wbfgs(j),j=1,ndv)
      end if
#else
         write (svhis_unit,712) ifun, iter, alfa, (wbfgs(j),j=1,ndv)
#endif

C     TEST WHETHER THE INITIAL POINT IS THE MINIMIZER.
      
      if (gsq .le. eps*eps*dmax1(1.d0,xsq) ) then

#ifdef _MPI_VERSION        
         if(rank.eq.0) then
#endif
            rewind (ac_unit)
            do i = 1,jbody
               write (ac_unit,900) ac_his(i,1), (ac_his(i,j+1),j=1,ifun)
            end do
            write(ghis_unit,500) icg,ifun,compObj,cgradmag
#ifdef _MPI_VERSION 
         end if
#endif
   
         return
      end if !(gsq .le. eps*eps*dmax1(1.d0,xsq) )


      !-- Write .bsp and .dvs restart files after 1st iteration

#ifdef _MPI_VERSION
      if (rank.eq.0) then
#endif
         rewind(bspr_unit)
         write (bspr_unit,*) jbsord
         write (bspr_unit,*) nc
         write (bspr_unit,*) jbody
c     -- write airfoil control points --
         do i = 1,nc
            write (bspr_unit, 910) bcp(i,1),bcp(i,2)
         end do

c     -- write parameter vector --
         do i = 1,jbody
            write (bspr_unit, 910) bt(i)
         end do

c     -- write knot vector --
         do i = 1,jbsord+nc
            write (bspr_unit, 910) bknot(i)
         end do

         rewind(dvsr_unit)
         do i = 1,ngdv
            write (dvsr_unit, 920) idv(i), dvs(i)
         end do

c     -- write best airfoil coordinates --
         rewind(n_best)
         write (n_best,890)
         do j = jtail1, jtail2
            write (n_best,896) x(j,1), y(j,1)
         end do
         write (n_best,895)
#ifdef _MPI_VERSION
      end if !(rank.eq.0)
#endif

************************************************************************
********************* END OF 1st DESIGN ITERATION **********************
************************************************************************

c     BEGIN THE MAJOR ITERATION LOOP.
C     FMIN IS THE CURRENT FUNCTION VALUE.

 40   continue !-- Loop back to this point if passed Wolfe conditions
      
      fmin = compObj
      gmin = compGrad
      

      !-- Check for orthogonality between new search direction and 
      !-- gradient at the last accepted design iteration. First:      
      !-- calculate the numerator of the 'orthoCheck' equation 
      !-- (dot product of search direction and gradient)

      numer = 0

      do i=1,ndv

         numer = numer + compGrad(i)*wbfgs(i)

      enddo


      !-- Calculate the magnitude (L2 norm) of the search direction at 
      !-- the current design iteration

      sdMag = 0

      do i=1,ndv

         sdMag = sdMag + wbfgs(i)*wbfgs(i)

      enddo

      sdMag = sqrt(sdMag)

      !-- Calculate the denominator of the 'orthoCheck' equation
   
      denom = cgradmag*sdMag

      !-- Calculate the 'orthoCheck' value at the current design 
      !-- iteration

      orthoCheck = numer/denom

      !-- If orthogonality between gradient and search direction is 
      !-- dectected i.e. value of orthoCheck is less that the 
      !-- orthogonality threshhold, then exit BFGS and execute the 
      !-- restart procedure.

      if ((abs(orthoCheck)).lt.(1.0e-8)) then

#ifdef _MPI_VERSION         
         if(rank.eq.0) then
#endif
            write(scr_unit,*)'OrthoCheck Failed...'
            write(scr_unit,*)'orthoCheck = ',orthoCheck
            write(scr_unit,*)
     &      'Search direction is orthogonal to the gradient'
#ifdef _MPI_VERSION 
         end if
#endif

#ifdef _MPI_VERSION 
         if(rank.eq.0) then
#endif
            rewind (ac_unit)
            do i = 1,jbody
               write (ac_unit,900) ac_his(i,1), (ac_his(i,j+1),j=1,ifun)
            end do
            write(ghis_unit,500) icg,ifun,compObj,cgradmag
#ifdef _MPI_VERSION 
         end if
#endif
         
         nflag = 6

         !-- Set the initial angle of attack values for the 1st 
         !-- iteration of the restart to the initial angle of attack 
         !-- values from the last successful design iteration before 
         !-- the restart.

         alphas = alphas_restart
         
         return

      endif   

C     IF OUTPUT IS DESIRED,TEST IF THIS IS THE CORRECT ITERATION
C     AND IF SO, WRITE OUTPUT.

      if (iout .eq.0) goto 60
      if (ioutk.ne.0) goto 50

#ifdef _MPI_VERSION
      if (rank.eq.0) then
#endif
         write(ghis_unit,500) icg,ifun,fmin,cgradmag
         call flush(ghis_unit)
#ifdef _MPI_VERSION
      end if
#endif

 50   ioutk=ioutk+1
      if (ioutk.eq.iout) ioutk=0
C     
C     BEGIN LINEAR SEARCH. ALFA IS THE STEPLENGTH.
C     SET ALFA TO THE NONRESTART CONJUGATE GRADIENT ALFA.
C     
 60   alfa=alfa*dg/dg1
C     
C     IF NMETH=1 OR A RESTART HAS BEEN PERFORMED, SET ALFA=1.0.
C     
      alfa=1.d0
C     
C     IF IT IS THE FIRST ITERATION, SET ALFA=1.0/DSQRT(GSQ),
C     WHICH SCALES THE INITIAL SEARCH VECTOR TO UNITY.

c     if (rsw) alfa=1.d0/dsqrt(gsq)
c     if (rsw) alfa=5.d-2/dsqrt(gsq)
      if (rsw) alfa = bstep/dsqrt(gsq)

#ifdef _MPI_VERSION
      !-- Just to be safe, broadcast alfa to every processor...
      call MPI_BCAST(alfa,1,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,ierr)
#endif

C     THE LINEAR SEARCH FITS A CUBIC TO F AND DAL, THE FUNCTION AND ITS
C     DERIVATIVE AT ALFA, AND TO FP AND DP,THE FUNCTION
C     AND DERIVATIVE AT THE PREVIOUS TRIAL POINT AP.
C     INITIALIZE AP ,FP,AND DP.


C     START CHANGES MADE BY LASLO
      

      if(new_search) then
c     left bracket
         aleft=0.d0
         fleft=fmin
         dleft=dg1

c     right bracket
         aright=0.d0
         fright=fleft
         dright=dleft
      else
         ap=0.d0
         fp=fmin
         dp=dg1
      endif
      

C     END CHANGES MADE BY LASLO

C     
C     SAVE THE CURRENT DERIVATIVE TO SCALE THE NEXT SEARCH VECTOR.
C     
      dg=dg1

     
C     UPDATE THE ITERATION.

      wt_iter=wt_iter+1
      iter=iter+1
      icg=icg+1
      ifsd=0
C     
C     CALCULATE THE CURRENT STEPLENGTH  AND STORE THE CURRENT X AND G.
C     

C     START CHANGES MADE BY LASLO

c     note these are redundant

c      ap=0.d0
c     fp=fmin
c      dp=dg1      

C     END CHANGES MADE BY LASLO

      step=0.d0
      do i=1,ndv
         step=step+wbfgs(i)*wbfgs(i)
         nxpi=nx+i
         ngpi=ng+i
         wbfgs(nxpi)=dvs(i)
         wbfgs(ngpi)=compGrad(i)
      enddo
      step=dsqrt(step)
c     
c     BEGIN THE LINEAR SEARCH ITERATION.

 80   continue !-- Loop back to this point if failed Wolfe conditions

      !-- Reset 'bad flow-solve' counter

      badfs=0

      !-- If warm-starts are enabled, store the  final angle of attack
      !-- from the last design iteration as the initial angle of 
      !-- attack used for this design iteration. Also store the
      !-- final flow solution from the last successful design 
      !-- iteration as the initial flow solution to be used for this 
      !-- design iteration (H.Buckley).

      if ((ifirst.eq.0).and.(warm_start)) then

         alphas_init = alphas
         
         q_init = qwarm

      end if

      !-- Check if the number of line search attempts has exceeded the
      !-- maximum allowable number of line search attempts. If so, then
      !-- execute the automatic restart procedure (This is done in
      !-- the offDesWts subroutine)
      
      if (ifsd.gt.20) then
         write (scr_unit,81) 
 81      format(/3x,'Line search is not converging!!',/)

#ifdef _MPI_VERSION      
         if(rank.eq.0) then
#endif
            rewind (ac_unit)
            do i = 1,jbody
               write (ac_unit,900) ac_his(i,1), (ac_his(i,j+1),j=1,ifun)
            end do
            write(ghis_unit,500) icg,ifun,compObj,cgradmag
#ifdef _MPI_VERSION 
         end if
#endif

         !-- Set the initial angle of attack values for the 1st 
         !-- iteration of the restart to the initial angle of attack 
         !-- values from the last successful design iteration before 
         !-- the restart.

         alphas = alphas_restart

         nflag = 5              !TML added this for automatic restart

         goto 171 !-- Enter weight update procedure if line search stall

         return
      end if

 200  continue !-- Loop back to this point if a bad flow-solve 

      !-- Check if the number of bad flow solves has exceeded the
      !-- maximum allowable number of bad flow solves. If so, then
      !-- execute the automatic restart procedure (This is done in
      !-- the offDesWts subroutine)
      
      if (badfs.gt.10) then
         write (scr_unit,82) 
 82      format(/3x,'Too many bad flow solves!!',/)

#ifdef _MPI_VERSION
         if(rank.eq.0) then
#endif
            rewind (ac_unit)
            do i = 1,jbody
               write (ac_unit,900) ac_his(i,1), (ac_his(i,j+1),j=1,ifun)
            end do
            write(ghis_unit,500) icg,ifun,compObj,cgradmag
#ifdef _MPI_VERSION
         end if
#endif

         !-- Set the initial angle of attack values for the 1st 
         !-- iteration of the restart to the initial angle of attack 
         !-- values from the last successful design iteration before 
         !-- the restart.

         alphas = alphas_restart

         nflag = 5              !TML added this for automatic restart

         goto 171 !-- Enter weight update procedure if exeeded bad flow 
                  !-- solves

         return
      end if

C     CALCULATE THE TRIAL POINT.

 90   do i=1,ndv
         nxpi=nx+i
         dvs(i)=wbfgs(nxpi)+alfa*wbfgs(i)
      enddo

C     EVALUATE THE FUNCTION AT THE TRIAL POINT.
      
 211  continue

      !-- Read information from restart file

      if (opt_restart) then

         rewind(72)

         read(72) wt_cycle
         read(72) nc,mpopt,mp
         read(72) (dvs(i),i = 1,nc+mpopt)
         read(72) (dv_scal(i),i = 1,ibsnc)
         read(72) pobj,obj, clt, cdt, cmt
         read(72) alpha, resid, slope,rhoinf,pinf
         read(72) (grad(i),i = 1,ibsnc)
         read(72) (alphas(i),i = 1,nopc)
         read(72) (WBFGS(i),i = 1,ibsnc*(ibsnc+7)/2)
         read(72) ifsd,ifun,nflag,iter,ioutk,ndvtot,nx,ng,ncons,badfs
         read(72) ico,icg,itfirst,nxpi,ngpi,ifirst,warmset,ngdv
         read(72) alfa,dg,step,fmin,dg1,bstep,ap
         read(72) dleft,fleft,aleft,dright,fright,aright,fp,dp      
         read(72) rsw,new_search,restart
         read(72) (obj0s(i),i = 1,mpopt)
         read(72) (objs(i),i = 1,nopc)
         read(72) (wmpo(i),i = 1,nopc) 
         read(72) ((grads(i,k),i = 1,20),k=1,ibsnc)
         read(72) (((((qwarm(i,j,k,l,m),i=1,warmset),j=1,jdim),
     &        k=1,kdim),l=1,ndim),m=1,mpopt)    
         read(72) ((((qtmp(j,k,l,m),j=1,jdim),
     &        k=1,kdim),l=1,ndim),m=1,mpopt)
         read(72) (((q(j,k,l),j=1,jdim),k=1,kdim),l=1,ndim)
         read(72) (cl(i),i = 1,mpopt)
         read(72) (cd(i),i = 1,mpopt)
         read(72) (cm(i),i = 1,mpopt)
         read(72) ((xsave(j,k),j=1,jdim),k=1,kdim)
         read(72) ((ysave(j,k),j=1,jdim),k=1,kdim)
         read(72) wt_iter, rest_iter
         read(72) MaxMach_r, cdt_r, clt_r
         read(72) ((x_r(j,k),j=1,jdim),k=1,kdim)
         read(72) ((y_r(j,k),j=1,jdim),k=1,kdim)
         read(72) (alphas_init(i),i = 1,nopc)
         read(72) (alphas_restart(i),i = 1,nopc)
      
         write(scr_unit,212) ifun
 212     format(/3x,'Restarting optimization at iteration [',i3,']') 

         !-- Set the warm-start solution to be saved to the 'q_restart##'
         !-- file
         
         q_init = qwarm

         !-- Increment the restart counter
   
         rest_iter = rest_iter + 1

         opt_restart=.false.
     
         !-- Decrement the iteration counter so that the counter number
         !-- for the 1st iteration of the restart matches the counter
         !-- number for the last iteration before the restart
   
         ifun = ifun - 1

      end if
   
c     -- adjust grid --
      if (ngdv.gt.0) then
c     -- restore original grid --
         do k = 1,kdim
            do j = 1,jdim
               x(j,k) = xsave(j,k)
               y(j,k) = ysave(j,k)
            end do
         end do

         ifun_tmp = ifun+1

         call regrid( -1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)
      end if
         

c     -- write current airfoil geometry and control points to file --
c     -- Only done by processor 0

#ifdef _MPI_VERSION
      if(rank.eq.0) then
#endif
         rewind (ac_unit)
         rewind (bc_unit)
         do j = jtail1,jtail2
            write (ac_unit,600) x(j,1), y(j,1)
         end do
         call flush(ac_unit)
         do i = 1,nc
            write (bc_unit,600) bcp(i,1), bcp(i,2)
         end do
         call flush(bc_unit)
#ifdef _MPI_VERSION
      end if
#endif

c     -- muti-point optimization loop --
c     -- if mpopt = 1 => single point design --
c     -- routines calcobj and calcgrad require ndv = ngdv+1 with the
c     aoa as the last design variable --

      !-- Increment design iteration counter

      ifun=ifun+1

      !-- Write information to restart file
      
      write(scr_unit,213) ifun
 213  format(/3x,'Writting to restart file at 
     & beginning of iteration [',i3,']')

      rewind(72)

      write(72) wt_cycle
      write(72) nc,mpopt,mp
      write(72) (dvs(i),i = 1,nc+mpopt)
      write(72) (dv_scal(i),i = 1,ibsnc)
      write(72) pobj,obj, clt, cdt, cmt
      write(72) alpha, resid, slope,rhoinf,pinf
      write(72) (grad(i),i = 1,ibsnc)
      write(72) (alphas(i),i = 1,nopc)
      write(72) (WBFGS(i),i = 1,ibsnc*(ibsnc+7)/2)
      write(72) ifsd,ifun,nflag,iter,ioutk,ndvtot,nx,ng,ncons,badfs
      write(72) ico,icg,itfirst,nxpi,ngpi,ifirst,warmset,ngdv
      write(72) alfa,dg,step,fmin,dg1,bstep,ap
      write(72) dleft,fleft,aleft,dright,fright,aright,fp,dp
      write(72)rsw,new_search,restart
      write(72) (obj0s(i),i = 1,mpopt)
      write(72) (objs(i),i = 1,nopc)
      write(72) (wmpo(i),i = 1,nopc)
      write(72) ((grads(i,k),i = 1,20),k=1,ibsnc)
      write(72) (((((qwarm(i,j,k,l,m),i=1,warmset),j=1,jdim),
     &        k=1,kdim),l=1,ndim),m=1,mpopt)
      write(72) ((((qtmp(j,k,l,m),j=1,jdim),
     &        k=1,kdim),l=1,ndim),m=1,mpopt)
      write(72) (((q(j,k,l),j=1,jdim),k=1,kdim),l=1,ndim)
      write(72) (cl(i),i = 1,mpopt)
      write(72) (cd(i),i = 1,mpopt)
      write(72) (cm(i),i = 1,mpopt)
      write(72) ((xsave(j,k),j=1,jdim),k=1,kdim)
      write(72) ((ysave(j,k),j=1,jdim),k=1,kdim)
      write(72) wt_iter, rest_iter
      write(72) MaxMach_r, cdt_r, clt_r
      write(72) ((x_r(j,k),j=1,jdim),k=1,kdim)
      write(72) ((y_r(j,k),j=1,jdim),k=1,kdim)
      write(72) (alphas_init(i),i = 1,nopc)
      write(72) (alphas_restart(i),i = 1,nopc)

      !-- Set file prefixes for a restart
      !-- For the inital run before any restarts, the output and restart
      !-- file prefixes are 'rest00' and rest00'

#ifdef _MPI_VERSION
      if (rank==0) then
#endif
         !-- Update the output file prefix to reflect the current value
         !-- of the restart counter

         d10  = rest_iter / 10
         d1   = rest_iter - (d10*10)

         output_file_prefix = 'rest'
         namelen =  len_trim(output_file_prefix)
         output_file_prefix(namelen+1:namelen+2)
     &          = char(d10+48)//char(d1+48)

         !-- Update the restart file prefix to reflect the previous value
         !-- of the restart counter

         d10  = (rest_iter-1) / 10
         d1   = (rest_iter-1) - (d10*10)

         restart_file_prefix = 'rest'
         namelen =  len_trim(restart_file_prefix)
         restart_file_prefix(namelen+1:namelen+2)
     &          = char(d10+48)//char(d1+48)

         opt_restart = .true.
         obj_restart = .true.

         rewind(prefix_unit)
         write(prefix_unit,*)output_file_prefix
         write(prefix_unit,*)restart_file_prefix
         write(prefix_unit,*)opt_restart
         write(prefix_unit,*)obj_restart
         
         opt_restart = .false.
         obj_restart = .false.
#ifdef _MPI_VERSION
      end if
#endif

      !-- store airfoil shape --
      i = 1
      do j = jtail1, jtail2
         ac_his(i,ifun+1) = y(j,1)
         i = i + 1
      end do

      !-- Write current design variables to file --

#ifdef _MPI_VERSION
      !-- Only done on processor 0
      if (rank.eq.0) then
#endif
         write (dvhis_unit,720) ifun, (dvs(j),j=1,ndv)
         call flush(dvhis_unit)

      !-- Continue writing search vector history file (H.Buckley) --
      !-- Only done on processor 0

         write (svhis_unit,712) ifun, iter, alfa, (wbfgs(j),j=1,ndv)
#ifdef _MPI_VERSION
      end if
#endif

      !-- Set design point operating conditions according to processor
      !-- rank. i.e. the processor with rank N will handle computation
      !-- of design point N+1
#ifdef _MPI_VERSION
      mp = rank+1
#else
      mp = 1
#endif

      !-- Define number of design variables at design point 

      if ( dvalfa(mp) ) then
         ndv = ngdv+1
      else
         ndv = ngdv
      end if

      !-- For points where AOA is a design varible, store AOA value for 
      !-- use in a restart

#ifdef _MPI_VERSION

      if (dvalfa(mp)) then

         !-- Collect AOA values and store in array 'alphas_init' on 
         !-- processor 0:         

         if (rank==0) then

            alphas_init(1) = dvs(idv(ngdv+mp))
  
         else
            
            call MPI_SEND(dvs(idv(ngdv+mp)), 1, MPI_DOUBLE_PRECISION, 
     &                 0, 1, COMM_CURRENT, ierr)            

         end if

      end if

      if (rank==0) then

         do i = 2, mpopt
            
            if (dvalfa(i)) then
            
               sendProc = i-1
               call MPI_RECV(dvs(idv(ngdv+mp)), 1, MPI_DOUBLE_PRECISION, 
     &           sendProc , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

               alphas_init(i) = dvs(idv(ngdv+mp))
            
            end if 

         end do
         
      end if

      !-- Broadcast the array 'alphas_init' to all processors

      call MPI_BCAST(alphas_init ,mpopt,MPI_DOUBLE_PRECISION,0,
     &   COMM_CURRENT, ierr)    

#else

      if (dvalfa(mp)) then

            alphas_init(1) = dvs(idv(ngdv+mp))

      end if

#endif

      if (opt_meth.ne.4) fsmach = fsmachs(mp)
      re_in = reno(mp)
      cl_tar = cltars(mp)
      cd_tar = cdtars(mp)
      wfl = wfls(mp)
      obj_func = objfuncs(mp)
      clalpha = clalphas(mp)
      clalpha2 = clalpha2s(mp)
      clopt = clopts(mp)
      obj0 = obj0s(mp)

      !-- If we are 'warm starting' the flow solver, then take the 
      !-- initial guess for angle of attack as the final angle of
      !-- attack from the previous iteration. Otherwise, for
      !-- 'cold-starting' the flow solver, always revert to the
      !-- original angle of attack specified in the input file as
      !-- the initial guess for angle of attack (H.Buckley)

      !-- adjust angle of attack --

      if (.not. clopt) then
         
         if ( dvalfa(mp) ) then

            atmp = dvs(ngdv+1)
            dvs(ngdv+1) = dvs(idv(ngdv+mp))
            alpha = dvs(ngdv+1)
          
         end if

      else
         if ((ifirst.eq.0).and.(warm_start)) then
            alpha = alphas(mp)
         else
            alpha = alphas_init(mp)
         end if
      end if


      frozen = .false.       !flag used for freezing dissipation

c     -- restore last converged solution --
      if ((ifirst.eq.0).and.(warm_start)) then
         do n=1,4
            do k=1,kdim
               do j=1,jdim

                  if(warmset.eq.1) then
                     q(j,k,n)=qwarm(1,j,k,n,mp)
                  else 
                     q(j,k,n)=qtmp(j,k,n,mp)
                  endif

               end do
            end do
         end do      
c     -- update turbulent viscosity --
         if (viscous .and. turbulnt .and. itmodel.eq.2) then
            do k = 1,kdim
               do j = 1,jdim
c     START CHANGES MADE BY LASLO
                  if(warmset.eq.1) then
                     turre(j,k) = qwarm(1,j,k,5,mp)
                  else 
                     turre(j,k) = qtmp(j,k,5,mp)
                  endif
C     END CHANGES MADE BY LASLO
               end do
            end do
         end if
      end if

c     -- compute obj. function --

      call CalcObj (obj, jdim, kdim, ndim, ifirst, indx, icol, iex,
     &        q, cp, xy, xyj, x, y, cp_tar, fmu, vort, turmu, work1,
     &        ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat,
     &        itfirst, obj0, mp, ifun, obj1, lcon )

      !-- If negative Jacobian or flow solve connvergence problems are
      !-- encountered at any design point, ensure that all processors
      !-- redo their respective flow solve computations at the trial
      !-- desing iteration with modified step size 'alfa'
      !--
      !--
      !-- First collect the values of badjac and resid from each 
      !-- processor and store in arrays on processor 0

      !-- Collect badjac values and store in array badjacs:  

#ifdef _MPI_VERSION       

      if (rank==0) then

         badjacs(1) = badjac
  
      else

         call MPI_SEND(badjac, 1, MPI_LOGICAL, 0, 1, 
     &                 COMM_CURRENT, ierr)            

      end if

      if (rank==0) then

         do i = 2, mpopt
            
            sendProc = i-1
            call MPI_RECV(badjac, 1, MPI_LOGICAL, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

            badjacs(i) = badjac
            
         end do
         
      end if
 
      !-- Broadcast the array of negative jacobian values to all 
      !-- processors

      call MPI_BCAST(badjacs,mpopt,MPI_LOGICAL,0,COMM_CURRENT,
     &     ierr)  

#else

         badjacs(1) = badjac

#endif

      !-- Collect resid values and store in array resids:

#ifdef _MPI_VERSION
     
      if (rank==0) then

         resids(1) = resid
  
      else
          
         call MPI_SEND(resid, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)            

      end if

      if (rank==0) then

         do i = 2, mpopt
            
            sendProc = i-1

            call MPI_RECV(resid, 1, MPI_DOUBLE_PRECISION, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

            resids(i) = resid
            
         end do
         
      end if

      !-- Broadcast the array of residual values to all 
      !-- processors

      call MPI_BCAST(resids,mpopt,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)   

#else

         resids(1) = resid

#endif   

      !-- Scan 'badjacs' and 'resids' arrays to determine if a 
      !-- negative Jacobian or flow solve convergence problem was
      !-- encountered at any design point

      do i = 1, mpopt

         !-- If a negative Jacobian value was detected at any design 
         !-- point then set 'badjac' equal to 'TRUE'

         if (badjacs(i)) then
            
            badjac = .true.

            goto 123

         end if

         !-- If flow solver problems were encountered at any design
         !-- point then set resid to the residual value at the first 
         !-- design point where a flow solve convergence problem was
         !-- encountered

         if (resids(i).gt. 1.d-8) then
            
            resid = resids(i)

            goto 123

         end if

      end do

 123  continue

c     -- detection of negative Jacobian --
      if (badjac) then
         write (scr_unit,905)
         call flush(6)
         badjac = .false.

c     START CHANGES MADE BY LASLO
         write (scr_unit,*) 'old alfa => ', alfa
         if(new_search) then
            alfa = 0.5*(alfa+aleft)
         else
            alfa = 0.5*alfa
         endif
         write (scr_unit,*) 'new alfa => ', alfa

C     END CHANGES MADE BY LASLO

         ifun = ifun - 1
         if ( dvalfa(mp) ) then
            dvs(idv(ngdv+mp)) = dvs(ngdv+1)
            dvs(ngdv+1) = atmp
         end if
         ndv = ndvtot

         !-- Increment 'bad flow slove' counter

         badfs = badfs + 1
         
         goto 200         
      end if
 905  format(3x,'Negative Jacobian detected, reducing alfa')

c     -- check convergence of flow solver --
c     -- note: qwarm is updated after successful line search --
      if (resid .gt. 1.d-8 ) then

         write (scr_unit,510) resid
 510     format(3x,'Flow solver convergence problems! Resid:',e12.4)

         if (ifirst.eq.0) then
c     -- restore last converged solution --
            write (scr_unit,520)
 520        format(3x,'Restoring last converged solution')
         else
            write (scr_unit,*) 'Solution reset to freestream!!'
            write (scr_unit,*) 'ifirst = 1'
         end if
         call flush(6)

c         if (clopt) alpha = alpha/2.d0
c         if (clopt) alphawarm = alpha

         if ( dvalfa(mp) ) then
            dvs(idv(ngdv+mp)) = dvs(ngdv+1)
            dvs(ngdv+1) = atmp
         end if
         ndv = ndvtot

         restart = .false.
         write (scr_unit,*) 'old alfa => ', alfa

c     START CHANGES MADE BY LASLO

         if(new_search) then
            alfa = 0.5*(alfa+aleft)
         else
            alfa = 0.5*alfa
         endif

C     END CHANGES MADE BY LASLO

         write (scr_unit,*) 'new alfa => ', alfa
         ifun = ifun - 1

         !-- Increment 'bad flow slove' counter

         badfs = badfs + 1

         goto 200

      else
      
         !-- Send final angle of attack from this iteration to array 
         !-- 'alphas' on processor 0 for warm-starting solution of next 
         !-- iteration

#ifdef _MPI_VERSION

         if (rank==0) then

            if (clopt) then
         
               alphas(1) = alpha

            end if

            do i = 2, mpopt

               if (clopts(i)) then

                  sendProc = i-1

                  call MPI_RECV(alpha, 1, MPI_DOUBLE_PRECISION,  
     &            sendProc, 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)
               
                  alphas(i) = alpha
               
               end if

            end do
            
         else

            if (clopt) then
            
               call MPI_SEND(alpha, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                    COMM_CURRENT, ierr)            
            
            end if 

         end if ! (rank==0)

         !-- Broadcast 'alphas' to all processors

         call MPI_BCAST(alphas,mpopt,MPI_DOUBLE_PRECISION,0,
     &        COMM_CURRENT,ierr)    

         !-- The storage of AOA's from all processors done on processor
         !-- zero overwrites the AOA for processor zero, so restore the
         !-- correct AOA on processor zero

         if (rank.eq.0) alpha = alphas(1)

#else

               alphas(1) = alpha

#endif

c     -- save converged solution for warm starts --
         do n=1,ndim
            do k=1,kdim
               do j=1,jdim
                  qtmp(j,k,n,mp)=q(j,k,n)
               end do
            end do
         end do

      end if !(resid .gt. 1.d-8 )

#ifdef _MPI_VERSION

      !-- Force all processors to pause here until the objective
      !-- function at every design point has been calculated

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      !-- Apply the design point weighting to the computed objective
      !-- function

      wobj = wmpo(mp)*obj

#ifdef _MPI_VERSION

      !-- Use MPI_REDUCE to compute the composite objective function
      !-- and store on processor 0

      call MPI_REDUCE(wobj,compObj,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,
     &     COMM_CURRENT, ierr)

      !-- Broadcast the composite objective function value to all
      !-- processors

      call MPI_BCAST(compObj,1,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)  

#else

      compObj = wobj

#endif

      !-- If logging of grid or solution history is enabled, then
      !-- write these values to file now (H.Buckley).

      if((grid_his).or.(sol_his)) then

         !-- Add jacobian scaling...don't ask why!!!!
            
         do n = 1,ndim
            do j = 1,jmax
               do k = 1,kmax
                  q(j,k,n) = (q(j,k,n))/(xyj(j,k))
               end do
            end do
         end do

         call history(x, y, q, q, ifun, turmu, xy, xyj, jdim, 
     &                      kdim, mp )

         !-- Remove jacobian scaling...pretend it was never there!!!!
            
         do n = 1,ndim
          do j = 1,jmax
             do k = 1,kmax
               q(j,k,n) = (q(j,k,n))*(xyj(j,k))
             end do
           end do
         end do

      end if
      

c         objs(mp) = obj


c     -- pareto front information --
c     -- Only done on processor 0
#ifdef _MPI_VERSION
      if(rank.eq.0)then
#endif
         if (obj_func.eq.6 .and. mpopt.eq.1) then
            write (par_unit,550) ico+1, fcl, fcd, ftc, fcl+ftc, fcd+ftc
            call flush(par_unit)
         end if
#ifdef _MPI_VERSION
      end if
#endif

c     -- check T.E. slope --
      theta1 = atan2d( y(jtail2,1)-y(jtail2-1,1), x(jtail2,1) -
     &        x(jtail2-1,1) )
         
      theta2 = atan2d( y(jtail1,1)-y(jtail1+1,1), x(jtail1,1) -
     &        x(jtail1+1,1))
c     slope = tand( .5d0*(theta1+theta2) )  
      slope = 0.5d0*(theta1+theta2)

c     -- output info --
      pobj = obj
      if ( obj_func.eq.5 ) pobj = 1.d0/pobj
      if ( obj_func.eq.4 ) pobj = - pobj

      !-- Write thickenss constraint info to file
      !-- Only don on processor 0
#ifdef _MPI_VERSION
      if (rank.eq.0)then
#endif
         if (mp.eq.1 .and.
     &        (ntcon.gt.0 .or. nrtcon.gt.0 .or. wac.gt.0)) then
            write (tcon_unit,720) ifun,(cty(i),i=1,ntcon), area,
     &           (crth(i),i=1,nrtcon), (crthx(i),i=1,nrtcon)
            call flush(tcon_unit)
         endif
#ifdef _MPI_VERSION
      end if
#endif
         
c     -- store pressure distribution --
      if (mpopt.gt.1) rewind (n_mpcp)
      if (ifun .lt. 199) then
         i = 1
         do j = jtail1, jtail2
            cp_his(i,ifun,mp) = cp(j,1)
            if (mpopt.gt.1) write (n_mpcp,900) x(j,1),cp(j,1)
            i = i + 1
         end do
      end if
         
c     -- compute obj. func. gradient --
      if (coef_frz) frozen = .true.

      call CalcGrad (obj, grad, jdim, kdim, ndim, ifirst, q, cp, xy,
     &        xyj, x, y, xsave, ysave, dx, dy, lambda, fmu, vort, turmu,
     &        cp_tar, bap, bcp, bt, bknot, dvs, idv, indx, icol, iex,
     &        work1, ia, ja, ipa,jpa, iat, jat, ipt, jpt, as, ast, pa,
     &        pat, opwk, obj0, mp, ifun)

      !-- Calculate the L2 norm for the gradient associated with the 
      !-- design point assigned to this processor

      gradmag = 0.0
      do i=1,ndv
         gradmag = gradmag + grad(i)*grad(i)
      end do
      gradmag = dsqrt(gradmag)

#ifdef _MPI_VERSION

      !-- Force all processors to pause here until the objective
      !-- function gradient at every design point has been calculated

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      !-- Reset ndv to total number of design variables 

      ndv = ndvtot

      !-- Initialize weighted gradient vector to zero

      do i=1,ndv
         wgrad(i)=0.0
      end do

      !-- Apply the design point weighting to the geometric design
      !-- variable components of the gradient vector

      do i=1,ngdv
         wgrad(i)= wmpo(mp)*grad(i)
      end do

#ifdef _MPI_VERSION

      !-- Use MPI_REDUCE to sum the weighted geometric design
      !-- variable components of the gradient vector from each processor
      !-- and store the result on processor 0

      call MPI_REDUCE(wgrad,compGrad,50,MPI_DOUBLE_PRECISION,MPI_SUM,
     &    0, COMM_CURRENT, ierr)

      !-- Add angle-of-attack gradient components to composite gradient
      !-- vector
      
      if (dvalfa(mp)) then

         walfaGrad =  wmpo(mp)*grad(ngdv+1)

         if (rank==0) then

            compGrad(idv(ngdv+mp)) = walfaGrad

         else
            
            call MPI_SEND(walfaGrad, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                    COMM_CURRENT, ierr)            
         end if

      end if

      if (rank==0) then

         do mp = 2, mpopt

            sendProc = mp-1

            if (dvalfa(mp)) then

              call MPI_RECV(walfaGrad, 1, MPI_DOUBLE_PRECISION, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

              compGrad(idv(ngdv+mp)) = walfaGrad

            end if
         end do
         mp = 1

      end if

      !-- Broadcast the composite gradient value to all processors

      call MPI_BCAST(compGrad,ndv,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)  

#else

      do i = 1,ndv
         compGrad(i) = wgrad(i)
      end do
      
      if (dvalfa(mp)) then

         walfaGrad =  wmpo(mp)*grad(ngdv+1)

         compGrad(idv(ngdv+mp)) = walfaGrad

      end if
    
#endif

      !-- Calculate the L2 norm fo the composite gradient

#ifdef _MPI_VERSION

      !-- Only done on processor 0

      if(rank.eq.0) then

         cgradmag = 0.0
         do i=1,ndv
            cgradmag = cgradmag + compGrad(i)*compGrad(i)
         end do
         cgradmag = dsqrt(cgradmag)

      end if

      !-- Broadcast the L2 norm fo the composite gradient to all
      !-- processors
      
      call MPI_BCAST(cgradmag,1,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)

#else

         cgradmag = 0.0
         do i=1,ndv
            cgradmag = cgradmag + compGrad(i)*compGrad(i)
         end do
         cgradmag = dsqrt(cgradmag)

#endif

      ico = ico+1

c     -- re-adjust design variable vector --
      if ( dvalfa(mp) ) then
         dvs(idv(ngdv+mp)) = dvs(ngdv+1)
         dvs(ngdv+1) = atmp
      end if


      !-- Write design point objective function and gradient results 
      !-- to the repsective design point .ohis file

      if (mpopt.gt.1) then
c     start change by huafei
         cd(mp)=cdt
         cl(mp)=clt
         cm(mp)=cmt
c     end change by huafei
         if(ohisout) write (n_mpo,700) ifun,iter,pobj,gradmag, clt, cdt,
     &           cmt, alpha, resid
         if (ohisout) call flush(n_mpo)
      end if

#ifdef _MPI_VERSION

      !-- Pause until all processors are caught up

      call MPI_BARRIER(COMM_CURRENT,ierr)

#endif

      !-- Write design point objective function and gradient results 
      !-- to standard output file
         
      if (mpopt.eq.1) then
         write (scr_unit,710) ico, iter, obj, gradmag
      else
         write (scr_unit,711) ico, iter, obj, gradmag, mp
      end if
      call flush(6)

      !-- Write composite objective function and gradient results 
      !-- to the main .ohis file
      !-- Only done by processor 0

      if (mpopt.gt.1) then
#ifdef _MPI_VERSION
         if(rank.eq.0) then
#endif
            write (ohis_unit,705)  
     &            ifun, ico, iter, compObj, cgradmag, slope       
            call flush(ohis_unit)
            if(graphout.eq..true.)then
               call graph_out(ac_his,cp_his,jdim,kdim,ifun,cd,cl,cm)
            end if
#ifdef _MPI_VERSION
         end if
#endif
      else
         cl(1)=clt
         cd(1)=cdt
         cm(1)=cmt
#ifdef _MPI_VERSION
         if(rank==0) then
#endif
            write (ohis_unit,702) ico, iter, pobj, gradmag, clt, cdt, 
     &            cmt, alpha, resid, slope
            call flush(ohis_unit)
#ifdef _MPI_VERSION
         end if
#endif
         if(graphout.eq..true.) then
            call graph_out(ac_his,cp_his,jdim,kdim,ifun,cd,cl,cm)
         end if
      end if

      !-- Write the composite gradient vector to the .gvhis file
      !-- Only done by processor 0
#ifdef _MPI_VERSION
      if(rank.eq.0)then
#endif
         write (gvhis_unit,810) ifun, (compGrad(i),i=1,ndv)
         call flush(gvhis_unit)
#ifdef _MPI_VERSION
      end if
#endif

      !-- Calculate the maximum mach number in the flow solution from
      !-- the current design iteration. 

      MaxMach = 0.0

      do j = 1, jdim

         do k = 1, kdim

            !-- Calculate velocities in x and y directions
            
            U = q(j,k,2)/q(j,k,1)
            V = q(j,k,3)/q(j,k,1)

            !-- Calculate pressure

            P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))

            !-- Calculate sound speed
            
            a = sqrt(1.4*P/q(j,k,1))

            !-- Calculate Mach number

            Mach = sqrt(U**2 + V**2)/a

            if (Mach.gt.MaxMach) then

               MaxMach = Mach
               jMaxMach = j
               kMaxMach = k

            end if

         end do

      end do

      !-- Save max Mach number to file

      if (mout) write(mach_unit,131) ifun,iter,MaxMach,jMaxMach,kMaxMach
 131  format(i4, 3x, i4, 3x, e16.10, 3x, i4, 3x, i4)

      !-- Increment search direction counter

      ifsd=ifsd+1

C     TEST IF THE MAXIMUM NUMBER OF FUNCTION CALLS HAVE BEEN USED.
      if(ifun.le.mxfun) goto 110
      nflag=1

c     -- rescale design variables --
c      do i = 1,ndv
c         dvs(i) = dvs(i)*dv_scal(i)
c      end do  

#ifdef _MPI_VERSION
      if(rank.eq.0) then
#endif
         rewind (ac_unit)
         do i = 1,jbody
            write (ac_unit,900) ac_his(i,1), (ac_his(i,j+1),j=1,ifun)
         end do
         write(ghis_unit,500) icg,ifun,compObj,cgradmag
#ifdef _MPI_VERSION
      end if
#endif

      !-- Set the initial angle of attack values for the 1st 
      !-- iteration of the restart to the initial angle of attack 
      !-- values from the last successful design iteration before 
      !-- the restart.

      alphas = alphas_restart

      write (scr_unit,*) 'Maximum number of function calls exceeded!!'
      return

C     
C     COMPUTE THE DERIVATIVE OF F AT ALFA. THIS IS THE RATE OF CHANGE OF 
C     THE OBJECTIVE FUNCTION WITH RESPECT TO ALFA, I.E. THE RATE OF CHANGE
C     OF OBJECTIVE FUNCTION AT THE CURRENT POINT WHEN A STEP IS TAKEN IN
C     IN THE SEARCH DIRECTION.
C 

      !-- Dot product of gradient vector at trial pt and search 
      !-- direction vector
 110  dal=0.d0
      do i=1,ndv
         dal=dal+compGrad(i)*wbfgs(i)
      enddo

      !-- Dot product of gradient vector at start pt and search 
      !-- dir vector
      dgTest=0.d0
      do i=1,ndv
         dgTest=dgTest+gmin(i)*wbfgs(i) 
      enddo


      !-- Print out variables related to the evaluation of the
      !-- strong Wolfe conditions

713   format (//3x, 'Variables used in evaluation of Wolfe condtions:'
     &        //3x, 'Design Iteration:', i4, 
     &        /3x, 'Search Direction #:', i4,  
     &        /3x, 'Step Size alfa:', e16.8,
     &        /3x, 'fmin:', e16.8, 
     &        /3x, 'dg: ', e16.8,
     &        /3x, 'compObj: ', e16.8,
     &        /3x, 'dal: ', e16.8, //) 

      write (scr_unit,713) ifun, iter, alfa, fmin, dg, compObj, dal

C     START CHANGES MADE BY LASLO

C     
C     TEST WHETHER THE NEW POINT HAS A NEGATIVE SLOPE BUT A HIGHER
C     FUNCTION VALUE THAN ALFA=0. IF THIS IS THE CASE,THE SEARCH
C     HAS PASSED THROUGH A LOCAL MAX AND IS HEADING FOR A DISTANT LOCAL
C     MINIMUM.

      if (compObj.gt.(fmin+1.e-4*alfa*dg).and. dal.lt.0.) then

         write(scr_unit,128) 
 128     format(/3x,'Search has passed thru a local maximum...',/3x,
     &     'Reduce step size by 1/3 and try search again',/)

         goto 160

      end if
     
C     IF NOT, TEST WHETHER THE STEPLENGTH CRITERIA HAVE BEEN MET.

C
C     THESE CRITERIA ARE KNOWN AS THE STRONG WOLFE CONDITIONS:

      if (compObj.gt.(fmin+1.e-4*alfa*dg) .OR. abs(dal/dg).gt.(0.9))
       
     >     goto 130

      write(scr_unit,129) 
 129  format(/3x,'Passed strong wolfe conditions',/)

      !-- A successful design iteration has been achieved, so save some
      !-- important information from this design iteration for use 
      !-- in the event of an auto-restart (H.Buckley)

      alphas_restart = alphas_init ! AOA values
      cdt_r = cdt
      clt_r = clt

      write(scr_unit,135) 
 135  format(/3x,'Set the AOA restart values to the initial AOA',/3x,
     &           'values from this successful iteration',/)

      !-- Write initial solution guess for this design iteration
      !-- to file

#ifdef _MPI_VERSION
      d10  = rank / 10
      d1   = rank - (d10*10)
#else
      d10  = 0 / 10
      d1   = 0 - (d10*10)
#endif

      !-- Create a unique 'initial solution guess' filename
      !-- for each processor

      filename = 'q_restart'
      namelen =  len_trim(filename)
      filename(namelen+1:namelen+2)=char(d10+48)//char(d1+48)
      
      !-- Build a 'remove' command that removes any exisitng q_restart## 
      !-- file 
          
      command = 'rm -f '
      command(7:39) = filename
      call system(command)    
      
      !-- Close old q_restart file

      close(456)
      
      !-- Open a new file to write a fresh q_restart file

      open(unit=456,file=filename,status='new',form='unformatted')

      if (ifirst.eq.0) then
         
         write(scr_unit,136)
 136     format(/3x,'Write init soln guess for this successful
     & design iteration to q_restart file')
         write(456) q_init

      end if

      close(456)

      !-- Replace the current grid file with the grid file corresponding
      !-- to the successful design iteration

#ifdef _MPI_VERSION
      if (rank.eq.0) then
#endif        
         ip=15
         call ioall (ip,0,jdim,kdim,q,work1(1,1),cp,work1(1,5),
     &        work1(1,6),work1(1,7),work1(1,8),xy,xyj,x,y, obj0s)
#ifdef _MPI_VERSION
      end if
#endif

      write(scr_unit,1002)

      !-- Store the maximum Mach number for this iteration

      MaxMach_r = MaxMach

      !-- Store the grid coordinates

      x_r = x
      y_r = y

#ifdef _MPI_VERSION
      call MPI_BARRIER(COMM_CURRENT, ierr)
#endif

C     IF THEY HAVE BEEN MET, TEST IF TWO POINTS HAVE BEEN TRIED
C     IF NMETH=0 AND IF THE TRUE LINE MINIMUM HAS NOT BEEN FOUND.
C     
      goto 170
C     
C     A NEW POINT MUST BE TRIED. USE CUBIC INTERPOLATION TO FIND
C     THE TRIAL POINT AT.
C

 130  write(scr_unit,132) 
 132  format(/3x,'Failed strong wolfe condtions..',/)

      write(scr_unit,133)
 133  format(/3x,'Executing line search algorithm to determine
     & new step size',/)

      write(scr_unit,1002)

c     NEW LINE SEARCH ALGORITHM


      if(new_search) then
         write(scr_unit,*) 'New line search beginning'
         if (dal.gt.0.d0) then
c     if this step and previous step bracket the solution         
c     use cubic interpolant of bracketting values to get next
c     estimate of alfa (at)
            u1=dleft+dal-3.d0*(fleft-compObj)/(aleft-alfa)
            u2=u1*u1-dleft*dal
            if(u2.lt.0.d0) u2=0.d0
            u2=dsqrt(u2)
            at=alfa-(alfa-aleft)*(dal+u2-u1)/(dal-dleft+2.d0*u2)

c     bisection search
c            at = (ap+alfa)/2.d0
            
            if(at.lt.1.1*dmin1(aleft,alfa).or.
     >           at.gt.0.9*dmax1(aleft,alfa))
     >           at = (aleft+alfa)/2.d0

            aright = alfa
            dright = dal
            fright = compObj
            alfa = at
            goto 80
         else if(dright.gt.0.d0) then
            u1=dright+dal-3.d0*(fright-compObj)/(aright-alfa)
            u2=u1*u1-dright*dal
            if(u2.lt.0.d0) u2=0.d0
            u2=dsqrt(u2)
            at=alfa-(alfa-aright)*(dal+u2-u1)/(dal-dright+2.d0*u2)
            
c     bisection search
c            at = (app+alfa)/2.d0

            if(at.lt.1.1*dmin1(aright,alfa) .or. 
     >           at.gt.0.9*dmax1(aright,alfa))
     >           at = (aright+alfa)/2.d0

            dleft = dal
            aleft = alfa
            fleft = compObj
            alfa = at
            goto 80        
         else
            u1=dleft+dal-3.d0*(fleft-compObj)/(aleft-alfa)
            u2=u1*u1-dleft*dal
            if(u2.lt.0.d0) u2=0.d0
            u2=dsqrt(u2)
            at=alfa-(alfa-aleft)*(dal+u2-u1)/(dal-dleft+2.d0*u2)

            if(at.lt.1.01*dmax1(aleft,alfa)) at = dmax1(aleft,alfa)*2.d0
            aleft = alfa
            dleft = dal
            fleft = compObj
            alfa = at
            goto 80
         endif
      endif  

c     old line search

      if ((ap-alfa) .eq. 0.d0) then
clb         if (clopt) alpha = alpha/2.d0
clb         if (clopt) alphas(mp) = alpha

            if ( dvalfa(mp) ) then
               dvs(idv(ngdv+mp)) = dvs(ngdv+1)
               dvs(ngdv+1) = atmp
            end if
            ndv = ndvtot

            restart = .false.
            write (scr_unit,*) 'old alfa2 => ', alfa

c     START CHANGES MADE BY LASLO

            if(new_search) then
               alfa = 0.5*(alfa+aleft)
            else
               alfa = 0.5*alfa
            endif

C     END CHANGES MADE BY LASLO

            write (scr_unit,*) 'new alfa2 => ', alfa
            ifun = ifun - 1
            goto 200

      end if

      u1=dp+dal-3.d0*(fp-compObj)/(ap-alfa)
      u2=u1*u1-dp*dal
      if(u2.lt.0.d0) u2=0.d0
      u2=dsqrt(u2)
      at=alfa-(alfa-ap)*(dal+u2-u1)/(dal-dp+2.d0*u2)
      
c     
C     TEST WHETHER THE LINE MINIMUM HAS BEEN BRACKETED.
C     
      if ((dal/dp).gt.0.) goto 140
C     
C     THE MINIMUM HAS BEEN BRACKETED. TEST WHETHER THE TRIAL POINT LIES
C     SUFFICIENTLY WITHIN THE BRACKETED INTERVAL.
C     IF IT DOES NOT, CHOOSE AT AS THE MIDPOINT OF THE INTERVAL.
C     
      if (at.lt.(1.01d0*dmin1(alfa,ap)) .or. 
     >     at.gt.(.99d0*dmax1(alfa,ap))      ) at=(alfa+ap)*.5d0
      goto 150
C     
C     THE MINIMUM HAS NOT BEEN BRACKETED. TEST IF BOTH POINTS ARE
C     GREATER THAN THE MINIMUM AND THE TRIAL POINT IS SUFFICIENTLY
C     SMALLER THAN EITHER.
C     
 140  if (dal.gt.0.d0 .and. 0.d0.lt.at .and.
     >     at.lt.(.99d0*dmin1(ap,alfa))     ) goto 150
C     
C     TEST IF BOTH POINTS ARE LESS THAN THE MINIMUM AND THE TRIAL POINT
C     IS SUFFICIENTLY LARGE.
C     
      if (dal.le.0.d0 .and. at.gt.(1.01d0*dmax1(ap,alfa))) goto 150
C     
C     IF THE TRIAL POINT IS TOO SMALL,DOUBLE THE LARGEST PRIOR POINT.
C     
      if (dal.le.0.d0) at=2.d0*dmax1(ap,alfa)
C     
C     IF THE TRIAL POINT IS TOO LARGE, HALVE THE SMALLEST PRIOR POINT.
C     
      if (dal.gt.0.d0) at=dmin1(ap,alfa)*.5d0
C     
C     SET AP=ALFA, ALFA=AT,AND CONTINUE SEARCH.
C     
 150  ap=alfa
      fp=compObj
      dp=dal
      alfa=at

      goto 80
C     
C     A RELATIVE MAX HAS BEEN PASSED.REDUCE ALFA AND RESTART THE SEARCH.
C     
 160  if(new_search) then
         aright = alfa
         fright = compObj
c     set dpp to have opposite gradient to ensure that next search
c     is bracketted
         dright = -dleft
         alfa = (alfa+aleft)/3.d0
         goto 80
      else

         alfa=alfa/3.d0
         ap=0.d0
         fp=fmin
         dp=dg
         goto 80
      endif

c     END CHANGES MADE BY LASLO

C     THE LINE SEARCH HAS CONVERGED. TEST FOR CONVERGENCE OF THE
c     ALGORITHM.

 170  gsq=0.d0
      xsq=0.d0
      do i=1,ndv
         gsq=gsq+compGrad(i)*compGrad(i)
         xsq=xsq+dvs(i)*dvs(i)
      enddo

c     -- store restart info since line search is converged --
c     -- don't need to store grid since it can be generated from the
c     original grid with regrid --
c     -- write b-spline restart file --

#ifdef _MPI_VERSION
      if (rank.eq.0) then
#endif
         rewind(bspr_unit)
         write (bspr_unit,*) jbsord
         write (bspr_unit,*) nc
         write (bspr_unit,*) jbody
c     -- write airfoil control points --
         do i = 1,nc
            write (bspr_unit, 910) bcp(i,1),bcp(i,2)
         end do

c     -- write parameter vector --
         do i = 1,jbody
            write (bspr_unit, 910) bt(i)
         end do

c     -- write knot vector --
         do i = 1,jbsord+nc
            write (bspr_unit, 910) bknot(i)
         end do

         rewind(dvsr_unit)
         do i = 1,ngdv
            write (dvsr_unit, 920) idv(i), dvs(i)
         end do

c     -- write best airfoil coordinates --
         rewind(n_best)
         write (n_best,890)
         do j = jtail1, jtail2
            write (n_best,896) x(j,1), y(j,1)
         end do
         write (n_best,895)
#ifdef _MPI_VERSION
      end if !(rank.eq.0)
#endif

 890  format ('#*NAME=MAIN')
 895  format ('#*EOR')
 896  format (2e18.8)
 900  format (500e16.6)
 910  format (2e24.16)
 920  format (i5,e24.16)

      if (gsq.le.eps*eps*dmax1(1.d0,xsq)) then

#ifdef _MPI_VERSION
         if(rank.eq.0) then
#endif
            rewind (ac_unit)
            do i = 1,jbody
               write (ac_unit,900) ac_his(i,1), (ac_his(i,j+1),j=1,ifun)
            end do
            write(ghis_unit,500) icg,ifun,compObj,cgradmag
#ifdef _MPI_VERSION
         end if
#endif

         return      

      end if 

      !-- If 'N' weight update iterations have been completed, or a line 
      !-- search stall has occured then update off-design weights and 
      !-- begin a new weight update cycle

 171  if((wt_iter==wt_cycle_iter).or.(ifsd.gt.20).or.(badfs.gt.10))then

         call offDesWts(nflag, ifun, ac_his, cp_his, MaxMach_r,
     &     clmax, alphamax, jdim, kdim, ndim, ifirst, indx, icol, 
     &     iex, q, cp, xy, xyj, x_r, y_r, cp_tar, fmu, vort, turmu, 
     &     work1, ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, 
     &     pat, itfirst, compObj, ifsd, mp, cdt_r, clt_r, badfs)

         if (nflag.eq.-1) return

      end if

C     SEARCH CONTINUES. SET WBFGS(I)=ALFA*WBFGS(I),THE FULL STEP VECTOR.

      do i=1,ndv
         wbfgs(i)=alfa*wbfgs(i)
      enddo

C     COMPUTE THE NEW SEARCH VECTOR. FIRST TEST WHETHER A
C     CONJUGATE GRADIENT OR A VARIABLE METRIC VECTOR IS USED.

      goto 330

C     ROUNDOFF PROBLEMS
 320  nflag=3
      return
C     
C     A VARIABLE METRIC ALGORITM IS BEING USED. CALCULATE Y AND D'Y.
C     
 330  u1=0.d0
      do i=1,ndv
         ngpi=ng+i
         wbfgs(ngpi)=compGrad(i)-wbfgs(ngpi)
         u1=u1+wbfgs(i)*wbfgs(ngpi)
      enddo

     
c     IF RSW=.TRUE.,SET UP THE INITIAL SCALED APPROXIMATE HESSIAN.
C     
      if (.not.rsw) goto 380
C     
C     CALCULATE Y'Y.
C     
      u2=0.d0
      do i=1,ndv
         ngpi=ng+i

c     START CHANGES MADE BY LASLO

c     if inithes = 0  set up with scaling 
C     if inithes = 1 set up without scaling
c     if inithes = 2 set up with diagonals being d(i)/v(i)
         if(inithes.gt.0) then 
            u2=u2+wbfgs(ngpi)*wbfgs(ngpi)
         else
            u2=u2+wbfgs(ngpi)*wbfgs(ngpi)*dv_scal(i)*dv_scal(i)
         endif

c     END CHANGES MADE BY LASLO

      enddo
C     
C     CALCULATE THE INITIAL HESSIAN AS H=(P'Y/Y'Y)*I
C     AND THE INITIAL U2=Y'HY AND WBFGS(NX+I)=HY.
C     


c     START CHANGES MADE BY LASLO


      ij=1
      u3=u1/u2
      u2 = 0.d0
      do i=1,ndv
         nxpi=nx+i
         ngpi=ng+i
         do j=i,ndv
            ncons1=ncons+ij
            wbfgs(ncons1)=0.d0
            if(i.eq.j) then
               if(inithes.eq.0) wbfgs(ncons1)=u3*dv_scal(i)*dv_scal(i)
               if(inithes.eq.1) wbfgs(ncons1)=u3
               if(inithes.eq.2) then
                   wbfgs(ncons1) = wbfgs(i)/wbfgs(ngpi)
                  if(wbfgs(ncons1).lt.1e-8) 
     >                 wbfgs(ncons1) = bstep*abs(dv_scal(i))
               endif
               u2 = u2 + wbfgs(ncons1)*wbfgs(ngpi)*wbfgs(ngpi)
               if(inithes.eq.0) then
                  wbfgs(nxpi)=u3*wbfgs(ngpi)*dv_scal(i)*dv_scal(i)
               else
                  wbfgs(nxpi)=wbfgs(ncons1)*wbfgs(ngpi)
               endif
            endif
            ij=ij+1
         enddo
c     if inithes is true set up initial hessian without scaling
c         if(inithes.eq.0) then
c             wbfgs(nxpi)=u3*wbfgs(ngpi)*dv_scal(i)*dv_scal(i)
c          else 
c             wbfgs(nxpi)=wbfgs(ngpi)*wbfgs(ncons1)
c          endif
      enddo
c      u2=u3*u2

c      write(scr_unit,*) u1, u2

C ----Printing Initial Hessian Matrix-------------------------
 
c     This should be uncommented to leave if hessian, however
c     This will ensure that the BFGS estimate of the Hessian
C     Is alway printed

#ifdef _MPI_VERSION
      if (rank==0) then
#endif
         if(.true.) then
c           if(hessian) then
            write(hest_unit,440) 'hest0= ['
            ij = 1
            do i=1,ndv
               do j=i,ndv
                  ncons1=ncons+ij
                  write(hest_unit,441) i, j, wbfgs(ncons1)
c     &              *dv_scal(i)*dv_scal(j)
                  ij=ij+1
               enddo
            enddo
            write(hest_unit,440) '];'
            call flush(hest_unit)
         end if
#ifdef _MPI_VERSION
      end if
#endif

c     END CHANGES MADE BY LASLO

      goto 430
c     
C     CALCULATE WBFGS(NX+I)=HY AND U2=Y'HY.
C     
 380  u2=0.d0
      do i=1,ndv
         u3=0.d0
         ij=i
         if (i.eq.1) goto 400
         ii=i-1
         do j=1,ii
            ngpj=ng+j
            ncons1=ncons+ij
            u3=u3+wbfgs(ncons1)*wbfgs(ngpj)
            ij=ij+ndv-j
         enddo
 400     do j=i,ndv
            ncons1=ncons+ij
            ngpj=ng+j
            u3=u3+wbfgs(ncons1)*wbfgs(ngpj)
            ij=ij+1
         enddo
         ngpi=ng+i
         u2=u2+u3*wbfgs(ngpi)
         nxpi=nx+i
         wbfgs(nxpi)=u3
      enddo
c     
C     CALCULATE THE UPDATED APPROXIMATE HESSIAN.
C     
 430  u4=1.d0+u2/u1
      do i=1,ndv
         nxpi=nx+i
         ngpi=ng+i
         wbfgs(ngpi)=u4*wbfgs(i)-wbfgs(nxpi)
      enddo
      ij=1
      do i=1,ndv
         nxpi=nx+i
         u3=wbfgs(i)/u1
         u4=wbfgs(nxpi)/u1
         do j=i,ndv
            ncons1=ncons+ij
            ngpj=ng+j
            wbfgs(ncons1) = wbfgs(ncons1)+u3*wbfgs(ngpj)-u4*wbfgs(j)
            ij=ij+1
         enddo
      enddo
C     
C     CALCULATE THE NEW SEARCH DIRECTION WBFGS(I)=-HG AND ITS DERIVATIVE
C     
      dg1=0.d0
      do i=1,ndv
         u3=0.d0
         ij=i
         if (i.eq.1) goto 470
         ii=i-1
         do j=1,ii
            ncons1=ncons+ij
            u3=u3-wbfgs(ncons1)*compGrad(j)
            IJ=IJ+ndv-J
         enddo         
 470     do j=i,ndv
            ncons1=ncons+ij
            u3=u3-wbfgs(ncons1)*compGrad(j)
            ij=ij+1
         enddo
         dg1=dg1+u3*compGrad(i)
         wbfgs(i)=u3
      enddo


C     START CHANGES MADE BY LASLO

c     This resets the hessian to be only diagonal values
c     When the product abs(g's) < 1e-8
c

      if(reset_hessian.and.abs(dg1).lt.1e-8) then
         ij = 1
         dg1 = 0.d0
         write(scr_unit,*) 'Resetting Hessian'
         do i=1,ndv
            do j=i,ndv
               ncons1=ncons+ij
               if(i.ne.j) then
c     set off diagonal value to zero
                  wbfgs(ncons1) = 0.d0
               else
c     set step = -Hii*gi
                  wbfgs(i) = -wbfgs(ncons1)*compGrad(i)
c     dg1 = step'*grad
                  dg1 = dg1 + wbfgs(i)*compGrad(i)
               endif
               ij=ij+1
            enddo
         enddo
      endif

c     END Changes Made by Laslo


C     START Changes made by Laslo
c
c
c
c
C ------------Printing Hessian Matrix-------------------------

      

c     This should be uncommented to leave if hessian, however
c     This will ensure that the BFGS estimate of the Hessian
C     Is alway printed

#ifdef _MPI_VERSION     
      if(rank==0) then
#endif
         if(.true.) then
c         if(hessian) then
            write(hest_unit,439) 'hest', iter, '= ['
c            write(hest_unit,440) 'i    j'
            ij = 1
            do i=1,ndv
               do j=i,ndv
                  ncons1=ncons+ij
                  write(hest_unit,441) i, j, wbfgs(ncons1)
c     &              *dv_scal(i)*dv_scal(j)
                  ij=ij+1
               enddo
            enddo
            write(hest_unit,440) ']'
            call flush(hest_unit)
         end if
#ifdef _MPI_VERSION
      end if
#endif
 
 439  format(A4,i2,A)
 440  format(A)
 441  format(2i4, 50e24.12)
 445  format(A3,i2,A)

C     END CHANGES MADE BY LASLO

C     TEST FOR A DOWNHILL DIRECTION.

C     START CHANGES MADE BY LASLO

c     This saves the converged solution from the previously 
c     accepted step to warm start always with previously 
c     accepted step solution

c     -- save converged solution --

      if(warmset.eq.1) then
         do mp=1,mpopt
            do n=1,ndim
               do k=1,kdim
                  do j=1,jdim
                     qwarm(1,j,k,n,mp)=qtmp(j,k,n,mp)
                  end do
               end do
            end do
         enddo
      endif

C     END CHANGES MADE BY LASLO

      if (dg1.gt.0.) goto 320
      rsw=.false.
      goto 40

 500  format(4x,i5,3x,i6,2x,d15.8,2x,d15.8)

      end                       !BFGS

