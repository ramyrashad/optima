c----------------------------------------------------------------------
c     -- fill jacobian matrix and preconditioner: s-a terms --
c     written by: marian nemec
c     date: jan. 2001
c----------------------------------------------------------------------

      subroutine fillast (jdim, kdim, ndim, indx, icol, as, pa, bm2,
     &      bm1, db, bp1, bp2)
      
#include "../include/arcom.inc"

      integer jdim, kdim, ndim, indx(jdim,kdim),icol(9),jsta,jsto
clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
clb
      double precision bm2(5,jdim,kdim), bm1(5,jdim,kdim)
      double precision db(5,jdim,kdim), bp1(5,jdim,kdim)
      double precision bp2(5,jdim,kdim)

c     -- preconditioner --
      if ( prec_mat ) then 
        n  = 5 
        do k = klow,kup
          do j = jlow,jup
            ij = ( indx(j,k) - 1 )*ndim + n
            ii = ( ij - 1 )*icol(5)
            do m = 1, ndim
              pa(ii+        m) = pa(ii+        m) + bm2(m,j,k)
              pa(ii+icol(1)+m) = pa(ii+icol(1)+m) + bm1(m,j,k)
              pa(ii+icol(2)+m) = pa(ii+icol(2)+m) + db(m,j,k)
              pa(ii+icol(3)+m) = pa(ii+icol(3)+m) + bp1(m,j,k)
              pa(ii+icol(4)+m) = pa(ii+icol(4)+m) + bp2(m,j,k)
            end do
          end do
        end do
      end if

c     -- second order jacobian --
      if ( jac_mat ) then
        n  = 5

        if(.not.periodic) then
           jsta=jlow+1
           jsto=jup-1
        else
           jsta=jlow
           jsto=jup
        end if

c     -- interior points --
        do k = klow+1,kup-1
          do j = jsta,jsto
            ij = ( indx(j,k) - 1 )*ndim + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m = 1,ndim
              as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
              as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,j,k)
              as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,j,k)  
              as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,j,k)
              as(ii+icol(7)+m) = as(ii+icol(7)+m) + bp2(m,j,k)
            end do
          end do
        end do

c     -- boundary nodes --

        if(.not.periodic) then

        j = jlow
        do k = klow+1,kup-1
          ij = ( indx(j,k) - 1 )*ndim + n
          ii = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m = 1,ndim
            as(ii+        m) = as(ii+        m) + bm2(m,j,k)
            as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,j,k)  
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,j,k)
            as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
          end do
        end do

        j = jup
        do k = klow+1,kup-1
          ij = ( indx(j,k) - 1 )*ndim + n
          ii = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m = 1,ndim
            as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,j,k)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,j,k)  
            as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,j,k)
            as(ii+icol(7)+m) = as(ii+icol(7)+m) + bp2(m,j,k)
          end do
        end do

        end if  !not periodic

        k = klow
        do j = jlow+1,jup-1
          ij = ( indx(j,k) - 1 )*ndim + n
          ii = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m = 1,ndim
            as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
            as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,j,k)  
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,j,k)
            as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
          end do
        end do

        k = kup
        do j = jlow+1,jup-1
          ij = ( indx(j,k) - 1 )*ndim + n
          ii = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m = 1,ndim
            as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,j,k)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,j,k)  
            as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,j,k)
            as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
          end do
        end do

        if(.not.periodic) then

        j = jlow
        k = klow
        ij = ( indx(j,k) - 1 )*ndim + n
        ii = ( ij - 1 )*icol(9)
        if (clopt) ii = ( ij - 1 )*(icol(9)+1)
        do m = 1,ndim
          as(ii+        m) = as(ii+        m) + bm2(m,j,k)
          as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,j,k)
          as(ii+icol(2)+m) = as(ii+icol(2)+m) + db(m,j,k)  
          as(ii+icol(3)+m) = as(ii+icol(3)+m) + bp1(m,j,k)
          as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp2(m,j,k)
        end do

        j = jlow
        k = kup
        ij = ( indx(j,k) - 1 )*ndim + n
        ii = ( ij - 1 )*icol(9)
        if (clopt) ii = ( ij - 1 )*(icol(9)+1)
        do m = 1,ndim
          as(ii+        m) = as(ii+        m) + bm2(m,j,k)
          as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,j,k)
          as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,j,k)  
          as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,j,k)
          as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp2(m,j,k)
        end do

        j = jup
        k = klow
        ij = ( indx(j,k) - 1 )*ndim + n
        ii = ( ij - 1 )*icol(9)
        if (clopt) ii = ( ij - 1 )*(icol(9)+1)
        do m = 1,ndim
          as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
          as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,j,k)
          as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,j,k)  
          as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,j,k)
          as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
        end do

        j = jup
        k = kup
        ij = ( indx(j,k) - 1 )*ndim + n
        ii = ( ij - 1 )*icol(9)
        if (clopt) ii = ( ij - 1 )*(icol(9)+1)
        do m = 1,ndim
          as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
          as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,j,k)
          as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,j,k)  
          as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,j,k)
          as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
        end do

      end if  !not periodic

      end if  !jac_mat 
      
      return                                                            
      end                       !fillast
