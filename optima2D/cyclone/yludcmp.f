c ********************************************************************
c ** LU Factorization --- for pentadiagonal systems                 **
c ** y sweep routine                                                **
c ** Crout's method of factorization applied to pentadiagonals      **
c ********************************************************************
      subroutine yludcmp(dm2,dm1,dd,dp1,dp2,jmax,kmax,
     &                   jlow,jup,klow,kup)
c ********************************************************************
c Variables:   dm2: vector 2 below diagonal
c              dm1: vector 1 below diagonal    
c              dd : vector of diagonal entries
c              dp1: vector 1 above diagonal
c              dp2: vector 2 above diagonal
c ********************************************************************
      dimension dm2(jmax,kmax), dm1(jmax,kmax), dd(jmax,kmax)
      dimension dp1(jmax,kmax), dp2(jmax,kmax) 
      do 199 j=jlow,jup  
c klow
         k = klow
         dm1(j,k+1) = dm1(j,k+1)/dd(j,k)
         dm2(j,k+2) = dm2(j,k+2)/dd(j,k)
c klow+1        
         k = klow + 1
         dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j,k-1)
         dm1(j,k+1) = (dm1(j,k+1)-dm2(j,k+1)*dp1(j,k-1))/dd(j,k)
         dm2(j,k+2) = dm2(j,k+2)/dd(j,k)
c ** Main Loop *******************************************************
         do 120 k = klow+2,kup-2
c                                                        /* beta's  */
           dp1(j,k-1) = dp1(j,k-1) - dm1(j,k-1)*dp2(j,k-2)
           dd(j,k) = dd(j,k)-dm2(j,k)*dp2(j,k-2)-dm1(j,k)*dp1(j,k-1)
c                                                        /* alpha's */
           dm1(j,k+1) = (dm1(j,k+1)-dm2(j,k+1)*dp1(j,k-1))/dd(j,k)
           dm2(j,k+2) = dm2(j,k+2)/dd(j,k)
  120    continue
c ** Main Loop *******************************************************
c kup-1
         k = kup - 1
         dp1(j,k-1) = dp1(j,k-1) - dm1(j,k-1)*dp2(j,k-2)
         dd(j,k) = dd(j,k)-dm2(j,k)*dp2(j,k-2)-dm1(j,k)*dp1(j,k-1)
         dm1(j,k+1) = (dm1(j,k+1)-dm2(j,k+1)*dp1(j,k-1))/dd(j,k)
c kup
         k = kup 
         dp1(j,k-1) = dp1(j,k-1) - dm1(j,k-1)*dp2(j,k-2)
         dd(j,k) = dd(j,k)-dm2(j,k)*dp2(j,k-2)-dm1(j,k)*dp1(j,k-1)
  199 continue
      return
      end
