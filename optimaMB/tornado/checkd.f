c     ------------------------------------------------------------------
c     -- check the diagonal of pa for zeroes --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine checkd( ib, jmax, kmax, nmax, indx, ipa, jpa, pa, js,
     &     je, ks, ke)

      implicit none
      integer ib, jmax, kmax, nmax, js, je, ks, ke, j, k, n, m
      integer ir,ic1,ic2,idiag
      integer indx(jmax,kmax), ipa(*), jpa(*)
      double precision pa(*)

      do n=1,nmax
         do k=ks,ke
            do j=js,je
            
               ir = ( indx(j,k) - 1 )*nmax + n
               ic1 = ipa(ir)
               ic2 = ipa(ir+1)-1
               idiag = 0
               do m = ic1,ic2
                  if ( jpa(m).eq.ir ) then
                     idiag = m
                  end if
               end do

               if (idiag.ne.0) then

c                  print *, ib,j,k,n,pa(idiag)
                  if ( abs(pa(idiag)) .lt. 1.e-10 ) then
                     write (*,*) 'zero on diagonal'
                     write (*,100) ib,j,k,n,ir,pa(ir)
                  end if
               else
                  write (*,*) 'idiag=0??'
                  write (*,*) ib,j,k,n,ir
               end if

            end do
         end do
      end do

 100  format(5i5,e12.4)

      return 
      end                       !checkd
