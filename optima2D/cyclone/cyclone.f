      subroutine cyclone ( jdimen, kdimen, ifirst, q, press, xy, xyj, x,
     &      y, turmu, fmu, vort, qp, qold, sndsp, s, xit, ett, ds, 
     &      spectx, specty, uu, vv, ccx, ccy, coef4x, coef4y, coef2x,
     &      coef2y, lux, luy, precon, gam,D_hat,exprhs,a_jk)

c     An implicit two-dimensional Navier-Stokes code. 

c     CYCLONE is based on ARC2D.
c     Originally written by J. L. Steger (1976)                  
c     modifications etc., by                           
c     Barton and Pulliam (1982)                     
c     vectorization Pulliam (1983)               
c     Pulliam and Barth (1984)           
c     Pulliam (10/84)
c     more mods by A. Pueyo (1994)
c     S. De Rango and D.W. Zingg (1995-1996)
c     CUSP and other mods added by M. Nemec (1998)

c     Code is dimensioned for a maximum of maxj points in j             
c     and any combination of jmax*kmax which does not exceed maxj*maxk:
c     a. That is you can run say 83 x 147 or whatever              
c     b. To change dimensions you only have to change parameter    
c     statements  maxj=maxj and maxjk=maxj*maxk in parm.inc
c     c. Do not exceed 1999 for maxj                               
c     d. You can reduce the memory requirements (so that larger  
c     grid can be used) by setting maxpen = 1. Note this disables
c     method 2 (block penta-inversion) since the array sizes for
c     a,b,c,e of common block 'scratch'
c     are not compatible with that option.
c     Setting maxpen = maxj*maxk reenables meth=2.

      implicit none
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdimen,kdimen,isteps,junit,j,ii,jj,ifirst,ip,nscal
      integer total_subit
      double precision totime2
      double precision qp(jdimen,kdimen,4),q(jdimen,kdimen,4)
      double precision qold(jdimen,kdimen,4)
      double precision press(jdimen,kdimen),sndsp(jdimen,kdimen)
      double precision s(jdimen,kdimen,4),xy(jdimen,kdimen,4)
      double precision xyj(jdimen,kdimen)
      double precision xit(jdimen,kdimen),ett(jdimen,kdimen)
      double precision x(jdimen,kdimen),y(jdimen,kdimen)
      double precision turmu(jdimen,kdimen),ds(jdimen,kdimen)
      double precision fmu(jdimen,kdimen),vort(jdimen,kdimen)
      double precision spectx(jdimen,kdimen,3),specty(jdimen,kdimen,3)
      double precision uu(jdimen,kdimen),vv(jdimen,kdimen)
      double precision ccx(jdimen,kdimen),ccy(jdimen,kdimen)
      double precision coef4x(jdimen,kdimen),coef4y(jdimen,kdimen)
      double precision coef2x(jdimen,kdimen),coef2y(jdimen,kdimen)
      double precision lux(jdimen*kdimen,4*5),luy(jdimen*kdimen,4*5)

      double precision precon(jdimen*kdimen,6),gam(jdimen*kdimen,16)

      common/scratch/ a,b,c,d,e,work1
      common/newwork/ dj,ddj

      double precision a(maxpen,4),b(maxpen,16),c(maxpen,16)
      double precision d(maxjk,16),e(maxpen,4),work1(maxjk,10)
      double precision dj(maxpen,3,4),ddj(maxpen,3,4)

      double precision cp(maxj),xn(maxj),yn(maxj),sn(maxj),cp2(maxj)
      double precision x2(maxj),y2(maxj),work2(maxj,30)
      common/wksp/cp,xn,yn,sn,cp2,x2,y2
      common/worksp/work2                         

      double precision dtiseq(20),dtmins(20),dtow2(20)
      integer jmxi(20),kmxi(20),jskipi(20),iends(20)
      integer isequal,iseqlev
      common/mesup/ dtiseq,dtmins,dtow2,isequal,iseqlev,
     &      jmxi,kmxi,jskipi,iends

      logical doit,bailout

csi
      double precision D_hat(jdimen,kdimen,4,6)
      double precision exprhs(jdimen,kdimen,4)
      double precision a_jk(6,6)
csi

c     -- timing variables --
      real*4 time1, time2, t, tarray(2),etime

c     initialization
c     ==============

c     -- addition by dave jones for time-expiry code --
#ifdef TIMEBOMB
      idate = 15
      imnth = 04
      iyear = 1998
      call kaboom (idate, imnth, iyear)
#endif

c     -- parameter initializations --
      istart = 0
      isteps = 0
      junit = 0

c     -- initialize variables --
      call initia ( ifirst,jdimen,kdimen,q,qold,press,sndsp,s,xy,xyj,
     &              xit,ett,ds,x,y,turmu,fmu,vort,precon ) 

c     -- negative jacobian detected --
      if (badjac) return

      istart = istart+1

c     -- calculate grid aspect ratios --
      call asprat ( jdimen,kdimen,x,y )

c     -- initialize time --
      totime1 = 0.0
      totime2 = 0.0

c     -- if grid output on unit 11 desired because internally           
c        generated grid is used  call ioall( 16) --

c     -- input grid sequence and time step parameters --
c      ip = 9                                                            
c      call ioall ( ip,junit,jdimen,kdimen,q,qold,press,sndsp,turmu,
c     &      fmu,vort,xy,xyj,x,y )

      if(isequal.ne.0)then                                              

c                          +-----------------------+            
c                          |+---------------------+|
c                          ||   mesh sequencing   ||
c                          ||   ===============   ||

        bailout=.false.
        do 100 ii = 1,isequal
c     -- option to sequence through coarse grids for initial good
c        guess also used to sequence through a set of time steps --

          iseqlev=ii

c     -- restart is set to false here if grdseq_rest = true --
c     -- if ii=1 then restart is set to true below for reasons 
c        explained there.  for ii & 1, we need to reset restart to 
c        false so that the Spalart-Allmaras model appropriately resets 
c        turre to 0 between transition points for the finer grid --
          if (grdseq_rest) restart = .false.

          istart = istart + isteps
          call meshup ( ii,jdimen,kdimen,q,qold,s,xy,xyj,xit,ett,x,y,ds,
     &               work1,work1(1,2),press,sndsp,turmu,fmu,vort,precon,
     &               work1(1,3) )

c     -- note: jdimen & kdimen remain same as on fine grid --
c     -- all other variables like jmax, kmax, jbegin, jend etc have
c        been changed appropriately for grid-sequencing --

c     -- grid-sequencing restart from a coarse grid --
c     -- restart must be set to true so that bcbody_ho does not ramp
c        in viscous bc effect and mutur does not change turre --

          if (grdseq_rest .and. ii.eq.1) then
            ip = 23
            restart=.true.
            call ioall ( ip,0,jdimen,kdimen,q,qold,press,sndsp,turmu,
     &            fmu,vort,xy,xyj,x,y )           
          endif
          
c     -- initialize pressure and sound speed --
          call calcps(jdimen,kdimen,q,press,sndsp,precon,xy,xyj)

c     -- set number of iterations each grid, reset dt etc --
          isteps   = iends(ii)                                         
          dt       = dtiseq(ii)                                        
          dtmin    = dtmins(ii)                                        
          dtow     = dtow2(ii)
          iend     = istart + isteps -1    

c     -- initialize variable dt --
          call varidt ( jacdt,jdimen,kdimen,q,sndsp,xy,xyj,ds,precon )

c     -- initialize forces --
          nscal = 0
          doit=.true.
          if (integ.eq.2) then
            call clcd ( jdimen,kdimen,q,press,x,y,xy,xyj,nscal,cli,cdi,
     &            cmi,clv,cdv,cmv )                     
          else
            call clcdho ( jdimen,kdimen,q,press,x,y,xy,xyj,nscal,cli,
     &            cdi,cmi,clv,cdv,cmv,work2,work2(1,2),doit )
            doit=.false.
          endif
          clt = cli + clv
          cdt = cdi + cdv
          cmt = cmi + cmv              

c                         +--------------------+
c                         |  outer iterations  |
c                         |  ================  |

      total_subit=0
      call preproc(jdimen,kdimen,s,qp,q,qold,a_jk)
c
c
csi   set number of stages for esdirk methods
      jstagemx=1
      if(jesdirk.eq.4)then
         jstagemx=6
      elseif(jesdirk.eq.3)then
         jstagemx=4
      endif

csi   start of physical time loop
      do 10 numiter = istart,iend

        t=etime(tarray) 
        time1=tarray(1)
        time2=tarray(2)

c     : Calculate Omega=omegaa*sin(2*pi*omegaf*t) or Omega=omegaa

       if (abs(omegaf) .le. fd_eta) then
         omega=omegaa
       else
         omega=omegaa*dsin(2.d0*pi*omegaf*(numiter-istart+1)*dtiseq(1))
       endif

c     : these iterations are done within the loop of mesh-sequence
c
      if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5) then
         write(subit_unit,*)' '
         write(subit_unit,*)'numiter=',numiter
         jsubit=0
      endif
c
c
c---- advance one time step ----

csi   one time step involves solution of jstagemx steady state problems per time step
      do 71 jstage=1,jstagemx

         ! ________
         ! UNSTEADY
         if (unsted) then

c        call calcps(jdimen,kdimen,q,press,sndsp,precon,xy,xyj)
         jsubit=0
         resid=1.d0
csi      if esdirk you need extra explicit terms on RHS D(Q)
         if(jesdirk.eq.4.or.jesdirk.eq.3)
     &        call calc_exprhs(jdimen,kdimen,D_hat,exprhs,a_jk)
csi      loop in pseudo time until steady state reached
csi      the solution to the steady state problem gives Q_k
         do 61 while ((resid.gt.simin_res.and.jsubit.lt.(jsubmx+1))
     &        .or.(jsubit.lt.1))
            if(.not.cmesh)then 
               call integrat(jdimen,kdimen,qp,q,qold,s,
     &              press,D_hat,exprhs,a_jk(jstage,jstage),
     &              sndsp,turmu,fmu,vort,x,y,xy,xyj,xit,ett,ds,spectx,
     &              specty,uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &              lux,luy,precon,gam)
            elseif(cmesh)then
               call cinteg(jdimen,kdimen,scaledq,qp,q,qold,s,press,
     &              sndsp,turmu,fmu,vort,x,y,xy,xyj,xit,ett,ds,
     &              spectx,specty,uu,vv,ccx,ccy,coef2x,coef2y,coef4x,
     &              coef4y,lux,luy,precon,gam)
            endif

           if (jstage.eq.1 .and. (jesdirk.eq.4 .or. jesdirk.eq.3)) then
              goto 71
           endif
 61     continue

        print *, numiter,jsubit,resid

        if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5) then
           total_subit=total_subit+jsubit-1
           if (thisout) write(turbhis_unit,*)'jstage=',jstage,' subit=',
     &                                       jsubit-1
           if (thisout) write(turbhis_unit,*) resid
        endif
        

      else

      ! ______
      ! STEADY

      do j=0,jsubmx
        if(.not.cmesh)then ! careful: for some reason cmesh is false even for a cmesh...
           call integrat(jdimen,kdimen,qp,q,qold,s,
     &              press,D_hat,exprhs,a_jk(jstage,jstage),
     &              sndsp,turmu,fmu,vort,x,y,xy,xyj,xit,ett,ds,spectx,
     &              specty,uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &              lux,luy,precon,gam)

c     -- check abnormal or nan resid --
          if ( abs( resid - 1000.0) .lt. 0.01 ) return

        elseif(cmesh)then ! careful: for some reason cmesh is false even for a cmesh...
          call cinteg ( jdimen,kdimen,qp,q,qold,s,press,sndsp,
     &          turmu,fmu,vort,x,y,xy,xyj,xit,ett,ds,spectx,specty,
     &          uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &          lux,luy,precon,gam )
        endif
c     
        if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5) then
          write(subit_unit,*)'subit=',jsubit
          write(subit_unit,*) resid
          jsubit=jsubit+1
        endif
      end do ! j=0,jsubmx
      
      endif ! if (unsted) then

csi   if done all stages, then update q and qold, this means q=qp    
      if (jstage.eq.jstagemx)then
        if (unsted .or. numiter.eq.iend)then 
           call postproc(jdimen,kdimen,qp,q,qold)
        endif
      endif
     
c     
c     **********************************************************************
c                                                                       
c     
      if (jstage.eq.jstagemx)then
c---- compute forces ----
         nscal = 0                                                         
         if (ii.eq.isequal .and. numiter.eq.iend) then
            if (iord.eq.4 .and. integ.eq.2) then
               integ=4
               write (out_unit,*)
     &              'Integ. of forces on last iter. ',
     &              'performed by clcdho.f'
            endif
         endif
c         if (integ.eq.2 .or. periodic) then
         if (integ.eq.2) then
            call clcd (jdimen,kdimen,qp,press,x,y,xy,xyj,nscal, 
     &           cli, cdi,cmi, clv, cdv, cmv)                     
         else
            call clcdho (jdimen,kdimen,qp,press,x,y,xy,xyj,nscal,
     &           cli,cdi,cmi,clv,cdv,cmv,work2,work2(1,2),doit)
         endif
         clt = cli + clv                                                   
         cdt = cdi + cdv                                                   
         cmt = cmi + cmv                  
         
c     -- timing results (no I/O) --
c     -- totime1: user time --
c     -- totime2: user + system time --
            t=etime(tarray)
            time1=tarray(1)-time1
            time2=tarray(2)-time2
            totime1=totime1 + time1
            totime2=totime2 + time1 + time2
 
            if (timing) then
              write(time_unit,*) numiter,totime1,totime2
              if (numiter.eq.iend) write (time_unit,*) 'average=',
     &           totime1/dble(numiter)
            call flush(time_unit)
         endif
c     
c     *******************    output    *******************
c     
c     -- output to .his file: resid, max resid, cl, cd... --
         ip = 19                                                           
         call ioall(ip,junit,jdimen,kdimen,qp,qold,press,sndsp,
     &        turmu,fmu,vort,xy,xyj,x,y)                                       
         if (hisout) call flush (his_unit)

         ! output flow residual to GNUplot data file
         if(gnuplot_res) then
            write(gnuplot_res_unit,*) numiter, resid, '   AF'
         end if
c     
c     -- header for tape6 output and output cfl number --
         if (mod(numiter,500). eq. 0  .or.                              
     &        numiter-istart+1 .eq. 1 ) then                   
            call eigcfl (jdimen,kdimen,qp,sndsp,xy,ds,work1,precon)
         endif                                                             
c     
c---- compute convergence rate ----
         if (.not.unsted .and. flowAnalysisOnly)
     &        call ratecalc(istart,numiter,resid)                         
c     
c     
         ! _______________________________________________________
         ! Transition Prediction: early termination of flow solver
         ! to predict and update transition points
         if ( freeTransition .and. earlyTP .and. .not. finalTP ) then
         if ( .not.unsted.and.
     &        resid.lt.earlyResidTP.and.
     &        ii.eq.isequal ) then
            if (numiter-istart+1 .lt. 200) goto 10
            write(out_unit,*)'****************************************'
            write(out_unit,*)'* Transition Prediction (TP): '
            write(out_unit,*)'* early termination for TP update'
            write(out_unit,*)'* resid = ', resid, ' earlyResidTP =',
     &           earlyResidTP
            write(out_unit,*)'****************************************'
            call postproc(jdimen,kdimen,qp,q,qold)
            bailout=.true.
            goto 101
         endif
         endif
c---- convergence criteria : resid < min_res
         if (.not.unsted.and.resid.lt.af_minr.and.ii.eq.isequal) then
            if (numiter-istart+1 .lt. 200) goto 10 ! RR: changed 20 to 200
            write(out_unit,*)'****************************************'
            write(out_unit,*)'* residual below min. resid., bail out *'
            write(out_unit,*)'****************************************'
            call postproc(jdimen,kdimen,qp,q,qold)
            bailout=.true.
            goto 101
         endif
c     
c---- save restart and flux budget every nq iterations ----
         if (mod(numiter-istart+1,nq) .eq. 0) then
            ip = 10                                       
            call ioall(ip,junit,jdimen,kdimen,qp,qold,press,sndsp,
     &           turmu,fmu,vort,xy,xyj,x,y)
c     -- store flux budget data --
            if (flbud) then
               ip = 24
               call ioall(ip,junit,jdimen,kdimen,qp,qold,press,sndsp,
     &              turmu,fmu,vort,xy,xyj,x,y)
            endif
c     -- store force and moment coefficients --
            ip = 11
            junit=ii
            call ioall(ip,junit,jdimen,kdimen,qp,qold,press,
     &           sndsp,turmu,fmu,vort,xy,xyj,x,y)
            do jj=1,5
               if (ldout) backspace (ld_unit)
            enddo
         endif
c---- save cp file every ncp iterations ----
         if (mod(numiter-istart+1,ncp) .eq. 0) then
c     -- store ascii cp data to tape 20
            ip = 6                                          
            call ioall(ip,junit,jdimen,kdimen,qp,qold,press,sndsp,
     &           turmu,fmu,vort,xy,xyj,x,y)
c     -- store ascii cf data to tape 17
            if (sknfrc) then
               ip = 26
               call ioall(ip,junit,jdimen,kdimen,qp,qold,press,
     &              sndsp,turmu,fmu,vort,xy,xyj,x,y)
            endif
         endif
      endif                     ! if (jstage.eq.jstagemx)then

 71   continue                  ! do 71 jstage=1,jstagemx
                                                    
 10   continue                  ! do 10 numiter = istart,iend

      if (thisout) write(turbhis_unit,*)'total_subit = ',total_subit
      
c                  | end loop on outer iterations |
c                  +------------------------------+
c                                                                       
      if (.not. unsted) numiter=numiter-1
c
c
       if (sv_grdseq .and. (ii.eq.isequal-2 .or. ii.eq.isequal-1)) then
          if (ii.eq.isequal-2) junit = qc_unit
          if (ii.eq.isequal-1) junit = qb_unit
          ip=22
          call ioall(ip,junit,jdimen,kdimen,qp,qold,press,sndsp,turmu,  
     &                  fmu,vort,xy,xyj,x,y)
       endif

 100  continue ! do 100 ii = 1,isequal

c                ||   end loop on mesh sequences   ||
c                |+--------------------------------+|
c                +----------------------------------+
c
 101  continue ! goto 101  (called when resid.lt.af_minr)

c     to find the 'if' for this 'else' ... goto begining of file and 
c     search for 'if(isequal.ne.0)then'
      else 
c
c---- no iterations of flow solver performed                              
        write (out_unit,*)
     &        '????????????????????????????????????????????????????'   
        write (out_unit,*)
     &        '? isequal = 0 so that no iterations of flow solver ?'   
        write (out_unit,*)
     &        '? will be performed for this pass                  ?'   
        write (out_unit,*)
     &        '????????????????????????????????????????????????????'   

      endif ! if(isequal.ne.0)then                                                            

c     -- update pressure and sound speed --
      call calcps (jdimen,kdimen,qp,press,sndsp,precon,xy,xyj) 

cc     -- frozen dissipation is used to verify accuracy of gradients --
c      if ( coef_frz .and. .not. frozen) then
c
c        ix = 1                                                  
c        iy = 2                                                  
c        call eigval(ix,iy,jdimen,kdimen,q,press,sndsp,xyj,xy,xit,uu,ccx)
c
c        ix = 3                                                  
c        iy = 4                                                  
c        call eigval(ix,iy,jdimen,kdimen,q,press,sndsp,xyj,xy,ett,vv,ccy)
c        
c        goto (300, 400, 500) ifrz_what
c
c 300    continue
c
c        write (*,310)
c 310    format (3x,'CYCLONE: Frozen turbulent, viscous and', /3x,
c     &        'dissipation coefficients.')
c
c        call fmun(jdimen, kdimen, q, press, fmu, sndsp)        
c        do k = 1,kend
c          do j = 1,jend
c            fmuf(j,k) = fmu(j,k)
c          end do
c        end do
c
c        if (itmodel.eq.1) then
c          call vortdxi(jdimen,kdimen,q,vort,xy) 
c          call vortdeta (jdimen,kdimen,q,vort,xy,work1)       
c          call blomax (jdimen,kdimen,q,press,vort,turmu,fmu,xy,xyj,x,y) 
c        else
c          call spalart(jdimen,kdimen,q,press,turmu,fmu,x,y,xy,
c     &               xyj,uu,vv,work1,work1(1,2),work1(1,3),work1(1,4),
c     &               work1(1,5),work1(1,6),work1(1,7),work1(1,8),
c     &               work1(1,9),work1(1,10),d(1,11),d,d(1,2),d(1,3),
c     &               d(1,4),d(1,5),d(1,6),d(1,7),d(1,8),d(1,9),d(1,10),
c     &               d(1,14),d(1,15),d(1,16))
c        end if
c
c        do k = 1,kend
c          do j = 1,jend
c            turmuf(j,k) = turmu(j,k)
c          end do
c        end do
c
c        goto 520
c
c 400    continue
c
c        write (*,410)
c 410    format (3x,
c     &        'CYCLONE: Frozen viscous and dissipation coefficients.')
c     &        
c        call fmun(jdimen, kdimen, q, press, fmu, sndsp)        
c        do k = 1,kend
c          do j = 1,jend
c            fmuf(j,k) = fmu(j,k)
c          end do
c        end do
c
c 500    continue
c
c        write (*,510)
c 510    format (3x,
c     &        'CYCLONE: Frozen dissipation coefficients.')
c
c 520    continue
c        ix = 1                                                  
c        iy = 2                                                  
c        ixy = 1                                                 
c        call spectx (jdimen,kdimen,uu,ccx,xyj,xy,coef4x,precon)
c        call gradcoef(ixy,jdimen,kdimen,press,xyj,coef2x,work1)     
c        call coef24x(jdimen,kdimen,coef2x,coef4x,
c     &        work1,work1(1,2),work1(1,5),work1(1,6)) 
c
c        ix = 3                                                  
c        iy = 4                                                  
c        ixy = 2                                                 
c        call specty(jdimen,kdimen,vv,ccy,xyj,xy,coef4y,precon)
c        call gradcoef(ixy,jdimen,kdimen,press,xyj,coef2y,work1)     
c        call coef24y(jdimen,kdimen,coef2y,coef4y,work1,work1(1,2),
c     &        work1(1,5),work1(1,6))
c
c        do k = 1,kend
c          do j = 1,jend
c            c2xf(j,k) = coef2x(j,k)
c            c4xf(j,k) = coef4x(j,k)
c            c2yf(j,k) = coef2y(j,k)
c            c4yf(j,k) = coef4y(j,k) 
c          end do
c        end do
c
c      end if


      return                                   
      end                       !cyclone
