                a1=xy(j,t,3)
                a2=xy(j,t,4)
                cs=sndsp(j,t)**2
c         
c               -for fourth difference
                dot1=phi(j,t)*work(1,j,k,2)
     &                -u(j,t)*work(2,j,k,2)
     &                -v(j,t)*work(3,j,k,2)
     &                      + work(4,j,k,2)
                dot2=-vv(j,t)*work(1,j,k,2)
     &                   + a1*work(2,j,k,2)
     &                   + a2*work(3,j,k,2)
c         
                fact1=(s1(j,t)*dot1*(gami/cs)+s2(j,t)*dot2)
                fact2=((s1(j,t)*dot2)/(a1**2+a2**2)+s2(j,t)*gami*dot1)
c
                temp2(1,j,k)=spect(j,t,1)*work(1,j,k,2)
     &                      +fact1                     
                temp2(2,j,k)=spect(j,t,1)*work(2,j,k,2)
     &                      +fact1*u(j,t)                 
     &                      +fact2*a1                  
                temp2(3,j,k)=spect(j,t,1)*work(3,j,k,2)
     &                      +fact1*v(j,t)                 
     &                      +fact2*a2                  
                temp2(4,j,k)=spect(j,t,1)*work(4,j,k,2)
     &                      +fact1*h(j,t)
     &                      +fact2*vv(j,t)
