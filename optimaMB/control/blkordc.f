c     ------------------------------------------------------------------
c     -- c-mesh ordering from optima2D -- 
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine blkordc (js, ks, ke, jmax, kmax, indx, lastblk)

      implicit none
      integer js, ks, ke, jmax, kmax, lastblk
      integer neven,ii,i,j,k,jf
      integer indx(jmax,kmax)

      neven = mod( jmax,2 )

      if (neven.eq.0) then
         ii = jmax/2
      else
         ii = (jmax-1)/2
      end if
      
      i = lastblk
      do j = js,ii
         do k = ke,ks,-1
            i = i+1
            indx(j,k) = i
c            write (*,*) j,k,indx(j,k)
         end do
         jf = jmax-j+1
         do k = ks,ke
            i = i+1
            indx(jf,k) = i
c            write (*,*) j,k,jf,indx(jf,k)
         end do
      end do

      if (neven.ne.0) then
         j = ii+1
         do k = ks,ke
            i = i+1
            indx(j,k) = i
c            write (*,*) j,k,indx(j,k)
         end do
      end if

c      write (*,*) js,je,ks,ke,jmax,kmax

      return
      end                       !blkordc
