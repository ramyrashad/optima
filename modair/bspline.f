c----------------------------------------------------------------------
c     airfoil geometry definition
c     fit a b-spline curve through given data points
c
c     references:
c     'curves and surfaces for computer-aided geometric design', 
c     4th ed., g. farin, 1997.
c     
c     `mathematical elements for computer graphics', 2nd ed., rogers 
c     and adams, 1990.
c
c     'intrinsic parametrization for approximation', j. hoschek, 
c     computer aided geometric design, 1988 (5), pg 27.
c
c     program uses lapack subroutines for least-squares problem
c     use flag -lcomplib.sgimath to compile on sgi machines
c
c     written by: marian nemec 
c     date: feb. 2000
c     mods: dec. 2000
c----------------------------------------------------------------------

      subroutine bspline ( xdata, ydata, bcp, t, knot, x, y, xp, yp,
     &      errorx, errory, corr, dist, rhs)

c     xdata, ydata => input: arrays of original data to be splined
c     x, y => output: arrays of approximated data points
c     jbody => input: number of data points (max. 400)
c     nc => input: number of control points
c     order => input: order of B-Spline basis 
c     MaxIter => input: maximum number of iterations ( < 50 ) used to
c                improve the approximation.
c     tol => convergence tolerance ( < 8.d-5 ) for the approximation.

c     the order of the B-Spline can range from 2 (linear) to jbody.
c     when the number of control points is equal to jbody, the 
c     resulting representation is a Bezier curve of degree jbody-1.

      implicit none

#include "parm.inc"

      integer i, ii, j, left, iter, jle

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot 

      integer imid

      integer input_unit, grid_unit, bsp_unit, new_grid_unit
      integer dvs_unit, btec_unit, etec_unit, gridb_unit, gridc_unit 
      common/units/ input_unit, grid_unit, new_grid_unit, dvs_unit,
     &      bsp_unit, btec_unit, etec_unit, gridb_unit, gridc_unit 

      double precision xdata(jbody), ydata(jbody), t(jbody), totalarc
      double precision x(jbody), y(jbody), knot(nc+iorder), bcp(nc,2) 
      double precision xp(jbody), yp(jbody), corr(jbody), dist(jbody)
      double precision errorx(jbody), errory(jbody)
            
      double precision BIatT(ibsord), Ncoef(ibody,ibsnc)

      double precision le_of_int, le_of_poly, NewDist, tn, resid, tmp
      double precision fac, dy, dx, defl, ang, nc_half, segment

      logical cont, werr, cont2, rep

c     -- variables for QR decomposition using LAPACK --
      integer info,lwork,iknt
      double precision work(ibody*ibsnc), rhs(jbody,2),rknt
      character trans

      double precision dBsp
      double precision dist1, dist2

cCO   Variables for smart stopping criterion (to avoid an increase in
cCO   the residual if the allowed number of iterations is excessive.
      double precision prev_resid
      logical smart_stop

c      open (unit=12,file='airfoil.dat', status='new')
c      write(12,17000)(xdata(i),ydata(i),i=1,jbody)
c17000 format(2f20.8)

c     -- find leading edge of airfoil --
      dist1=0.0
      do j=2,jbody-1
         dist2 = sqrt((xdata(j)-xdata(1))**2 + (ydata(j)-ydata(1))**2)
         if (dist2.gt.dist1) then
            dist1 = dist2
            jle = j
         endif
      end do

c     -- set variables for LAPACK routine dgels --
      lwork = ibody*ibsnc
      trans = 'N'

      nc = nc - 1

      write (*,6) nc+1
      write (*,7) iorder
 6    format (/3x,'Number of control points:',i4)
 7    format (3x,'Order of B-spline basis:',i3)

      if (jbody.gt.ibody) then
        write (*,*) 'JBODY is too large - check ibody: bspline.f!!'
        stop
      end if
      if (iorder.gt.ibsord) then
        write (*,*) 'ORDER is too large - check ibsord and ibskt!!'
        stop
      end if
      if ( (nc+1).gt.ibsnc) then
        write (*,*) 'NC is too large - check ibsnc and ibskt!!'
        stop
      end if

c     -- centripetal chord length parametrization --
c     -- to obtain chord length parametrization remove dsqrt --
c        from dsqrt(segment) --
      t(1) = 0.d0
      totalarc = 0.d0
      do i = 2, jbody 
        segment = dsqrt( (xdata(i)-xdata(i-1))**2 +
     &        (ydata(i)-ydata(i-1))**2 )
        totalarc = totalarc + dsqrt(segment) 
        t(i) = t(i-1) + dsqrt(segment)
      end do
      do i = 2, jbody-1
ctml        t(i) = t(i)/totalarc*dble(nc-iorder+2)
        t(i) = t(i)/totalarc*dble(nc-iorder)
      end do
ctml      t(jbody) = dble(nc-iorder+2)
      t(jbody) = dble(nc-iorder)

c     -- set knot vector --
      do i=1,iorder
        knot(i) = 0.d0
      end do
      
ctml      nc_half = dble(nc-iorder+2)*0.5d0
      nc_half = dble(nc-iorder)*0.5
ctml      fac = 2.d0/dble(nc-iorder+2)
      fac = 2.d0/dble(nc-iorder)
      tmp = 0.d0

ctml      do i=1,(nc-iorder+2)/2
      do i=1,(nc-iorder)/2
        tmp = tmp + fac
        ii = i+iorder
c        knot(ii) = i
        knot(ii) = nc_half*0.5d0*(1.d0 -dcos(3.1415926536d0*tmp))
      end do
c     -- imid is the location of the leading edge (or middle) knot --
c     -- repeat three knots around the leading edge --
      imid         = (nc-iorder)/2 + iorder + 1 
      knot(imid)   = knot(imid-1)
      knot(imid+1) = knot(imid-1)

c     -- march 12, 2002: this do loop modifies the knot sequence near
c     the trailing edge, change 5.0 to 1.0 (or comment out section) to
c     get the original knot sequence --
      write (*,*) 'ATTENTION: MODIFYING KNOT SEQUENCE'
ctml      knot(1+iorder) = 3.0*knot(1+iorder)
      knot(1+iorder) = 2.0*knot(1+iorder)
ctml      do i=2,(nc-iorder+2)/2
      do i=2,(nc-iorder)/2
        ii = i+iorder
ctml        write (*,*) ii,knot(ii)
ctml        knot(ii) = knot(ii) + knot(1+iorder)*(1.d0-knot(ii)/
ctml     &       knot((nc-iorder+2)/2+iorder))
        knot(ii) = knot(ii) + knot(1+iorder)*(1.d0-knot(ii)/
     &       knot((nc-iorder)/2+iorder))
      end do

c     -- copy lower surface knots to upper surface --
      ii = imid-2
      do i=imid+2,nc+1          
         knot(i) = nc_half + ( nc_half - knot(ii) )
         ii = ii - 1
      end do

      do i=nc+2,nc+iorder+1
ctml        knot(i) = dble(nc-iorder+2)
        knot(i) = dble(nc-iorder)
      end do

c     -- display knot vector --
      do i=1,nc+iorder+1
         write (*,16) i,knot(i)
      end do

      write (*,*)
 16   format (i3,f8.4)
 15   format (8f8.4)

      rep = .false.
      call askl('Modify knot vector?^',rep)
      if (rep) then
         cont2 = .true.
         do while (cont2)
            call aski('Enter knot index (999 to exit):^',iknt)
            if (iknt.eq.999) then
               cont2 = .false.
            else
               call askr('Enter knot value: ^',rknt)
               knot(iknt) = rknt
            end if
         end do
      end if

      write (*,*) 'Final knot vector:'
      write (*,15) ( knot(i), i=1,nc+iorder+1 )
      write (*,*)

ctml temp
c      open (unit=13,file='knot1', status='new')
c      write (13,15) ( knot(i), i=1,nc+iorder+1 )
ctml
c This check is okay too!


c     -- start least squares approximation with parameter correction --
      resid = 1.d0
      iter = 0
      write (*,*) '** Using QR decomposition to solve the Least-Square
     &     fit. '
      write (*,*)
     |    'The technique for solving the least-sqares problem may',
     |    'result in a residual that decreases initially, but then',
     |    'increases.  This can be problematic if too many iterations',
     |    'are permitted.  If the initial residual decrease is',
     |    'monotonic, the harmful extra iterations can be eliminated',
     |    'by stopping whenever the residual at a given iteration is',
     |    'larger than at the previous one.'
      call askl('Stop at first iteration where the residual increases?^',
     |    ,smart_stop)  !CO
c      write (*,18)
c      write (*,19)
c 18   format (3x,'** Using QR decomposition to solve ')
c 19   format (6x,'the Least-Squares fit. **',/)

 20   continue                  ! main least squares loop
      iter = iter + 1
      prev_resid = resid

c      write (*,21) iter
c 21   format (3x,'Iteration:',i4)

c     -- form matrix of B-spline basis functions [ N ] --
c     -- work only on increasing sequence of the knot vector --
      do j=1,jbody
        do i=1,nc+1
          Ncoef(j,i) = 0.d0
        end do
      end do

      tmp = -1.d0
      do j=1,jbody
        do left=iorder,nc+1

          if ( knot(left).le.t(j) .and. t(j).le.knot(left+1)
     &          .and. t(j).ne.tmp ) then 
            tmp = t(j)
            call Bval( knot, t(j), left, BIatT)
            do i=1,iorder
              Ncoef(j,i+left-iorder) = BIatT(i)
            end do
          end if

        end do
        rhs(j,1) = xdata(j)
        rhs(j,2) = ydata(j)
      end do

c     -- display b-spline basis function matrix [ N ] --
c         write (*,*) 'N-Matrix:'
c         do j=1,jbody
c            write (*,25) ( Ncoef(j,i), i=1,nc+1 ) 
c         end do
c 25   format (6f8.4)

c     -- solve for control points using QR decomposition (LAPACK) --
      call dgels(trans,jbody,nc+1,2,Ncoef,ibody,rhs,jbody,work, 
     &      lwork,info)
      if (info.eq.0) then
        if (dble(lwork).lt.work(1)) write (*,*) 'Increase lwork to:',
     &        work(1)
      else
        write (*,*)
        write (*,*) '*** Problems with QR decomposition. ***'
      end if

c     -- force T.E. of airfoil to avoid roundoff --
      rhs(1,1) = xdata(1)
      rhs(1,2) = ydata(1)
      rhs(nc+1,1) = xdata(jbody)
      rhs(nc+1,2) = ydata(jbody)

c     -- force knots at L.E. of airfoil --
      imid = (nc+1+1)/2
      rhs(imid,1)   = xdata(jle)
      rhs(imid-1,1) = rhs(imid,1)
      rhs(imid+1,1) = rhs(imid,1)      

      dist1 = abs( rhs(imid-1,2) - rhs(imid,2) )
      dist2 = abs( rhs(imid+1,2) - rhs(imid,2) )
      tmp = (dist1+dist2)/2.0
      rhs(imid,2) = ydata(jle)
c      rhs(imid-1,2) = rhs(imid,2) - cwfac*tmp
c      rhs(imid+1,2) = rhs(imid,2) + cwfac*tmp
      rhs(imid-1,2) = rhs(imid,2) - 1.0d0*tmp
      rhs(imid+1,2) = rhs(imid,2) + 1.0d0*tmp

c     -- evaluate b-spline given control points --
      call curve(knot, t, x, y, rhs)

c     -- compute error for the approximation --
      werr = .false.
      call error(x, y, xdata, ydata, resid, errorx, errory, dist, werr)

c     -- check exit condition --
      if ( (resid.lt.tol).or.(iter.ge.MaxIter) .or.
     |     (smart_stop .and. resid > prev_resid) ) goto 40

c     -- proceed with parameter correction --
c     -- find tangents using analytical derivative --
c      call tangents( knot, t, x, y, xp, yp, rhs)
      
      do j = 1, nc+1
         bcp(j,1) = rhs(j,1)
         bcp(j,2) = rhs(j,2)
      end do

      tmp = -1.d0
      do j=1,jbody
         do left=iorder,nc+1
           
            if ( knot(left).le.t(j) .and. t(j).le.knot(left+1)
     &           .and. t(j).ne.tmp ) then 
               tmp = t(j)
               xp(j) = dBsp (knot, bcp(1,1), t(j), left, 1)
               yp(j) = dBsp (knot, bcp(1,2), t(j), left, 1)
            end if
         enddo
      enddo  

c     -- length of interval --
      le_of_int = knot(nc+2) - knot(1)

c     -- length of control polygon --
      le_of_poly = 0.d0
      do i=1,nc
        le_of_poly = le_of_poly + dsqrt( ( rhs(i+1,1) - rhs(i,1) )**2
     &        +( rhs(i+1,2) - rhs(i,2) )**2 )
      end do

c     -- parameter correction --
      do j=1,jbody
        corr(j) = ( errorx(j)*xp(j) + errory(j)*yp(j) )/dsqrt(
     &        xp(j)**2+ yp(j)**2 )*le_of_int/le_of_poly
      end do

c     -- update parameter array t(j) --
      tmp = -1.d0
      fac = 1.d0
      do j=1,jbody
        cont = .true.          
        do while (cont)
          tn = t(j) + fac*corr(j) 

          do left=iorder,nc+1

            if ( knot(left).le.tn .and. tn.le.knot(left+1)
     &            .and. tn.ne.tmp ) then 
              tmp = tn
              call Bval( knot, t(j), left, BIatT)

              x(j) = 0.d0
              y(j) = 0.d0
              do i=1,iorder
                x(j) = x(j) + rhs(i+left-iorder,1)*BIatT(i)
                y(j) = y(j) + rhs(i+left-iorder,2)*BIatT(i)
              end do

            end if

          end do
          
c     -- compute new distance between points: if new distance is --
c     -- smaller then update t(j), else halve the correction and --
c     -- try again --
          NewDist = dsqrt( (xdata(j)-x(j))**2 + (ydata(j)-y(j))**2 )
          if (NewDist.gt.dist(j)) then
            fac = fac*0.5d0
          else
            t(j) = tn
            cont = .false.
            fac = 1.d0
          end if
        end do
      end do
      goto 20                   !least-squares correction loop

c     -- done parameter correction --
 40   continue

c     -- rescale and rotate airfoil such that leading edge
c        position and chord length equals the original data set --

c     -- find leading edge --
c     -- mn: commented out aug 28, 2002, replaced by more robust
c     algorithm --
c     do j=jbody/3,2*jbody/3
c     if ( dabs(ydata(j)).lt.1.d-5 ) jle = j
c     end do
ctml
c      dist1=0.0
c      do j=2,jbody-1
c         dist2 = sqrt((xdata(j)-xdata(1))**2 + (ydata(j)-ydata(1))**2)
c         if (dist2.gt.dist1) then
c            dist1 = dist2
c            jle = j
c         endif
c      end do
ctml end
      write (*,41) jle,xdata(jle),ydata(jle)
 41   format (/3x,'Original airfoil L.E. is at node',i4,' at location:',
     &     e12.4,',',e12.4) 
      write (*,42) x(jle),y(jle)
 42   format (3x,'L.E. of approximated airfoil is at',e12.4,',',e12.4)

      dx = rhs(1,1) - x(jle) 
      dy = y(jle) - rhs(1,2)
      defl = datan2d(dy,dx)

c     -- defl is the angle that is required to zero the y component
c        of the leading edge --
c     -- mn: commented out aug 28, 2002 --
c     -- mn: trailing edge doesn't need to be at 1,0 --
c     ang = datan2d(dabs( rhs(1,2) ),dx)
      ang = datan2d(ydata(jle)-rhs(1,2),rhs(1,1) - xdata(jle))

      defl = ang - defl

      do i=2,nc
        dx = rhs(1,1) - rhs(i,1) 
        dy = rhs(i,2) - rhs(1,2)
        tmp = dsqrt( dx**2 + dy**2 )
        ang = datan2d(dy,dx)
        rhs(i,1) = rhs(1,1) - tmp*dcosd(ang+defl)
        rhs(i,2) = rhs(1,2) + tmp*dsind(ang+defl)
      end do

c     -- evaluate b-spline given control points --
      call curve( knot, t, x, y, rhs)

c     -- scale the airfoil to zero the x component --
c     -- mn: commented out aug 28, 2002 --
c     -- mn: original leading edge doesn't need to be at 0,0 --
c     tmp = 1.d0/(1.d0-x(jle))
c     do i=1,nc+1
c     rhs(i,1) = (rhs(i,1) - x(jle))*tmp
c     end do
      tmp = 1.d0/(1.d0-(x(jle)-xdata(jle)))
      do i=2,nc
        rhs(i,1) = (rhs(i,1) - (x(jle)-xdata(jle)))*tmp
      end do

c     -- generate final geometry --
      call curve( knot, t, x, y, rhs)

      write (*,43) defl
      write (*,44) x(jle),y(jle)
 43   format (/3x,'Airfoil was rotated by',e12.4,' degrees and scaled.')
 44   format (3x,'New L.E. coordinates:',e12.4,','e12.4)

c     -- check leading edge 'curvature' --
      if ( (x(jle-1).le.x(jle)) .or. (x(jle+1).le.x(jle)) ) write (*,*)
     &      '***** Check leading edge of airfoil!! *****' 

c     -- compute final error of approximation and max. error --
      write (*,50) iter
 50   format (/3x,'Final results after',i4,' iterations:')
      werr = .true.
      call error( x, y, xdata, ydata, resid, errorx, errory, dist, werr)
      write (*,*)

      if (tecplot) then
c     -- tecplot format --
        open (unit=btec_unit,file='airfit.tec')
        open (unit=etec_unit,file='error.tec')
        write (btec_unit,60)
        write (btec_unit,70)
        write (btec_unit,100) (xdata(i), ydata(i), i=1, jbody)
        write (btec_unit,80)
        write (btec_unit,100) (x(i), y(i), i=1, jbody)
        write (btec_unit,90)
        write (btec_unit,100) (rhs(i,1), rhs(i,2), i=1, nc+1)

 60     format ('VARIABLES= x, y')
 70     format ('ZONE T="Original Points"')
 80     format ('ZONE T="Fitted Points"')      
 90     format ('ZONE T="Control Points"')
 100    format (2e16.8)

        write (etec_unit,110)
        write (etec_unit,120) ( i, dist(i), i=1, jbody)
 110    format ('VARIABLES= "Airfoil Node", Error')
 120    format (i5,e16.8)
      end if

      do i=1,jbody
        xdata(i) = x(i)
        ydata(i) = y(i)
      end do
 
      nc = nc+1

      do i=1,nc
        bcp(i,1)=rhs(i,1)
        bcp(i,2)=rhs(i,2)
      end do
      
      return
      end                       !bspline

c-----------------------------------------------------------------------
c-----------------------------------------------------------------------

c     given knots, parameter vector and control points find curve
c     coordinates x and y

      subroutine curve( knot, t, x, y, rhs )

      implicit none

#include "parm.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer i, j, left

      double precision t(jbody), knot(nc+iorder+1), x(jbody), y(jbody)
      double precision rhs(jbody,2), tmp, BIatT(ibsord)

      tmp = -1.d0
      do j=1,jbody
        do left=iorder,nc+1

          if ( knot(left).le.t(j) .and. t(j).le.knot(left+1)
     &          .and. t(j).ne.tmp ) then 
            tmp = t(j)
            call Bval( knot, t(j), left, BIatT)

            x(j) = 0.d0
            y(j) = 0.d0
            do i=1,iorder
              x(j) = x(j) + rhs(i+left-iorder,1)*BIatT(i)
              y(j) = y(j) + rhs(i+left-iorder,2)*BIatT(i)
            end do

          end if

        end do
      end do    

      return
      end                       !curve
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------

c     compute error of approximation and max. error

      subroutine error( x, y, xdata, ydata, resid, errorx, errory, dist,
     &      werr)

      implicit none

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer j, MaxNode

      double precision resid, MaxErr, errorx(jbody), errory(jbody)
      double precision dist(jbody), xdata(jbody), ydata(jbody)
      double precision x(jbody), y(jbody)

      logical werr

      resid = 0.d0
      MaxErr = 0.d0
      do j=1,jbody
        errorx(j) = xdata(j) - x(j)
        errory(j) = ydata(j) - y(j)
        dist(j) = dsqrt( errorx(j)**2 + errory(j)**2 ) 
        resid = resid + dist(j)
        if ( dist(j).gt.MaxErr ) then
          MaxErr = dist(j)
          MaxNode = j
        end if
      end do
      resid = resid/dble(jbody)

      if (werr) then
        write (*,15) resid
        write (*,20) MaxErr, MaxNode
      end if

 15   format (3x,'Average error of approximation:',e12.4)
 20   format (3x,'Max. error of approximation:',e12.4,1x,'at node',i4)

      return
      end                       !error
c----------------------------------------------------------------------
c----------------------------------------------------------------------
c     find tangents
c     use o(2) centred-difference formula
c
c      subroutine tangents (knot, t, x, y, xp, yp, rhs)
c
c      implicit none
c
c#include "parm.inc"
c
c      integer iorder, nc, maxiter, jbody, j  
c      double precision tol
c      logical tecplot
c      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot
c
c      double precision t(jbody), knot(nc+iorder+1), x(jbody), y(jbody)
c      double precision xp(jbody), yp(jbody), rhs(jbody,2)
c
c      double precision stepsize, tnew(ibody), xplus(ibody), yplus(ibody)
c      double precision xminus(ibody), yminus(ibody)
c
c      stepsize = 1.d-8
c
c      do j=1,jbody-1
c        tnew(j) = t(j) + stepsize
c      end do
c      tnew(jbody) = t(jbody)
c      
c      call curve(knot, tnew, xplus, yplus, rhs)
c
c      tnew(1) = t(1)
c      do j=2,jbody
c        tnew(j) = t(j) - stepsize
c      end do 
c
c      call curve(knot, tnew, xminus, yminus, rhs)
c
c      do j=2,jbody-1
c        xp(j) = ( xplus(j) - xminus(j) ) / (2.d0*stepsize)
c        yp(j) = ( yplus(j) - yminus(j) ) / (2.d0*stepsize)
c      end do
c
c      xp(1) = ( xplus(1) - x(1) ) / stepsize
c      yp(1) = ( yplus(1) - y(1) ) / stepsize
c
c      xp(jbody) = ( x(jbody) - xminus(jbody) ) / stepsize
c      yp(jbody) = ( y(jbody) - yminus(jbody) ) / stepsize
c
c      return
c      end                       !tangents
c-----------------------------------------------------------------------
