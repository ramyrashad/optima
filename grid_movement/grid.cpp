
#ifndef GRID_H
#include "grid.h"
#endif

#include <iostream>
#include <iomanip>



// form connectivity for a 2D structured grid
void read_connectivity (vector <Node>& node_loc, const int& jmax, 
			const int &kmax, const int& jtail1, const int& jtail2)
{
  int j, k;
	
  for (k=1; k<kmax; k++)
    {
      for (j=k*jmax; j<(k+1)*jmax-1; j++)
	{
	  node_loc[j].add_nei(node_loc[j+1]);
	  node_loc[j+1].add_nei(node_loc[j]);
	  if ( k<kmax-1 )
	    {
	      node_loc[j].add_nei(node_loc[jmax+j]);
	      node_loc[jmax+j].add_nei(node_loc[j]);
	    }
	}   
      // j = (k+1)*jmax-1;
      if ( k<kmax-1 )
	{
	  node_loc[j].add_nei(node_loc[jmax+j]);
	  node_loc[jmax+j].add_nei(node_loc[j]);		
	}
    }
	
  //cout << "up to here in connectivity" << endl;
  // k = 0;
  for (j=0; j<jtail2-2; j++)
    {
      node_loc[j].add_nei(node_loc[j+1]);
      node_loc[j+1].add_nei(node_loc[j]);
      node_loc[j].add_nei(node_loc[jmax+j]);
      node_loc[jmax+j].add_nei(node_loc[j]);
      
      if ( j<jtail1)
	{
	  node_loc[j].add_nei(node_loc[jmax+jmax-j-1]);
	  node_loc[jmax+jmax-j-1].add_nei(node_loc[j]);
	}
    }
  // j = jtail2-2;
  node_loc[j].add_nei(node_loc[jmax+j]);
  node_loc[jmax+j].add_nei(node_loc[j]);
  node_loc[j].add_nei(node_loc[jtail1-1]);
  node_loc[jtail1-1].add_nei(node_loc[j]);
   
  return;
}


void calc_stiffness (vector <Node>& node_loc, double *stiff_val,  
		     VECTOR_double &Fx, VECTOR_double &Fy, 
		     int *colind, int *rowptr)
{
  int i, j, row (0), mark, temp, num_nei, count (0), sum_ind;
  double k, sum;
  Node ** nei;
  double * dist;   double *xyz;   double *coord;
	
  rowptr[0] = 0;
  colind[0] = 0;
	
  int size = (int) node_loc.size();
  for (j=0; j<size; j++)
    {		
      if (node_loc[j].if_surf() == false)  
	{
	  sum = 0.;
	  temp = 0;  // counts the number of neighbours that are internal nodes
	  nei = node_loc[j].get_nei();				
	  num_nei = node_loc[j].get_num_nei();
	  dist = node_loc[j].get_dist();
			
	  for (i=0; i<num_nei; i++)   
	    if (node_loc[j].get_ID() < nei[i]->get_ID())
	      break;
	  mark = i;
			
	  for (i=0; i<num_nei; i++)   
	    { 
	      // stiffen if length of spring is less than 1cm
	      if (dist[i] > 0.001)
		k = 1./dist[i];
	      else 
		k = 5./(dist[i]);
	      sum = sum - k;

	      if (i == mark)  
		{
		  if (mark < num_nei)
		    {
		      sum_ind = count;
		      count = count +1;
		    }
		}	
	      if (nei[i]->if_surf() == false)
		{
		  temp = temp +1;
		  stiff_val[count] = k;
		  colind[count] = nei[i]->get_ind();	
		  count = count +1;
		}
	      else   
		{
		  xyz = nei[i]->get_xyz();
		  coord = nei[i]->get_coord();
		  Fy[row] = Fy[row] - k*(coord[1]-xyz[1]);
		  Fx[row] = Fx[row] - k*(coord[0]-xyz[0]);
		}
	    }
	  if (mark == num_nei)
	    {
	      sum_ind = count;
	      count = count +1;
	    }
			
	  stiff_val[sum_ind] = sum;
	  colind[sum_ind] = row;
			
	  rowptr[row+1] = rowptr[row] + temp + 1;
	  row = row + 1;
	}
    }

  nei = NULL;
  dist = NULL;
  coord = NULL;
  xyz = NULL;

  return;
}


void update (vector <Node>& node, const VECTOR_double &deltax,
	     const VECTOR_double &deltay)
{
  int count = 0;	
  Node *overlap;
  double *coord;
	
  int size = (int) node.size();
  for (int j=0; j<size; j++)
    {
      if (node[j].if_surf() == false)
	{
	  node[j].deform(deltax[count], deltay[count]);
	  count = count +1;
	}
      else 
	{
	  overlap = node[j].get_overlap();
	  if (overlap != NULL)
	    {
	      coord = overlap->get_coord();
	      node[j].set_coord(coord);
	    }
	}
    }
  overlap = NULL;
  coord = NULL;

  return;
}


void update (vector <Node>& node, const VECTOR_double &deltax,
	     const VECTOR_double &deltay, const VECTOR_double& deltaz)
{
  int count = 0;	
  Node *overlap;
  double *coord;
  int symm = 0;

  int size = (int) node.size();
  for (int j=0; j<size; j++)
    {
      if (node[j].if_surf() == false)
	{
	  symm = node[j].get_symm();
	  if (symm == 0)
	    node[j].deform(deltax[count], deltay[count], deltaz[count]);
	  else 
	    {
	      coord = node[j].get_coord();
	      if (symm == 1)
		{
		  coord[0] += deltax[count];
		  coord[1] += deltay[count];
		}
	      else if (symm == 2)
		{
		  coord[1] += deltay[count];
		  coord[2] += deltaz[count];
		}
	      else if (symm == 3)
		{
		  coord[0] += deltax[count];
		  coord[2] += deltaz[count];
		}
	    }
	  count = count +1;
	}
      else 
	{
	  overlap = node[j].get_overlap();
	  if (overlap != NULL)
	    {
	      coord = overlap->get_coord();
	      node[j].set_coord(coord);
	    }
	}
    }
  overlap = NULL;
  coord = NULL;

  return;
}



void get_num_nonzeros (vector <Node> &node,
		       int& num_inn_nodes, int& nonzeros)
{
  Node **nei;
  int num_nei, i, j, size;
	
  // Set number of boundary nodes preceding a given node
  size = (int) node.size();
  num_nei = 0;
  for (j=0; j<size; j++)
    {
      i = j - num_nei;
      if (node[j].if_surf() == true)
	num_nei++;
      node[j].set_ind(i);
    }

  
  for (j=0; j<size; j++) 
    {
      if (node[j].if_surf() == false) 
	{
	  nei = node[j].get_nei();
	  num_nei = node[j].get_num_nei();
	  num_inn_nodes ++;
	  for (int i=0; i<num_nei; i++)
	    {
	      if (nei[i]->if_surf() == false)
		nonzeros++;
	    }
	}
    }
  nonzeros = nonzeros + num_inn_nodes;	

  nei = NULL;

  return;
}




// function to read a structured grid from optima2D
void read_grid (char *filename, vector<Node>& node_loc,
		vector<Node*>& bnd_nodes, 
		const int &jtail1, const int& jtail2)
{
  const bool ThreeD = false;
  int k, j, nbytes, jmax, kmax;

  ifstream fin(filename, ios::in | ios::binary);
  if (! fin.is_open())
    {
      cout << "Error opening file grid file in function read_grid (grid.cpp)"
	   << endl; 
      exit(2);
    }

  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));

  fin.read(reinterpret_cast < char * > (&jmax), sizeof(int));
  ByteSwap5(jmax);
  fin.read(reinterpret_cast < char * > (&kmax), sizeof(int));
  ByteSwap5(kmax);

  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(int));

  fin.read(reinterpret_cast < char * > (&j), sizeof(int));

  nbytes = jmax * kmax ;
  node_loc.resize(nbytes);
  double *xold = new double [nbytes];
  double *yold = new double [nbytes];
  
  for (k=0; k<nbytes ; k++)  
    {
      fin.read(reinterpret_cast < char * > (&xold[k]), sizeof(double));
      ByteSwap5(xold[k]);
    }
  for (k=0; k<nbytes ; k++)
    {
      fin.read(reinterpret_cast < char * > (&yold[k]), sizeof(double));
      ByteSwap5(yold[k]);
    }
	
  while (fin.good())
    fin.read(reinterpret_cast < char * > (&j), sizeof(int));

  fin.close();

  // assign the nodal coordinates into the Node vector
  int index = 0;
  double *xy = NULL;
  if (ThreeD==false)
    xy = new double[2];
  nbytes = 0; 

  for (k=0; k<kmax; k++)
    {
      for (j=0; j<jmax; j++)
	{

	  xy[0] = xold[index];
	  xy[1] = yold[index];
	  node_loc[index] = Node(xy, index, ThreeD);
	
	  if ( (index%jmax == 0) || ((index+1) % jmax == 0))
	    node_loc[index].is_surf();
	  else if ((index>=jtail1-1) && (index<=jtail2-1)) 
	    {
	      node_loc[index].is_surf();
	      bnd_nodes[nbytes] = &node_loc[index];
	      nbytes ++;
	      if (nbytes > 100)
		bnd_nodes.push_back(&node_loc[index]);
	    }
	  else if ((index > jtail2-1) && (index < jmax))
	    {
	      //node_loc[index].is_surf();
	      node_loc[index].overlaps(&node_loc[jmax-j-1]);
	    }
	  else if ( k==kmax-1 )
	    node_loc[index].is_surf();
	  
	  index ++;
	}
    }
  node_loc[jtail2-1].overlaps(&node_loc[jtail1-1]);
  node_loc[jmax-1].overlaps(&node_loc[0]);
 
  if (nbytes != (int) bnd_nodes.size())
    bnd_nodes.resize(nbytes);

  read_connectivity(node_loc, jmax, kmax, jtail1, jtail2);
  
  delete [] xy;    xy = NULL;
  delete [] xold;  xold = NULL;
  delete [] yold;  yold = NULL;

  return;
}




// function to write the deformed structured grid back to optima2D
void write_grid (const vector<Node>& node, double *x, double *y)
{

  double *coord;
  	
  int size = (int) node.size();
  for (int j=0; j<size; j++)
    {
      coord = node[j].get_coord();
      x[j] = coord[0];
      y[j] = coord[1];
    }

  coord = NULL;
}


// function to read a 2D structured grid from optimaMB
void read_grid_mb (char *filename, vector<Node>& node_loc, 
		   vector<Node*>& bnd_nodes, const int* ibap, 
		   const int& nfoils, const int& nblk,
		   int* jmax, int* kmax)
{  
  ifstream fs (filename, ios::in | ios::binary);

  int num, k, j, i, temp;      // counters

  //   ofstream fout("gridtest.bin", ios::out | ios::binary);

  // READING NUMBER OF BLOCKS
  fs.read (reinterpret_cast < char * > (&num), sizeof(int));
  // fout.write (reinterpret_cast < char * > (&num), sizeof(int));
  fs.read (reinterpret_cast < char * > (&num), sizeof(int));
  //fout.write(reinterpret_cast < char * > (&num), sizeof(int));
  ByteSwap5(num);
  if (num != nblk)
    cerr << "wrong input grid file to gridmovement algorithm" << endl;

  fs.read (reinterpret_cast < char * > (&num), sizeof(int));
  //fout.write(reinterpret_cast < char * > (&num), sizeof(int));
   
  //READING CONNECTIVITY FILE
  j = nblk*4;      // 4 = # sides of block
  int * blk_con = new int [j];  
  int * side_con = new int [j];
  int * bnd  = new int [j];
  int * dir = new int [j];
  int * ifoil = new int [j];
   
  const bool ThreeD = false;
  read_connectivity_mb (filename, nblk, blk_con, side_con, bnd, dir, ifoil, 
			ThreeD);

  temp = 0;  // counts tot node
  // READING JMAX AND KMAX IN EACH BLOCK
  fs.read (reinterpret_cast < char * > (&num), sizeof(int));
  //fout.write(reinterpret_cast < char * > (&num), sizeof(int));
  for (j=0; j<nblk; j++)
    {
      fs.read (reinterpret_cast < char * > (&jmax[j]), sizeof(int));
      //fout.write(reinterpret_cast < char * > (&jmax[j]), sizeof(int));
      ByteSwap5(jmax[j]);
      fs.read (reinterpret_cast < char * > (&kmax[j]), sizeof(int));
      //fout.write(reinterpret_cast < char * > (&kmax[j]), sizeof(int));
      ByteSwap5(kmax[j]);

      temp += jmax[j]*kmax[j];
      // cout << "in read_grid_mb " << j << "  " << jmax[j] << "  " << kmax[j] << "  " << temp << endl;
    }
  //exit(9);
  fs.read (reinterpret_cast < char * > (&num), sizeof(int));
  //fout.write(reinterpret_cast < char * > (&num), sizeof(int));
   
  node_loc.resize(temp);
  vector <double> x ;   // stores nodal coordinate
  vector <double> y ;
  double *xy = new double [2];

   
  int tot_node (0);  int moving_bnd_nodes (0);
  // READING COORDINATES FOR EACH BLOCK
  for (i=0; i<nblk; i++)
    {
      fs.read (reinterpret_cast < char * > (&num), sizeof(int));
      // fout.write(reinterpret_cast < char * > (&num), sizeof(int));
      ByteSwap5(num);

      k = (jmax[i]*kmax[i]);
      x.resize(k);
      y.resize(k);

      for (j=0; j<k; j++)
	{
	  fs.read (reinterpret_cast < char * > (&x[j]), sizeof(double));
	  //  fout.write(reinterpret_cast < char * > (&x[j]), sizeof(double));
	  ByteSwap5(x[j]);

	}

      for (j=0; j<k; j++)
	{
	  fs.read (reinterpret_cast < char * > (&y[j]), sizeof(double));
	  // fout.write(reinterpret_cast < char * > (&y[j]), sizeof(double));
	  ByteSwap5(y[j]);
	}
       
      fs.read (reinterpret_cast < char * > (&num), sizeof(int));
      //   fout.write(reinterpret_cast < char * > (&num), sizeof(int));

      num = 0;
      for (k=0; k<kmax[i]; k++)
	{
	  for (j=0; j<jmax[i]; j++)
	    {
	      xy[0] = x[num];
	      xy[1] = y[num];
	      node_loc[tot_node] = Node(xy, tot_node, ThreeD);

	      if (j==0)  // current_side = 2;
		{
		  temp = i*4 + 1;
		  mark_boundary (node_loc, tot_node, blk_con[temp], 
				 side_con[temp], dir[temp], bnd[temp], 
				 ifoil[temp], k, jmax, kmax, i, 
				 bnd_nodes, moving_bnd_nodes);
		}
	      if (k==0)   // current_side = 1;
		{
		  temp = i*4;  // first entry of connectivity arrays
		  mark_boundary (node_loc, tot_node, blk_con[temp], 
				 side_con[temp], dir[temp], bnd[temp], 
				 ifoil[temp], j, jmax, kmax, i,
				 bnd_nodes, moving_bnd_nodes);
		}
	      if (j == jmax[i]-1)   //  current_side = 4;
		{
		  temp = i*4 + 3;
		  mark_boundary (node_loc, tot_node, blk_con[temp], 
				 side_con[temp], dir[temp], bnd[temp], 
				 ifoil[temp], k, jmax, kmax, i, 
				 bnd_nodes, moving_bnd_nodes);
		}
	      if (k == kmax[i]-1)  // current_side = 3;
		{
		  temp = i*4 + 2;
		  mark_boundary (node_loc, tot_node, blk_con[temp], 
				 side_con[temp], dir[temp], bnd[temp], 
				 ifoil[temp], j, jmax, kmax, i, 
				 bnd_nodes, moving_bnd_nodes);
		} 
	      num ++;
	      tot_node ++;
	    }
	}
    }
       
  delete [] xy;          xy = NULL;
  delete [] blk_con;     blk_con = NULL;
  delete [] side_con;    side_con = NULL;
  delete [] bnd;         bnd = NULL;
  delete [] ifoil;       ifoil = NULL;
  delete [] dir;         dir = NULL;
          
  fs.close();
  //   fout.close();

  synchronize (filename, node_loc, ibap, bnd_nodes, nfoils, jmax);
  
  add_neighbours (node_loc, jmax, kmax, nblk);

  return;
}

void read_connectivity_mb (const char *filename, const int& nblk, 
			   int* blk_con, int* side_con, 
			   int* bnd, int* dir, int * ifoil, 
			   const bool& ThreeD)
{

  char * con_file = strtok((char*) filename,".");
  strcat(con_file,".con");

  ifstream fin (con_file);
  int temp, i, j, n;
  /*
  string title;
  for (i=0; i<14; i++)
    fin >> title;
  */

  //  i = 2; // Number of header files to skip
  j = 512; // Maximum number of char expected in a single line in the header
  fin.ignore(j, '\n');
  fin.ignore(j, '\n');

  if (ThreeD==true)
    n = 6;  // sides of blocks
  else 
    n = 4; 

  for (i=0; i<nblk; i++)
    {
      for (j=0; j<n; j++)
	{
	  fin >> temp >> temp;
	  temp = i*n +j;
	  fin >> blk_con[temp] >> side_con[temp];
	  fin >> dir[temp] >> bnd [temp];
	  fin >> ifoil[temp];

	  // make the indexing of block same as in c++
	  // i.e. starting at 0 instead of 1
	  blk_con[temp] -= 1;
	      
	  fin >> temp >> temp;
	    
	}
    }  
  fin.close();

  con_file = NULL;

  return;
}

     
// Find an overlapping node
void mark_boundary (vector <Node>& node, const int& node_ind,
		    const int& blk_con, const int& side_con, const int& dir, 
		    const int& bnd_cond, const int& ifoil, const int& given_index, 
		    const int* jmax, const int *kmax, const int& current_blk,
		    vector<Node*>& bnd_nodes, int& moving_bnd_nodes)
{

  int overlap_ind (0), temp (0);    // index of the overlapping node 
  Node *overlap;          // overlapped node
  
  
  if (bnd_cond == 0)   // node lies on an interface
    {
      if (current_blk > blk_con)  // if connecting block # is smaller
	{
	  find_overlap (blk_con, side_con, dir, given_index, jmax, kmax, 
			overlap_ind);
	  overlap = node[overlap_ind].get_overlap();
	  node[node_ind].is_surf();
	  
	  temp = node[overlap_ind].get_ifoil();
	  if (temp != 0)
	    node[node_ind].set_ifoil(temp);

	  if (node[node_ind].get_ifoil() != 0)
	    {
	      node[overlap_ind].is_surf();
	      temp = node[node_ind].get_ifoil();
	      node[overlap_ind].set_ifoil(temp);
	      if (overlap != NULL)
		{
		  overlap->is_surf();
		  overlap->set_ifoil(temp);
		}
	    }

	  if (overlap != NULL)
	    overlap_ind = overlap->get_ID();

	  node[node_ind].overlaps(&node[overlap_ind]);

	  if (node[overlap_ind].if_surf() == true && temp != 0) 
	    {
	      temp = node[overlap_ind].get_ifoil();
	      node[node_ind].set_ifoil(temp);
	    }
	}
    }
  else   // node lies on a boundary far field or airfoil wall
    {
      overlap = node[node_ind].get_overlap();

      node[node_ind].set_ifoil(ifoil);

      if (bnd_cond == 1)   // node is on the airfoil wall
	{
	  node[node_ind].is_surf();
	  if (overlap == NULL)
	    {
	      bnd_nodes[moving_bnd_nodes] = &node[node_ind];
	      moving_bnd_nodes ++;
	    }
	  else
	    {
	      if (overlap->if_surf() == false)
		{
		  bnd_nodes[moving_bnd_nodes] = overlap;
		  overlap->is_surf();
		  overlap->set_ifoil(ifoil);
		  moving_bnd_nodes ++;
		}
	    }
	}
      else if (bnd_cond == 2) // bnd_cond == 2, farfield 
	{
	  node[node_ind].is_surf();
	  if (overlap != NULL)
	    {
	      overlap->is_surf();
	      overlap->set_ifoil(ifoil);
	    }
	}
    }
  overlap = NULL;

  return;
}


void find_overlap (const int& blk_con, const int& side_con, const int& dir, 
		   const int& given_index, const int* jmax, const int *kmax, 
		   int& overlap_ind)
{

  int i (0), actual_index (0);   
  if (dir < 1)
    {
      if (side_con == 2 || side_con == 4)
	actual_index = kmax[blk_con] - (given_index + 1);
      else if (side_con == 1 || side_con == 3)
	actual_index = jmax[blk_con] - (given_index + 1);
    }
  else if (dir == 1)   // if direction of coincident sides are identical: dir = 1
    actual_index = given_index;
  else 
    cerr << "Error determining direction of coincident side in connectivity file " << endl;

  overlap_ind = 0;  // counts node up to the connecting blk_con
  for (i=0; i<blk_con; i++)
    overlap_ind += jmax[i]*kmax[i];

  // counts node # up to side side_con
  if (side_con == 1)
    overlap_ind += actual_index;
  else if (side_con == 2)
    overlap_ind = overlap_ind + (actual_index+1)*jmax[blk_con] - 1;
  else if (side_con == 3)
    overlap_ind = overlap_ind + jmax[blk_con]*(kmax[blk_con]-1) + actual_index;
  else if (side_con == 4)
    overlap_ind = overlap_ind + jmax[blk_con]*(actual_index+1) - 1;
  else
    cerr << "Error with side block number" << endl;

   
}


void synchronize (char* filename, vector <Node>& node, const int* ibap,
		  vector <Node*>& bnd_nodes, const int& nfoils, const int* jmax)
{
  int num_bnd;             // number of boundary nodes preceding this node
  int i, j, k, size;   // counter

  // separating the nodes on the airfoil depending what airfoil
  // element it belongs to
  Node *** foil = new Node** [nfoils];
  for (i=0; i<nfoils; i++)
    foil[i] = new Node*[ibap[i]];

  size = (int) bnd_nodes.size();
  for (j=0; j<nfoils; j++)
    {
      num_bnd = 0;
      for (i=0; i<size; i++)
	{
	  k = abs(bnd_nodes[i]->get_ifoil());
	  if ( (k-1) == j ) 
	    {
	      foil[j][num_bnd] = bnd_nodes[i];
	      num_bnd ++;
	    }
	} 
    }

  // reading in stagnation point file
  char * stgfile = strtok(filename,".");
  strcat(stgfile,".stg");
 
  ifstream fin (stgfile);
                         
 
  string title;
  for (i=0; i<10; i++) 
    fin >> title;

  j = nfoils + nfoils;
  short *blknum = new short[j];

  string *blkcorner = new string[j];
  for (i=0; i<j; i++)
    fin >> blknum[i] >> blkcorner[i] >> title;
  fin.close();

  num_bnd = 0;
  for (i=0; i<nfoils; i++)
    {
      j = i + nfoils;
      if (blkcorner[i+nfoils]=="LR")
	{
	  // if LE & TE are on the same block
	  if (blknum[i] == blknum[j])
	    {
	      for (k=ibap[i]-1; k>=jmax[blknum[i]-1]; k--)
		{
		  bnd_nodes[num_bnd] = foil[i][k];
		  num_bnd ++;
		}
	      for (k=0; k<jmax[blknum[i]-1]; k++)
		{
		  bnd_nodes[num_bnd] = foil[i][k];
		  num_bnd ++;
		}
	    }
	  else
	    {
	      // find index of TE node
	      for (k=ibap[i]-1; k>jmax[blknum[i]-1]+jmax[blknum[j]-1]-2; k--)
		{
		  bnd_nodes[num_bnd] = foil[i][k];
		  num_bnd ++;
		}
	      for (k=0; k<jmax[blknum[i]-1]+jmax[blknum[j]-1]-1; k++)
		{
		  bnd_nodes[num_bnd] = foil[i][k];
		  num_bnd ++;
		}
	    }
	}
      bnd_nodes[num_bnd-ibap[i]]->overlaps(&node[bnd_nodes[num_bnd-1]->get_ID()]);
    }
   
  for (i=0; i<nfoils; i++)
    delete [] foil[i];
  delete [] foil;            foil = NULL;
  delete [] blknum;          blknum = NULL;
  delete [] blkcorner;       blkcorner = NULL;
  stgfile = NULL;
  
  return;
}


// form connectivity for structured grid for OPTIMAMB
void add_neighbours (vector <Node>& node, const int* jmax, 
		     const int* kmax, const int& nblk)
{

  int i (0), j (0), k (0), ncount (0), temp (0);
  Node *overlap0;  Node *overlap1;

  for (i=0; i<nblk; i++)
    {
      for (k=0; k<kmax[i]; k++)
	{
	  for (j=0; j<jmax[i]; j++)
	    {
	      overlap0 = node[ncount].get_overlap();	     
	      overlap1 = node[ncount+1].get_overlap();
	      if ( j < (jmax[i]-1))
		{
		  if (overlap1 == NULL)
		    {
		      if (overlap0 == NULL)
			{
			  node[ncount].add_nei(node[ncount+1]);
			  node[ncount+1].add_nei(node[ncount]);
			}
		      else
			{
			  overlap0->add_nei(node[ncount+1]);
			  node[ncount+1].add_nei(node[overlap0->get_ID()]);
			}
		    }
		  else
		    {
		      if (overlap0 == NULL)
			{
			  node[ncount].add_nei(node[overlap1->get_ID()]);
			  overlap1->add_nei(node[ncount]);
			}
		      else
			{
			  overlap1->add_nei(node[overlap0->get_ID()]);
			  overlap0->add_nei(node[overlap1->get_ID()]);
			}
		    }

		}
	      
		      
	      if ( k < (kmax[i]-1) )
		{
		  temp = ncount + jmax[i];
		  overlap1 = node[temp].get_overlap();
		  if (overlap1 == NULL)
		    {
		      if (overlap0 == NULL)
			{
			  node[ncount].add_nei(node[temp]);
			  node[temp].add_nei(node[ncount]);

			}		  
		      else
			{
			  overlap0->add_nei(node[temp]);
			  node[temp].add_nei(node[overlap0->get_ID()]);
			}
		    }
		  else
		    {
		      if (overlap0 == NULL)
			{
			  node[ncount].add_nei(node[overlap1->get_ID()]);
			  overlap1->add_nei(node[ncount]);
			}
		      else
			{
			  overlap1->add_nei(node[overlap0->get_ID()]);
			  overlap0->add_nei(node[overlap1->get_ID()]);
			}
		    }
		}
	      ncount ++;
	    }   
	}
    }
  overlap0 = NULL;
  overlap1 = NULL;

  return;
}





int max(const int& x, const int& y)
{
  if (x >= y)
    return x;
  else
    return y;
}

void deform_airfoil (char* filename, vector <Node*>& bnd_nodes)
{

  // reading in new boundary file
  char * foil = strtok(filename,".");
  strcat(foil,".foil");
 
  ifstream fin (foil);
            
  double * coord = new double [2];
  int j;

  int size = (int) bnd_nodes.size();
  for (j=0; j<size; j++)
    {
      fin >> coord[0] >> coord[1] ;
      bnd_nodes[j]->set_coord(coord);
    }

  fin.close();

  delete [] coord;  coord = NULL;
  foil = NULL;

  return;
}


void write_gridMB (const vector <Node>& node, const int& nblk, 
		   const int* jmax, const int* kmax, double *x, 
		   double* y, const int& nhalo, const int* lgptr)
{
  int i0, jjmp;  // same variables as used in control/ioall.f
  int i, j, k, temp (0);
  double *xy;

  for (i=0; i<nblk; i++)
    {
      i0 = lgptr[i] + nhalo*(jmax[i]+2*nhalo) + nhalo - 1;
      jjmp = jmax[i] + 2*nhalo;
      for (k=0; k<kmax[i]; k++)
	{
	  for (j=0; j<jmax[i]; j++)
	    {
	      xy = node[temp].get_coord();
	      x[i0+j+k*jjmp] = xy[0];
	      y[i0+j+k*jjmp] = xy[1];
	      temp ++;
	    }
	}
    }
  xy = NULL;

  return;
}


// function to read a structured grid from typhoonMB
void read_grid3D (char *filename, vector <Node>& node_loc,
		  vector<Node*>& bnd_nodes) 
{

  int i, k, j, m, nbytes, nblk, count, temp;
  const bool ThreeD = true; 

  ifstream fin(filename, ios::in | ios::binary);
  if (! fin.is_open())
    {
      cout << "Error opening file grid file in function read_grid3D (grid.cpp)"
	   << endl;
      exit(2);
    }
 
  // Reads the number of blocks in file
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));
  fin.read(reinterpret_cast < char * > (&nblk), sizeof(int));
  ByteSwap5(nblk);
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));

   
  //READING CONNECTIVITY FILE
  j = nblk*6;      // 6 = # faces of block
  int * blk_con = new int [j];  
  int * side_con = new int [j];
  int * bnd  = new int [j];
  int * dir = new int [j];
  int * ifoil = new int [j];

  read_connectivity_mb (filename, nblk, blk_con, side_con, bnd, dir, ifoil, 
			ThreeD);

  int *jmax = new int [nblk];
  int *kmax = new int [nblk];
  int *mmax = new int [nblk];

  // Reads the jmax, kmax and mmax in each block
  j = 0;
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));
  for (i=0; i<nblk; i++)
    {
      fin.read(reinterpret_cast < char * > (&jmax[i]), sizeof(int));
      ByteSwap5(jmax[i]);
      fin.read(reinterpret_cast < char * > (&kmax[i]), sizeof(int));
      ByteSwap5(kmax[i]);
      fin.read(reinterpret_cast < char * > (&mmax[i]), sizeof(int));
      ByteSwap5(mmax[i]);
      j += jmax[i]*kmax[i]*mmax[i];

    }
  node_loc.resize(j);

   fin.read(reinterpret_cast < char * > (&nbytes), sizeof(int));
 // est_moving_bnd_nodes (filename, j, jmax, kmax, mmax);
  //bnd_nodes.resize(j);

  ofstream ascii("header.dat");
  ascii << nblk << endl;
  for (i=0; i<nblk; i++)
    ascii << setw(10) << jmax[i] << setw(10) << kmax[i] 
	  << setw(10)<<mmax[i]<<endl;
  /****************************************************/

  vector <double> x;
  vector <double> y;
  vector <double> z;
  double *xyz = new double [3];


  // Reads the nodal coordinates for each block
  int tot_node = 0;  // keeps track of tot_node

  for (i=0; i<nblk; i++)
    {
      fin.read(reinterpret_cast < char * > (&nbytes), sizeof(int));
      
      nbytes = jmax[i]*kmax[i]*mmax[i];
      x.resize(nbytes);
      y.resize(nbytes);
      z.resize(nbytes);

      for (j=0; j<nbytes; j++)
	{
	  fin.read(reinterpret_cast < char * > (&x[j]), sizeof(double));
	  ByteSwap5(x[j]);
	}

      for (j=0; j<nbytes; j++)
	{
	  fin.read(reinterpret_cast < char * > (&y[j]), sizeof(double));
	  ByteSwap5(y[j]);
	}
      for (j=0; j<nbytes; j++)
	{
	  fin.read(reinterpret_cast < char * > (&z[j]), sizeof(double));
	  ByteSwap5(z[j]);
	}

      fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));

      count = 0;
      for (m=0; m<mmax[i]; m++)
	{
	  for (k=0; k<kmax[i]; k++)
	    {
	      for (j=0; j<jmax[i]; j++)
		{

		  xyz[0] = x[count];
		  xyz[1] = y[count];
		  xyz[2] = z[count];
		  node_loc[tot_node] = Node(xyz, tot_node, ThreeD);

		  if (j==0)  // current_side = 4;   //now side 1
		    {
		      temp = i*6;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, k, m, jmax, kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(2);
		    } 
		  if (k==0)  // current_side = 6;   //now side 3 
		    {
		      temp = i*6 + 2;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, j, m, jmax, kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(3);
		    } 
		  if (m==0)  // current_side = 1;    //now side 5
		    {
		      temp = i*6 + 4;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, j, k, jmax, kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(1);
		    }
		  if (j==jmax[i]-1)   // current_side = 2;
		    {
		      temp = i*6 + 1;  // first entry of connectivity arrays
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, k, m, jmax, kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(2);
		    }
		  if (k==kmax[i]-1)  // current_side = 5;   // now side 4
		    {
		      temp = i*6 + 3;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, j, m, jmax, kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(3);
		    } 
		  if (m==mmax[i]-1)   // current_side = 3;  // now side 6
		    {
		      temp = i*6 + 5;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, j, k, jmax, kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(1);
		    }
	
		  count ++;
		  tot_node ++;
		}
	    }
	}
    }

  delete [] xyz;         xyz = NULL;
  delete [] blk_con;     blk_con = NULL;
  delete [] side_con;    side_con = NULL;
 
  delete [] bnd;         bnd = NULL;
  delete [] dir;         dir = NULL;
  delete [] ifoil;       ifoil = NULL;
  
  add_neighbours3D (node_loc, jmax, kmax, mmax, nblk);

  delete [] jmax;        jmax = NULL;
  delete [] kmax;        kmax = NULL;
  delete [] mmax;        mmax = NULL;


  count = 0;
  for (i=0; i<tot_node; i++)
    if (node_loc[i].get_ifoil() != 0 && node_loc[i].get_overlap() == NULL)
      count ++;
  bnd_nodes.resize(count);

  // put nodes on airfoil/wing into bnd_nodes vector (moving boundary nodes)
  temp = 0;
  for (i=0; i<tot_node; i++)
    {
      if (node_loc[i].get_ifoil() != 0 && node_loc[i].get_overlap() == NULL)
	{
	  bnd_nodes[temp] = &node_loc[i];
	  temp ++;
	}
    }

  return;

}



void mark_boundary3D (vector <Node>& node, const int& node_ind, 
		      const int& blk_con, const int& side_con, const int& dir, 
		      const int& bnd, const int& ifoil, const int& current_blk, 
		      const int& j, const int& k, 
		      const int* jmax, const int* kmax, const int* mmax)
{

  int overlap_ind (0), temp (0);    // index of the overlapping node 
  Node *overlap;          // overlapped node
  Node *first;
    
  if (bnd == 0)   // node lies on an interface
    {
      if (current_blk > blk_con)  // if connecting block # is smaller
      	{
	  find_overlap3D (blk_con, side_con, dir, j, k, jmax, kmax, 
			  mmax, overlap_ind);
	  overlap = node[overlap_ind].get_overlap();
	  node[node_ind].is_surf();

	  first = node[node_ind].get_overlap();
	  if (first != NULL)
	    {
	      temp = first->get_ID();
	      if (overlap != NULL)
		overlap_ind = overlap->get_ID();
	      
	      if ( temp < overlap_ind )
		if (first->if_surf() == true)
		  if (first->get_ifoil() != 0)
		    node[overlap_ind].overlaps(&node[temp]);
		  else
		    { 
		      node[temp].overlaps(&node[overlap_ind]);
		      node[node_ind].overlaps(&node[overlap_ind]);
		      temp = node[overlap_ind].get_ifoil();
		      node[node_ind].set_ifoil(temp);
		    }
	    }
	  else
	    {
	      if (overlap != NULL)
		overlap_ind = overlap->get_ID();
	      
	      node[node_ind].overlaps(&node[overlap_ind]);
	      
	      if (node[overlap_ind].if_surf() == true) 
		{
		  temp = node[overlap_ind].get_ifoil();
		  node[node_ind].set_ifoil(temp);
		}
	    }
	}
    }
  else   // node lies on a boundary
    {
      overlap = node[node_ind].get_overlap();
      if (overlap == NULL)
	{
	  if (bnd == 1)
	    {
	      node[node_ind].set_ifoil(ifoil);
	      node[node_ind].is_surf();
	    }
	  else if (bnd == 2)
	    node[node_ind].is_surf();
	}
      else	  
	{
	  if (bnd == 1)   // node is on the airfoil wall
	    {
	      node[node_ind].is_surf();
	      node[node_ind].set_ifoil(ifoil);
	      overlap->set_ifoil(ifoil);
	      overlap->is_surf();
	    }
	  else 
	    {
	      node[node_ind].is_surf();
	      if (overlap->get_ifoil() != 0)
		{
		  temp = overlap->get_ifoil();
		  node[node_ind].set_ifoil(temp);
		}
	    }
	}
    }
  overlap = NULL;
  first = NULL;
  return;
}


void  find_overlap3D (const int& blk_con, const int& face_con, const int& dir, 
		      const int& j, const int& k, const int* jmax, 
		      const int* kmax, const int* mmax, int& overlap_ind)

{
  int count;  // counter

  overlap_ind = 0;
  for (count=0; count<blk_con; count++)
    overlap_ind += jmax[count]*kmax[count]*mmax[count];
  
  switch (dir) 
    {
    case -1:
      cerr << "Direction of connecting face is opposite - ";
      cerr << "overlap links have not been implemented" << endl;
      exit (10);
      break;
    default:
      switch (face_con)
	{
	case 5:
	  overlap_ind += k*jmax[blk_con] + j;
	  break;
	case 2:   // j=k , k=m
	  overlap_ind += jmax[blk_con]*kmax[blk_con]*k + jmax[blk_con]*(j+1) -1;
  	  break;
 	case 6:
	  overlap_ind += jmax[blk_con]*kmax[blk_con]*(mmax[blk_con]-1)
	    + jmax[blk_con]*k + j;
	  break;
	case 1:
	  overlap_ind += jmax[blk_con]*kmax[blk_con]*k + jmax[blk_con]*j;
	  break;
	case 4:
	  overlap_ind += jmax[blk_con]*kmax[blk_con]*k 
	    + jmax[blk_con]*(kmax[blk_con]-1) + j;
	  break;
	case 3:
	  overlap_ind += jmax[blk_con]*kmax[blk_con]*k + j;
	  break;
	default:
	  cerr << "Undefined connecting face number! " << endl;
	  exit (10);
	  break;
	}
      break;
    }   
}



// form connectivity for structured grid for OPTIMAMB
void add_neighbours3D (vector <Node>& node, const int* jmax, 
		       const int* kmax, const int* mmax, const int& nblk)
{

  int i (0), j (0), k (0), m (0), ncount (0), temp (0);
  Node *overlap0;  Node *overlap1;

  for (i=0; i<nblk; i++)
    {
      for (m=0; m<mmax[i]; m++)
	{
	  for (k=0; k<kmax[i]; k++)
	    {
	      for (j=0; j<jmax[i]; j++)
		{
		  overlap0 = node[ncount].get_overlap();	     
		  if ( j < (jmax[i]-1))
		    {
		      overlap1 = node[ncount+1].get_overlap();
		      if (overlap1 == NULL)
			{
			  if (overlap0 == NULL)
			    {
			      node[ncount].add_nei(node[ncount+1]);
			      node[ncount+1].add_nei(node[ncount]);
			    }
			  else
			    {
			      overlap0->add_nei(node[ncount+1]);
			      node[ncount+1].add_nei(node[overlap0->get_ID()]);
			    }
			}
		      else
			{
			  if (overlap0 == NULL)
			    {
			      node[ncount].add_nei(node[overlap1->get_ID()]);
			      overlap1->add_nei(node[ncount]);
			    }
			  else
			    {
			      overlap1->add_nei(node[overlap0->get_ID()]);
			      overlap0->add_nei(node[overlap1->get_ID()]);
			    }
			}

		    }

		  if ( k < (kmax[i]-1) )
		    {
		      temp = ncount + jmax[i];
		      overlap1 = node[temp].get_overlap();
		      if (overlap1 == NULL)
			{
			  if (overlap0 == NULL)
			    {
			      node[ncount].add_nei(node[temp]);
			      node[temp].add_nei(node[ncount]);

			    }		  
			  else
			    {
			      overlap0->add_nei(node[temp]);
			      node[temp].add_nei(node[overlap0->get_ID()]);
			    }
			}
		      else
			{
			  if (overlap0 == NULL)
			    {
			      node[ncount].add_nei(node[overlap1->get_ID()]);
			      overlap1->add_nei(node[ncount]);
			    }
			  else
			    {
			      overlap1->add_nei(node[overlap0->get_ID()]);
			      overlap0->add_nei(node[overlap1->get_ID()]);
			    }
			}
		    }

		  if ( m < (mmax[i]-1) )
		    {
		      temp = ncount + jmax[i]*kmax[i];
		      overlap1 = node[temp].get_overlap();
		      if (overlap1 == NULL)
			{
			  if (overlap0 == NULL)
			    {
			      node[ncount].add_nei(node[temp]);
			      node[temp].add_nei(node[ncount]);

			    }		  
			  else
			    {
			      overlap0->add_nei(node[temp]);
			      node[temp].add_nei(node[overlap0->get_ID()]);
			    }
			}
		      else
			{
			  if (overlap0 == NULL)
			    {
			      node[ncount].add_nei(node[overlap1->get_ID()]);
			      overlap1->add_nei(node[ncount]);
			    }
			  else
			    {
			      overlap1->add_nei(node[overlap0->get_ID()]);
			      overlap0->add_nei(node[overlap1->get_ID()]);
			    }
			}
		    }
		  //cout << node.size() << "  neighbours3D " << node[ncount] << endl;
		  // cout << ncount << "    " << node[ncount+1] << endl;
		  // cout << ncount << endl;
		  ncount ++;
		}   
	    }
	}
    }
  overlap0 = NULL;
  overlap1 = NULL;

  return;
}



void est_moving_bnd_nodes (char* gridfile, int& moving_bnd_nodes,
			   const int* jmax, const int* kmax, const int* mmax)
{
  char * stgfile = strtok(gridfile,".");
  strcat(stgfile,".stg");

  ifstream fin (stgfile);

  string title;
  do 
    fin >> title;
  while (title != "--");
  fin >> title >> title;

  int blkno;
  fin >> blkno;

  blkno -= 1;
   
  vector <int> a(3);
  a[0] = jmax[blkno];
  a[1] = kmax[blkno];
  a[2] = mmax[blkno];
   
  sort (a.begin(),a.end());

  moving_bnd_nodes = a[2]*a[1];
  //   cout << "estimate of moving_bnd_nodes " << moving_bnd_nodes << endl;
 
  stgfile = NULL;

  return;
}


void calc_stiffness (vector <Node>& node, double *stiff_val, 
		     VECTOR_double &Fx, VECTOR_double &Fy, VECTOR_double &Fz,
		     int *colind, int *rowptr)
{
  int i, j, row (0), mark, temp, num_nei, count (0), sum_ind, symm;
  double k, sum;
  Node ** nei;
  double * dist;   double *xyz;   double *coord;
	
  rowptr[0] = 0;
  colind[0] = 0;

  int size = (int) node.size();
  for (j=0; j<size; j++)
    {		
      if (node[j].if_surf() == false)  
	{
	  sum = 0.;
	  temp = 0;  // counts the number of neighbours that are internal nodes
	  nei = node[j].get_nei();				
	  num_nei = node[j].get_num_nei();
	  dist = node[j].get_dist();
			
	  for (i=0; i<num_nei; i++)   
	    if (node[j].get_ID() < nei[i]->get_ID())
	      break;
	  mark = i;
			
	  for (i=0; i<num_nei; i++)   
	    { 
	      symm = node[j].get_symm();
	      // stiffen if length of spring is less than 0.05
	      if (dist[i] > 0.05)
		k = 1./dist[i];
	      else 
		k = 10./(dist[i]);
	      sum = sum - k;

	      if (i == mark)  
		{
		  if (mark < num_nei)
		    {
		      sum_ind = count;
		      count = count +1;
		    }
		}	
	      if (nei[i]->if_surf() == false)
		{
		  temp = temp +1;
		  stiff_val[count] = k;
		  colind[count] = nei[i]->get_ind();	
		  count = count +1;
		}
	      else
		{
		  xyz = nei[i]->get_xyz();
		  coord = nei[i]->get_coord();
		  
		  if (symm == 0)
		    {
		      Fz[row] = Fz[row] - k*(coord[2]-xyz[2]);
		      Fy[row] = Fy[row] - k*(coord[1]-xyz[1]);
		      Fx[row] = Fx[row] - k*(coord[0]-xyz[0]);
		    }
		  else if (symm == 1)
		    {
		      Fy[row] = Fy[row] - k*(coord[1]-xyz[1]);
		      Fx[row] = Fx[row] - k*(coord[0]-xyz[0]);
		    }
		  else if (symm == 2)
		    {
		      Fy[row] = Fy[row] - k*(coord[1]-xyz[1]);
		      Fz[row] = Fz[row] - k*(coord[2]-xyz[2]);
		    }
		  else if (symm == 3)
		    {
		      Fz[row] = Fz[row] - k*(coord[2]-xyz[2]);
		      Fx[row] = Fx[row] - k*(coord[0]-xyz[0]);
		    }
		  else
		    {
		      cout << "Unknown symmetry plane" << endl;
		      exit (2);
		    }		  
		}
	    }
	  if (mark == num_nei)
	    {
	      sum_ind = count;
	      count = count +1;
	    }
			
	  stiff_val[sum_ind] = sum;
	  colind[sum_ind] = row;
			
	  rowptr[row+1] = rowptr[row] + temp + 1;
	  row = row + 1;
	}
    }
  nei = NULL;    dist = NULL;     xyz = NULL;    coord = NULL;
}


// write multibloc 2D structured grid to plot3D format
void write_plot3D(char* filename, const vector <Node>& node)
{

  int i, jmax, kmax;
  double *xy;

  ifstream fin(filename, ios::in | ios::binary);
  if (! fin.is_open())
    {
      cout << "Error opening file grid file in function write_plot3D (grid.cpp)"
	   << endl; 
      exit(2);
    }
 
  fin.read(reinterpret_cast < char * > (&i), sizeof(int));

  fin.read(reinterpret_cast < char * > (&jmax), sizeof(int));
  ByteSwap5(jmax);

  fin.read(reinterpret_cast < char * > (&kmax), sizeof(int));
  ByteSwap5(kmax);
  
  fin.read(reinterpret_cast < char * > (&i), sizeof(int));
  fin.close();

  ofstream fout("gridout_plot3D.bin", ios::out | ios::binary);

  fout.write(reinterpret_cast < char * > (&jmax), sizeof(int));
  fout.write(reinterpret_cast < char* > (&kmax), sizeof(int));

  jmax = (int) node.size();
  for (i=0; i<jmax; i++)
    {
      xy = node[i].get_coord();
      fout.write(reinterpret_cast < char* > (&xy[0]), sizeof(double));
    }

  for (i=0; i<jmax; i++)
    {
      xy = node[i].get_coord();
      fout.write(reinterpret_cast < char* > (&xy[1]), sizeof(double));
    }


  fout.close();
  xy = NULL;

  return;
}


void write_plot3DMB(const vector <Node>& node, const int& nblk,
		    int* jmax, int* kmax)
{

  int i, n, j;
  int num_blk = nblk;
  double *xy;

  int x (0);
  ofstream fout("gridout_plot3D.bin", ios::out | ios::binary);

  fout.write(reinterpret_cast < char * > (&num_blk), sizeof(int));

  for (i=0; i<nblk; i++)
    {
      fout.write(reinterpret_cast < char * > (&jmax[i]), sizeof(int));
      fout.write(reinterpret_cast < char* > (&kmax[i]), sizeof(int));
    }
  
  for (i=0; i<nblk; i++)
    {
      n = jmax[i]*kmax[i];
      for (j=0; j<n; j++)
	{
	  xy = node[x].get_coord();
	  fout.write(reinterpret_cast < char* > (&xy[0]), sizeof(double));
	  x++;
	}
      x -= n;
      for (j=0; j<n; j++)
	{
	  xy = node[x].get_coord();
	  fout.write(reinterpret_cast < char* > (&xy[1]), sizeof(double));
	  x ++;
	}
    }


  fout.close();
  xy = NULL;

  return;
}



void write3Dgrid_plot3D(const char* gridfile, const vector <Node>& node)
{

  int nblk, i, j;
  /* ifstream r ("grid_out.bin", ios::in | ios::binary);
     r.read(reinterpret_cast < char * > (&i), sizeof(int));
     r.read(reinterpret_cast < char * > (&nblk), sizeof(int));

     int *jmax = new int [nblk];
     int *kmax = new int [nblk];
     int *mmax = new int [nblk];
     for (i=0; i<nblk; i++)
     {
     r.read(reinterpret_cast < char * > (&jmax[i]), sizeof(int));
     r.read(reinterpret_cast < char* > (&kmax[i]), sizeof(int));
     r.read(reinterpret_cast < char* > (&mmax[i]), sizeof(int));
     }   
     r.close();
  */
  ifstream fin ("header.dat");
  fin >> nblk;
  int *jmax = new int [nblk];
  int *kmax = new int [nblk];
  int *mmax = new int [nblk];
  for (i=0; i<nblk; i++)
    fin >> jmax[i] >> kmax[i] >> mmax[i];
  fin.close();

  int k (0), count (0);
  double *xy;

  vector <double> x (9); 
  vector <double> y (9); 
  vector <double> z (9);

 

  ofstream fout(gridfile, ios::out | ios::binary);
  fout.write(reinterpret_cast < char * > (&nblk), sizeof(int));

  for (i=0; i<nblk; i++)
    {
      fout.write(reinterpret_cast < char * > (&jmax[i]), sizeof(int));
      fout.write(reinterpret_cast < char* > (&kmax[i]), sizeof(int));
      fout.write(reinterpret_cast < char* > (&mmax[i]), sizeof(int));
    }   

  
  for (i=0; i<nblk; i++)
    {
      //fout.write (reinterpret_cast < char * > (&num), sizeof(int));
      // fout.write(reinterpret_cast < char * > (&num), sizeof(int));
      //ByteSwap5(num);
  
      k = jmax[i]*kmax[i]*mmax[i];
      //cout << k << "  " << nblk << " " << node.size() << endl;
      x.resize(k);         y.resize(k);    z.resize(k);
 
      
      for (j=0; j<k; j++)
	{
	  xy = node[count].get_coord();
	  x[j] = xy[0];   y[j] = xy[1];  z[j] = xy[2];
	  count ++;

	}

      //cout << i << endl;
      for (j=0; j<k; j++)
	{
	  fout.write (reinterpret_cast < char * > (&x[j]), sizeof(double));
	  //  fout.write(reinterpret_cast < char * > (&x[j]), sizeof(double));
	  //ByteSwap5(x[j]);

	}

      for (j=0; j<k; j++)
	{
	  fout.write (reinterpret_cast < char * > (&y[j]), sizeof(double));
	  // fout.write(reinterpret_cast < char * > (&y[j]), sizeof(double));
	  //ByteSwap5(y[j]);
	}

      for (j=0; j<k; j++)
	{
	  fout.write (reinterpret_cast < char * > (&z[j]), sizeof(double));
	  // fout.write(reinterpret_cast < char * > (&y[j]), sizeof(double));
	  //ByteSwap5(y[j]);
	}
       
      //fout.write (reinterpret_cast < char * > (&num), sizeof(int));
      //   fout.write(reinterpret_cast < char * > (&num), sizeof(int));
 
    }	

  delete [] jmax;    jmax = NULL;
  delete [] kmax;    kmax = NULL;
  delete [] mmax;    mmax = NULL;

  fout.close();
}


