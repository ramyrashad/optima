c     CUSP Dissipation
c     -- calculate alpha* and beta, add dissipation  --
c     Written by: Marian Nemec
c     Date: April, 1998
c
      subroutine dcexpy ( jdim,kdim,q,s,xyj,xy,press,x,y,cbeta,wq,
     &      d_op,enth,bt,fluxe,fluxf,astar )
c
#include "../include/arcom.inc"
c
      integer i,j,k
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)
      dimension press(jdim,kdim),xy(jdim,kdim,4),x(jdim,kdim)
      dimension y(jdim,kdim),wq(2,jdim,kdim,4),d_op(jdim,kdim,4)
      dimension enth(jdim,kdim),bt(jdim,kdim),fluxe(4,2),fluxf(4,2)
      dimension astar(jdim,kdim),cbeta(jdim,kdim,3)
c     
      double precision Roe,u,v,renth,met1,met2,rss,ruu,rm,cap_ave,jave
      double precision dtd,p,up1,vp1,a0,iq1,iq1p,ir,iwq1,wq2,wq3,wq4
c
c     Calculate enthalpy (H=E+p/rho)
      do k=kbegin,kend
         do j=jbegin,jend
            enth(j,k) = (q(j,k,4) + press(j,k))/q(j,k,1)
         end do
      end do
c
      do k=kbegin,kup
        do j=jlow,jup
c     Calculate sound speed and mach number at state j+1/2 using 
c     Roe average.
          iq1 = 1.d0/q(j,k,1)
          iq1p = 1.d0/q(j,k+1,1)
          Roe = dsqrt((q(j,k+1,1)*xyj(j,k+1))/(q(j,k,1)*xyj(j,k)))
          ir = 1.d0/(Roe + 1.d0)           
          u = ( Roe*q(j,k+1,2)*iq1p + q(j,k,2)*iq1 )*ir
          v = ( Roe*q(j,k+1,3)*iq1p + q(j,k,3)*iq1 )*ir
          renth = ( Roe*enth(j,k+1) + enth(j,k) )*ir
c     
c     Use contravariant velocity, taken from subroutine 'eigval.f'.
c     Metric transformations for j+1/2 state. 
          met1 = ( xy(j,k+1,3) + xy(j,k,3) )*0.5d0
          met2 = ( xy(j,k+1,4) + xy(j,k,4) )*0.5d0
          rss = dsqrt( gami*( renth - ( u**2 + v**2 )*0.5d0) )
     $          *dsqrt( met1**2 + met2**2 )
          ruu = u*met1 + v*met2
          rm = ruu/rss
c     Store eigenvalues at half nodes -> to be used for LHS dissipation.
c     Note the last index '1' denotes xi sweep.
          cbeta(j,k,1) = ruu
          cbeta(j,k,2) = ruu + rss
          cbeta(j,k,3) = ruu - rss
c     
c     Calculate average contravariant velocity using arithmetic mean.
          u = q(j,k,2)*iq1
          v = q(j,k,3)*iq1
          up1 = q(j,k+1,2)*iq1p
          vp1 = q(j,k+1,3)*iq1p
          cap_ave = met1*0.5d0*( u + up1 ) + met2*0.5d0*( v + vp1 )
c     
c     Determine constants alpha* and beta for curvilinear coordinates.
          if (rm.le.1.d0.and.rm.ge.0.d0) then
            bt(j,k) = dmax1( 0.d0, (cap_ave + (ruu - rss))/(cap_ave
     $            -(ruu - rss)) )
          else if (rm.ge.-1.d0.and.rm.lt.0.d0) then
            bt(j,k) = -dmax1( 0.d0, (cap_ave + (ruu + rss))/(cap_ave 
     $            -(ruu + rss)) )
          else if (dabs(rm).gt.1.d0) then
            bt(j,k) = dsign(1.d0,rm)  
          end if
c     
c     Mean value of Jacobian:
          jave = 1.d0/(( xyj(j,k+1) + xyj(j,k) )*0.5d0)
          astar(j,k) = ( dabs(cap_ave) - bt(j,k)*cap_ave )*jave
          bt(j,k) = bt(j,k)*jave
c     
c     Forward difference for the flux vector, using the Roe averaged
c     flux vectors:
c     delta ^F = eta_x(k+1/2)*( delta E ) + eta_y(k+1/2)*( delta F).
c     
          if (dabs(bt(j,k)).gt.1.d-8) then
            do i=1,2
              iwq1 = 1.d0/wq(i,j,k,1)
              wq2 = wq(i,j,k,2)
              wq3 = wq(i,j,k,3)
              wq4 = wq(i,j,k,4)
c
              p = gami*( wq4 - 0.5d0*(wq2**2 + wq3**2 )*iwq1 )
c     
              fluxe(1,i) = wq2
              fluxe(2,i) = wq2*wq2*iwq1 + p
              fluxe(3,i) = wq2*wq3*iwq1
              fluxe(4,i) = wq2*iwq1*(wq4 + p)
c     
              fluxf(1,i) = wq3
              fluxf(2,i) = wq2*wq3*iwq1
              fluxf(3,i) = wq3*wq3*iwq1 + p
              fluxf(4,i) = wq3*iwq1*(wq4 + p)
            end do
c     
            do i=1,4
              d_op(j,k,i) = met1*(fluxe(i,1) - fluxe(i,2))
     $              + met2*(fluxf(i,1) - fluxf(i,2))
            end do
c     
          else
            do i=1,4
              d_op(j,k,i) = 0.d0
            end do
          end if
        end do
      end do
c     
c-------------------Dissipation for R.H.S.------------------------------
c
      dtd = dt / (1. + phidt)  
c     Add dissipation to R.H.S.
      do i=1,4
        do k=klow,kup
          do j=jlow,jup
            s(j,k,i) = s(j,k,i) + 0.5d0*dtd*(
     $            astar(j,k)*(wq(1,j,k,i) - wq(2,j,k,i))
     $            -astar(j,k-1)*(wq(1,j,k-1,i) - wq(2,j,k-1,i))
     $            +bt(j,k)*d_op(j,k,i)
     $            -bt(j,k-1)*d_op(j,k-1,i) )
c     Flux budget array:
            if (flbud.and.i.eq.2) then
              budget(j,k,2) = budget(j,k,2) + 0.5d0*dtd*(
     $              astar(j,k)*(wq(1,j,k,i) - wq(2,j,k,i))
     $              -astar(j,k-1)*(wq(1,j,k-1,i) - wq(2,j,k-1,i))
     $              +bt(j,k)*d_op(j,k,i)
     $              -bt(j,k-1)*d_op(j,k-1,i) )
            end if
          end do
        end do
      end do
c     
c     For LHS dissipation -> beta * eigenvalues
      do i=1,3
        do k=kbegin,kup
          do j=jlow,jup
            cbeta(j,k,i) = astar(j,k) + bt(j,k)*cbeta(j,k,i)
          end do
        end do
      end do
c     
      return
      end                       !dcexpy
