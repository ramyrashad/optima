c----------------------------------------------------------------------
c     -- setup block sparse jacobian for reordering --
c     
c     written by: marian nemec
c     date: july 2000
c----------------------------------------------------------------------
      subroutine blk_jac (jdim, kdim, jt1, jt2, ja, ia, iblock, indx)

c     implicit none

#include "../include/arcom.inc"
      
      integer j, k, jj, jk, jf, jt1, jt2
      integer jdim, kdim, iblock
c     integer jmax, kmax 
c     integer jup, kup, jbegin, jend, kbegin, kend, jlow, klow
      integer ja(jdim*kdim*iblock), ia(jdim*kdim+1), indx(jdim,kdim)

      if (.not.periodic) then
         jmaxtmp = jdim
      else
         jmaxtmp = jdim-1
      end if
 
      kmax = kdim

c                                                                       
c     set periodic index                                         
c
                                                           
      jbegin = 1                                                        
      jend = jmaxtmp                                                       
      kbegin = 1                                                        
      kend = kmax                                                       
      klow = 2                                                          
      kup = kmax-1                                                          
      do j = 1, jdim                                                
         jplus(j) = j+1                                                 
         jminus(j) = j-1 
      end do
c                                                                       
      if (.not.periodic) then                                        
         jplus(0)= 1
         jplus(jmaxtmp) = jmaxtmp                                             
         jminus(1) = 1                                                  
         jlow = 2                                                       
         jup = jmaxtmp-1 
      else 
         jbegin = 1                                                        
         jend = jmaxtmp
         jplus(0)= 1
         jplus(jmaxtmp) = 1 
         jplus(jmaxtmp+1) = 2                                                
         jminus(1) = jmaxtmp 
         jminus(0) = jmaxtmp-1
         jlow = 1                                                       
         jup = jmaxtmp                                                         
      endif


c     -- first order jacobian stencil --
c     -- interior nodes --
      do k = klow,kup
        do j = jlow,jup
          jk = indx(j,k)
          jj = (jk-1)*iblock
          ja(jj+1) = indx(jminus(j),k)
          ja(jj+2) = indx(j,k-1)
          ja(jj+3) = indx(j,k)
          ja(jj+4) = indx(j,k+1)
          ja(jj+5) = indx(jplus(j),k)
          ia(jk) = 5
        end do
      end do

c     -- B.C. body surface --
      k = 1
      if (.not.viscous) then
        do j = jt1,jt2
          jk = indx(j,k)
          jj = (jk-1)*iblock
          ja(jj+1) = indx(j,k)
          ja(jj+2) = indx(j,k+1)
          ja(jj+3) = indx(j,k+2)
          ia(jk) = 3
        end do
      else
        do j = jt1,jt2
          jk = indx(j,k)
          jj = (jk-1)*iblock
          ja(jj+1) = indx(j,k)
          ja(jj+2) = indx(j,k+1)
          ia(jk) = 2
        end do
      end if

      if(.not.periodic) then

c     -- B.C. wake 1 --
      k = 1
      do j = jlow,jt1-1
        jk = indx(j,k)
        jf = jmaxtmp-j+1
        jj = (jk-1)*iblock
        ja(jj+1) = indx(j,k)
        ja(jj+2) = indx(j,k+1)
        ja(jj+3) = indx(jf,k+1)
        ia(jk) = 3
      end do

c     -- B.C. wake 2 --
      k = 1
      do j = jt2+1,jup
        jf = jmaxtmp-j+1
        jk = indx(j,k)
        jj = (jk-1)*iblock
        ja(jj+1) = indx(j,k)
        ja(jj+2) = indx(j,k+1)
        ja(jj+3) = indx(jf,k+1)
        ia(jk) = 3
      end do

      end if  !not periodic

c     -- B.C. k=kmax --
      k = kmax
      do j = jlow,jup
        jk = indx(j,k)
        jj = (jk-1)*iblock
        ja(jj+1) = indx(j,k-1)
        ja(jj+2) = indx(j,k)
        ia(jk) = 2
      end do

      if(.not.periodic) then

c     -- B.C. j=1 --
      j = 1
      do k = kbegin,kmax
        jk = indx(j,k)
        jj = (jk-1)*iblock
        ja(jj+1) = indx(j,k)
        ja(jj+2) = indx(jplus(j),k)
        ia(jk) = 2
      end do

c     -- B.C. j=jmax --
      j = jmaxtmp
      do k = kbegin,kmax
        jk = indx(j,k)
        jj = (jk-1)*iblock
        ja(jj+1) = indx(jminus(j),k)
        ja(jj+2) = indx(j,k)
        ia(jk) = 2
      end do

      end if !not periodic

c$$$      if(periodic) then
c$$$
c$$$c     -- jend periodic bc --
c$$$      j=jmaxold
c$$$      do k = kbegin,kend
c$$$  
c$$$          jk  = indx(j,k) 
c$$$          jj  = (jk - 1 )*iblock
c$$$          jm1 = indx(1,k)
c$$$         
c$$$          ja(jj+1) = jm1 
c$$$          ja(jj+2) = jk          
c$$$          ia(jk) = 2
c$$$      end do
c$$$
c$$$      end if

      return
      end                       !blk_jac
