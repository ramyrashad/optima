C ********************************************************************
C ********************* OUTPUT VISCOUS VARIABLES   *******************
C ********************************************************************
c calling routine ioall
      subroutine viscout(jdim, kdim, iside, idir, q, x, y, xy,
     &     xyj, fmu,ifoil, iblk, jbegin, jend, kbegin, kend,
     &     iout1, iout2)

      implicit none
c
#include "../include/parms.inc"
#include "../include/common.inc"
c
      integer jdim,kdim,iside,idir,ifoil,iblk,jbegin,jend,kbegin,kend
      integer iout1,iout2,j,j1,j2,k,k1,k2,k3,k4,k5,kadd
      double precision cinf,alngth,tmp,amu,uinf2,r,r1,r2,rp1,rp2,rp3,rp4
      double precision u,up1,up2,rhoa,uf2,ustar,yplus,uplus,scis,tas
      double precision snorm,tauh,cf,tauw,etay,etax,xix,xiy,ueta,veta
      double precision uxi,vxi,up3,up4,v,vp1,vp2,vp3,vp4
      double precision q(jdim,kdim,4)
      double precision xy(jdim,kdim,4),xyj(jdim,kdim)
      double precision x(jdim,kdim),y(jdim,kdim)
      double precision fmu(jdim,kdim)

      double precision z(maxj),work(maxj,4)
      common/worksp/z,work
c
c     if idir=0 don't compute the viscous quantities
c                                     (used for inviscid walls)
c
      if (idir.eq.0) return
c     note: |idir| = element number, sign(idir) gives direction
c
c
c     viscous coefficent of friction calculation        
c     taken from P. Buning
c                                                                    
c     calculate the skin friction coefficient  (w => wall) 
c                                                                    
c      c  = tau   /  q    
c       f      w      inf 
c     
c      where
c     
c            tau  = mu*(du/dy-dv/dx)
c               w                   w
c     
c            q    =  1/2 * rhoinf * uinf * uinf
c             inf
c                                                                    
c            du/dy = (du/dxi * dxi/dy) + (du/deta * deta/dy) 
c            dv/dx = (dv/dxi * dxi/dx) + (dv/deta * deta/dx) 
c     
c     (definition from F.M. White, Viscous Fluid Flow, Mcgraw-Hill Inc.,
c     York,1974, p.50, but use freestream values instead of edge values.
c                                                                    
c                                                                    
c     for calculating cf, we need the coefficient of viscosity, mu. use
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1,rhoinf=1.

c     -set limits of integration
      j1 = jbegin
      j2 = jend
      if (iside.eq.1) k1 = kbegin
      if (iside.eq.3) k1 = kend
      if (iside.eq.2 .or. iside.eq.4) return
c
      cinf  = 1.
      alngth= 1.
c     -re already has fsmach scaling
      amu   = rhoinf*alngth/re
      uinf2 = fsmach**2
c
c     -set new limits
c
      k = k1
      if (iside.eq.1) kadd=1
      if (iside.eq.3) kadd=-1
c     
      k2 = k1 + kadd
      k3 = k2 + kadd
      k4 = k3 + kadd
      k5 = k4 + kadd
c
      tmp=float(kadd)/12.d0

      do 110 j=j1,j2
c       -xi direction    
        uxi=0.d0
        vxi=0.d0
c
c       -eta direction
        r  =1.d0/q(j,k1,1)
        rp1=1.d0/q(j,k2,1)
        rp2=1.d0/q(j,k3,1)
        rp3=1.d0/q(j,k4,1)
        rp4=1.d0/q(j,k5,1)
c
        u  =q(j,k1,2)*r
        up1=q(j,k2,2)*rp1
        up2=q(j,k3,2)*rp2
        up3=q(j,k4,2)*rp3
        up4=q(j,k5,2)*rp4
c
        v  =q(j,k1,3)*r
        vp1=q(j,k2,3)*rp1
        vp2=q(j,k3,3)*rp2
        vp3=q(j,k4,3)*rp3
        vp4=q(j,k5,3)*rp4
c
        if (iord.eq.4) then
          ueta= tmp*(-25.d0*u +48.d0*up1-36.d0*up2+16.d0*up3-3.d0*up4) 
          veta= tmp*(-25.d0*v +48.d0*vp1-36.d0*vp2+16.d0*vp3-3.d0*vp4) 
        else
c          if (lomet) then
c            ueta= up1-u
c            veta= vp1-v
c          else
            ueta= float(kadd)*(-3.*u + 4.*up1 - up2)*.5
            veta= float(kadd)*(-3.*v + 4.*vp1 - vp2)*.5
c          endif
        endif
c
c       -metric derivatives
        xix = xy(j,k1,1)                                             
        xiy = xy(j,k1,2)                                             
        etax = xy(j,k1,3)                                            
        etay = xy(j,k1,4)                                            
c
c       -compute tauw and cf
        tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))         
        cf= tauw/(.5d0*rhoinf*uinf2)

c       *************************************************
c       Compute u+ and y+ at the first point off the wall
c       *************************************************
c
c       -compute vorticity on body (xi derivatives are zero)
        tas = 0.5d0*abs(  (-3.d0*v+4.d0*vp1-vp2)*xy(j,k1,3)
     &                   -(-3.d0*u+4.d0*up1-up2)*xy(j,k1,4) )
c       
        scis = abs(xy(j,k1,3)*xy(j,k2,3)+xy(j,k1,4)*xy(j,k2,4))
        snorm = 1.d0/sqrt(scis)
        tauh = .5*(fmu(j,k1)+fmu(j,k2))*tas/re
        r1=q(j,k1,1)*xyj(j,k1)
        r2=q(j,k2,1)*xyj(j,k2)
        rhoa = .5*(r1+r2)
        uf2  = abs(tauh/rhoa) + 1.e-20
        ustar = sqrt(rhoa/r1)*(up1-u)
        yplus = sqrt(uf2)*re*snorm*rhoa/fmu(j,k1)                 
        uplus = ustar/sqrt(uf2)                               
c
        z(j) = cf
        work(j,1) = uplus
        work(j,2) = yplus
 110  continue
      if (matcharc2d .and. sngvalte) then
        z(j1)=.5*(z(j1)+z(j2))
        z(j2)=z(j1)
      endif

c     -output the results
      if (iout1.ne.0) then
         if (idir.gt.0) then
            do j=j1,j2,1
               write(iout1,801) ifoil,iblk,j,k,x(j,k1),y(j,k1),
     &              z(j),work(j,1),work(j,2)
            end do
         elseif (idir.lt.0) then
            do j=j2,j1,-1
               write(iout1,801) ifoil,iblk,j,k,x(j,k),y(j,k),z(j), 
     &              work(j,1),work(j,2)
            end do
         endif
      end if

      if (iout2.ne.0) then
         if (idir.gt.0) then
            do j=j1,j2,1
               write(iout2,802) x(j,k1),z(j),j
            end do
         elseif (idir.lt.0) then
            do j=j2,j1,-1
               write(iout2,802) x(j,k),z(j),j
            end do
         endif
      end if
      
 801  format(x,i2,2x,i3,2x,i3,2x,i3,5f12.6)
 802  format(2f14.6,i5)

      return
      end
