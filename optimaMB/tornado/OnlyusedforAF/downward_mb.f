c
c DOWNWARD SWEEP
c
c
      resid_e1=0.0
      resid_e2=0.0
      resid_k1=0.0
      resid_k2=0.0
      do j=jup,jlow,-1
         do k=kup,klow,-1
            denomk=1.-(dtm*ds(j,k))*(q11(j,k)+cx_k(j,k)+cy_k(j,k))
     $           /q(j,k,1)
            denome=1.-(dtm*ds(j,k))*(q22(j,k)+cx_e(j,k)+cy_e(j,k))
     $           /q(j,k,1)
            bbk(k)=-by_k(j,k)/denomk
            bbe(k)=-by_e(j,k)/denome
c
            aak(k)=-dy_k(j,k)/denomk
            aae(k)=-dy_e(j,k)/denome
c
            ddk(k)=q(j,k,1)/(dtm*ds(j,k))
            dde(k)=q(j,k,1)/(dtm*ds(j,k))
c
            cck(k)=dk_star(j,k)*q(j,k,1)/(dtm*ds(j,k))
            cce(k)=de_star(j,k)*q(j,k,1)/(dtm*ds(j,k))
         enddo
         call thomas(klow,kup,bbk,ddk,aak,cck)
         call thomas(klow,kup,bbe,dde,aae,cce)
         do k=kup,klow,-1
            resid_k1=resid_k1+(cck(k)-dk(j,k))**2
            resid_k2=resid_k2+(cck(k))**2
            dk(j,k)=cck(k)
            resid_e1=resid_e1+(cce(k)-de(j,k))**2
            resid_e2=resid_e2+(cce(k))**2
            de(j,k)=cce(k)
         enddo
      enddo
      resid_k1=sqrt(resid_k1)
      resid_k2=sqrt(resid_k2)
      resid_e1=sqrt(resid_e1)
      resid_e2=sqrt(resid_e2)
      resid_k=resid_k1/max(resid_k2,1.e-10)
      resid_e=resid_e1/max(resid_e2,1.e-10)
      residual=max(resid_k,resid_e)
