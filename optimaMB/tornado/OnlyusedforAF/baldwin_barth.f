C ********************************************************************
C ************** TURBULNT VISCOSITY **********************************
C ********************************************************************
c calling routine: etaexpl
      subroutine baldwin_barth(JMAX,KMAX,JBEGIN,JEND,KBEGIN,KEND,
     $   JLOW,JUP,KLOW,KUP,
     $   Q,TURMU,X,Y,xy,XYJ,DS,JTRANLO1,JTRANUP1,JTRANLO3,JTRANUP3,
     $   PRESS,VORT,TURRE,FNU,DAMP1M,damp,DAMP1,DAMP2,WORKX,WORKY,RAT,
c    $   AX,BX,CX,DX,EX,FX,AY,BY,CY,DY,EY,FY,GQ,GQN,
c removed ax,ay,ex,ey to save space --- they are not used
     $   BX,CX,DX,FX,BY,CY,DY,FY,GQN,GQ,
c     &   AA,AAN,SMIN1,SMIN3,smin,BND,FIRST,jlowte,jupte)
     &   AA,AAN,smin,BND,FIRST,jlowte,jupte)
c
c jlowte and jupte are used to adjust the distance wall functions 
c near the trailing edge 
C
C One Equation Turbulence Model
C
C VERSION 2.0 ( modified extensively for multi-block -- spring 1992) 
c               T. Nelson & P. Godin
C
C By Baldwin and Barth  NASA Ames R.C.
C Copyright 1990.
C
C All comments and other bad mouthing of this model should be directed
C to Timothy Barth, NASA Ames R.C., M.S. 202A-1, Moffett Field,CA
C (415)604-6740 
C 
#include "../include/common.inc"
C
      DIMENSION Q(JMAX,KMAX,4),TURMU(jMAX,kMAX),VORT(JMAX,KMAX)
      DIMENSION PRESS(JMAX,KMAX),XY(JMAX,KMAX,4),XYJ(JMAX,KMAX)
      DIMENSION FnU(JMAX,KMAX),DS(JMAX,KMAX)
      DIMENSION X(JMAX,KMAX),Y(JMAX,KMAX),TURRE(JMAX,KMAX)
      LOGICAL BND(4), FIRST
c
      dimension aa(0:jmax,0:kmax), aan(jmax,kmax)
c
c      DIMENSION SMIN1(jmax,kmax),SMIN3(jmax,kmax) ,smin(jmax,kmax)
      DIMENSION smin(jmax,kmax)

      DIMENSION
     >               GQ(0:JMAX,0:KMAX,2,2),
     >               GQN(JMAX,KMAX,2,2),
     >               DAMP1(JMAX,KMAX), DAMP1M(JMAX,KMAX),
     >               DAMP2(JMAX,KMAX), WORKX(KMAX,JMAX),
     >               WORKY(JMAX,KMAX),RAT(JMAX,KMAX),
     >               DAMP(jMAX,kMAX)

c     DIMENSION   AX(KMAX,JMAX),BX(KMAX,JMAX),CX(KMAX,JMAX),
c    >            DX(KMAX,JMAX),EX(KMAX,JMAX),FX(KMAX,JMAX),
c    >            AY(JMAX,KMAX),BY(JMAX,KMAX),CY(JMAX,KMAX),
c    >            DY(JMAX,KMAX),EY(JMAX,KMAX),FY(JMAX,KMAX)
c removed ax,ay,ex,ey to save space --- they are not used
      DIMENSION   BX(KMAX,JMAX),CX(KMAX,JMAX),
     >            DX(KMAX,JMAX),FX(KMAX,JMAX),
     >            BY(JMAX,KMAX),CY(JMAX,KMAX),
     >            DY(JMAX,KMAX),FY(JMAX,KMAX)
c
cerror
      if(periodic)then
         print *,'  TURBULENCE MODEL NOT SET UP FOR PERIODIC FLOW'
         STOP
      endif
cerror
      IF(TURBULNT)THEN
c ********************************************************************
c set up the various constants required for the turbulence modeling
c ********************************************************************
        AKARMAN = .41
        CMU     = .09
        C1E     = 1.20
        C2E     = 2.00
        SIGE    = AKARMAN**2/((C2E-C1E)*SQRT(CMU))
        SIGR    =  .3*SIGE
ctest         sigr    = sige
        CR1     =  8.0
ctest         CR1     =  0.0
        CR2     =  .89
        APLUS1  = 26.
c ********************************************************************
c note: APLUS2 is not used since D2 is not used
c ********************************************************************
        APLUS2  = 10.
c Boundary condition at inflow for Reynolds number 
c       RETINF  = .1
c number of iterations NNIT
        NNIT    =  1
        DTM     =  DT
        IF(JACDT.NE.0)DTM     = 50.*DT
        IF(DTM.GT.100)DTM     = 100.
c constants for Sutherland's law  (TINF=460 Rankine)
        C2B=198.6/TINF 
        C2BP = C2B + 1.
C
        IF (FIRST) THEN
          FIRST=.FALSE.
          if(bnd(1)) NNIT  = 100
          if(bnd(3)) NNIT  = 100
cdb       if(bnd(1)) NNIT  = 10
cdb       if(bnd(3)) NNIT  = 10
c ********************************************************************
C     COMPUTE GENERALIZED DISTANCE FUNCTIONS FROM EACH WALL
C                    3
C             _______________
C             |             |
C             |             |
C         2   |             |  4
C             |             |
C             |             |
C             ---------------
c                    1 
c   the goal of this section is to compute the functions
c            SMIN(J,K) the distance from grid point J,K to the airfoil
c                      surface
ctn ******************************************************************
ctn forget about checking the whole airfoil and interpolating 
ctn just assume the grid is orthogonal at the boundary 
ctn for the damping function.
ctn ******************************************************************
c initializing the variables

c            05/04/2000
c            -Stan De Rango ... rewriting this part to conserve memory
csd          IF (BND(1)) THEN
csd            DO K=KBEGIN,KEND
csdc           DO J=JBEGIN,JEND
csd            do j=jbegin,jlowte-1
csd              smin1(j,k) = 10000.
csd            enddo
csd            DO J=Jlowte,Jupte
csd              XPT1    = X(J,KBEGIN) 
csd              YPT1    = Y(J,KBEGIN)
csd              SMIN1(J,K) = SQRT((X(J,K)-XPT1)**2+(Y(J,K)-YPT1)**2)
csd            ENDDO
csd            do j=jupte+1,jend
csd              smin1(j,k) = 10000.
csd            enddo
csd            ENDDO
csd          ELSE
csd            do k=kbegin,kend
csd            do j=jbegin,jend
csd              SMIN1(J,K) = 10000.
csd            ENDDO
csd            ENDDO
csd          ENDIF
csd          IF (BND(3)) THEN
csd            DO K=KBEGIN,KEND
csd            do j=jbegin,jlowte-1
csd              smin3(j,k) = 10000.
csd            enddo
csd            DO J=Jlowte,jupte
csd              XPT3   = X(J,KEND) 
csd              YPT3   = Y(J,KEND) 
csd              SMIN3(J,K) = SQRT((X(J,K)-XPT3)**2+(Y(J,K)-YPT3)**2)
csd            ENDDO
csd            do j=jupte+1,jend
csd              smin3(j,k) = 10000.
csd            enddo
csd            ENDDO
csd          ELSE
csd            do k=kbegin,kend
csd            do j=jbegin,jend
csd              SMIN3(J,K) = 10000.
csd            ENDDO
csd            ENDDO
csd          ENDIF
c
c
          do k=kbegin,kend
          do j=jbegin,jend
            smin(j,k)=10000.
          enddo
          enddo
c
          do k=kbegin,kend
          do j=jlowte,jupte
            smin1=10000.
            smin3=10000.
            if (bnd(1)) then
              xpt   = x(j,kbegin) 
              ypt   = y(j,kbegin)
              smin1 = sqrt((x(j,k)-xpt)**2+(y(j,k)-ypt)**2)
            endif
            if (bnd(3)) then
              xpt   = x(j,kend) 
              ypt   = y(j,kend) 
              smin3 = sqrt((x(j,k)-xpt)**2+(y(j,k)-ypt)**2)
            endif
            smin(j,k)=min(smin1,smin3)
          enddo
          enddo
c
c         -this ensures that we pick the closest wall if block touches
c          two different elements
c          do 8 k=kbegin,kend
c          do 8 j=jbegin,jend
c            smin(j,k)=min(smin1(j,k),smin3(j,k))
c 8        continue

c?********************************************************************
c?initializing the two types of JACOBIANS
c?********************************************************************
c?initializing the two types of JACOBIANS
c?      AA(J,K) regular jacobian
c?             =dY/deta*dX/dxi-dY/dxi*dX/deta
c?      AAN(J,K) smoothed jacobian 
c?             = AA(J,K)+AA(J-1,K)+AA(J,K-1)+AA(J-1,K-1)
c?          (i.e. weighted average of the 9 points centered on J,K)
c?          (i.e. 1 2 1
c?                2 4 2
c?                1 2 1)
c?********************************************************************
ctn changed     initialization      0:kmax, 0:jmax for aa
ctn                                 1:kmax, 1:kmax for aan
ctn             was                 kbegin:kend, jbegin:kend for both !
          do k=0,kmax
          do j=0,jmax
            aa(j,k)= 0.0
          enddo
          enddo
ctn
          DO K=1,kmax
            DO J=1,jmax
              AAN(J,K) = 0.0
            ENDDO
          ENDDO
c
          DO K=KBEGIN,KEND-1
            DO J=JBEGIN,JEND
c compute the dx+(j,k) and dy+(j,k)
              DXA = X(J,K+1)-X(J,K)
              DYA = Y(J,K+1)-Y(J,K)
              SNX = DYA
              SNY = -DXA
              AA(J-1,K) = AA(J-1,K) + SNX*.5*(X(J,K+1)+X(J,K)) 
              AA(J,K)   = AA(J,K)   - SNX*.5*(X(J,K+1)+X(J,K)) 
            ENDDO
          ENDDO
c
          DO K=KBEGIN,KEND
            DO J=JBEGIN,JEND-1
              DXA = X(J+1,K)-X(J,K)
              DYA = Y(J+1,K)-Y(J,K)
              SNX = DYA
              SNY = -DXA
              AA(J,K-1) = AA(J,K-1) - SNX*.5*(X(J+1,K)+X(J,K)) 
              AA(J,K)   = AA(J,K)   + SNX*.5*(X(J+1,K)+X(J,K)) 
            ENDDO
          ENDDO
c
          DO K=KBEGIN,KEND-1
            DO J=JBEGIN,JEND-1
              AAN(J,K) = AAN(J,K) + AA(J,K)
              AAN(J+1,K) = AAN(J+1,K) + AA(J,K)
              AAN(J,K+1) = AAN(J,K+1) + AA(J,K)
              AAN(J+1,K+1) = AAN(J+1,K+1) + AA(J,K)
            ENDDO
          ENDDO
c
c in the wake make the smoothed jacobian symmetric with respect to the 
c eta=0 or body surface line
c
c            IF(JTAIL2 .NE. JMAX)THEN
c               K = 1
c               DO J=1,JTAIL1
c                  JJ = JMAX - J + 1
c                  TEMP1 = AAN(J,K)
c                  TEMP2 = AAN(JJ,K)
c                  AAN(J,K) = TEMP1 + TEMP2
c                  AAN(JJ,K)= TEMP2 + TEMP1
c               ENDDO
c            ENDIF
c 
c TURRE=NU*RT (i.e. the field variable)
c initialize TURRE=0.0 on body surface and 0.1 everywhere else
c (i.e the initial boundary condition)
c           
          IF(.NOT.RESTART)THEN
            DO K=KBEGIN,KEND
              DO J=JBEGIN,JEND
                TURRE(J,K) =  RETINF
              ENDDO
            ENDDO
          ELSE
            NNIT = 1
          ENDIF
c
C   solid bc's put in init  ---- were for every iteration
c   April 1 /93
          DO J=JLOW,JUP
            IF (BND(1)) TURRE(J,KBEGIN)=0.0
            IF (BND(3)) TURRE(J,KEND)=0.0
          ENDDO
          DO K=KLOW,KUP
            IF (BND(2)) TURRE(JBEGIN,K)=0.0
            IF (BND(4)) TURRE(JEND,K)=0.0
          ENDDO
C   solid bc's put in init  ---- were for every iteration
c   April 1 /93
        ENDIF            !endif for if(first)
c ********************************************************************
c  END OF INITIALIZATION
c ********************************************************************
C ********************************************************************
C  NOW THE MODEL    
C ********************************************************************
        DO K=KBEGIN,KEND
          DO J=JBEGIN,JEND
c define the physical quantities from the state variables
            RHO = Q(J,K,1)*XYJ(J,K)
            U   = Q(J,K,2)/Q(J,K,1)
            V   = Q(J,K,3)/Q(J,K,1)
            PRES= GAMI*(Q(J,K,4)*XYJ(J,K) - .5*RHO*(U**2+V**2))
c redefine the Q array as the physical quantities
            Q(J,K,1) = RHO
            Q(J,K,2) = U
            Q(J,K,3) = V
            Q(J,K,4) = PRES
c TT is the speed of sound 
            TT = GAMMA*Q(J,K,4)/Q(J,K,1)
c Sutherland's law defining viscosity
            FNU(J,K) = C2BP*TT*SQRT(TT)/( C2B+TT)/Q(J,K,1)
          ENDDO
        ENDDO
C ********************************************************************
c compute the velocity gradients
C ********************************************************************
        CALL GRADV(JMAX,KMAX,JBEGIN,JEND,KBEGIN,KEND,
     >             X,Y,AA,AAN,Q,GQ,GQN)
C ********************************************************************
c  calculate the damping functions for each wall
C ********************************************************************
	DO J=JBEGIN,JEND
          DO K=KBEGIN,KEND
            DAMP(J,K)=1.0
            DAMP1(J,K)=1.0
            DAMP1M(J,K)=1.0
            RAT(J,K)=1.0
            DAMP2(J,K)=1.0
           ENDDO
        ENDDO
        RATT= C1E/C2E
C STEP 1.    Calculate the damping factors at 1/2 points
        IF(BND(1).OR.BND(3))THEN 
          DO J=JBEGIN,JEND
          DO K=KBEGIN,KEND-1
            YPLS1=1.E9
            YPLS3=1.E9
            YPLSM1=1.E9
            YPLSM3=1.E9
            IF (BND(1)) THEN
ctn  changed to gqn because gq doesn't have j=jend !!!
ctn  shouldn't it be gqn not gq ???? 
              SSW    = ABS(GQN(J,KBEGIN,1,2) - GQN(J,KBEGIN,2,1))
c       if(k.eq.kbegin) write(99,*) j, 
c    &   sign(1.,X(J+1,K)-X(J,K))*(GQN(J,KBEGIN,1,2)-GQN(J,KBEGIN,2,1))
ctn  shouldn't it be gqn not gq ???? 
              WNU    = FNU(J,KBEGIN)
              RHOW   = Q(J,KBEGIN,1) 
c Ra=sqrt(Re*(|w|/nu+epsilon*M**2)=y/nu*sqrt(tau.w/rho.w)*Re^.5
c   =u.tau/nu*Re^.5=y+/y*Re^.5
c where u.tau=friction velocity
               RA      = SQRT( RE*(SSW/WNU + 1.E-10*FSMACH**2))
c y+=u.tau*y/nu*Re^.5 (i.e scaled with the Re number)  
c where: y(j,k)=smin(j,k)
csd               YPLS1      = RA*SMIN1(J,K)
               YPLS1      = RA*SMIN(J,K)
c insures a minimal y+ of .0001
               YPLS1      = MAX(YPLS1,.0001)
csd               YPLSM1     = RA*.5*(SMIN1(J,K)+SMIN1(J,K+1))
               YPLSM1     = RA*.5*(SMIN(J,K)+SMIN(J,K+1))
             ENDIF
             IF (BND(3)) THEN
ctn  gq is at the 1/2 points so the wall is at kend-1 on side 3
ctn  changed to gqn because gq doesn't have j=jend !!!
ctn  changed back to kend because gqn does have kend !!
               SSW    = ABS(GQN(J,KEND,1,2) - GQN(J,KEND,2,1))
ctn  shouldn't it be gqn not gq ???? 
               WNU    = FNU(J,KEND)
               RHOW   = Q(J,KEND,1) 
c Ra=sqrt(Re*(|w|/nu+epsilon*M**2)=y/nu*sqrt(tau.w/rho.w)*Re^.5
c   =u.tau/nu*Re^.5=y+/y*Re^.5
c where u.tau=friction velocity
               RA      = SQRT( RE*(SSW/WNU + 1.E-10*FSMACH**2))
c y+=u.tau*y/nu*Re^.5 (i.e scaled with the Re number)  
c where: y(j,k)=smin(j,k)
csd               YPLS3      = RA*SMIN3(J,K)
               YPLS3      = RA*SMIN(J,K)
c insures a minimal y+ of .0001
               YPLS3      = MAX(YPLS3,.0001)
csd               YPLSM3     = RA*.5*(SMIN3(J,K)+SMIN3(J,K+1))
               YPLSM3     = RA*.5*(SMIN(J,K)+SMIN(J,K+1))
             ENDIF
c YPLSM is an average in the eta direction of the two neighbouring YPLS
c
c temporarily put in a change to aplus1 and aplus2
c            if(bnd(1)) then
c              if( (sign(1.,X(J+1,kbegin)-X(J,kbegin)) 
c    &            *(GQN(J,KBEGIN,1,2)-GQN(J,KBEGIN,2,1))).lt.0.0) then
c                aplus1 = 1.0
c                aplus2 = 1.0
c              else 
c                aplus1 = 26.0
c                aplus2 = 10.0
c              endif
c            else
c              aplus1 = 26.0
c              aplus2 = 10.0
c            endif
c
             IF (YPLS1.LE.YPLS3) THEN
               YPLS=YPLS1
               YPLSM=YPLSM1
             ELSEIF (YPLS1.GT.YPLS3) THEN
               YPLS=YPLS3
               YPLSM=YPLSM3
             ENDIF
             EXP1      =       EXP(-(YPLS/APLUS1))
             EXP2      =       EXP(-(YPLS/APLUS2))
             DAMP1(J,K)  = (1.-EXP1)*(1.-EXP2)
             DAMP(J,K) = DAMP1(J,K)
             IF (YPLS1.LT.YPLS3) THEN
                IF ((J.LE.JTRANUP1).AND.(J.GE.JTRANLO1)) DAMP(J,K)=0.0
                IF (J.EQ.(JTRANLO1-1)) 
     >             DAMP(J,K)=0.01*DAMP1(J,K)
                IF (J.EQ.(JTRANLO1-2))
     >             DAMP(J,K)=.25*DAMP1(J,K)
                IF (J.EQ.(JTRANUP1+1))
     >             DAMP(J,K)=0.01*DAMP1(J,K)
                IF (J.EQ.(JTRANUP1+2))
     >             DAMP(J,K)=0.25*DAMP1(J,K)
             ELSEIF (YPLS3.LT.YPLS1) THEN
                IF ((J.LE.JTRANUP3).AND.(J.GE.JTRANLO3)) DAMP(J,K)=0.0
                IF (J.EQ.(JTRANLO3-1))
     >             DAMP(J,K)=0.01*DAMP1(J,K)
                IF (J.EQ.(JTRANLO3-2))
     >             DAMP(J,K)=.25*DAMP1(J,K)
                IF (J.EQ.(JTRANUP3+1))
     >             DAMP(J,K)=0.01*DAMP1(J,K)
                IF (J.EQ.(JTRANUP3+2))
     >             DAMP(J,K)=0.25*DAMP1(J,K)
             ENDIF
             DAMP1M(J,K) = (1. - EXP(-(YPLSM/APLUS1)))*
     >                       (1. - EXP(-(YPLSM/APLUS2)))

             DDY = EXP1/APLUS1*(1.-EXP2)
     >             + EXP2/APLUS2*(1.-EXP1)
             DD  = DAMP1(J,K)
             SDD = SQRT(DD)
c  DAMP2(j,k)=f2+(j,k), the damping function
             DAMP2(J,K) = 
     >              RATT + (1.-RATT)*( 1./(AKARMAN*YPLS) + DD )*
     >              (SDD + YPLS*DDY/SDD)
          ENDDO
c set bnd conditions for solid wall on side 3
          IF (BND(3)) THEN
            DAMP(J,KEND) = 0
            DAMP1 (J,KEND) = 0
            DAMP1M(J,KEND) = 0
            DAMP2(J,KEND) = 0.781
          ENDIF
          ENDDO
        endif
c ******************************************************************
c  RAT initializing, (RAT has the same function as f3 in the paper)
c ******************************************************************
         DO J=JLOW,JUP
            DO K=KLOW,KUP
               KP1 = K+1
               KM1 = K-1
               XY3P     = .5*(XY(J,K,3)+XY(J,KP1,3))
               XY4P     = .5*(XY(J,K,4)+XY(J,KP1,4))
               TTP      =  (XY3P*XY(J,K,3)+XY4P*XY(J,K,4))
               XY3M     = .5*(XY(J,K,3)+XY(J,KM1,3))
               XY4M     = .5*(XY(J,K,4)+XY(J,KM1,4))
               TTM      =  (XY3M*XY(J,K,3)+XY4M*XY(J,K,4))
               RAT(J,K) =  TTP*(TURRE(J,KP1)- TURRE(J,K))
     >                    -TTM*(TURRE(J,K)  - TURRE(J,KM1))
            ENDDO
         ENDDO
         DO K=KLOW,KUP
            DO J=JLOW,JUP
               JP1 = J+1
               JM1 = J-1
               XY1P     = .5*(XY(J,K,1)+XY(JP1,K,1))
               XY2P     = .5*(XY(J,K,2)+XY(JP1,K,2))
               TTP      =  (XY1P*XY(J,K,1)+XY2P*XY(J,K,2))
               XY1M     = .5*(XY(J,K,1)+XY(JM1,K,1))
               XY2M     = .5*(XY(J,K,2)+XY(JM1,K,2))
               TTM      =  (XY1M*XY(J,K,1)+XY2M*XY(J,K,2))
               RAT(J,K) =  RAT(J,K) + TTP*(TURRE(JP1,K)-TURRE(J,K))
     >              -TTM*(TURRE(J,K) -TURRE(JM1,K))
c up to here RAT=LAPLACIAN(nu*Rt) 
c (note: it's scaled using metrics and RE number)                   
            ENDDO
         ENDDO
         DO K=KBEGIN,KEND
            DO J=JBEGIN,JEND
               RRAT     = ABS(RAT(J,K))/RE
               VORT(j,k)     = ABS(GQN(J,K,1,2)-GQN(J,K,2,1))
ctn min added to prevent underflow for lousy compilers
               RAT(J,K)=1.+CR1*
     .                  exp(-min(CR2*VORT(j,k)/(RRAT+1.e-6),1000.))
c RAT=1.0+8.0exp((-.89*w*Rt)/|laplacian(nu*Rt)|)
            ENDDO
         ENDDO
c ******************************************************************
C             NOW SOLVE THE EQUATION
c ******************************************************************
         DO 500 NIT=1,NNIT
            DO K=KBEGIN,KEND
               DO J=JBEGIN,JEND
c setup the initial value of eddy viscosity
                  TURMU(J,K) = CMU*DAMP1(J,K)*RAT(J,K)*TURRE(J,K)
               ENDDO
            ENDDO
C F_ETA_ETA VISCOUS TERMS
C the diffusive/antidiffusive terms
C (i.e. (nu+nu.t/sige)*laplacian(nu*Rt)-1/sige*del(nu.t)*del(nu*Rt))
c (considering the eta derivative component only here)
            DO J=JLOW,JUP
               DO K=kLOW,KUP
                  KP1 = K+1
                  KM1 = K-1
                  XY3P     = .5*(XY(J,K,3)+XY(J,KP1,3))
                  XY4P     = .5*(XY(J,K,4)+XY(J,KP1,4))
                  TTP      =  (XY3P*XY(J,K,3)+XY4P*XY(J,K,4))
                  XY3M     = .5*(XY(J,K,3)+XY(J,KM1,3))
                  XY4M     = .5*(XY(J,K,4)+XY(J,KM1,4))
                  TTM      =  (XY3M*XY(J,K,3)+XY4M*XY(J,K,4))
c note again the scaling with the metrics and the Reynolds number 
                  CNUD=( FNU(J,K)+TURMU(J,K)/SIGR
     >                   + CMU*DAMP1(J,K)*TURRE(J,K)/SIGE )/RE
c ????????????????????????????????????????????????????????????????????
c this term should read 2.0*(FNU(J,K)+TURMU(J,K)/SIGE) (SEE PAPER P. 17)
c ????????????????????????????????????????????????????????????????????
c note: CDP and CDM are for the diffusive term (i.e. laplacian term)
                  CDP       =    TTP*CNUD
                  CDM       =    TTM*CNUD
                  TREP =.5*(TURRE(J,KP1)+TURRE(J,K))
                  TREM =.5*(TURRE(J,KM1)+TURRE(J,K))
c note: CAP & CAM are for the anti-diffusive term (i.e. del(nu*Rt) term)
                  CAP  =  CMU*TREP*DAMP1M(J,K  )*TTP/(SIGE*RE)
                  CAM  =  CMU*TREM*DAMP1M(J,KM1)*TTM/(SIGE*RE)
c the equations are solved using upwinding for spatial discretization
c                NU*DT d                DT  d               PHI   
c and dQ^(n+1) = ----- --(dQ^(n+1)) + ----- -- (Q^(n+1)) + ----- dQ^n
c                1+PHI dt             1+PHI dt             1+PHI
c for the time discretization where NU=1 PHI=0
                  BY(J,K)   =                        MIN(-CDM + CAM,0.0)
                  CY(J,K)   =  MAX( CDP - CAP,0.0) + MAX( CDM - CAM,0.0)
                  DY(J,K)   =  MIN(-CDP + CAP,0.0)
                  FY(J,K)   = - BY(J,K)*TURRE(J,KM1)
     >                        - CY(J,K)*TURRE(J,K  )
     >                        - DY(J,K)*TURRE(J,KP1)
               ENDDO
            ENDDO
C ADVECTIVE TERMS IN ETA
c
c                    d (nu*Rt)
C     i.e.  the V.c* ---------     term
c                     d(ETA)
c
            DO J=JLOW,JUP
               DO K=KLOW,KUP
c UU= contravariant component of velocity perpendicular with 
c the airfoil=V.c
                  UU = (XY(J,K,3)*Q(J,K,2)+XY(J,K,4)*Q(J,K,3))
c (note: the upwinding) 
                  SGNU = SIGN(1.,UU)
                  APP  = .5*(1.+SGNU)
                  APM  = .5*(1.-SGNU)
                  FY(J,K) = FY(J,K) - 
     >             UU*( APP*(TURRE(J,K)-TURRE(J,K-1))
     >                 +APM*(TURRE(J,K+1)-TURRE(J,K)) )
                  BY(J,K)   = BY(J,K)   - UU*APP
                  CY(J,K)   = CY(J,K)   + UU*(APP-APM)
                  DY(J,K)   = DY(J,K)   + UU*APM
               ENDDO
            ENDDO
C     E_XI_XI VISCOUS TERMS
C     (see comments for the F_ETA_ETA term)
            DO K=KLOW,KUP
               DO J=JLOW,JUP
                 JP1 = J+1
                 JM1 = J-1
                 XY1P     = .5*(XY(J,K,1)+XY(JP1,K,1))
                 XY2P     = .5*(XY(J,K,2)+XY(JP1,K,2))
                 TTP      =  (XY1P*XY(J,K,1)+XY2P*XY(J,K,2))
                 XY1M     = .5*(XY(J,K,1)+XY(JM1,K,1))
                 XY2M     = .5*(XY(J,K,2)+XY(JM1,K,2))
                 TTM      =  (XY1M*XY(J,K,1)+XY2M*XY(J,K,2))
                 CNUD=( FNU(J,K)+TURMU(J,K)/SIGR
     >                + CMU*DAMP1(J,K)*TURRE(J,K)/SIGE )/RE
                 CDP       =    TTP*CNUD
                 CDM       =    TTM*CNUD
                 TREP =.5*(TURRE(JP1,K)+TURRE(J,K))
                 TREM =.5*(TURRE(JM1,K)+TURRE(J,K))
                 CAP=CMU*TREP*.5*(DAMP1(J,K)+DAMP1(JP1,K))*TTP/(SIGE*RE)
                 CAM=CMU*TREM*.5*(DAMP1(JM1,K)+DAMP1(J,K))*TTM/(SIGE*RE)
                 BX(K,J)   =                        MIN(-CDM + CAM,0.0)
                 CX(K,J)   =  MAX( CDP - CAP,0.0) + MAX( CDM - CAM,0.0)
                 DX(K,J)   =  MIN(-CDP + CAP,0.0)
                 FY(J,K)   =  FY(J,K) - BX(K,J)*TURRE(JM1,K)
     >                - CX(K,J)*TURRE(J,K  )
     >                - DX(K,J)*TURRE(JP1,K) 
               ENDDO
            ENDDO
C ADVECTIVE TERMS IN XI
C (see comments for the advective terms in ETA)
            DO K=KLOW,KUP
               DO J=JLOW,JUP
                  UU = (XY(J,K,1)*Q(J,K,2)+XY(J,K,2)*Q(J,K,3))
                  SGNU = SIGN(1.,UU)
                  APP  = .5*(1.+SGNU)
                  APM  = .5*(1.-SGNU)
                  FY(J,K)= FY(J,K) - 
     >              UU*(APP*(TURRE(J,K)-TURRE(J-1,K))
     >                 +APM*(TURRE(J+1,K)-TURRE(J,K)) )
                  BX(K,J)   = BX(K,J)   - UU*APP
                  CX(K,J)   = CX(K,J)   + UU*(APP-APM)
                  DX(K,J)   = DX(K,J)   + UU*APM
               ENDDO
            ENDDO
c the last term to include is the production term
c the (ce1*f2-ce1)*sqrt(nu*RT*P) term
            DO K=KLOW,KUP
              KM1 = K - 1
              DO J=JLOW,JUP
                ux = gqn(j,k,1,1)
                uy = gqn(j,k,1,2)
                vx = gqn(j,k,2,1)
                vy = gqn(j,k,2,2)
c SS=sqrt(P/nu.t)
                SS = SQRT(ABS( 
     >               2.*(ux**2+vx*uy+vy**2)+uy**2+vx**2
     >               -.666666*(ux**2 + vy**2)))
c               SS = ABS(UY-VX)
c
c TT=(ce2*f2-ce1)*sqrt(P) (note: P contains a sqrt(nu*RT) also)
c ????????????????????????????????????????????????????????????????????
c shouldn't RAT be replaced by SQRT(RAT) 
c ????????????????????????????????????????????????????????????????????
                TT=(C2E*DAMP2(J,K)-C1E)*SQRT(CMU*DAMP(J,K))*SS*RAT(J,K)
c the a,b,c and f components of the tridiagonal are finally complete
c and scaled with the spatially varying DT=DS to obtain the final form
c (note: the +1 results fromthe LHS of (i.e. Q.(n+1)=Q.n+DT*Q'.(n+1))
                FACT = DS(J,K)*DTM
                BX(K,J) = BX(K,J)*FACT
                CX(K,J) = CX(K,J)*FACT + 1.
                DX(K,J) = DX(K,J)*FACT
                BY(J,K) = BY(J,K)*FACT
                CY(J,K) = CY(J,K)*FACT + 1.
                DY(J,K) = DY(J,K)*FACT
c (note: TT only contributes to the term at the nth time level)
                FY(J,K) = (FY(J,K)+TT*TURRE(J,K))*FACT
              ENDDO
            ENDDO
c solves the uncoupled system for the eta-direction first 
            CALL TRIV(Jmax,Kmax,JLOW,JUP,KLOW,KUP,
     $                WORKY,BY,CY,DY,FY)
            DO K=KLOW,KUP
              DO J=JLOW,JUP
c the FX matrix is the transpose of the FY matrix
c note: this results of matrix splitting in X and Y direction
                 FX(K,J) = FY(J,K)
              ENDDO
           ENDDO
c solves for the xi-direction
           CALL TRIV(Kmax,Jmax,KLOW,KUP,JLOW,JUP,
     $               WORKX,BX,CX,DX,FX)
           NEGN = 0
           SUMN = 0.0
           DO K=KLOW,KUP
              DO J=JLOW,JUP
c computing the resultant state variable from fx=d(mu*Rt)
c note the transpose of the resultant matrix is used
                 TURRE(J,K) = TURRE(J,K) + FX(K,J)
c                IF(TURRE(J,K).LT. 1.e-12)THEN
                 IF(TURRE(J,K).LT. 1.e-17)THEN
                    NEGN        =   NEGN + 1
c note: avoid negative state variables
c                   TURRE(J,K)  =    1.e-12
                    TURRE(J,K)  =    1.e-17
                    FX(K,J)     =    0.0
                 ENDIF
                 SUMN         =   SUMN + (FX(K,J))**2
              ENDDO
           ENDDO
c compute the RMS of the residual of (nu*Rt)
           SUMN = SQRT(SUMN)/FLOAT((JMAX-1)*(KMAX-1))
c
c Boundary conditions are applied with far-field b.c's 
c Don't apply any in this subroutine
c  
c TMAX is the maximum of the state variable in the whole domain
c          
           TMAX = 0.0
           DO K=KBEGIN,KEND-1
              DO J=JBEGIN,JEND
                 TMAX = MAX(TMAX,TURRE(J,K))
c
c finally set the eddy viscosity (mu.t) array
c
c ********************************************************************
c where's D2 or RAT in the equation for eddy viscosity 
c ********************************************************************
c
                 if (nnit.ne.1) then
                   TURMU(J,K) = 
     >                   CMU*DAMP1M(J,K)*.5*( 
     >                   TURRE(J,K)*Q(J,K,1)
     >                   + TURRE(J,K+1)*Q(J,K+1,1))
                 endif
              ENDDO
           ENDDO

ctn        write(49,'(A,e12.6,A,e12.6)')
ctn  $          ' RESID RE_T = ',SUMN,' MAX Rt = ',TMAX

 500   CONTINUE
c
c reset the state variables as they were 
c
       DO K=KBEGIN,KEND
          DO J=JBEGIN,JEND
             RHO = Q(J,K,1)
             U   = Q(J,K,2)
             V   = Q(J,K,3)
             PRES= Q(J,K,4)
             Q(J,K,1) = RHO/XYJ(J,K)
             Q(J,K,2) = Q(J,K,1)*u
             Q(J,K,3) = Q(J,K,1)*v
             Q(J,K,4) = (PRES/GAMI + .5*rho*(u**2+v**2))/XYJ(J,K)
c
c  form mu from nu
c
             fnu(j,k) = rho*fnu(j,k)
          ENDDO
       ENDDO
      ENDIF
c
      RETURN
      END
C====================================================================
      SUBROUTINE GRADV(JMAX,KMAX,JBEGIN,JEND,KBEGIN,KEND,
     >                X,Y,AA,AAN,Q,GQ,GQN)
c
c     compute the velocity gradients U.x V.x U.y V.y
C====================================================================
      DIMENSION AA(0:JMAX,0:KMAX),AAN(JMAX,KMAX)
      DIMENSION Q(JMAX,KMAX,4)
      DIMENSION X(JMAX,KMAX),Y(JMAX,KMAX)
      DIMENSION GQ(0:JMAX,0:KMAX,2,2),GQN(JMAX,KMAX,2,2)
c
c   note the zero starting index in the dimensions
c
c   note n=1 signifies the U component of the velocity vector
c        n=2 signifies the V component of the velocity vector
c
c
c   note the last index in the GQ array signifies with respect to which
c   coordinate the derivatives are:
c                          1 - wrt the xi coordinate
c                          2 - wrt the eta coordinate
c
c   initializing the velocity gradient vector GQ
c       and the smoothed gradient vector GQN
c       GQN(j,k)=GQ(j,k)+GQ(j-1,k)+GQ(j,k-1)+GQ(j-1,k-1)
c       weighted around a 9 point square (i.e.  1 2 1 
c                                               2 4 2   
c                                               1 2 1)
ctn changed initialization to 0:kmax, 0:jmax
ctn                from       kbegin:kend, jbegin,jend   } missed 1 !!!
c
      DO 10 N=1,2
      DO 10 K=0,KMAX
      DO 10 J=0,JMAX
       GQ(J,K,N,1) = 0.0
       GQ(J,K,N,2) = 0.0
 10   CONTINUE
      DO 15 N=1,2
      DO 15 K=1,KMAX
      DO 15 J=1,JMAX
        GQN(J,K,N,1) = 0.0
        GQN(J,K,N,2) = 0.0
 15   CONTINUE
c
c   compute U.x(j,k)=(Y.eta*U.xi-Y.xi*U.eta)/J
c           U.y(j,k)=(X.xi*U.eta-X.eta*U.xi)/J
c           etc.
c
      DO 20 NN=2,3
      N = NN-1
      DO 20 K=KBEGIN,KEND-1
      DO 20 J=JBEGIN,JEND
      DX = X(J,K+1)-X(J,K)
      DY = Y(J,K+1)-Y(J,K)
      SNX =  DY
      SNY = -DX
      GQ(J-1,K,N,1) = GQ(J-1,K,N,1) + SNX*.5*(Q(J,K+1,NN)+Q(J,K,NN)) 
      GQ(J,K,N,1)   = GQ(J,K,N,1)   - SNX*.5*(Q(J,K+1,NN)+Q(J,K,NN)) 
      GQ(J-1,K,N,2) = GQ(J-1,K,N,2) + SNY*.5*(Q(J,K+1,NN)+Q(J,K,NN)) 
      GQ(J,K,N,2)   = GQ(J,K,N,2)   - SNY*.5*(Q(J,K+1,NN)+Q(J,K,NN)) 
 20   CONTINUE

      DO 30 NN=2,3
      N = NN-1
      DO 30 K=KBEGIN,KEND
      DO 30 J=JBEGIN,JEND-1
      DX = X(J+1,K)-X(J,K)
      DY = Y(J+1,K)-Y(J,K)
      SNX =  DY
      SNY = -DX
      GQ(J,K-1,N,1) = GQ(J,K-1,N,1) - SNX*.5*(Q(J+1,K,NN)+Q(J,K,NN)) 
      GQ(J,K,N,1)   = GQ(J,K,N,1)   + SNX*.5*(Q(J+1,K,NN)+Q(J,K,NN)) 
      GQ(J,K-1,N,2) = GQ(J,K-1,N,2) - SNY*.5*(Q(J+1,K,NN)+Q(J,K,NN)) 
      GQ(J,K,N,2)   = GQ(J,K,N,2)   + SNY*.5*(Q(J+1,K,NN)+Q(J,K,NN)) 
 30   CONTINUE
    
      DO 90 M=1,2
      DO 90 N=1,2
      DO 90 K=KBEGIN,KEND-1
      DO 90 J=JBEGIN,JEND-1
      GQN(J,K,N,M) = GQN(J,K,N,M) + GQ(J,K,N,M)
      GQN(J+1,K,N,M) = GQN(J+1,K,N,M) + GQ(J,K,N,M)
      GQN(J,K+1,N,M) = GQN(J,K+1,N,M) + GQ(J,K,N,M)
      GQN(J+1,K+1,N,M) = GQN(J+1,K+1,N,M) + GQ(J,K,N,M)
 90   CONTINUE
c
c  make GQN symmetric with respect to the eta=0 line or airfoil surface
c  in the wake by adding top half to bottom half and vise-versa
c
c      if(jtail2 .ne. jmax)THEN
c      K = 1
c      DO 95 N=1,2
c      DO 95 J=1,JTAIL1
c      JJ = JMAX - J + 1
c      TEMP1 = GQN(J,K,N,1)
c      TEMP2 = GQN(JJ,K,N,1)
c      GQN(J,K,N,1) = TEMP1 + TEMP2
c      GQN(JJ,K,N,1)= TEMP2 + TEMP1
c      TEMP1 = GQN(J,K,N,2)
c      TEMP2 = GQN(JJ,K,N,2)
c      GQN(J,K,N,2) = TEMP1 + TEMP2
c      GQN(JJ,K,N,2)= TEMP2 + TEMP1
c 95   CONTINUE
c      endif
c
ctn gq is needed at j=jend but is not calculated 
ctn i.e. extra halo pt required
ctn because jend will not necessarily be a boundary !!!!
ctn as an interm measure, set gq(jend,k,n,i) = gq(jend-1,k,n,i)
ctn i.e. -- low order approx 
ctn
c     do 101 n=1,2
c     do 101 k=kbegin,kend-1
c       gq(jend,k,n,1) = gq(jend-1,k,n,1)
c       gq(jend,k,n,2) = gq(jend-1,k,n,2)
c101  continue
ctn
      DO 110 N=1,2
      DO 110 K=KBEGIN,KEND-1
      DO 110 J=JBEGIN,JEND-1
ctn loop changed ---- missed one point >>>> this affects damping
ctn at jend which feeds into the rest of the solution
ctn changed back
ctn   DO 110 J=JBEGIN,JEND
      GQ(J,K,N,1) = GQ(J,K,N,1)/AA(J,K)
      GQ(J,K,N,2) = GQ(J,K,N,2)/AA(J,K)
 110  CONTINUE

      DO 100 N=1,2
      DO 100 K=KBEGIN,KEND
      DO 100 J=JBEGIN,JEND
      GQN(J,K,N,1) = GQN(J,K,N,1)/AAN(J,K)
      GQN(J,K,N,2) = GQN(J,K,N,2)/AAN(J,K)
 100  CONTINUE
C !!!!!!!!!!!!!!!!!!!!EXPLICITLY COMPUTE GQ(KEND) AND GQ(JEND)!!!!!!!!
      RETURN
      END
