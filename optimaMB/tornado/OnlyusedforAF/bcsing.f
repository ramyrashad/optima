c ********************************************************************
c **       Routine to apply avg boundary conditions                 **
c **       For a singular point                                     **
c **  Note: javg/kavg can also be j,k                               **
c ********************************************************************
c calling routines: integrat
c
      subroutine bcsing(jdim1,kdim1,q1,xyj1,j1,k1,javg1,kavg1,
     &                  jdim2,kdim2,q2,xyj2,j2,k2,javg2,kavg2)
c
#include "../include/common.inc"
      dimension q1(jdim1,kdim1,4), xyj1(jdim1,kdim1)
      dimension q2(jdim2,kdim2,4), xyj2(jdim2,kdim2)
c
c Average the values in locations (javg1,kavg1) & (javg2,kavg2)
c and then store the results in (j1,k1) & (j2,k2) 
c 
      RR1 = 1./XYJ1(j1,k1)        
      RR2 = 1./XYJ2(j2,k2)        
c ************* average the density  *************************
      QSAV1=0.5*(  Q1(javg1,kavg1,1)*XYJ1(javg1,kavg1)
     &           + Q2(javg2,kavg2,1)*XYJ2(javg2,kavg2) )
c ************* average rho*U        *************************
      QSAV2=0.5*(  Q1(javg1,kavg1,2)*XYJ1(javg1,kavg1)
     &           + Q2(javg2,kavg2,2)*XYJ2(javg2,kavg2) )
c ************* average rho*V        *************************
      QSAV3=0.5*(  Q1(javg1,kavg1,3)*XYJ1(javg1,kavg1)
     &           + Q2(javg2,kavg2,3)*XYJ2(javg2,kavg2) )
c ************* average the pressure *************************
      PPP1 = GAMI*(Q1(javg1,kavg1,4)
     &             -0.5*(Q1(javg1,kavg1,2)**2+Q1(javg1,kavg1,3)**2)
     &                /Q1(javg1,kavg1,1))*XYJ1(javg1,kavg1)
      PPP2 = GAMI*(Q2(javg2,kavg2,4)
     &             -0.5*(Q2(javg2,kavg2,2)**2+Q2(javg2,kavg2,3)**2)
     &                /Q2(javg2,kavg2,1))*XYJ2(javg2,kavg2)
      PSAV = 0.5*(PPP1+PPP2)        
C ************* store the results    *************************
      Q1(j1,k1,1) = QSAV1*RR1
      Q2(j2,k2,1) = QSAV1*RR2
      Q1(j1,k1,2) = QSAV2*RR1
      Q2(j2,k2,2) = QSAV2*RR2
      Q1(j1,k1,3) = QSAV3*RR1
      Q2(j2,k2,3) = QSAV3*RR2
      Q1(j1,k1,4) = PSAV/GAMI*RR1+        
     *          0.5*(Q1(j1,k1,2)**2+Q1(j1,k1,3)**2)/Q1(j1,k1,1)
      Q2(j2,k2,4) = PSAV/GAMI*RR2+        
     *          0.5*(Q2(j2,k2,2)**2+Q2(j2,k2,3)**2)/Q2(j2,k2,1)
c
      return
      end
