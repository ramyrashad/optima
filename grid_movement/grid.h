#ifndef GRID_H
#define GRID_H

#include <fstream>
#include "Node.h"
#include "comprow_double.h"
#include "ByteSwap.h"
#include <cstring>
#include <string>

void calc_stiffness (vector <Node>& node, double *stiff_val, 
		     VECTOR_double &Fx, VECTOR_double &Fy, 
		     int *colind, int *rowptr);


void update (vector <Node>& node, const VECTOR_double &deltax,
	    const VECTOR_double &deltay);

void get_num_nonzeros (vector <Node> &node, 
		      int &num_inn_nodes, int& nonzeros);

void read_connectivity (vector <Node>& node_loc, const int& jmax, 
		       const int &kmax, const int& jtail1,
		       const int& jtail2);

// function to move grid in optima2D
void read_grid (char *filename, vector<Node>& node_loc, vector<Node*>& bnd_nodes, 
		const int &jtail1, const int& jtail2);

void write_grid (const vector<Node>& node_loc, double *x, 
		 double *y);


// Reading multiblock grid plot3D format
void read_grid_mb (char *filename, vector<Node>& node_loc, vector<Node*>& bnd_nodes, 
	        const int* ibap, const int& nfoils, const int& nblk,
                int* jmax, int* kmax);

void read_connectivity_mb (const char *filename, const int& nblk, 
			   int* blk_con, int* side_con, 
			   int* bnd, int* dir, int* ifoil, const 
			   bool& ThreeD);

void mark_boundary (vector <Node>& node, const int& node_ind, 
		    const int& blk_con, const int& side_con, const int& dir, 
		    const int& bnd_cond, const int& ifoil, const int& 
		    given_index, const int* jmax, const int *kmax, 
		    const int& current_blk, vector<Node*>& bnd_nodes,
		    int& moving_bnd_nodes);

void find_overlap (const int& blk_con, const int& side_con, const int& dir, 
		   const int& given_index, const int* jmax, const int *kmax, 
		   int& overlap_ind);

void synchronize (char *filename, vector <Node>& node, const int* ibap,
		vector <Node*>& bnd_nodes, const int& nfoils, const int* jmax);

void add_neighbours (vector <Node>& node_loc, const int* jmax, 
			const int* kmax, const int& nblk);

int max(const int& x, const int& y);

void deform_airfoil (char* filename, vector <Node*>& bnd_nodes);

void write_gridMB (const vector <Node>& node, const int& nblk, const
                int* jmax, const int* kmax, double * x, double * y,
	        const int& nhalo, const int* lgptr);


// 3D
void read_grid3D (char *filename, vector <Node>& node_loc, vector<Node*>& bnd_nodes);

void mark_boundary3D (vector <Node>& node, const int& node_ind, 
		      const int& blk_con, const int& side_con, const int& dir, 
		      const int& bnd, const int& ifoil, const int& current_blk, 
		      const int& j, const int& k, 
		      const int* jmax, const int* kmax, const int* mmax);

void  find_overlap3D (const int& blk_con, const int& side_con, const int& dir, 
		    const int& j, const int& k, const int* jmax, 
		    const int* kmax, const int* mmax, int& overlap_ind);

void add_neighbours3D (vector <Node>& node, const int* jmax, 
			const int* kmax, const int* mmax, const int& nblk);

void est_moving_bnd_nodes (char* gridfile, int& moving_bnd_nodes,
			   const int* jmax, const int* kmax, const int* mmax);

void calc_stiffness (vector <Node>& node, double *stiff_val, VECTOR_double &Fx, 
		     VECTOR_double &Fy, VECTOR_double &Fz,
		     int *colind, int *rowptr);

void update (vector <Node>& node, const VECTOR_double &deltax,
	     const VECTOR_double &deltay, const VECTOR_double& deltaz);

void write_plot3D(char* filename, const vector <Node>& node);

void write_plot3DMB(const vector <Node>& node, const int& nblk,
 	            int* jmax, int* kmax);

void write3Dgrid_plot3D(const char* gridfile, const vector <Node>& node);


#endif  // GRID_H		







