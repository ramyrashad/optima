c----------------------------------------------------------------------
c     -- define translational design variables --
c     -- used for gap and overlap optimization --
c     -- m. nemec, oct. 2001 --
c----------------------------------------------------------------------
      subroutine gapodv( x, y)

#include "mxfoil.inc"
#include "mxjk.inc"
#include "mxseg.inc"
#include "bsp.inc"

      logical cont, rep, repx, repy
      parameter (maxjk=maxj*maxk)
      double precision x(maxjk), y(maxjk)

      integer nfoils, nsegments(maxfoil), isegblk(maxsegs,maxfoil)
      integer isegside(maxsegs,maxfoil)
      common/sortedfoils/ nsegments, isegblk, isegside, nfoils

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      integer ndv, nbdv, ntdv, idvi(ibsnc), idvs(ibsnc), idvf(ibsnc)
      double precision dvs(ibsnc)
      common/dvinfo/ dvs, idvi, idvs, idvf, ndv, nbdv, ntdv

      i = 0
      cont = .true.

      do while (cont)

         call aski('Specify element?^', ifoil)
         call askl('Horizontal motion design variable?^',repx)
         call askl('Vertical motion design variable?^',repy)
         
c     -- trailing edge for element, upper and lower surface --
         iblk  = isegblk(1,ifoil)
         iside = isegside(1,ifoil)
         call getbounds(iblk,iside,jkstart,jkend)
         xtel = x(jkstart)
         ytel = y(jkstart)

         iblk  = isegblk(nsegments(ifoil),ifoil)
         iside = isegside(nsegments(ifoil),ifoil)
         call getbounds(iblk,iside,jkstart,jkend)
         xteu = x(jkend)
         yteu = y(jkend)

         write (*,*) 'xtel,ytel',xtel,ytel
         write (*,*) 'xteu,yteu',xteu,yteu

         if ( (abs(xteu-xtel) .gt. 1.e-14) .or. (abs(yteu-ytel) .gt.
     &        1.e-14) ) then
            stop 'gapodv: not setup for blunt t.e.'
         end if

         xte = xtel
         yte = ytel

         delx = 0.0

         if (repx) then 
            i = i + 1
            dvs(nbdv+i)  = xte
            idvf(nbdv+i) = ifoil
            idvs(nbdv+i) = 1
            idvi(nbdv+i) = 0
            ntdv = ntdv + 1
            write (*,10) xte
 10         format (/3x,'Current x-location',f10.5)
            rep = .false.
            call askl('Modify T.E. x-coordinate?^',rep)
            if (rep) then
               call askr('Enter new T.E. x-coordinate^',xte)
               delx = xte - dvs(nbdv+i)
               dvs(nbdv+i) = xte
            end if
         end if

         dely = 0.0      
         if (repy) then
            i = i + 1 
            dvs(nbdv+i)  = yte
            idvf(nbdv+i) = ifoil
            idvs(nbdv+i) = 2
            idvi(nbdv+i) = 0
            ntdv = ntdv + 1
            write (*,20) yte
 20         format (/3x,'Current y-location',f10.5)
            rep = .false.
            call askl('Modify T.E. y-coordinate?^',rep)
            if (rep) then
               call askr('Enter new T.E. y-coordinate^',yte)
               dely = yte - dvs(nbdv+i)
               dvs(nbdv+i) = yte
            end if
         end if

         write (*,*) delx,dely

         if ( abs(delx) .gt. 1.e-15 .or. abs(dely) .gt. 1.e-15 ) then
c     -- translate airfoil body --
            do ii = 1, ibap(ifoil) 
               bap(ii,1,ifoil) = bap(ii,1,ifoil) + delx
               bap(ii,2,ifoil) = bap(ii,2,ifoil) + dely
            end do

c     -- translate bspline control points --
c     -- needed for output bsp file --
            do iseg=1,nbseg(ifoil)
               do ii = 1,nbcp(ifoil,iseg)
                  bcp(ii,1,ifoil,iseg) = bcp(ii,1,ifoil,iseg) + delx
                  bcp(ii,2,ifoil,iseg) = bcp(ii,1,ifoil,iseg) + dely
               end do
            end do
         end if

         if ( abs(dely) .gt. 1.e-15 ) then
c     -- translate design variables --
            do ii = 1,nbdv
               dvs(ii) = dvs(ii) + dely
            end do
         end if

c     -- modify grid --
c     -- flag = true modifies grid in both x and y directions --
         flag = .true.
         call modall (ifoil, flag, x, y, .true.)

         rep = .false.
         call askl('Exit?^',rep)
         if (rep) cont=.false.

      end do

      ndv = ndv + ntdv

      iblk  = isegblk(1,ifoil)
      iside = isegside(1,ifoil)
      call getbounds(iblk,iside,jkstart,jkend)
      xtel = x(jkstart)
      ytel = y(jkstart)

      iblk  = isegblk(nsegments(ifoil),ifoil)
      iside = isegside(nsegments(ifoil),ifoil)
      call getbounds(iblk,iside,jkstart,jkend)
      xteu = x(jkend)
      yteu = y(jkend)
      
      write (*,*) 'xtel,ytel',xtel,ytel
      write (*,*) 'xteu,yteu',xteu,yteu
      
      return
      end                       !gapodv
