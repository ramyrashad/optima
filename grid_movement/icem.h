#ifndef ICEM_H
#define ICEM_H

#include "fstream"
#include "Face.h"
#include "domain.h"
#include "bocolib.h"
#include "element.h"
#include "Vol.h"

#define PID 4  // domain type is define by PID = property id

void read_icem_grid(const char* filename, vector <Node>& nodes, 
		    vector <Face>& elems, vector <Node*>& bnd_nodes);

void read_icem_grid(const char* filename, vector <Node>& nodes, 
		    vector <Vol>& elems, vector <Node*>& bnd_nodes);

void write_icem_grid(char* filename, const vector <Node>& nodes);


#endif  // ICEM_H
