C ********************************************************************
C ********************* LIFT AND DRAG CALCULATIONS *******************
C ********************************************************************
      subroutine clcdmb(q,press,x,y,xy,xyj,nscal,doit)

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer nscal,ntmp,ifoil,iseg,ib,is,lq,lg,itmp
      double precision q(*),press(*),x(*),y(*),xy(*),xyj(*)
      logical doit

c     Note: nscal=0 means press contains Jacobian scaling

  
c     Compute Forces
c     -zero quantities
      clt = 0.d0
      cdt = 0.d0
      cmt = 0.d0
      clit = 0.d0
      cdit = 0.d0
      cmit = 0.d0
      clvt = 0.d0
      cdvt = 0.d0
      cmvt = 0.d0
c
      if (doit) then
         ntmp=1
         intptr(1)=1
         do ifoil=1,nfoils
            do iseg=1,nsegments(ifoil)
               ntmp=ntmp+1
               ib = isegblk(iseg,ifoil)
c     
               itmp=jmax(ib)
               intptr(ntmp)=intptr(ntmp-1)+itmp
            enddo
         enddo
      endif
c     
      ntmp=0
      do ifoil=1,nfoils
c     
         clfoil(ifoil) = 0.d0
         cdfoil(ifoil) = 0.d0
         cmfoil(ifoil) = 0.d0
         clp(ifoil)=0.d0
         cdp(ifoil)=0.d0
         cmp(ifoil)=0.d0
         clf(ifoil)=0.d0
         cdf(ifoil)=0.d0
         cmf(ifoil)=0.d0
c     
         do iseg=1,nsegments(ifoil)
            ib = isegblk(iseg,ifoil)
            is = isegside(iseg,ifoil)
c
            lg=lgptr(ib)
            lq=lqptr(ib)

            call clcd(jmax(ib),kmax(ib),is,ibcdir(ib,is),
     &                q(lq),press(lg),x(lg),y(lg),xy(lq),
     &                xyj(lg),nscal,cli,cdi,cmi,clv,cdv,cmv,
     &                jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib))          
c     
c     -toggle total force and moment coefficients

            clit = clit + cli
            cdit = cdit + cdi
            cmit = cmit + cmi
            clvt = clvt + clv
            cdvt = cdvt + cdv
            cmvt = cmvt + cmv
            clt = clt + cli + clv
            cdt = cdt + cdi + cdv
            cmt = cmt + cmi + cmv
c     
c     -toggle elemental force and moment coefficients
            clfoil(ifoil) = clfoil(ifoil) + cli + clv
            cdfoil(ifoil) = cdfoil(ifoil) + cdi + cdv
            cmfoil(ifoil) = cmfoil(ifoil) + cmi + cmv
            clp(ifoil) = clp(ifoil) + cli
            cdp(ifoil) = cdp(ifoil) + cdi
            cmp(ifoil) = cmp(ifoil) + cmi
            clf(ifoil) = clf(ifoil) + clv
            cdf(ifoil) = cdf(ifoil) + cdv
            cmf(ifoil) = cmf(ifoil) + cmv
         enddo
      enddo
c     


      return
      end   !clcdmb
