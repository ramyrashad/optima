C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C----------------------------------------------------------------------C
C
      SUBROUTINE length_RANK(N,nchrom,ngene,nobfun,fit,irank,
     $     ndim,nacc,fitacc,iacc,iaccum)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTINE COMPUTES THE RANKING FOR EACH          C
C               OF THE NCHROM CHROMOSOMES. FOR MULTI-OBJECTIVE         C
C               CASES (NOBFUN>1) GOLDBERG RANKING IS USED ***          C
c        it also employs Dustin's maximal length sort and ranking      c
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      dimension fit(nobfun,nchrom),irank(nchrom)
C
      dimension fitacc(nobfun,ndim), iacc(2,ndim)
      DIMENSION         FITDUM(nobfun,ndim)
      DIMENSION         IACCDUM(2,ndim)

      dimension fit_leng(ndim),index_sort(ndim)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** COMPUTE RANKING ARRAY (IRANK) ***
C
c  initialize rank = 0 and compute lengths
      DO NC=1,NCHROM
         IRANK(NC) = 0
         fit_leng(nc) = 0.0
         do no = 1,nobfun
            fit_leng(nc) = fit(no,nc)**2 + fit_leng(nc)
         enddo
      ENDDO

      
      NCHROMM = NCHROM-1
      DO NCC=1,NCHROM
         DO NC=1,NCHROMM
            IF (fit_length(NC).LT.fit_length(NC+1)) THEN


stopped here????
               DO NO=1,NOBFUN
                  FITDUM          = FITSAV(NO,NC)
                  FITSAV(NO,NC)   = FITSAV(NO,NC+1)
                  FITSAV(NO,NC+1) = FITDUM
               ENDDO
               IDUM1 = ISAV(1,NC)
               ISAV(1,NC) = ISAV(1,NC+1)
               ISAV(1,NC+1) = IDUM1
               IDUM2 = ISAV(2,NC)
               ISAV(2,NC) = ISAV(2,NC+1)
               ISAV(2,NC+1) = IDUM2
            ENDIF
         ENDDO
      ENDDO

      DO ICOUNT=1,NCHROM
         INC = 0
         DO NC=1,NCHROM
            IF (IRANK(NC).NE.0) THEN
               INC = INC+1
               GO TO 1001
            ENDIF
            DO NCC=1,NCHROM
               IF (IRANK(NCC).NE.0.AND.IRANK(NCC).NE.ICOUNT) GO TO 1002
               INOF1 = 0
               INOF2 = 0
               DO NO=1,NOBFUN
                  IF (FIT(NO,NC).LE.FIT(NO,NCC)) THEN
                     INOF1 = INOF1+1
                  ENDIF
                  IF (FIT(NO,NC).LT.FIT(NO,NCC)) THEN
                     INOF2 = 1
                  ENDIF
               ENDDO
               IF (INOF1.EQ.NOBFUN.AND.INOF2.EQ.1) GO TO 1001
 1002          CONTINUE
            ENDDO
            IRANK(NC) = ICOUNT
 1001       CONTINUE
         ENDDO
         IF (INC.EQ.NCHROM) GO TO 1003
      ENDDO
 1003 CONTINUE

C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** CHECK CURRENT RANKS AGAINST THE ACCUMULATED FILE
C              OF NUMBER ONE RANKS (FITACC) SO THAT THE PARETO FRONT
C              WILL NOT DIGRESS. (NOBFUN>1 AND IACCUM=1) ***
C
C      IACCUM = 1
C
C-----------------------------------------------------------------------
C          *** SAVE CURRENT NUMBER ONE RANKED CHROMOSOMES--FITNESS
C              VALUES ONLY--INTO LARGE ACCUMULATION ARRAY (FITACC).
C              DO THIS FOR IPOP=1, NOBFUN>1 AND IACCUM=1 ONLY ***
C
      IF (N.EQ.1.AND.NOBFUN.GT.1.AND.IACCUM.EQ.1) THEN
         NACC = 0
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               NACC = NACC+1
               DO NO=1,NOBFUN
                  FITACC(NO,NACC) = FIT(NO,NC)
               ENDDO
               IACC(1,NACC) = N
cthp 5/25/01               IACC(1,NACC) = N-1
               IACC(2,NACC) = NC
            ENDIF
         ENDDO
      ENDIF
C
C-----------------------------------------------------------------------
C          *** REDUCE RANKING (I.E., INCREASE IRANK VALUE) FOR 
C              MEMBERS OF CURRENT GENERATION THAT ARE DOMINATED 
C              BY AT LEAST ONE INDIVIDUAL FROM THE ACCUMULATED 
C              FILE (FITACC). DO THIS ONLY WHEN NOBFUN>1, IPOP>1 
C              AND IACCUM=1 ***
C
      IF (NOBFUN.GT.1.AND.N.GT.1.AND.IACCUM.EQ.1) THEN
         DO NC=1,NCHROM
            IF (IRANK(NC).GE.2) IRANK(NC) = IRANK(NC)+1
         ENDDO
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               DO NCC=1,NACC
                  ITEST1 = 0
                  ITEST2 = 0
                  DO NO=1,NOBFUN
                     IF (FITACC(NO,NCC).GE.FIT(NO,NC)) THEN
                     ITEST1 = ITEST1+1
                     ENDIF
                  ENDDO
                  DO NO=1,NOBFUN
                     IF (FITACC(NO,NCC).GT.FIT(NO,NC)) THEN
                     ITEST2 = 1
                     ENDIF
                  ENDDO
                  IF (ITEST1.EQ.NOBFUN.AND.ITEST2.EQ.1) THEN
                     IRANK(NC) = 2
                  ENDIF
               ENDDO
            ENDIF
         ENDDO
      ENDIF
C
C-----------------------------------------------------------------------
C          *** ADD INDIVIDUALS TO THE ACCUMULATED FILE, BUT
C              ONLY IF THEY DO NOT ALREADY EXIST IN THE 
C              ACCUMULATED FILE. DO THIS ONLY WHEN NOBFUN>1, 
C              IPOP>1 AND IACCUM=1 ***
C
      IF (NOBFUN.GT.1.AND.N.GT.1.AND.IACCUM.EQ.1) THEN
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               DO NCC=1,NACC
                  ITEST = 0
                  DO NO=1,NOBFUN
                     IF (FITACC(NO,NCC).EQ.FIT(NO,NC)) THEN
                        ITEST = ITEST+1
                     ENDIF
                  ENDDO
                  IF (ITEST.EQ.NOBFUN) GO TO 2001
               ENDDO
C
               NACC = NACC+1
               DO NO=1,NOBFUN
                  FITACC(NO,NACC) = FIT(NO,NC)
               ENDDO
               IACC(1,NACC) = N
cthp 5/25/01               IACC(1,NACC) = N-1
               IACC(2,NACC) = NC
            ENDIF
 2001    CONTINUE
         ENDDO
      ENDIF
C
C-----------------------------------------------------------------------
C          *** REMOVE INDIVIDUALS FROM THE ACCUMULATED FILE
C              OF NUMBER ONE RANKS IF AN INDIVIDUAL FROM THE 
C              ACTIVE FILE (FIT) DOMINATES. DO THIS ONLY WHEN 
C              NOBFUN>1, IPOP>1 AND IACCUM=1 ***
C
      IF (NOBFUN.GT.1.AND.N.GT.1.AND.IACCUM.EQ.1) THEN
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               DO NCC=1,NACC
                  ITEST1 = 0
                  ITEST2 = 0
                  DO NO=1,NOBFUN
                     IF (FIT(NO,NC).GE.FITACC(NO,NCC)) THEN
                     ITEST1 = ITEST1+1
                     ENDIF
                  ENDDO
                  DO NO=1,NOBFUN
                     IF (FIT(NO,NC).GT.FITACC(NO,NCC)) THEN
                     ITEST2 = 1
                     ENDIF
                  ENDDO
                  IF (ITEST1.EQ.NOBFUN.AND.ITEST2.EQ.1) THEN
                     DO NO=1,NOBFUN
                        FITACC(NO,NCC) = 0.0
                     ENDDO
                  ENDIF
               ENDDO
            ENDIF
         ENDDO
C
         IAC  = NACC
         ICOUNT = 0
         DO NC=1,NACC
            ITEST = 0
            DO NO=1,NOBFUN
               IF (FITACC(NO,NC).EQ.0.0) THEN
                  ITEST = ITEST+1
               ENDIF
            ENDDO
            IF (ITEST.NE.NOBFUN) THEN
               ICOUNT = ICOUNT+1
               DO NO=1,NOBFUN
                  FITDUM(NO,ICOUNT) = FITACC(NO,NC)
               ENDDO
               IACCDUM(1,ICOUNT) = IACC(1,NC)
               IACCDUM(2,ICOUNT) = IACC(2,NC)
            ELSE
               IAC = IAC-1
            ENDIF
         ENDDO
C
         NACC = IAC
         DO NC=1,NACC
            DO NO=1,NOBFUN
               FITACC(NO,NC) = FITDUM(NO,NC)
            ENDDO
            IACC(1,NC) = IACCDUM(1,NC)
            IACC(2,NC) = IACCDUM(2,NC)
         ENDDO
C
C-----------------------------------------------------------------------
C           *** SORT THE ACCUMULATED FILE PARETO FRONT USING
C               THE FIRST FITNESS VALUE ***
         CALL SORT(NOBFUN,NACC,FITACC,IACC)
C
C-----------------------------------------------------------------------
C
      ENDIF
C          *** RANKING OPERATION COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
