      subroutine blomax(jdim,kdim,q,press,vort,turmu,fmu,xy,xyj,x,y)
c
c     calling subroutine 'etaexpl'
c
#include "../include/arcom.inc"
c                                                                       
c                                                                       
      dimension q(jdim,kdim,4),turmu(jdim,kdim),vort(jdim,kdim)         
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)         
      dimension fmu(jdim,kdim),x(jdim,kdim),y(jdim,kdim)
c                                                                       
      common/worksp/snor(maxj),tmo(maxj),tmi(maxj),uu(maxj),            
     *     tas(maxj),work(maxj,25)                                      
c                                                                       
c      common/turout/tauh(maxj), ustar(maxj),                            
c     >              yplus(maxj, maxk), uplus(maxj, maxk)                
c                                                                       
                                                                        
c                                                                       

c     -- numerical recipies array for solving 3x3 system --
      double precision nrA(3,3), nrD(3), nrB(3), nrindx(3)  

      data f27 /1.6d0/                                                  
      data fk,fkk,ydumf /0.4d0,0.0168d0,1.d0/                          
      data fkleb /0.3d0/                                               
      data fmutm/14.d0/                                             
c                                                                       
c  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
c  ******************  turmu comes in with vorticity *****************  
c  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
c                                                                       
c                                                                       

      kedge = nint(.75d0*dble(kend))

csd   -probably set turmu=0 from k=kedge,kup instead of k=1,kup
csd   -save a few cpu cylcles
c                                                                       
c     -note xyj(j,k) stores transform jacobian by which dep vars are
c      divided
c                                                                       
      ztmp=1.d0/26.d0
      do 80 j = jlow,jup                                                
c        find vorticity tas(k) and total velocity utot(k)               
         do 11 k = kbegin,kup                                            
            utot       = q(j,k,2)**2 + q(j,k,3)**2                      
            uu(k)      = sqrt(utot)/q(j,k,1)                            
            tas(k)     = vort(j,k)                                      
c            snor(k)    = 0.d0                                            
            turmu(j,k) = 0.d0                                            
   11    continue                                                        
c                                                                       
c        compute ra                                                       
c                                                                       
         k=kbegin                                                        
         wmu = .5d0*( fmu(j,k) + fmu(j,k+1))                            
         tau = abs(tas(k))                                               
         rhoav = 0.5d0*(xyj(j,k)*q(j,k,1) + xyj(j,k+1)*q(j,k+1,1))     
         ra = sqrt(re*rhoav*tau/wmu)*ztmp
         if (.not.cmesh .and. .not.periodic) then  
            if(j.lt.jtail1 .or. j.gt.jtail2) ra = 1.d3*ra              
         endif                                                           
c                                                                       
c        compute normal distance snor(k) and y*dudy                       
c                                                                       
         snor(1) = 0.d0                                                 
         ydum = 0.d0                                                      
         umin = uu(1)                                                    
         ydus = 0.d0                                                      
c
csd      -there is a lot of opportunity here to speed things up.
csd        -split do 20 loop into two seperate loops
csd            1) compute umin & snor  k=klow,kup
csd            2) compute rest k=klow,kedge
csd        -klow is always equal to 2 for cmesh=false and for periodic
csd         and non-periodic grids so simply initialize km2,ydum,um,ym
csd         to what you want before doing 2nd loop
csd      -doing all this would get rid of at least 2 costly if statements.
         do 20 k = klow,kup                                              
            km1=k-1
            if(uu(k) .lt. umin) umin = uu(k)                             
            scis = abs(xy(j,km1,3)*xy(j,k,3)+xy(j,km1,4)*xy(j,k,4))      
            scal = 1.d0/sqrt(scis)                                       
            snor(k) = snor(km1) + scal                                   
            if(k .gt. 2) go to 18                                       
            km2 = 1                                                      
            ydum = 1.d-3                                                 
            um = ydum                                                    
            ym = 0.5d0*snor(2)                                          
 18         continue
            snora = 0.5d0*(snor(k) + snor(km1))
            ydu = snora*abs(tas(km1))*(1.-exp(-ra*snora))  

            if(k .gt. kedge) go to 20                                    
            if(ydu .lt. ydum) go to 20                                    
            km2 = k - 1                                                 
            ydum = ydu                                                  
            um = 0.5d0*(uu(km1) + uu(k))                                  
            ym = snora                                                  
 20      continue

c                                                                       
c        interpolate to find ym, ydum, and um                          
c                                                                       
         if(km2 .lt. 2 .or. km2 .gt. kedge-1) go to 22                   

         ym3 = 0.5d0*(snor(km2+1) + snor(km2+2))                         
         ym1 = 0.5d0*(snor (km2-1) + snor (km2))                         

         ydum1 = ym1*abs(tas(km2-1))*(1.d0 - exp(-ra*ym1))              
         ydum3 = ym3*abs(tas(km2+1))*(1.d0 - exp(-ra*ym3))              

         c2 = ydum - ydum1                                             
         c3 = ydum3 - ydum                                             
         dy1 = ym - ym1                                                
         dy3 = ym3 - ym

         am = (dy3*dy3*c2 + dy1*dy1*c3)/(dy1*dy3*(dy1+dy3))            
         bm = (dy1*c3 - dy3*c2)/(dy1*dy3*(dy1 + dy3))                  

         if(bm .ge. 0.) go to 22                                       
c
         ymm = ym - 0.5d0*am/bm                                        
         ydu  = ydum - 0.25d0*am*am/bm                                 

         if(ydu.lt.ydum .or. ymm.lt.ym1 .or. ymm.gt.ym3) goto 22
c
         ydum = ydu                                               
         ym = ymm    

         if(ym .gt. snor(km2+1)) km2 = km2 + 1                    
         if(ym .lt. snor(km2)) km2 = km2 - 1                      
         um= ((snor(km2+1) - ym)*uu(km2)+(ym-snor(km2))*uu(km2+1))
     &      /(snor(km2+1) - snor(km2))                            
 22      continue

c     -----------------------------------------------------------
c     New code, recalculate ydum and ym using interpolation
c     avoid the use of integer km2
c     Jan 2001 by p. wong
c     remove this section will recover the original code
c     -----------------------------------------------------------

         if(numiter.gt.100) then
         
         do k=klow, kup
            km1=k-1
            scis = abs(xy(j,km1,3)*xy(j,k,3)+xy(j,km1,4)*xy(j,k,4))      
            scal = 1.d0/sqrt(scis)                                       
            snor(k) = snor(km1) + scal                                   
         end do

         km_p = 1
         ydum = 0.0
         do k=klow, kup
            km1=k-1
            snora = 0.5d0*(snor(k) + snor(km1))
            ydu = snora*abs(tas(km1))*(1.-exp(-ra*snora))  

            if (ydu.gt.ydum) then
              km_p = k
              ydum = ydu
            endif
         end do

            k = km_p
            km1 = k-1

            snora = 0.5d0*(snor(k-1) + snor(km1-1))
            snora1 = snora
            y1 = snora*abs(tas(km1-1))*(1.-exp(-ra*snora))  

            snora = 0.5d0*(snor(k) + snor(km1))
            snora2 = snora
            y2 = snora*abs(tas(km1))*(1.-exp(-ra*snora))  

            snora = 0.5d0*(snor(k+1) + snor(km1+1))
            snora3 = snora
            y3 = snora*abs(tas(km1+1))*(1.-exp(-ra*snora))  
         
         x1 = km_p-1.0
         x2 = km_p*1.0
         x3 = km_p+1.0

         nrA(1,1) = x1*x1
         nrA(1,2) = x1
         nrA(1,3) = 1.0

         nrA(2,1) = x2*x2
         nrA(2,2) = x2
         nrA(2,3) = 1.0

         nrA(3,1) = x3*x3
         nrA(3,2) = x3
         nrA(3,3) = 1.0
 
         nrB(1) = y1
         nrB(2) = y2
         nrB(3) = y3

         call ludcmp(nrA,3,3,nrindx,nrD)
         call lubksb(nrA,3,3,nrindx,nrB)

         xkm_p = -nrB(2)/2.0/nrB(1)
         ydum = nrB(1)*xkm_p*xkm_p + nrB(2)*xkm_p + nrB(3)
         ym = (xkm_p-x1)/(x3-x1)*(snora3-snora1) + snora1

         end if

c     -----------------------------
c     end section p.wong
c     -----------------------------

c                                                                       
c        compute outer eddy viscosity                                     
c                                                                       
         do 25 k=1,kedge                                                 
            snor(k) = 0.5d0*(snor(k) + snor(k+1))                        
            rhoav = 0.5d0*(xyj(j,k)*q(j,k,1) + xyj(j,k+1)*q(j,k+1,1))    
            ffc = fkk*f27*re*rhoav                                       
            tmo(k) = ffc*ym*ydum                                         
            ffcwk = ydumf*ydumf*ffc                                      
            udiff = abs(um - umin)                                       
            if(ydum .gt. udiff*ydumf) tmo(k) = ffcwk*ym*udiff*udiff/ydum 
            fia = fkleb*snor(k)/ym                                       
            if(fia .gt. 1.d5) fia = 1.d5                                 
            fi = 1.d0 + 5.5d0*fia**6                                      
            tmo(k) = tmo(k)/fi                                           
            tmo(k) = abs(tmo(k))                                         
 25      continue
c                                                                       
c        compute inner eddy viscosity                                     
c                                                                       
         do 30 k=1,kedge                                                
            tau = abs(tas(k))                                            
            rhoav = 0.5d0*(xyj(j,k)*q(j,k,1) + xyj(j,k+1)*q(j,k+1,1))     
            tmi(k) =rhoav*re*tau*(fk*snor(k)*(1.-exp(-ra*snor(k))))**2   
            tmi(k) = abs(tmi(k))                                        
 30      continue                                                        
c                                                                       
c        load viscosity coeffs. into array, use inner value until         
c        match point is reached                                           
c                                                                       
csd
csd      -I think you can speed this section up too.
csd       -see blomax_ho.f
         k = 1                                                           
 40      turmu(j,k) = tmi(k)
         k = k +1                                                        
         if( k.gt. kedge) go to 10                                       
         if( tmi(k) .le. tmo(k)) go to 40                              
 41      turmu(j,k) = tmo(k)                                           
         k = k +1                                                      
         if( k.le. kedge) go to 41                                     
 10      continue

 80   continue       

c      stop
                                                   
c                                                                       
c     transition model turned off for now                                
c     transition not computed but fixed from input in terms of % chord   
c                                                                       
      if(translo.ne.0.0)then                                           
c                                                                       
c        -zero turmu from jtranlo to jtranup  
c        -for cmesh logic shift jtranlo,jtranup,jrampup and
c         jramplo by jtail1-1              
c                                                                       
         jtlo   = jtranlo           
         jtup   = jtranup
         jrampu = jrampup1
         jrampl = jramplo1
         if(cmesh)then                                                  
           jtlo    = jtranlo-jtail1+1                                   
           jtup    = jtranup-jtail1+1
           jrampu  = jrampup1-jtail1+1
           jrampl  = jramplo1-jtail1+1
         endif                                                          
c                                                                       
c                                                                   
         if (ramptran.eq.0.) then
           do 455 k = kbegin,kend
           do 455 j = jtlo,jtup
             turmu(j,k) = 0.d0
 455       continue
c                                                                       
c          -original ramp           
c           thp 4/2/86  put in linear ramp for transition      
           if(jtup-jtlo .gt. 4)then                                   
             do 460 k = kbegin,kend                                  
               turmu(jtlo,k) = 0.5d0*turmu(jtlo-1,k)                
               turmu(jtup,k) = 0.5d0*turmu(jtup+1,k)                
 460         continue
           endif                                                 
c
         elseif (ramptran.gt.0) then
           include 'ramp.inc'
         elseif (ramptran.lt.0) then
c          -for now only linear ramp is used over
c           distance ramptran (percentage of chord) 
c          -ramp spans a minimum of two nodes ... 
c                i.e. like thp's ramp above
c          -jrampup and jramplo computed in setup.f
c
           do 456 k = kbegin,kend
           do 456 j = jtlo,jtup
             turmu(j,k) = 0.d0
 456       continue

           jrange=jrampu-jtup+1
           scal=1.d0/float(jrange)
           do 466 k=kbegin,kend
             jj=0
             do 465 j=jtup,jrampu
               jj=jj+1
               turmu(j,k)=scal*float(jj)*turmu(jrampu,k)
c               turmu(j,k)=ramp(j,1)*turmu(jrampup,k)
 465         continue
 466       continue
c          
           jrange=jtlo-jrampl+1
           scal=1.d0/float(jrange)
           do 468 k=kbegin,kend
             jj=0
             do 467 j=jtlo,jrampl,-1
               jj=jj+1
               turmu(j,k)=scal*float(jj)*turmu(jrampl,k)
c               turmu(j,k)=ramp(j,2)*turmu(jramplo,k)
 467         continue
 468       continue
         endif
      endif                                                             
c                                                                       
      if(.not.bcairf)then                                               
c        -for flat plate set eddy viscosity to zero       
c                                                                       
         do 885 k = kbegin, kend                                        
         do 885 j = jbegin, jtail1                                      
            turmu(j, k) = 0.0                                            
 885     continue                                                       
      endif                                                             
c 
      return
      end                                                             

c -------------------------------------------------------

      SUBROUTINE LUBKSB(A,N,NP,INDX,B)
      DIMENSION A(NP,NP),INDX(N),B(N)
      II=0
      DO 12 I=1,N
        LL=INDX(I)
        SUM=B(LL)
        B(LL)=B(I)
        IF (II.NE.0)THEN
          DO 11 J=II,I-1
            SUM=SUM-A(I,J)*B(J)
11        CONTINUE
        ELSE IF (SUM.NE.0.) THEN
          II=I
        ENDIF
        B(I)=SUM
12    CONTINUE
      DO 14 I=N,1,-1
        SUM=B(I)
        IF(I.LT.N)THEN
          DO 13 J=I+1,N
            SUM=SUM-A(I,J)*B(J)
13        CONTINUE
        ENDIF
        B(I)=SUM/A(I,I)
14    CONTINUE
      RETURN
      END

c ----------------------------------------------------------

      SUBROUTINE LUDCMP(A,N,NP,INDX,D)
      PARAMETER (NMAX=100,TINY=1.0E-20)
      DIMENSION A(NP,NP),INDX(N),VV(NMAX)
      D=1.
      DO 12 I=1,N
        AAMAX=0.
        DO 11 J=1,N
          IF (ABS(A(I,J)).GT.AAMAX) AAMAX=ABS(A(I,J))
11      CONTINUE
        IF (AAMAX.EQ.0.) PAUSE 'Singular matrix.'
        VV(I)=1./AAMAX
12    CONTINUE
      DO 19 J=1,N
        IF (J.GT.1) THEN
          DO 14 I=1,J-1
            SUM=A(I,J)
            IF (I.GT.1)THEN
              DO 13 K=1,I-1
                SUM=SUM-A(I,K)*A(K,J)
13            CONTINUE
              A(I,J)=SUM
            ENDIF
14        CONTINUE
        ENDIF
        AAMAX=0.
        DO 16 I=J,N
          SUM=A(I,J)
          IF (J.GT.1)THEN
            DO 15 K=1,J-1
              SUM=SUM-A(I,K)*A(K,J)
15          CONTINUE
            A(I,J)=SUM
          ENDIF
          DUM=VV(I)*ABS(SUM)
          IF (DUM.GE.AAMAX) THEN
            IMAX=I
            AAMAX=DUM
          ENDIF
16      CONTINUE
        IF (J.NE.IMAX)THEN
          DO 17 K=1,N
            DUM=A(IMAX,K)
            A(IMAX,K)=A(J,K)
            A(J,K)=DUM
17        CONTINUE
          D=-D
          VV(IMAX)=VV(J)
        ENDIF
        INDX(J)=IMAX
        IF(J.NE.N)THEN
          IF(A(J,J).EQ.0.)A(J,J)=TINY
          DUM=1./A(J,J)
          DO 18 I=J+1,N
            A(I,J)=A(I,J)*DUM
18        CONTINUE
        ENDIF
19    CONTINUE
      IF(A(N,N).EQ.0.)A(N,N)=TINY
      RETURN
      END
