       subroutine iomarkusstage(junit,stage,timeindex,jdim,kdim,q,xyj,
     &     turre)

      implicit none 

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

     
c     junit=1 write output_file
c     junit=3 read output_file

      integer strlen,junit,timeindex,namelen,numberlen,jdim,kdim,i,stage
      integer jj
      double precision dummy
      character number*4
      character stagenumber*1
c
      double precision q(jdim*kdim*4), xyj(jdim*kdim), turre(jdim*kdim)
      double precision qtmp(jdim*kdim*4),turretmp(jdim*kdim)
      logical writeok

      call i_to_s(timeindex,number)
      call i_to_s(stage,stagenumber)
     
      namelen = strlen(output_file_prefix)
      filena=output_file_prefix      
      filena(namelen+1:namelen+1) = stagenumber
      numberlen = len(number)      
      filena(namelen+2:namelen+1+numberlen) = number
      filena(namelen+numberlen+2:namelen+numberlen+3) ='.q'

      open(unit=n_all,file=filena,status='unknown',form='unformatted') 

                                                           

      if (junit.eq.1) then

         writeok=.false.
         jj=0

         do while (.not. writeok .and. jj.lt.3)

            write(n_all) nblks
            write(n_all) (jbmax(i),kbmax(i),i=1,nblks)
            do i=1,nblks
               write(n_all) fsmach, alpha, re*fsmach
               call writempr(n_all,q(lqptr(i)),xyj(lgptr(i)),
     &              turre(lgptr(i)),jmax(i),kmax(i),
     &              jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
            end do

            write(n_all) numiter
            rewind(n_all)

            writeok=.true.

            read(n_all) nblks 
            read(n_all) (jbmax(i),kbmax(i),i=1,nblks)
            do i=1,nblks
               read(n_all) dummy, dummy, dummy
               call readmpr(n_all,qtmp(lqptr(i)),turretmp(lgptr(i)), 
     &              jmax(i),kmax(i),jminbnd(i),jmaxbnd(i),
     &              kminbnd(i),kmaxbnd(i))
            end do
            read(n_all) numiter
            rewind(n_all)

            do i=1,nblks
              call compoutmpr(q(lqptr(i)),xyj(lgptr(i)),turre(lgptr(i)),
     &             qtmp(lqptr(i)),turretmp(lgptr(i)),jmax(i),kmax(i),
     &             jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i),writeok)
            end do

            if (.not. writeok) then
              jj=jj+1
              print *, 'IO error -> will try to rewrite file',numiter,jj
            end if
            
         end do                 !while

c        stop if trying to write it three times did not work
         if (jj.ge.3) stop
       
      else if (junit.eq.3) then
c     q contains the physical quantities (not scaled with J=xyj)

         read(n_all) nblks 
         read(n_all) (jbmax(i),kbmax(i),i=1,nblks)

         do i=1,nblks
           read(n_all) dummy, dummy, dummy
           call readmpr(n_all,q(lqptr(i)),turre(lgptr(i)), 
     &      jmax(i),kmax(i),jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
         end do

         read(n_all) numiter

      endif

      close (n_all)  
    
      return                                                            
      end 
