#include <stdio.h>
#include <string.h>
#include "snopt.hh"
#include "snoptProblem.hh"
#include "cuterProblem.hh"


int main( int argc, char **argv)
{
  integer Cold = 0;

  cuterProblem prob;
  prob.initialize();
  prob.setSpecFile( "sncuter.spc" );
  prob.solve( Cold );
}
