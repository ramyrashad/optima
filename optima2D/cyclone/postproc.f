      subroutine postproc(jdim,kdim,qp,q,qold)
c
#include "../include/arcom.inc"
c
      dimension qp(jdim,kdim,4),q(jdim,kdim,4),qold(jdim,kdim,4)       
c
c     -if statement here to avoid computation during steady runs
c      so that you don't use up the memory during grid sequencing
      if (unsted) then
        do 8 n=1,4
        do 8 k=1,kmax
        do 8 j=1,jmax
          qold(j,k,n)=q(j,k,n)
 8      continue
c        
        if (turbulnt.and.itmodel.eq.2) then
          do 9 k=1,kmax
          do 9 j=1,jmax
            turold(j,k)=turre1(j,k)
            turre1(j,k)=turre(j,k)
 9        continue
        endif
      endif
c
c     -for the way cyclone.f is setupwith q and qp for unsteady flow
c      computations, this loop is necessary for grid sequencing
      do 10 n=1,4
      do 10 k=1,kmax
      do 10 j=1,jmax
        q(j,k,n)=qp(j,k,n)
 10   continue
c
      return
      end

