      subroutine xi_visc(jdim,kdim,cb2,resiginv,xy,work,bx,cx,dx,fy)
c     calling subroutine: spalart
c
#include "../include/arcom.inc"
c
      dimension xy(jdim,kdim,4),work(jdim,kdim)
      dimension bx(kdim,jdim),cx(kdim,jdim),dx(kdim,jdim),fy(jdim,kdim)
c
c     ***********************
c     E_xi_xi Viscous Terms
c     ***********************
c     
      do k=klow,kup
        do j=jlow,jup
          jp1 = j+1
          jm1 = j-1
          xy1p     = .5*(xy(j,k,1)+xy(jp1,k,1))
          xy2p     = .5*(xy(j,k,2)+xy(jp1,k,2))
          ttp      =  (xy1p*xy(j,k,1)+xy2p*xy(j,k,2))
            
          xy1m     = .5*(xy(j,k,1)+xy(jm1,k,1))
          xy2m     = .5*(xy(j,k,2)+xy(jm1,k,2))
          ttm      =  (xy1m*xy(j,k,1)+xy2m*xy(j,k,2))
          
          cnud=cb2*resiginv*work(j,k)
          
          cdp       =    ttp*cnud
          cdm       =    ttm*cnud               
c     
          trem =.5*(work(jm1,k)+work(j,k))
          trep =.5*(work(j,k)+work(jp1,k))
c     
          cap  =  ttp*trep*(1.0+cb2)*resiginv
          cam  =  ttm*trem*(1.0+cb2)*resiginv
          
c     
          bx(k,j)   = cdm-cam
          cx(k,j)   = -cdp+cap-cdm+cam
          dx(k,j)   = cdp-cap
            
          fy(j,k)   =  fy(j,k) - bx(k,j)*turre(jm1,k)
     &                         - cx(k,j)*turre(j,k  )
     &                         - dx(k,j)*turre(jp1,k) 
          
c            bx(k,j)   = 0.d0
c            cx(k,j)   = 0.d0
c            dx(k,j)   = 0.d0
        enddo
      enddo
c
      return
      end
