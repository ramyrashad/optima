#ifndef NODE_H
#include "Node.h"
#endif

// Definition of default constructor
Node::Node()
{
  ifoil = num_nei = ind = ID = 0;
  xyz = dist = new_xyz = NULL;
  nei = NULL;
  surf = false;
  ThreeD = false;
  num_nei =0;
  overlap = NULL;
  symm = 0;
}


Node::Node(double *coord, const int& nodeID, const bool& dim)
{
  ifoil = 0;
  ThreeD = dim;
  if (ThreeD==true)
    {
      xyz = new double[3];
      new_xyz = new double[3];

      xyz[0] = coord[0];  xyz[1] = coord[1]; xyz[2] = coord[2];
      new_xyz[0] = coord[0];   new_xyz[1] = coord[1];
      new_xyz[2] = coord[2];
    }
  else
    {
      xyz = new double[2];
      new_xyz = new double[2];

      xyz[0] = coord[0];  xyz[1] = coord[1];
      new_xyz[0] = coord[0];   new_xyz[1] = coord[1];
    }

  num_nei = 0;
  ind = nodeID;
  ID = nodeID;
  symm = 0;
  
  nei = new Node*[1];
  dist = new double [1];
  surf = false;
  overlap = NULL;
}


Node::Node(const double& x, const double& y, const double& z, const int& nodeID)
{
  ifoil = 0;
  ThreeD = true;

  xyz = new double[3];
  new_xyz = new double[3];

  xyz[0] = x;  xyz[1] = y;  xyz[2] = z;
  new_xyz[0] = x;   new_xyz[1] = y;  new_xyz[2] = z;

  num_nei = 0;
  ind = nodeID;
  ID = nodeID;
  symm = 0;
  
  nei = new Node*[1];
  dist = new double [1];
  surf = false;
  overlap = NULL;
}


Node::Node(const double& x, const double& y, const int& nodeID)
{
  ifoil = 0;
  ThreeD = false;

  xyz = new double[2];
  new_xyz = new double[2];

  xyz[0] = x;  xyz[1] = y;
  new_xyz[0] = x;   new_xyz[1] = y;

  num_nei = 0;
  ind = nodeID;
  ID = nodeID;
  symm = 0;
  
  nei = new Node*[1];
  dist = new double [1];
  surf = false;
  overlap = NULL;
}



// Destructor - takes no action
Node::~Node() 
{
}

void Node::clearmem()  // clears dynamically allocated memory
{
  delete [] nei;      nei = NULL;
  delete [] dist;     dist = NULL;
  delete [] xyz;      xyz = NULL;
  delete [] new_xyz;  new_xyz = NULL;
  overlap = NULL;
  return;
}

ostream& operator <<(ostream& out, const Node& a)
{
  out.setf(ios::showpoint);
  out.setf(ios::fixed);
  out.setf(ios::left);

  if (a.ThreeD==true)
    out << setw(20) << a.xyz[0] << setw(20) << a.xyz[1]
	<< setw(20) << a.xyz[2] << setw(20) << a.ID << endl;
  else 
    out << setw(20) << a.xyz[0] << setw(20) << a.xyz[1]
	<< setw(20) << a.ID << endl;
  return out;
}

double* Node::get_dist()
{
  double* coord;
  double* dist_new = new double [num_nei];
  for (int j=0; j<num_nei; j++)
    {
      coord = nei[j]->get_xyz();
      dist_new[j] = 0.0;
      if (ThreeD==true)
	dist_new[j] = (xyz[0]-coord[0])*(xyz[0]-coord[0])
	  + (xyz[1]-coord[1])*(xyz[1]-coord[1])
	  + (xyz[2]-coord[2])*(xyz[2]-coord[2]);
      else
	dist_new[j] = (xyz[0]-coord[0])*(xyz[0]-coord[0])
	  + (xyz[1]-coord[1])*(xyz[1]-coord[1]);
      dist_new[j] = sqrt(dist_new[j]);
    }
  delete [] dist;
  dist = dist_new;
  coord = NULL;
  dist_new = NULL;

  return dist;
}


void Node::add_nei(Node& a)
{

  int i,j;
  bool add = true;   // determines whether the node has already been included
  vector <int> labels;
  vector <int> tag;
      
  vector <Node*> temp (1);

  if (num_nei > 0) 
    {
      for (i=0; i<num_nei; i++)
	{
	  if (nei[i]->get_ID() == a.get_ID())
	    {
	      add = false;
	      break;
	    }
	}
      if (add == true)
	{
	  num_nei = num_nei+1;
	  temp.resize (num_nei);
	  for (j=0; j<num_nei-1; j++)
	    temp[j] = nei[j];
	  temp[num_nei-1] = &a;
			
	  delete [] nei;  
	  nei = new Node* [num_nei];
			
	  if (temp[num_nei-2]->get_ID() > temp[num_nei-1]->get_ID())
	    {
	      labels.resize(num_nei);
	      tag.resize(num_nei);
				
	      for (j=0; j<num_nei; j++)
		{
		  labels[j] = temp[j]->get_ID();
		  tag[j] = j;
		}
	      tagSort (labels, tag, num_nei);
				
	      for (j=0; j<num_nei; j++)
		nei[j] = temp[tag[j]];
	    }
	  else
	    {
	      for (j=0; j<num_nei; j++) 
		nei[j] = temp[j];
	    }
	}
    }
  else
    {
      nei[0] = &a;
      num_nei = num_nei+1;
    }

  return;
}


void Node::is_surf()
{
  surf = true;
  return;
}


void Node::set_coord(double *a)
{
  if (ThreeD==true)
    {
      new_xyz[0] = a[0];   
      new_xyz[1] = a[1];   
      new_xyz[2] = a[2];
    }
  else
    {
      new_xyz[0] = a[0];   
      new_xyz[1] = a[1];
    }

  return;
}

void Node::set_xyz(double *a)
{
  if (ThreeD==true)
    {
      xyz[0] = a[0];   
      xyz[1] = a[1];   
      xyz[2] = a[2];
    }
  else
    {
      xyz[0] = a[0];   
      xyz[1] = a[1];
    }

  return;
}


void Node::set_symm(const int a)
{
  symm = a;
  return;
}

void Node::set_new_y(const double &y)
{
  new_xyz[1] = y;
  return;
}

void Node::deform (const double & xcoord, const double& ycoord,
		   const double& zcoord)
{
  new_xyz[0] += xcoord;
  new_xyz[1] += ycoord;
  new_xyz[2] += zcoord;
  return;
}

void Node::deform_xyz (const double & xcoord, const double& ycoord)
{
  xyz[0] += xcoord;
  xyz[1] += ycoord;
  return;
}


void Node::deform (const double & xcoord, const double& ycoord)
{
  new_xyz[0] += xcoord;
  new_xyz[1] += ycoord;
  return;
}

void Node::set_ind(const int& a)
{
  ind = a;
  return;
}

void Node::set_ifoil(const int& a)
{
  ifoil = a;
  return;
}

void Node::overlaps(Node* a)
{
  overlap = a;
  //surf = true;
  return;
}


