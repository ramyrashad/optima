c #########################
c ##                     ##
c ##  subroutine xiimpl  ##
c ##                     ##
c #########################
c
      subroutine xiimpl (jdim,kdim,q,s,press,sndsp,turmu,fmu,vort,x,y,
     >   xy,xyj,xit,ds,uu,ccx,coef2x,coef4x,spect,lu,precon,akk)
c                                                                       
c     main xi integration calls                               
c                                                                       
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim*kdim*4)                                          
      dimension press(jdim*kdim),sndsp(jdim*kdim)                       
      dimension s(jdim,kdim,4),xy(jdim*kdim*4),xyj(jdim*kdim)           
      dimension xit(jdim*kdim),ds(jdim,kdim)                            
      dimension x(jdim*kdim),y(jdim*kdim),turmu(jdim*kdim)              
      dimension vort(jdim*kdim),fmu(jdim*kdim)                          
c                                                                       
      dimension spect(jdim,kdim,3),precon(jdim,kdim,6)                  
      dimension uu(jdim*kdim),ccx(jdim*kdim)                            
      dimension coef2x(jdim*kdim),coef4x(jdim*kdim)                     
      double precision lu(jdim,kdim,4,5)
c                                                                       
      common/scratch/ a,b,c,d,e,work1
      common/newwork/ dj,ddj

      dimension a(maxpen,4),b(maxpen,16),c(maxpen,16)
      dimension d(maxjk,16),e(maxpen,4),work1(maxjk,10)
      dimension dj(maxpen,3,4),ddj(maxpen,3,4)
      double precision akk
c
c                                                                       
c                    5. scalar pentadiagonal algorithm
c=====================================================================
c      if(meth.eq.1 .or. meth.eq.3 .or. meth.eq.4 .or. meth.eq.5)then  
      if (meth.ne.2) then
c                                                                       
c
c---- 5-a-1. eigenvec. inverse * rhs ----
c     :                -1                                         
c     :               t_xi * s   eigenvector inverse multiply by rhs   
c     :                          call with 1,2 for xi metrics    
c     : !!! note : stepf2dx  must come before stepf2dy because we invert
c     : xi first and then eta.
c     :
      if(.not.cmesh .and. .not.orderxy)then                           
         ixx = 3                                               
         ixy = 4                                               
         iyx = 1                                               
         iyy = 2                                               
         call ninver(jdim,kdim,s,xy,ixx,ixy,iyx,iyy)           
      elseif (cmesh .or. orderxy)then                                   
c         if (prec.gt.0) call gaminv(jdim,kdim,q,xyj,sndsp,precon,s)
         ix = 1                                                    
         iy = 2                                                    
         if (prec.gt.0) then
            call gamtinv(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy,uu,precon)
         else
            call tkinv(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy,precon)
         endif
      endif                                                            
c
c
c---- 5-a-2.  formation and inversion of scalar-penta ----
      if (meth.eq.4 .or. meth.eq.5) then
         call stepf2dxlu(jdim,kdim,q,turmu,fmu,s,press,sndsp,xy,xyj,ds,
     >                        coef2x,coef4x,uu,ccx,work1,spect,lu,akk)
c
         if (meth.eq.4) then
            do 30 n=1,4
               call xludcmp3(lu(1,1,n,2),lu(1,1,n,3),lu(1,1,n,4),
     *                      jdim,kdim,jlow,jup,klow,kup)
               call xlubcsb3(lu(1,1,n,2),lu(1,1,n,3),lu(1,1,n,4),
     *                      s(1,1,n),jdim,kdim,jlow,jup,klow,kup)   
  30        continue
         else
            do 40 n=1,4
               call xludcmp(lu(1,1,n,1),lu(1,1,n,2),lu(1,1,n,3),
     *                      lu(1,1,n,4),lu(1,1,n,5),jdim,kdim,
     *                      jlow,jup,klow,kup)
               call xlubcsb(lu(1,1,n,1),lu(1,1,n,2),lu(1,1,n,3),
     *                      lu(1,1,n,4),lu(1,1,n,5),s(1,1,n),jdim,kdim,
     *                      jlow,jup,klow,kup)   
 40         continue
         endif
      else
        call stepf2dx(jdim,kdim,q,turmu,fmu,s,press,sndsp,xy,xyj,x,y,
     &        ds,coef2x,coef4x,uu,ccx,work1,spect,precon,akk)   
      endif
c
c
c---- for cmesh close tk_xi here ----
      if (cmesh .or. .not.orderxy .or. prec.gt.0)then
         ix = 1                                                    
         iy = 2                                                    
         call tk (jdim,kdim,q,sndsp,s,xyj,xy,ix,iy,precon)
      endif                                                           
csi   add terms to lhs for Nhat_d
      if ((meth.eq.1 .or. meth.eq.4 .or. meth.eq.5)
     >                                      .and. ismodel.gt.1) then
         fct1=1.d0
         fct=1.5d0*dt/dt2 
         if (ismodel.eq.2) then
            if (jesdirk.eq.4 .or. jesdirk .eq.3) then
               fct1=1.0d0/(akk*dt2)
               fct=dt/(akk*dt2)
            else
               fct1=1.5d0/dt2
               fct=1.5d0*dt/dt2 
            endif
         endif

         do 50 n=1,4
         do 50 k=klow,kup
         do 50 j=jlow,jup
             s(j,k,n)=s(j,k,n)*(fct1 + fct*ds(j,k))
 50      continue
      endif
c
c
c
c                     6. block algorithms 
c=====================================================================
      elseif (meth.eq.2) then
c                                                                       
c
c---- 6-a.  formation and inversion of block-matrix ----
      call stepx(jdim,kdim,q,s,press,sndsp,turmu,fmu,   
     >           ds,xy,xyj,xit,coef2x,coef4x,a,b,c,d,e,dj,ddj)   
c
      endif
c
c
      return                                             
      end                                                


