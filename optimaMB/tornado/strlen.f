C*********************************************************************
C*********************************************************************
      integer function strlen(string)

C     ARC2D V3.00 by Thomas H. Pulliam
C     NASA Ames Research Center
C     Copyright 1992 
C     Restricted to United States (NO Foreign Dissemination!!)

      character*(*) string
      integer start,end,slen,j,n

c     returns the number of characters in string, ignoring trailing blanks
c     As a side effect, it removes leading blanks by left shifting the 
c     entire string.

      n = len(string)
      do 5 j = n,1,-1
        if (string(j:j) .ne. ' ') start=j
5     continue

      do 10 j = 1,n
        if (string(j:j) .ne. ' ') end=j
10    continue
      slen = end-start+1

      if (start .ne. 1) then
         string(1:slen) = string(start:end)
         string(slen+1:n) = '                                         '       
      endif

      strlen = slen

      return
      end
