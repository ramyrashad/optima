c *******************************************************************
c ** subroutine to setup pointers                                  **
c ** for multi-block execution                                     **
c *******************************************************************
c calling routine: ioall
c
      subroutine setptr

      implicit none

      integer nbs,ltmpc,i,ltmp,laatmp,ip1
c            lgptr   :   grid pointer
c            lqptr   :   q pointer
c            lspptr  :   spect (radius) pointer
c            lwf1ptr :   work1 pointer for filterx work1(1)
c            lwf2ptr :   work1 pointer for filterx work1(2)
c            lwf3ptr :   work1 pointer for filterx work1(3)
c            lgqptr  :   pointer for gradients for Baldwin-Barth turb model
c
c            lspptr  :   pointer for spectral radius used in matrix
c                        dissipation
c            lprecptr :  pointer for array precon used in preconditioner
c            ldhatqptr:  dhatq pointer for ESDIRK routines
c            ldhattptr:  dhatt pointer for ESDIRK routines
c *******************************************************************
#include "../include/parms.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"

      if (mg .or. gseq) then
        nbs=nblkstot
      else
        nbs=nblks
      endif
c
      lgptr(1) = 1
      ltmpc = (jbmax(1)+2*nhalo)*(kbmax(1)+2*nhalo)
      lqptr(1) = lgptr(1)
      lspptr(1)= lgptr(1)
      lprecptr(1) = lgptr(1)
      lwf1ptr(1) = lgptr(1)
      lwf2ptr(1) = lgptr(1) + ltmpc
      lwf3ptr(1) = lwf2ptr(1) + ltmpc
      laaptr(1) = 1
      lgqptr(1) = 1
cmt ~~~
      ldhatqptr(1) = 1
      ldhattptr(1) = 1
cmt ~~~
      do i=1,nbs-1
        ltmp = (jbmax(i)+2*nhalo)*(kbmax(i)+2*nhalo)
        laatmp = (jbmax(i)+2*nhalo+1)*(kbmax(i)+2*nhalo+1)
        ip1 = i + 1
        ltmpc = (jbmax(ip1)+2*nhalo)*(kbmax(ip1)+2*nhalo)
        lgptr(ip1) = lgptr(i) + ltmp
        lqptr(ip1) = lqptr(i) + 4*ltmp
        lspptr(ip1)= lspptr(i) + 3*ltmp
        lprecptr(ip1)= lprecptr(i) + 8*ltmp
        lwf1ptr(ip1) = lwf1ptr(i) + 3*ltmp
        lwf2ptr(ip1) = lwf1ptr(ip1) + ltmpc
        lwf3ptr(ip1) = lwf2ptr(ip1) + ltmpc
        laaptr(ip1) = laaptr(i) + laatmp
        lgqptr(ip1) = lgqptr(i) + 4*laatmp
cmt ~~~
        ldhatqptr(ip1) = ldhatqptr(i) + 6*4*ltmp
        ldhattptr(ip1) = ldhattptr(i) + 6*ltmp
cmt ~~~
      end do

      return
      end                       ! setptr
