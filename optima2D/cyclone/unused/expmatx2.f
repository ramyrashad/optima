      subroutine expmatx2(jdim,kdim,q,coef2x,coef4x,sndsp,s,
     > spect,xyj,press,ccx,uu,xy,x,y,work,s1,s2,u,v,phi,h,temp,temp2)
c
      include '../include/arcom.inc'

      integer g,t
      dimension q(jdim,kdim,4),coef2x(jdim,kdim),coef4x(jdim,kdim)
      dimension sndsp(jdim,kdim),s(jdim,kdim,4),work(4,jdim,kdim,2)
      dimension spect(jdim,kdim,3),xyj(jdim,kdim),press(jdim,kdim)
      dimension ccx(jdim,kdim),uu(jdim,kdim),xy(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension temp(4,jdim,kdim),temp2(4,jdim,kdim)
      dimension s1(jdim,kdim),s2(jdim,kdim),u(jdim,kdim),v(jdim,kdim)
      dimension phi(jdim,kdim),h(jdim,kdim)
c
      dtd=dt/(1.+phidt)
c     (q(j+1)-q(j))
      do 25 i=1,4
         do 14 k=klow,kup
         do 14 j=jbegin,jup
            jp1=jplus(j)
            work(i,j,k,1)= q(jp1,k,i)*xyj(jp1,k) - q(j,k,i)*xyj(j,k)
 14      continue
c            
         if (.not.periodic) then
            do 15 k=klow,kup
               work(i,jend,k,1)=work(i,jend-1,k,1)
 15         continue
         endif
c     
c       (q(j+2)-3q(j+1)+3q(j)-q(j-1))
         do 18 k=klow,kup
         do 18 j=jlow,jup
         jp1=jplus(j)
         jm1=jminus(j)
         work(i,j,k,2)=work(i,jp1,k,1)-2.*work(i,j,k,1)+work(i,jm1,k,1)
  18     continue
c      
c        boundary conditions a la Kyle
         if (.not.periodic) then
            do 20 k=klow,kend
               work(i,jbegin,k,2)=q(jbegin+2,k,i)*xyj(jbegin+2,k)
     &                            -2.d0*q(jbegin+1,k,i)*xyj(jbegin+1,k)
     &                            +q(jbegin,k,i)*xyj(jbegin,k)
               work(i,jend,k,2)=0.0
 20         continue
         endif
 25   continue
c     
c     
      do 100 k=klow,kup
        do 98 j=jbegin,jend
            s1(j,k)=.5d0*(spect(j,k,2)+spect(j,k,3)-2.*spect(j,k,1))
            s2(j,k)=(spect(j,k,2)-spect(j,k,3))/
     &                                      (2.*ccx(j,k))
c     &                                      (2.*sndsp(j,k)*ccx(j,k))
            u(j,k)=q(j,k,2)/q(j,k,1)
            v(j,k)=q(j,k,3)/q(j,k,1)
            phi(j,k)=.5d0*(u(j,k)**2+v(j,k)**2)         
            h(j,k)=(q(j,k,4)+press(j,k))/q(j,k,1)
 98     continue
        do 99 j=jbegin,jup
            jj=j+1
            s1(j,k)=s1(j,k) + .5d0*(spect(jj,k,2)+spect(jj,k,3)-
     &                            2.*spect(jj,k,1))
            s2(j,k)=s2(j,k) + (spect(jj,k,2)-spect(jj,k,3))/
     &                                      (2.*ccx(jj,k))
c     &                                      (2.*sndsp(jj,k)*ccx(jj,k))
            u(j,k)=u(j,k) + q(jj,k,2)/q(jj,k,1)
            v(j,k)=v(j,k) + q(jj,k,3)/q(jj,k,1)
            phi(j,k)=phi(j,k) + .5d0*(u(jj,k)**2+v(jj,k)**2)         
            h(j,k)=h(j,k) + (q(jj,k,4)+press(jj,k))/q(jj,k,1)
 99     continue
 100  continue
c     
      if (iord.eq.4 .and. .not.periodic) then
        jl=jlow+1
        ju=jup-1
      else
        jl=jlow
        ju=jup
      endif
c
      if (flbud .and.
     &  (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
        do 105 k=kbegin,kend
        do 105 j=jbegin,jend
          budget(j,k,2)=0.d0
 105    continue
      endif

c     -if you didn't have to deal with periodic flows you could
c      just use t=j+g instead of t=jplus(j+g-1).
      do 888 g=0,0
         do 111 k=klow,kup
         do 111 j=jbegin,jup
            t=jplus(j+g-1)
            include 'matloopx.inc'
 111     continue
c      
         if (flbud .and.
     &      (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
            do 322 n=1,4
              do 240 k=klow,kup
              do 240 j=jl,ju
                t=jplus(j+g-1)
                tm1=jminus(t)
                jm1=jminus(j)
c              
                zz=dtd*(
     &          coef2x(t,k)*temp(n,j,k)-coef2x(tm1,k)*temp(n,jm1,k) -
     &          (coef4x(t,k)*temp2(n,j,k)-coef4x(tm1,k)*temp2(n,jm1,k)))
c
                s(j,k,n)=s(j,k,n) + zz
                if (n.eq.2) budget(j,k,2)=budget(j,k,2)+zz
 240          continue
 322        continue
         else
            do 222 n=1,4
              do 140 k=klow,kup
              do 140 j=jl,ju
                t=jplus(j+g-1)
                tm1=jminus(t)
                jm1=jminus(j)
c              
                s(j,k,n)=s(j,k,n) + dtd*(
     &             (coef2x(t,k)+coef2x(t+1,k))*temp(n,j,k)
     &                 -(coef2x(tm1,k)+coef2x(t,k))*temp(n,jm1,k) -
     &             ((coef4x(t,k)+coef4x(t+1,k))*temp2(n,j,k)
     &                 -(coef4x(tm1,k)+coef4x(t,k))*temp2(n,jm1,k)))
 140          continue
 222        continue
          endif
 888  continue
c
c
      if (.not.periodic .and. iord.eq.4) then
c     Boundary Conditions
         do 2000 n=1,4
         do 2100 k=klow,kup
          j=jlow
c            j=jbegin
          work(n,jbegin,k,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j+1,k,n)*
     &                       xyj(j+1,k)-q(j+2,k,n)*xyj(j+2,k)
c          work(n,j,k,2)=q(j,k,n)*xyj(j,k)-3.d0*(q(j+1,k,n)*xyj(j+1,k)
c     &                  -q(j+2,k,n)*xyj(j+2,k))-q(j+3,k,n)*xyj(j+3,k)
          j=jbegin
c          j=jlow
          work(n,jlow,k,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j+1,k,n)*xyj(j+1,k)
     &                     -q(j+2,k,n)*xyj(j+2,k)
c          work(n,j,k,2)=q(j,k,n)*xyj(j,k)-3.d0*(q(j+1,k,n)*xyj(j+1,k)
c     &                  -q(j+2,k,n)*xyj(j+2,k))-q(j+3,k,n)*xyj(j+3,k)
          j=jup-1
          work(n,j,k,2)=-q(j-1,k,n)*xyj(j-1,k)+2.d0*q(j,k,n)*xyj(j,k)
     &                  -q(j+1,k,n)*xyj(j+1,k)
c          work(n,j,k,2)=q(j+1,k,n)*xyj(j+1,k)-3.d0*(q(j,k,n)*xyj(j,k)
c     &                  -q(j-1,k,n)*xyj(j-1,k))-q(j-2,k,n)*xyj(j-2,k)
          j=jup
          work(n,j,k,2)=-q(j-1,k,n)*xyj(j-1,k)+2.d0*q(j,k,n)*xyj(j,k)
     &                  -q(j+1,k,n)*xyj(j+1,k)
c          work(n,j,k,2)=q(j+1,k,n)*xyj(j+1,k)-3.d0*(q(j,k,n)*xyj(j,k)
c     &                  -q(j-1,k,n)*xyj(j-1,k))-q(j-2,k,n)*xyj(j-2,k)
 2100    continue
 2000    continue
c     
         do 2500 g=0,0
           do 2400 k=klow,kup
             do 2200 j=jbegin,jlow
                t=j+g
                include 'matloopx.inc'
 2200        continue
             do 2250 j=jup-1,jup
                t=j+g
                include 'matloopx.inc'
 2250        continue
c     
             do 2300 n=1,4
             do 2300 j=jlow,jup,jup-jlow
              t=j+g
c
              s(j,k,n)=s(j,k,n) + dtd*(
     &        coef2x(t,k)*temp(n,j,k)-coef2x(t-1,k)*temp(n,j-1,k)
     &      - (coef4x(t,k)*temp2(n,j,k)-coef4x(t-1,k)*temp2(n,j-1,k)))
 2300        continue
 2400      continue
 2500    continue
      endif
c
      return
      end
