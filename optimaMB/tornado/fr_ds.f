c     ------------------------------------------------------------------
c     -- evaluate matrix-vector approximation --
c     -- note: S = -R(Q); reverse the sign of s-sr -- 
c     -- m. nemec, sept. 2001 --
c     -- calling routine: framux.f --
c     ------------------------------------------------------------------
      subroutine fr_ds( ntvar, jmax, kmax, nmax, ks, ke, js, je, indx,
     &     s, sr, rsa, rsar, v1, v2, eps1, dtvc)

      implicit none
      integer ntvar, jmax, kmax, nmax, ks, ke, js, je, indx(jmax,kmax)
      integer j,k,n,jk
      double precision s(jmax,kmax,4),sr(jmax,kmax,4),rsa(jmax,kmax)
      double precision rsar(jmax,kmax),v2(ntvar)
      double precision eps1
c     -- time vector in matrix-vector product --
      double precision dtvc(*), v1(ntvar)

      do n = 1,4
         do k = ks,ke
            do j = js,je
               jk = ( indx(j,k)-1 )*nmax + n
               v2(jk) = -eps1*( s(j,k,n) - sr(j,k,n) )
c        add the time step pointwise times v1
               v2(jk) = v2(jk) + v1(jk)*dtvc(jk)
            end do
         end do
      end do

      if (nmax.eq.5) then
         do k = ks,ke
            do j = js,je
               jk = ( indx(j,k)-1 )*5 + 5
               v2(jk) = -eps1*( rsa(j,k) - rsar(j,k) )
c        add the time step pointwise times v1
               v2(jk) = v2(jk) + v1(jk)*dtvc(jk)
            end do
         end do
      end if


      return
      end                       ! fr_ds
