      subroutine bcwake(jdim,kdim,q,press,xy,xit,ett,xyj,x,y)          
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c                                                                       
c                                                                       
      gm1i=1.d0/gami
c     bc for wake cut in a c mesh                                     
c                                                                       
c     ---------------------------------------------------------------
c                  permeable boundary condition  (wake)          
c     ---------------------------------------------------------------
c                                                                       
c
      js=jtail1-1
      if (iord.eq.4) then
         if( js.lt.2) go to 150                                         
         tmp=1.d0/6.d0
         do 10 j=jbegin,js                                           
            i = jend +1 - j                                            
            rrj = 1.d0/xyj(j,1)                                          
            rri = 1.d0/xyj(i,1)
c
            rhou2=q(i,3,1)*xyj(i,3)
            rhou =q(i,2,1)*xyj(i,2)
            rhol =q(j,2,1)*xyj(j,2)
            rhol2=q(j,3,1)*xyj(j,3)
            qsav = tmp*(-rhol2 + 4.d0*(rhol + rhou) -rhou2)     
c            qsav = 0.5d0*(rhol + rhou)     
            q(j,1,1) = qsav*rrj                                      
            q(i,1,1) = qsav*rri                                      
c
            uu2=q(i,3,2)*xyj(i,3)
            uu =q(i,2,2)*xyj(i,2)
            ul =q(j,2,2)*xyj(j,2)
            ul2=q(j,3,2)*xyj(j,3)
            qsav = tmp*(-ul2 + 4.d0*(ul + uu) -uu2)
c            qsav = 0.5d0*(ul + uu)
            q(j,1,2) = qsav*rrj                                    
            q(i,1,2) = qsav*rri                                    
c
            vu2=q(i,3,3)*xyj(i,3)
            vu =q(i,2,3)*xyj(i,2)
            vl =q(j,2,3)*xyj(j,2)
            vl2=q(j,3,3)*xyj(j,3)
            qsav = tmp*(-vl2 + 4.d0*(vl + vu) -vu2)
c            qsav = 0.5d0*(vl + vu)
            q(j,1,3) = qsav*rrj                                  
            q(i,1,3) = qsav*rri
c                                                                       
c           average pressure across wake cut                        
c                                                                       
            pu2=press(i,3)*xyj(i,3)
            pu =press(i,2)*xyj(i,2)
            pl =press(j,2)*xyj(j,2)
            pl2=press(j,3)*xyj(j,3)
            psav = tmp*(-pl2 + 4.d0*(pl + pu) -pu2)
c            psav = 0.5d0*(pl + pu)
c           
            q(j,1,4) = psav*gm1i*rrj +                                 
     *                 0.5d0*(q(j,1,2)**2+q(j,1,3)**2)/q(j,1,1)          
            q(i,1,4) = psav*gm1i*rri +                                 
     *                 0.5d0*(q(i,1,2)**2+q(i,1,3)**2)/q(i,1,1)          
   10       continue                                                    
  150    continue                                                       
      else
         if( js.lt.2) go to 350                                         
         do 20 j=jbegin,js                                           
            i = jend +1 - j                                            
            rrj = 1.d0/xyj(j,1)                                          
            rri = 1.d0/xyj(i,1)
c
            rhou=q(i,2,1)*xyj(i,2)
            rhol=q(j,2,1)*xyj(j,2)
            qsav = 0.5d0*(rhou+rhol)
            q(j,1,1) = qsav*rrj                                      
            q(i,1,1) = qsav*rri                                      
c
            uu=q(i,2,2)*xyj(i,2)
            ul=q(j,2,2)*xyj(j,2)
            qsav = 0.5d0*(uu+ul)
            q(j,1,2) = qsav*rrj                                    
            q(i,1,2) = qsav*rri
c                                    
            vu=q(i,2,3)*xyj(i,2)
            vl=q(j,2,3)*xyj(j,2)
            qsav = 0.5d0*(vu+vl)
            q(j,1,3) = qsav*rrj                                  
            q(i,1,3) = qsav*rri                                  
c                                                                       
c           average pressure across wake cut                           
c                                                                       
            psav = 0.5d0*(press(j,2)*xyj(j,2) + press(i,2)*xyj(i,2))
c           
            q(j,1,4) = psav*gm1i*rrj +                                 
     *                 0.5d0*(q(j,1,2)**2+q(j,1,3)**2)/q(j,1,1)          
            q(i,1,4) = psav*gm1i*rri +                                 
     *                 0.5d0*(q(i,1,2)**2+q(i,1,3)**2)/q(i,1,1)          
   20       continue                                                    
  350    continue                                                       
      endif
c                                                                       
      return                                                           
      end                                                              
