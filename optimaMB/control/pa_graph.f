c     -----------------------------------------------------------------
c     -- setup of preconditioner pointer arrays ipa and jpa --
c     -- m. nemec, aug. 2001 --
c     -----------------------------------------------------------------
      subroutine pa_int ( indx, jmax, kmax, nmax, js, je, ks, ke, jpa,
     &     ipa, icol) 

      implicit none
      integer jmax,kmax,nmax,js,je,ks,ke
      integer m,jm1,jp1,km1,kp1,j,k,n,jk,jj
      integer indx(jmax,kmax), jpa(*), ipa(*), icol(*)

c     -- interior nodes --

      do k = ks,ke
         do j = js,je
            do n = 1,nmax

               jk = ( indx(j,k) - 1 )*nmax + n
               jj = ( jk - 1 )*icol(5)

               jm1 = ( indx(j-1,k) - 1 )*nmax
               jp1 = ( indx(j+1,k) - 1 )*nmax
               km1 = ( indx(j,k-1) - 1 )*nmax
               kp1 = ( indx(j,k+1) - 1 )*nmax

               do m = 1,nmax
                  jpa(jj+        m) = jm1 + m 
                  jpa(jj+icol(1)+m) = km1 + m
                  jpa(jj+icol(2)+m) = jk  - n + m
                  jpa(jj+icol(3)+m) = kp1 + m
                  jpa(jj+icol(4)+m) = jp1 + m
               end do
               ipa(jk) = 5*nmax
            end do
         end do
      end do

      return
      end                       !pa_int
c     -----------------------------------------------------------------
      subroutine pa_bc( ib, iside, nmax, indx, jpa, ipa, icol)

      implicit none
      integer ib,iside,nmax
      integer k1,k2,k3,j1,j2,j3,ibn,isn
      integer indx(*), jpa(*), ipa(*), icol(*)

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"

      if (ibctype(ib,iside).eq.1) then
c     -- B.C. body surface --
         if (.not.viscous) then

            if (iside.eq.1) then
               k1 = kminbnd(ib)
               k2 = k1+1
               k3 = k1+2
               call pbcbi( k1, k2, k3, jminbnd(ib), jmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, jpa, ipa,
     &              icol)

            else if (iside.eq.3) then
               k1 = kmaxbnd(ib)
               k2 = k1-1
               k3 = k1-2
               call pbcbi( k1, k2, k3, jminbnd(ib), jmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, jpa, ipa,
     &              icol)

            else if (iside.eq.2) then
               j1 = jminbnd(ib) 
               j2 = j1+1
               j3 = j1+2
               call pbcbi24( j1, j2, j3, kminbnd(ib), kmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, jpa, ipa,
     &              icol)

            else
               write(*,*) 'order: problem on body (inviscid)'
               stop
            end if
         else                   !viscous flow
            if (iside.eq.1) then
               k1 = kminbnd(ib)
               k2 = k1+1
               call pbcbv( k1, k2, jminbnd(ib), jmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, jpa, ipa,
     &              icol)

            else if (iside.eq.3) then
               k1 = kmaxbnd(ib) 
               k2 = k1 - 1
               call pbcbv( k1, k2, jminbnd(ib), jmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, jpa, ipa,
     &              icol)

            else if (iside.eq.2) then
               j1 = jminbnd(ib) 
               j2 = j1+1
               call pbcbv24( j1, j2, kminbnd(ib), kmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, jpa, ipa,
     &              icol)

            else
               write(*,*) 'order: problem on body (viscous)'
               stop
            end if
         end if
      else if (ibctype(ib,iside).eq.2) then
c     -- B.C. inviscid far field (Riemann) --
         if (iside .eq. 1) then
            k1 = kminbnd(ib)
            k2 = k1+1
            call bc2s13( k1, k2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &           nmax, jminbnd(ib), jmaxbnd(ib), jpa, ipa, icol)
         else if (iside .eq. 2) then  
            j1 = jminbnd(ib)
            j2 = j1+1
            call bc2s24( j1, j2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &           nmax, kminbnd(ib)+1, kmaxbnd(ib)-1, jpa, ipa, icol)
         else if (iside .eq. 3) then
            k1 = kmaxbnd(ib)
            k2 = k1-1
            call bc2s13( k1, k2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &           nmax, jminbnd(ib), jmaxbnd(ib), jpa, ipa, icol)
         else if (iside .eq. 4) then 
            j1 = jmaxbnd(ib)
            j2 = j1-1
            call bc2s24( j1, j2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &           nmax, kminbnd(ib)+1, kmaxbnd(ib)-1, jpa, ipa, icol)
         end if

      else if (ibctype(ib,iside).eq.3) then
c     -- viscous outflow at j=jmin side 2 --
         j1 = jminbnd(ib)
         j2 = j1+1
         call bc2s24( j1, j2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &        nmax, kminbnd(ib)+1, kmaxbnd(ib)-1, jpa, ipa, icol)

      else if (ibctype(ib,iside).eq.4) then
c     -- viscous outflow at j=jmax side 4 --
         j1 = jmaxbnd(ib)
         j2 = j1-1
         call bc2s24( j1, j2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &        nmax, kminbnd(ib)+1, kmaxbnd(ib)-1, jpa, ipa, icol)
         
      else if (ibctype(ib,iside).eq.5 ) then
c     -- average BC across the boundary --
         if (iside .eq. 1 .or. iside .eq. 3) then
            ibn = lblkpt(ib,iside)
            isn = lsidept(ib,iside)
            call bc5s13( iside, isn, indx(lgptr(ib)), jmax(ib),
     &           kmax(ib), indx(lgptr(ibn)), jmax(ibn), kmax(ibn),
     &           jminbnd(ib), jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib),
     &           kminbnd(ibn), kmaxbnd(ibn), nmax, jpa, ipa, icol)
         else
            write (*,*) 'order: ibctype = 5'
            write (*,*) 'side != 1 or 3'
            stop
         end if
      end if

      return
      end                       !pa_bc
c     -----------------------------------------------------------------
      subroutine pa_sng( js1, ks1, js2, ks2, javg1, kavg1, javg2,
     &     kavg2, indx1, jmax1, kmax1, indx2, jmax2, kmax2, nmax, jpa,
     &     ipa, icol)

      implicit none
      integer js1,ks1,js2,ks2,javg1,kavg1,javg2,kavg2,jmax1,kmax1
      integer jmax2,kmax2,nmax,m,n,jk,jk1,jk2,jj,jk0
      integer indx1(jmax1,kmax1), indx2(jmax2,kmax2)
      integer jpa(*), ipa(*), icol(*) 

c     -- leading edge sindular points --

c     -- overwrite entries in sparse matrix --
      do n = 1,nmax

         jk = ( indx1(js1,ks1) - 1 )*nmax + n
         jj = (jk-1)*icol(5)

         jk0 = ( indx1(js1,ks1) - 1 )*nmax
         jk1 = ( indx1(javg1,kavg1) - 1 )*nmax
         jk2 = ( indx2(javg2,kavg2) - 1 )*nmax

         do m = 1,nmax
            jpa(jj+        m) = jk0 + m
            jpa(jj+icol(1)+m) = jk1 + m
            jpa(jj+icol(2)+m) = jk2 + m
            jpa(jj+icol(3)+m) = 0
            jpa(jj+icol(4)+m) = 0
         end do
         ipa(jk) = 3*nmax
      end do   

      do n = 1,nmax

         jk = ( indx2(js2,ks2) - 1 )*nmax + n
         jj = (jk-1)*icol(5)

         jk0 = ( indx2(js2,ks2) - 1 )*nmax
         jk1 = ( indx2(javg2,kavg2) - 1 )*nmax
         jk2 = ( indx1(javg1,kavg1) - 1 )*nmax

         do m = 1,nmax
            jpa(jj+        m) = jk0 + m
            jpa(jj+icol(1)+m) = jk1 + m
            jpa(jj+icol(2)+m) = jk2 + m
            jpa(jj+icol(3)+m) = 0
            jpa(jj+icol(4)+m) = 0
         end do
         ipa(jk) = 3*nmax
      end do   

      return
      end                       !pa_sng
c     -----------------------------------------------------------------
      subroutine pa_cop( ijte1, ikte1, ijte2, ikte2, indx1, jmax1,
     &     kmax1, indx2, jmax2, kmax2, nmax, jpa, ipa, icol)

      implicit none
      integer ijte1,ikte1,ijte2,ikte2,jmax1,kmax1,jmax2,kmax2,nmax
      integer m,n,jk,jj,jk1,jk0
      integer indx1(jmax1,kmax1), indx2(jmax2,kmax2)
      integer jpa(*), ipa(*), icol(*) 

c     -- copy points --

c     -- overwrite previous entries in sparse matrix --
      do n = 1,nmax

         jk = ( indx2(ijte2,ikte2) - 1 )*nmax + n
         jj = (jk-1)*icol(5)

         jk0 = ( indx2(ijte2,ikte2) - 1)*nmax
         jk1 = ( indx1(ijte1,ikte1) - 1)*nmax

         do m = 1,nmax
            jpa(jj+        m) = jk0 + m
            jpa(jj+icol(1)+m) = jk1 + m
            jpa(jj+icol(2)+m) = 0
            jpa(jj+icol(3)+m) = 0
            jpa(jj+icol(4)+m) = 0
         end do
         ipa(jk) = 2*nmax
      end do            

      return
      end                       !pa_cop
c     ------------------------------------------------------------------
c     ------------------------------------------------------------------
c     ------------------------------------------------------------------
      subroutine pbcbi( k1, k2, k3, js, je, indx, jmax, kmax, nmax, jpa,
     &     ipa, icol)

      implicit none
      integer k1, k2, k3, js, je, jmax, kmax, nmax
      integer jk,j,n,jj,kp0,kp1,kp2,m
      integer indx(jmax,kmax), jpa(*), ipa(*), icol(*)

      do j = js,je
         do n = 1,nmax
            jk = ( indx(j,k1) - 1 )*nmax + n
            jj = (jk-1)*icol(5)

            kp0 = ( indx(j,k1) - 1 )*nmax
            kp1 = ( indx(j,k2) - 1 )*nmax
            kp2 = ( indx(j,k3) - 1 )*nmax

            do m = 1,nmax
               jpa(jj+        m) = kp0 + m
               jpa(jj+icol(1)+m) = kp1 + m
               jpa(jj+icol(2)+m) = kp2 + m
            end do
            ipa(jk) = 3*nmax
         end do
      end do
     
      return 
      end                       !pbcbi
c     ------------------------------------------------------------------
      subroutine pbcbv(k1, k2, js, je, indx, jmax, kmax, nmax, jpa, ipa,
     &     icol)

      implicit none
      integer k1, k2, js, je, jmax, kmax, nmax
      integer j,n,m,jk,jj,kp0,kp1
      integer indx(jmax,kmax), jpa(*), ipa(*), icol(*)

      do j = js,je
         do n = 1,nmax
            jk = ( indx(j,k1) - 1 )*nmax + n
            jj = (jk-1)*icol(5)

            kp0 = ( indx(j,k1) - 1 )*nmax
            kp1 = ( indx(j,k2) - 1 )*nmax

            do m = 1,nmax
               jpa(jj+        m) = kp0 + m
               jpa(jj+icol(1)+m) = kp1 + m
            end do
            ipa(jk) = 2*nmax
         end do
      end do

      return 
      end                       !pbcbv
c     ------------------------------------------------------------------
      subroutine bc2s13( k1, k2, indx, jmax, kmax, nmax, js, je, jpa,
     &     ipa, icol)

      implicit none
      integer k1, k2, jmax, kmax, nmax, js, je
      integer j,n,m,jk,jj,kp0,kp1
      integer indx(jmax,kmax), jpa(*), ipa(*), icol(*)

      do j = js,je
         do n = 1,nmax
            jk = ( indx(j,k1) - 1 )*nmax + n
            jj = (jk-1)*icol(5)

            kp0 = ( indx(j,k1) - 1 )*nmax
            kp1 = ( indx(j,k2) - 1 )*nmax

            do m = 1,nmax
               jpa(jj+        m) = kp0 + m
               jpa(jj+icol(1)+m) = kp1 + m
            end do
            ipa(jk) = 2*nmax
         end do
      end do

      return 
      end                       !bc2s13
c     ------------------------------------------------------------------
      subroutine bc2s24( j1, j2, indx, jmax, kmax, nmax, ks, ke, jpa,
     &     ipa, icol)

      implicit none
      integer j1, j2, jmax, kmax, nmax, ks, ke
      integer k,n,jk,jj,jp0,jp1,m
      integer indx(jmax,kmax), jpa(*), ipa(*), icol(*)

      do k = ks,ke
         do n = 1,nmax
            jk = ( indx(j1,k) - 1 )*nmax + n
            jj = (jk-1)*icol(5)

            jp0 = ( indx(j1,k) - 1 )*nmax
            jp1 = ( indx(j2,k) - 1 )*nmax

            do m = 1,nmax
               jpa(jj+        m) = jp0 + m
               jpa(jj+icol(1)+m) = jp1 + m
            end do
            ipa(jk) = 2*nmax
         end do     
      end do

      return 
      end                       !bc2s24
c     -----------------------------------------------------------------
      subroutine bc5s13( is1, is2, indx, jmax, kmax, indxn, jmaxn,
     &     kmaxn, js, je, ks1, ke1, ks2, ke2, nmax, jpa, ipa, icol)

      implicit none
      integer is1,is2,jmax,kmax,jmaxn,kmaxn,js,je,ks1,ke1,ks2,ke2,nmax
      integer k1,k2,k3,if1,if2,j,n,m,jk,jj,kp0,kp1,kp2,jn
      integer indx(jmax,kmax), indxn(jmaxn,kmaxn)
      integer jpa(*), ipa(*), icol(*) 

c     -- logic to treat c- and h-grids --
      if ( is1.eq.1 ) then
         k1 = ks1
         k2 = ks1 + 1
      else                      ! is1=3
         k1 = ke1
         k2 = ke1 - 1 
      end if      

      if ( is2.eq.1 ) then
         k3 = ks2 + 1
      else                      ! is2=3 
         k3 = ke2 - 1
      end if

      if ( is1.eq.is2 ) then
         if1 = 1
         if2 = -1
      else
         if1 = 0
         if2 = 1
      end if     

      do j = js,je
         do n = 1,nmax
            jk = ( indx(j,k1) - 1 )*nmax + n
            jj = (jk-1)*icol(5)
            
            jn  = if1*(jmaxn + 1) + if2*j

            kp0 = ( indx(j,k1) - 1 )*nmax
            kp1 = ( indx(j,k2) - 1 )*nmax
            kp2 = ( indxn(jn,k3) - 1 )*nmax

            do m = 1,nmax
               jpa(jj+        m) = kp0 + m
               jpa(jj+icol(1)+m) = kp1 + m
               jpa(jj+icol(2)+m) = kp2 + m
            end do               
            ipa(jk) = 3*nmax
         end do
      end do

      return 
      end                       !bc5s13
c     -----------------------------------------------------------------


      subroutine pbcbv24(j1, j2, ks, ke, indx, jmax, kmax, nmax, jpa, 
     &     ipa, icol)

      implicit none
      integer j1, j2, ks, ke, jmax, kmax, nmax
      integer k,n,m,jk,jj,jp0,jp1
      integer indx(jmax,kmax), jpa(*), ipa(*), icol(*)

      do k = ks,ke
         do n = 1,nmax
            jk = ( indx(j1,k) - 1 )*nmax + n
            jj = (jk-1)*icol(5)

            jp0 = ( indx(j1,k) - 1 )*nmax
            jp1 = ( indx(j2,k) - 1 )*nmax

            do m = 1,nmax
               jpa(jj+        m) = jp0 + m
               jpa(jj+icol(1)+m) = jp1 + m
            end do
            ipa(jk) = 2*nmax
         end do
      end do

      return 
      end                       !pbcbv24
c     ------------------------------------------------------------------
      subroutine pbcbi24( j1, j2, j3, ks, ke, indx, jmax, kmax, nmax, 
     &     jpa, ipa, icol)

      implicit none
      integer j1, j2, j3, ks, ke, jmax, kmax, nmax
      integer k,n,jk,jj,jp0,jp1,jp2,m
      integer indx(jmax,kmax), jpa(*), ipa(*), icol(*)

      do k = ks,ke
         do n = 1,nmax
            jk = ( indx(j1,k) - 1 )*nmax + n
            jj = (jk-1)*icol(5)

            jp0 = ( indx(j1,k) - 1 )*nmax
            jp1 = ( indx(j2,k) - 1 )*nmax
            jp2 = ( indx(j3,k) - 1 )*nmax

            do m = 1,nmax
               jpa(jj+        m) = jp0 + m
               jpa(jj+icol(1)+m) = jp1 + m
               jpa(jj+icol(2)+m) = jp2 + m
            end do
            ipa(jk) = 3*nmax
         end do
      end do
     
      return 
      end                       !pbcbi24
