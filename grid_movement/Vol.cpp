#ifndef VOL_H
#include "Vol.h"
#endif


// Definition of default constructor for hexahedral element
// 8 node linear brick element
Vol::Vol()
{
	ID = 0;
	dim = 1;
	vertexID = NULL;
	nvertices = 0;
}


Vol::Vol(Node** corners, const int& cellID, const int& nv)
{
  int i;  // counter
  ID = cellID;

  dim = 3;
  nvertices = nv;
  vertexID = new int[nvertices];

  Node* overlapind;

  for (i=0; i<nvertices; i++)
    {
      overlapind  = corners[i]->overlap;
      if (overlapind == NULL)
	vertexID[i] = corners[i]->ID;
      else
	vertexID[i] = overlapind->ID;
    }

  overlapind = NULL;
  return; 
}


// Destructor - takes no action
Vol::~Vol() 
{
}


void Vol::clearmem()
{
  delete [] vertexID;
  vertexID = NULL;

}



// add neighbours to nodes that are connected by sharing the same cell volume
// for a hexahedron or tetrahedron
void Vol::add_nei (Node** corners)
{
  if (nvertices == 8)  // hexahedron
    {
  // adding opposite corners of 6 faces
  corners[0]->add_nei(*corners[2]);
  corners[2]->add_nei(*corners[0]);
  corners[1]->add_nei(*corners[3]);
  corners[3]->add_nei(*corners[1]);
		  
  corners[4]->add_nei(*corners[6]);
  corners[6]->add_nei(*corners[4]);
  corners[5]->add_nei(*corners[7]);
  corners[7]->add_nei(*corners[5]);

  corners[2]->add_nei(*corners[5]);
  corners[5]->add_nei(*corners[2]);
  corners[6]->add_nei(*corners[1]);
  corners[1]->add_nei(*corners[6]);

  corners[0]->add_nei(*corners[7]);
  corners[7]->add_nei(*corners[0]);
  corners[3]->add_nei(*corners[4]);
  corners[4]->add_nei(*corners[3]);

  corners[0]->add_nei(*corners[5]);
  corners[5]->add_nei(*corners[0]);
  corners[1]->add_nei(*corners[4]);
  corners[4]->add_nei(*corners[1]);

  corners[3]->add_nei(*corners[6]);
  corners[6]->add_nei(*corners[3]);
  corners[2]->add_nei(*corners[7]);
  corners[7]->add_nei(*corners[2]);
  
  // adding nodes at opposite corners[ of the hexahedral
  corners[0]->add_nei(*corners[6]);
  corners[6]->add_nei(*corners[0]);
  corners[1]->add_nei(*corners[7]);
  corners[7]->add_nei(*corners[1]);

  corners[4]->add_nei(*corners[2]);
  corners[2]->add_nei(*corners[4]);
  corners[5]->add_nei(*corners[3]);
  corners[3]->add_nei(*corners[5]);
    }
  else if (nvertices == 4)
    {
      corners[0]->add_nei(*corners[1]);
      corners[0]->add_nei(*corners[2]);
      corners[0]->add_nei(*corners[3]);

      corners[1]->add_nei(*corners[0]);
      corners[1]->add_nei(*corners[2]);
      corners[1]->add_nei(*corners[3]);

      corners[2]->add_nei(*corners[0]);
      corners[2]->add_nei(*corners[1]);
      corners[2]->add_nei(*corners[3]);

      corners[3]->add_nei(*corners[0]);
      corners[3]->add_nei(*corners[1]);
      corners[3]->add_nei(*corners[2]);
    }
  else
    {
      cerr << "Unknown element! " << endl;
      exit (2);
    }

  return;
}


ostream& operator <<(ostream& out, const Vol& a)
{
  out.setf(ios::showpoint);
  out.setf(ios::fixed);
  out.setf(ios::left);
  
  int i = 0;
  out << setw(10) << a.ID;
  for (i=0; i<a.nvertices; i++)
    out << setw(10) << a.vertexID[i];
  out << endl;
  return out;
}
