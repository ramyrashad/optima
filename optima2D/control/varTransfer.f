c-----------------------------------------------------------------------
c     Program name: varTransfer
c     Written By: Howard Buckley, March 2009
c     
c     This subroutine reads and writes optima2D variables to and from
c     the SNOPT user variable 'ru' so that optima2D variables 
c     are accessible while working within SNOPT
c
c-----------------------------------------------------------------------

      subroutine varTransfer(varStat,jdim,kdim,ndim,ifirst,ifun,itfirst,
     &      x,y,idv,bap,bcp,bt,bknot,indx,icol,iex,cp,xy,xyj,cp_tar,
     &      fmu,vort,turmu,ia,ja,ipa,jpa,iat,jat,ipt,jpt,as,ast,pa,
     &      pat,ru,ruIndex)

#ifdef _MPI_VERSION
      use mpi
#endif


      !-- Declare variables

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer
     &     jdim, kdim, ndim, ifirst, indx(maxj,maxk), icol(9), 
     &     iex(maxjk*4), idv(ibsnc), iren(maxjk), 
     &     ia(maxjk*nblk+2), ifun, iter, mp, varStat, ruIndex,
     &     ja(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1), 
     &     ipa(maxjk*nblk+2), jpa(maxjk*nblk2*5+1),
     &     iat(maxjk*nblk+2),
     &     jat(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1),
     &     ipt(maxjk*nblk+2), jpt(maxjk*nblk2*5+1), itfirst,
     &     i, j, k, l, m, n

      double precision
     &     q(maxjk,nblk), cp(maxjk),
     &     xy(maxjk,4), xyj(maxjk), x(maxj,maxk), y(maxj,maxk),
     &     vort(maxj,maxk),
     &     turmu(maxj,maxk),  fmu(maxj,maxk), 
     &     cp_tar(ibody,2), dvs(ibsnc), bt(ibody), bknot(ibskt),
     &     bap(ibody,2), bcp(ibsnc,2),
     &     as(maxj*maxk*nblk*(nblk*9+1)+maxj*nblk*3+1), 
     &     ast(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1),
     &     pa(maxjk*nblk2*5+1), 
     &     pat(maxjk*nblk2*5+1), cp_his(ibody,300,nopc),
     &     ac_his(ibody,300),  
     &     dvscale(ibsnc), 
     &     obj0s(mpopt), 
     &     qwarm(warmset,maxj,maxk,nblk,nopc),
     &     ru(30000000)

      i = 1

      if (varStat.eq.0) then

         !-- Write optima2D variables to ru

         ru(i) = jdim
         i = i+1
         ru(i) = kdim
         i = i+1
         ru(i) = ndim
         i = i+1
c         ru(i) = ifirst
c         i = i+1
c         ru(i) = ifun
c         i = i+1
         ru(i) = itfirst
         i = i+1
      
         do j=1,maxj
            do k=1,maxk
               ru(i) = x(j,k)
               i = i+1
            end do
         end do

         do j=1,maxj
            do k=1,maxk
               ru(i) = y(j,k)
               i = i+1
            end do
         end do

         do j=1,ibsnc
            ru(i) = idv(j)
            i = i+1
         end do

         do j=1,ibody
            do k=1,2
               ru(i) = bap(j,k)
               i = i+1
            end do
         end do

         do j=1,ibsnc
            do k=1,2
               ru(i) = bcp(j,k)
               i=i+1
            end do
         end do

         do j=1,ibody
            ru(i) = bt(j)
            i = i+1
         end do

         do j=1,ibskt
            ru(i) = bknot(j)
            i=i+1
         end do

         do j=1,maxj
            do k=1,maxk
               ru(i) = indx(j,k)
               i=i+1
            end do
         end do

         do j=1,9
            ru(i) = icol(j)
            i=i+1
         end do

         do j=1,maxjk*4
            ru(i) = iex(j)
            i=i+1
         end do

         do j=1,maxjk
            ru(i) = cp(j)
            i=i+1
         end do

c         do j=1,maxjk
c            do k=1,4
c               ru(i) = xy(j,k)
c               i=i+1
c            end do
c         end do

         do j=1,maxjk
            ru(i) = xyj(j)
            i=i+1
         end do

         do j=1,ibody
            do k=1,2
               ru(i) = cp_tar(j,k)
               i=i+1
            end do
         end do

         do j=1,maxj
            do k=1,maxk
               ru(i) = fmu(j,k)
               i=i+1
            end do
         end do

         do j=1,maxj
            do k=1,maxk
               ru(i) = vort(j,k)
               i=i+1
            end do
         end do

         do j=1,maxj
            do k=1,maxk
               ru(i) = turmu(j,k)
               i=i+1
            end do
         end do

         do j=1,maxjk*nblk+2
            ru(i) = ia(j)
            i=i+1
         end do

         do j=1,maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1
            ru(i) = ja(j)
            i=i+1
         end do

         do j=1,maxjk*nblk+2
            ru(i) = ipa(j)
            i=i+1
         end do

         do j=1,maxjk*nblk2*5+1
            ru(i) = jpa(j)
            i=i+1
         end do

c$$$         do j=1,maxjk*nblk+2
c$$$            ru(i) = iat(j)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1
c$$$            ru(i) = jat(j)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk+2
c$$$            ru(i) = ipt(j)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk2*5+1
c$$$            ru(i) = jpt(j)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1
c$$$            ru(i) = as(j)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1
c$$$            ru(i)=ast(j)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk2*5+1
c$$$            ru(i)=pa(j)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk2*5+1
c$$$            ru(i) = pat(j)
c$$$            i=i+1
c$$$         end do
c$$$
c$$$         do j=1,warmset
c$$$            do k=1,maxj
c$$$               do l=1,maxk
c$$$                  do m=1,nblk
c$$$                     do n=1,mpopt
c$$$                        ru(i) = qwarm(j,k,l,m,n)
c$$$                        i=i+1
c$$$                     end do
c$$$                  end do
c$$$               end do
c$$$            end do
c$$$         end do
c$$$
c$$$         do j=1,mpopt
c$$$            ru(i) = obj0s(j)
c$$$            i=i+1
c$$$         end do
c$$$
c$$$         do j=1,mpopt
c$$$            ru(i) = alphas(j)
c$$$            i=i+1
c$$$         end do
         

      else if (varStat.eq.1) then
         
         !-- Read optima2D variables from ru


         jdim = ru(i)
         i = i+1
         kdim = ru(i)
         i = i+1
         ndim = ru(i)
         i = i+1
c         ifirst = ru(i)
c         i = i+1
c         ifun = ru(i)
c         i = i+1
         itfirst = ru(i)
         i = i+1
      
         do j=1,maxj
            do k=1,maxk
               x(j,k) = ru(i)
               i = i+1
            end do
         end do

         do j=1,maxj
            do k=1,maxk
               y(j,k) = ru(i)
               i = i+1
            end do
         end do


         do j=1,ibsnc
            idv(j) = ru(i)
            i = i+1
         end do

         do j=1,ibody
            do k=1,2
               bap(j,k)= ru(i)
               i = i+1
            end do
         end do

         do j=1,ibsnc
            do k=1,2
               bcp(j,k) = ru(i)
               i=i+1
            end do
         end do

         do j=1,ibody
            bt(j) = ru(i)
            i = i+1
         end do

         do j=1,ibskt
            bknot(j) = ru(i)
            i=i+1
         end do

         do j=1,maxj
            do k=1,maxk
               indx(j,k) = ru(i)
               i=i+1
            end do
         end do

         do j=1,9
            icol(j) = ru(i)
            i=i+1
         end do

         do j=1,maxjk*4
            iex(j) = ru(i)
            i=i+1
         end do

         do j=1,maxjk
            cp(j) = ru(i)
            i=i+1
         end do

c         do j=1,maxjk
c            do k=1,4
c               xy(j,k) = ru(i)
c               i=i+1
c            end do
c         end do

         do j=1,maxjk
            xyj(j) = ru(i)
            i=i+1
         end do

         do j=1,ibody
            do k=1,2
               cp_tar(j,k) = ru(i)
               i=i+1
            end do
         end do

         do j=1,maxj
            do k=1,maxk
               fmu(j,k) = ru(i)
               i=i+1
            end do
         end do

         do j=1,maxj
            do k=1,maxk
               vort(j,k) = ru(i)
               i=i+1
            end do
         end do

         do j=1,maxj
            do k=1,maxk
               turmu(j,k) = ru(i)
               i=i+1
            end do
         end do

         do j=1,maxjk*nblk+2
            ia(j) = ru(i)
            i=i+1
         end do

         do j=1,maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1
            ja(j) = ru(i)
            i=i+1
         end do

         do j=1,maxjk*nblk+2
            ipa(j) = ru(i)
            i=i+1
         end do

         do j=1,maxjk*nblk2*5+1
            jpa(j) = ru(i)
            i=i+1
         end do

c$$$         do j=1,maxjk*nblk+2
c$$$            iat(j) = ru(i)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1
c$$$            jat(j) = ru(i)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk+2
c$$$            ipt(j) = ru(i)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk2*5+1
c$$$            jpt(j) = ru(i)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1
c$$$            as(j) = ru(i)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1
c$$$            ast(j) = ru(i)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk2*5+1
c$$$            pa(j) = ru(i)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,maxjk*nblk2*5+1
c$$$            pat(j) = ru(i)
c$$$            i=i+1
c$$$         end do

c$$$         do j=1,warmset
c$$$            do k=1,maxj
c$$$               do l=1,maxk
c$$$                  do m=1,nblk
c$$$                     do n=1,mpopt
c$$$                        qwarm(j,k,l,m,n) = ru(i)
c$$$                        i=i+1
c$$$                     end do
c$$$                  end do
c$$$               end do
c$$$            end do
c$$$         end do
c$$$
c$$$         do j=1,mpopt
c$$$            obj0s(j) = ru(i)
c$$$            i=i+1
c$$$         end do
c$$$
c$$$         do j=1,mpopt
c$$$            alphas(j) = ru(i)
c$$$            i=i+1
c$$$         end do
   
      end if

      ruIndex = i

      return

      end
