c*****************************************************************      
c*****************************************************************      
c  ********  VECTORIZED in K scalar penta solver   ***************      
C    SOLVES FOR TWO F'S AT A TIME                  ***************      
c*****************************************************************      
c*****************************************************************      
       subroutine xpenta2(JDIM,KDIM,a,b,c,d,e,x,y,f,jl,ju,kl,ku)        
       real ld,ld1,ld2,ldi                                              
       dimension a(JDIM,KDIM),b(JDIM,KDIM),c(JDIM,KDIM),d(JDIM,KDIM),   
     *           e(JDIM,KDIM),x(JDIM,KDIM),y(JDIM,KDIM),f(JDIM,KDIM,2)  
C                                                                       
c	! start forward generation process and sweep                          
C                                                                       
        j = jl                                                          
        do 1 k = kl,ku                                                  
        ld = c(j,k)                                                     
        ldi = 1./ld                                                     
        f(j,k,1) = f(j,k,1)*ldi                                         
        f(j,k,2) = f(j,k,2)*ldi                                         
        x(j,k) = d(j,k)*ldi                                             
        y(j,k) = e(j,k)*ldi                                             
1       continue                                                        
C                                                                       
        j = jl+1                                                        
        do 2 k = kl,ku                                                  
        ld1 = b(j,k)                                                    
        ld = c(j,k) - ld1*x(j-1,k)                                      
        ldi = 1./ld                                                     
        f(j,k,1) = (f(j,k,1) - ld1*f(j-1,k,1))*ldi                      
        f(j,k,2) = (f(j,k,2) - ld1*f(j-1,k,2))*ldi                      
        x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                            
        y(j,k) = e(j,k)*ldi                                             
2       continue                                                        
C                                                                       
            do 3 j = jl+2,ju-2                                          
                do 11 k = kl,ku                                         
                ld2 = a(j,k)                                            
                ld1 = b(j,k) - ld2*x(j-2,k)                             
                ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
                ldi = 1./ld                                             
                f(j,k,1) = (f(j,k,1) - ld2*f(j-2,k,1)                   
     *                      - ld1*f(j-1,k,1))*ldi                       
                f(j,k,2) = (f(j,k,2) - ld2*f(j-2,k,2)                   
     *                      - ld1*f(j-1,k,2))*ldi                       
                x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                    
                y(j,k) = e(j,k)*ldi                                     
11              continue                                                
3            continue                                                   
c                                                                       
        j = ju-1                                                        
        do 12 k = kl,ku                                                 
        ld2 = a(j,k)                                                    
        ld1 = b(j,k) - ld2*x(j-2,k)                                     
        ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))                     
        ldi = 1./ld                                                     
        f(j,k,1) = (f(j,k,1) - ld2*f(j-2,k,1) - ld1*f(j-1,k,1))*ldi     
        f(j,k,2) = (f(j,k,2) - ld2*f(j-2,k,2) - ld1*f(j-1,k,2))*ldi     
        x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                            
12      continue                                                        
c                                                                       
                j = ju                                                  
                do 13 k = kl,ku                                         
                ld2 = a(j,k)                                            
                ld1 = b(j,k) - ld2*x(j-2,k)                             
                ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
                ldi = 1./ld                                             
                f(j,k,1) = (f(j,k,1) - ld2*f(j-2,k,1) -                 
     *                     ld1*f(j-1,k,1))*ldi                          
                f(j,k,2) = (f(j,k,2) - ld2*f(j-2,k,2) -                 
     *                      ld1*f(j-1,k,2))*ldi                         
13              continue                                                
c                                                                       
c        !  back sweep solution                                         
c                                                                       
        do 14 k = kl,ku                                                 
        f(ju,k,1) = f(ju,k,1)                                           
        f(ju,k,2) = f(ju,k,2)                                           
        f(ju-1,k,1) = f(ju-1,k,1) - x(ju-1,k)*f(ju,k,1)                 
        f(ju-1,k,2) = f(ju-1,k,2) - x(ju-1,k)*f(ju,k,2)                 
14      continue                                                        
c                                                                       
        do 4 j = 2,ju-JL                                                
           jx = ju-j                                                    
           do 15 k = kl,ku                                              
           f(jx,k,1) = f(jx,k,1) - x(jx,k)*f(jx+1,k,1) -                
     *                 y(jx,k)*f(jx+2,k,1)                              
           f(jx,k,2) = f(jx,k,2) - x(jx,k)*f(jx+1,k,2) -                
     *                 y(jx,k)*f(jx+2,k,2)                              
15         continue                                                     
4       continue                                                        
c                                                                       
        return                                                          
        end                                                             
