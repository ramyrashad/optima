***********************************************************************
****    subroutine to form the correction for multigrid process    ****
***********************************************************************
c---- calling subroutine: mk_qp.f
c
      subroutine correction(iblk,jdim,kdim,s,qp,q0)
c
c     -Note: S here is treated as a working array
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
c
      dimension s(jdim,kdim,4),qp(jdim,kdim,4),q0(jdim,kdim,4)
c
      do 10 n = 1,4
      do 10 k = kminbnd(iblk),kmaxbnd(iblk)
      do 10 j = jminbnd(iblk),jmaxbnd(iblk)
        s(j,k,n) = qp(j,k,n) - q0(j,k,n)
 10   continue
c
      return
      end
