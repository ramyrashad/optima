c ********************************************************************
c **************** main integration routine **************************
c ******************** diagonal version ******************************
c *******  with implicit fourth order dissipation   ******************
c *             routine vectorized  thp  10/18/83                    *
c ********************************************************************
c calling routine: xiimpl
      subroutine stepf2dx(jdim,kdim,q,turmu,fmu,s,
     &   press,sndsp,xy,xyj,ds,
     &   coef2,coef4,uu,ccx,work,spect,precon,
     &   jbegin,jend,kbegin,kend,
     &   jlow,jup,klow,kup,meshlow,meshup)
c
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension ds(jdim,kdim), turmu(jdim, kdim), fmu(jdim,kdim)
      dimension spect(jdim,kdim,3)
c
      dimension coef2(jdim,kdim),coef4(jdim,kdim)
      dimension uu(jdim,kdim),ccx(jdim,kdim),precon(jdim,kdim,8)
c
      dimension work(jdim,kdim,10)
c
      logical meshlow, meshup
      logical zero2disp
c
      zero2disp = .false.
      dtd   = dt * thetadt/(1. + phidt)
      hd    = 0.5*dtd
c
c     dissipation coefficients for implicit side
c
c
c     nonlinear coef's contain dt
c     need to add theta scaling
c
      smudt = smuim*dt* thetadt/(1. + phidt)
c
c     -xi direction
c     -fill matrix for inversion for diagonal in xi
c     -set switches for eigenvalues
c     -do first two at the same time since they have the
c      same coefficients
c     
c
      do 300 n = 2,4
        if ( n .eq. 2) sn = 0.
        if ( n .eq. 3) sn = 1.
        if ( n .eq. 4) sn = -1.
        nm=n-1
c
        if (idmodel.eq.1) then
c       *******************************************
c       **        Scalar Dissipation Model       **
c       *******************************************
          do 210 k = klow,kup
          do 210 j = jlow+1,jup-1
            jp1 = j+1
            jm1 = j-1
            jp2 = j+2
            jm2 = j-2
            c2m = coef2(jm1,k)*dtd
            c4m = coef4(jm1,k)*dtd
            c2 = coef2(j,k)*dtd
            c4 = coef4(j,k)*dtd
            work(j,k,1) =  xyj(jm2,k)*c4m
            work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)
            work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
            work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
            work(j,k,5) = xyj(jp2,k)*c4
 210      continue
c             
          if (meshlow) then
            j = jlow
            jp1 = j + 1
            jm1 = j - 1
            jp2 = j + 2
            do 220 k = klow,kup
              if (zero2disp) then
                c2m = 0.0
                c2  = 0.0
              else
                c2m = coef2(jm1,k)*dtd
                c2 = coef2(j,k)*dtd
              endif
              c4m = coef4(jm1,k)*dtd
              c4 = coef4(j,k)*dtd
              work(j,k,1) =  0.
              work(j,k,2) = -(c2m + c4m + c4)*xyj(jm1,k)
              work(j,k,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
              work(j,k,5) = xyj(jp2,k)*c4
 220        continue
          else
            j = jlow
            jm1 = j - 1
            jm2 = j - 2
            jp1 = j + 1
            jp2 = j + 2
            do 222 k = klow,kup
              if (zero2disp) then
                c2m = 0.0
                c2  = 0.0
              else
                c2m = coef2(jm1,k)*dtd
                c2  = coef2(j,k)*dtd
              endif
              c4m = coef4(jm1,k)*dtd
              c4 = coef4(j,k)*dtd
              work(j,k,1) = 0.
c              work(j,k,1) = xyj(jm2,k)*c4m   !xyj(jm2 not defined yet
              work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)
              work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
              work(j,k,5) = xyj(jp2,k)*c4
 222        continue
          endif
          if (meshup) then
            j = jup
            jm1 = j - 1
            jm2 = j - 2
            jp1 = j + 1
            jp2 = j + 1
            do 224 k = klow,kup
              if (zero2disp) then
                c2m = 0.0
                c2  = 0.0
              else
                c2m = coef2(jm1,k)*dtd
                c2 = coef2(j,k)*dtd
              endif
              c4m = coef4(jm1,k)*dtd
              c4 = coef4(j,k)*dtd
              work(j,k,1) =  xyj(jm2,k)*c4m
              work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)
              work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)
              work(j,k,4) = -(c2 + c4 + c4m)*xyj(jp1,k)
              work(j,k,5) = 0.
 224        continue
          else	
            j = jup
            jp1 = j + 1
            jm1 = j - 1
            jm2 = j - 2
            do 226 k = klow,kup
              if (zero2disp) then
                c2m = 0.0
                c2  = 0.0
              else
                c2m = coef2(jm1,k)*dtd
                c2 = coef2(j,k)*dtd
              endif
              c4 = coef4(j,k)*dtd
              c4m = coef4(jm1,k)*dtd
              work(j,k,1) =  xyj(jm2,k)*c4m
              work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)
              work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
c               work(j,k,5) =  xyj(jp2,k)*c4 ! xyj(jp2 not defined yet
              work(j,k,5) = 0.
 226        continue
          endif
        elseif (idmodel.eq.2) then
c       *******************************************
c       **        Matrix Dissipation Model       **
c       *******************************************
          if (dis2x.eq.0.) then
            do 227 k = klow,kup
            do 227 j = jbegin,jup
              jp1=j+1
              work(j,k,7)=0.0
              work(j,k,8)=dtd*(coef4(j,k)*spect(j,k,nm)
     &                       + coef4(jp1,k)*spect(jp1,k,nm))
 227        continue
          else
            do 240 k = klow,kup
            do 240 j = jbegin,jup
              jp1=j+1
              work(j,k,7)=dtd*(coef2(j,k)*spect(j,k,nm)
     &                       + coef2(jp1,k)*spect(jp1,k,nm))
              work(j,k,8)=dtd*(coef4(j,k)*spect(j,k,nm)
     &                       + coef4(jp1,k)*spect(jp1,k,nm))
 240        continue
          endif
c
          do 250 k = klow,kup
          do 250 j = jlow+1,jup-1
            jm1 = j-1
            jp1 = j+1
            c2m = work(jm1,k,7)
            c4m = work(jm1,k,8)
            c2  = work(j,k,7)
            c4  = work(j,k,8)
c
            work(j,k,1) =  xyj(j-2,k)*c4m
            work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)
            work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
            work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
            work(j,k,5) = xyj(j+2,k)*c4
 250      continue
c     
          if (meshlow) then
            j = jlow
            jp1 = j + 1
            jm1 = j - 1
            jp2 = j + 2
            do 260 k = klow,kup
              c2m = work(jm1,k,7)
              c4m = work(jm1,k,8)
              c2  = work(j,k,7)
              c4  = work(j,k,8)
c              
              work(j,k,1) =  0.
              work(j,k,2) = -(c2m + c4m + c4)*xyj(jm1,k)
              work(j,k,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
              work(j,k,5) = xyj(jp2,k)*c4
 260        continue
          else
            j = jlow
            jm1 = j - 1
            jm2 = j - 2
            jp1 = j + 1
            jp2 = j + 2
            do 262 k = klow,kup
              c2m = work(jm1,k,7)
              c4m = work(jm1,k,8)
              c2  = work(j,k,7)
              c4  = work(j,k,8)
c                work(j,k,1) = 0.
              work(j,k,1) = xyj(jm2,k)*c4m
              work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)
              work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
              work(j,k,5) = xyj(jp2,k)*c4
 262        continue
          endif
          if (meshup) then
            j = jup
            jm1 = j - 1
            jm2 = j - 2
            jp1 = j + 1
            jp2 = j + 1
            do 264 k = klow,kup
              c2m = work(jm1,k,7)
              c4m = work(jm1,k,8)
              c2  = work(j,k,7)
              c4  = work(j,k,8)
              work(j,k,1) =  xyj(jm2,k)*c4m
              work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)
              work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)
              work(j,k,4) = -(c2 + c4 + c4m)*xyj(jp1,k)
              work(j,k,5) = 0.
 264        continue
          else	
            j = jup
            jp2 = j + 2
            jp1 = j + 1
            jm1 = j - 1
            jm2 = j - 2
            do 266 k = klow,kup
              c2m = work(jm1,k,7)
              c4m = work(jm1,k,8)
              c2  = work(j,k,7)
              c4  = work(j,k,8)
              work(j,k,1) =  xyj(jm2,k)*c4m
              work(j,k,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)
              work(j,k,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)
              work(j,k,5) =  xyj(jp2,k)*c4 
c                work(j,k,5) = 0.
 266        continue
          endif
        elseif(idmodel.eq.3)then
c       *******************************************
c       **        Const. Coef. Diss. Model       **
c       *******************************************
c       -implemented by Tom Nelson
          do 1210 k = klow,kup
          do 1210 j = jlow,jup
            rjjj = smudt*coef4(j,k)
            work(j,k,1) =  xyj(j-2,k)*rjjj
            work(j,k,2) = -4.*xyj(j-1,k)*rjjj
            work(j,k,3) = 6.*xyj(j,k)*rjjj
            work(j,k,4) = -4.*xyj(j+1,k)*rjjj
            work(j,k,5) = xyj(j+2,k)*rjjj
 1210     continue
c     
          j = jlow
          do 1220 k = klow,kup
            rjjj = smudt*coef4(j,k)
            work(j,k,1) =  0.
            work(j,k,2) = -rjjj*xyj(j-1,k)
            work(j,k,3) = 3.*xyj(j,k)*rjjj
            work(j,k,4) = -3.*rjjj*xyj(j+1,k)
            work(j,k,5) = xyj(j+2,k)*rjjj
 1220     continue
c     
          j = jup
          do 1222 k = klow,kup
            rjjj = smudt*coef4(j,k)
            work(j,k,1) =  xyj(j-2,k)*rjjj
            work(j,k,2) = -3.*rjjj*xyj(j-1,k)
            work(j,k,3) = 3.*xyj(j,k)*rjjj
            work(j,k,4) = -rjjj*xyj(j+1,k)
            work(j,k,5) = 0.
 1222     continue
        endif                   !idmodel
c     
c     
c       -add in flux jacobians, variable dt and identity
        if(viscous .and. visxi)then
          hre = dtd/re
c         -Sutherland equation
c     
          do 4000 k = klow,kup
          do 4000 j = jbegin,jend
            rinv = 1./q(j,k,1)
c           xi_x**2 + xi_y**2
            ximet = xy(j,k,1)**2 + xy(j,k,2)**2
c           turb. viscosity
            djac = hre/xyj(j,k)
            work(j,k,6) = djac*ximet
c           xyj/rho
            work(j,k,7) = rinv
 4000     continue
c     
          do 4401 k = klow,kup
          do 4401 j = jlow,jup
            jm1 = j-1
            jp1 = j+1
c           -thp-4/29/87  
c            -for now laminar flwo in xi viscous cases only
c             turm = turmu(j,k)
            turm = 0.0
            vnum1 = 0.5*(fmu(j,k)+fmu(jm1,k)) + turm
            vnum  = 0.5*(fmu(j,k)+fmu(jp1,k)) + turm
            ctm1 = 0.5*(work(jm1,k,6)+work(j,k,6))*vnum1
            ctp1 = 0.5*(work(jp1,k,6)+work(j,k,6))*vnum
            cttt = ctm1 + ctp1
            work(j,k,2) = work(j,k,2) - work(jm1,k,7)*ctm1
            work(j,k,3) = work(j,k,3) + work(j,k,7)*cttt
            work(j,k,4) = work(j,k,4) - work(jp1,k,7)*ctp1
 4401     continue
        endif
c       -end viscous
c
c
c       -add in flux jacobians and variable dt and identity
        if ((prec.eq.0).or.(n.eq.2)) then
          if (iord.eq.4) then
            thd=dtd/12.d0  
            do 231 k = klow,kup                                       
            do 231 j = jlow+1,jup-1                            
              work(j,k,1)= work(j,k,1) + (uu(j-2,k)+sn*ccx(j-2,k))*thd
              work(j,k,2)= work(j,k,2)-8.*(uu(j-1,k)+sn*ccx(j-1,k))*thd
              work(j,k,4)= work(j,k,4)+8.*(uu(j+1,k)+sn*ccx(j+1,k))*thd
              work(j,k,5)= work(j,k,5) - (uu(j+2,k)+sn*ccx(j+2,k))*thd
              work(j,k,1)= work(j,k,1)*ds(j,k)                     
              work(j,k,2)= work(j,k,2)*ds(j,k)                      
              work(j,k,3)= work(j,k,3)*ds(j,k)                       
              work(j,k,3)= work(j,k,3) + 1.0
              work(j,k,4)= work(j,k,4)*ds(j,k)                       
              work(j,k,5)= work(j,k,5)*ds(j,k)                    
 231        continue
c             
c           -using third order for boundary
            thd=dtd/6.d0
            do 232 k=klow,kup
              j=jlow
              work(j,k,2)= work(j,k,2)-2.*(uu(j-1,k)+sn*ccx(j-1,k))*thd
              work(j,k,3)= work(j,k,3) -   3.*(uu(j,k)+sn*ccx(j,k))*thd
              work(j,k,4)= work(j,k,4)+6.*(uu(j+1,k)+sn*ccx(j+1,k))*thd
              work(j,k,5)= work(j,k,5) -  (uu(j+2,k)+sn*ccx(j+2,k))*thd
              work(j,k,1)= work(j,k,1)*ds(j,k)                      
              work(j,k,2)= work(j,k,2)*ds(j,k)                      
              work(j,k,3)= work(j,k,3)*ds(j,k)                       
              work(j,k,3)= work(j,k,3) + 1.0
              work(j,k,4)= work(j,k,4)*ds(j,k)                       
              work(j,k,5)= work(j,k,5)*ds(j,k)                       
c     
              j=jup
              work(j,k,1)= work(j,k,1) +  (uu(j-2,k)+sn*ccx(j-2,k))*thd
              work(j,k,2)= work(j,k,2)-6.*(uu(j-1,k)+sn*ccx(j-1,k))*thd
              work(j,k,3)= work(j,k,3) +   3.*(uu(j,k)+sn*ccx(j,k))*thd 
              work(j,k,4)= work(j,k,4)+2.*(uu(j+1,k)+sn*ccx(j+1,k))*thd
              work(j,k,1)= work(j,k,1)*ds(j,k)                     
              work(j,k,2)= work(j,k,2)*ds(j,k)                        
              work(j,k,3)= work(j,k,3)*ds(j,k)                        
              work(j,k,3)= work(j,k,3) + 1.0
              work(j,k,4)= work(j,k,4)*ds(j,k)                        
              work(j,k,5)= work(j,k,5)*ds(j,k)                       
 232        continue
          else
            do 230 k = klow,kup
            do 230 j = jlow,jup
              work(j,k,2) = work(j,k,2) - (uu(j-1,k)+sn*ccx(j-1,k))*hd
              work(j,k,4) = work(j,k,4) + (uu(j+1,k)+sn*ccx(j+1,k))*hd
              work(j,k,1) = work(j,k,1)*ds(j,k)
              work(j,k,2) = work(j,k,2)*ds(j,k)
              work(j,k,3) = work(j,k,3)*ds(j,k)
              work(j,k,3) = work(j,k,3) + 1.
              work(j,k,4) = work(j,k,4)*ds(j,k)
              work(j,k,5) = work(j,k,5)*ds(j,k)
 230        continue
          endif
        else
c         -use the preconditioned 3rd and fourth eigenvalues
c         -only for iord.eq.4
c
          do 234 k = klow,kup                                         
          do 234 j = jlow,jup
            work(j,k,2) = work(j,k,2) - 
     &            (precon(j-1,k,5)+sn*precon(j-1,k,6))*hd     
            work(j,k,4) = work(j,k,4) + 
     &            (precon(j+1,k,5)+sn*precon(j+1,k,6))*hd     
            work(j,k,1) = work(j,k,1)*ds(j,k)
            work(j,k,2) = work(j,k,2)*ds(j,k)
            work(j,k,3) = work(j,k,3)*ds(j,k)
            work(j,k,3) = work(j,k,3) + 1.
            work(j,k,4) = work(j,k,4)*ds(j,k)
            work(j,k,5) = work(j,k,5)*ds(j,k)
 234      continue
        endif
c     
c
c       -invert LHS
        if(n.eq.2)then
c         -for n = 1 and 2 do both together
          call xpenta2(jdim,kdim,work(1,1,1),work(1,1,2),
     &          work(1,1,3),work(1,1,4),work(1,1,5),
     &          work(1,1,6),work(1,1,7),s(1,1,1),
     &          jlow,jup,klow,kup)
        else
c          -n = 3 or 4
          call xpenta(jdim,kdim,work(1,1,1),work(1,1,2),
     &          work(1,1,3),work(1,1,4),work(1,1,5),
     &          work(1,1,6),work(1,1,7),s(1,1,n),
     &          jlow,jup,klow,kup)
        endif
 300  continue
c
      return
      end
