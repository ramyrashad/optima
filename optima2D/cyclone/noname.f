      subroutine noname(jdim,kdim,s,ds,akk)
c
#include "../include/arcom.inc"
c
      dimension s(jdim,kdim,4),ds(jdim,kdim)
c
      if (jesdirk.eq.4)then
         fct=4.0*dt/dt2
      else
         fct=1.5*dt/dt2 
      endif
      do 50 n=1,4
      do 50 k=klow,kup
      do 50 j=jlow,jup
          s(j,k,n)=s(j,k,n)*(1. + fct*ds(j,k))
 50   continue
      return
      end
