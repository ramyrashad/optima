c *********************************************************************
c ** This routine automatically adjusts boundary condition type of   **
c ** of outflow boundary (assumes flat vertical boundary at back of  **
c ** flowfield)
c *********************************************************************
c calling routine: ioall
c
c
c *********************************************************************
c NOTE:
c
c ibctype(i,j) give the boundary condition type = 0  interface
c                                                 1  wall
c                                                 2  farfield
c                                                 3  outflow xi mim (side2)
c                                                 4  outflow xi max (side4)
c                                                 5  avg between blocks
c
c Thus for the Average psi BC option:
c   BC's on all interior psi lines (sides 1 & 3) that are not walls = 5
c *********************************************************************
c
      subroutine outflowbnd(jdim,kdim,x)

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
c
      integer jdim,kdim,nbs,iblk,iside,jkstart,jkend
      double precision x(jdim*kdim),xmax,x1
c
      if (mg .or. gseq) then
        nbs=nblkstot
      else
        nbs=nblks
      endif
c
      xmax=0.d0
      do 200 iblk=1,nblkstot
      do 200 iside=2,4,2
        if (ibctype(iblk,iside).eq.2) then
          call getbounds(iblk,iside,jkstart,jkend)
          x1 = x(jkstart)
          xmax = max(x1,xmax)
        endif
 200  continue
c
      do 300 iblk=1,nbs
      do 300 iside=2,4,2
        if (ibctype(iblk,iside).eq.2) then
          call getbounds(iblk,iside,jkstart,jkend)
          x1 = x(jkstart)
          if (x1.eq.xmax) then
            if (iside.eq.2) then
              ibctype(iblk,iside)=3
            elseif (iside.eq.4) then
              ibctype(iblk,iside)=4
            endif
          endif
        endif
 300  continue
c
      return
      end
