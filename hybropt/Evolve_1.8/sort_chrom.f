C     
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      SUBROUTINE SORT_chrom(NOBFUN,NCHROM,FIT,ngene,pop,i_mnx,debug)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTINE PERFORMS A SORT BASED ON THE FIRST     C
C               FITNESS VALUE ***                                      C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      logical debug
      DIMENSION FIT(nobfun,nchrom),pop(ngene,nchrom)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C
      if(i_mnx.eq.1)then
         NCHROMM = NCHROM-1
         DO NCC=1,NCHROM
            DO NC=1,NCHROMM
               IF (FIT(1,NC).LT.FIT(1,NC+1)) THEN
                  DO NO=1,NOBFUN
                     FITDUM          = FIT(NO,NC)
                     FIT(NO,NC)   = FIT(NO,NC+1)
                     FIT(NO,NC+1) = FITDUM
                  ENDDO
                  DO Ng=1,ngene
                     popDUM          = pop(Ng,NC)
                     pop(Ng,NC)   = pop(Ng,NC+1)
                     pop(Ng,NC+1) = popDUM
                  ENDDO
               ENDIF
            ENDDO
         ENDDO
      else
         NCHROMM = NCHROM-1
         DO NCC=1,NCHROM
            DO NC=1,NCHROMM
               IF (FIT(1,NC).GT.FIT(1,NC+1)) THEN
                  DO NO=1,NOBFUN
                     FITDUM          = FIT(NO,NC)
                     FIT(NO,NC)   = FIT(NO,NC+1)
                     FIT(NO,NC+1) = FITDUM
                  ENDDO
                  DO Ng=1,ngene
                     popDUM          = pop(Ng,NC)
                     pop(Ng,NC)   = pop(Ng,NC+1)
                     pop(Ng,NC+1) = popDUM
                  ENDDO
               ENDIF
            ENDDO
         ENDDO
      endif
C
C          *** SORT COMPLETED ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
