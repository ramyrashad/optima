c-----------------------------------------------------------------------
c     -- GMRES solve control --
c     -- followed SPARSKIT routine 'runrc' in 'itaux' --
c     -- m. nemec, sept. 2001 --
c     -- calling routine: nksolve.f --      
c-----------------------------------------------------------------------
      subroutine run_gmres( ifirst,nmax,ntvar,jacmat,qps,q,qos,press,
     &     sndsp, s, rsa, xy, xyj, xit, ett,ds, x, y, turmu, vort,turps,  
     &     turre,turos,fmu, vk, ve, tmp, uu, vv, ccx,ccy,coef2x,coef2y,
     &     coef4x,coef4y, precon, spectxi,specteta, indx, wk, alu, jlu,
     &     ju, as,ja, ia, rhs, sol, iex, bcf, ipar, fpar, iout1, iout2,
     &     exprhs,dhatq,exprsa,dhatt,dtvc)

      implicit none

#include "../include/parms.inc"

      logical jacmat
      integer ifirst,nmax,ntvar,its,j,iout1,iout2
      double precision q(*),        press(*),   sndsp(*)
      double precision s(*),        xy(*),      xyj(*)
      double precision xit(*),      ett(*),     ds(*)
      double precision x(*),        y(*),       turmu(*)
      double precision vort(*),     turre(*),   fmu(*)
      double precision vk(*),       ve(*),      tmp(*)
      double precision uu(*),       vv(*),      ccx(*)
      double precision ccy(*),      coef2x(*),  coef2y(*)
      double precision coef4x(*),   coef4y(*),  precon(*)
      double precision rhs(*),      sol(*),     spectxi(*)
      double precision specteta(*), as(*),      fpar(*)
      double precision wk(*),rsa(*), bcf(maxjb,4,4,mxbfine)
      integer indx(*),ipar(*),ia(*),ja(*),iex(maxjb,4,4,mxbfine)
      
      double precision resgmr,res1gmr

cmt ~~~
      DOUBLE PRECISION qps(*),qos(*),turps(*),turos(*)
      DOUBLE PRECISION exprhs(*),dhatq(*),exprsa(*),dhatt(*)
c     -- time vector in matrix-vector product --
      double precision dtvc(*)

c     -- ILU arrays --
      double precision alu(*)
      integer jlu(*), ju(*)

      its  = 0
      resgmr  = 0.0d0
      res1gmr = 0.0d0

c     -- gmres soluton array --
      do j = 1,ntvar
         sol(j) = 0.0d0
      end do

 100  continue

      call gmres (ntvar, rhs, sol, ipar, fpar, wk)

c     -- residual output --
      if (ipar(7).ne.its) then

c         write (*,*) 'inner',its, real(fpar(5))

         if (iout1.gt.0 .and. mod(its,10).eq.0) write (iout1,10) its,
     &        resgmr
         if (its.eq.1) res1gmr = resgmr 

         if (its.ne.0 .and. iout2.gt.0 ) write (iout2,10) its, resgmr/
     &        res1gmr

         its = ipar(7)
      endif
      resgmr = fpar(5)

      if (ipar(1).eq.1 .and. jacmat) then

         call amux(ntvar, wk(ipar(8)), wk(ipar(9)), as, ja, ia)
         goto 100

      else if (ipar(1).eq.1 .and. .not. jacmat) then

         call framux(ifirst,ntvar,nmax,q,qps,qos,press,sndsp,s,rsa,xy,
     &        xyj,xit,ett,ds,x,y,turmu, vort,turre,turps,turos,fmu,vk, 
     &        ve,tmp, uu, vv, ccx, ccy,coef2x, coef2y, coef4x, coef4y,
     &        precon, spectxi, specteta,indx, iex, bcf, wk(ipar(8)),
     &        wk(ipar(9)),exprhs,dhatq,exprsa,dhatt,dtvc)
         ifirst = 0
         goto 100

      else if (ipar(1).eq.2) then

         call atmux(ntvar, wk(ipar(8)), wk(ipar(9)), as, ja, ia)
         goto 100

      else if (ipar(1).eq.3 .or. ipar(1).eq.5) then             
         call lusol(ntvar, wk(ipar(8)), wk(ipar(9)), alu, jlu, ju)
         goto 100

      else if (ipar(1).eq.4 .or. ipar(1).eq.6) then

         call lutsol(ntvar, wk(ipar(8)), wk(ipar(9)), alu, jlu, ju)
         goto 100

      else if (ipar(1).le.0) then
         if (ipar(1).eq.0) then
c            print *, 'Iterations to converge:', its,real(fpar(5))
         else if (ipar(1).eq.-1) then
            print *, 'Iterative solver has iterated too many times.'
         else if (ipar(1).eq.-2) then
            print *, 'Iterative solver was not given enough work space.'
            print *, 'The work space should at least have ', ipar(4),
     &           ' elements.'
         else if (ipar(1).eq.-3) then
            print *, 'Iterative solver is facing a break-down.'
         else
            print *, 'Iterative solver terminated. code =', ipar(1)
         endif

      endif

      if (iout1.gt.0) write (iout1,10) ipar(7), fpar(5)

c     -- note: the "if" statements for res1gmr are required for
c     multiobjective runs when one of the objectives is not active --
 
      if (iout2.gt.0) then
         if (res1gmr.lt.2.2e-16) then
            write (iout2,20) ipar(7), fpar(5)
         else
            write (iout2,20) ipar(7), fpar(5)/res1gmr
         end if
      end if

c     -- fpar(12) is not used in SPARSKIT, here it is used to store the
c     relative inner residual --
      if (res1gmr.lt.2.2e-16) then
         fpar(12) = fpar(5)
      else
         fpar(12) = fpar(5)/res1gmr
      end if

 10   format (3x,i5,e14.5,e12.3)
 20   format (3x,i5,e14.5/)

      return
      end                       !run_gmres
c-----------------------------------------------------------------------
      function distdot(n,x,ix,y,iy)
      implicit none
      integer n, ix, iy
      real*8 distdot, x(*), y(*), ddot
      external ddot
      distdot = ddot(n,x,ix,y,iy)
      return
      end                       !distdot
