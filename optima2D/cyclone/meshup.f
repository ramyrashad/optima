      subroutine meshup(ii,jdim,kdim,q,qold,s,xy,xyj,xit,ett,x,y,ds,
     *           xxt,yyt,press,sndsp,turmu,fmu,vort,precon,turtmp)
c
      logical conser,qint,ctof                                          
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),qold(jdim,kdim,4)                        
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension xit(jdim,kdim),ett(jdim,kdim),ds(jdim,kdim)             
      dimension x(jdim,kdim),y(jdim,kdim),precon(jdim,kdim,6)          
      dimension press(jdim,kdim),sndsp(jdim,kdim)
      dimension turmu(jdim,kdim),fmu(jdim,kdim),vort(jdim,kdim)         
c                                                                       
c                                                                       
      dimension xxt(jdim,kdim),yyt(jdim,kdim),turtmp(jdim,kdim)
c                                                                       
      dimension dtiseq(20),dtmins(20),dtow2(20)
      dimension jmxi(20),kmxi(20),jskipi(20),iends(20)
      common/mesup/ dtiseq,dtmins,dtow2,isequal,iseqlev,
     &              jmxi,kmxi,jskipi,iends
c                                                                       
c     Added to save these  12/11/86  thp            
      common/save1/jmxfine,kmxfine,jt1fine,jt2fine,trlofine,trupfine    
c                                                                       
c     Grid sequence mip written by T. Barth  modified by THP 09/01/87   
c     Last modified by Stan De Rango April 1/98 to include interpolation
c     of turbulence quantities from coarse to fine grid for Spalart-
c     Allmaras model (ie. turre).
c
c     Program to start up through a sequence of coarse grids. A
c     coarsened grid is initially cut from the stored fine grid. 
c     Iterations are made and then a finer grid is cut out, the solution
c     interpolated etc.
c     
c     Only halving type grid reduction is allowed in xi, spacing in eta
c     is arbitrary and ultilizes a general interpolation routine.
c     This is a somewhat self contained sub., it undoes and redoes        
c     parts of initia to be self contained. Code changes appear in sc1.f   
c     (to sequence through grids) and in ioall.f (to read in parameters).   
c                                                                       
c     A number of checks are used to by pass logic.    thp  12/15/83       
c      1 if the number of points in the xi and eta direction are the    
c        same then interpolation bypassed                               
c      2 if restart,  return                                            
c                                                                       
      if(restart) return                              
c      if(jskipi(1).eq.1)return                         
c                                                                       
      cosang = cos(pi*alpha/180.d0)                                      
      sinang = sin(pi*alpha/180.d0)                                       
      if(ii .gt. 1) go to 40                                            
c                                                                       
c     Free stream start up, cut out a coarsened grid from the fine grid.
      jmxfine = jmaxold                                                 
      kmxfine = kmax                                                    
      jt1fine = jtail1                                                  
      jt2fine = jtail2                                                  
      jskp = jskipi(ii)                                                 
c                                                                       
c     Default logic for coarse meshes.
c     Some parameters only make sense on the finest mesh                 
c     such as transition information
c                                                                       
      trlofine = translo                                                
      trupfine = transup                                                
c                                                                       
c     Check that jmxfine is define for factor of 2's
c     
c     For 'o' mesh  jmxfine = 2*n+1                                       
c     For 'c' mesh  jmxfine = 2*n+1  so that outflow boundary is ok       
c     Also check jtail1 , jtail2                                         
c                                                                       
      if( jmxi(isequal) .ne. jmxfine)then                          
        write(out_unit,*)'********* error in input *************'           
        write(out_unit,*)' jmxi(isequal) not correct '                      
        write(out_unit,*)'********* error in input *************'           
        stop                                                       
      endif                                                        
c
      if (jskipi(ii).ne.1) then
      if( mod( (jmxfine/jskp)*jskp + 1 , jmxfine) .ne. 0)then      
        write(out_unit,*)'********* error in input *************'           
        write(out_unit,*)' jmxfine not correct'                             
        write(out_unit,*)'********* error in input *************'           
        stop                                                       
      endif                                                        
c
      if(.not.periodic)then                                          
        if( mod( (jt1fine/jskp)*jskp + 1 , jt1fine) .ne. 0)then      
          write(out_unit,*)'********* error in input *************'           
          write(out_unit,*)' jtail1  not correct for c mesh'                  
          write(out_unit,*)'********* error in input *************'           
          stop                                                       
        endif                                                        
      endif
      endif
c                                                                       
c                                                                       
c                                                                       
c     Check to see if jmxfine = jmxi(ii)  and  kmxfine = kmx(ii)          
      if(jmxi(ii).eq.jmxfine .and. kmxi(ii).eq.kmxfine)return           
c                                                                       
c     Make the coarsest grid                                               
      conser = .false.                                                  
      ctof = .false.                                                    
      qint = .false.                                                    
      jmax0 = jmxfine                                                   
      kmax0 = kmxfine                                                   
      jmax  = jmxi(ii)                                                  
      kmax  = kmxi(ii)                                                  
      call mip2(jdim,kdim,jmax0,kmax0,jmax,kmax,                        
     >                q,x,y,s,x,y,turtmp,conser,ctof,qint)                
c                                                                       
c     Reset q to free stream at angle of attack, alpha                    
                                                                        
      do 30 k=1,kmax                                                    
      do 30 j=1,jmax                                                    
         q(j,k,1) = rhoinf
         q(j,k,2) = rhoinf*uinf                                            
         q(j,k,3) = rhoinf*vinf                                            
         q(j,k,4) = einf                                                   
   30 continue                                                          
c
c     Go to end to form metrics, rescale q, set parameters etc              
      go to 99                                                          
c                                                                       
c*****************************************************************      
c                                                                       
c     Form larger grid and                                                
c        interpolate coarse grid solution onto finer grid               
c                                                                       
c*****************************************************************      
c                                                                       
   40 continue                                                          
c                                                                       
c                                                                       
c     If the number of points on the next level in both j and k are       
c     the same as on the previous level then bypass all the interpolation 
c     logic.  This is an option for changing the time steps.              
c                                                                       
      if( jmxi(ii) .eq. jmxi(ii-1) .and. kmxi(ii) .eq. kmxi(ii-1) )     
     *            return                                                
c                                                                       
c     First save coarse grid solution in temporary arrays                 
c                                                                       
      jmx1 = jmxi(ii-1)                                                 
      kmx1 = kmxi(ii-1)                                                 
      do 41 j = 1,jmx1                                                  
      do 41 k = 1,kmx1                                                  
         xxt(j,k) = x(j,k)                                                 
         yyt(j,k) = y(j,k)                                                 
         do 42 n = 1,4                                                     
            s(j,k,n) = q(j,k,n)*xyj(j,k)
 42      continue
   41 continue                                                          
c                                                                       
c     Read in finest grid from disk again                                  
c                                                                       
      rewind grid_unit
      rewind cylgrd_unit
      if(iread.eq.0)then                            
        read (cylgrd_unit) jmax2,kmax2 
        read (cylgrd_unit) ( (x(j,k),j=1,jmax2) ,k=1,kmax2),
     &                     ( (y(j,k),j=1,jmax2) ,k=1,kmax2)                    
      elseif (iread .eq. 1) then 
        read (grid_unit,351) jmax2, kmax2 
 351     format(2i5)
         read (grid_unit,352) ( (x(j,k),j=1,jmax2) ,k=1,kmax2),
     &                        ( (y(j,k),j=1,jmax2) ,k=1,kmax2)
 352     format(5e14.7)
      elseif (iread.eq.2) then                            
        read (grid_unit) jmax2,kmax2 
        read (grid_unit) ( (x(j,k),j=1,jmax2) ,k=1,kmax2),
     &                   ( (y(j,k),j=1,jmax2) ,k=1,kmax2)
      endif                                                             
 380  rewind grid_unit 
c                                                                       
c     Make the next grid                                                  
c                                                                       
      conser = .false.                                                  
      ctof = .false.                                                    
      qint = .false.                                                    
c     set jmax and kmax for the new grid                                  
      jskp = jskipi(ii)                                                 
      jmax = jmxi(ii)                                                   
      kmax = kmxi(ii)                                                   
c                                                                       
c     cut out coarse grid from finest one                                 
      if( jmax .eq. jmxfine .and. kmax .eq. kmxfine) go to 49           
c                                                                       
      jmax0 = jmxfine                                                   
      kmax0 = kmxfine                                                   
      call mip2(jdim,kdim,jmax0,kmax0,jmax,kmax,                        
     >                q,x,y,s,x,y,turtmp,conser,ctof,qint)        
c                                                                       
   49 continue                                                          
c                                                                       
c     interpolate coarse grid solution onto next grid                     
      conser = .false.                                                  
      ctof = .true.                                                     
      qint = .true.                                                     
      jmaxp  = jmxi(ii-1)                                               
      kmaxp  = kmxi(ii-1)                                               
      call mip2(jdim,kdim,jmax,kmax,jmaxp,kmaxp,                        
     >                q,x,y,s,xt,yt,turtmp,conser,ctof,qint)       
                                                                        
   60 continue                                                          
c                                                                       
   99 continue                                                          
c                                                                       
c********************************************************************   
c   set needed parameters such as jup,jplus,... then                    
c   form metrics, rescale q with jacobian, form s, switches etc         
c                                                                       
c********************************************************************   
c                                                                       
c                                                                       
      if ( .not.periodic ) then                                         
            if(jmax.ne.jmxfine)then                                     
            jtail1 = jt1fine/jskp + 1                                   
            jtail2 = jmax - jtail1 + 1                                  
            else                                                        
            jtail1 = jt1fine                                            
            jtail2 = jt2fine                                            
            endif                                                       
      endif                                                             
c                                                                       
c     reinitialize based on new mesh                                       
c                                                                       
c      ifirst=0
c      if (restart .and. isequal.gt.1 .and. ii.eq.1) ifirst=2
c      write(out_unit,*) 'initia being called from meshup'
c      call flush(6)
c
      ifirst=0                                                        
      call initia(ifirst,jdim,kdim,q,qold,press,sndsp,s,xy,xyj,
     *                  xit,ett,ds,x,y,turmu,fmu,vort,precon)  
c
      write (out_unit,*) '  dimensions, jmax,kmax,jtail1,jtail2  ',
     &      jmax,kmax,jtail1,jtail2                    
      call flush (out_unit)
      return                                                            
      end                                                               
