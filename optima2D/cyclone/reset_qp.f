      subroutine reset_qp(jdim,kdim,ndim,qp,q)
c
#include "../include/arcom.inc"
c
      dimension qp(jdim,kdim,ndim),q(jdim,kdim,ndim)

      do 3 n=1,ndim
      do 3 k=1,kmax
      do 3 j=1,jmax
         qp(j,k,n)=q(j,k,n)
 3    continue

      return
      end

