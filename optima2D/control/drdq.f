c----------------------------------------------------------------------
c     -- form dR/dQ --
c     written by: marian nemec
c     date: may 2000
c----------------------------------------------------------------------
      subroutine dRdQ (jdim, kdim, ndim, indx, icol, iex, as, pa, xy,
     &      xyj, x, y, q, press, sndsp, fmu, turmu, uu, vv, ccx, ccy,
     &      coef2, coef4, tmet, precon, work, chi, fv1, dfv1, diag,
     &      diagp, bm2, bm1, db, bp1, bp2, dmul, dchi, dprod, ddest,
     &      dsxip, dsxim,dsetap,dsetam,wkb1,wkb2,bcf,pdc,akk,imps)

      use disscon_vars
#include "../include/arcom.inc"

      integer j, k, m, n, ij, ik, jdim, kdim, ndim
      integer indx(jdim,kdim), iex(jdim,kdim,4), icol(9)

clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
clb

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4),xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim)

      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision fmu(jdim,kdim), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef2(jdim,kdim), coef4(jdim,kdim)
      double precision tmet(jdim,kdim),  bcf(jdim,kdim,4)
      double precision precon(jdim*kdim,6), pdc

      double precision work(jdim*kdim,14)
      double precision dmut(5,jdim,kdim), dmul(ndim,jdim,kdim)

      double precision diag(4,4,jdim,kdim)
      double precision diagp(jdim,kdim,2,4)

      double precision diagj(ndim,3,jdim,kdim),diagk(ndim,3,jdim,kdim)
      double precision diagjp(ndim,3,jdim,kdim),diagkp(ndim,3,jdim,kdim)
      double precision diagjj(ndim,3,jdim,kdim),diagkk(ndim,3,jdim,kdim)
      double precision diagpj(ndim,3,jdim,kdim),diagmj(ndim,3,jdim,kdim)
      double precision diagpk(ndim,3,jdim,kdim),diagmk(ndim,3,jdim,kdim)
      
c     -- variables used for S-A model linearization --
      double precision bm2(5,jdim,kdim), bm1(5,jdim,kdim)
      double precision db(5,jdim,kdim), bp1(5,jdim,kdim)
      double precision bp2(5,jdim,kdim)
      double precision chi(jdim,kdim), dchi(5,jdim,kdim)
      double precision fv1(jdim,kdim), dfv1(jdim,kdim)      
      double precision dprod(5,jdim,kdim), ddest(5,jdim,kdim) 
      double precision dsxip(5,jdim,kdim), dsxim(5,jdim,kdim) 
      double precision dsetam(5,jdim,kdim), dsetap(5,jdim,kdim)

c     -- additional work arrays --
      double precision wkb1(jdim*kdim*ndim*ndim)
      double precision wkb2(jdim*kdim*ndim*ndim)

c     -- frozen jacobian arrays --
      double precision c2xf(maxj,maxk), c4xf(maxj,maxk)
      double precision c2yf(maxj,maxk), c4yf(maxj,maxk)
      double precision fmuf(maxj,maxk), turmuf(maxj,maxk)
      double precision vortf(maxj,maxk), sapdf(maxj,maxk)
      double precision sadx(maxj,maxk), sady(maxj,maxk)
      double precision suuf(maxj,maxk), svvf(maxj,maxk)
      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &      sapdf, sadx, sady, suuf, svvf 

      logical imps

csi   add in the time terms along the diagonal of the A matrix
      if (unsted)then
         call filltime (jdim, kdim, ndim, xyj, work, coef2, coef4, 
     &        indx,icol, as, pa, pdc,akk)
      endif
c     -- forming d(R)/d(Q) --
c     -- form 4x4 inviscid flux jacobian matrix at each grid point --

c     -- x - direction --
c      if (.not. frozen) 

      call fillasx (jdim, kdim, ndim, indx, icol, q,
     &      xy, as, pa, diag)

c     -- y - direction --
c      if (.not. frozen) 
      call fillasy (jdim, kdim, ndim, indx, icol, q,
     &      xy, as, pa, diag)

c     -- find contravariant velocities uu and vv, used in
c     differentiation of S-A model and dissipation --
      call eigval ( 1, 2, jdim, kdim, q, press, sndsp, xyj, xy, tmet,
     &      uu, ccx)
      call eigval ( 3, 4, jdim, kdim, q, press, sndsp, xyj, xy, tmet,
     &      vv, ccy)

c      if (frozen) then
c        do k = 1,kend
c          do j = 1,jend
c            uu(j,k) = suuf(j,k)
c            vv(j,k) = svvf(j,k)
c          end do
c        end do
c      end if   

c     -- viscous terms --
      if (viscous) then

         call iv_mu (jdim, kdim, ndim,xyj, q, fmu, turmu, press,
     &        sndsp, dmul, dmut,chi, dchi, fv1, dfv1) 

         call ivy_q (jdim, kdim, ndim, xy, xyj, q, fmu, turmu,press,
     &        dmul,dmut,diagk,diagkp)

         if (visxi) call ivx_q (jdim,kdim,ndim,xy,xyj,q,fmu,turmu,press,
     &        dmul,dmut,diagj,diagjp)

         if (viscross) call ivc_q (jdim,kdim,ndim,xy,xyj,q,fmu,turmu,
     &        press,dmul,dmut,diagjj,diagkk,diagpj,diagmj,diagpk,diagmk)

         call fillasv (jdim, kdim, ndim, indx, icol, as, pa,diagj,diagk,
     &        diagjp,diagkp,diagjj,diagkk,diagpj,diagmj,diagpk,diagmk)

c     -- spalart - allmaras model contribution --
        if (turbulnt .and. itmodel.eq.2) then

c     -- zero S-A jacobian arrays --
           do k = kbegin,kend
              do j = jbegin,jend
                 do n = 1,5
                    bm2(n,j,k) = 0.d0
                    bm1(n,j,k) = 0.d0
                    db(n,j,k)  = 0.d0
                    bp1(n,j,k) = 0.d0
                    bp2(n,j,k) = 0.d0
                 end do
              end do
           end do

c     -- advective terms --
          call dsa_adv(jdim, kdim, q, xy, xyj, uu, vv, bm2, bm1, db,
     &          bp1, bp2, work(1,1), work(1,2))

c     -- production and destruction terms --
          call dsa_pd(jdim, kdim, q, xy, xyj, x, y, fmu, bm2, bm1, db,
     &          bp1, bp2, chi, dchi, fv1, dfv1, work(1,1), work(1,6),
     &          work(1,11), work(1,12), dprod, ddest, dsxip, dsxim,
     &          dsetap, dsetam)

c     -- diffusion terms --
          call dsa_dif (jdim, kdim, q, xy, xyj, fmu, dmul, work(1,1),
     &          work(1,2), work(1,3), bm2, bm1, db, bp1, bp2)

c     -- add to jacobian --
          call fillast (jdim, kdim, ndim, indx, icol, as, pa, bm2, bm1,
     &          db, bp1, bp2)

c     -- boundary conditions --
          if (imps) then
             call dsa_bc (jdim,kdim,ndim,indx,icol,q,xy,xyj,pa,as)
          end if
        end if
      end if

c     -- implicit artificial dissipation --
c     -- xi direction dissipation --
      ixy = 1
c     -- note coef4 holds spectral radius and coef2 is the pressure
c     switch --
      call spectx (jdim,kdim,uu,ccx,xyj,xy,coef4,precon)
      call gradcoef (ixy,jdim,kdim,press,xyj,coef2,work)

c     -- linearization of spectral radius for dissipation --
      if (jac_mat) then 
         call dspectx (jdim, kdim, ndim, indx, icol, q, xy, xyj, uu,
     &        sndsp, as, dprod, diagp, coef2, coef4, work, work(1,2),
     &        work(1,3), work(1,4), diag, wkb1, wkb2) 
      else
         call coef24x (jdim, kdim, coef2, coef4, work, work(1,2),
     &        work(1,5), work(1,6))
      end if

c     if (.not. frozen) 
      call fillasdx (jdim, kdim, ndim, xyj, work, coef2, coef4, indx,
     &     icol, as, pa, pdc)

c     -- eta direction dissipation --
      ixy = 2                                                 
      call specty (jdim,kdim,vv,ccy,xyj,xy,coef4,precon)
      call gradcoef (ixy,jdim,kdim,press,xyj,coef2,work)     

c     -- linearization of spectral radius for dissipation --
      if (jac_mat) then 
         call dspecty (jdim, kdim, ndim, indx, icol, q, xy, xyj, vv,
     &        sndsp, as, dprod, diagp, coef2, coef4, work, work(1,2),
     &        work(1,3), work(1,4), diag, wkb1, wkb2)
      else
         call coef24y (jdim, kdim, coef2, coef4, work, work(1,2),
     &        work(1,5), work(1,6))
      end if

c     if (.not. frozen) 
      call fillasdy (jdim, kdim, ndim, xyj, work, coef2, coef4, indx,
     &     icol, as, pa, pdc)

c     -- implicit b.c. --
      call impbc (jdim, kdim, ndim, indx, icol, iex, q, xy, xyj, x, y,
     &     as, pa, imps, bcf, press)
    
      return 
      end                       !drdq
