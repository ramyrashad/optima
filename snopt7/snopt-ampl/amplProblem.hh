#ifndef AMPLPROBLEM
#define AMPLPROBLEM

#include "snoptProblem.hh"

struct ASL;
struct Option_Info;

#ifdef __cplusplus
extern "C" {
#endif
  int amplusrfg_( integer    *Status, integer *n,    doublereal x[],
		   integer    *needF,  integer *neF,  doublereal F[],
		   integer    *needG,  integer *neG,  doublereal G[],
		   char       *cu,     integer *lencu,
		   integer    iu[],    integer *leniu,
		   doublereal ru[],    integer *lenru);

  int  amplusrf_( integer    *Status, integer *n,    doublereal x[],
		  integer    *needF,  integer *neF,  doublereal F[],
		  integer    *needG,  integer *neG,  doublereal G[],
		  char       *cu,     integer *lencu,
		  integer    iu[],    integer *leniu,
		  doublereal ru[],    integer *lenru );

#ifdef __cplusplus
}
#endif

/* class snoptProblem performs problem set up, initialization,  */
/* and problem specific variable storage. */
class amplProblem : public snoptProblem {
protected:
  int argc, ierr;
  char **argv;
  /** An ampl structure which for now holds name of solver. */
  Option_Info * oi;
  void makePrintName();
public:
  amplProblem( int argc, char **argv );
  ~amplProblem();
  virtual char  * pname() { return printname; }
  void alloc( integer n, integer neF );
  virtual void initialize();
};


#endif
