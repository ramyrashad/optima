c********************************************************
c*        Spalart-Allmaras one equation model           *
c********************************************************
c
      subroutine mutur(jdim,kdim,ifl,ifl2,q,press,turmu,fmu,x,y,xy,xyj,
     &                   uu,vv,delta_u,p,d,p1,d1,vorticity,p_prime,
     &                   d_prime,trip,work,workq,fnu,d_p_bar,
     &                   bx,cx,dx,by,cy,dy,fy,fx,ffy,ffx,fby)
c     &                   bx,cx,dx,by,cy,dy,fy,fx,ffy,ffx,fby,denom)
c

c
c        one equation turbulence model of Spalart-Allmaras
c                coded by : Philippe Godin
c
c        last modified by : Stan De Rango
c                           10/04/96
c          -program is more streamlined for efficiency and memory usage.
c          -not totally ... don't want to be fooling around
c                           with what "ain't" broke.
c          -lines commented out by me begin with 'csd'
c          -note: this subroutine was originally written for clarity and
c                 debuging purposes.  Many expressions appear to be repetitive.
c                 I've tried using more memory to store these frequently used
c                 expressions and save cpu time, but our SGI compilers are
c                 aware of the redundancy and optimize the code accordingly.
c          -the majority of the changes benefits the unsteady runs more than
c           steady computations.  When using the dual time-stepping scheme
c           you must subiterate dozens of times, hence I have tried to avoid
c           having to calculate many of the variables in each subiteration 
c           that don't change from subiteration to subiteration.
c
c
      include '../include/arcom.inc'
c
c
      dimension q(jdim,kdim,4),turmu(jdim,kdim)
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension fmu(jdim,kdim),turre2(maxj,maxk),uu(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim),vv(jdim,kdim)
      logical first
c
      common/mesup/ dtiseq(20),dtmins(20),
     &   isequal,jmxi(20),kmxi(20),jskipi(20),iends(20)
c
      dimension 
     &     p(jdim,kdim),d(jdim,kdim),delta_u(jdim,kdim),
     &     trip(jdim,kdim),d_prime(jdim,kdim),
     &     p_prime(jdim,kdim),d_p_bar(jdim,kdim),
     &     p1(jdim,kdim),d1(jdim,kdim),vorticity(jdim,kdim)
c
      dimension
     &     work(jdim,kdim),workq(jdim,kdim,3),fnu(jdim,kdim)
c
      dimension    
     &            bx(kdim,jdim),cx(kdim,jdim),dx(kdim,jdim),
     &            by(jdim,kdim),cy(jdim,kdim),dy(jdim,kdim),
     &            fy(jdim,kdim),fx(kdim,jdim),
     &            ffy(jdim,kdim),ffx(kdim,jdim),fby(jdim,kdim),
     &            denom(maxj,maxk)
c
      nnit=25
      isubmx=100
      if (meth.eq.2) isubmx=1000
      isub=0
      expon=1.d0/6.d0
      reinv=1.d0/re
c      retinf=1.d-1
      retinf=0.d0
      akarman = .41
      cb1     = 0.1355
csd   sigma   = 2.0/3.0
      sigmainv= 1.5d0
      resiginv= sigmainv*reinv
      cb2     = 0.622
      cw1     = cb1/(akarman**2)+(1.0+cb2)*sigmainv
      cw2     = 0.3
csd   underscore is equivalent to '**' ie. exponentiation
      cw3     = 2.0
      cw3_6   = 64.0
csd   const is used down below to aviod having to compute (...)**1/6
csd   inside a loop .. => very costly
      const   =(1.d0+cw3_6)**expon
      cv1     = 7.1
      cv1_3   = 357.911
      ct1     = 1.0
      ct2     = 2.0
      ct3     = 1.2
      ct4     = 0.5
      cr1     = 8.0
      cr2     = 0.89
c
c***********************************************************************
csd   Modified for time-accurate runs.
csd   => Original code had dtm=10 which may be fine for steady
csd      calculations but I need dtm=dt.
csd   => Remember that dt2=dt ... set in preproc.f
      if (unsted .and. ispmod.le.2) then
        dtm=dt2
      else
        dtm=dtow
      endif
c***********************************************************************
c
      do k=kbegin,kend
         do j=jbegin,jend
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
c     
            workq(j,k,1)=rho
            workq(j,k,2)=u
            workq(j,k,3)=v
            fnu(j,k)=fmu(j,k)/rho
         enddo
      enddo
c
csd   compute generalized distance function
csd   (smin and jmid are now stored in common blocks to avoid
csd   calculating the same quantities every iteration.)
c     
csd   Note: This loop is traversed only at the begining of a run.
csd         It is also traversed at the first iteration of any
csd         sequence when grid sequencing.
csd
      if (numiter.eq.istart .and. jsubit.eq.0) then
         do k=kbegin,kend
            do j=jbegin,jend
               j_point=j
               if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
               xpt1=x(j_point,kbegin)
               ypt1=y(j_point,kbegin)
               smin(j,k)=sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
            enddo
         enddo
c     
         chord=0.0
         fmin=100.0
         do j=jtranlo,jtranup-1
            chord=chord+sqrt((x(j,1)-x(j+1,1))**2+
     &           (y(j,1)-y(j+1,1))**2)
         enddo
         chord_2=chord/2.0
         do j=jtranlo,jtranup-1
            chord=chord+sqrt((x(j,1)-x(j+1,1))**2+
     &           (y(j,1)-y(j+1,1))**2)
            if (abs(chord-chord_2).lt.fmin) then
               fmin=abs(chord-chord_2)
               jmid=j+1
            endif
         enddo
c     
c         if (.not.restart) then 
         if (.not.restart .and. 
     &            (jmax.eq.jmxi(1) .and. kmax.eq.kmxi(1))) then
            do k=kbegin,kend
               do j=jbegin,jend
                  turre(j,k)=retinf
               enddo
            enddo
         elseif (.not.restart) then
            do k=kbegin,kend
               do j=jtranlo+1,jtranup-1
                 turre(j,k)=0.d0
               enddo
            enddo
         endif
c     
         do k=klow,kup
            do j=jlow,jmid                  
               d_trip(j,k)=sqrt((x(j,k)-x(jtranlo,1))**2
     &                               +(y(j,k)-y(jtranlo,1))**2)
            enddo
            do j=jmid+1,jup
               d_trip(j,k)=sqrt((x(j,k)-x(jtranup,1))**2
     &                               +(y(j,k)-y(jtranup,1))**2)
            enddo
         enddo
c     
         delta_xlo=0.5*(sqrt((x(jtranlo+1,1)-x(jtranlo-1,1))**2+
     &              (y(jtranlo+1,1)-y(jtranlo-1,1))**2))
         delta_xup=0.5*(sqrt((x(jtranup+1,1)-x(jtranup-1,1))**2+
     &              (y(jtranup+1,1)-y(jtranup-1,1))**2))
c     
         do j=jtail1,jtail2
            turre(j,1) = 0.0
         enddo
c        -end on if numiter.eq.istart
      endif
c     
c      
      do k=klow,kup
         do j=jlow,jmid                  
            delta_u(j,k)=max(1.d-50,
     &          sqrt((workq(j,k,2)-workq(jtranlo,1,2))**2
     &          +(workq(j,k,3)-workq(jtranlo,1,3))**2))
         enddo
         do j=jmid+1,jup
            delta_u(j,k)=max(1.d-50,
     &          sqrt((workq(j,k,2)-workq(jtranup,1,2))**2
     &          +(workq(j,k,3)-workq(jtranup,1,3))**2))
         enddo
      enddo
      
c      do j=jtail1,jtail2
c         turre(j,1) = 0.0
c      enddo
c     
c     
c     -tried to save time by using array vort(j,k), computed in
c      other subroutines, instead of calculating vorticity here.
c     -not the same answer so vorticity(j,k) stays in.
c
c     02/97 -the reason they are not the same is that here we compute
c            the vorticity at the k nodes, whereas vortdeta.f computes
c            it at k+1/2 nodes.
c
      if (iord.eq.4) then
         include 'vort_ord4.inc'
      else
         include 'vort_ord2.inc'
      endif
c
      omega_tlo=vorticity(jtranlo,1)
      omega_tup=vorticity(jtranup,1)
c      omega_tlo=max(1.d-50,vorticity(jtranlo,1))       
c      omega_tup=max(1.d-50,vorticity(jtranup,1))
c
c     
c ***************************************************************************
c                            Now the Model                      
c ***************************************************************************
c
csd   if ispmod=3, this is where we begin top of subiteration
 10   isub=isub + 1
c     
csd   z and zz are used as a temporary storage variables
      do 100 k=klow,kup
         do 90 j=jlow,jup
            zz=1.d0/(akarman*smin(j,k))**2
            chi_prime=1.d0/fnu(j,k)
            chi=turre(j,k)*chi_prime
            fv1=chi**3/(chi**3+cv1_3)
            fv1_prime=3.0*chi**2*chi_prime/(chi**3+cv1_3)
     &           -chi**3/(chi**3+cv1_3)**2*3.0*chi**2*chi_prime
            s=vorticity(j,k)*re
            fv2=1.0-(chi/(1.0+chi*fv1))
            fv2_prime=-chi_prime/(1.0+chi*fv1)+
     &           chi*(chi_prime*fv1+fv1_prime*chi)/((1.0+chi*fv1)**2)
            s_tilda=s+turre(j,k)*fv2*zz
            s_tilda_prime=zz*(fv2+turre(j,k)*fv2_prime)
      
            z=zz/s_tilda
            r=turre(j,k)*z
            r_prime=z-turre(j,k)*z/s_tilda*s_tilda_prime
            if (abs(r).ge.10.d0) then
csd            fw=(1.d0+cw3_6)**expon
               fw=const
               fw_prime=0.0
            else
               g=r+cw2*(r**6-r)
               z=((1.0+cw3_6)/(g**6+cw3_6))**expon 
               g_prime=r_prime+cw2*(6.0*r**5*r_prime-r_prime)
               fw=g*z
               fw_prime=g_prime*z*(1.-g**6/(g**6+cw3_6))
            endif
            ft2=ct3*exp(-ct4*chi**2)
            ft2_prime=-ct4*2.0*chi*chi_prime*ft2
      
      
            p1(j,k)=cb1*(1.0-ft2)*s_tilda*reinv
            p(j,k)=p1(j,k)*turre(j,k)
            p_prime(j,k)=cb1*(-ft2_prime*s_tilda+
     &           (1.0-ft2)*s_tilda_prime)*reinv
      
      
            d1(j,k)=((cw1*fw-cb1/(akarman)**2*ft2)
     &           *turre(j,k)/(smin(j,k))**2)*reinv
            d(j,k)=d1(j,k)*turre(j,k)
            d_prime(j,k)=(turre(j,k)/smin(j,k)**2*
     &           (cw1*fw_prime-cb1/(akarman)**2*ft2_prime)+
     &           (1.0/smin(j,k))**2*(cw1*fw
     &           -cb1/(akarman)**2*ft2))*reinv
 90      continue
 100  continue
c
c
      do 200 k=klow,kup
         do 180 j=jlow,jmid                  
             g_t=min(0.1,delta_u(j,k)/(omega_tlo*delta_xlo))
             factor=(ct2*(omega_tlo/delta_u(j,k))**2
     &          *(smin(j,k)**2+(g_t*d_trip(j,k))**2))
             if (factor.le.103.0) then
                ft1=ct1*g_t*exp(-factor)
                trip(j,k)=ft1*re*delta_u(j,k)**2
             else
                trip(j,k)=0.0
             endif
 180     continue
      
      
         do 190 j=jmid+1,jup
             g_t=min(0.1,delta_u(j,k)/(omega_tup*delta_xup))
             factor=(ct2*(omega_tup/delta_u(j,k))**2
     &          *(smin(j,k)**2+(g_t*d_trip(j,k))**2))
             if (factor.le.103.0) then
                ft1=ct1*g_t*exp(-factor)
                trip(j,k)=ft1*re*delta_u(j,k)**2
             else
                trip(j,k)=0.0
             endif
 190     continue
 200  continue
c     
      do k=kbegin,kend
         do j=jbegin,jend
            work(j,k)=fnu(j,k)+turre(j,k)
         enddo
      enddo
c
c     f_eta_eta viscous terms
c     
      do k=klow,kup
         kp1 = k+1
         km1 = k-1
         do j=jlow,jup
            xy3p     = .5*(xy(j,k,3)+xy(j,kp1,3))
            xy4p     = .5*(xy(j,k,4)+xy(j,kp1,4))
            ttp      =  (xy3p*xy(j,k,3)+xy4p*xy(j,k,4))
            
            xy3m     = .5*(xy(j,k,3)+xy(j,km1,3))
            xy4m     = .5*(xy(j,k,4)+xy(j,km1,4))
            ttm      =  (xy3m*xy(j,k,3)+xy4m*xy(j,k,4))
            
            cnud=cb2*resiginv*work(j,k)
            
            cdp       =    ttp*cnud
            cdm       =    ttm*cnud
            
            trem =.5*(work(j,km1)+work(j,k))
            trep =.5*(work(j,k)+work(j,kp1))
      
            cap  =  ttp*trep*(1.0+cb2)*resiginv
            cam  =  ttm*trem*(1.0+cb2)*resiginv
            
            by(j,k)   = cdm-cam
            cy(j,k)   = -cdp+cap-cdm+cam
            dy(j,k)   = cdp-cap
            
            fy(j,k)   = - by(j,k)*turre(j,km1)
     &           - cy(j,k)*turre(j,k  )
     &           - dy(j,k)*turre(j,kp1)
            
         enddo
      enddo
c     
c     advective terms in eta
c     
c      if (iord.eq.2) then
c        -first-order upwinding/downwinding        
         do k=klow,kup
         do j=jlow,jup
            sgnu = sign(1.,vv(j,k))
            app  = .5*(1.+sgnu)
            apm  = .5*(1.-sgnu)
            fy(j,k) = fy(j,k) - 
     &           vv(j,k)*( app*(turre(j,k)-turre(j,k-1))
     &           +apm*(turre(j,k+1)-turre(j,k)) )
            by(j,k)   = by(j,k)   - vv(j,k)*app
            cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
            dy(j,k)   = dy(j,k)   + vv(j,k)*apm
         enddo
         enddo
c      else
c         zz=1.d0/6.d0
c         do k=klow+1,kup-1
c         km2=k-2
c         km1=k-1
c         kp1=k+1
c         kp2=k+2
c         do j=jlow,jup
c            sgnu = sign(1.,vv(j,k))
c            app  = .5d0*(1.+sgnu)
c            apm  = .5d0*(1.-sgnu)
c            fy(j,k) = fy(j,k) - vv(j,k)*(
cc
cc          -second-order upwinding/downwinding
c     &     app*(turre(j,km2)-4.d0*turre(j,km1)+3.d0*turre(j,k))
c     &    +apm*(-3.d0*turre(j,k)+4.d0*turre(j,kp1)-turre(j,kp2)))*0.5d0
cc          -third-order biased upwinding/downwinding
cc     &     app*(turre(j,km2)-6.d0*turre(j,km1)
cc     &                              +3.d0*turre(j,k)+2.d0*turre(j,kp1))
cc     &    +apm*(-2.d0*turre(j,km1)-3.d0*turre(j,k)
cc     &                              +6.d0*turre(j,kp1)-turre(j,kp2)))*zz
cc
c            by(j,k)   = by(j,k)   - vv(j,k)*app
c            cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
c            dy(j,k)   = dy(j,k)   + vv(j,k)*apm
c         enddo
c         enddo
c
c         do k=klow,kup,kup-klow
c         km1=k-1
c         kp1=k+1
c         do j=jlow,jup
c            sgnu = sign(1.,vv(j,k))
c            app  = .5*(1.+sgnu)
c            apm  = .5*(1.-sgnu)
c            fy(j,k) = fy(j,k) - vv(j,k)*(
cc           -first-order upwinding/downwinding            
c     &            app*(turre(j,k)-turre(j,km1))
c     &           +apm*(turre(j,kp1)-turre(j,k)) )
cc
c            by(j,k)   = by(j,k)   - vv(j,k)*app
c            cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
c            dy(j,k)   = dy(j,k)   + vv(j,k)*apm
c         enddo
c         enddo
c      endif
c     
c     e_xi_xi viscous terms
c     
      do k=klow,kup
         do j=jlow,jup
            jp1 = j+1
            jm1 = j-1
            xy1p     = .5*(xy(j,k,1)+xy(jp1,k,1))
            xy2p     = .5*(xy(j,k,2)+xy(jp1,k,2))
            ttp      =  (xy1p*xy(j,k,1)+xy2p*xy(j,k,2))
            
            xy1m     = .5*(xy(j,k,1)+xy(jm1,k,1))
            xy2m     = .5*(xy(j,k,2)+xy(jm1,k,2))
            ttm      =  (xy1m*xy(j,k,1)+xy2m*xy(j,k,2))
            
            cnud=cb2*resiginv*work(j,k)
            
            cdp       =    ttp*cnud
            cdm       =    ttm*cnud               
c     
            trem =.5*(work(jm1,k)+work(j,k))
            trep =.5*(work(j,k)+work(jp1,k))
c            
            cap  =  ttp*trep*(1.0+cb2)*resiginv
            cam  =  ttm*trem*(1.0+cb2)*resiginv
            
c     
            bx(k,j)   = cdm-cam
            cx(k,j)   = -cdp+cap-cdm+cam
            dx(k,j)   = cdp-cap
            
            fy(j,k)   =  fy(j,k) - bx(k,j)*turre(jm1,k)
     &           - cx(k,j)*turre(j,k  )
     &           - dx(k,j)*turre(jp1,k) 
            
         enddo
      enddo
c     
c     advective terms in xi
c     
c      if (iord.eq.2) then
c        -first-order upwinding/downwinding
         do k=klow,kup
         do j=jlow,jup
            sgnu = sign(1.,uu(j,k))
            app  = .5*(1.+sgnu)
            apm  = .5*(1.-sgnu)
            fy(j,k)= fy(j,k) - 
     &           uu(j,k)*(app*(turre(j,k)-turre(j-1,k))
     &           +apm*(turre(j+1,k)-turre(j,k)) )
            bx(k,j)   = bx(k,j)   - uu(j,k)*app
            cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
            dx(k,j)   = dx(k,j)   + uu(j,k)*apm
         enddo
         enddo
c      else
c         zz=1.d0/6.d0
c         do k=klow,kup
c         do j=jlow+1,jup-1
c            jm2=j-2
c            jm1=j-1
c            jp1=j+1
c            jp2=j+2
c            sgnu = sign(1.,uu(j,k))
c            app  = .5*(1.+sgnu)
c            apm  = .5*(1.-sgnu)
c            fy(j,k)= fy(j,k) - uu(j,k)*(
cc           -second-order upwinding/downwinding
c     &      app*(3.d0*turre(j,k)-4.d0*turre(j-1,k)+turre(j-2,k))
c     &     +apm*(-3.d0*turre(j,k)+4.d0*turre(j+1,k)-turre(j+2,k)))*.5d0
cc           -third-order biased upwinding/downwinding
cc     &      app*(turre(jm2,k)-6.d0*turre(jm1,k)
cc     &                              +3.d0*turre(j,k)+2.d0*turre(jp1,k))
cc     &     +apm*(-2.d0*turre(jm1,k)-3.d0*turre(j,k)
cc     &                              +6.d0*turre(jp1,k)-turre(jp2,k)))*zz
c            bx(k,j)   = bx(k,j)   - uu(j,k)*app
c            cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
c            dx(k,j)   = dx(k,j)   + uu(j,k)*apm
c         enddo
c         enddo
cc
c         do k=klow,kup
c         do j=jlow,jup,jup-jlow
c            sgnu = sign(1.,uu(j,k))
c            app  = .5*(1.+sgnu)
c            apm  = .5*(1.-sgnu)
c            fy(j,k)= fy(j,k) - 
c     &           uu(j,k)*(app*(turre(j,k)-turre(j-1,k))
c     &           +apm*(turre(j+1,k)-turre(j,k)) )
c            bx(k,j)   = bx(k,j)   - uu(j,k)*app
c            cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
c            dx(k,j)   = dx(k,j)   + uu(j,k)*apm
c         enddo
c         enddo
c      endif
c
c
c ***************************************************************************
c                     Time Marching in Turbulence Model                      
c ***************************************************************************
c 
      if (ispmod.eq.1) then
         include 'org.inc'
      elseif (ispmod.eq.2) then
         include 'org2ord.inc'
      elseif (ispmod.eq.3) then
         include 'dualdt.inc'
c      else
c         include 'pull.inc'
      endif
c
c ***************************************************************************
c ***************************************************************************
c
c
c
c     apply boundary conditions
c       
 250  do k=kbegin,kend
         turre(jend,k)   = turre(jup,k)
         turre(jbegin,k) = turre(jlow,k)
      enddo
      
      do j=1,jtail1-1
         jj = jend - j + 1
         turre(j,1)  = .5d0*(turre(j,2)+turre(jj,2))
         turre(jj,1) = turre(j,1)
      enddo
c     
 222  format(16Hisub & tot_res =,I5,2e20.8)
      if (ispmod.ge.3) then
         tmp=tot_res
         tot_res=tot_res/tot_res2
         if (jsubit.eq.0) then
            if ((tot_res.gt.spmin_res) .and. (isub.lt.isubmx)) goto 10
         else
            if((tot_res.gt.spmin_res*1.d1).and.(isub.lt.isubmx)) goto 10
         endif
         write(6,222) isub,tot_res2,tmp
      endif
c
c
c
csd   -the eddy viscosity, turmu, is stored at half-points in eta direction.
csd    ie. at k+1/2 points.
csd              q(k+1/2)= .5*(q(k)+q(k+1)) + O(dy**2)
csd      if (iord.eq.4) then
csd         zz=1.d0/16.d0
csd         do 610 k=klow,kup-1
csd            km1=k-1
csd            kp1=k+1
csd            kp2=k+2
csd            do 600 j=jbegin,jend
csd               chim1=turre(j,km1)/fnu(j,km1)
csd               chi=turre(j,k)/fnu(j,k)
csd               chip1=turre(j,kp1)/fnu(j,kp1)
csd               chip2=turre(j,kp2)/fnu(j,kp2)
csd
csd               fv1m1=chim1**3/(chim1**3+cv1_3)
csd               fv1=chi**3/(chi**3+cv1_3)
csd               fv1p1=chip1**3/(chip1**3+cv1_3)
csd               fv1p2=chip2**3/(chip2**3+cv1_3)
csd
csd               fv1_c=zz*(-fv1m1+9.d0*(fv1+fv1p1)-fv1p2)
csdc
csd               turwrk_km1=turre(j,km1)*workq(j,km1,1)
csd               turwrk_k=turre(j,k)*workq(j,k,1)
csd               turwrk_kp1=turre(j,kp1)*workq(j,kp1,1)
csd               turwrk_kp2=turre(j,kp2)*workq(j,kp2,1)
csdc
csd               turmu(j,k) = fv1_c*zz*( 
csd     &            -turwrk_km1+9.d0*(turwrk_kp1+turwrk_k)-turwrk_kp2)
csd 600        continue
csd 610     continue
csdc
csd         k=kbegin
csd         kp1=k+1
csd         kp2=k+2
csd         zz=1.d0/8.d0
csd         do 620 j=jbegin,jend
csd               chi=turre(j,k)/fnu(j,k)
csd               chip1=turre(j,kp1)/fnu(j,kp1)
csd               chip2=turre(j,kp2)/fnu(j,kp2)
csdc
csd               fv1=chi**3/(chi**3+cv1_3)
csd               fv1p1=chip1**3/(chip1**3+cv1_3)
csd               fv1p2=chip2**3/(chip2**3+cv1_3)
csdc
csd               fv1_c=zz*(3.d0*fv1 + 6.d0*fv1p1 - fv1p2)
csdc
csd               turwrk_k=turre(j,k)*workq(j,k,1)
csd               turwrk_kp1=turre(j,kp1)*workq(j,kp1,1)
csd               turwrk_kp2=turre(j,kp2)*workq(j,kp2,1)
csdc
csd               turmu(j,k) = fv1_c*zz*( 
csd     &          3.d0*turwrk_k + 6.d0*turwrk_kp1 - turwrk_kp2)
csd 620     continue
csdc
csd         k=kup
csd         kp1=k+1
csd         do 630 j=jbegin,jend
csd            chi=turre(j,k)/fnu(j,k)
csd            fv1=chi**3/(chi**3+cv1_3)
csd            chip1=turre(j,kp1)/fnu(j,kp1)
csd            fv1p1=chip1**3/(chip1**3+cv1_3)
csd            fv1_c=0.5d0*(fv1+fv1p1)
csd            turmu(j,k) = fv1_c*.5d0*( 
csd     &           turre(j,k)*workq(j,k,1) + turre(j,kp1)*workq(j,kp1,1))
csd 630     continue
csd      else
         do k=kbegin,kup
            kp1=k+1
            do j=jbegin,jend
               chi=turre(j,k)/fnu(j,k)
               fv1=chi**3/(chi**3+cv1_3)
               chip1=turre(j,kp1)/fnu(j,kp1)
               fv1p1=chip1**3/(chip1**3+cv1_3)
               fv1_c=0.5d0*(fv1+fv1p1)
               turmu(j,k) = fv1_c*.5d0*( 
     &            turre(j,k)*workq(j,k,1) + turre(j,kp1)*workq(j,kp1,1))
            enddo
         enddo
csd      endif
c
      return
      end

c***********************************************************************
c*************** scalar tridiagonal ************************************
c***********************************************************************
      subroutine triv(jdim,kdim,jl,ju,kl,ku,x,a,b,c,f)
c
      dimension a(jdim,kdim),b(jdim,kdim),c(jdim,kdim)
      dimension x(jdim,kdim),f(jdim,kdim)
c
      do 10 j=jl,ju
      x(j,kl)=c(j,kl)/b(j,kl)
      f(j,kl)=f(j,kl)/b(j,kl)
 10   continue
      klp1 = kl +1
      do 1 i=klp1,ku
      do 20 j=jl,ju
         z=1./(b(j,i)-a(j,i)*x(j,i-1))
         x(j,i)=c(j,i)*z
         f(j,i)=(f(j,i)-a(j,i)*f(j,i-1))*z
 20   continue
1     continue
c
      kupkl=ku+kl
      do 2 i1=klp1,ku
         i=kupkl-i1
         do 30 j=jl,ju
         f(j,i)=f(j,i)-x(j,i)*f(j,i+1)
 30      continue
2     continue
c
      return
      end
