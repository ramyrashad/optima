      subroutine calcobjair(fobj,nout,skipend,jdim,kdim,deltat,y)

      implicit none

#include "../include/arcom.inc"

      integer i,j,k,nout,skipend,jdim,kdim

      double precision q(jdim,kdim,4),xyj(jdim,kdim)
      double precision turmu(jdim,kdim),y(jdim,kdim)

      double precision fobj, ftmp, qtar(jdim,kdim,4)

      double precision deltat
 
     
      fobj= 0.0d0    

      do i=skipend+1,nout

c        -- read physical target distribution --
         call iomarkus(4,i,jdim,kdim,qtar,xyj,turmu)

c        -- read physical flow field --         
         call iomarkus(3,i,jdim,kdim,q,xyj,turmu)
        
         do k = kbegin,kend
            do j = jbegin,jend
                                                                    
               fobj =  fobj + deltat* 0.5d0* (
     &              ( q(j,k,1) - qtar(j,k,1) )**2 + 
     &              ( q(j,k,2) - qtar(j,k,2) )**2 +
     &              ( q(j,k,3) - qtar(j,k,3) )**2 +
     &              ( q(j,k,4) - qtar(j,k,4) )**2 )
            end do
         end do
         
      end do

c     Constraint violation?
      ftmp= 0.0d0
      if (.not. sbbc .and. .not. periodic) 
     &     call CalcTCon (jdim, kdim, y, ftmp, .true.)
      fobj =  fobj + ftmp
     
 
      return
      end  !calcobjair




      subroutine calcobjair0(fobj,jdim,kdim,nout,skipend,
     &     x,y,xy,xyj)

      implicit none

#include "../include/arcom.inc"

      integer i,nout,skipend,jdim,kdim,tstart

      double precision q(jdim,kdim,4),xyj(*),xy(*),x(*),y(*)
      double precision turmu(jdim,kdim),precon(jdim,kdim,6)
      double precision fobj,ftmp,press(jdim,kdim)

      double precision cdmean,sndsp(jdim,kdim)
 
     
      fobj=0.0d0
      sumdt=0.0d0

      if (.not. errest) then
         tstart=skipend+1
      else
         tstart=skipend
      end if

      do i=tstart,nout

c        -- read physical flow field --
         
         call iomarkus(3,i,jdim,kdim,q,xyj,turmu)

c        -- calculate pressure

         call calcps (jdim, kdim, q, press, sndsp, precon, xy, xyj)

c        -- calculate drag

         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)

         cdmean = cdi + cdv

c        -- calculate mean drag
         if (.not. errest) then
c        simply sum them up and divide by total number
            fobj =  fobj + cdmean
         else
c        trapezoidal rule for every time step
            if (i.ne.tstart .and. i.ne.nout) then
c           interior points
               fobj =  fobj + cdmean*(dta(i)+dta(i+1))
               sumdt=sumdt+dta(i)
            else if (i.eq.tstart) then
c           first point
               fobj =  fobj + cdmean*dta(i+1)
            else if (i.eq.nout) then
c           last point
               fobj =  fobj + cdmean*dta(i)
               sumdt=sumdt+dta(i)
            end if
         end if         
      
      end do

      if (.not. errest) then
         fobj =  fobj/real(nout-skipend)
      else
         fobj =  fobj/(2.d0*sumdt) 
      end if


c     Constraint violation?
      ftmp= 0.0d0
      if (.not. sbbc .and. .not. periodic .and. .not. errest) 
     &     call CalcTCon (jdim, kdim, y, ftmp, .true.)
      fobj =  fobj + ftmp
     
 
      return
      end  !calcobjair0


      subroutine calcobjair5(fobj,jdim,kdim,nout,skipend,
     &     x,y,xy,xyj,cdmean,clmean,scalef)

      implicit none

#include "../include/arcom.inc"

      integer i,j,k,nout,skipend,jdim,kdim,tstart

      double precision q(jdim,kdim,4),xyj(*),xy(*),x(*),y(*)
      double precision turmu(jdim,kdim),precon(jdim,kdim,6)
      double precision fobj,ftmp,press(jdim,kdim),scalef

      double precision cdmean,clmean,sndsp(jdim,kdim)
 
     
      fobj= 0.0d0
      sumdt=0.0d0
      cdmean=0.0d0
      clmean=0.0d0

      if (.not. errest) then
         tstart=skipend+1
      else
         tstart=skipend
      end if

      do i=tstart,nout

c        -- read physical flow field --
         
         call iomarkus(3,i,jdim,kdim,q,xyj,turmu)

c        -- calculate pressure

         call calcps (jdim, kdim, q, press, sndsp, precon, xy, xyj)

c        -- calculate lift and drag at this time step

         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)

c        -- calculate mean lift and drag
         if (.not. errest) then
c        simply sum them up and divide by total number
            cdmean = cdmean+(cdi+cdv)
            clmean = clmean+(cli+clv)
         else
c        trapezoidal rule for every time step
            if (i.ne.tstart .and. i.ne.nout) then
c           interior points
               cdmean = cdmean+(cdi+cdv)*(dta(i)+dta(i+1))
               clmean = clmean+(cli+clv)*(dta(i)+dta(i+1))
               sumdt=sumdt+dta(i)
            else if (i.eq.tstart) then
c           first point
               cdmean = cdmean+(cdi+cdv)*dta(i+1)
               clmean = clmean+(cli+clv)*dta(i+1)
            else if (i.eq.nout) then
c           last point
               cdmean = cdmean+(cdi+cdv)*dta(i)
               clmean = clmean+(cli+clv)*dta(i)
               sumdt=sumdt+dta(i)
            end if
         end if         
      
      end do

c     -- finish calculate mean lift and drag
      if (.not. errest) then
         cdmean = cdmean/real(nout-skipend)
         clmean = clmean/real(nout-skipend)
      else
         cdmean = cdmean/(2.d0*sumdt) 
         clmean = clmean/(2.d0*sumdt)
      end if

c     -- calculate (mean drag / mean lift)
      fobj =  cdmean/clmean

c     scaling of the objective function
      if (scalef .eq. 1.d0 .and. .not. errest)  scalef=fobj        
      fobj=fobj/scalef

c     Constraint violation?
      ftmp= 0.0d0
      if (.not. sbbc .and. .not. periodic .and. .not. errest) 
     &     call CalcTCon (jdim, kdim, y, ftmp, .true.)
      fobj =  fobj + ftmp

      return
      end  !calcobjair5





      subroutine calcobjair8(fobj,nout,skipend,jdim,kdim,
     &     deltat,scalef,x,y,jsta,jsto)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i,j,k,nout,skipend,jdim,kdim,jsta,jsto

      double precision q(jdim,kdim,4),xyj(jdim,kdim)
      double precision turmu(jdim,kdim),x(jdim,kdim),y(jdim,kdim)

      double precision fobj,ftmp, qtar(jdim,kdim,4),scalef,deltat

      double precision pre,pret,dis
 
     
      fobj= 0.0d0

      do i=skipend+1,nout

c        -- read physical target distribution --
         call iomarkus(4,i,jdim,kdim,qtar,xyj,turmu)

c        -- read physical flow field --         
         call iomarkus(3,i,jdim,kdim,q,xyj,turmu)
     

c     Part I: C-shape
         k=klineconst

c        -- figure out jsta --
         dis =2.d0
         do j = jbegin,jend/2
             if (dabs(x(j,k)-remxmax).lt.dis) then
                dis = dabs(x(j,k)-remxmax)
                jsta=j 
             end if
         end do

c        -- calculate jsto --
         jsto=jend-jsta+1
        
         do j = jsta,jsto

            pre=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1))

            pret=gami*(qtar(j,k,4)-0.5*(qtar(j,k,2)**2+qtar(j,k,3)**2)
     &           /qtar(j,k,1))
                                                                    
            fobj =  fobj + deltat*0.5d0*(pre - pret)**2 
            
         end do

c     Part II: straight down close to x=remxmax above wake cut

         j=jsto

         do k = klineconst-1,1,-1

            pre=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1))

            pret=gami*(qtar(j,k,4)-0.5*(qtar(j,k,2)**2+qtar(j,k,3)**2)
     &           /qtar(j,k,1))
                                                                    
            fobj =  fobj + deltat*0.5d0*(pre - pret)**2 
            
         end do



c     Part III: straight down close to x=remxmax below wake cut

         j=jsta

         do k = 1,klineconst-1

            pre=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1))

            pret=gami*(qtar(j,k,4)-0.5*(qtar(j,k,2)**2+qtar(j,k,3)**2)
     &           /qtar(j,k,1))
                                                                    
            fobj =  fobj + deltat*0.5d0*(pre - pret)**2 
            
         end do

        
      end do !sum over time




c     scaling of the objective function

      if (scalef .eq. 1.d0)  scalef=fobj         
      fobj=fobj/scalef

c     Constraint violation?
      ftmp= 0.0d0
      if (.not. sbbc .and. .not. periodic) 
     &     call CalcTCon (jdim, kdim, y, ftmp, .true.)
      fobj =  fobj + ftmp           
 
      return
      end  !calcobjair8








      subroutine calcobjair9(fobj,nout,skipend,jdim,kdim,
     &     deltat,scalef,x,y)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i,j,k,ii,jj,kk,nout,skipend,jdim,kdim,jmy,kmy,jmytmp

      double precision q(jdim,kdim,4),xyj(jdim,kdim)
      double precision turmu(jdim,kdim),x(jdim,kdim),y(jdim,kdim)
      double precision xwant(1000),ywant(1000)

      double precision fobj,ftmp, qtar(jdim,kdim,4),scalef

      double precision deltat,xval(3),yval(3),betastar
      double precision ax,bx,cx,ay,by,cy,pre,pret
      double precision apx,bpx,cpx,apy,bpy,cpy,ppl,pne,pval(3)
      double precision aptx,bptx,cptx,apty,bpty,cpty,ptpl,ptne,ptval(3)
      double precision remdx,remdy,xp,yp,dis,distmp
       
      fobj= 0.0d0

      remdx=(remxmax-remxmin)/dble(remnx-1)
      remdy=(remymax-remymin)/dble(remny-1)
      do j = 1,remnx
         xwant(j)=remxmin+remdx*dble(j-1)
      end do
      do k = 1,remny
         ywant(k)=remymin+remdy*dble(k-1)
      end do

      do ii=skipend+1,nout

c        -- read physical target distribution --
         call iomarkus(4,ii,jdim,kdim,qtar,xyj,turmu)

c        -- read physical flow field --         
         call iomarkus(3,ii,jdim,kdim,q,xyj,turmu)




c     lower x-arm

         do jj = 1,remnx

c     -- compute point closest to desired point --

            xp = xwant(jj)
            yp = remymin 
        
            dis=2.d0
            do k = kbegin,kend
               do j = jbegin,jend/2
                  distmp = dsqrt((x(j,k)-xp)**2+(y(j,k)-yp)**2)
                  if (distmp.lt.dis) then
                     dis=distmp
                     jmy=j
                     kmy=k
                  endif
               end do
            end do


c     -- parametrize and use quadratic to calculate xval as well as pval and ptval

            do k=-1,1

               j=jmy+k

               cy=y(j,kmy-1)
               by=0.5d0*(4.d0*y(j,kmy)-3.d0*cy-y(j,kmy+1))
               ay=0.5d0*(cy-2.d0*y(j,kmy)+y(j,kmy+1))

               dis=0.25d0*by**2/ay**2-(cy-yp)/ay

               betastar=-0.5d0*by/ay-sqrt(dis)
               
               if (betastar .lt. 0.d0 .or. betastar .gt. 2.d0) then
                  betastar=-0.5d0*by/ay+sqrt(dis)
               end if

               cx=x(j,kmy-1)
               bx=0.5d0*(4.d0*x(j,kmy)-3.d0*cx-x(j,kmy+1))
               ax=0.5d0*(cx-2.d0*x(j,kmy)+x(j,kmy+1))

               xval(2+k)=ax*betastar**2+bx*betastar+cx
               
               ppl=gami*(q(j,kmy+1,4)-0.5d0*(q(j,kmy+1,2)**2+
     &              q(j,kmy+1,3)**2)/q(j,kmy+1,1))
               pne=gami*(q(j,kmy,4)-0.5d0*(q(j,kmy,2)**2+
     &              q(j,kmy,3)**2)/q(j,kmy,1))
           
               cpy=gami*(q(j,kmy-1,4)-0.5d0*(q(j,kmy-1,2)**2+
     &              q(j,kmy-1,3)**2)/q(j,kmy-1,1))

               bpy=0.5d0*(4.d0*pne-3.d0*cpy-ppl)
               apy=0.5d0*(cpy-2.d0*pne+ppl)

               pval(2+k)=apy*betastar**2+bpy*betastar+cpy

               ptpl=gami*(qtar(j,kmy+1,4)-0.5d0*(qtar(j,kmy+1,2)**2+
     &              qtar(j,kmy+1,3)**2)/qtar(j,kmy+1,1))
               ptne=gami*(qtar(j,kmy,4)-0.5d0*(qtar(j,kmy,2)**2+
     &              qtar(j,kmy,3)**2)/qtar(j,kmy,1))
           
               cpty=gami*(qtar(j,kmy-1,4)-0.5d0*(qtar(j,kmy-1,2)**2+
     &              qtar(j,kmy-1,3)**2)/qtar(j,kmy-1,1))

               bpty=0.5d0*(4.d0*ptne-3.d0*cpty-ptpl)
               apty=0.5d0*(cpty-2.d0*ptne+ptpl)

               ptval(2+k)=apty*betastar**2+bpty*betastar+cpty

            end do

c     parametrize and use quadratic to interpolate pre and pret
            cx=xval(3)
            bx=0.5d0*(4.d0*xval(2)-3.d0*cx-xval(1))
            ax=0.5d0*(cx-2.d0*xval(2)+xval(1))           
            
            dis=0.25d0*bx**2/ax**2-(cx-xp)/ax
         
            betastar=-0.5d0*bx/ax-sqrt(dis)
            
            if (betastar .lt. 0.d0 .or. betastar .gt. 2.d0) then
               betastar=-0.5d0*bx/ax+sqrt(dis)
            end if

            cpx=pval(3)
            bpx=0.5d0*(4.d0*pval(2)-3.d0*cpx-pval(1))
            apx=0.5d0*(cpx-2.d0*pval(2)+pval(1)) 

            pre=apx*betastar**2+bpx*betastar+cpx

            cptx=ptval(3)
            bptx=0.5d0*(4.d0*ptval(2)-3.d0*cptx-ptval(1))
            aptx=0.5d0*(cptx-2.d0*ptval(2)+ptval(1)) 

            pret=aptx*betastar**2+bptx*betastar+cptx

c     -- add to objective function
                                                                    
            fobj =  fobj + deltat*0.5d0*(pre-pret)**2 
            
         end do   ! lower x-arm






c     upper x-arm

         do jj = 1,remnx

c     -- compute point closest to desired point --

            xp = xwant(jj)
            yp = remymax 
        
            dis=2.d0
            do k = kbegin,kend
               do j = jbegin/2,jend
                  distmp = dsqrt((x(j,k)-xp)**2+(y(j,k)-yp)**2)
                  if (distmp.lt.dis) then
                     dis=distmp
                     jmy=j
                     kmy=k
                  endif
               end do
            end do


c     -- parametrize and use quadratic to calculate xval as well as pval and ptval

            do k=-1,1

               j=jmy+k

               cy=y(j,kmy-1)
               by=0.5d0*(4.d0*y(j,kmy)-3.d0*cy-y(j,kmy+1))
               ay=0.5d0*(cy-2.d0*y(j,kmy)+y(j,kmy+1))

               dis=0.25d0*by**2/ay**2-(cy-yp)/ay

               betastar=-0.5d0*by/ay-sqrt(dis)
               
               if (betastar .lt. 0.d0 .or. betastar .gt. 2.d0) then
                  betastar=-0.5d0*by/ay+sqrt(dis)
               end if

               cx=x(j,kmy-1)
               bx=0.5d0*(4.d0*x(j,kmy)-3.d0*cx-x(j,kmy+1))
               ax=0.5d0*(cx-2.d0*x(j,kmy)+x(j,kmy+1))

               xval(2+k)=ax*betastar**2+bx*betastar+cx

c               print *,xwant(jj),xval(2+k),ay*betastar**2+by*betastar+cy
               
               ppl=gami*(q(j,kmy+1,4)-0.5d0*(q(j,kmy+1,2)**2+
     &              q(j,kmy+1,3)**2)/q(j,kmy+1,1))
               pne=gami*(q(j,kmy,4)-0.5d0*(q(j,kmy,2)**2+
     &              q(j,kmy,3)**2)/q(j,kmy,1))
           
               cpy=gami*(q(j,kmy-1,4)-0.5d0*(q(j,kmy-1,2)**2+
     &              q(j,kmy-1,3)**2)/q(j,kmy-1,1))

               bpy=0.5d0*(4.d0*pne-3.d0*cpy-ppl)
               apy=0.5d0*(cpy-2.d0*pne+ppl)

               pval(2+k)=apy*betastar**2+bpy*betastar+cpy

               ptpl=gami*(qtar(j,kmy+1,4)-0.5d0*(qtar(j,kmy+1,2)**2+
     &              qtar(j,kmy+1,3)**2)/qtar(j,kmy+1,1))
               ptne=gami*(qtar(j,kmy,4)-0.5d0*(qtar(j,kmy,2)**2+
     &              qtar(j,kmy,3)**2)/qtar(j,kmy,1))
           
               cpty=gami*(qtar(j,kmy-1,4)-0.5d0*(qtar(j,kmy-1,2)**2+
     &              qtar(j,kmy-1,3)**2)/qtar(j,kmy-1,1))

               bpty=0.5d0*(4.d0*ptne-3.d0*cpty-ptpl)
               apty=0.5d0*(cpty-2.d0*ptne+ptpl)

               ptval(2+k)=apty*betastar**2+bpty*betastar+cpty

            end do

c     parametrize and use quadratic to interpolate pre and pret
            cx=xval(3)
            bx=0.5d0*(4.d0*xval(2)-3.d0*cx-xval(1))
            ax=0.5d0*(cx-2.d0*xval(2)+xval(1))           
            
            dis=0.25d0*bx**2/ax**2-(cx-xp)/ax
         
            betastar=-0.5d0*bx/ax-sqrt(dis)
            
            if (betastar .lt. 0.d0 .or. betastar .gt. 2.d0) then
               betastar=-0.5d0*bx/ax+sqrt(dis)
            end if

c            print *,xwant(jj),ax*betastar**2+bx*betastar+cx

            cpx=pval(3)
            bpx=0.5d0*(4.d0*pval(2)-3.d0*cpx-pval(1))
            apx=0.5d0*(cpx-2.d0*pval(2)+pval(1)) 

            pre=apx*betastar**2+bpx*betastar+cpx

            cptx=ptval(3)
            bptx=0.5d0*(4.d0*ptval(2)-3.d0*cptx-ptval(1))
            aptx=0.5d0*(cptx-2.d0*ptval(2)+ptval(1)) 

            pret=aptx*betastar**2+bptx*betastar+cptx

c     -- add to objective function
                                                                    
            fobj =  fobj + deltat*0.5d0*(pre-pret)**2 
            
         end do   ! upper x-arm





c     left y-arm

         do kk = 1,remny

c     -- compute point closest to desired point --

            xp = remxmin
            yp = ywant(kk)
        
            dis=2.d0
            do k = kbegin,kend
               do j = jbegin/3,2*jend/3
                  distmp = dsqrt((x(j,k)-xp)**2+(y(j,k)-yp)**2)
                  if (distmp.lt.dis) then
                     dis=distmp
                     jmy=j
                     kmy=k
                  endif
               end do
            end do

c     -- parametrize and use quadratic to calculate yval as well as pval and ptval

            do k=-1,1

               j=jmy+k

               cx=x(j,kmy+1)

               bx=0.5d0*(4.d0*x(j,kmy)-3.d0*cx-x(j,kmy-1))
               ax=0.5d0*(cx-2.d0*x(j,kmy)+x(j,kmy-1))           

               dis=0.25d0*bx**2/ax**2-(cx-xp)/ax

               betastar=-0.5d0*bx/ax-sqrt(dis)

               if (betastar .lt. 0.d0 .or. betastar .gt. 2.d0) then
                  betastar=-0.5d0*bx/ax+sqrt(dis)
               end if

               cy=y(j,kmy+1)
               by=0.5d0*(4.d0*y(j,kmy)-3.d0*cy-y(j,kmy-1))
               ay=0.5d0*(cy-2.d0*y(j,kmy)+y(j,kmy-1))

               yval(2+k)=ay*betastar**2+by*betastar+cy

c               print *,ywant(kk),yval(2+k),ax*betastar**2+bx*betastar+cx


               ppl=gami*(q(j,kmy-1,4)-0.5d0*(q(j,kmy-1,2)**2+
     &              q(j,kmy-1,3)**2)/q(j,kmy-1,1))
               pne=gami*(q(j,kmy,4)-0.5d0*(q(j,kmy,2)**2+
     &              q(j,kmy,3)**2)/q(j,kmy,1))
           
               cpx=gami*(q(j,kmy+1,4)-0.5d0*(q(j,kmy+1,2)**2+
     &              q(j,kmy+1,3)**2)/q(j,kmy+1,1))           

               bpx=0.5d0*(4.d0*pne-3.d0*cpx-ppl)
               apx=0.5d0*(cpx-2.d0*pne+ppl)

               pval(2+k)=apx*betastar**2+bpx*betastar+cpx

               ptpl=gami*(qtar(j,kmy-1,4)-0.5d0*(qtar(j,kmy-1,2)**2+
     &              qtar(j,kmy-1,3)**2)/qtar(j,kmy-1,1))
               ptne=gami*(qtar(j,kmy,4)-0.5d0*(qtar(j,kmy,2)**2+
     &              qtar(j,kmy,3)**2)/qtar(j,kmy,1))
           
               cptx=gami*(qtar(j,kmy+1,4)-0.5d0*(qtar(j,kmy+1,2)**2+
     &              qtar(j,kmy+1,3)**2)/qtar(j,kmy+1,1))           

               bptx=0.5d0*(4.d0*ptne-3.d0*cptx-ptpl)
               aptx=0.5d0*(cptx-2.d0*ptne+ptpl)

               ptval(2+k)=aptx*betastar**2+bptx*betastar+cptx              
             
            end do

c     parametrize and use quadratic to interpolate pre and pret
            cy=yval(3)
            by=0.5d0*(4.d0*yval(2)-3.d0*cy-yval(1))
            ay=0.5d0*(cy-2.d0*yval(2)+yval(1))           
            
            dis=0.25d0*by**2/ay**2-(cy-yp)/ay
         
            betastar=-0.5d0*by/ay-sqrt(dis)
            
            if (betastar .lt. 0.d0 .or. betastar .gt. 2.d0) then
               betastar=-0.5d0*by/ay+sqrt(dis)
            end if

c            print *,ywant(kk),ay*betastar**2+by*betastar+cy 

            cpy=pval(3)
            bpy=0.5d0*(4.d0*pval(2)-3.d0*cpy-pval(1))
            apy=0.5d0*(cpy-2.d0*pval(2)+pval(1)) 

            pre=apy*betastar**2+bpy*betastar+cpy

            cpty=ptval(3)
            bpty=0.5d0*(4.d0*ptval(2)-3.d0*cpty-ptval(1))
            apty=0.5d0*(cpty-2.d0*ptval(2)+ptval(1)) 

            pret=apty*betastar**2+bpty*betastar+cpty

c     -- add to objective function
                                                                    
            fobj =  fobj + deltat*0.5d0*(pre-pret)**2 
            
         end do   ! left y-arm





c     right y-arm

         do kk = 1,remny

c     -- compute point closest to desired point --

            xp = remxmax
            yp = ywant(kk)
        
            dis=2.d0
            do k = kbegin,kend
               do j = jbegin,jend
                  distmp = dsqrt((x(j,k)-xp)**2+(y(j,k)-yp)**2)
                  if (distmp.lt.dis) then
                     dis=distmp
                     jmytmp=j
                     kmy=k
                  endif
               end do
            end do

c            print *, yp,jmytmp,kmy,y(jmytmp,kmy)
            
c     -- parametrize and use quadratic to calculate yval as well as pval and ptval

            do j=-1,1

               k=kmy+j
               jmy=jmytmp

c              -- special wake cut treatment

               if (k.lt.kbegin) then
                  k=kbegin
                  jmy=jend-jmytmp+1
               end if

               cx=x(jmy+1,k)

               bx=0.5d0*(4.d0*x(jmy,k)-3.d0*cx-x(jmy-1,k))
               ax=0.5d0*(cx-2.d0*x(jmy,k)+x(jmy-1,k))           

               dis=0.25d0*bx**2/ax**2-(cx-xp)/ax

               betastar=-0.5d0*bx/ax-sqrt(dis)

               if (betastar .lt. 0.d0 .or. betastar .gt. 2.d0) then
                  betastar=-0.5d0*bx/ax+sqrt(dis)
               end if

               cy=y(jmy+1,k)
               by=0.5d0*(4.d0*y(jmy,k)-3.d0*cy-y(jmy-1,k))
               ay=0.5d0*(cy-2.d0*y(jmy,k)+y(jmy-1,k))

               yval(2+j)=ay*betastar**2+by*betastar+cy

c               print *,ywant(kk),yval(2+j),ax*betastar**2+bx*betastar+cx


               ppl=gami*(q(jmy-1,k,4)-0.5d0*(q(jmy-1,k,2)**2+
     &              q(jmy-1,k,3)**2)/q(jmy-1,k,1))
               pne=gami*(q(jmy,k,4)-0.5d0*(q(jmy,k,2)**2+
     &              q(jmy,k,3)**2)/q(jmy,k,1))
           
               cpx=gami*(q(jmy+1,k,4)-0.5d0*(q(jmy+1,k,2)**2+
     &              q(jmy+1,k,3)**2)/q(jmy+1,k,1))           

               bpx=0.5d0*(4.d0*pne-3.d0*cpx-ppl)
               apx=0.5d0*(cpx-2.d0*pne+ppl)

               pval(2+j)=apx*betastar**2+bpx*betastar+cpx

               ptpl=gami*(qtar(jmy-1,k,4)-0.5d0*(qtar(jmy-1,k,2)**2+
     &              qtar(jmy-1,k,3)**2)/qtar(jmy-1,k,1))
               ptne=gami*(qtar(jmy,k,4)-0.5d0*(qtar(jmy,k,2)**2+
     &              qtar(jmy,k,3)**2)/qtar(jmy,k,1))
           
               cptx=gami*(qtar(jmy+1,k,4)-0.5d0*(qtar(jmy+1,k,2)**2+
     &              qtar(jmy+1,k,3)**2)/qtar(jmy+1,k,1))           

               bptx=0.5d0*(4.d0*ptne-3.d0*cptx-ptpl)
               aptx=0.5d0*(cptx-2.d0*ptne+ptpl)

               ptval(2+j)=aptx*betastar**2+bptx*betastar+cptx              
             
            end do

c     parametrize and use quadratic to interpolate pre and pret
            cy=yval(3)
            by=0.5d0*(4.d0*yval(2)-3.d0*cy-yval(1))
            ay=0.5d0*(cy-2.d0*yval(2)+yval(1))           
            
            dis=0.25d0*by**2/ay**2-(cy-yp)/ay
         
            betastar=-0.5d0*by/ay-sqrt(dis)
            
            if (betastar .lt. 0.d0 .or. betastar .gt. 2.d0) then
               betastar=-0.5d0*by/ay+sqrt(dis)
            end if

c            print *,ywant(kk),ay*betastar**2+by*betastar+cy 

            cpy=pval(3)
            bpy=0.5d0*(4.d0*pval(2)-3.d0*cpy-pval(1))
            apy=0.5d0*(cpy-2.d0*pval(2)+pval(1)) 

            pre=apy*betastar**2+bpy*betastar+cpy

            cpty=ptval(3)
            bpty=0.5d0*(4.d0*ptval(2)-3.d0*cpty-ptval(1))
            apty=0.5d0*(cpty-2.d0*ptval(2)+ptval(1)) 

            pret=apty*betastar**2+bpty*betastar+cpty

c     -- add to objective function
                                                                    
            fobj =  fobj + deltat*0.5d0*(pre-pret)**2 
            
         end do   ! right y-arm



        
      end do   ! sum over time



c     scaling of the objective function

      if (scalef .eq. 1.d0)  scalef=fobj         
      fobj=fobj/scalef

c     Constraint violation?
      ftmp= 0.0d0
      if (.not. sbbc .and. .not. periodic) 
     &     call CalcTCon (jdim, kdim, y, ftmp, .true.)
      fobj =  fobj + ftmp 


      return
      end  !calcobjair9





      subroutine calcobjairnoise(fobj,nout,skipend,jdim,kdim,
     &     deltat,scalef,x,y,jsta,jsto,xy,dpod,win,po,ns,deriv)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i,j,k,nout,skipend,jdim,kdim,jsta,jsto
      integer tc,sc,Nsous,Nsout,INOUT

      double precision q(jdim,kdim,4),xyj(jdim,kdim)
      double precision turmu(jdim,kdim),x(jdim,kdim),y(jdim,kdim)
      double precision xy(jdim,kdim,4)

      double precision fobj,ftmp,scalef,deltat,win(tdim)

      double precision dis,den

      double precision ps(tdim,sdim),us(tdim,sdim),vs(tdim,sdim)
      double precision rhos(tdim,sdim),xs(sdim),ys(sdim),ns(sdim,2)
      double precision xo(sdim),yo(sdim),po(tdim,sdim)

      double precision pcomp(tdim),po_tar(tdim,sdim),poav(sdim)
      complex*16 dpod(Nobs,tdim,sdim,3)
      logical deriv
     
      fobj= 0.0d0


c     figure out jsta and jsto--

      if (.not. periodic) then   !C-Mesh

         dis =2.d0
         k=klineconst
         do j = jbegin,jend/2
            if (dabs(x(j,k)-remxmax).lt.dis) then
               dis = dabs(x(j,k)-remxmax)
               jsta=j 
            end if
         end do

c        -- calculate jsto --
         jsto=jend-jsta+1

      else  ! O-mesh

         jsta=jbegin
         jsto=jend

      end if

c
c     Calculate normal projections along the FWH control surface and save xs and ys
c     the defining values for the surface.
c 
c     Procedure: Along a constant eta line the normal is given by nabla eta = (etax,etay) and
c     along a constant xi line the normal is given by nabla xi = (xix,xiy).
c
c     Directivity is tricky: The normal has to point away from the surface. I walk a little bit along
c     the normal in question and check whether this point is inside the polygon. If it is I have to change
c     the signs of the normal components.

      sc=0

c     Part I: C-shape or O-shape
      k=klineconst
        
      do j = jsta,jsto

         sc=sc+1

         xs(sc)=x(j,k)
         ys(sc)=y(j,k)

         den=SQRT(xy(j,k,3)**2+xy(j,k,4)**2)
         ns(sc,1)=xy(j,k,3)/den
         ns(sc,2)=xy(j,k,4)/den

      end do


      if (.not. periodic) then   !C-Mesh

c     Part II: straight down close to x=remxmax above wake cut

         j=jsto

         do k = klineconst-1,1,-1
            
            sc=sc+1
            
            xs(sc)=x(j,k)
            ys(sc)=y(j,k)
  
            den=SQRT(xy(j,k,1)**2+xy(j,k,2)**2)
            ns(sc,1)=xy(j,k,1)/den
            ns(sc,2)=xy(j,k,2)/den
         
         end do
  

c     Part III: straight down close to x=remxmax below wake cut

         j=jsta
      
         do k = 1,klineconst-1
            
            sc=sc+1
            
            xs(sc)=x(j,k)
            ys(sc)=y(j,k)
            
            den=SQRT(xy(j,k,1)**2+xy(j,k,2)**2)
            ns(sc,1)=xy(j,k,1)/den
            ns(sc,2)=xy(j,k,2)/den
                     
         end do

      end if  !C-mesh

      Nsous=sc

C     Take care of directivity using subroutine PNPOLY
C     INOUT - THE SIGNAL RETURNED: 
C     -1 IF THE POINT IS OUTSIDE OF THE POLYGON, 
C     0 IF THE POINT IS ON AN EDGE OR AT A VERTEX, 
C     1 IF THE POINT IS INSIDE OF THE POLYGON.

      do i=1,Nsous

         call PNPOLY(xs(i)+1.d-5*ns(i,1),ys(i)+1.d-5*ns(i,2),
     &        xs,ys,Nsous,INOUT)

         if (INOUT .eq. 1) then
            ns(i,1)=-ns(i,1)
            ns(i,2)=-ns(i,2) 
         end if

      end do


c$$$      do i=1,Nsous
c$$$         print *, ns(i,1),ns(i,2)
c$$$c         print *, xs(i),ys(i)
c$$$      end do
c$$$      stop  




c
c     Calculate pressure, velocities, and density along the FWH control surface 
c     

      tc=0

      do i=skipend+1,nout

c        -- read physical flow field --         
         call iomarkus(3,i,jdim,kdim,q,xyj,turmu)

         sc=0
         tc=tc+1 

         pcomp(tc)=gami*(q(jobs,kobs,4)-0.5*(q(jobs,kobs,2)**2
     &           +q(jobs,kobs,3)**2)/q(jobs,kobs,1))

c$$$         print *,tc,pcomp(tc),gami*(q(jend-jobs+2,kobs,4) - 
c$$$     &        0.5*( q(jend-jobs+2,kobs,2)**2+q(jend-jobs+2,kobs,3)**2)
c$$$     &           /q(jend-jobs+2,kobs,1) ) 

c     Part I: C-shape or O-shape
         k=klineconst
        
         do j = jsta,jsto

            sc=sc+1  

            rhos(tc,sc)=q(j,k,1)
            us(tc,sc)=q(j,k,2)/q(j,k,1)
            vs(tc,sc)=q(j,k,3)/q(j,k,1)
            ps(tc,sc)=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)
     &           /q(j,k,1))

         end do

         if (.not. periodic) then !C-Mesh

c           Part II: straight down close to x=remxmax above wake cut

            j=jsto

            do k = klineconst-1,1,-1

               sc=sc+1  
               
               rhos(tc,sc)=q(j,k,1)
               us(tc,sc)=q(j,k,2)/q(j,k,1)
               vs(tc,sc)=q(j,k,3)/q(j,k,1)
               ps(tc,sc)=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)
     &              /q(j,k,1))
     
            end do
            
            

c           Part III: straight down close to x=remxmax below wake cut

            j=jsta

            do k = 1,klineconst-1

               sc=sc+1  
               
               rhos(tc,sc)=q(j,k,1)
               us(tc,sc)=q(j,k,2)/q(j,k,1)
               vs(tc,sc)=q(j,k,3)/q(j,k,1)
               ps(tc,sc)=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)
     &              /q(j,k,1))
                     
            end do

         end if     !C-Mesh

      end do !Calculate quantities along the FWH control surface


      Nsous=sc
      Nsout=tc
c
c     Set up observer points, we are interested in (only one supported for optimization for now)
c

      if ( abs(xobs).gt.0.d0 .or. abs(yobs).gt.0.d0 ) 
     &     then

         xo(1)=xobs
         yo(1)=yobs

      else

         do j=1,Nobs
            xo(j)=x(jobs+j-1,kobs)
            yo(j)=y(jobs+j-1,kobs)
         end do

      end if

c
c     For directivity plots, if outFWH=2
c

      if (outFWH.eq.2) then
         
         Nobs=64

         do j=1,Nobs
            xo(j)=xobs*DCOS(2.d0*4.d0*atan(1.d0)*(j-1)/DBLE(Nobs))
            yo(j)=xobs*DSIN(2.d0*4.d0*atan(1.d0)*(j-1)/DBLE(Nobs))
         end do

         deriv=.false.

      end if

c     Calculate the pressure po in these observer points via the FWH approach
c


      call calcFWH(Nsous,Nsout,xo,yo,po,xs,ys,ns,rhos,us,vs,ps,
     &     deltat,dpod,win,deriv,pcomp)

      
c
c     Calculate the objective function
c

      if (obj_func.eq.10) then 

         do j=1,Nobs
            do i=1,Nsout
               read ( cptar_unit, *) po_tar(i,j)
            end do
         end do
         rewind(cptar_unit)
      
         do j=1,Nobs
            do i=1,Nsout
               
               fobj =  fobj + (po(i,j)-po_tar(i,j))**2
               
            end do
         end do

      else if (obj_func.eq.14) then

         do j=1,Nobs
            poav(j)=0.d0
            do i=1,Nsout         
               poav(j)=poav(j)+po(i,j)
            end do
            poav(j)=poav(j)/Nsout
         end do
   
         do j=1,Nobs
            do i=1,Nsout
         
               fobj =  fobj + (po(i,j)-poav(j))**2

            end do
         end do

      end if


c     scaling of the objective function

      if (scalef .eq. 1.d0)  scalef=fobj         
      fobj=fobj/scalef

c     Constraint violation?
      ftmp= 0.0d0
      if (.not. sbbc .and. .not. periodic) 
     &     call CalcTCon (jdim, kdim, y, ftmp, .true.)
      fobj =  fobj + ftmp 
            
      return
      end  !calcobjairnoise






      subroutine calcdJdQnoise(dJdQ,q,count,nout,skipend,jdim,kdim,
     &           deltat,scalef,xyj,jsta,jsto,dpod,win,po,ns)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer j,k,l,n,nout,skipend,jdim,kdim,jsta,jsto
      integer sc,Nss,Nt,count,tnow,Nth

      double precision q(jdim,kdim,4),omeg(tdim),xyj(jdim,kdim)
      double precision ns(sdim,2),po_tar(tdim,sdim)

      double precision scalef,deltat,win(tdim),wsave(4*tdim+15)

      double precision,allocatable :: dJdQ(:,:,:),dpdq(:,:,:,:)
      double precision dF1dq(sdim,4),dF2dq(sdim,4)
      double precision po(tdim,sdim),dJdp(tdim,sdim)

      double precision fac,QS,ainf,scal,sum(sdim),dJdQtmp(sdim,4)
      double precision poav(sdim)

      complex*16 dpod(Nobs,tdim,sdim,3),tmp(tdim),dpFTdq(tdim,4),facc
      complex*16 facc2(tdim)

      logical odd

cmpr  Allocate some of the big arrays dynamically
      allocate(dpdq(Nobs,tdim,sdim,4),dJdQ(jdim,kdim,4))

c
c     calculate dQdq, dF1dq and dF2dq 
c     

cc    calculate some needed quantities

      Nt = nout-skipend
      tnow=count-skipend

      pi=4.d0*atan(1.d0)
      ainf = sqrt(gamma*pinf/rhoinf)
      uinf=fsmach*ainf*dcos(alpha*pi/180.d0)
      vinf=fsmach*ainf*dsin(alpha*pi/180.d0)

      sc=0
       
cc    Part I: C-shape or O-shape
      k=klineconst
        
      do j = jsta,jsto

         sc=sc+1 

         fac= gami*ns(sc,1)
         QS = q(j,k,2)*ns(sc,1)+q(j,k,3)*ns(sc,2)

         dF1dq(sc,1) = fac*0.5d0*( (q(j,k,2)/q(j,k,1))**2 + 
     &        (q(j,k,3)/q(j,k,1))**2  ) - q(j,k,2) * QS / q(j,k,1)**2 
         dF1dq(sc,2) = -fac*q(j,k,2)/q(j,k,1) + QS/q(j,k,1) + 
     &        (q(j,k,2)/q(j,k,1)-2.d0*uinf)*ns(sc,1)
         dF1dq(sc,3) = -fac*q(j,k,3)/q(j,k,1) + 
     &        (q(j,k,2)/q(j,k,1)-2.d0*uinf)*ns(sc,2)
         dF1dq(sc,4) = fac

         fac= gami*ns(sc,2)

         dF2dq(sc,1) = fac*0.5d0*( (q(j,k,2)/q(j,k,1))**2 + 
     &        (q(j,k,3)/q(j,k,1))**2  ) - q(j,k,3) * QS / q(j,k,1)**2 
         dF2dq(sc,2) = -fac*q(j,k,2)/q(j,k,1) + 
     &        (q(j,k,3)/q(j,k,1)-2.d0*vinf)*ns(sc,1)
         dF2dq(sc,3) = -fac*q(j,k,3)/q(j,k,1) + QS/q(j,k,1) + 
     &        (q(j,k,3)/q(j,k,1)-2.d0*vinf)*ns(sc,2)
         dF2dq(sc,4) = fac
         

      end do



      if (.not. periodic) then   !C-Mesh

cc       Part II: straight down close to x=remxmax above wake cut

         j=jsto
         
         do k = klineconst-1,1,-1
            
            sc=sc+1 
            
            fac= gami*ns(sc,1)
            QS = q(j,k,2)*ns(sc,1)+q(j,k,3)*ns(sc,2)
         
            dF1dq(sc,1) = fac*0.5d0*( (q(j,k,2)/q(j,k,1))**2 + 
     &           (q(j,k,3)/q(j,k,1))**2  ) - q(j,k,2) * QS / q(j,k,1)**2 
            dF1dq(sc,2) = -fac*q(j,k,2)/q(j,k,1) + QS/q(j,k,1) + 
     &           (q(j,k,2)/q(j,k,1)-2.d0*uinf)*ns(sc,1)
            dF1dq(sc,3) = -fac*q(j,k,3)/q(j,k,1) + 
     &           (q(j,k,2)/q(j,k,1)-2.d0*uinf)*ns(sc,2)
            dF1dq(sc,4) = fac

            fac= gami*ns(sc,2)
            
            dF2dq(sc,1) = fac*0.5d0*( (q(j,k,2)/q(j,k,1))**2 + 
     &           (q(j,k,3)/q(j,k,1))**2  ) - q(j,k,3) * QS / q(j,k,1)**2 
            dF2dq(sc,2) = -fac*q(j,k,2)/q(j,k,1) + 
     &           (q(j,k,3)/q(j,k,1)-2.d0*vinf)*ns(sc,1)
            dF2dq(sc,3) = -fac*q(j,k,3)/q(j,k,1) + QS/q(j,k,1) + 
     &           (q(j,k,3)/q(j,k,1)-2.d0*vinf)*ns(sc,2)
            dF2dq(sc,4) = fac                   
     
         end do


cc       Part III: straight down close to x=remxmax below wake cut

         j=jsta

         do k = 1,klineconst-1

            sc=sc+1 
            
            fac= gami*ns(sc,1)
            QS = q(j,k,2)*ns(sc,1)+q(j,k,3)*ns(sc,2)
            
            dF1dq(sc,1) = fac*0.5d0*( (q(j,k,2)/q(j,k,1))**2 + 
     &           (q(j,k,3)/q(j,k,1))**2  ) - q(j,k,2) * QS / q(j,k,1)**2 
            dF1dq(sc,2) = -fac*q(j,k,2)/q(j,k,1) + QS/q(j,k,1) + 
     &           (q(j,k,2)/q(j,k,1)-2.d0*uinf)*ns(sc,1)
            dF1dq(sc,3) = -fac*q(j,k,3)/q(j,k,1) + 
     &           (q(j,k,2)/q(j,k,1)-2.d0*uinf)*ns(sc,2)
            dF1dq(sc,4) = fac

            fac= gami*ns(sc,2)
            
            dF2dq(sc,1) = fac*0.5d0*( (q(j,k,2)/q(j,k,1))**2 + 
     &           (q(j,k,3)/q(j,k,1))**2  ) - q(j,k,3) * QS / q(j,k,1)**2 
            dF2dq(sc,2) = -fac*q(j,k,2)/q(j,k,1) + 
     &           (q(j,k,3)/q(j,k,1)-2.d0*vinf)*ns(sc,1)
            dF2dq(sc,3) = -fac*q(j,k,3)/q(j,k,1) + QS/q(j,k,1) + 
     &           (q(j,k,3)/q(j,k,1)-2.d0*vinf)*ns(sc,2)
            dF2dq(sc,4) = fac                    
         
         end do

      end if


      Nss=sc

c
c     Assemble dpdq = ifft(dpFTdq)
c   

cc    window function energy preserving factor

      odd=.true.
      if (mod(Nt,2).eq.0) odd=.false.

      if (odd) then
         Nth=(Nt+1)/2;         
      else 
         Nth=Nt/2+1;
      endif
 
cc    window function energy preserving factor

      scal=0.d0
      do l=1,Nt
         scal=scal+win(l)**2     
      end do
      scal=SQRT(scal/Nt)

cc    initialize omega and wsave for FFts
    
      do l=1,Nth
         omeg(l)=2.d0*pi*dble(l-1)/(Nt*deltat)
      end do 

      call zffti(Nt,wsave)

cc    sum to account for subtracting the mean

      do l=1,Nth
         facc2(l)=(0.d0,0.d0)
         do n=1,Nt
            facc2(l)=facc2(l)+CDEXP(-ci*omeg(l)*DBLE(n-1)*deltat)*win(n)
         end do
         facc2(l)=facc2(l)/Nt/scal
      end do  






cc    loop over all observer and spatial source points

      do k=1,Nobs

      do j=1,Nss

cc       account for fourier trafo and assemble dpFTdq
cc       (throw out upper half of frequencies and compensate by doubling the lower half)
cc       also apply window function to source terms Qs,Fs1 and Fs2 after subtracting their mean 


         do l=1,Nth

            do n=1,4
               dpFTdq(l,n)=dpod(k,l,j,2)*dF1dq(j,n) + 
     &                     dpod(k,l,j,3)*dF2dq(j,n) 
            end do             
            dpFTdq(l,2) = dpFTdq(l,2) + dpod(k,l,j,1)*ns(j,1)
            dpFTdq(l,3) = dpFTdq(l,3) + dpod(k,l,j,1)*ns(j,2)

            
            facc=win(tnow)/scal * CDEXP(-ci*omeg(l)*DBLE(tnow-1)*deltat) 
     &          - facc2(l)

            if (odd .or. l.le.Nth-1)   facc=2.d0*facc   !this accounts for doubling the lower half
            
            do n=1,4
               dpFTdq(l,n) = dpFTdq(l,n)*facc
            end do

         end do
 
cc       inverse fourier trafo 

         do n=1,4

            do l=1,Nt   
               tmp(l)=(0.d0,0.d0)
            end do
            
            do l=2,Nth   
               tmp(l)=dpFTdq(l,n)
            end do
            
            call zfftb(Nt,tmp,wsave)
            
            do l=1,Nt   
               dpdq(k,l,j,n)=DREAL(tmp(l))/Nt
            end do

         end do


      end do     ! loop over all spatial source points

      end do     ! loop over all observer points
          



c
c     Calculate dJdQ = dJdp * dpdq  with dJdp=2*(p-poav/N)
c
     

      if (obj_func.eq.10) then 

         do k=1,Nobs
            do l=1,Nt
               read ( cptar_unit, *) po_tar(l,k)
            end do
         end do 
         rewind(cptar_unit)
     
         do k=1,Nobs
            
            do l=1,Nt         
               dJdp(l,k)=2.d0 * ( po(l,k)- po_tar(l,k) ) / scalef
            end do

         end do

      else if (obj_func.eq.14) then

         do k=1,Nobs

            poav(k)=0.d0
            do l=1,Nt         
               poav(k)=poav(k)+po(l,k)
            end do
            poav(k)=poav(k)/Nt

            do l=1,Nt         
               dJdp(l,k)=2.d0 * ( po(l,k)-poav(k)/Nt ) / scalef
            end do

         end do

      end if


      do j=1,Nss
         do n=1,4
            dJdQtmp(j,n)=0.d0
            do k=1,Nobs
               do l=1,Nt
                  dJdQtmp(j,n) = dJdQtmp(j,n)+dJdp(l,k)*dpdq(k,l,j,n)
               end do
            end do
         end do
      end do


c
c     Cast back into (j,k) index form and multiply dJ/dQ by the metric Jacobian, to give dJ/d(Q hat)
c

      do n=1,4

         sc=0

cc       Part I: C-shape or O-shape
         k=klineconst        
         do j = jsta,jsto
            sc=sc+1             
            dJdQ(j,k,n)=dJdQtmp(sc,n) * xyj(j,k)                        
         end do


         if (.not. periodic) then !C-Mesh

cc          Part II: straight down close to x=remxmax above wake cut
            j=jsto         
            do k = klineconst-1,1,-1
               sc=sc+1 
               dJdQ(j,k,n)=dJdQtmp(sc,n) * xyj(j,k)                        
            end do

cc          Part III: straight down close to x=remxmax below wake cut
            j=jsta         
            do k = 1,klineconst-1
               sc=sc+1 
               dJdQ(j,k,n)=dJdQtmp(sc,n) * xyj(j,k)          
            end do

         end if

      end do  !n=1,4

cmpr  Deallocate memory
      deallocate(dpdq,dJdQ)

            
      return
      end  !calcdJdQnoise




      subroutine calcobjairnoisefd(fobj,qp,count,nout,skipend,jdim,kdim,
     &     deltat,scalef,x,y,jsta,jsto,xy,ns)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i,j,k,n,nout,skipend,jdim,kdim,jsta,jsto,count
      integer tc,sc,Nsous,Nsout

      double precision q(jdim,kdim,4),xyj(jdim,kdim),qp(jdim,kdim,4)
      double precision turmu(jdim,kdim),x(jdim,kdim),y(jdim,kdim)
      double precision xy(jdim,kdim,4)

      double precision fobj,scalef,deltat,win(tdim)

      double precision ps(tdim,sdim),us(tdim,sdim),vs(tdim,sdim)
      double precision rhos(tdim,sdim),po(tdim,sdim)

      double precision xs(sdim),ys(sdim),ns(sdim,2),xo(sdim),yo(sdim)

      double precision pcomp(tdim),po_tar(tdim,sdim),poav(sdim)
      complex*16,allocatable :: dpod(:,:,:,:)
      logical deriv

cmpr  Allocate some of the big arrays dynamically
      allocate(dpod(Nobs,tdim,sdim,3))

   
      fobj= 0.0d0

c
c     Calculate pressure, velocities, and density along the FWH control surface 
c     

      tc=0

      do i=skipend+1,nout


         if (i .ne. count) then

c        -- read physical flow field --         
            call iomarkus(3,i,jdim,kdim,q,xyj,turmu)

         else

            do n=1,4
               do k=kbegin,kend
                  do j=jbegin,jend          
                     q(j,k,n) = qp(j,k,n)
                  enddo
               enddo
            enddo

         end if

         sc=0
         tc=tc+1 

c     Part I: C-shape or O-shape

         k=klineconst
        
         do j = jsta,jsto

            sc=sc+1  

            rhos(tc,sc)=q(j,k,1)
            us(tc,sc)=q(j,k,2)/q(j,k,1)
            vs(tc,sc)=q(j,k,3)/q(j,k,1)
            ps(tc,sc)=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)
     &           /q(j,k,1))
            xs(sc)=x(j,k)
            ys(sc)=y(j,k)

         end do

         if (.not.periodic) then !C-Mesh

c        Part II: straight down close to x=remxmax above wake cut

            j=jsto

            do k = klineconst-1,1,-1
               
               sc=sc+1  
               
               rhos(tc,sc)=q(j,k,1)
               us(tc,sc)=q(j,k,2)/q(j,k,1)
               vs(tc,sc)=q(j,k,3)/q(j,k,1)
               ps(tc,sc)=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)
     &              /q(j,k,1))
               xs(sc)=x(j,k)
               ys(sc)=y(j,k)
     
            end do

c        Part III: straight down close to x=remxmax below wake cut

            j=jsta

            do k = 1,klineconst-1
               
               sc=sc+1  

               rhos(tc,sc)=q(j,k,1)
               us(tc,sc)=q(j,k,2)/q(j,k,1)
               vs(tc,sc)=q(j,k,3)/q(j,k,1)
               ps(tc,sc)=gami*(q(j,k,4)-0.5*(q(j,k,2)**2+q(j,k,3)**2)
     &              /q(j,k,1))
               xs(sc)=x(j,k)
               ys(sc)=y(j,k)
                     
            end do

         end if

      end do     !Calculate quantities along the FWH control surface

      Nsous=sc
      Nsout=tc
c
c     Set up observer points, we are interested in (only one supported for now)
c


      if ( abs(xobs).gt.0.d0 .or. abs(yobs).gt.0.d0 ) 
     &     then

         xo(1)=xobs
         yo(1)=yobs

      else

         do j=1,Nobs
            xo(j)=x(jobs+j-1,kobs)
            yo(j)=y(jobs+j-1,kobs)
         end do

      end if


c     Calculate the pressure po in these observer points via the FWH approach
c

      deriv=.false.

      call calcFWH(Nsous,Nsout,xo,yo,po,xs,ys,ns,rhos,us,vs,ps,
     &     deltat,dpod,win,deriv,pcomp)

      
c
c     Calculate the objective function
c


      if (obj_func.eq.10) then 

         do j=1,Nobs
            do i=1,Nsout
               read ( cptar_unit, *) po_tar(i,j)
            end do
         end do
         rewind(cptar_unit)
      
         do j=1,Nobs
            do i=1,Nsout
               
               fobj =  fobj + (po(i,j)-po_tar(i,j))**2
               
            end do
         end do

      else if (obj_func.eq.14) then

         do j=1,Nobs
            poav(j)=0.d0
            do i=1,Nsout         
               poav(j)=poav(j)+po(i,j)
            end do
            poav(j)=poav(j)/Nsout
         end do
   
         do j=1,Nobs
            do i=1,Nsout
         
               fobj =  fobj + (po(i,j)-poav(j))**2

            end do
         end do

      end if
     

c     scaling of the objective function
      
      fobj=fobj/scalef

cmpr  deallocate memory
      deallocate(dpod)

            
      return
      end  !calcobjairnoisefd





      subroutine calcobjair11(fobj,nout,skipend,jdim,kdim,
     &     deltat,scalef,x,y,xy,xyj,ns,J1,J2,printout)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i,j,k,l,nout,skipend,jdim,kdim,tc,sc,jp1,jm1,jp2,jm2,INOUT

      double precision q(jdim,kdim,4),xyj(jdim,kdim),xy(jdim,kdim,4)
      double precision turmu(jdim,kdim),x(jdim,kdim),y(jdim,kdim)
      double precision press(jdim,kdim),sndsp(jdim,kdim),fmu(jdim,kdim)

      double precision fobj,ftmp,scalef,deltat,precon(jdim,kdim,6)

      double precision den,Jtmp(tdim),J1(tdim),J2(tdim)

      double precision uxi,ueta,vxi,veta,xix,xiy,etax,etay,ux,uy,vx,vy

      double precision xs(sdim),ys(sdim),ns(sdim,2),ds(sdim)
      double precision Int1(tdim),Int2(tdim)
      double precision p11(tdim,sdim),p12(tdim,sdim),p22(tdim,sdim)

      logical printout

c
c     Calculate normal projections along the surface and save xs and ys
c     the defining values for the surface.

c     Procedure: Along a constant eta line the normal is given by nabla eta = (etax,etay) and
c     along a constant xi line the normal is given by nabla xi = (xix,xiy).

 
c     Directivity is tricky: The normal has to point away from the surface. I walk a little bit along
c     the normal in question and check whether this point is inside the polygon. If it is I have to change
c     the signs of the normal components.


      sc=1

      k=1
        
      do j = jtail1+1,jtail2-1

         sc=sc+1

         xs(sc)=x(j,k)
         ys(sc)=y(j,k)

         den=SQRT(xy(j,k,3)**2+xy(j,k,4)**2)
         ns(sc,1)=xy(j,k,3)/den
         ns(sc,2)=xy(j,k,4)/den
        
      end do

      sc=sc+1 

      xs(1)=x(jtail1,1)
      ys(1)=y(jtail1,1)
      xs(sc)=x(jtail2,1)
      ys(sc)=y(jtail2,1)



C     Take care of directivity using subroutine PNPOLY except for the two endpoints which are just copied
C     INOUT - THE SIGNAL RETURNED: 
C     -1 IF THE POINT IS OUTSIDE OF THE POLYGON, 
C     0 IF THE POINT IS ON AN EDGE OR AT A VERTEX, 
C     1 IF THE POINT IS INSIDE OF THE POLYGON.

      do i=2,sc-1

         call PNPOLY(xs(i)+1.d-5*ns(i,1),ys(i)+1.d-5*ns(i,2),
     &        xs,ys,sc,INOUT)

         if (INOUT .eq. 1) then
            ns(i,1)=-ns(i,1)
            ns(i,2)=-ns(i,2) 
         end if

      end do

      ns(1,1)=ns(2,1)
      ns(1,2)=ns(2,2)
      ns(sc,1)=ns(sc-1,1)
      ns(sc,2)=ns(sc-1,2)

c$$$      do j=1,sc
c$$$         print *, xs(j), ys(j) 
c$$$      enddo
c$$$      print *, 'Normals'
c$$$      do j=1,sc
c$$$         print *, ns(j,1), ns(j,2) 
c$$$      enddo
c$$$      stop

c     calc the distance of the source points for integration 

      do k=1,sc-1                              
         ds(k)=SQRT( (xs(k+1)-xs(k))**2 + (ys(k+1)-ys(k))**2)
      end do

c
c     Calculate compressive stress tensor along the FWH surface
c     p11=p-fmu*(4*du/dx-2*dv/dy)/3
c     p22=p-fmu*(4*dv/dy-2*du/dx)/3
c     p12=p21=-fmu*(du/dy+dv/dx)
c     Note: du/d. = ueta*deta/d. + uxi*dxi/d.
c           dv/d. = veta*deta/d. + vxi*dxi/d.

      tc=0

      do i=skipend+1,nout

         tc=tc+1 

c        -- read physical flow field --         
         call iomarkus(3,i,jdim,kdim,q,xyj,turmu)

c     -- calculate pressure and sound speed --

         call calcps (jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- compute laminar viscosity based on sutherland's law --
         if (viscous) call fmun(jdim, kdim, q, press, fmu, sndsp)

         k=1
         
         sc=1        
         j = jtail1                                                  
         jp1 = j+1  
         jp2 = j+2
c       -second-order biased
         uxi = -1.5*q(j,k,2)/q(j,k,1)+2.*q(jp1,k,2)/q(jp1,k,1)       
     *        -.5*q(jp2,k,2)/q(jp2,k,1)
         vxi = -1.5*q(j,k,3)/q(j,k,1)+2.*q(jp1,k,3)/q(jp1,k,1)       
     *        -.5*q(jp2,k,3)/q(jp2,k,1)
         ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *        -.5*q(j,k+2,2)/q(j,k+2,1)                              
         veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *        -.5*q(j,k+2,3)/q(j,k+2,1)
c
         xix = xy(j,k,1)                                             
         xiy = xy(j,k,2)                                             
         etax = xy(j,k,3)                                            
         etay = xy(j,k,4) 
         ux=uxi*xix+ueta*etax
         uy=uxi*xiy+ueta*etay
         vx=vxi*xix+veta*etax
         vy=vxi*xiy+veta*etay

         p11(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*ux-2.d0*vy)/3.d0
         p22(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*vy-2.d0*ux)/3.d0
         p12(tc,sc)=-fmu(j,k)/re*(uy+vx) 

c$$$         p11(tc,sc)=press(j,k)
c$$$         p22(tc,sc)=press(j,k)
c$$$         p12(tc,sc)=0.d0

                                                                                                                                                                 
         do j = jtail1+1,jtail2-1

            sc=sc+1                                             
            jp1 = j+1                                              
            jm1 = j-1  
c           -second-order central
            uxi = .5*(q(jp1,k,2)/q(jp1,k,1)-q(jm1,k,2)/q(jm1,k,1))      
            vxi = .5*(q(jp1,k,3)/q(jp1,k,1)-q(jm1,k,3)/q(jm1,k,1))      
c           -second-order biased
            ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *           -.5*q(j,k+2,2)/q(j,k+2,1)                              
            veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *           -.5*q(j,k+2,3)/q(j,k+2,1)                              
            xix = xy(j,k,1)                                             
            xiy = xy(j,k,2)                                             
            etax = xy(j,k,3)                                            
            etay = xy(j,k,4)                                            
            ux=uxi*xix+ueta*etax
            uy=uxi*xiy+ueta*etay
            vx=vxi*xix+veta*etax
            vy=vxi*xiy+veta*etay

            p11(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*ux-2.d0*vy)/3.d0
            p22(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*vy-2.d0*ux)/3.d0
            p12(tc,sc)=-fmu(j,k)/re*(uy+vx)

c$$$            p11(tc,sc)=press(j,k)
c$$$            p22(tc,sc)=press(j,k)
c$$$            p12(tc,sc)=0.d0

         end do

         sc=sc+1 
         j = jtail2                                                  
         jm1 = j-1 
         jm2 = j-2
c       -second-order biased
         uxi = 1.5*q(j,k,2)/q(j,k,1)-2.*q(jm1,k,2)/q(jm1,k,1)       
     *        +.5*q(jm2,k,2)/q(jm2,k,1)
         vxi = 1.5*q(j,k,3)/q(j,k,1)-2.*q(jm1,k,3)/q(jm1,k,1)       
     *        +.5*q(jm2,k,3)/q(jm2,k,1)                                
         ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *        -.5*q(j,k+2,2)/q(j,k+2,1)                              
         veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *        -.5*q(j,k+2,3)/q(j,k+2,1)                              
c     
         xix = xy(j,k,1)                                             
         xiy = xy(j,k,2)                                             
         etax = xy(j,k,3)                                            
         etay = xy(j,k,4) 
         ux=uxi*xix+ueta*etax
         uy=uxi*xiy+ueta*etay
         vx=vxi*xix+veta*etax
         vy=vxi*xiy+veta*etay

         p11(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*ux-2.d0*vy)/3.d0
         p22(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*vy-2.d0*ux)/3.d0
         p12(tc,sc)=-fmu(j,k)/re*(uy+vx)

c$$$         p11(tc,sc)=press(j,k)
c$$$         p22(tc,sc)=press(j,k)
c$$$         p12(tc,sc)=0.d0
         

      end do !Calculate stress tensor along the surface


c     Integrate along the curve with trapezoidal rule
 
      do l=1,tc
                             
         Int1(l)=0.d0
         Int2(l)=0.d0
    
         do k=1,sc-1   
            Int1(l)=Int1(l) + ( ns(k,1)*p11(l,k) + ns(k+1,1)*p11(l,k+1)  
     &           + ns(k,2)*p12(l,k) + ns(k+1,2)*p12(l,k+1) )* 0.50*ds(k)
            Int2(l)=Int2(l) + ( ns(k,2)*p22(l,k) + ns(k+1,2)*p22(l,k+1)  
     &           + ns(k,1)*p12(l,k) + ns(k+1,1)*p12(l,k+1) )* 0.50*ds(k)
         end do

      end do

c     Take time derivative (second or fourth order accurate)

      if (jesdirk.eq.1) then

         J1(1)=(-Int1(3)+4.d0*Int1(2)-3.d0*Int1(1))/(2.d0*deltat)
         J2(1)=(-Int2(3)+4.d0*Int2(2)-3.d0*Int2(1))/(2.d0*deltat)
         Jtmp(1)=J1(1)**2+J2(1)**2
         do l=2,tc-1
            J1(l)=(Int1(l+1)-Int1(l-1))/(2.d0*deltat)
            J2(l)=(Int2(l+1)-Int2(l-1))/(2.d0*deltat)
            Jtmp(l)=J1(l)**2+J2(l)**2
         end do
         J1(tc)=(Int1(tc-2)-4.d0*Int1(tc-1)+3.d0*Int1(tc))/(2.d0*deltat)
         J2(tc)=(Int2(tc-2)-4.d0*Int2(tc-1)+3.d0*Int2(tc))/(2.d0*deltat)
         Jtmp(tc)=J1(tc)**2+J2(tc)**2
         
      else if (jesdirk.eq.4) then

         J1(1)=(4.d0*Int1(4)-18.d0*Int1(3)+36.d0*Int1(2)-22.d0*Int1(1))/
     &         (12.d0*deltat)
         J2(1)=(4.d0*Int2(4)-18.d0*Int2(3)+36.d0*Int2(2)-22.d0*Int2(1))/
     &         (12.d0*deltat)
         J1(2)=(-2.d0*Int1(4)+12.d0*Int1(3)-6.d0*Int1(2)-4.d0*Int1(1))/
     &         (12.d0*deltat)
         J2(2)=(-2.d0*Int2(4)+12.d0*Int2(3)-6.d0*Int2(2)-4.d0*Int2(1))/
     &         (12.d0*deltat)
         do l=3,tc-2
            J1(l)=(Int1(l-2)-8.d0*Int1(l-1)+8.d0*Int1(l+1)-Int1(l+2))/
     &            (12.d0*deltat)
            J2(l)=(Int2(l-2)-8.d0*Int2(l-1)+8.d0*Int2(l+1)-Int2(l+2))/
     &            (12.d0*deltat)
         end do
         J1(tc-1)=(2.d0*Int1(tc-3)-12.d0*Int1(tc-2)+6.d0*Int1(tc-1)+
     &             4.d0*Int1(tc))/(12.d0*deltat)
         J2(tc-1)=(2.d0*Int2(tc-3)-12.d0*Int2(tc-2)+6.d0*Int2(tc-1)+
     &             4.d0*Int2(tc))/(12.d0*deltat)
         J1(tc)=(-4.d0*Int1(tc-3)+18.d0*Int1(tc-2)-36.d0*Int1(tc-1)+
     &            22.d0*Int1(tc))/(12.d0*deltat)
         J2(tc)=(-4.d0*Int2(tc-3)+18.d0*Int2(tc-2)-36.d0*Int2(tc-1)+
     &            22.d0*Int2(tc))/(12.d0*deltat)
        
      end if

c     Square and combine to Jtmp(t)
      
      do l=1,tc
         Jtmp(l)=J1(l)**2+J2(l)**2
      end do



c$$$      do l=1,tc
c$$$         print *,Jtmp(l) 
c$$$      end do
c$$$      stop


c     Time average

      fobj= 0.0d0
      do l=1,tc
         fobj=fobj+Jtmp(l) 
      end do
      fobj=fobj/tc


c     scaling of the objective function

      if (scalef .eq. 1.d0)  scalef=fobj         
      fobj=fobj/scalef

c     Constraint violation?
      ftmp= 0.0d0
      if (.not. sbbc .and. .not. periodic) 
     &     call CalcTCon (jdim, kdim, y, ftmp,printout)
      fobj =  fobj + ftmp 
         
 
      return
      end  !calcobjair11



      subroutine calcdJdQ11(jdim,kdim,ndim,q,xy,xyj,x,y,ns,dJdQ,deltat,
     &     scalef,nout,skipend,count,J1,J2)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     Argument data types; 
      integer jdim, kdim, ndim,nout,skipend,count,ctmp
      double precision q(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)
      double precision x(jdim,kdim),y(jdim,kdim),ns(sdim,2)
      double precision dJdQ(jdim,kdim,ndim),deltat,scalef
      double precision J1(tdim),J2(tdim)

c     Local Variables
      integer j,k,l,n,sc,tc,jp1,jp2,jm1,jm2,kp1,kp2
      double precision press(jdim,kdim),sndsp(jdim,kdim),fmu(jdim,kdim)
      double precision ds(sdim),precon(jdim,kdim,6),fac
      double precision uxi,ueta,vxi,veta,xix,xiy,etax,etay,ux,uy,vx,vy
      double precision dmul1,dmul2,dmul3,dmul4,c2b,c2bp,t1,t2,t3,t4,t5
      double precision dadq1,dadq2,dadq3,dadq4,dmuda,turmu(jdim,kdim),g2
      double precision dpr1,dpr2,dpr3,dpr4,dux(jdim,3,3),duy(jdim,3,3)
      double precision dvx(jdim,3,3),dvy(jdim,3,3)
      double precision vel11(jdim),vel12(jdim),vel22(jdim)
      double precision dp11(jdim,3,4),dp12(jdim,3,4),dp22(jdim,3,4)
      double precision dI1(jdim,3,4),dI2(jdim,3,4)


c     calculate pressure and sound speed --
      call calcps (jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     compute laminar viscosity based on sutherland's law --
      call fmun(jdim, kdim, q, press, fmu, sndsp)
     
c     calculate the velocity derivatives
      k=1
      j = jtail1                                                  
      jp1 = j+1  
      jp2 = j+2
c     -second-order biased
      uxi = -1.5*q(j,k,2)/q(j,k,1)+2.*q(jp1,k,2)/q(jp1,k,1)       
     *     -.5*q(jp2,k,2)/q(jp2,k,1)
      vxi = -1.5*q(j,k,3)/q(j,k,1)+2.*q(jp1,k,3)/q(jp1,k,1)       
     *     -.5*q(jp2,k,3)/q(jp2,k,1)
      ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *     -.5*q(j,k+2,2)/q(j,k+2,1)                              
      veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *     -.5*q(j,k+2,3)/q(j,k+2,1)
c
      xix = xy(j,k,1)                                             
      xiy = xy(j,k,2)                                             
      etax = xy(j,k,3)                                            
      etay = xy(j,k,4) 
      ux=uxi*xix+ueta*etax
      uy=uxi*xiy+ueta*etay
      vx=vxi*xix+veta*etax
      vy=vxi*xiy+veta*etay

      vel11(j)=(4.d0*ux-2.d0*vy)/3.d0
      vel22(j)=(4.d0*vy-2.d0*ux)/3.d0
      vel12(j)=(uy+vx) 
                                                                                                                                                                 
      do j = jtail1+1,jtail2-1
                                                    
         jp1 = j+1                                              
         jm1 = j-1  
c     -second-order central
         uxi = .5*(q(jp1,k,2)/q(jp1,k,1)-q(jm1,k,2)/q(jm1,k,1))      
         vxi = .5*(q(jp1,k,3)/q(jp1,k,1)-q(jm1,k,3)/q(jm1,k,1))      
c     -second-order biased
         ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *        -.5*q(j,k+2,2)/q(j,k+2,1)                              
         veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *        -.5*q(j,k+2,3)/q(j,k+2,1)                              
         xix = xy(j,k,1)                                             
         xiy = xy(j,k,2)                                             
         etax = xy(j,k,3)                                            
         etay = xy(j,k,4)                                            
         ux=uxi*xix+ueta*etax
         uy=uxi*xiy+ueta*etay
         vx=vxi*xix+veta*etax
         vy=vxi*xiy+veta*etay

         vel11(j)=(4.d0*ux-2.d0*vy)/3.d0
         vel22(j)=(4.d0*vy-2.d0*ux)/3.d0
         vel12(j)=(uy+vx)          

      end do

      j = jtail2                                                  
      jm1 = j-1 
      jm2 = j-2
c     -second-order biased
      uxi = 1.5*q(j,k,2)/q(j,k,1)-2.*q(jm1,k,2)/q(jm1,k,1)       
     *     +.5*q(jm2,k,2)/q(jm2,k,1)
      vxi = 1.5*q(j,k,3)/q(j,k,1)-2.*q(jm1,k,3)/q(jm1,k,1)       
     *     +.5*q(jm2,k,3)/q(jm2,k,1)                                
      ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *     -.5*q(j,k+2,2)/q(j,k+2,1)                              
      veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *     -.5*q(j,k+2,3)/q(j,k+2,1)                              
c     
      xix = xy(j,k,1)                                             
      xiy = xy(j,k,2)                                             
      etax = xy(j,k,3)                                            
      etay = xy(j,k,4) 
      ux=uxi*xix+ueta*etax
      uy=uxi*xiy+ueta*etay
      vx=vxi*xix+veta*etax
      vy=vxi*xiy+veta*etay
         
      vel11(j)=(4.d0*ux-2.d0*vy)/3.d0
      vel22(j)=(4.d0*vy-2.d0*ux)/3.d0
      vel12(j)=(uy+vx)


c     Calculate the derivative of the velocity derivatives w.r.t. Q

      do n=1,3
         do k = kbegin,kbegin+2
            do j = jtail1, jtail2
               dux(j,k,n)=0.d0
               duy(j,k,n)=0.d0
               dvx(j,k,n)=0.d0
               dvy(j,k,n)=0.d0
            end do
         end do
      end do

      k=1

      j = jtail1                                                  
      jp1 = j+1  
      jp2 = j+2

      xix = xy(j,k,1)                                             
      xiy = xy(j,k,2)                                             

      dux(j,k,1)=dux(j,k,1)+1.5d0*q(j,k,2)/q(j,k,1)**2*xix
      dux(jp1,k,1)=dux(jp1,k,1)-2.d0*q(jp1,k,2)/q(jp1,k,1)**2*xix
      dux(jp2,k,1)=dux(jp2,k,1)+0.5d0*q(jp2,k,2)/q(jp2,k,1)**2*xix
 
      duy(j,k,1)=duy(j,k,1)+1.5d0*q(j,k,2)/q(j,k,1)**2*xiy
      duy(jp1,k,1)=duy(jp1,k,1)-2.d0*q(jp1,k,2)/q(jp1,k,1)**2*xiy
      duy(jp2,k,1)=duy(jp2,k,1)+0.5d0*q(jp2,k,2)/q(jp2,k,1)**2*xiy

      dvx(j,k,1)=dvx(j,k,1)+1.5d0*q(j,k,3)/q(j,k,1)**2*xix
      dvx(jp1,k,1)=dvx(jp1,k,1)-2.d0*q(jp1,k,3)/q(jp1,k,1)**2*xix
      dvx(jp2,k,1)=dvx(jp2,k,1)+0.5d0*q(jp2,k,3)/q(jp2,k,1)**2*xix
     
      dvy(j,k,1)=dvy(j,k,1)+1.5d0*q(j,k,3)/q(j,k,1)**2*xiy
      dvy(jp1,k,1)=dvy(jp1,k,1)-2.d0*q(jp1,k,3)/q(jp1,k,1)**2*xiy
      dvy(jp2,k,1)=dvy(jp2,k,1)+0.5d0*q(jp2,k,3)/q(jp2,k,1)**2*xiy


      dux(j,k,2)=dux(j,k,2)-1.5d0/q(j,k,1)*xix
      dux(jp1,k,2)=dux(jp1,k,2)+2.d0/q(jp1,k,1)*xix
      dux(jp2,k,2)=dux(jp2,k,2)-0.5d0/q(jp2,k,1)*xix

      duy(j,k,2)=duy(j,k,2)-1.5d0/q(j,k,1)*xiy
      duy(jp1,k,2)=duy(jp1,k,2)+2.d0/q(jp1,k,1)*xiy
      duy(jp2,k,2)=duy(jp2,k,2)-0.5d0/q(jp2,k,1)*xiy

  
      dvx(j,k,3)=dvx(j,k,3)-1.5d0/q(j,k,1)*xix
      dvx(jp1,k,3)=dvx(jp1,k,3)+2.d0/q(jp1,k,1)*xix
      dvx(jp2,k,3)=dvx(jp2,k,3)-0.5d0/q(jp2,k,1)*xix
 
      dvy(j,k,3)=dvy(j,k,3)-1.5d0/q(j,k,1)*xiy
      dvy(jp1,k,3)=dvy(jp1,k,3)+2.d0/q(jp1,k,1)*xiy
      dvy(jp2,k,3)=dvy(jp2,k,3)-0.5d0/q(jp2,k,1)*xiy

      do j = jtail1+1, jtail2-1

         jp1 = j+1                                              
         jm1 = j-1 

         xix = xy(j,k,1)                                             
         xiy = xy(j,k,2)                                             

         dux(jp1,k,1)=dux(jp1,k,1)-0.5d0*q(jp1,k,2)/q(jp1,k,1)**2*xix
         dux(jm1,k,1)=dux(jm1,k,1)+0.5d0*q(jm1,k,2)/q(jm1,k,1)**2*xix

         duy(jp1,k,1)=duy(jp1,k,1)-0.5d0*q(jp1,k,2)/q(jp1,k,1)**2*xiy
         duy(jm1,k,1)=duy(jm1,k,1)+0.5d0*q(jm1,k,2)/q(jm1,k,1)**2*xiy

         dvx(jp1,k,1)=dvx(jp1,k,1)-0.5d0*q(jp1,k,3)/q(jp1,k,1)**2*xix
         dvx(jm1,k,1)=dvx(jm1,k,1)+0.5d0*q(jm1,k,3)/q(jm1,k,1)**2*xix

         dvy(jp1,k,1)=dvy(jp1,k,1)-0.5d0*q(jp1,k,3)/q(jp1,k,1)**2*xiy
         dvy(jm1,k,1)=dvy(jm1,k,1)+0.5d0*q(jm1,k,3)/q(jm1,k,1)**2*xiy


         dux(jp1,k,2)=dux(jp1,k,2)+0.5d0/q(jp1,k,1)*xix
         dux(jm1,k,2)=dux(jm1,k,2)-0.5d0/q(jm1,k,1)*xix

         duy(jp1,k,2)=duy(jp1,k,2)+0.5d0/q(jp1,k,1)*xiy
         duy(jm1,k,2)=duy(jm1,k,2)-0.5d0/q(jm1,k,1)*xiy

         dvx(jp1,k,3)=dvx(jp1,k,3)+0.5d0/q(jp1,k,1)*xix
         dvx(jm1,k,3)=dvx(jm1,k,3)-0.5d0/q(jm1,k,1)*xix

         dvy(jp1,k,3)=dvy(jp1,k,3)+0.5d0/q(jp1,k,1)*xiy
         dvy(jm1,k,3)=dvy(jm1,k,3)-0.5d0/q(jm1,k,1)*xiy

      end do

      j = jtail2                                                  
      jm1 = j-1 
      jm2 = j-2

      xix = xy(j,k,1)                                             
      xiy = xy(j,k,2)                                             

      dux(j,k,1)=dux(j,k,1)-1.5d0*q(j,k,2)/q(j,k,1)**2*xix
      dux(jm1,k,1)=dux(jm1,k,1)+2.d0*q(jm1,k,2)/q(jm1,k,1)**2*xix
      dux(jm2,k,1)=dux(jm2,k,1)-0.5d0*q(jm2,k,2)/q(jm2,k,1)**2*xix

      duy(j,k,1)=duy(j,k,1)-1.5d0*q(j,k,2)/q(j,k,1)**2*xiy
      duy(jm1,k,1)=duy(jm1,k,1)+2.d0*q(jm1,k,2)/q(jm1,k,1)**2*xiy
      duy(jm2,k,1)=duy(jm2,k,1)-0.5d0*q(jm2,k,2)/q(jm2,k,1)**2*xiy
      
      dvx(j,k,1)=dvx(j,k,1)-1.5d0*q(j,k,3)/q(j,k,1)**2*xix
      dvx(jm1,k,1)=dvx(jm1,k,1)+2.d0*q(jm1,k,3)/q(jm1,k,1)**2*xix
      dvx(jm2,k,1)=dvx(jm2,k,1)-0.5d0*q(jm2,k,3)/q(jm2,k,1)**2*xix     
      
      dvy(j,k,1)=dvy(j,k,1)-1.5d0*q(j,k,3)/q(j,k,1)**2*xiy
      dvy(jm1,k,1)=dvy(jm1,k,1)+2.d0*q(jm1,k,3)/q(jm1,k,1)**2*xiy
      dvy(jm2,k,1)=dvy(jm2,k,1)-0.5d0*q(jm2,k,3)/q(jm2,k,1)**2*xiy


      dux(j,k,2)=dux(j,k,2)+1.5d0/q(j,k,1)*xix
      dux(jm1,k,2)=dux(jm1,k,2)-2.d0/q(jm1,k,1)*xix
      dux(jm2,k,2)=dux(jm2,k,2)+0.5d0/q(jm2,k,1)*xix
    
      duy(j,k,2)=duy(j,k,2)+1.5d0/q(j,k,1)*xiy
      duy(jm1,k,2)=duy(jm1,k,2)-2.d0/q(jm1,k,1)*xiy
      duy(jm2,k,2)=duy(jm2,k,2)+0.5d0/q(jm2,k,1)*xiy

  
      dvx(j,k,3)=dvx(j,k,3)+1.5d0/q(j,k,1)*xix
      dvx(jm1,k,3)=dvx(jm1,k,3)-2.d0/q(jm1,k,1)*xix
      dvx(jm2,k,3)=dvx(jm2,k,3)+0.5d0/q(jm2,k,1)*xix

      dvy(j,k,3)=dvy(j,k,3)+1.5d0/q(j,k,1)*xiy
      dvy(jm1,k,3)=dvy(jm1,k,3)-2.d0/q(jm1,k,1)*xiy
      dvy(jm2,k,3)=dvy(jm2,k,3)+0.5d0/q(jm2,k,1)*xiy

      do j = jtail1, jtail2

         kp1 = k+1 
         kp2 = k+2

         etax = xy(j,k,3)                                            
         etay = xy(j,k,4)

         dux(j,k,1)=dux(j,k,1)+1.5d0*q(j,k,2)/q(j,k,1)**2*etax
         dux(j,kp1,1)=dux(j,kp1,1)-2.d0*q(j,kp1,2)/q(j,kp1,1)**2*etax
         dux(j,kp2,1)=dux(j,kp2,1)+0.5d0*q(j,kp2,2)/q(j,kp2,1)**2*etax         
         
         duy(j,k,1)=duy(j,k,1)+1.5d0*q(j,k,2)/q(j,k,1)**2*etay
         duy(j,kp1,1)=duy(j,kp1,1)-2.d0*q(j,kp1,2)/q(j,kp1,1)**2*etay
         duy(j,kp2,1)=duy(j,kp2,1)+0.5d0*q(j,kp2,2)/q(j,kp2,1)**2*etay
                
         dvx(j,k,1)=dvx(j,k,1)+1.5d0*q(j,k,3)/q(j,k,1)**2*etax
         dvx(j,kp1,1)=dvx(j,kp1,1)-2.d0*q(j,kp1,3)/q(j,kp1,1)**2*etax
         dvx(j,kp2,1)=dvx(j,kp2,1)+0.5d0*q(j,kp2,3)/q(j,kp2,1)**2*etax      

         dvy(j,k,1)=dvy(j,k,1)+1.5d0*q(j,k,3)/q(j,k,1)**2*etay
         dvy(j,kp1,1)=dvy(j,kp1,1)-2.d0*q(j,kp1,3)/q(j,kp1,1)**2*etay
         dvy(j,kp2,1)=dvy(j,kp2,1)+0.5d0*q(j,kp2,3)/q(j,kp2,1)**2*etay


         dux(j,k,2)=dux(j,k,2)-1.5d0/q(j,k,1)*etax
         dux(j,kp1,2)=dux(j,kp1,2)+2.d0/q(j,kp1,1)*etax
         dux(j,kp2,2)=dux(j,kp2,2)-0.5d0/q(j,kp2,1)*etax

         duy(j,k,2)=duy(j,k,2)-1.5d0/q(j,k,1)*etay
         duy(j,kp1,2)=duy(j,kp1,2)+2.d0/q(j,kp1,1)*etay
         duy(j,kp2,2)=duy(j,kp2,2)-0.5d0/q(j,kp2,1)*etay
  
 
         dvx(j,k,3)=dvx(j,k,3)-1.5d0/q(j,k,1)*etax
         dvx(j,kp1,3)=dvx(j,kp1,3)+2.d0/q(j,kp1,1)*etax
         dvx(j,kp2,3)=dvx(j,kp2,3)-0.5d0/q(j,kp2,1)*etax

         dvy(j,k,3)=dvy(j,k,3)-1.5d0/q(j,k,1)*etay
         dvy(j,kp1,3)=dvy(j,kp1,3)+2.d0/q(j,kp1,1)*etay
         dvy(j,kp2,3)=dvy(j,kp2,3)-0.5d0/q(j,kp2,1)*etay

      end do



c     Calculate the derivative of the stress tensor w.r.t. Q 

      do n=1,4
         do k = kbegin,kbegin+2
            do j = jtail1, jtail2
               dp11(j,k,n)=0.d0
               dp12(j,k,n)=0.d0
               dp22(j,k,n)=0.d0
            end do
         end do
      end do

      g2  = gamma*gami
      c2b = 198.6d0/tinf                                                  
      c2bp = c2b + 1.d0

      k=1
      do j = jtail1, jtail2

c        -- compute laminar d(fmu)/dQ --
         t1 = sndsp(j,k)*sndsp(j,k)
         t2 = t1 + 3.d0*c2b
         t3 = 1.d0/(t1 + c2b)
         dmuda = c2bp*t1*t2*t3**2
         
         t1 = 0.5d0/sndsp(j,k)
         t2 = q(j,k,2)*q(j,k,2)
         t3 = q(j,k,3)*q(j,k,3)
         t4 = 1.d0/q(j,k,1)
         t5 = t4*t4
         dadq1 = g2*t5*( (t2+t3)*t4 - q(j,k,4) )
         dadq2 = - g2*q(j,k,2)*t5
         dadq3 = - g2*q(j,k,3)*t5
         dadq4 = g2*t4
         
         dmul1 = dmuda*dadq1*t1
         dmul2 = dmuda*dadq2*t1
         dmul3 = dmuda*dadq3*t1
         dmul4 = dmuda*dadq4*t1

c        -- compute d(press)/dQ
         dpr1=gami*0.5d0*( (q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)**2 )
         dpr2=gami*( -q(j,k,2)/q(j,k,1) )
         dpr3=gami*( -q(j,k,3)/q(j,k,1) )
         dpr4=gami

c        -- combine derivatives
         dp11(j,k,1)=dpr1 - dmul1/re*vel11(j) 
     &        - fmu(j,k)/re*( 4.d0*dux(j,k,1)-2.d0*dvy(j,k,1) )/3.d0
         dp11(j,k,2)=dpr2 - dmul2/re*vel11(j) 
     &        - fmu(j,k)/re*( 4.d0*dux(j,k,2)-2.d0*dvy(j,k,2) )/3.d0
         dp11(j,k,3)=dpr3 - dmul3/re*vel11(j) 
     &        - fmu(j,k)/re*( 4.d0*dux(j,k,3)-2.d0*dvy(j,k,3) )/3.d0
         dp11(j,k,4)=dpr4 - dmul4/re*vel11(j) 
 
         dp22(j,k,1)=dpr1 - dmul1/re*vel22(j) 
     &        - fmu(j,k)/re*( 4.d0*dvy(j,k,1)-2.d0*dux(j,k,1) )/3.d0
         dp22(j,k,2)=dpr2 - dmul2/re*vel22(j) 
     &        - fmu(j,k)/re*( 4.d0*dvy(j,k,2)-2.d0*dux(j,k,2) )/3.d0
         dp22(j,k,3)=dpr3 - dmul3/re*vel22(j) 
     &        - fmu(j,k)/re*( 4.d0*dvy(j,k,3)-2.d0*dux(j,k,3) )/3.d0
         dp22(j,k,4)=dpr4 - dmul4/re*vel22(j) 

c$$$         dp11(j,k,1)=dpr1 
c$$$         dp11(j,k,2)=dpr2 
c$$$         dp11(j,k,3)=dpr3 
c$$$         dp11(j,k,4)=dpr4  
c$$$ 
c$$$         dp22(j,k,1)=dpr1 
c$$$         dp22(j,k,2)=dpr2
c$$$         dp22(j,k,3)=dpr3 
c$$$         dp22(j,k,4)=dpr4 

         dp12(j,k,1)=-fmu(j,k)/re*(duy(j,k,1)+dvx(j,k,1))
     &        -dmul1/re*vel12(j)
         dp12(j,k,2)=-fmu(j,k)/re*(duy(j,k,2)+dvx(j,k,2))
     &        -dmul2/re*vel12(j)
         dp12(j,k,3)=-fmu(j,k)/re*(duy(j,k,3)+dvx(j,k,3))
     &        -dmul3/re*vel12(j)
         dp12(j,k,4)=-dmul4/re*vel12(j)

      end do
     

      do k = kbegin+1,kbegin+2
         do j = jtail1, jtail2

            dp11(j,k,1)=-fmu(j,1)/re*( 4.d0*dux(j,k,1)-2.d0*dvy(j,k,1) )
     &           /3.d0
            dp11(j,k,2)=-fmu(j,1)/re*( 4.d0*dux(j,k,2)-2.d0*dvy(j,k,2) )
     &           /3.d0
            dp11(j,k,3)=-fmu(j,1)/re*( 4.d0*dux(j,k,3)-2.d0*dvy(j,k,3) )
     &           /3.d0
            
            dp22(j,k,1)=-fmu(j,1)/re*( 4.d0*dvy(j,k,1)-2.d0*dux(j,k,1) )
     &           /3.d0
            dp22(j,k,2)=-fmu(j,1)/re*( 4.d0*dvy(j,k,2)-2.d0*dux(j,k,2) )
     &           /3.d0
            dp22(j,k,3)=-fmu(j,1)/re*( 4.d0*dvy(j,k,3)-2.d0*dux(j,k,3) )
     &           /3.d0
           
            dp12(j,k,1)= -fmu(j,1)/re*(duy(j,k,1)+dvx(j,k,1))
            dp12(j,k,2)= -fmu(j,1)/re*(duy(j,k,2)+dvx(j,k,2))
            dp12(j,k,3)= -fmu(j,1)/re*(duy(j,k,3)+dvx(j,k,3))
           
         end do
      end do


c     Calculate the derivative of the integrals w.r.t. Q 

      do n = 1,4
         do k = kbegin,kbegin+2
            do j=jtail1,jtail2 
               dI1(j,k,n)=0.d0
               dI2(j,k,n)=0.d0
            end do
         end do 
      end do

c     -- calc the distance of the source points for integration (sc contains at the end the number of points around the airfoil)
      sc=1
      do j = jtail1,jtail2-1           
         ds(sc)=SQRT((x(j+1,1)-x(j,1))**2+(y(j+1,1)-y(j,1))**2) 
         sc=sc+1 
      end do
  
      do n = 1,4
         do k = kbegin,kbegin+2
            sc=1
            do j=jtail1,jtail2-1
               jp1=j+1
               fac=0.5d0*ds(sc)
               dI1(j,k,n)=dI1(j,k,n) + fac*
     &              ( ns(sc,1)*dp11(j,k,n)+ns(sc,2)*dp12(j,k,n) )
               dI1(jp1,k,n)=dI1(jp1,k,n) + fac*
     &              (ns(sc+1,1)*dp11(jp1,k,n)+ns(sc+1,2)*dp12(jp1,k,n))
               dI2(j,k,n)=dI2(j,k,n) + fac*
     &              ( ns(sc,2)*dp22(j,k,n)+ns(sc,1)*dp12(j,k,n) )
               dI2(jp1,k,n)=dI2(jp1,k,n) + fac*
     &              (ns(sc+1,2)*dp22(jp1,k,n)+ns(sc+1,1)*dp12(jp1,k,n))
               sc=sc+1
            end do
         end do 
      end do


c     Assemble dJdQ

c     -- calc total number of time steps
      tc=nout-skipend
c     -- calc temporary count variable to access the right J1 and J2 entry
      ctmp=count-skipend

      if (jesdirk.eq.1) then

         fac=1.d0/(tc*deltat)/scalef
         do n=1,4
            do k=kbegin,kbegin+2
               do j=jtail1,jtail2

cMPRtemp           for only three time instances
c$$$               if (count.eq.skipend+2 ) then
c$$$
c$$$                  dJdQ(j,k,n) =fac*( 
c$$$     &            4.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
c$$$     &           -4.d0*(J1(ctmp+1)*dI1(j,k,n) + J2(ctmp+1)*dI2(j,k,n)) )
c$$$
c$$$               else if (count.eq.skipend+1) then
c$$$
c$$$                  dJdQ(j,k,n) =fac*(  
c$$$     &                 -3.d0*(J1(ctmp)*dI1(j,k,n)+J2(ctmp)*dI2(j,k,n))
c$$$     &               - J1(ctmp+1)*dI1(j,k,n) - J2(ctmp+1)*dI2(j,k,n)
c$$$     &               + J1(ctmp+2)*dI1(j,k,n) + J2(ctmp+2)*dI2(j,k,n) )
c$$$
c$$$               else if (count.eq.nout) then 
c$$$
c$$$                  dJdQ(j,k,n) =fac*(  
c$$$     &                 3.d0*(J1(ctmp)*dI1(j,k,n)+J2(ctmp)*dI2(j,k,n))
c$$$     &               + J1(ctmp-1)*dI1(j,k,n) + J2(ctmp-1)*dI2(j,k,n) 
c$$$     &               - J1(ctmp-2)*dI1(j,k,n) - J2(ctmp-2)*dI2(j,k,n) )
c$$$        
c$$$               end if
cMPRtemp


c                 -- Figure out at which time step we are at (works only for at least six time instances)
                  if (count.gt.skipend+3 .and. count.lt.nout-2 ) then

                     dJdQ(j,k,n) =fac*(  
     &               J1(ctmp-1)*dI1(j,k,n) + J2(ctmp-1)*dI2(j,k,n)
     &             - J1(ctmp+1)*dI1(j,k,n) - J2(ctmp+1)*dI2(j,k,n) ) 

                  else if (count.eq.skipend+1) then
                     
                     dJdQ(j,k,n) =fac*(  
     &              -3.d0*(J1(ctmp)*dI1(j,k,n)+J2(ctmp)*dI2(j,k,n))
     &              -J1(ctmp+1)*dI1(j,k,n) - J2(ctmp+1)*dI2(j,k,n) )

                  else if (count.eq.skipend+2) then

                     dJdQ(j,k,n) =fac*(  
     &               4.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -J1(ctmp+1)*dI1(j,k,n) - J2(ctmp+1)*dI2(j,k,n) )

                  else if (count.eq.skipend+3) then
                     
                     dJdQ(j,k,n) =fac*(  
     &              -J1(ctmp-2)*dI1(j,k,n)-J2(ctmp-2)*dI2(j,k,n)
     &              +J1(ctmp-1)*dI1(j,k,n) + J2(ctmp-1)*dI2(j,k,n)
     &              -J1(ctmp+1)*dI1(j,k,n) - J2(ctmp+1)*dI2(j,k,n) )
                  
                  else if (count.eq.nout-2) then
                     
                     dJdQ(j,k,n) =fac*(  
     &              +J1(ctmp+2)*dI1(j,k,n)+J2(ctmp+2)*dI2(j,k,n)
     &              -J1(ctmp+1)*dI1(j,k,n) - J2(ctmp+1)*dI2(j,k,n)
     &              +J1(ctmp-1)*dI1(j,k,n) + J2(ctmp-1)*dI2(j,k,n) )

                  else if (count.eq.nout-1) then
                     
                     dJdQ(j,k,n) =fac*(  
     &              -4.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n))
     &              +J1(ctmp-1)*dI1(j,k,n) + J2(ctmp-1)*dI2(j,k,n) )
                  
                  else if (count.eq.nout) then 

                     dJdQ(j,k,n) =fac*(  
     &               3.d0*(J1(ctmp)*dI1(j,k,n)+J2(ctmp)*dI2(j,k,n))
     &              +J1(ctmp-1)*dI1(j,k,n) + J2(ctmp-1)*dI2(j,k,n) )
        
                  end if

               enddo
            enddo
         enddo

      else if (jesdirk.eq.4) then

         fac=1.d0/6.d0/(tc*deltat)/scalef
         do n=1,4
            do k=kbegin,kbegin+2
               do j=jtail1,jtail2

c                 -- Figure out at which time step we are at (works only for at least eight time instances)
                  if (count.gt.skipend+4 .and. count.lt.nout-3 ) then

                     dJdQ(j,k,n) =fac*(  
     &              -J1(ctmp-2)*dI1(j,k,n) - J2(ctmp-2)*dI2(j,k,n)
     &              +8.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -8.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n)) 
     &              +J1(ctmp+2)*dI1(j,k,n) + J2(ctmp+2)*dI2(j,k,n) ) 

                  else if (count.eq.skipend+1) then
                     
                     dJdQ(j,k,n) =fac*(  
     &              -22.d0*(J1(ctmp)*dI1(j,k,n)+J2(ctmp)*dI2(j,k,n))
     &              -4.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n))
     &              +J1(ctmp+2)*dI1(j,k,n) + J2(ctmp+2)*dI2(j,k,n) )

                  else if (count.eq.skipend+2) then

                     dJdQ(j,k,n) =fac*(  
     &               36.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -6.d0*(J1(ctmp)*dI1(j,k,n)+J2(ctmp)*dI2(j,k,n))
     &              -8.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n))
     &              +J1(ctmp+2)*dI1(j,k,n) + J2(ctmp+2)*dI2(j,k,n) )

                  else if (count.eq.skipend+3) then
                     
                     dJdQ(j,k,n) =fac*(  
     &              -18.d0*(J1(ctmp-2)*dI1(j,k,n)+J2(ctmp-2)*dI2(j,k,n))
     &              +12.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -8.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n))
     &              +J1(ctmp+2)*dI1(j,k,n) + J2(ctmp+2)*dI2(j,k,n) )


                  else if (count.eq.skipend+4) then

                     dJdQ(j,k,n) =fac*(  
     &               4.d0*(J1(ctmp-3)*dI1(j,k,n)+J2(ctmp-3)*dI2(j,k,n))
     &              -2.d0*(J1(ctmp-2)*dI1(j,k,n)+J2(ctmp-2)*dI2(j,k,n))
     &              +8.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -8.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n))
     &              +J1(ctmp+2)*dI1(j,k,n) + J2(ctmp+2)*dI2(j,k,n) )
  
                  else if (count.eq.nout-3) then

                     dJdQ(j,k,n) =fac*(  
     &              -4.d0*(J1(ctmp+3)*dI1(j,k,n)+J2(ctmp+3)*dI2(j,k,n))
     &              +2.d0*(J1(ctmp+2)*dI1(j,k,n)+J2(ctmp+2)*dI2(j,k,n))
     &              -8.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n))
     &              +8.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -J1(ctmp-2)*dI1(j,k,n) - J2(ctmp-2)*dI2(j,k,n) )
                  
                  else if (count.eq.nout-2) then

                     dJdQ(j,k,n) =fac*(  
     &               18.d0*(J1(ctmp+2)*dI1(j,k,n)+J2(ctmp+2)*dI2(j,k,n))
     &              -12.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n))
     &              +8.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -J1(ctmp-2)*dI1(j,k,n) - J2(ctmp-2)*dI2(j,k,n) )
           
                  else if (count.eq.nout-1) then

                     dJdQ(j,k,n) =fac*(  
     &              -36.d0*(J1(ctmp+1)*dI1(j,k,n)+J2(ctmp+1)*dI2(j,k,n))
     &              +6.d0*(J1(ctmp)*dI1(j,k,n)+J2(ctmp)*dI2(j,k,n))
     &              +8.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -J1(ctmp-2)*dI1(j,k,n) - J2(ctmp-2)*dI2(j,k,n) )
                                                         
                  else if (count.eq.nout) then 

                     dJdQ(j,k,n) =fac*(  
     &               22.d0*(J1(ctmp)*dI1(j,k,n)+J2(ctmp)*dI2(j,k,n))
     &              +4.d0*(J1(ctmp-1)*dI1(j,k,n)+J2(ctmp-1)*dI2(j,k,n))
     &              -J1(ctmp-2)*dI1(j,k,n) - J2(ctmp-2)*dI2(j,k,n) )                  
        
                  end if

               enddo
            enddo
         enddo

      end if  !jesdirk==1 or jesdirk==4



c     Multiply dJ/dQ by the metric Jacobian, to give dJ/d(Q hat)
      do n=1,4
         do k=kbegin,kbegin+2
            do j=jtail1,jtail2          
            dJdQ(j,k,n) = dJdQ(j,k,n) * xyj(j,k)
          enddo
        enddo
      enddo

      return
      end    !calcdJdQ11 


 

      subroutine calcdJdQc (jdim, kdim, ndim, Q, xy, xyj, x, y,
     |    c_cc, c_cn, c_cm, dJdQ)
      implicit none

#include "../include/arcom.inc"

c     Argument data types; 
      integer jdim, kdim, ndim
      double precision Q(jdim,kdim,ndim),xy(jdim,kdim,4),xyj(jdim,kdim),
     |    x(jdim,kdim), y(jdim,kdim)
      double precision c_cc, c_cn, c_cm
      double precision dJdQ(jdim, kdim, ndim)

c     Local Variables
      integer j,k,n,jpl,jmi,jmi2 ! indices, jpl=j+1, jmi=j-1, jmi2=j-2
      double precision temp, const
c         const is a combination of c_cc, c_cn, and c_cm: a constant by
c         which to multiply derivatives wrt Q

      if (c_cm .ne. 0.d0) then
        write (*,*) 'Warning: c_cm not yet implemented in dJdQ'
      endif

c     Inviscid contribution: pressure integral
      temp = gami*fsmach**(-2)
      do j = jtail1+1, jtail2
        jpl=jplus(j)
        jmi=jminus(j)
        jmi2=jminus(jmi)
        const = temp * (c_cc*(y(j,1)-y(jmi,1)) + c_cn*(x(jmi,1)-x(j,1))
     |      + c_cm*(x(j,1)+x(jmi,1))*.5*(x(j,1) -x(jmi,1)))

        dJdQ(j,1,1) = dJdQ(j,1,1) + 0.5d0*const*
     |       (Q(j,1,2)**2/Q(j,1,1)**2+Q(j,1,3)**2/Q(j,1,1)**2)
        dJdQ(j,1,2) = dJdQ(j,1,2) - const*Q(j,1,2)/Q(j,1,1)
        dJdQ(j,1,3) = dJdQ(j,1,3) - const*Q(j,1,3)/Q(j,1,1)
        dJdQ(j,1,4) = dJdQ(j,1,4) + const

        dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + 0.5d0* const* 
     |       (Q(jmi,1,2)**2/Q(jmi,1,1)**2+Q(jmi,1,3)**2/Q(jmi,1,1)**2)
        dJdQ(jmi,1,2) = dJdQ(jmi,1,2)-const*Q(jmi,1,2)
     |       /Q(jmi,1,1)
        dJdQ(jmi,1,3) = dJdQ(jmi,1,3)-const*Q(jmi,1,3)
     |       /Q(jmi,1,1)
        dJdQ(jmi,1,4) = dJdQ(jmi,1,4)+const
      enddo

c     Viscous contribution: shear stress integral
      if (viscous) then
        temp = 1.d0/(fsmach**2*re)
        do j = jtail1+1, jtail2
          jpl=jplus(j)
          jmi=jminus(j)
          jmi2=jminus(jmi)
          const = temp *
     |        (  c_cc*(x(j,1)-x(jmi,1))
     |         + c_cn*(y(j,1)-y(jmi,1))
     |         + c_cm*(  (x(j,1)+x(jmi,1))*.5*(y(j,1) -y(jmi,1))
     |                 - (y(j,1)+y(jmi,1))*.5*(x(j,1) -x(jmi,1))))

c         xiy*uxi-xix*vxi terms
          if (j > jtail1+1 .and. j < jtail2) then
c           Interior nodes, xiy*uxi-xix*vxi terms
            dJdQ(jpl,1,1) = dJdQ(jpl,1,1) - 0.5d0*const*
     |          (xy(j,1,2)*Q(jpl,1,2)-xy(j,1,1)*Q(jpl,1,3))
     |          /Q(jpl,1,1)**2
            dJdQ(jpl,1,2) = dJdQ(jpl,1,2) + 0.5d0*const*xy(j,1,2)/
     |          Q(jpl,1,1)
            dJdQ(jpl,1,3) = dJdQ(jpl,1,3) - 0.5d0*const*xy(j,1,1)/
     |          Q(jpl,1,1)

            dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + 0.5d0*const*
     |          (xy(j,1,2)*Q(jmi,1,2)-xy(j,1,1)*Q(jmi,1,3))
     |          /Q(jmi,1,1)**2
            dJdQ(jmi,1,2) = dJdQ(jmi,1,2) - 0.5d0*const*xy(j,1,2)/
     |          Q(jmi,1,1)
            dJdQ(jmi,1,3) = dJdQ(jmi,1,3) + 0.5d0*const*xy(j,1,1)/
     |          Q(jmi,1,1)

            dJdQ(j,1,1) = dJdQ(j,1,1) - 0.5d0*const*
     |          (xy(jmi,1,2)*Q(j,1,2)-xy(jmi,1,1)*Q(j,1,3))/Q(j,1,1)**2
            dJdQ(j,1,2) = dJdQ(j,1,2) + 0.5d0*const*xy(jmi,1,2)/Q(j,1,1)
            dJdQ(j,1,3) = dJdQ(j,1,3) - 0.5d0*const*xy(jmi,1,1)/Q(j,1,1)

            dJdQ(jmi2,1,1) = dJdQ(jmi2,1,1) + 0.5d0*const*
     |          (xy(jmi,1,2)*Q(jmi2,1,2)-xy(jmi,1,1)*Q(jmi2,1,3))/
     |          Q(jmi2,1,1)**2
            dJdQ(jmi2,1,2) = dJdQ(jmi2,1,2) -0.5d0*const*xy(jmi,1,2)/
     |          Q(jmi2,1,1)
            dJdQ(jmi2,1,3) = dJdQ(jmi2,1,3) +0.5d0*const*xy(jmi,1,1)/
     |          Q(jmi2,1,1)
          else if (j == jtail1+1) then
c           Boundary nodes, xiy*uxi-xix*vxi terms (uses upwind stencil)
            dJdQ(jpl,1,1) = dJdQ(jpl,1,1) - 0.5d0*const*
     |          (xy(j,1,2)*Q(jpl,1,2)-xy(j,1,1)*Q(jpl,1,3))
     |          /Q(jpl,1,1)**2
            dJdQ(jpl,1,2) = dJdQ(jpl,1,2) + 0.5d0*const*xy(j,1,2)/
     |          Q(jpl,1,1)
            dJdQ(jpl,1,3) = dJdQ(jpl,1,3) - 0.5d0*const*xy(j,1,1)/
     |          Q(jpl,1,1)

            dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + 0.5d0*const*
     |          (xy(j,1,2)*Q(jmi,1,2)-xy(j,1,1)*Q(jmi,1,3))
     |          /Q(jmi,1,1)**2
            dJdQ(jmi,1,2) = dJdQ(jmi,1,2) - 0.5d0*const*xy(j,1,2)/
     |          Q(jmi,1,1)
            dJdQ(jmi,1,3) = dJdQ(jmi,1,3) + 0.5d0*const*xy(j,1,1)/
     |          Q(jmi,1,1)

            dJdQ(j,1,1) = dJdQ(j,1,1) - const*
     |          (xy(jmi,1,2)*Q(j,1,2)-xy(jmi,1,1)*Q(j,1,3))/Q(j,1,1)**2
            dJdQ(j,1,2) = dJdQ(j,1,2) + const*xy(jmi,1,2)/Q(j,1,1)
            dJdQ(j,1,3) = dJdQ(j,1,3) - const*xy(jmi,1,1)/Q(j,1,1)

            dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + const*
     |          (xy(jmi,1,2)*Q(jmi,1,2)-xy(jmi,1,1)*Q(jmi,1,3))/
     |          Q(jmi,1,1)**2
            dJdQ(jmi,1,2) = dJdQ(jmi,1,2) -const*xy(jmi,1,2)/
     |          Q(jmi,1,1)
            dJdQ(jmi,1,3) = dJdQ(jmi,1,3) +const*xy(jmi,1,1)/
     |          Q(jmi,1,1)

          else if (j == jtail2) then
c           Boundary nodes, xiy*uxi-xix*vxi terms (uses upwind stencil)
            dJdQ(j,1,1) = dJdQ(j,1,1) - const*
     |          (xy(j,1,2)*Q(j,1,2)-xy(j,1,1)*Q(j,1,3))/Q(j,1,1)**2
            dJdQ(j,1,2) = dJdQ(j,1,2) + const*xy(j,1,2)/
     |          Q(j,1,1)
            dJdQ(j,1,3) = dJdQ(j,1,3) - const*xy(j,1,1)/
     |          Q(j,1,1)

            dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + const*
     |          (xy(j,1,2)*Q(jmi,1,2)-xy(j,1,1)*Q(jmi,1,3))
     |          /Q(jmi,1,1)**2
            dJdQ(jmi,1,2) = dJdQ(jmi,1,2) - const*xy(j,1,2)/
     |          Q(jmi,1,1)
            dJdQ(jmi,1,3) = dJdQ(jmi,1,3) + const*xy(j,1,1)/
     |          Q(jmi,1,1)

            dJdQ(j,1,1) = dJdQ(j,1,1) - 0.5d0*const*
     |          (xy(jmi,1,2)*Q(j,1,2)-xy(jmi,1,1)*Q(j,1,3))/Q(j,1,1)**2
            dJdQ(j,1,2) = dJdQ(j,1,2) + 0.5d0*const*xy(jmi,1,2)/Q(j,1,1)
            dJdQ(j,1,3) = dJdQ(j,1,3) - 0.5d0*const*xy(jmi,1,1)/Q(j,1,1)

            dJdQ(jmi2,1,1) = dJdQ(jmi2,1,1) + 0.5d0*const*
     |          (xy(jmi,1,2)*Q(jmi2,1,2)-xy(jmi,1,1)*Q(jmi2,1,3))/
     |          Q(jmi2,1,1)**2
            dJdQ(jmi2,1,2) = dJdQ(jmi2,1,2) -0.5d0*const*xy(jmi,1,2)/
     |          Q(jmi2,1,1)
            dJdQ(jmi2,1,3) = dJdQ(jmi2,1,3) +0.5d0*const*xy(jmi,1,1)/
     |          Q(jmi2,1,1)

          end if  ! xiy*uxi-xix*vxi terms

c         etay*ueta-etay*veta terms
          dJdQ(j,1,1) = dJdQ(j,1,1) + 1.5d0*const*
     |        (xy(j,1,4)*Q(j,1,2)-xy(j,1,3)*Q(j,1,3))/Q(j,1,1)**2
          dJdQ(j,1,2) = dJdQ(j,1,2) - 1.5d0*const*xy(j,1,4)/Q(j,1,1)
          dJdQ(j,1,3) = dJdQ(j,1,3) + 1.5d0*const*xy(j,1,3)/Q(j,1,1)

          dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + 1.5d0*const*
     |        (xy(jmi,1,4)*Q(jmi,1,2)-xy(jmi,1,3)*Q(jmi,1,3))
     |        /Q(jmi,1,1)**2
          dJdQ(jmi,1,2) = dJdQ(jmi,1,2) - 1.5d0*const*xy(jmi,1,4)/
     |        Q(jmi,1,1)
          dJdQ(jmi,1,3) = dJdQ(jmi,1,3) + 1.5d0*const*xy(jmi,1,3)/
     |        Q(jmi,1,1)

          dJdQ(j,2,1) = dJdQ(j,2,1) - 2.0d0*const*
     |        (xy(j,1,4)*Q(j,2,2)-xy(j,1,3)*Q(j,2,3))/Q(j,2,1)**2
          dJdQ(j,2,2) = dJdQ(j,2,2) + 2.0d0*const*xy(j,1,4)/Q(j,2,1)
          dJdQ(j,2,3) = dJdQ(j,2,3) - 2.0d0*const*xy(j,1,3)/Q(j,2,1)

          dJdQ(jmi,2,1) = dJdQ(jmi,2,1) - 2.0d0*const*
     |        (xy(jmi,1,4)*Q(jmi,2,2)-xy(jmi,1,3)*Q(jmi,2,3))
     |        /Q(jmi,2,1)**2
          dJdQ(jmi,2,2) = dJdQ(jmi,2,2) + 2.0d0*const*xy(jmi,1,4)/
     |        Q(jmi,2,1)
          dJdQ(jmi,2,3) = dJdQ(jmi,2,3) - 2.0d0*const*xy(jmi,1,3)/
     |        Q(jmi,2,1)

          dJdQ(j,3,1) = dJdQ(j,3,1) + 0.5d0*const*
     |        (xy(j,1,4)*Q(j,3,2)-xy(j,1,3)*Q(j,3,3))/Q(j,3,1)**2
          dJdQ(j,3,2) = dJdQ(j,3,2) - 0.5d0*const*xy(j,1,4)/Q(j,3,1)
          dJdQ(j,3,3) = dJdQ(j,3,3) + 0.5d0*const*xy(j,1,3)/Q(j,3,1)

          dJdQ(jmi,3,1) = dJdQ(jmi,3,1) + 0.5d0*const*
     |        (xy(jmi,1,4)*Q(jmi,3,2)-xy(jmi,1,3)*Q(jmi,3,3))
     |        /Q(jmi,3,1)**2
          dJdQ(jmi,3,2) = dJdQ(jmi,3,2) - 0.5d0*const*xy(jmi,1,4)/
     |        Q(jmi,3,1)
          dJdQ(jmi,3,3) = dJdQ(jmi,3,3) + 0.5d0*const*xy(jmi,1,3)/
     |        Q(jmi,3,1)
        enddo
      endif   ! Viscous contribution

c     Multiply dJ/dQ by the metric Jacobian, to give dJ/d(Q hat)
      do n=1,4
         do k=kbegin,kbegin+2
            do j=jtail1,jtail2          
            dJdQ(j,k,n) = dJdQ(j,k,n) * xyj(j,k)
          enddo
        enddo
      enddo

      return
      end    !calcdJdQc




      

      subroutine calcobjair11fd(fobj,qp,count,nout,skipend,jdim,kdim,
     &     deltat,scalef,x,y,xy,xyj,ns)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i,j,k,l,nout,skipend,jdim,kdim,tc,sc,jp1,jm1,jp2,jm2,count
      integer n

      double precision q(jdim,kdim,4),xyj(jdim,kdim),xy(jdim,kdim,4)
      double precision qp(jdim,kdim,4)
      double precision turmu(jdim,kdim),x(jdim,kdim),y(jdim,kdim)
      double precision press(jdim,kdim),sndsp(jdim,kdim),fmu(jdim,kdim)

      double precision fobj,ftmp,scalef,deltat,temp,precon(jdim,kdim,6)

      double precision det,a11,a12,a13,a21,a22,a23,a31,a32,a33
      double precision dlc,dcc,m,den,Jtmp(tdim),J1(tdim),J2(tdim)

      double precision uxi,ueta,vxi,veta,xix,xiy,etax,etay,ux,uy,vx,vy

      double precision xs(sdim),ys(sdim),ns(sdim,2),ds(sdim)
      double precision Int1(tdim),Int2(tdim)
      double precision p11(tdim,sdim),p12(tdim,sdim),p22(tdim,sdim)


c     -- calc the distance of the source points for integration (sc contains at the end the number of points around the airfoil)
      sc=1
      do j = jtail1,jtail2-1           
         ds(sc)=SQRT((x(j+1,1)-x(j,1))**2+(y(j+1,1)-y(j,1))**2) 
         sc=sc+1 
      end do

c
c     Calculate compressive stress tensor along the FWH surface
c     p11=p-fmu*(4*du/dx-2*dv/dy)/3
c     p22=p-fmu*(4*dv/dy-2*du/dx)/3
c     p12=p21=-fmu*(du/dy+dv/dx)
c     Note: du/d. = ueta*deta/d. + uxi*dxi/d.
c           dv/d. = veta*deta/d. + vxi*dxi/d.

      tc=0

      do i=skipend+1,nout

         tc=tc+1 

         if (i .ne. count) then

c        -- read physical flow field --         
            call iomarkus(3,i,jdim,kdim,q,xyj,turmu)

         else

            do n=1,4
               do k=kbegin,kend
                  do j=jbegin,jend          
                     q(j,k,n) = qp(j,k,n)
                  enddo
               enddo
            enddo

         end if

c     -- calculate pressure and sound speed --

         call calcps (jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- compute laminar viscosity based on sutherland's law --
         if (viscous) call fmun(jdim, kdim, q, press, fmu, sndsp)

         k=1
         

         sc=1        
         j = jtail1                                                  
         jp1 = j+1  
         jp2 = j+2
c       -second-order biased
         uxi = -1.5*q(j,k,2)/q(j,k,1)+2.*q(jp1,k,2)/q(jp1,k,1)       
     *        -.5*q(jp2,k,2)/q(jp2,k,1)
         vxi = -1.5*q(j,k,3)/q(j,k,1)+2.*q(jp1,k,3)/q(jp1,k,1)       
     *        -.5*q(jp2,k,3)/q(jp2,k,1)
         ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *        -.5*q(j,k+2,2)/q(j,k+2,1)                              
         veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *        -.5*q(j,k+2,3)/q(j,k+2,1)
c
         xix = xy(j,k,1)                                             
         xiy = xy(j,k,2)                                             
         etax = xy(j,k,3)                                            
         etay = xy(j,k,4) 
         ux=uxi*xix+ueta*etax
         uy=uxi*xiy+ueta*etay
         vx=vxi*xix+veta*etax
         vy=vxi*xiy+veta*etay

         p11(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*ux-2.d0*vy)/3.d0
         p22(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*vy-2.d0*ux)/3.d0
         p12(tc,sc)=-fmu(j,k)/re*(uy+vx) 

c$$$         p11(tc,sc)=press(j,k)
c$$$         p22(tc,sc)=press(j,k)
c$$$         p12(tc,sc)=0.d0
                                                                                                                                                                 
         do j = jtail1+1,jtail2-1

            sc=sc+1                                             
            jp1 = j+1                                              
            jm1 = j-1  
c           -second-order central
            uxi = .5*(q(jp1,k,2)/q(jp1,k,1)-q(jm1,k,2)/q(jm1,k,1))      
            vxi = .5*(q(jp1,k,3)/q(jp1,k,1)-q(jm1,k,3)/q(jm1,k,1))      
c           -second-order biased
            ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *           -.5*q(j,k+2,2)/q(j,k+2,1)                              
            veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *           -.5*q(j,k+2,3)/q(j,k+2,1)                              
            xix = xy(j,k,1)                                             
            xiy = xy(j,k,2)                                             
            etax = xy(j,k,3)                                            
            etay = xy(j,k,4)                                            
            ux=uxi*xix+ueta*etax
            uy=uxi*xiy+ueta*etay
            vx=vxi*xix+veta*etax
            vy=vxi*xiy+veta*etay

            p11(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*ux-2.d0*vy)/3.d0
            p22(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*vy-2.d0*ux)/3.d0
            p12(tc,sc)=-fmu(j,k)/re*(uy+vx)

c$$$            p11(tc,sc)=press(j,k)
c$$$            p22(tc,sc)=press(j,k)
c$$$            p12(tc,sc)=0.d0
           

         end do

         sc=sc+1 
         j = jtail2                                                  
         jm1 = j-1 
         jm2 = j-2
c       -second-order biased
         uxi = 1.5*q(j,k,2)/q(j,k,1)-2.*q(jm1,k,2)/q(jm1,k,1)       
     *        +.5*q(jm2,k,2)/q(jm2,k,1)
         vxi = 1.5*q(j,k,3)/q(j,k,1)-2.*q(jm1,k,3)/q(jm1,k,1)       
     *        +.5*q(jm2,k,3)/q(jm2,k,1)                                
         ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)       
     *        -.5*q(j,k+2,2)/q(j,k+2,1)                              
         veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)       
     *        -.5*q(j,k+2,3)/q(j,k+2,1)                              
c     
         xix = xy(j,k,1)                                             
         xiy = xy(j,k,2)                                             
         etax = xy(j,k,3)                                            
         etay = xy(j,k,4) 
         ux=uxi*xix+ueta*etax
         uy=uxi*xiy+ueta*etay
         vx=vxi*xix+veta*etax
         vy=vxi*xiy+veta*etay

         p11(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*ux-2.d0*vy)/3.d0
         p22(tc,sc)=press(j,k)-fmu(j,k)/re*(4.d0*vy-2.d0*ux)/3.d0
         p12(tc,sc)=-fmu(j,k)/re*(uy+vx)

c$$$         p11(tc,sc)=press(j,k)
c$$$         p22(tc,sc)=press(j,k)
c$$$         p12(tc,sc)=0.d0
         

      end do !Calculate stress tensor along the surface

c     Integrate along the curve with trapezoidal rule
 
      do l=1,tc
                             
         Int1(l)=0.d0
         Int2(l)=0.d0
    
         do k=1,sc-1   
            Int1(l)=Int1(l) + ( ns(k,1)*p11(l,k) + ns(k+1,1)*p11(l,k+1)  
     &           + ns(k,2)*p12(l,k) + ns(k+1,2)*p12(l,k+1) )* 0.50*ds(k)
            Int2(l)=Int2(l) + ( ns(k,2)*p22(l,k) + ns(k+1,2)*p22(l,k+1)  
     &           + ns(k,1)*p12(l,k) + ns(k+1,1)*p12(l,k+1) )* 0.50*ds(k)
         end do

      end do

c     Take time derivative (second or fourth order accurate)

      if (jesdirk.eq.1) then

         J1(1)=(-Int1(3)+4.d0*Int1(2)-3.d0*Int1(1))/(2.d0*deltat)
         J2(1)=(-Int2(3)+4.d0*Int2(2)-3.d0*Int2(1))/(2.d0*deltat)
         Jtmp(1)=J1(1)**2+J2(1)**2
         do l=2,tc-1
            J1(l)=(Int1(l+1)-Int1(l-1))/(2.d0*deltat)
            J2(l)=(Int2(l+1)-Int2(l-1))/(2.d0*deltat)
            Jtmp(l)=J1(l)**2+J2(l)**2
         end do
         J1(tc)=(Int1(tc-2)-4.d0*Int1(tc-1)+3.d0*Int1(tc))/(2.d0*deltat)
         J2(tc)=(Int2(tc-2)-4.d0*Int2(tc-1)+3.d0*Int2(tc))/(2.d0*deltat)
         Jtmp(tc)=J1(tc)**2+J2(tc)**2
         
      else if (jesdirk.eq.4) then

         J1(1)=(4.d0*Int1(4)-18.d0*Int1(3)+36.d0*Int1(2)-22.d0*Int1(1))/
     &         (12.d0*deltat)
         J2(1)=(4.d0*Int2(4)-18.d0*Int2(3)+36.d0*Int2(2)-22.d0*Int2(1))/
     &         (12.d0*deltat)
         J1(2)=(-2.d0*Int1(4)+12.d0*Int1(3)-6.d0*Int1(2)-4.d0*Int1(1))/
     &         (12.d0*deltat)
         J2(2)=(-2.d0*Int2(4)+12.d0*Int2(3)-6.d0*Int2(2)-4.d0*Int2(1))/
     &         (12.d0*deltat)
         do l=3,tc-2
            J1(l)=(Int1(l-2)-8.d0*Int1(l-1)+8.d0*Int1(l+1)-Int1(l+2))/
     &            (12.d0*deltat)
            J2(l)=(Int2(l-2)-8.d0*Int2(l-1)+8.d0*Int2(l+1)-Int2(l+2))/
     &            (12.d0*deltat)
         end do
         J1(tc-1)=(2.d0*Int1(tc-3)-12.d0*Int1(tc-2)+6.d0*Int1(tc-1)+
     &             4.d0*Int1(tc))/(12.d0*deltat)
         J2(tc-1)=(2.d0*Int2(tc-3)-12.d0*Int2(tc-2)+6.d0*Int2(tc-1)+
     &             4.d0*Int2(tc))/(12.d0*deltat)
         J1(tc)=(-4.d0*Int1(tc-3)+18.d0*Int1(tc-2)-36.d0*Int1(tc-1)+
     &            22.d0*Int1(tc))/(12.d0*deltat)
         J2(tc)=(-4.d0*Int2(tc-3)+18.d0*Int2(tc-2)-36.d0*Int2(tc-1)+
     &            22.d0*Int2(tc))/(12.d0*deltat)
        
      end if

c     Square and combine to Jtmp(t)
      
      do l=1,tc
         Jtmp(l)=J1(l)**2+J2(l)**2
      end do


c     Time average

      fobj= 0.0d0
      do l=1,tc
         fobj=fobj+Jtmp(l) 
      end do
      fobj=fobj/tc


c     scaling of the objective function

c$$$      if (scalef .eq. 1.d0)  scalef=fobj         
      fobj=fobj/scalef

c     Constraint violation?

      return
      end  !calcobjair11fd






      subroutine calcobjair13(ftmp,q,xyj,turmu,nout,skipend,jdim,kdim,
     &     tt)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i,j,k,nout,skipend,jdim,kdim

      double precision q(jdim,kdim,4),xyj(jdim,kdim)
      double precision turmu(jdim,kdim)     

      double precision ftmp,tt

      double precision vorti
 
      ftmp= 0.0d0

      do i=skipend+1,nout

         if (nout .gt. 1) then
c     -- read physical flow field at each desired time step --         
            call iomarkus(3,i,jdim,kdim,q,xyj,turmu)
         end if
 
         do k = kbegin,kend 
            do j = jbegin,jend
            
               if (k==kend) then

          vorti=(q(j,k,2)/q(j,k,1) - q(j,k-1,2)/q(j,k-1,1)) -
     &              (q(jplus(j),k,3)/q(jplus(j),k,1) - 
     &              q(jminus(j),k,3)/q(jminus(j),k,1))*0.5d0

               else if (k==kbegin) then

          vorti=(q(j,k+1,2)/q(j,k+1,1) - q(j,k,2)/q(j,k,1)) -
     &              (q(jplus(j),k,3)/q(jplus(j),k,1) - 
     &              q(jminus(j),k,3)/q(jminus(j),k,1))*0.5d0


               else                            
             
          vorti=(q(j,k+1,2)/q(j,k+1,1) - q(j,k-1,2)/q(j,k-1,1))*0.5d0 -
     &              (q(jplus(j),k,3)/q(jplus(j),k,1) - 
     &              q(jminus(j),k,3)/q(jminus(j),k,1))*0.5d0

               endif
                   
               ftmp =  ftmp + vorti**2

            end do
         end do

      end do
       
      ftmp=tt*0.5d0*ftmp
      
      return
      end  !calcobjair13


      subroutine calcdJdQ13(jdim,kdim,q,j,k,dfdQ,vorti,deltat)

      implicit none

#include "../include/arcom.inc"

      integer j,k,jdim,kdim,jpl,jmi,kpl,kmi

      double precision q(jdim,kdim,4),dfdQ(jdim,kdim,4),vorti
      double precision deltat,vor

      vor=vorti*deltat

      jpl=jplus(j)
      jmi=jminus(j)
      kpl=k+1
      kmi=k-1

      if ( k .gt. kbegin .and. k .lt. kend ) then
  
         dfdQ(j,kpl,1) =dfdQ(j,kpl,1)-vor*0.5d0*q(j,kpl,2)/q(j,kpl,1)**2    
         dfdQ(j,kmi,1) =dfdQ(j,kmi,1)+vor*0.5d0*q(j,kmi,2)/q(j,kmi,1)**2                
         dfdQ(jpl,k,1) =dfdQ(jpl,k,1)+vor*0.5d0*q(jpl,k,3)/q(jpl,k,1)**2                
         dfdQ(jmi,k,1) =dfdQ(jmi,k,1)-vor*0.5d0*q(jmi,k,3)/q(jmi,k,1)**2

         dfdQ(j,kpl,2) = dfdQ(j,kpl,2)+vor*0.5d0/q(j,kpl,1) 
         dfdQ(j,kmi,2) = dfdQ(j,kmi,2)-vor*0.5d0/q(j,kmi,1)
 
         dfdQ(jpl,k,3) = dfdQ(jpl,k,3)-vor*0.5d0/q(jpl,k,1) 
         dfdQ(jmi,k,3) = dfdQ(jmi,k,3)+vor*0.5d0/q(jmi,k,1)
           
      else if ( k == kbegin ) then

         dfdQ(j,kpl,1) =dfdQ(j,kpl,1)-vor*q(j,kpl,2)/q(j,kpl,1)**2    
         dfdQ(j,k,1) =dfdQ(j,k,1)+vor*q(j,k,2)/q(j,k,1)**2                
         dfdQ(jpl,k,1) =dfdQ(jpl,k,1)+vor*0.5d0*q(jpl,k,3)/q(jpl,k,1)**2                
         dfdQ(jmi,k,1) =dfdQ(jmi,k,1)-vor*0.5d0*q(jmi,k,3)/q(jmi,k,1)**2

         dfdQ(j,kpl,2) = dfdQ(j,kpl,2)+vor/q(j,kpl,1) 
         dfdQ(j,k,2) = dfdQ(j,k,2)-vor/q(j,k,1)
 
         dfdQ(jpl,k,3) = dfdQ(jpl,k,3)-vor*0.5d0/q(jpl,k,1) 
         dfdQ(jmi,k,3) = dfdQ(jmi,k,3)+vor*0.5d0/q(jmi,k,1)          
        
      else if ( k == kend ) then       

         dfdQ(j,k,1) =dfdQ(j,k,1)-vor*q(j,k,2)/q(j,k,1)**2    
         dfdQ(j,kmi,1) =dfdQ(j,kmi,1)+vor*q(j,kmi,2)/q(j,kmi,1)**2                
         dfdQ(jpl,k,1) =dfdQ(jpl,k,1)+vor*0.5d0*q(jpl,k,3)/q(jpl,k,1)**2                
         dfdQ(jmi,k,1) =dfdQ(jmi,k,1)-vor*0.5d0*q(jmi,k,3)/q(jmi,k,1)**2

         dfdQ(j,k,2) = dfdQ(j,k,2)+vor/q(j,k,1) 
         dfdQ(j,kmi,2) = dfdQ(j,kmi,2)-vor/q(j,kmi,1)
 
         dfdQ(jpl,k,3) = dfdQ(jpl,k,3)-vor*0.5d0/q(jpl,k,1) 
         dfdQ(jmi,k,3) = dfdQ(jmi,k,3)+vor*0.5d0/q(jmi,k,1)
        
      end if

      return
      end  !calcdJdQ13






C SUBROUTINE PNPOLY 
C 
C PURPOSE 
C TO DETERMINE WHETHER A POINT IS INSIDE A POLYGON 
C 
C USAGE 
C CALL PNPOLY (PX, PY, XX, YY, N, INOUT ) 
C 
C DESCRIPTION OF THE PARAMETERS 
C PX - X-COORDINATE OF POINT IN QUESTION. 
C PY - Y-COORDINATE OF POINT IN QUESTION. 
C XX - N LONG VECTOR CONTAINING X-COORDINATES OF 
C VERTICES OF POLYGON. 
C YY - N LONG VECTOR CONTAING Y-COORDINATES OF 
C VERTICES OF POLYGON. 
C N - NUMBER OF VERTICES IN THE POLYGON. 
C INOUT - THE SIGNAL RETURNED: 
C -1 IF THE POINT IS OUTSIDE OF THE POLYGON, 
C 0 IF THE POINT IS ON AN EDGE OR AT A VERTEX, 
C 1 IF THE POINT IS INSIDE OF THE POLYGON. 
C 
C REMARKS 
C THE VERTICES MAY BE LISTED CLOCKWISE OR ANTICLOCKWISE. 
C THE FIRST MAY OPTIONALLY BE REPEATED, IF SO N MAY 
C OPTIONALLY BE INCREASED BY 1. 
C THE INPUT POLYGON MAY BE A COMPOUND POLYGON CONSISTING 
C OF SEVERAL SEPARATE SUBPOLYGONS. IF SO, THE FIRST VERTEX 
C OF EACH SUBPOLYGON MUST BE REPEATED, AND WHEN CALCULATING 
C N, THESE FIRST VERTICES MUST BE COUNTED TWICE. 
C INOUT IS THE ONLY PARAMETER WHOSE VALUE IS CHANGED. 
C THE SIZE OF THE ARRAYS MUST BE INCREASED IF N > MAXDIM 
C WRITTEN BY RANDOLPH FRANKLIN, UNIVERSITY OF OTTAWA, 7/70. 
C 
C SUBROUTINES AND FUNCTION SUBPROGRAMS REQUIRED 
C NONE 
C 
C METHOD 
C A VERTICAL LINE IS DRAWN THRU THE POINT IN QUESTION. IF IT 
C CROSSES THE POLYGON AN ODD NUMBER OF TIMES, THEN THE 
C POINT IS INSIDE OF THE POLYGON. 
C 
C .................................................................. 
      SUBROUTINE PNPOLY(PX,PY,XX,YY,N,INOUT)
 
      REAL X(1000),Y(1000),XX(N),YY(N) 
      LOGICAL MX,MY,NX,NY 
      INTEGER O 
C OUTPUT UNIT FOR PRINTED MESSAGES 
      DATA O/6/ 

      MAXDIM=1000 

      IF(N.LE.MAXDIM)GO TO 6 
      WRITE(*,7) N
 7    FORMAT('0WARNING:',I5,' TOO GREAT FOR THIS VERSION OF PNPOLY') 
      RETURN 

 6    DO I=1,N 
         X(I)=XX(I)-PX 
         Y(I)=YY(I)-PY 
      END DO

      INOUT=-1 

      DO 2 I=1,N 
      J=1+MOD(I,N) 
      MX=X(I).GE.0.0 
      NX=X(J).GE.0.0 
      MY=Y(I).GE.0.0 
      NY=Y(J).GE.0.0 
      IF(.NOT.((MY.OR.NY).AND.(MX.OR.NX)).OR.(MX.AND.NX)) GO TO 2 
      IF(.NOT.(MY.AND.NY.AND.(MX.OR.NX).AND..NOT.(MX.AND.NX))) GO TO 3 
      INOUT=-INOUT 
      GO TO 2  
 3    IF((Y(I)*X(J)-X(I)*Y(J))/(X(J)-X(I))) 2,4,5  
 4    INOUT=0 
      RETURN  
 5    INOUT=-INOUT  
 2    CONTINUE
 
      RETURN 
      END 

