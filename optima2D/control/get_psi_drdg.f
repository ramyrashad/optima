c-----------------------------------------------------------------------
c get_psi_dRdG
c
c The following subroutines for get_psi_dRdG are defined in this file:
c add_psi_dEdG
c add_psi_dFdG
c add_psi_ADterm_xi
c add_psi_ADterm_eta
c get_dVdG_coefs
c add_psi_dSAdG
c add_delta_eta_I
c add_delta_xi_I
c ArrayStats
c
c Nomenclature used in comments:
c   Symbol        Meaning
c   ------        -------
c   da/db         derivative (Jacobian matrix) of a with respect to b
c   G             vector of grid node locations (x- & y-coordinates)
c   R             flow residual vector (R = -s in getrhs.f)
c   delta_xi(a)   finite difference of a in the xi direction
c   delta_eta(a)  finite difference of a in the eta direction
c
c Chad Oldfield, August 2005
c-----------------------------------------------------------------------

c---- get_psi_dRdG -----------------------------------------------------
c Computes
c     psi^T * dR/dG     (d => partial differentiation)
c ie: the adjoint vector multiplied by the Jacobian of the flow residual
c with respect to the grid node locations.  dR/dG is computed via the
c grid metrics, and follows the residual calculation used in PROBE.
c
c Calculation proceeds by adding adding the inviscid flux, artificial
c dissipation, viscous flux, turbulunce modelling, and boundary
c condition terms one at a time.
c
c Here, R is considered to be defined by
c   R = R(Q, G, X)
c where Q, G, and X are independant variables.  Therefore, dR/dG is
c evaluated with Q and X held constant (i.e. not with Q hat held constant)
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c ndim: number of governing equations
c q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c press: pressure (without the inverse of the metric jacobian included)
c sndsp: speed of sound
c fmu, turmu: molecular viscosity, turbulence viscosity (turmu is
c   evaluated at half nodes.  i.e. turmu(j,k) corresponds to the
c   turbulence viscosity at (j, k+1/2).
c x, y: cartesian coordinates of the grid nodes (i.e. G)
c xy, xyj: metrics, and metric jacobian
c psi: adjoint vector
c psina: adjoint vector element pertaining to the flow solver equation
c   that is added when lift is constrained by the flow solver
c
c Returns:
c psi_dRdG: psi^T * dR/dG, as above
c
c Limitations:
c Does not include viscous terms in the xi-direction, or viscous cross
c   terms.
c Only supports the Spalart-Allmaras turbulence model.
c Does not support calculation of J, the metric Jacobian, using cell
c   areas.
c Supports only a c-mesh.
c Does not support the circulation correction (need to look at angle of
c   attack relaxation and derivatives of the lift coefficient to include
c   it).
c
c Chad Oldfield, May 2005.
c-----------------------------------------------------------------------
      subroutine get_psi_dRdG (jdim, kdim, ndim, q, press, sndsp,
     |    fmu, turmu, x, y, xy, xyj, psi, psina, psi_dRdG)
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     Argument data types; see below for descriptions
      integer jdim, kdim, ndim
      double precision Q(jdim,kdim,ndim), press(jdim,kdim),
     |    sndsp(jdim,kdim),
     |    fmu(jdim,kdim), turmu(jdim,kdim),
     |    x(jdim,kdim), y(jdim,kdim), xyj(jdim,kdim), xy(jdim,kdim,4),
     |    psi(jdim, kdim, ndim), psina, psi_dRdG(jdim, kdim, 2)

c     Local variables
      integer j, k, n, dir
c         The partial differentiation at a single node is indexed as:
c             dR(j, k, n)/dG
c         so j, k, and n identify the row of the Jacobian matrix.
c         dir (1-2) is the index within a block of G (direction) or
c         psi_dRdG.
      double precision pressHat(jdim,kdim)  ! pressure, including the
c         metric jacobian.  i.e. p/J
      double precision epsilon(jdim,kdim), spect(jdim,kdim),
     |    junk(jdim,kdim)
c         epsilon: a parameter used in the artificial dissipation.
c         spect: spectral radius divided by the metric Jacobian.
c         junk: some space to pass to other subroutines.
      double precision fQ(jdim,kdim,4), fbfQ(jdim,kdim,4)
c         Forward difference of Q for 2nd order dissipation, and
c         forward-backward-forward difference of Q for 4th order.
      double precision ct(2), cn(2), d(2)  ! coefficients used for
c         differentiating the tangential and normal velocity components
c         for the boundary conditions.
      logical is_inflow  ! Used for evaluating far field boundary
c         conditions.  True for inflow, false for outflow.
      double precision c_cc, c_cn, c_cm  ! Variables for differentiating
c         the residual equation that is added if lift is constrained in
c         the flow solver:

c     Initialize psi_dRdG
      do dir = 1, 2
        do k = 1, kdim
          do j = 1, jdim
            psi_dRdG(j, k, dir) = 0.d0
          end do
        end do
      end do

c     Add inviscid fluxs (interior nodes)
      if (iord .eq. 2) then
c       Second order spatial differencing
        do k = klow, kup
          do j = jlow, jup
c           Add inviscid flux: psi^T * d/dG (dE/d xi)
            call add_psi_dEdG(jdim, kdim, ndim, q, press,
     |          j, k, j+1, k, 0.5d0, psi, psi_dRdG)
            call add_psi_dEdG(jdim, kdim, ndim, q, press,
     |          j, k, j-1, k, -0.5d0, psi, psi_dRdG)

c           Add inviscid flux: psi^T * d/dG (dF/d eta)
            call add_psi_dFdG(jdim, kdim, ndim, q, press,
     |          j, k, j, k+1, 0.5d0, psi, psi_dRdG)
            call add_psi_dFdG(jdim, kdim, ndim, q, press,
     |          j, k, j, k-1, -0.5d0, psi, psi_dRdG)
          end do
        end do
      elseif (iord .eq. 4) then
c       Fourth order spatial differencing
        write (*,*) 'Fourth order not implemented!'
      endif

c     Compute the pressure with the metric jacobian included (pressHat)
c     (For artificial dissipation)
      do k = kbegin, kend
        do j = jbegin, jend
          pressHat(j,k) = press(j,k)/xyj(j,k)
        end do
      end do

c     Compute the coefficient epsilon (for second order dissipation)
c     (For xi-direction artificial dissipation)
      if (dis2x .ne. 0.d0) then
        call gradcoef(1, jdim, kdim, pressHat, xyj, epsilon, junk)
        do k = kbegin, kend
          do j = jbegin, jend
            epsilon(j,k) = epsilon(j,k)*dis2x
          end do
        end do
      else
c       Bypass if there is no second-order dissipation
        do k = kbegin, kend
          do j = jbegin, jend
            epsilon(j,k) = 0.d0
          end do
        end do
      end if

c     Compute the spectral radius / metric Jacobian
c     (For xi-direction artificial dissipation)
      do k = kbegin, kend
        do j = jbegin, jend
          spect(j,k) = (dabs((xy(j,k,1)*Q(j,k,2)+xy(j,k,2)*Q(j,k,3))
     |        /Q(j,k,1)) + sndsp(j,k)*dsqrt(xy(j,k,1)**2+xy(j,k,2)**2))
     |        /xyj(j,k)
        end do
      end do

c     Evaluate 1st-order forward difference on Q to form fQ
c     (For xi-direction artificial dissipation)
      do n = 1,4
        do k = klow, kup
          do j = jbegin, jup
            fQ(j,k,n) = q(j+1,k,n) - q(j,k,n)
          end do
          fQ(jend,k,n) = fQ(jup,k,n)
        end do
      end do

c     Evaluate centered-difference on fQ to form fbfQ (see filterx.f)
c     (For xi-direction artificial dissipation)
      do n = 1,4
        do k = klow, kup
          fbfQ(jbegin,k,n) = q(jlow+1,k,n) - 2.d0*q(jlow,k,n)
     |        + q(jbegin,k,n)
          do j = jlow, jup
            fbfQ(j,k,n) = fQ(j+1,k,n) - 2.d0*fQ(j,k,n) + fQ(j-1,k,n)
          end do
          fbfQ(jend,k,n) = 0.d0
        end do
      end do

c     Add artificial dissipation (interior nodes, xi-direction)
      do k = klow, kup
        do j = jlow, jup
c         In averaging the value of the dissipation coefficients (d) to
c         obtain the value at the half nodes, and applying the backwards
c         difference, 4 terms result.
          call add_psi_ADterm_xi(jdim, kdim, j, k, j, k, j+1, k,
     |        1.d0, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj, sndsp,
     |        psi_dRdG)
          call add_psi_ADterm_xi(jdim, kdim, j, k, j, k, j, k,
     |        1.d0, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj, sndsp,
     |        psi_dRdG)
          call add_psi_ADterm_xi(jdim, kdim, j, k, j-1, k, j, k,
     |        -1.d0, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj, sndsp,
     |        psi_dRdG)
          call add_psi_ADterm_xi(jdim, kdim, j, k, j-1, k, j-1, k,
     |        -1.d0, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj, sndsp,
     |        psi_dRdG)
        end do
      end do

c     Compute the coefficient epsilon (for second order dissipation)
c     (For eta-direction artificial dissipation)
      if (dis2y .ne. 0.d0) then
        call gradcoef(2, jdim, kdim, pressHat, xyj, epsilon, junk)
        do k = kbegin, kend
          do j = jbegin, jend
            epsilon(j,k) = epsilon(j,k)*dis2y
          end do
        end do
      else
c       Bypass if there is no second-order dissipation
        do k = kbegin, kend
          do j = jbegin, jend
            epsilon(j,k) = 0.d0
          end do
        end do
      end if

c     Compute the spectral radius / metric Jacobian
c     (For eta-direction artificial dissipation)
      do k = kbegin, kend
        do j = jbegin, jend
          spect(j,k) = (dabs((xy(j,k,3)*Q(j,k,2)+xy(j,k,4)*Q(j,k,3))
     |        /Q(j,k,1)) + sndsp(j,k)*dsqrt(xy(j,k,3)**2+xy(j,k,4)**2))
     |        /xyj(j,k)
        end do
      end do

c     Evaluate 1st-order forward difference on Q to form fQ
c     (For eta-direction artificial dissipation)
      do n = 1, 4
        do k = kbegin,kup
          do j = jlow,jup
            fQ(j,k,n) = q(j,k+1,n) - q(j,k,n)
          end do
        end do
      end do

c     Evaluate centered-difference on fQ to form fbfQ (see filtery.f)
c     (For eta-direction artificial dissipation)
      do n = 1, 4
        do j = jlow,jup
          fbfQ(j,kbegin,n) = q(j,klow+1,n) - 2.d0*q(j,klow,n) 
     |        + q(j,kbegin,n)
          do k = klow,kup-1
            fbfQ(j,k,n) = fQ(j,k+1,n) - 2.d0*fQ(j,k,n) + fQ(j,k-1,n)
          end do
          fbfQ(j,kup,n) = fQ(j,kup-1,n) - fQ(j,kup,n)
        end do
      end do

c     Add artificial dissipation (interior nodes, eta-direction)
      do k = klow, kup
        do j = jlow, jup
c         In averaging the value of the dissipation coefficients (d) to
c         obtain the value at the half nodes, and applying the backwards
c         difference, 4 terms result:
          call add_psi_ADterm_eta(jdim, kdim, j, k, j, k, j, k+1,
     |        1.d0, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj, sndsp,
     |        psi_dRdG)
          call add_psi_ADterm_eta(jdim, kdim, j, k, j, k, j, k,
     |        1.d0, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj, sndsp,
     |        psi_dRdG)
          call add_psi_ADterm_eta(jdim, kdim, j, k, j, k-1, j, k,
     |        -1.d0, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj, sndsp,
     |        psi_dRdG)
          call add_psi_ADterm_eta(jdim, kdim, j, k, j, k-1,j, k-1,
     |        -1.d0, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj, sndsp,
     |        psi_dRdG)
        end do
      end do

c     Add viscous fluxes (interior nodes)
      if (viscous) then
        do k = klow, kup
          do j = jlow, jup
            call add_psi_dSdG(jdim, kdim, j, k, j, k, j, k+1,
     |          1.d0, .true., .true., psi, Q, sndsp, fmu, turmu,
     |          xy, xyj, psi_dRdG)
            call add_psi_dSdG(jdim, kdim, j, k, j, k, j, k,
     |          1.d0, .false., .true., psi, Q, sndsp, fmu, turmu,
     |          xy, xyj, psi_dRdG)
            call add_psi_dSdG(jdim, kdim, j, k, j, k-1, j, k,
     |          -1.d0, .true., .false., psi, Q, sndsp, fmu, turmu,
     |          xy, xyj, psi_dRdG)
            call add_psi_dSdG(jdim, kdim, j, k, j, k-1, j, k-1,
     |          -1.d0, .false., .true., psi, Q, sndsp, fmu, turmu,
     |          xy, xyj, psi_dRdG)
          end do
        end do
      endif

c     Add turbulence modelling terms (interior nodes)
      if (viscous .and. turbulnt .and. itmodel == 2) then
        call add_psi_dSAdG(jdim, kdim, Q, fmu, x, y,
     |      xy, xyj, psi, psi_dRdG)
      endif

c     Add boundary condition terms: inflow/outflow far field boundary
      do j = jlow, jup
c       Add terms for dVn(kend,j)/dG and dVt(kend,j)/dG
        k = kend
        is_inflow = .true.  ! so that it is computed in get_dVdG_coefs
        call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |      j, k, j, k, .true., .true., 0, is_inflow, ct, cn)
        d(1) =   psi(j,k,1)*cn(1)
     |         + psi(j,k,2)*cn(1)
     |         + psi(j,k,4)*ct(1)
        d(2) =   psi(j,k,1)*cn(2)
     |         + psi(j,k,2)*cn(2)
     |         + psi(j,k,4)*ct(2)
        call add_delta_xi_I(jdim, kdim, j, k, d, psi_dRdG)

        if (is_inflow) then
c         Add terms for dVn(freestream)/dG and dVt(freestream)/dG.
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        0, 0, j, k, .true., .true., 0, .false., ct, cn)
          d(1) = - psi(j,k,1)*cn(1)
     |           - psi(j,k,4)*ct(1)
          d(2) = - psi(j,k,1)*cn(2)
     |           - psi(j,k,4)*ct(2)
          call add_delta_xi_I(jdim, kdim, j, k, d, psi_dRdG)

c         Add terms for dVn(kup,j)/dG and dVt(kup,j)/dG.
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        j, k-1, j, k, .false., .true., 0, .false., ct, cn)
          d(1) = - psi(j,k,2)*cn(1)
          d(2) = - psi(j,k,2)*cn(2)
          call add_delta_xi_I(jdim, kdim, j, k, d, psi_dRdG)
        else  ! Outflow
c         Add terms for dVn(freestream)/dG and dVt(freestream)/dG.
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        0, 0, j, k, .false., .true., 0, .false., ct, cn)
          d(1) = - psi(j,k,1)*cn(1)
          d(2) = - psi(j,k,1)*cn(2)
          call add_delta_xi_I(jdim, kdim, j, k, d, psi_dRdG)

c         Add terms for dVn(kup,j)/dG and dVt(kup,j)/dG.
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        j, k-1, j, k, .true., .true., 0, .false., ct, cn)
          d(1) = - psi(j,k,2)*cn(1)
     |           - psi(j,k,4)*ct(1)
          d(2) = - psi(j,k,2)*cn(2)
     |           - psi(j,k,4)*ct(2)
          call add_delta_xi_I(jdim, kdim, j, k, d, psi_dRdG)
        endif
      end do

c     Add boundary condition terms: downstream outflow bondary
      if (.not. viscous) then  ! dR(j,k)/dG = 0  for a viscous analysis
c       Boundary downstream and below airfoil: j = jbegin
        do k = kbegin, kend
c         Add terms for dVn(k,jbegin)/dG and dVt(k,jbegin)/dG
          j = jbegin
          is_inflow = .true.  ! so that it is computed in get_dVdG_coefs
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        j, k, j, k, .true., .true., 1, is_inflow, ct, cn)
          d(1) =   psi(j,k,1)*cn(1)
     |           + psi(j,k,2)*cn(1)
     |           + psi(j,k,4)*ct(1)
          d(2) =   psi(j,k,1)*cn(2)
     |           + psi(j,k,2)*cn(2)
     |           + psi(j,k,4)*ct(2)
          call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)

          if (is_inflow) then
c           Add terms for dVn(freestream)/dG and dVt(freestream)/dG.
            call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |          0, 0, j, k, .true., .true., 1, .false., ct, cn)
            d(1) = - psi(j,k,1)*cn(1)
     |             - psi(j,k,4)*ct(1)
            d(2) = - psi(j,k,1)*cn(2)
     |             - psi(j,k,4)*ct(2)
            call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)

c           Add terms for dVn(k,jlow)/dG and dVt(k,jlow)/dG.
            call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |          j+1, k, j, k, .false., .true., 1, .false., ct, cn)
            d(1) = - psi(j,k,2)*cn(1)
            d(2) = - psi(j,k,2)*cn(2)
            call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)
          else  ! Outflow
c           Add terms for dVn(freestream)/dG and dVt(freestream)/dG.
            call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |          0, 0, j, k, .false., .true., 1, .false., ct, cn)
            d(1) = - psi(j,k,1)*cn(1)
            d(2) = - psi(j,k,1)*cn(2)
            call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)

c           Add terms for dVn(k,jlow)/dG and dVt(k,jlow)/dG.
            call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |          j+1, k, j, k, .true., .true., 1, .false., ct, cn)
            d(1) = - psi(j,k,2)*cn(1)
     |             - psi(j,k,4)*ct(1)
            d(2) = - psi(j,k,2)*cn(2)
     |             - psi(j,k,4)*ct(2)
            call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)
          endif
        end do

c       Boundary downstream and above airfoil: j = jend
        do k = kbegin, kend
c         Add terms for dVn(k,jend)/dG and dVt(k,jend)/dG
          j = jend
          is_inflow = .true.  ! so that it is computed in get_dVdG_coefs
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        j, k, j, k, .true., .true., 2, is_inflow, ct, cn)
          d(1) =   psi(j,k,1)*cn(1)
     |           + psi(j,k,2)*cn(1)
     |           + psi(j,k,4)*ct(1)
          d(2) =   psi(j,k,1)*cn(2)
     |           + psi(j,k,2)*cn(2)
     |           + psi(j,k,4)*ct(2)
          call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)

          if (is_inflow) then
c           Add terms for dVn(freestream)/dG and dVt(freestream)/dG.
            call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |          0, 0, j, k, .true., .true., 2, .false., ct, cn)
            d(1) = - psi(j,k,1)*cn(1)
     |             - psi(j,k,4)*ct(1)
            d(2) = - psi(j,k,1)*cn(2)
     |             - psi(j,k,4)*ct(2)
            call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)

c           Add terms for dVn(k,jup)/dG and dVt(k,jup)/dG.
            call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |          j-1, k, j, k, .false., .true., 2, .false., ct, cn)
            d(1) = - psi(j,k,2)*cn(1)
            d(2) = - psi(j,k,2)*cn(2)
            call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)
          else  ! Outflow
c           Add terms for dVn(freestream)/dG and dVt(freestream)/dG.
            call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |          0, 0, j, k, .false., .true., 2, .false., ct, cn)
            d(1) = - psi(j,k,1)*cn(1)
            d(2) = - psi(j,k,1)*cn(2)
            call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)

c           Add terms for dVn(k,jup)/dG and dVt(k,jup)/dG.
            call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |          j-1, k, j, k, .true., .true., 2, .false., ct, cn)
            d(1) = - psi(j,k,2)*cn(1)
     |             - psi(j,k,4)*ct(1)
            d(2) = - psi(j,k,2)*cn(2)
     |             - psi(j,k,4)*ct(2)
            call add_delta_eta_I(jdim, kdim, j, k, d, psi_dRdG)
          endif
        end do
      endif

c     Add boundary condition terms: airfoil boundary
c     For each j on the inviscid airfoil boundary, R(1,j) depends on
c     G(1,j), G(2,j), and G(3,j).
      if (.not. viscous) then  ! dR(j,k)/dG = 0  for a viscous analysis
        k = kbegin
        do j = jtail1, jtail2
c         Add dR(1,j)/dG(1,j)
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        j, k, j, k, .true., .true., 0, .false., ct, cn)
          d(1) =   psi(j,k,1)*cn(1)
     |           + psi(j,k,2)*ct(1)
          d(2) =   psi(j,k,1)*cn(2)
     |           + psi(j,k,2)*ct(2)
          call add_delta_xi_I(jdim, kdim, j, k, d, psi_dRdG)

c         Add dR(1,j)/dG(2,j)
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        j, k+1, j, k+1, .true., .false., 0, .false., ct, cn)
          d(1) = -2.d0 * psi(j,k,2)*ct(1)
          d(2) = -2.d0 * psi(j,k,2)*ct(2)
          call add_delta_xi_I(jdim, kdim, j, k+1, d, psi_dRdG)

c         Add dR(1,j)/dG(3,j)
          call get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |        j, k+2, j, k+2, .true., .false., 0, .false., ct, cn)
          d(1) = psi(j,k,2)*ct(1)
          d(2) = psi(j,k,2)*ct(2)
          call add_delta_xi_I(jdim, kdim, j, k+2, d, psi_dRdG)
        end do
      endif

c     Add boundary condition terms: wake cut
c     dR(j,k)/dG = 0  for these (j,k)

c     Add term for the residual equation that is added if lift is
c     constrained in the flow solver:
c       R_na = Cl - Cl*
c       d(R_na)/dG = d(R_na)/dG = d(Cl)/dG
      if (clopt) then
        c_cc = -psina * sin(alpha*pi/180.d0)
        c_cn = psina * cos(alpha*pi/180.d0)
        c_cm = 0.d0
        call add_dcdG(jdim, kdim, press, Q, xy, xyj, x, y, c_cc, c_cn,
     |      c_cm, psi_dRdG)
      endif

      end
c---- get_psi_dRdG -----------------------------------------------------

c---- add_psi_dEdG -----------------------------------------------------
c adds
c     c * psi(jpsi,kpsi) * dE(jE,kE)/dG
c     (d => partial differentiation)
c to a
c dE/dG is computed via the grid metrics, and follows the residual
c calculation used in PROBE.
c
c Intended for use in the compuation of the objective function gradient.
c
c Does not include viscous terms.  Does not support calculation of J,
c the metric Jacobian, using cell areas.  Supports only a c-mesh.
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c ndim: number of governing equations
c q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c press: pressure (without the inverse of the metric jacobian included)
c jpsi, kpsi: indices to be used for psi
c jE, kE: indices of the flux E to be differentiated with respect to G
c c: a constant
c psi: adjoint vector
c
c Returns:
c a: a with flux derivative added to it
c
c Chad Oldfield, May 2005.
c-----------------------------------------------------------------------
      subroutine add_psi_dEdG(jdim, kdim, ndim, q, press,
     |    jpsi, kpsi, jE, kE, c, psi, a)
      implicit none

#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim, ndim
      double precision q(jdim,kdim,ndim), press(jdim,kdim)
      integer jpsi, kpsi, jE, kE
      double precision c, psi(jdim, kdim, ndim), a(jdim, kdim, 2)

c     Local variables
      double precision d(2)  ! c*psi*dE/dG, without the spatial
c         operators del_eta
      double precision rho_u, rho_v, u, v, p, e  ! Primative variables
c         rho_u = rho*u, and rho_v = rho*v

c     Compute primative variabes
      rho_u = q(jE,kE,2)
      rho_v = q(jE,kE,3)
      u = q(jE,kE,2)/q(jE,kE,1)
      v = q(jE,kE,3)/q(jE,kE,1)
      p = press(jE,kE)
      e = q(jE,kE,4)

c     Compute d = c*psi*dE/dG, without the spatial operators del_eta
      d(1) = c*(- psi(jpsi,kpsi,1)*rho_v
     |          - psi(jpsi,kpsi,2)*rho_v*u
     |          - psi(jpsi,kpsi,3)*(rho_v*v+p)
     |          - psi(jpsi,kpsi,4)*v*(e+p)    )
      d(2) = c*(  psi(jpsi,kpsi,1)*rho_u
     |          + psi(jpsi,kpsi,2)*(rho_u*u+p)
     |          + psi(jpsi,kpsi,3)*rho_u*v
     |          + psi(jpsi,kpsi,4)*u*(e+p)    )
      call add_delta_eta_I(jdim, kdim, jE, kE, d, a)
      end
c---- add_psi_dEdG -----------------------------------------------------

c---- add_psi_dFdG -----------------------------------------------------
c adds
c     c * psi(jpsi,kpsi) * dF(jF,kF)/dG
c     (d => partial differentiation)
c to a
c dF/dG is computed via the grid metrics, and follows the residual
c calculation used in PROBE.
c
c Intended for use in the compuation of the objective function gradient.
c
c Does not support calculation of J, the metric Jacobian, using cell
c areas.  Supports only a c-mesh.
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c ndim: number of governing equations
c q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c press: pressure (without the inverse of the metric jacobian included)
c jpsi, kpsi: indices to be used for psi
c jF, kF: indices of the flux F to be differentiated with respect to G
c c: a constant
c psi: adjoint vector
c
c Returns:
c a: a with flux derivative added to it
c
c Chad Oldfield, May 2005.
c-----------------------------------------------------------------------
      subroutine add_psi_dFdG(jdim, kdim, ndim, q, press,
     |    jpsi, kpsi, jF, kF, c, psi, a)
      implicit none

#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim, ndim
      double precision q(jdim,kdim,ndim), press(jdim,kdim)
      integer jpsi, kpsi, jF, kF
      double precision c, psi(jdim, kdim, ndim), a(jdim, kdim, 2)

c     Local variables
      double precision d(2)  ! c*psi*dF/dG, without the spatial
c         operators del_xi
      double precision rho_u, rho_v, u, v, p, e  ! Primative variables
c         rho_u = rho*u, and rho_v = rho*v

c     Compute primative variabes
      rho_u = q(jF,kF,2)
      rho_v = q(jF,kF,3)
      u = q(jF,kF,2)/q(jF,kF,1)
      v = q(jF,kF,3)/q(jF,kF,1)
      p = press(jF,kF)
      e = q(jF,kF,4)

c     Compute d = c*psi*dF/dG, without the spatial operators del_xi
      d(1) = c*(  psi(jpsi,kpsi,1)*rho_v
     |             + psi(jpsi,kpsi,2)*rho_v*u
     |             + psi(jpsi,kpsi,3)*(rho_v*v+p)
     |             + psi(jpsi,kpsi,4)*v*(e+p)    )
      d(2) = c*(- psi(jpsi,kpsi,1)*rho_u
     |             - psi(jpsi,kpsi,2)*(rho_u*u+p)
     |             - psi(jpsi,kpsi,3)*rho_u*v
     |             - psi(jpsi,kpsi,4)*u*(e+p)    )
      call add_delta_xi_I(jdim, kdim, jF, kF, d, a)
      end
c---- add_psi_dFdG -----------------------------------------------------

c---- add_psi_ADterm_xi ------------------------------------------------
c Adds a term of
c   psi * d(artificial dissipation)/d(G)
c to psi_dRdG, where AD is in the xi-direction.
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c ndim: number of governing equations
c jpsi, kpsi:j and k indices for psi
c jfQ, kfQ:  j and k indices for fQ and fbfQ
c jd, kd:    j and k indices for d
c c_bd: the coefficient in the backwards difference stencil
c   that is applied to the artificial disspation coefficient.
c psi: adjoint vector
c Q: conserved variables (not Q hat)
c fQ: forward difference of conserved variables
c fbfQ: forward-backward-forward difference of conserved variables
c epsilon: second-order artificial dissipation coefficients.
c spect: spectral radius in xi-direction, divided by metric Jacobain
c xy, xyj: metrics, and metric jacobian
c sndsp: speed of sound
c
c Returns:
c psi_dRdG: with the dissipation term added
c
c Chad Oldfield, May 2005.
c-----------------------------------------------------------------------
      subroutine add_psi_ADterm_xi(jdim, kdim, jpsi, kpsi, jfQ, kfQ,
     |    jd, kd, c_bd, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj,
     |    sndsp, psi_dRdG)
      implicit none

#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim, jpsi, kpsi, jfQ, kfQ, jd, kd
      double precision c_bd, psi(jdim,kdim,4), Q(jdim,kdim,4),
     |    fQ(jdim,kdim,4), fbfQ(jdim,kdim,4), epsilon(jdim,kdim),
     |    spect(jdim,kdim), xy(jdim,kdim,4), xyj(jdim,kdim),
     |    sndsp(jdim,kdim), psi_dRdG(jdim,kdim,2)

c     Local variables
      double precision psi_fQ, psi_fbfQ  ! The product of the
c         appropriate elements of psi and fQ or fbfQ.
      double precision x_eta, y_eta, u, v, coef(2)  ! Metrics, velocity
c         components.  coef is successively altered until it is finally
c         a constant that is to be multiplied by delta_eta(I) to give
c         the derivative of the artificial dissipation term.

c     Compute psi_fQ and psi_fbfQ
      psi_fQ = psi(jpsi,kpsi,1) * fQ(jfQ,kfQ,1)
     |       + psi(jpsi,kpsi,2) * fQ(jfQ,kfQ,2)
     |       + psi(jpsi,kpsi,3) * fQ(jfQ,kfQ,3)
     |       + psi(jpsi,kpsi,4) * fQ(jfQ,kfQ,4)
      psi_fbfQ = psi(jpsi,kpsi,1) * fbfQ(jfQ,kfQ,1)
     |         + psi(jpsi,kpsi,2) * fbfQ(jfQ,kfQ,2)
     |         + psi(jpsi,kpsi,3) * fbfQ(jfQ,kfQ,3)
     |         + psi(jpsi,kpsi,4) * fbfQ(jfQ,kfQ,4)

c     d(d)/dG: compute coef = d(sigma*Jinv)/dG coefficient.  i.e. coef
c     must to be multiplied by delta_eta(I) to become d(sigma*Jinv)/dG.
c     The two elements of coef here are for d/dG1 and d/dG2.
      u = Q(jd,kd,2) / Q(jd,kd,1)
      v = Q(jd,kd,3) / Q(jd,kd,1)
      x_eta = -xy(jd,kd,2) / xyj(jd,kd)
      y_eta = xy(jd,kd,1) / xyj(jd,kd)
      coef(1) = sndsp(jd,kd) * (y_eta**2 + x_eta**2)**-0.5d0
      coef(2) = y_eta*coef(1)
      coef(1) = x_eta*coef(1)
      if ((kd .gt. kbegin) .or. (jd .lt. jtail1)
     |    .or. (jd .gt. jtail2) .or. .not. viscous) then
c       It is not the airfoil boundary, where Vt = Vn = 0 and so U = 0
c       (or the analysis is inviscid, so U is never set to 0)
        if ((y_eta*u) .ge. (x_eta*v)) then
c         U >= 0, so |U| = U
          coef(1) = -v + coef(1)
          coef(2) = u + coef(2)
        else
c         U < 0, so |U| = -U
          coef(1) = v + coef(1)
          coef(2) = -u + coef(2)
        endif
      endif

c     d(d)/dG: modify coef to be the coefficient for
c     d(-b_xi(Ad_xi)) / dG.  (b_xi refers to a backwards difference
c     operator in the xi-direction)  coef must be multiplied by
c     delta_eta(I) to complete the differentiation.
      if (dis4x*(spect(jfQ+1,kfQ) + spect(jfQ,kfQ)) .gt. 
     |    (epsilon(jfQ+1,kfQ)*spect(jfQ+1,kfQ) +
     |    epsilon(jfQ,kfQ)*spect(jfQ,kfQ))) then
c       4th order disspiation is active.
        coef(1) = -c_bd*coef(1) * 
     |      (psi_fQ*epsilon(jd,kd) - psi_fbfQ*(dis4x - epsilon(jd,kd)))
        coef(2) = -c_bd*coef(2) * 
     |      (psi_fQ*epsilon(jd,kd) - psi_fbfQ*(dis4x - epsilon(jd,kd)))
      else
c       4th order disspiation is inactive.
        coef(1) = -c_bd*coef(1) * (psi_fQ*epsilon(jd,kd))
        coef(2) = -c_bd*coef(2) * (psi_fQ*epsilon(jd,kd))
      endif
      
c     Multiply coef by delta_eta(I) and add to psi_dRdG
      call add_delta_eta_I(jdim, kdim, jd, kd, coef, psi_dRdG)

      end
c---- add_psi_ADterm_xi ------------------------------------------------

c---- add_psi_ADterm_eta -----------------------------------------------
c Adds a term of
c   psi * d(artificial dissipation)/d(G)
c to psi_dRdG, where AD is in the eta-direction.
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c ndim: number of governing equations
c jpsi, kpsi:j and k indices for psi
c jfQ, kfQ:  j and k indices for fQ and fbfQ
c jd, kd:    j and k indices for d
c c_bd: the coefficient in the backwards difference stencil
c   that is applied to the artificial disspation coefficient.
c psi: adjoint vector
c Q: conserved variables (not Q hat)
c fQ: forward difference of conserved variables
c fbfQ: forward-backward-forward difference of conserved variables
c epsilon: second-order artificial dissipation coefficients.
c spect: spectral radius in eta-direction, divided by metric Jacobain
c xy, xyj: metrics, and metric jacobian
c sndsp: speed of sound
c
c Returns:
c psi_dRdG: with the dissipation term added
c
c Chad Oldfield, May 2005.
c-----------------------------------------------------------------------
      subroutine add_psi_ADterm_eta(jdim, kdim, jpsi, kpsi, jfQ, kfQ,
     |    jd, kd, c_bd, psi, Q, fQ, fbfQ, epsilon, spect, xy, xyj,
     |    sndsp, psi_dRdG)
      implicit none

#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim, jpsi, kpsi, jfQ, kfQ, jd, kd
      double precision c_bd, psi(jdim,kdim,4), Q(jdim,kdim,4),
     |    fQ(jdim,kdim,4), fbfQ(jdim,kdim,4), epsilon(jdim,kdim),
     |    spect(jdim,kdim), xy(jdim,kdim,4), xyj(jdim,kdim),
     |    sndsp(jdim,kdim), psi_dRdG(jdim,kdim,2)

c     Local variables
      double precision psi_fQ, psi_fbfQ  ! The product of the
c         appropriate elements of psi and fQ or fbfQ.
      double precision x_xi, y_xi, u, v, coef(2)  ! Metrics, velocity
c         components.  coef is successively altered until it is finally
c         a constant that is to be multiplied by delta_xi(I) to give
c         the derivative of the artificial dissipation term.


c     Compute psi_fQ and psi_fbfQ
      psi_fQ = psi(jpsi,kpsi,1) * fQ(jfQ,kfQ,1)
     |       + psi(jpsi,kpsi,2) * fQ(jfQ,kfQ,2)
     |       + psi(jpsi,kpsi,3) * fQ(jfQ,kfQ,3)
     |       + psi(jpsi,kpsi,4) * fQ(jfQ,kfQ,4)
      psi_fbfQ = psi(jpsi,kpsi,1) * fbfQ(jfQ,kfQ,1)
     |         + psi(jpsi,kpsi,2) * fbfQ(jfQ,kfQ,2)
     |         + psi(jpsi,kpsi,3) * fbfQ(jfQ,kfQ,3)
     |         + psi(jpsi,kpsi,4) * fbfQ(jfQ,kfQ,4)

c     d(d)/dG: compute coef = d(sigma*Jinv)/dG coefficient.  i.e. coef
c     must to be multiplied by delta_xi(I) to become d(sigma*Jinv)/dG.
c     The two elements of coef here are for d/dG1 and d/dG2.
      u = Q(jd,kd,2) / Q(jd,kd,1)
      v = Q(jd,kd,3) / Q(jd,kd,1)
      x_xi = xy(jd,kd,4) / xyj(jd,kd)
      y_xi = -xy(jd,kd,3) / xyj(jd,kd)
      coef(1) = sndsp(jd,kd) * (x_xi**2 + y_xi**2)**-0.5d0
      coef(2) = y_xi*coef(1)
      coef(1) = x_xi*coef(1)
      if ((kd .gt. kbegin) .or. (jd .lt. jtail1)
     |    .or. (jd .gt. jtail2)) then
c       It is not the airfoil boundary, where Vt = 0 and so V = 0
        if ((x_xi*v) .ge. (y_xi*u)) then
c         V >= 0, so |V| = V
          coef(1) = v + coef(1)
          coef(2) = -u + coef(2)
        else
c         V < 0, so |V| = -V
          coef(1) = -v + coef(1)
          coef(2) = u + coef(2)
        endif
      endif

c     d(d)/dG: modify coef to be the coefficient for
c     d(-b_eta(Ad_xi)) / dG.  (b_eta refers to a backwards difference
c     operator in the eta-direction)  coef must be multiplied by
c     delta_xi(I) to complete the differentiation.
      if (dis4y*(spect(jfQ,kfQ+1) + spect(jfQ,kfQ)) .gt. 
     |    (epsilon(jfQ,kfQ+1)*spect(jfQ,kfQ+1) +
     |    epsilon(jfQ,kfQ)*spect(jfQ,kfQ))) then
c       4th order disspiation is active.
        coef(1) = -c_bd*coef(1) * 
     |      (psi_fQ*epsilon(jd,kd) - psi_fbfQ*(dis4y - epsilon(jd,kd)))
        coef(2) = -c_bd*coef(2) * 
     |      (psi_fQ*epsilon(jd,kd) - psi_fbfQ*(dis4y - epsilon(jd,kd)))
      else
c       4th order disspiation is inactive.
        coef(1) = -c_bd*coef(1) * (psi_fQ*epsilon(jd,kd))
        coef(2) = -c_bd*coef(2) * (psi_fQ*epsilon(jd,kd))
      endif

c     Multiply coef by delta_xi(I) and add to psi_dRdG
      call add_delta_xi_I(jdim, kdim, jd, kd, coef, psi_dRdG)

      end
c---- add_psi_ADterm_eta -----------------------------------------------

c---- add_psi_dSdG -----------------------------------------------------
c Adds a term of the derivative of the viscous terms (in the eta-
c direction) to psi_dRdG:
c   psi * d/dG ( d(S_hat)/d(eta) )
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c jpsi, kpsi: j and k indices for psi
c jc, kc:     j and k indices for the c_ coefficients (see local vars)
c jd, kd:     j and k indices for the d_ coefficients (see local vars)
c c_bd: the coefficient in the backwards difference stencil
c   that is applied to the viscous term.
c need_c:  True if jc or kc are different than they were the
c     previous time this subroutine was called.  This controls whether
c     or not the c_ coefficients (i.e. c_Jxx) are re-computed.
c need_d:  True if jd or kd are different than they were the
c     previous time this subroutine was called.  This controls whether
c     or not the d_ coefficients (i.e. d_Jxx_xi) are re-computed.
c psi: adjoint vector
c Q: conserved variables (not Q hat)
c sndsp: speed of sound
c fmu, turmu: molecular viscosity, turbulence viscosity (turmu is
c   evaluated at half nodes.  i.e. turmu(j,k) corresponds to the
c   turbulence viscosity at (j, k+1/2).
c xy, xyj: metrics, and metric jacobian
c 
c Returns:
c psi_dRdG: with the dissipation term added
c
c Chad Oldfield, August 2005.
c-----------------------------------------------------------------------
      subroutine add_psi_dSdG(jdim, kdim, jpsi, kpsi, jc, kc, jd, kd,
     |    c_bd, need_c, need_d, psi, Q, sndsp, fmu, turmu, xy, xyj,
     |    psi_dRdG)
      implicit none

#include "../include/arcom.inc"
#include "../include/visc.inc"
c visc.inc provides f34, f13, prlinv, & prtinv.

c     Argument data types; see above for descriptions
      integer jdim, kdim, jpsi, kpsi, jc, kc, jd, kd
      double precision c_bd
      logical need_c, need_d
      double precision psi(jdim,kdim,4), Q(jdim,kdim,4),
     |    sndsp(jdim,kdim), fmu(jdim,kdim), turmu(jdim,kdim),
     |    xy(jdim,kdim,4), xyj(jdim,kdim), psi_dRdG(jdim,kdim,2)

c     Local variables
      integer i
      double precision xi_x, xi_y, eta_x, eta_y, J, Jinv  ! Metrics
      double precision x_xi, y_xi, x_eta, y_eta  ! Metrics
      double precision, save :: d_Jxx_xi(2), d_Jxx_eta(2), d_Jxy_xi(2), 
     |    d_Jxy_eta(2), d_Jyy_xi(2), d_Jyy_eta(2)
c         These are the d_ coefficients for the derivatives of
c           J * eta_x * eta_x
c           J * eta_x * eta_y
c           J * eta_y * eta_y
c         with respect to G.  Each derivative is given by an expression
c         like (for the first one):
c           d_Jxx_xi * delta_xi(I) + d_Jxx_eta * delta_eta(I)
c         where delta_xi(I) is computed in add_delta_xi_I, and
c         delta_eta(I) is computed in add_delta_eta_I.  The two
c         components of these arrays correspond to derivatives in the
c         x- and y-directions.
      double precision d_xi(2), d_eta(2)
c         These are like d_Jxx_xi, etc., except they are coefficients
c         for the derivative of the viscous flux term.
      double precision, save :: c_Jxx(2:4), c_Jxy(2:4), c_Jyy(2:4)
c         These are the c_ coefficients that are multiplied by the d_
c         coefficients d_Jxx_xi, etc. to give d_xi and d_eta.  They
c         contain terms that are independant of whether the derivative
c         is with respect to the x- or the y-direction.  The indices
c         2:4 correspond to Q(j,k,2:4) and psi(j,k,2:4).
      double precision fu, fv  ! Forward difference of u and v
      double precision hu, hv  ! u and v evaluated at half node kc+1/2
      double precision temp

      if (need_d) then
c       Only compute d_ coefficients if they are different than the
c       last time this subroutine was called (to save time)

c       Compute metrics
        xi_x =  xy(jd,kd,1)
        xi_y =  xy(jd,kd,2)
        eta_x = xy(jd,kd,3)
        eta_y = xy(jd,kd,4)
        J =     xyj(jd,kd)
        Jinv = 1.d0/J
        x_xi = eta_y*Jinv
        y_xi = -eta_x*Jinv
        x_eta = -xi_y*Jinv
        y_eta = xi_x*Jinv

c       Compute d_ coefficients d_Jxx_xi, etc
        temp = eta_x**2
        d_Jxx_xi(1)  = -temp*y_eta
        d_Jxx_xi(2)  = temp*x_eta - 2.d0*eta_x
        d_Jxx_eta(1) = temp*y_xi
        d_Jxx_eta(2) = -temp*x_xi
        temp = eta_x*eta_y
        d_Jxy_xi(1)  = eta_x - temp*y_eta
        d_Jxy_xi(2)  = -eta_y + temp*x_eta
        d_Jxy_eta(1) = temp*y_xi
        d_Jxy_eta(2) = -temp*x_xi
        temp = eta_y**2
        d_Jyy_xi(1)  = 2.d0*eta_y - temp*y_eta
        d_Jyy_xi(2)  = temp*x_eta
        d_Jyy_eta(1) = temp*y_xi
        d_Jyy_eta(2) = -temp*x_xi
      endif

      if (need_c) then
c       Only compute c_ coefficients if they are different than the
c       last time this subroutine was called (to save time)

c       Evaluate forward difference of u and v
        fu = q(jc,kc+1,2)/q(jc,kc+1,1) - q(jc,kc,2)/q(jc,kc,1)
        fv = q(jc,kc+1,3)/q(jc,kc+1,1) - q(jc,kc,3)/q(jc,kc,1)

c       Evaluate u and v at half nodes
        hu = 0.5d0*(q(jc,kc+1,2)/q(jc,kc+1,1) + q(jc,kc,2)/q(jc,kc,1))
        hv = 0.5d0*(q(jc,kc+1,3)/q(jc,kc+1,1) + q(jc,kc,3)/q(jc,kc,1))

c       Evaluate viscosity at 1/2 nodes (turmu is already at 1/2 nodes),
c       and multiply by 0.5
        temp = -(0.5d0*(fmu(jc,kc+1) + fmu(jc,kc)) + turmu(jc,kc))/Re
     |      * 0.5d0 * c_bd

c       Compute c_ coefficients at (2) and (3)
        c_Jxx(2) = temp*fu*f43
        c_Jxy(2) = temp*fv*f13
        c_Jyy(2) = temp*fu

        c_Jxx(3) = temp*fv
        c_Jxy(3) = temp*fu*f13
        c_Jyy(3) = temp*fv*f43

c       Evaluate viscosity at 1/2 nodes (turmu is already at 1/2 nodes),
c       including the Prandtl numbers, division by (gamma - 1), forward
c       difference of speed of sound squared, and factor of 0.5.
        temp = -(0.5d0*(fmu(jc,kc+1) + fmu(jc,kc))*prlinv
     |          + turmu(jc,kc)*prtinv)/(Re*gami)
     |         * (sndsp(jc,kc+1)**2 - sndsp(jc,kc)**2) * 0.5d0 * c_bd
     
c       Compute c_ coefficients at (4)
        c_Jxx(4) = hu*c_Jxx(2) + hv*c_Jxx(3) + temp
        c_Jxy(4) = hu*c_Jxy(2) + hv*c_Jxy(3)
        c_Jyy(4) = hu*c_Jyy(2) + hv*c_Jyy(3) + temp

      endif

c     Compute d_xi and d_eta from the c_ and d_ coefficients
      d_xi(1) = 0.d0
      d_xi(2) = 0.d0
      d_eta(1) = 0.d0
      d_eta(2) = 0.d0
      do i = 2,4
        d_xi(1) = d_xi(1) + psi(jpsi,kpsi,i)
     |      *(  c_Jxx(i)*d_Jxx_xi(1)
     |        + c_Jxy(i)*d_Jxy_xi(1)
     |        + c_Jyy(i)*d_Jyy_xi(1))
        d_xi(2) = d_xi(2) + psi(jpsi,kpsi,i)
     |      *(  c_Jxx(i)*d_Jxx_xi(2)
     |        + c_Jxy(i)*d_Jxy_xi(2)
     |        + c_Jyy(i)*d_Jyy_xi(2))
        d_eta(1) = d_eta(1) + psi(jpsi,kpsi,i)
     |      *(  c_Jxx(i)*d_Jxx_eta(1)
     |        + c_Jxy(i)*d_Jxy_eta(1)
     |        + c_Jyy(i)*d_Jyy_eta(1))
        d_eta(2) = d_eta(2) + psi(jpsi,kpsi,i)
     |      *(  c_Jxx(i)*d_Jxx_eta(2)
     |        + c_Jxy(i)*d_Jxy_eta(2)
     |        + c_Jyy(i)*d_Jyy_eta(2))
      end do

c     Complete the differentiation: multiply out (d_xi * delta_xi(I))
c     and (d_eta * delta_eta(I)).
      call add_delta_xi_I(jdim, kdim, jd, kd, d_xi, psi_dRdG)
      call add_delta_eta_I(jdim, kdim, jd, kd, d_eta, psi_dRdG)

      end
c---- add_psi_dSdG -----------------------------------------------------

c---- get_dVdG_coefs ---------------------------------------------------
c The derivative of the tangential velocity with respect to the grid
c node locations, d(V_t(j,k))/dG, is given by
c   [ct(1) d/dG(del_xi G1), ct(2) d/dG(del_xi G2)]
c And the derivative of the normal velocity with respect to the grid
c node locations, d(V_n(j,k))/dG, is given by
c   [cn(1) d/dG(del_xi G1), cn(2) d/dG(del_xi G2)]
c This computes ct and cn so that add_delta_xi_I can be used to
c complete the computation of d(V_t(j,k))/dG and d(V_t(j,k))/dG later.
c
c Note:  The above is written for directions normal and tangential to
c boundaries aligned with the xi gridlines.  For boundaries aligned
c with eta gridlines (the two outtflow boundaries downstream of the
c airfoil), d/dG(del_xi ~) should be replaced with d/dG(del_eta ~).
c
c The normal and tangential directions are normal and tangential to
c lines of constant eta, in the physical coordinate frame.  That is,
c they are normal and tangential to the airfoil boundary, and to the
c far field boundary ahead and to the sides of the airfoil (but not
c the boundary downstream of the airfoil).
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c ndim: number of governing equations
c q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c xy, xyj: metrics, and metric jacobian
c j_Q, k_Q: indices of node at which to evaluate flow velocity
c   Set j_Q = 0 or k_Q = 0 to evaluate at freestream conditions.
c j_dir, k_dir: indices of node at which to evaluate the tangential
c   direction.
c need_tangential, need_normal: only computes ct or cn (respectively)
c   if true.
c boundary:  Indicates which c-mesh boundary the calculation is for.
c   This determines how the normal and tangential directions are
c   computed, and how is_inflow is selected.
c   0: airfoil boundary or inflow/outflow boundary upstream of airfoil
c   1: outflow boundary downstream and below airfoil (j = 1)
c   2: outflow boundary downstream and above airfoil (j = jend)
c is_inflow: if true, then the sign of the normal velocity is
c   evaluated to determine whether the flow is inflow or outflow.
c   Otherwize, this calculation is omitted.
c
c Returns:
c is_inflow: Retruns true if the flow at the node is into the domain
c   (inflow), and false if the flow at the node is out of the domain
c   (outflow).  Note that this value must initially be set to true for
c   this calculation to be made - othewize it is omitted.
c ct: the coefficients ct(1) and ct(2) to be used for the calculation of
c   d(V_t(j,k))/dG
c cn: the coefficients cn(1) and cn(2) to be used for the calculation of
c   d(V_n(j,k))/dG
c
c Chad Oldfield, June 2005.
c-----------------------------------------------------------------------
      subroutine get_dVdG_coefs (jdim, kdim, ndim, q, xy, xyj,
     |    j_Q, k_Q, j_dir, k_dir, need_tangential, need_normal,
     |    boundary, is_inflow, ct, cn)
      implicit none

#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim, ndim
      double precision q(jdim,kdim,ndim)
      double precision xyj(jdim,kdim), xy(jdim,kdim,4)
      integer j_Q, k_Q, j_dir, k_dir
      logical need_tangential, need_normal
      integer boundary
      logical is_inflow
      double precision ct(2), cn(2)

c     Local variables
      double precision x_etaxi, y_etaxi  ! Metrics.  These are
c         x_xi, y_xi if boundary = 0 (upstream boundary where xi is
c         constant), and they are x_eta, y_eta if boundary > 0
c         (downstream boundaries where eta is constant)
      double precision u, v  ! Cartesian velocity components
      double precision temp1, temp2  ! Temporary variables to avoid
c         repeated calculations

c     Preliminary calculations
      if (boundary .eq. 0) then
c       x_xi and y_xi
        x_etaxi = xy(j_dir,k_dir,4) / xyj(j_dir, k_dir)
        y_etaxi = -xy(j_dir,k_dir,3) / xyj(j_dir, k_dir)
      else
c       x_eta and y_eta
        x_etaxi = -xy(j_dir,k_dir,2) / xyj(j_dir, k_dir)
        y_etaxi = xy(j_dir,k_dir,1) / xyj(j_dir, k_dir)
      endif
      if ((j_Q .eq. 0) .or. (k_Q .eq. 0)) then
c       Evaluate at freestreem conditions
        u = uinf
        v = vinf
      else
c       Evaluate velocities at node (j_Q, k_Q)
        u = q(j_Q,k_Q,2) / q(j_Q,k_Q,1)
        v = q(j_Q,k_Q,3) / q(j_Q,k_Q,1)
      endif
      temp1 = (x_etaxi**2 + y_etaxi**2)**-0.5d0

c     Determine whether the flow is into or out of the domain:
c     Inflow when the normal velocity is negative (the normal direction
c     is positive out of the domain).  Note that when calclating Vn to
c     find its sign, temp1 is omitted, as temp1 > 0.
      if (is_inflow) then
        if (boundary .eq. 0) then
c         Upstream boundary
          is_inflow = ((x_etaxi*v - y_etaxi*u)) .le. 0.d0
        elseif (boundary .eq. 1) then
c         Downstream boundary, below airfoil (j = 1)
          is_inflow = ((y_etaxi*u - x_etaxi*v)) .gt. 0.d0
        elseif (boundary .eq. 2) then
c         Downstream boundary, above airfoil (j = jend)
          is_inflow = ((y_etaxi*u - x_etaxi*v)) .le. 0.d0
        endif
      endif

c     Compute coefficients for V_t
c     (This calculation is the same for all boundaries)
      if (need_tangential) then
        temp2 = (x_etaxi*u + y_etaxi*v) * temp1**3
        ct(1) = u*temp1 - x_etaxi*temp2
        ct(2) = v*temp1 - y_etaxi*temp2
      endif

      if (need_normal) then
        if (boundary .eq. 0) then
c         Upstream boundary => constant xi
          temp2 = (x_etaxi*v - y_etaxi*u) * temp1**3
          cn(1) = v*temp1 - x_etaxi*temp2
          cn(2) = -u*temp1 - y_etaxi*temp2
        else
c         Downstream boundaries => constant eta
          temp2 = (y_etaxi*u - x_etaxi*v) * temp1**3
          cn(1) = -v*temp1 - x_etaxi*temp2
          cn(2) = u*temp1 - y_etaxi*temp2
        endif
      endif

      end
c---- get_dVdG_coefs ---------------------------------------------------

c---- add_psi_dSAdG ----------------------------------------------------
c Adds the derivative of the Spalart-Allmaras turbulence model residual
c with respect to the grid node locations, G, premultiplied by psi,
c to psi_dRdG.  That is,
c   psi * d(R(5))/dG
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c Q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c fmu: molecular viscosity.
c x, y: cartesian coordinates of the grid nodes (i.e. G)
c xy, xyj: metrics, and metric jacobian
c psi: adjoint vector
c
c Returns:
c psi_dRdG: with the derivative of the turbulence moddeling terms added
c   to it.
c
c Chad Oldfield, August 2005.
c-----------------------------------------------------------------------
      subroutine add_psi_dSAdG(jdim, kdim, Q, fmu, x, y,
     |    xy, xyj, psi, psi_dRdG)
      implicit none

#include "../include/arcom.inc"
#include "../include/sam.inc"

c     Argument data types; see below for descriptions
      integer jdim, kdim
      double precision q(jdim,kdim,5),
     |    fmu(jdim,kdim), x(jdim,kdim), y(jdim,kdim),
     |    xyj(jdim,kdim), xy(jdim,kdim,4),
     |    psi(jdim, kdim, 5), psi_dRdG(jdim, kdim, 2)

c     Local variables
      integer j, k, jp1, jm1, kp1, km1, jWall, kWall ! Indices.
c         jWall and kWall are the indices of the nearest node on the
c         wall (airfoil surface)
      equivalence (kWall, kbegin)
      double precision xi_x, xi_y, eta_x, eta_y, mJ, Jinv,
     |    x_xi, x_eta, y_xi, y_eta  ! Metrics
      double precision u, ujp1, ujm1, ukp1, ukm1, u_xi, u_eta,
     |                 v, vjp1, vjm1, vkp1, vkm1, v_xi, v_eta
c         Velocity components at this node and neighbouring nodes, and
c         central differences of the velocity components in the xi- and
c         eta-directions.
      double precision vorticity, vortSign, chi, chi3, fv1, fv2, fv3
      double precision dw2, dw2Inv, akarman2Inv
c         Variables in the turbulence model.
      double precision S_tilde, r, g, fw  ! for the destruction term.
      double precision d_xi(2), d_eta(2)
c         These are the d_ coefficients for derivatives with respect to
c         G.  The derivative is given by:
c           d_xi * delta_xi(I) + d_eta * delta_eta(I)
c         where delta_xi(I) is computed in add_delta_xi_I, and
c         delta_eta(I) is computed in add_delta_eta_I.  The two
c         components of these arrays correspond to derivatives in the
c         x- and y-directions.
      integer j1, j2, k1, k2  ! For the diffusion term, indices of
c         neighbouring nodes
      double precision T1coef, T2coef, stencil,
     |    xi_x2, xi_y2, eta_x2, eta_y2, d_xi2(2), d_eta2(2)
c         Variables for the diffusion term:
c         Coefficionts for T1 and T2 in the diffusion term,
c         the backwards differencethe stencil, metrics at neigbouring
c         nodes for the diffusion term, and the equivalent of d_xi and
c         d_eta for neighbouring nodes.
      double precision temp, temp2

      ReInv = 1.d0/Re
      ReSigInv = ReInv * SigmaInv
      akarman2Inv = 1.d0/akarman**2

      do k = klow, kup
        do j = jlow, jup
c         Compute indices
          jp1 = j+1
          jm1 = j-1
          kp1 = k+1
          km1 = k-1
          jWall = max(jtail1, min(jtail2, j))

c         Compute metrics
          xi_x = xy(j,k,1)
          xi_y = xy(j,k,2)
          eta_x = xy(j,k,3)
          eta_y = xy(j,k,4)
          mJ = xyj(j,k)
          Jinv = 1.d0/mJ
          x_xi = eta_y*Jinv
          x_eta = -xi_y*Jinv
          y_xi = -eta_x*Jinv
          y_eta = xi_x*Jinv

c         Compute velocities
          u = Q(j,k,2)/Q(j,k,1)
          ujp1 = Q(jp1,k,2)/Q(jp1,k,1)
          ujm1 = Q(jm1,k,2)/Q(jm1,k,1)
          ukp1 = Q(j,kp1,2)/Q(j,kp1,1)
          ukm1 = Q(j,km1,2)/Q(j,km1,1)
          u_xi = 0.5d0*(ujp1 - ujm1)
          u_eta = 0.5d0*(ukp1 - ukm1)
          v = Q(j,k,3)/Q(j,k,1)
          vjp1 = Q(jp1,k,3)/Q(jp1,k,1)
          vjm1 = Q(jm1,k,3)/Q(jm1,k,1)
          vkp1 = Q(j,kp1,3)/Q(j,kp1,1)
          vkm1 = Q(j,km1,3)/Q(j,km1,1)
          v_xi = 0.5d0*(vjp1 - vjm1)
          v_eta = 0.5d0*(vkp1 - vkm1)

c         Convective term
          if ((eta_x*u + eta_y*v) >= 0.d0) then
            temp = psi(j,k,5) * (Q(j,k,5) - Q(j,km1,5))
          else
            temp = psi(j,k,5) * (Q(j,kp1,5) - Q(j,k,5))
          endif
          d_xi(1) = v * temp
          d_xi(2) = -u * temp
          if ((xi_x*u + xi_y*v) >= 0.d0) then
            temp = psi(j,k,5) * (Q(j,k,5) - Q(jm1,k,5))
          else
            temp = psi(j,k,5) * (Q(jp1,k,5) - Q(j,k,5))
          endif
          d_eta(1) = -v * temp
          d_eta(2) = u * temp

c         Production term: dependancy on G through vorticity/mJ
          chi = Q(j,k,5) * (Q(j,k,1) / fmu(j,k))
          chi3 = chi**3
          fv1 = chi3 / (chi3 + cv1_3)
          if (.not.originalSA) then 
             fv2 = (1+chi/cv2)**(-3)
             fv3=(1+chi*fv1)*(1-fv2)/chi
          else
             fv2 = 1.d0 - chi/(1.d0+chi*fv1)
          end if

          temp = xi_x*v_xi + eta_x*v_eta - xi_y*u_xi - eta_y*u_eta
          vorticity = abs(temp)
          vortSign = sign(1.d0, temp)
          if (vorticity <= 8.5d-10) then
            vorticity = 8.5d-10  ! vorticity cannot be less than this,
c               and so is constant with respect to G
            temp = -psi(j,k,5) * cb1 * Q(j,k,5) * vorticity
            if (.not.originalSA) temp = temp * fv3
            d_xi(1) = d_xi(1) + temp * y_eta
            d_xi(2) = d_xi(2) - temp * x_eta
            d_eta(1) = d_eta(1) - temp * y_xi
            d_eta(2) = d_eta(2) + temp * x_xi
          else
            temp = -psi(j,k,5) * cb1 * Q(j,k,5) * vortSign
            if (.not.originalSA) temp = temp * fv3
            d_xi(1) = d_xi(1) - temp*u_eta
            d_xi(2) = d_xi(2) - temp*v_eta
            d_eta(1) = d_eta(1) + temp*u_xi
            d_eta(2) = d_eta(2) + temp*v_xi
          endif
          
c         Production term: dependancy on G through 1/(dw2*mJ),
c         where dw2 is the square of the distance from the wall.
          dw2 = (x(j,k) - x(jWall,kWall))**2
     |        + (y(j,k) - y(jWall,kWall))**2
          dw2Inv = 1.d0/dw2
          temp = -psi(j,k,5) * cb1 * Q(j,k,5)**2 * fv2 * ReInv
     |        * akarman2Inv * dw2Inv
c         A portion of this dependancy is through the metrics...
          d_xi(1) = d_xi(1) + temp * y_eta
          d_xi(2) = d_xi(2) - temp * x_eta
          d_eta(1) = d_eta(1) - temp * y_xi
          d_eta(2) = d_eta(2) + temp * x_xi
c         and a portion of it is through the distance from the wall.
          temp = -temp * 2.d0 * Jinv * dw2Inv
          psi_dRdG(j,k,1) = psi_dRdG(j,k,1)
     |        + temp*(x(j,k) - x(jWall,kWall))
          psi_dRdG(j,k,2) = psi_dRdG(j,k,2)
     |        + temp*(y(j,k) - y(jWall,kWall))
          psi_dRdG(jWall,kWall,1) = psi_dRdG(jWall,kWall,1)
     |        - temp*(x(j,k) - x(jWall,kWall))
          psi_dRdG(jWall,kWall,2) = psi_dRdG(jWall,kWall,2)
     |        - temp*(y(j,k) - y(jWall,kWall))

c         Destruction term: dependancy on G through fw
          if (.not.originalSA) then
             S_tilde = vorticity*fv3*Re+Q(j,k,5)*akarman2Inv*dw2Inv*fv2
             r = Q(j,k,5)/(vorticity*fv3*Re*akarman**2*dw2+Q(j,k,5)*fv2)
          else
             S_tilde = vorticity * Re + Q(j,k,5)*akarman2Inv*dw2Inv*fv2
             r = Q(j,k,5) / (vorticity*Re*akarman**2*dw2 + Q(j,k,5)*fv2)
          end if
          if (abs(r) >= 10.d0) then
            fw = const  ! and d(fw)/dG = 0
          else
            g = r + cw2*(r**6 - r)
            fw = g * ((1.d0 + cw3_6)/(g**6 + cw3_6))**expon
            temp = psi(j,k,5) * cw1 * Q(j,k,5) * JInv * dw2Inv
     |          * cw3_6 * ((1.d0+cw3_6)/(g**6+cw3_6)**7)**expon
     |          * (1.d0 - cw2 + 6.d0*cw2*r**5)
     |          * (-r**2 * akarman**2)
c           This includes a dependancy on G through vorticity...
            if (vorticity .ne. 8.5d-10) then
              temp2 = temp * dw2 * mJ * vortSign
              if (.not.originalSA) temp2 = temp2 * fv3
              d_xi(1) = d_xi(1) - temp2*u_eta
              d_xi(2) = d_xi(2) - temp2*v_eta
              d_eta(1) = d_eta(1) + temp2*u_xi
              d_eta(2) = d_eta(2) + temp2*v_xi
              temp2 = temp * dw2* mJ * vorticity
              if (.not.originalSA) temp2 = temp2 * fv3
              d_xi(1) = d_xi(1) - temp2*y_eta
              d_xi(2) = d_xi(2) + temp2*x_eta
              d_eta(1) = d_eta(1) + temp2*y_xi
              d_eta(2) = d_eta(2) - temp2*x_xi
            endif  ! Otherwise vorticity is independent of G
c           ...and through distance from the wall
            temp = temp * vorticity * 2.d0
            if (.not.originalSA) temp = temp * fv3
            psi_dRdG(j,k,1) = psi_dRdG(j,k,1)
     |          + temp*(x(j,k) - x(jWall,kWall))
            psi_dRdG(j,k,2) = psi_dRdG(j,k,2)
     |          + temp*(y(j,k) - y(jWall,kWall))
            psi_dRdG(jWall,kWall,1) = psi_dRdG(jWall,kWall,1)
     |          - temp*(x(j,k) - x(jWall,kWall))
            psi_dRdG(jWall,kWall,2) = psi_dRdG(jWall,kWall,2)
     |          - temp*(y(j,k) - y(jWall,kWall))
          endif

c         Destruction term: dependancy on G through 1/(dw2*mJ),
c         where dw2 is the square of the distance from the wall.
          temp = psi(j,k,5) * cw1 * Q(j,k,5)**2 * fw * ReInv * dw2Inv
c         A portion of this dependancy is through the metrics...
          d_xi(1) = d_xi(1) + temp * y_eta
          d_xi(2) = d_xi(2) - temp * x_eta
          d_eta(1) = d_eta(1) - temp * y_xi
          d_eta(2) = d_eta(2) + temp * x_xi
c         and a portion of it is through the distance from the wall.
          temp = -temp * 2.d0 * Jinv * dw2Inv
          psi_dRdG(j,k,1) = psi_dRdG(j,k,1)
     |        + temp*(x(j,k) - x(jWall,kWall))
          psi_dRdG(j,k,2) = psi_dRdG(j,k,2)
     |        + temp*(y(j,k) - y(jWall,kWall))
          psi_dRdG(jWall,kWall,1) = psi_dRdG(jWall,kWall,1)
     |        - temp*(x(j,k) - x(jWall,kWall))
          psi_dRdG(jWall,kWall,2) = psi_dRdG(jWall,kWall,2)
     |        - temp*(y(j,k) - y(jWall,kWall))

c         Diffusion term
          temp = -psi(j,k,5) * ReSigInv
          T1coef = temp * (1 + cb2)
          T2coef = temp * (-cb2) * (fmu(j,k)/Q(j,k,1) + Q(j,k,5))
c         Differences in xi-direction
          stencil = -0.5d0
          do j1 = j-1,j
            temp = stencil * (Q(j1+1,k,5) - Q(j1,k,5)) *
     |          (T1coef * 0.5d0*(fmu(j1+1,k)/Q(j1+1,k,1) + Q(j1+1,k,5)
     |                           + fmu(j1,k)/Q(j1,k,1)   + Q(j1,k,5)  )
     |           + T2coef)
            do j2 = j1,j1+1
              xi_x2 = xy(j2,k,1)
              xi_y2 = xy(j2,k,2)
              eta_x2 = xy(j2,k,3)
              eta_y2 = xy(j2,k,4)
              d_xi2(1) = temp * (- y_eta * xi_x2**2
     |                           + x_eta * xi_y2*xi_x2)
              d_xi2(2) = temp * (- y_eta * xi_x2*xi_y2
     |                           + x_eta * xi_y2**2)
              d_eta2(1) = temp * (- y_eta * xi_x2*eta_x2
     |                            + x_eta * (xi_y2*eta_x2+xyj(j2,k)))
              d_eta2(2) = temp * (- y_eta * (xi_x2*eta_y2-xyj(j2,k))
     |                            + x_eta * xi_y2*eta_y2)
              call add_delta_eta_I(jdim, kdim, j2, k, d_eta2, psi_dRdG)
              call add_delta_xi_I(jdim, kdim, j2, k, d_xi2, psi_dRdG)
              d_eta(1) = d_eta(1) - temp*xy(j2,k,2)
              d_eta(2) = d_eta(2) + temp*xy(j2,k,1)
            end do
            stencil = 0.5d0
          end do
c         Differences in eta-direction
          stencil = -0.5d0
          do k1 = k-1,k
            temp = stencil * (Q(j,k1+1,5) - Q(j,k1,5)) *
     |          (T1coef * 0.5d0*(fmu(j,k1+1)/Q(j,k1+1,1) + Q(j,k1+1,5)
     |                           + fmu(j,k1)/Q(j,k1,1)   + Q(j,k1,5)  )
     |           + T2coef)
            do k2 = k1,k1+1
              xi_x2 = xy(j,k2,1)
              xi_y2 = xy(j,k2,2)
              eta_x2 = xy(j,k2,3)
              eta_y2 = xy(j,k2,4)
              d_xi2(1) = temp * (  y_xi * eta_x2*xi_x2
     |                           - x_xi * (eta_y2*xi_x2-xyj(j,k2)))
              d_xi2(2) = temp * (  y_xi * (eta_x2*xi_y2+xyj(j,k2))
     |                           - x_xi * eta_y2*xi_y2)
              d_eta2(1) = temp * (  y_xi * eta_x2**2
     |                            - x_xi * eta_y2*eta_x2)
              d_eta2(2) = temp * (  y_xi * eta_x2*eta_y2
     |                            - x_xi * eta_y2**2)
              call add_delta_eta_I(jdim, kdim, j, k2, d_eta2, psi_dRdG)
              call add_delta_xi_I(jdim, kdim, j, k2, d_xi2, psi_dRdG)
              d_xi(1) = d_xi(1) + temp*xy(j,k2,4)
              d_xi(2) = d_xi(2) - temp*xy(j,k2,3)
            end do
            stencil = 0.5d0
          end do

c         Multiply the coefficients by delta_xi(I) and delta_eta(I)
          call add_delta_xi_I(jdim, kdim, j, k, d_xi, psi_dRdG)
          call add_delta_eta_I(jdim, kdim, j, k, d_eta, psi_dRdG)
        end do
      end do

      end
c---- add_psi_dSAdG ----------------------------------------------------

c---- add_delta_eta_I --------------------------------------------------
c adds
c   d * delta_eta(I)
c to a
c (this can also be written as d * d/dG(delta_eta G(j,k))).
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c j, k: indices of node
c d: a constant array of length 2.  The first element is the constant
c   by which d/dG_1() is to be multiplied, and the second is the
c   constant by which d/dG_2() is to be multiplied.  G1 and G2 refer
c   to the x- and y- coordinates of the grid node, respectively.
c
c Returns:
c a: a with the derivatives added to it
c
c Chad Oldfield, June 2005.
c-----------------------------------------------------------------------
      subroutine add_delta_eta_I(jdim, kdim, j, k, d, a)
      implicit none
#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim
      integer j, k
      double precision d(2), a(jdim, kdim, 2)

c     Apply the spatial operators del_eta
      if (iord .eq. 2) then
c       Second order centred differences
        if (k .gt. 1) then
          if (k .lt. kdim) then
c           General node
            a(j,k+1,1) = a(j,k+1,1) + 0.5d0*d(1)
            a(j,k+1,2) = a(j,k+1,2) + 0.5d0*d(2)

            a(j,k-1,1) = a(j,k-1,1) - 0.5d0*d(1)
            a(j,k-1,2) = a(j,k-1,2) - 0.5d0*d(2)
          else
c           Far field node (inflow)
            a(j,k  ,1) = a(j,k  ,1) + 1.5d0*d(1)
            a(j,k  ,2) = a(j,k  ,2) + 1.5d0*d(2)

            a(j,k-1,1) = a(j,k-1,1) - 2.0d0*d(1)
            a(j,k-1,2) = a(j,k-1,2) - 2.0d0*d(2)

            a(j,k-2,1) = a(j,k-2,1) + 0.5d0*d(1)
            a(j,k-2,2) = a(j,k-2,2) + 0.5d0*d(2)
          endif
        else
          if (.not.cmesh .or.(j .gt. jtail1 .and. j .lt. jtail2)) then
c           Airfoil node
            a(j,1,1) = a(j,1,1) - 1.5d0*d(1)
            a(j,1,2) = a(j,1,2) - 1.5d0*d(2)

            a(j,2,1) = a(j,2,1) + 2.0d0*d(1)
            a(j,2,2) = a(j,2,2) + 2.0d0*d(2)

            a(j,3,1) = a(j,3,1) - 0.5d0*d(1)
            a(j,3,2) = a(j,3,2) - 0.5d0*d(2)
          else
c           Wake cut node
            a(j,2,1) = a(j,2,1) + 0.5d0*d(1)
            a(j,2,2) = a(j,2,2) + 0.5d0*d(2)

            a(jdim-j+1,2,1) = a(jdim-j+1,2,1) - 0.5d0*d(1)
            a(jdim-j+1,2,2) = a(jdim-j+1,2,2) - 0.5d0*d(2)
          endif
        endif
      elseif (iord .eq. 4) then
        write (*,*) 'Fourth order not implemented!'
      endif
      end
c---- add_delta_eta_I ------------------------------------------------

c---- add_delta_xi_I ------------------------------------------------
c adds
c   d * delta_xi(I)
c to a
c (this can also be written as d * d/dG(delta_xi G(j,k)))
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c j, k: indices of node
c d: a constant array of length 2.  The first element is the constant
c   by which d/dG_1() is to be multiplied, and the second is the
c   constant by which d/dG_2() is to be multiplied.  G1 and G2 refer
c   to the x- and y- coordinates of the grid node, respectively.
c
c Returns:
c a: a with the derivatives added to it
c
c Chad Oldfield, June 2005.
c-----------------------------------------------------------------------
      subroutine add_delta_xi_I(jdim, kdim, j, k, d, a)
      implicit none
#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim
      integer j, k
      double precision d(2), a(jdim, kdim, 2)

c     Apply the spatial operators del_xi
      if (iord .eq. 2) then
c       Second order centred differences
        if (j .gt. 1) then
          if (j .lt. jdim) then
c           General node
            a(j+1,k,1) = a(j+1,k,1) + 0.5d0*d(1)
            a(j+1,k,2) = a(j+1,k,2) + 0.5d0*d(2)

            a(j-1,k,1) = a(j-1,k,1) - 0.5d0*d(1)
            a(j-1,k,2) = a(j-1,k,2) - 0.5d0*d(2)
          else
c           Outflow node, j = jdim
            a(j  ,k,1) = a(j  ,k,1) + 1.5d0*d(1)
            a(j  ,k,2) = a(j  ,k,2) + 1.5d0*d(2)

            a(j-1,k,1) = a(j-1,k,1) - 2.0d0*d(1)
            a(j-1,k,2) = a(j-1,k,2) - 2.0d0*d(2)

            a(j-2,k,1) = a(j-2,k,1) + 0.5d0*d(1)
            a(j-2,k,2) = a(j-2,k,2) + 0.5d0*d(2)
          endif
        else
c         Outflow node, j = 1
          a(1,k,1) = a(1,k,1) - 1.5d0*d(1)
          a(1,k,2) = a(1,k,2) - 1.5d0*d(2)

          a(2,k,1) = a(2,k,1) + 2.0d0*d(1)
          a(2,k,2) = a(2,k,2) + 2.0d0*d(2)

          a(3,k,1) = a(3,k,1) - 0.5d0*d(1)
          a(3,k,2) = a(3,k,2) - 0.5d0*d(2)
        endif
      elseif (iord .eq. 4) then
        write (*,*) 'Fourth order not implemented!'
      endif
      end
c---- add_delta_xi_I ------------------------------------------------

c---- ArrayStats -------------------------------------------------------
c Computes statistical values about the vector a, and writes them to
c the screen.
c
c Requires:
c a: the array in question
c length: the length of the array (one dimensinal).  For a multi-
c   dimensinal array, say a(idim, jdim, kdim), pass in length =
c   idim * jdim * kdim, so that it is interpreted as a 1D array.
c
c Chad Oldfield, June 2005
c-----------------------------------------------------------------------
      subroutine ArrayStats(a, length, nameString)
      implicit none

c     Arguments; see above for descriptions.
      integer length
      double precision a(length)
      character*(*) nameString

c     Local variables
      integer i
      integer minI, maxI  ! indices of minimum & maximum values
      double precision mean, stdev  ! mean and standard deviation

      minI = 1
      maxI = 1
      mean = a(1)

c     Compute maximum, minimum, and mean
      do i = 2,length
        if (a(i) .gt. a(maxI)) then
          maxI = i
        elseif (a(i) .lt. a(minI)) then
          minI = i
        endif
        mean = mean + a(i)
      end do
      mean = mean / dble(length)

c     Compute standard deviation
      stdev = 0.d0
      do i = 1,length
        stdev = stdev + (a(i) - mean)**2
      end do
      stdev = dsqrt(stdev) / dble(length)

c     Write the results to the screen
      write (*,*) 'Array analysis (', nameString, '):'
      write (*,*) '  Size               = ', length
      write (*,*) '  Minimum value      = ', a(minI), minI
      write (*,*) '  Maximum value      = ', a(maxI), maxI
      write (*,*) '  Mean value         = ', mean
      write (*,*) '  Standard deviation = ', stdev
      end
c---- ArrayStats -------------------------------------------------------
