c----------------------------------------------------------------------
c     -- residuals at boundary nodes --
c     -- includes body and farfield residual --      
c     -- all subroutines are in this file --
c     -- calling routine: get_rhs --
c     -- m. nemec,  july 2001 --
c----------------------------------------------------------------------
      subroutine bcrhs( q, s, xy, xyj, x, y, press, turre, rsa)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"

      integer ib,is,lg,lq,k1,k2,j1,j2
      double precision q(*), s(*), xy(*), xyj(*), press(*), x(*), y(*)
      double precision turre(*), rsa(*)

      do ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)

         do is = 1,4

            if (ibctype(ib,is).eq.1) then
c     -- airfoil body --
               if (.not. viscous) then
c     -- inviscid body --
                  if (is.eq.1) then
                     call bcrb( jmax(ib), kmax(ib), jminbnd(ib),
     &                    jmaxbnd(ib), kminbnd(ib), 1, xy(lq),
     &                    xyj(lg), q(lq), s(lq), fsmach, gamma, gami)
                  else if (is.eq.3) then
                     call bcrb( jmax(ib), kmax(ib), jminbnd(ib),
     &                    jmaxbnd(ib), kmaxbnd(ib), -1, xy(lq),
     &                    xyj(lg), q(lq), s(lq), fsmach, gamma, gami)
                  else
                     stop 'bcrhs: problems with (is) for bcrb'
                  end if
               else             ! viscous flow

                  if (is.eq.1) then
                     k1 = kminbnd(ib)
                     k2 = k1+1
                     call bcrbv(ib,jmax(ib), kmax(ib), jminbnd(ib),
     &                    jmaxbnd(ib), k1, k2, xyj(lg), q(lq), s(lq),
     &                    press(lg), turre(lg), rsa(lg))

                  else if (is.eq.3) then
                     k1 = kmaxbnd(ib)
                     k2 = k1-1
                     call bcrbv(ib,jmax(ib), kmax(ib), jminbnd(ib),
     &                    jmaxbnd(ib), k1, k2, xyj(lg), q(lq), s(lq),
     &                    press(lg), turre(lg), rsa(lg))

                  else if (is.eq.2) then
                     j1 = jminbnd(ib)
                     j2 = j1+1
                     call bcrbv24(ib,jmax(ib), kmax(ib), kminbnd(ib),
     &                    kmaxbnd(ib), j1, j2, xyj(lg), q(lq), s(lq),
     &                    press(lg), turre(lg), rsa(lg))

                  else
                     stop 'bcrhs: problems with (is) for bcrbv'
                  end if
               end if

            else if (ibctype(ib,is).eq.2) then
c     -- farfield boundary --
               if (is.eq.1) then
                  call bcrfs13( kminbnd(ib), 1, jminbnd(ib),
     &                 jmaxbnd(ib), jmax(ib), kmax(ib), q(lq), s(lq),
     &                 xy(lq), xyj(lg), x(lg),y(lg), pi, alpha, gamma,
     &                 gami, fsmach, circul, chord, circb, rhoinf, uinf,
     &                 vinf, pinf, turre(lg), rsa(lg), turbulnt, retinf)
               else if (is.eq.2) then 
                  call bcrfs24( jminbnd(ib), 1, kminbnd(ib)+1,
     &                 kmaxbnd(ib)-1, jmax(ib), kmax(ib), q(lq), s(lq),
     &                 xy(lq), xyj(lg), x(lg), y(lg), pi, alpha, gamma,
     &                 gami, fsmach, circul, chord, circb, rhoinf, uinf,
     &                 vinf, pinf, turre(lg), rsa(lg), turbulnt, retinf)
               else if (is.eq.3) then
                  call bcrfs13( kmaxbnd(ib), -1, jminbnd(ib),
     &                 jmaxbnd(ib), jmax(ib), kmax(ib), q(lq), s(lq),
     &                 xy(lq), xyj(lg), x(lg), y(lg), pi, alpha, gamma,
     &                 gami, fsmach, circul, chord, circb, rhoinf, uinf,
     &                 vinf, pinf, turre(lg), rsa(lg), turbulnt, retinf)
               else if (is.eq.4) then
                  call bcrfs24( jmaxbnd(ib), -1, kminbnd(ib)+1,
     &                 kmaxbnd(ib)-1, jmax(ib), kmax(ib), q(lq), s(lq),
     &                 xy(lq), xyj(lg), x(lg), y(lg), pi, alpha, gamma,
     &                 gami, fsmach, circul, chord, circb, rhoinf, uinf,
     &                 vinf, pinf, turre(lg), rsa(lg), turbulnt, retinf)
               end if

            else if (ibctype(ib,is).eq.3) then
c     -- viscous outflow at j=jmin side 2 --
               j1 = jminbnd(ib)
               j2 = j1+1
c               write (*,*) 'calling bcrvout 3',ib,is,j1,j2 
               call bcrvout( j1, j2, kminbnd(ib)+1, kmaxbnd(ib)-1,
     &              jmax(ib), kmax(ib), q(lq), s(lq), xyj(lg),
     &              press(lg), turre(lg), rsa(lg), turbulnt)

            else if (ibctype(ib,is).eq.4) then
c     -- viscous outflow at j=jmax side 4 --
c               write (*,*) 'calling bcrvout 4'
               j1 = jmaxbnd(ib)
               j2 = j1-1
               call bcrvout( j1, j2, kminbnd(ib)+1, kmaxbnd(ib)-1,
     &              jmax(ib), kmax(ib), q(lq), s(lq), xyj(lg),
     &              press(lg), turre(lg), rsa(lg), turbulnt)
            end if
         end do
      end do

      return
      end                       !bcrhs
c     -----------------------------------------------------------------
c     -- airfoil body, inviscid flow --
c     -----------------------------------------------------------------
      subroutine bcrb( jmax, kmax, js, je, kbc, idir, xy, xyj, q, s,
     &     fsmach, gamma, gami)

      implicit none
      integer jmax, kmax, js, je, kbc, idir, kadd, j, k
      double precision q(jmax,kmax,4), s(jmax,kmax,4), xy(jmax,kmax,4)
      double precision xyj(jmax,kmax), gamma, gami, fsmach,  hinf
      double precision xy3,xy4,par,rho,u,v,rho1,u1,v1,pr1,vn1,vt1
      double precision pr2,vt2,pr3,vt3

      kadd = sign(1,idir)
      hinf = 1.d0/gami + 0.5d0*fsmach**2

      do j = js,je

         k = kbc
         xy3 = xy(j,k,3)
         xy4 = xy(j,k,4)           
         par = sqrt(xy3*xy3 + xy4*xy4)
         xy3 = xy3/par
         xy4 = xy4/par
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         rho1= rho
         u1  = u
         v1  = v
         pr1 = gami*(q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)/
     &        q(j,k,1))*xyj(j,k)        
         vn1 = xy3*u + xy4*v
         vt1 = xy4*u - xy3*v

         k = kbc + kadd
         xy3 = xy(j,k,3)
         xy4 = xy(j,k,4)
         par = sqrt(xy3*xy3 + xy4*xy4)
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr2 = gami*(q(j,k,4) -
     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
         vt2 = xy4/par*u - xy3/par*v

         k = kbc + 2*kadd
         xy3 = xy(j,k,3)
         xy4 = xy(j,k,4)
         par = sqrt(xy3*xy3 + xy4*xy4)
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr3 = gami*(q(j,k,4) -
     &        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
         vt3 = xy4/par*u - xy3/par*v

         s(j,kbc,1) = -( vn1 )
         s(j,kbc,2) = -( vt1-2.0*vt2+vt3 )
         s(j,kbc,3) = -( pr1-2.0*pr2+pr3 )
         s(j,kbc,4) = -( gamma/gami*pr1 + 0.5*rho1*(u1*u1+v1*v1) -
     &        rho1*hinf)
      end do

      return
      end                       !bcrb
c     -----------------------------------------------------------------
c     -- airfoil body, viscous flow --
c     -----------------------------------------------------------------
      subroutine bcrbv(ib,jmaxt, kmaxt,js, je, k1, k2, xyj, q, s, press,
     &     turre, rsa)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"

      integer ib,jmaxt, kmaxt, js, je, k1, k2, j
      double precision q(jmaxt,kmaxt,4),s(jmaxt,kmaxt,4)
      double precision press(jmaxt,kmaxt),xyj(jmaxt,kmaxt)
      double precision turre(jmaxt,kmaxt),rsa(jmaxt,kmaxt)
      double precision timetmp
      logical sbtrue

      sbtrue = sbbc .and. ib .eq. 8 .and. k1 .eq. kminbnd(ib)

      if ( sbtrue )  then
c     --Calculate Omega=omegaa*sin(2*pi*omegaf*t) or Omega=omegaa for suction/blowing BC

         if (omegaf .eq. 0.d0) then
            omega=omegaa
         else
            if (numiter-istart .le. nkskip) then
               timetmp=(numiter-istart)*dt2
            else
               timetmp=(numiter-istart-nkskip)*dt2+nkskip*dtbig
            end if
            omega=omegaa*dsin(2.d0*pi*omegaf*timetmp)
         endif

      end if   !Calculate Omega
      
      do j = js,je

         if (sbtrue .and. (j==15 .or. j==16)) then !suction/blowing BC
c            s(j,k1,1) = -( q(j,k1,1)*xyj(j,k1) - rhoinf )
            s(j,k1,2) = -( q(j,k1,2)*xyj(j,k1) - rhoinf*omega*
     &           dcos(sbeta*pi/180.d0-sdelta) )
            s(j,k1,3) = -( q(j,k1,3)*xyj(j,k1) - rhoinf*omega*
     &           dsin(sbeta*pi/180.d0-sdelta) ) 
         else
            s(j,k1,2) = -( q(j,k1,2)*xyj(j,k1) )
            s(j,k1,3) = -( q(j,k1,3)*xyj(j,k1) )
         end if 
    
         s(j,k1,1) = -( q(j,k1,1)*xyj(j,k1)-q(j,k2,1)*xyj(j,k2) )
         s(j,k1,4) = -( press(j,k1)*xyj(j,k1)-press(j,k2)*xyj(j,k2) )

      end do

c     -----------------------------------------------------------------
c     -- s-a model: airfoil body => J^(-1).nu_tilde = 0 --
c     -----------------------------------------------------------------
      if (turbulnt) then
         do j = js,je
            rsa(j,k1) = - turre(j,k1)
         end do
      end if

      return
      end                       !bcrbv

c     -----------------------------------------------------------------
c     -- airfoil body, viscous flow side 2 for blunt trailing edge --
c     -----------------------------------------------------------------

      subroutine bcrbv24(ib,jmaxt,kmaxt, ks, ke, j1, j2, xyj,q,s,press,
     &     turre, rsa)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"

      integer ib,jmaxt, kmaxt, ks, ke, j1, j2, k
      double precision q(jmaxt,kmaxt,4), s(jmaxt,kmaxt,4)
      double precision xyj(jmaxt,kmaxt), turre(jmaxt,kmaxt)
      double precision press(jmaxt,kmaxt), rsa(jmaxt,kmaxt)
      double precision timetmp
      logical sbtrue

      sbtrue = sbbc .and. ib .eq. 4 .and. j1 .eq. jminbnd(ib)

      if ( sbtrue )  then
c     --Calculate Omega=omegaa*sin(2*pi*omegaf*t) or Omega=omegaa for suction/blowing BC

         if (omegaf .eq. 0.d0) then
            omega=omegaa
         else
            if (numiter-istart .le. nkskip) then
               timetmp=(numiter-istart)*dt2
            else
               timetmp=(numiter-istart-nkskip)*dt2+nkskip*dtbig
            end if
            omega=omegaa*dsin(2.d0*pi*omegaf*timetmp)
         endif

      end if   !Calculate Omega

      do k = ks,ke

         if (sbtrue .and. (k==36 .or. k==37 .or. k==38)) then !suction/blowing BC
c            s(j1,k,1) = -( q(j1,k,1)*xyj(j1,k) - rhoinf )
            s(j1,k,2) = -( q(j1,k,2)*xyj(j1,k) - rhoinf*omega*
     &           dcos(sbeta*pi/180.d0-sdelta) )
            s(j1,k,3) = -( q(j1,k,3)*xyj(j1,k) - rhoinf*omega*
     &           dsin(sbeta*pi/180.d0-sdelta) ) 
         else
            s(j1,k,2) = -( q(j1,k,2)*xyj(j1,k) )
            s(j1,k,3) = -( q(j1,k,3)*xyj(j1,k) )
         end if 

         s(j1,k,1) = -( q(j1,k,1)*xyj(j1,k)-q(j2,k,1)*xyj(j2,k) )
         s(j1,k,4) = -( press(j1,k)*xyj(j1,k)-press(j2,k)*xyj(j2,k) )

      end do


c     -----------------------------------------------------------------
c     -- s-a model: airfoil body => J^(-1).nu_tilde = 0 --
c     -----------------------------------------------------------------
      if (turbulnt) then
         do k = ks,ke
            rsa(j1,k) = - turre(j1,k)
         end do
      end if

      return
      end                       !bcrbv24




c     ------------------------------------------------------------------
c     -- farfield boundary, sides 1 and 3 --
c     ------------------------------------------------------------------
      subroutine bcrfs13( kbc, idir, js, je, jmax, kmax, q, s, xy, xyj,
     &     x, y, pi, alpha, gamma, gami, fsmach, circul, chord, circb,
     &     rhoinf, uinf, vinf, pinf, turre, rsa, turbulnt, retinf)

      implicit none

      integer kbc,idir,js,je,jmax,kmax,kadd,j,k
      double precision q(jmax,kmax,4),   s(jmax,kmax,4), xy(jmax,kmax,4)
      double precision xyj(jmax,kmax),   x(jmax,kmax),   y(jmax,kmax)
      double precision turre(jmax,kmax), rsa(jmax,kmax)
      double precision pi,alpha,gamma,gami,fsmach,chord,circb,retinf
      double precision rhoinf,uinf,vinf,pinf,sgn,alphar,hstfs 
      double precision xy3,xy4,par,rho,u,v,pr,a,vn1,vt1,a1,rho1,pr1
      double precision xa,ya,radius,angl,cjam,sjam,qcirc,uf,vf,af2,af
      double precision vninf,vtinf,ainf,vn2,vt2,a2,rho2,pr2
      logical circul, turbulnt

      kadd = idir
      sgn  = -float(idir)

      alphar = alpha*pi/180.0  
      hstfs = 1.0/gami + 0.5*fsmach**2  

      if (circul) then
         do j = js,je

            k = kbc
            xy3 = xy(j,k,3)
            xy4 = xy(j,k,4)
            par = sqrt(xy3*xy3 + xy4*xy4)
            xy3 = xy3/par*sgn
            xy4 = xy4/par*sgn

            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &           q(j,k,1))*xyj(j,k)
            a   = sqrt(gamma*pr/rho)                    
            vn1 =  xy3*u + xy4*v
            vt1 = -xy4*u + xy3*v
            a1  = a
            rho1= rho
            pr1 = pr

c     -- reset free stream values with circulation correction --
            xa = x(j,k) - chord/4.0
            ya = y(j,k)
            radius = sqrt(xa**2+ya**2)
            angl = atan2(ya,xa)
            cjam = cos(angl)
            sjam = sin(angl)
            qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
            uf = uinf + qcirc*sjam
            vf = vinf - qcirc*cjam
            af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
            af = dsqrt(af2)

            vninf =  xy3*uf + xy4*vf
            vtinf = -xy4*uf + xy3*vf
            ainf = af

            k = kbc + kadd
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &           q(j,k,1))*xyj(j,k)
            a   = sqrt(gamma*pr/rho)                    
            vn2 =  xy3*u + xy4*v
            vt2 = -xy4*u + xy3*v
            a2  = a
            rho2= rho
            pr2 = pr

            k = kbc
            s(j,k,1) = vninf-2.0*ainf/gami - (vn1-2.0*a1/gami)
            s(j,k,2) = vn2+2.0*a2/gami - (vn1+2.0*a1/gami)

c     -- inflow --
            if (vn1.le.0.0) then
               s(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
               s(j,k,4) = vtinf - vt1
               if (turbulnt) rsa(j,k) = retinf - turre(j,k)

c     -- outflow --
            else
               s(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
               s(j,k,4) = vt2 - vt1
               if (turbulnt) rsa(j,k) = turre(j,k+kadd) - turre(j,k)

            endif
         end do

      else                      !no circulation correction
         do j = js,je

            k = kbc
            xy3 = xy(j,k,3)
            xy4 = xy(j,k,4)
            par = sqrt(xy3*xy3 + xy4*xy4)
            xy3 = xy3/par*sgn
            xy4 = xy4/par*sgn

            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &           q(j,k,1))*xyj(j,k)
            a   = sqrt(gamma*pr/rho)                    
            vn1 =  xy3*u + xy4*v
            vt1 = -xy4*u + xy3*v
            a1  = a
            rho1= rho
            pr1 = pr

            uf = uinf
            vf = vinf
            af = sqrt(gamma*pinf/rhoinf)

            vninf =  xy3*uf + xy4*vf
            vtinf = -xy4*uf + xy3*vf
            ainf = af

            k = kbc + kadd
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &           q(j,k,1))*xyj(j,k)
            a   = sqrt(gamma*pr/rho)                    
            vn2 =  xy3*u + xy4*v
            vt2 = -xy4*u + xy3*v
            a2  = a
            rho2= rho
            pr2 = pr

            k = kbc
            s(j,k,1) = vninf-2.0*ainf/gami - (vn1-2.0*a1/gami)
            s(j,k,2) = vn2+2.0*a2/gami - (vn1+2.0*a1/gami)

c     -- inflow --
            if (vn1.le.0.0) then
               s(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
               s(j,k,4) = vtinf - vt1
               if (turbulnt) rsa(j,k) = retinf - turre(j,k)

c     -- outflow --
            else
               s(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
               s(j,k,4) = vt2 - vt1
               if (turbulnt) rsa(j,k) = turre(j,k+kadd) - turre(j,k) 

            endif
         end do

      end if

      return
      end                       !bcrfs13
c     ------------------------------------------------------------------
c     -- farfield boundary, sides 2 and 4 --
c     ------------------------------------------------------------------
      subroutine bcrfs24( jbc, idir, ks, ke, jmax, kmax, q, s, xy, xyj,
     &     x, y, pi, alpha, gamma, gami, fsmach, circul, chord, circb,
     &     rhoinf, uinf, vinf, pinf, turre, rsa, turbulnt, retinf)

      implicit none
      integer jbc,idir,ks,ke,jmax,kmax,jadd,j,k
      double precision q(jmax,kmax,4),   s(jmax,kmax,4), xy(jmax,kmax,4)
      double precision xyj(jmax,kmax),   x(jmax,kmax),   y(jmax,kmax)
      double precision turre(jmax,kmax), rsa(jmax,kmax)
      double precision pi,alpha,gamma,gami,fsmach,chord,circb,retinf
      double precision rhoinf,uinf,vinf,pinf,sgn,alphar,hstfs 
      double precision xy1,xy2,par,rho,u,v,pr,a,vn1,vt1,a1,rho1,pr1
      double precision xa,ya,radius,angl,cjam,sjam,qcirc,uf,vf,af2,af
      double precision vninf,vtinf,ainf,vn2,vt2,a2,rho2,pr2
      logical circul, turbulnt

      jadd = idir
      sgn  = -float(idir)

      alphar = alpha*pi/180.0  
      hstfs = 1.0/gami + 0.5*fsmach**2  

      if (circul) then
         do k = ks,ke

            j = jbc
            xy1 = xy(j,k,1)
            xy2 = xy(j,k,2)
            par = sqrt( xy1*xy1 + xy2*xy2 )
            xy1 = xy1/par*sgn
            xy2 = xy2/par*sgn
c     
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/
     &           q(j,k,1))*xyj(j,k)
            a   = sqrt(gamma*pr/rho)                    
            vn1 = xy1*u + xy2*v
            vt1 = xy2*u - xy1*v
            a1  = a
            rho1= rho
            pr1 = pr
c     -- reset free stream values with circulation correction --
            xa = x(j,k) - chord/4.
            ya = y(j,k)
            radius = dsqrt(xa**2+ya**2)
            angl = atan2(ya,xa)
            cjam = cos(angl)
            sjam = sin(angl)
            qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
            uf = uinf + qcirc*sjam
            vf = vinf - qcirc*cjam
            af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
            af = sqrt( af2 )

            vninf = xy1*uf + xy2*vf
            vtinf = xy2*uf - xy1*vf
            ainf = af

            j = jbc + jadd
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &           q(j,k,1))*xyj(j,k)
            a   = sqrt(gamma*pr/rho)                    
            vn2 = xy1*u + xy2*v
            vt2 = xy2*u - xy1*v
            a2  = a
            rho2= rho
            pr2 = pr

            j = jbc
            s(j,k,1) = vninf-2.0*ainf/gami - (vn1-2.0*a1/gami)
            s(j,k,2) = vn2+2.0*a2/gami - (vn1+2.0*a1/gami)

c     -- inflow --
            if (vn1.le.0.0) then
               s(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
               s(j,k,4) = vtinf - vt1
               if (turbulnt) rsa(j,k) = retinf - turre(j,k)

c     -- outflow --
            else
               s(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
               s(j,k,4) = vt2 - vt1
               if (turbulnt) rsa(j,k) = turre(j+jadd,k) - turre(j,k) 

            endif
         end do

      else                      !no circulation correction 
         do k = ks,ke
            j = jbc

            xy1 = xy(j,k,1)
            xy2 = xy(j,k,2)
            par = sqrt( xy1*xy1 + xy2*xy2 )
            xy1 = xy1/par*sgn
            xy2 = xy2/par*sgn

            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/
     &           q(j,k,1))*xyj(j,k)
            a   = sqrt(gamma*pr/rho)                    
            vn1 = xy1*u + xy2*v
            vt1 = xy2*u - xy1*v
            a1  = a
            rho1= rho
            pr1 = pr

            uf = uinf
            vf = vinf
            af = dsqrt(gamma*pinf/rhoinf)

            vninf = xy1*uf + xy2*vf
            vtinf = xy2*uf - xy1*vf
            ainf = af

            j = jbc + jadd
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &           q(j,k,1))*xyj(j,k)
            a   = sqrt(gamma*pr/rho)                    
            vn2 = xy1*u + xy2*v
            vt2 = xy2*u - xy1*v
            a2  = a
            rho2= rho
            pr2 = pr

            j = jbc
            s(j,k,1) = vninf-2.0*ainf/gami - (vn1-2.0*a1/gami)
            s(j,k,2) = vn2+2.0*a2/gami - (vn1+2.0*a1/gami)

c     -- inflow --
            if (vn1.le.0.0) then
               s(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
               s(j,k,4) = vtinf - vt1
               if (turbulnt) rsa(j,k) = retinf - turre(j,k)

c     -- outflow --
            else
               s(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
               s(j,k,4) = vt2 - vt1
               if (turbulnt) rsa(j,k) = turre(j+jadd,k) - turre(j,k) 

            endif
c     if (k.eq.ke) then
c     write (*,*) j,k,'here'
c     write (*,*) s(j,k,1),s(j,k,2),s(j,k,3),s(j,k,4)
c     write (*,*) q(j,k,1),q(j,k,2),q(j,k,3),q(j,k,4)
c     end if
         end do
      end if

      return
      end                       !bcrfs24




c     ------------------------------------------------------------------
c     -- farfield boundary, viscous outflow  --
c     ------------------------------------------------------------------
      subroutine bcrvout( j1, j2, ks, ke, jmax, kmax, q, s, xyj, press,
     &     turre, rsa, turbulnt)

      implicit none

      integer j1, j2, ks, ke, jmax, kmax, k 
      double precision q(jmax,kmax,4),s(jmax,kmax,4),press(jmax,kmax)
      double precision xyj(jmax,kmax),turre(jmax,kmax),rsa(jmax,kmax)
      double precision pr1,pr2
      logical   turbulnt

      do k = ks,ke
         pr1 = press(j1,k)*xyj(j1,k)
         pr2 = press(j2,k)*xyj(j2,k)

         s(j1,k,1) = -( q(j1,k,1)*xyj(j1,k)-q(j2,k,1)*xyj(j2,k) )
         s(j1,k,2) = -( q(j1,k,2)*xyj(j1,k)-q(j2,k,2)*xyj(j2,k) )
         s(j1,k,3) = -( q(j1,k,3)*xyj(j1,k)-q(j2,k,3)*xyj(j2,k) )
         s(j1,k,4) = -( pr1-pr2 )
      end do     

      if (turbulnt) then
         do k = ks,ke
            rsa(j1,k) = turre(j2,k) - turre(j1,k)
         end do
      end if

      return
      end                       !bcrvout

