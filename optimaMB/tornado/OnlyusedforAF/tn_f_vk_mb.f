      do k=kbegin,kend
         do j=jbegin,jend
            vorty=abs(uy(j,k)-vx(j,k))
            turnu(j,k)=a_1*vk(j,k)/max(a_1*ve(j,k),vorty*f_2(j,k))
         enddo
      enddo
      if (bnd(1)) then
         k=kbegin
         do j=jlow,jup
            turnu(j,k)=0.0
         enddo
      endif
      if (bnd(3)) then
         k=kend
         do j=jlow,jup
            turnu(j,k)=0.0
         enddo
      endif
