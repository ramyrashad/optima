PLATFORM=$(shell uname -m)

TARGET=OPTIMA2D_$(PLATFORM)

# Location of SNOPT libraries
SNOPT_LIBS    =   ../snopt7/lib/libsnopt.a  \
                  ../snopt7/lib/libsnprint.a \
                  ../snopt7/lib/libblas.a

# Location of HYBROPT library
HYBROPT_LIBS  =   ../hybropt/libhybropt.a   \
                  ../hybropt/Evolve_1.8/libevolver.a

# Location of .mod files
MODDIR = $(PWD)/.moddir

# Location of SPARSKIT library
SKIT = ../SPARSKIT2/libskit_$(PLATFORM).a

# Location of SPARSELIB libraries
SPARSELIB_DIR = ../sparselib_1_6/lib
SPARSELIBS = $(SPARSELIB_DIR)/libsparse.a \
	     $(SPARSELIB_DIR)/libspblas.a \
	     $(SPARSELIB_DIR)/libmv.a

# Location of SNOPT libraries
SNOPTLIBDIR = snopt7/lib
SNOPTLIBS = $(SNOPTLIBDIR)/libsnoptf.a \
	$(SNOPTLIBDIR)/libsnopt.a \
	$(SNOPTLIBDIR)/libblas.a

# Location of PLOTLIB library
PLOTLIB = ../modair/Linux_$(PLATFORM)/libPltDP.a

# Location of X11 library
XLIB =  ${XLIB_PATH} -lX11

#For the Intel/AMD machines...
ifeq ($(PLATFORM),i386)
  PLATFORM = i686
endif
ifeq ($(PLATFORM),i486)
  PLATFORM = i686
endif
ifeq ($(PLATFORM),i586)
  PLATFORM = i686
endif

ifeq ($(PLATFORM), i686)

  FC  = ifort
  F77 = ifort
  CC  = icpc # this was gcc (Chad Oldfield)
  FFLAGS=-auto -r8 -O3 -tpp6 -fpp -mp -pc 80 -prec_div -align all \
   -module $(MODDIR)
  ifeq ($(DEBUG),ON)
    FFLAGS=-auto -r8 -g -CB -tpp6 -fpp -mp -warn all -pc 80 -prec_div \
	  -align all -module $(MODDIR) 
  endif
  LAPACKS=../LAPACK/blas_$(PLATFORM).a ../LAPACK/lapack_$(PLATFORM).a
  LDFLAGS = $(SKIT) $(SPARSELIBS) $(PLOTLIB) $(XLIB) $(LAPACKS) \
	    -Vaxlib \
	    -L/usr2/local_net1/intel/compiler80/lib/ -lcxa -lcprts 
#mb works with icpc link
#mb -L/usr2/local_net1/intel/compiler80/lib/ -lifcore -lifport for_main.o 
#-L/usr/lib/gcc-lib/i386-redhat-linux/3.3.2/ -lstdc++
#eliminated the library "tecio.a"
#    /nfs/kris/usr2/local_net1/tecplot10rev6/lib/tecio.a

#note change to "tecplot10rev6" from "tecplot10rev4" in FLAGS macro

endif

# For compile on SciNet or cfd lab machines
ifeq ($(PLATFORM),x86_64)

  F95ROOT=/scinet/gpc/intel/ics/composerxe-2011.0.084/mkl
  FC  = mpif90
  F77 = mpif90
  CC  = mpicc

  FFLAGS=-r8 -O2 -cpp -fpconstant -ftz -fp-model strict -align all \
    -convert big_endian -mcmodel=large -module $(MODDIR)
  # add mpi flag
  ifeq ($(MPI),ON)
  FFLAGS += -D_MPI_VERSION
  endif
  # add debug flag
  ifeq ($(DEBUG),ON)
	 FFLAGS += -check all
  endif

  LDFLAGS= $(SKIT) $(SPARSELIBS) $(PLOTLIB) $(XLIB) \
    -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -openmp -lpthread \
    -L/usr/lib/gcc/x86_64-redhat-linux/4.1.1/ -lstdc++ -mcmodel=large \
	 -shared-intel

endif

#  FFLAGS=-r8 -O2 -cpp -fpconstant -ftz -fp-model strict -align all \
#  -convert big_endian -mcmodel=large -D_MPI_VERSION -module $(MODDIR)
#  ifeq ($(debug),on)
#    FFLAGS=-r8 -O0 -cpp -fpconstant -ftz -fp-model strict -align all \
#    -g -check all -convert big_endian -mcmodel=large -D_MPI_VERSION \
#    -module $(MODDIR)
#  endif


# For the Itanium 2 machines...
ifeq ($(PLATFORM), ia64)

  FC  = mpif90
  F77 = mpif90
  CC  = mpicc

  LAPACKS=../LAPACK/blas_ia64.a ../LAPACK/lapack_ia64.a
  FFLAGS=-r8 -O2 -cpp -fpconstant -ftz -fp-model strict -align all \
  -convert big_endian -mcmodel=large -D_MPI_VERSION -module $(MODDIR)

  #FFLAGS=-r8 -O2 -cpp -fpconstant -ftz -mp -align all -D_MPI_VERSION \
  #-module $(MODDIR)

  ifeq ($(DEBUG),ON)
    FFLAGS=-r8 -cpp -fpconstant -ftz -mp -align all -g -check all \
    -D_MPI_VERSION -module $(MODDIR)
  endif
  LDFLAGS = $(SKIT) $(SPARSELIBS) $(PLOTLIB) $(XLIB) \
	    -Vaxlib -L/opt/intel/mkl72/lib/64/ -lmkl \
	    -L/usr/lib/gcc/ia64-redhat-linux/4.1.1/ -lstdc++

endif

# For the HP Alpha machines
ifeq ($(PLATFORM), alpha)

  F77 = mpif90
  FC  = mpif90
  CC  = mpiCC
  FFLAGS=-r8 -fast -cpp -f77rtl -convert big_endian
  ifeq ($(DEBUG),ON)
    FFLAGS=-r8 -g -C -cpp -convert big_endian -check bounds -std95 -u
  endif
  LDFLAGS = $(SKIT) $(SPARSELIBS) $(PLOTLIB) $(XLIB) \
	    -lcxml -L/usr/lib/compaq/cxx-6.5.9.31/alpha-linux/bin/  \
	    -lcxxstdma_rh70 -lcxxma_rh70
endif

export F76 FC CC FFLAGS LAPACKS
