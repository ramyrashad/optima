*     ------------------------------------------------------------------
*     Program name:  SNoptimize
*     Written by: Howard Buckley
*     Date: August 2008
*
*     This subroutine is the interface between Optima2D and SNOPT. The 
*     airfoil optimization problem is defined in the Optima2D input file.
*     The optimization inputs required by SNOPT are passed through this 
*     subroutine. SNOPT will make calls to various subroutines in 
*     Optima2D to utilize its flow solver, calculate objective functions
*     and objective function gradients, and evaluate geometric constraint
*     equations.
*
*
*     ------------------------------------------------------------------
      subroutine SNoptimize(jdim, kdim, ndim, ifirst, indx, icol, iex,
     &        q, cp, xy, xyj, x, y, dx, dy, lambda, fmu, vort, turmu,
     &        cp_tar, dvs, idv, bap,bcp,bt, bknot, work1, ia, ja, as,
     &        ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, cp_his,
     &        ac_his, opwk, ifun, iter, nflag,
     &        xsave, ysave, qtmp, itfirst, obj0s, ru, ruIndex)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      !-- Declare all Optima2D variables
      
      integer
     &     jdim, kdim, ndim, ifirst, indx(jdim,kdim), icol(9), i,
     &     iex(jdim,kdim,4), idv(nc+mpopt), j ,k ,l,
     &     ia(jdim*kdim*ndim+2), ifun, iter, nflag, ifun_tmp,
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1), mp, 
     &     ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1),
     &     iat(jdim*kdim*ndim+2),
     &     jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1), itfirst,
     &     gradtemp, varStat, ruIndex

      double precision
     &     q(jdim,kdim,ndim), cp(jdim,kdim), obj0, obj, fp, fmin, alfa,
     &     xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim),
     &     dx(jdim*kdim*incr), dy(jdim*kdim*incr), fmu(jdim,kdim), 
     &     lambda(2*jdim*kdim*incr), vort(jdim,kdim), turmu(jdim,kdim), 
     &     cp_tar(jbody,2), dvs(nc+mpopt), bt(jbody), bknot(jbsord+nc),
     &     bap(jbody,2), bcp(nc,2), work1(jdim*kdim,100),
     &     as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), 
     &     pat(jdim*kdim*ndim*ndim*5+1), cp_his(jbody,300,mpopt),
     &     ac_his(jbody,300), opwk(jdim*kdim,8), s_vector(ndv),
     &     xsave(jdim,kdim), ysave(jdim,kdim),dvscale(nc+mpopt), 
     &     at, ap, gsq, dg, dg1, dp, step, acc, dal, u1, u2, u3, u4, 
     &     eps, xsq, dv_scal(ibsnc), test, dv_scalMag, sdMag, numer,
     &     denom, tmp, stmp, gtmp(ibsnc), gtemp(ibsnc), fd_temp,
     &     qs(jdim,kdim,4), xs(jdim,kdim), ys(jdim,kdim),
     &     aleft, dleft, fleft, aright, dright, fright,
     &     garbage, dvs_avg, gmin(ibsnc), dgTest, gminMag,
     &     cd(mpopt),cl(mpopt),cm(mpopt), grad(ibsnc),
     &     qtmp, obj0s(mpopt), q_init, grads(mpopt,ibsnc), objs(mpopt),
     &     qwarm(warmset,maxj,maxk,nblk,nopc)

      logical
     &     rsw

      dimension      
     &     q_init(warmset,jdim,kdim,ndim,mpopt),      
     &     qtmp(jdim,kdim,ndim,mpopt) 
      
      character 
     &     command*60, filename*40

      !-- Declare all SNOPT variables

      integer
     &     maxF, maxn, nxname, nFname, lenA, lenG
      parameter
     &   ( maxF   = 30,
     &     maxn   = 50,
     &     lenA   = 50, lenG   = 600,
     &     nxname =  1, nFname =   1 )
      integer
     &     iAfun(lenA), jAvar(lenA), iGfun(lenG), jGvar(lenG),
     &     xstate(maxn), Fstate(maxF)
      character
     &     lfile*20, Prob*8, xnames(nxname)*8, Fnames(nFname)*8
      double precision
     &     ObjAdd, sInf, A(lenA), Flow(maxF), Fupp(maxF), F(maxF),
     &     xlow(maxn), xupp(maxn), sn_x(maxn), Fmul(maxF), xmul(maxn)
      integer
     &     Errors, neA, neG, ObjRow, INFO, iPrt, iPrint, iSpecs, iSum,
     &     iSumm, Major, mincw, miniw, minrw, nF, n, nInf, nOut, nS,
     &     d1, d10, namelen
      external
     &     usrfun

*     ------------------------------------------------------------------
*     SNOPT workspace

      integer               lenrw
      parameter          (  lenrw = 20000)
      double precision   rw(lenrw)
      integer               leniw
      parameter          (  leniw = 20000)
      integer            iw(leniw)
      integer               lencw
      parameter          (  lencw =   500)
      character          cw(lencw)*8

      integer             Cold,       Basis,      Warm
      parameter          (Cold   = 0, Basis  = 1, Warm  = 2)
*     ------------------------------------------------------------------
*     USER workspace

      integer               lenru
      parameter          (  lenru = 30000000)
      double precision   ru(lenru)
      integer               leniu
      parameter          (  leniu = 20000)
      integer            iu(leniu)
      integer               lencu
      parameter          (  lencu =   500)
      character          cu(lencu)*8

*     ------------------------------------------------------------------
*
*     Specify some of the SNOPT files.
*     iSpecs  is the Specs file   (0 if none).
*     iPrint  is the Print file   (0 if none).
*     iSumm   is the Summary file (0 if none).
*
*     nOut    is an output file used by the calling program.

      iSpecs =  4
      iSumm  =  6
      iPrint =  9
      nOut   =  6

      !-- Open the SNOPT Specs file

      lfile = 'SNOPT.spc'
      open( iSpecs, file=lfile, status='OLD',     err=800 )

      !-- Create a unique SNOPT Print file name for each processor
#ifdef _MPI_VERSION
      d10  = rank / 10
      d1   = rank - (d10*10)
#else
      d10  = 0
      d1   = - (d10*10)
#endif

      filename = 'SNOPT.out'
      namelen =  len_trim(filename)
      filename(namelen+1:namelen+2)=char(d10+48)//char(d1+48)

      !-- Open the SNOPT Print file

      open( iPrint, file=filename, status='UNKNOWN', err=800 )

*     ------------------------------------------------------------------
*     First,  snInit MUST be called to initialize optional parameters
*     to their default values.
*     ------------------------------------------------------------------
      call snInit
     &   ( iPrint, iSumm, cw, lencw, iw, leniw, rw, lenrw )

*     ------------------------------------------------------------------
*     Read a Specs file (Optional).
*     ------------------------------------------------------------------
      call snSpec
     &   ( iSpecs, INFO, cw, lencw, iw, leniw, rw, lenrw )

      if (INFO .ne. 101  .and.  INFO .ne. 107) then
         go to 990
      end if

*     Specify input data for the optimization problem

      Errors = 0
 
      call snOptData
     &   ( Errors, maxF, maxn,
     &     iAfun, jAvar, lenA, neA, A,
     &     iGfun, jGvar, lenG, neG,
     &     Prob, nF, n,
     &     ObjAdd, ObjRow, xlow, xupp, Flow, Fupp,
     &     sn_x, xstate, xmul, F, Fstate, Fmul,
     &     cw, lencw, iw, leniw, rw, lenrw, dvs, dvscale)
      if (Errors .gt. 0) go to 910


      !-- Pack 'dvs' amd dvscale' into user work array 'ru' so they can
      !-- be used by 'usrfun'

      i = ruIndex

      do j=1,ibsnc
         ru(i) = dvs(j)
         ru(ibsnc+i) = dvscale(j)
         i = i+1
      end do

*     ------------------------------------------------------------------
*     Specify any options not set in the Specs file.
*     i1 and i2 may refer to the Print and Summary file respectively.
*     Setting them to 0 suppresses printing.
*     ------------------------------------------------------------------
      Major    = 250
      iPrt     =   0
      iSum     =   0
      call snseti
     &   ( 'Major Iteration limit', Major, iPrt, iSum, Errors,
     &     cw, lencw, iw, leniw, rw, lenrw )
*     ------------------------------------------------------------------
*     Go for it, using a Cold start.
*     ------------------------------------------------------------------

      call snOptA
     &   ( Cold, nF, n, nxname, nFname,
     &     ObjAdd, ObjRow, Prob, usrfun,
     &     iAfun, jAvar, lenA, neA, A,
     &     iGfun, jGvar, lenG, neG,
     &     xlow, xupp, xnames, Flow, Fupp, Fnames,
     &     sn_x, xstate, xmul, F, Fstate, Fmul,
     &     INFO, mincw, miniw, minrw,
     &     nS, nInf, sInf,
     &     cu, lencu, iu, leniu, ru, lenru,
     &     cw, lencw, iw, leniw, rw, lenrw )

      if (INFO .eq. 82 .or. INFO .eq. 83 .or. INFO .eq. 84) then
         go to 920
      end if

      write(nOut, *) ' '
      write(nOut, *) 'sn_x = ', sn_x
      write(nOut, *) 'snOptA finished.'
      write(nOut, *) 'Input errors  =', Errors
      write(nOut, *) 'INFO          =', INFO
      write(nOut, *) 'nInf          =', nInf
      write(nOut, *) 'sInf          =', sInf
      if (ObjRow .gt. 0)
     &write(nOut, *) 'Obj           =', ObjAdd + F(ObjRow)
      if (INFO .ge. 30) go to 920

      stop

*     ------------------------------------------------------------------
*     Error exit.
*     ------------------------------------------------------------------
  800 write(nOut, 4000) 'Error while opening file', lfile
      stop

  910 write(nOut, *) ' '
      write(nOut, *) 'Insufficient space to hold the problem'
      stop

  920 write(nOut, *) ' '
      write(nOut, *) 'STOPPING because of error condition'

  990 stop

 4000 format(/  a, 2x, a  )
 4010 format(/  a, 2x, i6 )

      end ! SNoptimize

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++





