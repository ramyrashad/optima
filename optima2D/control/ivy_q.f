c----------------------------------------------------------------------
c     -- implicit thin-layer viscous terms in y-direction --
c     -- differentiation of the viscous flux vector w.r.t. Q^ --
c     -- written by: marian nemec --
c     -- heavily modified by markus rumpfkeil -
c     -- date: jan. 2006
c     Note: we need -Re^(-1)*d_eta Fhat
c----------------------------------------------------------------------

      subroutine ivy_q (jdim, kdim, ndim,xy, xyj,q,fmu,turmu,press,dmul,  
     &     dmut,dk,dpk)

      implicit none

#include "../include/arcom.inc"
#include "../include/visc.inc"

      integer jdim, kdim, ndim,j,k,n,kp

      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision fmu(jdim,kdim), turmu(jdim,kdim)
      double precision dk(ndim,3,jdim,kdim), dpk(ndim,3,jdim,kdim)
      double precision q2(jdim,kdim,4)
      double precision press(jdim,kdim)

      double precision dmul(ndim,jdim,kdim),dmut(5,jdim,kdim)

      double precision a1(jdim,kdim), a2(jdim,kdim), a3(jdim,kdim)
      double precision a4(jdim,kdim)

      double precision hre,hfmu,t1,t2,t3,r1,ah1,ah2,ah3,ah4
      double precision rp1,u1,v1,up1,vp1,ueta,veta,c2eta,uavg,vavg
      double precision vflux1,vflux2,vflux3,S1,S2

c     prlam   =  laminar prandtl number  = .72                      
c     prturb  =  turbulent prandtl number = .90                     
c     prlinv  =  1./(laminar prandtl number)                        
c     prtinv  =  1./(turbulent prandtl number)                      
c     f13     =  1/3                                                
c     f43     =  4/3                                                
c     hre     =  1/2 * reynolds number                           
c     fmu     =  laminar viscosity
c     turmu   =  turbulent viscosity      

      hre = 1.d0/(2.d0*re)
 
c     -- calculate alfas and useful variables --
      do k = kbegin,kend                                              
        do j = jlow,jup 
c     t1 = eta_x **2
c     t2 = eta_x*eta_y
c     t3 = eta_y **2          
          t1      = xy(j,k,3)*xy(j,k,3)                               
          t2      = xy(j,k,3)*xy(j,k,4)                               
          t3      = xy(j,k,4)*xy(j,k,4)                               
          r1      = hre/xyj(j,k)
          
          a1(j,k) = r1*( f43*t1 + t3    ) 
          a2(j,k) = r1*(     t2 * f13   ) 
          a3(j,k) = r1*( t1     + f43*t3)                        
          a4(j,k) = r1*( t1     + t3    )

          r1   = 1.d0/q(j,k,1)                                        
          q2(j,k,1) = r1                                           
          q2(j,k,2) = r1*q(j,k,2)                                  
          q2(j,k,3) = r1*q(j,k,3)                                  
          q2(j,k,4) = r1*q(j,k,4) - (q2(j,k,2)**2 +q2(j,k,3)**2)
        end do
      end do

c     -- entries for viscous Jacobian, treating mu as constant -- 
      do k = kbegin,kup
        kp = k + 1
        do j = jlow,jup

c     -- extrapolate fmu to get it at 1/2 nodes --
          hfmu = 0.5d0*(fmu(j,k)+fmu(j,kp))
c     -- laminar + turbulent viscosity at 1/2 nodes --
          t1 = hfmu + turmu(j,k)
c     -- mu_l/Pr_l + mu_t/Pr_t --
          t2 = hfmu*prlinv + turmu(j,k)*prtinv

c     -- evaluate alfas at k+1/2 factor 1/2 already in hre--
          ah1 = ( a1(j,k) + a1(j,kp) )*t1
          ah2 = ( a2(j,k) + a2(j,kp) )*t1
          ah3 = ( a3(j,k) + a3(j,kp) )*t1
          ah4 = ( a4(j,k) + a4(j,kp) )*t2*gamma
                                  
          ueta  = q2(j,kp,2)-q2(j,k,2)  
          veta  = q2(j,kp,3)-q2(j,k,3)   
          uavg  = 0.5d0*(q2(j,kp,2)+q2(j,k,2))
          vavg  = 0.5d0*(q2(j,kp,3)+q2(j,k,3))

          dk  (1,1,j,k) = -( ah1*q2(j,k,2)  + ah2*q2(j,k,3)) *q2(j,k,1)
          dpk (1,1,j,k) =  ( ah1*q2(j,kp,2) + ah2*q2(j,kp,3))*q2(j,kp,1)
          dk  (2,1,j,k) =  ah1*q2(j,k,1)                              
          dpk (2,1,j,k) = -ah1*q2(j,kp,1)
          dk  (3,1,j,k) =  ah2*q2(j,k,1)
          dpk (3,1,j,k) = -ah2*q2(j,kp,1)
          dk  (4,1,j,k) = 0.d0
          dpk (4,1,j,k) = 0.d0
          dk  (1,2,j,k) = -( ah2*q2(j,k,2) + ah3*q2(j,k,3))  *q2(j,k,1)
          dpk (1,2,j,k) =  ( ah2*q2(j,kp,2)+ ah3*q2(j,kp,3))*q2(j,kp,1)
          dk  (2,2,j,k) =  ah2*q2(j,k,1)                              
          dpk (2,2,j,k) = -ah2*q2(j,kp,1)                            
          dk  (3,2,j,k) =  ah3*q2(j,k,1)                              
          dpk (3,2,j,k) = -ah3*q2(j,kp,1)                            
          dk  (4,2,j,k) = 0.d0                                          
          dpk (4,2,j,k) = 0.d0   
 

          S1=ah1*ueta+ah2*veta
          S2=ah2*ueta+ah3*veta
                                     
          dk  (1,3,j,k) = ( -ah4*q2(j,k,4)+0.5d0*S1*q2(j,k,2)  
     &         +0.5d0*S2*q2(j,k,3) )*q2(j,k,1)
     &         + uavg*dk(1,1,j,k) + vavg*dk(1,2,j,k)

          dpk (1,3,j,k) =( ah4*q2(j,kp,4)+0.5d0*S1*q2(j,kp,2)  
     &         +0.5d0*S2*q2(j,kp,3) )*q2(j,kp,1) 
     &         + uavg*dpk(1,1,j,k) + vavg*dpk(1,2,j,k)    

          dk  (2,3,j,k) = -ah4*q2(j,k,2)*q2(j,k,1)+uavg*dk(2,1,j,k)+
     &         vavg*dk(2,2,j,k)  - 0.5d0*S1 * q2(j,k,1)         
          dpk (2,3,j,k) =ah4*q2(j,kp,2)*q2(j,kp,1)+uavg*dpk(2,1,j,k)+
     &         vavg*dpk(2,2,j,k) - 0.5d0*S1 * q2(j,kp,1)
     
          dk  (3,3,j,k) = -ah4*q2(j,k,3)*q2(j,k,1)+uavg*dk(3,1,j,k)+
     &         vavg*dk(3,2,j,k) - 0.5d0*S2 * q2(j,k,1) 
          dpk (3,3,j,k) = ah4*q2(j,kp,3)*q2(j,kp,1)+uavg*dpk(3,1,j,k)+
     &         vavg*dpk(3,2,j,k)- 0.5d0*S2 * q2(j,kp,1) 
     
          dk  (4,3,j,k) =  ah4*q2(j,k,1)                              
          dpk (4,3,j,k) = -ah4*q2(j,kp,1)                            
        end do
      end do

      if (turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kup
          do j = jlow,jup
            do n = 1,3              
              dk(5,n,j,k) = 0.d0
              dpk(5,n,j,k) = 0.d0
            end do
          end do
        end do
      end if

c     -- compute derivative terms due to laminar and turbulent (sa)
c     viscosity: dmu is negative to be consistent with inviscid terms --
      do k = kbegin,kup
        kp = k + 1
        do j = jlow,jup

c     -- calculate viscous flux vector without viscosity terms --
          r1    = 1.d0/q(j,k,1)                                         
          rp1   = 1.d0/q(j,kp,1)                                       
          up1   = q(j,kp,2)*rp1                                     
          u1    = q(j,k,2)*r1                                      
          vp1   = q(j,kp,3)*rp1                                     
          v1    = q(j,k,3)*r1                                      
          ueta  = up1 - u1                                            
          veta  = vp1 - v1                                            
          c2eta = gamma*(press(j,kp)*rp1 - press(j,k)*r1)/gami

c     -- evaluate alfas at k+1/2 factor 1/2 already in hre--
          ah1 = a1(j,kp) + a1(j,k)
          ah2 = a2(j,kp) + a2(j,k)
          ah3 = a3(j,kp) + a3(j,k)
          ah4 = a4(j,kp) + a4(j,k)

          vflux1 = ah1*ueta + ah2*veta
          vflux2 = ah2*ueta + ah3*veta
          vflux3 = 0.5d0*(up1 + u1)*vflux1 + 0.5d0*(vp1 +v1)*vflux2 

c     -- dmu/dQ terms factor 0.5d0 from averaging of mu--
          do n = 1,ndim
            dk(n,1,j,k) = dk(n,1,j,k) - 0.5d0*vflux1*(dmul(n,j,k) +
     &            dmut(n,j,k))  
            dk(n,2,j,k) = dk(n,2,j,k) - 0.5d0*vflux2*(dmul(n,j,k) +
     &            dmut(n,j,k)) 
            dk(n,3,j,k) = dk(n,3,j,k) - 0.5d0*( vflux3*(dmul(n,j,k) +
     &            dmut(n,j,k)) +ah4*c2eta*(dmul(n,j,k)*prlinv +
     &            dmut(n,j,k)*prtinv) )  

            dpk(n,1,j,k) = dpk(n,1,j,k) - 0.5d0*vflux1*(dmul(n,j,kp) +
     &            dmut(n,j,kp)) 
            dpk(n,2,j,k) = dpk(n,2,j,k) - 0.5d0*vflux2*(dmul(n,j,kp) +
     &            dmut(n,j,kp)) 
            dpk(n,3,j,k) = dpk(n,3,j,k) - 0.5d0*( vflux3*(dmul(n,j,kp)
     &            + dmut(n,j,kp)) + ah4*c2eta*(dmul(n,j,kp)*prlinv + 
     &            dmut(n,j,kp)*prtinv))

            if (n.eq.5) then
              if (abs(dk(5,3,j,k)) .lt. 5.d-16) then
                dk(5,1,j,k) = 0.d0
                dk(5,2,j,k) = 0.d0
                dk(5,3,j,k) = 0.d0
              end if
              if (abs(dpk(5,3,j,k)) .lt. 5.d-16) then
                dpk(5,1,j,k) = 0.d0
                dpk(5,2,j,k) = 0.d0
                dpk(5,3,j,k) = 0.d0
              end if
            end if

          end do
        end do
      end do

      return                                                            
      end                       !ivy_q
