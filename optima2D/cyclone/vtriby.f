      subroutine vtriby(JDIM,KDIM,A,B,C,X,F,KL,KU,JL,JU)                
c                                                                       
      DIMENSION A(JDIM,KDIM),B(JDIM,KDIM),C(JDIM,KDIM),                 
     >          X(JDIM,KDIM),F(JDIM,KDIM)                               
c                                                                       
      DO 10 J = JL,JU                                                   
      X(J,KL)=C(J,KL)/B(J,KL)                                           
      F(J,KL)=F(J,KL)/B(J,KL)                                           
10    CONTINUE                                                          
      KLP1 = KL +1                                                      
      DO 1 K=KLP1,KU                                                    
         DO 11 J = JL,JU                                                
         Z=1./(B(J,K)-A(J,K)*X(J,K-1))                                  
         X(J,K)=C(J,K)*Z                                                
         F(J,K)=(F(J,K)-A(J,K)*F(J,K-1))*Z                              
11       CONTINUE                                                       
1     continue                                                          
c                                                                       
      KUPKL=KU+KL                                                       
      DO 2 K1=KLP1,KU                                                   
         DO 12 J = JL,JU                                                
         K=KUPKL-K1                                                     
         F(J,K)=F(J,K)-X(J,K)*F(J,K+1)                                  
12       CONTINUE                                                       
2     continue                                                          
c                                                                       
      RETURN                                                            
      END                                                               
