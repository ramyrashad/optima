#ifndef MULT3D_H
#define MULT3D_H

#include "grid.h"
#include "grid3Dfe.h"

void read_mult3D (char *filename, vector <Node>& nodes, vector <Vol>& cells,
		  int* &imax, int* &jmax, int* &kmax, int& nblk);

void read_topo (const char* gridfile, vector <Node>& nodes, const int& nblk,
		int* &imax, int* &jmax, int* &kmax);

void find_overlap(int* &imax, int* &jmax,
		  int* &ncumu, const int& jsta, const int& jend, 
		  const int& adista, const int& adjsta, const int& adksta, 
		  const int& adiend, const int& adjend, const int& adkend, 
		  const int& adblk, const int& dit1, const int& dit2, 
		  vector <int>& ID);

//void deform_boundary (vector<Node>& nodes, int* &imax, int* &jmax, 
//		      int* &kmax, const int& nblk, double& delta);

void change_dihedral (vector<Node>& nodes, int* &imax, int* &jmax, 
		      int* &kmax, const int& nblk, double& delta);

void change_sweep (vector<Node>& nodes, int* &imax, int* &jmax, 
	    int* &kmax, const int& nblk, double& delta);

void change_twist (vector<Node>& nodes, int* &imax, int* &jmax, 
	    int* &kmax, const int& nblk, double& delta);

void write_mult3D(const char* gridout, const vector <Node>& node, 
		int* &jmax,  int* &kmax,  int* &mmax, int& nblk);
 
void write_mult3D_small_endian(const char* gridout, const vector <Node>& node, 
			       int* &jmax, int* &kmax, int* &mmax, int& nblk);

#endif   // MULT3D_H

