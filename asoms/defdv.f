c----------------------------------------------------------------------
c     -- define design variables --
c     -- modify airfoil shape for inverse design --
c     -- m. nemec --
c----------------------------------------------------------------------
      subroutine defdv(ifoil)

      implicit none

#include "mxfoil.inc"
#include "bsp.inc"

      integer ifoil

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      integer ndv, nbdv, ntdv, idvi(ibsnc), idvs(ibsnc), idvf(ibsnc)
      double precision dvs(ibsnc)
      common/dvinfo/ dvs, idvi, idvs, idvf, ndv, nbdv, ntdv

c     -- local variables --
      integer iseg, j, imod, ne, idvb, idve, iskp, nc, ivar(3), ndvt
      double precision tmp1
      logical errf, cont, moda, answer

      if (nbseg(ifoil).gt.0) write (*,10) ifoil
 10   format (/3x'Current airfoil:',i2)

      do iseg=1,nbseg(ifoil)

         if ( isurf(ifoil,iseg) .eq. 2) then
            write (*,20) iseg
 20         format (/3x,'Working on B-spline segment:',i3,4x,
     &           'Location: lower surface')
         else
            write (*,25) iseg
 25         format (3x,'Working on B-spline segment:',i3,4x,
     &           'Location: upper surface')
         end if

c     -- select design variables --
         write (*,30) 
 30      format (3x,'Current B-spline control points are:')
         nc = nbcp(ifoil,iseg)
         do j = 1,nc
            write (*,300) j,bcp(j,1,ifoil,iseg),bcp(j,2,ifoil,iseg)
         end do

         write (*,40) 
 40      format (/3x,
     &        'Define control points to be used as design variables:',
     &        /3x,'Enter Control Point index OR', /3x,
     &        'Enter start end increment Control Point index OR', /3x,
     &        'Enter -1 to EXIT')

         ndvt = 0
         ivar(1) = -1
         ivar(2) = -1
         ivar(3) = -1 

c     -- main loop for design variables --
 1000    continue

c         ndv = ndv + 1
         ndvt = ndvt + 1
         
         write (*,50)
 50      format (/3x,'Input design variable(s)   i>  ',$)
         ne = 3
         call readi( ne, ivar, errf)

         if (errf) then
            write (*,*) 'Your input is wrong => Try again!!'
            ndvt = ndvt - 1
            errf = .false.
            goto 1000
         end if

c     -- exit condition --
         if (ivar(1).eq.-1) goto 1100 

c     -- ne holds the number of input integers --
         if (ne .eq. 1) then
c     -- one control point entered --
            idvi(ndv+ndvt) = ivar(1)
            idvs(ndv+ndvt) = iseg
            idvf(ndv+ndvt) = ifoil
            dvs(ndv+ndvt) = bcp(idvi(ndv+ndvt),2,ifoil,iseg)
            ivar(1) = -1
            goto 1000
            
         else if (ne .eq. 2) then
c     -- input doesn't make sense --
            write (*,*) 'You must enter increment => Try again!'
            ndvt = ndvt - 1
            goto 1000

         else if (ne .eq. 3) then
c     -- triplet entered --
            idvb = ivar(1)
            idve = ivar(2)
            iskp = ivar(3)

            cont = .true.
            do while (cont)
               idvi(ndv+ndvt) = idvb
               idvs(ndv+ndvt) = iseg
               idvf(ndv+ndvt) = ifoil
               dvs(ndv+ndvt) = bcp(idvi(ndv+ndvt),2,ifoil,iseg)
               idvb = idvb + iskp
               ndvt = ndvt + 1
               if (idvb.gt.idve) then
                  ndvt = ndvt - 1
                  cont = .false.
               end if
            end do
            ivar(1) = -1
            ivar(2) = -1
            ivar(3) = -1

            goto 1000

         else
            write (*,*) 'defdv: ne value is wrong => program stopped'
            stop
         end if

 1100    continue 

         ndvt = ndvt - 1

         if (ndvt.ne.0) then
            write (*,60) ndvt
 60         format (/3x,'The selected',i4,' design variable(s) are:')
            do j = 1,ndvt
               write (*,400) j, idvi(ndv+j), dvs(ndv+j)
            end do
            call askl('Modify shape of the airfoil?^',moda)
         else
            write (*,70) 
 70         format (/3x,'No design variables selected.')
            moda = .false.
         end if

         if (moda) then
c     -- modify shape for inverse design --
            cont = .true.
            do while (cont)
               write (*,80)
 80            format (3x,'Current list of design variables:')
               do j = 1,ndvt
                  write (*,400) j, idvi(ndv+j), dvs(ndv+j)
               end do  

 100            continue

               call aski('Modify which design variable (999 exit)?^',
     &              imod)

               if ( imod.eq.999 ) go to 200
               write (*,110) dvs(ndv+imod)
 110           format (3x,'Current value of the design variable is', 
     &              e14.6)
               tmp1 = dvs(ndv+imod)
               call askr('Enter new y-coordinate^',dvs(ndv+imod))

               write (*,120) ( dvs(ndv+imod) - tmp1 )/ tmp1*1.d2
 120           format (3x,'The % change for the d.v. is:', f6.2 )

               call askl('Is this acceptable?^',answer)
               if (.not. answer) dvs(ndv+imod) = tmp1
               go to 100

 200            continue

               call askl('Change design variable(s)?^',answer)
               if ( .not.answer ) cont=.false. 
               
            end do
         end if

         ndv = ndv+ndvt
      end do

 300  format(i4,2e14.5)
 400  format(2i4,e14.5)

      return 
      end                       !defdv
