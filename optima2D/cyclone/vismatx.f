      subroutine vismatx(jdim,kdim,q,press,xy,xyj,turmu,fmu,a,b,c,d,e,  
     >                   dj,ddj)                                        
c
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),xyj(jdim,kdim),xy(jdim,kdim,4)           
      dimension turmu(jdim,kdim),press(jdim,kdim),fmu(jdim,kdim)        
c                                                                       
      dimension a(jdim,kdim,4),b(jdim,kdim,4,4),c(jdim,kdim,4,4),       
     >          d(jdim,kdim,4,4),e(jdim,kdim,4)                         
      dimension dj(jdim,kdim,3,4),ddj(jdim,kdim,3,4)                    
c                                                                       
c   note  :: a and e used as temporaries                                
c                                                                       
c                                                                       
c                                                                       
      data pr,prtr,frt/.72,.8,1.333333/                                 
c                                                                       
c                                                                       
      hd = dt * .5 * thetadt/(1. + phidt)                               
      hre = hd/re                                                       
      gpr = gamma/pr                                                    
c                                                                       
c                                                                       
      do 10 j = jbegin,jend                                             
         do 10 k = klow,kup                                             
c                                                                       
            rinv   = 1./q(j,k,1)                                        
            exs    = xy(j,k,1)**2                                       
            eys    = xy(j,k,2)**2                                       
            exy    = xy(j,k,1)*xy(j,k,2)*.3333333                       
            djac   = hre/xyj(j,k)                                       
c                                                                       
            e( j, k,1) = djac*( frt*exs + eys)                          
            e( j, k,2) = djac*exy                                       
            e( j, k,3) = djac*( exs + frt*eys)                          
            e( j, k,4) = gpr*djac*( exs + eys)                          
            a( j, k,1) = rinv                                           
            a( j, k,2) = rinv*q(j,k,2)                                  
            a( j, k,3) = rinv*q(j,k,3)                                  
            a( j, k,4) = rinv*q(j,k,4)                                  
     >                   - ( a(j,k,2)**2 + a(j,k,3)**2 )                
c                                                                       
10    continue                                                          
c                                                                       
      do 20 j = jbegin,jup                                              
         jp = jplus(j)                                                  
      do 20 k = klow,kup                                                
c                                                                       
c  turmu is at j,k+1/2 so average backward in k to get turmu at k points
c  average forward in j for compact differencing in xi                  
cthp-4/29/87  for now laminar flow in xi viscous cases only             
cthp-4/29/87            turm   = 0.25*(turmu(jp,k) + turmu(j,k) +       
cthp-4/29/87     >                     turmu(jp,k-1)+ turmu(j,k-1))     
            turm   = 0.0                                                
            fmum   = 0.5*(fmu(jp,k)   + fmu(j,k)  )                     
            vnu    = fmum + turm                                        
            gkap   = fmum + prtr*turm                                   
                                                                        
            cc1 = e( j, k,1) + e( jp, k,1)                              
            cc2 = e( j, k,2) + e( jp, k,2)                              
            cc3 = e( j, k,3) + e( jp, k,3)                              
            cc4 = e( j, k,4) + e( jp, k,4)                              
            cc1 = cc1*vnu                                               
            cc2 = cc2*vnu                                               
            cc3 = cc3*vnu                                               
            cc4 = cc4*gkap                                              
c                                                                       
            dj( j, k,1,1) =                                             
     >         - ( cc1*a(j,k,2) + cc2*a(j,k,3))*a(j,k,1)                
            ddj( j, k,1,1) =                                            
     >         - ( cc1*a(jp,k,2) + cc2*a(jp,k,3))*a(jp,k,1)             
            dj( j, k,1,2) = cc1*a( j, k,1)                              
            ddj( j, k,1,2) = cc1*a( jp, k,1)                            
            dj( j, k,1,3) = cc2*a( j, k,1)                              
            ddj( j, k,1,3) = cc2*a( jp, k,1)                            
            dj( j, k,1,4) = 0.                                          
            ddj( j, k,1,4) = 0.                                         
            dj( j, k,2,1) =                                             
     >        - ( cc2*a(j,k,2) + cc3*a(j,k,3))*a(j,k,1)                 
            ddj( j, k,2,1) =                                            
     >        - ( cc2*a(jp,k,2) + cc3*a(jp,k,3))*a(jp,k,1)              
            dj( j, k,2,2) = cc2*a( j, k,1)                              
            ddj( j, k,2,2) = cc2*a( jp, k,1)                            
            dj( j, k,2,3) = cc3*a( j, k,1)                              
            ddj( j, k,2,3) = cc3*a( jp, k,1)                            
            dj( j, k,2,4) = 0.                                          
            ddj( j, k,2,4) = 0.                                         
c                                                                       
            dj( j, k,3,1) = -(cc4*a(j,k,4) + cc1*a(j,k,2)**2 +          
     >                     2.*cc2*a(j,k,2)*a(j,k,3) +                   
     >                     cc3*a(j,k,3)**2)*a(j,k,1)                    
            ddj( j, k,3,1) = -(cc4*a(jp,k,4) + cc1*a(jp,k,2)**2 +       
     >                     2.*cc2*a(jp,k,2)*a(jp,k,3) +                 
     >                     cc3*a(jp,k,3)**2)*a(jp,k,1)                  
            dj( j, k,3,2) = -cc4*a(j,k,2)*a(j,k,1) - dj(j,k,1,1)        
            ddj( j, k,3,2) = -cc4*a(jp,k,2)*a(jp,k,1) - ddj(j,k,1,1)    
            dj( j, k,3,3) = -cc4*a(j,k,3)*a(j,k,1) - dj(j,k,2,1)        
            ddj( j, k,3,3) = -cc4*a(jp,k,3)*a(jp,k,1) - ddj(j,k,2,1)    
            dj( j, k,3,4) = cc4*a( j, k,1)                              
            ddj( j, k,3,4) = cc4*a( jp, k,1)                            
c                                                                       
   20 continue                                                          
c                                                                       
      do 30 j=jlow,kup                                                  
      jr = jminus(j)                                                    
      do 30 n=2,4                                                       
      do 30 m=1,4                                                       
         do 30 k = klow,kup                                             
            b(j,k,n,m) = b(j,k,n,m) - dj(jr, k,n-1,m)                   
            c(j,k,n,m) = c(j,k,n,m) + dj(j, k,n-1,m) +ddj(jr, k,n-1,m)  
            d(j,k,n,m) = d(j,k,n,m) - ddj(j, k,n-1,m)                   
   30 continue                                                          
c                                                                       
c                                                                       
      return                                                            
      end                                                               
