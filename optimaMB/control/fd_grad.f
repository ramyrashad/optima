c----------------------------------------------------------------------
c     -- calculate gradient using finite differences --
c     -- based on: "numerical methods for unconstrained optimization
c     and nonlinear equations", dennis and schnabel, pg. 322. --
c     -- m. nemec, aug. 2001 --
c----------------------------------------------------------------------
      subroutine fd_grad( ifirst, nmax, indx, q, xy, xyj, x, y, turmu,
     &     vort, turre, fmu, press, precon, sndsp, xit, ett, ds,vk, ve,
     &     coef2x, coef4x, coef2y, coef4y, s, uu, vv, ccx, ccy,spectxi,
     &     specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja, icol,xs, ys)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer ifirst, nmax, ntvar,ib,lg,lq,i,ifoil,j
      double precision q(*),             press(*),       sndsp(*)
      double precision s(*),             xy(*),          xyj(*)
      double precision xit(*),           ett(*),         ds(*)
      double precision x(*),             y(*),           turmu(*)
      double precision vort(*),          turre(*),       fmu(*)
      double precision vk(*),            ve(*),          tmp(*)
      double precision uu(*),            vv(*),          ccx(*)
      double precision ccy(*),           coef2x(*),      coef2y(*)
      double precision coef4x(*),        coef4y(*),      precon(*)
      double precision spectxi(*),       specteta(*),    wk1(*)

      integer ia(*), ja(*), ipa(*), jpa(*), icol(*), indx(*)

      double precision xs(*),ys(*),objp,objm,cosang,sinang
      double precision CalcObj,clt_tmp,cdt_tmp,dvtmp,stepsize

c     -- local variables --
      double precision qs(maxjkq), bapf(ibody,2)
      double precision xpres(maxjk), ypres(maxjk)
cmpr
      double precision qps(maxjkq),qos(maxjkq),turps(maxjk),turos(maxjk)

      clt_tmp = clt
      cdt_tmp = cdt

c     -- store base-state q and grid --
      do ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)
         call copy_array( jmax(ib), kmax(ib), 4, q(lq), qs(lq))

c     -- save present grid --
         call copy_array( jmax(ib), kmax(ib), 1, x(lg), xpres(lg))
         call copy_array( jmax(ib), kmax(ib), 1, y(lg), ypres(lg))

c     -- copy original grid into x and y --
         call copy_array( jmax(ib), kmax(ib), 1, xs(lg), x(lg))
         call copy_array( jmax(ib), kmax(ib), 1, ys(lg), y(lg))
      end do

c     -- central difference gradient approximation --
c     -- second order accurate --
      do i = 1,ndv
         dvtmp = dvs(i)
         stepsize = fd_eta*dvs(i)

         if ( abs(stepsize) .lt. 1.e-11 ) then
            stepsize = sign(1.0,stepsize)*1.e-11
            write (n_out,*) 'warning small stepsize'
         end if

c     -- evaluate (+) state --
         dvs(i) = dvs(i) + stepsize
         stepsize = dvs(i) - dvtmp

c     -- regrid domain --
         if ( i.le.ngdv ) then
c     -- store bap --            

            ifoil = idvf(i)
            do j = 1, ibap(ifoil) 
               bapf(j,1) = bap(j,1,ifoil)
               bapf(j,2) = bap(j,2,ifoil)
            end do
            call regrid ( (i), x, y)

         else
c     -- restore grid for present design iteration --
            do ib = 1,nblks
               lg = lgptr(ib)
               call copy_array( jmax(ib), kmax(ib), 1, xpres(lg), x(lg))
               call copy_array( jmax(ib), kmax(ib), 1, ypres(lg), y(lg))
            end do
            alpha = dvs(i)
         end if

c     -- calculate new objective function value at (+) --
         objp = CalcObj(ifirst,nmax,indx,q,qps,qos, xy, xyj,x, y, turmu,
     &        vort, turre,turps,turos,fmu,press,precon,sndsp,xit,ett,ds, 
     &        vk,ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,ccx, ccy,
     &        spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,ia, ja, icol)

c     -- restore base-state solution and grid --
         do ib = 1,nblks
            lq = lqptr(ib)
            call copy_array( jmax(ib), kmax(ib), 4, qs(lq), q(lq))
         end do

c     -- evaluate (-) state --
         dvs(i) = dvtmp - stepsize

         if ( i.le.ngdv ) then
c     -- restore bap --
            ifoil = idvf(i)  
            do j = 1, ibap(ifoil) 
               bap(j,1,ifoil) = bapf(j,1)
               bap(j,2,ifoil) = bapf(j,2)
            end do
c     -- restore grid --
            do ib = 1,nblks
               lg = lgptr(ib)
               call copy_array( jmax(ib), kmax(ib), 1, xs(lg), x(lg))
               call copy_array( jmax(ib), kmax(ib), 1, ys(lg), y(lg))
            end do
            call regrid ( (i), x, y)

         else 
            alpha = dvs(i)
         end if

c     -- calculate new objective function value (-) --
         objm = CalcObj(ifirst,nmax,indx,q,qps,qos, xy, xyj,x, y, turmu,
     &        vort, turre,turps,turos,fmu,press,precon,sndsp,xit,ett,ds, 
     &        vk,ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,ccx, ccy,
     &        spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,ia, ja, icol)
   
c     -- restore base-state solution --
         do ib = 1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            call copy_array( jmax(ib), kmax(ib), 4, qs(lq), q(lq))
            call copy_array( jmax(ib), kmax(ib), 1, xs(lg), x(lg))
            call copy_array( jmax(ib), kmax(ib), 1, ys(lg), y(lg))
         end do
         
c     -- evaluate gradient --
         grad(i) = (objp - objm) / (2.0*stepsize)
         write (n_out,*) 'grad i:',i,grad(i)
         call flush(n_out)
c     
         dvs(i) = dvtmp

         if (i.le.ngdv) then
c     -- restore bap --
            ifoil = idvf(i)  
            do j = 1, ibap(ifoil) 
               bap(j,1,ifoil) = bapf(j,1)
               bap(j,2,ifoil) = bapf(j,2)
            end do
         else
            alpha = dvs(i)
            cosang = cos(pi*alpha/180.0)                               
            sinang = sin(pi*alpha/180.0)
            uinf   = fsmach*cosang
            vinf   = fsmach*sinang
         end if
      end do

      clt = clt_tmp
      cdt = cdt_tmp

c     10   format(3x, 'D.V. #', i3,'  CYCLONE Resid.:', e10.3,
c     &      '  S-A Resid.:', e10.3)
c     
c     20   format(3x, 'D.V. #', i3,'  Flow Resid.:', e10.3)

      return
      end                       !fd_grad
