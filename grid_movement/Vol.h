#ifndef VOL_H
#define VOL_H

#include "Node.h"

class Vol
{  
 private:
   int ID;
   int nvertices;
   int dim;   // dimension
   int *vertexID;

  public:

  //Default constructor
  Vol(); 

  void clearmem();
  void add_nei (Node **corners);

  //Constructor of 8 node brick element cell
  Vol (Node** corners, const int& cellID, const int& nv);
   
  ~Vol();       // Destructor

  // accessor function
 int get_ID() const { return ID;}
 int *get_vertexID() const {return vertexID;}
 int get_nvertices() const {return nvertices;} 
 friend ostream& operator <<(ostream& out, const Vol& a);

};

#endif















