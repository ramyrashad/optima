c----------------------------------------------------------------------
c     -- unsteady airfoil optimization --
c
c     written by: markus rumpfkeil
c     date: May 2006
c----------------------------------------------------------------------

      subroutine unsteadyoptairfoil (nmax,indx,q,qps,qos,xy,xyj,x,y,
     &     turmu,vort,turre,turps,turos,fmu,press,precon,sndsp,xit,ett,
     &     ds,vk,ve,coef2x,coef4x,coef2y,coef4y,s, uu, vv, ccx, ccy,
     &     spectxi, specteta,tmp,wk1,ntvar,ipa,jpa,ia,ja,icol,mlim)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"
#include "../include/mbcom.inc"
#include "../include/scratch.inc"
#include "../include/turb.inc"

      double precision q(*),             press(*),      sndsp(*)
      double precision s(*),             xy(*),         xyj(*)
      double precision xit(*),           ett(*),        ds(*)
      double precision x(*),             y(*),          turmu(*)
      double precision vort(*),          turre(*),      fmu(*)
      double precision vk(*),            ve(*),         tmp(*)
      double precision uu(*),            vv(*),         ccx(*)
      double precision ccy(*),           coef2x(*),     coef2y(*)
      double precision coef4x(*),        coef4y(*),     precon(*)
      double precision spectxi(*),       specteta(*)
      double precision wk1(*),           qtar(maxjkq),  turretar(maxjkq)
      double precision qps(*),           qos(*),        meanpress(maxjk)
      double precision turps(*),         turos(*),      cptar(4*maxj)
c     ESDIRK adjoint variables arrays with pointers ldhatqptr, ldhattptr, respectively
      double precision,dimension(:),allocatable :: psies,psites
c     BDF2 adjoint variables arrays with pointers lqptr, lgptr, respectively
      double precision psi(maxjkq),psiold(maxjkq)
      double precision psit(maxjk),psitold(maxjk)

      integer ia(*),ja(*),ipa(*),jpa(*),icol(9),indx(*),inttmp(maxjk*5)

      double precision,dimension(:),allocatable :: as,pa
      
c     -- shuffle arrays required to keep track of row exchanges,
c     necessary for the adjoint problem but also used in the n-k flow
c     solver --
c     -- iex: integer array of row exchanges --
c     -- bcf: boundary condition factors --
      integer iex(maxjb,4,4,mxbfine),stage,stopstage,stopstage2
      double precision bcf(maxjb,4,4,mxbfine)

      integer nmax, ifirst, ntvar,lq,lg,skipend
      double precision scalef,deltat,deltatbig,dtbigtmp,dtsmalltmp
      double precision deltattmp,dfobjdQ(maxjkq),cdmean,clmean,fac

c     -- BFGS stuff --   
      character*60     task, csave, command, optrestart
      logical          lsave(4), precmat, jacmat , imps
      integer          ioprint,i,j,k,ib,count,count2,mlim
      integer          nbd(ndv), iwa(3*ndv), isave(44)
      integer          nout, nouttemp
      double precision fobj, factr, pgtol,dvssave(ndv+1), 
     &                 low(ndv+1),up(ndv+1),fobjp,fobjm,fobjpp,fobjmm, 
     &                 fgrad(ndv), dsave(29)
      double precision,dimension(:),allocatable :: wa 

c     -- arrays for initial grid --
      double precision xsave(maxjk),ysave(maxjk)

c     -- Gradient stuff --
      double precision Rp(maxjk*jacb), Rm(maxjk*jacb)
      double precision Rpp(maxjk*jacb), Rmm(maxjk*jacb)
      double precision Rsap(maxjk), Rsam(maxjk)
      double precision Rsapp(maxjk), Rsamm(maxjk)
      double precision dRdX(maxjkq),dRsadX(maxjk)
      complex*16,dimension(:,:,:,:),allocatable :: dpod
      double precision po(tdim,sdim),win(tdim),ns(sdim,2)

c     -- Output stuff --
      double precision xyjout(maxjk), maxgrad
      integer strlen, namelen,lentmp,ios
      logical objdone

      double precision dtiseq(-5:20),dtmins(-5:20),dtspiseq(-5:20)
      double precision vlx(-5:20),vnx(-5:20),vly(-5:20),vny(-5:20)
      integer iends(-5:20),jacdtseq(-5:20),isbiter(-5:20,2)      
      integer isequal
      common/mesup/ dtiseq, dtmins,dtspiseq, vlx, vnx, vly, vny, 
     &        isequal, iends, jacdtseq, isbiter

cmpr  Allocate some of the big arrays dynamically
      allocate(pa(maxjk*jacb2*5),as(maxjk*jacb2*9))
      allocate(wa(2*mlim*ndv+4*ndv+12*mlim*mlim+12*mlim))
      allocate(psies(6*maxjkq),psites(6*maxjk))
      allocate(dpod(Nobs,tdim,sdim,3))

c     -- save initial grid --

      do ib = 1,nblks
         lg = lgptr(ib)
         call copy_array( jmax(ib), kmax(ib),1,x(lg),xsave(lg))
         call copy_array( jmax(ib), kmax(ib),1,y(lg),ysave(lg))
      end do

      objdone=.false.

c     Define name for restart file

      namelen =  strlen(restart_file_prefix)
      optrestart = restart_file_prefix
      optrestart(namelen+1:namelen+7) = '.orest'

c     open output file (copy restart.ohis to actual file.ohis if opt_restart)

      if (opt_restart) then

         command = 'cp '      
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(4:3+namelen+5) = filena
         lentmp = 3+namelen+5+1  
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(lentmp+1:lentmp+namelen+5) = filena

         call system(command)

         open(unit=n_ohis,file=filena,status='old',access='append')

      else

         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix      
         filena(namelen+1:namelen+6) = '.ohis'
         open(unit=n_ohis,file=filena,status='new')

c     make sure the restart file.ohis is emptied, too

         command = 'cp '      
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(4:3+namelen+5) = filena
         lentmp = 3+namelen+5+1       
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(lentmp+1:lentmp+namelen+5) = filena
         
         call system(command)

c     open restart file

         open(unit=33,file=optrestart,form='unformatted',
     &        status='unknown')
         

      end if


      gami=0.4d0
      gamma=1.4d0

c     regulation constant
      scalef=0.d0

      call set_ajk

c     Calculate the number of outputs

      deltat=noutevery*dtiseq(1)
      deltatbig=nouteverybig*dtbig

      skipend=nkskip/nouteverybig

      nout=1
      nouttemp=0

      if (outtime) then
         nout=nkiends/noutevery+skipend
      end if

      if (igrad .eq. 1) then
         nouttemp=nkiends/noutevery+skipend
      end if







c$$$c     subtract mean pressure in each grid point
c$$$      do count = 1,nouttemp
c$$$
c$$$c     -- read target distribution at this time step if existent --
c$$$         call iomarkus(4,count,maxj,maxk,qtar,xyj,turretar)
c$$$            
c$$$c     -- halo column copy for q vector --
c$$$         call halo_q(1,nblks,maxj,maxk,qtar,turretar) 
c$$$
c$$$c     -- calculate pressure and sound speed --
c$$$         do ib=1,nblks
c$$$            lq = lqptr(ib)
c$$$            lg = lgptr(ib)
c$$$            call calcps(ib, jmax(ib), kmax(ib), qtar(lq), press(lg),
c$$$     &           sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
c$$$     &           jbegin2(ib),jend2(ib),kbegin2(ib),kend2(ib))
c$$$         end do
c$$$
c$$$c     -- sum pressure for all nodes and divide by total number of time steps
c$$$         do j=1,maxjk
c$$$            meanpress(j)=meanpress(j)+press(j)/nouttemp
c$$$         end do
c$$$
c$$$      end do
c$$$         
c$$$
c$$$c     -- output pressure fluctuation stored in psi1 
c$$$      do j=1,maxjk
c$$$         xyjout(j)=1.0d0
c$$$      end do
c$$$
c$$$      do count = 1,nouttemp
c$$$
c$$$c     -- read target distribution at this time step if existent --
c$$$         call iomarkus(4,count,maxj,maxk,qtar,xyj,turretar)
c$$$            
c$$$c     -- halo column copy for q vector --
c$$$         call halo_q(1,nblks,maxj,maxk,qtar,turretar) 
c$$$
c$$$c     -- calculate pressure and sound speed --
c$$$         do ib=1,nblks
c$$$            lq = lqptr(ib)
c$$$            lg = lgptr(ib)
c$$$            call calcps(ib, jmax(ib), kmax(ib), qtar(lq), press(lg),
c$$$     &           sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
c$$$     &           jbegin2(ib),jend2(ib),kbegin2(ib),kend2(ib))
c$$$         end do
c$$$
c$$$c     -- subtract mean pressure from pressure value in each grid point and store in psi1
c$$$
c$$$         do ib = 1,nblks
c$$$            do j=1,jmax(ib)
c$$$               do i=1,kmax(ib)
c$$$                 
c$$$                  lq=lqptr(ib)+(j-1)+(i-1)*jmax(ib)
c$$$                  lg=lgptr(ib)+(j-1)+(i-1)*jmax(ib)
c$$$
c$$$                  psi(lq)=press(lg)-meanpress(lg)                        
c$$$                    
c$$$               end do
c$$$            end do
c$$$         end do
c$$$                        
c$$$         call iomarkus(5,count,maxj,maxk,psi,xyjout,psit)
c$$$
c$$$      end do
c$$$
c$$$      stop

      
c     We suppress the default output with -1.
      
      ioprint = -1

c     The iteration will stop when
c     
c     (f^k - f^{k+1})/max{|f^k|,|f^{k+1}|,1} <= factr*epsmch
c
c     where epsmch is the machine precision which is automatically
c     generated by the code. Typical values for factr on a computer
c     with 15 digits of accuracy in double precision are:
c     factr=1.d+12 for low accuracy;
c           1.d+7  for moderate accuracy; 
c           1.d+1  for extremely high accuracy.
c     The user can suppress this termination test by setting factr=0.
c
c     The iteration will stop when
c     
c                 max{|proj g_i | i = 1, ..., n} <= pgtol
c
c     where pg_i is the ith component of the projected gradient.
c     The user can suppress this termination test by setting pgtol=0.
      
      factr=0.0d0
      pgtol=0.0d0   

c     First set bounds on the variables.
      
c     We now specify nbd which defines the bounds on the variables:

c       nbd(i)=0 if dvs(i) is unbounded,
c              1 if dvs(i) has only a lower bound,
c              2 if dvs(i) has both lower and upper bounds, 
c              3 if dvs(i) has only an upper bound. 
c              low(i)   specifies the lower bound on dvs(i)
c              up(i)    specifies the upper bounds on dvs(i)




cc     bounds for inverse design
      do i=1,ndv
         nbd(i)=2 
         low(i)=dvs(i)-3.5d-2
         up(i)=dvs(i)+3.5d-2
      end do


c     target design variables for laminar NACA with alpha=20
c$$$      dvs(1)= 0.8E-01
c$$$      dvs(2)= 0.4E-01
c$$$      dvs(3)=-0.4E-01
c$$$      dvs(4)=-0.8E-01

cc     bounds for slat gap and overlap
c$$$      nbd(1)=2 
c$$$      up(1)=dvs(1)+0.01d0
c$$$      low(1)=dvs(1)-0.015d0
c$$$      nbd(2)=2 
c$$$      up(2)=dvs(2)+0.012d0
c$$$      low(2)=dvs(2)-0.0035d0

c$$$      dvs(1)=up(1)
c$$$      dvs(2)=up(2)

cc     target slat gap and overlap
c$$$      dvs(1)=0.022d0
c$$$      dvs(2)=0.011d0

cc     original  slat gap and overlap 
cc      dvs(1)=0.0187720000d0
cc      dvs(2)=0.0050789998d0


c     bounds for Noiseturbinverse

c$$$      do i=1,ndv
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-0.55d0*abs(dvs(i))
c$$$         up(i)=dvs(i)+0.55d0*abs(dvs(i))
c$$$      end do

c     target design variables for Noiseturbinverse
c$$$      dvs(1)= 0.3E-01
c$$$      dvs(2)=-0.3E-01 

cc     bounds for blunt shape design
c$$$      do i=1,ndv
c$$$         nbd(i)=2 
c$$$         if (abs(dvs(i)) .lt. 0.2d0) then
c$$$            low(i)=dvs(i)-4.d-1*abs(dvs(i))
c$$$            up(i)=dvs(i)+4.d-1*abs(dvs(i))
c$$$         else
c$$$            low(i)=dvs(i)-3.d-3
c$$$            up(i)=dvs(i)+3.d-3
c$$$         end if
c$$$      end do

c$$$      do i=1,3
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-7.d-1*abs(dvs(i))
c$$$         up(i)=dvs(i)+7.d-1*abs(dvs(i))
c$$$      end do
c$$$
c$$$      do i=4,5
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-0.8d-1*abs(dvs(i))
c$$$         up(i)=dvs(i)+0.8d-1*abs(dvs(i))
c$$$      end do
c$$$
c$$$      do i=6,8
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-7.d-1*abs(dvs(i))
c$$$         up(i)=dvs(i)+7.d-1*abs(dvs(i))
c$$$      end do
c$$$
c$$$      do i=9,ndv
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-5.d-3
c$$$         up(i)=dvs(i)+1.d-2
c$$$      end do

c$$$      do i=1,4
c$$$         dvs(i)=up(i)
c$$$      end do
c$$$
c$$$      do i=5,8
c$$$         dvs(i)=low(i)
c$$$      end do
c$$$
c$$$      do i=9,ndv
c$$$         dvs(i)=up(i)
c$$$      end do


cc    bounds for NLR airfoil main shape and gap and overlap
c$$$      do i=1,2
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-0.17d-1
c$$$         up(i)=dvs(i)+0.2d-1
c$$$      end do
c$$$
c$$$      nbd(3)=2 
c$$$      low(3)=dvs(3)-0.0006d0
c$$$      up(3)=dvs(3)+0.01d0
c$$$      nbd(ndv)=2 
c$$$      low(ndv)=dvs(ndv)-0.015d0
c$$$      up(ndv)=dvs(ndv)+0.001d0


cc    bounds for suction/blowing BC

c$$$      do i=1,ndv
c$$$         nbd(i)=2
c$$$      end do
c$$$
c$$$      up(1)=fsmach/2.d0
c$$$      low(1)=-up(1)
c$$$c      up(2)=0.5d0      
c$$$c      low(2)=0.05d0
c$$$      up(2)=5.0d0      
c$$$      low(2)=0.5d0
c$$$      up(3)=180.d0     
c$$$      low(3)=0.d0


      if (iobjf .eq. 10 .or. iobjf .eq. 11) then
c     read in surface patches for inverse remote design FWH    

         ib = 1
         ios = 0
         filena = grid_file_prefix
         namelen =  strlen(grid_file_prefix)
         filena(namelen+1:namelen+6) = '.fwhsp'
         open(unit=n_all,file=filena,iostat=ios,err=55,status='old')
         read(n_all,*)
         read(n_all,*)
         read(n_all,*)

 54      read(n_all,58,end=57) Spvec(ib,1),Spvec(ib,2),Spvec(ib,3),
     &        Spvec(ib,4),Spvec(ib,5)

c        adjust Spjbegin,Spjend,Spkbegin,Spkend for halo points 
         do i=2,5
            Spvec(ib,i)=Spvec(ib,i)+nhalo
         end do

         ib = ib + 1
         goto 54
c     
 55      continue
         if (ios.ne.0) then
            write(n_out,*)'** error **'
            write(n_out,*)'file ',filena,' not found!'
            stop
         endif
c     
 57      continue
         close(n_all)
         Nsp = ib - 1 

 58      format(t2,i2,t8,i3,t15,i3,t21,i3,t28,i3)

c        adjust jobs,kobs for halo points 
         jobs=jobs+nhalo
         kobs=kobs+nhalo

      end if


c
c     We start the iteration by initializing task or by reading the optimization restart file
c     
      if (.not. opt_restart) then

          task = 'START'

      else 

         open(unit=34,file=optrestart,form='unformatted',
     &        status='unknown')

         read(34) ndv,mlim
         read(34) (dvs(i),i = 1,ndv) 
         read(34) fobj,scalef,cdmean,clmean
         read(34) objdone
         read(34) (fgrad(i),i = 1,ndv)
         read(34) (wa(i),i = 1,2*mlim*ndv+4*ndv+
     &        12*mlim*mlim+12*mlim)
         read(34) (iwa(i),i = 1,3*ndv)
         read(34) task
         read(34) csave
         read(34) (lsave(i),i = 1,4)
         read(34) (isave(i),i = 1,44)
         read(34) (dsave(i),i = 1,29)
         read(34) (win(i),i = 1,tdim)
         read(34) ((ns(i,j),i=1,sdim),j=1,2)
         read(34) ((po(i,j),i=1,tdim),j=1,sdim)
         read(34) ((((dpod(ib,i,j,k),ib=1,Nobs),i=1,tdim),
     &            j=1,sdim),k=1,3)
                 
         close(34)

cmpr        objdone=.false.

c     open restart file

         open(unit=33,file=optrestart,form='unformatted',
     &        status='unknown')

         write(*,*)
         write(*,*) 'Restart optimization run at iteration', isave(30)+1    
 
         if (objdone) then
            write(*,*) 'Start with Gradient Calculation'
            write(*,*)
            goto 112
         else
            write(*,*) 'Start with Objective Function Calculation'
            write(*,*)
cmpr            goto 112
         end if

      end if



c     ------- the beginning of the optimization loop ----------
      
 111  continue
      
c     This is the call to the L-BFGS-B code in bfgs.f
      
      call setulb(ndv,mlim,dvs,low,up,nbd,fobj,fgrad,factr,pgtol,wa,
     +     iwa,task,ioprint,csave,lsave,isave,dsave)

 112  continue  ! from optimization restart

      if (task(1:2) .eq. 'FG') then
       
         
c     the minimization routine has returned to request the
c     function fobj and gradient fgrad values at the current dvs
         
c     Compute objective function value f

         write(*,*)
         write(*,*) 'Dvs:', (dvs(i),i = 1,ndv)
         write(*,*)
        
         if (.not.sbbc) then
c        Calculate new boundary and regrid from first grid

            do ib = 1,nblks
               lg = lgptr(ib)
               call copy_array( jmax(ib), kmax(ib), 1, xsave(lg), x(lg))
               call copy_array( jmax(ib), kmax(ib), 1, ysave(lg), y(lg))
            end do

            call regrid(-1,x,y)
c           -- output current grid --
            call outgrd(x,y)

c            stop

         else          !suction/blowing BC

            omegaa=dvs(1)
            omegaf=dvs(2)
            sbeta=dvs(3)

         end if

         if (.not. objdone) then

c     -- unsteady flow solve
         
            ifirst = 1          ! indicates first call to flow solve
            restart=.true.

c$$$            ib=NKIENDS
c$$$            count2=NKSKIP
c$$$            NKIENDS=1
c$$$            NKSKIP=0
 
            call flow_solve (ifirst, nmax, indx, q,qps,qos, xy, xyj,x, 
     &        y,turmu,vort,turre,turps,turos,fmu,press,precon,sndsp,xit, 
     &        ett,ds, vk, ve, coef2x, coef4x, coef2y, coef4y, s, uu, 
     &        vv,ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa, 
     &        jpa,ia, ja, icol)

c$$$            NKIENDS=ib
c$$$            NKSKIP=count2
            
c           -- detection of negative Jacobian for optimization runs --
            if (badjac) stop
            
            if (iobjf .eq. 1) then
c           -- inverse design entire domain --            
               call calcobjair(fobj,xyj,nout,skipend,deltat)

            else if (iobjf .eq. 0) then
c           --  mean drag minimization --

            call calcobjair0(fobj,nout,skipend,x,y,xy,xyj,scalef)

            else if (iobjf .eq. 5) then
c           --  (mean drag / mean lift)  minimization --
            
            call calcobjair5(fobj,nout,skipend,x,y,xy,xyj,cdmean,clmean,
     &                       scalef)

            else if (iobjf .eq. 8) then
c           -- inverse remote design --
               call calcobjair8(fobj,nout,skipend,deltat,scalef,x,y)

            else if (iobjf .eq. 10 .or. iobjf .eq. 11) then
c           -- design using FWH--            
               call calcobjairnoise(fobj,nout,skipend,deltat,scalef,x,y,
     &              xy,dpod,win,po,ns,.true.)

            end if              ! iobjf?


            objdone=.true.

c           --save values for a potential optimization restart
  
            rewind(33)
            write(33) ndv,mlim
            write(33) (dvs(i),i = 1,ndv) 
            write(33) fobj,scalef,cdmean,clmean
            write(33) objdone
            write(33) (fgrad(i),i = 1,ndv)
            write(33) (wa(i),i = 1,2*mlim*ndv+4*ndv+
     &           12*mlim*mlim+12*mlim)
            write(33) (iwa(i),i = 1,3*ndv)
            write(33) task
            write(33) csave
            write(33) (lsave(i),i = 1,4)
            write(33) (isave(i),i = 1,44)
            write(33) (dsave(i),i = 1,29)
            write(33) (win(i),i = 1,tdim)
            write(33) ((ns(i,j),i=1,sdim),j=1,2)
            write(33) ((po(i,j),i=1,tdim),j=1,sdim)
            write(33) ((((dpod(ib,i,j,k),ib=1,Nobs),i=1,tdim),
     &                j=1,sdim),k=1,3)

         else

            ifirst = 1          ! indicates first call to flow solve
            restart=.true.

c           - initialize variables
            call initia(1,ifirst,maxj,maxk,q,qos,press,sndsp,s,xy,xyj,
     &           xit,ett,ds,x,y,turmu,fmu,vort,turre,turos,vk,ve,coef2x,
     &           coef4x,coef2y,coef4y,precon,nmax)

c           -calculate grid aspect ratios
            call asprat(x,y,.true.)

            iend = istart  
            
         end if   !.not. objdone

         
         objdone=.false.

         write(*,*)
         write (*,*) 'Objective function value:',fobj, scalef 
         write(*,*)           
 
      

c     Compute gradient g



       
         if (igrad .eq. 0) then
c     -- use finite differences to determine gradient --

            write(*,*)
            write(*,*) 'Start Gradient Calculation a la FD'
            write(*,*)

            do i=1,ndv+1
               dvssave(i)=dvs(i)
            enddo

            do count=1,ndv

               do i=1,ndv+1
                  dvs(i)=dvssave(i)
               enddo

               dvs(count)=dvs(count)+fd_eta

               if (.not.sbbc) then
c              Calculate new boundary and regrid from first grid
  
                  do ib = 1,nblks
                    lg = lgptr(ib)
                    call copy_array(jmax(ib),kmax(ib),1,xsave(lg),x(lg))
                    call copy_array(jmax(ib),kmax(ib),1,ysave(lg),y(lg))
                  end do

                  call regrid(-1,x,y)

               else             !suction/blowing BC

                  omegaa=dvs(1)
                  omegaf=dvs(2)
                  sbeta=dvs(3)

               end if
                             
c             -- unsteady flow solve
             
               ifirst = 1       ! indicates first call to flow solve
               restart=.true.

               call flow_solve (ifirst,nmax,indx, q,qps,qos,xy,xyj,x,y, 
     &         turmu,vort,turre,turps,turos,fmu,press,precon,sndsp,xit, 
     &         ett,ds, vk, ve, coef2x, coef4x, coef2y, coef4y, s, uu, 
     &         vv,ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa, 
     &         jpa,ia, ja, icol)  

               if (iobjf .eq. 1) then
c              -- inverse design entire domain--
                  call calcobjair(fobjp,xyj,nout,skipend,deltat)

               else if (iobjf .eq. 0) then
c              --  mean drag minimization --

                  call calcobjair0(fobjp,nout,skipend,x,y,xy,xyj,scalef)

               else if (iobjf .eq. 5) then
c              --  (mean drag / mean lift)  minimization --
            
                  call calcobjair5(fobjp,nout,skipend,x,y,xy,xyj,cdmean,
     &                             clmean,scalef)

               else if (iobjf .eq. 8) then
c              -- inverse remote design --
                  call calcobjair8(fobjp,nout,skipend,deltat,scalef,x,y)

               else if (iobjf .eq. 10 .or. iobjf .eq. 11) then
c               -- design using FWH--            
                  call calcobjairnoise(fobjp,nout,skipend,deltat,scalef,
     &                 x,y,xy,dpod,win,po,ns,.false.)

               end if           ! iobjf?

c$$$               fgrad(count)=(fobjp-fobj)/fd_eta
c$$$            
c$$$               write (*,*) 'Gradient',count,'value:',fgrad(count)
c$$$
c$$$            end do

               do i=1,ndv+1
                  dvs(i)=dvssave(i)
               enddo

               dvs(count)=dvs(count)-fd_eta

               
               if (.not.sbbc) then
c              Calculate new boundary and regrid from first grid 

                  do ib = 1,nblks
                    lg = lgptr(ib)
                    call copy_array(jmax(ib),kmax(ib),1,xsave(lg),x(lg))
                    call copy_array(jmax(ib),kmax(ib),1,ysave(lg),y(lg))
                  end do

                  call regrid(-1,x,y)

               else                !suction/blowing BC

                  omegaa=dvs(1)
                  omegaf=dvs(2)
                  sbeta=dvs(3)

               end if
               
c             -- unsteady flow solve

               ifirst = 1       ! indicates first call to flow solve
               restart=.true.

               call flow_solve (ifirst,nmax,indx, q,qps,qos,xy,xyj,x,y, 
     &         turmu,vort,turre,turps,turos,fmu,press,precon,sndsp,xit, 
     &         ett,ds, vk, ve, coef2x, coef4x, coef2y, coef4y, s, uu, 
     &         vv,ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa, 
     &         jpa,ia, ja, icol)  

               if (iobjf .eq. 1) then
c              -- inverse design entire domain --
                  call calcobjair(fobjm,xyj,nout,skipend,deltat)

               else if (iobjf .eq. 0) then
c              --  mean drag minimization --

                  call calcobjair0(fobjm,nout,skipend,x,y,xy,xyj,scalef)

               else if (iobjf .eq. 5) then
c              --  (mean drag / mean lift)  minimization --
            
                  call calcobjair5(fobjm,nout,skipend,x,y,xy,xyj,cdmean,
     &                             clmean,scalef)

               else if (iobjf .eq. 8) then
c              -- inverse remote design --
                  call calcobjair8(fobjm,nout,skipend,deltat,scalef,x,y)

               else if (iobjf .eq. 10 .or. iobjf .eq. 11) then
c               -- design using FWH--            
                  call calcobjairnoise(fobjm,nout,skipend,deltat,scalef,
     &                 x,y,xy,dpod,win,po,ns,.false.)

               end if           ! iobjf?

               fgrad(count)=(fobjp-fobjm)/(2.d0*fd_eta)
            
               write (*,*) 'Gradient',count,'value:',fgrad(count)

            end do

            do i=1,ndv+1
               dvs(i)=dvssave(i)
            enddo







         else if (igrad .eq. 1) then
c     -- use adjoint method to determine gradient --

            write(*,*)
            write(*,*) 'Start Gradient Calculation a la Adjoint'
            write(*,*)

            do i=1,ndv
               fgrad(i)=0.0d0
            enddo


            if (IMARCH.eq.4 )then
              
c     evolve psi for ESDIRK4:

c     -- initialize psies and psites to zero for all stages 
             
      do j = 1,6*maxjkq                       
         psies(j)=0.d0           
      end do

      do j = 1,6*maxjk                         
         psites(j)=0.d0              
      end do


      do count = nouttemp,0,-1

         if (count.ne.0) then
            stopstage=2
         else
            stopstage=6
         end if

c     -- account for big timejump in the begining
         if (skipend.gt.0 .and. count.le.skipend) then
            deltattmp=deltatbig
         else
            deltattmp=deltat
         end if

      do jstage = 6,stopstage,-1

c     -- read target distribution at this time step if existent --

         if ( jstage.eq.6 .and. (iobjf.eq.1 .or. iobjf.eq.8) 
     &        .and. count.gt.skipend) then
            if (iobjf .eq. 1) then
               call iomarkus(4,count,maxj,maxk,qtar,xyj,turretar)
            else if (iobjf .eq. 8) then
               call iocpmarkus(2,count,maxj,maxk,press,xyj,x,y,cptar)
            end if
         end if

c     --read physical flow field at this time step--
         if (jstage.ne.6) then
            call iomarkusstage(3,jstage,count,maxj,maxk,q,xyj,turre)          
         else
            call iomarkus(3,count,maxj,maxk,q,xyj,turre)
         end if

c     -- halo column copy for q vector --
         call halo_q(1,nblks,maxj,maxk,q,turre) 


         if (count.ne.0) then   !for count.eq.0 no adjoint problem 
                                !has to be solved       

c     -- calculate dfobjdQhat
         if (jstage.eq.6 .and. count.gt.skipend) then

            do j = 1,maxjkq
               dfobjdQ(j)=0.d0         
            end do

            call calcdfobjdQ(dfobjdQ,q,qtar,cptar,xyj,deltat,count,
     &      skipend,scalef,x,y,xy,cdmean,clmean,nouttemp,win,dpod,po,ns)

         end if

c     -- put in jacobian Q->Qhat --
         do ib=1,nblks
            call scjac(ib, jmax(ib), kmax(ib), q(lqptr(ib)), 
     &           xyj(lgptr(ib)), jbegin2(ib),jend2(ib),
     &           kbegin2(ib), kend2(ib))
         end do

c     -- calculate pressure and sound speed --
         do ib=1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call calcps(ib, jmax(ib), kmax(ib), q(lq), press(lg),
     &           sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
     &           jbegin2(ib),jend2(ib),kbegin2(ib),kend2(ib))
         end do

c     -- calculate laminar viscosity
         if(viscous) then
            do ib=1,nblks
               call fmun(jmax(ib), kmax(ib), q(lqptr(ib)),
     &           press(lgptr(ib)), fmu(lgptr(ib)), sndsp(lgptr(ib)),
     &           jbegin2(ib), jend2(ib),kbegin2(ib), kend2(ib))
            end do
         end if

c     -- calculate turbulent viscosity
         if (viscous .and. turbulnt .and. iturb.eq.3) then
            do ib=1,nblks
               lg=lgptr(ib)
               lq=lqptr(ib)
               call calcmut( jmax(ib),kmax(ib),jminbnd(ib),jmaxbnd(ib),
     &              kbegin2(ib), kend2(ib)-1,
     &              q(lq), turmu(lg), turre(lg), fmu(lg), xyj(lg))
            end do
c           update distances for the s-a model (array smin) --
            call autostan( 1, 1, maxj, maxk, x, y, d, d(1,2), .false.)
            do ib=1,nblks
               lg = lgptr(ib)
               call caldist(jmax(ib), kmax(ib), x(lg), y(lg), smin(lg),
     &              jlow(ib), jup(ib), klow(ib), kup(ib),icoord,xcoord,
     &              ycoord)
            end do
         endif

c     -- adjoint lhs without implicit boundary conditions and unsteady term: dR/dQhat --         
         jacmat = .true.
         precmat = .false.
         imps=.false.
         UNSTEADY=.false.
         dt=1.d0       

         call get_lhs(nmax, q, press, sndsp, xy, xyj, precon, xit, ett,
     &        fmu, x, y, turmu, vort, turre, vk, ve, coef4x, coef4y, uu,
     &        vv, ccx, ccy, coef2x, coef2y, indx, pdcg, pa, ipa, as, ia,
     &        icol, iex, bcf,ntvar,precmat,jacmat,imps)

         UNSTEADY=.true.

c     -- calculate RHS for ESDIRK adjoint problem, store temporarily in Matrix format in Rpp  
c        and copy in the end to Rp and Rsap. Use Rmm as temporary storage in Matrix format 
c        for psies and psites at the correct stage k multiplied with the correct factor -a_kj/a_kk.
c        Use Rm as temporary storage for the product (dR/dQ)^T*Rmm.

         do j=1,ntvar
            Rpp(j)=0.d0
            Rp(j)=0.d0
            Rm(j)=0.d0
            Rmm(j)=0.d0
         end do
         
         do j=1,maxjk
            Rsapp(j)=0.d0
            Rsap(j)=0.d0
         end do

         if (jstage.eq.6 .and. count.gt.skipend) then
c        dfobjdQ contribution
            fac=-1.d0

            do ib = 1,nblks
               lq = lqptr(ib)
               lg = lgptr(ib)
               call copyindexvsmatrix(jmax(ib),kmax(ib),4,fac,
     &              indx(lgptr(ib)),Rpp,dfobjdQ(lq),Rsapp(lg),1,1,
     &              jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib),
     &              .true.)
            end do 

         end if

         if (jstage.le.5 .or. count.eq.nouttemp) then
            stopstage2=jstage+1
         else
            stopstage2=2
         endif

         do stage=6,stopstage2,-1

            if (stopstage2.ne.2) then
               fac=-ajk(stage,jstage)/ajk(stage,stage)
            else
               fac=-ajk(stage,1)/ajk(stage,stage)
            end if

            do ib = 1,nblks
               lq = ldhatqptr(ib)
               lg = ldhattptr(ib)
               call copyindexvsmatrix(jmax(ib),kmax(ib),nmax,fac,
     &              indx(lgptr(ib)),Rmm,psies(lq),psites(lg),6,stage,
     &              jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib),
     &              .true.)
            end do

            if (stopstage2.eq.2) then
               do j=1,ntvar
                  Rm(j)=0.d0
               end do
               if (skipend.gt.0 .and. count.le.skipend-1) then
                  fac = -1.0d0/(deltatbig*ajk(stage,1))
               else
                  fac = -1.0d0/(deltat*ajk(stage,1))
               end if               
               do ib = 1,nblks
                  call filldiages(jmax(ib),kmax(ib),nmax,
     &                 jlow(ib),jup(ib),klow(ib),kup(ib),
     &                 indx(lgptr(ib)),Rm,fac)
               end do
               call apldia (ntvar,0,as,ja,ia,Rm,as,ja,ia,inttmp)
            end if


c           Multiply (dR/dQ)^T with Rmm store in Rm and add to Rpp
            call atmux(ntvar,Rmm,Rm,as,ja,ia)
            do j=1,ntvar
               Rpp(j)=Rpp(j)+Rm(j)
            end do

c           Undo addition of fac to diagonal
            if (stopstage2.eq.2) then
               do j=1,ntvar
                  Rm(j)=0.d0
               end do
               fac=-fac
               do ib = 1,nblks
                  call filldiages(jmax(ib),kmax(ib),nmax,
     &                 jlow(ib),jup(ib),klow(ib),kup(ib),
     &                 indx(lgptr(ib)),Rm,fac)
               end do
               call apldia (ntvar,0,as,ja,ia,Rm,as,ja,ia,inttmp)
            end if

         end do   !loop over stages

c$$$cmpr
c$$$         maxgrad=0.d0
c$$$         do j=1,ntvar
c$$$            maxgrad=maxgrad+Rpp(j)**2
c$$$         end do
c$$$         print *, 'RHS norm2',maxgrad
c$$$cmpr

         do ib = 1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call copyindexvsmatrix(jmax(ib),kmax(ib),nmax,fac,
     &           indx(lg),Rpp,Rp(lq),Rsap(lg),1,1,jminbnd(ib),
     &           jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib),.false.)
         end do


c     -- adjoint lhs including implicit boundary conditions and unsteady term: dRstar/dQhat --
         precmat = .true.
         imps=.true.
         dt=1.d0 
         dt2=deltattmp

         call get_lhs(nmax, q, press, sndsp, xy, xyj, precon, xit, ett,
     &        fmu, x, y, turmu, vort, turre, vk, ve, coef4x, coef4y, uu,
     &        vv, ccx, ccy, coef2x, coef2y, indx, pdcg, pa, ipa, as, ia,
     &        icol, iex, bcf,ntvar,precmat,jacmat,imps)

c     -- Solve adjoint problem with latest psi as initial guess

c$$$         if (jstage.le.5) then
c$$$            stage=jstage+1
c$$$         else
c$$$            stage=2
c$$$         endif
c$$$
c$$$         do ib = 1,nblks
c$$$            lq = ldhatqptr(ib)
c$$$            lg = ldhattptr(ib)
c$$$            call copy_arrayes(jmax(ib),kmax(ib),4,psies(lq),6,stage,
c$$$     &           psi(lqptr(ib)),1,1)
c$$$            call copy_arraytes(jmax(ib),kmax(ib),psites(lg),6,stage,
c$$$     &           psit(lgptr(ib)),1,1)
c$$$         end do

         do j=1,maxjkq
            psi(j)=0.d0
         end do
         do j=1,maxjk
            psit(j)=0.d0
         end do

         call solveadj(nmax,indx,ntvar,psi,psit,ja,ia,jpa,ipa,Rp,Rsap,
     &        as,pa,iex,bcf)

c$$$cmpr
c$$$         maxgrad=0.d0
c$$$         do j=1,maxjkq
c$$$            maxgrad=maxgrad+psi(j)**2
c$$$         end do
c$$$         print *, 'PSI norm2',maxgrad
c$$$cmpr



c     -- output psi^(n)

c$$$         if (jstage.eq.6) then 
c$$$            do j=1,maxjk
c$$$               xyjout(j)=1.0d0
c$$$            end do                        
c$$$            call iomarkus(5,7000+count,maxj,maxk,psi,xyjout,psit)
c$$$         end if

c     -- Copy solution into correct psi stage

         if (jstage.le.5 .or. count.eq.nouttemp) then
            stage=jstage
         else
            stage=1
         endif

         do ib = 1,nblks
            lq = ldhatqptr(ib)
            lg = ldhattptr(ib)
            call copy_arrayes(jmax(ib),kmax(ib),4,psi(lqptr(ib)),1,1,
     &           psies(lq),6,stage)
            call copy_arraytes(jmax(ib),kmax(ib),psit(lgptr(ib)),1,1,
     &           psites(lg),6,stage)
         end do

c     -- remove jacobian scaling Qhat->Q --
         do ib = 1,nblks
            call qmuj(jmax(ib),kmax(ib),q(lqptr(ib)),xyj(lgptr(ib)),
     &           jbegin2(ib), jend2(ib),
     &           kbegin2(ib), kend2(ib))
         end do 


         end if !count.ne.0  


         if (.not.sbbc) then
c        -- calculate dRdX and assemble lagrange contribution to gradient---- 

            do i=1,ndv
               dvssave(i)=dvs(i)
            end do

c           -- assemble multiplier for the partial derivative and store in psiold

            if (jstage.le.5 .or. count.eq.nouttemp) then
               stage=jstage
            else
               stage=1
            end if

            do j=1,maxjkq
               psiold(j)=0.d0
            end do
            do j=1,maxjk
               psitold(j)=0.d0
            end do

            do ib = 1,nblks
               call assemblemultiplier(jmax(ib),kmax(ib),nmax,stage,
     &              psies(ldhatqptr(ib)),psites(ldhattptr(ib)), 
     &              psiold(lqptr(ib)),psitold(lgptr(ib)),
     &              jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib))
            end do

c$$$cmpr
c$$$         maxgrad=0.d0
c$$$         do j=1,maxjkq
c$$$            maxgrad=maxgrad+psiold(j)**2
c$$$         end do
c$$$         print *, 'MUL norm2',maxgrad
c$$$cmpr 


            do count2=1,ndv

               dvs(count2)=dvssave(count2)+2.0d0*fd_eta
               
               call calcperturbrhs(count2,q,turre,Rpp,Rsapp,xsave,ysave,
     &         turmu,vort,fmu,press,precon,sndsp,ds,vk,ve,coef2x,coef4x,  
     &         coef2y,coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)              

               dvs(count2)=dvssave(count2)+fd_eta

               call calcperturbrhs(count2,q,turre,Rp,Rsap,xsave,ysave,
     &         turmu,vort,fmu,press,precon,sndsp,ds,vk,ve,coef2x,coef4x,  
     &         coef2y,coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)              

               dvs(count2)=dvssave(count2)-fd_eta

               call calcperturbrhs(count2,q,turre,Rm,Rsam,xsave,ysave,
     &         turmu,vort,fmu,press,precon,sndsp,ds,vk,ve,coef2x,coef4x,  
     &         coef2y,coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)               

               dvs(count2)=dvssave(count2)-2.0d0*fd_eta

               call calcperturbrhs(count2,q,turre,Rmm,Rsamm,xsave,ysave,
     &         turmu,vort,fmu,press,precon,sndsp,ds,vk,ve,coef2x,coef4x,  
     &         coef2y,coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)

               dvs(count2)=dvssave(count2)
             
c              -- assemble partial derivative --
c              Note: get_rhs returns the negative of the residual

               do j = 1,maxjkq                                              
                  dRdX(j) = ( Rpp(j)-8.0d0*Rp(j)+8.0d0*Rm(j)-Rmm(j) ) / 
     &                       (12.d0*fd_eta) 
               end do

               do j = 1,maxjk                                              
                  dRsadX(j) = ( Rsapp(j)-8.0d0*Rsap(j)+8.0d0*Rsam(j)
     &                    -Rsamm(j) ) / (12.d0*fd_eta) 
               end do

c               -- product of multiplier with residual sensitivity -- 
               do ib = 1,nblks
                  lg = lgptr(ib)
                  lq = lqptr(ib)
                  call vdotv(count2,fgrad,nmax,jmax(ib),kmax(ib),
     &                 jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib),
     &                 psiold(lq),dRdX(lq),psitold(lg),dRsadX(lg))
               end do

            end do   !count2 for dRdX

         else     !suction/blowing BC
c        -- calculate dRdX analytically and sum over n=1,...,N: psi^(n)^T * dRdX^(n)

            print *,'Suction/blowing BC for ESDIRK4 not implemented yet'
            stop
 
         end if

c        -- Copy psi at stage 1 into psi at stage 6 for new (previous) time step at the end of jstage==6
c           and zero out the psi values at stages 1-5
         if (jstage.eq.6 .and. count.ne.nouttemp .and. count.ne.0) then             
            do ib = 1,nblks
               lq = ldhatqptr(ib)
               lg = ldhattptr(ib)
               call copy_arrayes(jmax(ib),kmax(ib),4,psies(lq),6,1,
     &              psi(lqptr(ib)),1,1)
               call copy_arraytes(jmax(ib),kmax(ib),psites(lg),6,1,
     &              psit(lgptr(ib)),1,1)
            end do

            do j = 1,6*maxjkq                       
               psies(j)=0.d0           
            end do

            do j = 1,6*maxjk                         
               psites(j)=0.d0              
            end do

            do ib = 1,nblks
               lq = ldhatqptr(ib)
               lg = ldhattptr(ib)
               call copy_arrayes(jmax(ib),kmax(ib),4,psi(lqptr(ib)),1,1,
     &              psies(lq),6,6)
               call copy_arraytes(jmax(ib),kmax(ib),psit(lgptr(ib)),1,1,
     &              psites(lg),6,6)
            end do


         end if !Copy psi at stage 1 into psi at stage 6

      end do   !jstage
      end do   !count           





            else if (IMARCH.eq.2 )then

c     evolve psi for BDF2:   
c     psi^(n)= inv([dRstar/dQhat]^T) * ( (4*psi^{n+1} - psi^{n+2})/(2*dt) - (dfobj/dQhat)^T ) 

c     -- initialize psi^(N+2) and psi^(N+1) to zero 
             
      do j = 1,maxjkq                       
         psi(j)=0.d0
         psiold(j)=0.d0                 
      end do

      do j = 1,maxjk                         
         psit(j)=0.d0
         psitold(j)=0.d0                 
      end do           
  
      do count = nouttemp,1, -1

c     -- account for big timejump in the begining
         if (count .gt. skipend) then
            deltattmp=deltat
         else
            deltattmp=deltatbig
         end if

c     -- read target distribution at this time step if existent --

         if ( (iobjf.eq.1 .or. iobjf.eq.8) .and. count.gt.skipend) then
            if (iobjf .eq. 1) then
               call iomarkus(4,count,maxj,maxk,qtar,xyj,turretar)
            else if (iobjf .eq. 8) then
               call iocpmarkus(2,count,maxj,maxk,press,xyj,x,y,cptar)
            end if
         end if

c     --read physical flow field at this time step--

         call iomarkus(3,count,maxj,maxk,q,xyj,turre)

c     -- halo column copy for q vector --
         call halo_q(1,nblks,maxj,maxk,q,turre)        

c     -- calculate dfobjdQhat

         do j = 1,maxjkq                         
            dfobjdQ(j)=0.d0                          
         end do        

         if (count .gt. skipend) then

            call calcdfobjdQ(dfobjdQ,q,qtar,cptar,xyj,deltat,count,
     &      skipend,scalef,x,y,xy,cdmean,clmean,nouttemp,win,dpod,po,ns)

         end if

c     -- put in jacobian Q->Qhat --
         do ib=1,nblks
            call scjac(ib, jmax(ib), kmax(ib), q(lqptr(ib)), 
     &           xyj(lgptr(ib)), jbegin2(ib),jend2(ib),
     &           kbegin2(ib), kend2(ib))
         end do

c     -- calculate pressure and sound speed --
         do ib=1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call calcps(ib, jmax(ib), kmax(ib), q(lq), press(lg),
     &           sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
     &           jbegin2(ib),jend2(ib),kbegin2(ib),kend2(ib))
         end do

c     -- calculate laminar viscosity
         if(viscous) then
            do ib=1,nblks
               call fmun(jmax(ib), kmax(ib), q(lqptr(ib)),
     &           press(lgptr(ib)), fmu(lgptr(ib)), sndsp(lgptr(ib)),
     &           jbegin2(ib), jend2(ib),kbegin2(ib), kend2(ib))
            end do
         end if

c     -- calculate turbulent viscosity
         if (viscous .and. turbulnt .and. iturb.eq.3) then
            do ib=1,nblks
               lg=lgptr(ib)
               lq=lqptr(ib)
               call calcmut( jmax(ib),kmax(ib),jminbnd(ib),jmaxbnd(ib),
     &              kbegin2(ib), kend2(ib)-1,
     &              q(lq), turmu(lg), turre(lg), fmu(lg), xyj(lg))
            end do
c     -- update distances for the s-a model (array smin) --
            call autostan( 1, 1, maxj, maxk, x, y, d, d(1,2), .false.)
            do ib=1,nblks
               lg = lgptr(ib)
               call caldist(jmax(ib), kmax(ib), x(lg), y(lg), smin(lg),
     &              jlow(ib), jup(ib), klow(ib), kup(ib),icoord,xcoord,
     &              ycoord)
            end do
         endif
         
         jacmat = .true.
         precmat = .true.
         imps=.true.
         dt=1.d0
         dtbigtmp=dtbig
         dtbig=deltatbig
         dtsmalltmp=dtsmall
         dtsmall=deltat
         dt2=deltattmp 

c     -- adjoint lhs: dRstar/dQhat --
         call get_lhs(nmax, q, press, sndsp, xy, xyj, precon, xit, ett,
     &        fmu, x, y, turmu, vort, turre, vk, ve, coef4x, coef4y, uu,
     &        vv, ccx, ccy, coef2x, coef2y, indx, pdcg, pa, ipa, as, ia,
     &        icol, iex, bcf,ntvar,precmat,jacmat,imps)

         dtbig=dtbigtmp
         dtsmall=dtsmalltmp

c     -- calculate RHS: rhs=(4*psi^{n+1} - psi^{n+2})/(2*dt) - (dfobj/dQhat)^T 

         do ib = 1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call setupadjrhs(jmax(ib),kmax(ib),
     &           psi(lq),psit(lg),psiold(lq),psitold(lg),deltat,
     &           Rp(lq),Rsap(lg),deltatbig,count,skipend,deltattmp,
     &           jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib),nmax,
     &           jlow(ib),jup(ib),klow(ib),kup(ib),dfobjdQ(lq))
         end do 

c     -- copy psi into psiold --

         do ib = 1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call copy_array(jmax(ib),kmax(ib),4,psi(lq),psiold(lq))
            call copy_array(jmax(ib),kmax(ib),1,psit(lg),psitold(lg))
         end do



c     -- Calculate psi^(n)= inv(dRstar/dQhat)]^T) *[(4*psi^{n+1} - psi^{n+2})/(2*dt) - (dfobj/dQhat)^T]
c        using psi^(n+1) as initial guess for gmres

c$$$         do j=1,maxjkq
c$$$            psi(j)=0.d0
c$$$         end do
c$$$         do j=1,maxjk
c$$$            psit(j)=0.d0
c$$$         end do

         call solveadj(nmax,indx,ntvar,psi,psit,ja,ia,jpa,ipa,Rp,Rsap,
     &        as,pa,iex,bcf)



c     -- remove jacobian scaling Qhat->Q --
         do ib = 1,nblks
            call qmuj(jmax(ib),kmax(ib),q(lqptr(ib)),xyj(lgptr(ib)),
     &           jbegin2(ib), jend2(ib),
     &           kbegin2(ib), kend2(ib))
         end do 


c     -- output psi^(n) 

c$$$         do j=1,maxjk
c$$$            xyjout(j)=1.0d0
c$$$         end do
c$$$                        
c$$$         call iomarkus(5,9000+count,maxj,maxk,psi,xyjout,psit)


         if (.not.sbbc) then
c        -- calculate dRdX and sum over n=1,...,N:  psi^(n)^T * dRdX^(n) ---- 

            do i=1,ndv
               dvssave(i)=dvs(i)
            end do

            do count2=1,ndv

               dvs(count2)=dvssave(count2)+2.0d0*fd_eta
               
               call calcperturbrhs(count2,q,turre,Rpp,Rsapp,xsave,ysave,
     &         turmu,vort,fmu,press,precon,sndsp,ds,vk,ve,coef2x,coef4x,  
     &         coef2y,coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)              

               dvs(count2)=dvssave(count2)+fd_eta

               call calcperturbrhs(count2,q,turre,Rp,Rsap,xsave,ysave,
     &         turmu,vort,fmu,press,precon,sndsp,ds,vk,ve,coef2x,coef4x,  
     &         coef2y,coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)              

               dvs(count2)=dvssave(count2)-fd_eta

               call calcperturbrhs(count2,q,turre,Rm,Rsam,xsave,ysave,
     &         turmu,vort,fmu,press,precon,sndsp,ds,vk,ve,coef2x,coef4x,  
     &         coef2y,coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)               

               dvs(count2)=dvssave(count2)-2.0d0*fd_eta

               call calcperturbrhs(count2,q,turre,Rmm,Rsamm,xsave,ysave,
     &         turmu,vort,fmu,press,precon,sndsp,ds,vk,ve,coef2x,coef4x,  
     &         coef2y,coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)

               dvs(count2)=dvssave(count2)
             
c              -- assemble partial derivative --
c              Note: get_rhs returns the negative of the residual

               do j = 1,maxjkq                                              
                  dRdX(j) = ( Rpp(j)-8.0d0*Rp(j)+8.0d0*Rm(j)-Rmm(j) ) / 
     &                       (12.d0*fd_eta) 
               end do

               do j = 1,maxjk                                              
                  dRsadX(j) = ( Rsapp(j)-8.0d0*Rsap(j)+8.0d0*Rsam(j)
     &                    -Rsamm(j) ) / (12.d0*fd_eta) 
               end do

c              -- product of adjoint variable with residual sensitivity -- 
               do ib = 1,nblks
                  lg = lgptr(ib)
                  lq = lqptr(ib)
                  call vdotv(count2,fgrad,nmax,jmax(ib),kmax(ib),
     &                 jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib),
     &                 psi(lq),dRdX(lq),psit(lg),dRsadX(lg))
               end do

            end do   !count2 for dRdX

         else     !suction/blowing BC
c        -- calculate dRdX analytically and sum over n=1,...,N: psi^(n)^T * dRdX^(n)

            ib=4

            call calcdRdXsb(count,deltattmp,deltatbig,ib,fgrad,jmax(ib),
     &                     kmax(ib),jminbnd(ib),jmaxbnd(ib),kminbnd(ib),
     &                     kmaxbnd(ib),psi(lqptr(ib)),skipend)
 
         end if

            
         end do                 !count




      end if                    !evolve psi for ESDIRK4 or BDF2 


      if (.not.sbbc) then
c     -- calculate dJdX, sum over n=1,...,N and add to fgrad --

         do count2=1,ndv

            dvs(count2)=dvssave(count2)+2.0d0*fd_eta
            
            call calcperturbobj(fobjpp,xsave,ysave,nout,
     &           skipend,deltat,scalef)

            dvs(count2)=dvssave(count2)+fd_eta
            
            call calcperturbobj(fobjp,xsave,ysave,nout,
     &           skipend,deltat,scalef)
            
            dvs(count2)=dvssave(count2)-fd_eta
               
            call calcperturbobj(fobjm,xsave,ysave,nout,
     &           skipend,deltat,scalef)
            
            dvs(count2)=dvssave(count2)-2.0d0*fd_eta
               
            call calcperturbobj(fobjmm,xsave,ysave,nout,
     &           skipend,deltat,scalef)
            
            dvs(count2)=dvssave(count2)
            
c           -- assemble gradient --
            
            fgrad(count2) =  fgrad(count2) +(-fobjpp+8.d0*fobjp
     &           -8.d0*fobjm+fobjmm ) / (12.d0*fd_eta)

         end do                 !count2 for dJdX

      end if                    !.not.sbbc for dJdX


c     output the gradient values to the screen
 
      do count=1,ndv
         write (*,*) 'Gradient',count,'value:',fgrad(count)
      end do

      end if                    ! Calculate gradient with FD or Adjoint


c     output fobj and maxgrad to the .ohis file the first time
c     and copy the history file to the restart file

      if (isave(34) .eq. 0) then

         maxgrad=0.d0
         do i=1,ndv
            if (abs(fgrad(i)) .gt. maxgrad) maxgrad=abs(fgrad(i))
         end do

         write (n_ohis,705) isave(30),isave(34),fobj,maxgrad,dsave(13),
     &        (dvs(i),i = 1,ndv),(fgrad(i),i = 1,ndv)
         call flush(n_ohis)

         command = 'cp '      
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(4:3+namelen+5) = filena
         lentmp = 3+namelen+5+1       
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(lentmp+1:lentmp+namelen+5) = filena
         
         call system(command)

      end if


c     save values for a potential optimization restart

      rewind(33)
      write(33) ndv,mlim
      write(33) (dvs(i),i = 1,ndv) 
      write(33) fobj,scalef,cdmean,clmean
      write(33) objdone
      write(33) (fgrad(i),i = 1,ndv)
      write(33) (wa(i),i = 1,2*mlim*ndv+4*ndv+
     &        12*mlim*mlim+12*mlim)
      write(33) (iwa(i),i = 1,3*ndv)
      write(33) task
      write(33) csave
      write(33) (lsave(i),i = 1,4)
      write(33) (isave(i),i = 1,44)
      write(33) (dsave(i),i = 1,29)
      write(33) (win(i),i = 1,tdim)
      write(33) ((ns(i,j),i=1,sdim),j=1,2)
      write(33) ((po(i,j),i=1,tdim),j=1,sdim)
      write(33) ((((dpod(ib,i,j,k),ib=1,Nobs),i=1,tdim),
     &                j=1,sdim),k=1,3)
           


c     go back to the minimization routine.
      goto 111

      endif                     ! task = FG   : Determine f and g



c
      if (task(1:5) .eq. 'NEW_X') then 
    
c     the minimization routine has returned with a new iterate.
c     At this point have the opportunity of stopping the iteration 
c     or observing the values of certain parameters
c

c     Note: task(1:4) must be assigned the value 'STOP' to terminate  
c     the iteration and ensure that the final results are
c     printed in the default format. The rest of the character
c     string TASK may be used to store other information.

c     1) Terminate if the total number of fobj and grad evaluations
c     exceeds MXFUN.

         if (isave(34) .ge. mxfun)
     +    task='STOP: TOTAL NO. of f AND g EVALUATIONS EXCEEDS LIMIT'

c     2) Terminate if  max(|fgrad|) <= OPTOL

         maxgrad=0.d0
         do i=1,ndv
            if (abs(fgrad(i)) .gt. maxgrad) maxgrad=abs(fgrad(i))
         end do

         if (maxgrad .lt. optol)
     +      task='STOP: THE GRADIENT IS SUFFICIENTLY SMALL'

c     3) Terminate if (f^k - f^{k+1}) <= OPTOL

         if (abs(dsave(2)-fobj) .lt. optol*1.0d-6)
     +      task='STOP: THE CHANGE IN f IS SUFFICIENTLY SMALL'

c     We now wish to print the following information at each
c     iteration:
c     
c     1) the current iteration number, isave(30),
c     2) the total number of fobj and fgrad evaluations, isave(34),
c     3) the value of the objective function fobj,
c     4) the norm of the gradient and projected gradient,  maxgrad,dsave(13)
c     5) the values of the design variables
c     6) the values of the gradient
c     
c     See the comments at the end of driver1 for a description
c     of the variables isave and dsave.
         
         write (6,'(2(a,i5,4x),a,1p,d12.5,4x,a,1p,d12.5,4x,a,1p,d12.5,
     +        3x,a,1p,d12.5)') 'Iteration',isave(30),'nfg =',isave(34),
     +        'f =',fobj,'max |g| =',maxgrad,'|proj g| =',dsave(13)

         write (n_ohis,705) isave(30),isave(34),fobj,maxgrad,
     &        dsave(13),(dvs(i),i = 1,ndv),(fgrad(i),i = 1,ndv) 
         call flush(n_ohis)
 705     format (2i4, 50e16.8)



c     save values for a potential optimization restart
c     also copy the history file to the restart file

         rewind(33)
         write(33) ndv,mlim
         write(33) (dvs(i),i = 1,ndv) 
         write(33) fobj,scalef,cdmean,clmean
         write(33) objdone
         write(33) (fgrad(i),i = 1,ndv)
         write(33) (wa(i),i = 1,2*mlim*ndv+4*ndv+
     &        12*mlim*mlim+12*mlim)
         write(33) (iwa(i),i = 1,3*ndv)
         write(33) task
         write(33) csave
         write(33) (lsave(i),i = 1,4)
         write(33) (isave(i),i = 1,44)
         write(33) (dsave(i),i = 1,29)
         write(33) (win(i),i = 1,tdim)
         write(33) ((ns(i,j),i=1,sdim),j=1,2)
         write(33) ((po(i,j),i=1,tdim),j=1,sdim)
         write(33) ((((dpod(ib,i,j,k),ib=1,Nobs),i=1,tdim),
     &                j=1,sdim),k=1,3)        
         
         command = 'cp '      
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(4:3+namelen+5) = filena
         lentmp = 3+namelen+5+1       
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(lentmp+1:lentmp+namelen+5) = filena
         
         call system(command)

c        output grid to opt.grid 
         namelen = strlen('opt')
         filena  = 'opt'
         filena(namelen+1:namelen+6) = '.grid'

         open ( unit=n_all, file=filena, status='unknown',
     &        form='unformatted' )
      
         write(n_all) nblks
         write(n_all) (jbmax(i), kbmax(i),i=1,nblks)
         do i=1,nblks
            call writegrid(n_all,x(lgptr(i)),y(lgptr(i)),
     &           jbmax(i)+2*nhalo,kbmax(i)+2*nhalo,
     &           jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
         end do
         
         close (n_all)

c     If the run is to be terminated, we print also the information
c     contained in task as well as the final value of x.

         if (task(1:4) .eq. 'STOP') then
            write (6,*) task  
            write (6,*) 'Final X ='
            write (6,'((1x,1p, 2(1x,d11.4)))') (dvs(i),i = 1,ndv)
         endif

c          go back to the minimization routine.
         goto 111

      endif                     ! task = NEW_X

c     ---------- the end of the loop -------------
c     If task is neither FG nor NEW_X we terminate execution.

      if (task(1:4) .eq. 'WARN') then
         write (6,*) task
      end if

c     Write task anyway to check clean exit
      write(*,*) task

      close(n_ohis)


cmpr  Deallocate the big arrays
      deallocate(psies,wa,psies,psites,dpod)

      return 
      end                       !unsteadyoptairfoil





      subroutine calcdfobjdQ(dfobjdQ,q,qtar,cptar,xyj,deltat,count,
     &     skipend,scalef,x,y,xy,cdmean,clmean,nout,win,dpod,po,ns)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/mbcom.inc" 


      integer j,k,nx,ny,len(4),ib,is,lq,lg,num(4,maxj,3)
      integer count,skipend,nout,ifoil,iseg
      double precision q(*),xyj(*),qtar(*),x(*),y(*),dfobjdQ(*),xy(*)
      double precision xwant(maxj),ywant(maxk),scalef,deltat,fobj
      double precision xval(4,maxj),pval(4,maxj),cptar(*)
      double precision dpdQx1(maxk,3,4),dpdQy1(maxj,3,4),cdmean,clmean
      double precision dpdQx2(maxk,3,4),dpdQy2(maxj,3,4)
      double precision sin_alpha,cos_alpha,c_cn,c_cc
      complex*16 dpod(Nobs,tdim,sdim,3)
      double precision po(tdim,sdim),win(tdim),ns(sdim,2)

      double precision qtmp,fobjp,fobjm,fobjpp,fobjmm,dfobjdQtmp
      integer jk,index1,jj
      

      if (iobjf .eq. 1) then 


         do ib = 1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call calcdJdQ1(jmax(ib),kmax(ib),q(lq),qtar(lq),
     &           xyj(lg),deltat,dfobjdQ(lq),kminbnd(ib),kmaxbnd(ib),
     &           jminbnd(ib),jmaxbnd(ib))
         end do 
         
         
      else if (iobjf .eq. 0) then 
         
         
         c_cn = sin(alpha*pi/180.0)/(real(nout-skipend)*scalef) 
         c_cc = cos(alpha*pi/180.0)/(real(nout-skipend)*scalef)
         
         do ifoil=1,nfoils
            
            do iseg=1,nsegments(ifoil)
               
               ib = isegblk(iseg,ifoil)
               is = isegside(iseg,ifoil)
               lg=lgptr(ib)
               lq=lqptr(ib)
               
               call calcdJdQ0or5(jmax(ib),kmax(ib),is,ibcdir(ib,is), 
     &              q(lq),xy(lq),xyj(lg),x(lg),y(lg),c_cc,c_cn,
     &              dfobjdQ(lq),jminbnd(ib),jmaxbnd(ib),kminbnd(ib),
     &              kmaxbnd(ib))

            end do 
            
         end do


      else if (iobjf .eq. 5) then
         
         
c            do jj = 1,maxjkq

c$$$            do jj = 22000,114000
c$$$                  
c$$$               qtmp = q(jj)
c$$$               q(jj) = q(jj) + 2.d0*fd_eta             
c$$$               call calcobjair5fd(fobjpp,q,nout,skipend,x,y,xy,xyj,
c$$$     &              scalef,count)            
c$$$                     
c$$$               q(jj) = qtmp + fd_eta
c$$$               call calcobjair5fd(fobjp,q,nout,skipend,x,y,xy,xyj,
c$$$     &              scalef,count)             
c$$$
c$$$               q(jj) = qtmp - fd_eta             
c$$$               call calcobjair5fd(fobjm,q,nout,skipend,x,y,xy,xyj,
c$$$     &              scalef,count)                      
c$$$                     
c$$$               q(jj) = qtmp - 2.d0*fd_eta
c$$$               call calcobjair5fd(fobjmm,q,nout,skipend,x,y,xy,xyj,
c$$$     &              scalef,count)             
c$$$               q(jj) = qtmp  
c$$$                     
c$$$               dfobjdQ(jj) = (-fobjpp+8.d0*fobjp-
c$$$     &           8.d0*fobjm+fobjmm ) / (12.d0*fd_eta)
c$$$
c$$$               if (abs(dfobjdQ(jj)).gt.1.d-20) then 
c$$$                  print *, jj,dfobjdQ(jj)
c$$$                  
c$$$               
c$$$
c$$$c$$$c              figure out index location
c$$$c$$$               do ib = 1,nblks
c$$$c$$$                  do j=1,jmax(ib)
c$$$c$$$                     do k=1,kmax(ib)
c$$$c$$$                        do jk=1,4
c$$$c$$$                           index1=lqptr(ib)+(j-1)+(k-1)*jmax(ib)
c$$$c$$$     &                          +(jk-1)*jmax(ib)*kmax(ib)
c$$$c$$$                           if (index1.eq.jj) print *,'Hi',ib,j,
c$$$c$$$     &                          k,jk                          
c$$$c$$$                        end do
c$$$c$$$                     end do
c$$$c$$$                  end do
c$$$c$$$               end do
c$$$
c$$$               end if
c$$$
c$$$            end do
c$$$
c$$$            stop

         sin_alpha=sin(alpha*pi/180.0)/(real(nout-skipend)*scalef)
         cos_alpha=cos(alpha*pi/180.0)/(real(nout-skipend)*scalef)
         c_cn = sin_alpha/clmean - cos_alpha*cdmean/clmean**2
         c_cc = cos_alpha/clmean + sin_alpha*cdmean/clmean**2
         
         do ifoil=1,nfoils
            
            do iseg=1,nsegments(ifoil)
               
               ib = isegblk(iseg,ifoil)
               is = isegside(iseg,ifoil)
               lg=lgptr(ib)
               lq=lqptr(ib)
          
               call calcdJdQ0or5(jmax(ib),kmax(ib),is,ibcdir(ib,is), 
     &              q(lq),xy(lq),xyj(lg),x(lg),y(lg),c_cc,c_cn,
     &              dfobjdQ(lq),jminbnd(ib),jmaxbnd(ib),kminbnd(ib),
     &              kmaxbnd(ib))

            end do 

         end do

c$$$            do j = 1,maxjkq
c$$$               if (abs(dfobjdQ(j)).gt.1.d-20) print *, j,dfobjdQ(j)         
c$$$            end do
c$$$
c$$$c            stop

         
      else if (iobjf .eq. 8) then


c$$$            do j = 1,maxjkq
c$$$                  
c$$$               qtmp = q(j)
c$$$               q(j) = q(j) + 2.d0*fd_eta             
c$$$               call calcobjair8fd(fobjpp,deltat,scalef,x,y,q,cptar)             
c$$$                     
c$$$               q(j) = qtmp + fd_eta
c$$$               call calcobjair8fd(fobjp,deltat,scalef,x,y,q,cptar)    
c$$$
c$$$               q(j) = qtmp - fd_eta             
c$$$               call calcobjair8fd(fobjm,deltat,scalef,x,y,q,cptar)             
c$$$                     
c$$$               q(j) = qtmp - 2.d0*fd_eta
c$$$               call calcobjair8fd(fobjmm,deltat,scalef,x,y,q,cptar)    
c$$$               q(j) = qtmp  
c$$$                     
c$$$               dfobjdQfd(j) = (-fobjpp+8.d0*fobjp-
c$$$     &           8.d0*fobjm+fobjmm ) / (12.d0*fd_eta)
c$$$
c$$$               if (abs(dfobjdQfd(j)).gt.1.d-10) print *, j,dfobjdQfd(j)
c$$$
c$$$            end do
c$$$
c$$$            stop


         nx=NINT((remxmax-remxmin)/remdx+1.d0)
         ny=NINT((remymax-remymin)/remdy+1.d0)
         do j = 1,nx
            xwant(j)=remxmin+remdx*dble(j-1)
         end do
         do k = 1,ny
            ywant(k)=remymin+remdy*dble(k-1)
         end do
 
         do k=1,4
            len(k)=0
         end do

         do ib = 1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call interpolateboxyx(jmax(ib),kmax(ib),q(lq),ib,
     &           kminbnd(ib),kmaxbnd(ib),jminbnd(ib),jmaxbnd(ib),x(lg),
     &           y(lg),xval,pval,len,num,dpdQx1,dpdQy1,dpdQx2,
     &           dpdQy2,.true.)
         end do

         call interpolatebox(fobj,cptar,xval,pval,len,num,xwant,
     &        ywant,nx,ny,deltat,scalef,dpdQx1,dpdQy1,dpdQx2,dpdQy2,
     &        dfobjdQ,.true.)


      else if (iobjf .eq. 10 .or. iobjf .eq. 11) then


         call calcdJdQnoise(dfobjdQ,q,count,nout,skipend,deltat,
     &        scalef,dpod,win,po,ns)

c$$$            print *, 'Start calc dfdq fourth order',count
c$$$
c$$$            do jj = 1,maxjkq
c$$$                  
c$$$               qtmp = q(jj)
c$$$               q(jj) = q(jj) + 2.d0*fd_eta             
c$$$               call calcobjairnoisefd(fobjpp,q,count,nout,skipend,deltat,
c$$$     &              scalef,x,y,ns) 
c$$$                    
c$$$               q(jj) = qtmp + fd_eta
c$$$               call calcobjairnoisefd(fobjp,q,count,nout,skipend,deltat,
c$$$     &              scalef,x,y,ns)           
c$$$
c$$$               q(jj) = qtmp - fd_eta             
c$$$               call calcobjairnoisefd(fobjm,q,count,nout,skipend,deltat,
c$$$     &              scalef,x,y,ns)                  
c$$$                     
c$$$               q(jj) = qtmp - 2.d0*fd_eta
c$$$               call calcobjairnoisefd(fobjmm,q,count,nout,skipend,deltat,
c$$$     &              scalef,x,y,ns)         
c$$$               q(jj) = qtmp  
c$$$                     
c$$$               dfobjdQtmp = (-fobjpp+8.d0*fobjp-
c$$$     &           8.d0*fobjm+fobjmm ) / (12.d0*fd_eta)
c$$$
c$$$               if (abs(dfobjdQtmp-dfobjdQ(jj)).gt.5.d-12) then
c$$$
c$$$                  print *, jj,dfobjdQ(jj),dfobjdQtmp
c$$$                  
c$$$               
c$$$c              figure out index location
c$$$c$$$               do ib = 1,nblks
c$$$c$$$                  do j=1,jmax(ib)
c$$$c$$$                     do k=1,kmax(ib)
c$$$c$$$                        do jk=1,4
c$$$c$$$                           index1=lqptr(ib)+(j-1)+(k-1)*jmax(ib)
c$$$c$$$     &                          +(jk-1)*jmax(ib)*kmax(ib)
c$$$c$$$                           if (index1.eq.jj) print *,'Hi',ib,j,
c$$$c$$$     &                          k,jk                          
c$$$c$$$                        end do
c$$$c$$$                     end do
c$$$c$$$                  end do
c$$$c$$$               end do
c$$$
c$$$               end if
c$$$
c$$$            end do
c$$$
c$$$            print *, 'Done calc dfdq fourth order'

                                                            
      end if                    ! obj_func


c     Note: the mult. by jacobian here is used to cancel the jacobian in
c     dR/dQ, since the flow jacobian is formed with respect to the
c     J^-1(Q) variables vector while for optimization it is easier to
c     work with just Q - one can also think of it in terms of calculating
c     dJ/dQhat. The routine qmuj does the job just fine

      do ib = 1,nblks
         call qmuj(jmax(ib),kmax(ib),dfobjdQ(lqptr(ib)),
     &        xyj(lgptr(ib)),jbegin2(ib),jend2(ib),
     &        kbegin2(ib), kend2(ib))
      end do 


      return
      end    !calcdfobjdq



      


      subroutine solveadj(nmax,indx,ntvar,psi,psit,ja,ia,jpa,ipa,rhstmp,
     &     rhssatmp,as,pa,iex,bcf)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer nmax,ntvar,indx(*)
      double precision rhstmp(*),rhssatmp(*),psi(*),psit(*),pa(*),as(*)
      integer ia(*),ja(*),ipa(*),jpa(*),is,ib,lg,lq,j,its
      double precision rhs(maxjk*jacb),sol(maxjk*jacb)

c     -- shuffle arrays required to keep track of row exchanges,
c     necessary for the adjoint problem but also used in the n-k flow
c     solver --
c     -- iex: integer array of row exchanges --
c     -- bcf: boundary condition factors --
      integer iex(maxjb,4,4,mxbfine)
      double precision bcf(maxjb,4,4,mxbfine)

c     -- gmres control --
      integer ipar(16)
      double precision fpar(16)

c     -- ILU arrays --
      integer iwk,ierr
      parameter (iwk = maxjk*jacb2*5*5)
      integer,dimension(:),allocatable :: jw,levs 
      double precision,dimension(:),allocatable :: wk_ilu

c     -- local temp. sparse adjoint arrays --
      integer,dimension(:),allocatable :: iat,jat,ipat,jpat,ilu,jlu
      double precision,dimension(:),allocatable :: ast,pat,plu 
      double precision,allocatable :: wk_gmres(:)
 
cmpr  Allocate some of the big arrays dynamically
      allocate(ast(maxjk*jacb2*9),pat(maxjk*jacb2*5),plu(iwk))
      allocate(iat(maxjk*jacb+1),jat(maxjk*jacb2*9),ilu(maxjk*jacb+1))
      allocate(ipat(maxjk*jacb+1),jpat(maxjk*jacb2*5),jlu(iwk))
      allocate(wk_ilu(maxjk*nmax),jw(3*maxjk*nmax),levs(iwk))
c     -- work array *105 for gmres *8 for bcgstab--
      allocate(wk_gmres(maxjk*nmax*8))


c$$$      write (*,*) 'Checking diagonal of as...'
c$$$       
c$$$      call checkd( 1, jdim, kdim, nmax,indx, ia,ja,as,
c$$$     &     js, je,  ks, ke )
c$$$
c$$$      write (*,*) 'Checking diagonal of pa...'
c$$$       
c$$$      call checkd( 1, jdim, kdim, nmax,indx, ipa,jpa,pa,
c$$$     &     js, je,  ks, ke )

c     -- transpose -> ast=[dRstar/dQhat]^T --
      call csrcsc (ntvar, 1, 1, as, ja, ia, ast, jat, iat)
  
c     -- same for preconditioner     
      call csrcsc (ntvar, 1, 1, pa, jpa, ipa, pat, jpat, ipat)


c     -- diagonal scaling for all b.c. equations --
c     -- this operation is performed after taking the transpose,
c     special case for the adjoint problem --
      do ib=1,nblks
         do is=1,4
            if (ibctype(ib,is).gt.0 .and. ibctype(ib,is).lt.5) 
     &           call sadjoint (ib, is, jmax(ib), kmax(ib), nmax,
     &           kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib),
     &           bcf,indx(lgptr(ib)),rhstmp(lqptr(ib)),pa,as,ipa,ia)
         end do
      end do
   
c     -- setup rhs gmres --
      do j = 1,maxjk*jacb
         rhs(j) = 0.0d0
      end do

      do ib = 1,nblks
         call setrhs(jmax(ib),kmax(ib),nmax,kminbnd(ib),
     &        kmaxbnd(ib),jminbnd(ib),jmaxbnd(ib),indx(lgptr(ib)),
     &        rhstmp(lqptr(ib)),rhssatmp(lgptr(ib)),rhs,.true.)
      end do

c     -- setup initial guess (subroutine setrhs does the job just fine)--

      do j = 1,maxjk*jacb
         sol(j) = 0.0d0
      end do

      do ib = 1,nblks
         call setrhs(jmax(ib),kmax(ib),nmax,kminbnd(ib),
     &        kmaxbnd(ib),jminbnd(ib),jmaxbnd(ib),indx(lgptr(ib)),
     &        psi(lqptr(ib)),psit(lgptr(ib)),sol,.true.)
      end do

c     update psi=inv(ast)*rhs with gmres 

c     -- set the parameters for the iterative solvers in ipar(4): *105 for gmres *8 for bcgstab--

      ipar(1) = 0
      ipar(2) = 2
      ipar(3) = 1
      ipar(4) = maxjk*nmax*8
      ipar(5) = IMGMR
      ipar(6) = ITGMR
c$$$      fpar(1) = 0.d0
c$$$      fpar(2) = TOLGMR
      fpar(1) = TOLGMR
      fpar(2) = 0.d0
      fpar(11) = 0.d0

      its=0

c     -- sparskit iluk(p) preconditioner --
      call iluk (ntvar,pat,jpat,ipat,LFILG,plu,jlu,
     &      ilu,levs,iwk,wk_ilu,jw,ierr)


      if (ierr .ne. 0) print *, 'LU preconditioner error:', ierr      

 77   call BCGSTAB(ntvar,rhs,sol,ipar,fpar,wk_gmres)
           
c     --  output the residuals and update--

      if (ipar(7).ne.its) then
c         print *,its,fpar(5)
         its = ipar(7)
      endif
      
      if (ipar(1).eq.1) then
         call amux(ntvar,wk_gmres(ipar(8)),wk_gmres(ipar(9)),ast,
     &        jat,iat)
         goto 77
      else if (ipar(1).eq.2) then
         call atmux(ntvar,wk_gmres(ipar(8)),wk_gmres(ipar(9)),ast,
     &        jat,iat)
         goto 77
      else if (ipar(1).eq.3 .or. ipar(1).eq.5) then
         call lusol(ntvar,wk_gmres(ipar(8)),wk_gmres(ipar(9)),plu,
     &        jlu,ilu)
         goto 77
      else if (ipar(1).eq.4 .or. ipar(1).eq.6) then
         call lutsol(ntvar,wk_gmres(ipar(8)),wk_gmres(ipar(9)),plu,
     &        jlu,ilu)
         goto 77
      else if (ipar(1).le.0) then
         if (ipar(1).eq.0) then
c            print *, 'Iterations to converge:', its,fpar(5)
         else if (ipar(1).eq.-1) then
            print *, 'Iterative solver has iterated too many times.',
     &           fpar(5)
         else if (ipar(1).eq.-2) then
            print *, 'Iterative solver was not given enough work space.'
            print *, 'The work space should at least have ', ipar(4),
     &           ' elements.'
         else if (ipar(1).eq.-3) then
            print *, 'Iterative solver is facing a break-down.'
            stop
         else
            print *, 'Iterative solver terminated. code =', ipar(1)
            stop
         endif
      endif         

c     -- copy solution vector to psi and unshuffle it--

      do ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)
         call calcpsi(ib,jmax(ib),kmax(ib),nmax,jminbnd(ib),
     &        jmaxbnd(ib),jlow(ib),jup(ib),kminbnd(ib),kmaxbnd(ib),
     &        indx(lg),psi(lq),psit(lg),iex,sol,ibctype,.true.)
      end do

cmpr  Deallocate the big arrays
      deallocate(ast,pat,iat,jat,ipat,jpat,plu,ilu,jlu)
      deallocate(jw,levs,wk_ilu)
      deallocate(wk_gmres)

      return
      end                       !solveadj




      subroutine setupadjrhs(jdim,kdim,psi,psit,psiold,psitold,
     &           deltat,rhs,rhssa,deltatbig,count,skipend,deltattmp,
     &           js,je,ks,ke,nmax,jl,ju,kl,ku,dfobjdQ)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"


      integer jdim,kdim,js,je,ks,ke,count,skipend,nmax
      double precision deltat,deltatbig,deltattmp

      double precision psi(jdim,kdim,4),psit(jdim,kdim)
      double precision psiold(jdim,kdim,4),psitold(jdim,kdim)
      double precision rhs(jdim,kdim,4),rhssa(jdim,kdim)

      integer j,k,n,jl,ju,kl,ku
      double precision dfobjdQ(jdim,kdim,4)


      do n=1,4
         do k = ks,ke
            do j = js,je
                      
               if ( k .lt. kl .or. k .gt. ku .or.
     &              j .lt. jl .or. j .gt. ju) then 
             
                  rhs(j,k,n) = - dfobjdQ(j,k,n) 

               else

                  if (count .eq. skipend) then
                     rhs(j,k,n)=(deltat+deltatbig)*psi(j,k,n)/
     &                 (deltat*deltatbig) - psiold(j,k,n)/(2.d0*deltat)
                  else if (count .eq. skipend-1) then
                     rhs(j,k,n)=2.d0*psi(j,k,n)/deltatbig - deltat* 
     &                    psiold(j,k,n)/(deltatbig*(deltat+deltatbig))
                  else
                     rhs(j,k,n)=( 4.d0*psi(j,k,n)-psiold(j,k,n) )
     &                    /(2.d0*deltattmp)-dfobjdQ(j,k,n)
                  end if 
                    
               end if

            end do
         end do
      end do

      if (nmax .eq.5) then

         do k = ks,ke
            do j = js,je
                                          
               if ( k .lt. kl .or. k .gt. ku .or.
     &              j .lt. jl .or. j .gt. ju) then
             
                  rhssa(j,k) = 0.0d0 

               else

                  if (count .eq. skipend) then
                     rhssa(j,k)=(deltat+deltatbig)*psit(j,k)/
     &                 (deltat*deltatbig) - psitold(j,k)/(2.d0*deltat)
                  else if (count .eq. skipend-1) then
                     rhssa(j,k)=2.d0*psit(j,k)/deltatbig - deltat* 
     &                    psitold(j,k)/(deltatbig*(deltat+deltatbig))
                  else
                     rhssa(j,k)=( 4.d0*psit(j,k)-psitold(j,k) )
     &                    /(2.d0*deltattmp)
                  end if 
                    
               end if               
           
            end do
         end do
       
      end if   ! nmax .eq.5



      return
      end   !setupadjrhs





      subroutine assemblemultiplier(jdim,kdim,nmax,stage,psies,psites,
     &                 psiold,psitold,js,je,ks,ke)

      implicit none

#include "../include/common.inc"

      integer jdim,kdim,nmax,js,je,ks,ke,stage,j,k,n,i_stage

      double precision psies(jdim,kdim,4,6),psites(jdim,kdim,6)
      double precision psiold(jdim,kdim,4),psitold(jdim,kdim),coef


      DO n = 1,4
         DO k = ks,ke
            DO j = js,je
               DO i_stage = 6,stage+1,-1
                  coef = ajk(i_stage,stage)/ajk(i_stage,i_stage)
                  psiold(j,k,n)=psiold(j,k,n)+coef*psies(j,k,n,i_stage)
               ENDDO
               psiold(j,k,n)=psiold(j,k,n)+psies(j,k,n,stage)
            ENDDO
         ENDDO
      ENDDO

      if (nmax .eq.5) then

         DO k = ks,ke
            DO j = js,je
               DO i_stage = 6,stage+1,-1
                  coef = ajk(i_stage,stage)/ajk(i_stage,i_stage)
                  psitold(j,k)=psitold(j,k)+coef*psites(j,k,i_stage)
               ENDDO
               psitold(j,k)=psitold(j,k)+psites(j,k,stage)
            ENDDO
         ENDDO

      end if ! nmax .eq.5 

      return
      end   !assemblemultiplier






      subroutine calcperturbrhs(count2,q,turre,Rs,Rsa,xsave,ysave,turmu,
     &     vort,fmu, press,precon,sndsp,ds,vk,ve,coef2x,coef4x,coef2y,  
     &     coef4y,uu,vv,ccx,ccy,spectxi,specteta,tmp)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/units.inc"
#include "../include/mbcom.inc"
#include "../include/scratch.inc"
#include "../include/turb.inc"


      double precision q(*),turre(*),Rs(*),Rsa(*),xsave(*),ysave(*) 
      double precision xy(maxjk*4),xyj(maxjk),xit(maxjk)
      double precision ett(maxjk),x(maxjk),y(maxjk),exprhs(maxjkq)
      double precision exprsa(maxjk)
      double precision vort(*),fmu(*),press(*),sndsp(*),turmu(*)
      double precision tmp(*),ds(*),coef4x(*),coef4y(*),precon(*)
      double precision uu(*),vv(*),ccx(*),spectxi(*),specteta(*)
      double precision ccy(*),coef2x(*),coef2y(*),vk(*),ve(*)
      double precision dhatq(6*maxjkq),dhatt(6*maxjk)
      integer nside1,nside2,ii,i2,ib,lg,lq,count2

c     Calculate new boundary and regrid from first grid

      do ib = 1,nblks
         lg = lgptr(ib)
         call copy_array(jmax(ib),kmax(ib),1,xsave(lg),x(lg))
         call copy_array(jmax(ib),kmax(ib),1,ysave(lg),y(lg))
      end do

      call regrid(-1,x,y)
      
      do ii=1,nblks
         call xymets(ii,nblks,nhalo,jmax(ii),kmax(ii),
     &        x(lgptr(ii)),y(lgptr(ii)),xy(lqptr(ii)),
     &        xit(lgptr(ii)),ett(lgptr(ii)),xyj(lgptr(ii)), 
     &        jbegin(ii),jend(ii),kbegin(ii),kend(ii),
     &        jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii))
      end do

c     -- copy column of metrics for halos --
      do ib = 1,nblks
         do nside1 = 2,4,2
            i2 = lblkpt(ib,nside1)
            if (i2.ne.0) then
               nside2 = lsidept(ib,nside1)
               call fixmets( nhalo, nside1, nside2, jmax(ib),
     &              kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &              jbegin2(ib), jend2(ib),
     &              kminbnd(ib),kmaxbnd(ib),jmax(i2),kmax(i2),
     &              xy(lqptr(i2)), xyj(lgptr(i2)),
     &              jbegin2(i2), jend2(i2),
     &              kminbnd(i2), kmaxbnd(i2))
            endif
         end do
      end do
               
c     -- halo row metrics copy --
      do ib = 1, nblks
         do nside1 = 1,3,2
            i2 = lblkpt(ib,nside1)
            if (i2.ne.0 .and. 
     &           (ibctype(ib,nside1).ne.5 .or. nhalo.ge.2)) then
               nside2 = lsidept(ib,nside1)
               call fixmets( nhalo, nside1, nside2, jmax(ib),
     &              kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &              jbegin2(ib), jend2(ib),
     &              kbegin2(ib), kend2(ib),
     &              jmax(i2), kmax(i2), xy(lqptr(i2)),
     &              xyj(lgptr(i2)), jbegin2(i2),
     &              jend2(i2), kbegin2(i2), kend2(i2) )
            endif
         end do
      end do

c     -- put in jacobian Q->Qhat --
      do ii=1,nblks
         call scjac(ii, jmax(ii), kmax(ii), q(lqptr(ii)), 
     &        xyj(lgptr(ii)),jbegin2(ii),jend2(ii),
     &        kbegin2(ii), kend2(ii))
      end do

c     -- calculate pressure and sound speed,fmu doesn't need to be 
c        updated since it is independent of metric -- 
      do ii=1,nblks
         lq = lqptr(ii)
         lg = lgptr(ii)
         call calcps(ii, jmax(ii), kmax(ii), q(lq), press(lg),
     &        sndsp(lg), precon(lprecptr(ii)),xy(lq),xyj(lg),
     &        jbegin2(ii),jend2(ii),kbegin2(ii), kend2(ii))
      end do

c     -- update distances for the s-a model (array smin) --
      if (viscous .and. turbulnt .and. iturb.eq.3) then            
         call autostan(1,1,maxj,maxk,x,y,d,d(1,2),.false.)
         do ib=1,nblks
            lg = lgptr(ib)
            call caldist(jmax(ib),kmax(ib),x(lg),y(lg),
     &           smin(lg),jlow(ib),jup(ib),klow(ib),kup(ib),
     &           icoord,xcoord,ycoord)
         end do
      endif

      dt=1.d0
      UNSTEADY=.false. !done for speed, doesn't affect the result

      call get_rhs(q,q,q,press,sndsp,Rs,xy,xyj,xit,ett,ds,x,y,turmu,
     &    vort,turre,turre,turre,fmu,vk,ve,tmp,uu,vv,ccx,ccy,coef2x,
     &    coef2y,coef4x,coef4y,precon,spectxi,specteta,Rsa,exprhs,dhatq,
     &    exprsa,dhatt,.false.)

      UNSTEADY=.true.
      
c     -- remove jacobian scaling Qhat->Q --
      do ii = 1,nblks
         call qmuj(jmax(ii),kmax(ii),q(lqptr(ii)),
     &        xyj(lgptr(ii)),jbegin2(ii),jend2(ii),
     &        kbegin2(ii), kend2(ii))
      end do

      return
      end                       !calcperturbrhs





      subroutine calcdRdXsb(count,deltattmp,deltatbig,ib,fgrad,jmaxt,
     &                      kmaxt,js,je,ks,ke,psi,skipend)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer count,ib,jmaxt,kmaxt,js,je,ks,ke,count2,j,k,skipend
      double precision deltattmp,deltatbig,psi(jmaxt,kmaxt,4)
      double precision fgrad(ndv),timetmp,domegadX(2),dRdX

      if (omegaf .eq. 0.d0) then
         domegadX(1)=1.d0
         domegadX(2)=0.d0
      else  
         if (count .le. skipend) then
            timetmp=count*deltattmp
         else
            timetmp=(count-skipend)*deltattmp+skipend*deltatbig
         end if
         domegadX(1)=dsin(2.d0*pi*omegaf*timetmp)
         domegadX(2)=2.d0*pi*timetmp*omegaa*
     &        dcos(2.d0*pi*omegaf*timetmp)
      endif
      
      do count2=1,ndv
         
         do k = 36,38
            
            if (count2.le.2) then !w.r.t. omegaa,omegaf
               dRdX=-rhoinf*domegadX(count2)*
     &              dcos(sbeta*pi/180.d0-sdelta)
            elseif  (count2 .eq. 3) then !w.r.t. sbeta
               dRdX=rhoinf*omegaa*domegadX(1)*
     &              dsin(sbeta*pi/180.d0-sdelta)*pi/180.d0
            end if
            
            fgrad(count2) = fgrad(count2) + dRdX*psi(js,k,2)
            
            if (count2.le.2) then !w.r.t. omegaa,omegaf
               dRdX=-rhoinf*domegadX(count2)*
     &              dsin(sbeta*pi/180.d0-sdelta)
            elseif (count2 .eq. 3) then !w.r.t. sbeta
               dRdX=-rhoinf*omegaa*domegadX(1)*
     &              dcos(sbeta*pi/180.d0-sdelta)*pi/180.d0
            end if 

            fgrad(count2) =  fgrad(count2) + dRdX*psi(js,k,3)
            
         end do
         
      end do  ! count2
      
      return
      end     !calcdRdXsb





      subroutine calcperturbobj(fobj,xsave,ysave,nout,skipend,
     &     deltat,scalef)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"


      double precision xsave(*),ysave(*),fobj,deltat,scalef
      double precision xy(maxjk*4),xyj(maxjk),xit(maxjk)
      double precision ett(maxjk),x(maxjk),y(maxjk)  
      double precision cdmeantmp,clmeantmp
      complex*16,allocatable :: dpodtmp(:,:,:,:)      
      double precision potmp(tdim,sdim),wintmp(tdim),nstmp(sdim,2)
      integer nside1,nside2,ii,i2,ib,lg,nout,skipend

      allocate(dpodtmp(Nobs,tdim,sdim,3))

c     Calculate new boundary and regrid from first grid

      do ib = 1,nblks
         lg = lgptr(ib)
         call copy_array(jmax(ib),kmax(ib),1,xsave(lg),x(lg))
         call copy_array(jmax(ib),kmax(ib),1,ysave(lg),y(lg))
      end do

      call regrid(-1,x,y)
      
      do ii=1,nblks
         call xymets(ii,nblks,nhalo,jmax(ii),kmax(ii),
     &        x(lgptr(ii)),y(lgptr(ii)),xy(lqptr(ii)),
     &        xit(lgptr(ii)),ett(lgptr(ii)),xyj(lgptr(ii)), 
     &        jbegin(ii),jend(ii),kbegin(ii),kend(ii),
     &        jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii))
      end do

c     -- copy column of metrics for halos --
      do ib = 1,nblks
         do nside1 = 2,4,2
            i2 = lblkpt(ib,nside1)
            if (i2.ne.0) then
               nside2 = lsidept(ib,nside1)
               call fixmets( nhalo, nside1, nside2, jmax(ib),
     &              kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &              jbegin2(ib), jend2(ib),
     &              kminbnd(ib),kmaxbnd(ib),jmax(i2),kmax(i2),
     &              xy(lqptr(i2)), xyj(lgptr(i2)),
     &              jbegin2(i2), jend2(i2),
     &              kminbnd(i2), kmaxbnd(i2))
            endif
         end do
      end do
               
c     -- halo row metrics copy --
      do ib = 1, nblks
         do nside1 = 1,3,2
            i2 = lblkpt(ib,nside1)
            if (i2.ne.0 .and. 
     &           (ibctype(ib,nside1).ne.5 .or. nhalo.ge.2)) then
               nside2 = lsidept(ib,nside1)
               call fixmets( nhalo, nside1, nside2, jmax(ib),
     &              kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &              jbegin2(ib), jend2(ib),
     &              kbegin2(ib), kend2(ib),
     &              jmax(i2), kmax(i2), xy(lqptr(i2)),
     &              xyj(lgptr(i2)), jbegin2(i2),
     &              jend2(i2), kbegin2(i2),
     &              kend2(i2) )
            endif
         end do
      end do

      if (iobjf .eq. 1) then
c     -- inverse design entire domain --            
         call calcobjair(fobj,xyj,nout,skipend,deltat)

      else if (iobjf .eq. 0) then
c     --  mean drag minimization --
            
         call calcobjair0(fobj,nout,skipend,x,y,xy,xyj,scalef)

      else if (iobjf .eq. 5) then
c     --  (mean drag / mean lift)  minimization --
            
         call calcobjair5(fobj,nout,skipend,x,y,xy,xyj,cdmeantmp,
     &                       clmeantmp,scalef)

      else if (iobjf .eq. 8) then
c     -- inverse remote design --
         call calcobjair8(fobj,nout,skipend,deltat,scalef,x,y)

      else if (iobjf .eq. 10 .or. iobjf .eq. 11) then
c     -- design using FWH--            
         call calcobjairnoise(fobj,nout,skipend,deltat,scalef,
     &        x,y,xy,dpodtmp,wintmp,potmp,nstmp,.false.)

      end if                    ! iobjf?

      deallocate(dpodtmp)

      return
      end                       !calcperturbobj






      subroutine filldiages(jdim,kdim,nmax,jl,ju,kl,ku,
     &                      indx,vec,fac)

      implicit none      
      integer jdim,kdim,nmax,indx(jdim,kdim),jl,ju,kl,ku
      integer j,k,n,jk
      double precision vec(*),fac
 
      do k=kl,ku
         do j=jl,ju                                 
            jk = (indx(j,k) - 1)*nmax                                     
            do n=1,nmax                
               vec(jk+n) = fac 
            end do
         end do
      end do

      return
      end   !filldiages





      subroutine copyindexvsmatrix(jdim,kdim,nmax,fac,indx,q1,
     &           q2,q2t,maxstage,stage,js,je,ks,ke,ToMatrix)

      implicit none

      integer jdim,kdim,nmax,ks,ke,js,je,indx(jdim,kdim),j,k,n,jk,stage
      integer maxstage
      double precision q2(jdim,kdim,4,maxstage),q2t(jdim,kdim,maxstage)
      double precision q1(*),fac
      logical ToMatrix

      if (ToMatrix) then

         do n = 1,4
            do k = ks,ke
               do j = js,je
                  jk = (indx(j,k) - 1)*nmax + n
                  q1(jk) = fac*q2(j,k,n,stage)
               end do
            end do
         end do 

         if (nmax.eq.5) then
            do k = ks,ke
               do j = js,je
                  jk = (indx(j,k) - 1)*5 + 5
                  q1(jk) = fac*q2t(j,k,stage)
               end do
            end do
         end if
         
      else 

        do n = 1,4
            do k = ks,ke
               do j = js,je
                  jk = (indx(j,k) - 1)*nmax + n
                  q2(j,k,n,stage) = q1(jk) 
               end do
            end do
         end do 

         if (nmax.eq.5) then
            do k = ks,ke
               do j = js,je
                  jk = (indx(j,k) - 1)*5 + 5
                  q2t(j,k,stage) = q1(jk) 
               end do
            end do
         end if

      end if


      return
      end  !copyindexvsmatrix



C SUBROUTINE PNPOLY 
C 
C PURPOSE 
C TO DETERMINE WHETHER A POINT IS INSIDE A POLYGON 
C 
C USAGE 
C CALL PNPOLY (PX, PY, XX, YY, N, INOUT ) 
C 
C DESCRIPTION OF THE PARAMETERS 
C PX - X-COORDINATE OF POINT IN QUESTION. 
C PY - Y-COORDINATE OF POINT IN QUESTION. 
C XX - N LONG VECTOR CONTAINING X-COORDINATES OF 
C VERTICES OF POLYGON. 
C YY - N LONG VECTOR CONTAING Y-COORDINATES OF 
C VERTICES OF POLYGON. 
C N - NUMBER OF VERTICES IN THE POLYGON. 
C INOUT - THE SIGNAL RETURNED: 
C -1 IF THE POINT IS OUTSIDE OF THE POLYGON, 
C 0 IF THE POINT IS ON AN EDGE OR AT A VERTEX, 
C 1 IF THE POINT IS INSIDE OF THE POLYGON. 
C 
C REMARKS 
C THE VERTICES MAY BE LISTED CLOCKWISE OR ANTICLOCKWISE. 
C THE FIRST MAY OPTIONALLY BE REPEATED, IF SO N MAY 
C OPTIONALLY BE INCREASED BY 1. 
C THE INPUT POLYGON MAY BE A COMPOUND POLYGON CONSISTING 
C OF SEVERAL SEPARATE SUBPOLYGONS. IF SO, THE FIRST VERTEX 
C OF EACH SUBPOLYGON MUST BE REPEATED, AND WHEN CALCULATING 
C N, THESE FIRST VERTICES MUST BE COUNTED TWICE. 
C INOUT IS THE ONLY PARAMETER WHOSE VALUE IS CHANGED. 
C THE SIZE OF THE ARRAYS MUST BE INCREASED IF N > MAXDIM 
C WRITTEN BY RANDOLPH FRANKLIN, UNIVERSITY OF OTTAWA, 7/70. 
C 
C SUBROUTINES AND FUNCTION SUBPROGRAMS REQUIRED 
C NONE 
C 
C METHOD 
C A VERTICAL LINE IS DRAWN THRU THE POINT IN QUESTION. IF IT 
C CROSSES THE POLYGON AN ODD NUMBER OF TIMES, THEN THE 
C POINT IS INSIDE OF THE POLYGON. 
C 
C .................................................................. 
      SUBROUTINE PNPOLY(PX,PY,XX,YY,N,INOUT)
 
      REAL X(2000),Y(2000),XX(N),YY(N) 
      LOGICAL MX,MY,NX,NY 
      INTEGER O 
C OUTPUT UNIT FOR PRINTED MESSAGES 
      DATA O/6/ 

      MAXDIM=2000 

      IF(N.LE.MAXDIM)GO TO 6 
      WRITE(*,7) N
 7    FORMAT('0WARNING:',I5,' TOO GREAT FOR THIS VERSION OF PNPOLY') 
      RETURN 

 6    DO I=1,N 
         X(I)=XX(I)-PX 
         Y(I)=YY(I)-PY 
      END DO

      INOUT=-1 

      DO 2 I=1,N 
      J=1+MOD(I,N) 
      MX=X(I).GE.0.0 
      NX=X(J).GE.0.0 
      MY=Y(I).GE.0.0 
      NY=Y(J).GE.0.0 
      IF(.NOT.((MY.OR.NY).AND.(MX.OR.NX)).OR.(MX.AND.NX)) GO TO 2 
      IF(.NOT.(MY.AND.NY.AND.(MX.OR.NX).AND..NOT.(MX.AND.NX))) GO TO 3 
      INOUT=-INOUT 
      GO TO 2  
 3    IF((Y(I)*X(J)-X(I)*Y(J))/(X(J)-X(I))) 2,4,5  
 4    INOUT=0 
      RETURN  
 5    INOUT=-INOUT  
 2    CONTINUE
 
      RETURN 
      END 
