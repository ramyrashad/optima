C ********************************************************************
C ********************* LIFT AND DRAG CALCULATIONS *******************
C ********************************************************************
      subroutine clcdho(jdim,kdim,iside,idir,q,press,x,y,xy,xyj,
     &      nscal,cl,cd,cm,clvv,cdvv,cmvv,
     &      jbegin,jend,kbegin,kend)
c
#include "../include/parms.inc"
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim)
      logical   oddside
c
      dimension z(maxj),work(maxj,4)
      common/worksp/z,work
c
c     -if idir=0 don't integrat the pressure (used for inviscid walls
c      in viscous problems)
c
      if (idir.eq.0) return
c
c     -the parameter nscal determines whether the q variables are scaled
c      by the metric jacobians xyj.
c
c     -this routine supplies force and moment coeffs.  on airfoil surface
c
      sgn = float(sign(1,idir))
      cpc = 2./(gamma*fsmach**2)
c
      cl = 0.
      cd = 0.
      cm = 0.
      clvv = 0.
      cdvv = 0.
      cmvv = 0.
c
c     -set limits of integration
      j1 = jbegin
      j2 = jend
      k2 = kend
      jtp = jbegin + 1
      ktp = kbegin + 1
      if (iside.eq.1) k1 = kbegin
      if (iside.eq.2) j1 = jbegin
      if (iside.eq.3) k1 = kend
      if (iside.eq.4) j1 = jend
c
c     Note: This subroutine is validated for iside=1,3 and not really
c     run worthy for sides 2 & 4 ... since never run with side 2 or 4
c     on airfoil body.     
c
c     -set side type
      if ((iside.eq.1).or.(iside.eq.3)) then
        oddside = .true.
      else
        oddside = .false.
      endif
c
c     -compute cp at grid points and store in z array
      if (oddside) then
        do 10 j=j1,j2
          pp = press(j,k1)
          if (nscal.eq.0) pp = pp*xyj(j,k1)
          z(j) = (pp*gamma -1.)*cpc
   10   continue
      else
        do 11 k=k1,k2
          pp = press(j1,k)
          if (nscal.eq.0) pp = pp*xyj(j1,k)
          z(k) = (pp*gamma -1.)*cpc
   11   continue
      endif
c
c     -compute normal force coefficient and chord directed force coeff
c     -chord taken as one in all cases
c
      cn = 0.
      cc = 0.
      cmle = 0.
      if (oddside) then
        if (matcharc2d) then 
          do 12 j=jtp,j2
            jm1 = j-1
            cpav = (z(j) + z(jm1))*.5
            cn = cn - cpav*(x(j,k1) - x(jm1,k1))
            cc = cc + cpav*(y(j,k1) - y(jm1,k1))
            cmle=cmle+cpav*(x(j,k1)+x(jm1,k1))*.5*(x(j,k1)-x(jm1,k1))
 12       continue
        else
          do 13 j=jtp,j2
            jm1 = j-1
            cpav = (z(j) + z(jm1))*.5
            cn = cn - cpav*(x(j,k1) - x(jm1,k1))
            cc = cc + cpav*(y(j,k1) - y(jm1,k1))
            cmle=cmle+cpav*((x(j,k1)+x(jm1,k1))*.5*(x(j,k1)-x(jm1,k1))
     &                     +(y(j,k1)+y(jm1,k1))*.5*(y(j,k1)-y(jm1,k1)))
 13       continue
        endif
      else
        if (matcharc2d) then
          do 14 k=ktp,k2
            cpav = (z(k) + z(k-1))*.5
            cn = cn - cpav*(x(j1,k) - x(j1,k-1))
            cc = cc + cpav*(y(j1,k) - y(j1,k-1))
            cmle=cmle+cpav*(x(j1,k)+x(j1,k-1))*.5*(x(j1,k)-x(j1,k-1))
 14       continue
        else
          do 15 k=ktp,k2
            cpav = (z(k) + z(k-1))*.5
            cn = cn - cpav*(x(j1,k) - x(j1,k-1))
            cc = cc + cpav*(y(j1,k) - y(j1,k-1))
            cmle=cmle+cpav*((x(j1,k)+x(j1,k-1))*.5*(x(j1,k)-x(j1,k-1))
     &                     +(y(j1,k)+y(j1,k-1))*.5*(y(j1,k)-y(j1,k-1)))
 15       continue
        endif
      endif
c
c     **************************************
c     correct for curves going the wrong way
c     **************************************
      cn = sgn*cn
      cc = sgn*cc
      cmle = sgn*cmle
c
      cl = cn*cos(alpha*pi/180.)-cc*sin(alpha*pi/180.)
      cd = cn*sin(alpha*pi/180.) + cc*cos(alpha*pi/180.)
      cmqc = cmle +.25*cn
c
c     note: cmle is moment about (x,y)=(0,0) 
c                             but cmqc is moment about (0.25,0)
      cm = cmqc
c
c
c
c
c
c     *********************************************
c      Viscous Coefficient of Friction Calculation
c     *********************************************
      if(viscous)then
c
c
c     -calculation taken from p. buning
c
c      calculate the skin friction coefficient                             
c                                                                       
c      c  = tau   /            2                                        
c       f      w / 1/2*rho   *u                                         
c                         inf  inf                                      
c                                                                       
c      tau  = mu*(du/dy-dv/dx)                                          
c         w                   w                                         
c                                                                       
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc., 
c     york, 1974, p. 50, but use freestream values instead of edge values.
c                                                                       
c                                                                       
c     -for calculating cf, we need the coefficient of viscosity, mu.  use  
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1, rhoinf=1.  
c
c
c     -include viscous contribution only on sides 1 & 3 for now !!!!
c      not on blunt trailing edges
c
        if (.not.oddside) return
c     
        if (iside.eq.1) kadd = 1
        if(iside.eq.3) kadd = -1
c
        k  = k1
        k2 = k1 + kadd
        k3 = k2 + kadd
        k4 = k3 + kadd
        k5 = k4 + kadd
c
        cinf  = 1.
        alngth= 1.
c       -re already has fsmach scaling
        amu   = rhoinf*alngth/re
        uinf2 = fsmach**2
c
c
        tmp=float(kadd)/12.d0
        do 110 j = j1,j2
c         -xi direction
          uxi = 0.d0
          vxi = 0.d0
c
c         -eta direction
          r=1.d0/q(j,k1,1)
          rp1=1.d0/q(j,k2,1)
          rp2=1.d0/q(j,k3,1)
          rp3=1.d0/q(j,k4,1)
          rp4=1.d0/q(j,k5,1)
c
          u  =q(j,k1,2)*r
          up1=q(j,k2,2)*rp1
          up2=q(j,k3,2)*rp2
          up3=q(j,k4,2)*rp3
          up4=q(j,k5,2)*rp4
c
          v  =q(j,k1,3)*r
          vp1=q(j,k2,3)*rp1
          vp2=q(j,k3,3)*rp2
          vp3=q(j,k4,3)*rp3
          vp4=q(j,k5,3)*rp4
c     
          ueta= tmp*(-25.d0*u +48.d0*up1-36.d0*up2+16.d0*up3-3.d0*up4) 
          veta= tmp*(-25.d0*v +48.d0*vp1-36.d0*vp2+16.d0*vp3-3.d0*vp4) 
c
          xix = xy(j,k,1)
          xiy = xy(j,k,2)
          etax = xy(j,k,3)
          etay = xy(j,k,4)
          tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
          z(j)= tauw/(.5*rhoinf*uinf2)
 110    continue
c
c       -compute viscous normal and axial forces
c       -compute normal force coefficient and chord directed force coeff
c        chord taken as one in all cases
c
        cnv = 0.
        ccv = 0.
        cmlev = 0.
        do 112 j=jtp,j2
          jm1 = j-1
          cfav = (z(j) + z(jm1))*.5
          ccv = ccv + cfav*(x(j,k) - x(jm1,k))
          cnv = cnv + cfav*(y(j,k) - y(jm1,k))
          cmlev = cmlev + cfav*(
     &          (x(j,k)+x(jm1,k))*.5*(y(j,k) -y(jm1,k)) -
     &          (y(j,k)+y(jm1,k))*.5*(x(j,k) -x(jm1,k))  )
 112    continue
c
c       **************************************
c       correct for curves going the wrong way
c       **************************************
        cnv   = sgn*cnv
        ccv   = sgn*ccv
        cmlev = sgn*cmlev
c
        clvv = cnv*cos(alpha*pi/180.)-ccv*sin(alpha*pi/180.)
        cdvv = cnv*sin(alpha*pi/180.) + ccv*cos(alpha*pi/180.)
        cmqcv = cmlev +.25*cnv
        cmvv = cmqcv
      endif
c
      return
      end
