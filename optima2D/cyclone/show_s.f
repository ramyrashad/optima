      subroutine show_s(jdim,kdim,s)
c
#include "../include/arcom.inc"
c
      dimension s(jdim,kdim,4)

      do 3 n=1,4
      do 3 k=1,kdim
      do 3 j=1,jdim
         write(*,*)'s(',j,',',k,',',n,') = ',s(j,k,n)
 3    continue

      return
      end

