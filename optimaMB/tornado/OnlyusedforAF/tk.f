c***********************************************************************
c***************** tk multiply on s ************************************
c***********************************************************************
      subroutine tk(ix,iy,jdim,kdim,q,sndsp,s,xyj,xy,precon,
     &          jlow,jup,klow,kup)
c
c  ix = 1, iy = 2  xi  metrics
c  ix = 3, iy = 4  eta metrics
c
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),sndsp(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),precon(jdim,kdim,8)
c
      bt = 0.7071067811865475 
c      bt = 1./sqrt(2.)
      rgami = 1./gami
c
c     t  matrix multiply for diagonal algorithm
c      k
c
      if (prec.eq.0) then
        do 1 k = klow, kup
        do 1 j = jlow,jup
          rx      = xy(j,k,ix)
          ry      = xy(j,k,iy)
          rrxy    = 1./sqrt(rx**2+ry**2)
          rx      = rx*rrxy
          ry      = ry*rrxy
c
          rho    = q(j,k,1)*xyj(j,k)
          rhoinv = 1./q(j,k,1)
          u = q(j,k,2)*rhoinv
          v = q(j,k,3)*rhoinv
          alp0 = rho*bt/sndsp(j,k)
          s1 = s(j,k,1)
          s2 = s(j,k,2)
          sa = (s(j,k,3) + s(j,k,4))*alp0
          sm = (s(j,k,3) - s(j,k,4))*rho*bt
          uv2 = 0.5*(u**2 + v**2)
          rrx = rho*rx
          rry = rho*ry
c     
          s(j,k,1) = s1 + sa
          s(j,k,2) = u*s1 + rry*s2 + u*sa + rx*sm
          s(j,k,3) = v*s1 - rrx*s2 + v*sa + ry*sm
          s(j,k,4) = uv2*s1 + (rry*u - rrx*v)*s2 +
     >          (uv2+sndsp(j,k)**2*rgami)*sa +
     >          (rx*u + ry*v)*sm
c     
 1      continue
      else
cdu   use the preconditioned tk
cdu
        do 1000 k=klow,kup
        do 1000 j=jlow,jup
          dkx=xy(j,k,ix)
          dky=xy(j,k,iy)
c     
          u=q(j,k,2)/q(j,k,1)
          v=q(j,k,3)/q(j,k,1)
          uk=dkx*u+dky*v
c     
          dkb=sqrt(dkx**2+dky**2)
          dkx=dkx/dkb
          dky=dky/dkb
c     
          du1=1.0/(dkb*sndsp(j,k))
          du2=(-uk+precon(j,k,ix+4)+precon(j,k,iy+4))
          du3=(uk-precon(j,k,ix+4)+precon(j,k,iy+4))
c     
          t1=du1*(du2*s(j,k,3)+du3*s(j,k,4))
          t2=dky*s(j,k,2)+
     +          dkx*(s(j,k,3)-s(j,k,4))
          t3=-dkx*s(j,k,2)+
     +          dky*(s(j,k,3)-s(j,k,4))
          t4=s(j,k,1)
c     
          s(j,k,1)=t1
          s(j,k,2)=t2
          s(j,k,3)=t3
          s(j,k,4)=t4
 1000   continue
      endif
c     
      return
      end
