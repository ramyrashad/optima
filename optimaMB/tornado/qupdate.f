c     ------------------------------------------------------------------
c     -- solution update --
c     -- m. nemec, sept. 2001 --
c     modified by M. Rumpfkeil
c     May 2006
c     -- calling routine: nksolve.f --
c     ------------------------------------------------------------------
      subroutine qupdate( ib, jmax, kmax, nmax, q, indx, turre, xyj,
     &     sol, js, je, ks, ke)

      implicit none

#include "../include/parms.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"

      integer ib,jmax,kmax,nmax,indx(jmax,kmax),js,ks,je,ke,j,k,n,jk
      double precision sol(*), q(jmax,kmax,4)
      double precision turre(jmax,kmax), xyj(jmax,kmax)

      do n = 1,4     
         do k = ks,ke    
            do j = js,je
               jk = ( indx(j,k)-1)*nmax + n
               q(j,k,n) = q(j,k,n) + sol(jk)
            end do
         end do
      end do

      if (nmax.eq.5) then
         do k = ks,ke    
            do j = js,je

               jk = ( indx(j,k)-1)*5 + 5
               turre(j,k) = turre(j,k) + sol(jk)*xyj(j,k)

c              -- clip negative nutilde values --
               if (turre(j,k).lt.0.0d0 .and. 
     &              ((.not.unsteady .and. resid.gt.afres*1.d2).or.
     &              (unsteady .and. resid.gt.dualres*1.d2))) then

c                  print *, 'Negative',ib,j,k,turre(j,k)

                  if ( (ibctype(ib,1).eq.1 .and. k .eq. ks) .or.
     &                 (ibctype(ib,2).eq.1 .and. j .eq. js) .or.
     &                 (ibctype(ib,3).eq.1 .and. k .eq. ke) .or.
     &                 (ibctype(ib,4).eq.1 .and. j .eq. je) ) then
c                    -- airfoil body -- 
                     turre(j,k) = 1.0d-14                   

                  else
c                    -- other domain --
                     turre(j,k) = retinf
c                     turre(j,k) = abs(turre(j,k))

                  end if

               end if  !turre < 0.0

            end do
         end do
      end if  !nmax==5

      return                     
      end                       !qupdate
