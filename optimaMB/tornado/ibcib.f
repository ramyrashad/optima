c-----------------------------------------------------------------------
c     -- implicit body boundary condition for inviscid flow --
c     -- loosely based on probe (a. pueyo) --
c     -- m. nemec, july 2001 --
c-----------------------------------------------------------------------
      subroutine ibcib( imps, iblk, iside, jmax, kmax, jbegin, jend, k1,
     &     k2, k3, indx, q, xy, xyj, pa, as, icol, iex, bcf, gamma,
     &     gami, fsmach, maxjb, mxbfine, precmat, jacmat)

      implicit none 

      integer iblk, iside, jmax, kmax, jbegin, jend, k1, k2, k3
      integer maxjb,mxbfine,j,k,n1,n2,n3,nn,ki,ij,ip,ii,itmp
      integer indx(jmax,kmax),iex(maxjb,4,4,mxbfine),icol(*)   
      double precision q(jmax,kmax,4),pa(*),xy(jmax,kmax,4),as(*)
      double precision bcf(maxjb,4,4,mxbfine),xyj(jmax,kmax)
      double precision gamma,gami,fsmach,hinf,xy3,xy4,par,rho,u,v
      double precision b1,b0
      logical   precmat,jacmat,imps

c     -- local arrays --
      double precision pm(4,4), pn(4,4), cc(4,4), c1(4,4), c2(4,4)

c      write (*,*) 'ibcib', precmat, jacmat

      hinf = 1.0/gami + 0.5*fsmach**2

      do j = jbegin,jend
c     -- matrix for k=k1 --
         k = k1
         xy3 = xy(j,k,3)
         xy4 = xy(j,k,4)
         par = sqrt(xy3*xy3 + xy4*xy4)
         xy3 = xy3/par
         xy4 = xy4/par
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
c     -- matrix m1**-1 --
         pm(1,1) = 1.0
         pm(1,2) = 0.0
         pm(1,3) = 0.0
         pm(1,4) = 0.0
         pm(2,1) = -u/rho
         pm(2,2) = 1.0/rho
         pm(2,3) = 0.0
         pm(2,4) = 0.0
         pm(3,1) = -v/rho
         pm(3,2) = 0.0
         pm(3,3) = 1.0/rho
         pm(3,4) = 0.0
         pm(4,1) = 0.5*(u*u+v*v)*gami
         pm(4,2) = -u*gami
         pm(4,3) = -v*gami
         pm(4,4) = gami
c     -- matrix pn --
         pn(1,1) = 0.0
         pn(1,2) = xy3
         pn(1,3) = xy4
         pn(1,4) = 0.0
         pn(2,1) = 0.0
         pn(2,2) = xy4
         pn(2,3) =-xy3
         pn(2,4) = 0.0
         pn(3,1) = 0.0
         pn(3,2) = 0.0
         pn(3,3) = 0.0
         pn(3,4) = 1.0
         pn(4,1) = 0.5*(u*u+v*v) - hinf
         pn(4,2) = q(j,k,2)*xyj(j,k)
         pn(4,3) = q(j,k,3)*xyj(j,k)
         pn(4,4) = gamma/gami
c     -- multiplication --
         do n2 =1,4
            do n1 =1,4
               cc(n1,n2) = 0.0
               do nn =1,4
                  cc(n1,n2) = cc(n1,n2) + pn(n1,nn)*pm(nn,n2)*xyj(j,k)
               end do
            end do
         end do

c     -- matrix for k=k2 --
         k = k2
         xy3 = xy(j,k,3)
         xy4 = xy(j,k,4)
         par = sqrt(xy3*xy3 + xy4*xy4)
         xy3 = xy3/par
         xy4 = xy4/par
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
c     -- matrix m2**-1 --
         pm(1,1) = 1.0
         pm(1,2) = 0.0
         pm(1,3) = 0.0
         pm(1,4) = 0.0
         pm(2,1) = -u/rho
         pm(2,2) = 1.0/rho
         pm(2,3) = 0.0
         pm(2,4) = 0.0
         pm(3,1) = -v/rho
         pm(3,2) = 0.0
         pm(3,3) = 1.0/rho
         pm(3,4) = 0.0
         pm(4,1) = 0.5*(u*u+v*v)*gami
         pm(4,2) = -u*gami
         pm(4,3) = -v*gami
         pm(4,4) = gami
c     -- matrix pn --
         pn(1,1) = 0.0
         pn(1,2) = 0.0
         pn(1,3) = 0.0
         pn(1,4) = 0.0
         pn(2,1) = 0.0
         pn(2,2) = xy4
         pn(2,3) =-xy3
         pn(2,4) = 0.0
         pn(3,1) = 0.0
         pn(3,2) = 0.0
         pn(3,3) = 0.0
         pn(3,4) = 1.0
         pn(4,1) = 0.0
         pn(4,2) = 0.0
         pn(4,3) = 0.0
         pn(4,4) = 0.0
c     -- multiplication --
         do n2 =1,4
            do n1 =1,4
               c1(n1,n2) = 0.0
               do nn =1,4
                  c1(n1,n2) = c1(n1,n2) - 2.0*pn(n1,nn)*pm(nn,n2)
     &                 *xyj(j,k)
               end do
            end do
         end do

c     -- matrix for k=3 --
         k = k3
         xy3 = xy(j,k,3)
         xy4 = xy(j,k,4)
         par = sqrt(xy3*xy3 + xy4*xy4)
         xy3 = xy3/par
         xy4 = xy4/par         
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
c     -- matrix m2**-1 --
         pm(1,1) = 1.0
         pm(1,2) = 0.0
         pm(1,3) = 0.0
         pm(1,4) = 0.0
         pm(2,1) = -u/rho
         pm(2,2) = 1.0/rho
         pm(2,3) = 0.0
         pm(2,4) = 0.0
         pm(3,1) = -v/rho
         pm(3,2) = 0.0
         pm(3,3) = 1.0/rho
         pm(3,4) = 0.0
         pm(4,1) = 0.5*(u*u+v*v)*gami
         pm(4,2) = -u*gami
         pm(4,3) = -v*gami
         pm(4,4) = gami
c     -- matrix pn --
         pn(1,1) = 0.0
         pn(1,2) = 0.0
         pn(1,3) = 0.0
         pn(1,4) = 0.0
         pn(2,1) = 0.0
         pn(2,2) = xy4
         pn(2,3) =-xy3
         pn(2,4) = 0.0
         pn(3,1) = 0.0
         pn(3,2) = 0.0
         pn(3,3) = 0.0
         pn(3,4) = 1.0
         pn(4,1) = 0.0
         pn(4,2) = 0.0
         pn(4,3) = 0.0
         pn(4,4) = 0.0
c     -- multiplication --
         do n2 =1,4
            do n1 =1,4
               c2(n1,n2) = 0.0
               do nn =1,4
                  c2(n1,n2) = c2(n1,n2) + pn(n1,nn)*pm(nn,n2)*xyj(j,k)
               end do
            end do
         end do

         k = k1
c     -- reordering --
c     -- finding the biggest --      
         do n2 =1,3
            ki = n2
            b0 = dabs( cc(n2,n2) )
            do n1 =n2+1,4
               b1 = dabs( cc(n1,n2) )
               if (b1.gt.b0) then
                  ki = n1
                  b0 = b1
               end if
            end do
c     -- interchanging --
            if (ki.ne.n2) then
               do n3 =1,4
                  b1 = cc(n2,n3)
                  cc(n2,n3) = cc(ki,n3)
                  cc(ki,n3) = b1
                  b1 = c1(n2,n3)
                  c1(n2,n3) = c1(ki,n3)
                  c1(ki,n3) = b1
                  b1 = c2(n2,n3)
                  c2(n2,n3) = c2(ki,n3)
                  c2(ki,n3) = b1
               end do
               itmp = iex(j,n2,iside,iblk)
               iex(j,n2,iside,iblk) = iex(j,ki,iside,iblk)
               iex(j,ki,iside,iblk) = itmp
            endif
         end do

c     -- store diagonal scaling --
c     -- for adjoint problem - CANNOT scale here --
         do n1 =1,4
            bcf(j,n1,iside,iblk) = 1.0/cc(n1,n1)
         end do
         
c     -- store --
         if (imps) then

            if (precmat) then
               do n1 = 1,4
                  ij = ( indx(j,k)-1)*4 + n1
                  ip = ( ij - 1 )*icol(5)

                  do n2 = 1,4
                     pa(ip+n2)   = cc(n1,n2) 
                     pa(ip+4+n2) = c1(n1,n2)
                     pa(ip+8+n2) = c2(n1,n2)
                  end do
               end do
            end if

            if (jacmat) then
               do n1 = 1,4
                  ij = ( indx(j,k)-1)*4 + n1
                  ii = ( ij - 1 )*icol(9)

                  do n2 = 1,4
                     as(ii+n2)   = cc(n1,n2) 
                     as(ii+4+n2) = c1(n1,n2)
                     as(ii+8+n2) = c2(n1,n2)
                  end do
               end do
            end if

         end if
      end do

      return
      end                       !ibcib
