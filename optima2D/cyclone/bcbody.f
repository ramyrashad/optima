      subroutine bcbody_ho(jdim,kdim,press,q,xy,xit,ett,xyj,x,y,             
     >                  a,b,c,f,g,z)
c     >                  a,b,c,f,g,z,u,v)
c                                                                       
#include "../include/arcom.inc"
c
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)
c      dimension u(jdim,5),v(jdim,5)
      dimension u(maxj,1),v(maxj,1)
c                                                                       
      dimension a(jdim),b(jdim),c(jdim),d(maxj)
      dimension e(maxj),f(jdim),g(jdim),z(jdim)         
c                                                                       
c     -boundary conditions for airfoil topology
c     -C  or  O mesh type             
c                                                                       
c     -x coordinates of start and end of body                 
      j1 = jtail1                                                    
      j2 = jtail2                                                    
c                                                                    
c     -y coordinate along the body                            
      k = 1                                                          
c                                                                    
c     foso =0. for first order and =1. for second order accur
c     in the evaluation of dpdeta   default=0
c     first order second order => foso
c                                                                       
c                                                                       
c......................................................................  
c     This scaling is used if the calculation is not being restarted
c     It is used to bring the calculation up slowly, rather than   
c     impulsively.  It uses a sixth degree polynomial f            
c     for a smooth transition from scal slightly bigger than 0. at 
c     to first iteration, to scal = 1. after the strtit'th iteratio
c                                                                     
c     The scaling is turned off by restart if this is a restart run
c......................................................................  
c                                                                       
      t = float(numiter - 1)/ strtit                                    
      if( t .gt.1.d0) t = 1.d0                                          
      scal = (10.d0 -15.d0*t + 6.d0*t*t) *t**3                          
c                                                                       
c     no scaling if this is a restart                        
c                                                                       
      if( restart ) scal=1.d0 
c                                                                       
c
c     **************************************
c             inviscid bc at surface                                    
c     **************************************
c                                                                       
      if(.not.viscous) then                                           
c                                                                       
c        -obtain u and v on body via tangency and extrapolation of ucap 
c         if viscous flow, ucap = 0.0                                   
c        -inviscid velocity bc at body                           
c                                                                       
         do 30 j=j1, j2                                                 
         k = 2                                                           
c                                                                       
            xyss = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                      
            xyssr = 1.d0/xyss                                           
            xy3n2 = xy(j,k,3)*xyssr                                     
            xy4n2 = xy(j,k,4)*xyssr                                   
            rr = 1.d0/q(j,k,1)                                          
            u2 = q(j,k,2)*rr                                            
            v2 = q(j,k,3)*rr                                            
c                                                                       
         k = 3                                                          
c                                                                       
            xyss = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                      
            xyssr = 1.d0/xyss                                          
            xy3n3 = xy(j,k,3)*xyssr                                     
            xy4n3 = xy(j,k,4)*xyssr                                     
            rr = 1.d0/q(j,k,1)                                          
            u3 = q(j,k,2)*rr                                            
            v3 = q(j,k,3)*rr                                            
c                                                                       
         if (iord.eq.4) then
            k = 4                                                        
c                                                                       
               xyss = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                   
               xyssr = 1.d0/xyss                                         
               xy3n4 = xy(j,k,3)*xyssr                                   
               xy4n4 = xy(j,k,4)*xyssr                                   
               rr = 1.d0/q(j,k,1)                                       
               u4 = q(j,k,2)*rr                                         
               v4 = q(j,k,3)*rr                                         
c                                                                       
c            k = 5                                                       
cc                                                                      
c               xyss = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                  
c               xyssr = 1.d0/xyss                                       
c               xy3n5 = xy(j,k,3)*xyssr                                
c               xy4n5 = xy(j,k,4)*xyssr                                 
c               rr = 1.d0/q(j,k,1)                                       
c               u5 = q(j,k,2)*rr                                        
c               v5 = q(j,k,3)*rr
c           extrapolate tangential component of velocity               
c           -first order
c            utang = 2.d0*(xy4n2*u2 - xy3n2*v2) - (xy4n3*u3 - xy3n3*v3) 
c            if (j.eq.j1 .or. j.eq.j2) print *,'utang=',j,utang
c            print *,'utang=',numiter,j,k,utang
c           -second order
            utang = 3.d0*(xy4n2*u2 - xy3n2*v2) -
     >              3.d0*(xy4n3*u3 - xy3n3*v3) +
     >                 (xy4n4*u4 - xy3n4*v4) 
c           -third order
c            utang = 4.d0*(xy4n2*u2 - xy3n2*v2) -
c     &              6.d0*(xy4n3*u3 - xy3n3*v3) +
c     &              4.d0*(xy4n4*u4 - xy3n4*v4) -
c     &                   (xy4n5*u5 - xy3n5*v5)
         else
c           extrapolate tangential component of velocity                
            utang = 2.d0*(xy4n2*u2 - xy3n2*v2) - (xy4n3*u3 - xy3n3*v3)  
c            if (j.eq.j1 .or. j.eq.j2) print *,'utang=',j,utang
c            print *,'utang=',numiter,j,k,utang
         endif
c
c        -set normal component to zer0                                  
         vnorm = 0.d0                                                  
c                                                                       
c        -note !!! 12/13/84 thp  need to add unsteady terms           
c                                                                       
         k = 1                                                         
           xyss = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                      
           xyssr = 1.d0/xyss                                             
           xy3n1 = xy(j,k,3)*xyssr                                     
           xy4n1 = xy(j,k,4)*xyssr                                     
           rr=1.d0/q(j,k,1)
c                                                                      
           u1 =  xy4n1*utang + xy3n1*vnorm                             
           v1 = -xy3n1*utang + xy4n1*vnorm                             
           u1 = ( 1.d0 - scal)*q(j,k,2)*rr + scal*u1               
           v1 = ( 1.d0 - scal)*q(j,k,3)*rr + scal*v1               
c           if (j.eq.j1 .or. j.eq.j2) print *,'u at j=',j,u1,v1
c                                                                       
c          q2 and q3 now satisfy tangency... 
c          independently of whether density is lagged
c                                                                       
           q(j,k,2) = u1*q(j,k,1)                                      
           q(j,k,3) = v1*q(j,k,1)                                      
   30    continue                                                       
c                                                                       
c        average velocities coming off the trailing edge
c         if (.not.periodic) then
c         k=1
c         j=jtail1
c         u1=q(j,k,2)/q(j,k,1)
c         v1=q(j,k,3)/q(j,k,1)
c         j=jtail2
c         u2=q(j,k,2)/q(j,k,1)
c         v2=q(j,k,3)/q(j,k,1)
c         u1=0.5d0*(u1+u2)
c         v1=0.5d0*(v1+v2)
c         j=jtail1
c         q(j,k,2) = u1*q(j,k,1)                                      
c         q(j,k,3) = v1*q(j,k,1)                                      
c         j=jtail2
c         q(j,k,2) = u1*q(j,k,1)                                      
c         q(j,k,3) = v1*q(j,k,1)                                      
c         endif
         if( periodic )then                                             
c           at trailing edge do something different   v /= 0
            j = 1                                                     
            k = 1                                                     
            if(sharp) then                                              
c              stagnation at trailing edge                              
c              note this logic is usually not necessary except when     
c              grids are highly clustered at t. e. and a negative       
c              jacobian occurs (see xymets)                             
                  u1 = 0.d0                                              
                  v1 = 0.d0                                              
c                                                                       
c              q2 and q3 now satify tangency... independently of wheth
c              density is lagged                                      
c                                                                       
               u1 = ( 1.d0 - scal)*q(j,k,2)/q(j,k,1) + scal*u1          
               v1 = ( 1.d0 - scal)*q(j,k,3)/q(j,k,1) + scal*v1          
               q(j,k,2) = u1*q(j,k,1)                                
               q(j,k,3) = v1*q(j,k,1)                                
c                                                                       
            elseif(cusp)then                                            
c              smooth velocities off trailing edge                      
               u1l = q(2,k,2)/q(2,k,1)                                  
               v1l = q(2,k,3)/q(2,k,1)                                  
               u1u = q(jup,k,2)/q(jup,k,1)                              
               v1u = q(jup,k,3)/q(jup,k,1)                              
               u1 = 0.5d0*(u1l+u1u)                                    
               v1 = 0.5d0*(v1l+v1u)                                
c                                                                       
c              q2 and q3 now satify tangency... independently of wheth
c              density is lagged                                      
c                                                                       
               u1 = ( 1.d0 - scal)*q(j,k,2)/q(j,k,1) + scal*u1    
               v1 = ( 1.d0 - scal)*q(j,k,3)/q(j,k,1) + scal*v1         
               q(j,k,2) = u1*q(j,k,1)                                 
               q(j,k,3) = v1*q(j,k,1)                                
            endif                                                      
         endif                                                          
c                                                                       
c                                                                       
c---------------------------------------------------------------------  
c       satisfy momentum relation for normal pressure derivation        
c---------------------------------------------------------------------  
c     
csd      goto 937
csdc
csd      ja = j1                                                       
csd      jb = j2                                                      
csdc     for periodic meshes around sharp or cusp trailing edges         
csdc     we don't want to integrate across t. e.                         
csd      if(periodic)then                                                
csd         if(sharp .or. cusp) ja = j1+1                                
csd      endif                                                           
csdc
csd      if (iord.eq.4) then
csdc         do 31 k=1,kbc
csd         k=1
csd         do 31 j=ja-2,jb+2
csd            u(j,k)=q(j,k,2)/q(j,k,1)
csd            v(j,k)=q(j,k,3)/q(j,k,1)
csd 31      continue
csd         k = 1
csd         tp=1.d0/12.d0
csd         tp2=2.d0*tp
csd         do 32 j=ja,jb
csd            include 'matfil.inc'
csd 32      continue
csdc         j=ja
csdc         include 'matfil2.inc'
csdcc
csdc         j=ja+1
csdc         include 'matfil2.inc'
csdc
csdc         j=jb-1
csdc         include 'matfil2.inc'
csdcc
csdc         j=jb
csdc         include 'matfil2.inc'
csd      else
csd         k = 1                                                       
csd         do 40 j=ja,jb                                               
csd            jp1 = jplus(j)                                           
csd            jm1 = jminus(j)                                          
csd            a5 = 0.5d0                                              
csd            rho = q(j,k,1)*xyj(j,k)                                 
csd            uu=(xy(j,k,1)*q(j,k,2)+xy(j,k,2)*q(j,k,3))/q(j,k,1)+xit(j,k)
csdc           if you are at the trailing edge ... use first-order
csdc           one-sided differencing.
csd            if(j.eq.j1)then                                         
csd               jm1 = j                                              
csd               a5 = 1.d0                                             
csd            elseif(j.eq.j2)then                                      
csd               jp1 = j                                               
csd               a5 = 1.d0                                              
csd            endif                                                    
csd            uxi = a5*( q(jp1,k,2)/q(jp1,k,1) - q(jm1,k,2)/q(jm1,k,1))
csd            vxi = a5*( q(jp1,k,3)/q(jp1,k,1) - q(jm1,k,3)/q(jm1,k,1))
csdc
csdc
csd            ff = rho*uu*( xy(j,k,3)*uxi+xy(j,k,4)*vxi)              
csd            c1 = xy(j,k,3)*xy(j,k,1) + xy(j,k,2)*xy(j,k,4)          
csd            c2 = xy(j,k,3)**2 + xy(j,k,4)**2                       
csd            p2 = gami*(q(j,2,4)-.5d0*(q(j,2,2)**2+q(j,2,3)**2)/q(j,2,1)) 
csd            p3 = gami*(q(j,3,4)-.5d0*(q(j,3,2)**2+q(j,3,3)**2)/q(j,3,1)) 
csd            f(j)=-ff+c2*
csd     &              (-(1.d0+foso)*p2*xyj(j,2)+.5d0*foso*p3*xyj(j,3)) 
csd            a(j)=-.5d0*c1                                      
csd            c(j)=.5d0*c1                                        
csd            b(j)=-(1.d0+.5d0*foso)*c2                        
csd   40    continue
csd      endif
csdc                                                          
csdc                   periodic logic                                   
csdc                                                                     
csd      if(.not.periodic .or. sharp .or. cusp)then                      
csd         if (iord.eq.4) then
csd          tp=1.d0/12.d0
csd          jj=ja
csd          pm1 = gami*xyj(jj-1,k)*
csd     1    (q(jj-1,k,4)-.5d0*(q(jj-1,k,2)**2+q(jj-1,k,3)**2)/q(jj-1,k,1))  
csd          pm2 = gami*xyj(jj-2,k)*
csd     1    (q(jj-2,k,4)-.5d0*(q(jj-2,k,2)**2+q(jj-2,k,3)**2)/q(jj-2,k,1))  
csd          f(jj)=f(jj)-(b(jj)*pm1+a(jj)*pm2)
csdc         
csd          jj=ja+1
csd          pm2 = gami*xyj(jj-2,k)*
csd     1    (q(jj-2,k,4)-.5d0*(q(jj-2,k,2)**2+q(jj-2,k,3)**2)/q(jj-2,k,1))  
csd          f(jj)=f(jj)-a(jj)*pm2
csdc         
csd          jj=jb-1
csd          pp2 = gami*xyj(jj+2,k)*
csd     1    (q(jj+2,k,4)-.5d0*(q(jj+2,k,2)**2+q(jj+2,k,3)**2)/q(jj+2,k,1))  
csd          f(jj)=f(jj)-e(jj)*pp2
csd          
csd          jj=jb
csd          pp1 = gami*xyj(jj+1,k)*
csd     1    (q(jj+1,k,4)-.5d0*(q(jj+1,k,2)**2+q(jj+1,k,3)**2)/q(jj+1,k,1))  
csd          pp2 = gami*xyj(jj+2,k)*
csd     1    (q(jj+2,k,4)-.5d0*(q(jj+2,k,2)**2+q(jj+2,k,3)**2)/q(jj+2,k,1))  
csd          f(jj)=f(jj)-(d(jj)*pp1+e(jj)*pp2)
csdc         
csdc          if (numiter.eq.iend) then
csdc          do 1111 j=ja,jb
csdc             write(99,999) a(j),b(j),c(j),d(j),e(j),f(j)
csdc 1111     continue
csdc 999      format(6e13.3)
csdc          endif
csdc          stop
csd          call xludcmp(a,b,c,d,e,jdim,1,ja,jb,1,1)
csd          call xlubcsb(a,b,c,d,e,f,jdim,1,ja,jb,1,1)   
csd         else
csd            jj = jminus(ja)                                          
csd            pbc = gami *                                           
csd     1         (q(jj,k,4) -.5d0*( q(jj,k,2)**2 +q(jj,k,3)**2)/q(jj,k,1))  
csd            f(ja) = f(ja) - a(ja)*pbc*xyj(jj,k)                       
csd            jj = jplus(jb)                                            
csd            pbc = gami *                                             
csd     1         (q(jj,k,4) -.5d0*( q(jj,k,2)**2 +q(jj,k,3)**2)/q(jj,k,1))  
csd            f(jb) = f(jb) - c(jb)*pbc*xyj(jj,k)                      
csd            call trib(jdim,a,b,c,z,f,ja,jb)                          
csd         endif
csd         if(sharp .or. cusp)f(j1) = 0.5d0*(f(ja)+f(jb))               
csd      elseif(periodic)then                                          
csd         call trip(jdim,a,b,c,f,z,g,ja,jb)                       
csd      endif                                                         
csdc                                                                   
csdc---- pressure interpolation ----
csd 937  continue
         if (iord.eq.2) then
            do 50 j=j1,j2
               p2 =press(j,2)*xyj(j,2)
               p3 =press(j,3)*xyj(j,3)
c              -zeroeth order extrapolation
c               f(j) =p2
c              -first order
               f(j) =2.d0*p2 - p3
 50         continue
         else
            do 60 j=j1,j2
               p2 =press(j,2)*xyj(j,2)
               p3 =press(j,3)*xyj(j,3)
               p4 =press(j,4)*xyj(j,4)
               p5 =press(j,5)*xyj(j,5)
c              -first order extrapolation
c               f(j)= 2.d0*p2 - p3
c              -second order
c               f(j)= 3.d0*(p2 - p3)+p4
c              -third order
               f(j)= 4.d0*p2 - 6.d0*p3 + 4.d0*p4 - p5
 60         continue
         endif
c         if (.not.periodic) then
cc            f(j1)=0.5d0*(f(j1)+f(j2))
cc            f(j2)=f(j1)
c            q(j1,1,2)=0.d0
c            q(j2,1,2)=0.d0
c            q(j1,1,3)=0.d0
c            q(j2,1,3)=0.d0
c         endif
c                                                                       
c                                                                       
c        hstfs is the stagnation freestream enthalpy           
         hstfs = 1.d0/gami + 0.5d0*fsmach**2                         
         k=1
         do 65 j=j1,j2                                                  
c           rescale q2 and q3 to new density such that when      
c           reforming pressure  from q1 to q4,                   
c           the normal derivative of pressure is satisfied        
c                                                                
c           enforce constant free stream stagnation enthalpy at b
c                                                                       
            rinver = 1./q(j,k,1)                                        
            u1 = q(j,k,2)*rinver                                         
            v1 = q(j,k,3)*rinver                                         
            q(j,k,1) = gamma*f(j)/(gami*(hstfs -0.5*(u1**2 + v1**2))) 
            q(j,k,1) = q(j,k,1)/xyj(j,k)                                
c
            q(j,k,2)= u1*q(j,k,1)                                       
            q(j,k,3)= v1*q(j,k,1)                                       
            f(j) = f(j)/xyj(j,k)                                        
            q(j,k,4) = f(j)/gami +                                      
     %      .5d0 * ( q(j,k,2)**2+q(j,k,3)**2) /q(j,k,1)               
   65    continue                                                       
c                                                                       
c                                                                       
c********************************************************************** 
c********************************************************************** 
c      viscous surface bc                                              
c                                                                       
       elseif(viscous)then                                              
          k = 1                                                          
c         -viscous velocity bc at body                            
          do 35 j=j1, j2                                                 
c            cap u and cap v = 0.0                                      
             u1 = (xy(j,1,4)*(- xit(j,1)) + xy(j,1,2)*ett(j,1))/xyj(j,1)
             v1 = (-xy(j,1,3)*(- xit(j,1)) -xy(j,1,1)*ett(j,1))/xyj(j,1)
             u1 = ( 1.d0 - scal)*q(j,1,2)/q(j,1,1) + scal*u1            
             v1 = ( 1.d0 - scal)*q(j,1,3)/q(j,1,1) + scal*v1           
c                                                                        
c            q2 and q3 now satify tangency... independently of wheth
c            density is lagged                                      
c                                                                        
             q(j,1,2) = u1*q(j,1,1)                                      
             q(j,1,3) = v1*q(j,1,1)                                      
c                                                                        
   35     continue                                                       
c                                                                       
c...................................................................... 
c         surface condition on pressure                                 
c...................................................................... 
          if (iord.eq.2) then
             do 46 j = j1,j2                                           
c               extrapolate p
c               foso=0 => 0th order extrap. (1st order dp/dn=0)
c               foso=1 => 1st order extrap. 
c                                                                       
                p2 = press(j,2)*xyj(j,2)
                p3 = press(j,3)*xyj(j,3)
c            
                f(j)      = (1.d0+foso)*p2 - foso*p3
c
c               -force dp/dn=0 on surface to ...
c                -second-order
c                 f(j) = (4.d0*p2 - p3)/3.d0
c
c               note f(j) doesn't have jacobian in it                   
 46          continue
          else
             do 47 j = j1,j2                                          
                p2 = press(j,2)*xyj(j,2)
                p3 = press(j,3)*xyj(j,3)
                p4 = press(j,4)*xyj(j,4)
c            
c               -extrapolate                
c                0th-order
c                f(j) = p2
c                1st-order
c                f(j) = 2.d0*p2 - p3
c                2nd-order
c                f(j) = 3.d0*(p2 - p3)+p4
c
c               -force dp/dn=0 on surface to ...
c                -second-order
c                 f(j) = (4.d0*p2 - p3)/3.d0
c                -third-order
                 f(j) = (18.d0*p2 - 9.d0*p3 + 2.d0*p4)/1.1d1
c
c               -note f(j) doesn't have jacobian in it.
 47          continue
          endif
c                                                                       
c        sharp trailing edge                                            
c                                                                      
         if(periodic)then                                              
           if (sharp .or. cusp) then
             j = j1
             jpl = jplus(j)
             jmi = jminus(j)
             f(j) = 0.5d0*(f(jpl) + f(jmi))
           endif
         endif                                                         
c                                                                       
         do 48 j = j1, j2                                               
c           First order of density extrapolation seems to
c           be unstable for viscous calculations (if iord=2).              
c           In such cases use zero'th order                             
c           or constant wall temperature.                       
c                                                                       
            rinver = 1.d0/q(j,k,1)                                     
            rj = 1.d0/xyj(j,k)                                         
c                                                                       
            if(wtrat.ne.0.d0)then                                    
c              -fix wall temp   tim  barth 10/10/84                
c              -twall is the ratio of twall/tinf                        
c                                                                       
               prs = f(j)*rj                                           
               twall = wtrat/gamma                                     
               q(j,k,1) = prs/twall                                    
            else                                                    
               if (iord.eq.2) then
                  r2 = q(j,2,1)*xyj(j,2)
c                  r3 = q(j,3,1)*xyj(j,3)
c
c                 foso=0 => 0th order extrap. (1st order dp/dn=0)
c                 foso=1 => 1st order extrap. 
c                  q(j,k,1) = ((1.d0+foso)*r2 - foso*r3)*rj
                  q(j,k,1) = r2*rj
c
c                 -set d/dn (rho) = 0 (second-order)                 
c                  q(j,k,1) = (4.d0*r2 - r3)*rj/3.d0
               else
                  r2 = q(j,2,1)*xyj(j,2)
                  r3 = q(j,3,1)*xyj(j,3)
                  r4 = q(j,4,1)*xyj(j,4)
c
c                 -set d/dn (rho) = 0 (second-order)                 
c                  q(j,k,1) = (4.d0*r2 - r3)*rj/3.d0
c
c                 -set d/dn (rho) = 0 (third-order)                 
                  q(j,k,1) = (18.d0*r2 - 9.d0*r3 + 2.d0*r4)*rj/11.d0
c
c                 -extrapolate rho (second-order)
c                  q(j,k,1) = (3.d0*(r2 - r3)+r4)*rj
c
c                 -extrapolate rho (first-order)
c                  q(j,k,1)=(2.d0*r2-r3)*rj
               endif
            endif                                                   
            u1 = q(j,k,2)*rinver                                         
            v1 = q(j,k,3)*rinver                                         
            q(j,k,2) = u1*q(j,k,1)                                       
            q(j,k,3) = v1*q(j,k,1)                                       
            q(j,k,4) = f(j)/gami*rj +                                   
     %         .5d0 * ( q(j,k,2)**2+q(j,k,3)**2) /q(j,k,1)            
   48    continue                                                       
c                                                                       
c********************************************************************** 
c********************************************************************** 
c    end surface conditions                                             
c                                                                       
      endif                                                           
c                                                                       
      return                                                           
      end                                                              
