c----------------------------------------------------------------------
c     -- fill jacobian matrix and preconditioner: viscous terms --
c     written by: marian nemec
c     modified by markus rumpfkeil
c     date: jan. 2001
c----------------------------------------------------------------------

      subroutine fillasv (jdim,kdim,ndim,indx,icol,as,pa,dj,dk,dpj,dpk,
     &     crj,crk,crpj,crmj,crpk,crmk)

      implicit none
      
#include "../include/arcom.inc"

      integer jdim, kdim, ndim, indx(jdim,kdim), icol(9)
clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
clb
      double precision dj(ndim,3,jdim,kdim),dk(ndim,3,jdim,kdim)
      double precision dpj(ndim,3,jdim,kdim), dpk(ndim,3,jdim,kdim)
      double precision crj(ndim,3,jdim,kdim),crk(ndim,3,jdim,kdim)
      double precision crpj(ndim,3,jdim,kdim),crpk(ndim,3,jdim,kdim)
      double precision crmj(ndim,3,jdim,kdim),crmk(ndim,3,jdim,kdim)

      integer j,k,n,m,jsta,jsto,itmp,ii,ij

c     -- preconditioner --
      if ( prec_mat ) then 
        do k = klow,kup
          do j = jlow,jup
            itmp = ( indx(j,k) - 1 )*ndim
            do n  = 2,4 
              ij  = itmp + n
              ii  = ( ij - 1 )*icol(5)
              do m = 1,ndim

                pa(ii+icol(1)+m) = pa(ii+icol(1)+m) - dk(m,n-1,j,k-1)
                pa(ii+icol(2)+m) = pa(ii+icol(2)+m) + dk(m,n-1,j,k)
     &                - dpk(m,n-1,j,k-1)
                pa(ii+icol(3)+m) = pa(ii+icol(3)+m) + dpk(m,n-1,j,k)

                if (visxi) then                
                   pa(ii+       +m)= pa(ii+m) - dj(m,n-1,jminus(j),k)
                   pa(ii+icol(2)+m) = pa(ii+icol(2)+m) + dj(m,n-1,j,k)
     &                  - dpj(m,n-1,jminus(j),k)
                   pa(ii+icol(4)+m) = pa(ii+icol(4)+m) + dpj(m,n-1,j,k)
                end if

c$$$                 if (viscross) then
c$$$                    pa(ii+m)    = pa(ii+m)    
c$$$     &                   - 0.5d0*crmk(m,n-1,j,k)
c$$$     &                   + 0.5d0*crpk(m,n-1,j,k-1)
c$$$                    pa(ii+icol(1)+m) = pa(ii+icol(1)+m) 
c$$$     &                   - 0.5d0*crmj(m,n-1,j,k)
c$$$     &                   + 0.5d0*crpj(m,n-1,jminus(j),k)
c$$$                    pa(ii+icol(2)+m) = pa(ii+icol(2)+m)  
c$$$     &                   + 0.5d0*crk(m,n-1,jplus(j),k)  
c$$$     &                   - 0.5d0*crk(m,n-1,jminus(j),k)
c$$$     &                   + 0.5d0*crj(m,n-1,j,k+1) 
c$$$     &                   - 0.5d0*crj(m,n-1,j,k-1)
c$$$                    pa(ii+icol(3)+m) = pa(ii+icol(3)+m) 
c$$$     &                   + 0.5d0*crpj(m,n-1,j,k)
c$$$     &                   - 0.5d0*crmj(m,n-1,j,k+1)
c$$$                    pa(ii+icol(4)+m) = pa(ii+icol(4)+m) 
c$$$     &                   + 0.5d0*crpk(m,n-1,j,k)
c$$$     &                   - 0.5d0*crmk(m,n-1,jplus(j),k)                                                
c$$$                end if

              end do
            end do
          end do
        end do
      end if

c     -- second order jacobian --
      if ( jac_mat ) then

      if(.not.periodic) then
         jsta=jlow+1
         jsto=jup-1
      else
         jsta=jlow
         jsto=jup
      end if

c     -- interior points --
        do k = klow+1,kup-1
          do j = jsta,jsto
            itmp = ( indx(j,k) - 1 )*ndim
            do n  = 2,4 
              ij  = itmp + n
              ii  = ( ij - 1 )*icol(9)
              if (clopt) ii = ( ij - 1 )*(icol(9)+1)
              do m =1,ndim

                as(ii+icol(3)+m) = as(ii+icol(3)+m) - dk(m,n-1,j,k-1)
                as(ii+icol(4)+m) = as(ii+icol(4)+m) + dk(m,n-1,j,k)
     &                - dpk(m,n-1,j,k-1)
                as(ii+icol(5)+m) = as(ii+icol(5)+m) + dpk(m,n-1,j,k)

                if (visxi) then
                   as(ii+icol(1)+m) = as(ii+icol(1)+m) - 
     &                  dj(m,n-1,jminus(j),k)
                   as(ii+icol(4)+m) = as(ii+icol(4)+m) + dj(m,n-1,j,k)
     &                - dpj(m,n-1,jminus(j),k)
                   as(ii+icol(7)+m) = as(ii+icol(7)+m) + dpj(m,n-1,j,k)
                end if

              end do
            end do
          end do
        end do

c     -- boundary nodes --

        if(.not.periodic) then

        j = jlow
        do k = klow+1,kup-1
          itmp = ( indx(j,k) - 1 )*ndim
          do n  = 2,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m =1,ndim
              as(ii+icol(2)+m) = as(ii+icol(2)+m) - dk(m,n-1,j,k-1)
              as(ii+icol(3)+m) = as(ii+icol(3)+m) + dk(m,n-1,j,k)
     &              - dpk(m,n-1,j,k-1)
              as(ii+icol(4)+m) = as(ii+icol(4)+m) + dpk(m,n-1,j,k)

              if (visxi) then
                 as(ii+icol(1)+m) = as(ii+icol(1)+m) - 
     &                dj(m,n-1,jminus(j),k)
                 as(ii+icol(3)+m) = as(ii+icol(3)+m) + dj(m,n-1,j,k)
     &              - dpj(m,n-1,jminus(j),k)
                 as(ii+icol(6)+m) = as(ii+icol(6)+m) + dpj(m,n-1,j,k)
              end if

            end do
          end do
        end do

        j = jup
        do k = klow+1,kup-1
          itmp = ( indx(j,k) - 1 )*ndim
          do n  = 2,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m =1,ndim
              as(ii+icol(3)+m) = as(ii+icol(3)+m) - dk(m,n-1,j,k-1)
              as(ii+icol(4)+m) = as(ii+icol(4)+m) + dk(m,n-1,j,k)
     &              - dpk(m,n-1,j,k-1)
              as(ii+icol(5)+m) = as(ii+icol(5)+m) + dpk(m,n-1,j,k)

              if (visxi) then
                 as(ii+icol(1)+m) = as(ii+icol(1)+m) - 
     &                dj(m,n-1,jminus(j),k)
                 as(ii+icol(4)+m) = as(ii+icol(4)+m) + dj(m,n-1,j,k)
     &              - dpj(m,n-1,jminus(j),k)
                 as(ii+icol(7)+m) = as(ii+icol(7)+m) + dpj(m,n-1,j,k)
              end if

            end do
          end do
        end do

        end if  !not periodic

        k = klow
        do j = jsta,jsto
          itmp = ( indx(j,k) - 1 )*ndim
          do n  = 2,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m =1,ndim
              as(ii+icol(2)+m) = as(ii+icol(2)+m) - dk(m,n-1,j,k-1)
              as(ii+icol(3)+m) = as(ii+icol(3)+m) + dk(m,n-1,j,k)
     &              - dpk(m,n-1,j,k-1)
              as(ii+icol(4)+m) = as(ii+icol(4)+m) + dpk(m,n-1,j,k)

              if (visxi) then
                 as(ii+icol(1)+m) = as(ii+icol(1)+m) - 
     &                dj(m,n-1,jminus(j),k)
                 as(ii+icol(3)+m) = as(ii+icol(3)+m) + dj(m,n-1,j,k)
     &              - dpj(m,n-1,jminus(j),k)
                 as(ii+icol(6)+m) = as(ii+icol(6)+m) + dpj(m,n-1,j,k)
              end if

            end do
          end do
        end do

        k = kup
        do j = jsta,jsto
          itmp = ( indx(j,k) - 1 )*ndim
          do n  = 2,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m =1,ndim
              as(ii+icol(3)+m) = as(ii+icol(3)+m) - dk(m,n-1,j,k-1)
              as(ii+icol(4)+m) = as(ii+icol(4)+m) + dk(m,n-1,j,k)
     &              - dpk(m,n-1,j,k-1)
              as(ii+icol(5)+m) = as(ii+icol(5)+m) + dpk(m,n-1,j,k)

              if (visxi) then
                 as(ii+icol(1)+m) = as(ii+icol(1)+m) - 
     &                dj(m,n-1,jminus(j),k)
                 as(ii+icol(4)+m) = as(ii+icol(4)+m) + dj(m,n-1,j,k)
     &              - dpj(m,n-1,jminus(j),k)
                 as(ii+icol(6)+m) = as(ii+icol(6)+m) + dpj(m,n-1,j,k) 
              end if

            end do
          end do
        end do

        if(.not.periodic) then

        j = jlow
        k = klow
        itmp = ( indx(j,k) - 1 )*ndim
        do n  = 2,4 
          ij  = itmp + n
          ii  = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m =1,ndim
            as(ii+icol(1)+m) = as(ii+icol(1)+m) - dk(m,n-1,j,k-1)
            as(ii+icol(2)+m) = as(ii+icol(2)+m) + dk(m,n-1,j,k)
     &            - dpk(m,n-1,j,k-1)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + dpk(m,n-1,j,k)

            if (visxi) then
               as(ii        +m) = as(ii+m) - dj(m,n-1,jminus(j),k)
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + dj(m,n-1,j,k)
     &            - dpj(m,n-1,jminus(j),k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + dpj(m,n-1,j,k)
            end if

          end do
        end do

        j = jlow
        k = kup
        itmp = ( indx(j,k) - 1 )*ndim
        do n  = 2,4 
          ij  = itmp + n
          ii  = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m =1,ndim
            as(ii+icol(2)+m) = as(ii+icol(2)+m) - dk(m,n-1,j,k-1)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + dk(m,n-1,j,k)
     &            - dpk(m,n-1,j,k-1)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + dpk(m,n-1,j,k)

            if (visxi) then
               as(ii        +m) = as(ii+m) - dj(m,n-1,jminus(j),k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + dj(m,n-1,j,k)
     &            - dpj(m,n-1,jminus(j),k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + dpj(m,n-1,j,k)
            end if

          end do
        end do

        j = jup
        k = klow
        itmp = ( indx(j,k) - 1 )*ndim
        do n  = 2,4 
          ij  = itmp + n
          ii  = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m =1,ndim
            as(ii+icol(2)+m) = as(ii+icol(2)+m) - dk(m,n-1,j,k-1)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + dk(m,n-1,j,k)
     &            - dpk(m,n-1,j,k-1)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + dpk(m,n-1,j,k)

            if (visxi) then
               as(ii+icol(1)+m) = as(ii+icol(1)+m) - 
     &              dj(m,n-1,jminus(j),k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + dj(m,n-1,j,k)
     &            - dpj(m,n-1,jminus(j),k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + dpj(m,n-1,j,k)
            end if

          end do
        end do

        j = jup
        k = kup
        itmp = ( indx(j,k) - 1 )*ndim
        do n  = 2,4 
          ij  = itmp + n
          ii  = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m =1,ndim
            as(ii+icol(3)+m) = as(ii+icol(3)+m) - dk(m,n-1,j,k-1)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + dk(m,n-1,j,k)
     &            - dpk(m,n-1,j,k-1)
            as(ii+icol(5)+m) = as(ii+icol(5)+m) + dpk(m,n-1,j,k)

            if (visxi) then
               as(ii+icol(1)+m) = as(ii+icol(1)+m) - 
     &              dj(m,n-1,jminus(j),k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + dj(m,n-1,j,k)
     &            - dpj(m,n-1,jminus(j),k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + dpj(m,n-1,j,k)
            end if

          end do
        end do 
     
      end if  !not periodic

      end if  !jac_mat 
      
      return                                                            
      end                       !fillasv
