c     ------------------------------------------------------------------
c     -- double bandwidth node order (across the wake-cuts) --
c     -- m. nemec, sept. 2001 --
c     ------------------------------------------------------------------
      subroutine blkord2 (js, je, ji, ks, ke, ksn, ken, jmax, kmax,
     &     indx, jmaxn, kmaxn, indxn, lastblk)

      implicit none
      integer js,je,ji,ks,ke,ksn,ken,jmax,kmax,jmaxn,kmaxn,lastblk
      integer kc,j,k
      integer indx(jmax,kmax), indxn(jmaxn,kmaxn)

      kc = 0

      do j = js,je,ji
         
         do k = ke,ks,-1
            kc = kc+1
            indx(j,k) = kc + lastblk
c            write (*,*) j,k,indx(j,k) 
         end do

         lastblk = indx(j,ks)
         kc = 0
c         write (*,*)
c lastblk

         do k = ken,ksn,-1
            kc = kc+1
            indxn(j,k) = kc + lastblk
c            write (*,*) j,k,indxn(j,k)
         end do
         
         lastblk = indxn(j,ksn)
         kc = 0
c         write (*,*) 
      end do

      return
      end                       ! blkord2
