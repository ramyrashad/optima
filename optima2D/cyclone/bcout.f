      subroutine bcout(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,k1,k2,
     &                  jbc,idir)              
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c
      dimension vinff(4),vint(4),vinfp(4),vintp(4),qbar(4),qdif(4)
      dimension vec(4),xx(4,4),tmp(4)
c     -far field bc for o mesh outer boundary and c mesh k = kmax          
c                                                                       
c   .....................................................................  
c     Far field circulation based on potential vortex added to reduce      
c     the dependency on outer boundary location.  works well.  for         
c     instance, a naca0012 at m=0.63 a=2. shows no dependency on ob from   
c     4 - 96 chords.                                                       
c   .....................................................................  
c                                                                       
      gi = 1.d0/gamma                                                 
      gm1i = 1.d0/gami                                                
c                                                                    
      alphar = alpha*pi/180.d0
      cosang = cos( alphar )                                         
      sinang = sin( alphar )                                         
      ainf = sqrt(gamma*pinf/rhoinf)                                 
      hstfs = 1.d0/gami + 0.5d0*fsmach**2                                
c                                                                       
c                                                                       
      j = jbc
      jq= jbc
c     The normal points in the direction of increasing xi location ...
c     So Vn is positive for outflow at j=jmax and negative at j=1.
      sgn= -sign(1.d0,float(idir))
      jadd = sign(1,idir)
c
      if(.not.periodic .and. .not.viscous)then                          
         if (prec.eq.0) then
            do 60 k = k1,k2                                                
c                                                                   
c              -reset free stream values with circulation correction       
c              -this if statement is here so that when I test for uniform
c               flow convergence I don't get a floating point exception when
c               using this routine for the body too.
c              -circul should be set to false when testing uniform flow.
               if (circul) then
                  xa = x(j,k) - chord/4.d0
                  ya = y(j,k)                                            
                  radius = sqrt(xa**2+ya**2)                               
                  angl = atan2(ya,xa)                                      
                  cjam = cos(angl)                                         
                  sjam = sin(angl)                                        
                  qcirc=circb
     &                     /(radius*(1.d0-(fsmach*sin(angl-alphar))**2))  
               else
                  qcirc=0.d0
                  sjam=0.d0
                  cjam=0.d0
               endif
               uf = uinf + qcirc*sjam                                    
               vf = vinf - qcirc*cjam                                     
               af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))         
               af = sqrt(af2)        
c                                                                       
c              -metrics for normals
               rx=xy(j,k,1)
               ry=xy(j,k,2)
               rxy2=1.d0/sqrt(rx**2+ry**2)
c               print *,'rxy2=',j,rxy2
               rx=rx*rxy2*sgn
               ry=ry*rxy2*sgn
c        
c              -get extrapolated variables                                
               if (iord.eq.4) then
                  jq=j+jadd
                  xyj2 = xyj(jq,k)                                         
                  rho2 = q(jq,k,1)*xyj2                                   
                  rinv = 1.d0/q(jq,k,1)                                    
                  u2 = q(jq,k,2)*rinv                                     
                  v2 = q(jq,k,3)*rinv
                  e2 = q(jq,k,4)*xyj2
                  p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c        
                  jq=j+2*jadd
                  xyj3 = xyj(jq,k)                                       
                  rho3 = q(jq,k,1)*xyj3
                  rinv = 1.d0/q(jq,k,1)                                   
                  u3 = q(jq,k,2)*rinv                                     
                  v3 = q(jq,k,3)*rinv                                     
                  e3 = q(jq,k,4)*xyj3
                  p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c                 
                  jq=j+3*jadd
                  xyj4 = xyj(jq,k)                                       
                  rho4 = q(jq,k,1)*xyj4
                  rinv = 1.d0/q(jq,k,1)                                   
                  u4 = q(jq,k,2)*rinv                                     
                  v4 = q(jq,k,3)*rinv                                     
                  e4 = q(jq,k,4)*xyj4
                  p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c                 
c                  jq=j+4*jadd
c                  xyj5 = xyj(jq,k)                                       
c                  rho5 = q(jq,k,1)*xyj5
c                  rinv = 1.d0/q(jq,k,1                                   
c                  u5 = q(jq,k,2)*rinv                                   
c                  v5 = q(jq,k,3)*rinv                                     
c                  e5 = q(jq,k,4)*xyj5
c                  p5 = gami*(e5 - 0.5d0*rho5*(u5**2+v5**2))
c                 
c                 -3rd order extrapolation
c                  rhoext= 4.*(rho2+rho4) - 6.*rho3 -rho5
c                  uext= 4.*(u2+u4) -6.*u3 -u5
c                  vext= 4.*(v2+v4) -6.*v3 -v5
c                  pext= 4.*(p2+p4) -6.*p3 -p5
c                 -2nd order extrapolation
                  rhoext= 3.*(rho2 - rho3) + rho4
                  uext= 3.*(u2 - u3) + u4
                  vext= 3.*(v2 - v3) + v4
                  pext= 3.*(p2 - p3) + p4
c                 -1st order extrapolation
c                  rhoext= 2.*rho2 - rho3
c                  uext= 2.*u2 - u3
c                  vext= 2.*v2 - v3
c                  pext= 2.*p2 - p3
c                 -Zeroth order
c                  rhoext= rho2
c                  uext= u2
c                  vext= v2
c                  pext= p2               
               else
                  jq=j+jadd
                  xyj2 = xyj(jq,k)                                         
                  rho2 = q(jq,k,1)*xyj2                                   
                  rinv = 1.d0/q(jq,k,1)                               
                  u2 = q(jq,k,2)*rinv                                      
                  v2 = q(jq,k,3)*rinv
                  e2 = q(jq,k,4)*xyj2
                  p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c        
c                  jq=j+2*jadd
c                  xyj3 = xyj(jq,k)                                       
c                  rho3 = q(jq,k,1)*xyj3
c                  rinv = 1.d0/q(jq,k,1)                                   
c                  u3 = q(jq,k,2)*rinv                         
c                  v3 = q(jq,k,3)*rinv                                    
c                  e3 = q(jq,k,4)*xyj3
c                  p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c
c                 -1st order extrapolation
c                  rhoext= 2.*rho2 - rho3
c                  uext= 2.*u2 - u3
c                  vext= 2.*v2 - v3
c                  pext= 2.*p2 - p3
c                 -Zeroth order
                  rhoext= rho2
                  uext= u2
                  vext= v2
                  pext= p2               
               endif
               entext=pext/rhoext**gamma
               hext= gamma*pext/rhoext/gami + 0.5d0*(uext**2+vext**2)
               a2ext= gamma*pext/rhoext
c                                                        
c              -calc conserved and primitive variables for both
c               infinity and interior values
c        
c              -conserved variables at inf
c              -note that entinf=1/gamma so  
c                          rhoinf=(ainf**2/(gamma*entinf))**gm1i
               vinff(1)= af2**gm1i
               vinff(2)= vinff(1)*uf
               vinff(3)= vinff(1)*vf
               pf     = vinff(1)*af2*gi
               vinff(4)=pf*gm1i+0.5d0*(uf**2+vf**2)*vinff(1)
c        
c              -primitive variables at inf
c               vinfp(1)= vinff(1)
c               vinfp(2)= uf
c               vinfp(3)= vf
c               vinfp(4)= pf
c        
c              -conserved variables extrapolated from interior
c              -formula used for vint(1) here is equivalent to rhoext
c                  vint(1)= (a2ext/(gamma*entext))**gm1i
c                  vint(1)= a2ext**gm1i
               vint(1)= rhoext
               vint(2)= vint(1)*uext
               vint(3)= vint(1)*vext
               vint(4)= pext*gm1i +0.5d0*(uext**2+vext**2)*vint(1)
c        
c              -primitve variables extrapolated from interior
c               vintp(1)= vint(1)
c               vintp(2)= uext
c               vintp(3)= vext
c               vintp(4)= pext
c        
c        
c              -form simple average (conservative variables)
               qbar(1)= 0.5d0*(vinff(1) + vint(1))
               qbar(2)= 0.5d0*(vinff(2) + vint(2))
               qbar(3)= 0.5d0*(vinff(3) + vint(3))
               qbar(4)= 0.5d0*(vinff(4) + vint(4))
c        
c              -form difference (conservative variables)
               qdif(1)= vinff(1) - vint(1)
               qdif(2)= vinff(2) - vint(2)
               qdif(3)= vinff(3) - vint(3)
               qdif(4)= vinff(4) - vint(4)
c        
               rho=qbar(1)
               rhoinv=1.d0/rho
               u=qbar(2)*rhoinv
               v=qbar(3)*rhoinv
               e=qbar(4)
               p=gami*(e-0.5d0*rho*(u**2+v**2))
               a2bar=gamma*p*rhoinv
               ca=dsqrt(a2bar)
               snr=1.d0/ca
c        
c              *******************************************************
c              -multiply by Tkinv
               bt=1.d0/sqrt(2.d0)
               ucap=xy(j,k,1)*u + xy(j,k,2)*v
c               ucap=rx*u + ry*v
               vtot2=0.5d0*(u**2+v**2)
               beta=bt*rhoinv*snr
               ryu=ry*u
               rxu=rx*u
               ryv=ry*v
               rxv=rx*v
               
               t1= gami*(u*qdif(2) + v*qdif(3) - qdif(4)-vtot2*qdif(1))
               
               vec(1) =qdif(1) + t1*snr**2
               vec(2) =((-ryu+rxv)*qdif(1)+ry*qdif(2)-rx*qdif(3))*rhoinv
               
               t1= t1*beta
               t2= ((rxu+ryv)*qdif(1) - rx*qdif(2)-ry*qdif(3))*bt*rhoinv
               
               vec(3) = -(t1+t2)
               vec(4) = -(t1-t2)
c              *******************************************************
c              
c              -multipy by sign of eigenvalue matrix
               t1=sign(1.d0,ucap)
               vec(1) = t1*vec(1)
               vec(2) = t1*vec(2)
               vec(3) = sign(1.d0,ucap+ca)*vec(3)
               vec(4) = sign(1.d0,ucap-ca)*vec(4)
c              
c              *******************************************************
c              -multiply by Tk
               alp=rho*bt*snr
               t1= alp*(vec(3)+vec(4))
               t2= rho*bt*(vec(3)-vec(4))
               t3= rho*vec(2)
               t4= alp*t1
               tmp(1)= vec(1) + t1
               tmp(2)= vec(1)*u + ry*t3 + u*t4 + rx*t2
               tmp(3)= vec(1)*v - rx*t3 + v*t4 + ry*t2
               tmp(4)= vtot2*vec(1) + (ryu - rxv)*t3 +
     &                  (vtot2+ca**2*gm1i)*t1 + (rxu + ryv)*t2
c               tmp(2)= vec(1)*u + rho*ry*vec(2) + alp*u*t1 + rx*t2
c               tmp(3)= vec(1)*v - rho*rx*vec(2) + alp*v*t1 + ry*t2
c               tmp(4)= vtot2*vec(1) + rho*(ry*u - rx*v)*vec(2) +
c     &                  (vtot2+ca**2*gm1i)*t1 + (rx*u + ry*v)*t2
c              
c              *******************************************************
c              
               tmp(1)=qbar(1)-0.5d0*tmp(1)
               tmp(2)=qbar(2)-0.5d0*tmp(2)
               tmp(3)=qbar(3)-0.5d0*tmp(3)
               tmp(4)=qbar(4)-0.5d0*tmp(4)
c
c              -add jacobian                                             
               rjj = 1.d0/xyj(j,k)                                       
               q(j,k,1) = tmp(1)*rjj
               q(j,k,2) = tmp(2)*rjj                             
               q(j,k,3) = tmp(3)*rjj
               q(j,k,4) = tmp(4)*rjj
60          continue                                                  
         else
c        
            if (prec.ne.3) then
               print *,'BCFAR only written for prec=3'
               stop
            endif
c        
c        
cdu         -use the preconditioned characteristics to solve the bc's
c        
            do 1000 k=k1,k2
c        
c             -do circulation correction
               xa = x(j,k) - chord/4.                                   
               ya = y(j,k)                                                 
               radius = sqrt(xa**2+ya**2)                                  
               angl = atan2(ya,xa)                                         
               cjam = cos(angl)                                            
               sjam = sin(angl)                                            
               qcirc =circb/(radius*(1.d0-(fsmach*SIN(ANGL-ALPHAR))**2))  
               uf = uinf + qcirc*sjam                                      
               vf = vinf - qcirc*cjam                                     
               af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))                    
               af = sqrt(af2)                                             
c        
c             -calculate values for vinff
               vinff(1)=af2**gm1i
               vinff(2)=vinff(1)*uf
               vinff(3)=vinff(1)*vf
               pf=vinff(1)*af2*gi
               vinff(4)=pf*gm1i+0.5d0*(uf**2+vf**2)*vinff(1)
c        
c          -extrapolate for vint from interior (no Jacobian in these)
c            if ((j.lt.(j2-3)).and.(j.gt.(j1+3))) then
c               vint(1)=2.0*q(j,kq-1,1)*xyj(j,k-1)-q(j,kq-2,1)*xyj(j,k-2)
c               vint(2)=2.0*q(j,kq-1,2)*xyj(j,k-1)-q(j,kq-2,2)*xyj(j,k-2)
c               vint(3)=2.0*q(j,kq-1,3)*xyj(j,k-1)-q(j,kq-2,3)*xyj(j,k-2)
c               vint(4)=2.0*q(j,kq-1,4)*xyj(j,k-1)-q(j,kq-2,4)*xyj(j,k-2)
c            else
               vint(1)=q(j+jadd,k,1)*xyj(j+jadd,k)
               vint(2)=q(j+jadd,k,2)*xyj(j+jadd,k)
               vint(3)=q(j+jadd,k,3)*xyj(j+jadd,k)
               vint(4)=q(j+jadd,k,4)*xyj(j+jadd,k)
c            endif
c        
c             -form simple average
               qbar(1)=vinff(1)+vint(1)
               qbar(2)=vinff(2)+vint(2)
               qbar(3)=vinff(3)+vint(3)
               qbar(4)=vinff(4)+vint(4)
c        
c             -form difference
               qdif(1)=0.5d0*(vinff(1)-vint(1))
               qdif(2)=0.5d0*(vinff(2)-vint(2))
               qdif(3)=0.5d0*(vinff(3)-vint(3))
               qdif(4)=0.5d0*(vinff(4)-vint(4))
c        
c             -calculate properties at mean state
               rho=qbar(1)
               rhoinv=1.d0/rho
               u=qbar(2)*rhoinv
               v=qbar(3)*rhoinv
               uv2=u**2+v**2
               p=gami*(qbar(4)-0.5d0*(uv2)*rho)
               a2bar=gamma*p*rhoinv
               ca=dsqrt(a2bar)
               snr=1.d0/ca
               dma2=uv2/a2bar
c        
               duw=xy(j,k,1)**2+xy(j,k,2)**2
               dul=xy(j,k,3)**2+xy(j,k,4)**2
c        
               epsilon1=prphi*fsmach**2
               epsilon2=prxi*rhoinf*sqrt(max(duw,dul))*rhoinv/re/fsmach
c        
               dulim=max(epsilon1,epsilon2)
               epsilon=min(1.d0,max(dma2,dulim))
c               epsilon=1.d0
c        
c              used to be following 2 lines
c               rx=xy(j,k,3)
c               ry=xy(j,k,4)
               rx=xy(j,k,1)*sgn
               ry=xy(j,k,2)*sgn
c               ucap=(rx*u+ry*v)*sgn
               ucap=rx*u+ry*v
c        
c              -compute components of preconditioned eigenvalue
               dua=.5d0*(1.d0+epsilon)*ucap
c              note: important to use duw instead of dul in next line
               dub=sqrt((.5*(1.d0-epsilon)*ucap)**2+epsilon*duw*a2bar)
c        
               rxy=sqrt(rx**2+ry**2)
               rx=rx/rxy
               ry=ry/rxy
c        
c             ****************************************************
c             -multiply by Minv
               du4=gami*(.5*uv2*qdif(1)-u*qdif(2)-
     +              v*qdif(3)+qdif(4))
               du1=du4*rhoinv/ca
               du4=du4-a2bar*qdif(1)
               du2=(qdif(2)-u*qdif(1))*rhoinv
               du3=(qdif(3)-v*qdif(1))*rhoinv
c        
c             ****************************************************
c             -multiply by Tkinv
               t1=du4
               t2=ry*du2-rx*du3
               z1=(rx*du2+ry*du3)
               z2=.5/dub
               z3=rxy*ca*du1
               t3=(z3+(ucap-dua+dub)*z1)*z2
               t4=(z3+(ucap-dua-dub)*z1)*z2
c        
c             ****************************************************
c             -multiply by sign(Lambda)
               du1=sign(1.d0,ucap)*t1
               du2=sign(1.d0,ucap)*t2
               du3=sign(1.d0,dua+dub)*t3
               du4=sign(1.d0,dua-dub)*t4
c        
c             ****************************************************
c             -multiply by Tk
               t1=((dua+dub-ucap)*du3+(ucap-dua+dub)*du4)/rxy/ca
               t2=ry*du2+rx*(du3-du4)
               t3=-rx*du2+ry*(du3-du4)
               t4=du1
c        
c             ****************************************************
c             -multiply by M
               du1=(rho*t1-t4/ca)/ca
               du2=u*du1+rho*t2
               du3=v*du1+rho*t3
               du4=rho*((.5*uv2/ca+ca/gami)*t1+
     +              u*t2+v*t3)-.5*uv2/a2bar*t4
c        
c             -calculate the boundry Q
               t1=qbar(1) - .5d0*du1
               t2=qbar(2) - .5d0*du2
               t3=qbar(3) - .5d0*du3
               t4=qbar(4) - .5d0*du4
c        
c             -apply jacobian scaling
               q(j,k,1)=t1/xyj(j,k)
               q(j,k,2)=t2/xyj(j,k)
               q(j,k,3)=t3/xyj(j,k)
               q(j,k,4)=t4/xyj(j,k)
 1000       continue
         endif
      elseif(.not.periodic .and. viscous)then                           
c                                                                       
c        extrapolation on viscous outflow with pressure extrapolated      
c                                                                       
c        loop from kbegin to kend
         if (iord.eq.2) then
            do 250 k = k1,k2                                        
               jq=j+jadd
               rrj = xyj(jq,k)/xyj(j,k)                                  
               q(j,k,1) = q(jq,k,1)*rrj                                
               q(j,k,2) = q(jq,k,2)*rrj                                
               q(j,k,3) = q(jq,k,3)*rrj                                
               ppp = gami*(q(jq,k,4) -                                  
     *                0.5d0*(q(jq,k,2)**2+q(jq,k,3)**2)/q(jq,k,1) )   
               q(j,k,4) = ppp*gm1i*rrj +                                 
     *                   0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)        
250         continue
         else
            do 255 k = k1,k2                                        
c
               jq=j+jadd
               xyj2 = xyj(jq,k)                                           
               rho2 = q(jq,k,1)*xyj2                                     
               rinv = 1.d0/q(jq,k,1)                                      
               u2 = q(jq,k,2)*rinv                                        
               v2 = q(jq,k,3)*rinv
               e2 = q(jq,k,4)*xyj2
               p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c              
               jq=j+2*jadd
               xyj3 = xyj(jq,k)                                           
               rho3 = q(jq,k,1)*xyj3
               rinv = 1.d0/q(jq,k,1)                                
               u3 = q(jq,k,2)*rinv                                        
               v3 = q(jq,k,3)*rinv                                       
               e3 = q(jq,k,4)*xyj3
               p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c              
               jq=j+3*jadd
               xyj4 = xyj(jq,k)                                          
               rho4 = q(jq,k,1)*xyj4
               rinv = 1.d0/q(jq,k,1)                                       
               u4 = q(jq,k,2)*rinv                                        
               v4 = q(jq,k,3)*rinv                                       
               e4 = q(jq,k,4)*xyj4
               p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c
c              -zeroeth-order extrapolation
c               or setting d(var)/dn=0 to first-order
c                rhoext=rho2
c                uext=  u2
c                vext=  v2
c                pext=  p2
c              -first-order extraplation
c               rhoext=2.d0*rho2-rho3
c               uext=  2.d0*u2-u3    
c               vext=  2.d0*v2-v3    
c               pext=  2.d0*p2-p3
c              -second-order 
c               rhoext= 3.*(rho2 - rho3) + rho4
c               uext= 3.*(u2 - u3) + u4
c               vext= 3.*(v2 - v3) + v4
c               pext= 3.*(p2 - p3) + p4
c
c              -set d(var)/dn =0 to second-order
               third=1.d0/3.d0
               rhoext=(4.d0*rho2-rho3)*third
               uext=  (4.d0*u2 - u3)*third
               vext=  (4.d0*v2 - v3)*third
               pext=  (4.d0*p2 - p3)*third
c
c              -set d(var)/dn =0 to third-order
c               denom=1.d0/1.1d1
c               rhoext=(18.d0*rho2 - 9.d0*rho3 + 2.d0*rho4)*denom
c               uext=(18.d0*u2 - 9.d0*u3 + 2.d0*u4)*denom
c               vext=(18.d0*v2 - 9.d0*v3 + 2.d0*v4)*denom
c               pext=(18.d0*p2 - 9.d0*p3 + 2.d0*p4)*denom
c                       
               rrj=1.d0/xyj(j,k)
               q(j,k,1) = rhoext*rrj                                
               q(j,k,2) = uext*q(j,k,1)
               q(j,k,3) = vext*q(j,k,1)
               ppp = pext*rrj
               q(j,k,4) = ppp*gm1i +                                 
     *                   0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)        
 255        continue
         endif                                                      
      endif
c
      return                                                            
      end                                                               
