      subroutine nk_saveDhat(jdim, kdim, ndim,s, D_hat)
c                                                                       
#include "../include/arcom.inc"
c
      dimension s(jdim,kdim,ndim)
      dimension D_hat(jdim,kdim,ndim,6)
c
      do 500 n=1,ndim
      do 500 k=kbegin,kend
      do 500 j=jbegin,jend
            D_hat(j,k,n,jstage)=s(j,k,n)
 500  continue
c
      return
      end                                                       
                                                       
