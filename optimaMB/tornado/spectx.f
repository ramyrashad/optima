c***********************************************************************
c**************  compute eigenvalues ***********************************
c***********************************************************************
c calling routine:  xiexpl
      subroutine spectx(jdim,kdim,uu,ccx,xyj,xy,spect,precon,
     &     jbegin,jend,kbegin,kend)

      implicit none

#include "../include/common.inc"

      integer jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision rj,sigx,precon(jdim,kdim,8)
      double precision xyj(jdim,kdim),xy(jdim,kdim,4)
      double precision spect(jdim,kdim),uu(jdim,kdim),ccx(jdim,kdim)

c   compute dissipation scaling

      if(ispec.eq.0)then

c   constant

      do 50 k = kbegin,kend
      do 50 j = jbegin,jend
      rj = 1.d0/xyj(j,k)
      spect(j,k) = rj
50    continue

           ELSEIF (ispec.EQ.1) THEN

c  spectral radius

cmt ~~~ it is likely that we stick to this one, that means ispec=1 and no precon
              IF (prec.EQ.0) THEN
                 DO k=kbegin,kend                                         
                    DO j=jbegin,jend                                         
                       sigx = abs(uu(j,k)) + ccx(j,k)                               
                       spect(j,k) = sigx/xyj(j,k)                                         
                    ENDDO
                 ENDDO
              ELSE
                 DO k = kbegin,kend
                    DO j = jbegin,jend
                       sigx = abs(precon(j,k,5))+precon(j,k,6)
                       spect(j,k) = sigx/xyj(j,k)
                    ENDDO
                 ENDDO
              ENDIF

c  ispec = 3 use limit specral radii

         elseif(ispec.eq.3)then
           do 200 k=kbegin,kend
c
           do 250 j=jbegin,jend
           sigx = abs(uu(j,k)) + ccx(j,k)
c
           rj = 1.d0/xyj(j,k)
           spect(j,k) = sigx
250        continue
c
           do 260 j=jbegin,jend
           if(spect(j,k).lt.21.)spect(j,k)=21.
           if(spect(j,k).gt.1000.)spect(j,k)=1000.
260        continue
c
           do 270 j=jbegin,jend
           rj = 1.d0/xyj(j,k)
           spect(j,k) = spect(j,k)*rj
270        continue
c
200        continue
c
           elseif(ispec.eq.4)then
c
c  spectral radius
c
           do 400 k=kbegin,kend
           do 400 j=jbegin,jend
c
           sigx = abs(xy(j,k,1)) + abs(xy(j,k,2))
c
           rj = 1.d0/xyj(j,k)
           spect(j,k) = sigx*rj
c
400        continue
c
                       elseif(ispec.eq.-1)then
c
                       do 500 k=kbegin,kend
                       do 500 j=jbegin,jend
c
                       spect(j,k) = 64.d0/xyj(j,k)
c
500                    continue
c
        endif
c
c
                return
                end
