      subroutine map(jdim,kdim,ndim,var,d,pointer,                      
     *                              jj,jjj,jint,kk,kkk,kint,n,fwd)      
c                                                                       
c===================================================================    
      dimension d(jdim*kdim*ndim),var(jdim*kdim*ndim)                   
      integer pointer                                                   
      logical fwd                                                       
c                                                                       
c               *******    stack mapper    *******                      
c
      nm1jk=(n-1)*jdim*kdim
      if(fwd)then                                                       
c                                                                       
c           forward map                                                 
c                                                                       
          do 20 k=kk,kkk,kint                                           
            jtemp = nm1jk + (k-1)*jdim                        
              do 10 j=jj,jjj,jint                                       
                pointer = pointer + 1                                   
                var(pointer) = d(jtemp + j)                             
 10           continue                                                  
 20       continue                                                      
      else                                                              
c                                                                       
c          reverse map                                                  
c                                                                       
          do 50 k=kk,kkk,kint                                           
            jtemp = nm1jk + (k-1)*jdim                        
              do 40 j=jj,jjj,jint                                       
                pointer = pointer + 1                                   
                var(jtemp + j) = d(pointer)                             
 40           continue                                                  
 50       continue                                                      
      endif                                                             
      return                                                            
      end                                                               
