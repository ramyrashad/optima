C ********************************************************************
C ***        subroutine to output q                                ***
C ********************************************************************
      subroutine writeq(junit,q,xyj,jdim,kdim,j1,j2,k1,k2)

      implicit none
      integer junit,jdim,kdim,j1,j2,k1,k2,j,k,n
      double precision q(jdim,kdim,4), xyj(jdim,kdim)
      write(junit) (((q(j,k,n)*xyj(j,k),j=j1,j2),k=k1,k2),n=1,4)

      return 
      end
