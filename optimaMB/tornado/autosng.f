c *******************************************************************
c ** This routine automatically determines the special treatement  **
c ** at the singular points in viscous flow.  More specifically,   **
c ** the arrays determine how the pressure at the singular point   **
c ** ((jpsng1,kpsng1) in block # ibsng1 and (jpsng2,kpsng2) in     **
c ** block # ibsng2) is found - in this case it is an average of   **
c ** point (jsng1,ksng1) in block # ibsng1 and point (jsng2,ksng2) **
c ** in block # ibsng2.                                            **
c *******************************************************************
c calling routine: ioall
c
c
c
      subroutine autosng

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer ns,ii,iblkup,iblkdn
c
c ** MAIN LOOP **
c
      if (mg .or. gseq) then
        ns=nstgtot
      else
        ns=nstg
      endif

      nsng = 0
      do 100 ii=1,ns
        if (istgtyp(ii).eq.1) then
c
c         -find the upper and lower blocks that contain the l.e.
c
          if (istgcnr(ii).eq.2) then
            iblkup = istgblk(ii)
            iblkdn = lblkpt(istgblk(ii),2)
            iblkdn = lblkpt(iblkdn,1)
            iblkdn = lblkpt(iblkdn,4)
          elseif (istgcnr(ii).eq.3) then
            iblkdn = istgblk(ii)
            iblkup = lblkpt(istgblk(ii),2)
            iblkup = lblkpt(iblkup,3)
            iblkup = lblkpt(iblkup,4)
          endif
c
c set up singular point arrays
c upper block
          nsng = nsng + 1
          ibsng1(nsng) = iblkup
          jsng1(nsng) = nhalo + 1
          ksng1(nsng) = nhalo + 1
          jpsng1(nsng) = nhalo + 1
          kpsng1(nsng) = nhalo + 2
c lower block
          ibsng2(nsng) = iblkdn
          jsng2(nsng) = nhalo + 1
          ksng2(nsng) = nhalo + kbmax(iblkdn)
          jpsng2(nsng) = nhalo + 1
          kpsng2(nsng) = nhalo + kbmax(iblkdn) - 1
        endif
  100 continue
c
      return
      end
