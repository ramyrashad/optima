      subroutine mspecty(jdim,kdim,q,vv,ccy,xyj,xy,spect,sndsp)
c 
#include "../include/arcom.inc"
c
      dimension xyj(jdim,kdim),xy(jdim,kdim,4)                          
      dimension q(jdim,kdim,4),sndsp(jdim,kdim)
      dimension spect(jdim,kdim,3),vv(jdim,kdim),ccy(jdim,kdim)       
c                                                                       
c     compute dissipation scaling                                     
c                                                                       
      if (mscal) then
        do 110 k=kbegin,kend                                         
        do 110 j=jbegin,jend                                         
          rj = 1./xyj(j,k)
          rhoinv=1.d0/q(j,k,1)
          u=q(j,k,2)*rhoinv
          v=q(j,k,3)*rhoinv
          zmach=sqrt(u**2+v**2)/sndsp(j,k)
          if (zmach.gt.1.d0) zmach=1.d0
          temp=(abs(vv(j,k))+ccy(j,k))*rj*zmach
          spect(j,k,1)=max(abs(vv(j,k))*rj,vleta*temp)
          spect(j,k,2)=max(abs(vv(j,k)+ccy(j,k))*rj,vneta*temp)
          spect(j,k,3)=max(abs(vv(j,k)-ccy(j,k))*rj,vneta*temp)
 110    continue                                                     
      else
        do 100 k=kbegin,kend                                         
        do 100 j=jbegin,jend                                         
c                                                                       
c         sigy = abs(vv(j,k)) + ccy(j,k)                               
c         note: putting in jacobian                                  
          rj = 1./xyj(j,k)
          temp=(abs(vv(j,k))+ccy(j,k))*rj
          spect(j,k,1)=max(abs(vv(j,k))*rj,vleta*temp)
          spect(j,k,2)=max(abs(vv(j,k)+ccy(j,k))*rj,vneta*temp)
          spect(j,k,3)=max(abs(vv(j,k)-ccy(j,k))*rj,vneta*temp)
 100    continue
      endif
c                                                                       
      return                                                  
      end                                                     
