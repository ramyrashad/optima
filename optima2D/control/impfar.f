c---------------------------------------------------------------------
c     -- far-field boundary conditions for inviscid flow --
c     -- extrapolation is 0-order in space --
c     -- based on a. pueyo's routine impfari--
c     -- m. nemec, may 2000 --
c---------------------------------------------------------------------
      subroutine impfar (jdim, kdim, indx, iex, q, xy, xyj, x, y,
     &      as, pa, imps, bcf)

      use disscon_vars
#include "../include/arcom.inc"

      integer j, k, n1, n2, n3, nn, jdim, kdim, jsta, jsto
      integer indx(jdim,kdim), iex(jdim,kdim,4)
      dimension q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      dimension x(jdim,kdim), y(jdim,kdim)

      double precision as(jdim*kdim*4*(9*4+1)+jdim*4*3+1)
      double precision pa(jdim*kdim*4*20+1)
      double precision bcf(jdim,kdim,4)

      double precision m(4,4), pn(4,4), cc(4,4), c1(4,4), cc_tmp(4,4) 

      logical imps
      
      alphar = alpha*pi/180.d0     
      hstfs  = 1.d0/gami + 0.5d0*fsmach**2d0

c     -----------------------------------------------------------------
c     -- outer-boundary  k = kmax --
c     -----------------------------------------------------------------
      k = kend    

      if(.not.periodic) then
         jsta=jbegin+1
         jsto=jend-1
      else
         jsta=jbegin
         jsto=jend
      end if

      do j =jsta,jsto         

c     -- metrics terms --
        par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
        xy3 = xy(j,kend,3)/par
        xy4 = xy(j,kend,4)/par

c     -- matrix for k=kend --
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) -                                   
     >        0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/
     >        q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn1 = xy3*u + xy4*v
        vt1 = xy4*u - xy3*v

        a1  = a
        rho1= rho
        pr1 = pr
c     -- reset free stream values with circulation correction --
        if (circul) then
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = dsqrt(af2)        
        else
          uf = uinf
          vf = vinf
          af = dsqrt(gamma*pinf/rhoinf)
        endif
c     
        vninf = xy3*uf + xy4*vf
        vtinf = xy4*uf - xy3*vf
        ainf = af
c     -- matrix m1**-1 --
        m(1,1) = 1.d0
        m(2,1) = -u/rho
        m(3,1) = -v/rho
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(1,2) = 0.d0
        m(2,2) = 1.d0/rho
        m(3,2) = 0.d0
        m(4,2) = -u*gami
        m(1,3) = 0.d0
        m(2,3) = 0.d0
        m(3,3) = 1.d0/rho
        m(4,3) = -v*gami
        m(1,4) = 0.d0
        m(2,4) = 0.d0
        m(3,4) = 0.d0
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) = a/(gami*rho)
        pn(2,1) = -pn(1,1)
        pn(3,1) = gamma*rho**gami/pr
        pn(4,1) = 0.d0
        pn(1,2) = xy3
        pn(2,2) = xy3
        pn(3,2) = 0.d0
        pn(4,2) = xy4
        pn(1,3) = xy4
        pn(2,3) = xy4
        pn(3,3) = 0.d0
        pn(4,3) =-xy3
        pn(1,4) = -gamma/(gami*a*rho)
        pn(2,4) = -pn(1,4)
        pn(3,4) = -rho**gamma/pr**2
        pn(4,4) = 0.d0
        
c     -- multiplication & storage --
        do n1 = 1,4
          do n2 = 1,4
            cc(n1,n2) = 0.d0         
            do nn = 1,4
              cc(n1,n2) = cc(n1,n2) + pn(n1,nn)*m(nn,n2)*xyj(j,k)
            end do
          end do
        end do
        

c     -- matrix for k=kend-1 ----
        k = kend-1
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) -                                   
     >        0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/
     >        q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn2 = xy3*u + xy4*v
        vt2 = xy4*u - xy3*v
        a2  = a
        rho2= rho
        pr2 = pr
c     -- matrix m2**-1 --
        m(1,1) = 1.d0
        m(2,1) = -u/rho
        m(3,1) = -v/rho
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(1,2) = 0.d0
        m(2,2) = 1.d0/rho
        m(3,2) = 0.d0
        m(4,2) = -u*gami
        m(1,3) = 0.d0
        m(2,3) = 0.d0
        m(3,3) = 1.d0/rho
        m(4,3) = -v*gami
        m(1,4) = 0.d0
        m(2,4) = 0.d0
        m(3,4) = 0.d0
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) = 0.d0
        pn(2,1) = -a/(gami*rho)
        pn(1,2) = 0.d0
        pn(2,2) = xy3
        pn(1,3) = 0.d0
        pn(2,3) = xy4
        pn(1,4) = 0.d0
        pn(2,4) = gamma/(gami*a*rho)      
c     : inflow
        if (vn1.le.0.d0) then
          pn(3,1) = 0.d0
          pn(4,1) = 0.d0
          pn(3,2) = 0.d0
          pn(4,2) = 0.d0
          pn(3,3) = 0.d0
          pn(4,3) = 0.d0
          pn(3,4) = 0.d0
          pn(4,4) = 0.d0
c     : outflow
        else
          pn(3,1) = gamma*rho**gami/pr
          pn(4,1) = 0.d0
          pn(3,2) = 0.d0
          pn(4,2) = xy4
          pn(3,3) = 0.d0
          pn(4,3) = -xy3
          pn(3,4) = -rho**gamma/pr**2
          pn(4,4) = 0.d0           
        endif
c     -- multiplication --
        k = kend
        do n1 = 1,4
          do n2 = 1,4
            c1(n1,n2) = 0.d0
            do nn = 1,4
              c1(n1,n2) = c1(n1,n2) - pn(n1,nn)*m(nn,n2)*xyj(j,k-1)
            end do
          end do
        end do

c     -- reordering  --
        k = kend

        !- Save CC matrix before reordering. If the reordered CC matrix
        !- has a zero on any diagonal element the revert to the original
        !- CC matrix
        do n1 = 1,4
           do n2 = 1,4
              cc_tmp(n1,n2) = cc(n1,n2)
           end do
        end do

c     -- finding the biggest --  
        do n2 = 1,3
          k1 = n2
          b0 = dabs( cc(n2,n2) )
          do n1 = n2+1,4
            b1 = dabs( cc(n1,n2) )
            if ( b1.gt.b0 ) then
              k1 = n1
              b0 = b1
            end if
          end do
c     -- exchanging rows --
          if ( k1.ne.n2 ) then
            do n3 =1,4
              b1 = cc(n2,n3)
              cc(n2,n3) = cc(k1,n3)
              cc(k1,n3) = b1
              b1 = c1(n2,n3)
              c1(n2,n3) = c1(k1,n3)
              c1(k1,n3) = b1
            end do
c            ij = ( indx(j,k)-1)*4 + n2
c            ik = ( indx(j,k)-1)*4 + k1
c            itmp = iex(ij)
c            iex(ij) = iex(ik)
c            iex(ik) = itmp
            itmp = iex(j,k,n2)
            iex(j,k,n2) = iex(j,k,k1)
            iex(j,k,k1) = itmp
          endif
        end do

c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
        if ( dabs(cc(4,4)).lt.1.d-7 ) then
          do n3 =1,4
            b1 = cc(2,n3)
            cc(2,n3) = cc(4,n3)
            cc(4,n3) = b1
            b1 = c1(2,n3)
            c1(2,n3) = c1(4,n3)
            c1(4,n3) = b1
          end do
c          ij = ( indx(j,k)-1)*4 + 2
c          ik = ( indx(j,k)-1)*4 + 4
c          itmp = iex(ij)
c          iex(ij) = iex(ik)
c          iex(ik) = itmp
          itmp = iex(j,k,2)
          iex(j,k,2) = iex(j,k,4)
          iex(j,k,4) = itmp
        endif 

        !-- Check reordered CC matrix for zeros on the diagonal. If a 
        !-- zero is found, revert to the non-reordered CC matrix
        do n = 1,4
           if (cc(n,n).eq.0.0) then
              write(scr_unit,*)'impfar: farfield boundary'
              write(scr_unit,*)'Zero found on reordered CC diag...'
              write(scr_unit,*)'Revert to non-reordered CC matrix'
              write(scr_unit,*)''
              cc = cc_tmp
              goto 67
           end if
        end do

 67     continue

c     -- diagonal scaling --
        do n1 =1,4
           if(cc(n1,n1).eq.0.0)then
              write(*,*)'impfar: farfield boundary'
              write(*,*)'cc(n1,n1) = ',cc(n1,n1)
              write(*,*)'n1 = ',n1
              stop
           end if
           bcf(j,k,n1) = 1.d0/cc(n1,n1)
        end do
        
c     -- store in jacobian matrix --
        if (imps) then
           k = kend
           do n1 = 1,4
              ij = ( indx(j,k)-1)*4 + n1
              ii = ( ij - 1 )*36
              if (clopt) ii = ( ij - 1 )*37
              ip = ( ij - 1 )*20
              do n2 = 1,4
                 as(ii+n2) = c1(n1,n2)
                 as(ii+4+n2) = cc(n1,n2)
                 
                 pa(ip+n2) = c1(n1,n2)
                 pa(ip+4+n2) = cc(n1,n2)
              end do
           end do
        end if

      end do

c     -----------------------------------------------------------------
c     -- inflow boundary  j = 1 --
c     -----------------------------------------------------------------
      if(.not.periodic) then

      do k = kbegin, kend
c     
c     -- metrics terms --
        par = dsqrt(xy(jbegin,k,1)**2+(xy(jbegin,k,2)**2))
        xy1 = xy(jbegin,k,1)/par
        xy2 = xy(jbegin,k,2)/par

c     -- matrix for j=1 --
        j = 1
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) -                                   
     >        0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn1 = xy1*u + xy2*v
        vt1 =-xy2*u + xy1*v
        a1  = a
        rho1= rho
        pr1 = pr
c     -- reset free stream values with circulation correction --
        if (circul) then
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = dsqrt(af2)        
        else
          uf = uinf
          vf = vinf
          af = dsqrt(gamma*pinf/rhoinf)
        end if

        vninf = xy1*uf + xy2*vf
        vtinf =-xy2*uf + xy1*vf
        ainf = af
c     -- matrix m1**-1 --
        m(1,1) = 1.d0
        m(2,1) = -u/rho
        m(3,1) = -v/rho
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(1,2) = 0.d0
        m(2,2) = 1.d0/rho
        m(3,2) = 0.d0
        m(4,2) = -u*gami
        m(1,3) = 0.d0
        m(2,3) = 0.d0
        m(3,3) = 1.d0/rho
        m(4,3) = -v*gami
        m(1,4) = 0.d0
        m(2,4) = 0.d0
        m(3,4) = 0.d0
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) =-a/(gami*rho)
        pn(2,1) = -pn(1,1)
        pn(3,1) = gamma*rho**gami/pr
        pn(4,1) = 0.d0
        pn(1,2) = xy1
        pn(2,2) = xy1
        pn(3,2) = 0.d0
        pn(4,2) =-xy2
        pn(1,3) = xy2
        pn(2,3) = xy2
        pn(3,3) = 0.d0
        pn(4,3) = xy1
        pn(1,4) = gamma/(gami*a*rho)
        pn(2,4) = -pn(1,4)
        pn(3,4) = -rho**gamma/pr**2
        pn(4,4) = 0.d0        
c     -- multiplication --
        do n2 =1,4
          do n1 =1,4
            cc(n1,n2) = 0.d0
            do nn =1,4
              cc(n1,n2) = cc(n1,n2) + pn(n1,nn)*m(nn,n2)*xyj(j,k)
            end do
          end do
        end do

c     -- matrix for j=2 ----
        j = 2
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) -                                   
     >        0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn2 = xy1*u + xy2*v
        vt2 =-xy2*u + xy1*v
        a2  = a
        rho2= rho
        pr2 = pr
c     -- matrix m2**-1 --
        m(1,1) = 1.d0
        m(2,1) = -u/rho
        m(3,1) = -v/rho
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(1,2) = 0.d0
        m(2,2) = 1.d0/rho
        m(3,2) = 0.d0
        m(4,2) = -u*gami
        m(1,3) = 0.d0
        m(2,3) = 0.d0
        m(3,3) = 1.d0/rho
        m(4,3) = -v*gami
        m(1,4) = 0.d0
        m(2,4) = 0.d0
        m(3,4) = 0.d0
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) = 0.d0
        pn(2,1) = a/(gami*rho)
        pn(1,2) = 0.d0
        pn(2,2) = xy1
        pn(1,3) = 0.d0
        pn(2,3) = xy2
        pn(1,4) = 0.d0
        pn(2,4) = -gamma/(gami*a*rho)        
c     : inflow
        if (-vn1.le.0.d0) then
          pn(3,1) = 0.d0
          pn(4,1) = 0.d0
          pn(3,2) = 0.d0
          pn(4,2) = 0.d0
          pn(3,3) = 0.d0
          pn(4,3) = 0.d0
          pn(3,4) = 0.d0
          pn(4,4) = 0.d0
c     : outflow
        else
          pn(3,1) = gamma*rho**gami/pr
          pn(4,1) = 0.d0
          pn(3,2) = 0.d0
          pn(4,2) = -xy2
          pn(3,3) = 0.d0
          pn(4,3) = xy1
          pn(3,4) = -rho**gamma/pr**2
          pn(4,4) = 0.d0           
        endif
c     -- multiplication --
        j = 1
        do n2 =1,4
          do n1 =1,4
            c1(n1,n2) = 0.d0
            do nn =1,4
              c1(n1,n2) = c1(n1,n2) - pn(n1,nn)*m(nn,n2)*xyj(j+1,k)
            end do
          end do
        end do

c     -- reordering --
        j = 1

        !- Save CC matrix before reordering. If the reordered CC matrix
        !- has a zero on any diagonal element the revert to the original
        !- CC matrix
        do n1 = 1,4
           do n2 = 1,4
              cc_tmp(n1,n2) = cc(n1,n2)
           end do
        end do

c     -- finding the biggest --      
        do n2 =1,3
          k1 = n2
          b0 = dabs( cc(n2,n2) )
          do n1 =n2+1,4
            b1 = dabs( cc(n1,n2) )
            if ( b1.gt.b0 ) then
              k1 = n1
              b0 = b1
            endif
          end do
c     -- exchanging rows --
          if (k1.ne.n2) then
            do n3 =1,4
              b1 = cc(n2,n3)
              cc(n2,n3) = cc(k1,n3)
              cc(k1,n3) = b1
              b1 = c1(n2,n3)
              c1(n2,n3) = c1(k1,n3)
              c1(k1,n3) = b1
            end do
c            ij = ( indx(j,k)-1)*4 + n2
c            ik = ( indx(j,k)-1)*4 + k1
c            itmp = iex(ij)
c            iex(ij) = iex(ik)
c            iex(ik) = itmp
            itmp = iex(j,k,n2)
            iex(j,k,n2) = iex(j,k,k1)
            iex(j,k,k1) = itmp
          endif
        end do
c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
        if ( dabs(cc(4,4)).lt.1.d-7 ) then
          do n3 =1,4
            b1 = cc(2,n3)
            cc(2,n3) = cc(4,n3)
            cc(4,n3) = b1
            b1 = c1(2,n3)
            c1(2,n3) = c1(4,n3)
            c1(4,n3) = b1
          end do
c          ij = ( indx(j,k)-1)*4 + 2
c          ik = ( indx(j,k)-1)*4 + 4
c          itmp = iex(ij)
c          iex(ij) = iex(ik)
c          iex(ik) = itmp
          itmp = iex(j,k,2)
          iex(j,k,2) = iex(j,k,4)
          iex(j,k,4) = itmp
        endif 

        !-- Check reordered CC matrix for zeros on the diagonal. If a 
        !-- zero is found, revert to the non-reordered CC matrix
        do n = 1,4
           if (cc(n,n).eq.0.0) then
              write(scr_unit,*)'impfar: inflow boundary'
              write(scr_unit,*)'Zero found on reordered CC diag...'
              write(scr_unit,*)'Revert to non-reordered CC matrix'
              write(scr_unit,*)''
              cc = cc_tmp
              goto 68
           end if
        end do

 68     continue

c     -- diagonal scaling --
        do n1 =1,4
           if(cc(n1,n1).eq.0.0)then
              write(*,*)'impfar: inflow boundary'
              write(*,*)'cc(n1,n1) = ',cc(n1,n1)
              write(*,*)'n1 = ',n1
              stop
           end if
           bcf(j,k,n1) = 1.d0/cc(n1,n1)
        end do

c     -- store in jacobian matrix --
        if (imps) then
           j = jbegin
           do n1 = 1,4
              ij = ( indx(j,k) - 1)*4 + n1
              ii = ( ij - 1 )*36
              if (clopt) ii = ( ij - 1 )*37
              ip = ( ij - 1 )*20
              
              do n2 = 1,4
                 as(ii+n2) = cc(n1,n2)
                 as(ii+4+n2) = c1(n1,n2)
                 
                 pa(ip+n2) = cc(n1,n2)
                 pa(ip+4+n2) = c1(n1,n2)
              end do
           end do
        end if
      end do

c     -----------------------------------------------------------------
c     -- outflow boundary  j = jend --
c     -----------------------------------------------------------------
      do k = kbegin,kend

c     -- metrics terms --
        par = dsqrt(xy(jend,k,1)**2+(xy(jend,k,2)**2))
        xy1 = xy(jend,k,1)/par
        xy2 = xy(jend,k,2)/par

c     -- matrix for j=end --
        j = jend
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) -                                   
     >        0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn1 = xy1*u + xy2*v
        vt1 =-xy2*u + xy1*v
        a1  = a
        rho1= rho
        pr1 = pr
c     -- reset free stream values with circulation correction --
        if (circul) then
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = dsqrt(af2)        
        else
          uf = uinf
          vf = vinf
          af = dsqrt(gamma*pinf/rhoinf)
        endif
c     
        vninf = xy1*uf + xy2*vf
        vtinf =-xy2*uf + xy1*vf
        ainf = af
c     -- matrix m1**-1 --
        m(1,1) = 1.d0
        m(2,1) = -u/rho
        m(3,1) = -v/rho
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(1,2) = 0.d0
        m(2,2) = 1.d0/rho
        m(3,2) = 0.d0
        m(4,2) = -u*gami
        m(1,3) = 0.d0
        m(2,3) = 0.d0
        m(3,3) = 1.d0/rho
        m(4,3) = -v*gami
        m(1,4) = 0.d0
        m(2,4) = 0.d0
        m(3,4) = 0.d0
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) = a/(gami*rho)
        pn(2,1) = -pn(1,1)
        pn(3,1) = gamma*rho**gami/pr
        pn(4,1) = 0.d0
        pn(1,2) = xy1
        pn(2,2) = xy1
        pn(3,2) = 0.d0
        pn(4,2) =-xy2
        pn(1,3) = xy2
        pn(2,3) = xy2
        pn(3,3) = 0.d0
        pn(4,3) = xy1
        pn(1,4) = -gamma/(gami*a*rho)
        pn(2,4) = -pn(1,4)
        pn(3,4) = -rho**gamma/pr**2
        pn(4,4) = 0.d0       
c     -- multiplication --
        do n2 =1,4
          do n1 =1,4
            cc(n1,n2) = 0.d0
            do nn =1,4
              cc(n1,n2) = cc(n1,n2) + pn(n1,nn)*m(nn,n2)*xyj(j,k)
            end do
          end do
        end do

c     -- matrix for j=jend-1 --
        j = jend-1
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) -                                   
     >        0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn2 = xy1*u + xy2*v
        vt2 =-xy2*u + xy1*v
        a2  = a
        rho2= rho
        pr2 = pr
c     -- matrix m2**-1 --
        m(1,1) = 1.d0
        m(2,1) = -u/rho
        m(3,1) = -v/rho
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(1,2) = 0.d0
        m(2,2) = 1.d0/rho
        m(3,2) = 0.d0
        m(4,2) = -u*gami
        m(1,3) = 0.d0
        m(2,3) = 0.d0
        m(3,3) = 1.d0/rho
        m(4,3) = -v*gami
        m(1,4) = 0.d0
        m(2,4) = 0.d0
        m(3,4) = 0.d0
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) = 0.d0
        pn(2,1) = -a/(gami*rho)
        pn(1,2) = 0.d0
        pn(2,2) = xy1
        pn(1,3) = 0.d0
        pn(2,3) = xy2
        pn(1,4) = 0.d0
        pn(2,4) = gamma/(gami*a*rho)        
c     : inflow
        if (vn1.le.0.d0) then
          pn(3,1) = 0.d0
          pn(4,1) = 0.d0
          pn(3,2) = 0.d0
          pn(4,2) = 0.d0
          pn(3,3) = 0.d0
          pn(4,3) = 0.d0
          pn(3,4) = 0.d0
          pn(4,4) = 0.d0
c     : outflow
        else
          pn(3,1) = gamma*rho**gami/pr
          pn(4,1) = 0.d0
          pn(3,2) = 0.d0
          pn(4,2) =-xy2
          pn(3,3) = 0.d0
          pn(4,3) = xy1
          pn(3,4) = -rho**gamma/pr**2
          pn(4,4) = 0.d0
        endif
c     -- multiplication --
        j = jend
        do n2 =1,4
          do n1 =1,4
            c1(n1,n2) = 0.d0
            do nn =1,4
              c1(n1,n2) = c1(n1,n2) - pn(n1,nn)*m(nn,n2)*xyj(j-1,k)
            end do
          end do
        end do

c     -- reordering --
        j = jend

        !- Save CC matrix before reordering. If the reordered CC matrix
        !- has a zero on any diagonal element the revert to the original
        !- CC matrix
        do n1 = 1,4
           do n2 = 1,4
              cc_tmp(n1,n2) = cc(n1,n2)
           end do
        end do

c     -- finding the biggest --      
        do n2 =1,3
          k1 = n2
          b0 = dabs( cc(n2,n2) )
          do n1 = n2+1,4
            b1 = dabs( cc(n1,n2) )
            if (b1.gt.b0) then
              k1 = n1
              b0 = b1
            end if
          end do
c     -- exchanging rows --
          if (k1.ne.n2) then
            do n3 =1,4
              b1 = cc(n2,n3)
              cc(n2,n3) = cc(k1,n3)
              cc(k1,n3) = b1
              b1 = c1(n2,n3)
              c1(n2,n3) = c1(k1,n3)
              c1(k1,n3) = b1
            end do
c            ij = ( indx(j,k)-1)*4 + n2
c            ik = ( indx(j,k)-1)*4 + k1
c            itmp = iex(ij)
c            iex(ij) = iex(ik)
c            iex(ik) = itmp
            itmp = iex(j,k,n2)
            iex(j,k,n2) = iex(j,k,k1)
            iex(j,k,k1) = itmp
          end if
        end do
c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
        if ( dabs(cc(4,4)).lt.1.d-7 ) then
          do n3 =1,4
            b1 = cc(2,n3)
            cc(2,n3) = cc(4,n3)
            cc(4,n3) = b1
            b1 = c1(2,n3)
            c1(2,n3) = c1(4,n3)
            c1(4,n3) = b1
          end do
c          ij = ( indx(j,k)-1)*4 + 2
c          ik = ( indx(j,k)-1)*4 + 4
c          itmp = iex(ij)
c          iex(ij) = iex(ik)
c          iex(ik) = itmp
          itmp = iex(j,k,2)
          iex(j,k,2) = iex(j,k,4)
          iex(j,k,4) = itmp          
        endif 

        !-- Check reordered CC matrix for zeros on the diagonal. If a 
        !-- zero is found, revert to the non-reordered CC matrix
        do n = 1,4
           if (cc(n,n).eq.0.0) then
              write(scr_unit,*)'impfar: outflow boundary'
              write(scr_unit,*)'Zero found on reordered CC diag...'
              write(scr_unit,*)'Revert to non-reordered CC matrix'
              write(scr_unit,*)''
              cc = cc_tmp
              goto 69
           end if
        end do

 69     continue

c     -- diagonal scaling --
        do n1 =1,4
           
           if(cc(n1,n1).eq.0.0)then
              write(*,*)'impfar: outflow boundary'
              write(*,*)'cc(n1,n1) = ',cc(n1,n1)
              write(*,*)'n1 = ',n1
              stop
           end if
           bcf(j,k,n1) = 1.d0/cc(n1,n1)
        end do

c     -- store in jacobian matrix --
        if (imps) then       
           j = jend
           do n1 = 1,4
              ij = ( indx(j,k)-1)*4 + n1
              ii = ( ij - 1 )*36
              if (clopt) ii = ( ij - 1 )*37
              ip = ( ij - 1 )*20
              
              do n2 = 1,4
                 as(ii+n2) = c1(n1,n2)
                 as(ii+4+n2) = cc(n1,n2)

                 pa(ip+n2) = c1(n1,n2)
                 pa(ip+4+n2) = cc(n1,n2)
              end do
           end do
        end if
      end do
      
      end if  !not periodic

      return
      end                       !impfar
