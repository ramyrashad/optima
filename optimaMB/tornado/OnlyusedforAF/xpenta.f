c*****************************************************************      
c*****************************************************************      
c  ********  VECTORIZED in K scalar penta solver   ***************      
c*****************************************************************      
c*****************************************************************      
       subroutine xpenta(JDIM,KDIM,a,b,c,d,e,x,y,f,jl,ju,kl,ku)         
       real ld,ld1,ld2,ldi                                              
       dimension a(JDIM,KDIM),b(JDIM,KDIM),c(JDIM,KDIM),d(JDIM,KDIM),   
     *           e(JDIM,KDIM),x(JDIM,KDIM),y(JDIM,KDIM),f(JDIM,KDIM)    
C                                                                       
c	! start forward generation process and sweep  
C                                                                       
        j = jl                                                          
        do 1 k = kl,ku                                                  
        ld = c(j,k)                                                     
        ldi = 1./ld                                                     
        f(j,k) = f(j,k)*ldi                                             
        x(j,k) = d(j,k)*ldi                                             
        y(j,k) = e(j,k)*ldi                                             
1       continue                                                        
C                                                                       
        j = jl+1                                                        
        do 2 k = kl,ku                                                  
        ld1 = b(j,k)                                                    
        ld = c(j,k) - ld1*x(j-1,k)                                      
        ldi = 1./ld                                                     
        f(j,k) = (f(j,k) - ld1*f(j-1,k))*ldi                            
        x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                            
        y(j,k) = e(j,k)*ldi                                             
2       continue                                                        
C                                                                       
            do 3 j = jl+2,ju-2                                          
                do 11 k = kl,ku                                         
                ld2 = a(j,k)                                            
                ld1 = b(j,k) - ld2*x(j-2,k)                             
                ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
                ldi = 1./ld                                             
                f(j,k) = (f(j,k) - ld2*f(j-2,k) - ld1*f(j-1,k))*ldi     
                x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                    
                y(j,k) = e(j,k)*ldi                                     
11              continue                                                
3            continue                                                   
c                                                                       
        j = ju-1                                                        
        do 12 k = kl,ku                                                 
        ld2 = a(j,k)                                                    
        ld1 = b(j,k) - ld2*x(j-2,k)                                     
        ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))                     
        ldi = 1./ld                                                     
        f(j,k) = (f(j,k) - ld2*f(j-2,k) - ld1*f(j-1,k))*ldi             
        x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                            
12      continue                                                        
c                                                                       
                j = ju                                                  
                do 13 k = kl,ku                                         
                ld2 = a(j,k)                                            
                ld1 = b(j,k) - ld2*x(j-2,k)                             
                ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
                ldi = 1./ld                                             
                f(j,k) = (f(j,k) - ld2*f(j-2,k) - ld1*f(j-1,k))*ldi     
13              continue                                                
c                                                                       
c        !  back sweep solution                                         
c                                                                       
        do 14 k = kl,ku                                                 
        f(ju,k) = f(ju,k)                                               
        f(ju-1,k) = f(ju-1,k) - x(ju-1,k)*f(ju,k)                       
14      continue                                                        
c                                                                       
        do 4 j = 2,ju-JL                                                
           jx = ju-j                                                    
           do 15 k = kl,ku                                              
           f(jx,k) = f(jx,k) - x(jx,k)*f(jx+1,k) - y(jx,k)*f(jx+2,k)    
15         continue                                                     
4       continue                                                        
c                                                                       
        return                                                          
        end                                                             
