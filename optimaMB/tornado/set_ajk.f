c :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
c ::                                                                     ::
c :: mo's corner:                                                        ::
c ::                                                                     ::
c :: this is the subroutine which calculates the                         ::
c :: coefficients of the six-stage Runge-Kutta                           ::
c :: ESDIRK-4 time marching method                                       ::
c ::                                                                     ::
c :: based on Sammy Isono's 2D code                                      ::
c :: modifications for multiblock MAY 3, 2006 by Mo Tabesh               ::
c :: calling routine: nksolve                                            ::
c ::                                                                     ::
c :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

      SUBROUTINE set_ajk

      implicit none

#include "../include/common.inc"

      integer j,k

      DO j=1,6
         DO k=1,6
            ajk(j,k) = 0.d0
         ENDDO
      ENDDO

      IF (imarch.EQ.4)THEN

cmt ~~~ ESRIRK4 time marching method

         ajk(1,1) = 0.d0
         ajk(2,1) = 1.d0/4.d0
         ajk(2,2) = 1.d0/4.d0
         ajk(3,1) = 8611.d0/62500.d0
         ajk(3,2) = -1743.d0/31250.d0
         ajk(3,3) = 1.d0/4.d0
         ajk(4,1) = 5012029.d0/34652500.d0
         ajk(4,2) = -654441.d0/2922500.d0
         ajk(4,3) = 174375.d0/388108.d0
         ajk(4,4) = 1.d0/4.d0
         ajk(5,1) = 15267082809.d0/155376265600.d0
         ajk(5,2) = -71443401.d0/120774400.d0
         ajk(5,3) = 730878875.d0/902184768.d0
         ajk(5,4) = 2285395.d0/8070912.d0
         ajk(5,5) = 1.d0/4.d0
         ajk(6,1) = 82889.d0/524892.d0
         ajk(6,2) = 0.d0
         ajk(6,3) = 15625.d0/83664.d0
         ajk(6,4) = 69875.d0/102672.d0
         ajk(6,5) = -2260.d0/8211.d0
         ajk(6,6) = 1.d0/4.d0

      ELSE

cmt ~~~ BDF2 [and implicit euler?!] time marching method[s]

         ajk(1,1) = 1.d0
         ajk(2,2) = 1.d0
         ajk(3,3) = 1.d0
         ajk(4,4) = 1.d0

      ENDIF

      IF((imarch.NE.4).AND.(imarch.NE.2).AND.(imarch.NE.1)) THEN
         WRITE(*,*)"imarch has to be set to:"
         WRITE(*,*)"1 for Implicit Euler"
         WRITE(*,*)"2 for BDF-2"
         WRITE(*,*)"4 for ESDIRK-4"
         WRITE(*,*)"Sorry, no doughnut for you today!"
         STOP
      ENDIF

      RETURN
      END
