c----------------------------------------------------------------------
c     -- generate new b-spline curve coordinates given control points
c     and parameter vectors --
c     -- m. nemec, may 2001 --
c----------------------------------------------------------------------
      subroutine new_body(is, ie, nps, ncp, ifoil, bktmp, t,ap,cp,xory)

      implicit none

#include "../include/parms.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer is, ie, nps, ncp, j, i, ifoil, left, xory
      double precision t(nps), bktmp(jbsord+ncp), ap(nps), cp(ncp)
      double precision BIatT(ibsord), tmp

c     write (*,10) 
c     10   format (3x,'new_body: Generating New Airfoil Coordinates')

      tmp = -1.d0
      do j = 1,nps
         do left = jbsord,ncp

            if ( bktmp(left).le.t(j) .and. t(j).le.bktmp(left+1)
     &           .and. t(j).ne.tmp ) then 
               tmp = t(j)
               call Bval (bktmp, t(j), left, BIatT, ncp, jbsord)

               ap(j) = 0.0
               do i=1,jbsord
                  ap(j) = ap(j) + cp(i+left-jbsord)*BIatT(i)
               end do

            end if

         end do
      end do    

      j = 1     
      do i = is,ie
         bap(i,xory,ifoil) = ap(j)
         j = j+1
      end do

      return
      end                       !new_body

c-----------------------------------------------------------------------
c     calc. the value of all possibly nonzero b-spline basis functions
c     based on: carl de boor, 'practical guide to splines' pg. 134
c     
c     written by: marian nemec
c     date: Feb. 16, 2000
c-----------------------------------------------------------------------
      subroutine Bval ( bktmp, t, left, BIatT, ncp, jbsord)

      implicit none

#include "../include/optcom.inc"

      integer left,ncp,jbsord,k,kp1,i
      double precision bktmp(jbsord+ncp+1), BIatT(ibsord) 
      double precision deltaR(ibsord),deltaL(ibsord),t,tmp,saved

      k = 1
      BIatT(1) = 1.0
      if (k.ge.jbsord) return

      do while (k.lt.jbsord)
        kp1 = k+1
        deltaR(k) = bktmp(left+k) - t
        deltaL(k) = t - bktmp(left+1-k)
        saved = 0.d0

        do i = 1,k
          tmp = BIatT(i)/(deltaR(i) + deltaL(kp1-i))
          BIatT(i) = saved + deltaR(i)*tmp
          saved = deltaL(kp1-i)*tmp
        end do
        BIatT(kp1) = saved
        k = kp1
      end do

      return 
      end                       !Bval
