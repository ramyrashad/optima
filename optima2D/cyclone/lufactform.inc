c ++=============================++
c ||                             ||
c || include_file lufactform.inc ||
c ||                             ||
c ++=============================++
c
c*********************************************************************
c  this include file contains command lines from cinteg.f
c*********************************************************************
      if (jsubit .eq. 0) then 
c
c                       5.  implicit xi rhs
c                       ===================
c
c
c---- normal xi implicit integration ----
      call xiimpl(jdim,kdim,qp,s,press,sndsp,turmu,fmu,vort,x,y,xy,      
     >            xyj,xit,ds,uu,ccx,coef2x,coef4x,spect,lux,precon)
c
c     : logic for `c' mesh integration in xi requires special handleing      
c     : of lines coming off of the trailing edge                             
c                                                                       
c                                                                       
c  set integration limits                                               
c                                                                       
         kbegin  = 1                                                    
         kend    = 1                                                    
         klow    = kbegin                                               
         kup     = kend                                                 
         jbegin  = jtail2                                               
         jend    = jmax                                                 
         jlow    = jbegin+1                                             
         jup     = jend-1                                               
      call xiimpl(jdim,kdim,qp,s,press,sndsp,turmu,fmu,vort,x,y,xy,      
     >            xyj,xit,ds,uu,ccx,coef2x,coef4x,spect,lux,precon)
c                                                                       
c-----------------------------------------------------------------      
c                                                                       
c  reset limits of integration                                          
c                                                                       
          jlow   = jlowss                                               
          jup    = jupss                                                
          jbegin = jbeginss                                             
          jend   = jendss                                               
          klow   = klowss                                               
          kup    = kupss                                                
          kbegin = kbeginss                                             
          kend   = kendss                                               
c*********************************************************************  
c***   3.  implicit eta rhs                                         *** 
c*********************************************************************  
c                                                                       
c                                                                       
c        **********************************************                 
c        ********      shuffle data        ************                 
c        **********************************************                 
c                                                                       
c                                                                       
       fwd = .true.                                                     
c                                                                       
c  shuffle luy
       if (numiter.eq.istart .and. jsubit.eq.0) then
       ndim = 20                                                         
       call shuffl2(jdim,kdim,ndim,luy,jtail1,jtail2,                
     *            jmax,kmax,fwd,iplu0,iplu1)
       endif                              
c  shuffle qp                                                            
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,qp,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipq0,ipq1)                              
c  shuffle s                                                            
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,s,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ips0,ips1)                              
c  shuffle xy                                                           
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,xy,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipxy0,ipxy1)                            
c  shuffle xyj                                                          
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,xyj,work1,jtail1,jtail2,              
     *            jmax,kmax,fwd,ipxyj0,ipxyj1)                          
c  shuffle x                                                            
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,x,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipx0,ipx1)                              
c  shuffle y                                                            
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,y,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipy0,ipy1)                              
c  shuffle ett                                                          
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,ett,work1,jtail1,jtail2,              
c     *            jmax,kmax,fwd,ipett0,ipett1)                          
	if (viscous) then
c  shuffle turmu                                                        
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,turmu,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,iptu0,iptu1)                            
c  shuffle fmu                                                          
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,fmu,work1,jtail1,jtail2,              
     *            jmax,kmax,fwd,ipfu0,ipfu1)                            
c  shuffle vort                                                         
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,vort,work1,jtail1,jtail2,             
     *            jmax,kmax,fwd,ipvt0,ipvt1)                            
	endif
c  shuffle press                                                        
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,press,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ippr0,ippr1)                            
c  shuffle sndsp                                                        
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,sndsp,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ipsnd0,ipsnd1)                          
c  shuffle ds                                                           
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,ds,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipds0,ipds1)                            
       if (prec.gt.0) then
c  shuffle precon
       ndim = 6                                                         
       call shuffl(jdim,kdim,ndim,precon,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ipdu0,ipdu1) 
       endif
c                                                                       
c  call y integrator                                                    
c                                                                       
c        **********************************************                 
c        ********    airfoil block integration   ******                 
c        **********************************************                 
c                                                                       
      wake = .false.                                                    
c                                                                       
         jbegin   = 1                                                   
         jend     = jtail2-jtail1+1                                     
         jlow     = jbegin                                              
         jup      = jend                                                
         kbegin   = 1                                                   
         kend     = kmax                                                
         klow     = kbegin+1                                            
         kup      = kend-1                                              
      idimj = jtail2-jtail1+1                                           
      idimk = kmax                                                      
c*********************************************************************  
c***   7.  implicit eta rhs                                        ***  
c*********************************************************************  
c                                                                       
      call etaimpl(idimj,idimk,qp(ipq0),s(ips0),press(ippr0),            
     *     sndsp(ipsnd0),turmu(iptu0),fmu(ipfu0),vort(ipvt0),x(ipx0),   
     *     y(ipy0),xy(ipxy0),xyj(ipxyj0),ett(ipett0),ds(ipds0),vv(ipx0), 
     *     ccy(ipx0),coef2y(ipx0),coef4y(ipx0),spect,luy(iplu0),
     >     precon(ipdu0))   
c                                                                       
c*********************************************************************  
c*********************************************************************  
c                                                                       
c  note !!!! for now turbulence model is turned off in the wake.        
c            this is because the turbulence model assumes that          
c            k = 1 is a surface and in the wake block k = 1 is a        
c            far field boundary.                                        
c                                                                       
ccccc         turbsave = turbulnt                                       
ccccc         turbulnt = .false.                                        
c                                                                       
c        **********************************************                 
c        ********  wake block integration  ************                 
c        **********************************************                 
c                                                                       
        wake = .true.                                                   
c                                                                       
         jbegin  = 1                                                    
         jend    = jtail1-1                                             
         jlow    = jbegin                                               
         jup     = jend-1                                               
         kbegin  = 1                                                    
         kend    = 2*kmax-1                                             
         klow    = kbegin+1                                             
         kup     = kend-1                                               
c                                                                       
      idimj = jtail1-1                                                  
      idimk = 2*kmax-1                                                  
c                                                                       
c    multiple the metrics for j = 1,jtail1-1  k = 1,kmax                
c    by -1 to account for the rearrangement of coordinate directions    
c                                                                       
      jb = 1                                                            
      je = idimj                                                        
      kb = 1                                                            
      ke = kmax-1                                                       
      nb = 1                                                            
      ne = 4                                                            
      ndim = 4                                                          
      call swsign(idimj,idimk,ndim,xy(ipxy1),nb,ne,jb,je,kb,ke)         
c                                                                       
c*********************************************************************  
c***   7.  implicit eta rhs                                        ***  
c*********************************************************************  
c                                                                       
      call etaimpl(idimj,idimk,qp(ipq1),s(ips1),press(ippr1),            
     *   sndsp(ipsnd1),turmu(iptu1),fmu(ipfu1),vort(ipvt1),x(ipx1),   
     *   y(ipy1),xy(ipxy1),xyj(ipxyj1),ett(ipett1),ds(ipds1),         
     *   vv(ipx1),ccy(ipx1),coef2y(ipx1),coef4y(ipx1),spect,luy(iplu1),
     >   precon(ipdu1))
c                                                                       
c    multiple the metrics for j = 1,jtail1-1  k = 1,kmax                
c    by -1 to account for the rearrangement of coordinate directions    
c                                                                       
      jb = 1                                                            
      je = idimj                                                        
      kb = 1                                                            
      ke = kmax-1                                                       
      nb = 1                                                            
      ne = 4                                                            
      ndim = 4                                                          
      call swsign(idimj,idimk,ndim,xy(ipxy1),nb,ne,jb,je,kb,ke)         
c                                                                       
c                                                                       
c  reset j limits                                                       
c                                                                       
         jlow    = jlowss                                               
         jup     = jupss                                                
         jbegin  = jbeginss                                             
         jend    = jendss                                               
         klow    = klowss                                               
         kup     = kupss                                                
         kbegin  = kbeginss                                             
         kend    = kendss                                               
c                                                                       
c   reset turbulnt                                                      
c                                                                       
cccc         turbulnt = turbsave                                        
c                                                                       
c        **********************************************                 
c        ********      unshuffle data      ************                 
c        **********************************************                 
c                                                                       
c                                                                       
        fwd = .false.                                                   
c                                                                       
c  shuffle luy                                                            
c       ndim = 20                                                         
c       call shuffl(jdim,kdim,ndim,luy,work1,jtail1,jtail2,                
c     *            jmax,kmax,fwd,ipq0,ipq1)                              
c  shuffle qp                                                            
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,qp,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipq0,ipq1)                              
c  shuffle s                                                            
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,s,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ips0,ips1)                              
c  shuffle xy                                                           
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,xy,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipxy0,ipxt1)                            
c  shuffle xyj                                                          
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,xyj,work1,jtail1,jtail2,              
     *            jmax,kmax,fwd,ipxyj0,ipxyj1)                          
c  shuffle x                                                            
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,x,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipx0,ipx1)                              
c  shuffle y                                                            
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,y,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipy0,ipy1)                              
c  shuffle ett                                                          
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,ett,work1,jtail1,jtail2,              
c     *            jmax,kmax,fwd,ipett0,ipett1)                          
	if (viscous) then
c  shuffle turmu                                                        
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,turmu,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,iptu0,iptu1)                            
c  shuffle fmu                                                          
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,fmu,work1,jtail1,jtail2,              
     *            jmax,kmax,fwd,ipfu0,ipfu1)                            
c  shuffle vort                                                         
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,vort,work1,jtail1,jtail2,             
     *            jmax,kmax,fwd,ipvt0,ipvt1)                            
	endif
c  shuffle press                                                        
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,press,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ippr0,ippr1)                            
c  shuffle sndsp                                                        
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,sndsp,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ipsnd0,ipsnd1)                          
c  shuffle ds                                                           
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,ds,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipds0,ipds1)                            
       if (prec.gt.0) then
c  shuffle precon
       ndim = 6                                                         
       call shuffl(jdim,kdim,ndim,precon,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ipdu0,ipdu1) 
       endif
c                                                                       
c
c
c
c
c
c
c
c
c
c
c
c
c
      else
c
      ix = 1                                                    
      iy = 2
      call subit3(jdim,kdim,qp,sndsp,s,xyj,xy,ix,iy,lux)
c
c  logic for `c' mesh integration in xi requires special handleing      
c  of lines coming off of the trailing edge                             
c--------------------------------------------------------------------   
c  set integration limits                                               
c--------------------------------------------------------------------   
c                                                                       
         kbegin  = 1                                                    
         kend    = 1                                                    
         klow    = kbegin                                               
         kup     = kend                                                 
         jbegin  = jtail2                                               
         jend    = jmax                                                 
         jlow    = jbegin+1                                             
         jup     = jend-1
c                                               
      call subit3(jdim,kdim,qp,sndsp,s,xyj,xy,ix,iy,lux)
c
c--------------------------------------------------------------------   
c  reset limits of integration                                          
c--------------------------------------------------------------------   
c                                                                       
          jlow   = jlowss                                               
          jup    = jupss                                                
          jbegin = jbeginss                                             
          jend   = jendss                                               
          klow   = klowss                                               
          kup    = kupss                                                
          kbegin = kbeginss                                             
          kend   = kendss
c
c
      if (ismodel .eq. 2) then
         call noname(jdim,kdim,s,ds)
c        I'm not sure why the following loop doesn't do exactly what
c        the subroutine 'noname' does.
c         fct=1.5*dt/dt2
c         do 50 j=1,jdim*kdim*4
c             s(j)=s(j)*(1. + fct*ds(j))
c 50      continue
      endif
c   
c        **********************************************                 
c        ********      shuffle data        ************                 
c        **********************************************                 
c                                                                       
c                                                                       
       fwd = .true.                                                     
c                                                                       
c  shuffle luy
c  this is the shuffle in the 1st or later sub-iteration.        
c       ndim =20                                                         
c       call shuffl(jdim,kdim,ndim,luy,work1,jtail1,jtail2,                
c     *            jmax,kmax,fwd,iplu0,iplu1)                              
c  shuffle qp                                                            
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,qp,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipq0,ipq1)                              
c  shuffle s                                                            
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,s,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ips0,ips1)                              
c  shuffle xy                                                           
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,xy,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipxy0,ipxy1)                            
c  shuffle xyj                                                          
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,xyj,work1,jtail1,jtail2,              
     *            jmax,kmax,fwd,ipxyj0,ipxyj1)                          
c  shuffle x                                                            
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,x,work1,jtail1,jtail2,                
c     *            jmax,kmax,fwd,ipx0,ipx1)                              
c  shuffle y                                                            
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,y,work1,jtail1,jtail2,                
c     *            jmax,kmax,fwd,ipy0,ipy1)                              
cc  shuffle ett                                                          
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,ett,work1,jtail1,jtail2,              
c     *            jmax,kmax,fwd,ipett0,ipett1)                          
cc  shuffle turmu                                                        
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,turmu,work1,jtail1,jtail2,            
c     *            jmax,kmax,fwd,iptu0,iptu1)                            
cc  shuffle fmu                                                          
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,fmu,work1,jtail1,jtail2,              
c     *            jmax,kmax,fwd,ipfu0,ipfu1)                            
c  shuffle vort                                                         
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,vort,work1,jtail1,jtail2,             
c     *            jmax,kmax,fwd,ipvt0,ipvt1)                            
c  shuffle press                                                        
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,press,work1,jtail1,jtail2,            
c     *            jmax,kmax,fwd,ippr0,ippr1)                            
c  shuffle sndsp                                                        
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,sndsp,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ipsnd0,ipsnd1)                          
c  shuffle ds                                                           
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,ds,work1,jtail1,jtail2,               
c     *            jmax,kmax,fwd,ipds0,ipds1)                            
c
c  y integration                                                    
c                                                                       
c        **********************************************                 
c        ********    airfoil block integration   ******                 
c        **********************************************                 
c                                                                       
        wake = .false.                                                    
c                                                                       
           jbegin   = 1                                                   
           jend     = jtail2-jtail1+1                                     
           jlow     = jbegin                                              
           jup      = jend                                                
           kbegin   = 1                                                   
           kend     = kmax                                                
           klow     = kbegin+1                                            
           kup      = kend-1                                              
        idimj = jtail2-jtail1+1                                           
        idimk = kmax                                                      
c
c
        ix=3
        iy=4
        call subit3(idimj,idimk,qp(ipq0),sndsp(ipsnd0),s(ips0),
     *            xyj(ipxyj0),xy(ipxy0),ix,iy,luy(iplu0))
c                                                                       
c        **********************************************                 
c        ********  wake block integration  ************                 
c        **********************************************                 
c                                                                       
        wake = .true.                                                   
c                                                                       
         jbegin  = 1                                                    
         jend    = jtail1-1                                             
         jlow    = jbegin                                               
         jup     = jend-1                                               
         kbegin  = 1                                                    
         kend    = 2*kmax-1                                             
         klow    = kbegin+1                                             
         kup     = kend-1                                               
c                                                                       
      idimj = jtail1-1                                                  
      idimk = 2*kmax-1                                                  
c                                                                       
c    multiple the metrics for j = 1,jtail1-1  k = 1,kmax                
c    by -1 to account for the rearrangement of coordinate directions    
c                                                                       
      jb = 1                                                            
      je = idimj                                                        
      kb = 1                                                            
      ke = kmax-1                                                       
      nb = 1                                                            
      ne = 4                                                            
      ndim = 4                                                          
      call swsign(idimj,idimk,ndim,xy(ipxy1),nb,ne,jb,je,kb,ke)         
      call subit3(idimj,idimk,qp(ipq1),sndsp(ipsnd1),s(ips1),
     *            xyj(ipxyj1),xy(ipxy1),ix,iy,luy(iplu1))
c                                                                       
c    multiple the metrics for j = 1,jtail1-1  k = 1,kmax                
c    by -1 to account for the rearrangement of coordinate directions    
c                                                                       
      jb = 1                                                            
      je = idimj                                                        
      kb = 1                                                            
      ke = kmax-1                                                       
      nb = 1                                                            
      ne = 4                                                            
      ndim = 4                                                          
      call swsign(idimj,idimk,ndim,xy(ipxy1),nb,ne,jb,je,kb,ke)         
c                                                                       
c                                                                       
c  reset j limits                                                       
c                                                                       
         jlow    = jlowss                                               
         jup     = jupss                                                
         jbegin  = jbeginss                                             
         jend    = jendss                                               
         klow    = klowss                                               
         kup     = kupss                                                
         kbegin  = kbeginss                                             
         kend    = kendss                                               
c                                                                       
c        **********************************************                 
c        ********      unshuffle data      ************                 
c        **********************************************                 
c                                                                       
c                                                                       
        fwd = .false.                                                   
c                                                                       
c  shuffle luy
c  this is unshuffle in 2nd or later sub-iteration.    
c       ndim = 20                                                         
c       call shuffl(jdim,kdim,ndim,luy,work1,jtail1,jtail2,                
c     *            jmax,kmax,fwd,iplu0,iplu1)                              
c  shuffle qp                                                            
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,qp,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipq0,ipq1)                              
c  shuffle s                                                            
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,s,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ips0,ips1)                              
c  shuffle xy                                                           
       ndim = 4                                                         
       call shuffl(jdim,kdim,ndim,xy,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipxy0,ipxt1)                            
c  shuffle xyj                                                          
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,xyj,work1,jtail1,jtail2,              
     *            jmax,kmax,fwd,ipxyj0,ipxyj1)                          
c  shuffle x                                                            
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,x,work1,jtail1,jtail2,                
c     *            jmax,kmax,fwd,ipx0,ipx1)                              
c  shuffle y                                                            
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,y,work1,jtail1,jtail2,                
c     *            jmax,kmax,fwd,ipy0,ipy1)                              
c  shuffle ett                                                          
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,ett,work1,jtail1,jtail2,              
c     *            jmax,kmax,fwd,ipett0,ipett1)                          
c  shuffle turmu                                                        
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,turmu,work1,jtail1,jtail2,            
c     *            jmax,kmax,fwd,iptu0,iptu1)                            
c  shuffle fmu                                                          
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,fmu,work1,jtail1,jtail2,              
c     *            jmax,kmax,fwd,ipfu0,ipfu1)                            
c  shuffle vort                                                         
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,vort,work1,jtail1,jtail2,             
c     *            jmax,kmax,fwd,ipvt0,ipvt1)                            
c  shuffle press                                                        
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,press,work1,jtail1,jtail2,            
c     *            jmax,kmax,fwd,ippr0,ippr1)                            
c  shuffle sndsp                                                        
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,sndsp,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ipsnd0,ipsnd1)                          
c  shuffle ds                                                           
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,ds,work1,jtail1,jtail2,               
c     *            jmax,kmax,fwd,ipds0,ipds1)
      endif                            

