c ********************************************************************
c ** LU Factorization --- for tridiagonal systems                   **
c ** y sweep routine                                                **
c ** Crout's method of factorization applied to tridiagonals        **
c ********************************************************************
      subroutine yludcmp3(dm1,dd,dp1,jmax,kmax,
     &                   jlow,jup,klow,kup)
c ********************************************************************
c Variables:   dm1: vector 1 below diagonal    
c              dd : vector of diagonal entries
c              dp1: vector 1 above diagonal
c ********************************************************************
      dimension dm1(jmax,kmax), dd(jmax,kmax), dp1(jmax,kmax) 
c     outer loop over x
      do 199 j=jlow,jup   
c klow
         k = klow
         dm1(j,k+1) = dm1(j,k+1)/dd(j,k)
c klow+1        
         k = klow + 1
         dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j,k-1)
         dm1(j,k+1) = dm1(j,k+1)/dd(j,k)
c ** Main Loop *******************************************************
c        inner loop over y
         do 120 k = klow+2,kup-2 
c                                                        /* beta's  */
           dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j,k-1)
c                                                        /* alpha's */
           dm1(j,k+1) = dm1(j,k+1)/dd(j,k)
  120    continue
c ** Main Loop *******************************************************
c kup-1
         k = kup - 1
         dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j,k-1)
         dm1(j,k+1) = dm1(j,k+1)/dd(j,k)
c kup
         k = kup 
         dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j,k-1)
  199 continue
      return
      end
