********************************************************************
***    subroutine used to make the correction of q  (multigrid)  ***
********************************************************************
c     calling subroutine: mg.f
c
      subroutine mk_qp (lvl,jdim,kdim,qp,q,q0,s,xyj,turre)
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
c
      integer lvl,fblk,cblk,jdim,kdim
      dimension qp(jdim*kdim*4),q(jdim*kdim*4),q0(jdim*kdim*4)
      dimension s(jdim*kdim*4),xyj(jdim*kdim)
      dimension turre(jdim*kdim)
c
c
      call set_level (lvl+1)
c
      do iblk = nblkstart,nblkend
        call correction(iblk,jmax(iblk),kmax(iblk),
     &        s(lqptr(iblk)),qp(lqptr(iblk)),q0(lqptr(iblk)))
      enddo
c
      do iblk = nblkstart,nblkend
         fblk = iblk - nblks
         cblk = iblk
         call prolong (fblk,cblk,
     &         jmax(fblk),kmax(fblk), 
     &         s(lqptr(fblk)),xyj(lgptr(fblk)),turre(lgptr(fblk)),
     &         jmax(cblk),kmax(cblk),
     &         s(lqptr(cblk)),xyj(lgptr(cblk)),turre(lgptr(cblk)))
      enddo
c
      do iblk = nblkstart,nblkend
        call resets(jmax(iblk),kmax(iblk),s(lqptr(iblk)))
      enddo

      call set_level(lvl)
c
csd   -need to set all halo points in s to zero.
csd    -used to zero entire S in mk_pk but this is less expensive.
      do iblk = nblkstart,nblkend
        call resets2(iblk,jmax(iblk),kmax(iblk),s(lqptr(iblk)))
      enddo
c
      do iblk = nblkstart,nblkend
        call form_qp(iblk,jmax(iblk),kmax(iblk),qp(lqptr(iblk)),
     &                q(lqptr(iblk)),s(lqptr(iblk)))
      enddo

      do iblk = nblkstart,nblkend
         call resets(jmax(iblk),kmax(iblk),s(lqptr(iblk)))
      enddo

c
      return
      end









