      function  pchst(arg1,arg2)                                        
c                                                                       
      pchst = sign(1.,arg1) * sign(1.,arg2)                             
      if ((arg1.eq.0.) .or. (arg2.eq.0.)) pchst = 0.                
c                                                                     
      return                                                           
      end                                                              
