c     ******************************************************************
c     **       Routine to copy data from the interior of block2       **
c     **       to the halo points of block 1 for integer arrays!!     **
c     ******************************************************************
c     
c     When copying data, the sign does not change.
c     Note: the range of copying is  low to up.  Points must match by 
c     index in opposing blocks but low and up may differ so min/max 
c     must be used!

      subroutine halcpi(iblk,nhalo,ndim,nside1,nside2,
     &     jdim1,kdim1,iarr1,
     &     jbegin1,jend1,kbegin1,kend1,
     &     jdim2,kdim2,iarr2,
     &     jbegin2,jend2,kbegin2,kend2)

      implicit none
      integer iblk,nhalo,ndim,nside1,nside2,jdim1,kdim1,jdim2,kdim2
      integer jbegin1,jend1,kbegin1,kend1,jbegin2,jend2,kbegin2,kend2
      integer jb2,je2,kb2,ke2,kk,n,k1,k2,jj,j1h,j2i,j1,j2,k1h,k2i,ii
      integer iarr1(jdim1,kdim1,ndim),iarr2(jdim2,kdim2,ndim)

c     -form indices for begining and end of blocks
c     -note jbegin and jend extend full scope of block including nhalo
c     jb1=jbegin1+nhalo
c     je1=jend1-nhalo
      jb2=jbegin2+nhalo
      je2=jend2-nhalo

c     -note kbegin and kend extend full scope of block including nhalo
c     only when copying halo rows, during halo column copy ... not so
c     kb1=kbegin1+nhalo
c     ke1=kend1-nhalo
      kb2=kbegin2+nhalo
      ke2=kend2-nhalo

      do ii=0,nhalo-1
c     -- block 1: side 1 or 3 --
         if (nside1.eq.1 .or. nside1.eq.3) then
            if (nside1.eq.1) then
               k1h = kbegin1 + ii
            else
               k1h = kend1 - ii
            endif
            if (nside2.eq.1 .or. nside2.eq.3) then
c     -block 1 - side 1 or 3 connected to block 2 - side 1 or 3 
               if(nside2.eq.1) then
                  k2i = kb2 + nhalo - ii
               else
                  k2i = ke2 - nhalo + ii
               endif
               if(nside1.eq.nside2) then
                  j1 = max(jbegin1,jdim2+1-jend2)
                  j2 = min(jend1,jdim2+1-jbegin2)
                  do n=1,ndim
                     do jj=j1,j2
                        j1h = jj
                        j2i = jdim2 + 1 - jj
                        iarr1(j1h,k1h,n) = iarr2(j2i,k2i,n)
                     end do
                  end do
               else
                  j1 = max(jbegin1,jbegin2)
                  j2 = min(jend1,jend2)
                  do n=1,ndim
                     do jj=j1,j2 
                        j1h = jj
                        j2i = jj
                        iarr1(j1h,k1h,n) = iarr2(j2i,k2i,n)
                     end do
                  end do
               endif
            elseif(nside2.eq.2.or.nside2.eq.4)then
c     -block 1 - side 1 or 3 connected to block 2 - side 2 or 4 
               stop ' halocp error'
            endif

c     -- block 1: side 2 or 4 --
         elseif(nside1.eq.2.or.nside1.eq.4) then
            if(nside1.eq.2) then
               j1h = jbegin1 + ii
            else 
               j1h = jend1 - ii
            endif 
            if(nside2.eq.2.or.nside2.eq.4)then
c     sdr      -if nhalo=2 then second halo column is done first
               if(nside2.eq.2) then
                  j2i = jb2 + nhalo - ii
               else
                  j2i = je2 - nhalo + ii
               endif
               if(nside1.eq.nside2) then
                  k1 = max(kbegin1,kdim2+1-kend2)
                  k2 = min(kend1,kdim2+1-kbegin2)
                  do n=1,ndim
                     do kk=k1,k2
                        k1h = kk
                        k2i = kdim2 + 1 - kk
                        iarr1(j1h,k1h,n) = iarr2(j2i,k2i,n)
                     end do
                  end do
               else
                  k1 = max(kbegin1,kbegin2)
                  k2 = min(kend1,kend2)
                  do n=1,ndim
                     do kk=k1,k2
                        k1h = kk
                        k2i = kk
                        iarr1(j1h,k1h,n) = iarr2(j2i,k2i,n)
                     end do
                  end do
               endif
            elseif(nside2.eq.1.or.nside2.eq.3)then
               stop ' halocp error'
            endif
         else
            stop ' error in halocp, nside1'
         endif
      end do

      return
      end                       !halcpi
