c ********************************************************************
c ** LU Backsubstitution - for tridiagonal systems                  **
c ** y sweep routine                                                **
c ** Crout's method of factorization applied to tridiagonals        **
c ********************************************************************
      subroutine ylubcsb3(dm1,dd,dp1,b,jmax,kmax,
     &                   jlow,jup,klow,kup)
c ********************************************************************
c Variables:   dm1: vector 1 below diagonal    
c              dd : vector of diagonal entries
c              dp1: vector 1 above diagonal
c ********************************************************************
      dimension dm1(jmax,kmax), dd(jmax,kmax)
      dimension dp1(jmax,kmax), b(jmax,kmax) 
c     /* Forward Substitution */
      k = klow + 1   
      do 10 j=jlow,jup
        b(j,k) = b(j,k) - dm1(j,k)*b(j,k-1)
 10   continue
c
      do 50 k = klow+2,kup
        do 20 j=jlow,jup
          b(j,k) = b(j,k) - dm1(j,k)*b(j,k-1)
 20     continue
 50   continue
c
c     /* Back Substitution */
      k = kup  
      do 55 j=jlow,jup
        b(j,k) = b(j,k)/dd(j,k)
 55   continue
c
      k = kup - 1
      do 60 j=jlow,jup
        b(j,k) = (b(j,k) - dp1(j,k)*b(j,k+1))/dd(j,k)
 60   continue
c
      do 75 k = kup-2,klow,-1
         do 70 j=jlow,jup
             b(j,k)=(b(j,k)-dp1(j,k)*b(j,k+1))/dd(j,k)
 70      continue
 75   continue
      return
      end
