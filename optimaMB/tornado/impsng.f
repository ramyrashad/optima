c     ------------------------------------------------------------------
c     -- linearization of leading edge singular points --
c     -- inviscid flow --
c     -- m. nemec, aug. 2001 --
c     ------------------------------------------------------------------
      subroutine impsng( js1, ks1, js2, ks2, javg1, kavg1, javg2, kavg2,
     &     indx1, indx2, jmax1, kmax1, q, xyj, jmax2, kmax2, q2, xyj2,
     &     pa, as, icol, gami, precmat, jacmat)

      implicit none

      integer js1,ks1,js2,ks2,javg1,kavg1,javg2,kavg2,jmax1,kmax1,jmax2
      integer kmax2,indx1(jmax1,kmax1), indx2(jmax2,kmax2),icol(*)
      integer jk,jj,n,m,j,k,jp
      double precision xyj(jmax1,kmax1),xyj2(jmax2,kmax2),gami,rho,u,v
      double precision pa(*),as(*),q(jmax1,kmax1,4),q2(jmax2,kmax2,4)
      logical   precmat,jacmat

      if (precmat) then
c     -- zero entries in sparse matrix for (js1,ks1) block --
         do n = 1,4
            jk = ( indx1(js1,ks1) - 1 )*4 + n
            jj = (jk-1)*icol(5)
            do m = 1,4
               pa(jj+        m) = 0.0
               pa(jj+icol(1)+m) = 0.0
               pa(jj+icol(2)+m) = 0.0
               pa(jj+icol(3)+m) = 0.0
               pa(jj+icol(4)+m) = 0.0
            end do
         end do

c     -- average leading edge for (js1,ks1) block  --  
         do n = 1,3
            jk = ( indx1(js1,ks1) - 1 )*4 + n
            jj = (jk-1)*icol(5)
            pa(jj+        n) =    1.0*xyj(js1,ks1) 
            pa(jj+icol(1)+n) =  - 0.5*xyj(javg1,kavg1) 
            pa(jj+icol(2)+n) =  - 0.5*xyj2(javg2,kavg2)
         end do

c     -- zero entries in sparse matrix for (js2,ks2) block --
         do n = 1,4
            jk = ( indx2(js2,ks2) - 1 )*4 + n
            jj = (jk-1)*icol(5)
            do m = 1,4
               pa(jj+        m) = 0.0
               pa(jj+icol(1)+m) = 0.0
               pa(jj+icol(2)+m) = 0.0
               pa(jj+icol(3)+m) = 0.0
               pa(jj+icol(4)+m) = 0.0
            end do
         end do   

c     -- average leading edge for (js2,ks2) block  --  
         do n = 1,3
            jk = ( indx2(js2,ks2) - 1 )*4 + n
            jj = (jk-1)*icol(5)

            pa(jj+        n) =    1.0*xyj2(js2,ks2) 
            pa(jj+icol(1)+n) =  - 0.5*xyj2(javg2,kavg2) 
            pa(jj+icol(2)+n) =  - 0.5*xyj(javg1,kavg1)
         end do
      end if

      if (jacmat) then
c     -- zero entries in sparse matrix for (js1,ks1) block --
         do n = 1,4
            jk = ( indx1(js1,ks1) - 1 )*4 + n
            jj = (jk-1)*icol(9)
            do m = 1,4
               as(jj+        m) = 0.0
               as(jj+icol(1)+m) = 0.0
               as(jj+icol(2)+m) = 0.0
               as(jj+icol(3)+m) = 0.0
               as(jj+icol(4)+m) = 0.0
               as(jj+icol(5)+m) = 0.0
               as(jj+icol(6)+m) = 0.0
               as(jj+icol(7)+m) = 0.0
               as(jj+icol(8)+m) = 0.0
            end do
         end do

c     -- average leading edge for (js1,ks1) block  --  
         do n = 1,3
            jk = ( indx1(js1,ks1) - 1 )*4 + n
            jj = (jk-1)*icol(9)
            as(jj+        n) =    1.0*xyj(js1,ks1) 
            as(jj+icol(1)+n) =  - 0.5*xyj(javg1,kavg1) 
            as(jj+icol(2)+n) =  - 0.5*xyj2(javg2,kavg2)
         end do

c     -- zero entries in sparse matrix for (js2,ks2) block --
         do n = 1,4
            jk = ( indx2(js2,ks2) - 1 )*4 + n
            jj = (jk-1)*icol(9)
            do m = 1,4
               as(jj+        m) = 0.0
               as(jj+icol(1)+m) = 0.0
               as(jj+icol(2)+m) = 0.0
               as(jj+icol(3)+m) = 0.0
               as(jj+icol(4)+m) = 0.0
               as(jj+icol(5)+m) = 0.0
               as(jj+icol(6)+m) = 0.0
               as(jj+icol(7)+m) = 0.0
               as(jj+icol(8)+m) = 0.0
            end do
         end do   

c     -- average leading edge for (js2,ks2) block  --  
         do n = 1,3
            jk = ( indx2(js2,ks2) - 1 )*4 + n
            jj = (jk-1)*icol(9)

            as(jj+        n) =    1.0*xyj2(js2,ks2) 
            as(jj+icol(1)+n) =  - 0.5*xyj2(javg2,kavg2) 
            as(jj+icol(2)+n) =  - 0.5*xyj(javg1,kavg1)
         end do
      end if
      
c     -- average leading edge pressure for (js1,ks1) block  --  
      j = js1
      k = ks1

c     -- matrix for js1,ks1 --
      rho = q(j,k,1)*xyj(j,k)
      u   = q(j,k,2)/q(j,k,1)
      v   = q(j,k,3)/q(j,k,1)

      if (precmat) then
         jk = ( indx1(j,k) - 1 )*4 + 4
         jp = ( jk-1 )*icol(5)         
         pa(jp+1) = 0.5*(u*u+v*v)*gami*xyj(j,k)
         pa(jp+2) = -u*gami*xyj(j,k)
         pa(jp+3) = -v*gami*xyj(j,k)
         pa(jp+4) = gami*xyj(j,k)
      end if 

      if (jacmat) then
         jk = ( indx1(j,k) - 1 )*4 + 4
         jj = ( jk-1 )*icol(9)         
         as(jj+1) = 0.5*(u*u+v*v)*gami*xyj(j,k)
         as(jj+2) = -u*gami*xyj(j,k)
         as(jj+3) = -v*gami*xyj(j,k)
         as(jj+4) = gami*xyj(j,k)
      end if       

c     -- matrix for javg1,kavg1 --
      j = javg1
      k = kavg1
      rho = q(j,k,1)*xyj(j,k)
      u   = q(j,k,2)/q(j,k,1)
      v   = q(j,k,3)/q(j,k,1)

      if (precmat) then
         pa(jp+icol(1)+1) = -0.25*(u*u+v*v)*gami*xyj(j,k)
         pa(jp+icol(1)+2) = -0.5*(-u)*gami*xyj(j,k)
         pa(jp+icol(1)+3) = -0.5*(-v)*gami*xyj(j,k)
         pa(jp+icol(1)+4) = -0.5*gami*xyj(j,k)
      end if

      if (jacmat) then
         as(jj+icol(1)+1) = -0.25*(u*u+v*v)*gami*xyj(j,k)
         as(jj+icol(1)+2) = -0.5*(-u)*gami*xyj(j,k)
         as(jj+icol(1)+3) = -0.5*(-v)*gami*xyj(j,k)
         as(jj+icol(1)+4) = -0.5*gami*xyj(j,k)
      end if

c     -- matrix for javg2,kavg2 --
      j = javg2
      k = kavg2
      rho = q2(j,k,1)*xyj2(j,k)
      u   = q2(j,k,2)/q2(j,k,1)
      v   = q2(j,k,3)/q2(j,k,1)

      if (precmat) then
         pa(jp+icol(2)+1) = -0.25*(u*u+v*v)*gami*xyj2(j,k)
         pa(jp+icol(2)+2) = -0.5*(-u)*gami*xyj2(j,k)
         pa(jp+icol(2)+3) = -0.5*(-v)*gami*xyj2(j,k)
         pa(jp+icol(2)+4) = -0.5*gami*xyj2(j,k)
      end if

      if (jacmat) then
         as(jj+icol(2)+1) = -0.25*(u*u+v*v)*gami*xyj2(j,k)
         as(jj+icol(2)+2) = -0.5*(-u)*gami*xyj2(j,k)
         as(jj+icol(2)+3) = -0.5*(-v)*gami*xyj2(j,k)
         as(jj+icol(2)+4) = -0.5*gami*xyj2(j,k)
      end if

c     -- average leading edge pressure for (js2,ks2) block  --  
      j = js2
      k = ks2

c     -- matrix for js1,ks1 --
      rho = q2(j,k,1)*xyj2(j,k)
      u   = q2(j,k,2)/q2(j,k,1)
      v   = q2(j,k,3)/q2(j,k,1)

      if (precmat) then
         jk = ( indx2(j,k) - 1 )*4 + 4
         jp = ( jk-1 )*icol(5)
         pa(jp+1) = 0.5*(u*u+v*v)*gami*xyj2(j,k)
         pa(jp+2) = -u*gami*xyj2(j,k)
         pa(jp+3) = -v*gami*xyj2(j,k)
         pa(jp+4) = gami*xyj2(j,k)
      end if

      if (jacmat) then
         jk = ( indx2(j,k) - 1 )*4 + 4
         jj = ( jk-1 )*icol(9)
         as(jj+1) = 0.5*(u*u+v*v)*gami*xyj2(j,k)
         as(jj+2) = -u*gami*xyj2(j,k)
         as(jj+3) = -v*gami*xyj2(j,k)
         as(jj+4) = gami*xyj2(j,k)
      end if

c     -- matrix for javg2,kavg2 --
      j = javg2
      k = kavg2
      rho = q2(j,k,1)*xyj2(j,k)
      u   = q2(j,k,2)/q2(j,k,1)
      v   = q2(j,k,3)/q2(j,k,1)

      if (precmat) then
         pa(jp+icol(1)+1) = -0.25*(u*u+v*v)*gami*xyj2(j,k)
         pa(jp+icol(1)+2) = -0.5*(-u)*gami*xyj2(j,k)
         pa(jp+icol(1)+3) = -0.5*(-v)*gami*xyj2(j,k)
         pa(jp+icol(1)+4) = -0.5*gami*xyj2(j,k)
      end if

      if (jacmat) then
         as(jj+icol(1)+1) = -0.25*(u*u+v*v)*gami*xyj2(j,k)
         as(jj+icol(1)+2) = -0.5*(-u)*gami*xyj2(j,k)
         as(jj+icol(1)+3) = -0.5*(-v)*gami*xyj2(j,k)
         as(jj+icol(1)+4) = -0.5*gami*xyj2(j,k)
      end if

c     -- matrix for javg1,kavg1 --
      j = javg1
      k = kavg1
      rho = q(j,k,1)*xyj(j,k)
      u   = q(j,k,2)/q(j,k,1)
      v   = q(j,k,3)/q(j,k,1)

      if (precmat) then
         pa(jp+icol(2)+1) = -0.25*(u*u+v*v)*gami*xyj(j,k)
         pa(jp+icol(2)+2) = -0.5*(-u)*gami*xyj(j,k)
         pa(jp+icol(2)+3) = -0.5*(-v)*gami*xyj(j,k)
         pa(jp+icol(2)+4) = -0.5*gami*xyj(j,k)
      end if

      if (jacmat) then
         as(jj+icol(2)+1) = -0.25*(u*u+v*v)*gami*xyj(j,k)
         as(jj+icol(2)+2) = -0.5*(-u)*gami*xyj(j,k)
         as(jj+icol(2)+3) = -0.5*(-v)*gami*xyj(j,k)
         as(jj+icol(2)+4) = -0.5*gami*xyj(j,k)
      end if

      return
      end                       !impsng
