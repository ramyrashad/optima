      subroutine mcoef24y(jdim,kdim,coef2y,coef4y,pcoef)           
C                                                                       
#include "../include/arcom.inc"
c                                                                       
      dimension coef2y(jdim,kdim),coef4y(jdim,kdim)                     
      dimension pcoef(jdim,kdim)                       
c                                                                       
c     coef2y comes in as coefy (pressure gradiant coefficient)         
c     coef4y comes in as specy (spectral radius)                      
c                                                                       
c      do 1 k = kbegin,kend                                              
c      do 1 j = jbegin,jend                                              
c         pcoef(j,k) = coef2y(j,k)                                       
c 1    continue
c                                                                       
      eps4y = dis4y
      eps2y = dis2y                                                    
c                                                                       
c     form 2nd and 4th order dissipation coefficients in y            
c                                                                       
      do 10 k = kbegin,kup                                             
      do 10 j = jbegin,jend                                            
c         c2 = eps2y*(pcoef(j,k)+pcoef(j,k+1))
c         c2 = eps2y*(pcoef(j,k))
         c2 = eps2y*coef2y(j,k)
         c4 = eps4y-min(eps4y,c2)
         coef2y(j,k) = c2                                                
         coef4y(j,k) = c4                                               
10    continue
      k=kend
      km=k-1
      do 20 j=jbegin,jend
         coef4y(j,k)=coef4y(j,km)
 20   continue
c                                                                       
      return                                                         
      end                                                            







