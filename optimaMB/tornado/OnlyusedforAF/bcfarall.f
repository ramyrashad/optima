c ********************************************************************
c ********************* boundary conditions **************************
c ********************************************************************
c
c calling routine: bcupdate
c
      subroutine bcfarall(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
     &                    ibc,index1,index2,idir,iside)
c
c --------------------------------------------------------------------  
c - far field bc for outer boundary                                  -
c - far field characteristic like bc                                 -
c - applied to any side of a block                                   -
c --------------------------------------------------------------------
c.....................................................................  
c  far field circulation based on potential vortex added to reduce      
c  the dependency on outer boundary location.
c.....................................................................  
c                                                                       
#include "../include/common.inc"
c                                                                       
      dimension duqinf(4),duqext(4),duqbar(4),duqdif(4)
      dimension vk(jdim,kdim),ve(jdim,kdim)
      dimension q(jdim,kdim,4), turre(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c
      gi = 1./gamma                                                 
      gm1i = 1./gami                                                
      alphar = alpha*pi/180.                                         
      cosang = cos(alphar)                                         
      sinang = sin(alphar)                                         
      ainf = sqrt(gamma*pinf/rhoinf)                                 
      hstfs = 1./gami + 0.5*fsmach**2                                
c
      if (fsmach.gt.1.0) stop 'fsmach >1 not programmed yet'

      goto (100,200,100,200), iside
c    
  100 continue 
c ********************************************************************
c ** side 1, k=kbegin  or side 3, k=kend                            **
c ********************************************************************
      k    = ibc
      jlow = index1
      jup  = index2
      kadd = idir
      sgn  = -float(idir)
      if (prec.eq.0) then
        do 120 j = jlow,jup
c         -reset free stream values with circulation correction
c          determine far field quantities:   uf,  vf, af
          xa = x(j,k) - chord/4.d0
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc =circb/(radius*(1.d0-(fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = sqrt(af2)
c           
c         -determine extrapolated quantities:
c                                   rhoext,uext,vext,eext,pext,aext
          snorm = 1./sqrt(xy(j,k,3)**2+xy(j,k,4)**2)
          xy3h = xy(j,k,3)*snorm*sgn
          xy4h = xy(j,k,4)*snorm*sgn

          if (iord.eq.4) then
            kq=k+kadd
            xyj2 = xyj(j,kq)
            rho2 = q(j,kq,1)*xyj2                                     
            rinv = 1.d0/q(j,kq,1)                                       
            u2 = q(j,kq,2)*rinv                                        
            v2 = q(j,kq,3)*rinv
            e2 = q(j,kq,4)*xyj2
            p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
            turre2 = turre(j,kq)
            ve2 = ve(j,kq)
            vk2 = vk(j,kq)
c
            kq=k+2*kadd
            xyj3 = xyj(j,kq)
            rho3 = q(j,kq,1)*xyj3
            rinv = 1.d0/q(j,kq,1)                                       
            u3 = q(j,kq,2)*rinv                                        
            v3 = q(j,kq,3)*rinv                                       
            e3 = q(j,kq,4)*xyj3
            p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
            turre3 = turre(j,kq)
            ve3 = ve(j,kq)
            vk3 = vk(j,kq)
c     
            kq=k+3*kadd
            xyj4 = xyj(j,kq)
            rho4 = q(j,kq,1)*xyj4
            rinv = 1.d0/q(j,kq,1)
            u4 = q(j,kq,2)*rinv                                        
            v4 = q(j,kq,3)*rinv                                       
            e4 = q(j,kq,4)*xyj4
            p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c     
c           -2nd order extrapolation
            rhoext= 3.d0*(rho2 - rho3) + rho4
            uext= 3.d0*(u2 - u3) + u4
            vext= 3.d0*(v2 - v3) + v4
            pext= 3.d0*(p2 - p3) + p4
            turreext = 2.*turre2 - turre3
            veext = 2.*ve2 - ve3
            vkext = 2.*vk2 - vk3
c
c           -1st order extrapolation
c            rhoext= 2.d0*rho2 - rho3
c            uext= 2.d0*u2 - u3
c            vext= 2.d0*v2 - v3
c            pext= 2.d0*p2 - p3
c           -Zeroeth order extrapolation
c            rhoext= rho2
c            uext= u2
c            vext= v2
c            pext= p2
          else
            kq=k+kadd
            xyj2 = xyj(j,kq)
            rho2 = q(j,kq,1)*xyj2                                     
            rinv = 1.d0/q(j,kq,1)                                       
            u2 = q(j,kq,2)*rinv                                        
            v2 = q(j,kq,3)*rinv
            e2 = q(j,kq,4)*xyj2
            p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
            turre2 = turre(j,kq)
            ve2 = ve(j,kq)
            vk2 = vk(j,kq)
c
c            kq=k+2*kadd
c            xyj3 = xyj(j,kq)                                           
c            rho3 = q(j,kq,1)*xyj3
c            rinv = 1.d0/q(j,kq,1)                                       
c            u3 = q(j,kq,2)*rinv                                        
c            v3 = q(j,kq,3)*rinv                                       
c            e3 = q(j,kq,4)*xyj3
c            p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c           
c           -1st order extrapolation
c            rhoext= 2.d0*rho2 - rho3
c            uext= 2.d0*u2 - u3
c            vext= 2.d0*v2 - v3
c            pext= 2.d0*p2 - p3
c           -Zeroth-order extrapolation
            rhoext= rho2
            uext= u2
            vext= v2
            pext= p2
            turreext = turre2
            veext = ve2
            vkext = vk2
          endif
          aext   = sqrt(gamma*pext/rhoext)
c           
c         -determine the riemann invarients, r1 far field, r2 extrapolated
          r1 = (xy3h*uf + xy4h*vf)   - 2.*af*gm1i
          r2 = (xy3h*uext+xy4h*vext) + 2.*aext*gm1i
c         -determine the normal velocity and speed of sound from r1 & r2
          qn = (r1 + r2)*0.5
          cspe = (r2 - r1)*gami*0.25
          c2 = cspe**2
c           
c         -determine the tangential velocity and entropy
c             for inflow use free stream values, for outflow extrapolate
          if (qn .le. 0.0) then      
            qt = (-xy4h*uf + xy3h*vf)
            entro = gamma          
            turre(j,k) = retinf
c            turre(j,k) = turreext
            vk(j,k)= vk_inf
            ve(j,k)= ve_inf
          else                  
            qt = (-xy4h*uext + xy3h*vext) 
            entro = rhoext**gamma/pext 
            turre(j,k) = turreext
            vk(j,k)= vkext
            ve(j,k)= veext
          endif
c     
c         -determine the flow quantities qi from  r1, r2, qt and s
c          note: entro = 1/s
          u = (xy3h*qn - xy4h*qt)
          v = (xy4h*qn + xy3h*qt)
          q(j,k,1) = (c2*entro*gi)**gm1i
          q(j,k,1) = q(j,k,1)/xyj(j,k)
          pres = c2*q(j,k,1)*gi
          q(j,k,2) = q(j,k,1)*u
          q(j,k,3) = q(j,k,1)*v
          q(j,k,4) = pres*gm1i + 0.5*q(j,k,1)*(u**2+v**2)         
c          q(j,k,4) = pres*gm1i*rjj + 0.5*q(j,k,1)*(u**2+v**2)         
 120    continue
      else
        if (prec.ne.3) then
          print *,'BCFAR only written for prec=3'
          stop
        endif
c           
c          -use the preconditioned characteristics to solve the bc's
c           
            dulimit=prphi*fsmach**2
c           
            do 1000 j=jlow,jup
c           
c             -do circulation correction
               xa = x(j,k) - chord/4.                                   
               ya = y(j,k)                                               
               radius = sqrt(xa**2+ya**2)                           
               angl = atan2(ya,xa)                                  
               cjam = cos(angl)                                       
               sjam = sin(angl)                                       
               qcirc = circb/(radius* (1.-(fsmach*SIN(ANGL-ALPHAR))**2))
               uf = uinf + qcirc*sjam                                    
               vf = vinf - qcirc*cjam                                    
               af2 = gami*(hstfs - 0.5*(uf**2+vf**2))                    
               af = sqrt(af2)                                            
c           
c             -calculate values for duqinf
               duqinf(1)=af2**gm1i
               duqinf(2)=duqinf(1)*uf
               duqinf(3)=duqinf(1)*vf
               dupinf=duqinf(1)*af2*gi
               duqinf(4)=dupinf*gm1i+0.5*(uf**2+vf**2)*duqinf(1)
c           
c          -calculate values for duqext (no Jacobian in these)
c            if ((j.lt.(jup-3)).and.(j.gt.(jlow+3))) then
c               duqext(1)=2.0*q(j,k+kadd,1)*xyj(j,k+kadd)-
c     &                                 q(j,k-2*kadd,1)*xyj(j,k-2*kadd)
c               duqext(2)=2.0*q(j,k+kadd,2)*xyj(j,k+kadd)-
c     &                                 q(j,k-2*kadd,2)*xyj(j,k-2*kadd)
c               duqext(3)=2.0*q(j,k+kadd,3)*xyj(j,k+kadd)-
c     &                                 q(j,k-2*kadd,3)*xyj(j,k-2*kadd)
c               duqext(4)=2.0*q(j,k+kadd,4)*xyj(j,k+kadd)-
c     &                                 q(j,k-2*kadd,4)*xyj(j,k-2*kadd)
c            else
               duqext(1)=q(j,k+kadd,1)*xyj(j,k+kadd)
               duqext(2)=q(j,k+kadd,2)*xyj(j,k+kadd)
               duqext(3)=q(j,k+kadd,3)*xyj(j,k+kadd)
               duqext(4)=q(j,k+kadd,4)*xyj(j,k+kadd)
c            endif
c           
c             -form simple average
               duqbar(1)=0.5*(duqinf(1)+duqext(1))
               duqbar(2)=0.5*(duqinf(2)+duqext(2))
               duqbar(3)=0.5*(duqinf(3)+duqext(3))
               duqbar(4)=0.5*(duqinf(4)+duqext(4))
c           
c             -form difference
               duqdif(1)=0.5*(duqinf(1)-duqext(1))
               duqdif(2)=0.5*(duqinf(2)-duqext(2))
               duqdif(3)=0.5*(duqinf(3)-duqext(3))
               duqdif(4)=0.5*(duqinf(4)-duqext(4))
c           
c             -calculate properties at mean state
               durho=duqbar(1)
               duu=duqbar(2)/durho
               duv=duqbar(3)/durho
               duuv2=duu**2+duv**2
               dup=gami*(duqbar(4)-0.5*(duuv2)*durho)
               duc2=gamma*dup/durho
               duc=sqrt(duc2)
               duma2=duuv2/duc2
c           
               duw=xy(j,k,1)**2+xy(j,k,2)**2
               dul=xy(j,k,3)**2+xy(j,k,4)**2
c           
               dulim=max(dulimit,prxi*rhoinf*sqrt(max(duw,dul))
     +              /re/durho/fsmach)
               due=min(1.0,max(duma2,dulim))
c               due=1.0
c           
               dukx=xy(j,k,3)
               duky=xy(j,k,4)
               duuk=dukx*duu+duky*duv
c           
               dua=(1.0+due)/2.0*duuk
               dub=sqrt(((1.0-due)*duuk/2.0)**2+due*dul*duc2)
c           
               dul=sqrt(dul)
               dukx=dukx/dul
               duky=duky/dul
c           
c             -multiply by Minv
               du4=gami*(duuv2/2.0*duqdif(1)-duu*duqdif(2)-
     +              duv*duqdif(3)+duqdif(4))
               du1=du4/durho/duc
               du4=du4-duc2*duqdif(1)
               du2=(duqdif(2)-duu*duqdif(1))/durho
               du3=(duqdif(3)-duv*duqdif(1))/durho
c           
c             -multiply by Tkinv
               dut1=du4
               dut2=duky*du2-dukx*du3
               dut3=(dul*duc*du1+(duuk-dua+dub)*
     +              (dukx*du2+duky*du3))/dub/2.0
               dut4=(dul*duc*du1+(duuk-dua-dub)*
     +              (dukx*du2+duky*du3))/dub/2.0
c           
c             -multiply by sign(Lambda)
               du1=sign(1.0,duuk)*dut1
               du2=sign(1.0,duuk)*dut2
               du3=sign(1.0,dua+dub)*dut3
               du4=sign(1.0,dua-dub)*dut4
c           
c             -multiply by Tk
               dut1=((dua+dub-duuk)*du3+(duuk-dua+dub)*du4)/dul/duc
               dut2=duky*du2+dukx*(du3-du4)
               dut3=-dukx*du2+duky*(du3-du4)
               dut4=du1
c           
c             -multiply by M
               du1=(durho*dut1-dut4/duc)/duc
               du2=duu*du1+durho*dut2
               du3=duv*du1+durho*dut3
               du4=durho*((duuv2/2.0/duc+duc/gami)*dut1+
     +              duu*dut2+duv*dut3)-duuv2/2.0/duc2*dut4
c           
c             -calculate the boundry Q
               dut1=duqbar(1)-du1
               dut2=duqbar(2)-du2
               dut3=duqbar(3)-du3
               dut4=duqbar(4)-du4
c           
c             -apply
               q(j,k,1)=dut1/xyj(j,k)
               q(j,k,2)=dut2/xyj(j,k)
               q(j,k,3)=dut3/xyj(j,k)
               q(j,k,4)=dut4/xyj(j,k)
 1000       continue
      endif
  199 goto 999
c
  200 continue
c     *****************************************
c     ** side 2, j=jbegin  or side 4, j=jend **
c     *****************************************
      j = ibc
      klow = index1
      kup  = index2
      jadd = idir
      sgn  = -float(idir)
      if (prec.eq.0) then
        do 220 k = klow,kup           
c         -reset free stream values with circulation correction
c          determine far field quantities:   uf,  vf, af
          xa = x(j,k) - chord/4.d0
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc =circb/(radius*(1.d0-(fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))        
          af = sqrt(af2)        
c     
c         -determine extrapolated quantities:
c                                rhoext,uext,vext,eext,pext,aext
          snorm = 1./sqrt(xy(j,k,1)**2+xy(j,k,2)**2)
          xy1h = xy(j,k,1)*snorm*sgn
          xy2h = xy(j,k,2)*snorm*sgn
c
          if (iord.eq.4) then
            jq=j+jadd
            xyj2 = xyj(jq,k)
            rho2 = q(jq,k,1)*xyj2
            rinv = 1./q(jq,k,1)
            u2 = q(jq,k,2)*rinv
            v2 = q(jq,k,3)*rinv
            e2 = q(jq,k,4)*xyj2
            p2 = gami*(e2 - 0.5*rho2*(u2**2+v2**2))
            turre2 = turre(jq,k)
            ve2 = ve(jq,k)
            vk2 = vk(jq,k)
c     
            jq=j+2*jadd
            xyj3 = xyj(jq,k)
            rho3 = q(jq,k,1)*xyj3
            rinv = 1./q(jq,k,1)
            u3 = q(jq,k,2)*rinv
            v3 = q(jq,k,3)*rinv
            e3 = q(jq,k,4)*xyj3
            p3 = gami*(e3 - 0.5*rho3*(u3**2+v3**2))
            turre3 = turre(jq,k)
            ve3 = ve(jq,k)
            vk3 = vk(jq,k)
c     
            jq=j+3*jadd
            xyj4 = xyj(jq,k)
            rho4 = q(jq,k,1)*xyj4
            rinv = 1./q(jq,k,1)
            u4 = q(jq,k,2)*rinv
            v4 = q(jq,k,3)*rinv
            e4 = q(jq,k,4)*xyj4
            p4 = gami*(e4 - 0.5*rho4*(u4**2+v4**2))
c     
c           -2nd order extrapolation
            rhoext = 3.*(rho2 - rho3) + rho4
            uext = 3.*(u2 - u3) + u4
            vext = 3.*(v2 - v3) + v4
            pext = 3.*(p2 - p3) + p4
            turreext = 2.*turre2 - turre3
            veext = 2.*ve2 - ve3
            vkext = 2.*vk2 - vk3
          else
            jq=j+jadd
            xyj2 = xyj(jq,k)
            rho2 = q(jq,k,1)*xyj2
            rinv = 1./q(jq,k,1)
            u2 = q(jq,k,2)*rinv
            v2 = q(jq,k,3)*rinv
            e2 = q(jq,k,4)*xyj2
            p2 = gami*(e2 - 0.5*rho2*(u2**2+v2**2))
            turre2 = turre(jq,k)
            ve2 = ve(jq,k)
            vk2 = vk(jq,k)
c
c           -Zeroth order extrapolation
            rhoext = rho2
            uext = u2
            vext = v2
            pext = p2
            turreext = turre2
            veext = ve2
            vkext = vk2
          endif
          aext = sqrt(gamma*pext/rhoext)
c     
c         -determine the riemann invarients, 
c                                      r1 far field, r2 extrapolated
          r1 = (xy1h*uf + xy2h*vf)     - 2.*af*gm1i
          r2 = (xy1h*uext + xy2h*vext) + 2.*aext*gm1i
c         -determine the normal velocity and speed of sound
c                                                       from r1 & r2
          qn = 0.5*(r1 + r2)
          cspe = (r2 - r1)*gami*0.25
          c2 = cspe**2
c     
c         -determine the tangential velocity and entropy
c             for inflow use free stream values, for outflow extrapolate
          if(qn .le. 0.0)then
c             write (*,*) k,j,'inflow' 
            qt = (xy2h*uf - xy1h*vf)
            entro = gamma
            turre(j,k) = retinf
c            turre(j,k) = turreext
            vk(j,k)= vk_inf
            ve(j,k)= ve_inf
          else    
c            write (*,*) k,j,'outflow'  
            qt = (xy2h*uext - xy1h*vext)
            entro = rhoext**gamma/pext
            turre(j,k) = turreext
            vk(j,k)= vkext
            ve(j,k)= veext
          endif   
c     
c         -determine the flow quantities qi from  r1, r2, qt and s
c                                                   note: entro = 1/s
          u = (xy1h*qn + xy2h*qt)
          v = (xy2h*qn - xy1h*qt)
          q(j,k,1) = (c2*entro*gi)**gm1i
          q(j,k,1) = q(j,k,1)/xyj(j,k)
          pres = c2*q(j,k,1)*gi
c
          q(j,k,2) = q(j,k,1)*u
          q(j,k,3) = q(j,k,1)*v
          q(j,k,4) = pres*gm1i + 0.5*q(j,k,1)*(u**2+v**2)
 220    continue
      else
        if (prec.ne.3) then
          print *,'BCFAR only written for prec=3'
          stop
        endif
c           
c          -use the preconditioned characteristics to solve the bc's
c           
            dulimit=prphi*fsmach**2
c           
            do 2000 k=klow,kup
c           
c             -do circulation correction
               xa = x(j,k) - chord/4.                                   
               ya = y(j,k)                                               
               radius = sqrt(xa**2+ya**2)                                
               angl = atan2(ya,xa)                                       
               cjam = cos(angl)                                           
               sjam = sin(angl)                                           
               qcirc = circb/(radius* (1.-(fsmach*SIN(ANGL-ALPHAR))**2)) 
               uf = uinf + qcirc*sjam                                    
               vf = vinf - qcirc*cjam                                    
               af2 = gami*(hstfs - 0.5*(uf**2+vf**2))                     
               af = sqrt(af2)                                         
c           
c             -calculate values for duqinf
               duqinf(1)=af2**gm1i
               duqinf(2)=duqinf(1)*uf
               duqinf(3)=duqinf(1)*vf
               dupinf=duqinf(1)*af2*gi
               duqinf(4)=dupinf*gm1i+0.5*(uf**2+vf**2)*duqinf(1)
c           
c          -calculate values for duqext (no Jacobian in these)
c            if ((j.lt.(jup-3)).and.(j.gt.(jlow+3))) then
c               duqext(1)=2.0*q(j+jadd,k,1)*xyj(j+jadd,k)-
c     &                               q(j-2*jadd,k,1)*xyj(j-2*jadd,k)
c               duqext(2)=2.0*q(j+jadd,k,2)*xyj(j+jadd,k)-
c     &                               q(j-2*jadd,k,2)*xyj(j-2*jadd,k)
c               duqext(3)=2.0*q(j+jadd,k,3)*xyj(j+jadd,k)-
c     &                               q(j-2*jadd,k,3)*xyj(j-2*jadd,k)
c               duqext(4)=2.0*q(j+jadd,k,4)*xyj(j+jadd,k)-
c     &                               q(j-2*jadd,k,4)*xyj(j-2*jadd,k)
c            else
               duqext(1)=q(j+jadd,k,1)*xyj(j+jadd,k)
               duqext(2)=q(j+jadd,k,2)*xyj(j+jadd,k)
               duqext(3)=q(j+jadd,k,3)*xyj(j+jadd,k)
               duqext(4)=q(j+jadd,k,4)*xyj(j+jadd,k)
c            endif
c           
c             -form simple average
               duqbar(1)=0.5*(duqinf(1)+duqext(1))
               duqbar(2)=0.5*(duqinf(2)+duqext(2))
               duqbar(3)=0.5*(duqinf(3)+duqext(3))
               duqbar(4)=0.5*(duqinf(4)+duqext(4))
c           
c             -form difference
               duqdif(1)=0.5*(duqinf(1)-duqext(1))
               duqdif(2)=0.5*(duqinf(2)-duqext(2))
               duqdif(3)=0.5*(duqinf(3)-duqext(3))
               duqdif(4)=0.5*(duqinf(4)-duqext(4))
c           
c             -calculate properties at mean state
               durho=duqbar(1)
               duu=duqbar(2)/durho
               duv=duqbar(3)/durho
               duuv2=duu**2+duv**2
               dup=gami*(duqbar(4)-0.5*(duuv2)*durho)
               duc2=gamma*dup/durho
               duc=sqrt(duc2)
               duma2=duuv2/duc2
c           
               duw=xy(j,k,1)**2+xy(j,k,2)**2
               dul=xy(j,k,3)**2+xy(j,k,4)**2
c           
               dulim=max(dulimit,prxi*rhoinf*sqrt(max(duw,dul))
     +              /re/durho/fsmach)
               due=min(1.0,max(duma2,dulim))
c               due=1.0
c           
               dukx=xy(j,k,3)
               duky=xy(j,k,4)
               duuk=dukx*duu+duky*duv
c           
               dua=(1.0+due)/2.0*duuk
               dub=sqrt(((1.0-due)*duuk/2.0)**2+due*dul*duc2)
c           
               dul=sqrt(dul)
               dukx=dukx/dul
               duky=duky/dul
c           
c             -multiply by Minv
               du4=gami*(duuv2/2.0*duqdif(1)-duu*duqdif(2)-
     +              duv*duqdif(3)+duqdif(4))
               du1=du4/durho/duc
               du4=du4-duc2*duqdif(1)
               du2=(duqdif(2)-duu*duqdif(1))/durho
               du3=(duqdif(3)-duv*duqdif(1))/durho
c           
c             -multiply by Tkinv
               dut1=du4
               dut2=duky*du2-dukx*du3
               dut3=(dul*duc*du1+(duuk-dua+dub)*
     +              (dukx*du2+duky*du3))/dub/2.0
               dut4=(dul*duc*du1+(duuk-dua-dub)*
     +              (dukx*du2+duky*du3))/dub/2.0
c           
c             -multiply by sign(Lambda)
               du1=sign(1.0,duuk)*dut1
               du2=sign(1.0,duuk)*dut2
               du3=sign(1.0,dua+dub)*dut3
               du4=sign(1.0,dua-dub)*dut4
c           
c             -multiply by Tk
               dut1=((dua+dub-duuk)*du3+(duuk-dua+dub)*du4)/dul/duc
               dut2=duky*du2+dukx*(du3-du4)
               dut3=-dukx*du2+duky*(du3-du4)
               dut4=du1
c           
c             -multiply by M
               du1=(durho*dut1-dut4/duc)/duc
               du2=duu*du1+durho*dut2
               du3=duv*du1+durho*dut3
               du4=durho*((duuv2/2.0/duc+duc/gami)*dut1+
     +              duu*dut2+duv*dut3)-duuv2/2.0/duc2*dut4
c           
c             -calculate the boundry Q
               dut1=duqbar(1)-du1
               dut2=duqbar(2)-du2
               dut3=duqbar(3)-du3
               dut4=duqbar(4)-du4
c           
c             -apply
               q(j,k,1)=dut1/xyj(j,k)
               q(j,k,2)=dut2/xyj(j,k)
               q(j,k,3)=dut3/xyj(j,k)
               q(j,k,4)=dut4/xyj(j,k)
 2000        continue
      endif
 999  continue
c
      return                                                            
      end                                                               
