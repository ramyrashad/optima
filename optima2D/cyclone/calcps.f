c     subroutine to calculate pressure and sound speed

      subroutine calcps(jdim,kdim,q,press,sndsp,precon,xy,xyj)      

#include "../include/arcom.inc"
                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)
      dimension precon(jdim,kdim,6),xy(jdim,kdim,4),xyj(jdim,kdim)

c     -- pressure has jacobian --
c     -- sound speed does not have jacobian --
c     -- call after bc routine so that boundary values are correct --

      do k = kbegin,kend                                            
         do j = jbegin,jend                                            
            press(j,k) = gami*(q(j,k,4) -0.5*(q(j,k,2)**2 + q(j,k,3)**2)
     &           /q(j,k,1))
            if (press(j,k).lt.0.0 .or. q(j,k,1).lt.0.0) then
               write (scr_unit,*) '!! Negative Pressure at ',j,k
               resid = 1000.0
               return
            end if
            sndsp(j,k) = dsqrt(gamma*press(j,k)/q(j,k,1)) 
         end do
      end do

c     -- preconditioning stuff --
c     precon(j,k,   1) = epsilon
c                   2) = (u^2+v^2)/2
c                   3) = Ax
c                   4) = Bx
c                   5) = Ay
c                   6) = By where lambda_i = Ai +/- Bi

      if (prec.gt.0) then
        du1=0.0
        du2=0.0
        du3=0.0

        if (prec.eq.1) then
          du1=1.0
          epsilon1=1.0
        elseif (prec.eq.2) then
          du2=1.0
          epsilon1=1.0e-2
        else
          du3=1.0
          epsilon1=prphi*fsmach**2
        endif

        do k=kbegin,kend
          do j=jbegin,jend

            rho=q(j,k,1)*xyj(j,k)
            u=q(j,k,2)/q(j,k,1)
            v=q(j,k,3)/q(j,k,1)
            vel2=u**2+v**2
            c2=sndsp(j,k)**2

            precon(j,k,2)=.5d0*vel2

c     vel2 now mach**2
            vel2=vel2/c2
c     contravariant velocities
            ux=xy(j,k,1)*u+xy(j,k,2)*v
            uy=xy(j,k,3)*u+xy(j,k,4)*v
c     
            dul=xy(j,k,1)**2+xy(j,k,2)**2
            duw=xy(j,k,3)**2+xy(j,k,4)**2

            epsilon2=prxi*rhoinf*dsqrt(max(duw,dul))/(re*rho*fsmach)
            dlim=max(epsilon1,epsilon2)

c     for prec=1 epsilon=1
c     for prec=2 epsilon=local mach  => according to Dave Unrau
c     ... this is obsolete
c     for prec=3 epsilon=(local mach)**2
            epsilon=du1+du2*dsqrt(vel2)+du3*vel2
            epsilon=min(1.d0,max(epsilon,dlim))
c     
            precon(j,k,1)=epsilon
c     
c     forming first part of preconditioned eigenvalue
            precon(j,k,3)=.5d0*(1.0+epsilon)*ux
            precon(j,k,5)=.5d0*(1.0+epsilon)*uy
c     
c     forming second part of preconditioned eigenvalue
            precon(j,k,4)=dsqrt((.5d0*(1.d0-epsilon)*ux)**2+
     +            epsilon*(dul)*c2)
            precon(j,k,6)=dsqrt((.5d0*(1.d0-epsilon)*uy)**2+
     +            epsilon*(duw)*c2)
          end do
        end do
      endif
c     
      return                                                          
      end                       ! calcps
