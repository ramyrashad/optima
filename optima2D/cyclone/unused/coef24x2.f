c ##########################
c ##                      ##
c ##  subroutine coef24x  ##
c ##                      ##
c ##########################
c
      subroutine coef24x2 (jdim,kdim,coef2x,coef4x,pcoef,spect,
     &                                                   coef2,coef4)           
c
c*****************************************************************
c   coef2x comes in as coefx (pressure gradiant coefficient)            
c   coef4x comes in as specx (spectral radius)                          
c*****************************************************************
c
      include '../include/arcom.inc'
c                                                                       
      dimension coef2x(jdim,kdim),coef4x(jdim,kdim)                     
      dimension pcoef(jdim,kdim),spect(jdim,kdim)
      dimension coef2(jdim,kdim),coef4(jdim,kdim)
c
c
      do 2 k = kbegin,kend                                              
      do 2 j = jbegin,jend                                              
         pcoef(j,k) = coef2x(j,k)                                       
         spect(j,k) = coef4x(j,k)                                       
 2    continue                                                          
c
      eps4x = dis4x
      eps2x = dis2x                                                    
c
c
c---- form 2nd and 4th order dissipation coefficients in x ----
c
c     Why the two sets of coefficients/switches?
c     Well, Dave's version of preconditioning does not compute Gamma
c     at the half points.  So when we decided to do so, only the right
c     hand side was changed.  So the way stepf2dx.f and stepf2dy.f wants
c     the coefficients computed is the old way.  Rather than change 
c     those routines as well, I just made two sets of coefficients ...
c     one set for the RHS and the other for the LHS.
c     If you look at matrix dissipation, only one set is used but you
c     need to modify stepf2dx.f and stepf2dy.f accordingly.
c
      do 40 k = klow,kup                                            
      do 40 j = jbegin,jend  
         coef2(j,k)=eps2x*pcoef(j,k)*spect(j,k)
         coef4(j,k)=eps4x*spect(j,k)
 40   continue
c
      do 55 k=klow,kup
         do 50 j=jbegin,jup
            c2=coef2(j,k)+coef2(j+1,k)
            c4=coef4(j,k)+coef4(j+1,k)
            c4 = c4 - min(c4,c2)                                           
            coef4(j,k)=coef4(j,k)-min(coef2(j,k),coef4(j,k))
            coef2x(j,k) = c2                                               
            coef4x(j,k) = c4                                               
 50      continue
         coef4(jend,k)=coef4(jend,k)-min(coef2(jend,k),coef4(jend,k))
 55   continue
c
      return                                                         
      end
                                                            
