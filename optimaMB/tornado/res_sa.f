c     ------------------------------------------------------------------
c     -- S-A model residual equations --
c     -- m. nemec, sept. 2001 --
c     modified by Markus Rumpfkeil june 2006
c     ------------------------------------------------------------------
      subroutine res_sa(ib, jmax, kmax, jbegin, jend, jlow, jup,
     &     kbegin, kend, klow, kup, q, rsa, turre, xy, xyj, uu, vv, fmu,
     &     vort, smin,re)

      implicit none

#include "../include/parms.inc"
#include "../include/sam.inc"
#include "../include/trip.inc"
#include "../include/mbcom.inc"

      integer ib,jmax,kmax,jbegin,jend,jlow,jup,kbegin,kend,klow,kup
      double precision q(jmax,kmax,4),  rsa(jmax,kmax),   uu(jmax,kmax)
      double precision xy(jmax,kmax,4), xyj(jmax,kmax),   vv(jmax,kmax)
      double precision fmu(jmax,kmax),re
      double precision vort(jmax,kmax), turre(jmax,kmax),smin(jmax,kmax)

c     -- local variables --
      double precision workq(maxjb,maxkb,3), work(maxjb,maxkb)
      double precision reinv,const,resiginv,ujm1,ujp1,ukm1,ukp1
      double precision vjm1,vjp1,vkm1,vkp1,t0,t1,t2,tnum,tnu,tnup,trep
      double precision chi,chi3,fv1,fv2,fv3,ts,r,fw,g,pr,ds,rinv,cap
      double precision xy1p,xy2p,xy1m,xy2m,xy3p,xy4p,xy3m,xy4m
      double precision ttp,ttm,cnud,cddp,cddm,trem,cam
      integer j,k,kp1,km1,jp1,jm1

c     write (*,*) 'in res_sa'

      reinv    = 1.0d0/re
      resiginv = sigmainv*reinv
      const    = (1.0d0+cw3_6)**expon

      do k=kbegin,kend
         do j=jbegin,jend         
            workq(j,k,1)=q(j,k,1)*xyj(j,k)
            workq(j,k,2)=q(j,k,2)/q(j,k,1)
            workq(j,k,3)=q(j,k,3)/q(j,k,1)
         end do
      end do

c     -- calculate vorticity at node j,k --
      do k = klow,kup
         kp1=k+1
         km1=k-1
         do j = jlow,jup
            jp1=j+1
            jm1=j-1

            ujm1=workq(jm1,k,2)
            ujp1=workq(jp1,k,2)
            ukm1=workq(j,km1,2)
            ukp1=workq(j,kp1,2)
            vjm1=workq(jm1,k,3)
            vjp1=workq(jp1,k,3)
            vkm1=workq(j,km1,3)
            vkp1=workq(j,kp1,3)

            vort(j,k) = max(0.5d0*dabs(
     &           (vjp1-vjm1)*xy(j,k,1)+(vkp1-vkm1)*xy(j,k,3)
     &           -(ujp1-ujm1)*xy(j,k,2)-(ukp1-ukm1)*xy(j,k,4)),
     &           8.5e-10)
         end do
      end do

c     -- advective terms in xi direction --
      do k = klow,kup
         do j = jlow,jup
            t0 = dsign(1.0d0, uu(j,k))
            t1 = 0.5d0*(1.0d0 + t0)
            t2 = 0.5d0*(1.0d0 - t0)

            tnum = turre(j-1,k)
            tnu  = turre(j,k)
            tnup = turre(j+1,k)

c     -- S-A residual: storing -R(Q) for Newton's method --
            rsa(j,k) = rsa(j,k) - uu(j,k)/xyj(j,k)*( t1*( tnu - tnum ) +
     &           t2*( tnup - tnu ) )
         end do
      end do

c     -- advective terms in eta direction --
      do k = klow,kup
         do j = jlow,jup
            t0 = dsign(1.0d0, vv(j,k))
            t1 = 0.5d0*(1.0d0 + t0)
            t2 = 0.5d0*(1.0d0 - t0)

            tnum = turre(j,k-1)
            tnu  = turre(j,k)
            tnup = turre(j,k+1)

c     -- S-A residual: storing -R(Q) for Newton's method --
            rsa(j,k) = rsa(j,k) - vv(j,k)/xyj(j,k)*( t1*( tnu - tnum)
     &           + t2*( tnup - tnu ) )
         end do
      end do

c     -- calculate production and destruction terms --
      do k = klow,kup
         do j = jlow,jup
c     -- nu_tilde --
            tnu = turre(j,k)
c     -- chi = = tnu*rho/mul --
            chi = tnu*q(j,k,1)*xyj(j,k)/fmu(j,k)
c     -- chi**3 --
            chi3 = chi**3
            fv1 = chi3/(chi3+cv1_3)
            fv2 = (1.d0+chi/cv2)**(-3)
            fv3=(1+chi*fv1)*(1-fv2)/chi
c     -- t1 = 1/(k**2.d**2)
            t1 = 1.0d0/(akarman*smin(j,k))**2
c     -- S_tilde --
            ts = vort(j,k)*re*fv3 + tnu*t1*fv2
            r = tnu*t1/ts

            if ( dabs(r).ge.10.0d0 ) then
               fw = const
            else
               g = r + cw2*(r**6-r)
               fw = g*( (1.0+cw3_6)/(g**6+cw3_6) )**expon
            endif

            rinv = 1.0/xyj(j,k)
c     -- production term --
            pr = rinv*cb1*reinv*ts*tnu
c     -- destruction term --
            t1 = tnu/smin(j,k)
            ds = rinv*cw1*reinv*fw*t1**2

c     -- S-A residual: storing -R(Q) for Newton's method --
            rsa(j,k) = rsa(j,k) - ds 
c     -- No production term in cove area as well as at blunt trailing edge
c     !!Be careful hardwired!!
c            if (ib.ne.8.and.ib.ne.9 .or. (nblks.eq.4.and.ib.ne.4)) then
               rsa(j,k) = rsa(j,k) + pr
c            end if

         end do
      end do

c     -- work = nu_laminar + nu_turbulent --
      do k = kbegin,kend
         do j = jbegin,jend
            work(j,k) = fmu(j,k)/( q(j,k,1)*xyj(j,k) ) + turre(j,k)
         end do
      end do

c     -- diffusion terms in xi direction --
      do k = klow,kup
         do j = jlow,jup
            jp1 = j+1
            jm1 = j-1

            xy1p = 0.5d0*( xy(j,k,1) + xy(jp1,k,1) )
            xy2p = 0.5d0*( xy(j,k,2) + xy(jp1,k,2) )
            ttp  = ( xy1p*xy(j,k,1) + xy2p*xy(j,k,2) )
            
            xy1m = 0.5d0*( xy(j,k,1) + xy(jm1,k,1) )
            xy2m = 0.5d0*( xy(j,k,2) + xy(jm1,k,2) )
            ttm  = ( xy1m*xy(j,k,1) + xy2m*xy(j,k,2) )
            
            rinv = 1.0d0/xyj(j,k)

            cnud = cb2*resiginv*work(j,k)*rinv
            
            cddp       =    ttp*cnud
            cddm       =    ttm*cnud

            trem = 0.5d0*( work(jm1,k) + work(j,k) )
            trep = 0.5d0*( work(j,k) + work(jp1,k) )

            cap = ttp*trep*(1.0d0+cb2)*resiginv*rinv
            cam = ttm*trem*(1.0d0+cb2)*resiginv*rinv

            tnum = turre(jm1,k)
            tnu  = turre(j,k)
            tnup = turre(jp1,k)

            rsa(j,k) = rsa(j,k) + cap*( tnup - tnu ) - cam*( tnu -
     &           tnum ) - cddp*( tnup - tnu ) + cddm*( tnu - tnum )
         end do
      end do   

c     -- diffusion terms in eta direction --
      do k = klow,kup
         kp1 = k+1
         km1 = k-1
         do j = jlow,jup
            xy3p = 0.5d0*( xy(j,k,3) + xy(j,kp1,3) )
            xy4p = 0.5d0*( xy(j,k,4) + xy(j,kp1,4) )
            ttp  = ( xy3p*xy(j,k,3) + xy4p*xy(j,k,4) )
            
            xy3m = 0.5d0*( xy(j,k,3) + xy(j,km1,3) )
            xy4m = 0.5d0*( xy(j,k,4) + xy(j,km1,4) )
            ttm  = ( xy3m*xy(j,k,3) + xy4m*xy(j,k,4) )

            rinv = 1.0d0/xyj(j,k)

            cnud = cb2*resiginv*work(j,k)*rinv
            
            cddp = ttp*cnud
            cddm = ttm*cnud
            
            trem = 0.5d0*(work(j,km1)+work(j,k))
            trep = 0.5d0*(work(j,k)+work(j,kp1))
            
            cap  =  ttp*trep*(1.0d0+cb2)*resiginv*rinv
            cam  =  ttm*trem*(1.0d0+cb2)*resiginv*rinv
            
            tnum = turre(j,km1)
            tnu  = turre(j,k)
            tnup = turre(j,kp1)

            rsa(j,k) = rsa(j,k) + cap*( tnup - tnu ) - cam*( tnu -
     &           tnum ) - cddp*( tnup - tnu ) + cddm*( tnu - tnum )
         end do
      end do

      return
      end                       !res_sa
