C***********************************************************************
C****************** EXPLICIT VISCOUS TERM ******************************
C***********************************************************************
      subroutine visrhsnx(jdim,kdim,q,press,s,turmu,fmu,xy,xyj,c0,c1,c2,
     &                    c3, c4, c5, fmutemp, ttemp, beta, vflux,
     &                    jbegin,jend,jlow,jup,klow,kup)

      implicit none

#include "../include/common.inc"
#include "../include/visc.inc"
c
      integer jdim,kdim,jbegin,jend,jlow,jup,klow,kup,j,k,jp1,jm1
      double precision q(jdim,kdim,4),press(jdim,kdim),turmu(jdim,kdim)
      double precision s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)
      double precision fmu(jdim,kdim)
c
      double precision c0(jdim,kdim),beta(jdim,kdim),vflux(jdim,kdim,4)
      double precision c1(jdim,kdim),c2(jdim,kdim),c3(jdim,kdim)
      double precision c4(jdim,kdim),c5(jdim,kdim), fmutemp(jdim,kdim)
      double precision ttemp(jdim,kdim)
      double precision hre,g1,turmd,t1,t2,t3,r1,rr1,rrp1,uup1,uu1,vvp1
      double precision vv1,uxi,vxi,c2xi,fmumd,betmd,uximu,vximu,c2ximu
c
c
c         coded by tim barth -  1985
c
c         prlam   =  laminar prandtl number  = .72
c         prturb  =  turbulent prandtl number = .90
c         prlinv  =  1./(laminar prandtl number)
c         prtinv  =  1./(turbulent prandtl number)
c         f13     =  1/3
c         f23     =  2/3
c         f43     =  4/3
c         hre     =  1/2 dt * reynolds number
c         fmu     =  viscosity
c         turmu   =  turbulent viscosity
c         aa7      =  sound speed squared ... also temperature
c         beta    =  fmu/prlam + muturb/prturb
c
c
      hre   = 0.5d0*dt/(re* (1.d0 + phidt) )
      g1    = 1.d0/gami
c
c     -average fmu to get it at 1/2 pts (turmu is already at 1/2 pts)
c
      do 7 k = klow,kup
      do 7 j = jbegin,jup
        fmutemp(j,k) = 0.5d0*(fmu(j,k)+fmu(j+1,k))
 7    continue
c
c     -average backward in k to get turmu at k points
c     -average forward in j for compact differencing in xi
c
      do 10 k = klow,kup
      do 10 j = jbegin,jup
        jp1 = j + 1
        turmd = 0.25d0*(turmu(jp1,k)+ turmu(j,k) +
     >                turmu(jp1,k-1)+ turmu(j,k-1))
        beta(j,k) = (fmutemp(j,k)*prlinv + turmd*prtinv)*hre*g1
        c0(j,k)   = (fmutemp(j,k) + turmd)*hre
 10   continue
c---------------------------------------------------------------------
c          e_xi_xi viscous terms
c---------------------------------------------------------------------
      do 250 k=klow,kup
      do 250 j=jbegin,jend
        t1      = xy(j,k,1)*xy(j,k,1)
        t2      = xy(j,k,1)*xy(j,k,2)
        t3      = xy(j,k,2)*xy(j,k,2)
        r1      = 1./xyj(j,k)
        c1(j,k)   =  r1*( t1     +   t3    )
        c2(j,k)   =  r1*( f43*t1 +   t3    )
        c3(j,k)   =  r1*( t1     +   f43*t3)
        c4(j,k)   =  r1*(     t2*f13     )
 250  continue
c     
      do 260 k=klow,kup
      do 260 j=jbegin,jup
        jp1 = j + 1
        rr1   = 1.d0/q(j,   k, 1)
        rrp1  = 1.d0/q(jp1, k, 1)
        uup1  = q(jp1, k, 2)*rrp1
        uu1   = q(j,   k, 2)*rr1
        vvp1  = q(jp1, k, 3)*rrp1
        vv1   = q(j,   k, 3)*rr1
        uxi  = uup1 - uu1
        vxi  = vvp1 - vv1
        c2xi = gamma*(press(jp1, k)*rrp1 - press(j, k)*rr1)
        fmumd =  c0(j,k)
        betmd =  beta(j,k)
        uximu  = uxi*fmumd
        vximu  = vxi*fmumd
        c2ximu = c2xi*betmd
        vflux(j,k,2)=
     >        ( c2(jp1,k) + c2(j,k)  )*uximu
     >        + ( c4(jp1,k) + c4(j,k)  )*vximu
        vflux(j,k,3)=
     >        ( c4(jp1,k) + c4(j,k)  )*uximu
     >        + ( c3(jp1,k) + c3(j,k)  )*vximu
        vflux(j,k,4)= 0.5*(
     >        + (uup1 + uu1)*vflux(j,k,2)
     >        + (vvp1 + vv1)*vflux(j,k,3) )
     >        + ( c1(jp1,k) + c1(j,k) )*c2ximu
 260  continue
      do 300 k=klow,kup
      do 300 j=jlow,jup
        jm1 = j - 1
        s(j,k,2) = s(j,k,2) + vflux(j,k,2) - vflux(jm1,k,2)
        s(j,k,3) = s(j,k,3) + vflux(j,k,3) - vflux(jm1,k,3)
        s(j,k,4) = s(j,k,4) + vflux(j,k,4) - vflux(jm1,k,4)
 300  continue
c
      return
      end
