      subroutine visrhsnx(jdim,kdim,q,press,s,turmu,fmu,xy,xyj,c0,c1,c2,
     *                  c3, c4, c5, fmutemp, beta, vflux)        
c
#include "../include/arcom.inc"
#include "../include/visc.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),turmu(jdim,kdim)        
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension fmu(jdim,kdim)                                          
c                                                                       
      dimension c0(jdim,kdim),beta(jdim,kdim),vflux(jdim,kdim,4)        
      dimension c1(jdim,kdim),c2(jdim,kdim),c3(jdim,kdim)               
      dimension c4(jdim,kdim),c5(jdim,kdim), fmutemp(jdim,kdim)
c                                                                       
c                                                                       
c      parameter ( prlam = .72 , prturb=.90 )                            
c      parameter ( prlinv = 1./prlam , prtinv = 1./prturb )              
c      parameter ( f43 = 4./3.  , f13 = 1./3., f23 = 2./3. )             
c                                                                       
c         coded by tim barth -  1985                                    
c                                                                       
c         prlam   =  laminar prandtl number  = .72                      
c         prturb  =  turbulent prandtl number = .90                     
c         prlinv  =  1./(laminar prandtl number)                        
c         prtinv  =  1./(turbulent prandtl number)                      
c         f13     =  1/3                                                
c         f23     =  2/3                                                
c         f43     =  4/3                                                
c         hre     =  1/2 dt * reynolds number                           
c         fmu     =  viscosity                                          
c         turmu   =  turbulent viscosity                                
c         aa7      =  sound speed squared ... also temperature          
c         beta    =  fmu/prlam + muturb/prturb                          
c                                                                       
c                                                                       
c                                                                       
c      set up some temporary logical switches visxi,viseta,visxx        
c                                                                       
c                                                                       
c   
      hre   = dt/(re* (1. + phidt) )                                 
      g1    = 1./gami                                                   
c                                                                       
c*********************************************************************  
c*******  common variables for xi, eta and cross terms  **************  
c*********************************************************************  
c                                                                       
c                                                                       
c  averaged fmu to get it at 1/2 grid pnts  (turmu is already at 1/2 pts
c  i don't like doing this here since this is an xi operator, but       
c  i'll leave it for now.                                               
c                                                                       
      do 7 j = jbegin, jup            
      do 7 k = klow, kup                                                
       fmutemp(j, k) = 0.5d0*(fmu(j,k)+fmu(jplus(j),k))                       
 7    continue     

c beta = ((lam. vis.)/(lam. pran.) + (turb. vis.)/(turb. pran.))*dt/(Re*gami)
c      = [(total viscosity)/Pran.]*dt/(Re*gami)
c
c   c0 = (laminar viscosity + turbulent viscosity)*dt/Re 
                                                                        
      do 10 j = jbegin, jup     
         do 10 k = klow, kup   
            turmd  = turmu(j,k)  
            beta(j,k) = (fmutemp(j,k)*prlinv + turmd*prtinv)*hre*g1 
            c0(j,k)  = (fmutemp(j,k) + turmd)*hre  
 10   continue                                                          
c                                                                       
c*********************************************************************  
c                                                                       
c---------------------------------------------------------------------  
c          e_xi_xi viscous terms                                        
c---------------------------------------------------------------------  
c     t1 = xi_x **2
c     t2 = xi_x*xi_y
c     t3 = xi_y **2   

        do 250 k=klow,kup                                               
          do 250 j=jbegin,jend                               
            t1      = xy(j,k,1)*xy(j,k,1)                               
            t2      = xy(j,k,1)*xy(j,k,2)                               
            t3      = xy(j,k,2)*xy(j,k,2)                               
            r1      = 1.d0/xyj(j,k)                                       
            c1(j,k)   =  r1*( t1     +   t3    )                        
            c2(j,k)   =  r1*( f43*t1 +   t3    )                        
            c3(j,k)   =  r1*( t1     +   f43*t3)                        
            c4(j,k)   =  r1*(     t2*f13     )                          
 250    continue  

        if (iord.eq.4) then
           print *,'4th order not implemented for xi viscous terms'
           stop
        else
c        *********************************************************
c                              Second-Order
c        *********************************************************
c                                                                       
           do 260 j=jbegin,jup                                             
              jp1 = jplus(j)                                                
              do 260 k=klow,kup                                             
                 rr1   = 1./q(j,k,1)                                       
                 rrp1  = 1./q(jp1,k,1)                                       
                 uup1  = q(jp1,k,2)*rrp1                                     
                 uu1   = q(j,k,2)*rr1                                      
                 vvp1  = q(jp1,k,3)*rrp1                                     
                 vv1   = q(j,k,3)*rr1                                      
                 uxi  = uup1 - uu1                                             
                 vxi  = vvp1 - vv1                                             
                 c2xi = gamma*(press(jp1,k)*rrp1 - press(j,k)*rr1) 
                 uximu  = uxi*c0(j,k)                                           
                 vximu  = vxi*c0(j,k)                                            
                 c2ximu = c2xi*beta(j,k)
                                          
                 vflux(j,k,2)=0.5d0*( 
     >                ( c2(jp1,k) + c2(j,k)  )*uximu                            
     >                + ( c4(jp1,k) + c4(j,k)  )*vximu)                            
                 vflux(j,k,3)=0.5d0*( 
     >                ( c4(jp1,k) + c4(j,k)  )*uximu                            
     >                + ( c3(jp1,k) + c3(j,k)  )*vximu)                            
                 vflux(j,k,4)= 0.5d0*(                                         
     >                + (uup1 + uu1)*vflux(j,k,2)                                 
     >                + (vvp1 + vv1)*vflux(j,k,3)                               
     >                + ( c1(jp1,k) + c1(j,k) )*c2ximu)                            
 260       continue  
                                                        
           do 300 j=jlow,jup                                               
              jm1 = jminus(j)                                                 
              do 300 k=klow,kup                                               
                 s(j,k,2) = s(j,k,2) + vflux(j,k,2) - vflux(jm1,k,2)           
                 s(j,k,3) = s(j,k,3) + vflux(j,k,3) - vflux(jm1,k,3)           
                 s(j,k,4) = s(j,k,4) + vflux(j,k,4) - vflux(jm1,k,4)           
 300       continue     

        end if
c                                                                       
      return                                                            
      end                                                               
