c     ------------------------------------------------------------------
c     -- form dR/dQ and/or preconditioner --
c     -- m. nemec, aug. 2001
c     ------------------------------------------------------------------
      subroutine dRdQ( nmax, q, press, sndsp, xy, xyj, precon, xit,
     &     ett, fmu, x, y, turmu, vort, turre, vk, ve, coef4x, coef4y,
     &     uu, vv, ccx, ccy, coef2x, coef2y, indx, pdc, pa, as, icol,
     &     iex, bcf, precmat, jacmat, imps)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"
#include "../include/scratch.inc"
#include "../include/turb.inc"

      integer nmax,ib,i,lg,lq,k,j,n,is,ib2,is2,lq2,lg2
      double precision q(*),       press(*),     sndsp(*)
      double precision xy(*),      xyj(*),       precon(*)
      double precision xit(*),     ett(*),       fmu(*)
      double precision x(*),       y(*),         turmu(*)
      double precision vort(*),    turre(*),     vk(*) 
      double precision ve(*),      coef4x(*),    coef4y(*)
      double precision uu(*),      vv(*),        ccx(*)
      double precision ccy(*),     coef2x(*),    coef2y(*)
      integer iex(maxjb,4,4,mxbfine),indx(*),icol(*)

      double precision pa(*), as(*), g2, pdc
      double precision bcf(maxjb,4,4,mxbfine)
      logical   precmat, jacmat

c     -- local variables --
      logical   imps
c     -- s-a model work arrays --
      double precision chi(maxjb,maxkb),   dchi(5,maxjb,maxkb)
      double precision fv1(maxjb,maxkb),   dfv1(maxjb,maxkb)
      double precision bm2(5,maxjb,maxkb), bm1(5,maxjb,maxkb)
      double precision bp1(5,maxjb,maxkb), bp2(5,maxjb,maxkb)
      double precision db(5,maxjb,maxkb),  dmul(jacb,maxjb,maxkb)

      logical frozen
      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

c     -- form 4x4 inviscid flux jacobian matrix at each grid point --
      DO ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)

cmt     -- time - unsteady  --
         IF (unsteady) THEN
            CALL filltime(jmax(ib), kmax(ib), nmax, 
     &          ibctype(ib,1),ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &          kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib),
     &          jlow(ib), jup(ib),klow(ib), kup(ib),
     &          indx(lg),pa,as,icol,jacmat,precmat)
         ENDIF

c     -- x - direction --
         CALL fillax( jmax(ib), kmax(ib), nmax, 
     &        ibctype(ib,1),ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &        kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), 
     &        jbegin(ib) , jend(ib), klow(ib), kup(ib), 
     &        indx(lg), q(lq), xy(lq),
     &        pa, as,icol, gamma, gami, precmat, jacmat, maxjb, maxkb)

c     -- y - direction --
         CALL fillay( jmax(ib), kmax(ib), nmax,
     &        ibctype(ib,1),ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &        kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), 
     &        jlow(ib) , jup(ib), kbegin(ib), kend(ib), 
     &        indx(lg), q(lq), xy(lq),
     &        pa, as,icol, gamma, gami, precmat, jacmat, maxjb, maxkb)
      ENDDO

c     -- calculate uu, vv, ccx and ccy --
c     -- used in dissipation and S-A model --
      DO ib=1,nblks
         lg=lgptr(ib)
         lq=lqptr(ib)

         CALL eigval( 1, 2, jmax(ib), kmax(ib), q(lq), press(lg),
     &        sndsp(lg), xyj(lg), xy(lq), xit(lg), uu(lg), ccx(lg),
     &        jbegin2(ib), jend2(ib), kminbnd(ib), kmaxbnd(ib))

         CALL eigval( 3, 4, jmax(ib), kmax(ib), q(lq), press(lg),
     &        sndsp(lg), xyj(lg), xy(lq), ett(lg), vv(lg), ccy(lg),
     &        jminbnd(ib), jmaxbnd(ib), kbegin2(ib), kend2(ib))
      ENDDO
c     ------------------------------------------------------------------

c     -- viscous terms --
      IF (viscous) THEN

         DO ib = 1,nblks
            lg=lgptr(ib)
            lq=lqptr(ib)

c     -- differentiation of the viscous flux vector w.r.t. Q^ --
            CALL impvis( jmax(ib), kmax(ib), nmax,
     &          ibctype(ib,1),ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &          kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), 
     &          jbegin(ib), jend(ib), jlow(ib), jup(ib),
     &          klow(ib),kup(ib),
     &          xy(lq), xyj(lg), q(lq), turre(lg),fmu(lg), turmu(lg),
     &          press(lg), sndsp(lg), chi, dchi,fv1, dfv1, dmul,
     &          indx(lg), as, pa, icol, precmat, jacmat, re, gamma,
     &          gami, tinf)

c     -- spalart - allmaras model contribution --
            IF (turbulnt .AND. iturb.EQ.3) THEN

c     -- zero S-A jacobian arrays --
               DO k = 1,maxkb
                  DO j = 1,maxjb
                     DO n = 1,5
                        bm2(n,j,k) = 0.d0
                        bm1(n,j,k) = 0.d0
                        db(n,j,k)  = 0.d0
                        bp1(n,j,k) = 0.d0
                        bp2(n,j,k) = 0.d0
                     ENDDO
                  ENDDO
               ENDDO
   
c     -- advective terms --
               CALL dsa_adv(jmax(ib), kmax(ib), jminbnd(ib),
     &              jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), q(lq),
     &              turre(lg), xy(lq), xyj(lg), uu(lg), vv(lg), bm2,
     &              bm1, db, bp1, bp2)

c     -- production and destruction terms --
               CALL dsa_pd(ib, jmax(ib), kmax(ib), jbegin(ib), jend(ib), 
     &              jlow(ib), jup(ib), kbegin(ib), kend(ib), klow(ib),
     &              kup(ib), q(lq), turre(lg), xy(lq), xyj(lg),
     &              bm2, bm1, db, bp1, bp2, chi, dchi, fv1, dfv1,
     &              vort(lg), smin(lg), re)

c     -- diffusion terms --
               CALL dsa_dif( jmax(ib), kmax(ib), jbegin(ib), jend(ib),
     &              jlow(ib), jup(ib), kbegin(ib), kend(ib), klow(ib),
     &              kup(ib),  q(lq), turre(lg), xy(lq), xyj(lg),
     &              fmu(lg), dmul, bm2, bm1, db, bp1, bp2, re)

c     -- add to jacobian --
               CALL fillat (jmax(ib), kmax(ib), nmax, ibctype(ib,1),
     &              ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &              kminbnd(ib), kmaxbnd(ib), jminbnd(ib),
     &              jmaxbnd(ib), indx(lg), icol, as, pa, bm2, bm1, db,
     &              bp1, bp2, jacmat, precmat)

            ENDIF  !turbulnt

         ENDDO 

      ENDIF  !viscous

c     -- implicit artificial dissipation --
c     -- xi direction dissipation --
c     -- taken from disxnh2.inc --
c     -- form spectral radii for dissipation model --
      DO ib=1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)
         CALL spectx( jmax(ib), kmax(ib), uu(lg), ccx(lg),
     &        xyj(lg), xy(lq), coef4x(lg), precon(lprecptr(ib)), 
     &        jbegin2(ib), jend2(ib), klow(ib),kup(ib))
      ENDDO
      IF (dis2x .NE. 0.0) THEN
         STOP 'drdq: dis2x .ne. 0.0'
      ENDIF

c     -- compute the second and fourth order dissipation coefficients --
c     -- linearization of spectral radius for dissipation --
      IF (jacmat) THEN
         g2  = gamma*gami
         DO ib = 1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            CALL dspectx (jmax(ib), kmax(ib), nmax, indx(lg), icol,
     &          q(lq), xy(lq), xyj(lg), uu(lg), sndsp(lg), as,
     &          coef2x(lg), coef4x(lg),
     &          ibctype(ib,1),ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &          jbegin(ib), jend(ib), jlow(ib), jup(ib),
     &          jminbnd(ib), jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib),
     &          dis2x, dis4x, g2)
         ENDDO
      ELSE
         DO ib=1,nblks
            lg = lgptr(ib)
            CALL coef24x(jmax(ib),kmax(ib),coef2x(lg),coef4x(lg),
     &           d,d(1,2),jbegin2(ib),jend2(ib),klow(ib),kup(ib))
         ENDDO
      ENDIF

c     -- store contribution from dissipation --
      DO ib=1,nblks 
         lg = lgptr(ib)
         lq = lqptr(ib)
         CALL filladx( pdc, jmax(ib), kmax(ib), nmax, 
     &        ibctype(ib,1),ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &        kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), 
     &        jbegin(ib) , jend(ib), klow(ib), kup(ib), 
     &        indx(lg), xyj(lg), coef2x(lg), coef4x(lg), pa,as, icol,
     &        precmat, jacmat)
      ENDDO

c     -- eta direction dissipation --
c     -- taken from disynh2.inc --
c     -- form spectral radii for dissipation model --
      DO ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)
         CALL specty( jmax(ib), kmax(ib), vv(lg), ccy(lg),
     &        xyj(lg), xy(lq), coef4y(lg),
     &        precon(lprecptr(ib)), 
     &        jlow(ib), jup(ib),kbegin2(ib), kend2(ib))
      ENDDO

      
      IF (dis2y.NE.0.0) THEN
         STOP 'drdq: dis2y .ne. 0.0'
      ENDIF

c     -- linearization of spectral radius for dissipation --
      IF (jacmat) THEN
         g2  = gamma*gami
         DO ib = 1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            CALL dspecty (jmax(ib), kmax(ib), nmax, indx(lg), icol,
     &          q(lq), xy(lq), xyj(lg), vv(lg), sndsp(lg), as,
     &          coef2y(lg), coef4y(lg),
     &          ibctype(ib,1),ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &          jminbnd(ib), jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib),
     &          dis2y, dis4y, g2)
         ENDDO
      ELSE
c     -- compute the second and fourth order diss. coefficients --
c     -- do not need to copy coef2y and 4y into halos -- 
         DO ib = 1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            CALL coef24y( jmax(ib), kmax(ib), coef2y(lg), coef4y(lg), 
     &           d , d(1,2), jlow(ib), jup(ib), kbegin2(ib), kend2(ib))
         ENDDO
      ENDIF

c     -- store contribution from dissipation --
      DO ib = 1,nblks
         lg = lgptr(ib)
         lq = lqptr(ib)
         CALL fillady( pdc, jmax(ib), kmax(ib), nmax,
     &        ibctype(ib,1),ibctype(ib,2),ibctype(ib,3),ibctype(ib,4),
     &        kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), 
     &        jlow(ib) , jup(ib), kbegin(ib), kend(ib), 
     &        indx(lg), xyj(lg), coef2y(lg), coef4y(lg), pa,as, icol,
     &        precmat, jacmat)
      ENDDO


c     -- initialize row index to keep track of row shuffling --
      do ib = 1,nblks
         do is = 1,4
            do n = 1,4
               do j = 1,maxjb
                  iex(j,n,is,ib) = n
                  bcf(j,n,is,ib) = 1.0d0
               end do
            end do
         end do
      end do

      if (imps) then

c        -- average BC --
c        -- bctype = 5 and side = 1 or 3 --
         do ib = 1,nblks
            do is = 1,3,2
               if ( ibctype(ib,is).eq.5 ) then
                  ib2 = lblkpt(ib,is)
                  is2 = lsidept(ib,is)
                  lq  = lqptr(ib)
                  lq2 = lqptr(ib2)
                  lg  = lgptr(ib)
                  lg2 = lgptr(ib2)
                  call ibcavg( is, is2, indx(lg), xyj(lg), q(lq),
     &                 jmax(ib), kmax(ib), nmax, xyj(lg2), q(lq2),
     &                 jmax(ib2), kmax(ib2), jminbnd(ib), jmaxbnd(ib),
     &                 kminbnd(ib), kmaxbnd(ib), kminbnd(ib2),
     &                 kmaxbnd(ib2),pa,as,icol,gami,precmat,jacmat)
               end if
            end do
         end do

c        -- implicit b.c. for body and farfield --
         call impbc( imps, nmax, indx, q, xy, xyj, press, pa, as, icol,
     &        bcf, iex, precmat, jacmat)

c        -- copy points and singular points --
c        -- leading edge singular points --
         if (.not. viscous) then
            do i = 1,nsing
               call impsng( js1(i), ks1(i), js2(i), ks2(i), javg1(i), 
     &              kavg1(i), javg2(i), kavg2(i), indx(lgptr(ibs1(i))),
     &              indx(lgptr(ibs2(i))), jmax(ibs1(i)), kmax(ibs1(i)),
     &              q(lqptr(ibs1(i))),xyj(lgptr(ibs1(i))),jmax(ibs2(i)),
     &              kmax(ibs2(i)),q(lqptr(ibs2(i))),xyj(lgptr(ibs2(i))),
     &              pa,as, icol, gami, precmat, jacmat)

c        -- reset row index for row shuffling --
               do n = 1,4
                  iex(js1(i),n,1,ibs1(i)) = n
                  iex(js2(i),n,3,ibs2(i)) = n
                  bcf(js1(i),n,1,ibs1(i)) = 1.d0
                  bcf(js2(i),n,3,ibs1(i)) = 1.d0
               end do
            end do
         end if  !.not. viscous

c        -- trailing edge (and leading edge) point copies --
         do i=1,ncopy
            call impcop ( ijte1(i), ikte1(i), ijte2(i), ikte2(i),
     &           indx(lgptr(ibte1(i))), indx(lgptr(ibte2(i))), nmax,
     &           xyj(lgptr(ibte1(i))), jmax(ibte1(i)), kmax(ibte1(i)),
     &           xyj(lgptr(ibte2(i))),jmax(ibte2(i)),kmax(ibte2(i)),pa,
     &           as, icol, precmat, jacmat)
         end do
         
      end if   !imps

      return 
      end                       !drdq
