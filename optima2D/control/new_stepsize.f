c---- new_stepsize -----------------------------------------------------
c Computes a second order centred difference using several stepsizes,
c and estimates the truncation error at each stepsize.  (Used to
c computes a good stepsize to use for second-order centred difference
c calculations.  It was found that it is difficult to find region of the
c error-stepsize plot where truncation error dominates, and the error is
c low, due to irregularly shaped plots, so the function now only
c computes at several step sizes).
c
c This function should be used as follows:
c
c     stepsize = 0.d0
c     do while (new_stepsize(A, stepsize, maxstep, minstep))
c       fd(A, stepsize)
c     end do
c
c where fd() is the function that computes some a second-order centred
c difference using the given stepsize, and stores the result in A.
c
c To bypass it, and only run fd() once, set
c
c     stepsize = -1.d0
c
c Requires:
c A: the value computed with finite differences.
c stepsize: set to 0.d0 at the first call to new_stepsize
c
c Returns:
c stepsize: the next stepsize to try
c new_stepsize: True if another finite difference, using the given
c   stepsize, is requested.  False when a suitable stepsize has been
c   chosen, and the finite difference has been computed using that
c   stepsize.
c
c Chad Oldfield, October 2005
c-----------------------------------------------------------------------
      logical function new_stepsize(A, stepsize, maxstep, minstep)
      implicit none

c     Argument data types: see above for descriptions
      double precision A, stepsize, err, maxstep, minstep

c     Local variables
      integer iter ! iteration number
      double precision Ap, App, err_p, err_pp, slope, slope_p
c         Ap, App: previous two values of A
c         err, err_p, err_pp: error estimates for A, Ap, App
c         slope, slope_p: the slope of the log-log plot of error
      logical found_linear
c         true when the linear portion of the error plot has been found
      save iter, Ap, App, err_p, err_pp, slope_p, found_linear

c     Bypass the stepsize solution if stepsize is -1
      if (stepsize == -1.d0) then
c       This is the first time new_stepsize has been called: run
c       through the loop once.
        new_stepsize = .true.
        stepsize = -2.d0
        return
      elseif (stepsize == -2.d0) then
c       This is the second time new_stepsize has been called: exit.
        new_stepsize = .false.
        return
      endif

c     Startup
      if (stepsize == 0.d0) then
        write(*,*) 'Finite difference stepsize selection...'
        write(*,*) ' Stepsize    FD                     error'
        iter = -1
        found_linear = .false.
        stepsize = 2.d0 * maxstep ! To get error estimate at maxstep
        new_stepsize = .true.
        return
      endif

c     Generate and use the error plot to determine whether further
c     iterations are necessary.
      new_stepsize = .true.
      if (iter .ge. 1) then
c       Estimate the error, using a 4th order finite difference given
c       by: A(4) = (4.d0*A - Ap)/3.d0
        err = abs((A - Ap)/3.d0)

c       Write results to the screen
        write(*,'(1X,D9.2,D25.15,D10.2)') stepsize, A, err

c       if (iter .ge. 2) then
c         slope = (log10(err) - log10(err_p))
c    |        /(log10(stepsize) - log10(stepsize*2.d0))
c         if (iter .ge. 3) then
c           if ((abs(1.d0 - slope_p*0.5d0) < 0.3)
c    |          .and. (abs(1.d0 - slope/slope_p) < 0.1)) then
c             The linear portion of the error plot has been found
c             found_linear = .true.
c           elseif (found_linear) then
c             Now exiting the linear portion of the error plot
c             new_stepsize = .false.
c             stepsize = 2.d0*stepsize
c             A = Ap
c             err = err_p
c           endif
c         endif
c       endif
      endif

c     If the the next stepsize to be chosen is less than the minimum
c     allowable, then abort.
      if (new_stepsize .and. (stepsize < 2.d0*minstep)) then
        new_stepsize = .false.
        if (iter == 0) then
          write (*,*) 'Aborting new_stepsize before error estimate',
     |        ' could be completed'
          err = abs(A)
        elseif (iter == 1) then
          write (*,*) 'Aborting new_stepsize at first iteration'
        elseif (iter == 2) then
          write (*,*) 'Aborting new_stepsize at second iteration'
          write (*,'(1X,A,D11.3)') 'Slope = ', slope
          if (err > err_p) then
            stepsize = 2.d0*stepsize
            A = Ap
            err = err_p
          endif
        else
          write (*,'(1X,A,i4)') 'Aborting new_stepsize at iteration ',
     |        iter
          if (err_pp > err_p) then
            if (err > err_p) then
              stepsize = 2.d0*stepsize
              A = Ap
              err = err_p
            endif
          else
            if (err > err_pp) then
              stepsize = 4.d0*stepsize
              A = App
              err = err_pp
            endif
          endif
        endif
      endif

      if (new_stepsize) then
c       Iterate
        App = Ap
        Ap = A
        err_pp = err_p
        err_p = err
        slope_p = slope
        stepsize = stepsize * 0.5d0
        iter = iter + 1
      else
        write(*,'(1X,A,D9.2)') 'Selected stepsize: ', stepsize
      endif

      end
c---- new_stepsize -----------------------------------------------------

c---- new_stepsize_OLD -------------------------------------------------
c Computes a good stepsize to use for second-order centred difference
c calculations.  This function should be used as follows:
c
c     stepsize = 0.d0
c     do while (new_stepsize(A, stepsize, error))
c       fd(A, stepsize)
c     end do
c
c where fd() is the function that computes some a second-order centred
c difference using the given stepsize, and stores the result in A.  In
c fd(), the r8p_module must be used to estimate rounding errors.  So,
c the first line in fd() must be:
c
c     use r8p_module
c
c and when the centred difference is computed, the values at x +/-
c stepsize (say f_plus and f_minus) must be converted to type(r8p):
c
c     A = (r8p_convert(f_plus) - r8p_convert(f_minus)) / (2.d0*stepsize)
c
c Requires:
c A: the value computed with finite differences.
c stepsize: set to 0.d0 at the first call to new_stepsize
c
c Returns:
c stepsize: the next stepsize to try
c error: an error estimate for the final finite difference.
c new_stepsize: True if another finite difference, using the given
c   stepsize, is requested.  False when a suitable stepsize has been
c   chosen, and the finite difference has been computed using that
c   stepsize.
c
c Chad Oldfield, October 2005
c-----------------------------------------------------------------------
c      logical function new_stepsize_OLD(A, stepsize, error)
c      use r8p_module
c      implicit none
c
cc     Argument data types: see above for descriptions
c      type(r8p) A
c      double precision stepsize, error
c
cc     Local variables
c      integer steppower, minpower, maxpower, bestpower
cc         stepsize = 10.d0**steppower.
cc         minpower and maxpower are the minimum and maximum values of
cc         steppower that are tried.
cc         bestpower is the best one.
c      parameter(minpower = -7, maxpower = -4)
c      type(r8p) Ahis(minpower:maxpower)
cc         Ahis: a record of A for each stepsize.
c      double precision A_4, errors(minpower:maxpower)
cc         A_4: a fourth-order finite difference
cc         errors: the error estimates for each stepsize.
c      logical first  ! Two finite differences at each steppower.
cc         The first one is taken at stepsize = 10.d0**steppower.
cc         The second is taken at twice that stepsize, so that a fourth
cc         order centred difference can be constructed, and the
cc         truncation error can be estimated.  first = true when the
cc         most recent value of A was calculated at the first stepsize.
c      save steppower, Ahis, errors, first
c
c      if (stepsize == 0.d0) then
cc       Initialize
c        steppower = maxpower
c        stepsize = 10.d0**steppower
c        first = .true.
c        new_stepsize_OLD = .true.
c      elseif (steppower >= minpower) then
c        if (first) then
cc         Save this value of A, then compute A at twice the stepsize, so
cc         that a 4th order centred difference can be constructed.
c          Ahis(steppower) = A
c          stepsize = 2.d0*10.d0**steppower
c          first = .false.
c          new_stepsize_OLD = .true.
c        else
cc         Estimate the error, using a 4th order finite difference
c          A_4 = (4.d0*Ahis(steppower) - A)/3.d0
c          errors(steppower) = abs(A_4 - Ahis(steppower))
c     |        + get_error(Ahis(steppower))
c          
cc         Go to the next stepsize
c          steppower = steppower - 1
c          stepsize = 10.d0**steppower
c          first = .true.
c          new_stepsize_OLD = .true.
c        endif
c
c        if (steppower < minpower) then
cc         All of the stepsizes have been computed: choose the best one
cc         based on minimum error
c          error = errors(maxpower)
c          bestpower = maxpower
c          do steppower = maxpower-1, minpower, -1
c            if (errors(steppower) < error
c     |          .and. errors(steppower) .ne. 0.d0) then
c              error = errors(steppower)
c              bestpower = steppower
c            endif
c          enddo
c          stepsize = 10.d0**bestpower
c          A = Ahis(bestpower)
c
cc         Write results to the screen
c          write(*,*) 'Finite difference stepsize selection...'
c          write(*,*) ' Stepsize    FD                     error'
c          do steppower = maxpower, minpower, -1
c            if (steppower == bestpower) then
c              write(*,'(1X,D9.2,D25.15,D10.2,A)') 10.d0**steppower,
c     |            Ahis(steppower)%value, errors(steppower), ' <---'
c            else
c              write(*,'(1X,D9.2,D25.15,D10.2)') 10.d0**steppower,
c     |            Ahis(steppower)%value, errors(steppower)
c            endif
c          enddo
c
cc         Stop iterating
c          new_stepsize_OLD = .false.
c        endif
c      endif
c      end
c---- new_stepsize_OLD -------------------------------------------------
