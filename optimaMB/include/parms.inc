C      Parameter File

	integer maxj, maxk, maxjk, maxjkaa, maxjkq, maxblk, jacb 
	integer mxbfine,maxjb, maxkb, maxjkb, maxfoil, maxjfoil
	integer jacb2, maxpen, pmax 
        complex*16 ci
        integer tdim,sdim
c
c     FWH parameters
c
      parameter ( tdim=800, sdim=600, ci=(0.d0,1.d0) )

C     -note: maxpen must be looked at later !!!!!
      parameter (maxpen=1)
C
C     Parameter statements for multi-block code
C     -maxj and maxk give max for entire grid (320,000 pts)
c     -Note: maxj is used for workspace so j should never exceed maxj
c     -- maxj must be greater than the number of nodes on the surface 
c     of each airfoil => used in maxjfoil --

      parameter (maxj=280, maxk=140)
      parameter (maxjk=maxj*maxk)
      parameter (maxjkaa=(maxj+1)*(maxk+1))
      parameter (maxjkq=4*maxjk)
      parameter (maxblk=32,pmax=1)

c     -- mxbfine: maximum number of blocks on the finest grid i.e. top
c     level only for grid sequences and multigrid --
      parameter (mxbfine=32)
c
c     maximum for a single block   (52,500 pts)   
      parameter (maxjb=210, maxkb=80)
      parameter (maxjkb=maxjb*maxkb)
c
c     maximum number of elements  
c      parameter (maxfoil=1,maxjfoil=maxj*maxfoil)
c      parameter (maxfoil=2,maxjfoil=maxj*maxfoil)
      parameter (maxfoil=3,maxjfoil=maxj*maxfoil)
c     
c     -- sparse matrix jacobian size --
      parameter (jacb=5, jacb2=jacb*jacb)
