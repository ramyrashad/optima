      subroutine iomarkus(junit,timeindex,jdim,kdim,q,xyj,turmu)                           

#include "../include/arcom.inc"
#include "../include/optcom.inc"
     

      integer junit,timeindex,jdim,kdim
      dimension q(jdim,kdim,4),xyj(jdim,kdim),turmu(jdim,kdim)                       
      double precision retem                                                              
      integer strlen,namelen,numberlen
      integer jtmp,ktmp,jtmp2

      character  number*4

c     junit=1 write output_file
c     junit=2 write target_file
c     junit=3 read output_file
c     junit=4 read target_file

c     -- store a binary restart file --
      if(periodic .and. jmax.ne.jmaxold .and. junit .le.2) then                            
c     special logic for overlap o grid case                          
c     set jmaxold point from j = 1 point                             
        do k = 1,kmax                                             
c     note jacobian scaling ok                                    
          q(jmaxold,k,1) = q(1,k,1)                                   
          q(jmaxold,k,2) = q(1,k,2)                                   
          q(jmaxold,k,3) = q(1,k,3)                                   
          q(jmaxold,k,4) = q(1,k,4)                                   
          xyj(jmaxold,k) = xyj(1,k) 
          turre(jmaxold,k)= turre(1,k)
          turmu(jmaxold,k)= turmu(1,k)
        end do
      endif 

      call i_to_s(timeindex,number) 

      if (junit .eq. 1 .or. junit .eq. 3) then

         namelen = strlen(output_file_prefix)
         filena=output_file_prefix

      else if (junit.eq.2 .or. junit .eq. 4) then

         namelen = 3
         filena='tar'

      end if
    
      numberlen = len(number)
      
      filena(namelen+1:namelen+numberlen) = number

      filena(namelen+numberlen+1:namelen+numberlen+2) ='.q'

      open(unit=88,file=filena,status='unknown',form='unformatted')

      if (junit.eq.1 .or. junit .eq. 2) then

         write(88) jmaxold,kmax                                             
         write(88) fsmach,alpha,re*fsmach,totime                           
         write(88) (((q(j,k,n)*xyj(j,k),j=1,jmaxold) ,k=1,kmax) ,n=1,4)
         write(88) jtail1, numiter   

         if (itmodel.eq.2 .and. turbulnt .and. junit.eq.1) then
            write(88) ((turre(j,k), j=1,jmaxold) ,k=1,kmax)
            write(88) ((turmu(j,k), j=1,jmaxold) ,k=1,kmax)
         endif

      else if (junit.eq.3 .or. junit .eq. 4) then
c     q contains the physical quantities (not scaled with J=xyj)

         read(88) jtmp,ktmp   
         read(88) fsmach,alpha,retem,totime                           
         read(88) (((q(j,k,n),j=1,jtmp) ,k=1,ktmp) ,n=1,4)
         read(88) jtmp2, numiter 

         if (itmodel.eq.2 .and. turbulnt .and. junit.eq.3) then
            read(88,end=420) ((turre(j,k), j=1,jtmp) ,k=1,ktmp)
            read(88,end=420) ((turmu(j,k), j=1,jtmp) ,k=1,ktmp)
         endif

         goto 430

 420     do k=1,ktmp
            do j=1,jtmp
               turre(j,k)=retinf
               turmu(j,k)=0.d0
            enddo
         enddo
         print *, 'WARNING!!!'
         print *, 'Restart data for turbulence model not available.'
   
 430     re=retem/fsmach
      endif     
      
      close (88)                                                             
                                                         
      return                                                            
      end 

      subroutine i_to_s(intval,s)

      integer idig,intval,ipos,ival,i
      character ( len = * ) s

      s = ' '
  
      ival = intval

c  Working from right to left, strip off the digits of the integer
c  and place them into S(1:len ( s )).
c
      ipos = len(s) 

      do while ( ival /= 0 )

         idig = mod( ival, 10 )
         ival = ival / 10
         s(ipos:ipos) = char(idig + 48 )
         ipos = ipos - 1

      end do
c
c  Fill the empties with zeroes.
c
      do i = 1, ipos
         s(i:i) = '0'
      end do
 
      return
      end                                                              
