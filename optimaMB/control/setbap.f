c----------------------------------------------------------------------
c     -- get airfoil coordinates from grid --
c     -- m. nemec, june 2001 --
c----------------------------------------------------------------------
      subroutine setbap(x, y)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

c -- new common block for blunt TE --
#include "../include/bteinfo.inc"

      double precision x(maxjk), y(maxjk)

c     -- local variables --
      integer ifoil, ipoints,iseg,ib,is,ips,i,nt
      double precision ymid,dist,dist2,dx,dy,ang,xloc,xloc2,ctmp,thc
      double precision xte1,yte1
      double precision wk(ibody,2), xtmp(ibody,maxfoil), xmid(maxfoil)
      double precision xle(maxfoil), yle(maxfoil), xle2(maxfoil)
      
c     -- initialize bte,ite
      do ifoil=1,nfoils
         bte(ifoil) = .false.
         ite(ifoil) = 1
      enddo

c     -- initialize bap --
      do ifoil = 1,maxfoil
         do is = 1,2
            do ib = 1,ibody
               bap(ib,is,ifoil)  = 0.0
               bapo(ib,is,ifoil) = 0.0
            end do
         end do
      end do
c
      do ifoil=1,nfoils
         ipoints = 1
c
c     -- extract the airfoil coordinates from grid data --
         do iseg=1,nsegments(ifoil)
            ib = isegblk(iseg,ifoil)
            is = isegside(iseg,ifoil)
            call getxy(ipoints, jmax(ib), kmax(ib), jminbnd(ib),
     &           jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), is,
     &           x(lgptr(ib)), y(lgptr(ib)), wk, ibody)
c     -- detect blunt trailing edge --
c     -- and remember the index of lower corner of BTE --
            if (is.eq.2) then
               bte(ifoil) = .true.
               ite(ifoil) = ipoints - 1
            endif

         end do
c
         ipoints = ipoints - 1
         if (ibap(ifoil) .ne. ipoints) then
            write (*,*) 'setbap: number of points on airfoil ', ifoil,
     &           ' does not match the .bsp file', ibap(ifoil), ipoints
           stop
         end if
c         
         do is = 1, ipoints
            bap(is,1,ifoil) = wk(is,1)
            bap(is,2,ifoil) = wk(is,2)

c     -- store original airfoil points --
            bapo(is,1,ifoil) = wk(is,1)
            bapo(is,2,ifoil) = wk(is,2)
         end do

      end do

c     ------------------------------------------------------------------
c     -- setup constraints: thickness, gap and overlap --

c     -- store leading edge index --
      do ifoil = 1,nfoils
c     -- trailing edge --
         ips = ibap(ifoil)
         xmid(ifoil) = 0.5*(bap(1,1,ifoil) + bap(ips,1,ifoil))
         ymid = 0.5*(bap(1,2,ifoil) + bap(ips,2,ifoil))
         dist = 0.0

         do i=2,ips-1
            dist2 = sqrt( (bap(i,1,ifoil)-xmid(ifoil))**2 +
     &           (bap(i,2,ifoil)-ymid)**2 )
            if (dist2.gt.dist) then
               dist = dist2
               xle(ifoil) = bap(i,1,ifoil)
               yle(ifoil) = bap(i,2,ifoil)
               ile(ifoil) = i
            end if
         end do

c     -- find angle between chord and horizontal (T.E. is origin) --
         dx = xle(ifoil)-xmid(ifoil)
         dy = yle(ifoil)-ymid
         ang= -atan(dy/dx)

c     -- rotated leading edge --
         xle2(ifoil) = xmid(ifoil) + dx*cos(ang) - dy*sin(ang)

c     -- rotate element --
         do i=2,ips-1
            dx = bap(i,1,ifoil) - xmid(ifoil)
            dy = bap(i,2,ifoil) - ymid
            xtmp(i,ifoil) = xmid(ifoil) + dx*cos(ang) - dy*sin(ang)
         end do
      end do

c     -- thickness constraints --
      do nt = 1,ntcon
         ifoil = itcf(nt)

c     -- looking for closest point on lower surface --
         do i=2,ile(ifoil)
            xloc = abs((xle2(ifoil) - xtmp(i,ifoil))/(xle2(ifoil) -
     &           xmid(ifoil)))
            if ( xloc .le. ctx(nt) ) then
               jlotx(nt) = i
               ctmp = xloc
               goto 10
            end if
         end do
 10      continue

c     -- check immediate neighbour -- 
         xloc2 = (xle2(ifoil)-xtmp(i-1,ifoil))/(xle2(ifoil)-xmid(ifoil))

         if ( abs(xloc2 - ctx(nt)) .lt. abs(xloc - ctx(nt)) ) then
            jlotx(nt) = jlotx(nt)-1
            ctmp = abs(xloc2)
         end if

c     -- change the location of thickness constraint such that the
c     point on the upper surface is the closest point to the lower
c     surface location -- 

         ctx(nt) = ctmp

c     -- looking for closest point on upper surface --
         ips = ibap(ifoil)
         do i=ips-1,ile(ifoil),-1
            xloc = abs((xle2(ifoil) - xtmp(i,ifoil))/(xle2(ifoil) -
     &           xmid(ifoil)))
            if ( xloc .le. ctx(nt) ) then
               juptx(nt) = i
               goto 20
            end if
         end do
 20      continue

c     -- check immediate neighbour -- 
         if ( abs( (xle2(ifoil) - xtmp(i+1,ifoil))/(xle2(ifoil) -
     &        xmid(ifoil)) - ctx(nt) ) .lt. abs( xloc - ctx(nt)) )
     &        juptx(nt) = juptx(nt)+1
      end do

      write (n_out,200) ntcon, ngapc, novc
 200  format (3x,'Number of thickness constraints:',i3,/3x,
     &     'Number of gap constraints:',i3,/3x,
     &     'Number of overlap constraints:',i3/)
     
      if (ntcon .gt. 0) then
         write (n_out,220)
 220     format (/3x,'Cons.',3x,'Element',3x,'X-Coord',3x,'L.S. J#',
     &        3x, 'U.S. J#',3x,'Min. Thickness',3x,'Init. Thickness')
     
         do i = 1,ntcon
            ifoil = itcf(i)
            thc   = sqrt( (bap(juptx(i),1,ifoil) -
     &           bap(jlotx(i),1,ifoil))**2 + (bap(juptx(i),2,ifoil) -
     &           bap(jlotx(i),2,ifoil))**2)
            write (n_out,240) i, itcf(i), ctx(i), jlotx(i), juptx(i),
     &           cthtar(i), thc
         end do
 240     format (3x,i3,7x,i3,5x,f7.3,3x,i4,6x,i4,8x,f7.4,10x,f7.4)
         write (n_out,*)
      end if

      if (ngapc.ne.0 .or. novc.ne.0) then
         do ifoil = 2, nfoils
            xte1 = bap(1,1,ifoil-1)
            yte1 = bap(1,2,ifoil-1)
            gapd(ifoil) = 1.0
            do i = ile(ifoil),ibap(ifoil)
               dist = sqrt( (bap(i,2,ifoil) - yte1)**2 + 
     &              (bap(i,1,ifoil) - xte1)**2 )
               if (dist.lt.gapd(ifoil)) gapd(ifoil) = dist
            end do
            ovd(ifoil) = bap(ile(ifoil),1,ifoil) - xte1
         end do
      end if

      if (ngapc .gt. 0) then
         write (n_out,250)
 250     format (3x,'Cons.',3x,'Element',3x,'Gap Lower Bound',7x,
     &        'Gap Upper Bound',7x,'Initial Gap')
     
         do i = 1,ngapc
            write (n_out,260) i, ifgapc(i), gapl(i), gapu(i), 
     &           gapd(ifgapc(i))
         end do
 260     format (3x,i3,7x,i3,10x,f7.4,15x,f7.4,9x,e12.4)
         write (n_out,*)
      end if

      if (novc .gt. 0) then
         write (n_out,270)
 270     format (3x,'Cons.',3x,'Element',3x,'Overlap Lower Bound',3x,
     &        'Overlap Upper Bound',3x,'Initial Overlap')
     
         do i = 1,novc
            write (n_out,280) i, ifovc(i), ovl(i), ovu(i),
     &           ovd(ifovc(i))
         end do
 280     format (3x,i3,7x,i3,10x,f7.4,15x,f7.4,9x,e12.4)
         write (n_out,*)
      end if

c     write (opt_unit,260) wfd, wfl, wtc
c     260     format (/3x,'Weights WFD, WFL, and WTC = ',3f6.2/)
c     end if

      return
      end                       !setbap
