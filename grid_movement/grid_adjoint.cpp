#include <vector>
#include <cmath>
#include "comprow_double.h"
#include "ilupre_double.h"
#include "cg.h"
#include "Node.h"
#include "Face.h"
#include "gridfe.h"
#include "stopwatch.h"

double sqr(const double& a) {return a*a;}  // a^2

void init_grid (const int& jmax, const int& kmax, const int& jtail1,
    const int& jtail2, double* x, double* y, vector<Node>& nodes,
    int& moving_bnd_nodes, vector<Node*>& bnd_nodes,
    vector<Face>& faces);

void get_GA_lhs (CompRow_Mat_double& lhs, const vector <Node>& nodes,
    const vector <Face>& faces, const double *elemQo, double& meshQ);

void get_GA_rhs (MV_Vector_double& rhs, const MV_Vector_double& lambda,
    vector <Node>& nodes, const vector <Face>& faces, const
    double *elemQo, const int& num_inn_nodes, const int&
    moving_bnd_nodes, const int& jtail1, const int& jtail2, double* ddA,
    const double& incr_fraction);

void get_dEdG (double dEdG[4][2], double& E, const double Ge[4][2],
    const double& PhiTilde0);

void get_dKedGe (double dKedGe[8], double Ke[8][8], const double
    Ge[4][2], const double pre[8], const double post[8], const double&
    PhiTilde0, const vector<vector<vector<double> > >& dN, const double
    C[3][3]);

void add_to_ddA (const vector <Node>& nodes, const vector <Face>& faces,
    const MV_Vector_double& lambda, const int& jtail1,
    const int& jtail2, const int& moving_bnd_nodes,
    const double *elemQo, double& meshQ, double* ddA, const double&
    incr_fraction);

void restore_grid(vector<Node>& nodes, const double* x0, const double*
    y0, const double* dx, const double* dy, const int& incr);

extern "C" {
  void gridadjoint_(const int& jmax, const int& kmax, const int& jtail1,
      const int& jtail2, const int& incr, double* x0, double* y0,
      double* dx, double* dy, double* ddG, double* ddA, double* lambda_prev,
      const double& gatol, const int& gamaxit);
}

/* init_grid -----------------------------------------------------------
Initializes the node and face locations to match x, y.

Based on read_grid in gridfe.cpp.

Requires:
jmax, kmax: grid dimensions
jtail1, jtail2: (FORTRAN) indices of the beginning and end of the
  airfoil body.
x, y: grid node locations

Returns:
nodes: grid node locations
moving_bnd_nodes: number of boundary nodes that move (i.e. airfoil body
  nodes.
bnd_nodes: pointers to those of nodes that are boundary nodes.
faces: the 2D elements (faces) between the nodes.

Chad Oldfield, October 2005
----------------------------------------------------------------------*/
void init_grid (const int& jmax, const int& kmax, const int& jtail1,
    const int& jtail2, double* x, double* y, vector<Node>& nodes,
    int& moving_bnd_nodes, vector<Node*>& bnd_nodes,
    vector<Face>& faces)
{
  int n(jmax*kmax), j, k;  // number of nodes in grid; indices.

  nodes.resize(n);

  // assign the nodal coordinates into the Node vector
  int index (0);  // j, k index for the 1D array

  moving_bnd_nodes = 0;
  int num_bnd (0);      // marks the number of preceeding boundary nodes
  n = (int) bnd_nodes.size();  // n used as a check later

  // Read x, y into nodes.  Identify boundary, moving boundary, and
  // overlap nodes.
  for (k=0; k<kmax; k++)
    for (j=0; j<jmax; j++)
    {
      nodes[index] = Node(x[index], y[index], index);
      if ( (index%jmax == 0) || ((index+1) % jmax == 0))
      {
        nodes[index].is_surf();
        num_bnd++;
      } else if ((index>=jtail1-1) && (index<=jtail2-1))
      {
        nodes[index].is_surf();
        bnd_nodes[moving_bnd_nodes] = &nodes[index];
        moving_bnd_nodes ++;
        num_bnd ++;
      } else if ((index > jtail2-1) && (index < jmax))
      {
        nodes[index].is_surf();
        num_bnd ++;
        nodes[index].overlaps(&nodes[jmax-j-1]);
      } else if ( k==kmax-1 )
      {
        nodes[index].is_surf();
        num_bnd ++;
      }
      index ++;
    }
  nodes[jtail2-1].overlaps(&nodes[jtail1-1]);
  nodes[jtail2-1].is_surf();
  nodes[jmax-1].overlaps(&nodes[0]);
  nodes[jmax-1].is_surf();

  if (moving_bnd_nodes != n)
    {
      bnd_nodes.resize(moving_bnd_nodes);
      cerr << "Error with size of moving boundary nodes array"
         << n << " moving_bnd_nodes " << moving_bnd_nodes
         << endl;
      exit (9);
    }

  read_connectivity(nodes, jmax, kmax, jtail1, jtail2);

  // Form the faces of element
  faces.resize( (jmax-1)*(kmax-1) );
  form_faces (nodes, faces, jmax, kmax);
}
// init_grid -----------------------------------------------------------

/* get_GA_lhs ----------------------------------------------------------
Computes the left-hand side of a grid adjoint (GA) equation.  This is
just -K, where K is the stiffness matrix.

Requires:
node: grid node locations
faces: the 2D elements (faces) between the nodes.
elemQo: ??
meshQ: ??

Returns:
lhs: the left-hand side of the grid adjoint equation.

Chad Oldfield, October 2005
----------------------------------------------------------------------*/
void get_GA_lhs (CompRow_Mat_double& lhs, const vector <Node>& nodes,
    const vector <Face>& faces, const double *elemQo, double& meshQ)
{
  ini_stiff (lhs, nodes, faces);

  int i, j, k, jx2, kx2;  // counter
  int* ind;

  double **K = new double*[8];  // element stiffness matrix
  for (i=0; i<8; i++)
    K[i] = new double [8];
  vector < double*> vertices_coord (4);  // 4 corners of the face

  int row, column;
  int size = (int) faces.size();

  for (i=0; i<size; i++)
  {
    ind = faces[i].get_vertexID();
    for (j=0; j<4; j++)
      vertices_coord[j] = nodes[ind[j]].get_xyz();

    get_K (vertices_coord, K, elemQo[i], meshQ);
    for (j=0; j<4; j++)
      if (!nodes[ind[j]].if_surf())
      {
	jx2 = j*2;
        row = nodes[ind[j]].get_ind();
        for (k=0; k<4; k++)
          if (!nodes[ind[k]].if_surf())
          {
	    kx2 = k*2;
            column = nodes[ind[k]].get_ind();
            lhs.set(row  , column  ) += K[jx2  ][kx2  ];
            lhs.set(row  , column+1) += K[jx2  ][kx2+1];
            lhs.set(row+1, column  ) += K[jx2+1][kx2  ];
            lhs.set(row+1, column+1) += K[jx2+1][kx2+1];
          }
      }
  }

  for (i=0; i<8; i++)
    delete [] K[i];
  delete [] K;
}
// get_GA_lhs ----------------------------------------------------------

/* get_GA_rhs ----------------------------------------------------------
Computes the right-hand side of a grid adjoint (GA) equation, for all
but the last increment of the mesh perturbation.
  -lambda^(i+1) dr^(i+1)/dG^(i)

This also adds the term
  lambda^(i+1) dr^(i+1)/dA^(i)
to the vector ddA, to be passed to the FORTRAN code.  Here, the subscripts
^(...) indicate which increment of the grid pertubation the quantity is
for, r is the grid perturbation residul F-Ku, and A are the airfoil
boundary node locations.

Requires:
lambda: adjoint vector for next mesh perturbation increment (previous
  adjoint solution).
node: grid node locations
faces: the 2D elements (faces) between the nodes.
elemQo: Quality of the elements, for the original mesh
jtail1, jtail2: (FORTRAN) indices of the beginning and end of the
  airfoil body.
ddA: Derivatives with respect to Ga, to be passed back to FORTRAN code.
incr_fraction: i/n, where i is the current increment of the
  perturbation, 1 <= i <= n, and n is the total number of increments.

Returns:
rhs: the right-hand side of the grid adjoint equation.

Chad Oldfield, December 2005
----------------------------------------------------------------------*/
void get_GA_rhs (MV_Vector_double& rhs, const MV_Vector_double& lambda,
    vector <Node>& nodes, const vector <Face>& faces, const
    double *elemQo, const int& num_inn_nodes, const int&
    moving_bnd_nodes, const int& jtail1, const int& jtail2, double* ddA,
    const double& incr_fraction)
{
  int i, j, k;

  // Initialize rhs
  for (i=0; i<num_inn_nodes; i++)
    rhs[i] = 0.0;

  // form dN at each integration (Gauss quadrature) point.  The 3
  // indices are for:
  // [gauss point index][xi/eta direction][corner point index]
  vector<vector<vector<double> > >
      dN(4, vector<vector<double> >(2, vector<double>(4,0.0)));
  form_dN (dN[0], dN[1], dN[2], dN[3]);

  double C[3][3];  // Element Elasticity matrix
  double temp = 1./((1.+poisson)*(1.-2.*poisson));
  C[0][0] = temp*(1. - poisson);
  C[0][1] = temp*(poisson);
  C[0][2] = 0.;
  C[1][0] = C[0][1];
  C[1][1] = C[0][0];
  C[1][2] = 0.;
  C[2][0] = 0.;
  C[2][1] = 0.;
  C[2][2] = temp*(0.5 - poisson);


  int* ind;
  int size = (int) faces.size();
  Node* node(NULL);

  double Ke[8][8];  // Element stiffnes matrix
  double pre[8], post[8];  // dKe/dGe is a third-order tensor.  So, when
      // computing it, it is pre-multiplied by the row vector pre, and
      // post-multiplied by the column vector post.  This gives a result
      // that is just a vector, which is much more manageable.  pre is
      // typically lambda, and post is typically u.
  double dKedGe[8];  // pre * dKe/dGe * post
  double pre_Ke[8];  // pre * Ke
  double Ge[4][2];  // Grid node locations for the element
      // [node index][x/y dimension]

  for (i=0; i<size; i++)
  {
    ind = faces[i].get_vertexID();  // Indices of the face corner nodes.

    // Initialize pre and post, for calculation of dKedGe
    for (j = 0; j < 4; j++)
    {
      node = &nodes[ind[j]];
      if (!node->if_surf())
      {
        // These give rise to lambda * dK/dG * u
        pre[2*j  ] = lambda[node->get_ind()  ];
        pre[2*j+1] = lambda[node->get_ind()+1];
        post[2*j  ] = node->get_coord()[0] - node->get_xyz()[0];  // = u
        post[2*j+1] = node->get_coord()[1] - node->get_xyz()[1];  // = u
      } else
      {
        // These give rise to lambda * dF/dG
        // (= lambda * dKe/dGe * (Ae-Ge))
        pre[2*j  ] = 0.;  // Boundary rows of K are discarded.
        pre[2*j+1] = 0.;  // Boundary rows of K are discarded.
        post[2*j  ] = node->get_coord()[0] - node->get_xyz()[0];  // = A-G
        post[2*j+1] = node->get_coord()[1] - node->get_xyz()[1];  // = A-G
      }
      Ge[j][0] = node->get_xyz()[0];
      Ge[j][1] = node->get_xyz()[1];
    }

    // Compute pre * dKe/dGe * post, and Ke.
    get_dKedGe(dKedGe, Ke, Ge, pre, post, elemQo[i], dN, C);

//  // hack
//  // Compare dKedGe to what is found using finite differences
//  if (i==jtail1+1)
//  {
//    double meshQ, **Kptr, *xyz, area;
//    Kptr = new double*[8];
//    for (j = 0; j < 8; j++)
//      Kptr[j] = new double[8];
//    xyz = new double[2];
//    double fd0, fd1[8][5];
//    double epsilon[5];
//    epsilon[0] = pow(10.,-6.0);
//    epsilon[1] = pow(10.,-7.0);
//    epsilon[2] = pow(10.,-8.0);
//    epsilon[3] = pow(10.,-9.0);
//    epsilon[4] = pow(10.,-10.0);
//    fd0 = 0.;
//    meshQ = 0.;
//    get_K(nodes, ind, Kptr, elemQo[i], meshQ);
//    for (j = 0; j<8; j++)
//      for (k = 0; k<8; k++)
//        fd0 += pre[j]*Kptr[j][k]*post[k];
//    for (int I = 0; I<8; I++)
//      for (int jepsilon = 0; jepsilon < 5; jepsilon++)
//      {
//        xyz[0] = Ge[I/2][0];
//        xyz[1] = Ge[I/2][1];
//        xyz[I%2] += epsilon[jepsilon];
//        nodes[ind[I/2]].set_xyz(xyz);
//        Ge[I/2][I%2] += epsilon[jepsilon];
//        meshQ = 0.;
//        get_K(nodes, ind, Kptr, elemQo[i], meshQ);
//        fd1[I][jepsilon] = 0.;
//        for (j = 0; j<8; j++)
//          for (k = 0; k<8; k++)
//            fd1[I][jepsilon] += pre[j]*Kptr[j][k]*post[k];
//        xyz[I%2] -= epsilon[jepsilon];
//        nodes[ind[I/2]].set_xyz(xyz);
//        Ge[I/2][I%2] -= epsilon[jepsilon];
//      }
//    double dEdG[4][2], E;
//    get_dEdG (dEdG, E, Ge, elemQo[i]);
//    cout << "dKe/dGe: comparison to finite difference\n";
//    cout << "FD           Analytic\n";
//    for (j = 0; j<8; j++)
//    {
//      for (int jepsilon = 0; jepsilon<5; jepsilon++)
//        cout << (fd1[j][jepsilon]-fd0)/epsilon[jepsilon] << '\t';
//      cout << dKedGe[j] << '\n';
//    }
//    cout << "E   = " << E << endl;
//    cout << "fd0 = " << fd0 << endl;
//    cout << endl;
//    for (j = 0; j < 8; j++)
//      delete [] Kptr[j];
//    delete [] Kptr;
//    exit (0);
//  }

    // Compute pre_Ke
    for (j = 0; j < 8; j++)
    {
      pre_Ke[j] = 0.;
      for (k = 0; k < 8; k++)
        pre_Ke[j] += pre[k]*Ke[k][j];
    }

    // Patch dKedGe, etc. together to form dr/dG, ddA.
    for (j = 0; j < 4; j++)
    {
      node = &nodes[ind[j]];
      int j2 = 2*j;
      if (node->if_surf())
      {
        if ((ind[j] >= jtail1-1) && (ind[j] <= jtail2-1))
        {
          // Ke and dKedGe contribute to dF/dG, part of dr/dG in the
          // right-hand-side of the adjoint equation.  For these rows in
          // the adjoint system, the left-hand side is just the identity
          // matrix, so these values can simply be copied into lambda.
          // However, this portion of lambda is used only once: when
          // multiplyed by dr/dA to compute ddA.  That portion of dr/dA
          // is also an identity matrix, so these values can simply be
          // copied directly into ddA.
          ddA[ind[j]-jtail1+1                 ] -= incr_fraction *
              (pre_Ke[j2  ] - dKedGe[j2  ]);
          ddA[ind[j]-jtail1+1+moving_bnd_nodes] -= incr_fraction *
              (pre_Ke[j2+1] - dKedGe[j2+1]);

          // Ke contributes to dF/dA, but can't deal with it until
          // lambda is updated.  This is implemented in add_to_ddA.
        }
        else
        {
          // Don't use outer boundaries, as they never change.
        }
      } else
      {
        // Ke and dKedGe contribute to either dF/dG or (dK/dG*u and
        // K*du/dG), depending on whether there is a boundary node in
        // the face.  In either case, the contribution to dr/dG in the
        // right-hand-side of the adjoint equation is the same.
        rhs[node->get_ind()  ] +=
            pre_Ke[j2  ] - dKedGe[j2  ];
        rhs[node->get_ind()+1] +=
            pre_Ke[j2+1] - dKedGe[j2+1];
      }
    }
  }
}
// get_GA_rhs ----------------------------------------------------------

/* get_dEdG ------------------------------------------------------------
Returns the derivative of Young's Modulus, Ee, with respect to the grid
node locations, Ge, for the element.
  
  dEe/dGe

For grid perturbation increment i, this refers to Ee(i) and Ge(i-1).

Requires:
Ge: element corner node locations [node number][x/y dimension]
PhiTilde0: Quality of the element, for the original mesh

Returns:
dEdG: dEe/dGe

Chad Oldfield, December 2005
----------------------------------------------------------------------*/
void get_dEdG (double dEdG[4][2], double& E, const double Ge[4][2],
    const double& PhiTilde0)
{
  int i;

  // Initialize dEdG
  for (i = 0; i < 4; i++)
  {
    dEdG[i][0] = 0.;
    dEdG[i][1] = 0.;
  }

  // Compute element area
  double area = 0.5 * (  (Ge[2][0] - Ge[0][0]) * (Ge[3][1] - Ge[1][1])
                       - (Ge[3][0] - Ge[1][0]) * (Ge[2][1] - Ge[0][1]));
  double temp;

  // Compute coefficient for derivative with respect to E (Young's
  // Modulus)
  double lengths[4][3];  // The lengths of the sides of each of the 4
      // sub-triangles in the quadrilateral.  Indices are:
      // [triangle side][subtriangle index]
  lengths[0][0] = sqrt(sqr(Ge[0][0] - Ge[3][0]) + sqr(Ge[0][1] - Ge[3][1]));
  lengths[0][1] = sqrt(sqr(Ge[0][0] - Ge[1][0]) + sqr(Ge[0][1] - Ge[1][1]));
  lengths[0][2] = sqrt(sqr(Ge[3][0] - Ge[1][0]) + sqr(Ge[3][1] - Ge[1][1]));
  lengths[1][0] = lengths[0][1];
  lengths[1][1] = sqrt(sqr(Ge[1][0] - Ge[2][0]) + sqr(Ge[1][1] - Ge[2][1]));
  lengths[1][2] = sqrt(sqr(Ge[0][0] - Ge[2][0]) + sqr(Ge[0][1] - Ge[2][1]));
  lengths[2][0] = lengths[1][1];
  lengths[2][1] = sqrt(sqr(Ge[2][0] - Ge[3][0]) + sqr(Ge[2][1] - Ge[3][1]));
  lengths[2][2] = lengths[0][2];
  lengths[3][0] = lengths[2][1];
  lengths[3][1] = lengths[0][0];
  lengths[3][2] = lengths[1][2];

  double PhiTilde(0.), PhiTilde_i;
  
  // First compute the derivative of sqr({Phi hat}^i_Delta) for each triangle
  // in the quadrilateral element, and sum them together.
  double t, t0, t1, t2;
  int im1, ip1;
  for (i = 0; i < 4; i++)
  {
    // Indices of the other corners of the triangle.
    if (i == 0)
    {
      im1 = 3;
      ip1 = 1;
    } else if (i == 3)
    {
      im1 = 2;
      ip1 = 0;
    } else
    {
      im1 = i - 1;
      ip1 = i + 1;
    }

    // Some quantities that appear frequently...
    t0 = 1.0/(-lengths[i][0] + lengths[i][1] + lengths[i][2]);
    t1 = 1.0/( lengths[i][0] - lengths[i][1] + lengths[i][2]);
    t2 = 1.0/( lengths[i][0] + lengths[i][1] - lengths[i][2]);

    // Compute the element quality, PhiTilde_i.
    PhiTilde_i = 2.*lengths[i][0]*lengths[i][1]*lengths[i][2]*t0*t1*t2;
    PhiTilde += sqr(PhiTilde_i);

    // Dependancy on Ge through length[i][0]
    t = PhiTilde_i*2.*lengths[i][1]*lengths[i][2]*t0*t1*t2
        *(1.+lengths[i][0]*( t0-t1-t2));
    temp = t*(Ge[i  ][0] - Ge[im1][0])/lengths[i][0];
    dEdG[im1][0] -= temp;
    dEdG[i  ][0] += temp;
    temp = t*(Ge[i  ][1] - Ge[im1][1])/lengths[i][0];
    dEdG[im1][1] -= temp;
    dEdG[i  ][1] += temp;

    // Dependancy on Ge through length[i][1]
    t = PhiTilde_i*2.*lengths[i][0]*lengths[i][2]*t0*t1*t2
        *(1.+lengths[i][1]*(-t0+t1-t2));
    temp = t*(Ge[i  ][0] - Ge[ip1][0])/lengths[i][1];
    dEdG[ip1][0] -= temp;
    dEdG[i  ][0] += temp;
    temp = t*(Ge[i  ][1] - Ge[ip1][1])/lengths[i][1];
    dEdG[ip1][1] -= temp;
    dEdG[i  ][1] += temp;

    // Dependancy on Ge through length[i][2]
    t = PhiTilde_i*2.*lengths[i][0]*lengths[i][1]*t0*t1*t2
        *(1.+lengths[i][2]*(-t0-t1+t2));
    temp = t*(Ge[ip1][0] - Ge[im1][0])/lengths[i][2];
    dEdG[im1][0] -= temp;
    dEdG[ip1][0] += temp;
    temp = t*(Ge[ip1][1] - Ge[im1][1])/lengths[i][2];
    dEdG[im1][1] -= temp;
    dEdG[ip1][1] += temp;
  }
  PhiTilde = sqrt(PhiTilde);

  // dEdG is now d(sum(sqr({Phi hat}^i_Delta)))/dG.
  // Modify this to form the portion of dE/dG that is due to changes in
  // (Phi tilde)^gamma.
  temp = PowerLaw * pow(PhiTilde/PhiTilde0,PowerLaw)
      / (area*sqr(PhiTilde));
  for (i = 0; i < 4; i++)
  {
    dEdG[i][0] *= temp;
    dEdG[i][1] *= temp;
  }

  // To dE/dG, add the portion that is due to changes in area.
  E = pow(PhiTilde/PhiTilde0,PowerLaw) / area;  // Young's Modulus
  temp = -E / area * 0.5;
  dEdG[0][0] -= temp*(Ge[3][1] - Ge[1][1]);
  dEdG[0][1] += temp*(Ge[3][0] - Ge[1][0]);
  dEdG[1][0] += temp*(Ge[2][1] - Ge[0][1]);
  dEdG[1][1] -= temp*(Ge[2][0] - Ge[0][0]);
  dEdG[2][0] += temp*(Ge[3][1] - Ge[1][1]);
  dEdG[2][1] -= temp*(Ge[3][0] - Ge[1][0]);
  dEdG[3][0] -= temp*(Ge[2][1] - Ge[0][1]);
  dEdG[3][1] += temp*(Ge[2][0] - Ge[0][0]);
}
// get_dEdG ------------------------------------------------------------

/* get_dKedGe ----------------------------------------------------------
Returns the derivative of the element stiffness matrix with respect to
the grid node locations.  The derivative is pre-multiplied by the row
vector pre, and post-multiplied by the column vector post:
  
  pre * (dKe/dGe) * post

For grid perturbation increment i, this refers to Ke(i) and Ge(i-1).

The element stiffness matrix, Ke is calculated as well.

Requires:
Ge: element corner node locations [node number][x/y dimension]
pre: row vector that pre-multiplies dKe/dGe
post: column vector that post-multiplies dKe/dGe
PhiTilde0: Quality of the element, for the original mesh
dN: Derivatives of the element shape functions, evaluated for each gauss
  point and corner number.
  [gauss point index][xi/eta direction][corner point index]
C: Element elasticity matrix

Returns:
dKedGe: pre * dKe/dGe * post
Ke: Element stiffness matrix

Chad Oldfield, December 2005
----------------------------------------------------------------------*/
void get_dKedGe (double dKedGe[8], double Ke[8][8], const double
    Ge[4][2], const double pre[8], const double post[8], const double&
    PhiTilde0, const vector<vector<vector<double> > >& dN, const double
    C[3][3])
{
  int igauss, icorner, i, j, k;

  // Initialize
  for (i = 0; i < 8; i++)
  {
    dKedGe[i] = 0.;
    for (j = 0; j < 8; j++)
      Ke[i][j] = 0.;
  }

  // Differentiate E: Young's modulus.
  double dEdG[4][2], E;
  get_dEdG (dEdG, E, Ge, PhiTilde0);

  double J[2][2], Jdet, Jdetinv, dJdetdG[4][2];  // Jacobian, J;
      // determinant of J, Jdet; 1/Jdet; d(Jdet)/dG.

  double B[3][8], CB[3][8], a[8][8];
  double temp1[3], temp2[3], temp;

  // Initialize B, as it contains some zeros not assigned later.
  for (i = 0; i<3; i++)
    for (j = 0; j<8; j++)
      B[i][j] = 0.;

  for (igauss = 0; igauss < 4; igauss++)
  {
    // Calculate J & derivatives
    for (j = 0; j < 2; j++)
      for (i = 0; i < 2; i++)
      {
        J[j][i] = 0.;
        for (icorner = 0; icorner < 4; icorner++)
          J[j][i] += dN[igauss][j][icorner]*Ge[icorner][i];
      }
    Jdet = J[0][0]*J[1][1] - J[0][1]*J[1][0];
    Jdetinv = 1./Jdet;
    for (icorner = 0; icorner < 4; icorner++)
    {
      dJdetdG[icorner][0] =   J[1][1]*dN[igauss][0][icorner]
                            - J[0][1]*dN[igauss][1][icorner];
      dJdetdG[icorner][1] = - J[1][0]*dN[igauss][0][icorner]
                            + J[0][0]*dN[igauss][1][icorner];
    }

    // Calculate B and CB
    for (icorner = 0; icorner < 4; icorner++)
    {
      i = 2*icorner;
      B[2][i  ] = (- J[1][0]*dN[igauss][0][icorner]
                   + J[0][0]*dN[igauss][1][icorner])*Jdetinv;
      B[2][i+1] = (  J[1][1]*dN[igauss][0][icorner]
                   - J[0][1]*dN[igauss][1][icorner])*Jdetinv;
      B[0][i  ] = B[2][i+1];
      B[1][i+1] = B[2][i  ];
    }
    for (icorner = 0; icorner < 4; icorner++)
    {
      // Neglect the many zeros in the matrix multiplication C*B.
      i = 2*icorner;
      CB[0][i] = C[0][0]*B[0][i];
      CB[1][i] = C[1][0]*B[0][i];
      CB[2][i] = C[2][2]*B[2][i];
      i++;
      CB[0][i] = C[0][1]*B[1][i];
      CB[1][i] = C[1][1]*B[1][i];
      CB[2][i] = C[2][2]*B[2][i];
    }

    // Compute the stiffness matrix, Ke (or rather, the portion at this
    // Gauss point).
    // Ke(igauss) = B^T * CB * E
    for (i = 0; i<8; i++)
      for (j = 0; j<3; j++)
        for (k = 0; k<8; k++)
          Ke[i][k] += B[j][i] * E * CB[j][k] * Jdet;  // Could be faster!

    // Multiply out pre * transpose(B) * CB * post, for adding in
    // derivatives of E and Jdet.
    // Could be faster!  Use Ke/E here?
    for (i = 0; i < 3; i++)  // pre * transpose(B) -> temp1
    {
      temp1[i] = 0.;
      for (j = 0; j < 8; j++)
        temp1[i] += pre[j]*B[i][j];  // Could be faster!
    }
    for (j = 0; j < 3; j++)  // CB * post -> temp2
    {
      temp2[j] = 0.;
      for (i = 0; i < 8; i++)
        temp2[j] += CB[j][i]*post[i];
    }
    temp = 0.;
    for (i = 0; i < 3; i++)  // temp1 * temp2 -> temp
      temp += temp1[i]*temp2[i];

    // Add E and Jdet derivatives to dKedGe
    for (i = 0; i < 4; i++)
    {
      dKedGe[2*i  ] += temp*(Jdet*dEdG[i][0] - E*dJdetdG[i][0]);
      dKedGe[2*i+1] += temp*(Jdet*dEdG[i][1] - E*dJdetdG[i][1]);
    }

    // Add B derivatives to dKedGe.
    //
    // These occur in 2 places:
    // 1:  pre * d(B'|J|)/dG * C *  (B|J|)    / |J|
    // 2:  pre *  (B'|J|)    * C * d(B|J|)/dG / |J|
    //
    // First, compute d(B*Jdet)/dG.
    // A slice of d(B*Jdet)[i,j]/dG[k] where k is constant has the
    // following structure:
    //   [ a[1]  0     a[3]  0     a[5]  0     a[7]  0    ]
    //   [ 0     a[0]  0     a[2]  0     a[4]  0     a[6] ]
    //   [ a[0]  a[1]  a[2]  a[3]  a[4]  a[5]  a[6]  a[7] ]
    // So it is possible to store the 8x8x8 tensor d(B*Jdet)[i,j]/dG[k]
    // as an 8x8 array, a[ij][k].
    for (i = 0; i < 8; i++)
      for (j = 0; j < 8; j++)
        a[i][j] = 0.;
    for (icorner = 0; icorner < 4; icorner++)
    {
      i = 2*icorner;
      for (j = 0; j < 4; j++)
        if (j != icorner)  // (That would give temp = 0)
        {
          temp = - dN[igauss][1][j]*dN[igauss][0][icorner]
                 + dN[igauss][0][j]*dN[igauss][1][icorner];
          a[i  ][2*j  ] += temp;
          a[i+1][2*j+1] -= temp;
        }
    }

    // Add the first occurence of d(B|J|)/dG to dKedGe:
    //     pre * d(B'|J|)/dG * E * C *  (B|J|) / |J| * post
    //   = E * pre * d(B'|J|)/dG * CB * post
    //   = E * (     temp1     ) * ( temp2 )
    // temp1 is pre * d(B'|J|)/dG
    // temp2, as before, is CB * post
    for (j = 0; j < 8; j++)
    {
      temp1[0] = temp1[1] = temp1[2] = 0.;
      for (icorner = 0; icorner<4; icorner++)  // Could be faster!
          // (by omitting zeros)
      {
        i = 2*icorner;
        temp1[0] += pre[i  ] * a[i+1][j];
        temp1[1] += pre[i+1] * a[i  ][j];
        temp1[2] += pre[i  ] * a[i  ][j] + pre[i+1] * a[i+1][j];
      }
      for (i = 0; i < 3; i++)  // add to dKedGe
        dKedGe[j] += E*temp1[i]*temp2[i];
    }

    // Add the second occurence of d(B|J|)/dG to dKedGe:
    //     pre * (B'|J|) * E * C * d(B|J|)/dG / |J| * post
    //   = E * pre * B' * C' * d(B|J|)/dG * post
    //   = E * pre * (CB)' * d(B|J|)/dG * post
    //   = E * (  temp1  ) * (     temp2     )
    // temp1 is pre * (CB)'
    // temp2 is d(B|J|)/dG * post
    for (i = 0; i < 3; i++)
    {
      temp1[i] = 0.;
      for (j = 0; j < 8; j++)
        temp1[i] += pre[j]*CB[i][j];
    }
    for (j = 0; j < 8; j++)
    {
      temp2[0] = temp2[1] = temp2[2] = 0.;
      for (icorner = 0; icorner<4; icorner++)  // Could be faster!
          // (by omitting zeros)
      {
        i = 2*icorner;
        temp2[0] += a[i+1][j] * post[i  ];
        temp2[1] += a[i  ][j] * post[i+1];
        temp2[2] += a[i  ][j] * post[i  ] + a[i+1][j] * post[i+1];
      }
      for (i = 0; i < 3; i++)  // add to dKedGe
        dKedGe[j] += E*temp1[i]*temp2[i];
    }
  }
}
// get_dKedGe ----------------------------------------------------------

/* add_to_ddA ----------------------------------------------------------
Calculates the term
  lambda * dF/dGa
and adds it to ddA, so it can be returned to the FORTRAN code for
calculating dL/dX.  This must be called after lambda has been computed.

This neglects the two overlapping nodes at the trailing edge, as they
do not move, and the overlap complicates things.

Requires:
node: grid node locations
faces: the 2D elements (faces) between the nodes.
elemQo: ??
meshQ: ??
incr_fraction: i/n, where i is the current increment of the
  perturbation, 1 <= i <= n, and n is the total number of increments.

Returns:
ddA: Derivatives with respect to Ga, to be passed back to FORTRAN code.

Chad Oldfield, October 2005
----------------------------------------------------------------------*/
void add_to_ddA (const vector <Node>& nodes, const vector <Face>& faces,
    const MV_Vector_double& lambda, const int& jtail1,
    const int& jtail2, const int& moving_bnd_nodes,
    const double *elemQo, double& meshQ, double* ddA, const double&
    incr_fraction)
{
  int iF(jtail1-1);  // Face indices
  int iddA(1);  // ddA indices (neglect trailing edge)
  int iA; // Indices of face corners that touch the airfoil (0-1)
  int iI; // Indices of face corners that are interior nodes (2-3)
  int iL; // Indices for lambda

  int j, k;  // counter
  vector < double*> vertices_coord (4);  // 4 corners of the face

  double **K = new double*[8];  // element stiffness
  for (int i=0; i<8; i++)
    K[i] = new double [8];

  // Get face stiffness for the first face that touches a moving node.
  int* ind(faces[iF].get_vertexID());  // indices of nodes at the
      // corners of face iF.
 
  for (j=0; j<4; j++)
    vertices_coord[j] = nodes[ind[j]].get_xyz();

  get_K(vertices_coord, K, elemQo[iF], meshQ);

  for (j = jtail1; j < jtail2-1; j++)  // neglect trailing edge
  {
    // Two faces touch node j
    for (iF = j-1; iF <= j; iF++)
    {
      if (iF == j-1) // First face that touches j
        iA = 1;  // index in ind that corresponds to node j
      else // Second face that touches j
      {
        ind = faces[iF].get_vertexID();

      for (k=0; k<4; k++)
	vertices_coord[k] = nodes[ind[k]].get_xyz();

        get_K(vertices_coord, K, elemQo[iF], meshQ);
        iA = 0;  // index in ind that corresponds to node j
      }
      for (iI = 2; iI<=3; iI++)  // For each interior node on the face
      {
        iL = nodes[ind[iI]].get_ind();
        ddA[iddA] += incr_fraction *
            (  lambda[iL  ] * K[iI*2  ][iA*2]
             + lambda[iL+1] * K[iI*2+1][iA*2]);
        ddA[iddA+moving_bnd_nodes] += incr_fraction *
            (  lambda[iL  ] * K[iI*2  ][iA*2+1]
             + lambda[iL+1] * K[iI*2+1][iA*2+1]);
      }
    }

    // Increment to next node.
    iddA++;
  }

  for (int i=0; i<8; i++)
    delete [] K[i];
  delete [] K;
}
// add_to_ddA ----------------------------------------------------------

/* restore_grid --------------------------------------------------------
Restores the grid to what was found in the grid perturbation at
increment incr.  So at each node, xyz is the grid immediately before
computing that increment, and coord is the grid immediately after that
increment.

Requires:
x0, y0: The original grid
dx, dy: The computed perturbations to the grid.
incr: The increment number to restore.

Returns:
nodes: Adjusted to the given increment.

Chad Oldfield, January 2006
----------------------------------------------------------------------*/
void restore_grid(vector<Node>& nodes, const double* x0, const double*
    y0, const double* dx, const double* dy, const int& incr)
{
  int i, j;
  int size = (int) nodes.size();
  int incr_size = incr * (int) nodes.size();
  double temp;
  double *G_old = new double[2];
  double *G_new = new double[2];
  for (i = 0; i < size; i++)
  {
    // Find x-coordinates
    temp = x0[i];
    for (j = 0; j < incr_size; j+=size)
      temp += dx[i + j];
    G_old[0] = temp;
    G_new[0] = temp + dx[i + incr_size];

    // Find y-coordinates
    temp = y0[i];
    for (j = 0; j < incr_size; j+=size)
      temp += dy[i + j];
    G_old[1] = temp;
    G_new[1] = temp + dy[i + incr_size];

    // Update node
    nodes[i].set_xyz(G_old);
    nodes[i].set_coord(G_new);
  }
  delete [] G_old;
  delete [] G_new;
}
// restore_grid --------------------------------------------------------

/* grid_adjoint_ -------------------------------------------------------
Computes the portions of the dual adjoint equations that relate to the
finite element grid perturbation scheme.

Requires:
jmax, kmax: grid dimensions
jtail1, jtail2: (FORTRAN) indices of the beginning and end of the
  airfoil body.
incr: The number of increments used in the grid perturbation.
x0, y0: original grid node locations
dx, dy: The change in grid node locations (as found by movegridfe_).
  When incr > 1, these are a series of changes.
ddG: Derivitives with respect to G.  More specifically, in the first
  grid adjoint equation, this is the portion that has to do with the
  sensitivity of the objective function and the flow residual with
  respect to the grid.
gatol: residual tolerance to which the adjoint equations are to be
  solved (try 1.0e-15)
gamaxit: maximum number of conjugate gradient iterations to perform in
  solution of adjoint equation (try 5000)

Returns:
ddA: Derivatives with respect to the airfoil surface.  More
  specifically, in the last adjoint equation (i.e. the calculation of
  dL/dX, the objective function gradient), lamdda * dr/dA.

Chad Oldfield, October 2005
----------------------------------------------------------------------*/
void gridadjoint_(const int& jmax, const int& kmax, const int& jtail1,
    const int& jtail2, const int& incr, double* x0, double* y0,
    double* dx, double* dy, double* ddG, double* ddA, double* lambda_prev,
    const double& gatol, const int& gamaxit)
{
  cout << "\nGrid adjoint" << endl;

  int i, j;   // counter
  double residual, meshQ;
  int iterations;  // Number of conjugate gradient iterations
  double incr_fraction;  // i/n, where i is the current increment of the
      // perturbation, 1 <= i <= n, and n is the total number of
      // increments.

  // declare and initialize objects
  vector <Node> nodes;
  vector <Face> faces;
  int moving_bnd_nodes (jtail2-jtail1+1);
  vector <Node*> bnd_nodes(moving_bnd_nodes);

  // VARIABLES IN SOLVING MATRIX
  // counts the number of nonzeros in the stiffness matrix
  // and number of interior nodes
  int num_inn_nodes(0);
  int nonzeros = 2;  // nonzeros is set to 2 to indicate the
                     // degree of freedom of the problem
                     // it will be used/changed to store the number
                     // of nonzeros in the global stiffness matrix
                     // in the function get_nonzerosfe(...)

  init_grid (jmax, kmax, jtail1, jtail2, x0, y0, nodes,
      moving_bnd_nodes, bnd_nodes, faces);
  int size = (int) nodes.size();

  get_nonzerosfe (nodes, num_inn_nodes, nonzeros);

  double *stiff_val = new double [nonzeros];
  init_array(stiff_val, nonzeros);
  int *colind = new int [nonzeros];
  int *rowptr = new int [num_inn_nodes+1];

  j = 2;   // number of degrees of freedom, dimension of problem
  // construct the stiffness matrix and initialize all elements to be 0.0
  construct_stiffness_matrix (colind, rowptr, nodes, j);
  CompRow_Mat_double lhs (num_inn_nodes, num_inn_nodes, nonzeros,
                      stiff_val, rowptr, colind);

  VECTOR_double lambda(num_inn_nodes, 0.0), rhs(num_inn_nodes, 0.0);

  // initial element quality
  double* elemQo = new double[(int) faces.size()];
  // initial mesh quality
  double Qomax;
  get_ini_quality (faces, nodes, elemQo, Qomax);

  // Initialize ddA
  for (i = 0; i < (2*moving_bnd_nodes); i++)
    ddA[i] = 0.0;

  // Initialize stopwatch
  Stopwatch stopwatch;

  for (j=incr-1; j>=0; j--)
  {
    cout << "  Increment " << j << '\n';
    cout << "    Setting up linear system ... ";
    cout.flush();

    incr_fraction = ((double)(j+1))/((double)incr);

    // Compute the left- and right-hand sides of the adjoint equation.
    if (j == incr-1)
    {
      // Last perturbation of the grid => first adjoint equation.

      // Update grid to the current increment
      restore_grid(nodes, x0, y0, dx, dy, j);

      // Compute the left-hand side of the adjoint equation.
      if (j == 0)  // Do not calculate mesh quality at 1st iteration
        meshQ = -1.;
      else
        meshQ = 0.;
      get_GA_lhs (lhs, nodes, faces, elemQo, meshQ);

      // Copy ddG into the right-hand side of the adjoint equation
      int index = 0;
      for (int k = 0; k < num_inn_nodes; k++)
        rhs[k] = 0.0;
      for (int k = 0; k < size; k++)
      {
        if (!nodes[k].if_surf())
        {
          rhs[index++] -= ddG[k];
          rhs[index++] -= ddG[k+size];
        } else if (nodes[k].get_overlap() != NULL)
        {
          // Wake cut nodes have two overlapping nodes in the flow solver;
          // Add them together to form one wake cut node here.
          Node* node_ptr = nodes[k].get_overlap();
          if (!node_ptr->if_surf())
          {
            rhs[node_ptr->get_ind()  ]
                -= ddG[k];
            rhs[node_ptr->get_ind()+1]
                -= ddG[k+size];
          }
        } else if ((k >= jtail1-1) && (k <= jtail2-1))
        {
          // The solution for lambda at the boundary nodes is trivial:
          // since lhs = I at the boundary nodes, lambda = ddG.  For the
          // outer boundary, these values of lambda are never used, but
          // for the airfoil boundary, these values of lambda are used
          // only when evaluating lambda * dr/dA to give ddA.  Since
          // dr/dA = I at these nodes, ddA is simply ddG here.
          ddA[k-(jtail1-1)] += ddG[k];
          ddA[k-(jtail1-1)+moving_bnd_nodes] += ddG[k+size];
        }
      }
    } else
    {
      // First few perturbations of the grid => last few adjoint
      // equations.

      // Compute the right-hand side of the adjoint equation.  This is
      // caried out starting with the grid that the original
      // perturbation code computes at this increment.
      get_GA_rhs (rhs, lambda, nodes, faces, elemQo, num_inn_nodes,
          moving_bnd_nodes, jtail1, jtail2, ddA, incr_fraction);

      // Update the grid, for calculations at this increment - i.e.
      // starting with the grid that the original perturbation code
      // computed at the previous increment.
      restore_grid(nodes, x0, y0, dx, dy, j);

      // Compute the left-hand side of the adjoint equation.
      if (j==0) // Mesh quality is not considered in the first increment.
        meshQ = -1.;
      else
        meshQ = 0.;
      get_GA_lhs (lhs, nodes, faces, elemQo, meshQ);
    }

    // Warmstart lambda
    for (i = 0; i < size;  i++)
      if (!nodes[i].if_surf())
      {
        lambda[nodes[i].get_ind()] =
            lambda_prev[i + (2*j)*size];
        lambda[nodes[i].get_ind()+1] =
            lambda_prev[i + (2*j+1)*size];
      }

    cout << "Complete in " << stopwatch.Interval() << " seconds\n";
    cout << "    Solving ... ";
    cout.flush();

    // Precondition & solve adjoint equation
    CompRow_ILUPreconditioner_double P(lhs);     // ILU preconditioner
    residual = gatol;
    iterations = gamaxit;
    i = CG(lhs, lambda, rhs, P, iterations, residual);

    cout << "Complete in " << stopwatch.Interval() << " seconds\n";
    cout << "    residual = " << residual << ", " << iterations
         << " iterations\n";
    if (i > 0) cout << "WARNING: final grid was not achieved " 
         << "within specified tolerance!\n";
    cout.flush();

    // Save lambda for warmstartting
    for (i = 0; i < size; i++)
      if (!nodes[i].if_surf())
      {
        lambda_prev[i + (2*j  )*size] =
            lambda[nodes[i].get_ind()  ];
        lambda_prev[i + (2*j+1)*size] =
            lambda[nodes[i].get_ind()+1];
      }

    // Add the resulting term to ddA
    add_to_ddA (nodes, faces, lambda, jtail1, jtail2, moving_bnd_nodes,
        elemQo, meshQ, ddA, incr_fraction);
  }

  delete [] stiff_val;
  delete [] colind;
  delete [] rowptr;
  delete [] elemQo;

  for (j=0; j<size; j++)
    nodes[j].clearmem();
  size = (int) faces.size();
  for (j=0; j<size; j++)
    faces[j].clearmem();

  cout.flush();
  cerr.flush();
  return;
}
// grid_adjoint_ -------------------------------------------------------
