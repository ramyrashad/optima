#ifndef GRID3DFE_H
#include "grid3Dfe.h"
#endif


// function to read a structured grid from typhoonMB
void read_grid3Dfe (const char *filename, vector <Node>& node_loc,
		    vector<Node*>& bnd_nodes, vector <Vol>& cells, 
		    int& nblk, int*& jmax, int*& kmax, int*& mmax) 
{

  int i, k, j, m, nbytes, count, temp;
  const bool ThreeD = true;

  ifstream fin(filename, ios::in | ios::binary);
  if (! fin.is_open())
    {
      cout << "Error opening file gridfile in read grid"; 
      exit(2);
    }
  // Reads the number of blocks in file
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));
  fin.read(reinterpret_cast < char * > (&nblk), sizeof(int));
  ByteSwap5(nblk);
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));

   
  //READING CONNECTIVITY FILE
  j = nblk*6;      // 6 = # faces of block
  int * blk_con = new int [j];  
  int * side_con = new int [j];
  int * bnd  = new int [j];
  int * dir = new int [j];
  int * ifoil = new int [j];

  //cout <<"before read_connectivity " << endl;
  read_connectivity_mb (filename, nblk, blk_con, side_con, bnd, dir, 
			ifoil, ThreeD);
  //   cout << "after cconnectivity " << endl;
  jmax = new int [nblk];
  kmax = new int [nblk];
  mmax = new int [nblk];

  // Reads the jmax, kmax and mmax in each block
  j = 0;
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));
  nbytes = 0;
  for (i=0; i<nblk; i++)
    {
      fin.read(reinterpret_cast < char * > (&jmax[i]), sizeof(int));
      ByteSwap5(jmax[i]);
      fin.read(reinterpret_cast < char * > (&kmax[i]), sizeof(int));
      ByteSwap5(kmax[i]);
      fin.read(reinterpret_cast < char * > (&mmax[i]), sizeof(int));
      ByteSwap5(mmax[i]);
      j += jmax[i]*kmax[i]*mmax[i];
      
      //  cout <<" calculate the number of hexahedra" << endl;
      nbytes += (jmax[i]-1)*(kmax[i]-1)*(mmax[i]-1);
      
      //cout << i << "   " << jmax[i] << "  " << kmax[i] << "  " << mmax[i] << endl;
    }
  cells.resize(nbytes);
  node_loc.resize(j);

  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(int));

  //cout <<"after read in each blk dimensions" << endl;

  /*  ofstream ascii("header.dat");
  ascii << nblk << endl;
  for (i=0; i<nblk; i++)
    ascii << setw(10) << jmax[i] << setw(10) << kmax[i] << setw(10)
	  <<mmax[i]<<endl;
  */
  /****************************************************/
  vector <double> x;
  vector <double> y;
  vector <double> z;
  // double *xyz = new double [3];

  //int debug = 0;
  //  ofstream f1 ("surf1.dat");
  //ofstream f2 ("surf2.dat");
  // Reads the nodal coordinates for each block
  int tot_node = 0;  // keeps track of tot_node

  for (i=0; i<nblk; i++)
    {
      fin.read(reinterpret_cast < char * > (&nbytes), sizeof(int));
      
      nbytes = jmax[i]*kmax[i]*mmax[i];
      x.resize(nbytes);
      y.resize(nbytes);
      z.resize(nbytes);
      
      //cout << " reading in coords of blk " << i << " of " <<nblk << endl;
      
      for (j=0; j<nbytes; j++)
	{
	  fin.read(reinterpret_cast < char * > (&x[j]), sizeof(double));
	  ByteSwap5(x[j]);
	}

      for (j=0; j<nbytes; j++)
	{
	  fin.read(reinterpret_cast < char * > (&y[j]), sizeof(double));
	  ByteSwap5(y[j]);
	}
      for (j=0; j<nbytes; j++)
	{
	  fin.read(reinterpret_cast < char * > (&z[j]), sizeof(double));
	  ByteSwap5(z[j]);
	}

      fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));
      // cout << "blk info " << blk_con[i*6] << "  " << bnd[i*6] << endl;
      count = 0;
      for (m=0; m<mmax[i]; m++)
	{
	  for (k=0; k<kmax[i]; k++)
	    {
	      for (j=0; j<jmax[i]; j++)
		{

		  // xyz[0] = x[count];
		  //xyz[1] = y[count];
		  //xyz[2] = z[count];
		  //node_loc[tot_node] = Node(xyz, tot_node, ThreeD);
		  node_loc[tot_node] = Node(x[count], y[count], z[count],
					    tot_node);

		  if (j==0)  // with new index convention, current_side = 1;
		    {
		      temp = i*6;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, k, m, jmax, 
				       kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(2);
		    } 
		  if (k==0)  // current_side = 3;
		    {
		      temp = i*6 + 2;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, j, m, jmax, 
				       kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(3);
		    } 
		  if (m==0)  // current_side = 5;
		    {
		      temp = i*6 + 4;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, j, k, jmax, 
				       kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(1);
		    }
		  if (j==jmax[i]-1)   // current_side = 2;
		    {
		      temp = i*6 + 1;  // first entry of connectivity arrays
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, k, m, jmax, 
				       kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(2);
		    }
		  if (k==kmax[i]-1)  // current_side = 4;
		    {
		      temp = i*6 + 3;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, j, m, jmax, 
				       kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(3);
		    } 
		  if (m==mmax[i]-1)   //  current_side = 6;
		    {
		      temp = i*6 + 5;
		      mark_boundary3D (node_loc, tot_node, blk_con[temp], 
				       side_con[temp], dir[temp], bnd[temp], 
				       ifoil[temp], i, j, k, jmax, 
				       kmax, mmax);
		      if (bnd[temp]==3)
			node_loc[tot_node].set_symm(1);
		    }
	
		  count ++;
		  tot_node ++;
		}
	    }
	}
    }
  //cout <<"tot_node in read_grd is " << tot_node << " debug is " << debug << endl;


  //  delete [] xyz;         xyz = NULL;
  delete [] blk_con;     blk_con = NULL;
  delete [] side_con;    side_con = NULL;
 
  delete [] bnd;         bnd = NULL;
  delete [] dir;         dir = NULL;
  delete [] ifoil;       ifoil = NULL;
  
  add_neighbours3D (node_loc, jmax, kmax, mmax, nblk);


  //  cout << "form_cells" << endl;
  form_cells (node_loc, nblk, jmax, kmax, mmax, cells);


  /*
  // for Jason Hicken
  ofstream jason("bnd_index.dat");
  temp = 0;  // store cummulative nodes in block
  int yo = 0;
  for (i=0; i<nblk; i++)
    {
      for (m=0; m<mmax[i]; m++)
	{
	  for (k=0; k<kmax[i]; k++)
	    {
	      for (j=0; j<jmax[i]; j++)
		{
		  if (node_loc[yo].get_ifoil() != 0 && node_loc[yo].get_overlap() == NULL)
		    jason << setw(6) << i+1 << setw(6) << j+1 << setw(6) << k+1 << setw(6)
			  << m+1 << endl;
		  yo ++;
		}
	    }
	}
    }
  jason.close();  exit(22);
  */

  
  //  cout << " success forming cells, grid3dfe" << endl;
  /*  delete [] jmax;     jmax = NULL;
  delete [] kmax;     kmax = NULL;
  delete [] mmax;     mmax = NULL;
  */

  count = 0;
  for (i=0; i<tot_node; i++)
    if (node_loc[i].get_ifoil() != 0 && node_loc[i].get_overlap() == NULL)
      count ++;
  bnd_nodes.resize(count);

  //cout<<"put nodes on airfoil/wing into bnd_nodes vector (moving boundary nodes)"
  //      <<endl;

  //  ofstream fout("grid_no_sweep.surf");

  temp = 0;
  for (i=0; i<tot_node; i++)
    {
      if (node_loc[i].get_ifoil() != 0 && node_loc[i].get_overlap() == NULL)
	{
	  bnd_nodes[temp] = &node_loc[i];
	  
	  /*    ccc = node_loc[i].get_coord();
	      fout.setf(ios::fixed);  fout.setf(ios::showpoint);
	      fout.precision(10);
	      fout << setw(15) << ccc[0] << setw(15) << ccc[1] << setw(15) << ccc[2] 
	      << endl;*/
	  temp ++;
	}
    }
  //fout.close();  cout << "done writing out the boundary" << endl;
    //exit(12); 


  //  cout << "exiting read_grid3Dfe " << endl;
  return;
}



void form_cells (vector <Node>& node, const int& nblk, const int* jmax,
		 const int* kmax, const int* mmax, vector <Vol>& cells)
{
  int count (0);  // counts the number of hexahedron
  int temp (0);    // marks the node at which the series of nodes in the 
                   // cell begin
  int i, j , k, m, jk;   // counter

  /*
  for (i=0; i<node.size(); i++)
    cout << node[i];
  cout << "done listing nodes " << node.size() << endl;
  */

   Node ** corners = new Node* [8];
  int* vertexID;

    // assume elements are arranged counterclockwise
  //  bool counterclockwise = true;  

  //ofstream fout ("cells.dat" );

  for (i=0; i<nblk; i++)
    {
      jk = jmax[i]*kmax[i];
      
      //cout << i << "  " << jmax[i] << "  " << kmax[i] << "  " << mmax[i] << endl;
      /*
      // determine the element orientation in each block
      corners[0] = &node[temp];
      corners[1] = &node[temp+1];
      corners[2] = &node[temp+jmax[i]+1];
       corners[4] = &node[temp+jk];
      check_numbering (corners, counterclockwise);
      //  cout << i << " counterclockwise is " << counterclockwise << endl;
      */
      for (m=0; m<mmax[i]-1; m++)
	{
	  for (k=0; k<kmax[i]-1; k++)
	    {
	      for (j=0; j<jmax[i]-1; j++)
		{
		  /*switch (counterclockwise)
		    {
		    case true:
		      corners[0] = &node[temp];
		      corners[1] = &node[temp+1];
		      corners[2] = &node[temp+jmax[i]+1];
		      corners[3] = &node[temp+jmax[i]];
		      
		      corners[4] = &node[temp+jk];
		      corners[5] = &node[temp+jk+1];
		      corners[6] = &node[temp+jk+jmax[i]+1];
		      corners[7] = &node[temp+jk+jmax[i]];
		      break;
		    
		    case false:
		    
		      corners[0] = &node[temp];
		      corners[1] = &node[temp+jmax[i]];
		      corners[2] = &node[temp+jmax[i]+1];
		      corners[3] = &node[temp+1];
		      
		      corners[4] = &node[temp+jk];
		      corners[5] = &node[temp+jk+jmax[i]];
		      corners[6] = &node[temp+jk+jmax[i]+1];
		      corners[7] = &node[temp+jk+1];
		      break;
		  
		    default:
		      cerr <<"Unknown element orientation " << endl;
		      exit (10);
		      }
		  */
		      corners[0] = &node[temp];
		      corners[1] = &node[temp+1];
		      corners[2] = &node[temp+jmax[i]+1];
		      corners[3] = &node[temp+jmax[i]];
		      		      
		      corners[4] = &node[temp+jk];
		      corners[5] = &node[temp+jk+1];
		      corners[6] = &node[temp+jk+jmax[i]+1];
		      corners[7] = &node[temp+jk+jmax[i]];

		  cells[count] =  Vol(corners, count, 8);	
		  
	  // add opposite corner nodes of face to be neighbours " << count<< endl;
		  vertexID = cells[count].get_vertexID();
		 
		  add_cell_nei (vertexID, node);
		  // add nodes 
		  count ++;
		  temp ++;
		}
	      //fout <<" end of jmax " << endl;
	      temp ++;
	    }
	  //fout <<" end of kmax " << endl;	  
	  temp = temp + jmax[i];
	}
      //fout <<" end of mmax " << endl;	  
      temp = temp + jk;
    }
  //fout.close();
  delete [] corners;     corners = NULL;
 
  return;
}


void check_numbering (Node** corners, bool& check)
{
  double *prod = new double [3];
  double *x0 = corners[0]->get_xyz();
  
  cross_vec3D (x0, corners[1]->get_xyz(), 
	       corners[2]->get_xyz(), prod);
  
  double *x2 = corners[4]->get_xyz();


  // if the dot product of prod vector and the vector (x4-x0)
  // is less than 0, the normal (prod) is pointing outwards from 
  // the face, else, it points towards the opposite face
  if ( (prod[0]*(x2[0]-x0[0]) + 
	prod[1]*(x2[1]-x0[1]) + 
	prod[2]*(x2[2]-x0[2]) ) < 0.0)
    check = false;
  /*
    cout << prod[0] << " " << prod[1] << "  " << prod[2] <<
    " check is " <<  check << endl;
    exit (91) ;
  */
  delete [] prod;   prod = NULL;
  x0 = NULL;        x2 = NULL;

  return;
}




// assembles the global stiffness matrix
//template <class T1, class T2, class T3, class T4>
//void form_stiff_hex (T1 &alpha, T2 &node, T3 &cells, T4 &F)
void form_stiff_hex (CompRow_Mat_double& alpha, const vector <Node>& node, 
		     const vector <Vol>& cells, VECTOR_double &F, 
		     double*& elemQo, double& meshQ)
{
  int i, j, k;  // counter
  int* ind;

  
  double **K = new double*[24];  // element stiffness matrix
  for (i=0; i<24; i++)
    K[i] = new double [24];

  double *J = new double[9];   // Jacobian matrix
  double *invJ = new double[9];   // Jacobian matrix
  // form dN at each integration (Gauss quadrature) point, 
  // the gauss pt is specified when dN is fo
  vector< vector<double> > dN (3, vector<double>(8,0.0));   

  // calculate the integral values at points a, b, c ,d 
  vector <vector <double> > B (6, vector<double> (24,0.0) );    

  // temporary vector, work space
  vector < vector <double> > tempI (24, vector <double> (6, 0.0));


  //////////////////////

  int row, column, symmR, symmC;
  double val;
  double *coord;  double *xyz;

  int jx3=0, kx3=0;

  vector<double*> vertex_coords (8);
  

  int size = (int) cells.size();
  for (i=0; i<size; i++)
    {
      ind = cells[i].get_vertexID();

      for (j=0; j<8; j++)
	vertex_coords[j] = node[ind[j]].get_xyz();

      //cout << "get elem " << i << "'s stiffness matrix" << endl;
      get_K_hex (vertex_coords, K, J, invJ, dN, B, tempI, elemQo[i], meshQ);

      
      //  ofstream fo ("K.dat", ios::app);
      //for (int ii=0; ii<24; ii++)
      //	for (int jj=0; jj<24; jj++)
      //  fo << ii << "  " << jj << "  "  << K[ii][jj] << endl;
      //	fo.close();
      

      for (j=0; j<8; j++)   // number of nodes in the hexahedron
	{                   // going through the rows
	  if (node[ind[j]].if_surf() == false)
	    {
	      row = node[ind[j]].get_ind();
	      symmR =  node[ind[j]].get_symm();

	      jx3 = j*3;

	      //cout << "row is (ind) " <<row<<endl;
	      //cout.flush();  

	      if (symmR == 0)
		{
		  for (k=0; k<8; k++)   // going through columns
		    {
		      symmC = node[ind[k]].get_symm();
		      kx3 = k*3;
			  			  
		      if (node[ind[k]].if_surf() == false)
			{
			  column = node[ind[k]].get_ind();

			  if (symmC == 0)
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3][kx3+1];
			      alpha.set(row, column+1) = val;
		      
			      val = alpha(row, column+2) + K[jx3][kx3+2];
			      alpha.set(row, column+2) = val;

			      val = alpha(row+1, column) + K[jx3+1][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+1][kx3+1];
			      alpha.set(row+1, column+1) = val;

			      val = alpha(row+1, column+2) + K[jx3+1][kx3+2];
			      alpha.set(row+1, column+2) = val;

			      val = alpha(row+2, column) + K[jx3+2][kx3];
			      alpha.set(row+2, column) = val;

			      val = alpha(row+2, column+1) + K[jx3+2][kx3+1];
			      alpha.set(row+2, column+1) = val;

			      val = alpha(row+2, column+2) + K[jx3+2][kx3+2];
			      alpha.set(row+2, column+2) = val;
			    }
			  else if (symmC == 1)  // xy symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3][kx3+1];
			      alpha.set(row, column+1) = val;
		      
			      val = alpha(row+1, column) + K[jx3+1][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+1][kx3+1];
			      alpha.set(row+1, column+1) = val;

			      val = alpha(row+2, column) + K[jx3+2][kx3];
			      alpha.set(row+2, column) = val;

			      val = alpha(row+2, column+1) + K[jx3+2][kx3+1];
			      alpha.set(row+2, column+1) = val;
			    }
			  else if (symmC == 2)  // yz symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3+1];
			      alpha.set(row, column) = val;
		      
			      val = alpha(row, column+1) + K[jx3][kx3+2];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+1][kx3+1];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+1][kx3+2];
			      alpha.set(row+1, column+1) = val;

			      val = alpha(row+2, column) + K[jx3+2][kx3+1];
			      alpha.set(row+2, column) = val;

			      val = alpha(row+2, column+1) + K[jx3+2][kx3+2];
			      alpha.set(row+2, column+1) = val;
			    }
			  else // (symmC == 3)   // xz symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3][kx3+2];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+1][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+1][kx3+2];
			      alpha.set(row+1, column+1) = val;

			      val = alpha(row+2, column) + K[jx3+2][kx3];
			      alpha.set(row+2, column) = val;
			      val = alpha(row+2, column+1) + K[jx3+2][kx3+2];
			      alpha.set(row+2, column+1) = val;
			    }
			}
		      else
			{
			  coord = node[ind[k]].get_coord();
			  xyz = node[ind[k]].get_xyz();

			  if (symmC == 0)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+2] = F[row+2] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else if (symmC == 1)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1]);

			      F[row+2] = F[row+2] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1]);
			    }
			  else if (symmC == 2)
			    {
			      F[row] = F[row] - K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+2] = F[row+2] - K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else // if (symmC == 3)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+2] = F[row+2] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  
			}
		    }
		}
	      else if (symmR == 1)
		{
		  for (k=0; k<8; k++)   // going through columns
		    {
		      kx3 = k*3;
		      symmC = node[ind[k]].get_symm();
		      
		      if (node[ind[k]].if_surf() == false)
			{
			  column = node[ind[k]].get_ind();

			  if (symmC == 0)
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3][kx3+1];
			      alpha.set(row, column+1) = val;
		      
			      val = alpha(row, column+2) + K[jx3][kx3+2];
			      alpha.set(row, column+2) = val;

			      val = alpha(row+1, column) + K[jx3+1][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+1][kx3+1];
			      alpha.set(row+1, column+1) = val;

			      val = alpha(row+1, column+2) + K[jx3+1][kx3+2];
			      alpha.set(row+1, column+2) = val;

			    }
			  else if (symmC == 1)  // xy symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3][kx3+1];
			      alpha.set(row, column+1) = val;
		      
			      val = alpha(row+1, column) + K[jx3+1][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+1][kx3+1];
			      alpha.set(row+1, column+1) = val;
			    }
			  else if (symmC == 2)  // yz symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3+1];
			      alpha.set(row, column) = val;
		      
			      val = alpha(row, column+1) + K[jx3][kx3+2];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+1][kx3+1];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+1][kx3+2];
			      alpha.set(row+1, column+1) = val;
			    }
			  else //(symmC == 3)   // xz symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3][kx3+2];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+1][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+1][kx3+2];
			      alpha.set(row+1, column+1) = val;
			    }
			}
		      else
			{
			  coord = node[ind[k]].get_coord();
			  xyz = node[ind[k]].get_xyz();
			  if (symmC == 0)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else if (symmC == 1)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1]);
			    }
			  else if (symmC == 2)
			    {
			      F[row] = F[row] - K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else // if (symmC == 3)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);
			    }			    
			}
		    }
		}
	      else if (symmR == 2)
		{
		  for (k=0; k<8; k++)   // going through columns
		    {
		      symmC = node[ind[k]].get_symm();
		      kx3 = k*3;

		      if (node[ind[k]].if_surf() == false)
			{
			  column = node[ind[k]].get_ind();
			  
			  if (symmC == 0)
			    {
			      val = alpha(row, column) + K[jx3+1][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3+1][kx3+1];
			      alpha.set(row, column+1) = val;

			      val = alpha(row, column+2) + K[jx3+1][kx3+2];
			      alpha.set(row, column+2) = val;

			      val = alpha(row+1, column) + K[jx3+2][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+2][kx3+1];
			      alpha.set(row+1, column+1) = val;

			      val = alpha(row+1, column+2) + K[jx3+2][kx3+2];
			      alpha.set(row+1, column+2) = val;
			    }
			  else if (symmC == 1)  // xy symmetry plane
			    {
			      val = alpha(row, column) + K[jx3+1][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3+1][kx3+1];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+2][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+2][kx3+1];
			      alpha.set(row+1, column+1) = val;
			    }
			  else if (symmC == 2)  // yz symmetry plane
			    {
			      val = alpha(row, column) + K[jx3+1][kx3+1];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3+1][kx3+2];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+2][kx3+1];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+2][kx3+2];
			      alpha.set(row+1, column+1) = val;
			    }
			  else // (symmC == 3)   // xz symmetry plane
			    {
			      val = alpha(row, column) + K[jx3+1][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3+1][kx3+2];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+2][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+2][kx3+2];
			      alpha.set(row+1, column+1) = val;
			    }
			}
		      else
			{
			  coord = node[ind[k]].get_coord();
			  xyz = node[ind[k]].get_xyz();
		    
			  if (symmC == 0)
			    {
			      F[row] = F[row] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else if (symmC == 1)
			    {
			      F[row] = F[row] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1]);
			    }
			  else if (symmC == 2)
			    {
			      F[row] = F[row] - K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else // if (symmC == 3)
			    {
			      F[row] = F[row] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }			    
			}
		    }
		}
	      else if (symmR == 3)
		{
		  for (k=0; k<8; k++)   // going through columns
		    {
		      symmC = node[ind[k]].get_symm();
		      kx3 = k*3;

		      if (node[ind[k]].if_surf() == false)
			{
			  column = node[ind[k]].get_ind();
			  
			  if (symmC == 0)
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3][kx3+1];
			      alpha.set(row, column+1) = val;
		      
			      val = alpha(row, column+2) + K[jx3][kx3+2];
			      alpha.set(row, column+2) = val;

			      val = alpha(row+1, column) + K[jx3+2][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+2][kx3+1];
			      alpha.set(row+1, column+1) = val;

			      val = alpha(row+1, column+2) + K[jx3+2][kx3+2];
			      alpha.set(row+1, column+2) = val;
			    }
			  else if (symmC == 1)  // xy symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;

			      val = alpha(row, column+1) + K[jx3][kx3+1];
			      alpha.set(row, column+1) = val;
		      
			      val = alpha(row+1, column) + K[jx3+2][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+2][kx3+1];
			      alpha.set(row+1, column+1) = val;
			    }
			  else if (symmC == 2)  // yz symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3+1];
			      alpha.set(row, column) = val;
		      
			      val = alpha(row, column+1) + K[jx3][kx3+2];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+2][kx3+1];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+2][kx3+2];
			      alpha.set(row+1, column+1) = val;
			    }
			  else // (symmC == 3)   // xz symmetry plane
			    {
			      val = alpha(row, column) + K[jx3][kx3];
			      alpha.set(row, column) = val;
		      
			      val = alpha(row, column+1) + K[jx3][kx3+2];
			      alpha.set(row, column+1) = val;

			      val = alpha(row+1, column) + K[jx3+2][kx3];
			      alpha.set(row+1, column) = val;

			      val = alpha(row+1, column+1) + K[jx3+2][kx3+2];
			      alpha.set(row+1, column+1) = val;
			    }
			}
		      else
			{
			  coord = node[ind[k]].get_coord();
			  xyz = node[ind[k]].get_xyz();

			  if (symmC == 0)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else if (symmC == 1)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1]);
			    }
			  else if (symmC == 2)
			    {
			      F[row] = F[row] - K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else // if (symmC == 3)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }			    
			}
		    }
		}
	      else
		{
		  cerr << "Unknown symmetry plane specification! " << endl;
		  exit(2);
		}
	    }
	}
    }
  for (i=0; i<24; i++)
    delete [] K[i];
  delete [] K;              K = NULL;
  delete [] J;              J = NULL;
  delete [] invJ;           invJ = NULL;

  return;
}


// add neighbours to nodes that are connected by sharing the same cell volume
void add_cell_nei (int *vertexID, vector <Node>& node)
{
  // adding opposite corners of 6 faces
  node[vertexID[0]].add_nei(node[vertexID[2]]);
  node[vertexID[2]].add_nei(node[vertexID[0]]);
  node[vertexID[1]].add_nei(node[vertexID[3]]);
  node[vertexID[3]].add_nei(node[vertexID[1]]);
		  
  node[vertexID[4]].add_nei(node[vertexID[6]]);
  node[vertexID[6]].add_nei(node[vertexID[4]]);
  node[vertexID[5]].add_nei(node[vertexID[7]]);
  node[vertexID[7]].add_nei(node[vertexID[5]]);

  node[vertexID[2]].add_nei(node[vertexID[5]]);
  node[vertexID[5]].add_nei(node[vertexID[2]]);
  node[vertexID[6]].add_nei(node[vertexID[1]]);
  node[vertexID[1]].add_nei(node[vertexID[6]]);

  node[vertexID[0]].add_nei(node[vertexID[7]]);
  node[vertexID[7]].add_nei(node[vertexID[0]]);
  node[vertexID[3]].add_nei(node[vertexID[4]]);
  node[vertexID[4]].add_nei(node[vertexID[3]]);

  node[vertexID[0]].add_nei(node[vertexID[5]]);
  node[vertexID[5]].add_nei(node[vertexID[0]]);
  node[vertexID[1]].add_nei(node[vertexID[4]]);
  node[vertexID[4]].add_nei(node[vertexID[1]]);

  node[vertexID[3]].add_nei(node[vertexID[6]]);
  node[vertexID[6]].add_nei(node[vertexID[3]]);
  node[vertexID[2]].add_nei(node[vertexID[7]]);
  node[vertexID[7]].add_nei(node[vertexID[2]]);

  // adding nodes at opposite corners of the hexahedral
  node[vertexID[0]].add_nei(node[vertexID[6]]);
  node[vertexID[6]].add_nei(node[vertexID[0]]);
  node[vertexID[1]].add_nei(node[vertexID[7]]);
  node[vertexID[7]].add_nei(node[vertexID[1]]);

  node[vertexID[4]].add_nei(node[vertexID[2]]);
  node[vertexID[2]].add_nei(node[vertexID[4]]);
  node[vertexID[5]].add_nei(node[vertexID[3]]);
  node[vertexID[3]].add_nei(node[vertexID[5]]);

  return;
}

//template <class T1, class T2>
//void update3D (T1& node, const T2& du)
void update3D (vector <Node>& node, const VECTOR_double &du)
{
  int j, count = 0;
  Node *overlap;
  double *coord;
  int symm = 0;

  int size = (int) node.size(); 
  for (j=0; j<size; j++)
    {
      if (node[j].if_surf() == false)
	{
	  symm = node[j].get_symm();
	  if (symm == 0)
	    {
	      node[j].deform(du[count], du[count+1], du[count+2]);
	      count = count + 3;
	    }
	  else 
	    {
	      coord = node[j].get_coord();

	      if (symm == 1)
		{
		  coord[0] += du[count];
		  coord[1] += du[count+1];
		}
	      else if (symm == 2)
		{
		  coord[1] += du[count];
		  coord[2] += du[count+1];
		}
	      else if (symm == 3)
		{
		  coord[0] += du[count];
		  coord[2] += du[count+1];
		}
	      count +=2;
	    }
	}
      else
	{
	  overlap = node[j].get_overlap();
	  if (overlap != NULL)
	    {
	      coord = overlap->get_coord();
	      node[j].set_coord(coord);
	    }
	}
    }                                                         

  return;
}


void update3D (vector <Node>& node, const double* du)
{
  int j, count = 0;
  Node *overlap;
  double *coord;
  int symm = 0;

  int size = (int) node.size(); 
  for (j=0; j<size; j++)
    {
      if (node[j].if_surf() == false)
	{
	  symm = node[j].get_symm();
	  if (symm == 0)
	    {
	      node[j].deform(du[count], du[count+1], du[count+2]);
	      count = count + 3;
	    }
	  else 
	    {
	      coord = node[j].get_coord();

	      if (symm == 1)
		{
		  coord[0] += du[count];
		  coord[1] += du[count+1];
		}
	      else if (symm == 2)
		{
		  coord[1] += du[count];
		  coord[2] += du[count+1];
		}
	      else if (symm == 3)
		{
		  coord[0] += du[count];
		  coord[2] += du[count+1];
		}
	      count +=2;
	    }
	}
      else
	{
	  overlap = node[j].get_overlap();
	  if (overlap != NULL)
	    {
	      coord = overlap->get_coord();
	      node[j].set_coord(coord);
	    }
	}
    }                                                         

  return;
}




// assembles the global stiffness matrix
void form_stiff_tet (CompRow_Mat_double& alpha, 
		     const vector <Node>& node, 
		     const vector <Vol>& cells, VECTOR_double &F)
{
  int i, j, k;  // counter
  int* ind;

  double **K = new double*[12];  // element stiffness matrix
  for (i=0; i<12; i++)
    K[i] = new double [12];

  int row, column;
  double val;
  double *coord;  double *xyz;

  vector<double*> vertex_coords (4);
  
  int size = (int) cells.size();
  for (i=0; i<size; i++)
    {
      ind = cells[i].get_vertexID();

      for (j=0; j<4; j++)
	vertex_coords[j] = node[ind[j]].get_xyz();

      // cout << "get element " << i << " 's stiffness matrix " << size << " ";
      get_K_Tet (vertex_coords, K);
      // cout << ".. end " << endl;
      /*    ofstream fout ("k.dat");            
	    for (int ii=0; ii<24; ii++)
	    for (int jj=0; jj<24; jj++)
	    fout << ii << "  "  << jj << "  " <<K[ii][jj] << endl;
	    exit (9); 
      */

      for (j=0; j<4; j++)   // number of nodes in the tetrahedron
	{                   // going through the rows
	  if (node[ind[j]].if_surf() == false)
	    {
	
	      row = node[ind[j]].get_ind() * 3;
	      for (k=0; k<4; k++)   // going through columns
		{
		  if (node[ind[k]].if_surf() == false)
		    {
		      column = node[ind[k]].get_ind() * 3;

		      val = alpha(row, column) + K[j*3][k*3];
		      alpha.set(row, column) = val;

		      val = alpha(row, column+1) + K[j*3][k*3+1];
		      alpha.set(row, column+1) = val;
		      
		      val = alpha(row, column+2) + K[j*3][k*3+2];
		      alpha.set(row, column+2) = val;

		      val = alpha(row+1, column) + K[j*3+1][k*3];
		      alpha.set(row+1, column) = val;

		      val = alpha(row+1, column+1) + K[j*3+1][k*3+1];
		      alpha.set(row+1, column+1) = val;

		      val = alpha(row+1, column+2) + K[j*3+1][k*3+2];
		      alpha.set(row+1, column+2) = val;

		      val = alpha(row+2, column) + K[j*3+2][k*3];
		      alpha.set(row+2, column) = val;

		      val = alpha(row+2, column+1) + K[j*3+2][k*3+1];
		      alpha.set(row+2, column+1) = val;

		      val = alpha(row+2, column+2) + K[j*3+2][k*3+2];
		      alpha.set(row+2, column+2) = val;
		    }
		  else
		    {
		      coord = node[ind[k]].get_coord();
		      xyz = node[ind[k]].get_xyz();

		      //cout << coord[0] << "  " << xyz[0] << endl; 

		      F[row] = F[row] - K[j*3][k*3]*(coord[0]-xyz[0])
			- K[j*3][k*3+1]*(coord[1]-xyz[1])
			- K[j*3][k*3+2]*(coord[2]-xyz[2]);


		      //cout << "F[row] is " << row << "  " << F[row]
		      //   << endl; exit(9);

		      F[row+1] = F[row+1] - K[j*3+1][k*3]*(coord[0]-xyz[0])
			- K[j*3+1][k*3+1]*(coord[1]-xyz[1])
			- K[j*3+1][k*3+2]*(coord[2]-xyz[2]);

		      F[row+2] = F[row+2] - K[j*3+2][k*3]*(coord[0]-xyz[0])
			- K[j*3+2][k*3+1]*(coord[1]-xyz[1])
			- K[j*3+2][k*3+2]*(coord[2]-xyz[2]);
		    }
		}
	    }
	}

    }

  for (i=0; i<12; i++)
    delete [] K[i];
  delete [] K;

  return;
}



void update3D_xyz (vector <Node>& node, const VECTOR_double& du)
{
  int i, j;
  double *xyz = NULL;

  // updates the new coordinates 
  update3D (node, du);

  // copy the new coordinates to the iniial xyz coordinates
  i = (int) node.size();
  for (j=0; j<i; j++)
    {
      xyz = node[j].get_coord();
      node[j].set_xyz(xyz);
    }          

  xyz = NULL;

  return;
}


void add_stiff_val (int*& rowptr, int*& colind, double*& stiff_val, 
		    const vector <Node>& node, 
		    const vector <Vol>& cells, VECTOR_double &F, double*& 
		    elemQo, double& meshQ)
{
  int i, j, k;  // counter
  int* ind;

  double **K = new double*[24];  // element stiffness matrix
  for (i=0; i<24; i++)
    K[i] = new double [24];

  double *J = new double[9];   // Jacobian matrix
  double *invJ = new double[9];   // Jacobian matrix
  // form dN at each integration (Gauss quadrature) point, 
  // the gauss pt is specified when dN is fo
  vector< vector<double> > dN (3, vector<double>(8,0.0));   

  // calculate the integral values at points a, b, c ,d 
  vector <vector <double> > B (6, vector<double> (24,0.0) );    

  // temporary vector, work space
  vector < vector <double> > tempI (24, vector <double> (6, 0.0));


  //////////////////////

  int row, col, symmR, symmC;
  double *coord;  double *xyz;

  int jx3=0, kx3=0;

  vector<double*> vertex_coords (8);
  

  int size = (int) cells.size();
  for (i=0; i<size; i++)
    {
      ind = cells[i].get_vertexID();

      for (j=0; j<8; j++)
	vertex_coords[j] = node[ind[j]].get_xyz();

      //cout << "get elem " << i << "'s stiffness matrix" << endl;
      get_K_hex (vertex_coords, K, J, invJ, dN, B, tempI, elemQo[i], 
		 meshQ);

      for (j=0; j<8; j++)   // number of nodes in the hexahedron
	{                   // going through the rows
	  if (node[ind[j]].if_surf() == false)
	    {
	      row = node[ind[j]].get_ind();
	      symmR =  node[ind[j]].get_symm();

	      jx3 = j*3;

	      //cout << "row is (ind) " <<row<<endl;
	      //cout.flush();  

	      if (symmR == 0)
		{
		  for (k=0; k<8; k++)   // going through columns
		    {
		      symmC = node[ind[k]].get_symm();
		      kx3 = k*3;
			  			  
		      if (node[ind[k]].if_surf() == false)
			{
			  col = node[ind[k]].get_ind();

			  if (symmC == 0)
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row, col+2, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);

			      add_val_csr (row+1, col+2, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);

			      add_val_csr (row+2, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+2, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);

			      add_val_csr (row+2, col+2, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			  else if (symmC == 1)  // xy symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);

			      add_val_csr (row+2, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+2, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);
			    }
			  else if (symmC == 2)  // yz symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);

			      add_val_csr (row+2, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);

			      add_val_csr (row+2, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			  else // (symmC == 3)   // xz symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);

			      add_val_csr (row+2, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+2, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			}
		      else
			{
			  coord = node[ind[k]].get_coord();
			  xyz = node[ind[k]].get_xyz();

			  if (symmC == 0)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+2] = F[row+2] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else if (symmC == 1)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1]);

			      F[row+2] = F[row+2] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1]);
			    }
			  else if (symmC == 2)
			    {
			      F[row] = F[row] - K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+2] = F[row+2] - K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else // if (symmC == 3)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+2] = F[row+2] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  
			}
		    }
		}
	      else if (symmR == 1)
		{
		  for (k=0; k<8; k++)   // going through columns
		    {
		      kx3 = k*3;
		      symmC = node[ind[k]].get_symm();
		      
		      if (node[ind[k]].if_surf() == false)
			{
			  col = node[ind[k]].get_ind();

			  if (symmC == 0)
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row, col+2, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);

			      add_val_csr (row+1, col+2, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);
			    }
			  else if (symmC == 1)  // xy symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);
			    }
			  else if (symmC == 2)  // yz symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);
			    }
			  else //(symmC == 3)   // xz symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);
			    }
			}
		      else
			{
			  coord = node[ind[k]].get_coord();
			  xyz = node[ind[k]].get_xyz();
			  if (symmC == 0)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else if (symmC == 1)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1]);
			    }
			  else if (symmC == 2)
			    {
			      F[row] = F[row] - K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else // if (symmC == 3)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);
			    }			    
			}
		    }
		}
	      else if (symmR == 2)
		{
		  for (k=0; k<8; k++)   // going through columns
		    {
		      symmC = node[ind[k]].get_symm();
		      kx3 = k*3;

		      if (node[ind[k]].if_surf() == false)
			{
			  col = node[ind[k]].get_ind();
			  
			  if (symmC == 0)
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);

			      add_val_csr (row, col+2, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);

			      add_val_csr (row+1, col+2, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			  else if (symmC == 1)  // xy symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);
			    }
			  else if (symmC == 2)  // yz symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+1]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			  else // (symmC == 3)   // xz symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3+1][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			}
		      else
			{
			  coord = node[ind[k]].get_coord();
			  xyz = node[ind[k]].get_xyz();
		    
			  if (symmC == 0)
			    {
			      F[row] = F[row] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else if (symmC == 1)
			    {
			      F[row] = F[row] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+1]*(coord[1]-xyz[1]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1]);
			    }
			  else if (symmC == 2)
			    {
			      F[row] = F[row] - K[jx3+1][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else // if (symmC == 3)
			    {
			      F[row] = F[row] - K[jx3+1][kx3]*(coord[0]-xyz[0])
				- K[jx3+1][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }			    
			}
		    }
		}
	      else if (symmR == 3)
		{
		  for (k=0; k<8; k++)   // going through columns
		    {
		      symmC = node[ind[k]].get_symm();
		      kx3 = k*3;

		      if (node[ind[k]].if_surf() == false)
			{
			  col = node[ind[k]].get_ind();
			  
			  if (symmC == 0)
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row, col+2, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);

			      add_val_csr (row+1, col+2, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			  else if (symmC == 1)  // xy symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);

			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);
			    }
			  else if (symmC == 2)  // yz symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3+1]);
		      
			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+1]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			  else // (symmC == 3)   // xz symmetry plane
			    {
			      add_val_csr (row, col, rowptr, colind, 
					   stiff_val, K[jx3][kx3]);
		      
			      add_val_csr (row, col+1, rowptr, colind, 
					   stiff_val, K[jx3][kx3+2]);

			      add_val_csr (row+1, col, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3]);

			      add_val_csr (row+1, col+1, rowptr, colind, 
					   stiff_val, K[jx3+2][kx3+2]);
			    }
			}
		      else
			{
			  coord = node[ind[k]].get_coord();
			  xyz = node[ind[k]].get_xyz();

			  if (symmC == 0)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else if (symmC == 1)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+1]*(coord[1]-xyz[1]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+1]*(coord[1]-xyz[1]);
			    }
			  else if (symmC == 2)
			    {
			      F[row] = F[row] - K[jx3][kx3+1]*(coord[1]-xyz[1])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3+1]*(coord[1]-xyz[1])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }
			  else // if (symmC == 3)
			    {
			      F[row] = F[row] - K[jx3][kx3]*(coord[0]-xyz[0])
				- K[jx3][kx3+2]*(coord[2]-xyz[2]);

			      F[row+1] = F[row+1] - K[jx3+2][kx3]*(coord[0]-xyz[0])
				- K[jx3+2][kx3+2]*(coord[2]-xyz[2]);
			    }			    
			}
		    }
		}
	      else
		{
		  cerr << "Unknown symmetry plane specification! " << endl;
		  exit(2);
		}
	    }
	}
    }
  for (i=0; i<24; i++)
    delete [] K[i];
  delete [] K;              K = NULL;
  delete [] J;              J = NULL;
  delete [] invJ;           invJ = NULL;

  return;
}


void get_ini_quality (const vector <Vol>& cells, 
		      const vector <Node>& nodes,
		      double*& elemQo, double& Qomax)
{
  // calculates the initial element quality and the initial mesh
  // quality Qomax

  int i, j, k;  // counter
  int nvertices = cells[0].get_nvertices();

  vector<double*> vertices (nvertices);
  //  double *xyz;  
  int* vertexID;

  // ofstream fout("qhex.dat");

  k = (int) cells.size();
  for (j=0; j<k; j++)
    {
      vertexID = cells[j].get_vertexID();
    
      for (i=0; i<nvertices; i++)
	vertices[i] = nodes[vertexID[i]].get_xyz();
	
      get_Q_hex (vertices, elemQo[j]);
      // fout << elemQo[j] << endl;
      
    }
  // fout.close();

  Qomax = elemQo[0];
  for (j=1; j<k; j++)
    {
      if (elemQo[j] > Qomax)
	Qomax = elemQo[j];
    }

  vertexID = NULL;   // xyz = NULL;
  return;
}

