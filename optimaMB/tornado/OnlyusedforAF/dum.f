      subroutine dum(jdim,kdim,q,xyj,sndsp,precon,s,
     &               jlow,jup,klow,kup) 
cdu
cdu   this routine multiplies the vector 's' by the transfer matrix 'm'
cdu
#include "../include/common.inc"
c
c
      dimension q(jdim,kdim,4),xyj(jdim,kdim),sndsp(jdim,kdim)
      dimension precon(jdim,kdim,8),s(jdim,kdim,4)
c
c     note gami=gamma - 1
c
      do 1000 k=klow,kup
         do 1000 j=jlow,jup
c
            rho=q(j,k,1)*xyj(j,k)
            u=q(j,k,2)/q(j,k,1)
            v=q(j,k,3)/q(j,k,1)
cdu         c=sqrt(precon(j,k,3))
            c=sndsp(j,k)
c
c    store
c
            t1=s(j,k,1)
            t2=s(j,k,2)
            t3=s(j,k,3)
            t4=s(j,k,4)
c
c     multiply by M
c
            s(j,k,1)=(rho*t1-t4/c)/c
            s(j,k,2)=u*s(j,k,1)+rho*t2
            s(j,k,3)=v*s(j,k,1)+rho*t3
            s(j,k,4)=rho*((precon(j,k,2)/c+c/gami)*t1+
     +           u*t2+v*t3)-precon(j,k,2)/precon(j,k,3)*t4
c
 1000 continue
c
      return
      end
