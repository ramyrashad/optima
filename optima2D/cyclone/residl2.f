      subroutine residl2(jdim,kdim,ndim,s,nres,snew)

      use disscon_vars
#include "../include/arcom.inc"

      integer jdim, kdim, ndim, nres

      double precision s(jdim,kdim,ndim)
      double precision snew

c     -- compute l2 and max norms of density residual or total 
c     residual --

c     -- since cmesh not used in optimization or N-K algorithm, it is
c     commented out in max residual search --

c     -- total residual is better for S-A runs with N-K algorithm,
c     especially for optimization --

c     -- mods by: marian nemec, feb. 2001 --

      resid = 0.d0
      do n = 1,nres
        do k = klow,kup
          do j = jlow,jup 
            resid = resid + s(j,k,n)**2
          end do
        end do
      end do
      resid = resid + snew**2

c     -- trick to get the total residual for cyclone --
c     -- res_spl2 is stored as a global variable --      
      if (nres.ne.5) resid = resid + res_spl2

      resid = resid/dble( (jup-jlow+1)*(kup-klow+1) )
      resid = dsqrt( resid ) / dt
      !-- Store a reset value of residual
      resid_reset = resid

      !-- Calculate the L2 norm of the interior mean-flow resid eqs     
      if (dissCon.and.viscous.and.turbulnt.and.itmodel.eq.2) then
         residM = 0.d0
         do n = 1,4
            do k = klow,kup
               do j = jlow,jup 
                  residM = residM + s(j,k,n)**2
               end do
            end do
         end do
         residM = residM + snew**2

         residM = residM/dble( (jup-jlow+1)*(kup-klow+1) )
         residM = dsqrt( residM ) / dt
      end if

      !-- Calculate the L2 norm of all mean-flow resid eqs:
      !-- interior and boundary
      if (dissCon.and.viscous.and.turbulnt.and.itmodel.eq.2) then
         total_residM = 0.d0
         do n = 1,4
            do k = 1,kend
               do j = 1,jend 
                  total_residM = total_residM + s(j,k,n)**2
               end do
            end do
         end do
         total_residM = total_residM + snew**2

         total_residM = total_residM/dble( (jend-1+1)*(kend-1+1) )
         total_residM = dsqrt( total_residM ) / dt
      end if

      !-- Calculate the L2 norm of the interior turbulent resid eqs
      if (dissCon.and.viscous.and.turbulnt.and.itmodel.eq.2) then
         residT = 0.d0
         do k = klow,kup
            do j = jlow,jup 
               residT = residT + s(j,k,5)**2
            end do
         end do
         residT = residT + snew**2
         
         residT = residT/dble( (jup-jlow+1)*(kup-klow+1) )
         residT = dsqrt( residT ) / dt
      end if

      !-- Calculate the L2 norm of all the turbulent resid eqs:
      !-- interior and boundary
      if (dissCon.and.viscous.and.turbulnt.and.itmodel.eq.2) then
         total_residT = 0.d0
         do k = 1,kend
            do j = 1,jend 
               total_residT = total_residT + s(j,k,5)**2
            end do
         end do
         total_residT = total_residT + snew**2
         
         total_residT = total_residT/dble( (jend-1+1)*(kend-1+1) )
         total_residT = dsqrt( total_residT ) / dt
      end if

c     -- maximum density residual and location --
      residmx = 0.d0                                                   
      do k = klow,kup                                               
        absres = 0.d0                                                 
        do j = jlow, jup                                         
          abss = abs( s(j,k,1) )                             
          if ( abss .le. absres ) goto 10                          
          absres = abss
          imax=j
          k2=k                                         

c     sd
c     -- logic is in place to give exact index, not the one used after
c     the shuffling (easier to pinpoint error now.) --

c     if (cmesh) then
c     if (.not.wake) then
c     imax=imax+jtail1-1
c     else
c     if (k2.gt.kmax-1) then
c     c     upper wake cut
c     imax=jtail2+imax
c     k2=k-kmax+1
c     else
c     c     lower wake cut
c     imax=(jtail1-1)-imax+1
c     k2=kmax-k
c     endif
c     endif
c     endif

 10       continue                                                    
        end do

        absres = absres / dt                                        
        if ( absres .le. residmx ) goto 15                          
        maxres(1) = imax                                        
        maxres(2) = k2                                           
        residmx = absres                                         
 15     continue
      end do

      return                                                         
      end                       !residl2


************************************************************************
      !-- Program name: residFl2
      !-- Written by: Howard Buckley
      !-- Date: April 2010
      !-- 
      !-- Calculate norm of modified residual used in dissipation-based
      !-- continuation method for N-K globalization
************************************************************************
      subroutine residFl2(jdim,kdim,ndim,s,nres,snew)

      use disscon_vars
      implicit none

#include "../include/arcom.inc"

      integer 
     &     jdim, kdim, ndim, nres, n, j, k

      double precision 
     &     s(jdim,kdim,ndim), snew

      residF = 0.d0
      do n = 1,nres
        do k = klow,kup                                                
          do j = jlow,jup
            residF = residF + s(j,k,n)**2  
          end do
        end do
      end do
      residF = residF + snew**2

c     -- trick to get the total residual for cyclone --
      if (nres.ne.5) residF = residF + res_spl2

      residF = residF/dble( (jup-jlow+1)*(kup-klow+1) )
      residF = dsqrt( residF ) / dt

      !-- Calculate the L2 norm of the dbc-modified interior mean-flow 
      !-- residual eqs     
      if (dissCon.and.viscous.and.turbulnt.and.itmodel.eq.2) then
         residFM = 0.d0
         do n = 1,4
            do k = klow,kup
               do j = jlow,jup 
                  residFM = residFM + s(j,k,n)**2
               end do
            end do
         end do
         residFM = residFM + snew**2

         residFM = residFM/dble( (jup-jlow+1)*(kup-klow+1) )
         residFM = dsqrt( residFM ) / dt
      end if

      !-- Calculate the L2 norm of all dbc-modified mean-flow 
      !-- residual eqs: interior and boundary     
      if (dissCon.and.viscous.and.turbulnt.and.itmodel.eq.2) then
         total_residFM = 0.d0
         do n = 1,4
            do k = 1,kend
               do j = 1,jend 
                  total_residFM = total_residFM + s(j,k,n)**2
               end do
            end do
         end do
         total_residFM = total_residFM + snew**2

         total_residFM = total_residFM/dble( (jend-1+1)*(kend-1+1) )
         total_residFM = dsqrt( total_residFM ) / dt
      end if

      !-- Calculate the L2 norm of the dbc-modified interior turbulent 
      !-- residual eq     
      if (dissCon.and.viscous.and.turbulnt.and.itmodel.eq.2) then
         residFT = 0.d0
         do k = klow,kup
            do j = jlow,jup 
               residFT = residFT + s(j,k,5)**2
            end do
         end do
         residFT = residFT + snew**2
         
         residFT = residFT/dble( (jup-jlow+1)*(kup-klow+1) )
         residFT = dsqrt( residFT ) / dt
      end if

      !-- Calculate the L2 norm of all the dbc-modified turbulent 
      !-- residual eqs: interior and boundary     
      if (dissCon.and.viscous.and.turbulnt.and.itmodel.eq.2) then
         total_residFT = 0.d0
         do k = 1,kend
            do j = 1,jend
               total_residFT = total_residFT + s(j,k,5)**2
            end do
         end do
         total_residFT = total_residFT + snew**2
         
         total_residFT = total_residFT/dble( (jend-1+1)*(kend-1+1) )
         total_residFT = dsqrt( total_residFT ) / dt
      end if


      return                                                         
      end                       !residFl2

************************************************************************
      !-- Program name: BresidFl2
      !-- Written by: Howard Buckley
      !-- Date: April 2010
      !-- 
      !-- Calculate L2 norm of modified residual at the boundary nodes
      !-- used in dissipation-based
      !-- continuation method for N-K globalization
************************************************************************
      subroutine BresidFl2(jdim,kdim,ndim,s,nres,snew)

      use disscon_vars
      implicit none

#include "../include/arcom.inc"

      integer 
     &     jdim, kdim, ndim, nres, n, j, k

      double precision 
     &     s(jdim,kdim,ndim), snew

      BresidF = 0.d0

      !-- Wake nodes - bottom
      k = 1
      do n = 1,nres                                             
         do j = 1,jtail1-1
            BresidF = BresidF + s(j,k,n)**2  
         end do
      end do

      !-- Airfoil nodes
      k = 1
      do n = 1,nres                                             
         do j = jtail1,jtail2
            BresidF = BresidF + s(j,k,n)**2  
         end do
      end do

      !-- Wake nodes - top
      k = 1
      do n = 1,nres                                             
         do j = jtail2+1,kend
            BresidF = BresidF + s(j,k,n)**2  
         end do
      end do

      !-- Far-field boundary nodes
      k = kend
      do n = 1,nres                                             
         do j = 1,jend
            BresidF = BresidF + s(j,k,n)**2  
         end do
      end do

      !-- Inflow boundary nodes
      j = 1
      do n = 1,nres                                             
         do k = 2,kend-1
            BresidF = BresidF + s(j,k,n)**2  
         end do
      end do

      !-- Outflow boundary nodes
      j = jend
      do n = 1,nres                                             
         do k = 2,kend-1
            BresidF = BresidF + s(j,k,n)**2  
         end do
      end do
     
      BresidF = BresidF + snew**2

      BresidF = BresidF/dble( (jup-jlow+1)*(kup-klow+1) )
      BresidF = dsqrt( BresidF ) / dt

      !-- Calculate the residual of the modified mean-flow boundary eqs
      BresidFM = 0.d0

      !-- Wake nodes - bottom
      k = 1
      do n = 1,4                                             
         do j = 1,jtail1-1
            BresidFM = BresidFM + s(j,k,n)**2  
         end do
      end do

      !-- Airfoil nodes
      k = 1
      do n = 1,4                                             
         do j = jtail1,jtail2
            BresidFM = BresidFM + s(j,k,n)**2  
         end do
      end do

      !-- Wake nodes - top
      k = 1
      do n = 1,4                                             
         do j = jtail2+1,kend
            BresidFM = BresidFM + s(j,k,n)**2  
         end do
      end do

      !-- Far-field boundary nodes
      k = kend
      do n = 1,4                                             
         do j = 1,jend
            BresidFM = BresidFM + s(j,k,n)**2  
         end do
      end do

      !-- Inflow boundary nodes
      j = 1
      do n = 1,4                                             
         do k = 2,kend-1
            BresidFM = BresidFM + s(j,k,n)**2  
         end do
      end do

      !-- Outflow boundary nodes
      j = jend
      do n = 1,4                                             
         do k = 2,kend-1
            BresidFM = BresidFM + s(j,k,n)**2  
         end do
      end do
     
      BresidFM = BresidFM + snew**2

      BresidFM = BresidFM/dble( (jup-jlow+1)*(kup-klow+1) )
      BresidFM = dsqrt( BresidFM ) / dt

      !-- Calculate the residual of the modified turbulent boundary eq.
      if (viscous .and. turbulnt .and. itmodel .eq. 2) then
         BresidFT = 0.d0

         !-- Wake nodes - bottom
         k = 1
         do n = 5,5                                             
            do j = 1,jtail1-1
               BresidFT = BresidFT + s(j,k,n)**2  
            end do
         end do

         !-- Airfoil nodes
         k = 1
         do n = 5,5                                             
            do j = jtail1,jtail2
               BresidFT = BresidFT + s(j,k,n)**2  
            end do
         end do

         !-- Wake nodes - top
         k = 1
         do n = 5,5                                             
            do j = jtail2+1,kend
               BresidFT = BresidFT + s(j,k,n)**2  
            end do
         end do

         !-- Far-field boundary nodes
         k = kend
         do n = 5,5                                             
            do j = 1,jend
               BresidFT = BresidFT + s(j,k,n)**2  
            end do
         end do

         !-- Inflow boundary nodes
         j = 1
         do n = 5,5                                             
            do k = 2,kend-1
               BresidFT = BresidFT + s(j,k,n)**2  
            end do
         end do
         
         !-- Outflow boundary nodes
         j = jend
         do n = 5,5                                             
            do k = 2,kend-1
               BresidFT = BresidFT + s(j,k,n)**2  
            end do
         end do
     
         BresidFT = BresidFT + snew**2
         
         BresidFT = BresidFT/dble( (jup-jlow+1)*(kup-klow+1) )
         BresidFT = dsqrt( BresidFT ) / dt

      end if

      return                                                         
      end                       !BresidFl2

************************************************************************
      !-- Program name: Bresidl2
      !-- Written by: Howard Buckley
      !-- Date: April 2010
      !-- 
      !-- Calculate L2 norm of residual at the boundary nodes
************************************************************************
      subroutine Bresidl2(jdim,kdim,ndim,s,nres,snew)

      use disscon_vars
      implicit none

#include "../include/arcom.inc"

      integer 
     &     jdim, kdim, ndim, nres, n, j, k

      double precision 
     &     s(jdim,kdim,ndim), snew

      Bresid = 0.d0

      !-- Wake nodes - bottom
      k = 1
      do n = 1,nres                                             
         do j = 1,jtail1-1
            Bresid = Bresid + s(j,k,n)**2  
         end do
      end do

      !-- Airfoil nodes
      k = 1
      do n = 1,nres                                             
         do j = jtail1,jtail2
            Bresid = Bresid + s(j,k,n)**2  
         end do
      end do

      !-- Wake nodes - top
      k = 1
      do n = 1,nres                                             
         do j = jtail2+1,kend
            Bresid = Bresid + s(j,k,n)**2  
         end do
      end do

      !-- Far-field boundary nodes
      k = kend
      do n = 1,nres                                             
         do j = 1,jend
            Bresid = Bresid + s(j,k,n)**2  
         end do
      end do

      !-- Inflow boundary nodes
      j = 1
      do n = 1,nres                                             
         do k = 2,kend-1
            Bresid = Bresid + s(j,k,n)**2  
         end do
      end do

      !-- Outflow boundary nodes
      j = jend
      do n = 1,nres                                             
         do k = 2,kend-1
            Bresid = Bresid + s(j,k,n)**2  
         end do
      end do
     
      Bresid = Bresid + snew**2

      Bresid = Bresid/dble( (jup-jlow+1)*(kup-klow+1) )
      Bresid = dsqrt( Bresid ) / dt
 
      !-- Caluclate the residual for the mean-flow boundary equations
      BresidM = 0.d0

      !-- Wake nodes - bottom
      k = 1
      do n = 1,4                                             
         do j = 1,jtail1-1
            BresidM = BresidM + s(j,k,n)**2  
         end do
      end do

      !-- Airfoil nodes
      k = 1
      do n = 1,4                                             
         do j = jtail1,jtail2
            BresidM = BresidM + s(j,k,n)**2  
         end do
      end do

      !-- Wake nodes - top
      k = 1
      do n = 1,4                                             
         do j = jtail2+1,kend
            BresidM = BresidM + s(j,k,n)**2  
         end do
      end do

      !-- Far-field boundary nodes
      k = kend
      do n = 1,4                                             
         do j = 1,jend
            BresidM = BresidM + s(j,k,n)**2  
         end do
      end do

      !-- Inflow boundary nodes
      j = 1
      do n = 1,4                                             
         do k = 2,kend-1
            BresidM = BresidM + s(j,k,n)**2  
         end do
      end do

      !-- Outflow boundary nodes
      j = jend
      do n = 1,4                                             
         do k = 2,kend-1
            BresidM = BresidM + s(j,k,n)**2  
         end do
      end do
     
      BresidM = BresidM + snew**2

      BresidM = BresidM/dble( (jup-jlow+1)*(kup-klow+1) )
      BresidM = dsqrt( BresidM ) / dt

      !-- Caluclate the residual for the turbulent boundary equation
      if (viscous .and. turbulnt .and. itmodel .eq. 2) then

         BresidT = 0.d0

         !-- Wake nodes - bottom
         k = 1
         do n = 5,5                                             
            do j = 1,jtail1-1
               BresidT = BresidT + s(j,k,n)**2  
            end do
         end do
         
         !-- Airfoil nodes
         k = 1
         do n = 5,5                                             
            do j = jtail1,jtail2
               BresidT = BresidT + s(j,k,n)**2  
            end do
         end do

         !-- Wake nodes - top
         k = 1
         do n = 5,5                                             
            do j = jtail2+1,kend
               BresidT = BresidT + s(j,k,n)**2  
            end do
         end do

         !-- Far-field boundary nodes
         k = kend
         do n = 5,5                                             
            do j = 1,jend
               BresidT = BresidT + s(j,k,n)**2  
            end do
         end do

         !-- Inflow boundary nodes
         j = 1
         do n = 5,5                                             
            do k = 2,kend-1
               BresidT = BresidT + s(j,k,n)**2  
            end do
         end do
         
         !-- Outflow boundary nodes
         j = jend
         do n = 5,5                                             
            do k = 2,kend-1
               BresidT = BresidT + s(j,k,n)**2  
            end do
         end do
     
         BresidT = BresidT + snew**2

         BresidT = BresidT/dble( (jup-jlow+1)*(kup-klow+1) )
         BresidT = dsqrt( BresidT ) / dt

      end if     

      return                                                         
      end                       !Bresidl2
