c     -- constants associated with the S-A turbulence model --
c     -- m. nemec, sept. 2001 --

	double precision expon,akarman,cb1,sigmainv,cb2,cw1,cw2,cw3
	double precision cw3_6,cv1,cv1_3,cv2,ct1,ct2,ct3,ct4

      parameter (expon = 1.0/6.0, akarman = 0.41)
      parameter (cb1= 0.1355, sigmainv= 1.5)
      parameter (cb2 = 0.622, cw1 = cb1/(akarman**2)+(1.0+cb2)*sigmainv)
c     -- underscore is equivalent to '**' i.e. exponentiation --
      parameter (cw2 = 0.3, cw3 = 2.0, cw3_6 = 64.0)
c     -- const is used to aviod the computation of ( )**1/6 in a loop --
      parameter (cv1 = 7.1, cv1_3 = 357.911, cv2 = 5.0)
c     -- setting ct1 to 5 instead of 1 helps convergence --
      parameter (ct1 = 5.0, ct2 = 2.0, ct3 = 1.2, ct4 = 0.5)
