c----------------------------------------------------------------------
c     -- adjust grid in either x or y direction --
c     -- note: g is the x or y coordinate array to be modified --
c     --       ixy = 1 denotes x direction --
c     --           = 2 denotes y direction --
c     -- m. nemec, may 2001 --
c----------------------------------------------------------------------
      subroutine adjgrd(node, ip,  ifoil, js, je, ks, ke, istep, ixy,
     &     jdim, kdim, g, xo, yo, seg)

      implicit none

c     -- these includes are necessary to access bap array --
#include "../include/parms.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/common.inc"

      integer node, ip,  ifoil, js, je, ks, ke, istep, ixy, jdim, kdim
      integer ifirst,ilast,i,j,k,iiend
      double precision g(jdim,kdim), xo(jdim,kdim), yo(jdim,kdim)
      double precision seg(jdim*kdim),tmp,delta

      ifirst = 0
      ilast = 0

      ip = node
      do j = js, je, istep

         delta = bap(ip,ixy,ifoil) - g(j,ks)
         ip = ip + 1
         if ( abs( delta ) .gt. 1.e-15 ) then
c            write (*,*) 'adjusting grid!',ip-1,j,bap(ip-1,ixy,ifoil),
c     &           g(j,ks)
            ilast = j
            if (ifirst.eq.0) ifirst = j
c     -- calculate arclength for k-line at location j --
            i = 1
            seg(i) = 0.0
            do k = ks+istep, ke, istep 
               tmp = sqrt( (xo(j,k) - xo(j,k-istep) )**2 + ( yo(j,k) -
     &              yo(j,k-istep) )**2 )
               i = i+1
               seg(i) = seg(i-1) + tmp
            end do
            iiend = i
            i = 1
            do k = ks, ke, istep
               g(j,k) = g(j,k) + delta/2.0*(1.0 + cos(pi*seg(i)
     &              /seg(iiend)) )
c               g(j,k) = g(j,k) + delta*( 1.0 - seg(i)/seg(iiend) )
               i = i+1
            end do
         end if
      end do

c     write (*,10) ifirst,ilast
c     10   format (3x,'Adjusted Nodes:',i5,i5)

      return
      end                       !adjgrd


c     -- Adjust grid with side 2 next to the airfoil (for blunt t.e.) --
c----------------------------------------------------------------------
c     -- adjust grid in either x or y direction --
c     -- note: g is the x or y coordinate array to be modified --
c     --       ixy = 1 denotes x direction --
c     --           = 2 denotes y direction --
c     -- note: the original adjgrd is suitable for side 1 and 3, adjgrdbk2
c              is written for side 2 and 4
c     -- j. liu, may 2004 --
c----------------------------------------------------------------------

      subroutine adjgrdsd2(node, ip,  ifoil, js, je, ks, ke, istep, ixy,
     &     jdim, kdim, g, xo, yo, seg)

      implicit none

c     -- these includes are necessary to access bap array --
#include "../include/parms.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/common.inc"

      integer node, ip,  ifoil, js, je, ks, ke, istep, ixy, jdim, kdim
      integer ifirst,ilast,kstep,jstep,iiend,i,j,k
      double precision g(jdim,kdim), xo(jdim,kdim), yo(jdim,kdim)
      double precision seg(jdim*kdim),delta,tmp

      ifirst = 0
      ilast = 0
      kstep = istep
      jstep = -istep

      ip = node
      do k = ks, ke, kstep

         delta = bap(ip,ixy,ifoil) - g(js,k)
         ip = ip + 1
         if ( abs( delta ) .gt. 1.e-15) then
            ilast = k
            if (ifirst.eq.0) ifirst = k

c     -- calculate arclength for j-line at location k --
            i = 1
            seg(i) = 0.0
            do j = js+jstep, je, jstep
               tmp = sqrt( (xo(j,k) - xo(j-jstep,k) )**2 + 
     &              ( yo(j,k) - yo(j-jstep,k) )**2 )
               i = i + 1
               seg(i) = seg(i-1) + tmp
            end do
            iiend = i
            i = 1
            do j = js, je, jstep
               g(j,k) = g(j,k) + delta/2.0*(1.0 + cos(pi*seg(i)
     &              /seg(iiend)) )
c     g(j,k) = g(j,k) + delta*( 1.0 - seg(i)/seg(iiend) )
               i = i + 1
            end do
         end if
      end do

      return
      end                       !adjgrdsd2

