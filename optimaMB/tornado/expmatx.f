      subroutine expmatx(jdim,kdim,q,coef2x,coef4x,sndsp,s,
     &     spect,xyj,press,ccx,uu,xy,temp,temp2,work,s1,s2,
     &     u,v,phi,h,jbegin,jend,kbegin,kend)

      implicit none

#include "../include/common.inc"

      integer jdim,kdim,jbegin,jend,kbegin,kend,g,t,j,k,n,jlow,jup
      integer jm1,jp1,jp2,jl,ju
      double precision q(jdim,kdim,4),coef2x(jdim,kdim)
      double precision sndsp(jdim,kdim),s(jdim,kdim,4),press(jdim,kdim)
      double precision spect(jdim,kdim,3),xyj(jdim,kdim)
      double precision ccx(jdim,kdim),uu(jdim,kdim),xy(jdim,kdim,4)
      double precision temp(jdim,kdim,4),temp2(jdim,kdim,4),v(jdim,kdim)
      double precision work(4,jdim,kdim,2),coef4x(jdim,kdim)
      double precision s1(jdim,kdim),s2(jdim,kdim),u(jdim,kdim)
      double precision phi(jdim,kdim),h(jdim,kdim)
      double precision dtd,rhoinv,a1,a2,cs,dot1,dot2,fact1,fact2
c
c
      jlow=jbegin+1
      jup=jend-1
c
      dtd=dt/(1.d0+phidt)
c     (q(j+1)-q(j))
      do 11 n=1,4
        do 14 k=kbegin,kend
        do 14 j=jbegin,jup
          work(n,j,k,1)= q(j+1,k,n)*xyj(j+1,k) - q(j,k,n)*xyj(j,k)
 14     continue
c             
        j=jend
        jm1=j-1
        do 15 k=kbegin,kend
          work(n,j,k,1)=work(n,jm1,k,1)
 15     continue
c      
c       (q(j+2)-3q(j+1)+3q(j)-q(j-1))
        do 18 k=kbegin,kend
        do 18 j=jlow,jup
          work(n,j,k,2)=work(n,j-1,k,1)-2.*work(n,j,k,1)+work(n,j+1,k,1)
 18     continue
c       
c       boundary conditions
        j=jbegin
        jp1=j+1
        jp2=j+2
        do 20 k=kbegin,kend
          work(n,j,k,2)= q(j,k,n)*xyj(j,k)
     &              - 2.0*q(jp1,k,n)*xyj(jp1,k) + q(jp2,k,n)*xyj(jp2,k)
c 
          work(n,jend,k,2)=0.0
 20     continue
 11   continue
cc
cc
cc
      do 100 k=kbegin,kend
      do 100 j=jbegin,jend
        rhoinv=1.d0/q(j,k,1)
        s1(j,k)=.5d0*(spect(j,k,2)+spect(j,k,3)-2.*spect(j,k,1))
        s2(j,k)=(spect(j,k,2)-spect(j,k,3))/(2.*ccx(j,k))
        u(j,k)=q(j,k,2)*rhoinv
        v(j,k)=q(j,k,3)*rhoinv
        phi(j,k)=.5d0*(u(j,k)**2+v(j,k)**2)         
        h(j,k)=(q(j,k,4)+press(j,k))*rhoinv
 100  continue
c
      if (iord.eq.4) then
        jl=jlow+1
        ju=jup-1
      else
        jl=jlow
        ju=jup
      endif
c
      if (dis2x.eq.0) then
        do 103 n=1,4
        do 103 k=kbegin,kend
        do 103 j=jbegin,jup
          temp(j,k,n)=0.
 103    continue
      endif
c
      do 888 g=0,1
        if (dis2x.eq.0) then
c         -by-pass second-difference computation
          do 105 k=kbegin,kend
          do 105 j=jbegin,jup
            t=j+g
            a1=xy(t,k,1)
            a2=xy(t,k,2)
            cs=sndsp(t,k)**2
c       
c     -for fourth difference
            dot1=phi(t,k)*work(1,j,k,2)
     &           -u(t,k)*work(2,j,k,2)
     &           -v(t,k)*work(3,j,k,2)
     &           + work(4,j,k,2)
            dot2=-uu(t,k)*work(1,j,k,2)
     &           + a1*work(2,j,k,2)
     &           + a2*work(3,j,k,2)
c     dot1=ddot(4,r2,1,work(1,j,k,2),1)
c     dot2=ddot(4,r4,1,work(1,j,k,2),1)
c     
            fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
            fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c     
            temp2(j,k,1)=spect(t,k,1)*work(1,j,k,2)
     &           +fact1                   
            temp2(j,k,2)=spect(t,k,1)*work(2,j,k,2)
     &           +fact1*u(t,k)                 
     &           +fact2*a1                
            temp2(j,k,3)=spect(t,k,1)*work(3,j,k,2)
     &           +fact1*v(t,k)                 
     &           +fact2*a2                
            temp2(j,k,4)=spect(t,k,1)*work(4,j,k,2)
     &           +fact1*h(t,k)
     &           +fact2*uu(t,k)
 105     continue
        else
          do 111 k=kbegin,kend
          do 111 j=jbegin,jup
            t=j+g
c#include    "matloopx.inc"
            a1=xy(t,k,1)
            a2=xy(t,k,2)
            cs=sndsp(t,k)**2
c       
c     -for second difference
            dot1=phi(t,k)*work(1,j,k,1)
     &           -u(t,k)*work(2,j,k,1)
     &           -v(t,k)*work(3,j,k,1)
     &           + work(4,j,k,1)
            dot2=-uu(t,k)*work(1,j,k,1)
     &           + a1*work(2,j,k,1)
     &           + a2*work(3,j,k,1)
c     
c     -tried using blas functions => slower
c     dot1=ddot(4,r2,1,work(1,j,k,1),1)
c     dot2=ddot(4,r4,1,work(1,j,k,1),1)
c
            fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
            fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c     
            temp(j,k,1)=spect(t,k,1)*work(1,j,k,1)
     &           +fact1                   
            temp(j,k,2)=spect(t,k,1)*work(2,j,k,1)
     &           +fact1*u(t,k)                
     &           +fact2*a1                
            temp(j,k,3)=spect(t,k,1)*work(3,j,k,1)
     &           +fact1*v(t,k)                
     &           +fact2*a2                
            temp(j,k,4)=spect(t,k,1)*work(4,j,k,1)
     &           +fact1*h(t,k)
     &           +fact2*uu(t,k)
c     -for fourth difference
            dot1=phi(t,k)*work(1,j,k,2)
     &           -u(t,k)*work(2,j,k,2)
     &           -v(t,k)*work(3,j,k,2)
     &           + work(4,j,k,2)
            dot2=-uu(t,k)*work(1,j,k,2)
     &           + a1*work(2,j,k,2)
     &           + a2*work(3,j,k,2)
c     dot1=ddot(4,r2,1,work(1,j,k,2),1)
c     dot2=ddot(4,r4,1,work(1,j,k,2),1)
c       
            fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
            fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c     
            temp2(j,k,1)=spect(t,k,1)*work(1,j,k,2)
     &           +fact1                   
            temp2(j,k,2)=spect(t,k,1)*work(2,j,k,2)
     &           +fact1*u(t,k)                 
     &           +fact2*a1                
            temp2(j,k,3)=spect(t,k,1)*work(3,j,k,2)
     &           +fact1*v(t,k)                 
     &           +fact2*a2                
            temp2(j,k,4)=spect(t,k,1)*work(4,j,k,2)
     &           +fact1*h(t,k)
     &           +fact2*uu(t,k)
 111      continue
       endif
c       
        do 222 n=1,4
          do 140 k=kbegin,kend
          do 140 j=jl,ju
            t=j+g
            s(j,k,n)=s(j,k,n) + dtd*(
     &           coef2x(t,k)* temp(j,k,n)-coef2x(t-1,k)* temp(j-1,k,n)
     &        - (coef4x(t,k)*temp2(j,k,n)-coef4x(t-1,k)*temp2(j-1,k,n)))
 140      continue
 222    continue
 888  continue
c
      if (iord.eq.4) then
c       -Boundary Conditions
        do 2000 n=1,4
        do 2000 k=kbegin,kend
          j=jlow
          work(n,jbegin,k,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j+1,k,n)*
     &                       xyj(j+1,k)-q(j+2,k,n)*xyj(j+2,k)
          j=jbegin
          work(n,jlow,k,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j+1,k,n)*xyj(j+1,k)
     &                     -q(j+2,k,n)*xyj(j+2,k)
          j=jup-1
          work(n,j,k,2)=-q(j-1,k,n)*xyj(j-1,k)+2.d0*q(j,k,n)*xyj(j,k)
     &                  -q(j+1,k,n)*xyj(j+1,k)
          j=jup
          work(n,j,k,2)=-q(j-1,k,n)*xyj(j-1,k)+2.d0*q(j,k,n)*xyj(j,k)
     &                  -q(j+1,k,n)*xyj(j+1,k)
 2000   continue
c     
        do 2500 g=0,1
          do 2400 k=kbegin,kend
            do 2250 j=jbegin,jlow
              t=j+g
c#include      "matloopx.inc"
              a1=xy(t,k,1)
              a2=xy(t,k,2)
              cs=sndsp(t,k)**2
c     
c     -for second difference
              dot1=phi(t,k)*work(1,j,k,1)
     &             -u(t,k)*work(2,j,k,1)
     &             -v(t,k)*work(3,j,k,1)
     &             + work(4,j,k,1)
              dot2=-uu(t,k)*work(1,j,k,1)
     &             + a1*work(2,j,k,1)
     &             + a2*work(3,j,k,1)
c     
c     -tried using blas functions => slower
c     dot1=ddot(4,r2,1,work(1,j,k,1),1)
c     dot2=ddot(4,r4,1,work(1,j,k,1),1)
c     
              fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
              fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c     
              temp(j,k,1)=spect(t,k,1)*work(1,j,k,1)
     &             +fact1                   
              temp(j,k,2)=spect(t,k,1)*work(2,j,k,1)
     &             +fact1*u(t,k)                
     &             +fact2*a1                
              temp(j,k,3)=spect(t,k,1)*work(3,j,k,1)
     &             +fact1*v(t,k)                
     &             +fact2*a2                
              temp(j,k,4)=spect(t,k,1)*work(4,j,k,1)
     &             +fact1*h(t,k)
     &             +fact2*uu(t,k)
c     -for fourth difference
              dot1=phi(t,k)*work(1,j,k,2)
     &             -u(t,k)*work(2,j,k,2)
     &             -v(t,k)*work(3,j,k,2)
     &             + work(4,j,k,2)
              dot2=-uu(t,k)*work(1,j,k,2)
     &             + a1*work(2,j,k,2)
     &             + a2*work(3,j,k,2)
c     dot1=ddot(4,r2,1,work(1,j,k,2),1)
c     dot2=ddot(4,r4,1,work(1,j,k,2),1)
c       
              fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
              fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c     
              temp2(j,k,1)=spect(t,k,1)*work(1,j,k,2)
     &             +fact1                   
              temp2(j,k,2)=spect(t,k,1)*work(2,j,k,2)
     &             +fact1*u(t,k)                 
     &             +fact2*a1                
              temp2(j,k,3)=spect(t,k,1)*work(3,j,k,2)
     &             +fact1*v(t,k)                 
     &             +fact2*a2                
              temp2(j,k,4)=spect(t,k,1)*work(4,j,k,2)
     &             +fact1*h(t,k)
     &             +fact2*uu(t,k)
 2250       continue
            do 2275 j=jup-1,jup
              t=j+g
c#include      "matloopx.inc"
              a1=xy(t,k,1)
              a2=xy(t,k,2)
              cs=sndsp(t,k)**2
c       
c     -for second difference
              dot1=phi(t,k)*work(1,j,k,1)
     &             -u(t,k)*work(2,j,k,1)
     &             -v(t,k)*work(3,j,k,1)
     &             + work(4,j,k,1)
              dot2=-uu(t,k)*work(1,j,k,1)
     &             + a1*work(2,j,k,1)
     &             + a2*work(3,j,k,1)
c     
c     -tried using blas functions => slower
c     dot1=ddot(4,r2,1,work(1,j,k,1),1)
c     dot2=ddot(4,r4,1,work(1,j,k,1),1)
c
              fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
              fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c     
              temp(j,k,1)=spect(t,k,1)*work(1,j,k,1)
     &             +fact1                   
              temp(j,k,2)=spect(t,k,1)*work(2,j,k,1)
     &             +fact1*u(t,k)                
     &             +fact2*a1                
              temp(j,k,3)=spect(t,k,1)*work(3,j,k,1)
     &             +fact1*v(t,k)                
     &             +fact2*a2                
              temp(j,k,4)=spect(t,k,1)*work(4,j,k,1)
     &             +fact1*h(t,k)
     &             +fact2*uu(t,k)
c     -for fourth difference
              dot1=phi(t,k)*work(1,j,k,2)
     &             -u(t,k)*work(2,j,k,2)
     &             -v(t,k)*work(3,j,k,2)
     &             + work(4,j,k,2)
              dot2=-uu(t,k)*work(1,j,k,2)
     &             + a1*work(2,j,k,2)
     &             + a2*work(3,j,k,2)
c     dot1=ddot(4,r2,1,work(1,j,k,2),1)
c     dot2=ddot(4,r4,1,work(1,j,k,2),1)
c       
              fact1=(s1(t,k)*dot1*(gami/cs)+s2(t,k)*dot2)
              fact2=((s1(t,k)*dot2)/(a1**2+a2**2)+s2(t,k)*gami*dot1)
c     
              temp2(j,k,1)=spect(t,k,1)*work(1,j,k,2)
     &             +fact1                   
              temp2(j,k,2)=spect(t,k,1)*work(2,j,k,2)
     &             +fact1*u(t,k)                 
     &             +fact2*a1                
              temp2(j,k,3)=spect(t,k,1)*work(3,j,k,2)
     &             +fact1*v(t,k)                 
     &             +fact2*a2                
              temp2(j,k,4)=spect(t,k,1)*work(4,j,k,2)
     &             +fact1*h(t,k)
     &             +fact2*uu(t,k)
 2275       continue
c     
            do 2300 n=1,4
            do 2300 j=jlow,jup,jup-jlow
              t=j+g
c
              s(j,k,n)=s(j,k,n) + dtd*(
     &         coef2x(t,k)* temp(j,k,n)-coef2x(t-1,k)* temp(j-1,k,n)
     &      - (coef4x(t,k)*temp2(j,k,n)-coef4x(t-1,k)*temp2(j-1,k,n)))
 2300       continue
 2400     continue
 2500   continue
      endif
      return
      end
