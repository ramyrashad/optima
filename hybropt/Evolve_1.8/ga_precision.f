      subroutine ga_precision(nchrom,ngene,pop,igeneprec,debug)
      dimension pop(ngene,nchrom),igeneprec(ngene)
      logical debug
c     fixes precision of data to igeneprec decimals
      if(debug)then
         write(49,*)'before precision operation'
         write(49,*)' nc          pop = '
         do nc = 1,nchrom
            write(49,'(i5,3(2x,e24.16))')
     $           nc,(pop(ng,nc),ng=1,ngene)
         enddo
      endif

      do ng = 1,ngene
         i = abs(igeneprec(ng))
         p1 = 10.0**(i)
         p2 = 10.0**(-i)
         do nc = 1,nchrom
            c = abs(pop(ng,nc))*p1
c$$$            d= float(int(c))
            d = aint(c)
            if(d.lt.0)then
               print *,'conversion failed int too big'
            else
               pop(ng,nc) = sign(1.0,pop(ng,nc))*d*p2
            endif
         enddo
      enddo
      if(debug)then
         write(49,*)'after precision operation'
         write(49,*)' nc          pop = '
         do nc = 1,nchrom
            write(49,'(i5,3(2x,e24.16))')
     $           nc,(pop(ng,nc),ng=1,ngene)
         enddo
      endif
      return
      end
      
