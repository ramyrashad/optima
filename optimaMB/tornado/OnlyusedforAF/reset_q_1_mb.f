c
         do k=kbegin,kend
            do j=jbegin,jend
               rho = q(j,k,1)*xyj(j,k)
               u   = q(j,k,2)/q(j,k,1)
               v   = q(j,k,3)/q(j,k,1)
               pressure= gami*(q(j,k,4)*xyj(j,k) - .5*rho*(u**2+v**2))
               q(j,k,1) = rho
               q(j,k,2) = u
               q(j,k,3) = v
               q(j,k,4) = pressure
               tt = gamma*q(j,k,4)/q(j,k,1)
               fnu(j,k) = c2bp*tt*sqrt(tt)/( c2b+tt)/q(j,k,1)/re
c
c     removing local time stepping
c
c               ds(j,k)=1.0
            enddo
         enddo
