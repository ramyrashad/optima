c
c convection terms  for the k-equation in all blocks (explicit and implicit)
c on rhs of equation
c
      do k=klow,kup
         do j=jlow,jup
c
c     in the xi direction
c
            uc=q(j,k,2)*xy(j,k,1)+q(j,k,3)*xy(j,k,2)
            ucplus=uc/2.*(1.+sign(1.,uc))
            ucminus=uc/2.*(1.-sign(1.,uc))
c
            bx_k(j,k)=ucplus*q(j,k,1)
            cx_k(j,k)=(-ucplus+ucminus)*q(j,k,1)
            dx_k(j,k)=-ucminus*q(j,k,1)
            rhs_k(j,k)=bx_k(j,k)*vk(j-1,k)
     $         +cx_k(j,k)*vk(j,k)+dx_k(j,k)*vk(j+1,k)
c
            bx_e(j,k)=ucplus*q(j,k,1)
            cx_e(j,k)=(-ucplus+ucminus)*q(j,k,1)
            dx_e(j,k)=-ucminus*q(j,k,1)
            rhs_e(j,k)=bx_e(j,k)*ve(j-1,k)
     $         +(cx_e(j,k))*ve(j,k)+dx_e(j,k)*ve(j+1,k)
c
c           in the eta direction
c
            vc=q(j,k,2)*xy(j,k,3)+q(j,k,3)*xy(j,k,4)
            vcplus=vc/2.*(1.+sign(1.,vc))
            vcminus=vc/2.*(1.-sign(1.,vc))
c
            by_k(j,k)=vcplus*q(j,k,1)
            cy_k(j,k)=(-vcplus+vcminus)*q(j,k,1)
            dy_k(j,k)=-vcminus*q(j,k,1)
            rhs_k(j,k)=rhs_k(j,k)+by_k(j,k)*vk(j,k-1)
     $         +(cy_k(j,k))*vk(j,k)+dy_k(j,k)*vk(j,k+1)
c
            by_e(j,k)=vcplus*q(j,k,1)
            cy_e(j,k)=(-vcplus+vcminus)*q(j,k,1)
            dy_e(j,k)=-vcminus*q(j,k,1)
            rhs_e(j,k)=rhs_e(j,k)+by_e(j,k)*ve(j,k-1)
     $         +(cy_e(j,k))*ve(j,k)+dy_e(j,k)*ve(j,k+1)
         enddo
      enddo
