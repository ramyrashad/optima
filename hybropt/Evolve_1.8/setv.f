c======================================================================
c
      subroutine setv(m,s,f)

C     ARC2D V3.00 by Thomas H. Pulliam
C     NASA Ames Research Center
C     Copyright 1992 
C     Restricted to United States (NO Foreign Dissemination!!)

      dimension f(m)
      do i= 1,m
         f(i)= s
      enddo

      return
      end
