      subroutine xidif(jdim,kdim,x,y,xy)
c
#include "../include/arcom.inc"
c
      dimension xy(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim)
c
c
c     xy4 =xxi,xy3 = yxi
c
      if (iord.eq.4) then
         tmp=1.d0/12.d0
         if (periodic) then
            do 1 k=kbegin,kend
            do 1 j=jlow,jup
            jp1=jplus(j)
            jp2=jplus(jp1)
            jm1=jminus(j)
            jm2=jminus(jm1)
            xy(j,k,4)= tmp*(-x(jp2,k)+8.d0*(x(jp1,k)-x(jm1,k))+x(jm2,k))
            xy(j,k,3)= tmp*(-y(jp2,k)+8.d0*(y(jp1,k)-y(jm1,k))+y(jm2,k))
 1          continue
         else
            do 6 k=kbegin,kend
               do 5 j=jbegin+2,jend-2
                  xy(j,k,4)= tmp*
     &                     (-x(j+2,k)+8.d0*(x(j+1,k)-x(j-1,k))+x(j-2,k))
                  xy(j,k,3)= tmp*
     &                     (-y(j+2,k)+8.d0*(y(j+1,k)-y(j-1,k))+y(j-2,k))
c                  write(99,77) j,k,xy(j,k,3),xy(j,k,4)
 5             continue
c
               j = jbegin+1
c              -second order
c              xy(j,k,4) = (x(j+1,k) - x(j-1,k))*.5d0
c              xy(j,k,3) = (y(j+1,k) - y(j-1,k))*.5d0
c              - third order
               xy(j,k,4)=2.d0*tmp*(-2.d0*x(j-1,k) - 3.d0*x(j,k) +
     &                                  6.d0*x(j+1,k) - x(j+2,k))
               xy(j,k,3)=2.d0*tmp*(-2.d0*y(j-1,k) - 3.d0*y(j,k) +
     &                                  6.d0*y(j+1,k) - y(j+2,k))
c               xy(j,k,4)=tmp*(-5.d0*x(j-1,k) - 2.d0*x(j,k) +
c     &              6.d0*x(j+1,k) + 2.d0*x(j+2,k) - x(j+3,k))
c               xy(j,k,3)=tmp*(-5.d0*y(j-1,k) - 2.d0*y(j,k) +
c     &              6.d0*y(j+1,k) + 2.d0*y(j+2,k) - y(j+3,k))
c
               j = jend-1
c              -second order
c              xy(j,k,4) = (x(j+1,k) - x(j-1,k))*.5d0
c              xy(j,k,3) = (y(j+1,k) - y(j-1,k))*.5d0
c              - third order
               xy(j,k,4)=2.d0*tmp*(x(j-2,k) - 6.d0*x(j-1,k) +
     &                                  3.d0*x(j,k) + 2.d0*x(j+1,k))
               xy(j,k,3)=2.d0*tmp*(y(j-2,k) - 6.d0*y(j-1,k) +
     &                                  3.d0*y(j,k) + 2.d0*y(j+1,k))
c               xy(j,k,4)=tmp*(x(j-3,k) - 2.d0*x(j-2,k) -
c     &               6.d0*x(j-1,k) + 2.d0*x(j,k) + 5.d0*x(j+1,k))
c               xy(j,k,3)=tmp*(y(j-3,k) - 2.d0*y(j-2,k) -
c     &               6.d0*y(j-1,k) + 2.d0*y(j,k) + 5.d0*y(j+1,k))
c
c              -second order
c              j = jbegin
c              xy(j,k,4) = .5d0*( -3.d0*x(j,k) +4.d0*x(j+1,k) - x(j+2,k))
c              xy(j,k,3) = .5d0*( -3.d0*y(j,k) +4.d0*y(j+1,k) - y(j+2,k))
c              j = jend
c              xy(j,k,4) = ( 3.d0*x(j,k) -4.d0*x(j-1,k) + x(j-2,k))*.5d0
c              xy(j,k,3) = ( 3.d0*y(j,k) -4.d0*y(j-1,k) + y(j-2,k))*.5d0
cc              -third order
               j = jbegin
               xy(j,k,4)=2.d0*tmp*(-11.d0*x(j,k) + 18.d0*x(j+1,k) -
     &                                  9.d0*x(j+2,k) + 2.d0*x(j+3,k))
               xy(j,k,3)=2.d0*tmp*(-11.d0*y(j,k) + 18.d0*y(j+1,k) -
     &                                  9.d0*y(j+2,k) + 2.d0*y(j+3,k))
c
c              - third order
               j = jend
               xy(j,k,4)=2.d0*tmp*(11.d0*x(j,k) - 18.d0*x(j-1,k) +
     &                                  9.d0*x(j-2,k) - 2.d0*x(j-3,k))
               xy(j,k,3)=2.d0*tmp*(11.d0*y(j,k) - 18.d0*y(j-1,k) +
     &                                  9.d0*y(j-2,k) - 2.d0*y(j-3,k))
 6          continue
         endif
c         if (cmesh) then
c            k=1
c            j=jtail1-1
c              xy(j,k,4)=2.d0*tmp*(x(j-2,k) - 6.d0*x(j-1,k)
c     &                                    + 3.d0*x(j,k) + 2.d0*x(j+1,k))
c              xy(j,k,3)=2.d0*tmp*(y(j-2,k) - 6.d0*y(j-1,k)
c     &                                    + 3.d0*y(j,k) + 2.d0*y(j+1,k))
c            j=jtail2+1
c              xy(j,k,4)=2.d0*tmp*(-2.d0*x(j-1,k) - 3.d0*x(j,k) +
c     &                                         6.d0*x(j+1,k) - x(j+2,k))
c              xy(j,k,3)=2.d0*tmp*(-2.d0*y(j-1,k) - 3.d0*y(j,k) +
c     &                                         6.d0*y(j+1,k) - y(j+2,k))
c         endif
c         do 20 j=jlow,jup
c            jp1 = jplus(j)
c            jm1 = jminus(j)
c            xy(j,k,4) = (x(jp1,k) - x(jm1,k))*.5d0
c            xy(j,k,3) = (y(jp1,k) - y(jm1,k))*.5d0
c 20      continue
cc         do 21 k=1,2
c            j = jbegin
c            xy(j,k,4) = .5d0*( -3.d0*x(j,k) +4.d0*x(j+1,k) - x(j+2,k))
c            xy(j,k,3) = .5d0*( -3.d0*y(j,k) +4.d0*y(j+1,k) - y(j+2,k))
c            j = jend
c            xy(j,k,4) = ( 3.d0*x(j,k) -4.d0*x(j-1,k) + x(j-2,k))*.5d0
c            xy(j,k,3) = ( 3.d0*y(j,k) -4.d0*y(j-1,k) + y(j-2,k))*.5d0
cc 21      continue
      else
         do 11 k=kbegin,kend
            do 10 j=jlow,jup
               jp1 = jplus(j)
               jm1 = jminus(j)
               xy(j,k,4) = (x(jp1,k) - x(jm1,k))*.5d0
               xy(j,k,3) = (y(jp1,k) - y(jm1,k))*.5d0
c              write(99,77) j,k,xy(j,k,3),xy(j,k,4)
 10         continue
C
            if(.not.periodic)then
              j = jbegin
              xy(j,k,4) = .5d0*( -3.d0*x(j,k) +4.d0*x(j+1,k) - x(j+2,k))
              xy(j,k,3) = .5d0*( -3.d0*y(j,k) +4.d0*y(j+1,k) - y(j+2,k))
              j = jend
              xy(j,k,4) = ( 3.d0*x(j,k) -4.d0*x(j-1,k) + x(j-2,k))*.5d0
              xy(j,k,3) = ( 3.d0*y(j,k) -4.d0*y(j-1,k) + y(j-2,k))*.5d0
            endif
   11    continue
      endif
 77   format(2I5,2e25.15)
      return
      end
