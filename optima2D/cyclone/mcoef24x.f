      subroutine mcoef24x(JDIM,KDIM,COEF2x,COEF4x,pCOEF)           
C
#include "../include/arcom.inc"
c                                                                       
      dimension coef2x(jdim,kdim),coef4x(jdim,kdim)
      dimension pcoef(jdim,kdim)
c
c     coef2x comes in as coefx (pressure gradiant coefficient)
c     coef4x comes in as specx (spectral radius)
c                                                                       
c                                                                       
      do 2 k = kbegin,kend                                              
      do 2 j = jbegin,jend                                              
         pcoef(j,k) = coef2x(j,k)                                       
2     continue                                                          
c                                                                       
      eps4x = dis4x
      eps2x = dis2x                                                    
c                                                                       
c     form 2nd and 4th order dissipation coefficients in y
c                                                                       
      do 20 k = kbegin,kend                                            
      do 20 j = jbegin,jend                                             
         c2=eps2x*pcoef(j,k)
         c4=eps4x-min(eps4x,c2)
         coef2x(j,k)=c2
         coef4x(j,k)=c4
20    continue                                                       
                                                                        
      return                                                         
      end                                                            
