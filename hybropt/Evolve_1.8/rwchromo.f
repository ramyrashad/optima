C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C   RRRRR   W     W   CCCC   H    H  RRRRR    OOOO   M     M   OOOO    C
C   R    R  W  W  W  C    C  H    H  R    R  O    O  MM   MM  O    O   C
C   RRRRR   W W W W  C       HHHHHH  RRRRR   O    O  M M M M  O    O   C
C   R    R  WW   WW  C    C  H    H  R   R   O    O  M  M  M  O    O   C
C   R    R  W     W   CCCC   H    H  R    R   OOOO   M     M   OOOO    C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE RWCHROMO(nchrom,NGENE,nobfun,POP,FIT,
     $     NPI,IPARM,IPARM_chm,NPR,PARMR,
     $     PARMR_chm,NPL,PARML,PARML_chm,
     $     IPOP,IRW,NOUT,fitmin,fitmax,ga_file_prefix,
     $     cputime,nfuneval,cputi,nfun,nbest,imove,cbest,debug,
     $     io_34,fit_min_max,arc2d_io,field_pop,field_fit,field_log,
     $     field_ngene,field_nobfun, compute_fit, compute_pass_thu)
      LOGICAL FILEX,debug,io_34,fit_min_max,arc2d_io
      LOGICAL PARML,cbest,parml_chm,compute_pass_thu
      character*80 ga_file_prefix
      integer strlen
      character*80 filena
      SAVE
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** THIS SUBROUTINE READS THE FILE numopt.dat,              C
C              WHICH IS USED BY SUBROUTINES SURFMOD AND                C
C              PARSEC TO ESTABLISH WING SURFACE DEFINITIONS ***        C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      DIMENSION pop(ngene,nchrom)
      DIMENSION fit(nobfun,nchrom)
      DIMENSION IPARM(20), PARMR(20), PARML(10)
      DIMENSION IPARM_chm(20,nchrom), PARMR_chm(20,nchrom), 
     $     PARML_chm(10,nchrom)
      dimension fitmin(10),fitmax(10)
      dimension cputi(nchrom),nfun(nchrom)
      logical compute_fit(nchrom)
      character*40 field_pop(ngene),field_fit(nobfun),field_log,
     $     field_ngene,field_nobfun
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** FIRST HALF OF SUBROUTINE EXECUTES A chromo.i 
C              READ (IRW=1) AND THE SECOND HALF EXECUTES A 
C              WRITE (IRW=2) ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** IRW=1...READ chromo.i FILE ***
C-----------------------------------------------------------------------
C          *** CHECK FOR EXISTENCE OF FILE (chromo.i). IF
C              IT DOES NOT EXIST, WRITE ERROR MESSAGE AND 
C              TERMINATE EXECUTION ***

      if(debug.and. irw.eq.1)
     $     write(49,*)' Call to rwchromo: reading in chromosomes'
      if(debug.and. irw.eq.2)
     $     write(49,*)' Call to rwchromo: writing out chromosomes'
C

      do nc = 1,nchrom

         namelen =  strlen(ga_file_prefix)
         filena = ga_file_prefix
         filena(namelen+1:namelen+2) = '.'
         CALL INTCHA( nc,filena(namelen+2:),LQFILE )
         filelen =  strlen(filena)

         IF (IRW.EQ.1) THEN
            INQUIRE (FILE=filena,EXIST=FILEX)
            IF (.NOT.FILEX) THEN
               OPEN (50,FILE='chromo.error',STATUS='UNKNOWN',
     $              FORM='FORMATTED')
               WRITE (50,2001)
               close(50)
               WRITE (*,2001)
               if(io_34)write(34,2001) NC
 2001          FORMAT ('RWCHROMO: CANNOT FIND filename.',I3,
     1              ', EXECUTION TERMINATED'/)
               STOP
            ENDIF
C
C-----------------------------------------------------------------------
C          *** OPEN FILE AND EXECUTE READ ***
C
            OPEN (4,FILE=filena,STATUS='UNKNOWN',FORM='FORMATTED')
            if(arc2d_io)then
               READ(4,*,ERR=1001) Ngn
               READ(4,*,ERR=1001) (pop(NN,nc),NN=1,Ngn)
               READ(4,*,ERR=1001) Nfn
               READ(4,*,ERR=1001) (fit(NN,nc),NN=1,Nfn)
               READ(4,*,ERR=1001) NPI
               IF (NPI.GT.0) THEN
                  READ(4,*,ERR=1001) (IPARM_chm(NN,nc),NN=1,NPI)
               ENDIF
               READ(4,*,ERR=1001) NPR
               IF (NPR.GT.0) THEN
                  READ(4,*,ERR=1001) (PARMR_chm(NN,nc),NN=1,NPR)
               ENDIF
               READ(4,*,ERR=1001) NPL
               IF (NPL.GT.0) THEN
                  READ(4,*,ERR=1001) (PARML_chm(NN,nc),NN=1,NPL)
               ENDIF
cthp_2005               compute_fit(nc) = parml_chm(1,nc)
               if(fit_min_max)then
                  READ (4,*,ERR=1001) (fitmin(NN),NN=1,NOBFUN)
                  READ (4,*,ERR=1001) (fitmax(NN),NN=1,NOBFUN)
               endif
            else
               READ(4,*,Err=1001)field_ngene,ngn
               do ng = 1,ngn
                  READ(4,*,Err=1001)field_pop(ng),pop(ng,nc)
               enddo
               READ(4,*,Err=1001)field_nobfun,nfn
               do nf = 1,nfn
                  READ(4,*,Err=1001)field_fit(nf),fit(nf,nc)
               enddo
               read(4,*,Err=1001)field_log,parml_chm(1,nc) 
cthp_2005               compute_fit(nc) = parml_chm(1,nc)
            endif               !arc2d_io
            CLOSE (4)

C
C-----------------------------------------------------------------------
C          *** WRITE VALUES FROM chromo.i TO UNIT 34 BUT ONLY IF
C              NOUT>1 AND THEN RETURN ***
C
            IF (NOUT.GE.2) THEN
               IF (NC.LE.9) THEN
                  if(io_34)write(34,200) 
 200              FORMAT (/61(1H*))
                  if(io_34)write(34,201) NC,IPOP
 201              FORMAT ('RWCHROMO: GEOMETRY PARAMETERS FROM chromo.',
     1                 I1,'  GENERATION =',I4)
               ENDIF
               IF (NC.GT.9) THEN
                  if(io_34)write(34,200) 
                  if(io_34)write(34,202) NC,IPOP
 202              FORMAT ('RWCHROMO: GEOMETRY PARAMETERS FROM chromo.',
     1                 I2,' GENERATION =',I4)
               ENDIF
               if(io_34)write(34,203) Ngene
 203           FORMAT ('       NDV      =',I8)
               if(io_34)write(34,204) (pop(NN,nc),NN=1,Ngene)
 204           FORMAT ('       pop      =',10F8.5/20X,10F8.5/20X,10F8.5/
     1              20X,10F8.5/20X,10F8.5/20X,10F8.5/
     2              20X,10F8.5/20X,10F8.5/20X,10F8.5/
     3              20X,10F8.5/20X,10F8.5/20X,10F8.5)
               if(io_34)write(34,205) Nobfun
 205           FORMAT ('       NOBFUN   =',I8)
               if(io_34)write(34,206) (fit(NN,nc),NN=1,Nobfun)
 206           FORMAT ('       FIT    =',10F8.3)
               if(arc2d_io)then
                  if(io_34)write(34,207) NPI
 207              FORMAT ('          NPI      =',I8)
                  IF (NPI.GT.0) THEN
                     if(io_34)write(34,208) (IPARM_chm(NN,nc),NN=1,NPI)
 208                 FORMAT ('       IPARM    =',10I8)
                  ENDIF
                  if(io_34)write(34,209) NPR
 209              FORMAT ('          NPR      =',I8)
                  IF (NPR.GT.0) THEN
                     if(io_34)write(34,210) (PARMR_chm(NN,nc),NN=1,NPR)
 210                 FORMAT ('       PARMR    =',10F8.4)
                  ENDIF
                  if(io_34)write(34,211) NPL
 211              FORMAT ('          NPL      =',I8)
                  IF (NPL.GT.0) THEN
                     if(io_34)write(34,212) (PARML_chm(NN,nc),NN=1,NPL)
 212                 FORMAT ('       PARML    =',10L8)
                  ENDIF
               else
                     if(io_34)write(34,212) PARML_chm(1,nc)
               endif            !arc2d_io
            ENDIF
c$$$            RETURN
            go to 1002
C     
C-----------------------------------------------------------------------
C          *** WRITE ERROR MESSAGE INDICATING ERROR
C              DURING READ AND TERMINATE EXECUTION ***
C
 1001       CONTINUE
            CLOSE (4)
            OPEN (50,FILE='chromo.error',STATUS='UNKNOWN',
     $           FORM='FORMATTED')
            WRITE (50,2003)nc
            close(50)
            WRITE (*,2003)nc
            if(io_34)write(34,2003) NC
 2003       FORMAT ('RWCHROMO: READING ERROR DETECTED IN chromo.',I2,
     1           ', EXECUTION TERMINATED'/)
            STOP
 1002       Continue

            if(arc2d_io)then
               if(npr.gt.0)CPUTIME  = CPUTIME+PARMR_chm(1,nc)
               if(npi.gt.0)NFUNEVAL = NFUNEVAL+IPARM_chm(1,nc)
               if(npr.gt.0)CPUTI(NC) = PARMR_chm(1,nc)
               if(npi.gt.0)NFUN(NC)  = IPARM_chm(1,nc)
            endif

         ENDIF
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** IRW=2...WRITE chromo.i FILE ***
C
         IF (IRW.EQ.2) THEN
            if(npl.gt.0)then
cthp_2005c$$$               PARML_chm(1,nc) = .TRUE.
cthp_2005c$$$               IF (NC.LE.NBEST) PARML_chm(1,nc) = CBEST
cthp_2005c$$$               IF (NC.EQ.1.AND.IMOVE.EQ.1) PARML_chm(1,nc) = .TRUE.
               if(compute_pass_thu)then
                  PARML(1) = .TRUE.
                  IF (NC.LE.NBEST) PARML(1) = CBEST
               else
                  parml(1) = compute_fit(nc)
               endif

               if(debug)then
                  if(nc.eq.1)write(49,*)'nc compute_fit'
                  write(49,'(i5,L4)') nc,compute_fit(nc)
               endif
               IF (NC.EQ.1.AND.IMOVE.EQ.1) PARML(nc) = .TRUE.
            endif
c$$$            if(npr.gt.0)PARMR_chm(1,nc) = CPUTI(NC)
c$$$            if(npi.gt.0)IPARM_chm(1,nc) = NFUN(NC)
            if(npr.gt.0)PARMR(1) = CPUTI(NC)
            if(npi.gt.0)IPARM(1) = NFUN(NC)
            OPEN (4,FILE=filena,STATUS='UNKNOWN',FORM='FORMATTED')
            if(arc2d_io)then
               WRITE (4,'(8x,i6)') Ngene
               WRITE (4,'(3((2x,e24.16)))') (pop(NN,nc),NN=1,Ngene)
               WRITE (4,'(8x,i6)') Nobfun
               WRITE (4,'(3((2x,e24.16)))') (fit(NN,nc),NN=1,Nobfun)
               WRITE (4,'(8x,i6)') NPI
               IF (NPI.GT.0) THEN
                  WRITE (4,'(3((2x,i8)))') (IPARM(NN),NN=1,NPI)
               ENDIF
               WRITE (4,'(8x,i6)') NPR
               IF (NPR.GT.0) THEN
                  WRITE (4,'(3((2x,e24.16)))') (PARMR(NN),NN=1,NPR)
               ENDIF
               WRITE (4,'(8x,i6)') NPL
               IF (NPL.GT.0) THEN
                  WRITE (4,'(8((2x,L8)))') (PARML(NN),NN=1,NPL)
               ENDIF
               if(fit_min_max)then
                  WRITE (4,'(3((2x,e24.16)))') (fitmin(NN),NN=1,Nobfun)
                  WRITE (4,'(3((2x,e24.16)))') (fitmax(NN),NN=1,Nobfun)
               endif
            else
               Write(4,'(2x,a40,i8)')field_ngene,ngene
               do ng = 1,ngene
                  write(4,'(2x,a40,e16.8)')field_pop(ng),pop(ng,nc)
               enddo
               write(4,'(2x,a40,i8)')field_nobfun,nobfun
               do nf = 1,nobfun
                  write(4,'(2x,a40,e16.8)')field_fit(nf),fit(nf,nc)
               enddo
               write(4,'(2x,a40,L9)')field_log,parml(1)
            endif               !arc2d_io
            CLOSE (4)
         ENDIF
C
C-----------------------------------------------------------------------
      enddo
      if(debug)then
         write(49,*)'after operations'
         write(49,*)' nc          pop = '
         do nc = 1,nchrom
            write(49,'(i5,3(2x,e24.16))')
     $           nc,(pop(ng,nc),ng=1,ngene)
         enddo
         do nc = 1,nchrom
            write(49,*)'fit(',nc,') = ',(fit(no,nc),no=1,nobfun)
         enddo
      endif
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      RETURN
      END
