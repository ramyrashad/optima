c     ------------------------------------------------------------------
c     -- modify grid --
c     -- m. nemec --
c     ------------------------------------------------------------------
      subroutine modall (ifoil, flag, x, y, gapopt)

      implicit none

#include "mxblk.inc"
#include "mxfoil.inc"
#include "mxseg.inc"
#include "mxjk.inc"

      integer ifoil, maxjk
      parameter (maxjk=maxj*maxk)
      double precision x(maxjk), y(maxjk)
      logical flag, gapopt

      integer nblks, jbmax(maxblk), kbmax(maxblk)
      common/mblock/ nblks, jbmax, kbmax

      integer lgptr(maxblk)
      common/ptrs/ lgptr

      integer nfoils, nsegments(maxfoil), isegblk(maxsegs,maxfoil)
      integer isegside(maxsegs,maxfoil)
      common/sortedfoils/ nsegments, isegblk, isegside, nfoils

      integer lblkpt(maxblk,4), lsidept(maxblk,4), idir(maxblk,4)
      integer ibctype(maxblk,4), ibcdir(maxblk,4)
      common/connect/ lblkpt, lsidept, idir, ibctype, ibcdir

      integer nstg, istgblk(maxblk), istgtyp(maxblk), istgcnr(maxblk)
      common/stgpt/nstg, istgblk, istgtyp, istgcnr

c     -- local variables --
      integer node, i, iseg, ib, is
      integer iblock, iblkup, iblkdn, iblkfr, iblkaft
      double precision work2(maxjk), xo(maxjk), yo(maxjk)
      
      node = 1
c     -- adjust grid blocks with sides next to airfoil --
      write (*,10) 
 10   format (/3x,'Modifying blocks with sides in contact',
     &     ' with surface of airfoil...',/3x,'Block',3x,'Side',3x,
     &     'Node Index')
      do iseg=1,nsegments(ifoil)
         ib = isegblk(iseg,ifoil)
         is = isegside(iseg,ifoil)
         call modblk(node, ifoil, jbmax(ib), kbmax(ib), is,
     &        x(lgptr(ib)), y(lgptr(ib)), xo, yo, work2, flag)
         write (*,20) ib, is, node
      end do
 20   format (4x,i3,5x,i2,6x,i4)

c     -- modify blocks from l.e. stagnation points --
c     -- h-mesh topology --
      do i = 1,nstg

c     -- trap l.e. stagnation point --
         if (istgtyp(i).eq.1 .and. nblks.gt.4) then

c     -- find the upper and lower blocks in front of the l.e. point --
            if (istgcnr(i).eq.2) then
               iblkup = istgblk(i)
               iblkfr = lblkpt(istgblk(i),2)
               iblock = lblkpt(iblkfr,1)
               iblkdn = lblkpt(iblock,4)
c     write (*,*) 'L.E. is in LL corner of block',iblkup
c     write (*,*) 'Block ahead',iblkfr
c     write (*,*) 'Block under',iblock
c     write (*,*) 'Block down',iblkdn

c     -- check if l.e. point is on the current airfoil -- 
               flag = .false.
               do iseg=1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  if (is.eq.1 .and. ib.eq.iblkup) flag = .true.
               end do

            else
               stop 'modify: problem with l.e. stg. location!!'
            endif

            if (flag) then
c     -- modify l.e. block on upper surface --
               write (*,30) iblkfr, iblkup
               call modle( 1, jbmax(iblkfr), jbmax(iblkfr),
     &              jbmax(iblkfr)-1, 1, 1, -1, 1, kbmax(iblkup), 1,
     &              jbmax(iblkup), kbmax(iblkup), x(lgptr(iblkup)),
     &              y(lgptr(iblkup)), jbmax(iblkfr), kbmax(iblkfr),
     &              x(lgptr(iblkfr)), y(lgptr(iblkfr)), work2 )
c     -- modify l.e. block on lower surface --
               write (*,30) iblock, iblkdn
               call modle(  1, jbmax(iblkfr), jbmax(iblkfr),
     &              jbmax(iblkfr)-1, 1, 1, -1, kbmax(iblkdn), 1, -1,
     &              jbmax(iblkdn), kbmax(iblkdn), x(lgptr(iblkdn)),
     &              y(lgptr(iblkdn)), jbmax(iblock), kbmax(iblock),
     &              x(lgptr(iblock)), y(lgptr(iblock)), work2 )
 30            format (3x,'Modifying L. E. block',i3,
     &              ' based on block',i3)
            end if
         end if

c     -- trap t.e. stagnation point --
         if (istgtyp(i).eq.-1 .and. nblks.gt.4 .and. gapopt) then

c     -- find the upper and lower blocks aft of the t.e. point --
            if (istgcnr(i).eq.1) then
               iblkup  = istgblk(i)
               iblkaft = lblkpt(istgblk(i),4)
               iblock  = lblkpt(iblkaft,1)
               iblkdn  = lblkpt(iblock,2)
c               write (*,*) 'T.E. is in LR corner of block',iblkup
c               write (*,*) 'Block aft',iblkaft
c               write (*,*) 'Block under',iblock
c               write (*,*) 'Block down',iblkdn

c     -- check if t.e. point is on the current airfoil -- 
               flag = .false.
               do iseg=1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  if (is.eq.1 .and. ib.eq.iblkup) flag = .true.
               end do

            else
               stop 'modify: problem with t.e. stg. location!!'
            endif

            if (flag) then
c     -- modify t.e. block on upper surface --
               write (*,40) iblkaft, iblkup
               call modle( jbmax(iblkup), 1, 1, 1, jbmax(iblkaft), 
     &              jbmax(iblock)-1, 1, 1, kbmax(iblkup), 1,
     &              jbmax(iblkup), kbmax(iblkup), x(lgptr(iblkup)),
     &              y(lgptr(iblkup)), jbmax(iblkaft), kbmax(iblkaft),
     &              x(lgptr(iblkaft)), y(lgptr(iblkaft)), work2 )
c     -- modify t.e. block on lower surface --
               write (*,40) iblock, iblkdn
               call modle( jbmax(iblkdn), 1, 1, 1, jbmax(iblock),
     &              jbmax(iblock)-1, 1, kbmax(iblkdn), 1, -1,
     &              jbmax(iblkdn), kbmax(iblkdn), x(lgptr(iblkdn)),
     &              y(lgptr(iblkdn)), jbmax(iblock), kbmax(iblock),
     &              x(lgptr(iblock)), y(lgptr(iblock)), work2 )
 40            format (3x,'Modifying T. E. block',i3,
     &              ' based on block',i3)
            end if
         end if
      end do

      return
      end                       !modall
