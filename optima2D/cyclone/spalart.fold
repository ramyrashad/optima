c     ****************************************************************
c     *         Spalart-Allmaras one-equation turbulence model       *
c     ****************************************************************

      subroutine spalart(jdim, kdim, q, press, turmu, fmu, x, y, xy,
     &      xyj, uu, vv, delta_u, p, d, p1, d1, vorticity, p_prime,
     &      d_prime, trip, work, workq, fnu, d_p_bar, bx, cx, dx, by,
     &      cy, dy, fy, fx, ffy, ffx, fby)

c     -- coded by : p. godin --
c     -- mods : s. de rango 10/04/96 --
c     -program is more streamlined for efficiency and memory usage.
c     -not totally ... don't want to be fooling around
c                        with what "ain't" broke.
c     -lines commented out by me begin with 'csd'
c     -note: this subroutine was originally written for clarity and
c              debuging purposes.  Many expressions appear to be
c              repetitive.  I've tried using more memory to store
c              these frequently used expressions and save cpu time,
c              but our SGI compilers are aware of the redundancy and
c              optimize the code accordingly. 
c       -the majority of the changes benefits the unsteady runs more
c        than steady computations.  When using the dual time-stepping
c        scheme you must subiterate dozens of times, hence I have
c        tried to avoid having to calculate many of the variables in
c        each subiteration that don't change from subiteration to
c        subiteration.

#include "../include/arcom.inc"
#include "../include/sam.inc"

      dimension q(jdim,kdim,4),turmu(jdim,kdim)
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension fmu(jdim,kdim),turre2(maxj,maxk),uu(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim),vv(jdim,kdim)
      dimension tmpx(maxj,maxk),tmpy(maxj,maxk),workph(maxj,maxk)
      logical first

      dimension dtiseq(20),dtmins(20),dtow2(20)
      dimension jmxi(20),kmxi(20),jskipi(20),iends(20)
      common/mesup/ dtiseq,dtmins,dtow2,isequal,iseqlev,
     &              jmxi,kmxi,jskipi,iends

      dimension 
     &      p(jdim,kdim),d(jdim,kdim),delta_u(jdim,kdim),
     &      trip(jdim,kdim),d_prime(jdim,kdim),
     &      p_prime(jdim,kdim),d_p_bar(jdim,kdim),
     &      p1(jdim,kdim),d1(jdim,kdim),vorticity(jdim,kdim)

      dimension
     &      work(jdim,kdim),workq(jdim,kdim,3),fnu(jdim,kdim)

      dimension    
     &      bx(kdim,jdim),cx(kdim,jdim),dx(kdim,jdim),
     &      by(jdim,kdim),cy(jdim,kdim),dy(jdim,kdim),
     &      fy(jdim,kdim),fx(kdim,jdim),
     &      ffy(jdim,kdim),ffx(kdim,jdim),fby(jdim,kdim),
     &      denom(maxj,maxk)

c     -- frozen jacobian arrays --
      double precision c2xf(maxj,maxk), c4xf(maxj,maxk)
      double precision c2yf(maxj,maxk), c4yf(maxj,maxk)
      double precision fmuf(maxj,maxk), turmuf(maxj,maxk)
      double precision vortf(maxj,maxk), sapdf(maxj,maxk)
      double precision sadx(maxj,maxk), sady(maxj,maxk)
      double precision suuf(maxj,maxk), svvf(maxj,maxk)
      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &      sapdf, sadx, sady, suuf, svvf 

      isubmx=100
      if (meth.eq.2) isubmx=1000
      isub=0
c      expon=1.d0/6.d0
      reinv=1.d0/re
c      akarman = .41
c      cb1     = 0.1355
c      sigmainv= 1.5d0
      resiginv= sigmainv*reinv
c      cb2     = 0.622
c      cw1     = cb1/(akarman**2)+(1.0+cb2)*sigmainv
c      cw2     = 0.3
cc     -- underscore is equivalent to '**' i.e. exponentiation --
c      cw3     = 2.0
c      cw3_6   = 64.0
cc     -- const is used to aviod the computation of ( )**1/6 in a loop
c      const   =(1.d0+cw3_6)**expon
c      cv1     = 7.1
c      cv1_3   = 357.911
c      ct1     = 5.0
c      ct2     = 2.0
c      ct3     = 1.2
c      ct4     = 0.5
c      cr1     = 8.0
c      cr2     = 0.89

c***********************************************************************
csd   Modified for time-accurate runs.
csd   => Original code had dtm=10 which may be fine for steady
csd      calculations but I need dtm=dt.
csd   => Remember that dt2=dt ... set in preproc.f
      if (unsted .and. ispmod.le.2) then
        dtm=dt2
      else if (unsted .and. ispmod.eq.3) then   ! dual time stepping
        dtm=dtow
      else   ! steady
         dtm=10.d0
      endif
c***********************************************************************

      do k=kbegin,kend
         do j=jbegin,jend
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)

            workq(j,k,1)=rho
            workq(j,k,2)=u
            workq(j,k,3)=v
c     -- compute inverse of fnu to avoid divisions later --
            fnu(j,k)=rho/fmu(j,k)
         enddo
      enddo

csd   compute generalized distance function
csd   (smin and jmid are now stored in common blocks to avoid
csd   calculating the same quantities every iteration.)
c     
csd   Note: This loop is traversed only at the begining of a run.
csd         It is also traversed at the first iteration of any
csd         sequence when grid sequencing.
csd
      if (numiter.eq.istart .and. jsubit.eq.0) then
        do k=kbegin,kend
          do j=jbegin,jend
            j_point=j
            if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
            xpt1=x(j_point,kbegin)
            ypt1=y(j_point,kbegin)
            smin(j,k)=sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
          enddo
        enddo
c     
        chord=0.0
        fmin=100.0
        do j=jtranlo,jtranup-1
          chord=chord+sqrt((x(j,1)-x(j+1,1))**2+
     &          (y(j,1)-y(j+1,1))**2)
        enddo

        chord_2=0.5*chord
        chord=0.0
        do j=jtranlo,jtranup-1
          chord=chord+sqrt((x(j,1)-x(j+1,1))**2+
     &          (y(j,1)-y(j+1,1))**2)
          if (abs(chord-chord_2).lt.fmin) then
            fmin=abs(chord-chord_2)
            jmid=j+1
          endif
        enddo
c     
        if (.not.restart) then
c     -note: iseqlev is equal to grid level in mesh seq. passes.
c     -next logic check is true for :
c     1) no grid-sequencing performed
c     2) you are on coarsest grid of grid-sequencing
           if (iseqlev.eq.1) then
              do k=kbegin,kend
                 do j=jbegin,jend
                    turre(j,k)=retinf
                 enddo
              enddo
           else
              if (jmxi(iseqlev).ne.jmxi(iseqlev-1)) then
                 if (iord.eq.4) then
                    do k=klow,kend
                       do j=jtranlo+1,jtranup-1
                          turre(j,k)=1.d-8
                       enddo
                    enddo
                 else
                    do k=klow,kend
                       do j=jtranlo+1,jtranup-1
                          turre(j,k)=0.d0
                       enddo
                    enddo
                 endif
              endif
           endif
        else
           if (zeroturre) then
              do k=kbegin,kend
                 do j=jbegin,jend
                    turre(j,k)=retinf
                 enddo
              enddo
           endif
        endif
c     
        do k=klow,kup
          do j=jlow,jmid                  
            d_trip(j,k)=sqrt((x(j,k)-x(jtranlo,1))**2
     &            +(y(j,k)-y(jtranlo,1))**2)
          enddo
          do j=jmid+1,jup
            d_trip(j,k)=sqrt((x(j,k)-x(jtranup,1))**2
     &            +(y(j,k)-y(jtranup,1))**2)
          enddo
        enddo
c     
        delta_xlo=0.5*(sqrt((x(jtranlo+1,1)-x(jtranlo-1,1))**2+
     &        (y(jtranlo+1,1)-y(jtranlo-1,1))**2))
        delta_xup=0.5*(sqrt((x(jtranup+1,1)-x(jtranup-1,1))**2+
     &        (y(jtranup+1,1)-y(jtranup-1,1))**2))
c     
        do j=jtail1,jtail2
          turre(j,kbegin) = 0.d0
        enddo
c     -end on if numiter.eq.istart
      endif
c     
c     *************************************************
c       Preliminary Calculations Before Crux of Model
c     *************************************************
c      
      do k=klow,kup
        do j=jlow,jmid                  
          delta_u(j,k)=max(1.d-50,
     &          sqrt((workq(j,k,2)-workq(jtranlo,1,2))**2
     &          +(workq(j,k,3)-workq(jtranlo,1,3))**2))
        enddo
        do j=jmid+1,jup
          delta_u(j,k)=max(1.d-50,
     &          sqrt((workq(j,k,2)-workq(jtranup,1,2))**2
     &          +(workq(j,k,3)-workq(jtranup,1,3))**2))
        enddo
      enddo
      
c     -tried to save time by using array vort(j,k), computed in
c      other subroutines, instead of calculating vorticity here.
c     -not the same answer so vorticity(j,k) stays in.
c
c     02/97 -the reason they are not the same is that here we compute
c            the vorticity at the k nodes, whereas vortdeta.f computes
c            it at k+1/2 nodes.

      if (iord.eq.4) then
        call vort_o4(jdim,kdim,workq,xy,vorticity)
      else
        call vort_o2(jdim,kdim,workq,xy,vorticity)
      endif

      if (frozen .and. ( ifrz_what.eq.1 .or. ifrz_what.eq.3)) then
         do k = 1,kend
            do j = 1,jend
               vorticity(j,k) = vortf(j,k)
            end do
         end do
      end if  
      
      omega_tlo=vorticity(jtranlo,1)
      omega_tup=vorticity(jtranup,1)
c      omega_tlo=max(1.d-50,vorticity(jtranlo,1))       
c      omega_tup=max(1.d-50,vorticity(jtranup,1))
c
c ***************************************************************************
c                            Now the Model                      
c ***************************************************************************
c
csd   if ispmod=3, this is where we begin top of subiteration

 10   isub=isub + 1
c   
csd   z and zz are used as a temporary storage variables
      do 100 k=klow,kup
         do 90 j=jlow,jup
            zz=1.d0/(akarman*smin(j,k))**2
            chi_prime=fnu(j,k)
c            chi_prime=1.d0/fnu(j,k)
            chi=turre(j,k)*chi_prime
c
            z=chi**2/(chi**3+cv1_3)
            fv1=z*chi
            fv1_prime=3.0*z*chi_prime*(1.0-fv1)

            s=vorticity(j,k)*re

            z=1.0/(1.0+chi*fv1)
            fv2=1.0-chi*z
            fv2_prime=z*(-chi_prime +
     &            chi*(chi_prime*fv1+fv1_prime*chi)*z)

            s_tilda=s+turre(j,k)*fv2*zz
            s_tilda_prime=zz*(fv2+turre(j,k)*fv2_prime)
      
            z=zz/s_tilda
            r=turre(j,k)*z
            r_prime=z-r/s_tilda*s_tilda_prime

            if (abs(r).ge.10.d0) then
               fw=const
               fw_prime=0.0
            else
               g=r+cw2*(r**6-r)
               z=((1.0+cw3_6)/(g**6+cw3_6))**expon 
               g_prime=r_prime+cw2*(6.0*r**5*r_prime-r_prime)
               fw=g*z
               fw_prime=g_prime*z*(1.-g**6/(g**6+cw3_6))
            endif
            ft2=ct3*exp(-ct4*chi**2)
c     mn: zero trip terms
            ft2 = 0.d0
            ft2_prime=-ct4*2.0*chi*chi_prime*ft2
      
      
            p1(j,k)=cb1*(1.0-ft2)*s_tilda*reinv
            p(j,k)=p1(j,k)*turre(j,k)
            p_prime(j,k)=cb1*(-ft2_prime*s_tilda+
     &           (1.0-ft2)*s_tilda_prime)*reinv
      
            z=cb1/akarman**2
            z1=1.0/smin(j,k)**2
            z2=cw1*fw_prime-z*ft2_prime
            d1(j,k)=(cw1*fw-z*ft2)*z1*reinv
            d_prime(j,k)=turre(j,k)*z1*z2*reinv + d1(j,k)
            d1(j,k)=d1(j,k)*turre(j,k)
            d(j,k)=d1(j,k)*turre(j,k)
 90      continue
 100  continue

      do 200 k=klow,kup
         do 180 j=jlow,jmid                  
             g_t=min(0.1,delta_u(j,k)/(omega_tlo*delta_xlo))
             factor=(ct2*(omega_tlo/delta_u(j,k))**2
     &          *(smin(j,k)**2+(g_t*d_trip(j,k))**2))
             if (factor.le.103.0) then
                ft1=ct1*g_t*exp(-factor)
c     mn: zero trip terms
                ft1 = 0.d0               
                trip(j,k)=ft1*re*delta_u(j,k)**2
             else
                trip(j,k)=0.0
             endif
 180     continue
      
      
         do 190 j=jmid+1,jup
             g_t=min(0.1,delta_u(j,k)/(omega_tup*delta_xup))
             factor=(ct2*(omega_tup/delta_u(j,k))**2
     &          *(smin(j,k)**2+(g_t*d_trip(j,k))**2))
             if (factor.le.103.0) then
                ft1=ct1*g_t*exp(-factor)
c     mn: zero trip terms
                ft1 = 0.d0   
                trip(j,k)=ft1*re*delta_u(j,k)**2
             else
                trip(j,k)=0.0
             endif
 190     continue
 200  continue
c     
      do k=kbegin,kend
         do j=jbegin,jend
            work(j,k)=1.0/fnu(j,k)+turre(j,k)
         enddo
      enddo
c
c     -Note: Sometimes, if you run into a stability problem that a lower
c      time-step or grid-sequencing cannot resolve, turn xi_visc terms
c      to low-order while eta_visc terms remain high-order.
c
      call eta_visc(jdim,kdim,cb2,resiginv,xy,work,workph,
     &              tmpx,tmpy,by,cy,dy,fy)
      call eta_adv(jdim,kdim,vv,by,cy,dy,fy)
      call xi_visc(jdim,kdim,cb2,resiginv,xy,work,bx,cx,dx,fy)
      call xi_adv(jdim,kdim,uu,bx,cx,dx,fy)

c ************************************************************************
c                     Time Marching in Turbulence Model                    
c *************************************************************************
      if (ispmod.ge.3) then
         ss=1.5d0*(1.d0+dtm)/dt2
      else
         ss=1.d0      
      end if

      do k=kbegin,kend
         do j=jbegin,jend
            turre2(j,k)=turre(j,k)
            ffy(j,k)=0.0
            ffx(k,j)=0.0
            fx(k,j)=0.0
         enddo
      enddo   

      do k=klow,kup
         do j=jlow,jup

            d_p_bar(j,k)=(max(d1(j,k)-p1(j,k),0.0)+
     &           max(d_prime(j,k)-p_prime(j,k),0.0)*
     &           turre(j,k))

            denom(j,k)=1.0d0/(ss+dtm*(cx(k,j)+cy(j,k)+d_p_bar(j,k)))

            bx(k,j) = bx(k,j)*dtm*denom(j,k)
            cx(k,j) = 1.0
            dx(k,j) = dx(k,j)*dtm*denom(j,k)
            by(j,k) = by(j,k)*dtm*denom(j,k)
            cy(j,k) = 1.0
            dy(j,k) = dy(j,k)*dtm*denom(j,k)

         enddo
      enddo

c     -- invert LHS --

      if (nnit.eq.1) then
         do k = klow,kup
            do j = jlow,jup
               if (ispmod.ge.3) then
                 ttt=.5d0*(3.d0*turre(j,k)-4.d0*turre1(j,k)+turold(j,k))
     &                /dt2
               else
                  ttt=0.d0
               end if
               tt=p(j,k)-d(j,k)+trip(j,k)-ttt
               fby(j,k) = (fy(j,k)+tt)*dtm*denom(j,k)
            end do
         end do

         call triv(jdim,kdim,jlow,jup,klow,kup,work,by,cy,dy,fby)
      
         do k = klow,kup
            do j = jlow,jup
               fx(k,j) = fby(j,k)
            end do
         end do
      
         call triv(kdim,jdim,klow,kup,jlow,jup,work,bx,cx,dx,fx)
      
         negn = 0
         do k = klow,kup
            do j = jlow,jup
               turre(j,k) = turre(j,k) + fx(k,j)
               if(turre(j,k) .lt. 0.0d0) then
                  negn        =   negn + 1
c                  turre(j,k)  =   turre2(j,k)*0.5d0
                  turre(j,k)  =   0.0d0
                  fx(k,j)     =    0.0d0
               endif
            end do
         end do

      else
         residual=1.0d2
         isub2=0         
         do while ( (residual.gt.spmin_res).and.(isub2.lt.nnit) )
            isub2=isub2+1
            do k = klow,kup
               do j = jlow,jup
                  tt=p(j,k)-d(j,k)+trip(j,k)
                  
                  fby(j,k) = (fy(j,k)+tt)*dtm*denom(j,k)
     $                 + by(j,k)*(ffy(j,k-1)-fx(k-1,j))
     $                 + dy(j,k)*(ffy(j,k+1)-fx(k+1,j))
               end do
            end do
            
            call triv(jdim,kdim,jlow,jup,klow,kup,work,by,cy,dy,fby)
            
            do k = klow,kup
               do j = jlow,jup
                  fx(k,j) = fby(j,k)
                  ffy(j,k) = fby(j,k)
               end do
            end do
          
            call triv(kdim,jdim,klow,kup,jlow,jup,work,bx,cx,dx,fx)
            
            negn = 0
            resid_term1=0.0
            resid_term2=0.0
            
            do k=klow,kup
               do j=jlow,jup
                  turre(j,k) = turre2(j,k) + fx(k,j)
                  if(turre(j,k) .lt. 0.0)then
                     negn        =   negn + 1
                     turre(j,k)  =    turre2(j,k)*0.5d0
                  endif
                  resid_term1=resid_term1+(fx(k,j)-ffx(k,j))**2
                  resid_term2=resid_term2+(fx(k,j))**2
                  ffx(k,j)=fx(k,j)
               end do
            end do
            residual1=sqrt(resid_term1)
            residual2=sqrt(resid_term2)
            residual=residual1/max(residual2,1.d-10)
         end do
      endif            ! nnit.eq.1

c      if (negn .ne. 0) write (*,*) 'NEGATIVE NU',numiter,isub,negn

c     -- mn --
c     -- added division by xyj in res=fx(k,j)**2 line and res_spl2 used
c     in computing total residual in residl2.f --

      res_spl=0.0d0
      resmax=0.0d0
      jresmax=0
      kresmax=0
      do j=jlow,jup
         do k=klow,kup
            res=fx(k,j)**2/xyj(j,k)
            res_spl=res_spl + res
            if (res.gt.resmax) then
               resmax=res
               jresmax=j
               kresmax=k
            endif
         enddo
      enddo
      res_spl2 = res_spl
      res_spl=sqrt(res_spl)/float((jup-jlow+1)*(kup-klow+1))/dt2
      resmax=sqrt(resmax)

      if (.not.unsted) then
         if (thisout) write (turbhis_unit,498) numiter, res_spl, isub,
     &        jresmax, kresmax, resmax
 498     format(I7,1x,e15.8,3I6,1x,e15.8)
      end if



c ***************************************************************************


c ************************************************************************
c                       Boundary Conditions 
c ************************************************************************
c     -- farfield and outflow boundaries --
 250  km1=kup
      km2=kend-2
      km3=kend-3
      jb3=jbegin+3
      jb2=jbegin+2
      jb1=jbegin+1
      je1=jend-1
      je2=jend-2
      je3=jend-3
c
c     -I should really check to see if flow is inflow or outflow
      if (ispbc.le.1) then
        factor=float(ispbc)

c     -- mn mod --
c     -----------------------------------------------------------------
c     -- outer-boundary  k = kmax --
c     -----------------------------------------------------------------
        do j = jbegin+1,jend-1
c     -- metrics terms --
          par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
          xy3 = xy(j,kend,3)/par
          xy4 = xy(j,kend,4)/par
        
          u   = q(j,kend,2)/q(j,kend,1)
          v   = q(j,kend,3)/q(j,kend,1)
          vn = xy3*u + xy4*v

          if (vn.le.0.d0) then
c     -- inflow --
c            write (*,*) 'INFLOW'
            turre(j,kend) = retinf
          else
c     -- outflow --          
c            write (*,*) 'OUTFLOW'
            turre(j,kend) = turre(j,k-1)
          endif
        end do
c     -- mn mod end --
c
c       ZEROETH ORDER => ispbc=0
c       FIRST ORDER   => ispbc=1
c       -extrapolate farfield
cmn        do j=jlow,jup
cmn          turre(j,kend)  =(1.+factor)*turre(j,km1)-factor*turre(j,km2)
cmn        enddo
c
c       -extrapolate outflow
        do k=kbegin,kend
          turre(jbegin,k)=(1.+factor)*turre(jb1,k)-factor*turre(jb2,k)
          turre(jend,k)  =(1.+factor)*turre(je1,k)-factor*turre(je2,k)
        enddo
c
      else
c       SECOND ORDER
c       -extrapolate farfield
        do j=jlow,jup
          turre(j,kend)=3.d0*(turre(j,km1)-turre(j,km2))+turre(j,km3)
        enddo
c
c       -extrapolate outflow
        do k=kbegin,kend
          turre(jbegin,k)=3.d0*(turre(jb1,k)-turre(jb2,k))+turre(jb3,k)
          turre(jend,k)=3.d0*(turre(je1,k)-turre(je2,k))+turre(je3,k)
        enddo
      endif

c     -- wake-cut --
      if (iord.eq.2) then
        do j=1,jtail1-1
          jj = jend - j + 1
          turre(j,1)  = .5d0*(turre(j,2)+turre(jj,2))
          turre(jj,1) = turre(j,1)
        enddo
      else
        k=1
        kp1=k+1
        kp2=k+2
        zz=1.d0/6.d0
        do j=1,jtail1-1
          jj = jend -j + 1
          tl2=turre(j,kp2)
          tl =turre(j,kp1)
          tu =turre(jj,kp1)
          tu2=turre(jj,kp2)
c
          turre(j,k) = zz*(-tl2 + 4.d0*(tl + tu) -tu2)
          turre(jj,k)= turre(j,k)
        enddo
      endif
c     

      if (ispmod.ge.3) then
c         write (*,*) 'Turbulence Model',numiter,isub,res_spl
         if (jsubit.eq.0) then
            if ((res_spl.gt.spmin_res) .and. (isub.lt.isubmx)) goto 10
         else
            if((res_spl.gt.spmin_res*1.d1).and.(isub.lt.isubmx)) goto 10
         endif
      endif


c ************************************************************************
c                     Calculate Eddy Viscosity 
c ************************************************************************


c     -the eddy viscosity, turmu, is stored at half-points
c      in eta direction.  ie. at k+1/2 points.
c              q(k+1/2)= .5*(q(k)+q(k+1)) + O(dy**2)

      if (iord.eq.4) then
        zz=1.d0/16.d0
        do 610 k=klow,kup-1
          km1=k-1
          kp1=k+1
          kp2=k+2
          do 600 j=jbegin,jend
            chim1=turre(j,km1)*fnu(j,km1)
            chi=turre(j,k)*fnu(j,k)
            chip1=turre(j,kp1)*fnu(j,kp1)
            chip2=turre(j,kp2)*fnu(j,kp2)
            
            fv1m1=chim1**3/(chim1**3+cv1_3)
            fv1=chi**3/(chi**3+cv1_3)
            fv1p1=chip1**3/(chip1**3+cv1_3)
            fv1p2=chip2**3/(chip2**3+cv1_3)
            
            fv1_c=zz*(-fv1m1+9.d0*(fv1+fv1p1)-fv1p2)
c     
            turwrk_km1=turre(j,km1)*workq(j,km1,1)
            turwrk_k=turre(j,k)*workq(j,k,1)
            turwrk_kp1=turre(j,kp1)*workq(j,kp1,1)
            turwrk_kp2=turre(j,kp2)*workq(j,kp2,1)
c     
            turmu(j,k) = fv1_c*zz*( 
     &            -turwrk_km1+9.d0*(turwrk_kp1+turwrk_k)-turwrk_kp2)
 600      continue
 610    continue
c     
        k=kbegin
        kp1=k+1
        kp2=k+2
        zz=1.d0/8.d0
        do 620 j=jbegin,jend
          chi=turre(j,k)*fnu(j,k)
          chip1=turre(j,kp1)*fnu(j,kp1)
          chip2=turre(j,kp2)*fnu(j,kp2)
c     
          fv1=chi**3/(chi**3+cv1_3)
          fv1p1=chip1**3/(chip1**3+cv1_3)
          fv1p2=chip2**3/(chip2**3+cv1_3)
c
          fv1_c=zz*(3.d0*fv1 + 6.d0*fv1p1 - fv1p2)
c
          turwrk_k=turre(j,k)*workq(j,k,1)
          turwrk_kp1=turre(j,kp1)*workq(j,kp1,1)
          turwrk_kp2=turre(j,kp2)*workq(j,kp2,1)
c
          turmu(j,k) = fv1_c*zz*( 
     &          3.d0*turwrk_k + 6.d0*turwrk_kp1 - turwrk_kp2)
 620    continue
c
        k=kup
        kp1=k+1
        do 630 j=jbegin,jend
          chi=turre(j,k)*fnu(j,k)
          fv1=chi**3/(chi**3+cv1_3)
          chip1=turre(j,kp1)*fnu(j,kp1)
          fv1p1=chip1**3/(chip1**3+cv1_3)
          fv1_c=0.5d0*(fv1+fv1p1)
          turmu(j,k) = fv1_c*.5d0*( 
     &           turre(j,k)*workq(j,k,1) + turre(j,kp1)*workq(j,kp1,1))
 630    continue
      else
        do k=kbegin,kup
          kp1=k+1
          do j=jbegin,jend
            chi=turre(j,k)*fnu(j,k)
            fv1=chi**3/(chi**3+cv1_3)
            tur1 = fv1*turre(j,k)*workq(j,k,1)

            chip1=turre(j,kp1)*fnu(j,kp1)
            fv1p1=chip1**3/(chip1**3+cv1_3)
            tur2 = fv1p1*turre(j,kp1)*workq(j,kp1,1)

            turmu(j,k) = 0.5d0*(tur1+tur2)

          enddo
        enddo
        
      endif

c      print *, '      Turbulence', isub,res_spl

      return
      end
