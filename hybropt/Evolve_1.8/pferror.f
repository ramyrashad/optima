C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C            PPPP  FFFFF  EEEEE  RRRR   RRRR    OOO   RRRR             C
C            P   P F      E      R   R  R   R  O   O  R   R            C
C            PPPP  FFFF   EEE    RRRR   RRRR   O   O  RRRR             C
C            P     F      E      R   R  R   R  O   O  R   R            C
C            P     F      EEEEE  R   R  R   R   OOO   R   R            C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE PFERROR(ndim,nobfun,nacc,fitacc,nfuneval,i_mnx,i_err_p)
      SAVE
      LOGICAL FILEX
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTINE COMUPTES THE ERROR BETWEEN             C
C               THE EXISTING PARETO FRONT (FITACC) AND THAT            C
C               STORED IN THE ARRRAY EXACT  ***                        C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      dimension fitacc(nobfun,ndim)

      dimension EXACT(nobfun,ndim)

C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
c     read in exact pareto-front
      INQUIRE (FILE='exact_pareto.dat',EXIST=FILEX)
      IF (.NOT.FILEX) THEN
         WRITE (*,2001)
 2001    FORMAT ('PFERROR: CANNOT FIND exact_pareto.dat',
     1        ', EXECUTION TERMINATED'/)
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C          *** OPEN FILE AND EXECUTE READ ***
C
      OPEN (4,FILE='exact_pareto.dat',
     $     STATUS='UNKNOWN',FORM='FORMATTED')
      READ (4,*,ERR=1001) IPOPDUM,NEXACT
      DO NA=1,NEXACT
         READ (4,*,ERR=1001) I1,I2,(EXACT(NO,NA),NO=1,NOBFUN)
      ENDDO
      CLOSE (4)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** COMPUTE AREA BETWEEN THE EXACT PARETO FRONT
C              AND THE CURRENT PARETO FRONT. USE THIS AS A
C              MEASURE OF THE ERROR. THE ALGORITHM USED 
C              DIVIDES THE OVERALL AREA INTO A SERIES OF
C              TRIANGLES. IC COUNTER IS ASSOCIATED WITH THE
C              CURRENT ACCUMULATION FILE AND THE IE COUNTER
C              IS ASSOCIATED WITH THE EXACT PARETO FRONT ***
C-----------------------------------------------------------------------
C          *** SETUP ***
C
      NEXACTM  = NEXACT-1
      NACCM    = NACC-1
      IC       = 1
      IE       = 1
      AREA     = 0.0
      X1       = FITACC(1,IC)
      Y1       = FITACC(2,IC)
      X2       = EXACT(1,IE)
      Y2       = EXACT(2,IE)
 1000 CONTINUE
C
c  if i_err_p=2  bypass points of the accum file outside the exact PF
      if(i_err_p .eq. 2)then
         if(imnx.eq.0)then
            if(fitacc(2,ic).lt.exact(2,1))then
               ic = ic+1
               go to 1000
            endif
            if(fitacc(2,ic).gt.exact(1,nexact))then
               ic = ic+1
               go to 1000
            endif
         endif
         if(imnx.eq.1)then
            if(fitacc(2,ic).gt.exact(2,1))then
               ic = ic+1
               go to 1000
            endif
            if(fitacc(2,ic).lt.exact(1,nexact))then
               ic = ic+1
               go to 1000
            endif
         endif
      endif
C-----------------------------------------------------------------------
C          *** DO THIS IF THE END OF THE CURRENT ACCUMULATION
C              FILE HAS BEEN REACHED (IC=NACC) ***
C
      IF (IC.EQ.NACC) THEN
         X3    = EXACT(1,IE+1)
         Y3    = EXACT(2,IE+1)
         DAREA = 0.5*ABS((X2-X1)*(Y3-Y1)-(X3-X1)*(Y2-Y1))
         AREA  = AREA+DAREA
CCCCCCCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (33,444) IC,IE,X1,Y1,X2,Y2,X3,Y3,DAREA,AREA
C  444 FORMAT ('IC,IE,XY1,XY2,XY3,DAREA,AREA=',2I5,6F10.6,2F12.6)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         X2    = X3
         Y2    = Y3
         IE    = IE+1
         ERR   = AREA
         IF (IE.GE.NEXACT.AND.IC.GE.NACC) go to 3000
         GO TO 1000
      ENDIF
C
C-----------------------------------------------------------------------
C          *** DO THIS IF THE END OF THE EXACT PARETO FRONT
C              FILE HAS BEEN REACHED (IE=NEXACT) ***
C
      IF (IE.EQ.NEXACT) THEN
         X3    = FITACC(1,IC+1)
         Y3    = FITACC(2,IC+1)
         DAREA = 0.5*ABS((X2-X1)*(Y3-Y1)-(X3-X1)*(Y2-Y1))
         AREA  = AREA+DAREA
CCCCCCCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (33,444) IC,IE,X1,Y1,X2,Y2,X3,Y3,DAREA,AREA
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         X1    = X3
         Y1    = Y3
         IC    = IC+1
         ERR   = AREA
C         IDEBUG = 1
C         IF (IDEBUG.EQ.1) STOP
         IF (IE.GE.NEXACT.AND.IC.GE.NACC) go to 3000
         GO TO 1000
       ENDIF
C
C-----------------------------------------------------------------------
C          *** BASIC LOGIC ***
C
      IF (FITACC(1,IC+1).GE.EXACT(1,IE+1)) THEN
         X3    = FITACC(1,IC+1)
         Y3    = FITACC(2,IC+1)
         DAREA = 0.5*ABS((X2-X1)*(Y3-Y1)-(X3-X1)*(Y2-Y1))
         AREA  = AREA+DAREA
CCCCCCCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (33,444) IC,IE,X1,Y1,X2,Y2,X3,Y3,DAREA,AREA
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         X1    = X3
         Y1    = Y3
         IC    = IC+1
      ELSE
         X3    = EXACT(1,IE+1)
         Y3    = EXACT(2,IE+1)
         DAREA = 0.5*ABS((X2-X1)*(Y3-Y1)-(X3-X1)*(Y2-Y1))
         AREA  = AREA+DAREA
CCCCCCCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (33,444) IC,IE,X1,Y1,X2,Y2,X3,Y3,DAREA,AREA
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         X2    = X3
         Y2    = Y3
         IE    = IE+1
      ENDIF
      ERR = AREA
      IF (IE.GE.NEXACT.AND.IC.GE.NACC) go to 3000
      GO TO 1000
C
C-----------------------------------------------------------------------
C          *** WRITE ERROR MESSAGE INDICATING ERROR
C              DURING READ AND TERMINATE EXECUTION ***
C
 1001 CONTINUE
      CLOSE (4)
      WRITE (*,2003)
 2003 FORMAT ('pferror: READING ERROR DETECTED IN exact_pareto.dat',
     1     ', EXECUTION TERMINATED'/)
      STOP
C-----------------------------------------------------------------------
 3000 continue

 36   write(36,'(4x,i8,4x,e24.16)')nfuneval,err
      close(36)
 
      return
C
      END



