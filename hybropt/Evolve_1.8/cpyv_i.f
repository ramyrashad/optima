c======================================================================
      subroutine cpyv_i(m,i1,i2)

C     ARC2D V3.00 by Thomas H. Pulliam
C     NASA Ames Research Center
C     Copyright 1992 
C     Restricted to United States (NO Foreign Dissemination!!)

      dimension i1(m),i2(m)
      do i= 1,m
         i2(i)= i1(i)
      enddo

      return
      end
