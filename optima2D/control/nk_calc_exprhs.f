      subroutine nk_calc_exprhs(jdim,kdim,ndim,D_hat,exprhs,a_jk)
 
#include "../include/arcom.inc"

      double precision exprhs(jdim,kdim,ndim)
      double precision D_hat(jdim,kdim,ndim,6)
      double precision a_jk(6,6)

      do 400 n=1,ndim
      do 400 k=kbegin,kend
      do 400 j=jbegin,jend
         exprhs(j,k,n)=0.0
 400  continue
      if (jstage.gt.1)then
         do 500 n=1,ndim
         do 500 k=kbegin,kend
         do 500 j=jbegin,jend
         do 500 i_stage=1,jstage-1
            exprhs(j,k,n)=exprhs(j,k,n)+
     &           (((a_jk(jstage,i_stage))/(a_jk(jstage,jstage)))*
     &           D_hat(j,k,n,i_stage))
 500     continue
      endif


      return
      end                                                       
