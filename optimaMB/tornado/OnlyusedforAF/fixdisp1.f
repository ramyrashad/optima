c ********************************************************************
c **       Routine to copy data from the interior of block2         **
c **       to the halo points of block 1                            **
c **                                                                **
c ********************************************************************
c calling routine: xiexpl  etaexpl
c
c The fourth order dissipation is calculated by
c take a forward difference, a centered difference and finally a 
c backward difference of   JQ. 
c
c After the forward difference a halo copy is needed.  When 
c the coordinate direction is reversed the value of the derivative
c in the opposite block has the opposite sign.
c  is one point farther away than usual and 
c
c   ---------------------------------------------------------
c  |   copy to | copy from  | copy to | copy from  | sign    |
c  |   Block 1 | Block 2    |         |            |         |
c  |     Side  |  Side      |         |            |         |
c  |---------------------------------------------------------|
c  |      1    |   1        | kbegin  |  klow      |   -1    |
c  |      1    |   3        | kbegin  |  kup  - 1  |    1    |
c  |      2    |   2        | jbegin  |  jlow      |   -1    |
c  |      2    |   4        | jbegin  |  jup  - 1  |    1    |
c  |      3    |   1        | kend    |  klow + 1  |    1    |
c  |      3    |   3        | kend    |  kup  - 2  |   -1    |
c  |      4    |   2        | jend    |  jlow + 1  |    1    |
c  |      4    |   4        | jend    |  jup  - 2  |   -1    |
c   ---------------------------------------------------------
c Otherwise the value is in the usual location and has the same sign.

c
      subroutine fixdisp1(nside1,nside2,
     &  jdim1,kdim1,arr1,
     &  jbegin1,jend1,kbegin1,kend1,jlow1,jup1,klow1,kup1,
     &  jdim2,kdim2,arr2,
     &  jbegin2,jend2,kbegin2,kend2,jlow2,jup2,klow2,kup2)
c
      dimension arr1(jdim1,kdim1)
      dimension arr2(jdim2,kdim2)
c
c ********************************************************************
c **  block 1   :      side 1 or 3                                  **
c ********************************************************************
      if(nside1.eq.1.or.nside1.eq.3) then
        if (nside1.eq.1) then
          k1h = kbegin1
        else
          k1h = kend1 
        endif
c *** block 1 - side 1 or 3 connected to block 2 - side 1 or 3 
        if (nside2.eq.1.or.nside2.eq.3) then
          if(nside1.eq.nside2) then
            if(nside2.eq.1) then
              k2i = klow2 
            else
              k2i = kup2 - 2
            endif
            j1 = max(jlow1,jdim2+1-jup2)
            j2 = min(jup1,jdim2+1-jlow2)
            do 110 jj=j1,j2
              j1h = jj
              j2i = jdim2 + 1 - jj
              arr1(j1h,k1h) = -arr2(j2i,k2i)
  110       continue
          else
            if(nside2.eq.1) then
              k2i = klow2 + 1
            else
              k2i = kup2 - 1
            endif
            j1 = max(jlow1,jlow2)
            j2 = min(jup1,jup2)
            do 115 jj=j1,j2 
              j1h = jj
              j2i = jj
              arr1(j1h,k1h)  = arr2(j2i,k2i)
  115       continue
          endif
c *** block 1 - side 1 or 3 connected to block 2 - side 2 or 4 
        elseif(nside2.eq.2.or.nside2.eq.4)then
          stop ' fixdisp1 not prog yet for 1-3 to 2-4'
          if(nside2.eq.2) then 
            j2i = jlow2 + 1
          else
            j2i = jup2 - 1 
          endif
          if ( (nside1.eq.1.and.nside2.eq.2)
     &     .or.(nside1.eq.3.and.nside2.eq.4) )  then
            j1 = max(jlow1,klow2)
            j2 = min(jup1,kup2)
            do 120 jj=j1,j2
              j1h = jj
              k2i = jj
              arr1(j1h,k1h) = arr2(j2i,k2i)
  120       continue
          else
            j1 = max(jlow1,kdim2+1-kup2)
            j2 = min(jup1,kdim2+1-klow2)
            do 125 jj=j1,j2
              j1h = jj
              k2i = kdim2 + 1 - jj 
              arr1(j1h,k1h) = arr2(j2i,k2i)
  125       continue
          endif
        endif
c ********************************************************************
c **  block 1   :      side 2 or 4                                  **
c ********************************************************************
      elseif(nside1.eq.2.or.nside1.eq.4) then
        if(nside1.eq.2) then
          j1h = jbegin1
        else 
          j1h = jend1
        endif 
        if(nside2.eq.2.or.nside2.eq.4)then
          if(nside1.eq.nside2) then
            if(nside2.eq.2) then
              j2i = jlow2 
            else
              j2i = jup2 - 2
            endif
            k1 = max(klow1,kdim2+1-kup2)
            k2 = min(kup1,kdim2+1-klow2)
            do 210 kk=k1,k2
              k1h = kk
              k2i = kdim2 + 1 - kk
              arr1(j1h,k1h) = -arr2(j2i,k2i)
  210       continue
          else
            if(nside2.eq.2) then
              j2i = jlow2 + 1
            else
              j2i = jup2 - 1
            endif
            k1 = max(klow1,klow2)
            k2 = min(kup1,kup2)
            do 215 kk=k1,k2
              k1h = kk
              k2i = kk
              arr1(j1h,k1h) = arr2(j2i,k2i)
  215       continue
          endif
        elseif(nside2.eq.1.or.nside2.eq.3)then
          stop ' fixdisp1 not prog for sides 2-4 to 1-3'
          if(nside2.eq.1) then 
            k2i = klow2 + 1
          else
            k2i = kup2 - 1 
          endif
          if ( (nside1.eq.2.and.nside2.eq.1)
     &     .or.(nside1.eq.4.and.nside2.eq.3) )  then
            k1 = max(klow1,jlow2)
            k2 = min(kup1,jup2)
            do 220 kk=k1,k2
              k1h = kk
              j2i = kk
              arr1(j1h,k1h) = arr2(j2i,k2i)
  220       continue
          else
            k1 = max(klow1,jdim2+1-jup2)
            k2 = min(kup1,jdim2+1-jlow2)
            do 225 kk=k1,k2
              k1h = kk
              j2i = jdim2 + 1 - kk
              arr1(j1h,k1h) = arr2(j2i,k2i)
  225       continue
          endif
        else
          stop ' fixdisp1 error'
        endif
      else
        stop ' error in fixdisp1, nside1'
      endif
      return
      end
