c     ------------------------------------------------------------------
c     -- vector dot product of the residual sensitivity and the adjoint
c     variables --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine vdotv( in, grad, nmax, jmax, kmax, js, je, ks, ke, psi,
     &     dRdX, psit, dRsadX)

      implicit none

#include "../include/optcom.inc"

      integer in,nmax,jmax, kmax,js,je,ks,ke,j,k,n
      double precision grad(ibsnc)
      double precision dRdX(jmax,kmax,4), psi(jmax,kmax,4)
      double precision dRsadX(jmax,kmax), psit(jmax,kmax) 

      do n = 1,4
         do k = ks,ke
            do j = js,je
               grad(in) = grad(in) + dRdX(j,k,n)*psi(j,k,n)
c               if ( n.eq.1 .and. j.eq.10 .and. k.eq.10) write (*,10) j,k
c     &              ,n,dRdX(j,k,n),psi(j,k,n)
            end do
         end do
      end do
c 10   format(3i5,2e12.5)

      if (nmax.eq.5) then
         do k = ks,ke
            do j = js,je
               grad(in) = grad(in) + dRsadX(j,k)*psit(j,k)
            end do
         end do
      end if

      return
      end                       !vdotv
