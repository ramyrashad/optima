c----------------------------------------------------------------------
c     -- differentiation of spectral radius for JST dissipation --
c     written by: marian nemec
c     date: jan. 2001
c----------------------------------------------------------------------

      subroutine dspecty(jdim, kdim, ndim, indx, icol, q, xy, xyj, vv,
     &     sndsp, as, dspdq, work, coef2, coef4, pcoef, spect, c2, c4,
     &     db, bm1, bp1)

#include "../include/arcom.inc"

      integer jdim, kdim, ndim, indx(jdim,kdim), icol(9), jsta, jsto

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
clb
      double precision xyj(jdim,kdim)
      double precision sndsp(jdim,kdim), dspdq(4,jdim,kdim)
      double precision work(jdim,kdim,2,4), dvvdq(3), vv(jdim,kdim) 
      double precision coef2(jdim,kdim), coef4(jdim,kdim)
      double precision pcoef(jdim,kdim), spect(jdim,kdim)
      double precision c2(jdim,kdim), c4(jdim,kdim)
      double precision db(4,4,jdim,kdim), bm1(4,4,jdim,kdim)
      double precision bp1(4,4,jdim,kdim)

c     -- form derivative of spectral radius at node j,k --
      g2  = gamma*gami
      do k = kbegin,kend
         do j = jbegin,jend

            ri  = 1.d0/q(j,k,1)
            ri2 = ri*ri

            t1 = 0.5d0/sndsp(j,k)
            t2 = q(j,k,2)*q(j,k,2)
            t3 = q(j,k,3)*q(j,k,3)

            dadq1 = g2*ri2*( (t2+t3)*ri - q(j,k,4) )
            dadq2 = - g2*q(j,k,2)*ri2
            dadq3 = - g2*q(j,k,3)*ri2
            dadq4 = g2*ri

            met = dsqrt( xy(j,k,3)**2+xy(j,k,4)**2 )*t1

            dspdq(1,j,k) = dadq1*met
            dspdq(2,j,k) = dadq2*met
            dspdq(3,j,k) = dadq3*met
            dspdq(4,j,k) = dadq4*met
         end do
      end do      

      if (.not. frozen .or. ( frozen .and. ifrz_what.eq.2 ) ) then
         do k = kbegin,kend
            do j = jbegin,jend

               ri  = 1.d0/q(j,k,1)
               ri2 = ri*ri
               dvvdq(1) = - ri2*(q(j,k,2)*xy(j,k,3) +q(j,k,3)*xy(j,k,4))
               dvvdq(2) = ri*xy(j,k,3) 
               dvvdq(3) = ri*xy(j,k,4)
               
               if ( vv(j,k) .lt. 0.d0 ) then
                  do n = 1,3
                     dvvdq(n) = - dvvdq(n)
                  end do
               end if

               dspdq(1,j,k) = dvvdq(1) + dspdq(1,j,k)
               dspdq(2,j,k) = dvvdq(2) + dspdq(2,j,k)
               dspdq(3,j,k) = dvvdq(3) + dspdq(3,j,k)
            end do
         end do
      end if

c     -- form dissipation differences --

      do n = 1,4                                                     
c     -- 1st-order forward difference --
         do k = kbegin,kup
            kp = k+1
            do j = jlow,jup
               work(j,k,1,n) = q(j,kp,n)*xyj(j,kp) - q(j,k,n)*xyj(j,k)
            end do
         end do

c     -- apply cent-dif to 1st-order forward --
         do k =klow,kup-1
            kp = k+1
            km = k-1
            do j =jlow,jup
               work(j,k,2,n) = work(j,kp,1,n) - 2.d0*work(j,k,1,n) +
     &              work(j,km,1,n) 
            end do
         end do

c     -- points on boundaries: rows 2 & kmax-1 --
         kb = kbegin
         do j = jlow,jup
            work(j,kb,2,n) = q(j,kb+2,n)*xyj(j,kb+2) - 2.d0
     &           *q(j,kb+1,n)*xyj(j,kb+1) + q(j,kb,n)*xyj(j,kb)
            work(j,kup,2,n) = work(j,kup-1,1,n) - work(j,kup,1,n)
         end do
      end do

c     -- taken from coef24y.f --

      do k = kbegin,kend                                              
         do j = jbegin,jend
            pcoef(j,k) = coef2(j,k)
            spect(j,k) = coef4(j,k)
         end do
      end do

      do k = kbegin,kup                                          
         kp = k+1
         do j = jbegin,jend                                         
            c2(j,k) = dis2y*(pcoef(j,kp)*spect(j,kp) +
     &           pcoef(j,k)*spect(j,k))
            coef2(j,k) = c2(j,k)
            c4(j,k) = dis4y*(spect(j,kp) + spect(j,k))
            coef4(j,k) = c4(j,k) - min( c4(j,k),c2(j,k) )
         end do
      end do

c     -- linearize c4 = c4 - min(c4,c2) --
c     -- pressure switch (pcoef) is treated as constant --
      do k = klow,kup                                            
         km = k-1
         kp = k+1
         do j = jlow,jup
            pcm = dis2y*pcoef(j,km)/xyj(j,km)
            pc  = dis2y*pcoef(j,k)/xyj(j,k)
            pcp = dis2y*pcoef(j,kp)/xyj(j,kp)

            d4m = dis4y/xyj(j,km)
            d4  = dis4y/xyj(j,k)
            d4p = dis4y/xyj(j,kp)

            if ( c4(j,k) .le. c2(j,k) ) then
c     -- no O(3) dissipation: c4 = 0 --
               do n = 1,4
                  do m = 1,4
                     bm1(m,n,j,k) = pcm*dspdq(m,j,km)*work(j,km,1,n)
                     db(m,n,j,k) = - pc*dspdq(m,j,k)*(work(j,k,1,n) -
     &                    work(j,km,1,n))
                     bp1(m,n,j,k) = - pcp*dspdq(m,j,kp)*work(j,k,1,n)
                  end do
               end do
            else
c     -- mostly O(3) dissipation: c4 = c4 - c2 --
               do n = 1,4
                  do m = 1,4
                     bm1(m,n,j,k) = - d4m*dspdq(m,j,km)*work(j,km,2,n) +
     &                    pcm*dspdq(m,j,km)*work(j,km,1,n)  
                     db(m,n,j,k) = d4*dspdq(m,j,k)*(work(j,k,2,n) -
     &                    work(j,km,2,n) ) - pc*dspdq(m,j,k)*( work(j,k
     &                    ,1,n)-work(j,km,1,n) ) 
                     bp1(m,n,j,k) = d4p*dspdq(m,j,kp)*work(j,k,2,n) -
     &                    pcp*dspdq(m,j,kp)*work(j,k,1,n) 
                  end do
               end do
            end if
         end do
      end do

c     -- add to jacobian --

      if(.not.periodic) then
         jsta=jlow+1
         jsto=jup-1
      else
         jsta=jlow
         jsto=jup
      end if

      do k = klow+1,kup-1

         do j = jsta,jsto

            itmp = ( indx(j,k) - 1 )*ndim
            do n = 1,4 

               ij = itmp + n
               ii = ( ij - 1 )*icol(9)
               if (clopt) ii = ( ij - 1 )*(icol(9)+1)

               do m =1,4
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,n,j,k)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
                  as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
               end do
            end do
         end do

      end do 

      if(.not.periodic) then

      j = jlow
      do k = klow+1,kup-1
         itmp = ( indx(j,k) - 1 )*ndim
         do n = 1,4 

            ij = itmp + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)

            do m =1,4
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,n,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,n,j,k) 
            end do
         end do
      end do

      j = jup
      do k = klow+1,kup-1
         itmp = ( indx(j,k) - 1 )*ndim
         do n = 1,4 

            ij = itmp + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)

            do m =1,4
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k) 
            end do
         end do
      end do

      end if

      k = klow

      do j = jsta,jsto

         itmp = ( indx(j,k) - 1 )*ndim
         do n = 1,4 

            ij = itmp + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)

            do m =1,4
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,n,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,n,j,k)
            end do
         end do
      end do

      k = kup

      do j = jsta,jsto

         itmp = ( indx(j,k) - 1 )*ndim
         do n = 1,4 

            ij = itmp + n
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)

            do m =1,4
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
            end do
         end do
      end do 


      if(.not.periodic) then

      k = klow
      j = jlow
      itmp = ( indx(j,k) - 1 )*ndim
      do n = 1,4 

         ij = itmp + n
         ii = ( ij - 1 )*icol(9)
         if (clopt) ii = ( ij - 1 )*(icol(9)+1)

         do m =1,4
            as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)
            as(ii+icol(2)+m) = as(ii+icol(2)+m) + db(m,n,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + bp1(m,n,j,k)
         end do
      end do

      k = kup
      j = jlow
      itmp = ( indx(j,k) - 1 )*ndim
      do n = 1,4 

         ij = itmp + n
         ii = ( ij - 1 )*icol(9)
         if (clopt) ii = ( ij - 1 )*(icol(9)+1)

         do m =1,4
            as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,n,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,n,j,k)
         end do
      end do

      k = klow
      j = jup
      itmp = ( indx(j,k) - 1 )*ndim
      do n = 1,4 

         ij = itmp + n
         ii = ( ij - 1 )*icol(9)
         if (clopt) ii = ( ij - 1 )*(icol(9)+1)

         do m =1,4
            as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,n,j,k)
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,n,j,k)
         end do
      end do

      k = kup
      j = jup
      itmp = ( indx(j,k) - 1 )*ndim
      do n = 1,4 

         ij = itmp + n
         ii = ( ij - 1 )*icol(9)
         if (clopt) ii = ( ij - 1 )*(icol(9)+1)

         do m =1,4
            as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,n,j,k)
            as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
            as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
         end do
      end do

      end if


      return
      end                       !dspecty
