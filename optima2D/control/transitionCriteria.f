      ! ================================================================
      ! ================================================================
      ! 
      subroutine AHD_Granville(
           !-- input
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     dstar, theta, Hfactor, iHfactor, Re_x, Re_dstar, Re_theta,
     &     Pohlhausen, s_arc,
           !-- in/out
     &     Hi, polyPh, Re_theta_CR, jCR, xCR, sCR, MeanPh,
     &     Re_theta_TR, jTR)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will compute and search for the
      ! critical point and transition point based on the Graville,
      ! incompressible AHD, or compressible AHD criteria (depending on
      ! transPredictMethod)
      !
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
      
      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim,
     &     ndim
      double precision
     &     q(jdim,kdim,ndim),
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim),
     &     dstar(jdim),
     &     theta(jdim),
     &     Hfactor(jdim),
     &     iHfactor(jdim),
     &     Re_x(jdim),
     &     Re_dstar(jdim),
     &     Re_theta(jdim),
     &     Pohlhausen(jdim),
     &     s_arc(jdim)

      ! ________________________
      ! declare in/out variables
      integer
     &     jCR,
     &     jTR
      double precision
     &     Hi(jdim),
     &     polyPh(jdim),
     &     Re_theta_CR(jdim),
     &     xCR,
     &     sCR,
     &     MeanPh(jdim),
     &     Re_theta_TR(jdim)
      
      ! _____________________
      ! Function declarations
      double precision trapezoidalIntegration
      double precision linearInterp

      ! _______________________
      ! declare local variables
      integer
     &     j,
     &     jm1,
     &     jp1,
     &     jCRm1,
     &     kd
      logical
     &     usePolyPh
      double precision
     &     Ph,
     &     sumPh(jdim),
     &     Re_theta_CR_found,
     &     m1,
     &     m2,
     &     b1,
     &     b2,
     &     x_ratio,
     &     PhCR,
     &     numerPh,
     &     denomPh,
     &     temp_s,
     &     temp_Ph,
     &     A,
     &     B,
     &     C,
     &     D,
     &     E,
     &     F,
     &     M

      ! __________________________________
      ! Set-up Constants for AHD Criterion
      M = fsmach
      if ( transPredictMethod .eq. 1 ) then
         ! Incompressible (original) AHD
         A = -206.0
         B = 25.7
         C = 16.8
         D = 2.77
         E = 52.0
         F = 14.8
      else if ( transPredictMethod .eq. 2 ) then
         ! Compressible (new) AHD
         A =  98.64*M**3 - 356.44*M**2 + 117.13*M - 236.69
         B = -13.04*M**4 + 38.5*M**3 - 30.07*M**2 + 10.89*M + 22.7
         C =  0.21*M**3 + 4.79*M**2 - 1.76*M + 22.56
         D = -3.48*M**4 + 6.26*M**3 - 3.45*M**2 + 0.23*M + 12
         E =  0.6711*M**3 - 0.7379*M**2 + 0.167*M + 51.904
         F =  0.03016*M**5 - 0.7061*M**4 + 0.3232*M**3 - 0.0083*M**2
     &        - 0.1745*M + 14.6;
      end if ! transPredictMethod 

      ! ___________________________
      ! LOOP to find Critical Point

      do j=jblStart,jblEnd,jblinc

         ! ________________________
         ! Preliminaries for each j
         jp1 = j+jblinc
         jm1 = j-jblinc

         if (foundBLEdge(j)) then


            ! ___________________________________________________
            ! Shape Factor based on Pohlhausen Number (Hi)

            ! Note that I have decided not to use the shape factor based
            ! on Pohlhausen as I find it does not match the boundary
            ! layer codes as closely for a fine grid (maybe better for a
            ! coarse grid). In any case, the AHD and Granville criteria
            ! suggest using the incompressible shape factor even for
            ! compressible flows (hence the use of iHfactor below).

            Ph = Pohlhausen(j)
            polyPh(j) = 
     &           - 8838.4*Ph**4 + 1105.1*Ph**3 - 67.962*Ph**2
     &           + 17.574*Ph + 2.0593 

            if ((polyPh(j) .lt. 0d0)) then
               Hi(j) = iHfactor(j)

               write(36,171) j, polyPh(j), Hi(j)
 171           format('CAUTION: polyPh is NEGATIVE',
     &              ' at j = ', I3,
     &              ' Ph = ',1p,e14.7
     &              ' using Hi = ', e14.7, ' instead')
               
            else
               ! RR: create input parameter
               usePolyPh = .false.
               if (usePolyPh) then
                  Hi(j) = 4.02923 - dsqrt(polyPh(j))
                  !Hi(j) = 0.5*(Hfactor(j) + Hi(j)) ! try taking the average :)
               else
                  Hi(j) = iHfactor(j)
               end if
            end if

            ! ____________________________________________________
            ! Critical Reynolds number based on momentum thickness
            select case (transPredictMethod)
            case (1:2) ! Incompressible and Compressible AHD method

               Re_theta_CR(j) = dexp(E/Hi(j) - F)

            case (3) ! Granville's method

               if (Hi(j).ge.2.591) then
                  Re_theta_CR(j) = 54.2124d0 / (Hi(j)*(Hi(j)-2.48d0))
     &                 + 31.6d0/Hi(j)
               else 
                  Re_theta_CR(j) = 520d0/Hi(j)
     &                 + 2.5E6/Hi(j)*(1d0/Hi(j)-1d0/2.591d0)**1.95d0
               end if

            case default
               stop ' transPredictMethod assigned wrong value '
            end select ! transPredictMethod

            if ((.not.foundCR) .and.
     &          (Re_theta(j).ge.Re_theta_CR(j))) then
               ! ____________________
               ! Critical Point Found
               foundCR = .true.
               jCR = j
            end if

         end if   ! foundBLEdge=.true.
      end do   ! j-loop

      ! ______________________________
      ! LOOPS to find Transition Point

      if (foundCR) then
         ! _________________________________
         ! Interpolate critical point values
         ! Find intersection of Re_theta and Re_theta_CR curves occuring
         ! between jCR and jCR minus one.
         jCRm1 = jCR - jblinc
         ! Line Re_theta
         m1 = (Re_theta(jCR)-Re_theta(jCRm1))/(x(jCR,1)-x(jCRm1,1)) ! slope
         b1 = Re_theta(jCRm1)-m1*x(JCRm1,1) ! y-intercept
         ! Line Re_theta_CR
         m2 =(Re_theta_CR(jCR)-Re_theta_CR(jCRm1))/(x(jCR,1)-x(jCRm1,1)) ! slope
         b2 = Re_theta_CR(jCRm1)-m2*x(JCRm1,1) ! y-intercept
         ! Intersection pont
         xCR = (b2 - b1) / (m1 - m2)
         Re_theta_CR_found = m2*xCR + b2


         ! Linearly interpolate for sCR and PhCR using xCR
         x_ratio = (xCR - x(jCRm1,1)) / (x(jCR,1) - x(jCRm1,1))
         sCR = s_arc(jCRm1)
     &        + x_ratio*(s_arc(jCR)-s_arc(jCrm1))
         PhCR = Pohlhausen(jCRm1)
     &        + x_ratio*(Pohlhausen(jCR)-Pohlhausen(jCRm1))
         
         ! Temporarily replace critcal node with interpolated vales
         temp_s = s_arc(jCR) 
         temp_Ph = Pohlhausen(jCR) 
         s_arc(jCR) = sCR
         Pohlhausen(jCR) = PhCR

         ! _______________________
         ! Mean Pohlhaussen number
         sumPh(jCR) = Pohlhausen(jCR)
         do j=jCR+jblinc,jblEnd,jblinc
            jm1 = j-jblinc
            jp1 = j+jblinc
            
            if (foundBLEdge(j)) then
               select case (transPredictMethod)
               case (1:3) ! Incompressible AHD criterion
                  MeanPh(j) = 
     &                 trapezoidalIntegration(
     &                 jdim, s_arc, Pohlhausen, jCR, j, jblinc)
                  MeanPh(j) = 
     &                 MeanPh(j)/(s_arc(j)-s_arc(jCR))
!               case (3) ! Granville's criterion
!                  kd = k_deltaBL(j)
!                  numerPh = Re_theta(j)**2
!     &                 - velBLEdge(j)/velBLEdge(jCR)*Re_theta_CR(jCR)**2
!                  denomPh = Re_x(j)
!     &                 - velBlEdge(j)/velBLedge(jCR)*Re_x(jCR) 
!                  MeanPh(j) = 4d0/45d0 - 0.2d0*numerPh/denomPh
               case default
                  stop ' transPredictMethod assigned wrong value '
               end select ! transPredictMethod
            end if

         end do ! jCR --> jblEnd

         do j=jCR+jblinc,jblEnd,jblinc

            jm1 = j-jblinc
            jp1 = j+jblinc

            ! ____________________
            ! Transition Criterion
            select case (transPredictMethod)

            case (1:2) ! Incompressible and Compressible AHD criterion

               Re_theta_TR(j) =
     &              Re_theta_CR_found + A*dexp(B*MeanPh(j))
     &              *(dlog(C*Tu)-D*MeanPh(j))

            case (3) ! Granville's criterion

               Re_theta_TR(j) =
     &              Re_theta_CR_found + 375d0
     &              + dexp(6.1d0+55d0*MeanPh(j))

            case default
               stop ' transPredictMethod assigned wrong value '
            end select ! transPredictMethod
            
            ! __________________________
            ! Check Transition Criterion
            if ((.not.foundTR) .and.
     &           (Re_theta(j).ge.Re_theta_TR(j))) then
               foundTR = .true.
               jTR = j
            end if

         end do ! j=jCR --> jblEnd

      else 
         ! set error flag: critical point not found
         errorTrackerTP = 2

         write(34,*)
         write(34,*)
         write(34,*) 'ERROR: CRITICAL POINT NOT FOUND ON SURFACE =',
     &        surfaceBL
         write(34,*)
         write(34,*)

      endif ! (foundCR)

      ! Put back the actual nodal value (instead of the interpolated value)
      s_arc(jCR) = temp_s
      Pohlhausen(jCR) = temp_Ph
      
      
      return
      end subroutine AHD_Granville









      ! ================================================================
      ! ================================================================
      ! 
      subroutine Michel_HRx(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     dstar, theta, Hfactor, Re_x, Re_dstar, Re_theta,
     &     Pohlhausen, s_arc)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will compute and search for the
      ! transition point based on the Michel, or H-Rx criteria
      ! (depending on transPredictMethod)
      !
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------
      
#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim,
     &     ndim
      double precision
     &     q(jdim,kdim,ndim),
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim),
     &     dstar(jdim),
     &     theta(jdim),
     &     Hfactor(jdim),
     &     Re_x(jdim),
     &     Re_dstar(jdim),
     &     Re_theta(jdim),
     &     Pohlhausen(jdim),
     &     s_arc(jdim)
      
      stop'Michels Transition Criterion Not Yet Implemented'

      return
      end subroutine Michel_HRx





      ! ================================================================
      ! ================================================================
      ! 
      subroutine MATTC(
           ! input
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     dstar, theta, Hfactor, Re_x, Re_dstar, Re_theta,
     &     Pohlhausen, s_arc,
           ! in/out
     &     NtsInt, jTR)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will compute and search for the
      ! transition point based on the MATTC transition prediciton method
      !
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------
      
#ifdef _MPI_VERSION
      use mpi
#endif
      implicit none
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim,
     &     ndim
      double precision
     &     q(jdim,kdim,ndim),
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim),
     &     dstar(jdim),
     &     theta(jdim),
     &     Hfactor(jdim),
     &     Re_x(jdim),
     &     Re_dstar(jdim),
     &     Re_theta(jdim),
     &     Pohlhausen(jdim),
     &     s_arc(jdim)
      
      ! _____________________
      ! Function declarations
      double precision trapezoidalIntegration

      ! _____________________________________________________
      ! declare output variables from getCurvilinearVariables
      double precision
     &     vel(jdim,kdim),
     &     uu(jdim,kdim),
     &     vv(jdim,kdim),
     &     pp(jdim,kdim),
     &     rho(jdim,kdim),
     &     aa(jdim,kdim),
     &     MM(jdim,kdim),
     &     dyy(jdim,kdim)

      ! _______________________
      ! declare local variables
      integer
     &     j,
     &     jm1,
     &     jp1,
     &     jTR,
     &     count
      logical
     &     useArcLength
      double precision
     &     Cp(jdim),
     &     Nts(jdim),
     &     NtsInt(jdim),
     &     dNts(jdim),
     &     dNtsdx(jdim),
     &     dx(jdim),
     &     dy(jdim),
     &     ds(jdim),
     &     Rm,
     &     B, 
     &     A, a0, a1, a2,
     &     dCpdx(jdim),
     &     dCpdxi,
     &     dCpds_1(jdim),
     &     dCpds_4(jdim),
     &     dCpds(jdim),
     &     denom,
     &     Cs, Cx, C,
     &     Cp_stag,
     &     dCp_stag,
     &     D,
     &     xx(jdim)

      ! _________________________
      ! initialize values to zero
      select case (surfaceBL)
      case (1) ! upper surface
         do j=1,jdim
            Cp(j) = 0d0
            Nts(j) = 0d0
            NtsInt(j) = 0d0
            xx(j) = 0d0
            dNts(j) = 0d0
            dNtsdx(j) = 0d0
            dx(j) = 0d0
            dy(j) = 0d0
            ds(j) = 0d0
            dCpds_1(j) = 0d0
            dCpds_4(j) = 0d0
            dCpds(j) = 0d0
            dCpdx(j) = 0d0
         end do
      case (2) ! lower surface
         do j=jblSP,jtail1, jblinc
            Cp(j) = 0d0
            Nts(j) = 0d0
            NtsInt(j) = 0d0
            xx(j) = 0d0
            dNts(j) = 0d0
            dNtsdx(j) = 0d0
            dx(j) = 0d0
            dy(j) = 0d0
            ds(j) = 0d0
            dCpds_1(j) = 0d0
            dCpds_4(j) = 0d0
            dCpds(j) = 0d0
            dCpdx(j) = 0d0
         end do
      case default
         stop ' transitionCriterion: surfaceBL assigned wrong value '
      end select ! surfaceBL

      useArcLength = .true.

      ! __________________________________________________
      ! Compute variables in curvilinear coordinate system
      call getCurvilinearVariables(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     vel, uu, vv, pp, rho, aa, MM, dyy)
      ! ________________________________________________________
      ! Get the coefficient of pressure along surface of airfoil
      do j = jlow,jup
         Cp(j) = 2.0d0/(gamma*fsmach*fsmach) * (gamma*pp(j,1) - 1.0d0)
      end do

      ! __________________________
      ! Compute the N-factor curve

      Nts(jblSP) = 0.0
      Nts(jblStart-jblinc) = 0.0
      Cp_stag = Cp(jblSP)

      ! RR: debug

      write (*,*) 'TITLE="Transition Data"'
      write(*,*) 'VARIABLES="x","j","dx","A","B","C","D",
     & "dNts","Nts","NtsInt","dCpdx", "dCpds", "Cp"'
      write(*,*) 'ZONE T="Surface = ', surfaceBL, '"'

      !write (*,*) 'jblSP', jblSP, ' jblLE', jblLE, ' jblStart', jblStart
!      write (*,811) 
! 811  format(
!     &     ' j',T15,   
!     &     ' x',T30,
!     &     ' dx',T45,
!     &     ' A',T60,
!     &     ' B',T75,
!     &     ' C',T90,
!     &     ' D',T105,
!     &     ' dNts',T120,
!     &     ' Nts',T135,
!     &     ' dCpdx',T150,
!     &     ' dCpds',T165,
!     &     ' Rm')

      !do j = jblSP+jblinc, jblEnd, jblinc
      !if (surfaceBL.eq.2) jblStart = jblLE
      count = 0
      ! RR ? jblStart? 
      jblStart = jblSP+jblinc
      do j = jblStart, jblEnd, jblinc

         ! ________________________
         ! Preliminaries for each j
         jp1 = j+jblinc
         jm1 = j-jblinc
         dx(j) = x(j,1)-x(jm1,1)
         dy(j) = y(j,1)-y(jm1,1)
         ds(j) = dsqrt(dx(j)*dx(j) + dy(j)*dy(j))

         if (foundBLedge(j)) then
         ! ________________________________________
         ! Compute the change in N-factor given by
         ! MATTC Method: dNts = B*C*D*dx

         ! ______
         ! Find B
         !Rm = Re_x(j) / 10**6 !< chord Reynolds number in millions
         Rm = re_in / 10**6 !< chord Reynolds number in millions
         if (Rm.lt.0) Rm = 0.0
         B = 14.0d0*Rm**0.5
         !B = 14.0d0*dsqrt(Re_x(j)/10**6)
         !B = dsqrt(14.0d0*Rm)

         ! ______
         ! Find C
         
         !-- find A
         !a0 = -(1.d0-0.01d0*Rm)*dlog(Rm+1.d0)
         a0 = -(dlog(Rm+1.d0))**(1.d0-0.01d0*Rm)
         !a0 = -dlog((Rm+1.d0)**(1.d0-0.01d0*Rm))
         a1 = -1.9d0
         a2 =  0.6d0
         A = a0*(1 + a1*x(j,1) + a2*x(j,1)*x(j,1))

         !-- find pressure gradient wrt to chord position
         dCpdx(j) = 
     &        0.5 * ( (Cp(jp1)-Cp(j)) / dx(j) +
     &                (Cp(j)-Cp(jm1)) / dx(jm1) )
         !-- find pressure gradient wrt to arclength
         !-- option 1
         dCpds_1(j) = 
     &        0.5 * ( (Cp(jp1)-Cp(j)) / ds(j) + 
     &                (Cp(j)-Cp(jm1)) / ds(jm1) )
         !-- option 4
         dCpdxi = 0.5*(Cp(jp1)-Cp(jm1)) 
         denom = dsqrt(  xy(j,1,3)*xy(j,1,3) + xy(j,1,4)*xy(j,1,4) )
         dCpds_4(j) = (xy(j,1,4)*xy(j,1,1)-xy(j,1,3)*xy(j,1,2))*dCpdxi
     &        /denom

         !-- using option 4
         dCpds(j) = dCpds_4(j)

         !-- subtract A from pressure gradient
         Cs = dCpds(j) - A
         Cx = dCpdx(j) - A
         if (useArcLength) then
            C = Cs
         else
            C = Cx
         end if 
         ! -- C must be between 0 and 1
         if (C.lt.0.0) then
            C = 0.0
!         else if (C.gt.1.0) then
!            C = 1.0 
         end if 

         ! ______
         ! Find D
         !dCp_stag = Cp_stag - Cp(j)
         dCp_stag = Cp_stag - Cp(j)
         D = 2.0d0 / dCP_stag

         !if (abs(dCp_stag).gt.1E-10) then 
         !   D = 2.0 / dCP_stag
         !else
         !   D = 2.0 / 1E-10
         !end if

         ! ____________
         ! Compute dNts
         dNtsdx(j) = B*C*D
         if ( useArcLength ) then
            dNts(j) = B*C*D*ds(j)
         else
            dNts(j) = B*C*D*dx(j)
         end if ! useArcLength

         ! ___________
         ! Compute Nts
         Nts(j) = Nts(jm1) + dNts(j)
         xx(j) = x(j,1)

         ! ___________________________________________________
         ! Integrate for N-factor curve
         if (j.ne.jblStart) then
            if ( useArcLength ) then
               NtsInt(j) = trapezoidalIntegration(
     &              jdim,s_arc,dNtsdx,jblStart,j,jblinc)
            else 
               NtsInt(j) = trapezoidalIntegration(
     &              jdim,xx,Nts,jblStart,j,jblinc)
            end if ! useArcLength
         end if 
         
         if (j.ne.jblStart) then
            write(*,812) x(j,1), j, dx(j), A, B, C, D, dNts(j), Nts(j),
     &           NtsInt(j), dCpdx(j), dCpds(j), Cp(j)
 812        format(
     &           g14.7,T15,
     &           5XI3,T30,
     &           g14.7,T45,
     &           g14.7,T60,
     &           g14.7,T75,
     &           g14.7,T90,
     &           g14.7,T105,
     &           g14.7,T120,
     &           g14.7,T135,
     &           g14.7,T150,
     &           g14.7,T165,
     &           g14.7,T180,
     &           g14.7)
         end if

!         write (*,*) 'sf = ', surfaceBL, 'j =', j, 'x = ', x(j,1),
!     &        ' Nts = ', Nts(j), 'C = ', C

      end if ! foundBLEdge

      end do ! j=jblSP+jblinc --> jblEnd

      ! _____________________________
      ! N-factor Transition Criterion
      do j=jblSP,jblEnd,jblinc

         if (foundBLEdge(j)) then
            if ((.not.foundTR) .and. (NtsInt(j).ge.Ncrit)) then
               foundTR = .true.
               jTR = j
               exit
            end if
         end if

      end do ! j=jblSP+jblinc --> jblEnd

      return
      end subroutine MATTC

      ! ================================================================
      ! ================================================================
      ! 
      subroutine eN_Drela(
           ! input
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     dstar, theta, Hfactor, iHfactor, Re_x, Re_dstar, Re_theta,
     &     Pohlhausen, s_arc,
           ! in/out
     &     Hi, polyPh, Re_theta_CR, jCR, xCR, sCR, MeanPh,
     &     Nts, jTR)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will compute and search for the
      ! transition point based Drela's e^N envelope method as used in
      ! XFOIL and MSES.
      !
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
      
      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim,
     &     ndim
      double precision
     &     q(jdim,kdim,ndim),
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim),
     &     dstar(jdim),
     &     theta(jdim),
     &     Hfactor(jdim),
     &     iHfactor(jdim),
     &     Re_x(jdim),
     &     Re_dstar(jdim),
     &     Re_theta(jdim),
     &     Pohlhausen(jdim),
     &     s_arc(jdim)

      ! ________________________
      ! declare in/out variables
      integer
     &     jCR,
     &     jTR
      double precision
     &     Hi(jdim),
     &     Re_theta_CR(jdim),
     &     xCR,
     &     sCR,
     &     NtsInt(jdim),
      ! not used:
     &     polyPh(jdim),
     &     MeanPh(jdim),
     &     Re_theta_TR(jdim)
      
      ! _____________________
      ! Function declarations
      double precision trapezoidalIntegration
      double precision linearInterp

      ! _______________________
      ! declare local variables
      integer
     &     j,
     &     jm1,
     &     jp1,
     &     jCRm1,
     &     jHiMax,
     &     jcheck,
     &     jtmp,
     &     jMonotone,
     &     jPt,
     &     jPtm,
     &     jtf,
     &     extrapolateBL,
     &     kd,
     &     orderNfactor 
      logical
     &     usePolyPh,
     &     foundHiMax,
     &     addSmallTerm,
     &     foundjMonotone
      double precision
     &     Ph,
     &     sumPh(jdim),
     &     Re_theta_CR_found,
     &     m1,
     &     m2,
     &     b1,
     &     b2,
     &     x_ratio,
     &     PhCR,
     &     numerPh,
     &     denomPh,
     &     temp_s,
     &     temp_Ph,
     &     dNtsds(jdim),
     &     Hdiff,
     &     Hicheck,
     &     Hi_j,
     &     H_j,
     &     theta_j,
     &     Re_theta_j,
     &     velBLEdge_j,
     &     dNtsds_j,
     &     dNtsds_jm1,
     &     aafac,
     &     exN,
     &     arg,
     &     dNadd(jdim),
     &     dN_SQ,
     &     ds,
     &     Nts(jdim),
     &     gm1,
     &     gm1i,
     &     vsq,
     &     Msq,
     &     Hk  



      ! 
      ! Initialize variables
      !
      select case (surfaceBL)
      case (1) ! upper surface
         do j=1,jdim
            Nts(j) = 0.d0
            NtsInt(j) = 0.d0
            dNtsds(j) = 0.d0
            dNadd(j) = 0.d0
         end do
      case (2) ! lower surface
         do j=jblSP,jtail1, jblinc
            Nts(j) = 0.d0
            NtsInt(j) = 0.d0
            dNtsds(j) = 0.d0
            dNadd(j) = 0.d0
         end do
      end select

      jHiMax = -1
      foundHiMax = .false.
      jMonotone = -1


      !
      ! Write statements
      !
      write (*,*) 'TITLE="Transition Data"'
      write(*,*) 'VARIABLES="x","j","s","Nts","NtsInt","dNtsds",
     &   "Hi","Hi_j","Hk"'
      write(*,*) 'ZONE T="Surface = ', surfaceBL, '"'

      do j=jblStart,jblEnd,jblinc

         Hi(j) = iHfactor(j)
         ! 
         ! Shape Factor based on Pohlhausen Number (Hi)
         !

         ! Note that I have decided not to use the shape factor based
         ! on Pohlhausen as I find it does not match the boundary
         ! layer codes as closely for a fine grid (maybe better for a
         ! coarse grid). 
         
         usePolyPh = .false. ! RR: create input parameter
         if (usePolyPh) then
            if (foundBLEdge(j)) then
               Ph = Pohlhausen(j)
               polyPh(j) = 
     &           - 8838.4*Ph**4 + 1105.1*Ph**3 - 67.962*Ph**2
     &           + 17.574*Ph + 2.0593 

               if ((polyPh(j) .lt. 0d0)) then
                  Hi(j) = iHfactor(j)

                  write(36,171) j, polyPh(j), Hi(j)
 171           format('CAUTION: polyPh is NEGATIVE',
     &              ' at j = ', I3,
     &              ' Ph = ',1p,e14.7
     &              ' using Hi = ', e14.7, ' instead')

               else
                  Hi(j) = 4.02923 - dsqrt(polyPh(j))
                  Hi(j) = 0.5*(Hfactor(j) + Hi(j)) ! try taking the average :)
               end if ! polyPh.lt.0
            end if ! foundBLEdge
         end if ! usePolyPh
      end do ! j


      ! 
      ! In order to extrapolate the boundary layer properties
      ! from the laminar region into the turbulent region, we need to
      ! first find the maximum value of the shape factor and the nearest
      ! monotonmically increasing (upstream) region. 
      !
      Hicheck = 0.0
      jtf = jTransForced(surfaceBL)
      do jtmp = jtf, jBLStart, -jblinc
         if (foundBLEdge(jtmp)) then
            if (Hi(jtmp).gt.Hicheck) then
               Hicheck = Hi(jtmp)
               jHiMax = jtmp
            end if
         end if
      end do ! j
      do jtmp = jHiMax, jBLStart, -jblinc
         if (foundBLEdge(jtmp)) then
            if ( ! check for montonically increasing H
     &           (Hi(jtmp).gt.Hi(jtmp-jblinc)) 
     &              .and.
     &           (Hi(jtmp-jblinc).gt.Hi(jtmp-2*jblinc))
     &              .and.
     &           (Hi(jtmp-2*jblinc).gt.Hi(jtmp-3*jblinc))
     &         ) then
               jMonotone = jtmp
               foundjMonotone = .true.
               exit
            end if 
         end if ! foundBLEdge
      end do ! j
      write (34, *)
      write (34, *) 'From eN method:'
      write (34, *) 'sf       = ', surfaceBL
      write (34, *) 'jtf      = ', jtf
      write (34, *) 'jHiMax   = ', jHiMax
      write (34, *) 'jMontone = ', jMonotone
      write (34, *)
      if (.not.foundjMonotone) then
         write (36, *) 'Monotone region not found on surf = ',
     &           surfaceBL
      end if
      


      ! 
      ! Loop to obtain N-factor curve
      !
      do j=jblStart+jblinc,jblEnd,jblinc

         ! Preliminaries for each j
         jp1 = j+jblinc
         jm1 = j-jblinc

         ds = s_arc(j) - s_arc(jm1)

         if (foundBLEdge(j)) then

            !
            ! Extrapolate boundary layer properties
            ! This will allow the N-factor curve to continue increasing
            ! in the turbulent region
            !
            Hi_j = Hi(j)
            Re_theta_j = Re_theta(j)
            theta_j = theta(j)
            velBLEdge_j = velBLEdge(j)

            dNtsds_jm1 = dNtsds(jm1)

            if (foundjMonotone .and. x(j,1).gt.x(jHiMax,1)) then
              jPt  = jMonotone
              jPtm = jMonotone - 2*jblinc
              extrapolateBL = 1 ! 0 = no extrapolation, 1 = constant, 2 = linear
              select case (extrapolateBL)
              case (0) ! no extrapolation
                 Hi_j = Hi(j)
                 Re_theta_j = Re_theta(j)
                 theta_j = theta(j)
                 velBLEdge_j = velBLEdge(j)
              case (1) ! constant extrapolation
                 Hi_j = Hi(jHiMax)
                 Re_theta_j = Re_theta(jHiMax)
                 theta_j = theta(jHiMax)
                 velBLEdge_j = velBLEdge(jHiMax)
              case (2) ! linear  extrapolation
                 Hi_j = linearInterp(
     &                x(jPtm,1),Hi(jPtm),
     &                x(jPt,1),Hi(jPt),
     &                x(j,1))
                 Hi_j = Hi_j + (Hi(jHiMax)-Hi(jMonotone))
                 Re_theta_j = linearInterp(
     &                x(jPtm,1),Re_theta(jPtm),
     &                x(jPt,1),Re_theta(jPt),
     &                x(j,1))
                 Re_theta_j = Re_theta_j + 
     &                 (Re_theta(jHiMax)-Re_theta(jMonotone))
                 theta_j = linearInterp(
     &                x(jPtm,1),theta(jPtm),
     &                x(jPt,1),theta(jPt),
     &                x(j,4))
                 theta_j = theta_j + (theta(jHiMax)-theta(jMonotone))
                 velBLEdge_j = linearInterp(
     &                x(jPtm,1),velBLEdge(jPtm),
     &                x(jPt,1),velBLEdge(jPt),
     &                x(j,4))
                 velBLEdge_j = velBLEdge_j + (velBLEdge(jHiMax)-
     &                velBLEdge(jMonotone))
              case default
                 stop ' extrapolateBL assigned wrong value '
              end select ! surfaceBL
            else
               Hi_j = Hi(j)
               Re_theta_j = Re_theta(j)
               theta_j = theta(j)
               velBLEdge_j = velBLEdge(j)
            endif

            !write (*, '(4g14.7)') Hi_j, Re_theta_j, theta_j, velBLEdge_j
            
            ! 
            ! Compute kinematic shape factor
            !

            gm1  = gamma - 1.d0
            gm1i = 1.d0 / gm1
            vsq  = velBLEdge_j*velBLEdge_j
            Msq  = vsq / (gm1*(gm1i-0.5*vsq))
            Hk   = (Hi_j - 0.725*gm1*Msq) / (1.0 + 0.2825*gm1*Msq)

            ! 
            ! Compute the amplification rate (=dN/ds) using Drela's
            ! XFOIL/MSES method
            !

            dNtsds_j = 0.0

            call getAmplificationRate( 
     &            Hi_j, theta_j, Re_theta_j, dNtsds_j)

            dNtsds(j) = dNtsds_j

            ! RR: make new input paramter: orderNfactor
            dN_SQ = 0.0
            orderNfactor = 1 ! don't use 2 (doesn't work)
            if(orderNfactor.eq.2) then
               ! Take rms-average of amplification rates over current 
               ! interval. Remeber to subtract off the small addition
               ! term that may have been added to dN/ds at previous station.
               dN_SQ = 0.5d0*(   dNtsds_j*dNtsds_j + 
     &                         dNtsds_jm1*dNtsds_jm1 )
               dN_SQ = dN_SQ*dN_SQ

               if (dN_SQ.le.0.0) then
                  dNtsds_j = 0.0
               else
                  dNtsds_j = dsqrt(dN_SQ)
               end if
            else if (orderNfactor.eq.1) then
               dNtsds_j = 0.5d0*(dNtsds_jm1 + dNtsds_j)
            end if ! orderNfactor


            dNtsds(j) = dNtsds_j
            !
            ! Small addition term (dN) to ensure dN/ds > 0 near N = Ncrit
            !
            addSmallTerm = .true.
            if (addSmallTerm) then
               Nts(j) = Nts(jm1) + dNtsds(j)*ds ! only a temporary calc
               aafac = 15.0 
               arg = min( aafac*(Ncrit-0.5*(Nts(j)+Nts(jm1))),aafac)
               if(arg.le.0.0) then
                  exN = 1.0
               else
                  exN = dexp(-arg)
               endif
               ! leave this in for now, but really the theta's should be
               ! replaced with theta_j and theta_jm1
               dNadd(j) = exN * 0.002/(theta(jm1)+theta(j))
               ! Adujusted dN/ds
               dNtsds(j) = dNtsds(j) + dNadd(j)
            end if
 
            ! 
            ! Final compuation of Nfactor based on adjusted dN/ds
            !
            Nts(j) = Nts(jm1) + dNtsds(j)*ds

            !
            ! Integrate for N-factor curve 
            !
            if (j.ne.jblStart) then
               NtsInt(j) = trapezoidalIntegration(
     &              jdim,s_arc,dNtsds,jblStart,jblStart,jblinc)
            end if 

            write(*,813) x(j,1), j, s_arc(j), Nts(j), NtsInt(j), 
     &           dNtsds(j), Hi(j), Hi_j, Hk
 813        format(
     &           g14.7,T15,
     &           5XI3,T30,
     &           g14.7,T45,
     &           g14.7,T60,
     &           g14.7,T75,
     &           g14.7,T90,
     &           g14.7,T105,
     &           g14.7,T120,
!     &           g14.7,T135,
!     &           g14.7,T150,
!     &           g14.7,T165,
!     &           g14.7,T180,
!     &           g14.7,T195,
     &           g14.7)

         end if   ! foundBLEdge=.true.
      end do   ! j-loop
      
      ! _____________________________
      ! N-factor Transition Criterion
      do j=jblSP,jblEnd,jblinc
         if (foundBLEdge(j)) then
            if ((.not.foundTR) .and. (Nts(j).ge.Ncrit)) then
               foundTR = .true.
               jTR = j
               exit
            end if
         end if
      end do

      write (34,*) 'sf =', surfaceBL
      write(34,*) 
     & 'jTR = ', jTR, 
     & 'jMontone =' , jMonotone, 
     & 'jHiMax =' , jHiMax 
      write (34,*)
     & 'xTR = ', x(jTR,1), 
     & 'xMontone =' , x(jMonotone,1),
     & 'xHiMax =' , x(jHiMax,1) 

      if (.not.foundTR) then
         ! trick to get correct errorFlagTP value in 
         ! transitionPrediction.f
         foundCR = .true.
      end if

      return
      end subroutine eN_Drela


      ! ================================================================
      ! ================================================================
      ! 
      SUBROUTINE getAmplificationRate( HK, TH, RT, AX )
      !
      ! ================================================================
      ! ================================================================
       
C==============================================================
C     Amplification rate routine for envelope e^n method.
C     Reference:
C                Drela, M., Giles, M.,
C               "Viscous/Inviscid Analysis of Transonic and 
C                Low Reynolds Number Airfoils", 
C                AIAA Journal, Oct. 1987.
C
C     NEW VERSION.   March 1991
C          - m(H) correlation made valid up to H=20
C          - non-similar profiles are used for H > 5 
C            in lieu of Falkner-Skan profiles.  These are
C            more representative of separation bubble profiles.
C--------------------------------------------------------------
C
C     input :   HK     kinematic shape parameter
C               TH     momentum thickness
C               RT     momentum-thickness Reynolds number
C
C     output:   AX     envelope spatial amplification rate
C               AX_(.) sensitivity of AX to parameter (.)
C
C
C     Usage: The log of the envelope amplitude N(x) is 
C            calculated by integrating AX (= dN/dx) with 
C            respect to the streamwise distance x.
C                      x
C                     /
C              N(x) = | AX(H(x),Th(x),Rth(x)) dx
C                     /
C                      0
C            The integration can be started from the leading
C            edge since AX will be returned as zero when RT
C            is below the critical Rtheta.  Transition occurs
C            when N(x) reaches Ncrit (Ncrit= 9 is "standard").
C==============================================================
      IMPLICIT REAL (A-H,M,O-Z)
ccc   DATA DGR / 0.04 /
ccc   DATA DGR / 0.08 /
      DATA DGR / 0.16 /
C

      !write (*, '(4g14.7)') HK, TH, RT, AX
      
      HMI = 1.0d0/(HK - 1.0d0)
      HMI_HK = -HMI**2d0
C
C---- log10(Critical Rth) - H   correlation for Falkner-Skan profiles
      !AA    = HMI**(43/100)
      AA    = 2.492*HMI**0.43
      AA_HK =   (AA/HMI)*0.43 * HMI_HK
C
      BB    = TANH(14.0*HMI - 9.24)
      BB_HK = (1.0 - BB*BB) * 14.0 * HMI_HK
C
      GRCRIT = AA    + 0.7*(BB + 1.0)
      GRC_HK = AA_HK + 0.7* BB_HK
C
C
      GR = LOG10(RT)
      GR_RT = 1.0 / (2.3025851*RT)
C
      IF(GR .LT. GRCRIT-DGR) THEN
C
C----- no amplification for Rtheta < Rcrit
       AX    = 0.
       AX_HK = 0.
       AX_TH = 0.
       AX_RT = 0.
C
      ELSE
C
C----- Set steep cubic ramp used to turn on AX smoothly as Rtheta 
C-     exceeds Rcrit (previously, this was done discontinuously).
C-     The ramp goes between  -DGR < log10(Rtheta/Rcrit) < DGR
C
       RNORM = (GR - (GRCRIT-DGR)) / (2.0*DGR)
       RN_HK =     -  GRC_HK       / (2.0*DGR)
       RN_RT =  GR_RT              / (2.0*DGR)
C
       IF(RNORM .GE. 1.0) THEN
        RFAC    = 1.0
        RFAC_HK = 0.
        RFAC_RT = 0.
       ELSE
        RFAC    = 3.0*RNORM**2 - 2.0*RNORM**3
        RFAC_RN = 6.0*RNORM    - 6.0*RNORM**2
C
        RFAC_HK = RFAC_RN*RN_HK
        RFAC_RT = RFAC_RN*RN_RT
       ENDIF
C
C----- Amplification envelope slope correlation for Falkner-Skan
       ARG    = 3.87*HMI    - 2.52
       ARG_HK = 3.87*HMI_HK
C
       EX    = EXP(-ARG**2)
       EX_HK = EX * (-2.0*ARG*ARG_HK)
C
       DADR    = 0.028*(HK-1.0) - 0.0345*EX
       DADR_HK = 0.028          - 0.0345*EX_HK
C
C----- new m(H) correlation    1 March 91
       BRG = -20.0*HMI
       AF = -0.05 + 2.7*HMI -  5.5*HMI**2 + 3.0*HMI**3 + 0.1*EXP(BRG)
       AF_HMI =     2.7     - 11.0*HMI    + 9.0*HMI**2 - 2.0*EXP(BRG)
       AF_HK = AF_HMI*HMI_HK
C
       
C       RFAC = 1.0 ! RR: uncomment to prevent cubic (smooth) ramp up

       AX    = (AF   *DADR/TH                ) * RFAC
       AX_HK = (AF_HK*DADR/TH + AF*DADR_HK/TH) * RFAC
     &       + (AF   *DADR/TH                ) * RFAC_HK
       AX_TH = -AX/TH
       AX_RT = (AF   *DADR/TH                ) * RFAC_RT
C
      ENDIF
C
      !write (*, '(4g14.7)') HK, TH, RT, AX

      RETURN
      END ! DAMPL
