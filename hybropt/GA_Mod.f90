!
! ===================================================================
!
module GA_Mod
!
! ------------------------------------------------------------------- 
!
! Purpose: 
!    Contains neccessary subroutines to interface with the
!    Genetic Algorithm 
!
! Author:  
!    Oleg Chernukhin, Sept 2010
!
! -------------------------------------------------------------------

use CommonH
use Snopt_Mod
use SolverInterface_Mod

implicit none

! _____________________________
! module variables, interfaces

   integer :: fslimit, fscount

   !-- Interface to Evolver (Genetic Algorithm)
   interface
      subroutine evolvint (nFncI, nXI, XlowI, XuppI, &
                           popX, popFnc, ipop, i_ran_seed, &
                           nchrom, npop, iselect, prob_pert, &
                           prob_mut, prob_avg, xbest, xavg, xpert, &
                           xmut, calcfitint)
         interface 
            subroutine calcfitint(nx, pop, npop, fnc, nfnc, fit)
               integer :: nx, npop, nfnc
               real(kind(1.d0)) :: pop(npop, nx), fnc(npop, nfnc)
               real(kind(1.d0)) :: fit(npop)
            end subroutine
         end interface
         integer :: nFncI, ipop, i_ran_seed, nchrom, &
                    nXI, npop, iselect
          real(kind(1.d0)) :: prob_pert, prob_mut, prob_avg, &
                              xbest, xavg, pert, xmut, &
                              XlowI(nXI), XuppI(nXI), &
                              popX(nchrom, nXI), popFnc(nchrom, nFncI)
      end subroutine
   end interface

! ___________________
! module subroutines
contains
!
! =====================================================================
!
   subroutine runGenetic(popX, popFnc, mingen, maxgen, ranseed, &
                         selmethod, prob_pert, prob_mut, prob_avg, &
                         xbest, xavg, xpert, xmut, &
                         dosurr, sn_opt, maj_its_lim, flow_sol_lim)
!
! --------------------------------------------------------------------
!
! Purpose:
!    Runs the Genetic Algorithm for the specified number of 
!    generations. Depending on the input parameters, the objective
!    evaluation can be done through the flow-solver or surrogate. The
!    constraints can be satisfied using Quadratic Penalty Method
!    and/or SNOPT.
!
! Pre:
!    All common optimization parameters are set.
!    popX is filled with the initial sample
!
! --------------------------------------------------------------------
! _______________________
! variable specification
!

   !-- subroutine arguments
   integer :: mingen, maxgen, ranseed, selmethod, &
              maj_its_lim, flow_sol_lim, ga_rest_unit1, ga_rest_unit2
   real(kind=dp) :: prob_pert, prob_mut, prob_avg, xbest, xavg, &
                    xpert, xmut
   real(kind=dp) :: popX(popsize, nX), popFnc(popsize, nFnc)
   logical :: dosurr, & !-- If true, use surrogate model
              sn_opt !-- Optimize with SNOPT

   !-- local variables
   integer :: ii, jj, kk, mm, stat, altranseed, iPrint, iSumm, acc_feval,&
              file_stat
   real(kind=dp) :: X(nX), Fnc(nFnc), Grad(nnzGrad), fit(popsize)
   logical :: needF, needG, loc_opt(popsize), file_exists

! ___________________________________
! begin main execution

   !-- Set some local parameters
   fslimit = flow_sol_lim
   fscount = 0
   needF = .true.
   needG = .true.
   if (.not. sn_opt) needG = .false.
   iPrint = iPrintC
   iSumm = iSummC
   loc_opt(:) = .false.
 
   !TEMPORARY
   ga_rest_unit1 = 21986
   ga_rest_unit2 = 21987

   !-- Fill in popX, if this is a restart
   if (mingen > 1) then
      inquire(file='ga_restart.dat', exist=file_exists)
      call mpi_barrier(HYBROPT_COMM, ierr)

      if (myrank_glob .and. (.not. file_exists)) then
         write(*,*) 'ERROR: GA_Mod:runGenetic :: ', &
                    'ga_restart.dat file not found'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif

      open(unit=ga_rest_unit1, file='ga_restart.dat', status='old', &
           form='formatted', iostat=file_stat)
      if (file_stat /= 0) then
         write(*,*) 'ERROR: GA_Mod:runGenetic :: ', &
                    'error opening ga_restart.dat file'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
      
      do ii = 1, popsize
         do jj = 1, nX
            read(ga_rest_unit1, *) kk, mm, popX(kk, mm)
         end do
      end do

   endif

   !-- Now, iterate through the subsequent generations
   do ii = mingen, maxgen

      ind_feval = 0
      if (sn_opt) then

         !-- Use SNOPT to optimize the initial population

         if (myrank_loc == 0 .and. opt_output > 1) then
            write(iPrint,*) '========= SNOPT OUTPUT BELOW ========='
            write(iSumm,*)  '========= SNOPT OUTPUT BELOW ========='
         endif

         fscount = 0
         call runSNOPT(popX, popFnc, maj_its_lim, opt_output, stat, &
                       gasn_usrfun)
         fscount = 0

         !-- Identify local optimum
         loc_opt(:) = .false.
         if (stat == 1) loc_opt(mycid) = .true.

         if (myrank_loc == 0 .and. opt_output > 1) then
            write(iPrint,*) '========= END OF SNOPT OUTPUT ========='
            write(iSumm,*)  '========= END OF SNOPT OUTPUT ========='
         endif

      else

         !-- Satisfy linear constraints only. 
         if (.not. dosurr) then
            call runSNOPT(popX, popFnc, 10, 0, stat, &
                          lincon_usrfun)
         endif

      endif

      !-- Since popX may have been been modified, synchronize it globally
      do jj = 1, popsize
         call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, &
                        (jj-1)*ppc, HYBROPT_COMM, ierr)
      end do

      !-- Fill in X with values in popX, re-zero Fnc
      X(:) = popX(mycid, :)
      Fnc(:) = 0.0d0

      !-- Perform flowsolve. Note that if sn_opt is ON, flowsolve
      !-- will be avoided, since the database contains the result
      !-- from the last iteration of SNOPT
      if (.not. dosurr .and. ii == mingen) then
         stat = 1
      else
         stat = 0
      endif
      call evalf(nX, X, nFnc, Fnc, nnzGrad, Grad, needF, needG, &
                 stat, dosurr, useDatabase)

      !-- If stat == -1 then problems with flow solver (or surrogate 
      !-- model)at this design point. Set Fnc(1) to a maximum value 
      !-- (for minimization only)
      if (stat == -1) then
         Fnc(:) = 0.0d0
         Fnc(1) = infBnd
      endif

      !-- Calculate total number of function evaluations performed
      if (ii == mingen) acc_feval = 0
      tot_feval = 0
      call mpi_allreduce(ind_feval, tot_feval, 1, MPI_INTEGER, MPI_SUM, &
                         HYBROPT_COMM, ierr)
      acc_feval = acc_feval + tot_feval

      !-- Store Fnc values in popFnc, broadcast popFnc, loc_opt
      popFnc(mycid,:) = Fnc(:)
      do jj = 1, popsize
         call mpi_bcast(popFnc(jj,:), nFnc, MPI_DOUBLE_PRECISION, &
                        (jj-1)*ppc, HYBROPT_COMM, ierr)
         call mpi_bcast(loc_opt(jj), 1, MPI_LOGICAL, &
                        (jj-1)*ppc, HYBROPT_COMM, ierr)
      end do

      altranseed = ranseed + ii

      !-- Calculate fitness and print out the data for this generation
      if (myrank_loc == 0 .and. btest(opt_output,0)) then

         !-- Calculate fitness 
         call calcFit(nX, popX, popsize, popFnc, nFnc, fit)

         !-- Print out results 
         write(iPrint, *) '----- GENERATION', ii, '-----'
         do kk = 1, popsize
            if (loc_opt(kk)) then
               write(iPrint, *) kk, popFnc(kk, 1), fit(kk), &
                                '<-- Loc. Opt.'
            else
               write(iPrint, *) kk, popFnc(kk, 1), fit(kk)
            endif
         end do
         write(iPrint, *) '----- END OF GENERATION', ii, '-----'

         !-- Print the best fitness value, and total number of function
         !-- evaluations for the current generation
         write(iSumm, *) ii, ',', tot_feval, ',', acc_feval, &
                              ',', minval(fit(:))
      endif

      !-- Get next generation from the GA
      if (myrank_glob == 0) then
         call evolvint(nFnc, nX, Xlow, Xupp, popX, popFnc, ii, &
                       altranseed, popsize, maxgen, selmethod, &
                       prob_pert, prob_mut, prob_avg, &
                       xbest, xavg, xpert, xmut, calcFit)
      endif

      !-- Broadcast popX to all processes
      do jj = 1, popsize
         call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, 0, &
                        HYBROPT_COMM, ierr)
      end do

      !-- Re-zero popFnc
      popFnc(:,:) = 0.0d0

      !-- Write ga_restart.dat file with popX(:,:) for restarts
      if (ii == maxgen .and. myrank_glob == 0) then
         open(unit=ga_rest_unit2, file='ga_restart2.dat', status='new', &
              form='formatted', iostat=ierr)
         do kk = 1, popsize
            do jj = 1, nX
               write(ga_rest_unit2, *) kk, jj, popX(kk, jj)
            end do
          end do
         close(ga_rest_unit2)
      endif

   end do

end subroutine
!
! ===================================================================
!
subroutine lincon_usrfun(stat, nvar, dv, needf, nF, f, needG, lenG, G, &
                         cu, lencu, iu, leniu, ru, lenru)
!
! -------------------------------------------------------------------
!
! Purpose:
!    We want SNOPT to adjust the initial guess X such that the
!    linear constraints are satisfied. All we want to do here is set 
!    stat to -2 and return. No objective/constraints/gradient 
!    evaluations are neccessary.
!
! -------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   integer, intent(in) :: nvar, needf, nF, needG, lenG, lencu, leniu, &
                          lenru
   real(kind=dp), intent(in) :: dv(nvar)
   character(len=8), intent(inout) :: cu(lencu)
   integer, intent(inout) :: stat, iu(leniu)
   real(kind=dp), intent(inout) :: f(nF), G(lenG), ru(lenru)

! _____________________
! begin main execution

   stat = -2
   return

end subroutine
!
! =====================================================================
!
subroutine gasn_usrfun(stat, nvar, dv, needf, nF, f, needG, lenG, G, &
                       cu, lencu, iu, leniu, ru, lenru)
!
! ---------------------------------------------------------------------
! 
! Purpose:
!    Subroutine usrfun for SNOPT.
!
! ---------------------------------------------------------------------
! ______________________
! variable specification

   !-- subroutine arguments
   integer, intent(in) :: nvar, needf, nF, needG, lenG, lencu, leniu, &
                          lenru
   real(kind=dp), intent(in) :: dv(nvar)
   character(len=8), intent(inout) :: cu(lencu)
   integer, intent(inout) :: stat, iu(leniu)
   real(kind=dp), intent(inout) :: f(nF), G(lenG), ru(lenru)

   !-- local variables
   logical :: needobj, needgrad
   integer :: ii

! _____________________
! begin main execution

   !-- Synchronize all variables
   call mpi_bcast(stat, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(nvar, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(dv, nvar, MPI_DOUBLE_PRECISION, 0, comm(mycid), ierr)
   call mpi_bcast(needf, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(nF, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(f, nF, MPI_DOUBLE_PRECISION, 0, comm(mycid), ierr)
   call mpi_bcast(needG, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(lenG, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(G, lenG, MPI_DOUBLE_PRECISION, 0, comm(mycid), ierr)
   call mpi_bcast(lencu, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(leniu, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(lenru, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(cu, lencu, MPI_CHARACTER, 0, comm(mycid), ierr)
   call mpi_bcast(iu, leniu, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(ru, lenru, MPI_DOUBLE_PRECISION, 0, comm(mycid), ierr)

#if 1
   !-- Load balancing mechanism
   if (hybropt_method == 3 .and. load_bal_tol < 1.0d0) then
      if (myrank_glob == 0) then
         doneflag(1) = .false. !-- Global root is not done
         do ii = 2, popsize
            !-- Check who is done
            call mpi_test(done_recv_req(ii), doneflag(ii), statmpi_array, ierr)
         end do
 
         !-- Decide if it's time to terminate
         call hm_load_bal_decide(doneflag, decision_term)

         if (decision_term) then
            !-- Send terminate signal to others
            do ii = 2, popsize
               call mpi_send(decision_term, 1, MPI_LOGICAL, &
                             (ii-1)*ppc, term_tag, HYBROPT_COMM, &
                             ierr)
            end do
         endif

      elseif (myrank_loc == 0) then
         !-- Check for terminate signal
         call mpi_test(term_recv_req, termflag, statmpi_array, ierr)
         if (termflag) decision_term = .true.
      endif

      !-- Broadcast decision_term to non-roots
      call mpi_bcast(decision_term, 1, MPI_LOGICAL, 0, comm(mycid), ierr)

      !-- Exit if we've decided to terminate
      if (decision_term) then
         stat = -2
         if (myrank_loc == 0 .and. opt_output > 1) then
            write(iPrintC,*) 'INFO: Terminate signal'
         endif
         return      
      endif
   endif
#endif

   !-- Exit if this is the last call to usrfun
   if (stat >= 2) return

   !-- Exit if objective evaluation limit is exceeded
   if (fscount >= fslimit) then
      fscount = 0
      stat = -2
      if (myrank_loc == 0 .and. opt_output > 1) then
         write(iPrintC,*) 'INFO: Flow solve limit'
      endif
      return
   else
      fscount = fscount + 1
   endif

   needobj = .true.
   if (needf == 0) needobj = .false.
   needgrad = .true.
   if (needG == 0) needgrad = .false.

   !-- Evaluate objective
   call evalf(nvar, dv, nF, f, lenG, G, needobj, needgrad, &
              stat, .false., useDatabase)

   call mpi_barrier(comm(mycid), ierr)

end subroutine
!
! =====================================================================
!
subroutine calcFit(ndv, popX, npop, popFnc, nf, fit)
!
! ---------------------------------------------------------------------
!
! Purpose:
!    Calculates fitness using Quadratic Penalty Method
!
! ---------------------------------------------------------------------
! ___________________________________
! variable specification

   !-- subroutine arguments
   integer :: ndv, npop, nf
   real(kind=dp) :: popX(npop, ndv), popFnc(npop, nf)
   real(kind=dp) :: fit(npop)

   !-- local variables
   integer :: kk, jj, mm
   real(kind=dp) :: tmpcv

! _____________________
! begin main execution

   do kk = 1, popsize
      
      !-- Nonlinear component of objective
      fit(kk) = popFnc(kk, 1)
   
      !-- Add linear component of objective
      do jj = 1, nnzLinG
         if (iLfun(jj) == 1) then
            fit(kk) = fit(kk) + LinG(jj) * popX(kk, jLvar(jj))
         endif
      end do

      !-- Add penalty terms to fitness
      do jj = 2, nFnc

         !-- Nonlinear component of constraint
         tmpcv = popFnc(kk, jj)

         !-- Add linear component of constraint
         do mm = 1, nnzLinG
            if (iLfun(mm) == jj) then
               tmpcv = tmpcv + LinG(mm) * popX(kk, jLvar(mm))
            endif
         end do

         !-- Add penalty term if constraint is violated
         if (tmpcv .lt. Flow(jj)) then
            fit(kk) = fit(kk) + penalty * abs(tmpcv - Flow(jj))**2
         elseif (tmpcv .gt. Fupp(jj)) then
            fit(kk) = fit(kk) + penalty * abs(tmpcv - Fupp(jj))**2
         endif

      end do

   end do

end subroutine
!
! =====================================================================
!
end module

