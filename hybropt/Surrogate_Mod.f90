!
! =====================================================================
!
module Surrogate_Mod
!
! --------------------------------------------------------------------- 
! Purpose: Contains subroutines for surrogate models
!
! Author:  Oleg Chernukhin, Aug 2010
! ---------------------------------------------------------------------
!

use CommonH
use Database_Mod
use Snopt_Mod

implicit none

! _________________________________________
! module variables, constants and structures

type(dbEntry), allocatable :: neib(:)
integer :: currFnc

! _________________________________
! module subroutines
contains
!
! ===================================================================
!
subroutine evalSurrogate(nX, X, nF, F, nG, G, stat, smodel)
!
! -------------------------------------------------------------------
! Purpose: 
!    Entry point to creating a surrogate model      
!
! -------------------------------------------------------------------
! _______________________
! variable specification
        
   !-- subroutine arguments
   integer :: nX, nF, nG, stat, smodel
   real(kind=dp) :: X(nX), F(nF), G(nG)

   !-- local variables
   integer :: ii
   real(kind=dp) :: del(nF)

! ________________________
! begin main execution
 
   !-- First, check if there are any values in the database
   if (.not. allocated(allentries)) then
      stat = -1
      return
   endif
   if (size(allentries)<1) then
      stat = -1 
      return
   endif
 
   !-- Case select
   select case (smodel)

   case(1)
      !-- Kriging Model
      call krigingModel(nX, X, nF, F, nG, G, stat)
     
   case(2)
      !-- Radial Basis Function
      call radialBasis(nX, X, nF, F, nG, G, stat)

   case(3)
      !-- Gradient-enhanced RBF
      call gradientRBF(nX, X, nF, F, nG, G, stat)

   case default

      write(*,*) 'ERROR: wrong surrogate model'
      call mpi_abort(MPI_COMM_WORLD, 0, ierr)

   end select

end subroutine
!
! =====================================================================
!
subroutine krigingModel(nX, X, nF, F, nG, G, stat)
!
! ---------------------------------------------------------------------
!
! Purpose:
!    Constructs Kriging surrogate model and returns its value at 
!    point X.
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   integer :: nX, nF, nG, stat
   real(kind=dp) :: X(nX), F(nF), G(nG)

   !-- local variables
   integer :: nnpt, ii, jj, kk, info, actnnpt
   integer, allocatable :: ipiv(:)
   real(kind=dp), allocatable :: Rmtx(:,:), Rinv(:,:), rv(:,:), &
                                 yv(:,:), fbv(:,:), ymbf(:,:), t1(:,:)
   real(kind=dp) :: theta, beta, rz(1,1)

! _______________________
! begin main execution

   !-- Return, unless this is a local root
   if (myrank_loc .ne. 0) return

   !-- Set desired number of neighbours
   nnpt = 5

   !-- Allocate space for entries
   if (allocated(neib)) then
      do ii = 1, size(neib)
         deallocate(neib(ii)%X)
         deallocate(neib(ii)%Fnc)
         deallocate(neib(ii)%G)
      end do
      deallocate(neib)
   endif
   allocate(neib(nnpt))
   do ii = 1, nnpt
      allocate(neib(ii)%X(nX))
      allocate(neib(ii)%Fnc(nF))
      allocate(neib(ii)%G(nG))
   end do

   !-- Select suitable neighbouring points
   call selectNeighbours(nnpt, nX, X, actnnpt)
   nnpt = actnnpt

   !-- Make sure some suitable neighbours have been found
   if (nnpt < 1) then
      do ii = 1, nnpt
         deallocate(neib(ii)% X)
         deallocate(neib(ii)% Fnc)
         deallocate(neib(ii)% G)
      end do
      deallocate(neib)
      stat = -1
      return
   endif

   !-- Allocate work arrays
   allocate(Rmtx(nnpt, nnpt), Rinv(nnpt, nnpt), yv(nnpt,1), &
            fbv(nnpt,1), rv(nnpt, 1), ymbf(nnpt, 1), t1(nnpt, nnpt))

   !-- Iterate through each objective and constraint functions
   do kk = 1, nF

      currFnc = kk
      theta = 1.0d0

      !-- Find optimal theta
      call runSnoptMLE(theta, mle_usrfun)

      !-- Compute correlation matrix R
      Rmtx = computeR(nnpt, neib, theta)

      !-- Compute inverse of matrix R
      Rinv = computeRinv(nnpt, Rmtx)

      !-- Compute beta
      beta = betaHat(nnpt, Rinv, neib, kk)

      !-- Compute correlation vector rv
      rv = computeRV(nnpt, neib, X, theta)

      !-- Estimate of the function
      do ii = 1, nnpt
         yv(ii, 1) = neib(ii)% Fnc(kk)
         fbv(ii,1) = beta
      end do
      ymbf(:,:) = yv(:,:) - fbv(:,:)

      call dgemm('t', 'n', 1, nnpt, nnpt, 1.0d0, rv, &
                 nnpt, Rinv, nnpt, 0.0d0, t1, 1)
      call dgemm('n', 'n', 1, 1, nnpt, 1.0d0, t1, 1, ymbf, &
                 nnpt, 0.0d0, rz, 1)

      F(kk) = beta + rz(1,1)

   end do

   deallocate(Rmtx, Rinv)
   
end subroutine
!
! =====================================================================
!
function computeR(ne, rdata, theta)
!
! ---------------------------------------------------------------------
!
! Purpose: Computes R (used in constructing Kriging surface)
!    
! ---------------------------------------------------------------------
! ________________________
! variable specification

   !-- function arguments
   integer :: ne
   real(kind=dp) :: theta
   type(dbEntry) :: rdata(ne)

   !-- function output
   real(kind=dp) :: computeR(ne, ne)

   !-- local variables
   integer :: ii, jj
   real(kind=dp) :: x1(rdata(1)%nX), x2(rdata(1)%nX)

! _____________________
! begin main execution

   computeR(:,:) = 0.0d0
   
   do ii = 1, ne
      do jj = 1, ne
         x1(:) = rdata(ii)%X(:)
         x2(:) = rdata(jj)%X(:)
         computeR(ii, jj) = corrR(size(x1), x1, x2, theta)
      end do
   end do 

end function
!
! =====================================================================
!
function computeRV(ne, rdata, X, theta)
!
! ---------------------------------------------------------------------
!
! Purpose: Computes R-vector (used in constructing Kriging surface)
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- function arguments
   integer :: ne
   type(dbEntry) :: rdata(ne)
   real(kind=dp) :: theta
   real(kind=dp) :: X(rdata(1)%nX)

   !-- function output
   real(kind=dp) :: computeRV(ne,1)

   !-- local variables
   integer :: ii
   real(kind=dp) :: x1(rdata(1)%nX), x2(rdata(1)%nX)

! _____________________
! begin main execution

   computeRV(:,:) = 0.0d0

   do ii = 1, ne
      x1(:) = X(:)
      x2(:) = rdata(ii)%X(:)
      computeRV(ii, 1) = corrR(size(x1), x1, x2, theta)
   end do

end function
!
! =====================================================================
!
function corrR(nx, x1, x2, theta)
!
! ---------------------------------------------------------------------
!
! Purpose: computes correlation function (used in constructing 
!          Kriging surfaces)
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- function arguments
   integer :: nx
   real(kind=dp) :: x1(nx), x2(nx), theta

   !-- function output
   real(kind=dp) :: corrR

   !-- local variables
   real(kind=dp) :: del(nx)

! _____________________
! begin main execution  

   !-- Exponential correlation function
   del(:) = (abs(x1(:) - x2(:)))**2
   corrR = exp(-theta * sum(del))   

end function
!
! =====================================================================
!
function detR(ne, R)
!
! ---------------------------------------------------------------------
!
! Purpose: computes determinant of matrix R(ne,ne)
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- function arguments
   integer :: ne
   real(kind=dp) :: R(ne, ne)

   !-- function output
   real(kind=dp) :: detR   

   !-- local variables
   integer :: ipiv(ne), info, ii
   real(kind=dp) :: rcopy(ne, ne)

! _____________________
! begin main execution

   rcopy(:,:) = R(:,:)

   call dgetrf(ne, ne, rcopy, ne, ipiv, info)

   detR = 1.0d0
   do ii = 1, ne
      if (ipiv(ii) .ne. ii) then
         detR = -detR * rcopy(ii, ii)
      else
         detR =  detR * rcopy(ii, ii)
      endif
   end do

end function
!
! =====================================================================
!
function computeRinv(ne, R)
!
! ---------------------------------------------------------------------
!
! Purpose: computes inverse of matrix R(ne,ne)
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- function arguments
   integer :: ne
   real(kind=dp) :: R(ne, ne)

   !-- function output
   real(kind=dp) :: computeRinv(ne, ne)

   !-- local variables
   integer :: nrow, ncol, lda, info, lwork
   integer :: ipiv(ne)
   real(kind=dp) :: rcopy(ne, ne)
   real(kind=dp) :: work(4*ne)

! _____________________
! begin main execution

   nrow = ne
   ncol = ne
   lda = ne
   lwork = 4*ne
   rcopy(:,:) = R(:,:)
   computeRinv(:,:) = 0.0d0

   call dgetrf(nrow, ncol, rcopy, lda, ipiv, info)
   call dgetri(nrow, rcopy, lda, ipiv, work, lwork, info)

   computeRinv(:,:) = rcopy(:,:)

end function
!
! =====================================================================
!
function varianceK(ne, Rinv, bhat, rdata, fidx)
!
! ---------------------------------------------------------------------
!
! Purpose: computes variance value for MLE problem (used for
!          constructing Kriging surfaces)
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- function arguments
   integer :: ne, fidx
   real(kind=dp) :: Rinv(ne, ne), bhat
   type(dbEntry) :: rdata(ne)

   !-- function output
   real(kind=dp) :: varianceK

   !-- local variables
   integer :: ii
   real(kind=dp) :: y(ne,1), fb(ne,1), ymfb(ne,1), t1(1,ne), t2(1,1)

! _____________________
! begin main execution

   do ii = 1, ne
      y(ii,1) = rdata(ii)%Fnc(fidx)
   end do
 
   fb(:,:) = bhat
   ymfb(:,:) = y(:,:) - fb(:,:)

   call dgemm('t','n',1,ne,ne,1.0d0,ymfb,ne,Rinv,ne,0.0d0,t1,1)
   call dgemm('n','n',1,1,ne,1.0d0,t1,1,ymfb,ne,0.0d0,t2,1)
   
   varianceK = t2(1,1) / ne

end function
!
! =====================================================================
!
function betaHat(ne, Rinv, rdata, fidx)
!
! ---------------------------------------------------------------------
!
! Purpose: computes value of beta-hat (used in constructing Kriging
!          surfaces)
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- function arguments
   integer :: ne, fidx
   real(kind=dp) :: Rinv(ne,ne)
   type(dbEntry) :: rdata(ne)   

   !-- function output
   real(kind=dp) :: betaHat

   !-- local variables
   integer :: ii
   real(kind=dp) :: fv(ne,1), yv(ne, 1)
   real(kind=dp) :: t1(1,ne), t2(1,1), t2s, t3(1,ne)

! ___________________________________
! begin main execution
   
   !-- Refer to "Using Gradients to Construct Cokriging Approximation
   !-- Models for High-Dimensional Design Optimization Problems" paper
   !-- by H.-S. Chung and J. Alonso (AIAA Journal, 2002-0317)

   fv(:,:) = 1.0d0
   do ii = 1, ne
      yv(ii,1) = rdata(ii)%Fnc(fidx)
   end do

   !-- t1 = fv' * Rinv
   call dgemm('t', 'n', 1, ne, ne, 1.0d0, fv, ne, Rinv, ne, 0.0d0, t1, 1)

   !-- t2 = t1 * fv
   call dgemm('n', 'n', 1, 1, ne, 1.0d0, t1, 1, fv, ne, 0.0d0, t2, 1)

   !-- t2s = t2^-1 
   t2s = 1.0d0 / t2(1,1)

   !-- t1 = t2s * fv'
   t1(:,:) = t2s

   !-- t3 = t1 * Rinv
   call dgemm('n', 'n', 1, ne, ne, 1.0d0, t1, 1, Rinv, ne, 0.0d0, t3, 1)

   !-- t2 = t3 * yv
   call dgemm('n', 'n', 1, 1, ne, 1.0d0, t3, 1, yv, ne, 0.0d0, t2, 1)

   betahat = t2(1,1)

end function
!
! =====================================================================
!
function mleobj(ne, var, detrm)
!
! ---------------------------------------------------------------------
!
! Purpose: calculates function value for Maximum Likelihood Estimator 
!          optimization problem
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- function arguments
   integer :: ne
   real(kind=dp) :: var, detrm
   
   !-- function output
   real(kind=dp) :: mleobj

! ________________________
! begin main execution

   mleobj = -0.5d0 * ne * log(var) + log(detrm)

end function
!
! =====================================================================
!
subroutine mle_usrfun(stat, nvar, dv, needf, nF, f, needG, lenG, G, &
                      cu, lencu, iu, leniu, ru, lenru)
!
! ---------------------------------------------------------------------
!
! Purpose: Subroutine usrfun for SNOPT. Used in MLE optimization
!          problem, for constructing Kriging surfaces
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   integer, intent(in) :: nvar, needf, nF, needG, lenG, lencu, leniu, &
                          lenru
   real(kind=dp), intent(in) :: dv(nvar)
   character(len=8), intent(inout) :: cu(lencu)
   integer, intent(inout) :: stat, iu(leniu)
   real(kind=dp), intent(inout) :: f(nF), G(lenG), ru(lenru)

   !-- local variables
   real(kind=dp) :: beta, var, detrm
   real(kind=dp) :: Rmtx(size(neib), size(neib))
   real(kind=dp) :: Rinv(size(neib), size(neib))

! __________________________
! begin main execution
   Rmtx = computeR(size(neib), neib, dv(1))
   Rinv = computeRinv(size(neib), Rmtx)
   beta = betaHat(size(neib), Rinv, neib, currFnc)
   var = varianceK(size(neib), Rinv, beta, neib, currFnc)
   detrm = detR(size(neib), Rmtx)
   f(1) = mleobj(size(neib), var, detrm)

end subroutine
!
! =====================================================================
!
subroutine radialBasis(nX, X, nF, F, nG, G, stat)
!
! ---------------------------------------------------------------------
!
! Purpose: Constructs radial basis function (RBF) surrogate model,
!          and approximates the value of the function at point X
! 
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   integer :: nX, nF, nG, stat
   real(kind=dp) :: X(nX), F(nF), G(nG)   

   !-- local variables
   integer :: nnpt, ii, jj, kk, nrhs, info, lda, ldb, actnnpt
   type(dbEntry), allocatable :: neib(:)
   real(kind=dp), allocatable :: lhsK(:,:), rhsK(:,:), &
                                 gvec(:,:), hvec(:,:), bvec(:,:), &
                                 entryLHS(:,:)
   integer, allocatable :: ipiv(:)
   real(kind=dp) :: mean, fv, sigma, del(nX)
 
! ___________________________________
! begin execution

   !-- This may or may not be temporary, depending on the size
   !-- of the system that needs to be solved for surrogate model
   if (myrank_loc .ne. 0) return
  
   !-- Set initial number of neighbours 
   !-- Note: this should depend on problem dimensionality
   nnpt = 20

   !-- Allocate space for entries
   allocate(neib(nnpt))
   do ii = 1, nnpt
      allocate(neib(ii)%X(nX))
      allocate(neib(ii)%Fnc(nF))
      allocate(neib(ii)%G(nG))
   end do

   !-- Select suitable neighbouring points
   call selectNeighbours(nnpt, nX, X, actnnpt)
   nnpt = actnnpt

   !-- Make sure some suitable neighbours have been found
   if (nnpt < 1) then
      stat = -1
      return
   endif

   !-- Allocate space for the linear system
   allocate(rhsK(nnpt, 1), lhsK(nnpt, nnpt))   
   allocate(entryLHS(1, 1))
   allocate(ipiv(nnpt))

   !-- Set sigma
!   sigma = 1.0d0

!   sigma = 1.0d0 / sqrt(2.0d0)

   del(:) = X(:) - allentries(ii)% X(:)
   del(:) = del(:) * del(:)
   sigma = sqrt(sum(del(:)))

   !-- Fill in the LHS only once
   do ii = 1, nnpt
      do jj = 1, nnpt
         !-- First entry is the radial basis function         
         lhsK(ii,jj) = rbf(nX, neib(ii)%X(:), neib(jj)%X(:), sigma)
      end do
   end do

   !-- Calculate surrogate model for each objective and constraint
   do kk = 1, nF
  
      !-- Fill in the RHS
      do ii = 1, nnpt
         rhsK(ii, 1) = neib(ii)%Fnc(kk)
      end do

      !-- Solve the system
      nrhs = 1
      lda = nnpt
      ldb = nnpt
      call dgesv(nnpt, nrhs, lhsK(:,:), lda, ipiv(:), rhsK(:,:), ldb, info)
      if (info .ne. 0) then
         print *, 'ERROR: RBF'
         stat = -1
         return
      endif

      !-- Calculate the function value
      fv = 0.0d0
      do ii = 1, nnpt
         fv = fv + rhsK(ii, 1) * rbf(nX, X(:), neib(ii)%X(:), sigma)
      end do

      !-- Update the function value
      F(kk) = fv

   end do

end subroutine
!
! =====================================================================
!
subroutine gradientRBF(nX, X, nF, F, nG, G, stat)
!
! ---------------------------------------------------------------------
!
! Purpose: constructs gradient-enhanced radial basis function 
!          surrogate model
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   integer :: nX, nF, nG, stat
   real(kind=dp) :: X(nX), F(nF), G(nG)   

   !-- local variables
   integer :: nnpt, ii, jj, kk, nrhs, info, lda, ldb, actnnpt
   type(dbEntry), allocatable :: neib(:)
   real(kind=dp), allocatable :: lhsK(:,:), rhsK(:,:), &
                                 gvec(:,:), hvec(:,:), bvec(:,:), &
                                 entryLHS(:,:)
   integer, allocatable :: ipiv(:)
   real(kind=dp) :: mean, fv, sigma, del(nX)
 
! ________________________
! begin main execution

   !-- This may or may not be temporary, depending on the size
   !-- of the system that needs to be solved for surrogate model
   if (myrank_loc .ne. 0) return
  
   !-- Set initial number of neighbours 
   !-- Note: this should depend on problem dimensionality
   nnpt = 5

   !-- Allocate space for entries
   if (allocated(neib)) then
      do ii = 1, size(neib) 
         deallocate(neib(ii)%X)
         deallocate(neib(ii)%Fnc)
         deallocate(neib(ii)%G)
      end do
      deallocate(neib)
   endif
   allocate(neib(nnpt))
   do ii = 1, nnpt
      allocate(neib(ii)%X(nX))
      allocate(neib(ii)%Fnc(nF))
      allocate(neib(ii)%G(nG))
   end do

   !-- Select suitable neighbouring points
   call selectNeighbours(nnpt, nX, X, actnnpt)
   nnpt = actnnpt

   !-- Make sure some suitable neighbours have been found
   if (nnpt < 1) then
      do ii = 1, nnpt
         deallocate(neib(ii)% X)
         deallocate(neib(ii)% Fnc)
         deallocate(neib(ii)% G)
      end do
      deallocate(neib)
      stat = -1
      return
   endif

   !-- Allocate space for the linear system
   allocate(rhsK(nnpt*(nX+1), 1), lhsK(nnpt*(nX+1), nnpt*(nX+1)))   
   allocate(gvec(nX, 1), bvec(nX, 1), hvec(nX, nX), entryLHS(nX+1, nX+1))
   allocate(ipiv(nnpt))

   !-- Set sigma
   sigma = 1.0d0

   !-- Fill in the LHS only once
   do ii = 1, nnpt
      do jj = 1, nnpt
         entryLHS(:,:) = 0.0d0

         !-- Calculate gradient of RBF
         gvec(:,:) = rbfp(nX, neib(ii)%X(:), neib(jj)%X(:), sigma)

         !-- Calculate hessian matrix of RBF
         hvec(:,:) = rbfpp(nX, neib(ii)%X(:), neib(jj)%X(:), sigma)

         !-- First entry is the radial basis function         
         entryLHS(1,1) = rbf(nX, neib(ii)%X(:), neib(jj)%X(:), sigma)

         !-- Remaining entries in first column are gradient values of rbf
         entryLHS(2:nX+1,1:1) = gvec(:,:)

         !-- Remaining entries in first row are gradient values of rbf
         entryLHS(1:1,2:nX+1) = transpose(gvec(:,:))

         !-- Remaining entries in the matrix are hessian values of rbf
         entryLHS(2:nX+1,2:nX+1) = hvec(:,:)

         !-- Copy this entry in the appropriate location in lhsK
         lhsK(((ii-1)*(nX+1)+1):((ii-1)*(nX+1)+1+nX), &
              ((jj-1)*(nX+1)+1):((jj-1)*(nX+1)+1+nX)) &
              = entryLHS(:,:)
      end do
   end do

   !-- Calculate surrogate model for each objective and constraint
   do kk = 1, nF
  
      !-- Re-zero rhsK
      rhsK(:,:) = 0.0d0

      !-- Fill in the RHS
      do ii = 1, nnpt
         !-- First entry is function value
         rhsK((ii-1)*(nX+1)+1, 1) = neib(ii)%Fnc(kk)
         
         !-- Subsequent entries are derivative values
         !-- Enter non-zero values using 
         !-- nnzGrad, iGfun, jGvar, and G
         do jj = 1, nnzGrad
            if (iGfun(jj) == kk) then
               rhsK((ii-1)*(nX+1)+1+jGvar(jj), 1) = neib(ii)%G(jj)
            endif
         end do
      end do

      !-- Solve the system
      nrhs = 1
      lda = nnpt
      ldb = nnpt
      call dgesv(nnpt, nrhs, lhsK(:,:), lda, ipiv(:), rhsK(:,:), ldb, info)
      if (info .ne. 0) then
         print *, 'ERROR: GRADIENT-ENHANCED RBF'
         stat = -1
         return
      endif

      !-- Calculate the function value
      fv = 0.0d0
      do ii = 1, nnpt
         fv = fv + rhsK((ii-1)*(nX+1)+1, 1) * &
              rbf(nX, X(:), neib(ii)%X(:), sigma)
      end do
   
      do ii = 1, nnpt
         bvec(:,:) = rbfp(nX, X(:), neib(ii)%X(:), sigma)
         do jj = 1, nX
            fv = fv + rhsK((ii-1)*(nX+1)+1+jj, 1) * bvec(jj, 1)
         end do
      end do

      !-- Update the function value
      F(kk) = fv

   end do

end subroutine
!
! =====================================================================
!
function rbf(nd, d1, d2, sigma)
!
! ---------------------------------------------------------------------
! 
! Purpose: computes the value of the radial basis function
!
! ---------------------------------------------------------------------
! _______________________
! variable specifiaction

   !-- function arguments
   integer :: nd
   real(kind=dp) :: d1(nd), d2(nd), sigma

   !-- function output
   real(kind=dp) :: rbf

   !-- local variables
   real(kind=dp) :: del(nd), h

! _____________________________
! begin main execution

   del(:) = d1(:) - d2(:)
   del(:) = del(:) * del(:)
   h = sqrt(sum(del(:)))

   !-- exponential RBF   
   rbf = exp(-h**2/(2*sigma**2))

end function
!
! =====================================================================
!
function rbfp(nd, d1, d2, sigma)
!
! ---------------------------------------------------------------------
!
! Purpose: computes first derivative of RBF
!
! --------------------------------------------------------------------- 
! ________________________
! variable specification

   !-- function arguments
   integer :: nd
   real(kind=dp) :: d1(nd), d2(nd), sigma

   !-- function output
   real(kind=dp) :: rbfp(nd,1)

   !-- local variables
   integer :: ii
   real(kind=dp) :: del(nd), h

! _______________________
! begin main execution

   del(:) = d1(:) - d2(:)

   do ii = 1, nd
      rbfp(ii,1) = - (1/sigma**2) * rbf(nd, d1, d2, sigma) * del(ii);
   end do

end function
!
! ===================================================================
!
function rbfpp(nd, d1, d2, sigma)
!
! ---------------------------------------------------------------------
!
! Purpose: computes second derivative of RBF
!
! ---------------------------------------------------------------------
! ______________________________
! variable specification

   !-- function arguments
   integer :: nd
   real(kind=dp) :: d1(nd), d2(nd), sigma

   !-- function output
   real(kind=dp) :: rbfpp(nd, nd)

   !-- local variables
   integer :: ii, jj
   real(kind=dp) :: del(nd)

! ________________________
! begin main execution

   del(:) = d1(:) - d2(:)

   !-- Fill-in the common terms
   do ii = 1, nd
      do jj = 1, nd
        rbfpp(ii, jj) = rbf(nd, d1, d2, sigma) / sigma**4;
      end do
   end do
 
   !-- Fill-in the distinct terms
   do ii = 1, nd
      do jj = 1, nd
         if (ii == jj) then          
            rbfpp(ii, jj) = rbfpp(ii, jj) * &
                            -1.d0 * (sigma**2 - del(ii)**2);            
         else
            rbfpp(ii, jj) = rbfpp(ii, jj) * &
                            del(ii) * del(jj);
         endif
      end do
   end do

end function
!
! ===================================================================
!
subroutine selectNeighbours(nn, nX, X, ann)
!
! -------------------------------------------------------------------
!
! Purpose:
!    Attempts to select nn suitable points from the database. The
!    first criteria is proximity to the point X. It must be a
!    valid entry. Ideally, the neighbours would be evenly spaced out.
!    If only ann < nn suitable neighbours are found, the calling
!    process should read value ann and take appropriate action.
!
! -------------------------------------------------------------------
! ________________________
! variable specification

   !-- subroutine arguments
   integer, intent(in) :: nn, nX
   real(kind=dp), intent(in) :: X(nX)
   integer, intent(out) :: ann

   !-- local variables
   integer :: ii, jj, kk, ne, tmpval
   integer, allocatable :: sort(:), sel(:)
   real(kind=dp), allocatable :: del(:), dist(:)
   logical :: done

! ___________________________________
! begin main execution

   ann = nn
   ne = size(allentries)
   if (ne <= nn) then
      !-- Copy values from allentries to neib
      do ii = 1, ne
         neib(ii)% nX = allentries(ii)% nX
         do jj = 1, neib(ii)%nX
            neib(ii)%X(jj) = allentries(ii)%X(jj)
         end do
         neib(ii)% nFnc = allentries(ii)% nFnc
         do jj = 1, neib(ii)%nFnc
            neib(ii)%Fnc(jj) = allentries(ii)%Fnc(jj)
         end do
         neib(ii)% haveG = allentries(ii)% haveG
         neib(ii)% nG = allentries(ii)% nG
         do jj = 1, neib(ii)%nG
            neib(ii)%G(jj) = allentries(ii)%G(jj)
         end do
         neib(ii)% valid = allentries(ii)% valid
      end do
      ann = ne
   else

      allocate(sort(ne), dist(ne), del(nX), sel(nn))

      !-- Calculate distances
      do ii = 1, ne
         del(:) = X(:) - allentries(ii)% X(:)
         del(:) = del(:) * del(:)
         dist(ii) = sqrt(sum(del(:))) 
         sort(ii) = ii
      end do
   
      !-- Sort
      !-- Insertion sort with only sort(:) being re-arranged
      do ii = 2, ne
         tmpval = sort(ii)
         jj = ii - 1
         done = .false.  
         do
            if (dist(sort(jj)) > dist(ii)) then
               sort(jj+1) = sort(jj)
               jj = jj - 1
               if (jj < 1) done = .true.
            else
               done = .true.
            endif
            if (done) exit
         end do
         sort(jj+1) = tmpval
      end do

      !-- Select points
      ann = 0
      jj = 1
      do ii = 1, ne
         if (allentries(sort(ii))% valid) then
            !-- Need to do additional checks when selecting neighbours
            !-- TBI...

            sel(jj) = sort(ii)
            ann = jj

            jj = jj + 1

         endif

         if (jj > size(sel)) then
            exit
         endif

      end do     


      !-- Copy values from allentries to neib
      do ii = 1, ann
         neib(ii)% nX = allentries(sel(ii))% nX
         do jj = 1, neib(ii)%nX
            neib(ii)%X(jj) = allentries(sel(ii))%X(jj)
         end do
         neib(ii)% nFnc = allentries(sel(ii))% nFnc
         do jj = 1, neib(ii)%nFnc
            neib(ii)%Fnc(jj) = allentries(sel(ii))%Fnc(jj)
         end do
         neib(ii)% haveG = allentries(sel(ii))% haveG
         neib(ii)% nG = allentries(sel(ii))% nG
         do jj = 1, neib(ii)%nG
            neib(ii)%G(jj) = allentries(sel(ii))%G(jj)
         end do
         neib(ii)% valid = allentries(sel(ii))% valid
      end do

   endif

   if (allocated(sort)) deallocate(sort)
   if (allocated(dist)) deallocate(dist)
   if (allocated(del)) deallocate(del)
   if (allocated(sel)) deallocate(sel)

end subroutine
!
! =====================================================================
!
end module

