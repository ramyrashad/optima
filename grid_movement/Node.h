
#ifndef NODE_H
#define NODE_H
#include <iostream>
#include <iomanip>
#include <cmath>                  
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include "tagSort.h" 

using namespace std;

class Node  
{
private:

	double *xyz;		// stores the coordinates of the node
	double *new_xyz;      
	double *dist;   // stores the distance to each neighbours
	int ifoil, num_nei, ID, ind;
	Node ** nei;
	bool surf, ThreeD; // indicates whether the node lies on the boundary
	Node *overlap;   // show the node that this node overlaps
	int symm;   // symmetry: 1=xy, 2=yz, 3=xz

public:
	
	//Default constructor
	Node(); 
	
	//Constructor parameters
	Node(double *coord, const int& nodeID, const bool& dim);
	Node(const double& x, const double& y, const int& nodeID);
	Node(const double& x, const double& y, const double& z, const int& nodeID);

	~Node();       // Destructor
	// clears all dynamically allocated memory 
	void clearmem();
	
	// Accessor functions
        double get_x() const {return xyz[0];}
	double get_xnew() const {return new_xyz[0];}
        double * get_xyz() const {return xyz;}
	double * get_coord() const { return new_xyz;}
	Node **get_nei() const {return nei;}
	int get_num_nei() const { return num_nei;}
	bool if_surf() const { return surf;}
	int get_ID() const { return ID;}
	int get_ind() const { return ind;}
	int get_ifoil() const { return ifoil;}
	int get_symm() const { return symm;}
	bool get_dim() const { return ThreeD;}
        double* get_dist();
	void overlaps(Node* a);
	Node *get_overlap() const {return overlap;}
	
	// Output operator
	friend ostream& operator <<(ostream& out, const Node& a);
	friend class Face;
	friend class Vol;
		
	// function to add a neighbour to a node
	void add_nei(Node& a);
		
	// function to indicate whether the node is on the surface/boundary
	void is_surf();
	
	// function to set the coordinate of a node
	void set_coord(double *a);
	void set_xyz(double *a);
	void set_new_y(const double &y);
	void set_symm(const int a);

	void set_ind(const int& a);
	void set_ifoil(const int& a);
	
	void deform (const double & xcoord, const double& ycoord);
	void deform_xyz (const double & xcoord, const double& ycoord);
        void deform (const double & xcoord, const double& ycoord,
		   const double& zcoord);
	
};

#endif //NODE_H

