      subroutine residinf(jdim,kdim,s)
     &      

#include "../include/arcom.inc"

      integer jdim, kdim

      double precision s(jdim,kdim,4)


      resid = 0.d0
      do n = 1,4
        do k = 1,kmax
          do j = 1,jmax
             if (abs(s(j,k,n)).gt.resid)then
                resid=abs(s(j,k,n))
             endif
          end do
        end do
      end do


      return                                                         
      end                       !residl2
