************************************************************************
      !-- Program name: wt_int_dps
      !-- Written by: Howard Buckley
      !-- Date: Dec 2010
      !-- 
      !-- This subroutine generates design points for an objective 
      !-- function defined as the weighted integral of D/L over a range
      !-- of operating coditions.
************************************************************************


      subroutine wt_int_dps(int_wts, wt_int_m, wt_int_cl, ondpTotal,
     &     delm, delwt, delalt, wt_int_re, int_wts2)

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      
      !-- Declare Optima2D variables

      integer 
     &     i, j, k, ii, ondpTotal, mp

      double precision 
     &     delwt, delm, delalt,  mach, cltarg, a, 
     &     wt_int_m(nm*nwt*nalt), wt_int_cl(nm*nwt*nalt),
     &     wt_int_re(nm*nwt*nalt), cl_inv(nm,nwt,nalt),
     &     t(nm,nwt,nalt), d(nm,nwt,nalt), int_wts(nm*nwt*nalt),
     &     int_wts2(nm*nwt*nalt),
     &     t0, p0, mu0, g, M, R, L, ar, lift_per_span, alt,
     &     wt, talt, palt, rho, sndspalt, reyno, mu, gam, K_mdo

#ifdef _MPI_VERSION
      mp = rank+1
#else
      mp = 1
#endif

      !-- Define constants used in calculation of operating conditions
      t0 = 288.15 ! Air temp at sea level (K)
      p0 = 101325 ! Air press at sea level (Pa)
      mu0 = 1.7894e-5 ! Dynamic viscosity of air at sea level (kg/m.s)
      g = 9.81 ! Accel due to gravity
      M = 0.0289644 ! Molar density for dry air
      R = 8.31447 ! Universal gas constant
      L = 0.00649 ! Temperature lapse rate for air (K/m) upto 11,000m
      ar = (wspan**2)/warea ! Wing aspect ratio
      gam = 1.4 ! Ratio of specific heats
      sweep = sweep*pi/180 ! Convert wing sweep angle to radians  
      K_mdo = 34.163 ! Value taken from MDO lab's 'pyAtmospherics.py' code

      !-- Set the initial value of the Mach # DV to the MLOW value
      !--  from the opt.inp file
      if (dvmach(mp)) dvmach0 = mlow*cos(sweep)    

      !-- Calculate the interval spacing for the ranges of 
      !-- Mach number, aircraft weight, anb aircraft cruise alt
      if (nm.le.1) then
         delm = 0.0
      else
         delm = (mup-mlow)/(nm-1)
      end if
      
      if (nwt.le.1) then
         delwt = 0.0
      else
         delwt = (wtup-wtlow)/(nwt-1)
      end if

      if (nalt.le.1) then
         delalt = 0.0
      else
         delalt = (altup - altlow)/(nalt-1)
      end if

      !-- Total number of on-design points required for weighted 
      !-- integral
      ondpTotal = nm*nwt*nalt

      !-- Update the total number of design points needed for the 
      !-- optimization
      mpopt = ondpTotal + noffcon

      !-- Generate design point Mach numbers
      ii = 1
      do 5 i = 1,nm
         mach = (mlow + delm*(i-1))*cos(sweep)
      do 5 j = 1,nwt
      do 5 k = 1,nalt
         wt_int_m(ii) = mach
         ii = ii+1
 5    continue         

      !-- Generate design point lift coefficients
      ii = 1
      do 10 i = 1,nm 
         mach = (mlow + delm*(i-1))*cos(sweep)
      do 10 j = 1,nwt
         wt = wtlow + delwt*(j-1) ! Aircraft weight at design pt
         lift_per_span = wt*g/wspan 
      do 10 k = 1,nalt
         alt = altlow + delalt*(k-1) ! Aircraft altitude at design pt
         if (alt.lt.11000) then ! Use lapse rate to calc air temp at alt
            talt = t0 - L*alt 
         else ! Use a constant air temp for altitudes above 11,000 m
            talt = 273.15 - 56.5
         end if

         if (alt.lt.11000) then ! Use lapse rate to calc air temp at alt
            palt = p0*(1 - (L*alt/t0))**((g*M)/(R*L)) ! Air press at alt
         else ! Use formula from MDO lab's 'pyAtmospherics.py' code
            palt = p0*0.22336*exp((-K_mdo*(alt-11000)*1.d-3)/216.65)
         end if
         rho = (palt*M)/(R*talt)
         sndspalt = dsqrt(gam*palt/rho)
         cltarg = 2*lift_per_span*cos(sweep)/
     &            (rho*((mach*sndspalt)**2)*ref_chord)
         cl_inv(i,j,k) = 1.d0/cltarg
         wt_int_cl(ii) = cltarg
         ii = ii+1
 10   continue

      !-- Generate design point Reynolds numbers
      ii = 1
      do 15 i = 1,nm
         mach = (mlow + delm*(i-1))*cos(sweep)
      do 15 j = 1,nwt
      do 15 k = 1,nalt
         alt = altlow + delalt*(j-1) ! Aircraft altitude at design pt
         write(*,*)'altitude =',alt
         if (alt.lt.11000) then ! Use lapse rate to calc air temp at alt
            talt = t0 - L*alt 
         else ! Use a constant air temp for altitudes above 11,000 m
            talt = 273.15 - 56.5
         end if
         write(*,*)'temperature at altitude=',talt

         if (alt.lt.11000) then ! Use lapse rate to calc air temp at alt
            palt = p0*(1 - (L*alt/t0))**((g*M)/(R*L)) ! Air press at alt
         else ! Use formula from MDO lab's 'pyAtmospherics.py' code
            palt = p0*0.22336*exp((-K_mdo*(alt-11000)*1.d-3)/216.65)
         end if

         write(*,*)'pressure at altitude=',palt
         rho = (palt*M)/(R*talt)
         write(*,*)'air density at altitude=',rho
         sndspalt = dsqrt(gam*palt/rho)
         write(*,*)'sound speed at altitude=',sndspalt
         mu = mu0*((talt/t0)**1.5)*(t0 + 110.0)/(talt + 110.0)
         reyno = rho*(mach*sndspalt)*ref_chord/mu
         wt_int_re(ii) = reyno
         ii = ii+1
 15   continue

      !-- Generate design point weights for integral approximation via
      !-- 3D trapezoidal quadrature rule - Corners of the cube first:
      t(1,1,1)       = 1.0
      t(nm,1,1)      = 1.0
      t(1,nwt,1)     = 1.0
      t(nm,nwt,1)    = 1.0
      t(1,1,nalt)    = 1.0
      t(nm,1,nalt)   = 1.0
      t(1,nwt,nalt)  = 1.0
      t(nm,nwt,nalt) = 1.0

      !-- Edges of the cube:
      do i = 2,nm-1
         t(i,1,1)      = 2.0
         t(i,nwt,1)    = 2.0
         t(i,nwt,nalt) = 2.0
         t(i,1,nalt)   = 2.0
      end do

      do j = 2,nwt-1
         t(1,j,1)      = 2.0
         t(nm,j,1)     = 2.0
         t(nm,j,nalt)  = 2.0
         t(1,j,nalt)   = 2.0
      end do

      do k = 2,nalt-1
         t(1,1,k)      = 2.0
         t(nm,1,k)     = 2.0
         t(nm,nwt,k)   = 2.0
         t(1,nwt,k)    = 2.0
      end do

      !-- Faces of the cube:
      i = 1
      do j = 2,nwt-1
         do k = 2,nalt-1
            t(i,j,k) = 4.0
         end do
      end do

      i = nm
      do j = 2,nwt-1
         do k = 2,nalt-1
            t(i,j,k) = 4.0
         end do
      end do

      j = 1
      do i = 2,nm-1
         do k = 2,nalt-1
            t(i,j,k) = 4.0
         end do
      end do

      j = nwt
      do i = 2,nm-1
         do k = 2,nalt-1
            t(i,j,k) = 4.0
         end do
      end do

      k = 1
      do i = 2,nm-1
         do j = 2,nwt-1
            t(i,j,k) = 4.0
         end do
      end do

      k = nalt
      do i = 2,nm-1
         do j = 2,nwt-1
            t(i,j,k) = 4.0
         end do
      end do

      !-- Cube interior points:
      do i = 2,nm-1
         do j = 2,nwt-1
            do k = 2,nalt-1
               t(i,j,k) = 8.0
            end do
         end do
      end do

      !-- Generate weights based on designer-specified function to 
      !-- prioritize operating conditions
      
      if (wt_func.eq.0) then

         !-- Equal priority assigned to all operating conditions
         do i = 1,nm
            do j = 1,nwt
               do k = 1,nalt
                  d(i,j,k) = 1.0
               end do
            end do
         end do

      else if (wt_func.eq.1) then

         !-- Exponential weight function that varies with Mach number
         a = log(10.d0)/(mup-mlow)
         do i = 1,nm
            mach = mlow + delm*(i-1)
            do j = 1,nwt
               do k = 1,nalt
                  d(i,j,k) = exp(a*(mach-mlow))
               end do
            end do
         end do

      else if (wt_func.eq.2) then

         !-- A mix of exponenital weighting and equal weighting
         !-- controlled by OMGA
         if (mup.eq.mlow) then
            a = 0.d0
         else
            a = log(20.d0)/(mup-mlow)
         end if

         do i = 1,nm
            mach = mlow + delm*(i-1)
            do j = 1,nwt
               do k = 1,nalt
                  d(i,j,k) = omga*exp(a*(mach-mlow)) + (1.d0-omga)*1.d0
               end do
            end do
         end do

      else if (wt_func.eq.3) then

         !-- Vary the exponent 'a' using OMGA
         if (mup.eq.mlow) then
            a = 0.d0
         else
            a = log(20.d0)/(mup-mlow)
         end if

         do i = 1,nm
            mach = mlow + delm*(i-1)
            do j = 1,nwt
               do k = 1,nalt
                  d(i,j,k) = exp(a*(mach-mlow))
               end do
            end do
         end do
      else if (wt_func.eq.4) then

         !-- A mix of exponenital weighting and equal weighting
         !-- controlled by OMGA
         !-- Variable weighting at highest Mach # controlled by
         !-- input parameter EXP_WT
         if (mup.eq.mlow) then
            a = 0.d0
         else
            a = log(exp_wt)/(mup-mlow)
         end if

         do i = 1,nm
            mach = mlow + delm*(i-1)
            do j = 1,nwt
               do k = 1,nalt
                  d(i,j,k) = omga*exp(a*(mach-mlow)) + (1.d0-omga)*1.d0
               end do
            end do
         end do

      end if
      
      !-- The final design point weights are the product of the
      !-- trapezoidal weights and designer-priority weights.
      ii = 1
      if (objfuncs(1).eq.8) then
         do i = 1,nm
            do j = 1,nwt
               do k = 1,nalt
               !-- *NOTE: the '1/Cl*' factor is only applicable when 
               !-- obj func is defined as the weighted integral of D/L, 
               !-- which is equivilant to the weighted integral of 
               !-- Cd/Cl. At each design point, we are actually 
               !-- minimizing Cd subject to a lift constraint of Cl*.
               !-- Therefore a factor of 1/Cl* is taken out of the 
               !-- design point objective function and incorporated 
               !-- into the design point weight. 
                  if (dl_int) then
                     int_wts(ii) = t(i,j,k)*d(i,j,k)*cl_inv(i,j,k)
                     int_wts2(ii) = t(i,j,k)*cl_inv(i,j,k)
                     ii = ii+1
                  else 
                     int_wts(ii) = t(i,j,k)*d(i,j,k)
                     int_wts2(ii) = t(i,j,k)
                     ii = ii+1
                  end if
               end do
            end do
         end do
      !-- If using the range objective function defined
      !-- as Cd/(M*Cl), do not multiply design point weights 
      !-- by 1/Cl*.
      else
         do i = 1,nm
            do j = 1,nwt
               do k = 1,nalt
                  int_wts(ii) = t(i,j,k)
                  ii = ii+1
               end do
            end do
         end do
      end if

      return
      
      end

