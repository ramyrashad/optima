#include <stdlib.h>
#include <string.h>
#include "snopt.h"

void snClearLastError( snProblem* prob )
{
  if ( prob->error_msg != NULL ) {
    /* has already been assigned allocated */
    free( prob->error_msg );
    prob->error_msg = NULL;
  }

  prob->error = SN_OK;
  prob->error_msg_len = 0;
}

void snSetError( snProblem* prob , int type , const char* error_msg )
{
  snClearLastError( prob );

  prob->error_msg_len = strlen( error_msg ) + 1;
  prob->error_msg = (char*)malloc( sizeof(char) * prob->error_msg_len );
  prob->error = type;
  strcpy( prob->error_msg , error_msg );
}

int snGetError( snProblem* prob , char** error_msg )
{
  if ( prob->error == SN_OK )
    /* if everything is ok, no message */
    return SN_OK;

  if ( *error_msg == NULL )
    /* caller just wants code, but no message */
    return prob->error;

  /* free'ing of *error_msg is the callers responsibility */
  *error_msg = (char*)malloc( sizeof(char) * prob->error_msg_len );
  strcpy( *error_msg , prob->error_msg );

  return prob->error;
}

