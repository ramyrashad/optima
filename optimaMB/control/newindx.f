c     -----------------------------------------------------------------
c     -- set reordered indx array --
c     -- m. nemec, july 2001 --
c     -----------------------------------------------------------------
      subroutine newindx( jmax, kmax, indx, iren, js, je, ks, ke)

      implicit none
      integer jmax, kmax,js, je, ks, ke,j,k
      integer indx(jmax,kmax), iren(*)

      do k = ks,ke
         do j = js,je
            indx(j,k) = iren(indx(j,k))
         end do
      end do
      
      return 
      end                       ! newindx
