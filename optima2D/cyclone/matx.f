c ##########################
c ##                      ##
c ##  subroutine matx     ##
c ##                      ##
c ##########################
c
      subroutine matx (jdim,kdim,q,s,xy,xyj,aa,sndsp,uu,ccx,
     &                           spect,coef2,coef4,work,tmp,tmp2)            
c
c******************************************************************
c   this subroutine follows very faithfully t. pulliam notes, p.31
c
c   fourth order smoothing, added explicitly to rhs                     
c   second order near shocks with pressure grd coeff.                   
c                                                                       
c  xi direction                                                         
c                                                                       
c   start differences each variable separately                          
c******************************************************************
c                                                                       
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)          
      dimension work(jdim,kdim,4),uu(jdim,kdim),ccx(jdim,kdim)
      dimension tmp(4,jdim,kdim),tmp2(4,jdim,kdim),aa(4,4,jdim,kdim)
      dimension vec(4),xy(jdim,kdim,4),spect(jdim,kdim,3)
c
      do 10 n = 1,4                                                     
c----    1st-order forward difference ----
         do 15 k =klow,kup                                                
         do 15 j =jlow,jup                                                
c            tmp(n,j,k) = q(j+1,k,n)*xyj(j+1,k) - q(j,k,n)*xyj(j,k)        
            tmp(n,j,k) = - q(j,k,n)*xyj(j,k) + q(j+1,k,n)*xyj(j+1,k)
 15      continue
c  --    c mesh bc --
         if (.not.periodic) then                                          
            j1 = jbegin
            j1p= j1+1
            j2 = jend                                                      
            do 16 k = klow,kup                                             
c               tmp(n,j1,k) = q(j1p,k,n)*xyj(j1p,k)-q(j1,k,n)*xyj(j1,k)
               tmp(n,j1,k) = -q(j1,k,n)*xyj(j1,k)+q(j1p,k,n)*xyj(j1p,k)
               tmp(n,j2,k) = tmp(n,j2-1,k)                                  
 16         continue                                                       
         endif                                                          
c        
c        
c----    apply cent-dif to 1st-order forward ----
         do 17 k =klow,kup                                                
         do 17 j =jlow,jup                                                
            tmp2(n,j,k) = tmp(n,j-1,k)-2.d0*tmp(n,j,k)+tmp(n,j+1,k)     
 17      continue                                                          
c  --    c mesh bc --
         if (.not.periodic) then                                          
            j1  = jbegin
            j1p = j1+1
            j1p2= j1+2
            j2  = jend                                                      
            do 18 k =klow,kup                                             
               tmp2(n,j1,k) = q(j1,k,n)*xyj(j1,k) -                   
     >              2.d0*q(j1p,k,n)*xyj(j1p,k) + q(j1p2,k,n)*xyj(j1p2,k)    
               tmp2(n,j2,k) = 0.                                   
 18         continue
     $              
         endif                                                          
 10   continue                                                          
c
c
c
      gm1i=1.d0/gami
      do 19 k = kbegin,kend                                            
      do 19 j = jbegin,jend       
c
c        -need to compute  J^{-1} * T * |eigenvalue matrix| * T^{-1}
c
c        -compute magnitude of eigenvalues
c         rj =1.d0/xyj(j,k)
c         vec(1)=abs(uu(j,k))*rj
c         vec(2)=vec(1)
c         vec(3)=abs(uu(j,k)+ccx(j,k))*rj
c         vec(4)=abs(uu(j,k)-ccx(j,k))*rj
c
         vec(1)=spect(j,k,1)
c         vec(2)=vec(1)
         vec(3)=spect(j,k,2)
         vec(4)=spect(j,k,3)
c
         v3p4= vec(3) + vec(4)
         v4m3= vec(4) - vec(3)
c
         rhoinv=1.d0/q(j,k,1)
         u=q(j,k,2)*rhoinv
         v=q(j,k,3)*rhoinv
         vtot2=0.5d0*(u**2+v**2)
         c=sndsp(j,k)
         snr=1.d0/c
         snr2=snr**2
         phi2=vtot2*gami
         phia2=(phi2+c**2)*gm1i
c         beta=bt*rhoinv*snr
c         bt=1.d0/sqrt(2.d0)
c         alp=bt*rho*snr
         abeta=.5d0*snr2
c
         rx=xy(j,k,1)
         ry=xy(j,k,2)
         rxy2=1.d0/sqrt(rx**2+ry**2)
         rx=rx*rxy2
         ry=ry*rxy2
         ryu=ry*u
         rxu=rx*u
         ryv=ry*v
         rxv=rx*v
         rxa=rx*c
         rya=ry*c
         theta=rxu+ryv
         atheta=c*theta
         theta2=ryu-rxv
c
         upx=(u+rxa)*vec(3)
         umx=(u-rxa)*vec(4)
         vpy=(v+rya)*vec(3)
         vmy=(v-rya)*vec(4)
c
         uv34m=upx - umx
         uv34p=upx + umx
         vv34m=vpy - vmy
         vv34p=vpy + vmy
c
         pav3=(phia2+atheta)*vec(3)
         pav4=(phia2-atheta)*vec(4)
c
         zz=1.d0-phi2*snr2
         aa(1,1,j,k)=vec(1)*zz + abeta*phi2*v3p4 + atheta*v4m3
         aa(2,1,j,k)=vec(1)*(zz*u - ry*theta2) + abeta*(
     &                       phi2*uv34p - atheta*uv34m)
         aa(3,1,j,k)=vec(1)*(zz*v + rx*theta2) + abeta*(
     &                       phi2*vv34p - atheta*vv34m)
         aa(4,1,j,k)=vec(1)*(zz*gm1i*phi2-theta2**2) + abeta*(
     &                       pav3*(phi2-atheta) + pav4*(phi2+atheta))
c
         yy=gami*u
         zz=yy*snr2
         aa(1,2,j,k)=vec(1)*zz - abeta*(rxa*v4m3 + yy*v3p4)
         aa(2,2,j,k)=vec(1)*(zz*u + ry**2) + abeta*(
     &                       rxa*uv34m - yy*uv34p)
         aa(3,2,j,k)=vec(1)*(zz*v - rx*ry) + abeta*(
     &                       rxa*vv34m - yy*vv34p)
         aa(4,2,j,k)=vec(1)*(zz*gm1i*phi2 + ry*theta2) + abeta*(
     &                       pav3*(rxa-yy) - pav4*(rxa+yy))   
c                 
         yy=gami*v
         zz=yy*snr2
         aa(1,3,j,k)=vec(1)*zz - abeta*(rya*v4m3 + yy*v3p4)       
         aa(2,3,j,k)=vec(1)*(zz*u - ry*rx) + abeta*(              
     &                       rya*uv34m - yy*uv34p)             
         aa(3,3,j,k)=vec(1)*(zz*v + rx**2) + abeta*(
     &                       rya*vv34m - yy*vv34p)
         aa(4,3,j,k)=vec(1)*(zz*gm1i*phi2 - rx*theta2) + abeta*(
     &                       pav3*(rya-yy) - pav4*(rya+yy))   
c
         zz=-vec(1)*snr2
         aa(1,4,j,k)=gami*(zz + abeta*v3p4)
         aa(2,4,j,k)=gami*(zz*u + abeta*uv34p)
         aa(3,4,j,k)=gami*(zz*v + abeta*vv34p)
         aa(4,4,j,k)=zz*phi2 + abeta*gami*(pav3 + pav4)
c
 19   continue
c
      if (dis2x.gt.0.) then
         do 50 k=klow,kup
         do 50 j=jbegin,jup
            jj=jplus(j)
            do 25 n=1,4
               sum=0.
               do 20 i=1,4
                  sum=sum+(coef2(j,k)*aa(n,i,j,k)+
     &                             coef2(jj,k)*aa(n,i,jj,k))*tmp(i,j,k)
 20            continue
c           
               do 21 i=1,4
                  sum=sum-(coef4(j,k)*aa(n,i,j,k)+
     &                            coef4(jj,k)*aa(n,i,jj,k))*tmp2(i,j,k)
 21            continue
               work(j,k,n)=sum
 25         continue
 50      continue
      else
         do 150 k=klow,kup
         do 150 j=jbegin,jup
            jj=jplus(j)
            do 125 n=1,4
               sum=0.
               do 121 i=1,4
                  sum=sum-(coef4(j,k)*aa(n,i,j,k)+
     &                            coef4(jj,k)*aa(n,i,jj,k))*tmp2(i,j,k)
 121           continue
               work(j,k,n)=sum
 125        continue
 150     continue
      endif
c
c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
         s(j,k,n)=s(j,k,n) + (work(j,k,n) - work(j-1,k,n))*dtd
 200  continue
c
      return                                                            
      end                                                               
