      subroutine mcoef24y(jdim,kdim,coef2y,coef4y,
     &                    jbegin,jend,kbegin,kend)

      implicit none
                                                      
#include "../include/common.inc"

      integer jdim,kdim,jbegin,jend,kbegin,kend,j,k,km
      double precision coef2y(jdim,kdim),coef4y(jdim,kdim),c2,c4              
c                                                                       
c     coef2y comes in as coefy (pressure gradiaet coefficient)            
c     coef4y comes in as specy (spectral radius)                          
c                                                                       
c     form 2nd and 4th order dissipation coefficients in y                 
c                                                                       
      do 10 k = kbegin,kend-1 
      do 10 j = jbegin,jend
        c2 = dis2y*coef2y(j,k)
        c4 = dis4y-min(dis4y,c2)
        coef2y(j,k) = c2
        coef4y(j,k) = c4
 10   continue
c     
      k=kend
      km=k-1
      do 20 j=jbegin,jend
        coef4y(j,k)=coef4y(j,km)
 20   continue
c     
      return                                                         
      end                                                            







