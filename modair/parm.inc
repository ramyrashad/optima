c     -- parameters for MODAIR --
      integer ibsord, ibsnc, ibskt, ibody, maxj, maxk
c     -- max. number of knots -> ibskt = ibsord + ibsnc --
      parameter (ibsord=25, ibody=650, ibsnc=100, ibskt=125)
      parameter (maxj=665, maxk=250)
