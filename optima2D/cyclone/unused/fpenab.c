#include<sys/fpu.h>
extern intfpe();
void fpenab_()
{
 union fpc_csr f;

 f.fc_word=get_fpc_csr();
 f.fc_struct.en_divide0 = 1;
 f.fc_struct.en_underflow = 0;
 f.fc_struct.en_overflow = 1;
 f.fc_struct.en_invalid = 1;
 set_fpc_csr(f.fc_word);
}
