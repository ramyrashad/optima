c-----------------------------------------------------------------------
c     -- GMRES solve control --
c     -- followed SPARSKIT routine 'runrc' in 'itaux' --
c     
c     Written by: M. Nemec, Sept. 2000
c-----------------------------------------------------------------------
      subroutine run_gmres (jdim, kdim, ndim, indx, iex, qnow, q,
     &     qold, xy, xyj, x, y, precon, coef2, coef4, uu, vv, ccx, 
     &     ccy, ds, press, tmet, spect, gam, turmu, fmu, vort, 
     &     sndsp, s, bcf, ifirst, rhs, sol, as, ja, ia, alu, jlu, 
     &     ju, qtmp, stmp, wk, work, iout1, iout2, framux, rhsnamult, 
     &     D_hat, exprhs,a_jk, its_nk_val,dtvec)
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim, kdim, ndim, na, indx(jdim,kdim), iout1, iout2

      integer its, ifirst, iwk_gmres, iex(jdim,kdim,4)
      integer ja(*), ia(*), jlu(*), ju(*) 

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision qnow(jdim,kdim,ndim), qold(jdim,kdim,ndim)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)

      double precision s(jdim,kdim,ndim),rhs(jdim*kdim*ndim+1),alu(*)
      double precision sol(jdim*kdim*ndim+1), bcf(jdim,kdim,4), res
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision qtmp(jdim,kdim,ndim), stmp(jdim,kdim,ndim)
      double precision wk(*), res1, work(jdim,kdim,4),rhsnamult

      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim),a_jk(6,6)
c     -- JG: time vector in matrix-vector product --
      double precision dtvec(*)


      na = jmax*kdim*ndim
      if (clalpha2 .or. clopt) na=na+1
    
      its  = 0
      res  = 0.0
      res1 = 0.0

c     -- gmres soluton array --
      do j = 1,na
        sol(j) = 0.d0
      end do

 100  call gmres(na, rhs, sol, ipar, fpar, wk)

      if (ipar(1).lt.0) then
         write(out_unit,101)
 101     format(/3x,'Convergence problems with GMRES')
         write(out_unit,102) ipar(1)
 102     format(3x,'On exit from GMRES, ipar(1) =',1x,i2,/)         
      end if
      
c     -- residual output --
      if (ipar(7).ne.its) then
c        write (*, *) 'inner', its, real(fpar(5))
        if (iout1.gt.0 .and. mod(its,10).eq.0) write (iout1,10) its,res
        if (its.eq.1) res1 = res
        if (its.ne.0 .and. iout2.gt.0 .and. gmresout) 
     &     write (iout2,10) its, res/res1
        its = ipar(7)
      endif
      res = fpar(5)

      if (ipar(1).eq.1 .and. jac_mat) then
        call amux(na, wk(ipar(8)), wk(ipar(9)), as, ja, ia)
        goto 100
      else if (ipar(1).eq.1 .and. .not. jac_mat) then

        call framux (na, jdim, kdim, ndim, indx, iex, wk(ipar(8)),
     &        wk(ipar(9)), q, xy, xyj, x, y, precon, coef2, coef4, uu,
     &        vv, ccx, ccy, ds, press, tmet, spect, gam, turmu, fmu,
     &        vort, sndsp, s, bcf, qtmp, qnow, qold, stmp, 
     &        ifirst, as, work, rhsnamult, D_hat, exprhs, a_jk,dtvec)
        ifirst = 0
        goto 100
      else if (ipar(1).eq.2) then
        call atmux(na, wk(ipar(8)), wk(ipar(9)), as, ja, ia)
        goto 100
      else if (ipar(1).eq.3 .or. ipar(1).eq.5) then
        call lusol(na,wk(ipar(8)),wk(ipar(9)),alu,jlu,ju)
        goto 100
      else if (ipar(1).eq.4 .or. ipar(1).eq.6) then
        call lutsol(na,wk(ipar(8)),wk(ipar(9)),alu,jlu,ju)
        goto 100
      endif

      if (iout1.gt.0) write (iout1,10) ipar(7), fpar(5)

c     -- note: the "if" statements for res1 are required for
c     multiobjective runs when one of the objectives is not active --
 
      if (iout2.gt.0) then
         if (res1.lt.2.2e-16) then
            if (gmresout) write (iout2,20) ipar(7), fpar(5)
         else
            if (gmresout) write (iout2,20) ipar(7), fpar(5)/res1
         end if
      end if

c     -- fpar(12) is not used in SPARSKIT, here it is used to store the
c     relative residual --
      if (res1.lt.2.2e-16) then
         fpar(12) = fpar(5)
      else
         fpar(12) = fpar(5)/res1
      end if

 10   format (3x,i5,e14.5)
 20   format (3x,i5,e14.5/)

      its_nk_val=its
      return
      end                       !run_gmres
c-----------------------------------------------------------------------
      function distdot(n,x,ix,y,iy)
      integer n, ix, iy
      real*8 distdot, x(*), y(*), ddot
      external ddot
      distdot = ddot(n,x,ix,y,iy)
      return
      end                       !distdot
