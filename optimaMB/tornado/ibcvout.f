c     -----------------------------------------------------------------
c     -- viscous outflow boundary  j = jmaxbnd --
c     -- m. nemec, sept. 2001 --
c     -----------------------------------------------------------------
      subroutine ibcvout(iblk, iside, imps, j1, j2, ks, ke, jmax, kmax,
     &     nmax, q, xyj, press, indx, as, pa, icol, gami, jacmat,
     &     precmat, bcf, iex)

      implicit none

#include "../include/parms.inc"

      integer iblk,iside,j1,j2,ks,ke,jmax,kmax,nmax,k,n1,n2,n3,j
      integer ii,ij,ip,itmp,k1
      double precision q(jmax,kmax,4),xyj(jmax,kmax),as(*),pa(*)
      double precision press(jmax,kmax),u,v,pr1,b0,b1,pr2,gami
      logical   jacmat,           precmat,           imps     

      integer iex(maxjb,4,4,mxbfine),indx(jmax,kmax),icol(*)
      double precision bcf(maxjb,4,4,mxbfine)

c     -- local arrays --
      double precision cc(4,4), c1(4,4)

      do k = ks,ke

         do n1 = 1,4
            do n2 = 1,4
               cc(n1,n2) = 0.d0
               c1(n1,n2) = 0.d0
            end do
         end do

c     -- matrix for j=jmax --
         j   = j1
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr1 = press(j,k)*xyj(j,k)

c     -- (ro)1   = (ro)2  or Tw = const. --
         cc(1,1) = xyj(j,k)
c     -- (ro*u)1 = (ro*u)2 --
         cc(2,2) = xyj(j,k)
c     -- (ro*v)1 = (ro*v)2 --
         cc(3,3) = xyj(j,k)
c     -- interp. press. --
         cc(4,1) = 0.5*(u*u+v*v)*gami*xyj(j,k)
         cc(4,2) = -u*gami*xyj(j,k)
         cc(4,3) = -v*gami*xyj(j,k)
         cc(4,4) = gami*xyj(j,k)

c     -- matrix for j=max-1 --
         j   = j2
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr2 = press(j,k)*xyj(j,k)

c     -- (ro)1   = (ro)2  or Tw = const. --
         c1(1,1) = -xyj(j,k)
c     -- (ro*u)1 = (ro*u)2 --
         c1(2,2) = -xyj(j,k)
c     -- (ro*v)1 = (ro*v)2 --
         c1(3,3) = -xyj(j,k)
c     -- interp. press. --
         c1(4,1) = -0.5*(u*u+v*v)*gami*xyj(j,k)
         c1(4,2) = u*gami*xyj(j,k)
         c1(4,3) = v*gami*xyj(j,k)
         c1(4,4) = -gami*xyj(j,k)
         
c     -- reordering --
         j = j1
c     -- finding the biggest --      
         do n2 =1,3
            k1 = n2
            b0 = abs(cc(n2,n2))
            do n1 =n2+1,4
               b1 = abs(cc(n1,n2))
               if(b1.gt.b0) then
                  k1 = n1
                  b0 = b1
               endif
            end do
c     -- exchanging rows --
            if (k1.ne.n2) then
               do n3 =1,4
                  b1 = cc(n2,n3)
                  cc(n2,n3) = cc(k1,n3)
                  cc(k1,n3) = b1
                  b1 = c1(n2,n3)
                  c1(n2,n3) = c1(k1,n3)
                  c1(k1,n3) = b1
               end do
               itmp = iex(k,n2,iside,iblk)
               iex(k,n2,iside,iblk) = iex(k,k1,iside,iblk)
               iex(k,k1,iside,iblk) = itmp
            endif
         end do
c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
         if ( abs(cc(4,4)).lt.1.e-7 ) then
            do n3 =1,4
               b1 = cc(2,n3)
               cc(2,n3) = cc(4,n3)
               cc(4,n3) = b1
               b1 = c1(2,n3)
               c1(2,n3) = c1(4,n3)
               c1(4,n3) = b1
            end do
            itmp = iex(k,2,iside,iblk)
            iex(k,2,iside,iblk) = iex(k,4,iside,iblk)
            iex(k,4,iside,iblk) = itmp
         endif 

c     -- diagonal scaling --
         do n1 =1,4
            bcf(k,n1,iside,iblk) = 1.0/cc(n1,n1)
         end do

c     -- store --
         if (imps) then
            j = j1

            if (precmat) then
               do n1 = 1,4
                  ij = ( indx(j,k)-1)*nmax + n1
                  ip = ( ij - 1 )*icol(5)

                  do n2 = 1,4
                     pa(ip+        n2) = cc(n1,n2)
                     pa(ip+icol(1)+n2) = c1(n1,n2)
                  end do
               end do
            end if

            if (jacmat) then
               do n1 = 1,4
                  ij = ( indx(j,k)-1)*nmax + n1
                  ii = ( ij - 1 )*icol(9)

                  do n2 = 1,4
                     as(ii+        n2) = cc(n1,n2)
                     as(ii+icol(1)+n2) = c1(n1,n2)
                  end do
               end do
            end if
            
         end if
      end do

      if (nmax.eq.5 .and. imps) then
c     -- s-a model viscous outflow --
         j = j1
         n1 = 5
         n2 = 5 
         do k = ks,ke

            ij = ( indx(j,k)-1)*nmax + n1
            ii = ( ij - 1 )*icol(9)
            ip = ( ij - 1 )*icol(5)

            if (jacmat) then
               as(ii+        n2) =  xyj(j1,k)
               as(ii+icol(1)+n2) = -xyj(j2,k)
            end if

            if (precmat) then
               pa(ip+        n2) =  xyj(j1,k)
               pa(ip+icol(1)+n2) = -xyj(j2,k)
            end if
         end do
      end if

      return
      end                       !ibcvout
