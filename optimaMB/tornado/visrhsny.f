C ********************************************************************
C ***************** EXPLICIT VISCOUS TERM ****************************
C ********************************************************************
c calling routine: etaexpl
      subroutine visrhsny(iblk,jdim,kdim,q,press,s,turmu,fmu,xy,xyj,
     &      c0,c1,c2,c3,c4,c5,fmutemp,ttemp,beta,vflux,
     &      jlow,jup,kbegin,kend,klow,kup) 

      implicit none
c
#include "../include/common.inc"
#include "../include/visc.inc"
c
      integer iblk,jdim,kdim,jlow,jup,kbegin,kend,klow,kup,k,km1,kp1,kp2
      integer j,n,kp3,km2,km3
      double precision q(jdim,kdim,4),press(jdim,kdim),turmu(jdim,kdim)        
      double precision s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      double precision fmu(jdim,kdim)                                          
c                                                                       
      double precision c0(jdim,kdim),beta(jdim,kdim),vflux(jdim,kdim,4)        
      double precision c1(jdim,kdim),c2(jdim,kdim),c3(jdim,kdim)               
      double precision c4(jdim,kdim),c5(jdim,kdim), fmutemp(jdim,kdim)
      double precision ttemp(jdim,kdim) 
      double precision hre,g1,xx,yy,zz,t1,t2,t3,r,r1,rm1,rp1,turmd,rp2
      double precision um1,u,up1,up2,vm1,v,vp1,vp2,ueta,veta,c2eta
      double precision uetamu,vetamu,c2etamu,c1ext,c2ext,c3ext,c4ext
      double precision uext,vext,rp3,up3,vp3,rm2,um2,vm2,rr1,rrp1,uup1
      double precision uu1,vvp1,vv1
c                                                                       
c                                                                       
c                                                                       
c       Originally coded by Tim Barth -  1985
c                                                                       
c       prlam   =  laminar prandtl number  = .72                      
c       prturb  =  turbulent prandtl number = .90                     
c       prlinv  =  1./(laminar prandtl number)                        
c       prtinv  =  1./(turbulent prandtl number)                      
c       f13     =  1/3                                                
c       f23     =  2/3                                                
c       f43     =  4/3                                                
c       hre     =  1/2 dt * reynolds number                           
c       fmu     =  viscosity                                          
c       turmu   =  turbulent viscosity                                
c       aa7      =  sound speed squared ... also temperature          
c       beta    =  fmu/prlam + muturb/prturb                          
c                                                                       
c                                                                       
c                                                                       
c     set up some temporary logical switches visxi,viseta,visxx        
c                                                                       
      hre   = dt/(re*(1.d0 + phidt))                                 
      g1    = 1.d0/gami                                                   
c                                                                       
c     **************************************************************
c     *******  common variables for xi, eta and cross terms  *******  
c     **************************************************************
c                                                                       
c     -interpolate fmu to get it at 1/2 pts (turmu is already at 1/2 pts)
c                                                                       
      xx=1.d0/8.d0
      yy=1.d0/24.d0
      zz=1.d0/16.d0
      if (iord.eq.4) then
        do 2  k = klow,kup-1
          km1=k-1
          kp1=k+1
          kp2=k+2
          do 1  j = jlow,jup
c           -fourth order interpolation
            fmutemp(j,k)=zz*(-fmu(j,km1)+9.d0*(fmu(j,k)+fmu(j,kp1))-
     &                          fmu(j,kp2))
 1        continue
 2      continue
        k=kbegin
        kp1=k+1
        kp2=k+2
        do 3 j=jlow,jup
c         -third order interpolation
          fmutemp(j,k)=xx*(3.d0*fmu(j,k)+6.d0*fmu(j,kp1)-fmu(j,kp2))
 3      continue
        k=kup
        km1=k-1
        kp1=k+1
        do 4 j=jlow,jup
c         -third order interpolation
          fmutemp(j,k)=xx*(-fmu(j,km1)+6.d0*fmu(j,k)+3.d0*fmu(j,kp1))
 4      continue
      else
        do 5 k = kbegin,kup
        do 5 j = jlow,jup
c         -second order
          fmutemp(j, k) = .5*(fmu(j,k)+fmu(j,k+1))
 5      continue
      endif
c                                                                        
c beta = ((lam. vis.)/(lam. pran.) + (turb. vis.)/(turb. pran.))*dt/(Re*gami)
c      = [(total viscosity)/Pran.]*dt/(Re*gami)
c
c   c0 = (laminar viscosity + turbulent viscosity)*dt/Re
      do 10 k = kbegin, kup                                             
      do 10 j = jlow, jup                                             
        turmd     =  turmu(j,k)                                         
        beta(j,k) = (fmutemp(j,k)*prlinv + turmd*prtinv)*hre*g1
        c0(j,k)   = (fmutemp(j,k) + turmd)*hre                       
 10   continue                                                          
c
c     ********************************************************************
c     ********************************************************************
c
c     f_eta_eta viscous terms                                   
c
c     t1 = eta_x **2
c     t2 = eta_x*eta_y
c     t3 = eta_y **2
      do 100 k=kbegin,kend                                              
      do 100 j=jlow,jup                                               
        t1      = xy(j,k,3)*xy(j,k,3)
        t2      = xy(j,k,3)*xy(j,k,4)
        t3      = xy(j,k,4)*xy(j,k,4)
        r1      = 1./xyj(j,k)                        
        c1(j,k)   =  r1*( t1     +   t3    )
        c2(j,k)   =  r1*( f43*t1 +   t3    )
        c3(j,k)   =  r1*( t1     +   f43*t3)
        c4(j,k)   =  r1*(     t2*f13     )
 100  continue
c
      if (iord.eq.4) then
c       *********************************************************
c                             Fourth-Order
c       *********************************************************
        do 105 k=klow,kup-1
          km1=k-1
          kp1=k+1
          kp2=k+2
          do 104 j=jlow,jup
c
            rm1=1.d0/q(j,km1,1)
            r=1.d0/q(j,k,1)
            rp1=1.d0/q(j,kp1,1)
            rp2=1.d0/q(j,kp2,1)
c
            um1=q(j,km1,2)*rm1
            u  =q(j,k,2)*r
            up1=q(j,kp1,2)*rp1
            up2=q(j,kp2,2)*rp2
c
            vm1=q(j,km1,3)*rm1
            v  =q(j,k,3)*r
            vp1=q(j,kp1,3)*rp1
            vp2=q(j,kp2,3)*rp2
c
            ueta  = yy*(um1 + 27.d0*(up1-u) - up2)
            veta  = yy*(vm1 + 27.d0*(vp1-v) - vp2)

            c2eta = gamma*(press(j,kp1)*rp1-press(j,k)*r)
c
            uetamu  = ueta*c0(j,k)
            vetamu  = veta*c0(j,k)                                        
            c2etamu = c2eta*beta(j,k)                                     
c
c           -fourth order extrap. of variables at half node
            c1ext=zz*(-c1(j,km1) +9.d0*(c1(j,k)+c1(j,kp1)) -c1(j,kp2))
            c2ext=zz*(-c2(j,km1) +9.d0*(c2(j,k)+c2(j,kp1)) -c2(j,kp2))
            c3ext=zz*(-c3(j,km1) +9.d0*(c3(j,k)+c3(j,kp1)) -c3(j,kp2))
            c4ext=zz*(-c4(j,km1) +9.d0*(c4(j,k)+c4(j,kp1)) -c4(j,kp2))

            uext=zz*(-um1 + 9.d0*(u+up1) - up2)
            vext=zz*(-vm1 + 9.d0*(v+vp1) - vp2)
c
            vflux(j,k,2)= c2ext*uetamu + c4ext*vetamu
            vflux(j,k,3)= c4ext*uetamu + c3ext*vetamu     
            vflux(j,k,4)= uext*vflux(j,k,2) + vext*vflux(j,k,3)
     &                                                   + c1ext*c2etamu
 104      continue
 105    continue
c
        k=kbegin
        kp1=k+1
        kp2=k+2
        kp3=k+3
        do 106 j=jlow,jup                                               
          r  =1.d0/q(j,k,1)
          rp1=1.d0/q(j,kp1,1)
          rp2=1.d0/q(j,kp2,1)
          rp3=1.d0/q(j,kp3,1)
c     
          u  =q(j,k,2)*r
          up1=q(j,kp1,2)*rp1
          up2=q(j,kp2,2)*rp2
          up3=q(j,kp3,2)*rp3
c
          v  =q(j,k,3)*r
          vp1=q(j,kp1,3)*rp1
          vp2=q(j,kp2,3)*rp2
          vp3=q(j,kp3,3)*rp3
c
          ueta  = yy*(-up3 + 3.d0*up2 +21.d0*up1 -23.d0*u)
          veta  = yy*(-vp3 + 3.d0*vp2 +21.d0*vp1 -23.d0*v)
          c2eta = gamma*yy*(-23.d0*press(j,k)*r + 21.d0*press(j,kp1)
     &          *rp1 +3.d0*press(j,kp2)*rp2 -press(j,kp3)*rp3)
c          c2eta = gamma*(press(j,kp1)*rp1-press(j,k)*r)
          uetamu  = ueta*c0(j,k)                                       
          vetamu  = veta*c0(j,k)                                        
          c2etamu = c2eta*beta(j,k)                                    
c         -third order extrap. of variables at half node
          c1ext=xx*(3.d0*c1(j,k) +6.d0*c1(j,kp1) -c1(j,kp2))
          c2ext=xx*(3.d0*c2(j,k) +6.d0*c2(j,kp1) -c2(j,kp2))
          c3ext=xx*(3.d0*c3(j,k) +6.d0*c3(j,kp1) -c3(j,kp2))
          c4ext=xx*(3.d0*c4(j,k) +6.d0*c4(j,kp1) -c4(j,kp2))
          uext=xx*(3.d0*u +6.d0*up1 -up2)
          vext=xx*(3.d0*v +6.d0*vp1 -vp2)
c
          vflux(j,k,2)= c2ext*uetamu + c4ext*vetamu
          vflux(j,k,3)= c4ext*uetamu + c3ext*vetamu   
          vflux(j,k,4)= uext*vflux(j,k,2) + vext*vflux(j,k,3)
     &                                                   + c1ext*c2etamu
 106    continue

        k=kup
        kp1=k+1
        km1=k-1
        km2=k-2
        do 107 n=2,4
        do 107 j=jlow,jup
c         -third-order derivatives at half node
          rm2=1.d0/q(j,km2,1)
          rm1=1.d0/q(j,km1,1)
          r  =1.d0/q(j,k,1)
          rp1=1.d0/q(j,kp1,1)
c     
          um2=q(j,km2,2)*rm2
          um1=q(j,km1,2)*rm1
          u  =q(j,k,2)*r
          up1=q(j,kp1,2)*rp1
c     
          vm2=q(j,km2,3)*rm2
          vm1=q(j,km1,3)*rm1
          v  =q(j,k,3)*r
          vp1=q(j,kp1,3)*rp1
c
          ueta  = yy*(um2 -3.d0*um1 -21.d0*u +23.d0*up1)
          veta  = yy*(vm2 -3.d0*vm1 -21.d0*v +23.d0*vp1)
          c2eta = gamma*(press(j,km2)*rm2 -3.d0*press(j,km1)*rm1 
     &          -21.d0*press(j,k)*r +23.d0*press(j,kp1)*rp1)*yy
c          c2eta = gamma*(press(j,kp1)*rp1-press(j,k)*r)
          uetamu  = ueta*c0(j,k)                      
          vetamu  = veta*c0(j,k)                                        
          c2etamu = c2eta*beta(j,k)                                 
c         -third order extrap. of variables at +1/2 half node
          c1ext=xx*(-c1(j,km1) + 6.d0*c1(j,k) + 3.d0*c1(j,kp1))
          c2ext=xx*(-c2(j,km1) + 6.d0*c2(j,k) + 3.d0*c2(j,kp1))
          c3ext=xx*(-c3(j,km1) + 6.d0*c3(j,k) + 3.d0*c3(j,kp1))
          c4ext=xx*(-c4(j,km1) + 6.d0*c4(j,k) + 3.d0*c4(j,kp1))
          uext=xx*(-um1 + 6.d0*u + 3.d0*up1)
          vext=xx*(-vm1 + 6.d0*v + 3.d0*vp1)
c
          vflux(j,k,2)= c2ext*uetamu + c4ext*vetamu
          vflux(j,k,3)= c4ext*uetamu + c3ext*vetamu
          vflux(j,k,4)= uext*vflux(j,k,2) + vext*vflux(j,k,3)
     &                                                   + c1ext*c2etamu
 107    continue
c
c       ***                                        ***
c       ***********  Flux Differentiation  ***********
c       ***                                        ***
c
c       -the fluxes are stored at the kk=k+1/2 nodes.
c       -so find d/deta (vflux) at kk-1/2 node.
        do 230 n=2,4
        do 230 k=klow+1,kup-1   
          km2=k-2
          km1=k-1
          kp1=k+1
          do 229 j=jlow,jup
c           -fourth-order
            s(j,k,n) = s(j,k,n) + yy*(vflux(j,km2,n)-27.d0*(
     &             vflux(j,km1,n)-vflux(j,k,n)) -vflux(j,kp1,n))
c           -second-order
c             s(j,k,n) = s(j,k,n) + vflux(j,k,n) - vflux(j,km1,n)
 229      continue
 230    continue
c
        k=klow
        km1=k-1
        kp1=k+1
        kp2=k+2
        do 235 n=2,4
        do 235 j=jlow,jup
c         -third-order
          s(j,k,n) = s(j,k,n) + yy*(-23.d0*vflux(j,km1,n)+
     &        21.d0*vflux(j,k,n) + 3.d0*vflux(j,kp1,n) - vflux(j,kp2,n))
c         -second-order            
c            s(j,k,n) = s(j,k,n) + vflux(j,k,n) - vflux(j,km1,n)
 235    continue
c
        k=kup
        km1=k-1
        km2=k-2
        km3=k-3
        do 237 n=2,4
        do 237 j=jlow,jup
c         -third-order           
          s(j,k,n) = s(j,k,n) + yy*(23.d0*vflux(j,k,n) - 
     &          21.d0*vflux(j,km1,n)-3.d0*vflux(j,km2,n)+vflux(j,km3,n))
c         -second-order           
c            s(j,k,n) = s(j,k,n) + vflux(j,k,n) - vflux(j,km1,n)
 237    continue
c
      else
c       *********************************************************
c                             Second-Order
c       *********************************************************
        do 111 k=kbegin,kup
          kp1 = k+1
          do 110 j=jlow,jup
            rr1   = 1./q(j,k,1)
            rrp1  = 1./q(j,kp1,1)
            uup1  = q(j,kp1,2)*rrp1
            uu1   = q(j,k,2)*rr1
            vvp1  = q(j,kp1,3)*rrp1
            vv1   = q(j,k,3)*rr1
            ueta  = uup1 - uu1
            veta  = vvp1 - vv1
            c2eta = gamma*(press(j,kp1)*rrp1  - press(j,k)*rr1)
c            fmumd   = c0(j,k)
c            betmd   = beta(j,k)
            uetamu  = ueta*c0(j,k)
            vetamu  = veta*c0(j,k)
            c2etamu = c2eta*beta(j,k)
c     
            vflux(j,k,2)=0.5d0*(
     &            (c2(j,kp1) + c2(j,k))*uetamu
     &          + (c4(j,kp1) + c4(j,k))*vetamu)
            vflux(j,k,3)=0.5d0*(
     &            (c4(j,kp1) + c4(j,k))*uetamu
     &          + (c3(j,kp1) + c3(j,k))*vetamu)
            vflux(j,k,4)= 0.5*(
     &          + (uup1 + uu1)*vflux(j,k,2)
     &          + (vvp1 + vv1)*vflux(j,k,3)
     &          + (c1(j,kp1) + c1(j,k))*c2etamu)
 110      continue
 111    continue
c
        do 205 k=klow,kup
          km1 = k-1
          do 200 j=jlow,jup
            s(j,k,2) = s(j,k,2) + vflux(j,k,2) - vflux(j,km1,2)
            s(j,k,3) = s(j,k,3) + vflux(j,k,3) - vflux(j,km1,3)
            s(j,k,4) = s(j,k,4) + vflux(j,k,4) - vflux(j,km1,4)
 200      continue
 205    continue
      endif
c                                                                       
      return                                                            
      end                                                               
