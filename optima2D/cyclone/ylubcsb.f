c ********************************************************************
c ** LU Backsubstitution - for pentadiagonal systems                **
c ** y sweep routine                                                **
c ** Crout's method of factorization applied to pentadiagonals      **
c ********************************************************************
      subroutine ylubcsb(dm2,dm1,dd,dp1,dp2,b,jmax,kmax,
     &                   jlow,jup,klow,kup)
c ********************************************************************
c Variables:   dm2: vector 2 below diagonal
c              dm1: vector 1 below diagonal    
c              dd : vector of diagonal entries
c              dp1: vector 1 above diagonal
c              dp2: vector 2 above diagonal
c ********************************************************************
      dimension dm2(jmax,kmax), dm1(jmax,kmax), dd(jmax,kmax)
      dimension dp1(jmax,kmax), dp2(jmax,kmax), b(jmax,kmax) 
      do 199 j=jlow,jup
c       /* Forward Substitution     */
        k = klow + 1                    
        b(j,k) = b(j,k) - dm1(j,k)*b(j,k-1)
        do 50 k = klow+2,kup
          b(j,k) = b(j,k) - dm2(j,k)*b(j,k-2) - dm1(j,k)*b(j,k-1)
   50   continue
c       /* Back Substitution        */
        k = kup                         
        b(j,k) = b(j,k)/dd(j,k)
        k = kup - 1
        b(j,k) = (b(j,k) - dp1(j,k)*b(j,k+1))/dd(j,k)
        do 75 k = kup-2,klow,-1
          b(j,k)=(b(j,k)-dp2(j,k)*b(j,k+2)-dp1(j,k)*b(j,k+1))/dd(j,k)
   75   continue
  199 continue
      return
      end
