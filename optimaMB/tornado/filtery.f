C ********************************************************************
C ** SECOND -  FOURTH :  ORDER EXPLICIT FILTER ***********************
C ********************************************************************
      subroutine filtery(n,jdim,kdim,q,xyj,coef2,coef4,work,tmp,
     &          jbegin,jend,kbegin,kend)

      implicit none
c                                                                       
#include "../include/common.inc"
c                                                                       
      integer n,jdim,kdim,jbegin,jend,kbegin,kend,klow,kup,j,k
      integer kpl,kmi,kb
      double precision q(jdim,kdim,4),xyj(jdim,kdim)            
      double precision coef2(jdim,kdim),coef4(jdim,kdim)                       
      double precision work(jdim,kdim,3),tmp(jdim,kdim,4),dtd                
c                                                                       
c     fourth order smoothing, added explicitly to rhs                     
c     second order near shocks with pressure grd coeff.                   
c                                                                       
c     eta direction
c              
c     start differences each variable separately

      klow = kbegin+1
      kup  = kend-1
                                            
      do 35 k = kbegin,kup 
        kpl = k+1  
        do 35 j = jbegin,jend
          work(j,k,1) = q(j,kpl,n)*xyj(j,kpl) - q(j,k,n)*xyj(j,k)
35    continue        
c
c     -for fourth-difference
      do 36 k = klow,kup-1
        kpl = k+1        
        kmi = k-1        
        do 36 j = jbegin,jend        
          work(j,k,2) = work(j,kpl,1) - 2.* work(j,k,1) + work(j,kmi,1)
36    continue
c
c     -boundary
      kb = kbegin
      do 37 j = jbegin,jend
        work(j,kb,2) = q(j,kb+2,n)*xyj(j,kb+2) -
     &        2.*q(j,kb+1,n)*xyj(j,kb+1) + q(j,kb,n)*xyj(j,kb)
        work(j,kup,2) = work(j,kup-1,1) - work(j,kup,1)
 37   continue
c
      do 38 k = kbegin,kup
      do 38 j = jbegin,jend
        work(j,k,3) = (coef2(j,k)*work(j,k,1) - coef4(j,k)*work(j,k,2))
38    continue
c                                                                       
c     -add in dissipation
      dtd = dt / (1.d0 + phidt)                                           
      do 40 k = klow,kup                                                
      do 40 j = jbegin,jend                                                
         tmp(j,k,n) = (work(j,k,3) - work(j,k-1,3))*dtd           
 40   continue
c
      return                                                            
      end                                                               
