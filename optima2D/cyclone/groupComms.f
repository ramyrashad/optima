
      subroutine groupComms()

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer ii

#ifdef _MPI_VERSION
 
      !-- if we have already grouped communicators, return
      if (commsgrouped) then
         return
      endif     
 
      mycommid = floor(rank / dble(procs_per_chromo)) + 1

      do ii = 1, popsize
         call mpi_comm_split(COMM_CURRENT, mycommid, ii, commarr(ii), 
     &                       ierr)
      end do

      do ii = 1, popsize
         call mpi_comm_rank(commarr(ii), rank, ierr)
         call mpi_comm_size(commarr(ii), size, ierr)
      end do
      COMM_CURRENT = commarr(mycommid)

      commsgrouped = .true.

#else
      stop 'groupComms.f: requires compilation with MPI'
#endif 

      end
