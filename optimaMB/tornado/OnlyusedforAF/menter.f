c*****************************************************************************
c*                                                                           *
c*                            k-w SST MODEL                                  *
c*                                                                           *
c*****************************************************************************
c
      subroutine menter
     $     (iblk,jmax,kmax,jbegin,jend,kbegin,kend,jlow,jup,
     $     klow,kup,q,turmu,vk,ve,x,y,xy,xyj,ds,jtranlo1,jtranup1,
     $     jtranlo3,jtranup3,press,vort,fnu,
csd     $     smin1,smin2,smin3,smin4,smin,
     $     smin,
     $     jtranlo,jtranup,kwall,jmid,bnd,first)
c
c ?? maybe pass dk,de for subiteration scheme
c
c
#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/units.inc"
c#include "/usr/people/philippe/tornado/include/../include/parms.inc"
c#define debug
c
c#include "/usr/people/philippe/m_menter/source/bsl/decl_mb.f"
c
c     declarations for turb_mb.f
c
      dimension bnd(4)
      logical bnd,first
#include "../include/trip.inc"
c
      dimension q(jmax,kmax,4),turmu(jmax,kmax),vort(jmax,kmax)
      dimension press(jmax,kmax),xy(jmax,kmax,4),xyj(jmax,kmax)
      dimension fnu(jmax,kmax),ds(jmax,kmax)
c      dimension smin1(jmax,kmax),smin3(jmax,kmax)
c      dimension smin2(jmax,kmax),smin4(jmax,kmax)
      dimension smin(jmax,kmax),jtranlo(jmax,kmax),jtranup(jmax,kmax)
      dimension x(jmax,kmax),y(jmax,kmax),kwall(jmax,kmax)
      dimension vk(jmax,kmax),ve(jmax,kmax)
c
c?? get rid of one of the two visc3 visc4
c
c
cd     logical restart_ke
cd     common/zmentor1/ t_step_kw,vk_inf,ve_inf,turnu_inf
cd     common/zmentor2/ itertotal,EPS_ke,delta_ke,k2_l
cd     common/zmentor3/ restart_ke
c      logical restart
c
c
c     2)   declarations
c
c
      dimension dk(maxjb,maxkb),de(maxjb,maxkb)
      dimension dk_star(maxjb,maxkb),de_star(maxjb,maxkb)
      dimension bx_k(maxjb,maxkb),cx_k(maxjb,maxkb),dx_k(maxjb,maxkb)
      dimension rhs_k(maxjb,maxkb)
      dimension bx_e(maxjb,maxkb),cx_e(maxjb,maxkb),dx_e(maxjb,maxkb)
      dimension rhs_e(maxjb,maxkb)
      dimension by_k(maxjb,maxkb),cy_k(maxjb,maxkb),dy_k(maxjb,maxkb)
      dimension by_e(maxjb,maxkb),cy_e(maxjb,maxkb),dy_e(maxjb,maxkb)
      dimension ux(maxjb,maxkb),uy(maxjb,maxkb),vx(maxjb,maxkb)
      dimension vy(maxjb,maxkb)
c     dimension ds1(maxjb,maxkb)
      dimension turnu(maxjb,maxkb)
      dimension q11(maxjb,maxkb)
      dimension q22(maxjb,maxkb)
      dimension yplus(maxjb,maxkb)
      dimension p1(maxjb,maxkb),p2(maxjb,maxkb)
      dimension kedge(maxjb)
c
      dimension sigma_k(maxjb,maxkb),sigma_e(maxjb,maxkb)
      dimension beta(maxjb,maxkb),betas(maxjb,maxkb)
      dimension gam(maxjb,maxkb),cdiff(maxjb,maxkb),f_2(maxjb,maxkb)
      dimension f_1(maxjb,maxkb)
c
      dimension aak(maxjb),aae(maxjb),bbk(maxjb),bbe(maxjb)
      dimension cck(maxjb),cce(maxjb),ddk(maxjb),dde(maxjb)      
      logical lu_solver,wilcox,bradshaw
c
c
      lu_solver=.false.
c*********************************************************************
c      do k=kbegin,kend
c        do j=jbegin,jend
c            ds(j,k)=1.0/(1.+sqrt(xyj(j,k)))
c         enddo
c      enddo
c*********************************************************************
      itotaliter=1
c      call fpenab
c
      if ((.not.restart).and.first) write(n_out,*)
     &     'MUST HAVE INITIALIZATION',
     &     ' PROFILES TO RUN THIS TURBULENCE MODEL'
c     
      if (bnd(1).and.first) then
         if (jtranlo1.gt.0) jtranlo1=jtranlo1-2
         if ((jtranup1.ne.jmax).and.(jtranup1.gt.0)) 
     $        jtranup1=jtranup1+2
      endif
      if (bnd(3).and.first) then
         if (jtranlo3.gt.0) jtranlo3=jtranlo3-2
         if ((jtranup3.ne.jmax).and.(jtranup3.gt.0))
     $        jtranup3=jtranup3+2
      endif
c      if (iblk.eq.8) then
c         jtranlo3=0
c         jtranup3=2
c      endif
c
#ifndef debug
#include "const_mb.f"
#endif
c
c      if (iiseq.gt.1) dtm=dtiseq(iiseq)
c
#ifndef debug
#include "reset_q_1_mb.f"
#endif
#ifndef debug
#include "grad_vel_mb.f"
#endif
#ifndef debug
#include "flow_geom_mb_v1.f"
#endif
c
      do k=kbegin,kend
         do j=jbegin,jend
            dk_star(j,k)=0.0
            de_star(j,k)=0.0
            dk(j,k)=0.0
            de(j,k)=0.0
         enddo
      enddo
      do k=klow,kup
         do j=jlow,jup
            rhs_k(j,k)=0.0
            rhs_e(j,k)=0.0
         enddo
      enddo
      if (first) then
         itotaliter=itertotal
#ifndef debug
#include "tn_f_tm_mb.f"
#endif
c
c     outputting the turbulence variables 
c     (vk,uu,uv,vv)
      write(34) ((-turnu(j,k)*vort(j,k),j=2,jmax-1),k=2,kmax-1),
     $((-turnu(j,k)*(4./3.*ux(j,k)-2./3.*vy(j,k))
c     $+2./3.*vk(j,k)
     $     ,j=2,jmax-1),k=2,kmax-1),
     $     ((-turnu(j,k)*(uy(j,k)+vx(j,k)),j=2,jmax-1),
     $     k=2,kmax-1),
     $((-turnu(j,k)*(-2./3.*ux(j,k)+4./3.*vy(j,k))
c     $+2./3.*vk(j,k)
     $           ,j=2,jmax-1),k=2,kmax-1)
c
c
#ifndef debug
#include "ini_vkve_mb.f"
#endif
      endif
c**************************************************************
c transfered boundary condition on ve to the end of the routine
c**************************************************************
      if (bnd(1)) then
         k=kbegin
         do j=jlow,jup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j,k+1)**2)
cc            do k=kbegin+1,kbegin+5
cc               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
cc            enddo
         enddo
      endif
c************
c  most use a smaller wall sapcing at a blunt trailing edge
c*************
      if (bnd(2)) then
         j=jbegin
         do k=klow,kup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j+1,k)**2)
c            do k=kbegin+1,kbegin+5
c               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
c            enddo
         enddo
      endif
      if (bnd(3)) then
         k=kend
         do j=jlow,jup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j,k-1)**2)
cc            do k=kend-1,kend-5,-1
cc               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
cc            enddo
         enddo
      endif
      if (bnd(4)) then
         j=jend
         do k=klow,kup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j-1,k)**2)
c            do k=kbegin+1,kbegin+5
c               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
c            enddo
         enddo
      endif
      do 9999 iturbiter=1,itotaliter
#ifndef debug
#include "damp_mb.f"
#endif
#ifndef debug
#include "tn_f_vk_mb.f"
#endif
c
c#ifndef debug
c     include 'transition_mb_1
c     .f'
c#endif
cd        call trans
cc        ^^^^^^^^^^
cd    $          (maxblk,jmax,kmax,maxjb,maxkb,kbegin,kend,
cd    $          jtranlo,jtranup,re,fsmach,
cd    $          uy,vx,x,y,ve,beta_w,smin,turnu,vk,
cd    $          multi_trans,trip_present,bnd,iblk)
c     
c      if (iblk.eq.8) then
c         open(unit=77,file='tornado.f77',status='unknown',
c     $        form='unformatted')
c         open(unit=78,file='tornado.f78',status='unknown',
c     $        form='unformatted')
c         write(78) jup-jlow+1,kup-klow+1
c         write(78) ((x(j,k),j=jlow,jup),k=klow,kup),
c     $        ((y(j,k),j=jlow,jup),k=klow,kup)
c         write(77) jup-jlow+1,kup-klow+1
c         write(77) fsmach, alpha, re*fsmach, totime
c         write(77) ((f_1(j,k),j=jlow,jup),k=klow,kup),
c     $        ((f_2(j,k),j=jlow,jup),k=klow,kup),
c     $        ((turmu(j,k)*vort(j,k),j=jlow,jup),k=klow,kup),
c     $        ((smin(j,k),j=jlow,jup),k=klow,kup)
c         write(77)jtail1,numiter
c         rewind(77)
c         rewind(78)
c      endif
#ifndef debug
#include "convect_mb.f"
#endif
#ifndef debug
#include "diffus_mb.f"
#endif
#ifndef debug
#include "source_mb.f"
#endif
c
c*********
         dtm=t_step_kw
c*********
         isub=1
         nnit_max=30
         residual=100.
         resid_max=0.1
c         do while ((residual.gt.resid_max).and.(isub.le.nnit(iblk)))
         do while ((residual.gt.resid_max).and.(isub.le.nnit_max))
c
#ifndef debug
#include "upward_mb.f"
#endif
#ifndef debug
#include "downward_mb.f"
#endif
c#ifndef debug
c     c   #include "/usr/people/philippe/m_menter/source/bsl
c     /bc_delta_mb.f'
c#endif
cd        call bc_delta
cc        ^^^^^^^^^^^^^
cd    $          (maxjb,maxkb,jmax,kmax,xy,q,
cd    $          dk,dk_star,de,de_star,vk,ve,vk_inf,ve_inf)
c
         write(n_out,*) 'isub=',isub,'residual=',residual
         isub=isub+1
      enddo
#ifndef debug
#include "update_mb.f"
#endif
 9999 continue
#ifndef debug
#include "calc_tm_mb.f"
#endif
c
      if (bnd(1)) then
         k=kbegin
         do j=jlow,jup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j,k+1)**2)
c            do k=kbegin+1,kbegin+5
c               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
c            enddo
         enddo
      endif
c************
c  most use a smaller wall sapcing at a blunt trailing edge
c*************
      if (bnd(2)) then
         j=jbegin
         do k=klow,kup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j+1,k)**2)
c            do k=kbegin+1,kbegin+5
c               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
c            enddo
         enddo
      endif
      if (bnd(3)) then
         k=kend
         do j=jlow,jup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j,k-1)**2)
c            do k=kend-1,kend-5,-1
c               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
c            enddo
         enddo
      endif
      if (bnd(4)) then
         j=jend
         do k=klow,kup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j-1,k)**2)
c            do k=kbegin+1,kbegin+5
c               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
c            enddo
         enddo
      endif
c
#ifndef debug
#include "reset_q_2_mb.f"
#endif
c
c
      do k=kbegin,kup
         do j=jbegin,jend
            turmu(j,k)=turmu(j,k)*re
c     ds(j,k)=1.0
c            vk(j,k)=0.0
         enddo
      enddo
c     
c
      return
      end
