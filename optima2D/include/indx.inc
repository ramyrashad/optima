c     -- common block for grid indexes --

#include "../include/parm.inc"

      integer jmax, kmax, jm, km, jbegin, jend, kbegin, kend, jlow, jup
      integer klow, kup,  jmaxold, jplus(0:2*maxj), jminus(0:2*maxj)  
      common/index/                                                     
     &      jmax,       kmax,      jm,       km,      jbegin,   jend,
     &      kbegin,     kend,      jlow,     jup,     klow,     kup,
     &      jmaxold,    jplus,     jminus   

