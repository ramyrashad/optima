#ifndef YMP
d     subroutine grad_vel(jmax,kmax,maxjb,maxkb,jlow,jup,klow,kup,
d    $    jbegin,jend,kbegin,kend,
d    $    q,xy,ux,uy,vx,vy)
d     dimension q(jmax,kmax,4),xy(jmax,kmax,4),ux(maxjb,maxkb)
d     dimension uy(maxjb,maxkb),vx(maxjb,maxkb),vy(maxjb,maxkb)
d     write(6,*) 'in grad_vel_bsl.f'
#endif
c
c     5)   compute the velocity gradients ux,uy,vx,vy
c
      k=kbegin
      do j=jlow,jup
         ux(j,k)=xy(j,k,3)*0.5*(-3.*q(j,k,2)+4.*q(j,k+1,2)-q(j,k+2,2))
     $        +xy(j,k,1)*0.5*(q(j+1,k,2)-q(j-1,k,2))
         vx(j,k)=xy(j,k,3)*0.5*(-3.*q(j,k,3)+4.*q(j,k+1,3)-q(j,k+2,3))
     $        +xy(j,k,1)*0.5*(q(j+1,k,2)-q(j-1,k,2))
         uy(j,k)=xy(j,k,4)*0.5*(-3.*q(j,k,2)+4.*q(j,k+1,2)-q(j,k+2,2))
     $        +xy(j,k,2)*0.5*(q(j+1,k,2)-q(j-1,k,2))
         vy(j,k)=xy(j,k,4)*0.5*(-3.*q(j,k,3)+4.*q(j,k+1,3)-q(j,k+2,3))
     $        +xy(j,k,2)*0.5*(q(j+1,k,2)-q(j-1,k,2))
      enddo
      k=kend
      do j=jlow,jup
         ux(j,k)=-xy(j,k,3)*0.5*(-3.*q(j,k,2)+4.*q(j,k-1,2)-q(j,k-2,2))
     $        +xy(j,k,1)*0.5*(q(j+1,k,2)-q(j-1,k,2))
         vx(j,k)=-xy(j,k,3)*0.5*(-3.*q(j,k,3)+4.*q(j,k-1,3)-q(j,k-2,3))
     $        +xy(j,k,1)*0.5*(q(j+1,k,2)-q(j-1,k,2))
         uy(j,k)=-xy(j,k,4)*0.5*(-3.*q(j,k,2)+4.*q(j,k-1,2)-q(j,k-2,2))
     $        +xy(j,k,2)*0.5*(q(j+1,k,2)-q(j-1,k,2))
         vy(j,k)=-xy(j,k,4)*0.5*(-3.*q(j,k,3)+4.*q(j,k-1,3)-q(j,k-2,3))
     $        +xy(j,k,2)*0.5*(q(j+1,k,2)-q(j-1,k,2))
      enddo
      j=jbegin
      do k=klow,kup
         ux(j,k)=xy(j,k,3)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $        +xy(j,k,1)*0.5*(-3.*q(j,k,2)+4.*q(j+1,k,2)-q(j+2,k,2))
         vx(j,k)=xy(j,k,3)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $        +xy(j,k,1)*0.5*(-3.*q(j,k,2)+4.*q(j+1,k,2)-q(j+2,k,2))
         uy(j,k)=xy(j,k,4)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $        +xy(j,k,2)*0.5*(-3.*q(j,k,2)+4.*q(j+1,k,2)-q(j+2,k,2))
         vy(j,k)=xy(j,k,4)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $        +xy(j,k,2)*0.5*(-3.*q(j,k,3)+4.*q(j+1,k,3)-q(j+2,k,3))
      enddo
      j=jend
      do k=klow,kup
         ux(j,k)=xy(j,k,3)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $        -xy(j,k,1)*0.5*(-3.*q(j,k,2)+4.*q(j-1,k,2)-q(j-2,k,2))
         vx(j,k)=xy(j,k,3)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $        -xy(j,k,1)*0.5*(-3.*q(j,k,3)+4.*q(j-1,k,3)-q(j-2,k,3))
         uy(j,k)=xy(j,k,4)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $        -xy(j,k,2)*0.5*(-3.*q(j,k,2)+4.*q(j-1,k,2)-q(j-2,k,2))
         vy(j,k)=xy(j,k,4)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $        -xy(j,k,2)*0.5*(-3.*q(j,k,3)+4.*q(j-1,k,3)-q(j-2,k,3))
      enddo
c
      do k=klow,kup
         do j=jlow,jup
            ux(j,k)=xy(j,k,3)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $           +xy(j,k,1)*0.5*(q(j+1,k,2)-q(j-1,k,2))
            vx(j,k)=xy(j,k,3)*0.5*(q(j,k+1,3)-q(j,k-1,3))
     $           +xy(j,k,1)*0.5*(q(j+1,k,3)-q(j-1,k,3))
            uy(j,k)=xy(j,k,4)*0.5*(q(j,k+1,2)-q(j,k-1,2))
     $           +xy(j,k,2)*0.5*(q(j+1,k,2)-q(j-1,k,2))
            vy(j,k)=xy(j,k,4)*0.5*(q(j,k+1,3)-q(j,k-1,3))
     $           +xy(j,k,2)*0.5*(q(j+1,k,3)-q(j-1,k,3))
         enddo
      enddo
      ux(jbegin,kbegin)=0.5*(ux(jbegin+1,kbegin)+ux(jbegin,kbegin+1))
      ux(jbegin,kend)=0.5*(ux(jbegin+1,kend)+ux(jbegin,kend-1))
      ux(jend,kbegin)=0.5*(ux(jend-1,kbegin)+ux(jend,kbegin+1))
      ux(jend,kend)=0.5*(ux(jend-1,kend)+ux(jend,kend-1))
      uy(jbegin,kbegin)=0.5*(uy(jbegin+1,kbegin)+uy(jbegin,kbegin+1))
      uy(jbegin,kend)=0.5*(uy(jbegin+1,kend)+uy(jbegin,kend-1))
      uy(jend,kbegin)=0.5*(uy(jend-1,kbegin)+uy(jend,kbegin+1))
      uy(jend,kend)=0.5*(uy(jend-1,kend)+uy(jend,kend-1))
      vx(jbegin,kbegin)=0.5*(vx(jbegin+1,kbegin)+vx(jbegin,kbegin+1))
      vx(jbegin,kend)=0.5*(vx(jbegin+1,kend)+vx(jbegin,kend-1))
      vx(jend,kbegin)=0.5*(vx(jend-1,kbegin)+vx(jend,kbegin+1))
      vx(jend,kend)=0.5*(vx(jend-1,kend)+vx(jend,kend-1))
      vy(jbegin,kbegin)=0.5*(vy(jbegin+1,kbegin)+vy(jbegin,kbegin+1))
      vy(jbegin,kend)=0.5*(vy(jbegin+1,kend)+vy(jbegin,kend-1))
      vy(jend,kbegin)=0.5*(vy(jend-1,kbegin)+vy(jend,kbegin+1))
      vy(jend,kend)=0.5*(vy(jend-1,kend)+vy(jend,kend-1))
c
#ifndef YMP
d
d     return
d     end
#endif




