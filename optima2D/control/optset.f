c----------------------------------------------------------------------
c     -- read in files needed for optimization --
c     -- setup sparse jacobian --
c     written by: marian nemec
c     date: april 2000
c----------------------------------------------------------------------

      subroutine OptSet (jdim, kdim, cp_tar, dvs, idv, bcp, bap, bt,
     &      bknot, x, y, dx, dy, lambda, dvlow, dvupp) 

#ifdef _MPI_VERSION
      use mpi
#endif

      
#include "../include/arcom.inc"  
#include "../include/optcom.inc"  
#include "../include/mpi_info.inc"

      integer jdim, kdim, i, idv(nc+mpopt), h, ii

      double precision cp_tar(jbody,2), bcp(nc,2), bap(jbody,2)
      double precision bt(jbody), bknot(jbsord+nc)
      double precision dvs(nc+mpopt)
      double precision x(jdim,kdim), y(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision lambda(2*jdim*kdim*incr)
      double precision dvupp(nc+mpopt)
      double precision dvlow(nc+mpopt)

      jup = jmax-1
      kup = kmax-1
      jbegin = 1
      jend = jmax
      kbegin = 1
      kend = kmax
      jlow = 2
      klow = 2

c     -- read target pressure distribution 'cp_tar' if necessary--
      if ( obj_func.eq.1 .and. .not. opt_meth.eq.2  ) then
        do i = 1,jbody
          read ( cptar_unit, *) cp_tar(i,1),cp_tar(i,2)
c          write(scr_unit,*) cp_tar(i,1), cp_tar(i,2)
        end do
      end if

      ndv = 0
      ngdv = 0
      if (.not. ckfile) then
c     -- read initial airfoil control points --
         do i = 1,nc
            read ( bdef_unit, *) bcp(i,1),bcp(i,2)
c            write(scr_unit,*) bcp(i,1), bcp(i,2)
         end do

c     -- read initial parameter vector --
         do i = 1,jbody
            read ( bdef_unit, *) bt(i)
c            write(scr_unit,*) bt(i)
         end do

c     -- read initial knot vector --
         do i = 1,jbsord+nc
            read ( bdef_unit, *) bknot(i)
c            write(scr_unit,*) bknot(i)
         end do

c     -- read design variables --
         do i = 1,999
            if ( opt_meth .eq. 11 ) then
               read (dvs_unit,FMT=*,end=10) idv(i), dvs(i),
     &                                      dvlow(i), dvupp(i)
             else 
               read (dvs_unit,FMT=*,end=10) idv(i), dvs(i)
             end if
         end do
 10      continue
         ndv = i-1
         ngdv = ndv
      end if



c     Initialize elasticity grid perturbations dx & dy, and grid adjoint
c     vectors lambda.
      if ((opt_meth == 2 .or. opt_meth == 3 .or. opt_meth == 4) .and.
     |    (incr .ne. 0)) then
        do i = 1, jdim*kdim*incr
          dx(i) = 0.d0
          dy(i) = 0.d0
          lambda(i) = 0.d0
          lambda(i+jdim*kdim*incr) = 0.d0
        enddo
      endif


c     -- generate correct grid for M vs. Cd sweep at fixed Cl --
      if (opt_meth.eq.4 .and. ngdv.gt.0) then
         write (scr_unit,*) 'creating grid'
         call regrid( -1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot,x,y,dx,dy,.false.)
         ngdv = 0
         ndv = 0
      end if

c     -- multi-point optimization with alpha as design variable --
c     --  M vs. Cd sweep at fixed Cl --
      ii = 0
      do i=1, mpopt
         if ( dvalfa(i) ) then
            ii=ii+1
            dvs(ngdv+ii) = alphas(i)
            idv(ngdv+i)  = ngdv+ii
            ndv = ndv + 1
         end if
      end do

      !-- Store number of AOA design variables
      ndvalfa = ii

c     -- Set Mach number as a design variable if using an aircraft range
c     -- or endurance objective function
      if (ndvmach.gt.0) then
         do h = 1,ndvmach
            ii=ii+1
            dvs(ngdv+ii) = dvmach0 ! Initialize mach # design variable
            ndv = ndv + 1
         end do
      end if      
      
      if (ndvmach.eq.1) then
         idv(ngdv+mpopt+1) = ngdv + ndvalfa + 1
         idv(ngdv+mpopt+2) = ngdv + ndvalfa + 1
      else if (ndvmach.eq.2) then
         idv(ngdv+mpopt+1) = ngdv + ndvalfa + 1
         idv(ngdv+mpopt+2) = ngdv + ndvalfa + 2
      end if
      
      machdv1_index = idv(ngdv+mpopt+1)
      machdv2_index = idv(ngdv+mpopt+2)
      
      if (.not.ckfile) then
         if (rank.eq.0) then
            write (opt_unit,20)
 20         format (/3x,'Airfoil control points:'/)
            do i=1,nc
               write (opt_unit,30) i, bcp(i,1), bcp(i,2)
            end do
 30         format(i4, 2e14.6)
         end if
      end if

      if(rank.eq.0) then
         write (opt_unit,40)
 40      format (/3x,'Design variables:'/)
         do i=1,ndv
            write (opt_unit,50) i, dvs(i)
         end do
 50      format(i4, e14.6)

         ii = 0
         do i=1, mpopt      
            if ( dvalfa(i) ) then
               ii = ii + 1
               write (opt_unit,52) i
               write (opt_unit,54) dvs(ngdv+ii), ngdv+ii
            end if
         end do

         do i=1,mpopt
            if ( dvmach(i) ) then     
               ii = ii + 1
               write (opt_unit,522) i
               write (opt_unit,544) dvs(ngdv+ii), ngdv+ii
            end if
         end do

 52      format (/3x,'Angle of attack is a design variable for',
     &     ' design point', i3)
 54      format (3x,'Initial AOA:',f6.2,1x,'DVS index:',i4)

 522     format (/3x,'Mach number is a design variable for',
     &     ' design point', i3)    
 544     format (3x,'Initial Mach #:',f6.2,1x,'DVS index:',i4) 

         write (opt_unit,60) ndv
 60      format (/3x,'Number of design variables:', i4/)

      end if ! (rank.eq.0)

      if (obj_func.gt.4 .or. (opt_meth.eq.2 .and. .not.periodic)) then

c     -- deternine nodes closest to x-location of thickness constraints --
         do i = 1, ntcon
            do j=jtail2,jtail1,-1
               if ( x(j,1) .le. ctx(i) ) then
                  juptx(i) = j
                  goto 80
               end if
            end do
 80         continue

            do j=jtail1,jtail2
               if ( x(j,1) .le. ctx(i) ) then
                  jlotx(i) = j
                  goto 100
               end if
            end do
 100        continue

            if ( abs( x(jlotx(i)-1,1) - ctx(i) ) .lt.
     &           abs( x(jlotx(i),1) - ctx(i)) ) jlotx(i) = jlotx(i)-1

            if ( abs( x(juptx(i)+1,1) - ctx(i) ) .lt.
     &           abs( x(juptx(i),1) - ctx(i)) ) juptx(i) = juptx(i)+1
         end do

c     -- Do same for Range Thickness Constraints: determine nodes
c     closest to x-location of thickness constraints --

c     -- split range into subdivisions (effectively the range is a
C     series of regular thickness constraints, with only the max
C     thickness violation among the positions being counted)

         do nt = 1, nrtcon
            do i = 1, crtxn(nt)+1
               crtxsub(nt,i) = crtxl(nt) + (i-1)*((crtxt(nt) - crtxl(nt)
     &              )/crtxn(nt))

               !write(scr_unit,*) "i=",i
               !write(scr_unit,*) "crtxsub(nt,i)=",crtxsub(nt,i)

            end do
         end do

c     -- calculate initial area for area constraint --

         if (opt_cycle.eq.1) then 

            areainit = 0.0

            !-- this allows specifically setting the reference area
            !-- in the opt.inp file, instead of just area fraction
            if (arearef .ne. 0) then
               areainit = arearef
            else
               do j=jtail2,jtail1+1,-1
                  areainit = areainit 
     &                     + ( x(j,1)-x(j-1,1) )*( (y(j,1)+y(j-1,1))/2 )
c                 note: the negatives should work out correctly
c                 write(scr_unit,*) 'j=',j
c                 write(scr_unit,*) 'area=',areainit
               end do
            end if

         end if

c     -- find closest coordinates --

         do i = 1, nrtcon
            do subi = 1, crtxn(i)+1
               do j=jtail2,jtail1,-1
                  if ( x(j,1) .le. crtxsub(i,subi) ) then
                     juprtxsub(i,subi) = j
                     goto 81
                  end if
               end do
 81            continue

               do j=jtail1,jtail2
                  if ( x(j,1) .le. crtxsub(i,subi) ) then
                     jlortxsub(i,subi) = j
                     goto 101
                  end if
               end do
 101           continue

               if ( abs( x(jlortxsub(i,subi)-1,1) - ctx(i) ) .lt.
     &              abs( x(jlortxsub(i,subi),1) - crtxsub(i,subi)) )
     &              jlortxsub(i,subi) = jlortxsub(i,subi)-1

               if ( abs( x(juprtxsub(i,subi)+1,1) - crtxsub(i,subi) )
     &              .lt.abs( x(juprtxsub(i,subi),1) - crtxsub(i,subi)) )
     &              juprtxsub(i,subi) = juprtxsub(i,subi)+1
            end do
         end do

         if(rank.eq.0) then

            write (opt_unit,200) ntcon
 200        format (3x,'Number of thickness constraints:',i3)

            if (ntcon .gt. 0) then
               write (opt_unit,220)
 220           format (/3x,'Cons. #',3x,'X-Coord',3x,'L.S. J# and X',3x,
     &           'U.S. J# and X',3x,'Min. Thickness')

               do i = 1,ntcon
                write (opt_unit,240) i, ctx(i), jlotx(i), x(jlotx(i),1),
     &              juptx(i), x(juptx(i),1), cty_tar(i) 
               end do
 240           format (5x,i3,5x,f6.3,5x,i4,f7.4,5x,i4,f7.4,5x,f7.4)
            end if

            write (opt_unit,201) nrtcon
 201        format (3x,'Number of range thickness constraints:',i3)

         end if ! (rank.eq.0)

         if (nrtcon .gt. 0) then
           if(rank.eq.0)then
               write (opt_unit,221)
 221           format (/3x,'Cons.',3x,3x,'Leading X',3x
     $           ,'L.S. J#',3x, 'U.S. J#',3x,'Trailing X',3x
     $           ,'L.S. J#',3x,'U.S. J#',3x,'Sub Points',3x
     $           ,'Min. Thickness',3x,'Init. Thickness',3x
     $           ,'Max. Thick X-Coord')
            end if !(rank.eq.0)

c     -- find max thickness among subdivisions in range specified --
            do i = 1,nrtcon

               maxrthc = 0.0
            
               do subi = 1, crtxn(i)+1
               maxrthctemp = y(juprtxsub(i,subi),1) - y(jlortxsub(i,subi
     &              ),1)

                  !write(scr_unit,*) 'subi=',subi
                  !write(scr_unit,*) 'maxrthctemp=',maxrthctemp

                  if (maxrthc .lt. maxrthctemp) then
                     maxrthc = maxrthctemp
                     maxrthcxloc = crtxsub(i,subi)
                  end if
               end do

               !write(scr_unit,*) 'maxrthc=',maxrthc
               !write(scr_unit,*) 'maxrthcxloc=',maxrthcxloc

               if(rank.eq.0)then

                  write (opt_unit,241) i, crtxl(i), jlortxsub(i,1),
     $           juprtxsub(i,1), crtxt(i), jlortxsub(i,crtxn(i)+1),
     $           juprtxsub(i,crtxn(i)+1), crtxn(i), crthtar(i), maxrthc,
     $           maxrthcxloc
            
c     -- write out sub-points --
                  do subi = 1, crtxn(i)+1
                     write (opt_unit,242) crtxsub(nt,subi)
                  end do 
               end if !(rank.eq.0)
            end do
 241        format 
     &       (3x,i3,7x,i3,5x,f7.3,5x,i4,6x,i4,6x,f7.4,6x,i4,6x,i4,6x
     &        ,i4,9x,f7.4,10x,f7.4,11x,f7.4)
 242        format (3x,f7.3)
            write (opt_unit,*)

         end if ! (nrtcon.gt.0)

         if(rank.eq.0)then
            write (opt_unit,260) wfd, wtc
 260        format (/3x,'Weight factors WFD and WTC = ',2f6.2/)
            write (opt_unit,270) 
 270        format (3x,'Weight factor WFL at each design point:')
            do i=1,mpopt
               write (opt_unit,280) i,wfls(i)
            end do
 280        format (8x,i3,f6.2)
         end if ! (rank.eq.0)

      end if

c     -- check if target pressure distribution x-coordinate matches 
c     body coordinates of initial grid --

      if ( obj_func.eq.1 .and. .not. opt_meth.eq.2) 
     &        call cp_interp (jdim, kdim, cp_tar,x)

      if(rank.eq.0)then
         call flush (opt_unit)
      end if

      return
      end                       !optset
