      SUBROUTINE save_dhatq(jdim,kdim,nmax,jlow,jup,klow,kup,s,dhatq,
     &     rsa,dhatt)

      implicit none

#include "../include/common.inc"
#include "../include/parms.inc"

      INTEGER jdim,kdim,jlow,jup,klow,kup,j,k,n,nmax
      DOUBLE PRECISION s(jdim,kdim,4),dhatq(jdim,kdim,4,6)
      DOUBLE PRECISION rsa(jdim,kdim),dhatt(jdim,kdim,6)

      DO n = 1,4
         DO k = klow,kup
            DO j = jlow,jup
               dhatq(j,k,n,jstage) = s(j,k,n)
            ENDDO
         ENDDO
      ENDDO

      if (nmax .eq. 5) then
         DO k = klow,kup
            DO j = jlow,jup
               dhatt(j,k,jstage) = rsa(j,k)
            ENDDO
         ENDDO
      end if

      RETURN
      END
