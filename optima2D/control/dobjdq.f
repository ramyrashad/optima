c----------------------------------------------------------------------
c     -- subroutine to calculate the derivative of the objective
c     function with respect to the state vector q ala analytic formula
c     and finite differences. --
c     
c     written by: marian nemec, april 2000
c     modified by: laura billing, september 2005 (obj_func=8 added)
c----------------------------------------------------------------------
      subroutine dOBJdQ (jdim, kdim, ndim, x, y, xy, xyj, q, dOdQ,
     &      cp_tar, cpa, press, obj0)

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit 
     &     none
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      !-- Declare all variables

      integer 
     &     jdim, kdim, ndim, i, j, jj, k, n, na, j2, k2, j3, k3,
     &     jMaxMach, kMaxMach, nscal, j1, diff_flag, jtp, ja, jb, jp1,
     &     jm1, jm2, mp

      double precision 
     &     q(jdim,kdim,ndim),xyj(jdim,kdim),xy(jdim,kdim,4),
     &     x(jdim,kdim), y(jdim,kdim), dOdQ(jdim,kdim,ndim),
     &     dcddqFD(jdim,kdim,ndim),dmmdqFD(jdim,kdim,ndim), 
     &     cp_tar(jbody,2), cpa(jdim,kdim), press(jdim,kdim),
     &     MaxMachp, MaxMachm, aMaxMach, uMaxMach, vMaxMach,
     &     pMaxMach, ksMachp, ksMachm, ks, MaxMach,
     &     dMdQ(4), dadQ(4), dadq1, dq1dQ(4), dPrdQ(4),
     &     dMdu, dudQ(4), dMdv, dvdQ(4), dMda, dadP,
     &     dksdQ(4), dksdg, dgdQ(4), dgdM, P, a, U, V, Mach,
     &     dksNumer, g, dksMachdks, tmp, dobjdcd, dobjdcl,
     &     dksdrc, ksSum1c, ksSum2c, ksSum3c, rKSc,
     &     dksdr1, ksSum11, ksSum21, ksSum31, rKS1,
     &     dksdrd, ksSum1d, ksSum2d, ksSum3d, rKSd,
     &     drhoddksdrc, drhoddksdr1, drhoddksdrd, drhodQ(4),
     &     ddksdrcdrho, ddksdr1drho, ddksdrdgc, ddksdrdg1,
     &     dksMachdQ(4), drhodQ_top(4), drhodQ_bot,
     &     dpdq(jdim,4), dzdq(jdim,4), dcdidq(jdim,4),
     &     dccdq(jdim,4),dcndq(jdim,4), dclidq(jdim,4),
     &     dcnvdq(jdim,kdim,4),dccvdq(jdim,kdim,4),
     &     dcdtdq(jdim,kdim,4),dcdvvdq(jdim,kdim,4),
     &     dcltdq(jdim,kdim,4), c_upp, c_low,
     &     dclvvdq(jdim,kdim,4), stepsize, pp, press_tmp, cdtp, cdtm,
     &     cltp, cltm, objp, objm, cmtp, cmtm, cpc, clt2,
     &     dcfavjm1dq(jdim,kdim,4), dcfavjdq(jdim,kdim,4),
     &     dcfavjp1dq(jdim,kdim,4), dcfavjp2dq(jdim,kdim,4),
     &     qtemp(jdim,kdim,4), uinf2,
     &     z(maxj),work(maxj,29), cinf, alngth, amu,    
     &     xixjm1, xiyjm1, xixj, xiyj, etaxj, etayj,
     &     xixjp1, xiyjp1,
     &     obj0, L2dOdQ

      common/worksp/z,work

#ifdef _MPI_VERSION
      mp = rank+1
#else
      mp = 1
#endif

c     -- note 1: the mult. by jacobian is used to cancel the jacobian in
c     dR/dQ, since the flow jacobian is formed with respect to the
c     J^-1(Q) variables vector while for optimization it is easier to
c     work with just Q i.e. (rho, rho*u, rho*v, e). --

c     -- note 2: for viscous flow dOBJdQ is kept zero at the body for
c     the momentum equation since the gradient is independent of these
c     values --

c     -- note 3: q does not contain jacobian --

c     -- note 4: for euler cases drag is only dependent on pressure at
c     airfoil boundary, else Dobj/Dq is zero. --

c     -- note 5: Dobj/Dq is calculated using central-differences --

c     -- note 6: for viscous cases drag depends on pressure at the body
c     as well as Q at k=1, 2 and 3. -- 

c     -- note 7: for euler cases lift is only dependent on pressure at
c     airfoil boundary , else Dobj/Dq is zero. --

c     -- note 8: for viscous cases lift depends on pressure at the body
c     as well as Q at k=1, 2 and 3. --

 
      !-- Initialize some arrays

      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
            dOdQ(j,k,n) = 0.d0
          end do
        end do
      end do

      do n = 1,4
         do k = 1,kdim
            do j = 1,jdim
               dclvvdq(j,k,n) = 0.d0
               dcnvdq(j,k,n) = 0.d0               
               dccvdq(j,k,n) = 0.d0
               dcfavjm1dq(j,k,n) = 0.d0
               dcfavjdq(j,k,n) = 0.d0
               dcfavjp1dq(j,k,n) = 0.d0
               dcfavjp2dq(j,k,n) = 0.d0
               dcdtdq(j,k,n) = 0.d0
               dcltdq(j,k,n) = 0.d0
               dcdvvdq(j,k,n) = 0.d0
          end do
        end do
      end do 

      do n = 1,4
         do j = 1,jdim
            dpdq(j,n) = 0.d0
            dzdq(j,n) = 0.d0
            dcdidq(j,n) = 0.d0
            dccdq(j,n) = 0.d0
            dcndq(j,n) = 0.d0
            dclidq(j,n) = 0.d0
         end do
      end do

      if (obj_func.eq.1) then
c     -- inverse design --
c     -- use analytic derivation --
         
         if (.not. viscous) then
            i = 1
            do j = jtail1,jtail2

               dOdQ(j,1,1) = ( xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     &              /q(j,1,1)**2*( q(j,1,2)**2+ q(j,1,3)**2 )/(fsmach
     &              *fsmach))

               dOdQ(j,1,2) = ( - xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )
     &              *gami*q(j,1,2)/q(j,1,1)*2.d0/(fsmach*fsmach) )

               dOdQ(j,1,3) = (- xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     &              *q(j,1,3)/q(j,1,1)*2.d0/(fsmach*fsmach))

               dOdQ(j,1,4) = (xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     &              *2.d0/(fsmach*fsmach))

               i = i + 1
            end do
         else

c     -- viscous flow --
            i = 1

            do j = jtail1,jtail2

               dOdQ(j,1,1) = ( xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     &              /q(j,1,1)**2*( q(j,1,2)**2+ q(j,1,3)**2 )/(fsmach
     &              *fsmach))
               dOdQ(j,1,4) = (xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     &              *2.d0/(fsmach*fsmach))

               i = i + 1
            end do
         end if

c     -- use finite-difference formula --
c     -- q does not contain jacobian --
c     -- the pressure objective function depends on the boundary 
c     pressure, else Dobj/Dq is zero. --

c     cpc = 2.d0/(gamma*fsmach*fsmach)
c     do n = 1,4
c     i = 1
c     do j = jtail1,jtail2
c     tmp = q(j,1,n)
c     stepsize = fd_eta*tmp
c     q(j,1,n) = q(j,1,n) + stepsize
c     stepsize = q(j,1,n) - tmp
c     pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2 )/
c     &            q(j,1,1) )
c     cp = ( pp*gamma - 1.d0 )*cpc
c     objp = 0.5d0*( cp - cp_tar(i,2) )**2
c     
c     q(j,1,n) = tmp - stepsize
c     
c     pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2 )/
c     &            q(j,1,1) )
c     cp = ( pp*gamma - 1.d0 )*cpc
c     objm = 0.5d0*( cp - cp_tar(i,2) )**2
c     
c     dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0*stepsize)
c     q(j,1,n) = tmp
c     i = i + 1
c     end do
c     end do

      else if ( obj_func.eq.2 ) then
c     -- drag minimization, gradients must be calculated ala finite
c     differences and clapha must be turned to true --

c     v1_n = 0.d0
c     do n=1,4
c     do k=1,kend
c     do j=1,jend
c     v1_n = v1_n + q(j,k,n)**2
c     end do
c     end do
c     end do
c     v1_n = dsqrt(v1_n)
c     fd_eta  = dsqrt(2.2d-16) / v1_n

         do n = 1,4
            do j = jtail1,jtail2
               tmp = q(j,1,n)
               stepsize = fd_eta*tmp

               if ( dabs(stepsize) .lt. 1.d-9 ) then
                  stepsize = 1.d-9*dsign(1.d0,stepsize)
c                  write (opt_unit,20) 
c                  write (opt_unit,30) j, k, n, tmp, stepsize
               end if

               q(j,1,n) = q(j,1,n) + stepsize
               stepsize = q(j,1,n) - tmp
               pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2
     &              )/q(j,1,1) )
               press_tmp = press(j,1)
               press(j,1) = pp
               call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
               cdtp = cdi + cdv
               press(j,1) = press_tmp
               q(j,1,n) = tmp - stepsize
               pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2
     &              )/q(j,1,1) )
               press_tmp = press(j,1)
               press(j,1) = pp
               call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
               cdtm = cdi + cdv
               press(j,1) = press_tmp
               q(j,1,n) = tmp  

               dOdQ(j,1,n) = xyj(j,1)*( cdtp - cdtm ) / (2.d0*stepsize)
            end do
         end do

      else if ( obj_func.eq.3 ) then
c     -- drag minimization with composite objective function --

c     v1_n = 0.d0
c     do n=1,4
c     do k=1,kend
c     do j=1,jend
c     v1_n = v1_n + q(j,k,n)**2
c     end do
c     end do
c     end do
c     v1_n = dsqrt(v1_n)
c     fd_eta  = dsqrt(2.2d-16) / v1_n

         if (.not. viscous) then
            do n = 1,4
               do j = jtail1,jtail2
                  tmp = q(j,1,n)
                  stepsize = fd_eta*tmp

                  if ( dabs(stepsize) .lt. 1.d-9 ) then
                     stepsize = 1.d-9*dsign(1.d0,stepsize)
c                     write (opt_unit,20) 
c                     write (opt_unit,30) j, k, n, tmp, stepsize
                  end if

                  q(j,1,n) = q(j,1,n) + stepsize
                  stepsize = q(j,1,n) - tmp
                  pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
     &                 **2)/q(j,1,1) )
                  press_tmp = press(j,1)
                  press(j,1) = pp

                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cdtp = cdi + cdv
                  cltp = cli + clv
                  objp = wfd*( cdtp-cd_tar )**2 + wfl*( cltp-cl_tar )**2

                  press(j,1) = press_tmp
                  q(j,1,n) = tmp - stepsize
                  pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
     &                 **2)/q(j,1,1) )
                  press_tmp = press(j,1)
                  press(j,1) = pp

                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cdtp = cdi + cdv
                  cltp = cli + clv
                  objm = wfd*( cdtp-cd_tar )**2 + wfl*( cltp-cl_tar )**2

                  press(j,1) = press_tmp
                  q(j,1,n) = tmp  

                  dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
     &                 *stepsize)
               end do
            end do
         else
c     -- viscous flow --
            k=1
            do n = 1,4, 3
               do j = jtail1,jtail2
                  tmp = q(j,k,n)
                  stepsize = fd_eta*tmp

                  if ( dabs(stepsize) .lt. 1.d-9 ) then
                     stepsize = 1.d-9*dsign(1.d0,stepsize)
c                     write (opt_unit,20) 
c                     write (opt_unit,30) j, k, n, tmp, stepsize
                  end if

                  q(j,k,n) = q(j,k,n) + stepsize
                  stepsize = q(j,k,n) - tmp
                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                 **2 )/q(j,k,1) )

                  press_tmp = press(j,k)
                  press(j,k) = pp
                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cdtp = cdi + cdv
                  cltp = cli + clv

                  objp = wfd*( cdtp-cd_tar )**2 + wfl*( cltp-cl_tar )**2
                  press(j,k) = press_tmp

                  q(j,k,n) = tmp - stepsize
                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                 **2 )/q(j,k,1) )
                  press_tmp = press(j,k)
                  press(j,k) = pp
                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cdtp = cdi + cdv
                  cltp = cli + clv

                  objm = wfd*( cdtp-cd_tar )**2 + wfl*( cltp-cl_tar )**2
                  press(j,k) = press_tmp
                  q(j,k,n) = tmp  

                  dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                 *stepsize)
               end do
            end do                       

            do n = 1,4
               do k = 2,3
                  do j = jtail1,jtail2
                     tmp = q(j,k,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
c                        write (opt_unit,20) 
c                        write (opt_unit,30) j, k, n, tmp, stepsize
                     end if

                     q(j,k,n) = q(j,k,n) + stepsize
                     stepsize = q(j,k,n) - tmp
                     pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
     &                    q(j,k,3)**2 )/q(j,k,1) )

                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                    cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv

                     objp = wfd*( cdtp-cd_tar )**2 + wfl*( cltp-cl_tar )
     &                    **2
                     press(j,k) = press_tmp

                     q(j,k,n) = tmp - stepsize
                     pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k
     &                    ,3)**2 )/q(j,k,1) )
                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                    cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv

                     objm = wfd*( cdtp-cd_tar )**2 + wfl*( cltp-cl_tar )
     &                    **2
                     press(j,k) = press_tmp
                     q(j,k,n) = tmp  

                     dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                    *stepsize)

                  end do
               end do           
            end do

         end if        
      else if ( obj_func.eq.4 ) then
c     -- lift maximization  --

c     v1_n = 0.d0
c     do n=1,4
c     do k=1,kend
c     do j=1,jend
c     v1_n = v1_n + q(j,k,n)**2
c     end do
c     end do
c     end do
c     v1_n = dsqrt(v1_n)
c     fd_eta  = dsqrt(2.2d-16) / v1_n

        if (.not. viscous) then
          do n = 1,4
            do j = jtail1,jtail2
              tmp = q(j,1,n)
              stepsize = fd_eta*tmp

              if ( dabs(stepsize) .lt. 1.d-9 ) then
                stepsize = 1.d-9*dsign(1.d0,stepsize)
c                write (opt_unit,20) 
c                write (opt_unit,30) j, k, n, tmp, stepsize
              end if

              q(j,1,n) = q(j,1,n) + stepsize
              stepsize = q(j,1,n) - tmp
              pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2
     &              )/q(j,1,1) )
              press_tmp = press(j,1)
              press(j,1) = pp

              call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
              cltp = cli + clv
              objp = -cltp

              press(j,1) = press_tmp
              q(j,1,n) = tmp - stepsize
              pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2
     &              )/q(j,1,1) )
              press_tmp = press(j,1)
              press(j,1) = pp

              call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
              cltp = cli + clv
              objm = -cltp

              press(j,1) = press_tmp
              q(j,1,n) = tmp  

              dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0*stepsize)
            end do
          end do
        else
c     -- viscous flow --
          k=1
          do n = 1,4,3
            do j = jtail1,jtail2
              tmp = q(j,k,n)
              stepsize = fd_eta*tmp

              if ( dabs(stepsize) .lt. 1.d-9 ) then
                stepsize = 1.d-9*dsign(1.d0,stepsize)
c                write (opt_unit,20) 
c                write (opt_unit,30) j, k, n, tmp, stepsize
              end if

              q(j,k,n) = q(j,k,n) + stepsize
              stepsize = q(j,k,n) - tmp
              pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &              **2 )/q(j,k,1) )

              press_tmp = press(j,k)
              press(j,k) = pp
              call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
              cltp = cli + clv

              objp = -cltp
              press(j,k) = press_tmp

              q(j,k,n) = tmp - stepsize
              pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &              **2 )/q(j,k,1) )
              press_tmp = press(j,k)
              press(j,k) = pp
              call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
              cltp = cli + clv

              objm = -cltp
              press(j,k) = press_tmp
              q(j,k,n) = tmp  

              dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &              *stepsize)
            end do
          end do                       

          do n = 1,4
            do k = 2,3
              do j = jtail1,jtail2
                tmp = q(j,k,n)
                stepsize = fd_eta*tmp

                if ( dabs(stepsize) .lt. 1.d-9 ) then
                  stepsize = 1.d-9*dsign(1.d0,stepsize)
c                  write (opt_unit,20) 
c                  write (opt_unit,30) j, k, n, tmp, stepsize
                end if

                q(j,k,n) = q(j,k,n) + stepsize
                stepsize = q(j,k,n) - tmp
                pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                **2 )/q(j,k,1) )

                press_tmp = press(j,k)
                press(j,k) = pp
                call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                cli, cdi, cmi, clv, cdv, cmv)
                cltp = cli + clv

                objp = -cltp
                press(j,k) = press_tmp

                q(j,k,n) = tmp - stepsize
                pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                **2 )/q(j,k,1) )
                press_tmp = press(j,k)
                press(j,k) = pp
                call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                cli, cdi, cmi, clv, cdv, cmv)
                cltp = cli + clv

                objm = -cltp
                press(j,k) = press_tmp
                q(j,k,n) = tmp  

                dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                *stepsize)

              end do
            end do           
          end do

        end if
        
      else if ( obj_func.eq.5 ) then
c     -- max (Cl/Cd) --

c     v1_n = 0.d0
c     do n=1,4
c     do k=1,kend
c     do j=1,jend
c     v1_n = v1_n + q(j,k,n)**2
c     end do
c     end do
c     end do
c     v1_n = dsqrt(v1_n)
c     fd_eta  = dsqrt(2.2d-16) / v1_n

        if (.not. viscous) then
          do n = 1,4
            do j = jtail1,jtail2
              tmp = q(j,1,n)
              stepsize = fd_eta*tmp

              if ( dabs(stepsize) .lt. 1.d-9 ) then
                stepsize = 1.d-9*dsign(1.d0,stepsize)
c                write (opt_unit,20) 
c                write (opt_unit,30) j, k, n, tmp, stepsize
              end if

              q(j,1,n) = q(j,1,n) + stepsize
              stepsize = q(j,1,n) - tmp
              pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2
     &              )/q(j,1,1) )
              press_tmp = press(j,1)
              press(j,1) = pp

              call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
              cdtp = cdi + cdv
              cltp = cli + clv
              objp = cdtp/cltp

              press(j,1) = press_tmp
              q(j,1,n) = tmp - stepsize
              pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2
     &              )/q(j,1,1) )
              press_tmp = press(j,1)
              press(j,1) = pp

              call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
              cdtp = cdi + cdv
              cltp = cli + clv
              objm = cdtp/cltp

              press(j,1) = press_tmp
              q(j,1,n) = tmp  

              dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0*stepsize)
            end do
          end do

        else

c     -- viscous flow --
c     -- note 1: dOBJdQ is kept zero at the body for the momentum
c     equation since the gradient is independent of these values --
          k=1
          do n = 1,4,3
            do j = jtail1,jtail2
              tmp = q(j,k,n)
              stepsize = fd_eta*tmp

              if ( dabs(stepsize) .lt. 1.d-9 ) then
                stepsize = 1.d-9*dsign(1.d0,stepsize)
c                write (opt_unit,20) 
c                write (opt_unit,30) j, k, n, tmp, stepsize
              end if

              q(j,k,n) = q(j,k,n) + stepsize
              stepsize = q(j,k,n) - tmp
              pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &              **2 )/q(j,k,1) )

              press_tmp = press(j,k)
              press(j,k) = pp
              call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
              cdtp = cdi + cdv
              cltp = cli + clv

              objp = cdtp/cltp
              press(j,k) = press_tmp

              q(j,k,n) = tmp - stepsize
              pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &              **2 )/q(j,k,1) )
              press_tmp = press(j,k)
              press(j,k) = pp
              call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &              cdi, cmi, clv, cdv, cmv)
              cdtp = cdi + cdv
              cltp = cli + clv

              objm = cdtp/cltp
              press(j,k) = press_tmp
              q(j,k,n) = tmp  

              dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &              *stepsize)
            end do
          end do                       

          do n = 1,4
            do k = 2,3
              do j = jtail1,jtail2
                tmp = q(j,k,n)
                stepsize = fd_eta*tmp

                if ( dabs(stepsize) .lt. 1.d-9 ) then
                  stepsize = 1.d-9*dsign(1.d0,stepsize)
c                  write (opt_unit,20) 
c                  write (opt_unit,30) j, k, n, tmp, stepsize
                end if

                q(j,k,n) = q(j,k,n) + stepsize
                stepsize = q(j,k,n) - tmp
                pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                **2 )/q(j,k,1) )

                press_tmp = press(j,k)
                press(j,k) = pp
                call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                cli, cdi, cmi, clv, cdv, cmv)
                cdtp = cdi + cdv
                cltp = cli + clv

                objp = cdtp/cltp
                press(j,k) = press_tmp

                q(j,k,n) = tmp - stepsize
                pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                **2 )/q(j,k,1) )
                press_tmp = press(j,k)
                press(j,k) = pp
                call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                cli, cdi, cmi, clv, cdv, cmv)
                cdtp = cdi + cdv
                cltp = cli + clv

                objm = cdtp/cltp
                press(j,k) = press_tmp
                q(j,k,n) = tmp  

                dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                *stepsize)

              end do
            end do           
          end do

        end if
      else if ( obj_func.eq.6 ) then
c     -- drag minimization with composite objective function --

c     v1_n = 0.d0
c     do n=1,4
c     do k=1,kend
c     do j=1,jend
c     v1_n = v1_n + q(j,k,n)**2
c     end do
c     end do
c     end do
c     v1_n = dsqrt(v1_n)
c     fd_eta  = dsqrt(2.2d-16) / v1_n

         if (.not. viscous) then
            do n = 1,4
               do j = jtail1,jtail2
                  tmp = q(j,1,n)
                  stepsize = fd_eta*tmp

                  if ( dabs(stepsize) .lt. 1.d-9 ) then
                     stepsize = 1.d-9*dsign(1.d0,stepsize)
c                     write (opt_unit,20) 
c                     write (opt_unit,30) j, k, n, tmp, stepsize
                  end if

                  q(j,1,n) = q(j,1,n) + stepsize
                  stepsize = q(j,1,n) - tmp
                  pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
     &                 **2)/q(j,1,1) )
                  press_tmp = press(j,1)
                  press(j,1) = pp

                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cdtp = cdi + cdv
                  cltp = cli + clv

                  if (cdtp.gt.cd_tar) then
                     objp = wfd*( 1.d0 - cdtp/cd_tar )**2
                  else
                     objp = 0.0
                  end if
                  objp = objp + wfl*( 1.d0 - cltp/cl_tar )**2

                  press(j,1) = press_tmp
                  q(j,1,n) = tmp - stepsize
                  pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
     &                 **2)/q(j,1,1) )
                  press_tmp = press(j,1)
                  press(j,1) = pp

                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cdtp = cdi + cdv
                  cltp = cli + clv

                  if (cdtp.gt.cd_tar) then
                     objm = wfd*( 1.d0 - cdtp/cd_tar )**2
                  else
                     objm = 0.0
                  end if

                  objm = objm + wfl*( 1.d0 - cltp/cl_tar )**2

                  press(j,1) = press_tmp
                  q(j,1,n) = tmp  

                  dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
     &                 *stepsize)
               end do
            end do
         else
c     -- viscous flow --
            k=1
            do n = 1,4,3
               do j = jtail1,jtail2
                  tmp = q(j,k,n)
                  stepsize = fd_eta*tmp

                  if ( dabs(stepsize) .lt. 1.d-9 ) then
                     stepsize = 1.d-9*dsign(1.d0,stepsize)
c                     write (opt_unit,20) 
c                     write (opt_unit,30) j, k, n, tmp, stepsize
                  end if

                  q(j,k,n) = q(j,k,n) + stepsize
                  stepsize = q(j,k,n) - tmp
                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                 **2 )/q(j,k,1) )

                  press_tmp = press(j,k)
                  press(j,k) = pp
                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cdtp = cdi + cdv
                  cltp = cli + clv

                  if (cdtp.gt.cd_tar) then
                     objp = wfd*( 1.d0 - cdtp/cd_tar )**2
                  else
                     objp = 0.0
                  end if
                  objp = objp + wfl*( 1.d0 - cltp/cl_tar )**2
                  press(j,k) = press_tmp

                  q(j,k,n) = tmp - stepsize
                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                 **2 )/q(j,k,1) )
                  press_tmp = press(j,k)
                  press(j,k) = pp
                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cdtp = cdi + cdv
                  cltp = cli + clv

                  if (cdtp.gt.cd_tar) then
                     objm = wfd*( 1.d0 - cdtp/cd_tar )**2
                  else
                     objm = 0.0
                  end if
                  objm = objm + wfl*( 1.d0 - cltp/cl_tar )**2
                  press(j,k) = press_tmp
                  q(j,k,n) = tmp  

                  dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                 *stepsize)
               end do
            end do                       

            do n = 1,4
               do k = 2,3
                  do j = jtail1,jtail2
                     tmp = q(j,k,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
c                        write (opt_unit,20) 
c                        write (opt_unit,30) j, k, n, tmp, stepsize
                     end if

                     q(j,k,n) = q(j,k,n) + stepsize
                     stepsize = q(j,k,n) - tmp
                     pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
     &                    q(j,k,3)**2 )/q(j,k,1) )

                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                    cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv
                     
                     if (cdtp.gt.cd_tar) then
                        objp = wfd*( 1.d0 - cdtp/cd_tar )**2
                     else
                        objp = 0.0
                     end if
                     objp = objp + wfl*( 1.d0 - cltp/cl_tar )**2
                     press(j,k) = press_tmp

                     q(j,k,n) = tmp - stepsize
                     pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k
     &                    ,3)**2 )/q(j,k,1) )
                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                    cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv

                     if (cdtp.gt.cd_tar) then
                        objm = wfd*( 1.d0 - cdtp/cd_tar )**2
                     else
                        objm = 0.0
                     end if
                     objm = objm + wfl*( 1.d0 - cltp/cl_tar )**2
                     press(j,k) = press_tmp
                     q(j,k,n) = tmp  

                     dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                    *stepsize)

                  end do
               end do           
            end do

         end if 

      else if ( obj_func.eq.7 ) then
c     -- lift/moment with composite objective function --

c     v1_n = 0.d0
c     do n=1,4
c     do k=1,kend
c     do j=1,jend
c     v1_n = v1_n + q(j,k,n)**2
c     end do
c     end do
c     end do
c     v1_n = dsqrt(v1_n)
c     fd_eta  = dsqrt(2.2d-16) / v1_n

         if (.not. viscous) then
            do n = 1,4
               do j = jtail1,jtail2
                  tmp = q(j,1,n)
                  stepsize = fd_eta*tmp

                  if ( dabs(stepsize) .lt. 1.d-9 ) then
                     stepsize = 1.d-9*dsign(1.d0,stepsize)
c                     write (opt_unit,20) 
c                     write (opt_unit,30) j, k, n, tmp, stepsize
                  end if

                  q(j,1,n) = q(j,1,n) + stepsize
                  stepsize = q(j,1,n) - tmp
                  pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
     &                 **2)/q(j,1,1) )
                  press_tmp = press(j,1)
                  press(j,1) = pp

                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cmtp = cmi + cmv
                  cltp = cli + clv

c                  if (cdtp.gt.cd_tar) then
                     objp = wfd*( 1.d0 - cmtp/cd_tar )**2
c                  else
c                     objp = 0.0
c                  end if
                  objp = objp + wfl*( 1.d0 - cltp/cl_tar )**2

                  press(j,1) = press_tmp
                  q(j,1,n) = tmp - stepsize
                  pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
     &                 **2)/q(j,1,1) )
                  press_tmp = press(j,1)
                  press(j,1) = pp

                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cmtp = cmi + cmv
                  cltp = cli + clv

c                  if (cdtp.gt.cd_tar) then
                     objm = wfd*( 1.d0 - cmtp/cd_tar )**2
c                  else
c                     objm = 0.0
c                  end if

                  objm = objm + wfl*( 1.d0 - cltp/cl_tar )**2

                  press(j,1) = press_tmp
                  q(j,1,n) = tmp  

                  dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
     &                 *stepsize)
               end do
            end do
         else
c     -- viscous flow --
            k=1
            do n = 1,4,3
               do j = jtail1,jtail2
                  tmp = q(j,k,n)
                  stepsize = fd_eta*tmp

                  if ( dabs(stepsize) .lt. 1.d-9 ) then
                     stepsize = 1.d-9*dsign(1.d0,stepsize)
c                     write (opt_unit,20) 
c                     write (opt_unit,30) j, k, n, tmp, stepsize
                  end if

                  q(j,k,n) = q(j,k,n) + stepsize
                  stepsize = q(j,k,n) - tmp
                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                 **2 )/q(j,k,1) )

                  press_tmp = press(j,k)
                  press(j,k) = pp
                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cmtp = cmi + cmv
                  cltp = cli + clv

c                  if (cdtp.gt.cd_tar) then
                     objp = wfd*( 1.d0 - cmtp/cd_tar )**2
c                  else
c                     objp = 0.0
c                  end if
                  objp = objp + wfl*( 1.d0 - cltp/cl_tar )**2
                  press(j,k) = press_tmp

                  q(j,k,n) = tmp - stepsize
                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
     &                 **2 )/q(j,k,1) )
                  press_tmp = press(j,k)
                  press(j,k) = pp
                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                  cmtp = cmi + cmv
                  cltp = cli + clv

c                  if (cdtp.gt.cd_tar) then
                     objm = wfd*( 1.d0 - cmtp/cd_tar )**2
c                  else
c                     objm = 0.0
c                  end if
                  objm = objm + wfl*( 1.d0 - cltp/cl_tar )**2
                  press(j,k) = press_tmp
                  q(j,k,n) = tmp  

                  dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                 *stepsize)
               end do
            end do                       

            do n = 1,4
               do k = 2,3
                  do j = jtail1,jtail2
                     tmp = q(j,k,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
c                        write (opt_unit,20) 
c                        write (opt_unit,30) j, k, n, tmp, stepsize
                     end if

                     q(j,k,n) = q(j,k,n) + stepsize
                     stepsize = q(j,k,n) - tmp
                     pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
     &                    q(j,k,3)**2 )/q(j,k,1) )

                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                    cli, cdi, cmi, clv, cdv, cmv)
                     cmtp = cmi + cmv
                     cltp = cli + clv
                     
c                     if (cdtp.gt.cd_tar) then
                     objp = wfd*( 1.d0 - cmtp/cd_tar )**2
c                     else
c                        objp = 0.0
c                     end if
                     objp = objp + wfl*( 1.d0 - cltp/cl_tar )**2
                     press(j,k) = press_tmp

                     q(j,k,n) = tmp - stepsize
                     pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k
     &                    ,3)**2 )/q(j,k,1) )
                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                    cli, cdi, cmi, clv, cdv, cmv)
                     cmtp = cmi + cmv
                     cltp = cli + clv

c                     if (cdtp.gt.cd_tar) then
                     objm = wfd*( 1.d0 - cmtp/cd_tar )**2
c                     else
c                        objm = 0.0
c                     end if
                     objm = objm + wfl*( 1.d0 - cltp/cl_tar )**2
                     press(j,k) = press_tmp
                     q(j,k,n) = tmp  

                     dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                    *stepsize)

                  end do
               end do           
            end do

         end if 

      else if(obj_func.eq.8.or.(obj_func.eq.19.and.lcdm_grad.eq.1)) then

      !-- If OBJ_FUNC=8 then drag minimization with variable angle of 
      !-- attack --
      !-- If OBJ_FUNC=19 for lift-constrained drag min, then 2
      !-- gradients must be evaluated; 1 for the drag objective and
      !-- 1 for the lift constraint.

c     v1_n = 0.d0
c     do n=1,4
c     do k=1,kend
c     do j=1,jend
c     v1_n = v1_n + q(j,k,n)**2
c     end do
c     end do
c     end do
c     v1_n = dsqrt(v1_n)
c     fd_eta  = dsqrt(2.2d-16) / v1_n

clb   Finite difference calculation of dJ/dQ, for objective 8.  Analytic
clb   calculation is used (below).
clb         if (.not. viscous) then
clb            do n = 1,4
clb               do j = jtail1,jtail2
clb                  tmp = q(j,1,n)
clb                  stepsize = fd_eta*tmp
clb
clb                  if ( dabs(stepsize) .lt. 1.d-9 ) then
clb                     stepsize = 1.d-9*dsign(1.d0,stepsize)
clbc                     write (opt_unit,20) 
clbc                     write (opt_unit,30) j, k, n, tmp, stepsize
clb                  end if
clb
clb                  q(j,1,n) = q(j,1,n) + stepsize
clb                  stepsize = q(j,1,n) - tmp
clb                  pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
clb     &                 **2)/q(j,1,1) )
clb                  press_tmp = press(j,1)
clb                  press(j,1) = pp
clb
clb                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
clb     &                 cli, cdi, cmi, clv, cdv, cmv)
clb                  cdtp = cdi + cdv
clb                  cltp = cli + clv
clb                  objp = cdtp
clb
clb                  press(j,1) = press_tmp
clb                  q(j,1,n) = tmp - stepsize
clb                  pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
clb     &                 **2)/q(j,1,1) )
clb                  press_tmp = press(j,1)
clb                  press(j,1) = pp
clb
clb                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
clb     &                 cli, cdi, cmi, clv, cdv, cmv)
clb                  cdtm = cdi + cdv
clb                  cltm = cli + clv
clb                  objm = cdtm
clb
clb                  press(j,1) = press_tmp
clb                  q(j,1,n) = tmp  
clb
clbclb                  dOdQ(j,1,n) = ( objp - objm ) / (2.d0
clbclb     &                 *stepsize)
clbclb                  dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
clbclb     &                 *stepsize)
clb                  dcddqFD(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
clb     &                 *stepsize)
clb               end do
clb            end do
clb         else
clbc     -- viscous flow --
clb            k=1
clb            do n = 1,4!,3
clb               do j = jtail1,jtail2
clb                  tmp = q(j,k,n)
clb                  stepsize = fd_eta*tmp
clb
clb                  if ( dabs(stepsize) .lt. 1.d-9 ) then
clb                     stepsize = 1.d-9*dsign(1.d0,stepsize)
clbc                     write (opt_unit,20) 
clbc                     write (opt_unit,30) j, k, n, tmp, stepsize
clb                  end if
clb
clb                  q(j,k,n) = q(j,k,n) + stepsize
clb                  stepsize = q(j,k,n) - tmp
clb                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
clb     &                 **2 )/q(j,k,1) )
clb
clb                  press_tmp = press(j,k)
clb                  press(j,k) = pp
clb                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
clb     &                 cli, cdi, cmi, clv, cdv, cmv)
clb                  cdtp = cdi + cdv
clb                  cltp = cli + clv
clbclb                  write(*,*) 'dobjdq cltp', cltp
clb
clb                  objp = cdtp
clb                  press(j,k) = press_tmp
clb
clb                  q(j,k,n) = tmp - stepsize
clb                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
clb     &                 **2 )/q(j,k,1) )
clb                  press_tmp = press(j,k)
clb                  press(j,k) = pp
clb                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
clb     &                 cli, cdi, cmi, clv, cdv, cmv)
clb                  cdtm = cdi + cdv
clb                  cltm = cli + clv
clbclb                  write(*,*) 'dobjdq cltm', cltm
clb
clb                  objm = cdtm
clb                  press(j,k) = press_tmp
clb                  q(j,k,n) = tmp  
clb
clbclb                  dcddqFD(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
clbclb     &                 *stepsize)
clb                  dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
clb     &                 *stepsize)
clb               end do
clb            end do                       
clb
clb            do n = 1,4
clb               do k = 2,3
clb                  do j = jtail1,jtail2
clb                     tmp = q(j,k,n)
clb                     stepsize = fd_eta*tmp
clb
clb                     if ( dabs(stepsize) .lt. 1.d-9 ) then
clb                        stepsize = 1.d-9*dsign(1.d0,stepsize)
clbc                        write (opt_unit,20) 
clbc                        write (opt_unit,30) j, k, n, tmp, stepsize
clb                     end if
clb
clb                     q(j,k,n) = q(j,k,n) + stepsize
clb                     stepsize = q(j,k,n) - tmp
clb                     pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
clb     &                    q(j,k,3)**2 )/q(j,k,1) )
clb
clb                     press_tmp = press(j,k)
clb                     press(j,k) = pp
clb                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
clb     &                    cli, cdi, cmi, clv, cdv, cmv)
clb                     cdtp = cdi + cdv
clb                     cltp = cli + clv
clbclb                     write(*,*) 'dobjdq cltp', cltp
clb
clb                     objp = cdtp
clb                     press(j,k) = press_tmp
clb
clb                     q(j,k,n) = tmp - stepsize
clb                     pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 
clb     &                    + q(j,k,3)**2 )/q(j,k,1) )
clb                     press_tmp = press(j,k)
clb                     press(j,k) = pp
clb                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
clb     &                    cli, cdi, cmi, clv, cdv, cmv)
clb                     cdtm = cdi + cdv
clb                     cltm = cli + clv
clbclb                     write(*,*) 'dobjdq cltm', cltm
clb                  
clb                     objm = cdtm
clb                     press(j,k) = press_tmp
clb                     q(j,k,n) = tmp  
clb
clbclb                     dcddqFD(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
clbclb     &                    *stepsize)
clb                     dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
clb     &                    *stepsize)
clb
clb                  end do
clb               end do           
clb            end do
clb         end if

clb   ANALYTICAL CALCULATION OF dobjdq
         nscal = 1
clb         if( nscal .eq.0) then
clb            do n = 1,4
clb               do k = 1,kend
clb                  do j = 1,jend
clb                     qtemp(j,k,n) = q(j,k,n)
clb                     q(j,k,n) = q(j,k,n)*xyj(j,k)
clbclb               this gives q without the jacobian xyj.  This is Q instead of Q^.  
clb                  end do
clb               end do
clb            end do
clb         end if
         
      
c                        
         cpc = 2.d0/(gamma*fsmach**2)                             
c     
         do j=1,jdim
            do n=1,4
               dpdq(j,n)=0.d0
               dcndq(j,n)=0.d0
               dccdq(j,n)=0.d0
               dcdidq(j,n)=0.d0
               do k=1,kdim
                  dcnvdq(j,k,n)=0.d0
                  dccvdq(j,k,n)=0.d0
                  dcdvvdq(j,k,n)=0.d0
                  
                  dcfavjm1dq(j,k,n) = 0.d0
                  dcfavjdq(j,k,n) = 0.d0
                  dcfavjp1dq(j,k,n) = 0.d0
                  dcfavjp2dq(j,k,n) = 0.d0
               end do
            end do
         end do
c                                                                       
c  set limits of integration                                            
c                                                                       
         j1 = jlow                                                         
         j2 = jup                                                          
         jtp = jlow                                                        
         if(.not.periodic)then                                             
            j1 = jtail1                                                       
            j2 = jtail2                                                       
            jtp = jtail1+1                                                    
         endif                                                             
c                                                                       
c     compute cp at grid points and store in z array     
c                                                                       
         do j=j1,j2                                                     
clb      dpdq is the derivative of p wrt q.  Second index is n.
            dpdq(j,1)=0.5*gami*(q(j,1,2)**2+q(j,1,3)**2)
     &           /q(j,1,1)**2
            dpdq(j,2)=-gami*q(j,1,2)/q(j,1,1)
            dpdq(j,3)=-gami*q(j,1,3)/q(j,1,1)
            dpdq(j,4)=gami
               
            do n=1,4
               dzdq(j,n)=dpdq(j,n)*gamma*cpc
            end do
         end do

c                                                                       
c     compute normal force coefficient and chord directed force coeff     
c     chord taken as one in all cases
c     
c                                                                       
c      cn = 0.                                                           
c      cc = 0.                                                           
c    
c     For periodic flows:
c     -going from interval contained between (jmax,1), then (1,2)
c      and ending on (jmax-1,jmax). Note: jlow=1,jup=jmax,jmax=jmaxold-1
c
         do j=jtp,j2-1                                                    
c        jm1 = jminus(j)                                                
c        cpav = (z(j) + z(jm1))*.5   
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*((x(j,1)-x(j-1,1))
     &              +(x(j+1,1)-x(j,1)))                         
               dccdq(j,n) = dzdq(j,n)*0.5*((y(j,1)-y(j-1,1))
     &              +(y(j+1,1)-y(j,1))) 
               dcdidq(j,n) = sin(alpha*pi/180.d0)*dcndq(j,n)
     &              +cos(alpha*pi/180.d0)*dccdq(j,n)
            end do
c        cmle = cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))    
c
c        Tornado does it this way.
c         cmle = cmle + cpav*
c     &    (  (x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1)) +                  
c     &       (y(j,1)+y(jm1,1))*.5*(y(j,1) -y(jm1,1))   )                
c
         end do
         j=j1
         do n=1,4
            dcndq(j,n) = -dzdq(j,n)*0.5*(x(j+1,1)-x(j,1))
            dccdq(j,n) = dzdq(j,n)*0.5*(y(j+1,1)-y(j,1))
            dcdidq(j,n) = sin(alpha*pi/180.d0)*dcndq(j,n)
     &           +cos(alpha*pi/180.d0)*dccdq(j,n)
         end do
         j=j2
         do n=1,4
            dcndq(j,n) = -dzdq(j,n)*0.5*(x(j,1)-x(j-1,1))
            dccdq(j,n) = dzdq(j,n)*0.5*(y(j,1)-y(j-1,1))
            dcdidq(j,n) = sin(alpha*pi/180.d0)*dcndq(j,n)
     &           +cos(alpha*pi/180.d0)*dccdq(j,n)
         end do
         
                                                                       
         if (viscous) then                                                 
c        write (*,*)  ' in viscous'
c                                                                       
c     viscous coefficent of friction calculation                           
c     taken from p. buning                                               
c                                                                       
c     calculate the skin friction coefficient                             
c                                                                       
c      c  = tau   /            2                                        
c       f      w / 1/2*rho   *u                                         
c                         inf  inf                                      
c                                                                       
c      tau  = mu*(du/dy-dv/dx)                                          
c         w                   w                                         
c                                                                       
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc., 
c     york, 1974, p. 50, but use freestream values instead of edge values.
c                                                                       
c                                                                       
c     for calculating cf, we need the coefficient of viscosity, mu.  use 
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1, rhoinf=1.  
c                                                                       
            cinf  = 1.                                                        
            alngth= 1.                                                        
c     re already has fsmach scaling                                       
            amu   = rhoinf*alngth/re                                          
            uinf2 = fsmach**2                                                 
c     
            k = 1                                                              
            ja = jlow                                                         
            jb = jup                                                          
c                                                                       
            if(.not.periodic)then 

clb   ********************************************************************                                         
               j = jtail1                                                  
               jp1 = j+1
               
               xixj = xy(j,k,1)                                             
               xiyj = xy(j,k,2)                                             
               etaxj = xy(j,k,3)                                            
               etayj = xy(j,k,4)
               xixjp1 = xy(jp1,k,1)
               xiyjp1 = xy(jp1,k,2)
               
               dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +q(j,k,2)/(q(j,k,1)**2)*xiyj
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -q(j,k,3)/(q(j,k,1)**2)*xixj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
               dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
               
               dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.d0/q(j,k,1)*xiyj
     &              -1.5d0/q(j,k,1)*etayj
               dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
               dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.d0/q(j,k,1)*xixj
     &              +1.5d0/q(j,k,1)*etaxj
               dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
                  
               dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj

               dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
               dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
               dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
               dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
               dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ********************************************************************

               j=jtail1+1
               jp1 = j+1
               jm1 = j-1
               
               xixjm1 = xy(jm1,k,1)
               xiyjm1 = xy(jm1,k,2)
               xixj = xy(j,k,1)                                             
               xiyj = xy(j,k,2)                                             
               etaxj = xy(j,k,3)                                            
               etayj = xy(j,k,4)
               xixjp1 = xy(jp1,k,1)
               xiyjp1 = xy(jp1,k,2)
                  
               dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +q(j,k,3)/(q(j,k,1)**2)*xixjm1
               dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
               dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
                  
               dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +1.d0/q(j,k,1)*xiyjm1
               dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
               dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
               dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &              -1.d0/q(j,k,1)*xixjm1
               dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.5d0/q(j,k,1)*etaxj
               dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
               
               dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
               dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
               dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
               dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
               dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
               dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
               dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj


clb   ********************************************************************     
               j = jtail2-1 
               jp1 = j+1
               jm1 = j-1
               
               xixjm1 = xy(jm1,k,1)
               xiyjm1 = xy(jm1,k,2)
               xixj = xy(j,k,1)                                             
               xiyj = xy(j,k,2)                                             
               etaxj = xy(j,k,3)                                            
               etayj = xy(j,k,4)
               xixjp1 = xy(jp1,k,1)
               xiyjp1 = xy(jp1,k,2)
               
               dcfavjp1dq(j,k,1) = q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
               dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               
               dcfavjp1dq(j,k,2) = -1.d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
               dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
               dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
               
               dcfavjp1dq(j,k,3) = 1.d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
               dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj-.5d0/q(j,k,1)
     &              *xixjm1
               dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
               
               dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
               dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
               dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
               dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
               dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
               dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
               dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj

clb   ********************************************************************

               j = jtail2                                                  
               jp1 = j+1
               jm1 = j-1
               
               xixjm1 = xy(jm1,k,1)
               xiyjm1 = xy(jm1,k,2)
               xixj = xy(j,k,1)                                             
               xiyj = xy(j,k,2)                                             
               etaxj = xy(j,k,3)                                            
               etayj = xy(j,k,4)
               xixjp1 = xy(jp1,k,1)
               xiyjp1 = xy(jp1,k,2)
               
               dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               dcfavjdq(j,k,1) = -q(j,k,2)/(q(j,k,1)**2)*xiyj
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              +q(j,k,3)/(q(j,k,1)**2)*xixj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               
               dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
               dcfavjdq(j,k,2) = 1.d0/q(j,k,1)*xiyj
     &              -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
               
               dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
               dcfavjdq(j,k,3) = -1.d0/q(j,k,1)*xixj
     &              +1.5d0/q(j,k,1)*etaxj
     &              -.5d0/q(j,k,1)*xixjm1
                  
               dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
               dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/q(j,k+2,1)**2*etaxj
               dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ********************************************************************                            
c                                                                       
c       -set new limits                                                       
               ja = jtail1+2                                               
               jb = jtail2-2                                               
            endif                                                          
c     
            do j = ja,jb                                            
               jp1 = j+1                                            
               jm1 = j-1 
               jm2 = j-2

               xixjm1 = xy(jm1,k,1)
               xiyjm1 = xy(jm1,k,2)
               xixj = xy(j,k,1)                                             
               xiyj = xy(j,k,2)                                             
               etaxj = xy(j,k,3)                                            
               etayj = xy(j,k,4)
               xixjp1 = xy(jp1,k,1)
               xiyjp1 = xy(jp1,k,2)
               
               dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
               dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
               
               dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
               dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
               dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
               dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
               dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
               dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &              -.5d0/q(j,k,1)*xixjm1
               dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.5d0/q(j,k,1)*etaxj
               dcfavjp2dq(j,k,3) = -.5d0/q(j,k,1)*xixjp1
               
               dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
               dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
               dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
               dcfavjdq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               dcfavjp1dq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
               dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
               dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
               dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
        

c                                                                       
            end do
c      write (*,*) 'done 110'
clb      if (sngvalte) then
clbc       -average trailing edge value
clb        z(j1)=.5d0*(z(j1)+z(j2))
clb        z(j2)=z(j1)
clb      endif
c                                                                       
c     Compute viscous normal and axial forces                             
c                                                                       
c     -compute normal force coefficient and chord directed force coeff     
c     -chord taken as one in all cases                                      
c                                                                       
                      
            do j=j1,j2
               do n = 1,3
                  do k = 1,3
                     dcnvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                    dcfavjm1dq(j,k,n)*(y(j-1,1)-y(j-2,1))
     &                    + dcfavjdq(j,k,n)*(y(j,1)-y(j-1,1))
     &                    + dcfavjp1dq(j,k,n)*(y(j+1,1)-y(j,1))
     &                    + dcfavjp2dq(j,k,n)*(y(j+2,1)-y(j+1,1)))
                     dccvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                    dcfavjm1dq(j,k,n)*(x(j-1,1)-x(j-2,1))
     &                    + dcfavjdq(j,k,n)*(x(j,1)-x(j-1,1))
     &                    + dcfavjp1dq(j,k,n)*(x(j+1,1)-x(j,1))
     &                    + dcfavjp2dq(j,k,n)*(x(j+2,1)-x(j+1,1)))
                     dcdvvdq(j,k,n)=dcnvdq(j,k,n)*sin(alpha*pi/180.d0)
     &                    +dccvdq(j,k,n)*cos(alpha*pi/180.d0)
                  end do
               end do
            end do
         end if
               
         do j=j1,j2                                                   
            do k=1,3
               do n=1,4
                  if (k .eq. 1) then
                     dOdQ(j,k,n) = dcdidq(j,n)
                     if (viscous) dOdQ(j,k,n) = dOdQ(j,k,n) 
     &                    + dcdvvdq(j,k,n)
                  else
                     if (viscous) then
                        dOdQ(j,k,n) = dcdvvdq(j,k,n)
                     else 
                        dOdQ(j,k,n) = 0.d0
                     endif
                  end if
                  dOdQ(j,k,n) = dOdQ(j,k,n)*xyj(j,k)
               end do
            end do
         end do

      else if(obj_func.eq.15.or.(obj_func.eq.19.and.lcdm_grad.eq.2))then
     
      !-- If OBJ_FUNC=15 then J = Cl 
      !-- 
      !-- If OBJ_FUNC=19 for lift-constrained drag min, then 2
      !-- gradients must be evaluated; 1 for the drag objective and
      !-- 1 for the lift constraint.

         diff_flag = 1

         if (diff_flag.eq.0) then

            !-- Finite difference calculation of dJ/dQ for objective 15. 
            !-- This is useful to verify the analytic derivative 
            !-- calculation that follows
         
            if (.not. viscous) then
               do n = 1,4
                  do j = jtail1,jtail2
                     tmp = q(j,1,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
c                        write (opt_unit,20) 
c                        write (opt_unit,30) j, k, n, tmp, stepsize
                     end if

                     q(j,1,n) = q(j,1,n) + stepsize
                     stepsize = q(j,1,n) - tmp
                     pp = gami*( q(j,1,4) 
     &               - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2)/q(j,1,1) )
                     press_tmp = press(j,1)
                     press(j,1) = pp

                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv
                     objp = cltp

                     press(j,1) = press_tmp
                     q(j,1,n) = tmp - stepsize
                     pp = gami*( q(j,1,4) 
     &                - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2)/q(j,1,1) )
                     press_tmp = press(j,1)
                     press(j,1) = pp

                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtm = cdi + cdv
                     cltm = cli + clv
                     objm = cltm

                     press(j,1) = press_tmp
                     q(j,1,n) = tmp  

clb                     dOdQ(j,1,n) = ( objp - objm ) / (2.d0
clb     &                    *stepsize)
clb                     dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
clb     &                    *stepsize)
                     dcddqFD(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
     &                 *stepsize)
                  end do
               end do
            else
c     -- viscous flow --
               k=1
               do n = 1,4!,3
                  do j = jtail1,jtail2
                     tmp = q(j,k,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
c                        write (opt_unit,20) 
c                        write (opt_unit,30) j, k, n, tmp, stepsize
                     end if

                     q(j,k,n) = q(j,k,n) + stepsize
                     stepsize = q(j,k,n) - tmp
                     pp = gami*( q(j,k,4) 
     &                - 0.5d0*( q(j,k,2)**2 + q(j,k,3)**2 )/q(j,k,1) )

                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv
clb                  write(*,*) 'dobjdq cltp', cltp

                     objp = cltp
                     press(j,k) = press_tmp

                     q(j,k,n) = tmp - stepsize
                     pp = gami*( q(j,k,4) 
     &                - 0.5d0*( q(j,k,2)**2 + q(j,k,3)**2 )/q(j,k,1) )
                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtm = cdi + cdv
                     cltm = cli + clv
clb                     write(*,*) 'dobjdq cltm', cltm

                     objm = cltm
                     press(j,k) = press_tmp
                     q(j,k,n) = tmp  

clb                     dcddqFD(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
clb     &                 *stepsize)
                     dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                 *stepsize)
                  end do
               end do                       

               do n = 1,4
                  do k = 2,3
                     do j = jtail1,jtail2
                        tmp = q(j,k,n)
                        stepsize = fd_eta*tmp

                        if ( dabs(stepsize) .lt. 1.d-9 ) then
                           stepsize = 1.d-9*dsign(1.d0,stepsize)
c                           write (opt_unit,20) 
c                           write (opt_unit,30) j, k, n, tmp, stepsize
                        end if

                        q(j,k,n) = q(j,k,n) + stepsize
                        stepsize = q(j,k,n) - tmp
                        pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
     &                    q(j,k,3)**2 )/q(j,k,1) )

                        press_tmp = press(j,k)
                        press(j,k) = pp
                        call clcd 
     &                  (jdim, kdim, q, press, x, y, xy, xyj, 1,      
     &                    cli, cdi, cmi, clv, cdv, cmv)
                        cdtp = cdi + cdv
                        cltp = cli + clv
clb                        write(*,*) 'dobjdq cltp', cltp

                        objp = cltp
                        press(j,k) = press_tmp

                        q(j,k,n) = tmp - stepsize
                        pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 
     &                    + q(j,k,3)**2 )/q(j,k,1) )
                        press_tmp = press(j,k)
                        press(j,k) = pp
                        call clcd 
     &                  (jdim, kdim, q, press, x, y, xy, xyj, 1,  
     &                    cli, cdi, cmi, clv, cdv, cmv)
                        cdtm = cdi + cdv
                        cltm = cli + clv
clb                        write(*,*) 'dobjdq cltm', cltm
                  
                        objm = cltm
                        press(j,k) = press_tmp
                        q(j,k,n) = tmp  

clb                     dcddqFD(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
clb     &                    *stepsize)
                        dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                    *stepsize)

                     end do
                  end do           
               end do
            end if

         else if (diff_flag.eq.1) then

            !-- Analytic derivative calculation of dJ/dQ for off-design 
            !-- constraint function J = Lift

            nscal = 1
                        
            cpc = 2.d0/(gamma*fsmach**2)                             
     
            do j=1,jdim
               do n=1,4
                  dpdq(j,n)=0.d0
                  dcndq(j,n)=0.d0
                  dccdq(j,n)=0.d0
                  dclidq(j,n)=0.d0
                  do k=1,kdim
                     dcnvdq(j,k,n)=0.d0
                     dccvdq(j,k,n)=0.d0
                     dcdvvdq(j,k,n)=0.d0
                  
                     dcfavjm1dq(j,k,n) = 0.d0
                     dcfavjdq(j,k,n) = 0.d0
                     dcfavjp1dq(j,k,n) = 0.d0
                     dcfavjp2dq(j,k,n) = 0.d0
                  end do
               end do
            end do
c                                                                       
c  set limits of integration                                            
c                                                                       
            j1 = jlow
            j2 = jup  
            jtp = jlow
            if(.not.periodic)then
               j1 = jtail1
               j2 = jtail2
               jtp = jtail1+1
            endif
c 
c        compute cp at grid points and store in z array     
c                                                                       
            do j=j1,j2 
clb         dpdq is the derivative of p wrt q.  Second index is n.
               dpdq(j,1)=0.5*gami*(q(j,1,2)**2+q(j,1,3)**2)
     &           /q(j,1,1)**2
               dpdq(j,2)=-gami*q(j,1,2)/q(j,1,1)
               dpdq(j,3)=-gami*q(j,1,3)/q(j,1,1)
               dpdq(j,4)=gami
               
               do n=1,4
                  dzdq(j,n)=dpdq(j,n)*gamma*cpc
               end do
            end do

c                                                                       
c     compute normal force coefficient and chord directed force coeff
c     chord taken as one in all cases
c     
c                                                                       
c      cn = 0.                                                           
c      cc = 0.                                                           
c    
c     For periodic flows:
c     -going from interval contained between (jmax,1), then (1,2)
c      and ending on (jmax-1,jmax). Note: jlow=1,jup=jmax,jmax=jmaxold-1
c
            do j=jtp,j2-1 
c        jm1 = jminus(j)                                                
c        cpav = (z(j) + z(jm1))*.5   
               do n=1,4
                  dcndq(j,n) = -dzdq(j,n)*0.5*((x(j,1)-x(j-1,1))
     &                 +(x(j+1,1)-x(j,1)))                         
                  dccdq(j,n) = dzdq(j,n)*0.5*((y(j,1)-y(j-1,1))
     &                 +(y(j+1,1)-y(j,1))) 
                  dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &                 -sin(alpha*pi/180.d0)*dccdq(j,n)
               end do
c        cmle = cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))    
c
c        Tornado does it this way.
c         cmle = cmle + cpav*
c     &    (  (x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1)) +                  
c     &       (y(j,1)+y(jm1,1))*.5*(y(j,1) -y(jm1,1))   )                
c
            end do
            j=j1
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*(x(j+1,1)-x(j,1))
               dccdq(j,n) = dzdq(j,n)*0.5*(y(j+1,1)-y(j,1))
               dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &           -sin(alpha*pi/180.d0)*dccdq(j,n)
            end do
            j=j2
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*(x(j,1)-x(j-1,1))
               dccdq(j,n) = dzdq(j,n)*0.5*(y(j,1)-y(j-1,1))
               dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &           -sin(alpha*pi/180.d0)*dccdq(j,n)
            end do
         
                                                                       
            if (viscous) then
c        write (*,*)  ' in viscous'
c                                                                       
c     viscous coefficent of friction calculation 
c     taken from p. buning                                               
c                                                                       
c     calculate the skin friction coefficient
c                                                                       
c      c  = tau   /            2                                        
c       f      w / 1/2*rho   *u                                         
c                         inf  inf                                      
c                                                                       
c      tau  = mu*(du/dy-dv/dx)                                          
c         w                   w                                         
c                                                                       
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc.,
c     york, 1974, p. 50, but use freestream values instead of edge values
c                                                                       
c                                                                       
c     for calculating cf, we need the coefficient of viscosity, mu.  use 
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1, rhoinf=1. 
c                                                                       
               cinf  = 1.               
               alngth= 1.         
c     re already has fsmach scaling                                   
               amu   = rhoinf*alngth/re                               
               uinf2 = fsmach**2          
               k = 1                         
               ja = jlow               
               jb = jup                             
                                                                       
               if(.not.periodic)then 

clb   *****************************************************************
                  j = jtail1                                           
                  jp1 = j+1
                  xixj = xy(j,k,1)       
                  xiyj = xy(j,k,2)                       
                  etaxj = xy(j,k,3)         
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
               
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +q(j,k,2)/(q(j,k,1)**2)*xiyj
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -q(j,k,3)/(q(j,k,1)**2)*xixj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
 
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.d0/q(j,k,1)*xiyj
     &              -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.d0/q(j,k,1)*xixj
     &              +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
                  
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj

                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ******************************************************************

                  j=jtail1+1
                  jp1 = j+1
                  jm1 = j-1
               
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)    
                  xiyj = xy(j,k,2)  
                  etaxj = xy(j,k,3)     
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
                  
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +1.d0/q(j,k,1)*xiyjm1
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &              -1.d0/q(j,k,1)*xixjm1
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
               
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj


clb   ******************************************************************
                  j = jtail2-1 
                  jp1 = j+1
                  jm1 = j-1
               
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)      
                  xiyj = xy(j,k,2)           
                  etaxj = xy(j,k,3)           
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
               
                  dcfavjp1dq(j,k,1) = q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               
                  dcfavjp1dq(j,k,2) = -1.d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
               
                  dcfavjp1dq(j,k,3) = 1.d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj-.5d0/q(j,k,1)
     &              *xixjm1
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
               
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj

clb   *****************************************************************

                  j = jtail2        
                  jp1 = j+1
                  jm1 = j-1
               
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)         
                  xiyj = xy(j,k,2)                      
                  etaxj = xy(j,k,3)                 
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
               
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjdq(j,k,1) = -q(j,k,2)/(q(j,k,1)**2)*xiyj
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              +q(j,k,3)/(q(j,k,1)**2)*xixj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  dcfavjdq(j,k,2) = 1.d0/q(j,k,1)*xiyj
     &              -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
               
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  dcfavjdq(j,k,3) = -1.d0/q(j,k,1)*xixj
     &              +1.5d0/q(j,k,1)*etaxj
     &              -.5d0/q(j,k,1)*xixjm1
                  
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/q(j,k+2,1)**2*etaxj
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ******************************************************************
c       -set new limits                                        
                  ja = jtail1+2                                    
                  jb = jtail2-2                                          
               endif          
c     
               do j = ja,jb                                            
                  jp1 = j+1                                            
                  jm1 = j-1 
                  jm2 = j-2

                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)  
                  xiyj = xy(j,k,2)             
                  etaxj = xy(j,k,3)  
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
               
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
               
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &              -.5d0/q(j,k,1)*xixjm1
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = -.5d0/q(j,k,1)*xixjp1
               
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
                  dcfavjdq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjp1dq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                                                   
               end do

c     Compute viscous normal and axial forces 
c     -compute normal force coefficient and chord directed force coeff 
c     -chord taken as one in all cases        
c                                                                       
                      
               do j=j1,j2
                  do n = 1,3
                     do k = 1,3
                       dcnvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                    dcfavjm1dq(j,k,n)*(y(j-1,1)-y(j-2,1))
     &                    + dcfavjdq(j,k,n)*(y(j,1)-y(j-1,1))
     &                    + dcfavjp1dq(j,k,n)*(y(j+1,1)-y(j,1))
     &                    + dcfavjp2dq(j,k,n)*(y(j+2,1)-y(j+1,1)))
                       dccvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                    dcfavjm1dq(j,k,n)*(x(j-1,1)-x(j-2,1))
     &                    + dcfavjdq(j,k,n)*(x(j,1)-x(j-1,1))
     &                    + dcfavjp1dq(j,k,n)*(x(j+1,1)-x(j,1))
     &                    + dcfavjp2dq(j,k,n)*(x(j+2,1)-x(j+1,1)))
                       dclvvdq(j,k,n)=dcnvdq(j,k,n)*cos(alpha*pi/180.d0)
     &                    -dccvdq(j,k,n)*sin(alpha*pi/180.d0)
                     end do
                  end do
               end do
            end if
         
         end if !(diff_flag.eq.0)
      
         do j=j1,j2                                                   
            do k=1,3
               do n=1,4
                  if (k .eq. 1) then
                     dOdQ(j,k,n) = dclidq(j,n)
                     if (viscous) dOdQ(j,k,n) = dOdQ(j,k,n) 
     &                    + dclvvdq(j,k,n)
                  else
                     if (viscous) then
                        dOdQ(j,k,n) = dclvvdq(j,k,n)
                     else 
                        dOdQ(j,k,n) = 0.d0
                     endif
                  end if
                  dOdQ(j,k,n) = dOdQ(j,k,n)*xyj(j,k)

                  !-- Get bounds on off-design constraint
                  if (off_flags(mp)) then
                     c_upp = c_upp1(mp)
                     c_low = c_low1(mp)
                  end if
                  !-- Scale by constraint bound
                  if (scale_con) dOdQ(j,k,n) = dOdQ(j,k,n)/c_low

               end do
            end do
         end do

      else if ( obj_func.eq.16 ) then

         diff_flag = 1
         if (diff_flag.eq.0) then

            !-- Finite difference calculation of dJ/dQ for objective 16,
            !-- J = maximum mach number in flow field

            !-- Find the coordinates of the node where the maximum Mach
            !-- number occurs (jMaxMach,kMaxMach) and calculate dJ/dQ
            !-- at only this location

            call getMaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)

            do n = 1,4

               k = kMaxMach
               j = jMaxMach
               tmp = q(j,k,n)
               stepsize = fd_eta*tmp

               if ( dabs(stepsize) .lt. 1.d-9 ) then
                  stepsize = 1.d-9*dsign(1.d0,stepsize)
               end if

               !-- Calc objective function value at +ve perturbed
               !-- flow solution

               q(j,k,n) = q(j,k,n) + stepsize
               stepsize = q(j,k,n) - tmp

               call getMaxMach
     &                 (q,jdim,kdim,ndim,MaxMachp,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)
 
               objp = MaxMachp

               !-- Calc objective function value at -ve perturbed
               !-- flow solution

               q(j,k,n) = tmp - stepsize

               call getMaxMach
     &                 (q,jdim,kdim,ndim,MaxMachm,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)

 
               objm = MaxMachm

               q(j,k,n) = tmp  
 
               !-- Calculate finite differenced gradient

               dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                    *stepsize)

               if ((dOdQ(j,k,n)).ne.0) then
                  write(scr_unit,*) 'dOdQ(j,k,n)=',dOdQ(j,k,n)
                  write(scr_unit,*) 'j,k,n =',j,k,n                
               end if
                      
            end do

         else if (diff_flag.eq.1) then

            !-- Analytic calculation of dJ/dQ, for objective 11,
            !-- J = maximum mach number in flow field

            !-- Find the coordinates of the node where the maximum Mach
            !-- number occurs (jMaxMach,kMaxMach) and calculate dJ/dQ
            !-- at only this location

            call getMaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)

            !-- Mach number = M(u,v,a), J = M, dJ/dQ = dM/dQ
            !-- dM/dQ = (dM/du)(du/dQ)+(dM/dv)(dv/dQ)+(dM/da)(da/dQ)

            !-- Calculate (dM/du) at node where max Mach number occurs

            dMdu = 
     &      (1/aMaxMach)*((uMaxmach)/(dsqrt(uMaxMach**2+vMaxMach**2)))

            !-- Calculate (du/dQ) at node where max Mach number occurs
            
            j=jMaxMach
            k=kMaxMach

            dudQ(1) = (-q(j,k,2))/(q(j,k,1)**2)
            dudQ(2) = 1/(q(j,k,1))
            dudQ(3) = 0
            dudQ(4) = 0

            !-- Calculate (dM/dv) at node where max Mach number occurs

            dMdv = 
     &      (1/aMaxMach)*((vMaxmach)/(dsqrt(uMaxMach**2+vMaxMach**2)))

            !-- Calculate (dv/dQ) at node where max Mach number occurs
            
            j=jMaxMach
            k=kMaxMach

            dvdQ(1) = (-q(j,k,3))/(q(j,k,1)**2)
            dvdQ(2) = 0
            dvdQ(3) = 1/(q(j,k,1))
            dvdQ(4) = 0

            !-- Calculate (dM/da) at node where max Mach number occurs
            
            dMda = 
     &      (-1/(aMaxMach**2))*(dsqrt(uMaxMach**2+vMaxMach**2))

            !-- Calculate (da/dQ) at node where max Mach number occurs
            !-- soundspeed = a(pressure,density) = a(P,q1)
            !-- da/dQ = (da/dP)(dP/dQ) + (da/dq1)(dq1/dQ)

            j=jMaxMach
            k=kMaxMach

            dadP = (0.5)*(1.4/q(j,k,1))*(1/aMaxMach)

            dPrdQ(1) = (0.2)*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1)**2
            dPrdQ(2) = (-0.4)*(q(j,k,2)/q(j,k,1))
            dPrdQ(3) = (-0.4)*(q(j,k,3)/q(j,k,1))
            dPrdQ(4) = 0.4

            dadq1 = ((-0.5)*(1.4)*pMaxMach)/((q(j,k,1)**2)*(aMaxMach))

            dq1dQ(1) = 1
            dq1dQ(2) = 0
            dq1dQ(3) = 0
            dq1dQ(4) = 0

            dadQ(1) = dadP*dPrdQ(1) + dadq1*dq1dQ(1)
            dadQ(2) = dadP*dPrdQ(2) + dadq1*dq1dQ(2)
            dadQ(3) = dadP*dPrdQ(3) + dadq1*dq1dQ(3)
            dadQ(4) = dadP*dPrdQ(4) + dadq1*dq1dQ(4)

            !-- Calculate (dM/dQ) at node where max Mach number occurs
            !-- dM/dQ = (dM/du)(du/dQ)+(dM/dv)(dv/dQ)+(dM/da)(da/dQ)

            dMdQ(1) = dMdu*dudQ(1) + dMdv*dvdQ(1) + dMda*dadQ(1)
            dMdQ(2) = dMdu*dudQ(2) + dMdv*dvdQ(2) + dMda*dadQ(2)
            dMdQ(3) = dMdu*dudQ(3) + dMdv*dvdQ(3) + dMda*dadQ(3)
            dMdQ(4) = dMdu*dudQ(4) + dMdv*dvdQ(4) + dMda*dadQ(4)

            j=jMaxMach
            k=kMaxMach

            dOdQ(j,k,1) = dMdQ(1)*xyj(j,k)
            dOdQ(j,k,2) = dMdQ(2)*xyj(j,k)
            dOdQ(j,k,3) = dMdQ(3)*xyj(j,k)
            dOdQ(j,k,4) = dMdQ(4)*xyj(j,k)

            do n=1,4
               write(scr_unit,*) 'dOdQ(j,k,n)=',dOdQ(j,k,n)
               write(scr_unit,*) 'j,k,n =',j,k,n
            end do
            
         end if

      else if ( obj_func.eq.17 ) then

         diff_flag = 1
         if (diff_flag.eq.0) then

            !-- Finite difference calculation of dJ/dQ, for objective 17,
            !-- J = maximum mach number in flow field estimated using
            !-- the KS function
  
            do j = jmstart, jmend, jminc !-- Increment j by jminc 

               do k = kmstart, kmend, kminc !-- Increment k by kminc

                  do n = 1,4

                     tmp = q(j,k,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
                     end if

                     !-- Calc objective function value at +ve perturbed
                     !-- flow solution

                     q(j,k,n) = q(j,k,n) + stepsize
                     stepsize = q(j,k,n) - tmp

                     call KSmaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,
     &                 kMaxMach,aMaxMach,uMaxMach,vMaxMach,pMaxMach,ks,
     &                 ksMachp)
 
                     objp = ksMachp

                     !-- Calc objective function value at -ve perturbed
                     !-- flow solution

                     q(j,k,n) = tmp - stepsize

                     call KSmaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,
     &                 kMaxMach,aMaxMach,uMaxMach,vMaxMach,pMaxMach,ks,
     &                 ksMachm)
 
                     objm = ksMachm

                     q(j,k,n) = tmp  
 
                     !-- Calculate finite differenced gradient

                     dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                    *stepsize)
                  end do
                  
               end do
               
            end do

         else if (diff_flag.eq.1) then

            !-- Analytic calculation of dJ/dQ, for objective 17,
            !-- J = maximum mach number in flow field estimated using
            !-- the KS function

            if (adptKS) then ! Add (dks/drho)(drho/dQ) to dksdQ

               !-- Calculate dks/drho at the nominal value of 
               !-- rhoKS (See KSmaxMach.f)

               rKSc = rhoKS

               call dksdr(jdim, kdim, ndim, rKSc, q, dksdrc, ksSum1c,
     &                     ksSum2c, ksSum3c)

               !-- Calculate dks/drho at the perturbed value of 
               !-- rhoKS (See KSmaxMach.f)

               rKS1 = rhoKS + 0.001

               call dksdr(jdim, kdim, ndim, rKS1, q, dksdr1, ksSum11,
     &                     ksSum21, ksSum31)

               !-- Calculate dks/drho at the value of rhoKS used
               !-- to calculate the KS function (See KSmaxMach.f)

               rKSd = rKS_used

               call dksdr(jdim, kdim, ndim, rKSd, q, dksdrd, ksSum1d,
     &                     ksSum2d, ksSum3d)
               !-- Calculate (drho/ddksdr1) (Matlab differentiation)

               drhoddksdrc = rKSd*(-1/dksdrc/log(dksdr1/dksdrc)
     &                      *log(rKS1/rKSc)/log(10.0)
     &                       +log(dksdrd/dksdrc)/log(dksdr1/dksdrc)**2
     &                        *log(rKS1/rKSc)/log(10.0)/dksdrc)

               !-- Calculate (drho/ddksdr1) (Matlab differentiation)

               drhoddksdr1 = -rKSd*log(dksdrd/dksdrc)/log(dksdr1/dksdrc)
     &                        **2*log(rKS1/rKSc)/log(10.0)/dksdr1

               !-- Calculate (ddksdrc/drho)

               ddksdrcdrho = (1/rKSc)*((ksSum3c/ksSum1c)
     &                       - (ksSum2c/ksSum1c)**2)
     &                        - (2/rKSc**2)*(ksSum2c/ksSum1c)
     &                         + (2/rKSc**3)*log(ksSum1c)

               !-- Calculate (ddksdr1/drho)

               ddksdr1drho = (1/rKS1)*((ksSum31/ksSum11)
     &                       - (ksSum21/ksSum11)**2)
     &                        - (2/rKS1**2)*(ksSum21/ksSum11)
     &                         + (2/rKS1**3)*log(ksSum11)

               !-- drhodQ_bot = 
               !--    1 - (drho/dksdrho)*(dksdrho/drho)
               !--        - (drho/dksdrho1)*(dksdrho1/drho)
                     
               drhodQ_bot = 1 - drhoddksdrc*ddksdrcdrho
     &                         - drhoddksdr1*ddksdr1drho

            end if

            do j = jmstart, jmend, jminc !-- Increment j by jminc 

               do k = kmstart, kmend, kminc !-- Increment k by kminc
                           
                  !-- KS estimate of max Mach = ksMach(ks), J = ksMach
                  !-- dJ/dQ = dksMach/dQ
                  !-- dksMach/dQ = (dksMach/dks)(dks/dQ)
                  dksMachdks = Mstar

                  !-- dks/dQ = (dks/dg)(dg/dQ)
                  !-- 
                  !-- OR
                  !--
                  !-- dks/dQ = (dks/dg)(dg/dQ) + (dks/drho)(drho/dQ) if
                  !-- using the Poon/Martins adaptive KS approach

                  !-- Evaluate Mach # at (j,k)
            
                  U = q(j,k,2)/q(j,k,1)
                  V = q(j,k,3)/q(j,k,1)

                  P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))
         
                  a = sqrt(1.4*P/q(j,k,1))

                  Mach = sqrt(U**2 + V**2)/a

                  !-- Calculate the value of constraint 'g' using 
                  !-- Mach # at (j,k)

                  g = Mach/Mstar  - 1

                  !-- Calculate numerator of dks/dg expression using
                  !-- 'gmax' previously calculated in KSmaxMach

                  dksNumer = exp(rKS_used*(g-gmax))

                  !-- Calculate (dks/dg) using 'dksDenom' previously
                  !-- calculated in KSmaxMach

                  dksdg = dksNumer/dksDenom

                  !-- dg/dQ = (dg/dM)(dM/dQ)

                  dgdM = 1/Mstar

                  !- Mach number = M(u,v,a)
                  !- dM/dQ = (dM/du)(du/dQ)+(dM/dv)(dv/dQ)+(dM/da)(da/dQ)

                  !-- Calculate (dM/du) 

                  dMdu = (1/a)*((U)/(dsqrt(U**2+V**2)))

                  !-- Calculate (du/dQ) 
            
                  dudQ(1) = (-q(j,k,2))/(q(j,k,1)**2)
                  dudQ(2) = 1/(q(j,k,1))
                  dudQ(3) = 0
                  dudQ(4) = 0

                  !-- Calculate (dM/dv) 

                  dMdv = (1/a)*((V)/(dsqrt(U**2+V**2)))

                  !-- Calculate (dvtop/dQ) 
            
                  dvdQ(1) = (-q(j,k,3))/(q(j,k,1)**2)
                  dvdQ(2) = 0
                  dvdQ(3) = 1/(q(j,k,1))
                  dvdQ(4) = 0

                  !-- Calculate (dM/da)
            
                  dMda = (-1/(a**2))*(dsqrt(U**2+V**2))

                  !-- Calculate (da/dQ) 
                  !-- soundspeed = a(pressure,density) = a(P,q1)
                  !-- da/dQ = (da/dP)(dP/dQ) + (da/dq1)(dq1/dQ)

                  dadP = (0.5)*(1.4/q(j,k,1))*(1/a)

                  dPrdQ(1) = 
     &                   (0.2)*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1)**2
                  dPrdQ(2) = (-0.4)*(q(j,k,2)/q(j,k,1))
                  dPrdQ(3) = (-0.4)*(q(j,k,3)/q(j,k,1))
                  dPrdQ(4) = 0.4

                  dadq1 = 
     &               ((-0.5)*(1.4)*P)/((q(j,k,1)**2)*(a))

                  dq1dQ(1) = 1
                  dq1dQ(2) = 0
                  dq1dQ(3) = 0
                  dq1dQ(4) = 0

                  dadQ(1) = dadP*dPrdQ(1) + dadq1*dq1dQ(1)
                  dadQ(2) = dadP*dPrdQ(2) + dadq1*dq1dQ(2)
                  dadQ(3) = dadP*dPrdQ(3) + dadq1*dq1dQ(3)
                  dadQ(4) = dadP*dPrdQ(4) + dadq1*dq1dQ(4)

                  !- Calculate (dM/dQ)             
                  !- dM/dQ = (dM/du)(du/dQ)+(dM/dv)(dv/dQ)+(dM/da)(da/dQ)

                  dMdQ(1) = dMdu*dudQ(1) + dMdv*dvdQ(1) + dMda*dadQ(1)
                  dMdQ(2) = dMdu*dudQ(2) + dMdv*dvdQ(2) + dMda*dadQ(2)
                  dMdQ(3) = dMdu*dudQ(3) + dMdv*dvdQ(3) + dMda*dadQ(3)
                  dMdQ(4) = dMdu*dudQ(4) + dMdv*dvdQ(4) + dMda*dadQ(4)

                  !-- Calculate (dg/dQ)
                 
                  dgdQ(1) = dgdM*dMdQ(1)
                  dgdQ(2) = dgdM*dMdQ(2)
                  dgdQ(3) = dgdM*dMdQ(3)
                  dgdQ(4) = dgdM*dMdQ(4)

                  !-- Calculate (dks/dQ)

                  dksdQ(1) = dksdg*dgdQ(1)
                  dksdQ(2) = dksdg*dgdQ(2)
                  dksdQ(3) = dksdg*dgdQ(3)
                  dksdQ(4) = dksdg*dgdQ(4)
            
                  if (adptKS) then ! Add (dks/drho)(drho/dQ) to dksdQ

                     !-- Calculate (ddksdr/dg) evaluated at nominal
                     !-- value of rhoKS 
                     
                     ddksdrdgc = (exp(rKSc*(g-gmax))/ksSum1c)
     &                              *((g-gmax) - (ksSum2c/ksSum1c))

                     !-- Calculate (ddksdr/dg) evaluated at perturbed
                     !-- value of rhoKS
                     
                     ddksdrdg1 = (exp(rKS1*(g-gmax))/ksSum11)
     &                              *((g-gmax) - (ksSum21/ksSum11))

                     !-- Calculate drho/dQ. rho = rho(dksdrc,dksdr1)
                     !-- drho/dQ = drhodQ_top/drhodQ_bottom 

                     !-- drhodQ_top = 
                     !--   (drho/ddksdrc)*(ddksdrc/dg)*(dg/dQ)
                     !--   + (drho/ddksdr1)*(ddksdr1/dg)*(dg/dQ)

                     drhodQ_top(1) = (drhoddksdrc*ddksdrdgc
     &                                 +drhoddksdr1*ddksdrdg1)*dgdQ(1)
                     drhodQ_top(2) = (drhoddksdrc*ddksdrdgc
     &                                 +drhoddksdr1*ddksdrdg1)*dgdQ(2)
                     drhodQ_top(3) = (drhoddksdrc*ddksdrdgc
     &                                 +drhoddksdr1*ddksdrdg1)*dgdQ(3)
                     drhodQ_top(4) = (drhoddksdrc*ddksdrdgc
     &                                 +drhoddksdr1*ddksdrdg1)*dgdQ(4)

                     !-- drho/dQ = drhodQ_top/drhodQ_bottom

                     drhodQ(1) = drhodQ_top(1)/drhodQ_bot
                     drhodQ(2) = drhodQ_top(2)/drhodQ_bot
                     drhodQ(3) = drhodQ_top(3)/drhodQ_bot
                     drhodQ(4) = drhodQ_top(4)/drhodQ_bot

                     !-- Calculate (dks/dQ)
 
                     dksdQ(1) = dksdQ(1) + dksdrd*drhodQ(1)
                     dksdQ(2) = dksdQ(2) + dksdrd*drhodQ(2)
                     dksdQ(3) = dksdQ(3) + dksdrd*drhodQ(3)
                     dksdQ(4) = dksdQ(4) + dksdrd*drhodQ(4)

                  end if

                  !-- Calculate (dksMach/dQ)

                  dksMachdQ(1) = dksMachdks*dksdQ(1)
                  dksMachdQ(2) = dksMachdks*dksdQ(2)
                  dksMachdQ(3) = dksMachdks*dksdQ(3)
                  dksMachdQ(4) = dksMachdks*dksdQ(4)                

                  dOdQ(j,k,1) = dksMachdQ(1)*xyj(j,k)
                  dOdQ(j,k,2) = dksMachdQ(2)*xyj(j,k)
                  dOdQ(j,k,3) = dksMachdQ(3)*xyj(j,k)
                  dOdQ(j,k,4) = dksMachdQ(4)*xyj(j,k)

               end do

            end do

         end if

         if (scale_con) then
            do j=1,jdim
               do k=1,kdim
                  do n=1,4
                     !-- Get bounds on off-design constraint
                     if (off_flags(mp)) then
                        c_upp = c_upp1(mp)
                        c_low = c_low1(mp)
                     end if
                     !-- Scale by constraint bound
                     dOdQ(j,k,n) = dOdQ(j,k,n)/c_upp
                  end do
               end do
            end do
         end if

      else if ( obj_func.eq.18 ) then

         diff_flag = 1

         if (diff_flag.eq.0) then

            !-- Finite difference calculation of dJ/dQ for objective 18.
            !-- This is useful to verify the analytic derivative 
            !-- calculation that follows
         
            if (.not. viscous) then
               do n = 1,4
                  do j = jtail1,jtail2
                     tmp = q(j,1,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
                     end if

                     q(j,1,n) = q(j,1,n) + stepsize
                     stepsize = q(j,1,n) - tmp
                     pp = gami*( q(j,1,4) 
     &               - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2)/q(j,1,1) )
                     press_tmp = press(j,1)
                     press(j,1) = pp

                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv
                     objp = 1.d0/cltp

                     press(j,1) = press_tmp
                     q(j,1,n) = tmp - stepsize
                     pp = gami*( q(j,1,4) 
     &                - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2)/q(j,1,1) )
                     press_tmp = press(j,1)
                     press(j,1) = pp

                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtm = cdi + cdv
                     cltm = cli + clv
                     objm = 1.d0/cltm

                     press(j,1) = press_tmp
                     q(j,1,n) = tmp  

clb                     dOdQ(j,1,n) = ( objp - objm ) / (2.d0
clb     &                    *stepsize)
clb                     dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
clb     &                    *stepsize)
                     dcddqFD(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
     &                 *stepsize)
                  end do
               end do
            else
c     -- viscous flow --
               k=1
               do n = 1,4!,3
                  do j = jtail1,jtail2
                     tmp = q(j,k,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
                     end if

                     q(j,k,n) = q(j,k,n) + stepsize
                     stepsize = q(j,k,n) - tmp
                     pp = gami*( q(j,k,4) 
     &                - 0.5d0*( q(j,k,2)**2 + q(j,k,3)**2 )/q(j,k,1) )

                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv

                     objp = 1.d0/cltp
                     press(j,k) = press_tmp

                     q(j,k,n) = tmp - stepsize
                     pp = gami*( q(j,k,4) 
     &                - 0.5d0*( q(j,k,2)**2 + q(j,k,3)**2 )/q(j,k,1) )
                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtm = cdi + cdv
                     cltm = cli + clv

                     objm = 1.d0/cltm
                     press(j,k) = press_tmp
                     q(j,k,n) = tmp  

                     dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                 *stepsize)
                  end do
               end do                       

               do n = 1,4
                  do k = 2,3
                     do j = jtail1,jtail2
                        tmp = q(j,k,n)
                        stepsize = fd_eta*tmp

                        if ( dabs(stepsize) .lt. 1.d-9 ) then
                           stepsize = 1.d-9*dsign(1.d0,stepsize)
                        end if

                        q(j,k,n) = q(j,k,n) + stepsize
                        stepsize = q(j,k,n) - tmp
                        pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
     &                    q(j,k,3)**2 )/q(j,k,1) )

                        press_tmp = press(j,k)
                        press(j,k) = pp
                        call clcd 
     &                  (jdim, kdim, q, press, x, y, xy, xyj, 1,      
     &                    cli, cdi, cmi, clv, cdv, cmv)
                        cdtp = cdi + cdv
                        cltp = cli + clv

                        objp = 1.d0/cltp
                        press(j,k) = press_tmp

                        q(j,k,n) = tmp - stepsize
                        pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 
     &                    + q(j,k,3)**2 )/q(j,k,1) )
                        press_tmp = press(j,k)
                        press(j,k) = pp
                        call clcd 
     &                  (jdim, kdim, q, press, x, y, xy, xyj, 1,  
     &                    cli, cdi, cmi, clv, cdv, cmv)
                        cdtm = cdi + cdv
                        cltm = cli + clv
                  
                        objm = 1.d0/cltm
                        press(j,k) = press_tmp
                        q(j,k,n) = tmp  

                        dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                    *stepsize)

                     end do
                  end do           
               end do
            end if

            !-- Calculate the L2 norm of dOdQ
            L2dOdQ = 0.d0
            do n = 1,ndim
               do k = 1,kend
                  do j = 1,jend
                     L2dOdQ = L2dOdQ + dOdQ(j,k,n)**2
                  end do
               end do
            end do            
            
            L2dOdQ = dsqrt(L2dOdQ)

         else if (diff_flag.eq.1) then

            !-- Analytic derivative calculation of dJ/dQ for off-design 
            !-- constraint function J = 1/Lift

            nscal = 1              
c                        
            cpc = 2.d0/(gamma*fsmach**2)                             
c     
            do j=1,jdim
               do n=1,4
                  dpdq(j,n)=0.d0
                  dcndq(j,n)=0.d0
                  dccdq(j,n)=0.d0
                  dclidq(j,n)=0.d0
                  do k=1,kdim
                     dcnvdq(j,k,n)=0.d0
                     dccvdq(j,k,n)=0.d0
                     dcdvvdq(j,k,n)=0.d0
                  
                     dcfavjm1dq(j,k,n) = 0.d0
                     dcfavjdq(j,k,n) = 0.d0
                     dcfavjp1dq(j,k,n) = 0.d0
                     dcfavjp2dq(j,k,n) = 0.d0
                  end do
               end do
            end do
c                                                                       
c  set limits of integration                                            
c                                                                       
            j1 = jlow
            j2 = jup  
            jtp = jlow
            if(.not.periodic)then
               j1 = jtail1
               j2 = jtail2
               jtp = jtail1+1
            endif
c 
c        compute cp at grid points and store in z array     
c                                                                       
            do j=j1,j2 
clb         dpdq is the derivative of p wrt q.  Second index is n.
               dpdq(j,1)=0.5*gami*(q(j,1,2)**2+q(j,1,3)**2)
     &           /q(j,1,1)**2
               dpdq(j,2)=-gami*q(j,1,2)/q(j,1,1)
               dpdq(j,3)=-gami*q(j,1,3)/q(j,1,1)
               dpdq(j,4)=gami
               
               do n=1,4
                  dzdq(j,n)=dpdq(j,n)*gamma*cpc
               end do
            end do

c                                                                       
c     compute normal force coefficient and chord directed force coeff
c     chord taken as one in all cases
c     
c                                                                       
c      cn = 0.
c      cc = 0.
c    
c     For periodic flows:
c     -going from interval contained between (jmax,1), then (1,2)
c      and ending on (jmax-1,jmax). Note: jlow=1,jup=jmax,jmax=jmaxold-1
c
            do j=jtp,j2-1 
c        jm1 = jminus(j)                                                
c        cpav = (z(j) + z(jm1))*.5   
               do n=1,4
                  dcndq(j,n) = -dzdq(j,n)*0.5*((x(j,1)-x(j-1,1))
     &                 +(x(j+1,1)-x(j,1)))                         
                  dccdq(j,n) = dzdq(j,n)*0.5*((y(j,1)-y(j-1,1))
     &                 +(y(j+1,1)-y(j,1))) 
                  dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &                 -sin(alpha*pi/180.d0)*dccdq(j,n)
               end do
c        cmle = cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))    
c
c        Tornado does it this way.
c         cmle = cmle + cpav*
c     &    (  (x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1)) +
c     &       (y(j,1)+y(jm1,1))*.5*(y(j,1) -y(jm1,1))   )
c
            end do
            j=j1
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*(x(j+1,1)-x(j,1))
               dccdq(j,n) = dzdq(j,n)*0.5*(y(j+1,1)-y(j,1))
               dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &           -sin(alpha*pi/180.d0)*dccdq(j,n)
            end do
            j=j2
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*(x(j,1)-x(j-1,1))
               dccdq(j,n) = dzdq(j,n)*0.5*(y(j,1)-y(j-1,1))
               dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &           -sin(alpha*pi/180.d0)*dccdq(j,n)
            end do
         
                                                                       
            if (viscous) then
c        write (*,*)  ' in viscous'
c                                                                       
c     viscous coefficent of friction calculation 
c     taken from p. buning
c                                                                       
c     calculate the skin friction coefficient
c                                                                       
c      c  = tau   /            2                                        
c       f      w / 1/2*rho   *u                                         
c                         inf  inf                                      
c                                                                       
c      tau  = mu*(du/dy-dv/dx)                                          
c         w                   w                                         
c                                                                       
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc.
c     york, 1974, p. 50, but use freestream values instead of edge vals
c                                                                       
c                                                                       
c     for calculating cf, we need the coefficient of viscosity, mu. use
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1, rhoinf=1.
c                                                                       
               cinf  = 1.               
               alngth= 1.         
c     re already has fsmach scaling                                   
               amu   = rhoinf*alngth/re                               
               uinf2 = fsmach**2          
               k = 1                         
               ja = jlow               
               jb = jup                             
                                                                       
               if(.not.periodic)then 

clb   *****************************************************************
                  j = jtail1                                           
                  jp1 = j+1
                  xixj = xy(j,k,1)       
                  xiyj = xy(j,k,2)                       
                  etaxj = xy(j,k,3)         
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
               
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +q(j,k,2)/(q(j,k,1)**2)*xiyj
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -q(j,k,3)/(q(j,k,1)**2)*xixj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
 
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.d0/q(j,k,1)*xiyj
     &              -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.d0/q(j,k,1)*xixj
     &              +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
                  
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj

                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ******************************************************************

                  j=jtail1+1
                  jp1 = j+1
                  jm1 = j-1
               
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)    
                  xiyj = xy(j,k,2)  
                  etaxj = xy(j,k,3)     
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
                  
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +1.d0/q(j,k,1)*xiyjm1
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &              -1.d0/q(j,k,1)*xixjm1
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
               
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj


clb   ******************************************************************
                  j = jtail2-1 
                  jp1 = j+1
                  jm1 = j-1
               
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)      
                  xiyj = xy(j,k,2)           
                  etaxj = xy(j,k,3)           
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
               
                  dcfavjp1dq(j,k,1) = q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               
                  dcfavjp1dq(j,k,2) = -1.d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
               
                  dcfavjp1dq(j,k,3) = 1.d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj-.5d0/q(j,k,1)
     &              *xixjm1
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
               
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj

clb   *****************************************************************

                  j = jtail2        
                  jp1 = j+1
                  jm1 = j-1
               
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)         
                  xiyj = xy(j,k,2)                      
                  etaxj = xy(j,k,3)                 
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
               
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjdq(j,k,1) = -q(j,k,2)/(q(j,k,1)**2)*xiyj
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              +q(j,k,3)/(q(j,k,1)**2)*xixj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
               
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  dcfavjdq(j,k,2) = 1.d0/q(j,k,1)*xiyj
     &              -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
               
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  dcfavjdq(j,k,3) = -1.d0/q(j,k,1)*xixj
     &              +1.5d0/q(j,k,1)*etaxj
     &              -.5d0/q(j,k,1)*xixjm1
                  
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-.5d0*q(j,k+2,3)/q(j,k+2,1)**2*etaxj
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ******************************************************************
c       -set new limits                                        
                  ja = jtail1+2 
                  jb = jtail2-2  
               endif          
c     
               do j = ja,jb                                            
                  jp1 = j+1                                            
                  jm1 = j-1 
                  jm2 = j-2

                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)  
                  xiyj = xy(j,k,2)             
                  etaxj = xy(j,k,3)  
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
               
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &              *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &              -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &              +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &              +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &              -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &              -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
               
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &              +.5d0/q(j,k,1)*xiyjm1
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &              -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
               
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &              -.5d0/q(j,k,1)*xixjm1
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &              +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = -.5d0/q(j,k,1)*xixjp1
               
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &              *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
               
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
               
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
               
                  dcfavjdq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjp1dq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &              *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
               
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
               
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                                                   
               end do

c     Compute viscous normal and axial forces 
c     -compute normal force coefficient and chord directed force coeff 
c     -chord taken as one in all cases        
c                                                                       
                      
               do j=j1,j2
                  do n = 1,3
                     do k = 1,3
                       dcnvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                    dcfavjm1dq(j,k,n)*(y(j-1,1)-y(j-2,1))
     &                    + dcfavjdq(j,k,n)*(y(j,1)-y(j-1,1))
     &                    + dcfavjp1dq(j,k,n)*(y(j+1,1)-y(j,1))
     &                    + dcfavjp2dq(j,k,n)*(y(j+2,1)-y(j+1,1)))
                       dccvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                    dcfavjm1dq(j,k,n)*(x(j-1,1)-x(j-2,1))
     &                    + dcfavjdq(j,k,n)*(x(j,1)-x(j-1,1))
     &                    + dcfavjp1dq(j,k,n)*(x(j+1,1)-x(j,1))
     &                    + dcfavjp2dq(j,k,n)*(x(j+2,1)-x(j+1,1)))
                       dclvvdq(j,k,n)=dcnvdq(j,k,n)*cos(alpha*pi/180.d0)
     &                    -dccvdq(j,k,n)*sin(alpha*pi/180.d0)
                     end do
                  end do
               end do
            end if

            !-- Need to multiply dclvvdq and dclidq by -1/Cl^2 to get 
            !-- dOdQ. So calculate -1/Cl^2 here before calculating dOdQ:

            do n = 1,4
               do k = 2,3
                  do j = jtail1,jtail2

                     press(j,k) = gami*(q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
     &                    q(j,k,3)**2 )/q(j,k,1) )
                     
                  end do
               end do
            end do

            call clcd 
     &           (jdim, kdim, q, press, x, y, xy, xyj, 1,      
     &           cli, cdi, cmi, clv, cdv, cmv)
            
            clt2 = -1.d0*(cli + clv)**-2

            !-- Now calculate dOdQ
            do j=j1,j2
               do k=1,3
                  do n=1,4
                     if (k .eq. 1) then
                        dOdQ(j,k,n) = dclidq(j,n)*clt2
                        if (viscous) dOdQ(j,k,n) = dOdQ(j,k,n) 
     &                       + dclvvdq(j,k,n)*clt2
                     else
                        if (viscous) then
                           dOdQ(j,k,n) = dclvvdq(j,k,n)*clt2
                        else 
                           dOdQ(j,k,n) = 0.d0
                        endif
                     end if
                     dOdQ(j,k,n) = dOdQ(j,k,n)*xyj(j,k)
                  end do
               end do
            end do
         
            !-- Calculate the L2 norm of dOdQ
            L2dOdQ = 0.d0
            do n = 1,ndim
               do k = 1,kend
                  do j = 1,jend
                     L2dOdQ = L2dOdQ + dOdQ(j,k,n)**2
                  end do
               end do
            end do            
            
            L2dOdQ = dsqrt(L2dOdQ)

         !-- Calculate the L2 norm of dOdQ
         L2dOdQ = 0.d0
         do n = 1,ndim
            do k = 1,kend
               do j = 1,jend
                  L2dOdQ = L2dOdQ + dOdQ(j,k,n)**2
               end do
            end do
         end do            
         
         L2dOdQ = dsqrt(L2dOdQ)
            
         end if !(diff_flag.eq.0)

      else if (obj_func.eq.20.or.obj_func.eq.21) then

         !-- Objective function J = 1/R (Inverse aircraft range factor):
         !-- R = (Mach)*(Cl/Cd)
         !-- J = Cd/(Mach*Cl) (OBJ_FUNC.eq.20)
         !-- 
         !-- OR
         !--
         !-- Objective function J = Cd/Cl (OBJ_FUNC.eq.21)
         !-- Inverse aircraft endurance factor

         diff_flag = 1

         if (diff_flag.eq.0) then

            !-- Finite difference calculation of dJ/dQ for objective 20.
            !-- This is useful to verify the analytic derivative 
            !-- calculation that follows
         
            if (.not. viscous) then
               do n = 1,4
                  do j = jtail1,jtail2
                     tmp = q(j,1,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
                     end if

                     q(j,1,n) = q(j,1,n) + stepsize
                     stepsize = q(j,1,n) - tmp
                     pp = gami*( q(j,1,4) 
     &               - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2)/q(j,1,1) )
                     press_tmp = press(j,1)
                     press(j,1) = pp

                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv                    
                     
                     if (obj_func.eq.20) then
                        objp = cdtp/(fsmach*cltp)
                     else if (obj_func.eq.21) then
                        objp = cdtp/(cltp)
                     end if

                     press(j,1) = press_tmp
                     q(j,1,n) = tmp - stepsize
                     pp = gami*( q(j,1,4) 
     &                - 0.5d0*( q(j,1,2)**2 + q(j,1,3)**2)/q(j,1,1) )
                     press_tmp = press(j,1)
                     press(j,1) = pp

                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtm = cdi + cdv
                     cltm = cli + clv

                     if (obj_func.eq.20) then
                        objm = cdtm/(fsmach*cltm)
                     else if (obj_func.eq.21) then
                        objm = cdtm/(cltm)
                     end if

                     press(j,1) = press_tmp
                     q(j,1,n) = tmp  

clb                     dOdQ(j,1,n) = ( objp - objm ) / (2.d0
clb     &                    *stepsize)
clb                     dOdQ(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
clb     &                    *stepsize)
                     dcddqFD(j,1,n) = xyj(j,1)*( objp - objm ) / (2.d0
     &                 *stepsize)
                  end do
               end do
            else
c     -- viscous flow --
               k=1
               do n = 1,4!,3
                  do j = jtail1,jtail2
                     tmp = q(j,k,n)
                     stepsize = fd_eta*tmp

                     if ( dabs(stepsize) .lt. 1.d-9 ) then
                        stepsize = 1.d-9*dsign(1.d0,stepsize)
                     end if

                     q(j,k,n) = q(j,k,n) + stepsize
                     stepsize = q(j,k,n) - tmp
                     pp = gami*( q(j,k,4) 
     &                - 0.5d0*( q(j,k,2)**2 + q(j,k,3)**2 )/q(j,k,1) )

                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtp = cdi + cdv
                     cltp = cli + clv

                     if (obj_func.eq.20) then
                        objp = cdtp/(fsmach*cltp)
                     else if (obj_func.eq.21) then
                        objp = cdtp/(cltp)
                     end if

                     press(j,k) = press_tmp

                     q(j,k,n) = tmp - stepsize
                     pp = gami*( q(j,k,4) 
     &                - 0.5d0*( q(j,k,2)**2 + q(j,k,3)**2 )/q(j,k,1) )
                     press_tmp = press(j,k)
                     press(j,k) = pp
                     call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &                 cli, cdi, cmi, clv, cdv, cmv)
                     cdtm = cdi + cdv
                     cltm = cli + clv

                     if (obj_func.eq.20) then
                        objm = cdtm/(fsmach*cltm)
                     else if (obj_func.eq.21) then
                        objm = cdtm/(cltm)
                     end if

                     press(j,k) = press_tmp
                     q(j,k,n) = tmp  

                     dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                 *stepsize)
                  end do
               end do                       

               do n = 1,4
                  do k = 2,3
                     do j = jtail1,jtail2
                        tmp = q(j,k,n)
                        stepsize = fd_eta*tmp

                        if ( dabs(stepsize) .lt. 1.d-9 ) then
                           stepsize = 1.d-9*dsign(1.d0,stepsize)
                        end if

                        q(j,k,n) = q(j,k,n) + stepsize
                        stepsize = q(j,k,n) - tmp
                        pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
     &                    q(j,k,3)**2 )/q(j,k,1) )

                        press_tmp = press(j,k)
                        press(j,k) = pp
                        call clcd 
     &                  (jdim, kdim, q, press, x, y, xy, xyj, 1,      
     &                    cli, cdi, cmi, clv, cdv, cmv)
                        cdtp = cdi + cdv
                        cltp = cli + clv

                        if (obj_func.eq.20) then
                           objp = cdtp/(fsmach*cltp)
                        else if (obj_func.eq.21) then
                           objp = cdtp/(cltp)
                        end if

                        press(j,k) = press_tmp

                        q(j,k,n) = tmp - stepsize
                        pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 
     &                    + q(j,k,3)**2 )/q(j,k,1) )
                        press_tmp = press(j,k)
                        press(j,k) = pp
                        call clcd 
     &                  (jdim, kdim, q, press, x, y, xy, xyj, 1,  
     &                    cli, cdi, cmi, clv, cdv, cmv)
                        cdtm = cdi + cdv
                        cltm = cli + clv

                        if (obj_func.eq.20) then
                           objm = cdtm/(fsmach*cltm)
                        else if (obj_func.eq.21) then
                           objm = cdtm/(cltm)
                        end if
                  
                        press(j,k) = press_tmp
                        q(j,k,n) = tmp  

                        dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.d0
     &                    *stepsize)

                     end do
                  end do           
               end do
            end if

            !-- Calculate the L2 norm of dOdQ
            L2dOdQ = 0.d0
            do n = 1,ndim
               do k = 1,kend
                  do j = 1,jend
                     L2dOdQ = L2dOdQ + dOdQ(j,k,n)**2
                  end do
               end do
            end do            
            
            L2dOdQ = dsqrt(L2dOdQ)
           
         else if (diff_flag.eq.1) then

            !-- dJ/dQ = [dJ/d(Cd)]*[d(Cd)/dQ] + [dJ/d(Cl)]*[d(Cl)/dQ]
         
            !-- Calculate [dJ/d(Cd)] and  [dJ/d(Cl)]
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &           cli, cdi, cmi, clv, cdv, cmv)
            cdt = cdi + cdv
            clt = cli + clv
            
            if (obj_func.eq.20) then
               dobjdcd = 1.d0/(fsmach*clt)
               dobjdcl = -cdt/(fsmach*(clt**2))
            else if (obj_func.eq.21) then
               dobjdcd = 1.d0/(clt)
               dobjdcl = -cdt/(clt**2)
            end if
            
            !-- Analytical calculation of [d(Cd)/dQ]
            nscal = 1
            
            cpc = 2.d0/(gamma*fsmach**2)                             
            
            do j=1,jdim
               do n=1,4
                  dpdq(j,n)=0.d0
                  dcndq(j,n)=0.d0
                  dccdq(j,n)=0.d0
                  dcdidq(j,n)=0.d0
                  do k=1,kdim
                     dcnvdq(j,k,n)=0.d0
                     dccvdq(j,k,n)=0.d0
                     dcdvvdq(j,k,n)=0.d0
                     
                     dcfavjm1dq(j,k,n) = 0.d0
                     dcfavjdq(j,k,n) = 0.d0
                     dcfavjp1dq(j,k,n) = 0.d0
                     dcfavjp2dq(j,k,n) = 0.d0
                  end do
               end do
            end do
            
            !-- Set limits of integration
                                                                       
            j1 = jlow
            j2 = jup
            jtp = jlow

            if(.not.periodic)then
               j1 = jtail1
               j2 = jtail2
               jtp = jtail1+1
            endif
                                                                       
            !-- Compute cp at grid points and store in z array
            do j=j1,j2 
clb      dpdq is the derivative of p wrt q.  Second index is n.
               dpdq(j,1)=0.5*gami*(q(j,1,2)**2+q(j,1,3)**2)
     &              /q(j,1,1)**2
               dpdq(j,2)=-gami*q(j,1,2)/q(j,1,1)
               dpdq(j,3)=-gami*q(j,1,3)/q(j,1,1)
               dpdq(j,4)=gami
               
               do n=1,4
                  dzdq(j,n)=dpdq(j,n)*gamma*cpc
               end do
            end do
            
c     
c     compute normal force coefficient and chord directed force coeff
c     chord taken as one in all cases
c     
c                                                                       
c      cn = 0.                                                           
c      cc = 0.                                                           
c    
c     For periodic flows:
c     -going from interval contained between (jmax,1), then (1,2)
c      and ending on (jmax-1,jmax). Note: jlow=1,jup=jmax,jmax=jmaxold-1
c
            do j=jtp,j2-1
c        jm1 = jminus(j)                                                
c        cpav = (z(j) + z(jm1))*.5   
               do n=1,4
                  dcndq(j,n) = -dzdq(j,n)*0.5*((x(j,1)-x(j-1,1))
     &                 +(x(j+1,1)-x(j,1)))                         
                  dccdq(j,n) = dzdq(j,n)*0.5*((y(j,1)-y(j-1,1))
     &                 +(y(j+1,1)-y(j,1))) 
                  dcdidq(j,n) = sin(alpha*pi/180.d0)*dcndq(j,n)
     &                 +cos(alpha*pi/180.d0)*dccdq(j,n)
               end do
c        cmle = cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))    
c
c        Tornado does it this way.
c         cmle = cmle + cpav*
c     &    (  (x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1)) +                  
c     &       (y(j,1)+y(jm1,1))*.5*(y(j,1) -y(jm1,1))   )                
c
            end do
            j=j1
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*(x(j+1,1)-x(j,1))
               dccdq(j,n) = dzdq(j,n)*0.5*(y(j+1,1)-y(j,1))
               dcdidq(j,n) = sin(alpha*pi/180.d0)*dcndq(j,n)
     &              +cos(alpha*pi/180.d0)*dccdq(j,n)
            end do
            j=j2
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*(x(j,1)-x(j-1,1))
               dccdq(j,n) = dzdq(j,n)*0.5*(y(j,1)-y(j-1,1))
               dcdidq(j,n) = sin(alpha*pi/180.d0)*dcndq(j,n)
     &              +cos(alpha*pi/180.d0)*dccdq(j,n)
            end do
            
            
            if (viscous) then   
c        write (*,*)  ' in viscous'
c                                                                       
c     viscous coefficent of friction calculation 
c     taken from p. buning                                               
c                                                                       
c     calculate the skin friction coefficient
c                                                                       
c      c  = tau   /            2                                        
c       f      w / 1/2*rho   *u                                         
c                         inf  inf                                      
c                                                                       
c      tau  = mu*(du/dy-dv/dx)                                          
c         w                   w                                         
c                                                                       
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc.,
c     york, 1974, p. 50, but use freestream values instead of edge values
c                                                                       
c                                                                       
c     for calculating cf, we need the coefficient of viscosity, mu.  use 
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1, rhoinf=1. 
c                                                                       
               cinf  = 1. 
               alngth= 1. 
c     re already has fsmach scaling                                      
               amu   = rhoinf*alngth/re     
               uinf2 = fsmach**2                                                 
c     
               k = 1                                                              
               ja = jlow                                                         
               jb = jup                                                          
c                                                                       
               if(.not.periodic)then 

clb   ********************************************************************                                         
                  j = jtail1                                                  
                  jp1 = j+1
               
                  xixj = xy(j,k,1)                                             
                  xiyj = xy(j,k,2)                                             
                  etaxj = xy(j,k,3)                                            
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &                 +q(j,k,2)/(q(j,k,1)**2)*xiyj
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -q(j,k,3)/(q(j,k,1)**2)*xixj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
               
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &                 -1.d0/q(j,k,1)*xiyj
     &                 -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
                  
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &                 +1.d0/q(j,k,1)*xixj
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
                  
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ********************************************************************

                  j=jtail1+1
                  jp1 = j+1
                  jm1 = j-1
                  
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)                                             
                  xiyj = xy(j,k,2)                                             
                  etaxj = xy(j,k,3)                                            
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &                 -q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &                 +q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
                  
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &                 +1.d0/q(j,k,1)*xiyjm1
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &                 -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
                  
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &                 -1.d0/q(j,k,1)*xixjm1
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
                  
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ********************************************************************     
                  j = jtail2-1 
                  jp1 = j+1
                  jm1 = j-1
                  
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)                                             
                  xiyj = xy(j,k,2)                                             
                  etaxj = xy(j,k,3)                                            
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjp1dq(j,k,1) = q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &                 -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &                 +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &                 *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  
                  dcfavjp1dq(j,k,2) = -1.d0/q(j,k,1)*xiyjp1
     &                 -1.5d0/q(j,k,1)*etayj
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &                 +.5d0/q(j,k,1)*xiyjm1
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  
                  dcfavjp1dq(j,k,3) = 1.d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj-.5d0/q(j,k,1)
     &                 *xixjm1
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj

clb   ********************************************************************

                  j = jtail2                                                  
                  jp1 = j+1
                  jm1 = j-1
                  
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)                                             
                  xiyj = xy(j,k,2)                                             
                  etaxj = xy(j,k,3)                                            
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &                 *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjdq(j,k,1) = -q(j,k,2)/(q(j,k,1)**2)*xiyj
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 +q(j,k,3)/(q(j,k,1)**2)*xixj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &                 -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &                 +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  dcfavjdq(j,k,2) = 1.d0/q(j,k,1)*xiyj
     &                 -1.5d0/q(j,k,1)*etayj
     &                 +.5d0/q(j,k,1)*xiyjm1
                  
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  dcfavjdq(j,k,3) = -1.d0/q(j,k,1)*xixj
     &                 +1.5d0/q(j,k,1)*etaxj
     &                 -.5d0/q(j,k,1)*xixjm1
                  
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/q(j,k+2,1)**2*etaxj
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ********************************************************************                            
c                                                                       
c       -set new limits                                                       
                  ja = jtail1+2                                               
                  jb = jtail2-2                                               
               endif                                                          
c     
               do j = ja,jb                                            
                  jp1 = j+1                                            
                  jm1 = j-1 
                  jm2 = j-2
                  
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)                                             
                  xiyj = xy(j,k,2)                                             
                  etaxj = xy(j,k,3)                                            
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &                 *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &                 -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &                 +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
                  
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &                 +.5d0/q(j,k,1)*xiyjm1
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &                 -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
                  
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &                 -.5d0/q(j,k,1)*xixjm1
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = -.5d0/q(j,k,1)*xixjp1
                  
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjdq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjp1dq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  
                  
c     
               end do
c      write (*,*) 'done 110'
clb      if (sngvalte) then
clbc       -average trailing edge value
clb        z(j1)=.5d0*(z(j1)+z(j2))
clb        z(j2)=z(j1)
clb      endif
c                                                                       
c     Compute viscous normal and axial forces                             
c                                                                       
c     -compute normal force coefficient and chord directed force coeff     
c     -chord taken as one in all cases                                      
c                                                                       
                      
               do j=j1,j2
                  do n = 1,3
                     do k = 1,3
                       dcnvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                       dcfavjm1dq(j,k,n)*(y(j-1,1)-y(j-2,1))
     &                       + dcfavjdq(j,k,n)*(y(j,1)-y(j-1,1))
     &                       + dcfavjp1dq(j,k,n)*(y(j+1,1)-y(j,1))
     &                       + dcfavjp2dq(j,k,n)*(y(j+2,1)-y(j+1,1)))
                       dccvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                       dcfavjm1dq(j,k,n)*(x(j-1,1)-x(j-2,1))
     &                       + dcfavjdq(j,k,n)*(x(j,1)-x(j-1,1))
     &                       + dcfavjp1dq(j,k,n)*(x(j+1,1)-x(j,1))
     &                       + dcfavjp2dq(j,k,n)*(x(j+2,1)-x(j+1,1)))
                       dcdvvdq(j,k,n)=dcnvdq(j,k,n)*sin(alpha*pi/180.d0)
     &                       +dccvdq(j,k,n)*cos(alpha*pi/180.d0)
                     end do
                  end do
               end do
            end if
            
            do j=j1,j2       
               do k=1,3
                  do n=1,4
                     if (k .eq. 1) then
                        dcdtdq(j,k,n) = dcdidq(j,n)
                        if (viscous) dcdtdq(j,k,n) = dcdtdq(j,k,n) 
     &                       + dcdvvdq(j,k,n)
                     else
                        if (viscous) then
                           dcdtdq(j,k,n) = dcdvvdq(j,k,n)
                        else 
                           dcdtdq(j,k,n) = 0.d0
                        endif
                     end if
                  end do
               end do
            end do
         
            !-- Analytic derivative calculation of [dJ/d(Cl)]

            nscal = 1
            
            cpc = 2.d0/(gamma*fsmach**2)                             
            
            do j=1,jdim
               do n=1,4
                  dpdq(j,n)=0.d0
                  dcndq(j,n)=0.d0
                  dccdq(j,n)=0.d0
                  dclidq(j,n)=0.d0
                  do k=1,kdim
                     dcnvdq(j,k,n)=0.d0
                     dccvdq(j,k,n)=0.d0
                     dcdvvdq(j,k,n)=0.d0
                     
                     dcfavjm1dq(j,k,n) = 0.d0
                     dcfavjdq(j,k,n) = 0.d0
                     dcfavjp1dq(j,k,n) = 0.d0
                     dcfavjp2dq(j,k,n) = 0.d0
                  end do
               end do
            end do
c     
c     set limits of integration     
c                                                                     
            j1 = jlow
            j2 = jup  
            jtp = jlow
            if(.not.periodic)then
               j1 = jtail1
               j2 = jtail2
               jtp = jtail1+1
            endif
c 
c        compute cp at grid points and store in z array     
c                                                                       
            do j=j1,j2 
clb         dpdq is the derivative of p wrt q.  Second index is n.
               dpdq(j,1)=0.5*gami*(q(j,1,2)**2+q(j,1,3)**2)
     &              /q(j,1,1)**2
               dpdq(j,2)=-gami*q(j,1,2)/q(j,1,1)
               dpdq(j,3)=-gami*q(j,1,3)/q(j,1,1)
               dpdq(j,4)=gami
               
               do n=1,4
                  dzdq(j,n)=dpdq(j,n)*gamma*cpc
               end do
            end do
         
c                                                                       
c     compute normal force coefficient and chord directed force coeff
c     chord taken as one in all cases
c     
c                                                                       
c      cn = 0.                                                           
c      cc = 0.                                                           
c    
c     For periodic flows:
c     -going from interval contained between (jmax,1), then (1,2)
c      and ending on (jmax-1,jmax). Note: jlow=1,jup=jmax,jmax=jmaxold-1
c
            do j=jtp,j2-1 
c        jm1 = jminus(j)                                                
c        cpav = (z(j) + z(jm1))*.5   
               do n=1,4
                  dcndq(j,n) = -dzdq(j,n)*0.5*((x(j,1)-x(j-1,1))
     &                 +(x(j+1,1)-x(j,1)))                         
                  dccdq(j,n) = dzdq(j,n)*0.5*((y(j,1)-y(j-1,1))
     &                 +(y(j+1,1)-y(j,1))) 
                  dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &                 -sin(alpha*pi/180.d0)*dccdq(j,n)
               end do
c     cmle = cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))    
c
c        Tornado does it this way.
c         cmle = cmle + cpav*
c     &    (  (x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1)) +                  
c     &       (y(j,1)+y(jm1,1))*.5*(y(j,1) -y(jm1,1))   )                
c
            end do
            j=j1
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*(x(j+1,1)-x(j,1))
               dccdq(j,n) = dzdq(j,n)*0.5*(y(j+1,1)-y(j,1))
               dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &              -sin(alpha*pi/180.d0)*dccdq(j,n)
            end do
            j=j2
            do n=1,4
               dcndq(j,n) = -dzdq(j,n)*0.5*(x(j,1)-x(j-1,1))
               dccdq(j,n) = dzdq(j,n)*0.5*(y(j,1)-y(j-1,1))
               dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &              -sin(alpha*pi/180.d0)*dccdq(j,n)
            end do
            
            
            if (viscous) then
c        write (*,*)  ' in viscous'
c                                                                       
c     viscous coefficent of friction calculation 
c     taken from p. buning                                               
c                                                                       
c     calculate the skin friction coefficient
c                                                                       
c      c  = tau   /            2                                        
c       f      w / 1/2*rho   *u                                         
c                         inf  inf                                      
c                                                                       
c      tau  = mu*(du/dy-dv/dx)                                          
c         w                   w                                         
c                                                                       
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc.,
c     york, 1974, p. 50, but use freestream values instead of edge values
c                                                                       
c                                                                       
c     for calculating cf, we need the coefficient of viscosity, mu.  use 
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1, rhoinf=1. 
c                                                                       
               cinf  = 1.               
               alngth= 1.         
c     re already has fsmach scaling                                   
               amu   = rhoinf*alngth/re                               
               uinf2 = fsmach**2          
               k = 1                         
               ja = jlow               
               jb = jup                             
               
               if(.not.periodic)then 
                  
clb   ****************************************************************
                  j = jtail1                                           
                  jp1 = j+1
                  xixj = xy(j,k,1)       
                  xiyj = xy(j,k,2)                       
                  etaxj = xy(j,k,3)         
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &                 +q(j,k,2)/(q(j,k,1)**2)*xiyj
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -q(j,k,3)/(q(j,k,1)**2)*xixj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
                  
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &                 -1.d0/q(j,k,1)*xiyj
     &                 -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
                  
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &                 +1.d0/q(j,k,1)*xixj
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
                  
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
               

clb   ******************************************************************

                  j=jtail1+1
                  jp1 = j+1
                  jm1 = j-1
                  
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)    
                  xiyj = xy(j,k,2)  
                  etaxj = xy(j,k,3)     
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &                 -q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &                 +q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
                  
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &                 +1.d0/q(j,k,1)*xiyjm1
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &                 -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
                  
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &                 -1.d0/q(j,k,1)*xixjm1
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
                  
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  
               
clb   ******************************************************************
                  j = jtail2-1 
                  jp1 = j+1
                  jm1 = j-1
                  
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)      
                  xiyj = xy(j,k,2)           
                  etaxj = xy(j,k,3)           
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjp1dq(j,k,1) = q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &                 -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &                 +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &                 *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  
                  dcfavjp1dq(j,k,2) = -1.d0/q(j,k,1)*xiyjp1
     &                 -1.5d0/q(j,k,1)*etayj
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &                 +.5d0/q(j,k,1)*xiyjm1
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  
                  dcfavjp1dq(j,k,3) = 1.d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj-.5d0/q(j,k,1)
     &                 *xixjm1
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
               
clb   *****************************************************************

                  j = jtail2        
                  jp1 = j+1
                  jm1 = j-1
                  
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)         
                  xiyj = xy(j,k,2)                      
                  etaxj = xy(j,k,3)                 
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &                 *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjdq(j,k,1) = -q(j,k,2)/(q(j,k,1)**2)*xiyj
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 +q(j,k,3)/(q(j,k,1)**2)*xixj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &                 -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &                 +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  dcfavjdq(j,k,2) = 1.d0/q(j,k,1)*xiyj
     &                 -1.5d0/q(j,k,1)*etayj
     &                 +.5d0/q(j,k,1)*xiyjm1
                  
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  dcfavjdq(j,k,3) = -1.d0/q(j,k,1)*xixj
     &                 +1.5d0/q(j,k,1)*etaxj
     &                 -.5d0/q(j,k,1)*xixjm1
                  
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-.5d0*q(j,k+2,3)/q(j,k+2,1)**2*etaxj
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  

clb   ******************************************************************
c       -set new limits                                        
                  ja = jtail1+2                                    
                  jb = jtail2-2 
               endif          
c     
               do j = ja,jb                                            
                  jp1 = j+1                                            
                  jm1 = j-1 
                  jm2 = j-2
                  
                  xixjm1 = xy(jm1,k,1)
                  xiyjm1 = xy(jm1,k,2)
                  xixj = xy(j,k,1)  
                  xiyj = xy(j,k,2)             
                  etaxj = xy(j,k,3)  
                  etayj = xy(j,k,4)
                  xixjp1 = xy(jp1,k,1)
                  xiyjp1 = xy(jp1,k,2)
                  
                  dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)
     &                 *xiyjm1+.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &                 -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &                 +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
                  dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &                 +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &                 -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
                  dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &                 -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
                  
                  dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
                  dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &                 +.5d0/q(j,k,1)*xiyjm1
                  dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &                 -1.5d0/q(j,k,1)*etayj
                  dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
                  
                  dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
                  dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &                 -.5d0/q(j,k,1)*xixjm1
                  dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &                 +1.5d0/q(j,k,1)*etaxj
                  dcfavjp2dq(j,k,3) = -.5d0/q(j,k,1)*xixjp1
                  
                  dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)
     &                 *etayj+2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
                  
                  dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
                  
                  dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
                  
                  dcfavjdq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  dcfavjp1dq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)
     &                 *etayj-0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
                  
                  dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
                  
                  dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
                  
               end do
            
c     Compute viscous normal and axial forces 
c     -compute normal force coefficient and chord directed force coeff 
c     -chord taken as one in all cases        
c                                                                       
                      
               do j=j1,j2
                  do n = 1,3
                     do k = 1,3
                       dcnvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                       dcfavjm1dq(j,k,n)*(y(j-1,1)-y(j-2,1))
     &                       + dcfavjdq(j,k,n)*(y(j,1)-y(j-1,1))
     &                       + dcfavjp1dq(j,k,n)*(y(j+1,1)-y(j,1))
     &                       + dcfavjp2dq(j,k,n)*(y(j+2,1)-y(j+1,1)))
                       dccvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                       dcfavjm1dq(j,k,n)*(x(j-1,1)-x(j-2,1))
     &                       + dcfavjdq(j,k,n)*(x(j,1)-x(j-1,1))
     &                       + dcfavjp1dq(j,k,n)*(x(j+1,1)-x(j,1))
     &                       + dcfavjp2dq(j,k,n)*(x(j+2,1)-x(j+1,1)))
                       dclvvdq(j,k,n)=dcnvdq(j,k,n)*cos(alpha*pi/180.d0)
     &                       -dccvdq(j,k,n)*sin(alpha*pi/180.d0)
                    end do
                 end do
              end do
           end if
           
           do j=j1,j2                                                   
              do k=1,3
                 do n=1,4
                    if (k .eq. 1) then
                       dcltdq(j,k,n) = dclidq(j,n)
                       if (viscous) dcltdq(j,k,n) = dcltdq(j,k,n) 
     &                      + dclvvdq(j,k,n)
                    else
                       if (viscous) then
                          dcltdq(j,k,n) = dclvvdq(j,k,n)
                       else 
                          dcltdq(j,k,n) = 0.d0
                       endif
                    end if
                 end do
              end do
           end do
           
           do j=j1,j2                                                   
              do k=1,3
                 do n=1,4
                    dOdQ(j,k,n) = dobjdcd*dcdtdq(j,k,n) 
     &                   + dobjdcl*dcltdq(j,k,n)
                    dOdQ(j,k,n) = dOdQ(j,k,n)*xyj(j,k)
                 end do
              end do
           end do

           !-- Calculate the L2 norm of dOdQ
           L2dOdQ = 0.d0
           do n = 1,ndim
              do k = 1,kend
                 do j = 1,jend
                    L2dOdQ = L2dOdQ + dOdQ(j,k,n)**2
                 end do
              end do
           end do            
           
           L2dOdQ = dsqrt(L2dOdQ)
         
        end if ! (diff_flag.eq.1)

      end if ! (obj_func.eq.1)
      
      if (obj_func.eq.19.and.lcdm_grad.eq.2) then
         do n = 1,ndim
            do j = 1,jdim
               do k = 1,kdim
                  dOdQ(j,k,n) = dOdQ(j,k,n) / 1.d0
               enddo
            enddo
         enddo
      else
         do n = 1,ndim
            do j = 1,jdim
               do k = 1,kdim
                  dOdQ(j,k,n) = dOdQ(j,k,n) / obj0
               enddo
            enddo
         enddo
      end if

      return
      end                       !dobjdq
