c ##########################
c ##                      ##
c ##  subroutine scaledt  ##
c ##                      ##
c ##########################
c
      subroutine scaledt (jdim,kdim,s,ds)                                
c                                                                       
#include "../include/arcom.inc"
c
      dimension s(jdim,kdim,4),ds(jdim,kdim)                            
c                                                                       
c                                                                       
c---- scale with spatially variable time step ----
      do 10 m = 1,4                                                     
      do 10 k = kbegin,kend                                             
      do 10 j = jbegin,jend                                             
         s(j,k,m) = s(j,k,m)*ds(j,k)                                    
 10   continue                                                          
c
c
      return                                                            
      end                                                               
