c     ------------------------------------------------------------------
c     -- differentiation of diffusion terms for the S-A model --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine dsa_dif( jmax, kmax, jbegin, jend, jlow, jup, kbegin,
     &     kend, klow, kup, q, turre, xy, xyj, fmu, dmul, bm2, bm1, db,
     &     bp1, bp2, re)

      implicit none

#include "../include/parms.inc"
#include "../include/sam.inc"

      integer jmax, kmax, jbegin, jend, jlow, jup, kbegin,kend,klow,kup 
      integer j,k,n,kp,km,jp,jm
      double precision q(jmax,kmax,4),xy(jmax,kmax,4),turre(jmax,kmax)
      double precision xyj(jmax,kmax),bm2(5,jmax,kmax),bm1(5,jmax,kmax)
      double precision db(5,jmax,kmax),bp1(5,jmax,kmax)
      double precision bp2(5,jmax,kmax),fmu(jmax,kmax),dmul(5,jmax,kmax)

c     -- local arrays --
      double precision dnu(5,maxjb,maxkb),tpl(maxjb,maxkb)
      double precision wk(maxjb,maxkb)

      double precision resiginv,difa,difb,tnu,ri,xy1p,xy2p,ttp,xy1m,xy2m
      double precision re,ttm,trem,trep,cap,cam,cnub,cbp,cbm,cnua,xy3p
      double precision xy4p,xy3m,xy4m

      resiginv= sigmainv/re
      difa = (1.0+cb2)*resiginv
      difb = cb2*resiginv

c     -- tpl = nu_turbulent + nu_laminar --
      do k = kbegin,kend
         do j = jbegin,jend
            tnu = turre(j,k)
            tpl(j,k) = fmu(j,k)/(q(j,k,1)*xyj(j,k)) + tnu
         end do
      end do

c      write (*,*) 'done tpl'

c     -- dnu = d( nu + nu_tilde )/d(Q^) --
      do k = kbegin,kend
         do j = jbegin,jend
c            write (*,*) j,k,q(j,k,1),xyj(j,k),dmul(1,j,k)
            ri = 1.0/(q(j,k,1)*xyj(j,k))
            dnu(1,j,k) = - fmu(j,k)*ri*ri*xyj(j,k) + ri*dmul(1,j,k)
            dnu(2,j,k) = ri*dmul(2,j,k)
            dnu(3,j,k) = ri*dmul(3,j,k)
            dnu(4,j,k) = ri*dmul(4,j,k)
            dnu(5,j,k) = xyj(j,k)
         end do
      end do

c      write (*,*) 'done dnu'

c     -- diffusion terms in xi direction --
      do k = klow,kup
         do j = jbegin,jup
            jp = j + 1
            wk(j,k) = turre(jp,k) - turre(j,k)
         end do
      end do

c      write (*,*) 'done wk'
      do k = klow,kup
         do j = jlow,jup
            jp = j+1
            jm = j-1
            xy1p = 0.5*(xy(j,k,1)+xy(jp,k,1))
            xy2p = 0.5*(xy(j,k,2)+xy(jp,k,2))
            ttp = (xy1p*xy(j,k,1)+xy2p*xy(j,k,2))

            xy1m = 0.5*(xy(j,k,1)+xy(jm,k,1))
            xy2m = 0.5*(xy(j,k,2)+xy(jm,k,2))
            ttm =  (xy1m*xy(j,k,1)+xy2m*xy(j,k,2))

c     -- contribution by holding ( nu + nu_tilde ) constant --
            trem = 0.5*(tpl(jm,k)+tpl(j,k))
            trep = 0.5*(tpl(j,k)+tpl(jp,k))

            cap = ttp*trep*difa
            cam = ttm*trem*difa

            cnub = difb*tpl(j,k)
            cbp = ttp*cnub
            cbm = ttm*cnub 

            bm2(5,j,k) = bm2(5,j,k) + (cbm-cam)/xyj(j,k)*xyj(jm,k)
            db(5,j,k)  = db(5,j,k)  - cbp + cap - cbm + cam
            bp2(5,j,k) = bp2(5,j,k) + (cbp-cap)/xyj(j,k)*xyj(jp,k)

c     -- contribution from ( nu + nu_tilde) term --
            cnua = difa/xyj(j,k)
            cap = ttp*cnua*wk(j,k)
            cam = ttm*cnua*wk(jm,k)

            cnub = difb/xyj(j,k)
            cbp = ttp*cnub*wk(j,k)
            cbm = ttm*cnub*wk(jm,k)               

            do n = 1,5
               bm2(n,j,k) = bm2(n,j,k) + dnu(n,jm,k)*0.5*cam
               db(n,j,k)  = db(n,j,k)  + dnu(n,j,k)*( 0.5*( - cap +
     &              cam)+ cbp - cbm )
               bp2(n,j,k) = bp2(n,j,k) - dnu(n,jp,k)*0.5*cap
            end do
         end do
      end do

c      write (*,*) 'done xi'

c     -- diffusion terms in eta direction --
      do k = kbegin,kup
         kp = k + 1
         do j = jlow,jup
            wk(j,k) = turre(j,kp) - turre(j,k)
         end do
      end do

      do k = klow,kup
        kp = k+1
        km = k-1
        do j = jlow,jup
          xy3p = 0.5*(xy(j,k,3)+xy(j,kp,3))
          xy4p = 0.5*(xy(j,k,4)+xy(j,kp,4))
          ttp  =  (xy3p*xy(j,k,3)+xy4p*xy(j,k,4))
          
          xy3m = 0.5*(xy(j,k,3)+xy(j,km,3))
          xy4m = 0.5*(xy(j,k,4)+xy(j,km,4))
          ttm  =  (xy3m*xy(j,k,3)+xy4m*xy(j,k,4))
          
c     -- contribution by holding ( nu + nu_tilde ) constant --
          trem = 0.5*(tpl(j,km) + tpl(j,k))
          trep = 0.5*(tpl(j,k) + tpl(j,kp))
          
          cap  = ttp*trep*difa
          cam  = ttm*trem*difa

          cnub = difb*tpl(j,k)
          cbp  = ttp*cnub
          cbm  = ttm*cnub
          
          bm1(5,j,k) = bm1(5,j,k) + (cbm-cam)/xyj(j,k)*xyj(j,km)
          db(5,j,k)  = db(5,j,k)  - cbp+cap-cbm+cam
          bp1(5,j,k) = bp1(5,j,k) + (cbp-cap)/xyj(j,k)*xyj(j,kp)

c     -- contribution from ( nu + nu_tilde) term --
          cnua = difa/xyj(j,k)
          cap  = ttp*cnua*wk(j,k)
          cam  = ttm*cnua*wk(j,km)
          
          cnub = difb/xyj(j,k)
          cbp  = ttp*cnub*wk(j,k)
          cbm  = ttm*cnub*wk(j,km)
          
          do n = 1,5
            bm1(n,j,k) = bm1(n,j,k) + dnu(n,j,km)*0.5*cam
            db(n,j,k)  = db(n,j,k)  + dnu(n,j,k)*( 0.5*( - cap + cam )
     &            + cbp - cbm)
            bp1(n,j,k) = bp1(n,j,k) - dnu(n,j,kp)*0.5*cap 
          end do
          
        end do
      end do
      
      return
      end                       !dsa_dif
