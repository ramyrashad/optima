             do 112 k=kbegin,kup
                   t=k+g
                do 112 j=jlow,jup
                   a1=xy(j,t,3)
                   a2=xy(j,t,4)
                   cs=sndsp(j,t)**2
c
                   s1=(spect(j,t,2)+spect(j,t,3)-2.0*spect(j,t,1))/2.0
                   s2=(spect(j,t,2)-spect(j,t,3))/
     &                                          (2.*sndsp(j,t)*ccy(j,t))
c
                   u=q(j,t,2)/q(j,t,1)
                   v=q(j,t,3)/q(j,t,1)
                   phi=(u**2+v**2)/2
                   dot1=phi*work(j,k,1,i)
     &                   -u*work(j,k,2,i)
     &                   -v*work(j,k,3,i)
     &                  + work(j,k,4,i)
                   dot2=-vv(j,k)*work(j,k,1,i)
     &                       + a1*work(j,k,2,i)
     &                       + a2*work(j,k,3,i)
c
                   fact1=(s1*dot1*(gami/cs)+s2*dot2)
                   fact2=((s1*dot2)/(a1**2+a2**2)+s2*gami*dot1)

cold                   h=(q(j,t,3)/q(j,t,1))+press(j,t)*xyj(j,t)
c                   h=(q(j,t,4)+press(j,t))/q(j,t,1)
                   temp(j,k,1)=spect(j,k,1)*work(j,k,1,i)
     &                         +fact1
                   temp(j,k,2)=spect(j,k,1)*work(j,k,2,i)
     &                         +fact1*u
     &                         +fact2*a1
                   temp(j,k,3)=spect(j,k,1)*work(j,k,3,i)
     &                         +fact1*v
     &                         +fact2*a2
                   temp(j,k,4)=(abs(vv(j,k)) + ccy(j,k))*
     &                         work(j,k,4,i)/xyj(j,k)
c                   temp(j,k,4)=spect(j,k,1)*work(j,k,4,i)
c     &                         +fact1*h
c     &                         +fact2*vv(j,k)
 112               continue
