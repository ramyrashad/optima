c     ------------------------------------------------------------------
c     -- linearization of copy trailing edge points --
c     -- m. nemec, aug. 2001 --
c     ------------------------------------------------------------------
      subroutine impcop( ijte1, ikte1, ijte2, ikte2, indx1, indx2, nmax,
     &     xyj1, jmax1, kmax1, xyj2, jmax2, kmax2, pa, as, icol,
     &     precmat, jacmat)

      implicit none

      integer ijte1,ikte1,ijte2,ikte2,nmax,jmax1,kmax1,jmax2,kmax2
      integer indx1(jmax1,kmax1), indx2(jmax2,kmax2),icol(*)
      integer jk,jj,n,m
      double precision xyj1(jmax1,kmax1),xyj2(jmax2,kmax2)
      double precision pa(*),as(*)
      logical   precmat,jacmat

c     -- zero previous entries in sparse matrix --
      if (precmat) then
         do n = 1,nmax
            jk = ( indx2(ijte2,ikte2) - 1 )*nmax + n
            jj = (jk-1)*icol(5)
            do m = 1,nmax
               pa(jj+        m) = 0.0
               pa(jj+icol(1)+m) = 0.0
               pa(jj+icol(2)+m) = 0.0
               pa(jj+icol(3)+m) = 0.0
               pa(jj+icol(4)+m) = 0.0
            end do
         end do            

c     -- copy t. e. points --
         do n = 1,nmax
            jk = ( indx2(ijte2,ikte2) - 1 )*nmax + n
            jj = (jk-1)*icol(5)     
            pa(jj+        n) =  xyj2(ijte2,ikte2)
            pa(jj+icol(1)+n) = -xyj1(ijte1,ikte1)
         end do
      end if

      if (jacmat) then
         do n = 1,nmax
            jk = ( indx2(ijte2,ikte2) - 1 )*nmax + n
            jj = (jk-1)*icol(9)
            do m = 1,nmax
               as(jj+        m) = 0.0
               as(jj+icol(1)+m) = 0.0
               as(jj+icol(2)+m) = 0.0
               as(jj+icol(3)+m) = 0.0
               as(jj+icol(4)+m) = 0.0
               as(jj+icol(5)+m) = 0.0
               as(jj+icol(6)+m) = 0.0
               as(jj+icol(7)+m) = 0.0
               as(jj+icol(8)+m) = 0.0
            end do
         end do            

c     -- copy t. e. points --
         do n = 1,nmax
            jk = ( indx2(ijte2,ikte2) - 1 )*nmax + n
            jj = (jk-1)*icol(9)     
            as(jj+        n) =  xyj2(ijte2,ikte2)
            as(jj+icol(1)+n) = -xyj1(ijte1,ikte1)
         end do
      end if

      return
      end                       !impcop
