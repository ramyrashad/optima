c********************************************************
c*        Spalart-Allmaras one equation model           *
c********************************************************
c
      subroutine spalart_allmaras(iblk,jmax,kmax,
     &   jtlo,jtup,
     &   jbegin,jend,kbegin,kend,jlow,jup,klow,kup,
     &   q,turmu,x,y,xy,xyj,
     &   press,vorticity,turre,fnu,uu,vv,
     &   work,bx,cx,dx,fx,by,cy,dy,fy,ffy,fby,ffx,
     &   smin,kwall,jmid,bnd,first,
     &   workq,p,d,trip,d_prime,p_prime,d_p_bar,p1,d1,turre1,denom,
     &   tmpx,tmpy,workph,itrans,itranj,itranside,nhalo,
     &   jminbnd,jmaxbnd,kminbnd,kmaxbnd,
c     &   nfoils,xte,yte,
     &   nsd1,nsd2,nsd3,nsd4,
     &   icoord,xcoord,ycoord)
c
c     One equation turbulence model of Spalart-Allmaras
c     coded by     : Philippe Godin 06/97
c     revision: 01/01/2000 by Stan De Rango
c     revision: 2002 by M. Nemec
c
c     
#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/units.inc"
#include "../include/sam.inc"
#include "../include/trip.inc"
c     
      dimension jlocat(maxj),work2(maxj,4)
      common/worksp/jlocat,work2
c
      dimension q(jmax,kmax,4),turmu(jmax,kmax),vorticity(jmax,kmax)
      dimension xy(jmax,kmax,4),xyj(jmax,kmax)
      dimension fnu(jmax,kmax),press(jmax,kmax)
c      dimension smin1(maxj,maxk),smin3(maxj,maxk)
      dimension kwall(jmax,kmax),smin(jmax,kmax)
      dimension x(jmax,kmax),y(jmax,kmax)
      dimension uu(jmax,kmax),vv(jmax,kmax),turre(jmax,kmax)
      dimension itrans(maxblk),itranj(maxblk,2),itranside(maxblk,2)
      dimension xcoord(maxjfoil),ycoord(maxjfoil)
c      dimension xte(maxfoil),yte(maxfoil)
c     
c     
c      dimension 
c     &     p(maxjb,maxkb),d(maxjb,maxkb),
c     &     trip(maxjb,maxkb),d_prime(maxjb,maxkb),
c     &     p_prime(maxjb,maxkb),d_p_bar(maxjb,maxkb),
c     &     p1(maxjb,maxkb),d1(maxjb,maxkb),
c     &     turre1(maxjb,maxkb)
      dimension 
     &     p(jmax,kmax),d(jmax,kmax),
     &     trip(jmax,kmax),d_prime(jmax,kmax),
     &     p_prime(jmax,kmax),d_p_bar(jmax,kmax),
     &     p1(jmax,kmax),d1(jmax,kmax),
     &     turre1(jmax,kmax)
c     
      dimension
     &     work(jmax,kmax),workq(jmax,kmax,3),denom(jmax,kmax),
     &     tmpx(jmax,kmax),tmpy(jmax,kmax),workph(jmax,kmax)
c     
      dimension   
     &     bx(kmax,jmax),cx(kmax,jmax),dx(kmax,jmax),
     &     by(jmax,kmax),cy(jmax,kmax),dy(jmax,kmax),
     &     fy(jmax,kmax),fx(kmax,jmax),
     &     ffy(jmax,kmax),ffx(kmax,jmax),fby(jmax,kmax)

c     
      logical bnd(4),first

c     *********************************************************************
c     - trip_present(blk#) =>  set to true if block contains trip point

c     - multi_trans(blk #) =>  set to true it there are more than one
c                              trip points set for the same side of a block

c     - itrans(blk #)      =>  number of trip points for given block

c     - itranside(blk #,1) =>  side that trip belongs to for lower
c                              surface trip point

c     - itranside(blk #,2) =>  side that trip belongs to for upper
c                              surface trip point
c     *********************************************************************

      reinv=1.0/re
      resiginv= sigmainv*reinv
      const   =(1.d0+cw3_6)**expon

      dtm=dtsp

c     *****************************************************
c     -note: fnu contains rho/mu .. ie.inverse of nu=mu/rho
c     *****************************************************

      do 2 k=kbegin,kend
      do 2 j=jbegin-nsd2,jend+nsd4
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        workq(j,k,1) = rho
        workq(j,k,2) = u
        workq(j,k,3) = v
 2    continue
c
c     
      if (first) then     
c       -compute generalized distance function
        if (matcharc2d) then
          if (iblk.eq.2) then
            do 3 k=klow,kup
            do 3 j=jlow,jup
              xpt1=x(j,kminbnd)
              ypt1=y(j,kminbnd)
              smin(j,k)=sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
 3          continue
          else
            if (iblk.eq.3) then
              xte1=x(jminbnd,kminbnd)
              yte1=y(jminbnd,kminbnd)
            else
c             -iblk=1             
              xte1=x(jmaxbnd,kminbnd)
              yte1=y(jmaxbnd,kminbnd)
            endif
            do 4 k=klow,kup
            do 4 j=jlow,jup
              smin(j,k)=sqrt((x(j,k)-xte1)**2+(y(j,k)-yte1)**2)
 4          continue
          endif
        else
          do 5 k=klow,kup
          do 5 j=jlow,jup
            dmin=1.e6
            do ic=1,icoord
              ss=sqrt((x(j,k)-xcoord(ic))**2+(y(j,k)-ycoord(ic))**2)
              if (ss.lt.dmin) dmin=ss
            enddo
c            if (dmin.eq.0.) dmin=1.e-30
            smin(j,k)=dmin
c            write (*,*) iblk,j,k,smin(j,k)
c            smin(j,k)=.01
 5        continue
        endif
c
c       June 1999
c       S. DeRango  - One day I'd like to come back and clean
c                     up this upper-surface and lower-surface
c                     transition stuff.  When I first coded it up
c                     I thought I would one day need to distinguish
c                     if the trip point was on the upper or lower
c                     surface.  It turns out you don't ... only
c                     adds a little confusion to the logic.

        multi_trans(iblk)=.false.
        if (itrans(iblk).gt.0) then
c          -there is at least one transition point
          trip_present(iblk)=.true.
          jtran1=itranj(iblk,1)
          jtran2=itranj(iblk,2)
          if (jtran1.gt.0) then
c           -the trans. point is on lower surface
            jtranlo1=jtran1
          else
            jtranlo1=-99
          endif
          if (jtran2.gt.0) then
c           -the trans. point is on upper surface
            jtranup1=jtran2
          else
            jtranup1=-99
          endif
c         -checking to see if there are multiple transition points per
c          block is not as simple as checking if itrans(iblk).gt.1.
c         -what about the case where a block has both sides 1 & 3 touching
c          an airfoil surface (ie. side3 touching slat and side1 touching 
c          main element and both sides having a trip point)
          if (itrans(iblk).gt.1) then
            if (bnd(1).and.bnd(3)) then
              multi_trans(iblk)=.false.
            else
              multi_trans(iblk)=.true.
            endif
          endif
        else
          trip_present(iblk)=.false.
          jtranup1=-99
          jtranlo1=-99
        endif
        jtlo=jtranlo1
        jtup=jtranup1
c
c       calculate distance to closest wall.
        if (trip_present(iblk)) then
          do 10 k=klow,kup
          do 10 j=jlow,jup
            if (multi_trans(iblk)) then
c             -side 1 always touches airfoil.
              kwall(j,k)=kbegin
            else
              if (itrans(iblk).eq.1) then
                if (jtlo.gt.0) then
c                 lower surface
                  ii=1
                else
c                 upper surface
                  ii=2
                endif
                if (itranside(iblk,ii).eq.3) then
                  kwall(j,k)=kend
                elseif(itranside(iblk,ii).eq.1) then
                  kwall(j,k)=kbegin
                endif
              else
c               -we have 1 transition point on side 3 for one element
c                and 1 transition pt. on side 1 for another element
c               -choose closest wall                  
c
c               -distance to side 1
                sx=x(j,k)-x(j,kbegin)
                sy=y(j,k)-y(j,kbegin)
                dis1=sqrt(sx**2 + sy**2)
c               -distance to side 3
                sx=x(j,k)-x(j,kend)
                sy=y(j,k)-y(j,kend)
                dis2=sqrt(sx**2 + sy**2)
c
                if (dis1.le.dis2) then
                  kwall(j,k)=kbegin
                else 
                  kwall(j,k)=kend
                endif
              endif
            endif
 10       continue
        endif
c
c
        if (multi_trans(iblk).and.trip_present(iblk)) then
c         S.D
c         -the assumption made here is that if multi_trans=true
c          ... then only side 1 forms part of airfoil boundary
c          ... hence kbegin is used as index for x and y location.
          chord=0.0
          fmin=100.0
          if (jtlo.le.0) jtlo=2
          if (jtup.le.0) jtup=2
          do 11 jj=jtlo,jtup-1
            work(jj,kbegin)=sqrt((x(jj,kbegin)-x(jj+1,kbegin))**2
     &            +(y(jj,kbegin)-y(jj+1,kbegin))**2)
            chord=chord+work(jj,kbegin)
 11       continue
c        
          chord_2=.5d0*chord
          chord=0.0
          do 12 jj=jtlo,jtup-1
            chord=chord+work(jj,kbegin)
            if (abs(chord-chord_2).lt.fmin) then
              fmin=abs(chord-chord_2)
              jmid=jj+1
            endif
 12       continue
        endif
c     
        if ((.not.restart .and. numiter.eq.1) .or.
     &        (restart .and. zeroturre)) then
          do 16 k=kbegin,kend
          do 16 j=jbegin,jend
            turre(j,k)=retinf
 16       continue
        endif
c     
        if (.not.restart .or. (restart.and.zeroturre)) then
          if (bnd(1)) then
            do j=jlow,jup
              turre(j,kbegin) = 0.0
            enddo
          endif
          if (bnd(3)) then
            do j=jlow,jup
              turre(j,kend)=0.0
            enddo
          endif
c
c         Stan D.R. 02/10/98
c         -following necessary for blunt trailing edge
          if (bnd(2)) then
            do j=klow,kup
              turre(jbegin,k) = 0.0
            enddo
          endif
          if (bnd(4)) then
            do j=klow,kup
              turre(jend,k)=0.0
            enddo
          endif
        endif
c     
csd   -this is the endif for the 'if (first) then' statement.
      endif
c     
c ************************************************************************
c                            Now the Model                      
c ************************************************************************
c     
      if (trip_present(iblk)) then
c       -provide loop indices for vorticity calc. on airfoil surface
        if (multi_trans(iblk)) then
          jvort1=jtlo
          jvort2=jtup
c
          do j=jlow,jup
            if (j.le.jmid) then
              jlocat(j)=jtlo
            else
              jlocat(j)=jtup
            endif
          enddo
        else
c         -not multiple transitions in block
csd       I realize that you don't really need to know weather you
c         are dealing with an upper-surface transition point or a
c         lower-surface transition point.  Doing so requires more if
c         statements ... but thats the way it is for now
          if (itranj(iblk,1).gt.0) then
            jvort1=jtlo
            jvort2=jvort1
            do j=jlow,jup
              jlocat(j)=jtlo
            enddo
          else
            jvort2=jtup
            jvort1=jvort2
            do j=jlow,jup
              jlocat(j)=jtup
            enddo
          endif
        endif
      endif
c
c
csd   Note: Philippe Godin computes only the eta derivatives for
c           vorticity.  To match ARC2D I added the xi derivatives aswell.
c
      if (iord.eq.2) then 
        call vort_o2(iblk,jmax,kmax,jvort1,jvort2,
     &        jlow,jup,klow,kup,kbegin,kend,bnd,workq,xy,vorticity)
      else
c       -iord=4
        call vort_o4(iblk,jmax,kmax,jvort1,jvort2,jlow,jup,
     &        klow,kup,kbegin,kend,nsd2,nsd4,bnd,workq,xy,vorticity)
      endif
c     
c
      do 50 k=klow,kup
      do 50 j=jlow,jup
        zz=1.d0/(akarman*smin(j,k))**2
        chi_prime=fnu(j,k)
        chi=turre(j,k)*chi_prime

        z=chi**2/(chi**3+cv1_3)
        fv1=z*chi
        fv1_prime=3.0*z*chi_prime*(1.0-fv1)
        s=vorticity(j,k)*re

        z=1.0/(1.0+chi*fv1)
        fv2=1.0-chi*z
        fv2_prime=z*(-chi_prime +
     &        chi*(chi_prime*fv1+fv1_prime*chi)*z)
        s_tilda=s+turre(j,k)*fv2*zz
        s_tilda_prime=zz*(fv2+turre(j,k)*fv2_prime)

        z=zz/s_tilda
        r=turre(j,k)*z
        r_prime=z-r/s_tilda*s_tilda_prime

        if (abs(r).ge.10.0) then
          fw=const
          fw_prime=0.0
        else
          g=r+cw2*(r**6-r)
          z=((1.0+cw3_6)/(g**6+cw3_6))**expon
          g_prime=r_prime+cw2*(6.0*r**5*r_prime-r_prime)
          fw=g*z
          fw_prime=g_prime*z*(1.-g**6/(g**6+cw3_6))
        endif

c     -- mn: ftfac is set in input.f for fully turbulent or transition
c     cases --
        ft2=ftfac*ct3*exp(-ct4*chi**2)
        ft2_prime=-ct4*2.0*chi*chi_prime*ft2
c     
        p1(j,k)=cb1*(1.0-ft2)*s_tilda*reinv
        p(j,k)=p1(j,k)*turre(j,k)
        p_prime(j,k)=cb1*(-ft2_prime*s_tilda+
     &        (1.0-ft2)*s_tilda_prime)*reinv
c     
        z=cb1/akarman**2
        z1=1.0/smin(j,k)**2
        z2=cw1*fw_prime-z*ft2_prime
        d1(j,k)=(cw1*fw-z*ft2)*z1*reinv
        d_prime(j,k)=turre(j,k)*z1*z2*reinv + d1(j,k)
        d1(j,k)=d1(j,k)*turre(j,k)
        d(j,k)=d1(j,k)*turre(j,k)
 50   continue

      if (trip_present(iblk)) then
        do 52 k=klow,kup
        do 52 j=jlow,jup
          jloc=jlocat(j)
c     
          kk=kwall(j,k)
          d_trip=sqrt((x(j,k)-x(jloc,kk))**2+(y(j,k)-y(jloc,kk))**2)
          delta_x=0.5*(sqrt((x(jloc+1,kk)-x(jloc-1,kk))**2
     &                       +(y(jloc+1,kk)-y(jloc-1,kk))**2))
c     
          omega_t=vorticity(jloc,kk)
c     
          delta_u=sqrt((workq(j,k,2)-workq(jloc,kk,2))**2+
     &          (workq(j,k,3)-workq(jloc,kk,3))**2)
          if (delta_u.le.1.e-50) delta_u=1.e-50
c     
          g_t=min(0.1,delta_u/(omega_t*delta_x))
          factor=(ct2*(omega_t/delta_u)**2*(
     &          smin(j,k)**2+(g_t*d_trip)**2))
          if (factor.le.103.0) then
c     -- mn: ftfac is set in input.f for fully turbulent or transition
c     cases --
            ft1=ftfac*ct1*g_t*exp(-factor)
            trip(j,k)=ft1*re*delta_u**2
          else
            trip(j,k)=0.0
          endif
 52     continue
      else
        do 53 k=klow,kup
        do 53 j=jlow,jup
          trip(j,k)=0.0
 53     continue
      endif
c         
      do 55 k=kbegin,kend
      do 55 j=jbegin,jend
        work(j,k)=1.0/fnu(j,k)+turre(j,k)
 55   continue
c     
c 
c     -Advective and diffusive terms       
      iordtmp=iord
      iord=2
      call eta_visc(jmax,kmax,jlow,jup,klow,kup,cb2,resiginv,
     &                  turre,xy,work,by,cy,dy,fy,tmpx,tmpy,workph)
      iord=iordtmp
      call eta_adv(jmax,kmax,jlow,jup,klow,kup,vv,
     &                                  turre,xy,workq,by,cy,dy,fy)

      call xi_visc(jmax,kmax,jlow,jup,klow,kup,cb2,resiginv,
     &                                   turre,xy,work,bx,cx,dx,fy)

      call xi_adv(jmax,kmax,jlow,jup,klow,kup,uu,turre,bx,cx,dx,fy)

csd **************************************************************************
csd                       Time Marching in Turbulence Model
csd **************************************************************************

c 777  tt=etime(tarray)
c      time3=tarray(1)
c      time4=tarray(2)
c
      do 75 k=klow,kup
      do 75 j=jlow,jup
c     
        d_p_bar(j,k)=(max(d1(j,k)-p1(j,k),0.0)+
     &                max(d_prime(j,k)-p_prime(j,k),0.0)*turre(j,k))
c     
        denom(j,k)=dtm/(1.0+dtm*(cx(k,j)+cy(j,k)+d_p_bar(j,k)))
        bx(k,j) = bx(k,j)*denom(j,k)
        cx(k,j) = 1.0
        dx(k,j) = dx(k,j)*denom(j,k)
        by(j,k) = by(j,k)*denom(j,k)
        cy(j,k) = 1.0
        dy(j,k) = dy(j,k)*denom(j,k)
 75   continue
c      
      do 77 j=jbegin,jend
      do 77 k=kbegin,kend
        fx(k,j)=0.0
 77   continue
c     
      if (nnit_sp.eq.1) then
        do 80 k=klow,kup
        do 80 j=jlow,jup
c     
          tt=(p(j,k)-d(j,k))+trip(j,k)
c            
          fy(j,k) = (fy(j,k)+tt)*denom(j,k)
 80     continue
c     
        call triv(jmax,kmax,jlow,jup,klow,kup,work,by,cy,dy,fy)
c      
        do 81 k=klow,kup
        do 81 j=jlow,jup
          fx(k,j) = fy(j,k)
 81     continue
c     
        call triv(kmax,jmax,klow,kup,jlow,jup,work,bx,cx,dx,fx)
c     
        neqn = 0
        do 85 k=klow,kup
        do 85 j=jlow,jup
          tmp= turre(j,k) + fx(k,j)
          if(tmp.lt.0.0)then
            neqn        =   neqn + 1
            turre(j,k)  =   turre(j,k)*.5d0
          else
            turre(j,k)=tmp
          endif
 85     continue
c     
      else
c
c       ********************************************************
c             Subiterations to Reduce Factorisation Error
c       ********************************************************
c
        isub=1
        resid_max=1.d-2
        residual=1.d2
c     
        do 78 k=kbegin,kend
        do 78 j=jbegin,jend
          turre1(j,k)=turre(j,k)
          ffy(j,k)=0.0
 78     continue
c     
        do 79 j=jbegin,jend
        do 79 k=kbegin,kend
          ffx(k,j)=0.0
 79     continue
c
        do 500 while ((residual.gt.resid_max) .and.
     &                                      (isub.le.nnit_sp))
          do 90 k=klow,kup
          do 90 j=jlow,jup
c     
            tt=(p(j,k)-d(j,k))+trip(j,k)
c            
            fby(j,k) = (fy(j,k)+tt)*denom(j,k)
     &            + by(j,k)*(ffy(j,k-1)-fx(k-1,j))
     &            + dy(j,k)*(ffy(j,k+1)-fx(k+1,j))
 90       continue
c     
          call triv(jmax,kmax,jlow,jup,klow,kup,work,by,cy,dy,fby)
          
          do 91 k=klow,kup
          do 91 j=jlow,jup
            fx(k,j) = fby(j,k)
            ffy(j,k) = fby(j,k)
 91       continue
c     
          call triv(kmax,jmax,klow,kup,jlow,jup,work,bx,cx,dx,fx)
c     
          neqn = 0
          resid_term1=0.0
          resid_term2=0.0
c     
          do 95 k=klow,kup
          do 95 j=jlow,jup
            turre(j,k) = turre1(j,k) + fx(k,j)
            if(turre(j,k).lt. 0.0)then
              neqn        =   neqn + 1
              turre(j,k)  =   turre1(j,k)*.5d0
c              fx(k,j)     =    0.0
            endif
            resid_term1=resid_term1+(fx(k,j)-ffx(k,j))**2
            resid_term2=resid_term2+(fx(k,j))**2
 95       continue
c     
          do 96 j=jlow,jup
          do 96 k=klow,kup
            ffx(k,j)=fx(k,j)
 96       continue
c     
          residual1=sqrt(resid_term1)
          residual2=sqrt(resid_term2)
          residual=residual1/max(residual2,1.e-10)
c
          isub=isub+1
 500    continue
      endif

c     mn 
c     -- added division by xyj in res=fx(k,j)**2 line and tresid2 used
c     in computing total residual in residl2.f --

      res_spl=0.0
      resmx=0.0

      j1 = jlow
      j2 = jup
      k1 = kminbnd+1
      k2 = kmaxbnd-1
      
      do j=j1,j2
         do k=k1,k2
            res=fx(k,j)**2/xyj(j,k)
            res_spl=res_spl + res
            if (res.gt.resmx) then
               resmx=res
               mxtur(1)=j-nhalo
               mxtur(2)=k-nhalo
               mxtur(3)=iblk
            endif
         end do
      end do

      tresid2 = tresid2 + res_spl 
      res_spl = sqrt(res_spl/float((j2-j1+1)*(k2-k1+1)))
      tresid  = tresid+res_spl
      resmx   = sqrt(resmx)

      if (resmx.gt.tresmx) tresmx=resmx

      if (neqn.ne.0) then
        write(n_out,*)'Negative updates in SP.=>',numiter,iblk,neqn
      endif

c     Stan/Luis 24/09/98
c     Note: turre is not updated at the farfield boundary, including 
c           outflow.  It is done so in bcupdate.f when bcfarall.f is
c           called.  Doing it this way might not be a good thing.
c           Turre at the boundaries is used in formulating turmu ...
c           so when you use the boundary values below you are using old
c           values ... there is a "one iteration" lag in the updates.

c      do 90 k=kbegin,kup
c      do 90 j=jbegin,jend
c         chi=turre(j,k)/fnu(j,k)
c         fv1=chi**3/(chi**3+cv1_3)
c         chi_1=turre(j,k+1)/fnu(j,k+1)
c         fv1_1=chi_1**3/(chi_1**3+cv1_3)
c         fv1_c=0.5*(fv1+fv1_1)
c         turmu(j,k) = fv1_c*.5*(turre(j,k)*workq(j,k,1)
c     &                                 + turre(j,k+1)*workq(j,k+1,1))
c 90   continue
c
 999  return
      end
