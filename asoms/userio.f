c
c     -- user input routines with prompting and error trapping --
c     
c     -- taken from MSES by M. Drela --
c     -- mods and adjustments: M . Nemec --

      SUBROUTINE ASKI(PROMPT,IINPUT)

c     -- integer input --

      CHARACTER*(*) PROMPT
      INTEGER IINPUT

      NP = INDEX(PROMPT,'^') - 1
      IF(NP.EQ.0) NP = LEN(PROMPT)

 10   WRITE(*,1000) PROMPT(1:NP)
      READ (*,*,ERR=10) IINPUT
      RETURN

 1000 FORMAT(/3x, A,'   i>  ',$)
      END ! ASKI


      SUBROUTINE ASKR(PROMPT,RINPUT)

c     -- real input --

      CHARACTER*(*) PROMPT
      REAL RINPUT

      NP = INDEX(PROMPT,'^') - 1
      IF(NP.EQ.0) NP = LEN(PROMPT)

 10   WRITE(*,1000) PROMPT(1:NP)
      READ (*,*,ERR=10) RINPUT
      RETURN

 1000 FORMAT(/3x, A,'   r>  ',$)
      END ! ASKR


      SUBROUTINE ASKL(PROMPT,LINPUT)

c     -- logical input --

      CHARACTER*(*) PROMPT
      LOGICAL LINPUT
      CHARACTER*1 CHAR

      NP = INDEX(PROMPT,'^') - 1
      IF(NP.EQ.0) NP = LEN(PROMPT)

 10   WRITE(*,1000) PROMPT(1:NP)
      READ (*,1010) CHAR
      IF(CHAR.EQ.'y') CHAR = 'Y'
      IF(CHAR.EQ.'n') CHAR = 'N'
      IF(CHAR.NE.'Y' .AND. CHAR.NE.'N') GO TO 10

      LINPUT = CHAR .EQ. 'Y'
      RETURN

 1000 FORMAT(/3x, A,' y/n>  ',$)
 1010 FORMAT(A)
      END ! ASKL


      SUBROUTINE ASKS(PROMPT,INPUT)

c     -- string of arbitrary length input --

      CHARACTER*(*) PROMPT
      CHARACTER*(*) INPUT

      NP = INDEX(PROMPT,'^') - 1
      IF(NP.EQ.0) NP = LEN(PROMPT)

      WRITE(*,1000) PROMPT(1:NP)
      READ (*,1010) INPUT

      RETURN

 1000 FORMAT(/A,'   s>  ',$)
 1010 FORMAT(A)
      END ! ASKS


      SUBROUTINE READI(N,IVAR,ERROR)
      DIMENSION IVAR(N)
      LOGICAL ERROR

c     -- reads N integer variables, leaving unchanged 
c     if only <return> is entered --

      DIMENSION IVTMP(40)
      CHARACTER*80 LINE

      READ(*,1000) LINE
 1000 FORMAT(A80)

      DO 10 I=1, N
        IVTMP(I) = IVAR(I)
 10   CONTINUE

      CALL GETINT(LINE,IVTMP,N,ERROR)

      IF(ERROR) RETURN

      DO 20 I=1, N
        IVAR(I) = IVTMP(I)
 20   CONTINUE

      RETURN
      END ! READI



      SUBROUTINE READR(N,VAR,ERROR)
      DIMENSION VAR(N)
      LOGICAL ERROR

c     -- reads N real variables, leaving unchanged 
c     if only <return> is entered --

      DIMENSION VTMP(40)
      CHARACTER*80 LINE

      READ(*,1000) LINE
 1000 FORMAT(A80)

      DO 10 I=1, N
        VTMP(I) = VAR(I)
 10   CONTINUE

      NTMP = 40
      CALL GETFLT(LINE,VTMP,NTMP,ERROR)

      IF(ERROR) RETURN

      DO 20 I=1, N
        VAR(I) = VTMP(I)
 20   CONTINUE

      RETURN
      END ! READR


      SUBROUTINE GETINT(INPUT,A,N,ERROR)
      CHARACTER*80 INPUT
      INTEGER A(*)
      LOGICAL ERROR

c     -- parses character string INPUT into an array
c     of integer numbers returned in A(1...N) --

      CHARACTER*82 REC

      REC = INPUT//' ,'
      N = 0

      K = 1
      DO 10 IPASS=1, 80
        KSPACE = INDEX(REC(K:82),' ') + K - 1
        KCOMMA = INDEX(REC(K:82),',') + K - 1

        IF(K.EQ.KSPACE) THEN
         K = K+1
         GO TO 9
        ENDIF

        IF(K.EQ.KCOMMA) THEN
         N = N+1
         K = K+1
         GO TO 9
        ENDIF

        N = N+1
        K = MIN0(KSPACE,KCOMMA) + 1

  9     IF(K.GE.80) GO TO 11
 10   CONTINUE

 11   READ(REC(1:80),*,ERR=20) (A(I),I=1,N)
      ERROR = .FALSE.
      RETURN

 20   CONTINUE
ccc   WRITE(*,*) 'GETINT: String-to-integer conversion error.'
      N = 0
      ERROR = .TRUE.
      RETURN
      END


      SUBROUTINE GETFLT(INPUT,A,N,ERROR)
      CHARACTER*80 INPUT
      REAL A(*)
      LOGICAL ERROR

c     -- parses character string INPUT into an array
c     of real numbers returned in A(1...N) --

      CHARACTER*82 REC

      REC = INPUT//' ,'
      N = 0

      K = 1
      DO 10 IPASS=1, 80
        KSPACE = INDEX(REC(K:82),' ') + K - 1
        KCOMMA = INDEX(REC(K:82),',') + K - 1

        IF(K.EQ.KSPACE) THEN
         K = K+1
         GO TO 9
        ENDIF

        IF(K.EQ.KCOMMA) THEN
         N = N+1
         K = K+1
         GO TO 9
        ENDIF

        N = N+1
        K = MIN0(KSPACE,KCOMMA) + 1

  9     IF(K.GE.80) GO TO 11
 10   CONTINUE

 11   READ(REC(1:80),*,ERR=20) (A(I),I=1,N)
      ERROR = .FALSE.
      RETURN

 20   CONTINUE
ccc   WRITE(*,*) 'GETFLT: String-to-real conversion error.'
      N = 0
      ERROR = .TRUE.
      RETURN
      END
