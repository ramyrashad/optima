      subroutine expmatx(jdim,kdim,q,coef2x,coef4x,sndsp,s,
     > spect,xyj,press,ccx,uu,xy,x,y,work,s1,s2,u,v,phi,h,temp,temp2,
     > specthalf)
c
      include '../include/arcom.inc'

      integer g,t,tm1
      dimension q(jdim,kdim,4),coef2x(jdim,kdim),coef4x(jdim,kdim)
      dimension sndsp(jdim,kdim),s(jdim,kdim,4),work(4,jdim,kdim,2)
      dimension spect(jdim,kdim,3),xyj(jdim,kdim),press(jdim,kdim)
      dimension ccx(jdim,kdim),uu(jdim,kdim),xy(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim),specthalf(jdim,kdim,3)
      dimension temp(4,jdim,kdim),temp2(4,jdim,kdim)
      dimension s1(jdim,kdim),s2(jdim,kdim),u(jdim,kdim),v(jdim,kdim)
      dimension phi(jdim,kdim),h(jdim,kdim),up(4),upp(4)
c
      dtd=dt/(1.+phidt)
c     (q(j+1)-q(j))
      do 25 i=1,4
         do 14 k=klow,kup
         do 14 j=jbegin,jup
            jp1=jplus(j)
            work(i,j,k,1)= q(jp1,k,i)*xyj(jp1,k) - q(j,k,i)*xyj(j,k)
 14      continue
c            
         if (.not.periodic) then
            do 15 k=klow,kup
               work(i,jend,k,1)=work(i,jend-1,k,1)
 15         continue
         endif
c     
c        (q(j+2)-3q(j+1)+3q(j)-q(j-1))
         do 18 k=klow,kup
         do 18 j=jlow,jup
          jp1=jplus(j)
          jm1=jminus(j)
          work(i,j,k,2)=work(i,jp1,k,1)-2.*work(i,j,k,1)+work(i,jm1,k,1)
  18     continue
c      
c        boundary conditions a la Kyle
         if (.not.periodic) then
            do 20 k=klow,kend
               work(i,jbegin,k,2)=q(jbegin+2,k,i)*xyj(jbegin+2,k)
     &                            -2.d0*q(jbegin+1,k,i)*xyj(jbegin+1,k)
     &                            +q(jbegin,k,i)*xyj(jbegin,k)
               work(i,jend,k,2)=0.d0
 20         continue
         endif
 25   continue
c
c
      do 50 n=1,3
      do 50 k=klow,kup
      do 50 j=jbegin,jup
        specthalf(j,k,n)=.5d0*(spect(j,k,n)+spect(j+1,k,n))
 50   continue

      do 51 k=klow,kup
      do 51 j=jbegin,jup
        jp1=j+1
        up(1)=q(j,k,1)*xyj(j,k)
        upp(1)=q(jp1,k,1)*xyj(jp1,k)
c
        up(2)=q(j,k,2)/q(j,k,1)
        upp(2)=q(jp1,k,2)/q(jp1,k,1)
c
        up(3)=q(j,k,3)/q(j,k,1)
        upp(3)=q(jp1,k,3)/q(jp1,k,1)
c
        up(4)=gami*(q(j,k,4)*xyj(j,k) - 0.5d0*up(1)*(up(2)**2+up(3)**2))
        upp(4)=gami*(q(jp1,k,4)*xyj(jp1,k) - 
     &                               0.5d0*upp(1)*(upp(2)**2+upp(3)**2))
 51   continue

      do 100 k=klow,kup
      do 100 j=jbegin,jend
c           -calc Roe's average using primitive variables
c            (see pg 465 of Hirsh Vol.2)
         he = gamma*upp(4)/gami/upp(1) + 
     &                                0.5d0*(upp(2)**2+upp(3)**2)
         hi = gamma*up(4)/gami/up(1) + 
     &                                0.5d0*(up(2)**2+up(3)**2)
c        
         sre    = dsqrt(upp(1))
         sri    = dsqrt(up(1))
         denom  = sre + sri
         rhoa   = sre*sri
         ua     = (upp(2)*sre + up(2)*sri)/denom
         va     = (upp(3)*sre + up(3)*sri)/denom
         ha     = (he*sre + hi*sri)/denom
         ca     = dsqrt( gami*(ha - 0.5d0*(ua**2 + va**2)) )
c
         s1(j,k)=.5d0*(specthalf(j,k,2)+specthalf(j,k,3)-
     &                                   2.*specthalf(j,k,1))
         s2(j,k)=(specthalf(j,k,2)-specthalf(j,k,3))/
     &                                       (2.*ca*ccx(j,k))
         u(j,k)=ua
         v(j,k)=va
         phi(j,k)=.5d0*(u(j,k)**2+v(j,k)**2)         
         h(j,k)=ha
 100  continue
c
c     -if you didn't have to deal with periodic flows you could
c      just use t=j+g instead of t=jplus(j+g-1).
      zdisx=0.d0
      do 200 g=0,0
         do 110 k=klow,kup
         do 110 j=jbegin,jup
            t=jplus(j+g-1)
            include 'matloopx.inc'
 110     continue
c
         do 150 n=1,4
            do 120 k=klow,kup
            do 120 j=jlow,jup
               t=jplus(j+g-1)
               tp1=jplus(t)
               tm1=jminus(t)               
               jm1=jminus(j)
c
c               c2=coef2x(t,k)+coef2x(tp1)
c               c4=coef4x(t,k)+coef4x(tp1)
               s(j,k,n)=s(j,k,n) + dtd*(
     &         coef2x(t,k)*temp(n,j,k)-coef2x(tm1,k)*temp(n,jm1,k)
     &       - (coef4x(t,k)*temp2(n,j,k)-coef4x(tm1,k)*temp2(n,jm1,k)))
 120        continue
 150     continue
 200  continue
c      zdisx=dsqrt(zdisx/dble((jup-jlow+1)*(kup-klow+1)))
c      if (numiter.eq.iend) 
c     &write (*,*) 'Xi density diss. norm =',zdisx
c      
      return
      end
