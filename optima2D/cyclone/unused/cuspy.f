      subroutine cuspy(jdim,kdim,q,coef2y,coef4y,sndsp,s,
     >spect,xyj,press,ccy,vv,xy,x,y)
c
c     prototype matrix dissipation subroutine
c     january 5, 1995
c
c
c
      include '../include/arcom.inc'
c
c **************************************************************
c may.95  budget() added by kyle frew
c ***************************************************************
       include 'budget.inc'


       dimension q(jdim,kdim,4),coef2y(jdim,kdim),coef4y(jdim,kdim),
     &           sndsp(jdim,kdim),s(jdim,kdim,4),work(maxj,maxk,4,2),
     &           spect(maxj,maxk,3),xyj(jdim,kdim),press(jdim,kdim),
     &      ccy(jdim,kdim),vv(jdim,kdim),xy(jdim,kdim,4),
     &      x(jdim,kdim),y(jdim,kdim),alph(maxj,maxk),beta(maxj,maxk)

       double precision u,s1,s2,cs,dot1,dot2,fact1,fact2,
     &      temp(maxj,maxk,4),phi,a1,a2,h,dtd,
     &      temp2(maxj,maxk,4)
       integer i,j,k,g,l,t,k1,k2
c
c ** k.frew june 1995
c     
       do i=1,4
       do k=kbegin,kend
          do j=jbegin,jend
             temp2(j,k,i)=0.0
             budget2(j,k,2)=0.0
          end do
       end do
       end do
c
c ** end of k.frew 
c
       dtd=dt/(1. +phidt)
       do 11 i=1,4
          do 14 k=kbegin,kup
             kp=k+1
             do 14 j=jlow,jup
                work(j,k,i,1)= q(j,kp,i)*xyj(j,kp)
     &                        -q(j,k,i) *xyj(j,k)
 14       continue

          do 15 j=jlow,jup
             work(j,kend,i,1)=work(j,kup,i,1)
 15       continue

          do 18 k=klow,kup-1
             kp=k+1
             km=k-1
             do 18 j=jlow,jup
                work(j,k,i,2)=     work(j,kp,i,1)
     &                         -2.*work(j,k,i,1)
     &                            +work(j,km,i,1)
 18    continue
c       
c   boundary conditions big time
c234567
       k1=kbegin
       k2=kup
       do 20 j=jlow,jup
c first order
          work(j,k1,i,2)=    q(j,k1+2,i)*xyj(j,k1+2)
     &                   -2.*q(j,k1+1,i)*xyj(j,k1+1)
     &                      +q(j,k1,i)  *xyj(j,k1)
          work(j,k2,i,2)=work(j,k2-1,i,1)-work(j,k2,i,1)
 20    continue

 11    continue
c
c calculate cusp coefficients alph and beta
c     frew october 95 (non-curvilinear co-ordinates)!!
c
c       do k=kbegin,kup
c          do j=jbegin,jend
c             dot1=spect(j,k,3)
c             dot2=spect(j,k,2)
c             s2=q(j,k,3)/q(j,k,1)
c             s1=s2/sndsp(j,k)
c             alph(j,k)=abs(s1)
c     subsonic
c             if ((s1.ge.0).and.(s1.le.1)) then
c                beta(j,k)=max(0,2*s1-1)/2.
c             else if ((s1.ge.-1).and.(s1.le.0)) then
c                beta(j,k)=max(0,2*s1+1)/2.
c             endif
c             s1=q(j,k,3)/q(j,k,1)
c             s2=q(j,k+1,3)/q(j,k+1,1)
c             s1=(s1+s2)/2.
c             alph(j,k)=(alph(j,k)*sndsp(j,k)-beta(j,k)*s1)
c     &       /2./xyj(j,k)
c     super sonic
c             if ((s1.ge.0).and.(s1.le.1.0)) then
c                beta(j,k)=max(0,(vv+dot1)/(vv-dot1))
c             else if ((s1.ge.-1.).and.(s1.le.0)) then
c                beta(j,k)=max(0,(vv+dot2)/(vv-dot2))
c             else
c                beta(j,k)=sign(1,spsnd(j,k))
c             endif
c          end do
c       end do
c       do j=jbegin,jend
c          beta(j,kend)=beta(j,kup)
c          alph(j,kend)=alph(j,kup)/2./xyj(j,k)
c       end do
c
c
c calculate cusp coefficients alph and beta
c     frew october 95 (curvilinear co-ordinates)!!
c
       do k=klow,kup
          do j=jlow,jup
c             dot1=spect(j,k,3)
c             dot2=spect(j,k,2)
             s2=vv(j,k)
             s1=vv(j,k)/ccy(j,k)
             alph(j,k)=abs(s1)
c     subsonic
             if ((s1.ge.0).and.(s1.le.1)) then
                beta(j,k)=max(0,2*s1-1)/2.
             else if ((s1.ge.-1).and.(s1.le.0)) then
                beta(j,k)=min(0,2*s1+1)/2.
             endif
             s1=(vv(j,k)+vv(j,k+1))/2.
             alph(j,k)=(alph(j,k)*ccy(j,k)-beta(j,k)*s1)
     &       /2./xyj(j,k)
c     super sonic
c             if ((s1.ge.0).and.(s1.le.1.0)) then
c                beta(j,k)=max(0,(vv+dot1)/(vv-dot1))
c             else if ((s1.ge.-1.).and.(s1.le.0)) then
c                beta(j,k)=max(0,(vv+dot2)/(vv-dot2))
c             else
c                beta(j,k)=sign(1,spsnd(j,k))
c             endif
          end do
       end do
       do j=jbegin,jup
c          beta(j,kend)=0. 
c          alph(j,kend)=0.
c          beta(j,kbegin)=0.
c          alph(j,kbegin)=0.
          beta(j,kend)=beta(j,kup)
          alph(j,kend)=alph(j,kup)
          beta(j,kbegin)=beta(j,klow)
          alph(j,kbegin)=beta(j,klow)
       end do
c
c
c multiply matrix
c
c
       do 888 g=0,1
          do 888 i=1,2
             do 111 k=kbegin,kup
                   t=k+g
                do 111 j=jlow,jup
                   a1=xy(j,t,3)
                   a2=xy(j,t,4)
                   cs=sndsp(j,t)**2
                   s1=(spect(j,t,2)+spect(j,t,3)-2.0*spect(j,t,1))
     &                  /2.0*(gami/cs)
                   s2=(spect(j,t,2)-spect(j,t,3))/ccy(j,t)
                   u=q(j,t,2)/q(j,t,1)
                   v=q(j,t,3)/q(j,t,1)
                   phi=(u**2+v**2)/2
                   dot1=phi*work(j,k,1,i)
     &                   -u*work(j,k,2,i)
     &                   -v*work(j,k,3,i)
     &                  + 1.*work(j,k,4,i)
                   dot2=-vv(j,k)*work(j,k,1,i)
     &                       + a1*work(j,k,2,i)
     &                       + a2*work(j,k,3,i)
                   fact1=(s1*dot1+s2*dot2)
                   fact2=((s1*dot2)/(a1**2+a2**2)+s2*gami*dot1)
c                   h=(q(j,t,3)+press(j,t))*xyj(j,t)
                   h=(q(j,t,3)/q(j,t,1))+press(j,t)*xyj(j,t)
                   temp(j,k,1)=spect(j,k,1)*work(j,k,1,i)
     &                         +fact1
                   temp(j,k,2)=spect(j,k,1)*work(j,k,2,i)
     &                         +fact1*u
     &                         +fact2*a1
                   temp(j,k,3)=spect(j,k,1)*work(j,k,3,i)
     &                         +fact1*v
     &                         +fact2*a2
                   temp(j,k,4)=spect(j,k,1)*work(j,k,4,i)
     &                         +fact1*h
     &                         +fact2*vv(j,k)
 111               continue
c23456789 123456789 123456789 123456789 123456789 123456789 1234567890 
                   do 222 l= 1,4
                      if(i.eq.1)then
c                         do 140 k=klow,kup
c                            t=k+g
c                            do 140 j=jlow,jup
c                            s(j,k,l)=s(j,k,l)+
c     &                     beta(j,k)*(temp(j,k,l)
c     &                     -temp(j,k-1,l))*dtd
c                            temp2(j,k,l)=temp2(j,k,l)+ 
c     &                     beta(j,k)*(temp(j,k,l)
c     &                     -temp(j,k-1,l))*dtd
c                  
c                            if (l.eq.ib) then
c                               budget2(j,k,2)=budget2(j,k,2)-
c     &                                 beta(j,k)*(temp(j,k,l)
c     &                                 -temp(j,k-1,l))*dtd
c                            endif
c
c 140                     continue
c                         
                      else
                         do 150 k=klow,kup
                            t=k+g
                            do 150 j=jlow,jup
                            s(j,k,l)=s(j,k,l)-
     &                              (beta(j,t)*temp(j,k,l)
     &                              -beta(j,t-1)*temp(j,k-1,l))*dtd-(alph(j,t)
     $                              *work(j,k,l,i)-alph(j,t-1)*work(j,k
     $                              -1,l,i))*dtd
                            temp2(j,k,l)=temp2(j,k,l)-
     &                              (beta(j,t)*temp(j,k,l)
     &                              -beta(j,t-1)*temp(j,k-1,l))*dtd-(alph(j,t)
     $                              *work(j,k,l,i)-alph(j,t-1)*work(j,k
     $                              -1,l,i))*dtd
c     &                      beta(j,k)*(temp(j,k,l)
c     &                           -temp(j,k-1,l))*dtd-alph(j,k)*(work(j
c     $                           ,k,l,i)-work(j,k-1,l,i))*dtd
                            if (l.eq.ib) then
                               budget2(j,k,2)=budget2(j,k,2)-
     &                              (beta(j,t)*temp(j,k,l)
     &                              -beta(j,t-1)*temp(j,k-1,l))*dtd-(alph(j,t)
     $                              *work(j,k,l,i)-alph(j,t-1)*work(j,k
     $                              -1,l,i))*dtd
c     &                      beta(j,k)*(temp(j,k,l)
c     &                              -temp(j,k-1,l))*dtd-alph(j,k)
c     $                              *(work(j,k,l,i)-work(j,k-1,l,i))*dtd

                            endif
 150                     continue
                   
                      endif
 222               continue
 888                  continue
c     
c     adding loop to pull off dissipation of temp2
c     
                   if ((.not.wake).and.(mod(numiter,100).eq.0)) then
                         write(92,*)
     $                        'variables= "x","y","rho","xm","ym","e"'
                         write(92,*) 'zone f=point,i
     $                        =',jtail2-jtail1+1,', j=',kmax
                         do k=1,kmax
                            do j=1,jtail2-jtail1+1
                               write(92,3) x(j,k),y(j,k),temp2(j,k,1)
     $                              ,temp2(j,k,2),temp2(j,k,3),temp2(j,k
     $                              ,4)
                            end do
                         end do
 3                       format(6(e18.11,1x))
                         rewind 92
                      endif


                            
                      return
                      end






