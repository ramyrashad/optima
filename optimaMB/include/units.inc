c     -- i/o files common block --

c     -- optimization related files --
      integer n_ohis, n_ghis, n_ds, n_dvhis, n_gvhis, n_fhis,
     &     n_ac, n_cphis, n_cfhis, n_gmres, n_dvr, n_con

c     -- flow solver related files --
c     -- n_all can be used at any time => close it when done -- 
      integer n_all, n_out, n_cp, n_cf, n_ld, n_eld, n_q, n_t, n_his,
     &     n_this, n_time, n_polar, n_q2, n_t2,
     &     n_qk,n_q2k,n_tk,n_t2k

c     -- multi-point optimization files --
      integer n_mpo(9), n_mpcp(9) 

      common/units/ n_all, n_out, n_ohis, n_ghis, n_cp, n_cf,
     &     n_ld, n_eld, n_q, n_t, n_his, n_this, n_time, n_ds, n_dvhis,
     &     n_gvhis, n_fhis, n_ac, n_cphis, n_cfhis, n_gmres, n_dvr,
     &     n_con, n_polar, n_mpo, n_mpcp, n_q2, n_t2,
     &     n_qk,n_q2k,n_tk,n_t2k
