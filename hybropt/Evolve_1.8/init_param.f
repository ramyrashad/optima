C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C  IIIII  NN    N  IIIII  TTTTTTT   PPPP    AAA   RRRR    AAA  M     M C
C    I    N N   N    I       T      P   P  A   A  R   R  A   A MM   MM C
C    I    N  N  N    I       T      PPPP   AAAAA  RRRR   AAAAA M M M M C
C    I    N   N N    I       T      P      A   A  R   R  A   A M  M  M C
C  IIIII  N    NN  IIIII     T  === P      A   A  R   R  A   A M     M C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      subroutine init_param(nchrom,NPI,IPARM,IPARM_chm,NPR,PARMR,
     $     PARMR_chm,NPL,PARML,PARML_chm)
      LOGICAL PARML,cbest,parml_chm
      DIMENSION IPARM(20), PARMR(20), PARML(10)
      DIMENSION IPARM_chm(20,nchrom), PARMR_chm(20,nchrom), 
     $     PARML_chm(10,nchrom)


      do nc = 1,nchrom
         do i = 1,npi
            iparm_chm(i,nc) = iparm(i)
         enddo
         do i = 1,npr
            parmr_chm(i,nc) = parmr(i)
         enddo
         do i = 1,npl
            parml_chm(i,nc) = parml(i)
         enddo
      enddo

      return
      end
