c*******************************************************************
c**** subroutine to restrict the residual from the fine block to ***
c**** the coarse block                                           ***
c*******************************************************************
c     calling subroutine : mk_pk.f

      subroutine rf2rc (fblk,cblk,
     &                  jdimf,kdimf,rf,xyjf,
     &                  jdimc,kdimc,rc,xyjc)
c
#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
c
      integer fblk,cblk,jdimf,kdimf,jdimc,kdimc
      dimension rf(jdimf,kdimf,4),xyjf(jdimf,kdimf)
      dimension rc(jdimc,kdimc,4),xyjc(jdimc,kdimc)
      double precision suma,sumb
c
      jmnc=jminbnd(cblk)
      jmxc=jmaxbnd(cblk)
      kmnc=kminbnd(cblk)
      kmxc=kmaxbnd(cblk)
c
      jmnf=jminbnd(fblk)
      jmxf=jmaxbnd(fblk)
      kmnf=kminbnd(fblk)
      kmxf=kmaxbnd(fblk)
c
c     -remove the jacobian from fine q
      do 10 n = 1,4
      do 10 k = kmnf,kmxf
      do 10 j = jmnf,jmxf
         rf(j,k,n) = rf(j,k,n)*xyjf(j,k)
 10   continue
c
c
      tmp=1.d0/16.d0
      tmp2=1.d0/8.d0
      do 20 n  = 1,4
      do 20 kc = kmnc+1,kmxc-1
      do 20 jc = jmnc+1,jmxc-1
        jf = 2*jc - jmnc
        kf = 2*kc - kmnc
        suma = tmp*(rf(jf-1,kf-1,n) + rf(jf-1,kf+1,n)       
     &            + rf(jf+1,kf-1,n) + rf(jf+1,kf+1,n))
c     
        sumb = tmp2*(rf(jf,kf-1,n) + rf(jf-1,kf,n)
     &             + rf(jf,kf+1,n) + rf(jf+1,kf,n))
c
        rc(jc,kc,n) = (suma + sumb + .25d0*rf(jf,kf,n))
 20   continue
c
c     -put the jacobian back in q

c     fine q:
      do 30 n = 1,4
      do 30 k = kmnf,kmxf
      do 30 j = jmnf,jmxf
        rf(j,k,n) = rf(j,k,n)/xyjf(j,k)
 30   continue
c
c     coarse q:
      do 40 n = 1,4
      do 40 k = kmnc,kmxc
      do 40 j = jmnc,jmxc
         rc(j,k,n) = rc(j,k,n)/xyjc(j,k)
 40   continue
c
c
      return
      end


