c     ------------------------------------------------------------------
c     -- calculate adjoint variable psi --
c     -- m. nemec, oct. 2001 --
c     -- calling routine: adjoint.f --
c     ------------------------------------------------------------------
      subroutine calcpsi( ib, jmax, kmax, nmax, js, je, jlow, jup, ks,
     &     ke, indx, psi, psit, iex, sol, ibctype, adj)

      implicit none

#include "../include/parms.inc"

      integer ib, jmax, kmax, nmax, js, je, jlow, jup, ks, ke
      integer j,k,n,jk,is
      double precision sol(*),psi(jmax,kmax,4),psit(jmax,kmax)
      integer iex(maxjb,4,4,mxbfine),indx(jmax,kmax),ibctype(maxblk,4)
      logical   adj

      do n = 1,4
         do k = ks,ke
            do j = jlow,jup
               jk = ( indx(j,k) - 1 )*nmax + n
               psi(j,k,n) = sol(jk)
            end do
         end do
      end do

c     -- unshuffle adjoint vector at boundaries --
      if (adj) then
         do is=1,4
            if (ibctype(ib,is).gt.0 .and. ibctype(ib,is).lt.5) then

               if (is.eq.1) then
                  k = ks
                  do n = 1,4
                     do j = js, je
                        jk = ( indx(j,k) - 1 )*nmax + n
                        psi(j,k,iex(j,n,is,ib)) = sol(jk)
                     end do
                  end do

               else if (is.eq.2) then
                  j = js
                  do n = 1,4
                     do k = ks+1, ke-1
                        jk = ( indx(j,k) - 1 )*nmax + n
                        psi(j,k,iex(k,n,is,ib)) = sol(jk)
                     end do
                  end do
                  
               else if (is.eq.3) then
                  k = ke
                  do n = 1,4
                     do j = js, je
                        jk = ( indx(j,k) - 1 )*nmax + n
                        psi(j,k,iex(j,n,is,ib)) = sol(jk)
                     end do
                  end do

               else if (is.eq.4) then
                  j = je
                  do n = 1,4
                     do k = ks+1, ke-1
                        jk = ( indx(j,k) - 1 )*nmax + n
                        psi(j,k,iex(k,n,is,ib)) = sol(jk)
                     end do
                  end do
               end if
            end if
         end do
      end if

      if (nmax.eq.5) then 
         n = 5 
         do k = ks,ke
            do j = js,je
               jk = ( indx(j,k) - 1 )*5 + 5
               psit(j,k) = sol(jk)
            end do
         end do
      end if

      return
      end                       !calcpsi
