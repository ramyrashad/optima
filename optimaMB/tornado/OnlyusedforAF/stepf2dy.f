      subroutine stepf2dy(jdim,kdim,q,turmu,fmu,s,
     &          press,sndsp,xy,xyj,ds,
     &          coef2,coef4,vv,ccy,work,spect,precon,
     &          jbegin,jend,kbegin,kend,klow,kup,
     &          meshlow,meshup)
c
c calling routine: etaimpl
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension ds(jdim,kdim),turmu(jdim,kdim),fmu(jdim,kdim)
      dimension coef2(jdim,kdim),coef4(jdim,kdim)
      dimension vv(jdim,kdim),ccy(jdim,kdim),precon(jdim,kdim,8)
      dimension work(jdim,kdim,10),spect(jdim,kdim,3)
c
      logical meshlow, meshup, zero2disp, addvisc
c
c     -set up some constants  
      addvisc=.false.
      zero2disp = .false.
      dtd   = dt * thetadt/(1. + phidt)
      hd    = 0.5*dtd
c
      if(viscous .and. viseta) then
        addvisc=.true.
c       -hd/re is original
        hre = dtd*visceig/re
c       -increase viscous eigenvalue          
c        hre = dtd/re*gamma/0.72
        do 4000 k = kbegin,kend
        do 4000 j = jbegin,jend
          rinv = 1./q(j,k,1)
          etamet = xy(j,k,3)**2 + xy(j,k,4)**2 
          djac = hre/xyj(j,k)
          work(j,k,8) = djac*etamet
          work(j,k,9) = rinv
 4000   continue
c
        do 4100 k = kbegin,kup
          kp1=k+1
          do 4050 j = jbegin,jend
            vnum = 0.5*(fmu(j,k)+fmu(j,kp1)) + turmu(j,k)
            work(j,k,10) = 0.5*(work(j,k,8)+work(j,kp1,8))*vnum
 4050     continue
 4100   continue
      endif

c     -set eigenvalue swtiches  ... do n = 1 and 2 together
      do 900 n = 2,4
        if ( n .eq. 2) sn = 0.
        if ( n .eq. 3) sn = 1.
        if ( n .eq. 4) sn = -1.
        nm=n-1
c
c       *******************************************
c       **        Scalar Dissipation Model       **
c       *******************************************
c
c       -set up dissipation --- fourth order in interior
        if (idmodel.eq.1) then
          do 428 k = klow+1,kup-1
            km1 = k-1
            do 430 j = jbegin,jend
              c2m = coef2(j,km1)*dtd
              c4m = coef4(j,km1)*dtd
              c2 = coef2(j,k)*dtd
              c4 = coef4(j,k)*dtd
              work(j,k,1)  =  xyj(j,k-2)*c4m
              work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)
              work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
              work(j,k,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)
              work(j,k,5)  = xyj(j,k+2)*c4
 430         continue
 428       continue
c
c          -set up dissipation --- lower boundary and interface values
           if (meshlow) then 
c            -physical boundary
             k   = klow
             km1 = k-1
             do 422 j = jbegin,jend
               c2m = coef2(j,km1)*dtd
               c4m = coef4(j,km1)*dtd
               c2 = coef2(j,k)*dtd
               c4 = coef4(j,k)*dtd
               work(j,k,1) =  0.
               work(j,k,2) = -(c2m + c4m + c4)*xyj(j,km1)
               work(j,k,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)
               work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)
               work(j,k,5) = xyj(j,k+2)*c4
 422         continue
           else
c            -interface condition
             k = klow
             km1 = k-1
             do 425 j = jbegin,jend
               if (zero2disp) then
                 c2m = 0.0
                 c2  = 0.0
               else
                 c2m = coef2(j,km1)*dtd
                 c2 = coef2(j,k)*dtd
               endif
               c4m = coef4(j,km1)*dtd
               c4 = coef4(j,k)*dtd
c              -4th order dissipation
               work(j,k,1)  =  0.
c              work(j,k,1)  =  xyj(j,k-2)*c4m  /* xyj(k-2 not defined yet */
               work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)
               work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
               work(j,k,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)
               work(j,k,5)  = xyj(j,k+2)*c4
 425         continue
           endif
c
c          -set up dissipation --- lower boundary and interface values
           if (meshup) then
c            -physical boundary
             k   = kup
             km1 = k-1
             do 426 j = jbegin,jend
               c2m = coef2(j,km1)*dtd
               c4m = coef4(j,km1)*dtd
               c2 = coef2(j,k)*dtd
               c4 = coef4(j,k)*dtd
               work(j,k,1) =  xyj(j,k-2)*c4m
               work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)
               work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)
               work(j,k,4)  = -(c2 + c4 + c4m)*xyj(j,k+1)
               work(j,k,5)  = 0.
 426         continue
           else
c            -interface condition
             k = kup
             km1 = k-1
             do 427 j = jbegin,jend
               if (zero2disp) then
                 c2m = 0.0
                 c2  = 0.0
               else
                 c2m = coef2(j,km1)*dtd
                 c2  = coef2(j,k)*dtd
               endif
               c4m = coef4(j,km1)*dtd
               c4 = coef4(j,k)*dtd
c              -4th order approximation
               work(j,k,1)  = xyj(j,k-2)*c4m
               work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)
               work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
               work(j,k,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)
c              -xyj(k+2 not defined yet
c              work(j,k,5)  = xyj(j,k+2)*c4
               work(j,k,5)  = 0.   
 427         continue
           endif
c
c
c
c
c
c
        else
c         *******************************************
c         **        Matrix Dissipation Model       **
c         *******************************************

          if (dis2y.eq.0.) then
            do 1390 k=kbegin,kup
              kp1=k+1
              do 1380 j=jbegin,jend
                work(j,k,7)=0.0
                work(j,k,8)=dtd*(coef4(j,k)*spect(j,k,nm)
     &                         + coef4(j,kp1)*spect(j,kp1,nm))
 1380         continue
 1390       continue
          else
            do 1410 k=kbegin,kup
              kp1=k+1
              do 1400 j=jbegin,jend
                work(j,k,7)=dtd*(coef2(j,k)*spect(j,k,nm)
     &                         + coef2(j,kp1)*spect(j,kp1,nm))
                work(j,k,8)=dtd*(coef4(j,k)*spect(j,k,nm)
     &                         + coef4(j,kp1)*spect(j,kp1,nm))
 1400         continue
 1410       continue
          endif
c
          do 1428 k = klow+1,kup-1
            km2 = k-2
            km1 = k-1
            kp1 = k+1
            kp2 = k+2
            do 1430 j = jbegin,jend
              c2m = work(j,km1,7) 
              c4m = work(j,km1,8) 
              c2  = work(j,k,7) 
              c4  = work(j,k,8) 
c     
              work(j,k,1)  =  xyj(j,km2)*c4m
              work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)
              work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
              work(j,k,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,kp1)
              work(j,k,5)  = xyj(j,kp2)*c4
 1430       continue
 1428     continue
c
c          -set up dissipation --- lower boundary and interface values
          if (meshlow) then   
c           -physical boundary
            k   = klow
            km1 = k-1
            kp1 = k+1
            do 1422 j = jbegin,jend
              c2m = work(j,km1,7) 
              c4m = work(j,km1,8) 
              c2  = work(j,k,7) 
              c4  = work(j,k,8) 
c     
              work(j,k,1) =  0.
              work(j,k,2) = -(c2m + c4m + c4)*xyj(j,k-1)
              work(j,k,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)
              work(j,k,4) = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)
              work(j,k,5) = xyj(j,k+2)*c4
 1422       continue
          else
c           -interface condition
            k = klow
            km1 = k-1
            kp1 = k+1
            do 1425 j = jbegin,jend
              c2m = work(j,km1,7) 
              c4m = work(j,km1,8) 
              c2  = work(j,k,7) 
              c4  = work(j,k,8) 
c
c             -4th order dissipation
              work(j,k,1)  =  0.
c              -xyj(k-2 not defined yet
c              work(j,k,1)  =  xyj(j,k-2)*c4m
              work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,k-1)
              work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
              work(j,k,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,k+1)
              work(j,k,5)  = xyj(j,k+2)*c4
 1425       continue
          endif
c
c         - set up dissipation --- lower boundary and interface values
          if (meshup) then
c           -physical boundary
            k   = kup
            km1 = k-1
            kp1 = k+1
            do 1426 j = jbegin,jend
              c2m = work(j,km1,7) 
              c4m = work(j,km1,8) 
              c2  = work(j,k,7) 
              c4  = work(j,k,8) 
c
              work(j,k,1) =  xyj(j,k-2)*c4m
              work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)
              work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)
              work(j,k,4)  = -(c2 + c4 + c4m)*xyj(j,kp1)
              work(j,k,5)  = 0.
 1426       continue
          else
c           -interface condition
            k = kup
            km1 = k-1
            kp1 = k+1
            do 1427 j = jbegin,jend
              c2m = work(j,km1,7) 
              c4m = work(j,km1,8) 
              c2  = work(j,k,7) 
              c4  = work(j,k,8) 
c
c             -4th order approximation
              work(j,k,1)  = xyj(j,k-2)*c4m
              work(j,k,2)  = -(c2m + 3.*c4m + c4)*xyj(j,km1)
              work(j,k,3)  = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)
              work(j,k,4)  = -(c2 + 3.*c4 + c4m)*xyj(j,kp1)
c              work(j,k,5)  = xyj(j,k+2)*c4
c              -xyj(k+2) not defined yet
              work(j,k,5)  = 0.   
 1427       continue
          endif
        endif
c
c
c       ***************************************
c       **  Add approx. viscous eigenvalues  **
c       ***************************************
c 
        if(addvisc) then
c         -apply a linearization of the preconditioner to the viscous
c          terms
          if (prec.gt.0) then
            if (n.eq.3) then
              do 1900 k=kbegin,kup
              do 1900 j=jbegin,jend
                duterm=((precon(j,k,1)-1.0)*(precon(j,k,7)-vv(j,k))/
     &                precon(j,k,8)+precon(j,k,1)+1.0)/2.0
                work(j,k,9)=work(j,k,9)*duterm
 1900         continue
            elseif (n.eq.4) then
              do 2900 k=kbegin,kup
              do 2900 j=jbegin,jend
                duterm=((-precon(j,k,1)+1.0)*(precon(j,k,7)-vv(j,k))/
     &                precon(j,k,8)+precon(j,k,1)+1.0)/2.0
                work(j,k,9)=work(j,k,9)*duterm
 2900         continue
            endif
          endif
c
          do 3100 k = klow,kup
            kp1=k+1
            km1=k-1
            do 3000 j = jbegin,jend
              ctm1=work(j,km1,10)
              ctp1=work(j,k,10)
              cttt = ctm1 + ctp1
              work(j,k,2) = work(j,k,2) - work(j,km1,9)*ctm1
              work(j,k,3) = work(j,k,3) + work(j,k,9)*cttt
              work(j,k,4) = work(j,k,4) - work(j,kp1,9)*ctp1
 3000       continue
 3100     continue
        endif
c
c
c       -add in flux jacobians, variable dt and identity
        if ((prec.eq.0).or.(n.eq.2)) then
          if (iord.eq.4) then
            thd=dtd/12.d0
            do 531 k = klow+1,kup-1
            do 531 j = jbegin,jend
              work(j,k,1)= work(j,k,1) + (vv(j,k-2)+sn*ccy(j,k-2))*thd
              work(j,k,2)= work(j,k,2)-8.*(vv(j,k-1)+sn*ccy(j,k-1))*thd
              work(j,k,4)= work(j,k,4)+8.*(vv(j,k+1)+sn*ccy(j,k+1))*thd
              work(j,k,5)= work(j,k,5) - (vv(j,k+2)+sn*ccy(j,k+2))*thd 
              work(j,k,1)= work(j,k,1)*ds(j,k)                        
              work(j,k,2)= work(j,k,2)*ds(j,k)                       
              work(j,k,3)= work(j,k,3)*ds(j,k)                       
              work(j,k,3)= work(j,k,3) + 1.0
              work(j,k,4)= work(j,k,4)*ds(j,k)                       
              work(j,k,5)= work(j,k,5)*ds(j,k)                        
 531        continue
c     
c           -using third order for boundary
            thd=dtd/6.d0
            do 532 j=jbegin,jend
              k=klow
              work(j,k,2)= work(j,k,2)-2.*(vv(j,k-1)+sn*ccy(j,k-1))*thd
              work(j,k,3)= work(j,k,3) -   3.*(vv(j,k)+sn*ccy(j,k))*thd
              work(j,k,4)= work(j,k,4)+6.*(vv(j,k+1)+sn*ccy(j,k+1))*thd 
              work(j,k,5)= work(j,k,5) -  (vv(j,k+2)+sn*ccy(j,k+2))*thd
              work(j,k,1)= work(j,k,1)*ds(j,k)                       
              work(j,k,2)= work(j,k,2)*ds(j,k)                       
              work(j,k,3)= work(j,k,3)*ds(j,k)                        
              work(j,k,3)= work(j,k,3) + 1.0
              work(j,k,4)= work(j,k,4)*ds(j,k)                        
              work(j,k,5)= work(j,k,5)*ds(j,k)                       
c               
              k=kup
              work(j,k,1)= work(j,k,1) +  (vv(j,k-2)+sn*ccy(j,k-2))*thd
              work(j,k,2)= work(j,k,2)-6.*(vv(j,k-1)+sn*ccy(j,k-1))*thd 
              work(j,k,3)= work(j,k,3) +   3.*(vv(j,k)+sn*ccy(j,k))*thd
              work(j,k,4)= work(j,k,4)+2.*(vv(j,k+1)+sn*ccy(j,k+1))*thd
              work(j,k,1)= work(j,k,1)*ds(j,k)                     
              work(j,k,2)= work(j,k,2)*ds(j,k)                      
              work(j,k,3)= work(j,k,3)*ds(j,k)
              work(j,k,3)= work(j,k,3) + 1.0
              work(j,k,4)= work(j,k,4)*ds(j,k)                         
              work(j,k,5)= work(j,k,5)*ds(j,k)                        
 532        continue
          else
            do 435 k = klow,kup
            do 435 j = jbegin,jend
              work(j,k,2) = work(j,k,2) - (vv(j,k-1)+sn*ccy(j,k-1))*hd
              work(j,k,4) = work(j,k,4) + (vv(j,k+1)+sn*ccy(j,k+1))*hd
              work(j,k,1) = work(j,k,1)*ds(j,k)
              work(j,k,2) = work(j,k,2)*ds(j,k)
              work(j,k,3) = work(j,k,3)*ds(j,k) + 1.0
              work(j,k,4) = work(j,k,4)*ds(j,k)
              work(j,k,5) = work(j,k,5)*ds(j,k)
 435        continue
          endif
        else
c         -use preconditioned 3rd and 4th eigenvalues
          do 536 k = klow,kup
          do 536 j = jbegin,jend
            work(j,k,2) = work(j,k,2) - 
     +            (precon(j,k-1,7)+sn*precon(j,k-1,8))*hd       
            work(j,k,4) = work(j,k,4) + 
     +            (precon(j,k+1,7)+sn*precon(j,k+1,8))*hd       
            work(j,k,1) = work(j,k,1)*ds(j,k)            
            work(j,k,2) = work(j,k,2)*ds(j,k)                  
            work(j,k,3) = work(j,k,3)*ds(j,k)      
            work(j,k,3) = work(j,k,3) + 1.
            work(j,k,4) = work(j,k,4)*ds(j,k)            
            work(j,k,5) = work(j,k,5)*ds(j,k) 
 536      continue
        endif
c
c
c       -solve the system of equations
        if (n.eq.2) then
c         -invert the first two together
          call ypenta2(jdim,kdim,work(1,1,1),work(1,1,2),work(1,1,3),
     *          work(1,1,4),work(1,1,5),work(1,1,6),work(1,1,7),
     *          s(1,1,1),klow,kup,jbegin,jend)
        else
c         -do n = 3 and 4 separately
          call ypenta(jdim,kdim,work(1,1,1),work(1,1,2),work(1,1,3),
     *          work(1,1,4),work(1,1,5),work(1,1,6),work(1,1,7),
     *          s(1,1,n),klow,kup,jbegin,jend)
        endif
 900  continue 
c
      return
      end
