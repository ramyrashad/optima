************************************************************************
      !-- Program name: wt_int_dps
      !-- Written by: Howard Buckley
      !-- Date: Sept 2011
      !-- 
      !-- This subroutine stores values of the on-design objective funcs
      !-- for use in subsequent calculations
************************************************************************

      subroutine storeOndpObjs(dpobj1,ondpobjs)

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      
      !-- Declare variables

      integer 
     &     i, j, sendProc

      double precision 
     &     dpobj1, dpobj1_proc_0, ondpobjs(mpopt-noffcon)


      !-- Send on-design objective function values to array 'ondpobjs' 
      !-- on processor 0

#ifdef _MPI_VERSION
 
      if ((off_flags(rank+1)).eq.(.false.).and.rank.ne.0) then

         call MPI_SEND(dpobj1, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)

      end if

      if (rank==0) then

         j = 1

         if ((off_flags(rank+1)).eq.(.false.)) then
            ondpobjs(1) = dpobj1
            j = j+1
         end if

         dpobj1_proc_0 = dpobj1

         do i = 2, mpopt

            if ((off_flags(i)).eq.(.false.)) then

               sendProc = i-1

               call MPI_RECV(dpobj1, 1, MPI_DOUBLE_PRECISION, 
     &                             sendProc , 1, COMM_CURRENT, 
     &                             MPI_STATUS_IGNORE, ierr)

               ondpobjs(j) = dpobj1

               j = j+1

            end if

         end do

         !-- Reset dpobj1 for processor 0

         dpobj1 = dpobj1_proc_0

      end if

#else

      if ((off_flags(1)).eq.(.false.)) then

         ondpobjs(1) = dpobj1

      end if

#endif

      return

      end
