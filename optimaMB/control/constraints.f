      subroutine constraints(thc,goc)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"     
#include "../include/optcom.inc"
#include "../include/optima.inc"

      double precision thc,goc,xte1,yte1,dist
      integer ifoil,i

c     -- thickness constraints --
      thc = 0.0d0
      do i=1,ntcon
         ifoil = itcf(i)
         cth(i) = sqrt( (bap(juptx(i),2,ifoil) -
     &        bap(jlotx(i),2,ifoil))**2 + (bap(juptx(i),1,ifoil) -
     &        bap(jlotx(i),1,ifoil))**2 )
            
         if (cth(i) .lt. cthtar(i) ) then
            thc = thc + wtc*( 1.0 - cth(i)/cthtar(i) )**2
         end if
      end do


      
c     -- gap and overlap constraints --
      if (ngapc.ne.0 .or. novc.ne.0) then
         do ifoil = 2, nfoils
            xte1 = bap(1,1,ifoil-1)
            yte1 = bap(1,2,ifoil-1)
            gapd(ifoil) = 1.0d0
            do i = ile(ifoil),ibap(ifoil)
               dist = sqrt( (bap(i,2,ifoil) - yte1)**2 + 
     &              (bap(i,1,ifoil)-xte1)**2 )
               if (dist.lt.gapd(ifoil)) gapd(ifoil) = dist
            end do
            ovd(ifoil) = bap(ile(ifoil),1,ifoil) - xte1
         end do
      end if

      goc = 0.0
      do i=1,ngapc
         ifoil = ifgapc(i)
            
         if ( gapd(ifoil) .gt. gapu(i) ) then
            goc = goc + wgo*( 1.0 - gapd(ifoil)/gapu(i) )**2
c     write (*,*) 'Upper gap active', gapd(ifoil), gapu(i)
         end if
         
         if ( gapd(ifoil) .lt. gapl(i) ) then
            goc = goc + wgo*( 1.0 - gapd(ifoil)/gapl(i) )**2
c     write (*,*) 'Lower gap active', gapd(ifoil), gapl(i)
         end if
      end do
      
      do i=1,novc
         ifoil = ifovc(i)
         
         if ( ovd(ifoil) .gt. ovu(i) ) then
            goc = goc + wgo*( 1.0 - ovd(ifoil)/ovu(i) )**2
c     write (*,*) 'Upper overlap active', ovd(ifoil), ovu(i)
         end if
         
         if ( ovd(ifoil) .lt. ovl(i) ) then
            goc = goc + wgo*( 1.0 - ovd(ifoil)/ovl(i) )**2
c     write (*,*) 'Lower overlap active', ovd(ifoil), ovl(i)
         end if
      end do

    

      return
      end                       ! constraints
