c     ------------------------------------------------------------------
c     -- subroutine to calculate the derivative of the objective
c     function with respect to the state vector q ala analytic formula
c     and finite differences. --
c     -- m. nemec, oct. 2001 --

c     -- note 1: the mult. by jacobian is used to cancel the jacobian in
c     dR/dQ, since the flow jacobian is formed with respect to the
c     J^-1(Q) variables vector while for optimization it is easier to
c     work with just Q i.e. (rho, rho*u, rho*v, e). --

c     -- note 2: for viscous flow dOBJdQ is kept zero at the body for
c     the momentum equation since the gradient is independent of these
c     values --

c     -- note 3: q does not contain jacobian --

c     -- note 4: for euler cases drag is only dependent on pressure at
c     airfoil boundary, else Dobj/Dq is zero. --

c     -- note 5: Dobj/Dq is calculated using cenral-differences --

c     -- note 6: for viscous cases drag depends on pressure at the body
c     as well as Q at k=1, 2 and 3. -- 

c     -- note 7: for euler cases lift is only dependent on pressure at
c     airfoil boundary , else Dobj/Dq is zero. --

c     -- note 8: for viscous cases lift depends on pressure at the body
c     as well as Q at k=1, 2 and 3. --

c     ------------------------------------------------------------------
      subroutine dOBJdQ ( x, y, xy, xyj, q, dOdQ, press)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer ib,ifoil,ic,iseg,is,lq,lg,ibn
      double precision q(*), xyj(*), xy(*), x(*), y(*), dOdQ(*)
      double precision press(*)
      double precision clis(maxsegs,maxfoil), cdis(maxsegs,maxfoil)
      double precision cmis(maxsegs,maxfoil), cmvs(maxsegs,maxfoil)
      double precision clvs(maxsegs,maxfoil), cdvs(maxsegs,maxfoil)

c     -- reset dOdQ --
      do ib = 1,nblks
         call resets( jmax(ib), kmax(ib), dOdQ(lqptr(ib)) )
      end do

      if ( iobjf.eq.1 ) then
c     -- inverse design --
c     -- use analytic derivation --
         do ifoil = 1,nfoils
            ic = 1
            do iseg = 1,nsegments(ifoil)
               ib = isegblk(iseg,ifoil)
               is = isegside(iseg,ifoil)
               lq = lqptr(ib)
               lg = lgptr(ib)
               call dides ( ic, ifoil, is, jminbnd(ib), jmaxbnd(ib),
     &              kminbnd(ib), kmaxbnd(ib), jmax(ib), kmax(ib),
     &              xyj(lg), q(lq), dOdQ(lq), gami, fsmach, viscous,
     &              cpi, cp_tar)
            end do
         end do

      else if ( iobjf.eq.2 .or. iobjf.eq.3 ) then

         do ifoil=1,nfoils
            do iseg=1,nsegments(ifoil)
               ib = isegblk(iseg,ifoil)
               is = isegside(iseg,ifoil)
               lg = lgptr(ib)
               lq = lqptr(ib)
               call clcd(jmax(ib), kmax(ib), is, ibcdir(ib,is), q(lq),
     &              press(lg), x(lg), y(lg), xy(lq), xyj(lg), 1,
     &              cli, cdi, cmi, clv, cdv, cmv, jminbnd(ib),
     &              jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib))

               clis(iseg,ifoil) = cli
               cdis(iseg,ifoil) = cdi
               cmis(iseg,ifoil) = cmi
               clvs(iseg,ifoil) = clv
               cdvs(iseg,ifoil) = cdv
               cmvs(iseg,ifoil) = cmv
            end do
         end do

         if (.not. viscous) then
            do ifoil = 1,nfoils
               ibn = 1
               do iseg = 1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  call dobj2i( is, ifoil, iseg, ibcdir(ib,is),
     &                 nsegments, cdis, cdvs, clis, clvs, maxsegs,
     &                 maxfoil, nfoils, jmax(ib), kmax(ib), jminbnd(ib),
     &                 jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), q(lq),
     &                 dOdQ(lq), press(lg), xyj(lg), xy(lq), x(lg),
     &                 y(lg), gami)
               end do
            end do
         else                   !viscous flow
            do ifoil = 1,nfoils
               ibn = 1
               do iseg = 1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  call dobj2v( is, ifoil, iseg, ibcdir(ib,is),
     &                 nsegments, cdis, cdvs, clis, clvs, maxsegs,
     &                 maxfoil, nfoils, jmax(ib), kmax(ib), jminbnd(ib),
     &                 jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), q(lq),
     &                 dOdQ(lq), press(lg), xyj(lg), xy(lq), x(lg),
     &                 y(lg), gami)
               end do
            end do
         end if

c        iblk = 1
c        jo = jminbnd(iblk) 
c        ko = kminbnd(iblk)
c        no = 1
c        nmax = 4
c
c10      continue
c
c      write (*,*) 'going fd_dq',nmax
c        call fd_dq( jo, ko, no, jmax(iblk), kmax(iblk), nmax,
c    &        q(lqptr(iblk)), turre(lgptr(iblk)), xyj(lgptr(iblk)),
c    &        tmpv, stepsize)
c      write (*,*) 'done fd_dq'
c
c     -- halo column copy for q vector --
c     do ii1 = 1,nblks
c        do is1=2,4,2
c           ii2 = lblkpt(ii1,is1)
c           if (ii2.ne.0) then
c              is2=lsidept(ii1,is1)
c              call halocp(ii1, nhalo, 4, is1, is2, jmax(ii1),
c    &              kmax(ii1), q(lqptr(ii1)), jbegin(ii1)-nsd2(ii1),
c    &              jend(ii1)+nsd4(ii1), kminbnd(ii1), kmaxbnd(ii1),
c    &              jmax(ii2), kmax(ii2), q(lqptr(ii2)), jbegin(ii2)
c    &              -nsd2(ii2), jend(ii2)+nsd4(ii2), kminbnd(ii2),
c    &              kmaxbnd(ii2))
c           endif
c        end do
c     end do
c
c     if (nmax.eq.5) then
c        do ib1 = 1,nblks
c           do is1=2,4,2
c              ib2 = lblkpt(ib1,is1)
c              if (ib2.ne.0) then
c                 is2=lsidept(ib1,is1)
c                 call halocp(ib1, nhalo, 1, is1,is2, jmax(ib1),
c    &                 kmax(ib1), turre(lgptr(ib1)),
c    &                 jbegin(ib1)-nsd2(ib1), jend(ib1)+nsd4(ib1),
c    &                 kminbnd(ib1), kmaxbnd(ib1), jmax(ib2), kmax(ib2),
c    &                 turre(lgptr(ib2)), jbegin(ib2)-nsd2(ib2),
c    &                 jend(ib2)+nsd4(ib2), kminbnd(ib2), kmaxbnd(ib2))
c              endif
c           end do
c        end do
c     end if
c
c     -- calculate pressure and sound speed --
c      write (*,*) 'going calcps'
c     do ii=1,nblks
c        lq = lqptr(ii)
c        lg = lgptr(ii)
c        call calcps( ii, jmax(ii), kmax(ii), q(lq), press(lg),
c    &        sndsp(lg), precon(lprecptr(ii)), xy(lq), xyj(lg),
c    &        jbegin(ii)-nsd2(ii), jend(ii)+nsd4(ii), 
c    &        kminbnd(ii), kmaxbnd(ii))
c     end do
c      write (*,*) 'done calcps'
c     
c     do ifoil = 1,nfoils
c        ibn = 1
c        do iseg = 1,nsegments(ifoil)
c           ib = isegblk(iseg,ifoil)
c           is = isegside(iseg,ifoil)
c           call getcp(ibn, jmax(ib), kmax(ib), jminbnd(ib),
c    &           jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), ifoil, is,
c    &           press(lgptr(ib)), xyjtmp(lgptr(ib)))
c        end do
c     end do
c
c     -- reset q vector --
c     call fd_qr( jo, ko, no, jmax(iblk), kmax(iblk), nmax, 
c    &     q(lqptr(iblk)), turre(lgptr(iblk)), tmpv)
c
c     dxx = 1.0/stepsize
c     call fd_dodq(obj, dxx, jo, ko, no, iblk, stepsize, jmax(iblk),
c    &     kmax(iblk), dOdQ(lqptr(iblk)), xyj(lgptr(iblk)),
c    &     dOdQ2(lqptr(iblk)))
c
c     if (no.lt.nmax) then
c        no = no + 1
c        goto 10
c     else
c        no = 1
c        if ( ko.lt.kmaxbnd(iblk) ) then
c           ko = ko + 1
c           goto 10
c        else
c           ko = kminbnd(iblk)
c           if ( jo.lt.jmaxbnd(iblk) ) then
c              jo = jo + 1
c              goto 10
c           else
c              if (iblk.lt.nblks) then
c                 write (*,*) 'Block: ',iblk
c                 iblk = iblk + 1
c                 jo = jminbnd(iblk)
c                 goto 10
c              end if
c           end if
c        end if
c     end if        
c
c      do ib = 1,nblks
c         call qmuj( jmax(ib), kmax(ib), q(lqptr(ib)), xyj(lgptr(ib)),
c     &        jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
c     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
c      end do
 
      end if

      return
      end                       !dobjdq

c      subroutine fd_dodq( obj, dxx, jo, ko, no, iblk, stepsize, jmax,
c     &     kmax, dOdQ, xyj, dOdQ2)
c
c#include "../include/parms.inc"
c#include "../include/mbcom.inc"
c#include "../include/optcom.inc"
c#include "../include/optima.inc"
c
c      double precision dOdQ(jmax,kmax,4), xyj(jmax,kmax), dOdQ2(jmax,kmax,4)
c
c      objp = 0.0
c      do ifoil = 1,nfoils
c         do i = 1, icpn(ifoil)
c            objp = objp + 0.5*( cpi(i,ifoil) - cp_tar(i,ifoil) )**2
c         end do
c      end do      
c
c      dOdQ(jo,ko,no) = xyj(jo,ko)*(objp - obj)*dxx
c
c      if (dOdQ(jo,ko,no).ne.0.0) write(*,10) jo,ko,no,iblk,
c     &     dOdQ(jo,ko,no),dodq2(jo,ko,no)
c
c 10   format(4i5,2e14.5)
c
c      return
c      end
