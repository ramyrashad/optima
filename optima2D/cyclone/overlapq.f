      subroutine overlapq(JDIM,KDIM,NDIM,Q,XYJ,JMAX,JTAIL1)             
C                                                                       
        DIMENSION Q(JDIM,KDIM,NDIM),XYJ(JDIM,KDIM)                      
C                                                                       
C  GET K = 1 ;;J = 1 TO JTAIL1-1 POINTS FROM J = JTAIL2+1 TO JMAX       
C                                                                       
C    JTAIL2 = JMAX-JTAIL1+1                                             
C                                                                       
        DO 10 N = 1,NDIM                                                
        DO 10 J = 1,JTAIL1-1                                            
        JJ = JMAX-J+1                                                   
              Q(J,1,N) = Q(JJ,1,N)*XYJ(JJ,1)/XYJ(J,1)                   
10      CONTINUE                                                        
C                                                                       
        RETURN                                                          
        END                                                             
