C ********************************************************************
C ********************* LIFT AND DRAG CALCULATIONS *******************
C ********************************************************************
      subroutine clcdho2(jdim,kdim,iside,idir,q,press,x,y,xy,xyj,
     &      nscal,cl,cd,cm,clvv,cdvv,cmvv,
     &      cp,cp2,sn,xn,yn,x2,y2,
     &      jbegin,jend,kbegin,kend,int)
c
#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/units.inc"
c
      integer count,jd,inter,iptr,int
      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension cp(jdim),cp2(jdim),sn(jdim)
      dimension xn(jdim),yn(jdim),x2(jdim),y2(jdim)
      external f,f2,f3,f4,dglegq
      logical   oddside
      integer levmax,iarr
      parameter(levmax=5000,iarr=levmax+1)
      common/var/jd,count,iptr
c
c     -worksp has space for 5*maxj
      dimension z(maxj),work(maxj,3)
      common/worksp/z,work
c
c     cp  = used to store both coefficient of pressure and friction
c     xn  = chordwise direction (pos. in direction of LE to TE)
c     yn  = normal to chord
c     sn  = parameter approximating arclength (going from lower TE to
c           upper TE)
c     zeta= dummy variable for parameterization of xn,yn,cp
c     dx  = d(xn)/d(sn)
c     dy  = d(yn)/d(sn)
c     x2,y2,cp2 = double derivative of corresponding variable
c
c
c     -if idir=0 don't integrat the pressure (used for inviscid walls
c      in viscous problems)
c
      if (idir.eq.0) return
c
c     -the parameter nscal determines whether the q variables are scaled
c      by the metric jacobians xyj.
c
c     -this routine supplies force and moment coeffs.  on airfoil surface
c
      iptr=int
      sgn = float(sign(1,idir))
c
      cl = 0.
      cd = 0.
      cm = 0.
      clvv = 0.
      cdvv = 0.
      cmvv = 0.
c
c     -set limits of integration
      j1 = jbegin
      j2 = jend
      jtp = jbegin + 1
      if (iside.eq.1) k1 = kbegin
      if (iside.eq.3) k1 = kend
      if (iside.eq.2 .or. iside.eq.4) then
        write(n_out,*) 'High-order integration set-up for non-blunt'
        write(n_out,*) 'trailing edges only.'
        stop
      endif
      jd=j2-j1+1
c
c     ****************************************************      
c     *                   Funct: Cp(sn)                  *
c     ****************************************************      
      cpc = 2.d0/(gamma*fsmach**2)                                        
c
c     compute cp at grid points     
c                                                                       
      do 10 j=j1,j2
        pp = press(j,k1)
        if(nscal.eq.0) pp = pp*xyj(j,k1)
        jj=j-j1+1
        cp(jj) = (pp*gamma -1.d0)*cpc
c        write(90,777) xn(jj),cp(jj),yn(jj)
c 777    format(3e15.8)
 10   continue
c
c     Boundary Conditions
      yp1=bc(jd,1,1,sn,cp)
      ypn=bc(jd,jd,-1,sn,cp)
c
c      print *,'bc of cp',yp1,ypn
      call spline(jd,sn,cp,yp1,ypn,cp2,work,work(1,2),work(1,3))
c
c     ****************************************************
c
c     -integrate along arclength
c     -compute normal force coefficient and chord directed force coeff     
c     -chord taken as one in all cases

      sstart=sn(1)
      send=sn(jd)
      count=0
      maxlev = levmax
      tol=1.d-11

      cc = daquad(dglegq,3,f,sstart,send,tol,maxlev,sing,errest)
c      print *,'count for cc',count,errest
      if (tol.ne.0.d0)
     &     write (n_out,*) 'Routine failed for cc. tol = ',tol,
     &     '  sing = ',sing
c
      count=0
      maxlev = levmax
      tol=1.d-11
      cn = daquad(dglegq,3,f2,sstart,send,tol,maxlev,sing,errest)
c      print *,'count for cn',count,errest
      if (tol.ne.0.d0)
     &     write (n_out,*) 'Routine failed for cn. tol = ',tol,
     &     '  sing = ',sing
c
c     -compute normal force coefficient and chord directed force coeff
c     -chord taken as one in all cases
      cmle = 0.d0
      if (matcharc2d) then
        do 11 j=jtp,j2
          jm1 = j-1
          jj = j-jtp+2
          jjm1 = jj-1
          cpav = (cp(jj) + cp(jjm1))*.5d0
          cmle = cmle + cpav*(x(j,k1)+x(jm1,k1))*.5*(x(j,k1) -x(jm1,k1))
 11     continue
      else
        do 12 j=jtp,j2
          jm1 = j-1
          jj = j-jtp+2
          jjm1 = jj-1
          cpav = (cp(jj) + cp(jjm1))*.5d0
          cmle=cmle+cpav*((x(j,k1)+x(jm1,k1))*.5*(x(j,k1)-x(jm1,k1))
     &                   +(y(j,k1)+y(jm1,k1))*.5*(y(j,k1)-y(jm1,k1)))
 12     continue
      endif
c
c     **************************************
c     correct for curves going the wrong way
c     **************************************
      cn = sgn*cn
      cc = sgn*cc
      cmle = sgn*cmle
c
      cl = cn*cos(alpha*pi/180.) - cc*sin(alpha*pi/180.)
      cd = cn*sin(alpha*pi/180.) + cc*cos(alpha*pi/180.)
      cmqc = cmle +.25*cn
c
c     note: cmle is moment about (x,y)=(0,0) 
c                             but cmqc is moment about (0.25,0)
      cm = cmqc
c
c
c
c
c
c     *********************************************
c      Viscous Coefficient of Friction Calculation
c     *********************************************

      if(viscous)then                                                 
c                                                                       
c       viscous coefficent of friction calculation        
c       taken from P. Buning
c                                                                      
c       calculate the skin friction coefficient  (w => wall) 
c                                                                      
c        c  = tau   /  q    
c         f      w      inf 
c       
c        where
c       
c              tau  = mu*(du/dy-dv/dx)
c                 w                   w
c       
c              q    =  1/2 * rhoinf * uinf * uinf
c               inf
c                                                                      
c              du/dy = (du/dxi * dxi/dy) + (du/deta * deta/dy) 
c              dv/dx = (dv/dxi * dxi/dx) + (dv/deta * deta/dx) 
c       
c       (definition from F.M. White, Viscous Fluid Flow, Mcgraw-Hill Inc.,
c       York,1974, p.50, but use freestream values instead of edge values.
c                                                                      
c                                                                      
c       for calculating cf, we need the coefficient of viscosity, mu. use
c       re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1,rhoinf=1.
c                                                                       
        cinf  = 1.d0
        alngth= 1.d0
c       re already has fsmach scaling              
        amu   = rhoinf*alngth/re                                  
        uinf2 = fsmach**2
c                                                                         
c       k= k1                                                             
        if (iside.eq.1) kadd=1
        if (iside.eq.3) kadd=-1
c                                                                       
        k2 = k1 + kadd
        k3 = k2 + kadd
        k4 = k3 + kadd
        k5 = k4 + kadd
c
        tmp=float(kadd)/12.d0
        do 110 j = j1,j2                                            
c     
c         Note: for viscous flows u and v = 0 on body ...
c               consequently, the xi direction derivatives can be skipped.
c               -only during first strtit iterations is u and v non-zero
c                but we dont't care about cf then. 
c
c         xi direction    
          uxi=0.d0
          vxi=0.d0
c
c         eta direction
          r  =1.d0/q(j,k1,1)
          rp1=1.d0/q(j,k2,1)
          rp2=1.d0/q(j,k3,1)
          rp3=1.d0/q(j,k4,1)
          rp4=1.d0/q(j,k5,1)
c
          u  =q(j,k1,2)*r
          up1=q(j,k2,2)*rp1
          up2=q(j,k3,2)*rp2
          up3=q(j,k4,2)*rp3
          up4=q(j,k5,2)*rp4
c
          v  =q(j,k1,3)*r
          vp1=q(j,k2,3)*rp1
          vp2=q(j,k3,3)*rp2
          vp3=q(j,k4,3)*rp3
          vp4=q(j,k5,3)*rp4
c
          ueta= tmp*(-25.d0*u +48.d0*up1-36.d0*up2+16.d0*up3-3.d0*up4) 
          veta= tmp*(-25.d0*v +48.d0*vp1-36.d0*vp2+16.d0*vp3-3.d0*vp4) 
c
c         metric derivatives
          xix = xy(j,k1,1)                                             
          xiy = xy(j,k1,2)                                             
          etax = xy(j,k1,3)                                            
          etay = xy(j,k1,4)                                            
c
          tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))         
          jj=j-j1+1
c          print *,'tauw=',jj,sn(jj)/sn(jd),tauw
          cp(jj)= tauw/(.5d0*rhoinf*uinf2)
c          if (numiter.eq.100) write(90,*) numiter,jj,cp(jj)
 110    continue
        if (matcharc2d .and. sngvalte) then
c         -average trailing edge value
          cp(1)=.5d0*(cp(1)+cp(jd))
          cp(jd)=cp(1)
        endif
c
c       -the array cp now contains the coefficient of friction
c       -spline for coefficient of friction below
c
c       ****************************************************      
c       *                   Funct: Cf(sn)                  *
c       ****************************************************      
c
c       Boundary Conditions
        yp1=bc(jd,1,1,sn,cp)
        ypn=bc(jd,jd,-1,sn,cp)
c        
c        print *,'bc of cp',yp1,ypn
        call spline(jd,sn,cp,yp1,ypn,cp2,work,work(1,2),work(1,3))
c        
c       ****************************************************
c
c       -integrate along arclength
c       -compute viscous normal and axial forces 
c       -chord taken as one in all cases

        sstart=sn(1)
        send=sn(jd)
        count=0
        maxlev = levmax
        tol=1.d-11
         
        ccv = daquad(dglegq,3,f3,sstart,send,tol,maxlev,sing,errest)
c        print *,'count for ccv',count,errest
        if (tol.ne.0.d0) write (n_out,*) 
     &    'Routine failed for ccv. tol = ',tol,'  sing = ',sing
c        
        count=0
        maxlev = levmax
        tol=1.d-11
        cnv = daquad(dglegq,3,f4,sstart,send,tol,maxlev,sing,errest)
c        print *,'count for cnv',count,errest
        if (tol.ne.0.d0) write (n_out,*)
     &    'Routine failed for cnv. tol = ',tol,'  sing = ',sing
        cmlev = 0.d0                                             
        do 111 j=jtp,j2                                          
          jm1 = j-1 
          jj = j-jtp+2
          jjm1 = jj-1
          cfav = ( cp(jj) + cp(jjm1))*.5d0
          cmlev = cmlev + cfav*                                       
     &         (  (x(j,k1)+x(jm1,k1))*.5*(y(j,k1) -y(jm1,k1)) -
     &            (y(j,k1)+y(jm1,k1))*.5*(x(j,k1) -x(jm1,k1))   )
 111    continue
c       **************************************
c       correct for curves going the wrong way
c       **************************************
        cnv   = sgn*cnv
        ccv   = sgn*ccv
        cmlev = sgn*cmlev
c
        clvv = cnv*cos(alpha*pi/180.) - ccv*sin(alpha*pi/180.)
        cdvv = cnv*sin(alpha*pi/180.) + ccv*cos(alpha*pi/180.)
        cmqcv = cmlev +.25*cnv
        cmvv = cmqcv
c     -this is the endif for viscous case         
      endif
c
      return
      end






c     *************** External Functions for Integration ***************
c
      double precision function f(stmp)
c     -chord directed component of pressure
c
      include '../include/parms.inc'
      integer count,jd,iptr
      double precision dx,dy,cptmp,denom,ndotx,stmp
      common/var/jd,count,iptr
      dimension cp(maxjfoil),xn(maxjfoil),yn(maxjfoil)
      dimension sn(maxjfoil),cp2(maxjfoil)
      dimension x2(maxjfoil),y2(maxjfoil)
      common/wksp/cp,xn,yn,sn,cp2,x2,y2
c
c     cp = coefficient of pressure
c     n  = normal to surface
c     xn = chordwise direction (pos. in direction of LE to TE)
c     yn = normal to chord
c     sn = parameter approximating arclength (going from lower TE to
c          upper TE)
c     dx = d(xn)/d(sn)
c     dy = d(yn)/d(sn)
c     x2,y2,cp2 = double derivative of corresponding variable
c
      call splintder(jd,sn(iptr),xn(iptr),x2(iptr),stmp,dx)
      call splintder(jd,sn(iptr),yn(iptr),y2(iptr),stmp,dy)
      call splint(jd,sn(iptr),cp(iptr),cp2(iptr),stmp,cptmp)
c
      denom=dsqrt(dx**2+dy**2)
      ndotx=-dy/denom
      f=-cptmp*ndotx
      count=count+1
      return
      end
c
c
      double precision function f2(stmp)
c     -normal component of pressure
c
      include '../include/parms.inc'
      integer count,jd,iptr
      double precision dx,dy,cptmp,denom,ndoty,stmp
      common/var/jd,count,iptr
      dimension cp(maxjfoil),xn(maxjfoil),yn(maxjfoil)
      dimension sn(maxjfoil),cp2(maxjfoil)
      dimension x2(maxjfoil),y2(maxjfoil)
      common/wksp/cp,xn,yn,sn,cp2,x2,y2
c
      call splintder(jd,sn(iptr),xn(iptr),x2(iptr),stmp,dx)
      call splintder(jd,sn(iptr),yn(iptr),y2(iptr),stmp,dy)
      call splint(jd,sn(iptr),cp(iptr),cp2(iptr),stmp,cptmp)
c
      denom=dsqrt(dx**2+dy**2)
      ndoty=dx/denom
      f2=-cptmp*ndoty
      count=count+1
      return
      end
c
c
      double precision function f3(stmp)
c     -chord directed component of shear stress
c
      include '../include/parms.inc'
      integer count,jd,iptr
      double precision dx,dy,cftmp,denom,tdotx,stmp
      common/var/jd,count,iptr
      dimension cf(maxjfoil),xn(maxjfoil),yn(maxjfoil)
      dimension sn(maxjfoil),cf2(maxjfoil)
      dimension x2(maxjfoil),y2(maxjfoil)
      common/wksp/cf,xn,yn,sn,cf2,x2,y2
c
c     cf = coefficient of friction
c     t  = tangent to surface
c     xn = chordwise direction (pos. in direction of LE to TE)
c     yn = normal to chord
c     sn = parameter approximating arclength (going from lower TE to
c          upper TE)
c     dx = d(xn)/d(sn)
c     dy = d(yn)/d(sn)
c     x2,y2,cf2 = double derivative of corresponding variable
c
      call splintder(jd,sn(iptr),xn(iptr),x2(iptr),stmp,dx)
      call splintder(jd,sn(iptr),yn(iptr),y2(iptr),stmp,dy)
      call splint(jd,sn(iptr),cf(iptr),cf2(iptr),stmp,cftmp)
c
      denom=dsqrt(dx**2+dy**2)
      tdotx=dx/denom
      f3=cftmp*tdotx
      count=count+1
      return
      end
c
c
      double precision function f4(stmp)
c     -normal component of shear stress
c
      include '../include/parms.inc'
      integer count,jd,iptr
      double precision dx,dy,cftmp,denom,tdoty,stmp
      common/var/jd,count,iptr
      dimension cf(maxjfoil),xn(maxjfoil),yn(maxjfoil)
      dimension sn(maxjfoil),cf2(maxjfoil)
      dimension x2(maxjfoil),y2(maxjfoil)
      common/wksp/cf,xn,yn,sn,cf2,x2,y2
c
      call splintder(jd,sn(iptr),xn(iptr),x2(iptr),stmp,dx)
      call splintder(jd,sn(iptr),yn(iptr),y2(iptr),stmp,dy)
      call splint(jd,sn(iptr),cf(iptr),cf2(iptr),stmp,cftmp)
c
      denom=dsqrt(dx**2+dy**2)
      tdoty=dy/denom
      f4=cftmp*tdoty
      count=count+1
      return
      end
