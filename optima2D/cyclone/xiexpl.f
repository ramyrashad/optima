c #########################
c ##                     ##
c ##  subroutine xiexpl  ##
c ##                     ##
c #########################
c
      subroutine xiexpl(jdim,kdim,q,s,s0,press,sndsp,turmu,fmu,vort,x,y,
     &  xy,xyj,xit,ds,uu,ccx,coef2x,coef4x,spect,precon,gam)
c                                                                       
c     main xi integration calls
c                                                                       
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4)                                          
      dimension spect(jdim,kdim,3)                     
      dimension press(jdim,kdim),sndsp(jdim,kdim)                       
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension xit(jdim,kdim),ds(jdim,kdim),s0(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim),turmu(jdim,kdim)              
      dimension vort(jdim,kdim),fmu(jdim,kdim)
      dimension precon(jdim,kdim,6),gam(jdim,kdim,16)
c                                                                       
      dimension uu(jdim,kdim),ccx(jdim,kdim)                            
      dimension coef2x(jdim,kdim),coef4x(jdim,kdim)                     
c                                                                       
      common/scratch/ a,b,c,d,e,work1

      dimension a(maxpen,4),b(maxpen,16),c(maxpen,16)
      dimension d(maxjk,16),e(maxpen,4),work1(maxjk,10)


c     -- frozen jacobian arrays --
      double precision c2xf(maxj,maxk), c4xf(maxj,maxk)
      double precision c2yf(maxj,maxk), c4yf(maxj,maxk)
      double precision fmuf(maxj,maxk), turmuf(maxj,maxk)
      double precision vortf(maxj,maxk), sapdf(maxj,maxk)
      double precision sadx(maxj,maxk), sady(maxj,maxk)
      double precision suuf(maxj,maxk), svvf(maxj,maxk)
      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &      sapdf, sadx, sady, suuf, svvf 

c                                                                       
c                       explicit fluxes in xi
c                       =====================

      call rhsx (jdim,kdim,q,s,s0,press,xy,xyj,xit,work1)
c     
c     viscous rhs
c     ===========
c     
      if(viscous) then  

c     -compute vorticity for turbulence model
c     -Spalart-Allmaras model calculates it internally        
        if(turbulnt .and. itmodel.ne.2) 

     &        call vortdxi(jdim,kdim,q,vort,xy)  
c       !-- For thin-layer approximation (default), streamwise viscous 
        !-- flux derivatives are neglected, i.e. visxi = .false.
        if(visxi) call visrhsnx(jdim,kdim,q,press,s,turmu,fmu,xy,xyj,   
     &        work1(1,1), work1(1,2), work1(1,3), work1(1,4),
     &        work1(1,5), work1(1,6), work1(1,7),             
     &        work1(1,8), d)                       
      endif


c     
c     explicit artificial dissipation
c     ===============================
c     
c     
c---- 3-a-1. calculate eigenvalues for xi and eta ----
      ix = 1                                                  
      iy = 2         

      call eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,xit,uu,ccx)

c     -- optimization debug: freezing absolute values --
      if (frozen .and. ( ifrz_what.eq.1 .or. ifrz_what.eq.3)) then
         do k = 1,kend
            do j = 1,jend
               uu(j,k) = suuf(j,k)
            end do
         end do
      end if   

c     3-b. artificial dissipation terms : meth = 1,2,3,4,5
c     ----------------------------------------------------
c     
      if (idmodel.eq.2) then
c     *****************************************************************
c     **                       matrix diss model                     **
c     *****************************************************************
        ixy=1
        call mspectx(jdim,kdim,q,uu,ccx,xyj,xy,spect,sndsp)
        call gradcoef(ixy,jdim,kdim,press,xyj,coef2x,work1)
        call mcoef24x(jdim,kdim,coef2x,coef4x,work1)
        call expmatx(jdim,kdim,q,coef2x,coef4x,sndsp,s,spect,
     &        xyj,press,ccx,uu,xy,x,y,work1,work1(1,9), 
     &        work1(1,10),d,d(1,2),d(1,3),d(1,4),d(1,5),d(1,9))

      else if (idmodel.eq.1) then
c     *****************************************************************
c     **                       scalar diss model                     **
c     *****************************************************************
        ixy = 1                                                 
c---- form spectral radii for scalar dissipation model ----

        call spectx (jdim,kdim,uu,ccx,xyj,xy,coef4x,precon)

        call gradcoef(ixy,jdim,kdim,press,xyj,coef2x,work1)   

c     -- optimization debug: frozen pressure switch --
        if (frozen .and. ifrz_what.ge.2) then
           do k = 1,kend
              do j = 1,jend
                 coef2x(j,k) = c2xf(j,k)
              end do
           end do
        end if

        call coef24x(jdim, kdim, coef2x, coef4x, work1, work1(1,2),
     &       work1(1,5), work1(1,6)) 
     
        if (prec.gt.0) then
          call filterx2(jdim,kdim,q,s,xyj,gam,sndsp,precon,
     &          work1,work1(1,5),work1(1,6),work1(1,7),d)
c     
        else

          call filterx(jdim,kdim,q,s,s0,xyj,coef2x,coef4x,
     &          sndsp,precon,work1,work1(1,4))
        endif

      else if (idmodel.eq.3) then    
c     *****************************************************************
c     **             constant coefficient scalar diss model          **
c     *****************************************************************
        ixy = 1                                                 
c---- form spectral radii
        call spectx (jdim,kdim,uu,ccx,xyj,xy,coef4x,precon)
        call smoothx(jdim,kdim,q,s,xyj,coef4x,work1)          
c     
      else if (idmodel.eq.4) then
c     *****************************************************************
c     **                         cusp diss model                     **
c     *****************************************************************
        ixy = 1
        call dclax ( jdim,kdim,q,xyj,x,y,work1,d,press,coef2x,d(1,9) )
        if (prec.gt.0) then
          call dcexpx2 ( jdim,kdim,q,s,xyj,xy,press,x,y,precon,d,
     $          work1,work1(1,2),a,b,d(1,9),spect )
        else
          call dcexpx ( jdim,kdim,q,s,xyj,xy,press,x,y,spect,
     &          d,d(1,9),d(1,13),d(1,14),b,c,d(1,15) )
        end if
c     
      endif
c  
     
      return                                                          
      end                       !xiexpl
