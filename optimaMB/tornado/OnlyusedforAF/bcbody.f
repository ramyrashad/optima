CC ********************* BOUNDARY CONDITIONS **************************
c ** Application of solid wall boundary conditions on side 1        **
c
c     calling routine:  bcupdate
c     last revision: SDR 03/03/99
c
      subroutine bcbody(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,f,
     &                  j1,j2,ibcdir,kbc,idir)
c
c
c     Notes:   the j range for the bc's is j1 to j2
c              the line for application of bc is k = kbc 
c
c              if the flow is viscous and ibcdir=0 then apply 
c              inviscid wall conditions
c
C                                                       
#include "../include/parms.inc"
#include "../include/common.inc"
c                                                      
      dimension q(jdim,kdim,4),press(jdim,kdim)     
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)       
      dimension xit(jdim,kdim),ett(jdim,kdim)       
      dimension x(jdim,kdim),y(jdim,kdim),f(jdim)          
c
      kadd = sign(1,idir)
      sgn  = idir
c
c     -boundary conditions for surface boundary at k=kbegin
c
c     -This scaling is used if the calculation is not being restarted
c       It is used to bring the calculation up slowly, rather than   
c       impulsively.  It uses a sixth degree polynomial f            
c       for a smooth transition from scal slightly bigger than 0. at
c       to first iteration, to scal = 1. after the strtit'th iteration.
c       The scaling is turned off by restart if this is a restart run.
c                                                                       
      t = float(numiter - 1)/ strtit
      if(t.gt.1.) t = 1.                                              
      scal = (10. -15.*t + 6.*t*t) *t**3                                
c                                                                       
c     -no scaling if this is a restart                        
      if(restart) scal=1.                                             
c
      if(.not.viscous .or. ibcdir.eq.0) then
c       -inviscid bc at surface
c                                                                       
c       -obtain u and v on body via tangency and extrapolation of ucap
c       -if viscous flow, ucap = 0.0                                 
c       -first order extrapolation on cap-u                    
c                                                                       
        do 30 j=j1, j2
c                     
          kq = kbc + kadd
c                                                                       
          xyss = sqrt(xy(j,kq,3)**2+xy(j,kq,4)**2)
          xyssr = 1./xyss                                             
          xy3n2 = xy(j,kq,3)*xyssr
          xy4n2 = xy(j,kq,4)*xyssr 
          rr = 1./q(j,kq,1)
          u2 = q(j,kq,2)*rr
          v2 = q(j,kq,3)*rr
c     
          kq = kbc + 2*kadd
c     
          xyss = sqrt(xy(j,kq,3)**2+xy(j,kq,4)**2)
          xyssr = 1./xyss
          xy3n3 = xy(j,kq,3)*xyssr
          xy4n3 = xy(j,kq,4)*xyssr
          rr = 1./q(j,kq,1)
          u3 = q(j,kq,2)*rr
          v3 = q(j,kq,3)*rr
c 
c         -extrapolate tangential component of velocity
          utang = 2.*(xy4n2*u2 - xy3n2*v2) - (xy4n3*u3 - xy3n3*v3)
c
c         -set normal component to zer0
          vnorm = 0.
c
c         note !!! 12/13/84 thp  need to add unsteady terms
c
          kq = kbc
c     
          xyss = sqrt(xy(j,kq,3)**2+xy(j,kq,4)**2)
          xyssr = 1./xyss                                             
          xy3n1 = xy(j,kq,3)*xyssr
          xy4n1 = xy(j,kq,4)*xyssr
c     
          u1 =  xy4n1*utang + xy3n1*vnorm                             
          v1 = -xy3n1*utang + xy4n1*vnorm                             
          u1 = ( 1. - scal)*q(j,kq,2)/q(j,kq,1) + scal*u1
          v1 = ( 1. - scal)*q(j,kq,3)/q(j,kq,1) + scal*v1
c     
c         -q2 and q3 now satify tangency... independently of whether
c          density is lagged                                      
          q(j,kq,2) = u1*q(j,kq,1)
          q(j,kq,3) = v1*q(j,kq,1)
 30     continue
c
c                  
        kq  = kbc
        kq2 = kbc + kadd
        kq3 = kbc + 2*kadd
        do 50 j=j1,j2
          p2=press(j,kq2)*xyj(j,kq2)
          p3=press(j,kq3)*xyj(j,kq3)
          f(j) =2.d0*p2 - p3
 50     continue
c      
c       -hstfs is the stagnation freestream enthalpy           
        hstfs = 1./gami + 0.5*fsmach**2                                
        do 45 j=j1,j2                                                  
c                                                                       
c         rescale q2 and q3 to new density such that when      
c         reforming pressure  from q1 to q4,                   
c         the normal derivative of pressure is satified        
c         enforce constant free stream stagnation enthalpy at body
c                                                                       
          rinver = 1./q(j,kq,1)
          u = q(j,kq,2)*rinver
          v = q(j,kq,3)*rinver
          q(j,kq,1) = gamma*f(j)/(gami*(hstfs-0.5*(u**2 + v**2) ) )
          q(j,kq,1) = q(j,kq,1)/xyj(j,kq)
          q(j,kq,2) = u*q(j,kq,1)
          q(j,kq,3) = v*q(j,kq,1)
          f(j) = f(j)/xyj(j,kq)
          q(j,kq,4) = f(j)/gami +
     &          .5*(q(j,kq,2)**2+q(j,kq,3)**2)/q(j,kq,1)
 45     continue                                                       
c                                                                       
c
      elseif (viscous) then
c       -viscous surface bc on side 1
        kq = kbc
        kq2 = kq + kadd
        kq3 = kq + 2*kadd
        kq4 = kq + 3*kadd
c
        do 60 j=j1, j2                                                 
c         -cap u and cap v = 0.0
          u1 = (xy(j,kq,4)*(-xit(j,kq)) + xy(j,kq,2)*ett(j,kq))
     &             /xyj(j,kq)
          v1 = (- xy(j,kq,3)*(-xit(j,kq)) -xy(j,kq,1)*ett(j,kq))
     &             /xyj(j,kq)
          u1 = (1. - scal)*q(j,kq,2)/q(j,kq,1) + scal*u1               
          v1 = (1. - scal)*q(j,kq,3)/q(j,kq,1) + scal*v1               
c         -q2 and q3 now satify tangency
c         -note: q2 and q3 contain just velocity values and not
c                not momentum yet ... also have no jacobian
          q(j,kq,2) = u1
          q(j,kq,3) = v1
 60     continue

c       -surface condition on pressure and density
c        -dp/dn = 0.0 
c        -extrap p
c        -note: f(j) doesn't have jacobian in it
c        -first order of density extrapolation seems to      
c         be unstable for viscous calculations ... zeroeth order used. 

        if (iord.eq.2) then
          do 65 j =j1,j2                                               
            r2 = q(j,kq2,1)*xyj(j,kq2)
            p2=press(j,kq2)*xyj(j,kq2)
c            p3=press(j,kq3)*xyj(j,kq3)

            f(j)=p2                       !pressure
c            f(j)=2.*p2 - p3
            q(j,kq,1) = r2/xyj(j,kq)      !density 
 65       continue
        else
c         *****  higher-order  *****
          do 70 j =j1,j2
c           -pressure at the boundary            
            p2 = press(j,kq2)*xyj(j,kq2)
            p3 = press(j,kq3)*xyj(j,kq3)
            p4 = press(j,kq4)*xyj(j,kq4)
c
c           force dp/dn=0 on surface to ...
c           -second-order
c            f(j) = (4.d0*p2 - p3)/3.d0
c           -third-order
            f(j) = (18.d0*p2 - 9.d0*p3 + 2.d0*p4)/1.1d1
c
c
c           -density at the boundary
            r2 = q(j,kq2,1)*xyj(j,kq2)
            r3 = q(j,kq3,1)*xyj(j,kq3)
            r4 = q(j,kq4,1)*xyj(j,kq4)
c
c           -set d/dn (rho) = 0 (second-order)                 
c            q(j,k,1) = (4.d0*r2 - r3)*rj/3.d0
c           -set d/dn (rho) = 0 (third-order)                 
            q(j,kq,1) = (18.d0*r2 - 9.d0*r3 + 2.d0*r4)/(xyj(j,kq)*11.d0)
 70       continue
        endif
          
c                                                                       
        if (wtrat.ne.0.0) then
          do 72 j=j1,j2
c           -fix wall temp   tim  barth 10/10/84                
c           -twall is the ratio of twall/tinf                            
            prs = f(j)/xyj(j,kq)                                           
            twall = wtrat/gamma                                     
            q(j,kq,1) = prs/twall                                    
 72       continue
        endif
c
c       -now that density is already updated, convert q2 and q3 to momentum
        do 75 j = j1,j2
          q(j,kq,2) = q(j,kq,2)*q(j,kq,1)                                       
          q(j,kq,3) = q(j,kq,3)*q(j,kq,1)                                       
          q(j,kq,4) = f(j)/(gami*xyj(j,kq)) +                                   
     &          .5 * (q(j,kq,2)**2+q(j,kq,3)**2) /q(j,kq,1)
 75     continue
      endif                                                           
c
      return                                                           
      end                                                              
