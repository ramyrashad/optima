c----------------------------------------------------------------------
c     -- get airfoil coordinates from grid --
c     -- m. nemec --
c----------------------------------------------------------------------
      subroutine setup(x, y)

      implicit none

#include "mxblk.inc"
#include "mxfoil.inc"
#include "mxseg.inc"
#include "mxjk.inc"
#include "bsp.inc"

      integer maxjk
      parameter (maxjk=maxj*maxk)
      double precision x(maxjk), y(maxjk)

      integer nblks, jbmax(maxblk), kbmax(maxblk)
      common/mblock/ nblks, jbmax, kbmax

      integer lgptr(maxblk)
      common/ptrs/ lgptr

      integer nfoils, nsegments(maxfoil), isegblk(maxsegs,maxfoil)
      integer isegside(maxsegs,maxfoil)
      common/sortedfoils/ nsegments, isegblk, isegside, nfoils

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

c     -- local variables --
      integer ifoil, ipoints, iseg, ib, is
      double precision wk(ibody,2)

      do ifoil=1,nfoils
         ipoints = 1

c     -- extract the airfoil coordinates from grid data --
         do iseg=1,nsegments(ifoil)
            ib = isegblk(iseg,ifoil)
            is = isegside(iseg,ifoil)
            call getxy(ipoints, jbmax(ib), kbmax(ib), is, x(lgptr(ib)),
     &           y(lgptr(ib)), wk, ibody)
         end do

         ipoints = ipoints - 1
         ibap(ifoil) = ipoints
         
         do is = 1, ipoints
            bap(is,1,ifoil) = wk(is,1)
            bap(is,2,ifoil) = wk(is,2)
         end do

      end do

      return
      end                       !setup
