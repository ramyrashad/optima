c     ------------------------------------------------------------------
c     -- central-difference of residual vectors --
c     -- m. nemec, oct. 2001 --
c     -- called from dresdx.f --
c     ------------------------------------------------------------------
      subroutine fdrdx(dxx, js, je, ks, ke, jmax, kmax, Rp, Rm,
     &     dRdX, Rsap, Rsam, dRsadx, viscous, turbulnt)

      implicit none

      integer js,je,ks,ke,jmax,kmax,j,k,n
      double precision Rp(jmax,kmax,4),Rm(jmax,kmax,4),dRdX(jmax,kmax,4)
      double precision Rsap(jmax,kmax),Rsam(jmax,kmax),dRsadx(jmax,kmax)
      double precision dxx
      logical   viscous, turbulnt

      do n = 1,4
         do k = ks,ke
            do j = js,je
               dRdX(j,k,n) = ( Rp(j,k,n) - Rm(j,k,n) )*dxx
            end do
         end do
      end do

      if (viscous .and. turbulnt) then
         do k = ks,ke
            do j = js,je
               dRsadX(j,k) = ( Rsap(j,k) - Rsam(j,k) )*dxx
            end do
         end do
      end if

      return
      end                       !fdrdx
