!=======================================================================
!
      subroutine history(x, y, q, qold, ifun, turmu, xy, xyj, jdim, kdim
     &                    , mp )
!
!-------------------------------------------------------------------
! Purpose: This function writes the grid and solution to file at 
!          each iteration.
!
! Author:   Howard Buckley
!-------------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif


#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

! _______________________
! variable specification

      !-- Subroutine arguments
      integer ifun, jdim, kdim, mp                                  
      dimension q(jdim,kdim,4),qold(jdim,kdim,4),turmu(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim)   

      !-- Local variables
      integer d100, d10, d1, namelen, mp100, mp10, mp1
      character command*60, history_file_prefix*60
      character history_file_prefix0*60 

      double precision cpc, cp, pp
      double precision cftmp(maxj) 
! _____________________
! begin main execution

      !-- Create a unique history file prefix for each processor 
      !-- for example, if history_file_prefix is 'rest##', then 
      !-- processors with ranks 0, 1, 2,... will have 
      !-- history_file_prefix = rest##-00, rest##-01, rest##-02,...

      history_file_prefix = output_file_prefix0
      history_file_prefix0 = output_file_prefix0

#ifdef _MPI_VERSION
         d10  = rank / 10
         d1   = rank - (d10*10)
#else
         rank = 0
         d10  = 0 / 10
         d1   = 0 - (d10*10)
#endif

      !-- Create a unique history file prefix for each processor

      namelen =  len_trim(history_file_prefix)
      history_file_prefix(namelen+1:namelen+3)
     &         = '-'//char(d10+48)//char(d1+48)

      !-- If solution history logging is enabled then write soltion to 
      !-- file
      
      if (sol_his) then
         
         !-- Create file name for solution 'q'  --
         namelen =  len_trim(history_file_prefix)
         filena = history_file_prefix
         filena(namelen+1:namelen+9) = '.qhis'

         !-- Open file to store solution

         open(unit=991,file=filena,status='new',form='unformatted')

         !-- Write solution to file

         if(periodic .and. jmax.ne.jmaxold)then                          
         !-- special logic for overlap o grid case                   
         !-- set jmaxold point from j = 1 point  
           do k = 1,kmax                                             
           !-- note jacobian scaling ok                             
             q(jmaxold,k,1) = q(1,k,1)                                   
             q(jmaxold,k,2) = q(1,k,2)                                   
             q(jmaxold,k,3) = q(1,k,3)                                   
             q(jmaxold,k,4) = q(1,k,4)                                   
             xyj(jmaxold,k) = xyj(1,k)                                   
           end do
         endif   
     
         !-- rewind because this may be called periodically   
         if (unsted) then
           write(q2_unit) jmaxold,kmax    
           write(q2_unit) fsmach,alpha,re*fsmach,totime 
           write(q2_unit) (((qold(j,k,n)*xyj(j,k),j=1,jmaxold),k=1,kmax) 
     &           ,n=1,4)
           write(q2_unit) jtail1, numiter-1 

           if (itmodel.eq.2) then
             write(q2_unit) ((turold(j,k), j=1,jmaxold),k=1,kmax)
             write(q2_unit) ((turmu(j,k), j=1,jmaxold) ,k=1,kmax)
           endif
           rewind (q2_unit)
         endif

         write(991) jmaxold,kmax           
         write(991) fsmach,alpha,re*fsmach,totime    
         write(991) (((q(j,k,n)*xyj(j,k),j=1,jmaxold) ,k=1,kmax) ,n=1,4)
         write(991) jtail1, numiter             
                                                                       
         if (itmodel.eq.2) then
           write(991) ((turre(j,k), j=1,jmaxold) ,k=1,kmax)
           write(991) ((turmu(j,k), j=1,jmaxold) ,k=1,kmax)
         endif

         rewind(991)                  

                                      
         !-- Build a 'move' command that moves the .q file 
         !-- to the '/history/' directory
       
         namelen = len_trim(history_file_prefix)
         command = 'mv '
         command(4:60)=history_file_prefix
         command(namelen+4:60)='.qhis ./history/'
         command(namelen+20:60)=history_file_prefix

         namelen = len_trim(command)

         !-- Represent the design iteration number 'ifun' as a
         !-- string so that it can be appended to the file name

         d100 = ifun / 100
         d10  = (ifun - (d100*100) ) / 10
         d1   = ifun - (d100*100) - (d10*10)

         !-- Append the design iteration number, and  file
         !-- extension '.q' to the name of the file to be moved
         !-- to the '/history/' directory

         command(namelen+1:namelen+1) = '-'
         command(namelen+2:namelen+4) = 
     &       char(d100+48)//char(d10+48)//char(d1+48)
         command(namelen+5:namelen+6) = '.q'

         !-- Call the move command

         call system(command)

         close(991)

      end if ! (sol_his)

      !-- If grid history logging is enabled then write grid to file

      if ((grid_his).and.(rank.eq.0)) then

         !-- Create file name for grid  --
         namelen =  len_trim(history_file_prefix0)
         filena = history_file_prefix0
         filena(namelen+1:namelen+9) = '.ghist'

         !-- Open file to store solution

         open(unit=992,file=filena,status='new',form='unformatted')

         !-- Write grid to file
      
         write(992) jdim,kdim
         write(992) ((x(j,k),j=1,jdim),k=1,kdim),
     &     ((y(j,k),j=1,jdim),k=1,kdim)

         !-- Build a 'move' command that moves the .g file 
         !-- to the '/history/' directory
       
         namelen = len_trim(history_file_prefix0)
         command = 'mv '
         command(4:39)=history_file_prefix0
         command(namelen+4:39)='.ghist ./history/'
         command(namelen+21:39)=history_file_prefix0
         namelen = len_trim(command)

         !-- Represent the design iteration number 'ifun' as a
         !-- string so that it can be appended to the file name

         d100 = ifun / 100
         d10  = (ifun - (d100*100) ) / 10
         d1   = ifun - (d100*100) - (d10*10)

         !-- Append the design iteration number and  file
         !-- extension '.g' to the name of the file to be moved
         !-- to the '/history/' directory

         command(namelen+1:namelen+1) = '-'
         command(namelen+2:namelen+4) = 
     &       char(d100+48)//char(d10+48)//char(d1+48)
         command(namelen+5:namelen+14) = '.g'

         !-- Call the move command

         call system(command)

         close(992)

      end if ! (grid_his)

      !-- If cp history logging is enabled then write grid to file

      if ((cpp_his).and.(rank.eq.0)) then

         !-- Create file name for pressure coefficients  --
         namelen =  len_trim(history_file_prefix0)
         filena = history_file_prefix0
         filena(namelen+1:namelen+7) = '.cphis'

         !-- Open file to store pressure coefficients

         open(unit=993,file=filena,status='new',form='formatted')

        !-- write coefficient of pressure
        write (993,800) 
        write (993,801)
        write (993,802) ifun, alpha
 800    format('TITLE="Coefficient of Pressure"')
 801    format('VARIABLES="x/c","y/c","Cp"')
 802    format('ZONE T="It=',I3,', AoA=',f5.2,'"')

        cpc = 2.d0/(gamma*fsmach*fsmach)                                 
        do j = jtail1,jtail2                                          
           ! copied from calcps.f
           pp = gami*(q(j,1,4) -0.5*(q(j,1,2)**2 + q(j,1,3)**2)
     &           /q(j,1,1))
           pp = pp*xyj(j,1)
           cp = (pp*gamma-1.d0)*cpc
           write(993,803) x(j,1), y(j,1), cp
 803       format(3e25.16)
        end do
      
         !-- Build a 'move' command that moves the .cphis file 
         !-- to a .cp file in the '/history/' directory
       
         namelen = len_trim(history_file_prefix0)
         command = 'mv '
         command(4:60)=history_file_prefix0
         command(namelen+4:60)='.cphis ./history/'
         command(namelen+21:60)=history_file_prefix0
         namelen = len_trim(command)

         !-- Represent the design iteration number 'ifun' as a
         !-- string so that it can be appended to the file name

         d100 = ifun / 100
         d10  = (ifun - (d100*100) ) / 10
         d1   = ifun - (d100*100) - (d10*10)

         !-- Append the design iteration number and  file
         !-- extension '.cphis' to the name of the file to be moved
         !-- to the '/history/' directory

         command(namelen+1:namelen+1) = '-'
         command(namelen+2:namelen+4) = 
     &       char(d100+48)//char(d10+48)//char(d1+48)
         command(namelen+5:namelen+15) = '.cp'

         !-- Call the move command

         call system(command)

         close(993)

      end if ! (cpp_his)

      !-- If cf history logging is enabled then write to file

      if ((cf_his).and.(rank.eq.0)) then

         !-- Create file name for pressure coefficients  --
         namelen =  len_trim(history_file_prefix0)
         filena = history_file_prefix0
         filena(namelen+1:namelen+7) = '.cfhis'

         !-- Open file to store pressure coefficients

         open(unit=994,file=filena,status='new',form='formatted')

        !-- write coefficient of pressure
        write (994,900) 
        write (994,901)
        write (994,902) ifun, alpha
 900    format('TITLE="Skin Friction Coefficient"')
 901    format('VARIABLES="x/c","y/c","Cf"')
 902    format('ZONE T="It=',I3,', AoA=',f5.2,'"')

        call skinfrct(jdim,kdim,q,x,y,xy,cftmp)

        do j = jtail1,jtail2                                          
           write(994,903) x(j,1), y(j,1), cftmp(j)
 903       format(3e25.16)
        end do
      
         !-- Build a 'move' command that moves the .g file 
         !-- to the '/history/' directory
       
         namelen = len_trim(history_file_prefix0)
         command = 'mv '
         command(4:60)=history_file_prefix0
         command(namelen+4:60)='.cfhis ./history/'
         command(namelen+21:60)=history_file_prefix0
         namelen = len_trim(command)

         !-- Represent the design iteration number 'ifun' as a
         !-- string so that it can be appended to the file name

         d100 = ifun / 100
         d10  = (ifun - (d100*100) ) / 10
         d1   = ifun - (d100*100) - (d10*10)

         !-- Append the design iteration number and  file
         !-- extension '.cphis' to the name of the file to be moved
         !-- to the '/history/' directory

         command(namelen+1:namelen+1) = '-'
         command(namelen+2:namelen+4) = 
     &       char(d100+48)//char(d10+48)//char(d1+48)
         command(namelen+5:namelen+15) = '.cf'

         !-- Call the move command

         call system(command)

         close(994)

      end if ! (cf_his)

      return

      end subroutine history


