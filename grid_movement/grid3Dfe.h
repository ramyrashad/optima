#ifndef GRID3DFE_H
#define GRID3DFE_H
//#define ThreeD
                                                                              
#include "gridfe.h"               
#include "stiff3D.h"
#include "Vol.h"


void read_grid3Dfe (const char *filename, vector <Node>& node_loc, 
		  vector<Node*>& bnd_nodes, vector <Vol>& cells);

void form_cells (vector <Node>& node, const int& nblk, 
                 const int* jmax, const int* kmax, 
                 const int* mmax, vector <Vol>& cells);

void check_numbering (Node** corners, bool& check);

void form_stiff_hex (CompRow_Mat_double& alpha, 
		const vector <Node>& 
		node, const vector <Vol>& cells, 
		VECTOR_double &F);
void form_stiff_tet (CompRow_Mat_double& alpha, 
		const vector <Node>& 
		node, const vector <Vol>& cells, 
		VECTOR_double &F);

void construct_stiffness_matrix3D (int* colind, int* rowptr, 
				  vector <Node>& node);

void add_cell_nei (int *vertexID, vector <Node>& node);

void update3D (vector <Node>& node, const VECTOR_double& du);

#endif    // GRID3DFE_H
