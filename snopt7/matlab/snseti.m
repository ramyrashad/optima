% function snseti( option, ivalue )
%     Sets an optional INTEGER parameter of snopt.
%     The string "option" will be read
%     by snopt and assigned value "ivalue". If the string contains a
%     setting that snopt understands, snopt will set internal parameters
%     accordingly. For a description of available parameters, see
%     the snopt documentation.
%
%     Do not try to set the unit number of the summary or print file.
%     Use the MATLAB functions snsummary and snprintfile instead.
function snseti( option, ivalue )

%setoption = snoptcmex( 0, 'SetOptionI' );
setoption = 3;
snoptcmex( setoption, option, ivalue );