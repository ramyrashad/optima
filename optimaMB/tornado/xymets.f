C ********************************************************************
C ********************* METRIC CALCULATIONS **************************
C ********************************************************************
c calling routine: setup
      subroutine xymets(iblk,nblks,nhalo,jdim,kdim,
     &                 x,y,xy,xit,ett,xyj,
     &                 jbegin,jend,kbegin,kend,
     &                 jminbnd,jmaxbnd,kminbnd,kmaxbnd)

      implicit none
c
#include "../include/common.inc"
c     
      integer iblk,nblks,nhalo,jdim,kdim
      integer jbegin,jend,kbegin,kend,jminbnd,jmaxbnd,kminbnd,kmaxbnd
      double precision xy(jdim*kdim*4),xyj(jdim*kdim)                          
      double precision xit(jdim*kdim),ett(jdim*kdim)                           
      double precision x(jdim,kdim),y(jdim,kdim) 


      if ( sbbc .and. iblk .eq. 4 )  then
cmpr  angle of the perturbation slot for suction/blowing BC
         sdelta=-datan((y(jminbnd,36)-y(jminbnd,37))/
     &        (x(jminbnd,36)-x(jminbnd,37)))
      end if                         
c                                                                       
c ********************************************************************
c    compute xi derivatives of x, y                                     
c ********************************************************************

cmt ~~~ remember the NK version doesn't work with higher order methods
cmt ~~~ iord=4 needs to be implemented later
c      if (iord.eq.4) then
c        call xidif(jdim,kdim,x,y,xy,
c     &                 jbegin-nsd2,jend+nsd4,kminbnd,kmaxbnd)
c      else
        call xidif(jdim,kdim,x,y,xy,
     &                 jbegin,jend,kminbnd,kmaxbnd)
c      endif

c                                                                       
c ********************************************************************
c   compute  eta derivatives of x, y                                    
c ********************************************************************

        call etadif(iblk,nblks,jdim,kdim,x,y,xy,
     &                    jminbnd,jmaxbnd,kbegin,kend)

c ********************************************************************
c   form metrics and jacobian                                           
c ********************************************************************
c                                                                       
      if (jacarea) then
c       -calc. jacobian using cell areas
        call calcmet2(iblk,jdim,kdim,x,y,xy,xyj,
     &                      jminbnd,jmaxbnd,kminbnd,kmaxbnd)
      else
c       -calc. jacobian using metric terms
        call calcmet(iblk,nblks,nhalo,jdim,kdim,xy,xit,ett,xyj,
     &                     jminbnd,jmaxbnd,kminbnd,kmaxbnd)
      endif
c
      return                                                            
      end                                                               
