C ********************************************************************
C ************** LAMINAR VISCOSITY BASED ON SUTHERLANDS LAW  *********
C ********************************************************************
      subroutine fmun(jdim,kdim,q,press,fmu,sndsp,
     &                jbegin,jend,kbegin,kend)

      implicit none
c
c     calling routine: integrat.f
c
#include "../include/common.inc"
c
      integer jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision q(jdim,kdim,4),press(jdim,kdim)
      double precision sndsp(jdim,kdim),fmu(jdim,kdim)
      double precision c2b,c2bp,t1,t2
c
      c2b = 198.6d0/tinf
      c2bp = c2b + 1.
      do 11 k = kbegin,kend
      do 11 j = jbegin,jend
c        tt    = gamma*press(j,k)/q(j,k,1)
c        fmu(j,k) = c2bp*tt*sqrt(tt)/(c2b+tt)
c        fmu(j,k) = c2bp*tt**1.5/( c2b+tt)
c
        t1 = sndsp(j,k)
        t2 = t1**2
        fmu(j,k) = c2bp*t2*t1/(c2b+t2)
   11 continue
c
      return
      end
