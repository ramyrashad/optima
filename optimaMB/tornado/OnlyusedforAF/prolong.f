*********************************************************************
****  subroutine to prolong from a coarse block to a fine block  ****
*********************************************************************
c     Calling subroutine: main.f

      subroutine prolong(fblk,cblk,jdimf,kdimf,qf,xyjf,turref,jdimc,
     &      kdimc,qc,xyjc,turrec)

#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
c
      integer fblk,cblk,jdimf,kdimf,jdimc,kdimc
      dimension qf(jdimf,kdimf,4),xyjf(jdimf,kdimf)
      dimension qc(jdimc,kdimc,4),xyjc(jdimc,kdimc)
      dimension turref(jdimf,kdimf),turrec(jdimc,kdimc)

      jmnc=jminbnd(cblk)
      jmxc=jmaxbnd(cblk)
      kmnc=kminbnd(cblk)
      kmxc=kmaxbnd(cblk)
c     -remove the jacobian 
      do 10 n = 1,4
      do 10 k = kmnc,kmxc
      do 10 j = jmnc,jmxc
         qc(j,k,n) = qc(j,k,n)*xyjc(j,k)
 10   continue

c     -finding fine q in from LL corner to UR interior corner
      do 20 n=1,4
      do 20 kc = kmnc,kmxc-1
      do 20 jc = jmnc,jmxc-1
c        jf = (jc-(nhalo+1))*2 + (nhalo+1)
c        kf = (kc-(nhalo+1))*2 + (nhalo+1)
c        jf = (jc-jmnc)*2 + jmnc
c        kf = (kc-kmnc)*2 + kmnc
        jf = 2*jc-jmnc
        kf = 2*kc-kmnc
c       copy value at node:
        qf(jf,kf,n) = qc(jc,kc,n)
c       node b/w coarse nodes in k-dir:
        qf(jf,kf+1,n) = .5d0*(qc(jc,kc,n) + qc(jc,kc+1,n))
c       node b/w coarse nodes in j-dir:
        qf(jf+1,kf,n) = .5d0*(qc(jc,kc,n) + qc(jc+1,kc,n))
c       fine node formed from four coarse nodes:
        qf(jf+1,kf+1,n) = .25d0*(qc(jc,kc,n)   + qc(jc+1,kc,n) + 
     >                           qc(jc,kc+1,n) + qc(jc+1,kc+1,n))
 20   continue
c
c     -finding nodes on block side 3, except UR corner node
c
      kc = kmxc
      kf = 2*kc-kmnc

      do 30 n = 1,4
      do 30 jc = jmnc,jmxc-1
        jf = 2*jc-jmnc
c        jf = (jc-jmnc)*2 + jmnc
c       copy value at node:
        qf(jf,kf,n) = qc(jc,kc,n)
c       node b/w coarse nodes j & j+1:
        qf(jf+1,kf,n) = .5d0*(qc(jc,kc,n) + qc(jc+1,kc,n))
 30   continue


c     finding nodes on block side 4, except UR corner node
      jc = jmxc
      jf = 2*jc-jmnc

      do 40 n = 1,4
      do 40 kc= kmnc,kmxc-1
        kf = 2*kc-kmnc
c       copy value at node:
        qf(jf,kf,n) = qc(jc,kc,n)
c       node b/w coarse nodes j & j+1:
        qf(jf,kf+1,n) = .5d0*(qc(jc,kc,n) + qc(jc,kc+1,n))
 40   continue
c
c     form fine UR node:
      jc = jmxc
      kc = kmxc
      jf = 2*jc-jmnc
      kf = 2*kc-kmnc
c
      do 45 n = 1,4
        qf(jf,kf,n) = qc(jc,kc,n)
 45   continue
c
c     -put the jacobian back in q
c
c     fine q:
      do 50 n = 1,4
      do 50 k = kminbnd(fblk),kmaxbnd(fblk)
      do 50 j = jminbnd(fblk),jmaxbnd(fblk)
        qf(j,k,n) = qf(j,k,n)/xyjf(j,k)
 50   continue
c
c     coarse q:
      do 60 n = 1,4
      do 60 k = kmnc,kmxc
      do 60 j = jmnc,jmxc
         qc(j,k,n) = qc(j,k,n)/xyjc(j,k)
 60   continue
c

c     *******************************************
c           PROLONGUING TURRE WHEN REQUIRED
c     *******************************************
c
      if(gseq.and.turbulnt.and.iturb.lt.4) then
        call prlng_turre(fblk,cblk,jdimf,kdimf,turref,
     &                             jdimc,kdimc,turrec)
      endif
c
      return
      end
