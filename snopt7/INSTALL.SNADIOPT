INSTALL - Mike Gertz and Philip Gill         January 18, 2001

This file contains installation instructions for the
packages SNOPT and SNADIFOR. For a brief description
of these packages, see the accompanying README file.

The installation instructions that follow assume that
you are installing on a machine that runs a version of
the Unix-based operating system, such as Linux, IRIX or
SunOS. For instructions on how to install on
alternative operating systems, please see the
ALTERNATIVE OPERATING SYSTEMS section below.

THE BOTTOM LINE
---------------

If your system has all the necessary prerequisites (see
the PREREQUISITES section below) the installation
process will be quite simple. There is no harm in
trying the installation before you read the
PREREQUISITES section. If the installation fails, you
will learn what is missing.

Those familiar with the GNU installation process should
find no surprises here.  Use the cd command to the
working directory "snopt" containing the SNOPT
distribution.  At the command line, type

    ./configure
    make

The current directory should now contain a working
distribution which may be used immediately.  Try it
out. At the command line type:

   cd examples
   ./run snmain ../lib

Snopt should have solved a sample optimization problem
for you.

If this didn't work, read the PREREQUISITES and
TROUBLESHOOTING sections. To learn how to install into
a (more or less) permanent location, read the
INSTALLATION LOCATION section.

If sections below suggest setting certain options
before running configure, you may safety run
"configure" followed by "make" again.

You may read the man pages for snadiopt.pl in the
man/man1 directory:

	cd man/man1
	nroff -man snadiopt.pl.1 | more

(or just set your MANPATH environment variable)


PREREQUISITES
-------------

To use this package effectively, you need to have
several pieces of software installed on you machine:

1) SNOPT - This NLP solver performs the task of solving
   the given optimization problem. It is likely that
   you received SNOPT along with this package. Follow
   the installation instructions for the SNOPT package.

   SNOPT is built as a set of libraries. The
   "configure" script will try to locate these
   libraries in either the standard system locations,
   or in their default location (currently, a
   subdirectory of $HOME/out). If you build SNOPT in an
   alternative location, set the SNOPTLIBDIR
   environment variable to be the full path to the
   SNOPT libraries before running configure.  In bash
   you set environment variables with a command like:

	   export SNOPTLIBDIR=/the/correct/dir

   Notice that there are no spaces surrounding the
   "=". In csh, you set an environment variable with a
   command like

	   setenv SNOPTLIBDIR /the/correct/dir

   If you don't understand this, ask your system
   administrator.

   Finally, SNOPT is built as a set of "shared
   libraries". If you did not install SNOPT in one of
   the system directories (such as /usr/local/lib), you
   will probably need to tell the operating system
   where to find the SNOPT shared libraries. This is
   done by setting the LD_LIBRARY_PATH variable, for
   instance

       export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/the/correct/dir

   in bash or

       setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH:/the/correct/dir

    in csh.

2) ADIFOR - ADIFOR is a package that differentiates
   FORTRAN code. It is unlikely that you received
   ADIFOR with this distribution. You will find
   instruction on how to obtain ADIFOR on the web page:

      http://www-unix.mcs.anl.gov/autodiff/ADIFOR/

   This package is designed to work with ADIFOR version 2
   (specifically version 2.0D).

   There is no way for the "configure" script to guess
   where ADIFOR has been installed, so you *must* set
   the AD_LIB and AD_HOME environment variables before
   running configure. Normally AD_HOME will be set to
   the full path of a directory whose name is similar
   to "ADIFOR2.O". Similarly, AD_LIB will be point to a
   directory with a name similar to "ADIFOR2.O.lib".
   See the ADIFOR manual for more information.

   The configure script will try to guess the right
   value for the environment variable AD_OS, but you
   may need to set this variable if configure guesses
   wrong.

3) A working FORTRAN 77 compiler. The configure script
   will try to locate a FORTRAN compiler for you. If it
   cannot find one, or you do not like the one it
   chooses, set the F77 environment variable and run
   configure again.

4) perl - A text processing language. Perl is a popular
   language, and is available on most systems these
   days. Make sure that the version of perl that you
   are using is version 5 or better. You may find the
   version number by typing:

      perl --version

   (If that command doesn't give you a version number,
   then it is definitely NOT version 5.)

   The configure script will try to locate a version of
   perl that is installed on your machine. If you want
   to use a specific version of perl, set the PERL
   environment variable to point to the perl executable
   before running configure. If perl is not installed
   on your machine, copies are freely available at
   www.cpan.org.

5) GNU make - A utility to maintain groups of
   programs. You almost certainly have a version of
   make installed, which may or may not be gnu make. To
   find out, type:

       make --version

   If you get anything other than a message telling you
   that this is a version of GNU make, try:

       gmake --version

   If "gmake" isn't GNU make either, then GNU make
   isn't installed. You may obtain a copy at
   www.gnu.org. Although we try not to be too tricky,
   we do use some features of GNU make (and we trust
   that we will get more predictable behavior using GNU
   make than we would if we used vendor-specific
   versions of make.)

INSTALLATION LOCATION
---------------------

By default, if you type:

   make install

(after you have run configure), this package will try
to install itself into the /usr/local directory
tree. Of course, you will be unable to install to this
location unless you have privileged access to the
machine. Furthermore, even if you have privileged
access, your system administrator may want you to
install new packages in a different location.

If you are unsure that the package should be installed
in /usr/local, we recommend that you install in
$HOME/software (the pattern $HOME will be expanded to
the full path of your home directory). It is not
difficult to change the location of the installation
later.  You may install in $HOME/software by typing:

	./configure --prefix=$HOME/software
	make install

You may choose any other location by setting the appropriate prefix.


TROUBLESHOOTING
---------------

Read the PREREQUISITES section before you read this one.

PROBLEM) After installation, the command "snadiopt.pl"
    was not found.

SOLUTION) If you are using csh, type "rehash". If you
    are using bash, type "hash -r". (If you don't know
    what you are using, bring up a new command window.)
    Try "snadiopt.pl" again.

    If the command is still not found, then the file
    snadiopt.pl is not located in a directory that your
    system searches for executable files. The file
    snadiopt.pl is located in the subdirectory (of this
    directory) named "bin". You can always execute the
    command by typing the full path to the command on
    the command line. For example, typing:

       /path/to/this/package/bin/snadiopt.pl -o hs47 hs47.f

    in the examples directory should work (where, of
    course, you replace "/path/to/this/package" with
    the correct path.)

    Alternatively, ask your system administrator how to
    set your "PATH" environment variable.

PROBLEM) "configure" didn't complete successfully.

SOLUTION) This could be a bug in our configure
    script. However, the configure script is designed
    to validate the configuration of your system, and
    will stop under certain conditions (for instance,
    if the FORTRAN compiler can't create executables.)

PROBLEM) Typing "make" results in

        "make: Fatal error: No arguments to build"

    or some similar message.

SOLUTION) The version of make that you are using is
    probably not GNU make. Try typing "gmake"
    instead. If that doesn't work, then GNU make is not
    installed. See PREREQUISITES for more information.

PROBLEM) The example program compiles (and links)
   successfully, but will not run. It gives an error
   similar to:

      hs47: error in loading shared libraries: libsnopt.so: cannot
	  open shared object file: No such file or directory

SOLUTION) You must tell your operating system the
   location of the SNOPT libraries. See the SNOPT
   subsection of PREREQUISITES section above.


ALTERNATIVE OPERATING SYSTEMS
-----------------------------

 The FORTRAN code used in this package and the FORTRAN
code generated by ADIFOR is reasonably close to the
FORTRAN 77 standard, and should compile on most
platforms. The source for the ADIFOR libraries and
instructions on how to compile this source are provided
as part of the ADIFOR distribution. The ADIFOR program
itself, however, is only available on certain
platforms, currently certain version of Unix and
Windows. See:

   http://www-unix.mcs.anl.gov/autodiff/ADIFOR/

for more information. We have no control over which
platforms are supported.

Macintosh - MacOS X is based on a version of
            UNIX. Developer versions of MacOS X are
            already available, and there will be a
            public beta this summer. At this point, we
            wouldn't recommend anyone trying to port
            this package to an older version of
            MacOS. There is currently not a version of
            the ADIFOR program available for MacOS X.

  Windows - You should be able to use this package with
            the Cygwin tools:

               http://sourceware.cygnus.com/cygwin/

