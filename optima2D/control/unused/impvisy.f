c----------------------------------------------------------------------
c     -- implicit thin-layer viscous terms in y-direction --
c     -- based on probe, a. pueyo --
c     -- written by: marian nemec --
c     -- date: november, 2000
c----------------------------------------------------------------------

      subroutine impvisy (jdim, kdim, indx, xy, xyj, q, fmu, turmu, as,
     &      pa, a, q2, dj, ddj)

#include "../include/arcom.inc"

      integer jdim, kdim, indx(jdim,kdim)

      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision as(jdim*kdim*4*36), pa(jdim*kdim*4*20)
      double precision fmu(jdim,kdim), turmu(jdim,kdim)
      double precision dj(jdim,kdim,3,4), ddj(jdim,kdim,3,4)
      double precision a(jdim,kdim,4), q2(jdim,kdim,4)

      data pr, prtr /0.72, 0.8/
      re2 = 1.d0/(2.d0*re)
      gpr = gamma/pr
      f43 = 4.d0/3.d0
      f13 = 1.d0/3.d0

c     -- compute alfas and non-conserv. variables --
      do k = kbegin,kend
        do j = jlow,jup
          rinv   = 1.d0/q(j,k,1)                                        
          exs    = xy(j,k,3)**2                                       
          eys    = xy(j,k,4)**2                                       
          exy    = xy(j,k,3)*xy(j,k,4)*f13
          re2j   = re2/xyj(j,k)
c     -- alfas --
          a(j,k,1) = re2j * (f43*exs+eys)                          
          a(j,k,2) = re2j * exy                                       
          a(j,k,3) = re2j * (exs+f43*eys)                          
          a(j,k,4) = re2j * gpr * (exs+eys)                          
c     -- non-cons. variables --
          q2(j,k,1) = rinv                                           
          q2(j,k,2) = rinv*q(j,k,2)                                  
          q2(j,k,3) = rinv*q(j,k,3)                                  
          q2(j,k,4) = rinv*q(j,k,4) - (q2(j,k,2)**2 +q2(j,k,3)**2)
        end do
      end do

c     -- compute alf_{k+1/2}*bet_{k+1} & alf_{k+1/2}*bet_{k} --
      do k = kbegin,kup
        kp = k + 1
        do j = jlow,jup
c     -- average fmu to j,k+1/2 point; turmu is at j,k+1/2 --
          fmum = 0.5d0 * (fmu(j,kp)+fmu(j,k))
          turm = turmu(j,k)                   
          vnu  = fmum + turm                  
          gkap = fmum + prtr*turm             
c     -- evaluate alfas at k+1/2 --
          ah1 = a(j,k,1) + a(j,kp,1)
          ah2 = a(j,k,2) + a(j,kp,2)
          ah3 = a(j,k,3) + a(j,kp,3)
          ah4 = a(j,k,4) + a(j,kp,4)
          ah1 = ah1*vnu                 
          ah2 = ah2*vnu                 
          ah3 = ah3*vnu                 
          ah4 = ah4*gkap                
c     -- entries on M (viscous) Jacobian, at k+1/2 --
          dj  (j,k,1,1) = -( ah1*q2(j,k,2)  + ah2*q2(j,k,3)) *q2(j,k,1)
          ddj (j,k,1,1) = -( ah1*q2(j,kp,2) + ah2*q2(j,kp,3))*q2(j,kp,1)
          dj  (j,k,1,2) = ah1*q2(j,k,1)                              
          ddj (j,k,1,2) = ah1*q2(j,kp,1)
          dj  (j,k,1,3) = ah2*q2(j,k,1)
          ddj (j,k,1,3) = ah2*q2(j,kp,1)
          dj  (j,k,1,4) = 0.d0
          ddj (j,k,1,4) = 0.d0
          dj  (j,k,2,1) = -( ah2*q2(j,k,2) + ah3*q2(j,k,3))  *q2(j,k,1)
          ddj (j,k,2,1) = -( ah2*q2(j,kp,2) + ah3*q2(j,kp,3))*q2(j,kp,1)
          dj  (j,k,2,2) = ah2*q2( j, k,1)                              
          ddj (j,k,2,2) = ah2*q2( j, kp,1)                            
          dj  (j,k,2,3) = ah3*q2( j, k,1)                              
          ddj (j,k,2,3) = ah3*q2( j, kp,1)                            
          dj  (j,k,2,4) = 0.                                          
          ddj (j,k,2,4) = 0.                                         
          dj  (j,k,3,1) = -(ah4*q2(j,k,4) + ah1*q2(j,k,2)**2 + 2.*ah2
     &          *q2(j,k,2)*q2(j,k,3) + ah3*q2(j,k,3)**2)*q2(j,k,1)
          ddj (j,k,3,1) = -(ah4*q2(j,kp,4) + ah1*q2(j,kp,2)**2 + 2.*ah2
     &          *q2(j,kp,2)*q2(j,kp,3) + ah3*q2(j,kp,3)**2)*q2(j,kp,1)
          dj  (j,k,3,2) = -ah4*q2(j,k,2)*q2(j,k,1) - dj(j,k,1,1)        
          ddj (j,k,3,2) = -ah4*q2(j,kp,2)*q2(j,kp,1) - ddj(j,k,1,1)    
          dj  (j,k,3,3) = -ah4*q2(j,k,3)*q2(j,k,1) - dj(j,k,2,1)        
          ddj (j,k,3,3) = -ah4*q2(j,kp,3)*q2(j,kp,1) - ddj(j,k,2,1)    
          dj  (j,k,3,4) = ah4*q2(j,k,1)                              
          ddj (j,k,3,4) = ah4*q2(j,kp,1)                            
        end do
      end do

c     -- preconditioner: differencing and storing --
      if ( prec_mat ) then 
        do n  = 2,4 
          do k = klow,kup
            do j = jlow,jup
              ij  = ( indx(j,k) - 1 )*4 + n
              ii  = ( ij - 1 )*20
              do m = 1,4
                pa(ii+4+m)  = pa(ii+4+m)  - dj(j,k-1,n-1,m)
                pa(ii+8+m)  = pa(ii+8+m)  + dj(j,k,n-1,m)
     &                + ddj(j,k-1,n-1,m)
                pa(ii+12+m) = pa(ii+12+m) - ddj(j,k,n-1,m) 
              end do
            end do
          end do
        end do
      end if

c     -- second order jacobian: differencing and storing --
c     -- interior points --
      if ( jac_mat ) then
        do j = jlow+1,jup-1
          do k = klow+1,kup-1
            do n  = 2,4 
              ij  = ( indx(j,k) - 1 )*4 + n
              ii  = ( ij - 1 )*36
              do m =1,4
                as(ii+12+m) = as(ii+12+m) - dj(j,k-1,n-1,m)
                as(ii+16+m) = as(ii+16+m) + dj(j,k,n-1,m)
     &                + ddj(j,k-1,n-1,m)
                as(ii+20+m) = as(ii+20+m) - ddj(j,k,n-1,m) 
              end do
            end do
          end do
        end do

c     -- boundary nodes --
        j = jlow
        do k = klow+1,kup-1
          do n  = 2,4 
            ij  = ( indx(j,k) - 1 )*4 + n
            ii  = ( ij - 1 )*36
            do m =1,4
              as(ii+8+m)  = as(ii+8+m)  - dj(j,k-1,n-1,m)
              as(ii+12+m) = as(ii+12+m) + dj(j,k,n-1,m)
     &              + ddj(j,k-1,n-1,m)
              as(ii+16+m) = as(ii+16+m) - ddj(j,k,n-1,m) 
            end do
          end do
        end do

        j = jup
        do k = klow+1,kup-1
          do n  = 2,4 
            ij  = ( indx(j,k) - 1 )*4 + n
            ii  = ( ij - 1 )*36
            do m =1,4
              as(ii+12+m) = as(ii+12+m) - dj(j,k-1,n-1,m) 
              as(ii+16+m) = as(ii+16+m) + dj(j,k,n-1,m)
     &              + ddj(j,k-1,n-1,m)
              as(ii+20+m) = as(ii+20+m) - ddj(j,k,n-1,m)
            end do
          end do
        end do

        k = klow
        do j = jlow+1,jup-1
          do n  = 2,4 
            ij  = ( indx(j,k) - 1 )*4 + n
            ii  = ( ij - 1 )*36
            do m =1,4
              as(ii+8+m)  = as(ii+8+m)  - dj(j,k-1,n-1,m)  
              as(ii+12+m) = as(ii+12+m) + dj(j,k,n-1,m)
     &              + ddj(j,k-1,n-1,m)
              as(ii+16+m) = as(ii+16+m) - ddj(j,k,n-1,m)
            end do
          end do
        end do

        k = kup
        do j = jlow+1,jup-1
          do n  = 2,4 
            ij  = ( indx(j,k) - 1 )*4 + n
            ii  = ( ij - 1 )*36
            do m =1,4
              as(ii+12+m) = as(ii+12+m) - dj(j,k-1,n-1,m)  
              as(ii+16+m) = as(ii+16+m) + dj(j,k,n-1,m)
     &              + ddj(j,k-1,n-1,m)
              as(ii+20+m) = as(ii+20+m) - ddj(j,k,n-1,m)
            end do
          end do
        end do

        j = jlow
        k = klow
        do n  = 2,4 
          ij  = ( indx(j,k) - 1 )*4 + n
          ii  = ( ij - 1 )*36
          do m =1,4
            as(ii+4+m)  = as(ii+4+m)  - dj(j,k-1,n-1,m)  
            as(ii+8+m)  = as(ii+8+m)  + dj(j,k,n-1,m)
     &            + ddj(j,k-1,n-1,m)
            as(ii+12+m) = as(ii+12+m) - ddj(j,k,n-1,m)
          end do
        end do

        j = jlow
        k = kup
        do n  = 2,4 
          ij  = ( indx(j,k) - 1 )*4 + n
          ii  = ( ij - 1 )*36
          do m =1,4
            as(ii+8+m)  = as(ii+8+m)  - dj(j,k-1,n-1,m)  
            as(ii+12+m) = as(ii+12+m) + dj(j,k,n-1,m)
     &            + ddj(j,k-1,n-1,m)
            as(ii+16+m) = as(ii+16+m) - ddj(j,k,n-1,m)
          end do
        end do

        j = jup
        k = klow
        do n  = 2,4 
          ij  = ( indx(j,k) - 1 )*4 + n
          ii  = ( ij - 1 )*36
          do m =1,4
            as(ii+8+m)  = as(ii+8+m)  - dj(j,k-1,n-1,m)  
            as(ii+12+m) = as(ii+12+m) + dj(j,k,n-1,m)
     &            + ddj(j,k-1,n-1,m)
            as(ii+16+m) = as(ii+16+m) - ddj(j,k,n-1,m)
          end do
        end do

        j = jup
        k = kup
        do n  = 2,4 
          ij  = ( indx(j,k) - 1 )*4 + n
          ii  = ( ij - 1 )*36
          do m =1,4
            as(ii+12+m) = as(ii+12+m) - dj(j,k-1,n-1,m)  
            as(ii+16+m) = as(ii+16+m) + dj(j,k,n-1,m)
     &            + ddj(j,k-1,n-1,m)
            as(ii+20+m) = as(ii+20+m) - ddj(j,k,n-1,m)
          end do
        end do      
      end if

      return                                                            
      end                       !impvisy
