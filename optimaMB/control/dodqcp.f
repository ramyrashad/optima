c     ------------------------------------------------------------------
c     -- copy sensitivity of objective function at trailing edge (and
c     leading edge) points --
c     -- m. nemec, oct. 2001 --
c     -- called from: dobjdq.f --
c     ------------------------------------------------------------------
      subroutine dodqcp( ijte1, ikte1, ijte2, ikte2, jmax1, kmax1,
     &     dOdQ1, jmax2, kmax2, dOdQ2)

      implicit none
      integer ijte1,ikte1,ijte2,ikte2,jmax1,kmax1,jmax2,kmax2,n
      double precision dOdQ1(jmax1,kmax1,4), dOdQ2(jmax2,kmax2,4)

      do n=1,4
         dOdQ2(ijte2,ikte2,n) = dOdQ1(ijte1,ikte1,n)
      end do

      return
      end                       !dodqcp
