c---- add_dJdX ---------------------------------------------------------
c Computes the derivative of the objective function, J, with respect to
c the design variables, X, and adds it to A.  This is done with the
c grid, G, and the flowfield, Q, held constant.

c Note that most design variables only effect the objective function
c through the grid or the flowfield.  However, the angle of attack
c directly effects J, so this function computes dJ/d(alhpa).
c
c Here, J is considered to be defined as
c   J = J(Q, G, X)
c where Q, G, and X are independant variables.  Therefore, dJ/dX is
c evaluated with Q and G held constant (i.e. not with Q hat held constant)
c
c Requires:
c obj0: initial objective function
c
c Returns:
c A: with the derivative of the objective function with respect to the
c   design variables added to it.
c
c Limitations:
c
c Chad Oldfield, September 2005.
c-----------------------------------------------------------------------
      subroutine add_dJdX(A, obj0)
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     Argument data types; see above for descriptions
      double precision A(ndv), obj0

c     dJ/dX is nonzero only if the angle of attack is a design variable.
      if (ndv == ngdv) then
        return
      endif

      if (obj_func == 1) then
c       Inverse design (match target pressure distribution).
c       Cp is independent of X, so dJ/dX = 0.
      elseif (obj_func == 2) then
c       Not active.
      elseif (obj_func == 3) then
c       Not active.
      elseif (obj_func == 4) then
c       Not active.
      elseif (obj_func == 5) then
c       Maximize lift to drag ratio.
        A(ndv) = A(ndv) + (1.d0 + (cdt/clt)**2)*pi/180.d0/obj0
      elseif (obj_func == 6) then
c       Lift-constrained drag minimization.
        A(ndv) = A(ndv) + (- wfd*(1.d0-cdt/cd_tar)*clt/cd_tar
     |                     + wfl*(1.d0-clt/cl_tar)*cdt/cl_tar)*pi/90.d0
     |                    /obj0
      elseif (obj_func == 7) then
c       Maximize lift subject to a moment constraint.
        A(ndv) = A(ndv) + wfl*(1.d0-clt/cl_tar)*cdt*pi/(cl_tar*90.d0)
     |           /obj0
      elseif (obj_func == 8) then
        A(ndv) = clt*pi/180.d0/obj0
      elseif (obj_func == 9) then
c       Inverse design gridshape (match remote target pressure distribution).
c       Cp is independent of X, so dJ/dX = 0.
      elseif (obj_func == 12) then
c       Inverse design boxshape (match remote target pressure distribution).
c       Cp is independent of X, so dJ/dX = 0.
      endif

      end
c---- add_dJdX ---------------------------------------------------------
