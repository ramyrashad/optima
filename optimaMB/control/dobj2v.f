c     ------------------------------------------------------------------
c     -- linearization of the lift-drag objective function with
c     respect to the q vector, viscous flow --
c     -- m. nemec, nov. 2001 --
c     -- called from: dobjdq.f --
c     ------------------------------------------------------------------
      subroutine dobj2v( is, ifh, isgh, ibcdir, nsegments, cdis, cdvs,
     &     clis, clvs, maxs, maxf, nfoils, jmax, kmax, js, je, ks, ke, 
     &     q, dOdQ, press, xyj, xy, x, y, gami)

      implicit none
      
#include "../include/parms.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer is,ifh,isgh,ibcdir,maxs,maxf,nfoils,jmax,kmax,js,je,ks,ke
      integer ifoil,iseg,k,k1,k2,ki,j,j1,j2,ji,n,nsegments(maxf)
      double precision q(jmax,kmax,4),  dOdQ(jmax,kmax,4),xyj(jmax,kmax)
      double precision xy(jmax,kmax,4), press(jmax,kmax)
      double precision x(jmax,kmax),    y(jmax,kmax)
      double precision cdis(maxs,maxf),cdvs(maxs,maxf),objp,objm
      double precision clis(maxs,maxf),clvs(maxs,maxf)
      double precision cdth,clth,qtmp,stepsize,pp,gami,press_tmp
      double precision cli, cdi, cmi, clv, cdv, cmv, cdtp, cltp 
      
c     -- find the total lift and drag --
      cdth = 0.d0
      clth = 0.d0
      do ifoil = 1,nfoils
         do iseg = 1,nsegments(ifoil)
            cdth = cdth + cdis(iseg,ifoil) + cdvs(iseg,ifoil)
            clth = clth + clis(iseg,ifoil) + clvs(iseg,ifoil)
         end do
      end do

c     -- subtract the current segment --
      cdth = cdth - cdis(isgh,ifh) - cdvs(isgh,ifh)
      clth = clth - clis(isgh,ifh) - clvs(isgh,ifh)

      if (is.eq.1) then
         k = ks
         k1= ks+1
         k2= ks+2
         ki= 1
      else if (is.eq.3) then
         k = ke
         k1= ke-1
         k2= ks-2
         ki= -1
      else
         if (is.eq.2) then
            j = js
            j1= js+1
            j2= js+2
            ji= 1
            goto 200
         else
            stop 'dobj2v.f: not setup for side4'
         endif
      end if

      do n = 1,4,3
         do j = js,je
            qtmp = q(j,k,n)
            stepsize = fd_eta*qtmp

            if ( abs(stepsize) .lt. 1.e-10 ) then
               stepsize = 1.e-10*sign(1.0,stepsize)
            end if

c     -- positive perturbation --
            q(j,k,n) = q(j,k,n) + stepsize
            stepsize = q(j,k,n) - qtmp

            pp = gami*( q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)
     &           /q(j,k,1) )
            press_tmp = press(j,k)
            press(j,k) = pp

            call clcd( jmax, kmax, is, ibcdir, q, press, x, y, xy, xyj,
     &           1, cli, cdi, cmi, clv, cdv, cmv, js, je, ks, ke)

            cdtp = cdi + cdv + cdth
            cltp = cli + clv + clth
           
            if (cdtp.gt.cd_tar) then
               objp = wfd*( 1.0 - cdtp/cd_tar )**2
            else
               objp = 0.d0
            end if

            if (iobjf.eq.3) cltp = cltp/cdtp
            objp = objp + wfl*( 1.0 - cltp/cl_tar )**2

            press(j,k) = press_tmp

c     -- negative perturbation --
            q(j,k,n) = qtmp - stepsize
            pp = gami*( q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)
     &           /q(j,k,1) )
            press_tmp = press(j,k)
            press(j,k) = pp

            call clcd( jmax, kmax, is, ibcdir, q, press, x, y, xy, xyj,
     &           1, cli, cdi, cmi, clv, cdv, cmv, js, je, ks, ke)

            cdtp = cdi + cdv + cdth
            cltp = cli + clv + clth

            if (cdtp.gt.cd_tar) then
               objm = wfd*( 1.0 - cdtp/cd_tar )**2
            else
               objm = 0.d0
            end if

            if (iobjf.eq.3) cltp = cltp/cdtp
            objm = objm + wfl*( 1.0 - cltp/cl_tar )**2

            press(j,k) = press_tmp
            q(j,k,n) = qtmp  

            dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.0*stepsize)
         end do
      end do

      do n = 1,4
         do k = k1,k2,ki
            do j = js,je
               qtmp = q(j,k,n)
               stepsize = fd_eta*qtmp

               if ( abs(stepsize) .lt. 1.e-10 )
     &              stepsize = 1.e-10*sign(1.0,stepsize)

c     -- positive perturbation --
               q(j,k,n) = q(j,k,n) + stepsize
               stepsize = q(j,k,n) - qtmp

               pp = gami*( q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)
     &              /q(j,k,1) )
               press_tmp = press(j,k)
               press(j,k) = pp

               call clcd( jmax, kmax, is, ibcdir, q, press, x, y, xy,
     &              xyj,1, cli, cdi, cmi, clv, cdv, cmv, js, je, ks, ke)

               cdtp = cdi + cdv + cdth
               cltp = cli + clv + clth
               
               if (cdtp.gt.cd_tar) then
                  objp = wfd*( 1.0 - cdtp/cd_tar )**2
               else
                  objp = 0.d0
               end if

               if (iobjf.eq.3) cltp = cltp/cdtp
               objp = objp + wfl*( 1.0 - cltp/cl_tar )**2

               press(j,k) = press_tmp

c     -- negative perturbation --
               q(j,k,n) = qtmp - stepsize
               pp = gami*( q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)
     &              /q(j,k,1) )
               press_tmp = press(j,k)
               press(j,k) = pp

               call clcd( jmax, kmax, is, ibcdir, q, press, x, y, xy,
     &              xyj,1, cli, cdi, cmi, clv, cdv, cmv, js, je, ks, ke)

               cdtp = cdi + cdv + cdth
               cltp = cli + clv + clth

               if (cdtp.gt.cd_tar) then
                  objm = wfd*( 1.0 - cdtp/cd_tar )**2
               else
                  objm = 0.d0
               end if

               if (iobjf.eq.3) cltp = cltp/cdtp
               objm = objm + wfl*( 1.0 - cltp/cl_tar )**2

               press(j,k) = press_tmp
               q(j,k,n) = qtmp  

               dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.0*stepsize)
            end do
         end do
      end do

      goto 999

 200   continue

      do n = 1,4,3
         do k = ks,ke
            qtmp = q(j,k,n)
            stepsize = fd_eta*qtmp

            if ( abs(stepsize) .lt. 1.e-10 ) then
               stepsize = 1.e-10*sign(1.0,stepsize)
            end if

c     -- positive perturbation --
            q(j,k,n) = q(j,k,n) + stepsize
            stepsize = q(j,k,n) - qtmp

            pp = gami*( q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)
     &           /q(j,k,1) )
            press_tmp = press(j,k)
            press(j,k) = pp

            call clcd( jmax, kmax, is, ibcdir, q, press, x, y, xy, xyj,
     &           1, cli, cdi, cmi, clv, cdv, cmv, js, je, ks, ke)

            cdtp = cdi + cdv + cdth
            cltp = cli + clv + clth
            
            if (cdtp.gt.cd_tar) then
               objp = wfd*( 1.0 - cdtp/cd_tar )**2
            else
               objp = 0.d0
            end if

            if (iobjf.eq.3) cltp = cltp/cdtp
            objp = objp + wfl*( 1.0 - cltp/cl_tar )**2

            press(j,k) = press_tmp

c     -- negative perturbation --
            q(j,k,n) = qtmp - stepsize
            pp = gami*( q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)
     &           /q(j,k,1) )
            press_tmp = press(j,k)
            press(j,k) = pp

            call clcd( jmax, kmax, is, ibcdir, q, press, x, y, xy, xyj,
     &           1, cli, cdi, cmi, clv, cdv, cmv, js, je, ks, ke)

            cdtp = cdi + cdv + cdth
            cltp = cli + clv + clth

            if (cdtp.gt.cd_tar) then
               objm = wfd*( 1.0 - cdtp/cd_tar )**2
            else
               objm = 0.d0
            end if

            if (iobjf.eq.3) cltp = cltp/cdtp
            objm = objm + wfl*( 1.0 - cltp/cl_tar )**2

            press(j,k) = press_tmp
            q(j,k,n) = qtmp  

            dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.0*stepsize)
         end do
      end do

      do n = 1,4
         do j = j1,j2,ji
            do k = ks,ke
               qtmp = q(j,k,n)
               stepsize = fd_eta*qtmp

               if ( abs(stepsize) .lt. 1.e-10 )
     &              stepsize = 1.e-10*sign(1.0,stepsize)

c     -- positive perturbation --
               q(j,k,n) = q(j,k,n) + stepsize
               stepsize = q(j,k,n) - qtmp

               pp = gami*( q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)
     &              /q(j,k,1) )
               press_tmp = press(j,k)
               press(j,k) = pp

               call clcd( jmax, kmax, is, ibcdir, q, press, x, y, xy,
     &              xyj,1, cli, cdi, cmi, clv, cdv, cmv, js, je, ks, ke)

               cdtp = cdi + cdv + cdth
               cltp = cli + clv + clth
               
               if (cdtp.gt.cd_tar) then
                  objp = wfd*( 1.0 - cdtp/cd_tar )**2
               else
                  objp = 0.d0
               end if

               if (iobjf.eq.3) cltp = cltp/cdtp
               objp = objp + wfl*( 1.0 - cltp/cl_tar )**2

               press(j,k) = press_tmp

c     -- negative perturbation --
               q(j,k,n) = qtmp - stepsize
               pp = gami*( q(j,k,4) - 0.5*( q(j,k,2)**2 + q(j,k,3)**2)
     &              /q(j,k,1) )
               press_tmp = press(j,k)
               press(j,k) = pp

               call clcd( jmax, kmax, is, ibcdir, q, press, x, y, xy,
     &              xyj,1, cli, cdi, cmi, clv, cdv, cmv, js, je, ks, ke)

               cdtp = cdi + cdv + cdth
               cltp = cli + clv + clth

               if (cdtp.gt.cd_tar) then
                  objm = wfd*( 1.0 - cdtp/cd_tar )**2
               else
                  objm = 0.d0
               end if

               if (iobjf.eq.3) cltp = cltp/cdtp
               objm = objm + wfl*( 1.0 - cltp/cl_tar )**2

               press(j,k) = press_tmp
               q(j,k,n) = qtmp  

               dOdQ(j,k,n) = xyj(j,k)*( objp - objm ) / (2.0*stepsize)
            end do
         end do
      end do




 999  continue

      return 
      end                       !dobj2v
