      subroutine calc_Dhat(jdim,kdim,s,D_hat)
c                                                                       
#include "../include/arcom.inc"
c
      double precision D_hat(jdim,kdim,4,6)
      dimension s(jdim,kdim,4)
c     
      do 500 n=1,4
      do 500 k=klow,kup
      do 500 j=jlow,jup
c            D_hat(j,k,n,jstage)=s(j,k,n)+D_hat(j,k,n,jstage)
             D_hat(j,k,n,jstage)=s(j,k,n)
 500  continue
c
      return
      end                                                       
                                                       
