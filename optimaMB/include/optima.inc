c     -- optimization parameters --

c     note: in commons put reals first, then integers and logicals
c     avoids warnings on f77 compilers
c     written by: m. nemec

      integer ndpts,igrad,ioptm,iobjf,mxfun,mpopt,inord,ireord
      integer itgmr, imgmr,lfilg,mpit
      double precision fd_eta,opt_tol,optol,wfl,wfd,wtc,wgo,cd_tar
      double precision cl_tar,bfstep,pdcg,tolgmr,wmpo,fsmachs,cltars
      double precision reno,alphas,cdtars,wfls	

c     -- ndpts: number of design points for multi-point optimization --
      parameter (ndpts=5)
      logical wstart, dvalfa(ndpts), opt_restart
      dimension wmpo(ndpts), fsmachs(ndpts), cltars(ndpts), reno(ndpts)
      dimension alphas(ndpts), cdtars(ndpts), wfls(ndpts)
      common/optpar/ fd_eta, opt_tol, wfl, wfd, wtc, wgo, cd_tar,
     &     cl_tar, optol, bfstep, pdcg, tolgmr, wmpo, fsmachs, cltars,  
     &     reno, alphas, cdtars, wfls, ioptm, iobjf, igrad, inord,
     &     ireord, mxfun, itgmr, imgmr,lfilg, mpopt, mpit, wstart,
     &     dvalfa, opt_restart

c     -- b-spline common block --
c     jbsord: order of b-spline
c     nbseg:  number of b-spline segments on each airfoil
c     nbcp:   number of b-spline control points per segment
c     isurf:  b-spline location, upper,lower or blunt trailing edge surface
c     ibap:   number of grid points on the body (per element)
c     ibstart: body grid index of b-spline starting point
c     ibend:   body grid index of b-spline end point
c     bt: b-spline parameter vector
c     bcp: b-spline control points
c     bap: b-spline airfoil points
c     
      integer jbsord, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)

      double precision bstart,bend,bap,bapo,bt,bcp,bcpo
      
      dimension bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      dimension bap(ibody,2,maxfoil), bapo(ibody,2,maxfoil)
      dimension bt(ibody,maxfoil,maxbseg)
      dimension bcp(ibsnc,2,maxfoil,maxbseg)
      dimension bcpo(ibsnc,2,maxfoil,maxbseg)
      
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bapo, bt, bcp, bcpo, bknot,
     &     nbcp, isurf, ibap, ibstart, ibend, jbsord, nbseg

c     -- design variables common block --
c     idvi: design variable control point index
c     idvs: design variable airfoil surface, upper or lower surface
c     location or blunt trailing edge
c     idvf: design variable airfoil, index of element
c     dvs:  vector of design variable values

      integer idvi(ibsnc), idvs(ibsnc), idvf(ibsnc),
     &     icpn(maxfoil+1),ndv, nbdv, ntdv, ngdv 
      double precision dvs,grad,cp_tar,cpi,xcp_tar,tdelx,tdely
      double precision remxmin,remxmax,remymin,remymax,remdx,remdy

      dimension dvs(ibsnc), grad(ibsnc), cp_tar(ibody,maxfoil+1)
      dimension cpi(ibody,maxfoil), xcp_tar(ibody,maxfoil+1)
      common/dvinfo/ remxmin,remxmax,remymin,remymax,remdx,remdy,
     &     dvs, grad, cp_tar, cpi, xcp_tar, tdelx, tdely,
     &     idvi, idvs, idvf, icpn, ndv, nbdv, ntdv, ngdv

c     -- thickness constraints --
      integer nthc,jlotx,juptx,itcf,ntcon
      double precision ctx, cth, cthtar
      parameter (nthc=10)      
      dimension jlotx(nthc), juptx(nthc), itcf(nthc)
      dimension ctx(nthc), cth(nthc), cthtar(nthc)
      common/tccom/ ctx, cth, cthtar, jlotx, juptx, itcf, ntcon 

c     -- gap and overlap constraints --
      integer ile, ifgapc, ngapc,ifovc, novc
      double precision gapd, gapu, gapl, ovd, ovu, ovl

      dimension gapd(maxfoil), gapu(maxfoil), gapl(maxfoil)
      dimension ovd(maxfoil), ovu(maxfoil), ovl(maxfoil), ile(maxfoil)
      dimension ifgapc(maxfoil), ifovc(maxfoil)
      common/gocom/ gapd, gapu, gapl, ovd, ovu, ovl, ile, ifgapc, ngapc,
     &     ifovc, novc
