cc     -- write out node order in tecplot format --
c     -- m. nemec, july 2001 --

      subroutine writeord(n_all, x, y, jmax, kmax, jbegin, jend, kbegin,
     &     kend, indx)

      implicit none
      integer n_all,jmax, kmax, jbegin, jend, kbegin,kend,j,k
      double precision x(jmax,kmax), y(jmax,kmax)
      integer indx(jmax,kmax)

      write(n_all,100) ((x(j,k), j=jbegin,jend), k=kbegin,kend),
     &     ((y(j,k), j=jbegin,jend), k=kbegin,kend)
      write(n_all,110) ((indx(j,k), j=jbegin,jend), k=kbegin,kend)

 100  format (8e14.6)
 110  format (8i6)
c
      return 
      end                       ! writeord
