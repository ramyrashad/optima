c ********************************************************************
c ********************* xi integration routine   *********************
c ********************************************************************
c calling routine: integrat
c
      subroutine xiimpl(jdim,kdim,q,s,press,sndsp,turmu,
     &                  fmu,vort,x,y,xy,xyj,xit,ds,uu,ccx,
     &                  coef2x,coef4x,spect,precon)
c
c     main xi integration calls
c

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/scratch.inc"
c
      dimension q(jdim*kdim*4),spect(jdim*kdim*3),precon(jdim*kdim*8)
      dimension press(jdim*kdim),sndsp(jdim*kdim)
      dimension s(jdim*kdim*4),xy(jdim*kdim*4),xyj(jdim*kdim)
      dimension xit(jdim*kdim),ds(jdim*kdim)
      dimension x(jdim*kdim),y(jdim*kdim),turmu(jdim*kdim)
      dimension vort(jdim*kdim),fmu(jdim*kdim)
      dimension uu(jdim*kdim),ccx(jdim*kdim)
      dimension coef2x(jdim*kdim),coef4x(jdim*kdim)
c
c     <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     <<<                     implicit operators                  >>>
c     <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c
c     -integration   xi - eta order
c     -note:  stepf2dx  must come before stepf2dy !!!
c
      if (prec.gt.0) then
        do 55 ii=nblkstart,nblkend
c         -precondition right hand side with gamma-inverse
          call gaminv(jmax(ii),kmax(ii),q(lqptr(ii)),xyj(lgptr(ii)),
     &          sndsp(lgptr(ii)),precon(lprecptr(ii)),s(lqptr(ii)),
     &          jlow(ii),jup(ii),klow(ii),kup(ii))
 55     continue
      endif

      do 75 ii=nblkstart,nblkend
        ix = 1
        iy = 2
        call tkinv(ix,iy,jmax(ii),kmax(ii),q(lqptr(ii)),
     &        sndsp(lgptr(ii)),s(lqptr(ii)),xyj(lgptr(ii)),
     &        xy(lqptr(ii)),precon(lprecptr(ii)),
     &        jbegin(ii),jend(ii),kbegin(ii),kend(ii))
 75   continue
c     
c     -increase matrix diss. limiters on LHS to improve robustness.
c      if (idmodel.eq.2 .and. vlxi.lt.0.3) then
c        vltmp=vlxi
c        vlxi=0.3
c        do 105 ii=nblkstart,nblkend
c          lg=lgptr(ii)
c          lq=lqptr(ii)
c          call mspectx(jmax(ii),kmax(ii),uu(lg),ccx(lg),
c     &          xyj(lg),xy(lq),spect(lspptr(ii)),jbegin(ii),jend(ii),
c     &          kbegin(ii),kend(ii))
c 105    continue
c        vlxi=vltmp
c      endif
c
      do 120 ii=nblkstart,nblkend
        call stepf2dx(jmax(ii),kmax(ii),q(lqptr(ii)),
     &        turmu(lgptr(ii)),fmu(lgptr(ii)),
     &        s(lqptr(ii)),press(lgptr(ii)),
     &        sndsp(lgptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &        ds(lgptr(ii)),coef2x(lgptr(ii)),coef4x(lgptr(ii)),
     &        uu(lgptr(ii)),ccx(lgptr(ii)),
     &        work1,spect(lspptr(ii)),precon(lprecptr(ii)),
     &        jbegin(ii),jend(ii),kbegin(ii),kend(ii),
     &        jlow(ii),jup(ii),klow(ii),kup(ii),
     &        xi1mesh(ii),xi2mesh(ii))
 120  continue
c     write(6,*) 'after stepf2dx'
c     call flushit(6)
      
c     -close tk_xi here
c     -for multi-block must apply tk prior to interface condition
      ix = 1
      iy = 2
      do 150 ii=nblkstart,nblkend
        call tk(ix,iy,jmax(ii),kmax(ii),q(lqptr(ii)),
     &        sndsp(lgptr(ii)),s(lqptr(ii)),xyj(lgptr(ii)),
     &        xy(lqptr(ii)),precon(lprecptr(ii)),
     &        jbegin(ii),jend(ii),kbegin(ii),kend(ii))
 150  continue
c     
      return
      end
c
