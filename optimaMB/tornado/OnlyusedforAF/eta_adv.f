      subroutine eta_adv(jmax,kmax,jlow,jup,klow,kup,
     &                                  vv,turre,xy,workq,by,cy,dy,fy)
c     calling subroutine: spalart_allmaras
c
      dimension xy(jmax,kmax,4),fy(jmax,kmax),workq(jmax,kmax,3)
      dimension by(jmax,kmax),cy(jmax,kmax),dy(jmax,kmax)
      dimension turre(jmax,kmax),vv(jmax,kmax)
c
c     **********************
c     Advective Terms in Eta
c     **********************
c     
c     -first-order upwinding/downwinding        
      do k=klow,kup
      do j=jlow,jup
c        vv = (xy(j,k,3)*workq(j,k,2)+xy(j,k,4)*workq(j,k,3))
        sgnu = sign(1.,vv(j,k))
        app  = .5*(1.+sgnu)
        apm  = .5*(1.-sgnu)
        fy(j,k) = fy(j,k) - vv(j,k)*( app*(turre(j,k)  -turre(j,k-1))
     &                               +apm*(turre(j,k+1)-turre(j,k) ))
        by(j,k)   = by(j,k)   - vv(j,k)*app
        cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
        dy(j,k)   = dy(j,k)   + vv(j,k)*apm
      enddo
      enddo

      return
      end
