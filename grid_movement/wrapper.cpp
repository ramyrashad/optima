#include <iostream>
#include <iomanip>
#include <fstream>
#include "ByteSwap.h"

using namespace std;

extern "C" {
  void movegridfe_(char *filena, const int& namelen, const int &jtail1, 
             const int& jtail2, double* x, double* y, const double* by,
             double* dx, double* dy, const int& incr,
             const int& update_dxy);
}
extern "C" {
  void grid_adjoint_(const int& jmax, const int& kmax, const int& jtail1,
      const int& jtail2, const int& incr, double* x0, double* y0,
      double* dx, double* dy, double* ddG, double* ddA, double* lambda_prev);
}

int main()
{
  int jtail1(15), jtail2(94);  // FORTRAN indices
  int incr(2);
  int namelen(10);
  char filena[] = "grid-new.g";
  int update_dxy(1);

  // Set up initial grid
  int j, jmax, kmax;
  ifstream fin(filena, ios::in | ios::binary);
  if (! fin.is_open())
    cout << "Error opening file grid file in function move_grid"; 
  fin.read(reinterpret_cast < char * > (&j), sizeof(int));
  fin.read(reinterpret_cast < char * > (&jmax), sizeof(int));
  ByteSwap5(jmax);
  fin.read(reinterpret_cast < char * > (&kmax), sizeof(int));
  ByteSwap5(kmax);
  fin.read(reinterpret_cast < char * > (&j), sizeof(int));
  fin.read(reinterpret_cast < char * > (&j), sizeof(int));
  int n = jmax * kmax ;
  double *x0 = new double[n];
  double *y0 = new double[n];
  for (j=0; j<n; j++)  
  {
    fin.read(reinterpret_cast < char * > (&x0[j]), sizeof(double));
    ByteSwap5(x0[j]);
  }
  for (j=0; j<n; j++)
  {
    fin.read(reinterpret_cast < char * > (&y0[j]), sizeof(double));
    ByteSwap5(y0[j]);
  }
  fin.close();

  double *dx = new double[incr*n];
  double *dy = new double[incr*n];
  for (j = 0; j<incr*n; j++)
    dx[j] = dy[j] = 0.;
  double *x = new double[n];
  double *y = new double[n];
  for (j = 0; j<n; j++)
    x[j] = y[j] = 0.;
  double *by = new double[jtail2-jtail1+1];
  for (j = 0; j<jtail2-jtail1+1; j++)
    by[j] = y0[j+jtail1-1] + 0.001*sin(x0[j+jtail1-1]-1.);
  double *ddG = new double[2*n];
  for (j = 0; j<2*n; j++)
    ddG[j] = 1.;
  double *ddA = new double[2*(jtail2-jtail1+1)];
  for (j = 0; j<2*(jtail2-jtail1+1); j++)
    ddA[j] = 0.;
  double *lambda_prev = new double[2*incr*n];
  for (j = 0; j<2*incr*n; j++)
    lambda_prev[j] = 0.;

  // Perturb the grid
  movegridfe_(filena, namelen, jtail1, jtail2, x, y, by, dx, dy, incr, update_dxy);

  // Compute the gradient
  grid_adjoint_(jmax, kmax, jtail1, jtail2, incr, x0, y0, dx, dy, ddG,
      ddA, lambda_prev);

  // Clean up
  delete [] x0;
  delete [] y0;
  delete [] dx;
  delete [] dy;
  delete [] x;
  delete [] y;
  delete [] by;
  delete [] ddG;
  delete [] ddA;
  delete [] lambda_prev;

  return 0;
}
