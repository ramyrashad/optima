      subroutine coef24y2(jdim,kdim,coef2y,coef4y,pcoef,
     &                                              spect,coef2,coef4)           
c
      include '../include/arcom.inc'
c                                                                       
      dimension coef2y(jdim,kdim),coef4y(jdim,kdim)                     
      dimension pcoef(jdim,kdim),spect(jdim,kdim)
      dimension coef2(jdim,kdim),coef4(jdim,kdim)
c                                                                       
c     coef2y comes in as coefy (pressure gradiant coefficient)            
c     coef4y comes in as specy (spectral radius)                          
c                                                                       
      do 1 k = kbegin,kend                                              
      do 1 j = jbegin,jend                                              
         pcoef(j,k) = coef2y(j,k)                                       
         spect(j,k) = coef4y(j,k)                                       
1     continue                                                          
c                                                                       
      eps4y = dis4y
      eps2y = dis2y                                                    
c                                                                       
c     form 2nd and 4th order dissipation coefficients in y                 
c                                                                       
      do 40 k = kbegin,kend 
      do 40 j = jlow,jup                                             
         coef2(j,k)=eps2y*pcoef(j,k)*spect(j,k)
         coef4(j,k)=eps4y*spect(j,k)
 40   continue
c
      do 100 k=kbegin,kup
      kp1=k+1
      do 100 j=jlow,jup
         c2 = coef2(j,k)+coef2(j,kp1)
         c4 = coef4(j,k)+coef4(j,kp1)
         c4 = c4 - min(c4,c2)                                           
         coef4(j,k)=coef4(j,k)-min(coef2(j,k),coef4(j,k))
         coef2y(j,k) = c2                                               
         coef4y(j,k) = c4                                               
 100  continue
c
      k=kend
      do 200 j=jlow,jup
         coef4(j,k)=coef4(j,k)-min(coef2(j,k),coef4(j,k))
 200  continue
c
      return                                                         
      end                                                            
