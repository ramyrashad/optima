#ifndef TET_H
#define TET_H

#include "Tet.h"

class Tet  
{  
 private:
   int ID;
   short nvertices;
   int dim;   // dimension
   int *vertexID;

  public:

  //Default constructor
  Tet(); 

  void clearmem();

  //Constructor of 8 node brick element cell
  Tet (Node** corners, const int& cellID);
   
  ~Tet();       // Destructor

  // accessor function
 int get_ID() const { return ID;}
 int *get_vertexID() const {return vertexID;}
 short get_nvertices() const {return nvertices;} 

};

#endif















