c----------------------------------------------------------------------
c     -- unsteady optimization for airfoils--
c
c     written by: markus rumpfkeil
c     date: february 2006
c----------------------------------------------------------------------
      subroutine unsteadyoptimization(jdim, kdim, ndim,ifirst,indx,icol, 
     &      iex,ia,ja,as,ipa,jpa, pa, iat, jat, ast, ipt, jpt, pat, q,
     &      qp, qold, press, xy, xyj, x, y, fmu, vort, turmu, work1,
     &      D_hat,exprhs,a_jk,mlim,dvs,idv,bcp,bap,bt,bknot)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim, kdim, ndim,icol(9),na, ifirst,stage,ist,jki,ib
      integer indx(jdim,kdim),iex(jdim*kdim*ndim),stopstage,stopstage2

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision qp(jdim,kdim,ndim),qold(jdim,kdim,ndim)
      double precision press(jdim,kdim),sndsp(jdim,kdim),tmet(jdim,kdim)  
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)
      double precision xsave(jdim,kdim),ysave(jdim,kdim)
      double precision xstore(jdim,kdim,ndv,4),ystore(jdim,kdim,ndv,4)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision lambda(2*jdim*kdim*incr),psina,obj0

      double precision ns(sdim,2),J1(tdim),J2(tdim)

      logical dragdfdx,flagnoisedfdx,flag11dfdx,analyticdLdX,dcondx

c     -- optimization and b-spline arrays --
      integer idv(nc)
      double precision bap(jbody,2),bcp(nc,2),dvstmp(ndv+1)
      double precision bt(jbody), bknot(jbsord+nc),dvs(ndv+1)

c     -- sparse adjoint arrays --
      integer ia(jdim*kdim*ndim+2),iat(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)  

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100)
      double precision work(jdim*kdim,4)

csi
      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim)
      double precision a_jk(6,6),fobjpp,fobjmm
csi

c     -- BFGS stuff -- 
  
      character*60     task, csave,command,optrestart
      logical          lsave(4),imps,objdone 
      integer          mlim, iprint,i,j,k,count,count2,nout,
     &                 nbd(ndv), iwa(3*ndv), isave(44)
      double precision fobj,factr,fobjp,fobjm,
     &                 low(ndv+1),up(ndv+1),fgrad(ndv), dsave(29),pgtol 
     
      double precision,dimension(:),allocatable :: wa    

      double precision gradnorm,fgradtmp(ndv)
      double precision cdtp,cdtm,cdtpp,cdtmm,cdmean,gradnorm2,coef
      double precision cltp,cltm,cltpp,cltmm,clmean,timetmp,win(tdim)
      double precision psi(jdim,kdim,ndim),psiold(jdim,kdim,ndim),psitmp
      double precision,dimension(:,:,:,:),allocatable :: psies
      
      double precision dRdX,Rp(jdim,kdim,ndim),Rm(jdim,kdim,ndim),dRdX3 
      double precision Rpp(jdim,kdim,ndim), Rmm(jdim,kdim,ndim),dRdX2 

      double precision deltat,deltattmp,scalef,deltatbig
      complex*16,dimension(:,:,:,:),allocatable :: dpod

      integer strlen,namelen,skipend,jsta,jsto,lentmp

      double precision qtmp,stepsize,maxgrad,domegadX(2)
      double precision tpp,tp,tm,tmm

      double precision uu(jdim,kdim),vv(jdim,kdim),po(tdim,sdim)
      double precision ccx(jdim,kdim),ccy(jdim,kdim) 
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), spect(jdim,kdim,3)
      double precision ds(jdim,kdim), s(jdim,kdim,ndim)
      double precision bcf(jdim,kdim,4), gam(jdim,kdim,16)
    
      double precision dtiseq(20),dtmins(20),dtow2(20)
      integer jmxi(20),kmxi(20),jskipi(20),iends(20)
      integer isequal,iseqlev

      common/mesup/ dtiseq,dtmins,dtow2,isequal,iseqlev,
     &      jmxi,kmxi,jskipi,iends


cmpr  Allocate some of the big arrays dynamically
      allocate(psies(jdim,kdim,ndim,6))
      allocate(wa(2*mlim*ndv+4*ndv+12*mlim*mlim+12*mlim))
      allocate(dpod(Nobs,tdim,sdim,3))

c     -- save initial grid --

      do j = jbegin,jend
         do k = kbegin,kend
            xsave(j,k) = x(j,k)
            ysave(j,k) = y(j,k)
         end do
      end do

      objdone=.false.

c     Define name for restart file

      namelen =  strlen(restart_file_prefix)
      optrestart = restart_file_prefix
      optrestart(namelen+1:namelen+7) = '.orest'

      if (opt_restart) then

c        copy restart.ohis to actual file.ohis 

         command = 'cp '      
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(4:3+namelen+5) = filena
         lentmp = 3+namelen+5+1  
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(lentmp+1:lentmp+namelen+5) = filena

         call system(command)

      else

c        open restart file

         open(unit=33,file=optrestart,form='unformatted',
     &        status='unknown')

      end if

c     open .ohis file
      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix      
      filena(namelen+1:namelen+6) = '.ohis'
      open(unit=ohis_unit,file=filena,status='unknown',access='append')


      gamma=1.4d0
      gami=0.4d0    

c     regulation constant
      scalef=1.d0

      call set_ajk(a_jk)

c     Calculate the number of outputs

      deltat=noutevery*dtiseq(1)
      deltatbig=nouteverybig*dtbig

      skipend=nk_skip/nouteverybig
     
      nout=nk_iends/noutevery+skipend
      
c     We suppress the default output with -1.
      
      iprint = -1

c     The iteration will stop when
c
c     (f^k - f^{k+1})/max{|f^k|,|f^{k+1}|,1} <= factr*epsmch
c
c     where epsmch is the machine precision which is automatically
c     generated by the code. Typical values for factr on a computer
c     with 15 digits of accuracy in double precision are:
c     factr=1.d+12 for low accuracy;
c     1.d+7  for moderate accuracy; 
c     1.d+1  for extremely high accuracy.
c     The user can suppress this termination test by setting factr=0.
c     
c     The iteration will stop when
c     
c                 max{|proj g_i | i = 1, ..., n} <= pgtol
c
c     where pg_i is the ith component of the projected gradient.
c     The user can suppress this termination test by setting pgtol=0.
      

      factr=0.0d0
      pgtol=0.0d0
      
     
c     We now specify nbd which defines the bounds on the variables:

c     nbd(i)=0 if dvs(i) is unbounded,
c          1 if dvs(i) has only a lower bound,
c          2 if dvs(i) has both lower and upper bounds, 
c          3 if dvs(i) has only an upper bound. 
c     low(i)   specifies the lower bound on dvs(i)
c     up(i)    specifies the upper bounds on dvs(i)


      if (.not. periodic .and. .not. sbbc) then

c     bounds for inverse design
         do i=1,ndv
            nbd(i)=2 
            low(i)=dvs(i)-0.5d0*abs(dvs(i))
            up(i)=dvs(i)+0.5d0*abs(dvs(i))
         end do

c     bounds for buffeting with 10DV

c$$$      do i=1,4
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-0.30d0*abs(dvs(i))
c$$$         up(i)=dvs(i)+0.30d0*abs(dvs(i))
c$$$      end do
c$$$
c$$$      do i=5,6
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-0.10d0*abs(dvs(i))
c$$$         up(i)=dvs(i)+0.10d0*abs(dvs(i))
c$$$      end do
c$$$
c$$$      do i=7,8
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-0.10d0*abs(dvs(i))
c$$$         up(i)=dvs(i)+0.10d0*abs(dvs(i))
c$$$      end do
c$$$
c$$$      do i=9,10
c$$$         nbd(i)=2 
c$$$         low(i)=dvs(i)-0.20d0*abs(dvs(i))
c$$$         up(i)=dvs(i)+0.20d0*abs(dvs(i))
c$$$      end do


c$$$      do i=1,ndv/2
c$$$         dvs(i)=up(i)
c$$$      end do
c$$$
c$$$      do i=ndv/2+1,ndv
c$$$         dvs(i)=low(i)
c$$$      end do

cc     bounds for blakeReno 15DV with FE grid perturbation
c     original dvs
c$$$      dvs(1)=0.4999992853476780E-01
c$$$      dvs(2)=0.5000013041155907E-01
c$$$      dvs(3)=0.4999975070031144E-01
c$$$      dvs(4)=0.5000047982595041E-01
c$$$      dvs(5)=0.4999908669710421E-01
c$$$      dvs(6)=0.5000171863883106E-01      
c$$$      dvs(7)=0.4999680936819338E-01
c$$$      dvs(8)=0.5000571841366712E-01
c$$$      dvs(9)=0.4999101011314644E-01
c$$$      dvs(10)=0.5000423052349676E-01
c$$$      dvs(11)=0.5012082585860872E-01
c$$$      dvs(12)=0.4384847748737959E-01
c$$$      dvs(13)=0.2867436660191790E-01
c$$$      dvs(14)=-0.2147132634876335E-02
c$$$      dvs(15)=-0.3430892689862159E-01


c$$$      do i=1,ndv-3
c$$$         nbd(i)=2 
c$$$         up(i)=dvs(i)+2.5d-2
c$$$      end do
c$$$
c$$$      nbd(ndv-2)=2 
c$$$      up(ndv-2)=dvs(ndv-2)+5.d-2
c$$$      nbd(ndv-1)=2 
c$$$      up(ndv-1)=dvs(ndv-1)+8.d-2
c$$$      nbd(ndv)=2
c$$$      up(ndv)=dvs(ndv)+9.d-2
c$$$
c$$$      low(1)=0.033338198208539
c$$$      low(2)=0.027821174265668
c$$$      low(3)=0.022304472700659
c$$$      low(4)=0.016787231961608
c$$$      low(5)=0.011270981369461
c$$$      low(6)=0.005752874282495
c$$$      low(7)=0.000238244459404
c$$$      low(8)=-0.005282843198648
c$$$      low(9)=-0.010792139195415
c$$$      low(10)=-0.016322060120274
c$$$      low(11)=-0.021824085350990
c$$$      low(12)=-0.027288436220206
c$$$      low(13)=-0.034160877614004
c$$$      low(14)=-0.042036945816603
c$$$      low(15)=-0.047186056422758

c$$$c     start with the thickness constraint line
c$$$      dvs(1)=0.045243655095473
c$$$      dvs(2)=0.038938484875049
c$$$      dvs(3)=0.032633683086468
c$$$      dvs(4)=0.026328265098981
c$$$      dvs(5)=0.020023978707956
c$$$      dvs(6)=0.013717570608566
c$$$      dvs(7)=0.007415136525033
c$$$      dvs(8)=0.001105322058688
c$$$      dvs(9)=-0.005191016223332
c$$$      dvs(10)=-0.011510925851742
c$$$      dvs(11)=-0.017798954686846
c$$$      dvs(12)=-0.024043927108807
c$$$      dvs(13)=-0.031898145844576
c$$$      dvs(14)=-0.040899366647547
c$$$      dvs(15)=-0.046784064483152

c     start with something else
c$$$      do i=1,ndv
c$$$         dvs(i)=up(i)
c$$$      end do 




cc     bounds for blakeReno 15DV with FE grid perturbation and finer mesh 499x149
c$$$c original dvs
c$$$      dvs(1)=0.4999995472095203E-01
c$$$      dvs(2)=0.5000004532352278E-01
c$$$      dvs(3)= 0.4999993730397999E-01
c$$$      dvs(4)= 0.5000010255053826E-01
c$$$      dvs(5)=0.4999982065931115E-01
c$$$      dvs(6)=0.5000031737528125E-01
c$$$      dvs(7)=0.4999946084523246E-01
c$$$      dvs(8)= 0.5000072690576801E-01
c$$$      dvs(9)=0.5000054186362919E-01
c$$$      dvs(10)= 0.4998465941911875E-01
c$$$      dvs(11)=0.5016902339485621E-01
c$$$      dvs(12)=0.4391163104844670E-01
c$$$      dvs(13)= 0.2870843019157736E-01
c$$$      dvs(14)=-0.2250785127894096E-02
c$$$      dvs(15)=-0.3430338851453339E-01


c$$$      do i=1,ndv-3
c$$$         nbd(i)=2 
c$$$         up(i)=dvs(i)+2.5d-2
c$$$      end do
c$$$
c$$$      nbd(ndv-2)=2 
c$$$      up(ndv-2)=dvs(ndv-2)+4.d-2
c$$$      nbd(ndv-1)=2 
c$$$      up(ndv-1)=dvs(ndv-1)+6.d-2
c$$$      nbd(ndv)=2
c$$$      up(ndv)=dvs(ndv)+7.d-2
c$$$
c$$$      low(1) =   0.033446355152843
c$$$      low(2) =   0.027935433839173
c$$$      low(3) =   0.022425185897696
c$$$      low(4) =   0.016913765210642
c$$$      low(5) =   0.011404435988991
c$$$      low(6) =   0.005891344131043
c$$$      low(7) =   0.000385025113395
c$$$      low(8) =  -0.005133407572145
c$$$      low(9) =  -0.010630460210298
c$$$      low(10)=  -0.016163788653335
c$$$      low(11)=  -0.021646579520487
c$$$      low(12)=  -0.027119607822271
c$$$      low(13)=  -0.033969382406015
c$$$      low(14)=  -0.041826392893216
c$$$      low(15)=  -0.046971197690419

c$$$c     start with the thickness constraint line
c$$$      dvs(1) = 0.045367263031821
c$$$      dvs(2) = 0.039069067244769
c$$$      dvs(3) = 0.032771641025939
c$$$      dvs(4) = 0.026472874526448
c$$$      dvs(5) = 0.020176498273133
c$$$      dvs(6) = 0.013875821864049
c$$$      dvs(7) = 0.007582885843880
c$$$      dvs(8) = 0.001276105631834
c$$$      dvs(9) =-0.005006240240341
c$$$      dvs(10)=-0.011330044175240
c$$$      dvs(11)=-0.017596090880557
c$$$      dvs(12)=-0.023850980368310
c$$$      dvs(13)=-0.031679294178303
c$$$      dvs(14)=-0.040658734735104
c$$$      dvs(15)=-0.046538511646193


c     start with something else
c$$$      do i=1,ndv
c$$$         dvs(i)=up(i)
c$$$      end do 


cc     bounds for blakeReno 6DV with FE grid perturbation
c     original dvs 
c$$$      dvs(1)=0.5010988575914259E-01
c$$$      dvs(2)=0.4979481559731495E-01
c$$$      dvs(3)=0.5039214345232278E-01
c$$$      dvs(4)=0.4881190647759569E-01
c$$$      dvs(5)=0.5680323266342228E-01
c$$$      dvs(6)=-0.1726210459822014E-01  

c$$$      do i=1,ndv-2
c$$$         nbd(i)=2 
c$$$         up(i)=dvs(i)+2.5d-2
c$$$      end do
c$$$
c$$$      nbd(ndv-1)=2 
c$$$      up(ndv-1)=dvs(ndv-1)+2.5d-2
c$$$      nbd(ndv)=2
c$$$      up(ndv)=dvs(ndv)+9.d-2
c$$$
c$$$      low(1)=0.026155423367606
c$$$      low(2)=0.015474083067374
c$$$      low(3)=0.003598600333494
c$$$      low(4)=-0.010497835146480
c$$$      low(5)=-0.031758868184981
c$$$      low(6)=-0.044452943849733

c$$$c     start with the thickness constraint line
c$$$      dvs(1)=  0.037034769562978
c$$$      dvs(2)=  0.024827523505570
c$$$      dvs(3)=  0.011255543238278
c$$$      dvs(4)= -0.004854668738834
c$$$      dvs(5)= -0.029152992211407
c$$$      dvs(6)= -0.043660507256838

c     start with something else
c$$$      do i=1,ndv
c$$$         dvs(i)=up(i)
c$$$      end do 



cc    bounds for suction/blowing BC
c$$$      do i=1,ndv
c$$$         nbd(i)=2
c$$$      end do
c$$$
c$$$      up(1)=fsmach/3.d0
c$$$      low(1)=-up(1)
c$$$      up(2)=0.5d0      
c$$$      low(2)=0.05d0
c$$$      up(3)=180.d0     
c$$$      low(3)=0.d0


cc    bounds for rotating cylinder
c$$$      do i=1,ndv
c$$$         nbd(i)=2
c$$$      end do
c$$$
c$$$      low(1)=0.0d0
c$$$      low(2)=-1.0d-3
c$$$      up(1)=1.9d0
c$$$      up(2)=0.3d0

      else !periodic or suction/blowing BC

         do i=1,ndv
            nbd(i)=2
         end do

         up(1)=0.35d0
         low(1)=-0.35d0

         up(2)=0.45d0
         low(2)=1.d-3

         up(3)=90.d0        
         low(3)=-90.d0

c     We now define the starting point for rotating cylinder or suction/blowing BC             
         dvs(1)=omegaa
         dvs(2)=omegaf
         dvs(3)=sbeta

      end if

c
c     We start the iteration by initializing task or by reading the optimization restart file
c     
      if (.not. opt_restart) then

          task = 'START'

      else 

         open(unit=34,file=optrestart,form='unformatted',
     &        status='old')

         read(34) ndv,mlim
         read(34) (dvs(i),i = 1,ndv) 
         read(34) fobj,scalef,cdmean,clmean,jsta,jsto
         read(34) objdone
         read(34) (fgrad(i),i = 1,ndv)
         read(34) (wa(i),i = 1,2*mlim*ndv+4*ndv+
     &        12*mlim*mlim+12*mlim)
         read(34) (iwa(i),i = 1,3*ndv)
         read(34) task
         read(34) csave
         read(34) (lsave(i),i = 1,4)
         read(34) (isave(i),i = 1,44)
         read(34) (dsave(i),i = 1,29)
         if (obj_func.eq.10 .or. obj_func.eq.11 .or. 
     &        obj_func.eq.14) then
            read(34) (J1(i),i = 1,tdim)
            read(34) (J2(i),i = 1,tdim)
            read(34) (win(i),i = 1,tdim)
            read(34) ((ns(i,j),i=1,sdim),j=1,2)
            read(34) ((po(i,j),i=1,tdim),j=1,sdim)
            read(34) ((((dpod(ib,i,j,k),ib=1,Nobs),i=1,tdim),
     &           j=1,sdim),k=1,3)
         end if

         close(34)

c     open restart file

         open(unit=33,file=optrestart,form='unformatted',
     &        status='unknown')

         write(*,*)
         write(*,*) 'Restart optimization run at iteration', isave(30)+1    
 
         if (objdone) then
            write(*,*) 'Start with Gradient Calculation'
            write(*,*)
            goto 112
         else
            write(*,*) 'Start with Objective Function Calculation'
            write(*,*)
         end if 

      end if

c     ------- the beginning of the optimization loop ----------
 
 111  continue
      
c     This is the call to the L-BFGS-B code in bfgspro.f
      
      call setulb(ndv,mlim,dvs,low,up,nbd,fobj,fgrad,factr,pgtol,wa,
     +     iwa,task,iprint,csave,lsave,isave,dsave)

 112  continue  ! from optimization restart
 
      if (task(1:2) .eq. 'FG') then

c     the minimization routine has returned to request the
c     function fobj and gradient fgrad values at the current dvs

                          
c     Compute objective function value f

         write(*,*)
         write(*,*) 'Dvs:', (dvs(i),i = 1,ndv)
         write(*,*)

         if (.not.sbbc .and. .not. periodic) then
c        -- restore original grid and regrid--
         
            call regrid(-1,jdim,kdim,dvs,idv,x,y,bap,bcp,bt,bknot,
     &           xsave,ysave,dx,dy,.true.)
cmpr           stop

            if (dvalfa(1)) alpha=dvs(ndv)

         else  !suction/blowing BC or rotating cylinder

            omegaa=dvs(1)
            omegaf=dvs(2)
            sbeta=dvs(3)

         end if

         if (.not. objdone) then

c     -- unsteady flow solve
  
            ifirst = 1          ! indicates first call to flow solve if =1
            restart = .true.

c$$$         k=NK_IENDS
c$$$         j=NK_SKIP
c$$$         NK_IENDS=1
c$$$         NK_SKIP=0
            
            call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &        ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat,
     &        q, qp, qold,press, xy, xyj, x, y, fmu, vort, turmu, work1,
     &        D_hat,exprhs,a_jk)

c$$$         NK_IENDS=k
c$$$         NK_SKIP=j

c        -- detection of negative Jacobian for optimization runs --
            if (badjac) stop   
         
            if (obj_func .eq. 1) then
c           -- inverse design --
 
               call calcobjair(fobj,nout,skipend,jdim,kdim,deltat,y)           

            else if (obj_func .eq. 0) then
c           --  mean drag  minimization --
            
               call calcobjair0(fobj,jdim,kdim,nout,skipend,x,y,xy,xyj)

            else if (obj_func .eq. 5) then
c           --  (mean drag / mean lift)  minimization --
            
               call calcobjair5(fobj,jdim,kdim,nout,skipend,x,y,xy,xyj,
     &              cdmean,clmean,scalef)

            else if (obj_func .eq. 9) then
c           -- inverse remote design gridshape--
            
               call calcobjair8(fobj,nout,skipend,jdim,kdim,
     &              deltat,scalef,x,y,jsta,jsto)

            else if (obj_func .eq. 10 .or. obj_func .eq. 14) then
c           -- design using FWH--

               call calcobjairnoise(fobj,nout,skipend,jdim,kdim,deltat,
     &              scalef,x,y,jsta,jsto,xy,dpod,win,po,ns,.true.)
               
            else if (obj_func .eq. 11) then
c           -- minimize total radiated acoustic power --
            
               call calcobjair11(fobj,nout,skipend,jdim,kdim,
     &              deltat,scalef,x,y,xy,xyj,ns,J1,J2,.true.)

            else if (obj_func .eq. 12) then
c           -- inverse remote design boxshape--
            
               call calcobjair9(fobj,nout,skipend,jdim,kdim,
     &              deltat,scalef,x,y)

            end if              ! obj_func

            objdone=.true.

c           --save values for a potential optimization restart
  
            rewind(33)
            write(33) ndv,mlim
            write(33) (dvs(i),i = 1,ndv) 
            write(33) fobj,scalef,cdmean,clmean,jsta,jsto
            write(33) objdone
            write(33) (fgrad(i),i = 1,ndv)
            write(33) (wa(i),i = 1,2*mlim*ndv+4*ndv+
     &           12*mlim*mlim+12*mlim)
            write(33) (iwa(i),i = 1,3*ndv)
            write(33) task
            write(33) csave
            write(33) (lsave(i),i = 1,4)
            write(33) (isave(i),i = 1,44)
            write(33) (dsave(i),i = 1,29)
            if (obj_func.eq.10 .or. obj_func.eq.11 .or. 
     &        obj_func.eq.14) then
               write(33) (J1(i),i = 1,tdim)
               write(33) (J2(i),i = 1,tdim)
               write(33) (win(i),i = 1,tdim)
               write(33) ((ns(i,j),i=1,sdim),j=1,2)
               write(33) ((po(i,j),i=1,tdim),j=1,sdim)
               write(33) ((((dpod(ib,i,j,k),ib=1,Nobs),i=1,tdim),
     &              j=1,sdim),k=1,3)
            end if

         else
  
            ifirst = 1          ! indicates first call to flow solve if =1
            restart = .true.

            call initia ( ifirst,jdim,kdim,q,qold,press,work1(1,6),
     &      work1(1,7),xy,xyj,work1(1,12),work1(1,13),work1(1,14),x,y,
     &      turmu,fmu,vort,work1(1,15) )
            call asprat (jdim,kdim,x,y)

            restart=.false.

            iend = istart

         end if   !.not. objdone
     
         objdone=.false.

         write(*,*)
         write (*,*) 'Objective function value:',fobj,scalef 
         write(*,*)

cmpr     temporary
cmpr         stop

c     Compute gradient g for the sample problem.


       
         if (gradient .eq. 0) then
c     -- use finite differences to determine gradient

            print *,'Calculate gradient a la finite differences'

            do count=1,ndv

               do i=1,ndv+1
                  dvstmp(i)=dvs(i)
               enddo

               dvstmp(count)=dvstmp(count)+fd_eta

               if (.not.sbbc .and. .not. periodic) then
c              -- restore original grid and regrid--
         
                  call regrid(-1,jdim,kdim,dvstmp,idv,x,y,bap,bcp,bt,
     &                 bknot,xsave,ysave,dx,dy,.false.)

                  if (dvalfa(1)) alpha=dvstmp(ndv)

               else !suction/blowing BC or rotating cylinder

                  omegaa=dvstmp(1)
                  omegaf=dvstmp(2)
                  sbeta=dvstmp(3)

               end if
            
               ifirst = 1       ! indicates first call to flow solve if =1
               restart = .true.
           
               call flow_solve (jdim, kdim, ndim, ifirst,indx,icol,iex,
     &              ia, ja, as, ipa, jpa, pa, iat, jat,ast,ipt,jpt,pat,
     &              q,qp,qold,press,xy,xyj,x,y,fmu, vort, turmu, work1,
     &              D_hat,exprhs,a_jk)

c              -- detection of negative Jacobian for optimization runs --
               if (badjac) stop

               if (obj_func .eq. 1) then
c               -- inverse design --
 
                  call calcobjair(fobjp,nout,skipend,jdim,kdim,deltat,y) 

               else if (obj_func .eq. 0) then
c              --  mean drag  minimization --
            
                  call calcobjair0(fobjp,jdim,kdim,nout,skipend,x,y,
     &                 xy,xyj)

               else if (obj_func .eq. 5) then
c              --  (mean drag / mean lift)  minimization --
            
                  call calcobjair5(fobjp,jdim,kdim,nout,skipend,x,y,
     &                 xy,xyj,cdmean,clmean,scalef)

               else if (obj_func .eq. 9) then
c              -- inverse remote design gridshape--
            
                  call calcobjair8(fobjp,nout,skipend,jdim,
     &                 kdim,deltat,scalef,x,y,jsta,jsto) 

               else if (obj_func .eq. 10 .or. obj_func .eq. 14) then
c               -- design using FWH--
            
                  call calcobjairnoise(fobjp,nout,skipend,jdim,kdim,
     &            deltat,scalef,x,y,jsta,jsto,xy,dpod,win,po,ns,.false.)
                  
               else if (obj_func .eq. 11) then
c              -- minimize total radiated acoustic power --
            
                  call calcobjair11(fobjp,nout,skipend,jdim,kdim,
     &                 deltat,scalef,x,y,xy,xyj,ns,J1,J2,.false.)

               else if (obj_func .eq. 12) then
c              -- inverse remote design boxshape--
            
                  call calcobjair9(fobjp,nout,skipend,jdim,
     &                 kdim,deltat,scalef,x,y) 
            
               end if           ! obj_func

c$$$               fgrad(count)=(fobjp-fobj)/fd_eta
c$$$
c$$$               write (*,*) 'Gradient',count,'value:',fgrad(count)
c$$$
c$$$            end do

               do i=1,ndv+1
                  dvstmp(i)=dvs(i)
               enddo

               dvstmp(count)=dvstmp(count)-fd_eta

               if (.not.sbbc .and. .not. periodic) then
c              -- restore original grid and regrid--
                   
                  call regrid(-1,jdim,kdim,dvstmp,idv,x,y,bap,bcp,bt,
     &                 bknot,xsave,ysave,dx,dy,.false.)

                  if (dvalfa(1)) alpha=dvstmp(ndv)

               else !suction/blowing BC or rotating cylinder

                  omegaa=dvstmp(1)
                  omegaf=dvstmp(2)
                  sbeta=dvstmp(3)

               end if
                       
               ifirst = 1       ! indicates first call to flow solve if =1
               restart = .true.
           
               call flow_solve (jdim, kdim, ndim, ifirst,indx,icol,iex,
     &              ia, ja, as, ipa, jpa, pa, iat, jat,ast,ipt,jpt,pat,
     &              q,qp,qold,press,xy,xyj,x,y,fmu, vort, turmu, work1,
     &              D_hat,exprhs,a_jk)

c              -- detection of negative Jacobian for optimization runs --
               if (badjac) stop

               if (obj_func .eq. 1) then
c               -- inverse design --

                  call calcobjair(fobjm,nout,skipend,jdim,kdim,deltat,y) 
            
               else if (obj_func .eq. 0) then
c              --  mean drag  minimization --

                  call calcobjair0(fobjm,jdim,kdim,nout,skipend,x,y,
     &                 xy,xyj)

               else if (obj_func .eq. 5) then
c              --  (mean drag / mean lift)  minimization --
            
                  call calcobjair5(fobjm,jdim,kdim,nout,skipend,x,y,
     &                 xy,xyj,cdmean,clmean,scalef) 

               else if (obj_func .eq. 9) then
c               -- inverse remote design gridshape--
            
                  call calcobjair8(fobjm,nout,skipend,jdim,
     &                 kdim,deltat,scalef,x,y,jsta,jsto)

               else if (obj_func .eq. 10 .or. obj_func .eq. 14) then
c               -- design using FWH--
            
                  call calcobjairnoise(fobjm,nout,skipend,jdim,kdim,
     &            deltat,scalef,x,y,jsta,jsto,xy,dpod,win,po,ns,.false.)

               else if (obj_func .eq. 11) then
c              -- minimize total radiated acoustic power --
            
                  call calcobjair11(fobjm,nout,skipend,jdim,kdim,
     &                 deltat,scalef,x,y,xy,xyj,ns,J1,J2,.false.)

               else if (obj_func .eq. 12) then
c               -- inverse remote design boxshape--
            
                  call calcobjair9(fobjm,nout,skipend,jdim,
     &                 kdim,deltat,scalef,x,y)
           
               end if           ! obj_func


               fgrad(count)=(fobjp-fobjm)/(2.d0*fd_eta)

               write (*,*) 'Gradient',count,' value:',fgrad(count)

            end do














         else if (gradient .eq. 1) then
c         -- use adjoint method to determine gradient

            print *,'Calculate gradient a la adjoint method'

            do i=1,ndv
               fgrad(i)=0.d0
            end do

            na = jmax*kdim*ndim

            if (sbbc .or. periodic) then
               omegaa=dvs(1)
               omegaf=dvs(2)
               sbeta=dvs(3)
            end if




            if (jesdirk.eq.4 .or. jesdirk.eq.3) then

c     evolve psi for ESDIRK4:

c     -- initialize psies and psites to zero for all stages

      if(jesdirk.eq.4)then
         jstagemx=6
      elseif(jesdirk.eq.3)then
         jstagemx=4
      endif 

      do i=1,ndim
         do k = kbegin,kend
            do j = jbegin,jend
               do stage=1,jstagemx
               
                  psies(j,k,i,stage)=0.d0

               end do
            end do
         end do
      end do

      analyticdLdX=.false.



      do count = nout,0,-1

         if (count.ne.0) then
            stopstage=2
         else
            stopstage=jstagemx
         end if


      do jstage = jstagemx,stopstage,-1

         if (count.ne.0) then   !for count.eq.0 no adjoint problem has to be solved

            call unsteadyadjoint(jdim,kdim,ndim,indx,icol,psi,psies, 
     &      psiold,iex,ia,ja,as,ipa,jpa,pa,iat,jat,ast,ipt,jpt,pat,q,
     &      press,sndsp,xy,xyj,x,y,uu,vv,ccx,ccy,coef2,coef4,tmet,
     &      precon,bcf,fmu,vort,turmu,work1,a_jk,count,nout,skipend,
     &      po,dpod,win,ns,J1,J2,deltat,deltatbig,deltattmp,scalef,jsta,
     &      jsto,na,cdmean,clmean,.false.)
 
         end if    !count.ne.0 

           
         if (.not.sbbc .and. .not. periodic) then
c        -- calculate dRdX and assemble lagrange contribution to gradient---- 

c          -- set some flags concerning which objective function is used
           dragdfdx= (count .gt. skipend) .and. jstage.eq.jstagemx .and.
     &           ((obj_func .eq. 0) .or. (obj_func .eq. 5))

           flagnoisedfdx =( (obj_func .eq. 10) .or. (obj_func .eq. 14) )  
     &          .and. (count .eq. skipend+1) .and. jstage.eq.jstagemx

           flag11dfdx = (obj_func .eq. 11) .and. (count .eq. skipend+1) 
     &          .and. jstage.eq.jstagemx

           dcondx = obj_func.ne.10 .and. obj_func.ne.11  
     &          .and. obj_func.ne.14 .and. count.eq.skipend+1 
     &          .and. jstage.eq.jstagemx

           stepsize = fd_eta
c          -- check for tiny stepsize --
           if ( stepsize.lt.1.e-10 ) stepsize = 1.e-10

c          -- assemble multiplier for the partial derivative and store in psiold

           if (jstage.lt.jstagemx .or. count.eq.nout) then
              stage=jstage
           else
              stage=1
           endif

           do i=1,ndim
              do k = kbegin,kend
                 do j = jbegin,jend
                    psiold(j,k,i)=0.d0
                    do ist=jstagemx,stage+1,-1
                       coef = a_jk(ist,stage)/a_jk(ist,ist)
                       psiold(j,k,i)=psiold(j,k,i)+coef*psies(j,k,i,ist)                        
                    end do
                    psiold(j,k,i)=psiold(j,k,i)+psies(j,k,i,stage)
                 end do
              end do
           end do
           
           do i=1,ndv
              dvstmp(i)=dvs(i)
           enddo

c     -- loop over all design variables
          
           do count2=1,ndv

              dvstmp(count2)=dvs(count2)+2.0d0*stepsize
              if (dvalfa(1)) alpha=dvstmp(ndv)

              call calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,xsave,
     &             ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &             flag11dfdx,dcondx,tpp,fobjpp,Rpp,cdtpp,cltpp,nout,
     &             skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &             bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &             tmet,spect,gam,turmu,fmu,vort,sndsp,1,D_hat,work)

              dvstmp(count2)=dvs(count2)+stepsize
              if (dvalfa(1)) alpha=dvstmp(ndv)

              call calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,xsave,
     &             ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &             flag11dfdx,dcondx,tp,fobjp,Rp,cdtp,cltp,nout,
     &             skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &             bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &             tmet,spect,gam,turmu,fmu,vort,sndsp,2,D_hat,work)

              dvstmp(count2)=dvs(count2)-stepsize
              if (dvalfa(1)) alpha=dvstmp(ndv)

              call calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,xsave,
     &             ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &             flag11dfdx,dcondx,tm,fobjm,Rm,cdtm,cltm,nout,
     &             skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &             bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &             tmet,spect,gam,turmu,fmu,vort,sndsp,3,D_hat,work)

              dvstmp(count2)=dvs(count2)-2.0d0*stepsize
              if (dvalfa(1)) alpha=dvstmp(ndv)

              call calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,xsave,
     &             ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &             flag11dfdx,dcondx,tmm,fobjmm,Rmm,cdtmm,cltmm,nout,
     &             skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &             bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &             tmet,spect,gam,turmu,fmu,vort,sndsp,4,D_hat,work)

              dvstmp(count2)=dvs(count2)


c             -- assemble partial derivative and product of multiplier with residual sensitivity--
c             Note: get_rhs returns the negative of the residual
              do i=1,ndim  
                 do k = kbegin,kend
                    do j = jbegin,jend
                       
                       dRdX =( Rpp(j,k,i)-8.d0*Rp(j,k,i)+8.d0*Rm(j,k,i)-
     &                      Rmm(j,k,i) ) / (12.d0*stepsize)

                       fgrad(count2) =  fgrad(count2)+dRdX*psiold(j,k,i)
                       
                    end do
                 end do
              end do


c             -- calc gradient w.r.t. X for obj_func .eq. 0 and 5--
              if (dragdfdx) then
                 if (obj_func .eq. 0) then
                    fgrad(count2) =  fgrad(count2) +(-cdtpp+8.d0*cdtp-
     &                   8.d0*cdtm+cdtmm ) / (12.d0*stepsize) 
     &                   /real(nout-skipend) 
                 else               
                    fgrad(count2) =  fgrad(count2) 
     &            + (-cdtpp+8.d0*cdtp-8.d0*cdtm+cdtmm )/(12.d0*stepsize) 
     &            / (real(nout-skipend)*clmean*scalef)
     &            - (-cltpp+8.d0*cltp-8.d0*cltm+cltmm )/(12.d0*stepsize) 
     &            * cdmean / (real(nout-skipend) * clmean**2 * scalef) 
                 end if
              end if

c             -- calc gradient w.r.t. X of thickness constraint --               
              if (dcondx)  then
                 fgrad(count2) =  fgrad(count2) +(-tpp+8.d0*tp-
     &                8.d0*tm+tmm ) / (12.d0*stepsize) 
              end if


c             -- calc gradient w.r.t. X for obj_func .eq. 10, 11 or 14-- 
c             Note: sum over n automatically done in calcobjairXX

              if (flagnoisedfdx .or. flag11dfdx) then 
                 fgrad(count2) =  fgrad(count2) + (-fobjpp+8.d0*fobjp-
     &                8.d0*fobjm+fobjmm ) / (12.d0*stepsize) 
              end if 

       
           end do    ! count2 

         else     !suction/blowing BC
c        -- calculate dRdX analytically and sum over n=1,...,N: psi^(n)^T * dRdX^(n)

            print *,'Suction/blowing BC for ESDIRK not implemented yet'
            stop
 
         end if

c        -- Copy psi at stage 1 into psi at jstagemx for new (previous) time step at the end of jstage==jstagemx
c           and zero out the psi values at stages 1 to jstagemx-1
         if (jstage.eq.jstagemx.and.count.ne.nout.and.count.ne.0) then

            do i=1,ndim
               do k = kbegin,kend
                  do j = jbegin,jend
                     psies(j,k,i,jstagemx)=psies(j,k,i,1)
                  end do
               end do
            end do 

            do i=1,ndim
               do k = kbegin,kend
                  do j = jbegin,jend
                     do stage=1,jstagemx-1               
                        psies(j,k,i,stage)=0.d0                        
                     end do
                  end do
               end do
            end do

         end if !Copy psi at stage 1 into psi at jstagemx

       

      end do    !jstage
      end do    ! count 






               
            else if (jesdirk.eq.1) then     
            
c     evolve psi for BDF2:   
c     psi^(n)= inv([dRstar/dQhat]^T) * ( (4*psi^{n+1} - psi^{n+2})/(2*dt) - (dfobj/dQhat)^T )

c     -- set psi^(N+1)=0 and psi^(N+2)=0

      do i=1,ndim
         do k = kbegin,kend
            do j = jbegin,jend
               
               psi(j,k,i)=0.d0
               psiold(j,k,i)=0.d0
               
            end do
         end do
      end do
              
cmpr  there is a bug if analyticdLdX=.true. is used together 
cmpr  with opt_restart=.true. and it happens to ''Start with Gradient Calculation''
c      analyticdLdX=.true.
      analyticdLdX=.false.

      jstagemx=1
      jstage=1
      
      do count = nout, 1, -1


         call unsteadyadjoint(jdim,kdim,ndim,indx,icol,psi,psies, 
     &      psiold,iex,ia,ja,as,ipa,jpa,pa,iat,jat,ast,ipt,jpt,pat,q,
     &      press,sndsp,xy,xyj,x,y,uu,vv,ccx,ccy,coef2,coef4,tmet,
     &      precon,bcf,fmu,vort,turmu,work1,a_jk,count,nout,skipend,
     &      po,dpod,win,ns,J1,J2,deltat,deltatbig,deltattmp,scalef,jsta,
     &      jsto,na,cdmean,clmean,.false.)
           
c     -- Compute dRdX and calculate the objective function gradient --

         dragdfdx= (count.gt.skipend) .and. 
     &        ( (obj_func.eq.0) .or. (obj_func.eq.5) )


         if (.not.analyticdLdX .and. .not.sbbc .and. .not.periodic) then 
c     Finite difference dRdX and sum over n=1,...,N: psi^(n)^T * dRdX^(n) 

c     -- set some flags concerning which objective function is used
           flagnoisedfdx =( (obj_func .eq. 10) .or. (obj_func .eq. 14) )  
     &          .and. (count .eq. skipend+1) 

           flag11dfdx = (obj_func .eq. 11) .and. (count .eq. skipend+1)

           dcondx = obj_func .ne. 10 .and. obj_func .ne. 11 
     &          .and. obj_func .ne. 14 .and. count .eq. skipend+1

           stepsize = fd_eta
c          -- check for tiny stepsize --
           if ( stepsize.lt.1.e-10 ) stepsize = 1.e-10
           
           do i=1,ndv
              dvstmp(i)=dvs(i)
           enddo

c     -- loop over all design variables
          
           do count2=1,ndv

              dvstmp(count2)=dvs(count2)+2.0d0*stepsize
              if (dvalfa(1)) alpha=dvstmp(ndv)

              call calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,xsave,
     &             ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &             flag11dfdx,dcondx,tpp,fobjpp,Rpp,cdtpp,cltpp,nout,
     &             skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &             bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &             tmet,spect,gam,turmu,fmu,vort,sndsp,1,D_hat,work)

              dvstmp(count2)=dvs(count2)+stepsize
              if (dvalfa(1)) alpha=dvstmp(ndv)

              call calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,xsave,
     &             ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &             flag11dfdx,dcondx,tp,fobjp,Rp,cdtp,cltp,nout,
     &             skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &             bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &             tmet,spect,gam,turmu,fmu,vort,sndsp,2,D_hat,work)

              dvstmp(count2)=dvs(count2)-stepsize
              if (dvalfa(1)) alpha=dvstmp(ndv)

              call calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,xsave,
     &             ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &             flag11dfdx,dcondx,tm,fobjm,Rm,cdtm,cltm,nout,
     &             skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &             bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &             tmet,spect,gam,turmu,fmu,vort,sndsp,3,D_hat,work)

              dvstmp(count2)=dvs(count2)-2.0d0*stepsize
              if (dvalfa(1)) alpha=dvstmp(ndv)

              call calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,xsave,
     &             ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &             flag11dfdx,dcondx,tmm,fobjmm,Rmm,cdtmm,cltmm,nout,
     &             skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &             bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &             tmet,spect,gam,turmu,fmu,vort,sndsp,4,D_hat,work)

              dvstmp(count2)=dvs(count2)


c             -- assemble partial derivative --
c             Note: get_rhs returns the negative of the residual
              do i=1,ndim  
                 do k = kbegin,kend
                    do j = jbegin,jend
                       
                       dRdX =( Rpp(j,k,i)-8.d0*Rp(j,k,i)+8.d0*Rm(j,k,i)-
     &                      Rmm(j,k,i) ) / (12.d0*stepsize)

                       fgrad(count2) =  fgrad(count2)+dRdX*psi(j,k,i)
                       
                    end do
                 end do
              end do


c             -- calc gradient w.r.t. X for obj_func .eq. 0 and 5--
              if (obj_func.eq.0 .and. count.gt.skipend) then
                 fgrad(count2) =  fgrad(count2) +(-cdtpp+8.d0*cdtp-
     &                8.d0*cdtm+cdtmm ) / (12.d0*stepsize) 
     &                /real(nout-skipend) 
              end if

              if (obj_func.eq.5 .and. count.gt.skipend) then
                 fgrad(count2) =  fgrad(count2) 
     &           + (-cdtpp+8.d0*cdtp-8.d0*cdtm+cdtmm )/(12.d0*stepsize)
     &           / (real(nout-skipend) * clmean) / scalef
     &           - (-cltpp+8.d0*cltp-8.d0*cltm+cltmm )/(12.d0*stepsize) 
     &           * cdmean / (real(nout-skipend) * clmean**2) / scalef  
              end if

c             -- calc gradient w.r.t. X of thickness constraint --               
              if (dcondx)  then
                 fgrad(count2) =  fgrad(count2) +(-tpp+8.d0*tp-
     &                8.d0*tm+tmm ) / (12.d0*stepsize) 
              end if


c             -- calc gradient w.r.t. X for obj_func .eq. 10, 11 or 14-- 
c             Note: sum over n automatically done in calcobjairXX

              if (flagnoisedfdx .or. flag11dfdx) then 
                 fgrad(count2) =  fgrad(count2) + (-fobjpp+8.d0*fobjp-
     &                8.d0*fobjm+fobjmm ) / (12.d0*stepsize) 
              end if 

       
           end do    ! count2


         else if (analyticdLdX .and. .not.sbbc .and. .not.periodic) then
cCO   Analytically, Compute the dL/dX, the derivative of the Lagrangian,
cCO   L, with respect to the design variables, X.
cCO   (dL/dX = the objective function gradient, for those who use the
cCO   chain rule to derive adjoint equations)
cMPR  Chads code expects -psi, and physical q and pressure

           do i = 1,ndim 
              do k = kbegin,kend   
                 do j = jbegin,jend
                    psi(j,k,i) = -psi(j,k,i)            
                 end do
              end do
           end do
      
           call get_dLdX (jdim,kdim,ndim,q,press,sndsp,fmu,turmu,xy, 
     &          xyj,psi,psina,x,y,xsave,ysave,dx,dy,lambda,bt,bknot,idv,
     &          fgradtmp,obj0,real(nout-skipend),dragdfdx,(count.eq.1))
     &          

           do count2=1,ndv
              fgrad(count2)=fgrad(count2)+fgradtmp(count2)
           end do

           do i = 1,ndim 
              do k = kbegin,kend 
                 do j = jbegin,jend 
                    psi(j,k,i) = -psi(j,k,i)            
                 end do
              end do
           end do
           
         else      !suction/blowing BC or rotating cylinder
c     calculate dRdX and sum over n=1,...,N: psi^(n)^T * dRdX^(n)

           if (omegaf .eq. 0.d0) then
              domegadX(1)=1.d0
              domegadX(2)=0.d0
           else  
              if (count .le. skipend) then
                 timetmp=count*deltattmp
              else
                 timetmp=(count-skipend)*deltattmp+skipend*deltatbig
              end if
              domegadX(1)=dsin(2.d0*pi*omegaf*timetmp)
              domegadX(2)=2.d0*pi*timetmp*omegaa*
     &             dcos(2.d0*pi*omegaf*timetmp)
           endif
               
           do count2=1,ndv
              
              if (sbbc) then    !suction/blowing BC

                 if (count2 .le. 2) then !w.r.t. amplitude, frequency                                   
                    dRdX2=-rhoinf*domegadX(count2)*
     &                   dcos(sbeta*pi/180.d0-sdelta)
                    dRdX3=-rhoinf*domegadX(count2)*
     &                   dsin(sbeta*pi/180.d0-sdelta)
                 elseif  (count2 .eq. 3) then !w.r.t. sbeta
                    dRdX2=rhoinf*omegaa*domegadX(1)*
     &                   dsin(sbeta*pi/180.d0-sdelta)*pi/180.d0
                    dRdX3=-rhoinf*omegaa*domegadX(1)*
     &                      dcos(sbeta*pi/180.d0-sdelta)*pi/180.d0
                 end if
           
                 do j = jsls,jsle
                                     
                    fgrad(count2) = fgrad(count2) + dRdX2*psi(j,1,2)    
                    fgrad(count2) = fgrad(count2) + dRdX3*psi(j,1,3)
              
                 end do
                 
              else              !rotating cylinder

                 do j = jbegin,jend                                      
                    dRdX2=-0.5d0*domegadX(count2)*xy(j,1,4)/
     &                   SQRT(xy(j,1,3)*xy(j,1,3)+xy(j,1,4)*xy(j,1,4))
                    
                    fgrad(count2) = fgrad(count2) + dRdX2*psi(j,1,2)
                    
                    dRdX3=0.5d0*domegadX(count2)*xy(j,1,3)/
     &                   SQRT(xy(j,1,3)*xy(j,1,3)+xy(j,1,4)*xy(j,1,4))

                    fgrad(count2) =  fgrad(count2) + dRdX3*psi(j,1,3)               
                 end do

              end if            !suction/blowing BC or rotating cylinder
               
           end do               ! count2

         end if   ! analytic or finite difference dRdX for airfoils or 
!                   suction/blowing BC or rotating cylinder        

      end do    ! count 


      end if    !evolve psi for BDF2 or ESDIRK4

c     output the gradient values to the screen

      do i=1,ndv       
         write (*,*) 'Gradient',i,' value:',fgrad(i)
      end do

      end if                    ! Calculate gradient


c     -- output fobj and gradient to the .ohis file the first time
c     and copy the history file to the restart file

      if (isave(34) .eq. 0) then

         maxgrad=0.d0
         gradnorm2=0.d0
         do i=1,ndv
            gradnorm2=gradnorm2+fgrad(i)**2
            if (abs(fgrad(i)) .gt. maxgrad) maxgrad=abs(fgrad(i))
         end do
         gradnorm2=SQRT(gradnorm2)

         write (ohis_unit,705) isave(30),isave(34),fobj,maxgrad,
     &        gradnorm2,dsave(13),(dvs(i),i=1,ndv),(fgrad(i),i=1,ndv) 
         call flush(ohis_unit)

         command = 'cp '      
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(4:3+namelen+5) = filena
         lentmp = 3+namelen+5+1       
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(lentmp+1:lentmp+namelen+5) = filena
   
         call system(command)

      end if

c     save values for a potential optimization restart

      rewind(33)
      write(33) ndv,mlim
      write(33) (dvs(i),i = 1,ndv) 
      write(33) fobj,scalef,cdmean,clmean,jsta,jsto
      write(33) objdone
      write(33) (fgrad(i),i = 1,ndv)
      write(33) (wa(i),i = 1,2*mlim*ndv+4*ndv+
     &        12*mlim*mlim+12*mlim)
      write(33) (iwa(i),i = 1,3*ndv)
      write(33) task
      write(33) csave
      write(33) (lsave(i),i = 1,4)
      write(33) (isave(i),i = 1,44)
      write(33) (dsave(i),i = 1,29)
      if (obj_func.eq.10 .or. obj_func.eq.11 .or. 
     &        obj_func.eq.14) then
         write(33) (J1(i),i = 1,tdim)
         write(33) (J2(i),i = 1,tdim)
         write(33) (win(i),i = 1,tdim)
         write(33) ((ns(i,j),i=1,sdim),j=1,2)
         write(33) ((po(i,j),i=1,tdim),j=1,sdim)
         write(33) ((((dpod(ib,i,j,k),ib=1,Nobs),i=1,tdim),
     &        j=1,sdim),k=1,3)
      end if

c     go back to the minimization routine.
      goto 111

      endif                     ! task = FG   : Determine f and g



c
      if (task(1:5) .eq. 'NEW_X') then 
    
c     the minimization routine has returned with a new iterate.
c     At this point have the opportunity of stopping the iteration 
c     or observing the values of certain parameters
c

c     Note: task(1:4) must be assigned the viex(j,k,n)alue 'STOP' to terminate  
c     the iteration and ensure that the final results are
c     printed in the default format. The rest of the character
c     string TASK may be used to store other information.
         
c     1) Terminate if the total number of fobj and grad evaluations
c     exceeds MXFUN.

         if (isave(34) .ge. opt_iter)
     +     task='STOP: TOTAL NO. of f AND g EVALUATIONS EXCEEDS LIMIT'
 

c     2) Terminate if  max(|fgrad|) <= OPTOL

         maxgrad=0.d0
         gradnorm2=0.d0
         do i=1,ndv
            gradnorm2=gradnorm2+fgrad(i)**2
            if (abs(fgrad(i)) .gt. maxgrad) maxgrad=abs(fgrad(i))
         end do
         gradnorm2=SQRT(gradnorm2)

         if (maxgrad .le. opt_tol)
     +      task='STOP: THE GRADIENT IS SUFFICIENTLY SMALL'        

c     3) Terminate if (f^k - f^{k+1}) <= OPTOL

         if (abs(dsave(2)-fobj) .le. opt_tol*1.0d-6)
     +      task='STOP: THE CHANGE IN f IS SUFFICIENTLY SMALL'


c     We now wish to print the following information at each
c     iteration:
c     
c     1) the current iteration number, isave(30),
c     2) the total number of fobj and fgrad evaluations, isave(34),
c     3) the value of the objective function fobj,
c     4) the norm of the gradient and projected gradient,  maxgrad,dsave(13)
c     5) the value of the design variables
c     
c     See the comments at the end of driver1 for a description
c     of the variables isave and dsave.
         
         write (6,'(2(a,i5,4x),a,1p,d12.5,4x,a,1p,d12.5,4x,a,1p,d12.5,
     +        3x,a,1p,d12.5)') 'Iteration',isave(30),'nfg =',isave(34),
     +        'f =',fobj,'max |g| =',maxgrad,'|proj g| =',dsave(13)

         write (ohis_unit,705) isave(30),isave(34),fobj,maxgrad,
     &        gradnorm2,dsave(13),(dvs(i),i=1,ndv),(fgrad(i),i=1,ndv) 
         call flush(ohis_unit)
 705     format (2i4, 50e16.8)


c     save values for a potential optimization restart
c     also copy the history file to the restart file

         rewind(33)
         write(33) ndv,mlim
         write(33) (dvs(i),i = 1,ndv) 
         write(33) fobj,scalef,cdmean,clmean,jsta,jsto
         write(33) objdone
         write(33) (fgrad(i),i = 1,ndv)
         write(33) (wa(i),i = 1,2*mlim*ndv+4*ndv+
     &        12*mlim*mlim+12*mlim)
         write(33) (iwa(i),i = 1,3*ndv)
         write(33) task
         write(33) csave
         write(33) (lsave(i),i = 1,4)
         write(33) (isave(i),i = 1,44)
         write(33) (dsave(i),i = 1,29)
         if (obj_func.eq.10 .or. obj_func.eq.11 .or. 
     &        obj_func.eq.14) then
            write(33) (J1(i),i = 1,tdim)
            write(33) (J2(i),i = 1,tdim)
            write(33) (win(i),i = 1,tdim)
            write(33) ((ns(i,j),i=1,sdim),j=1,2)
            write(33) ((po(i,j),i=1,tdim),j=1,sdim)
            write(33) ((((dpod(ib,i,j,k),ib=1,Nobs),i=1,tdim),
     &           j=1,sdim),k=1,3)
         end if

         command = 'cp '      
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(4:3+namelen+5) = filena
         lentmp = 3+namelen+5+1       
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+6) = '.ohis'
         command(lentmp+1:lentmp+namelen+5) = filena
   
         call system(command)

c     If the run is to be terminated, we print also the information
c     contained in task as well as the final value of x.
         
         if (task(1:4) .eq. 'STOP') then
            write (6,*) task  
            write (6,*) 'Final X ='
            write (6,'((1x,1p, 2(1x,d11.4)))') (dvs(i),i = 1,ndv)
         endif

c     go back to the minimization routine.
         goto 111

      endif                     ! task = NEW_X

c     ---------- the end of the loop -------------
c     If task is neither FG nor NEW_X we terminate execution.

      if (task(1:4) .eq. 'WARN') then
         write (6,*) task
      end if

c     Write task anyway to check clean exit
      write (*,*) task

      close(ohis_unit)

cmpr  Deallocate the big arrays
      deallocate(psies,wa,dpod)

      return
      end                       !unsteadyoptimization


















      subroutine calcperturbrhs(count2,jdim,kdim,ndim,q,dvstmp,
     &     xsave,ysave,xstore,ystore,count,dragdfdx,flagnoisedfdx,
     &     flag11dfdx,dcondx,tfd,fobjfd,Rfd,cdtfd,cltfd,nout,
     &     skipend,deltat,scalef,jsta,jsto,dpod,win,idv,bcp,bap,
     &     bt,bknot,precon,coef2,coef4,uu,vv,ccx,ccy,ds,press,
     &     tmet,spect,gam,turmu,fmu,vort,sndsp,number,D_hat,work)


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"


      integer count2,jdim,kdim,ndim,count,nout,skipend,jsta,jsto,j,k,i
      
      double precision q(jdim,kdim,ndim),xy(jdim,kdim,4),fobjfd
      double precision press(jdim,kdim),sndsp(jdim,kdim),tmet(jdim,kdim)  
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)
      double precision xsave(jdim,kdim),ysave(jdim,kdim)
      double precision xstore(jdim,kdim,ndv,4),ystore(jdim,kdim,ndv,4)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)

      integer idv(nc),j_point,number
      double precision ns(sdim,2),J1(tdim),J2(tdim)
      double precision bap(jbody,2),bcp(nc,2),dvstmp(ndv+1)
      double precision bt(jbody),bknot(jbsord+nc),cdtfd,cltfd

      double precision work(jdim*kdim,4),D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim),a_jk(6,6),win(tdim)      
      double precision Rfd(jdim,kdim,ndim),deltat,scalef,xpt1,ypt1,tfd
      complex*16 dpod(Nobs,tdim,sdim,3)

      double precision uu(jdim,kdim),vv(jdim,kdim),po(tdim,sdim)
      double precision ccx(jdim,kdim),ccy(jdim,kdim) 
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), spect(jdim,kdim,3)
      double precision ds(jdim,kdim),gam(jdim,kdim,16)

      logical dragdfdx,flagnoisedfdx,flag11dfdx,dcondx  

c     -- if first time regrid and store the grid so that we are able to read it in at consecutive time steps
      if (count .eq. nout .and. jstage .eq. jstagemx) then

c        -- restore original grid and regrid--
         
         call regrid(-1,jdim,kdim,dvstmp,idv,x,y,bap,bcp,bt,
     &        bknot,xsave,ysave,dx,dy,.false.)

         do k = kbegin,kend 
            do j = jbegin,jend                    
               xstore(j,k,count2,number) = x(j,k)
               ystore(j,k,count2,number) = y(j,k)
            end do
         end do
               
      else
         
c        -- read in perturbed grid--
         do k = kbegin,kend 
            do j = jbegin,jend                    
               x(j,k) = xstore(j,k,count2,number)
               y(j,k) = ystore(j,k,count2,number)
            end do
         end do
         
      end if

c     -- recalculate metrics --
      call xidif ( jdim, kdim, x, y, xy)
      call etadif ( jdim, kdim, x, y, xy)
      call calcmet ( jdim, kdim, xy, xyj)

c     -- thickness constraint --
      tfd = 0.d0
      if (dcondx) call CalcTCon (jdim, kdim, y, tfd, .false.)

c     -- compute dfobjdX for obj_func .eq. 10 or 14 --
      if (flagnoisedfdx) call calcobjairnoise(fobjfd,nout,skipend,jdim,
     &     kdim,deltat,scalef,x,y,jsta,jsto,xy,dpod,win,po,ns,.false.)
      
c     -- compute dfobjdX for obj_func .eq. 11 --
      if (flag11dfdx) call calcobjair11(fobjfd,nout,skipend,jdim,
     &     kdim,deltat,scalef,x,y,xy,xyj,ns,J1,J2,.false.)
            
c     -- put in jacobian Q->Qhat--
      do i = 1,ndim
         do k = kbegin,kend 
            do j = jbegin,jdim          
               q(j,k,i) = q(j,k,i)/xyj(j,k)
            end do
         end do
      end do 

c     -- calculate pressure and sound speed --
c     note laminar viscosity is independent of the grid perturbation and 
c     doesn't need to be recalculated but it happens in get_rhs anyway
      call calcps (jdim,kdim, q, press,sndsp,precon,xy,xyj)

c     -- compute drag for dfobjdX --
      if (dragdfdx) then 
         call clcd(jdim,kdim,q,press,x,y,xy,xyj,0,
     &        cli,cdi,cmi,clv,cdv,cmv)
         cdtfd = cdi + cdv
         cltfd = cli + clv
      end if

c     -- compute generalized distance function --
      if (viscous .and. turbulnt .and. itmodel.eq.2) then
         do k = kbegin,kend
            do j = jbegin,jend
               j_point=j
               if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
               xpt1 = x(j_point,kbegin)
               ypt1 = y(j_point,kbegin)
               smin(j,k) =dsqrt((x(j,k)-xpt1)**2+
     &              (y(j,k)-ypt1)**2)
            end do
         end do
      end if

      UNSTED=.false.            !done for speed, doesn't affect the result
      dt=1.d0                   !Precaution for AF leftovers

      call get_rhs (jdim,kdim,ndim,q,q,q,xy,xyj,x,y,
     &     precon,coef2, coef4, uu, vv, ccx, ccy,ds,press,tmet,spect,
     &     gam,turmu,fmu,vort,sndsp,Rfd,work,D_hat,exprhs,a_jk,.false.)

      UNSTED=.true.

c     -- remove jacobian scaling Qhat->Q--
      do i = 1,ndim
         do k = kbegin,kend
            do j = jbegin,jend                 
               q(j,k,i) = q(j,k,i)*xyj(j,k)             
            end do
         end do
      end do

      
      return
      end  !calcperturbrhs




