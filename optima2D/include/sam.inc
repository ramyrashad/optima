c     -- constants associated with the S-A turbulence model --
c     written by: marian nemec, jan. 2001

      double precision expon, reinv, akarman, cb1, sigmainv
      double precision resiginv, cb2, cw1, cw2, cw3, cw3_6, const
      double precision cv1, cv1_3, cv2, ct1, ct2, ct3, ct4  

      parameter (expon = 1.d0/6.d0, akarman = 0.41d0)
      parameter (cb1= 0.1355, sigmainv= 1.5d0)
      parameter (cb2 = 0.622, cw1 = cb1/(akarman**2)+(1.0+cb2)*sigmainv)
c     -- underscore is equivalent to '**' i.e. exponentiation --
      parameter (cw2 = 0.3, cw3 = 2.0, cw3_6 = 64.0)
      parameter (cv1 = 7.1, cv1_3 = 357.911,cv2=5.0d0)
c     -- setting ct1 to 5 instead of 1 helps convergence --
!      parameter (ct1 = 1.0, ct2 = 2.0, ct3 = 1.2, ct4 = 0.5)	
      parameter (ct1 = 5.0, ct2 = 2.0, ct3 = 1.2, ct4 = 0.5)	
!      parameter (ct1 = 5.0, ct2 = 2.0, ct3 = 1.1, ct4 = 2.0) ! J.Driver values
      parameter (const =(1.d0+cw3_6)**expon)
