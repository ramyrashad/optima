      subroutine gaminv(jdim,kdim,q,xyj,sndsp,precon,s,
     &                  jlow,jup,klow,kup)
cdu
cdu  this routine multiplies the vector inverse transfer matrix
cdu  and inverse preconditioner
cdu
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),xyj(jdim,kdim),sndsp(jdim,kdim)
      dimension s(jdim,kdim,4),precon(jdim,kdim,8)
c
c     note gami=gamma - 1
c
c
      do 1000 k=klow,kup
         do 1000 j=jlow,jup
c
            rho=q(j,k,1)*xyj(j,k)
            u=q(j,k,2)/q(j,k,1)
            v=q(j,k,3)/q(j,k,1)
cdu         c=sqrt(precon(j,k,3))
            c=sndsp(j,k)
c
c     multiply by M inverse
c
            t4=gami*(precon(j,k,2)*s(j,k,1)-u*s(j,k,2)-
     +           v*s(j,k,3)+s(j,k,4))
            t1=t4/rho/c
            t4=t4-precon(j,k,3)*s(j,k,1)
            t2=(s(j,k,2)-u*s(j,k,1))/rho
            t3=(s(j,k,3)-v*s(j,k,1))/rho
c
c     multiply by Gamma Inverse
c
            s(j,k,1)=t1*precon(j,k,1)
            s(j,k,2)=t2
            s(j,k,3)=t3
            s(j,k,4)=t4
c
 1000 continue
c
      return
      end
