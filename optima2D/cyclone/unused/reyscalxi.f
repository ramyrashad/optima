      subroutine reyscalxi(JDIM,KDIM,Q,PRESS,VORT,TURMU,FMU,
     &                   X,Y,XY,XYJ)    
c     parameter (maxj=497, maxk=97)                                     
      include '../include/arcom.inc'
      include 'matrix.inc'
      include 'reyno.inc'
c                                                                       
c
c
c may.95  budget() added by kyle frew
c ***************************************************************
      include 'budget.inc'
                                                                       
      dimension q(jdim,kdim,4),turmu(jdim,kdim),vort(jdim,kdim)         
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)         
      dimension fmu(jdim,kdim)                                          
      dimension x(jdim,kdim),y(jdim,kdim)                               
c
c
c
      double precision a1,a2,a3,a4,x1,x2,temp
c
c calculate coefficients of cubic based 
c on reynolds limits (x1,x2)
c
      x1=rl
      x2=ru
      temp=x1**3-3.*x1**2*x2+3.*x1*x2**2-x2**3
      a1=((x1-3.*x2)*x1**2)/temp
      a2=(6.*x1*x2)/temp
      a3=-3.*(x1+x2)/temp
      a4=2./(temp)
c
c apply reynolds limiter
c
      do k=kbegin,kend
         do j=jbegin,jend
c            r0=1./xyj(j,k)
            r0=1.
c     regular refunc
            rl=sqrt(xy(j,k,3)**2+xy(j,k,4)**2)
            refunc(j,k)=sqrt(q(j,k,2)**2+q(j,k,3)**2)
     &                  *rl*r0
     &                  /(fmu(j,k)+turmu(j,k))*re
c tanh function
c            refunc(j,k)=0.5*(tanh(rl*(refunc(j,k)-ru))+1.0)
            if (refunc(j,k).ge.x2) then
               refunc(j,k)=1.0
            elseif (refunc(j,k).le.x1) then
               refunc(j,k)=0.0
            else
               r0=refunc(j,k)
               refunc(j,k)=a1+a2*r0+a3*(r0**2)+a4*(r0**3)
            endif
            budget2(j,k,3)=refunc(j,k)
      end do
      end do

            return
            end



c













