

c----------------------------------------------------------------------
c     -- Compute Thickness Constraint Value --
c
c     Original Written by Po-yang Jay Liu for SNOPT
c     date: June 2002
c     Modified by Timothy Leung for BFGS (August 2004)
c----------------------------------------------------------------------

      subroutine CalcTCon (jdim, kdim, y, t3, printout)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     -- specify the variable type --
      integer i, jdim, kdim
      double precision y(jdim,kdim), t3
      logical printout


      t3 = 0.d0
      do i=1,ntcon
         cty(i) = y(juptx(i),1) - y(jlotx(i),1)
         
         if (cty(i) .lt. cty_tar(i) ) then
            t3 = t3 + wtc*( 1.d0 - cty(i)/cty_tar(i) )**2
            if (printout) then
               print *, 'Constraint violation', i,cty(i),cty_tar(i)
            end if

         end if
      end do

      return
      end                       !CalcTCon





c----------------------------------------------------------------------
c     -- Compute Area Constraint Value --
c
c     Original Written by Samy Elias
c     Modified by Timothy Leung (August 2004)
c----------------------------------------------------------------------
      subroutine CalcACon(jdim, kdim, x, y, a3)
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     -- specify the variable type and dimensions--
      integer j,jdim, kdim
      double precision x(jdim,kdim), y(jdim,kdim), a3

      area = 0.0
      do j=jtail2,jtail1+1,-1
         area = area + ( x(j,1)-x(j-1,1) )*( (y(j,1)+y(j-1,1))/2 )
      end do

      a3 = 0.0
      if (area .lt. areafac*areainit) then
         a3 = wac*( 1.d0 - area/(areafac*areainit))**2
      endif

      return
      end                       !CalcACon





c----------------------------------------------------------------------
c     -- Compute Range (floating) Thickness Constraint Penalty Value --
c     
c     Original Written by Samy Elias
c     Modified by Timothy Leung (August 2004)
c----------------------------------------------------------------------
      subroutine CalcRTCon(jdim,kdim,y,rt3)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim,kdim
      double precision y(jdim,kdim), rt3

      integer i

      maxrthc = 0.0
      maxrthctemp = 0.0
      maxrthctot = 0.0
      do i=1,nrtcon
         do subi = 1, crtxn(i)+1
            maxrthctemp = y(juprtxsub(i,subi),1) - y(jlortxsub(i,subi
     &           ),1)
            if (maxrthc .lt. maxrthctemp) then
               crth(i) = maxrthctemp
               crthx(i) = crtxsub(i,subi)
               maxrthc = maxrthctemp
            end if
         end do
         if (crth(i) .lt. crthtar(i) ) then
            maxrthctot = maxrthctot + wtc*( 1.0 - crth(i)/crthtar(i)
     &           )**2
         end if
      end do

      rt3 = maxrthctot

      return
      end                       !CalcRTCon


c---- CalcRCCon --------------------------------------------------------
c Computes radius of curvature constraint about the airfoil
c
c Chad Oldfield, June 2006
c----------------------------------------------------------------------

      subroutine CalcRCCon (jdim, kdim, x, y, rc_con)
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     Parameters; see above for descriptions
      integer jdim, kdim
      double precision x(jdim,kdim), y(jdim,kdim), rc_con
      
c     Variables that should be in include file
c     Local variables
      integer i, j
      double precision xa, ya, xb, yb, xc, yc, r, rmin
c         xa, ya, xb, yb, xc, yc: 3 adjacent points on the airfoil
c           used to define a circle.
c         r: the radius of the circle.

      rc_con = 0.d0

      if (wrc_con == 0.d0) then
        return
      endif

      rewind rccon_unit

      xb = x(jtail1  ,1)
      yb = y(jtail1  ,1)
      xc = x(jtail1+1,1)
      yc = y(jtail1+1,1)
      do j = jtail1+1, jtail2-1
c       Compute the radius of curvature
        xa = xb
        ya = yb
        xb = xc
        yb = yc
        xc = x(j+1,1)
        yc = y(j+1,1)
        r = sqrt(0.25d0
     |           *((xc - xb)**2 + (yc - yb)**2)
     |           *((xc - xa)**2 + (yc - ya)**2)
     |           *((xa - xb)**2 + (ya - yb)**2)
     |           /(xa*yb - xa*yc - xb*ya + xb*yc + xc*ya - xc*yb)**2)

c       Interpolate to find the minimum radius of curvature.  xrc_tar is
c       assumed to be sorted in order of increasing values.
        i = 1
        do while ((xrc_tar(i+1) < xb) .and. (i+1 < nrc))
          i = i+1
        enddo
        rmin = (xb - xrc_tar(i))*(rc_tar(i+1)-rc_tar(i))
     |      /(xrc_tar(i+1)-xrc_tar(i)) + rc_tar(i)

c       Penalize if the radius of curvature is less than the minimum.
        if (r < rmin) then
          rc_con = rc_con + (r/rmin - 1.d0)**2
        endif
        write (rccon_unit,'(4f24.16)') x(j,1), y(j,1), rmin, r
      enddo
      rc_con = rc_con * wrc_con
      call flush(rccon_unit)
      
      end
c---- CalcRCCon --------------------------------------------------------
