dnl Process this file with autoconf to produce a configure script.
AC_INIT
AC_CONFIG_SRCDIR([amplProblem.cc])

dnl this will always be configured as part of a larger package
AC_CONFIG_AUX_DIR(config)

AC_ARG_WITH(ampl, [--with-ampl: support for the Ampl modeling language],,
	      with_ampl=yes )

AC_MSG_CHECKING(for GNU make)
unset gnu_make
if test -n "$MAKE"; then
	if  $MAKE --version 2> /dev/null | grep GNU > /dev/null; then
		gnu_make="$MAKE"
	fi
elif  make --version 2> /dev/null | grep GNU > /dev/null; then
	gnu_make=make
elif gmake --version 2> /dev/null | grep GNU > /dev/null; then
	gnu_make=gmake
fi

if test -n "$gnu_make"; then
	AC_MSG_RESULT($gnu_make)
	GNU_MAKE=$gnu_make
else
	AC_MSG_RESULT(no)
fi

AC_SUBST(GNU_MAKE)

dnl if CC is not set, force us to check cc first (before gcc)
if test -z "$CC"; then
	AC_CHECK_PROG(CC, cc, cc)
fi
CFLAGS=-O
AC_PROG_CC

dnl Use native CC first
if test -z "$CXX"; then
   AC_CHECK_PROG(CXX,CC,CC)
fi
CXXFLAGS=-O
AC_PROG_CXX


case "$host_os" in

linux-gnu* )
	dnl if F77 is not set, g77 is taken as the default
;;
*)
	dnl if F77 is not set, force us to check f77 first (before g77)
	if test -z "$F77"; then
	   AC_CHECK_PROG(F77, f77, f77)
	fi
;;
esac

FFLAGS=-O
AC_PROG_F77
AC_F77_LIBRARY_LDFLAGS
AC_LANG_FORTRAN77

if test "$with_ampl" = yes; then
   if test -z "$AMPL_SOLVERS"; then
	with_ampl=no
	AC_MSG_WARN( [The AMPL_SOLVERS environment variable is not set.])
	AC_MSG_WARN( [I do not know where to find the AMPL libraries.] )
	AMPL_SOLVERS="\${HOME}/Ampl/solvers"
   else
	dnl AMPL_SOLVERS is set. Do AMPL_SOLVERS and AMPL_OS combine to give a valid location
	dnl for the AMPL libraries

	ampl__ok=1
	AMPLLIBS="${AMPL_SOLVERS}/amplsolver.a"

	AC_MSG_CHECKING(whether the AMPL library (amplsolver.a) exists)

	if test ! -f $AMPLLIBS; then
		AC_MSG_RESULT(no)
		with_ampl=no
		warning=`echo The AMPL library, $AMPLIBS, does not exist. \
			Check the values of the AMPL_SOLVERS  \
			environment variable.`
		AC_MSG_WARN($warning)
	else
	   	dnl Librariy exists
		AC_MSG_RESULT(found)
	fi

   fi # end AMPL_SOLVERS is set
fi

AC_SUBST(AMPL_SOLVERS)

if test "$with_ampl" = yes; then
   all_ampl_if_enabled="all_ampl"
   AMPL_ENABLED=ampl_enabled
   rm -f DISABLED
else
   all_ampl_if_enabled='# Ampl support has been disabled'
   AMPL_ENABLED='ampl_disabled # AMPL support has been disabled'
   echo AMPL support has been disabled. > DISABLED
fi

AC_SUBST(AMPL_ENABLED)
AC_SUBST(all_ampl_if_enabled)

AC_CONFIG_FILES([GNUmakefile])
AC_OUTPUT
