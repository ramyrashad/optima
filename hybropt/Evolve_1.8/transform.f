C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C    TTTTT  RRRR   AAAAA  N   N   SSS   FFFFF   OOO   RRRR   M    M    C
C      T    R   R  A   A  NN  N  S      F      O   O  R   R  MM  MM    C
C      T    RRRR   AAAAA  N N N   SSS   FFFF   O   O  RRRR   M MM M    C
C      T    R   R  A   A  N  NN      S  F      O   O  R   R  M    M    C
C      T    R   R  A   A  N   N   SSS   F       OOO   R   R  M    M    C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE TRANSFORM(NNN,NGENE,NCHROM,POP,W,U,R,ITRANS,
     $     genemin,genemax,scale_genes,debug)
c$$$      SAVE
      logical debug,scale_genes
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** THIS SUBROUTINE TRANSFORMS A SET OF NC VECTORS          C
C              EACH WITH NG ELEMENTS USING SIMPLIFIED GRAM-            C
C              SCHMIDT ORTHONORMALIZATION. THE FIRST COORDINATE        C
C              OF THE NEW SYSTEM IS CONSTRUCTED SO AS TO ALIGN         C
C              WITH POP(,2)-POP(,1) ***                                    C
C                                                                      C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      DIMENSION         POP(ngene,nchrom)
      DIMENSION         R(ngene,ngene)
      DIMENSION         U(ngene,ngene)
      DIMENSION         V(ngene)
      DIMENSION         W(ngene,ngene)
      DIMENSION         DUM(ngene)
      DIMENSION         GENEMIN(ngene),GENEMAX(ngene)
C
C-----------------------------------------------------------------------
      if(debug)write(49,*)' In transform'
C-----------------------------------------------------------------------
C          *** FORMAL PARAMETER DEFINITIONS ***
C
C   ITRANS...INTEGER VARIABLE THAT CONTROLS WHETHER THE STANDARD
C           TRANSFORMATION IS PERFORMED OR ITS INVERSE
C           ITRANS= 2...PERFORM INITIALIZATION, COMPUTE 
C                      TRANSFORMATION COEFFICIENTS AND TRANSFORM
C                      NCHROM VECTORS
C           ITRANS= 1...TRANSFORM NCHROM VECTORS USING STORED
C                      TRANSFORMATION MATRIX
C           ITRANS=-1...INVERSE TRANSFORM NCHROM VECTORS USING STORED
C                      TRANSFORMATION MATRIX
C           ITRANS=-2...COMPUTE INVERSE TRANSFORMATION MATRIX AND
C                      INVERSE TRANSFORM NCHROM VECTORS. THIS 
C                      ASSUMES THAT ORIGINAL TRANSFORMATION
C                      MATRIX IS ALREADY STORED
C   NC......INTEGER LIMIT FOR THE NUMBER OF VECTORS BEING 
C           TRANSFORMED
C   NG......INTEGER LIMIT FOR THE NUMBER OF ELEMENTS IN EACH
C           VECTOR
C   POP.....TWO-DIMENSIONAL REAL ARRAY CONTAINING THE VECTOR 
C           ELEMENTS--INPUTS AS WELL AS OUTPUTS. FIRST SUBSCRIPT 
C           IS THE ELEMENT COUNTER AND THE SECOND SUBSCRIPT IS THE
C           VECTOR COUNTER
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** INITIALIZE PARAMETERS BUT ONLY IF ITRANS=2 ***
C
      if(debug)then
         if(itrans.ge.1)write(49,*)'before transformation '
         if(itrans.le.-1)write(49,*)'before inverse transformation'
         write(49,*)' nc          pop = '
         do nc = 1,nchrom
            write(49,'(i5,3(2x,e24.16))')
     $           nc,(POP(ng,nc),ng=1,ngene)
         enddo
      endif

c    normalizing  genes 0 -> 1.0
      if(scale_genes .and. itrans.ge.1)then
         do ng = 1,ngene
            scal = 1.0/(genemax(ng)-genemin(ng))
            do nc = 1,nchrom
               POP(ng,nc) = (pop(ng,nc)-genemin(ng))*scal
            enddo
         enddo
         if(debug)then
            write(49,*)'after scaling '
            write(49,*)' nc          pop = '
            do nc = 1,nchrom
               write(49,'(i5,3(2x,e24.16))')
     $              nc,(POP(ng,nc),ng=1,ngene)
            enddo
         endif
      endif

      IF (ITRANS.EQ.2) THEN
         DO NG=1,NGENE
            R(NG,1) = POP(NG,1)-POP(NG,2)
         ENDDO
         DO NGG=2,NGENE
            DO NG=1,NGENE
               R(NG,NGG) = 0.0
               IF (NG.EQ.NGG) R(NG,NGG) = 1.0
            ENDDO
         ENDDO
      ENDIF
C
C-----------------------------------------------------------------------
C          *** CHECK R FOR LINEAR INDEPENDENCE. BECAUSE OF HOW
C              THE R(,2) THROUGH R(,NGG) VECTORS HAVE BEEN FORMED,
C              THE FIRST ELEMENT OF R(,1) CANNOT BE ZERO ***
C
      IF (R(1,1).EQ.0.0) THEN
         IF (NNN.NE.0) THEN
            WRITE (6,101) NNN
c$$$            WRITE (34,101) NNN
  101       FORMAT ('*** LINEAR DEPENDENCE DETECTED AT IPOP =',I6,
     1              ' CONTINUE WITHOUT GENE-SPACE TRANSFORMATION ***')
         ENDIF
         RETURN
      ENDIF
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** COMPUTE TRANSFORMATION MATRIX COEFFICIENTS U(NG,NG)
C              BUT ONLY IF ITRANS=1 ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** START WITH U(NG,1) ***
C
      IF (ITRANS.EQ.2) THEN
         SUM = 0.0
         DO NG=1,NGENE
            SUM = SUM+R(NG,1)**2
         ENDDO
         SUM = SQRT(SUM)
         DO NG=1,NGENE
            U(NG,1) = R(NG,1)/SUM
         ENDDO
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** NOW DO NGG=2,NGENE  ***
C
         DO NGG=2,NGENE
            NSUM = NGG-1
            DO NG=1,NGENE
               V(NG) = R(NG,NGG)
            ENDDO
            DO NS=1,NSUM
               DO NG=1,NGENE
                  V(NG) = V(NG)-U(NGG,NS)*U(NG,NS)
               ENDDO
            ENDDO
C
            SUM = 0.0
            DO NG=1,NGENE
               SUM = SUM+V(NG)**2
            ENDDO
            SUM = SQRT(SUM)
            DO NG=1,NGENE
               U(NG,NGG) = V(NG)/SUM
            ENDDO
         ENDDO
c$$$         if(debug)then
c$$$            DO NGG=1,NGENE
c$$$               DO NG=1,NGENE
c$$$                  WRITE (49,201) NGG,NG,U(NG,NGG)
c$$$ 201              FORMAT ('NGG,NG,U =',2I4,F15.10)
c$$$               ENDDO
c$$$            ENDDO
c$$$         endif
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ENDIF
C
C          *** TRANSFORMATION MATRIX (U(NG,NGG)) COMPUTATION 
C              IS COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** TRANSFORM NC VECTORS (POP(NG,NC)), RETURN THE 
C              RESULT IN POP BUT ONLY IF ITRANS=1 ***
C
      IF (ITRANS.GE.1) THEN
         DO NC=1,NCHROM
            DO NG=1,NGENE
               DUM(NG) = 0.0
               DO NGG=1,NGENE
                  DUM(NG) = DUM(NG)+U(NGG,NG)*POP(NGG,NC)
               ENDDO
            ENDDO
            DO NG=1,NGENE
               POP(NG,NC) = DUM(NG)
            ENDDO
         ENDDO
      ENDIF
C
C          *** TRANSFORMATION OPERATION COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** TRANSFORM NC VECTORS (POP(NG,NC)) BACK TO THE
C              ORIGINIAL COORDINATE SYSTEM, RETURN THE 
C              RESULT IN POP BUT ONLY IF ITRANS=-1 ***
C-----------------------------------------------------------------------
C          *** FIRST COMPUTE INVERSE OF ORIGINAL TRANSFORMATION
C              MATRIX. BECAUSE THIS IS A UNITARY TRANSOFRMATION
C              INVERSE MATRIX IS SIMPLY THE TRANSPOSE OF THE
C              ORIGINAL MATRIX ***
C
      IF (ITRANS.EQ.-2) THEN
         DO NGG=1,NGENE
            DO NG=1,NGENE
               W(NG,NGG) = U(NGG,NG)
            ENDDO
         ENDDO
CCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
c$$$         if(debug)then
c$$$            DO NGG=1,NGENE
c$$$               DO NG=1,NGENE
c$$$                  WRITE (49,202) NGG,NG,W(NG,NGG)
c$$$ 202              FORMAT ('NGG,NG,W =',2I4,F15.10)
c$$$               ENDDO
c$$$            ENDDO
c$$$         endif
      ENDIF
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C          *** TRANSFORM NC VECTORS (POP(NG,NC)) BACK ***
C
      IF (ITRANS.LE.-1) THEN
         DO NC=1,NCHROM
            DO NG=1,NGENE
               DUM(NG) = 0.0
               DO NGG=1,NGENE
                  DUM(NG) = DUM(NG)+W(NGG,NG)*POP(NGG,NC)
               ENDDO
            ENDDO
            DO NG=1,NGENE
               POP(NG,NC) = DUM(NG)
            ENDDO
         ENDDO
      ENDIF

C-----------------------------------------------------------------------
c    unnormalizing  genes 0 -> 1.0
      if(scale_genes .and. itrans.le.-1)then
         if(debug)then
            write(49,*)'before unscaling '
            write(49,*)' nc          pop = '
            do nc = 1,nchrom
               write(49,'(i5,3(2x,e24.16))')
     $              nc,(POP(ng,nc),ng=1,ngene)
            enddo
         endif
         do ng = 1,ngene
            scal = (genemax(ng)-genemin(ng))
            do nc = 1,nchrom
               POP(ng,nc) = pop(ng,nc)*scal +genemin(ng)
            enddo
         enddo
         if(debug)then
            write(49,*)'after unscaling '
            write(49,*)' nc          pop = '
            do nc = 1,nchrom
               write(49,'(i5,3(2x,e24.16))')
     $              nc,(POP(ng,nc),ng=1,ngene)
            enddo
         endif
      endif
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** CHECK TO MAKE SURE THAT NONE OF THE GENES IS
C              OUT OF RANGE. IF THEY ARE RESET THEM TO THE
C              APPROPRIATE GENEMAX OR GENEMIN VALUE ***
C
      if(itrans.le.-1)then
         DO NC=1,NCHROM
            DO NG=1,NGENE
               IF (POP(NG,NC).LT.GENEMIN(NG)) POP(NG,NC) = GENEMIN(NG)
               IF (POP(NG,NC).GT.GENEMAX(NG)) POP(NG,NC) = GENEMAX(NG)
            ENDDO
         ENDDO
      endif

      if(debug)then
         write(49,*)'after transformation'
         write(49,*)' nc          pop = '
         do nc = 1,nchrom
            write(49,'(i5,3(2x,e24.16))')
     $           nc,(POP(ng,nc),ng=1,ngene)
         enddo
      endif
C
C          *** TRANSFORMATION OPERATION COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
