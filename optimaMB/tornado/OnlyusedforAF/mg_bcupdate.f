c ********************************************************************
c ********************* main integration routine *********************
c ********************************************************************
c calling routine:  main
c
      subroutine mg_bcupdate(lvl,mglev,
     &                    npts2,jdim,kdim,q,pk,s,press,
     &                    sndsp,turmu,fmu,vort,turre,vk,ve,
     &                    x,y,xy,xyj,xit,ett,ds,
     &                    uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &                    spectxi,specteta,precon,tmp)
c
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer   fblk,cblk
      dimension npts2(maxblk)
      dimension q(jdim*kdim*4),  press(jdim*kdim),  sndsp(jdim*kdim)
      dimension s(jdim*kdim*4),  xy(jdim*kdim*4),   xyj(jdim*kdim)
      dimension xit(jdim*kdim),  ett(jdim*kdim),    ds(jdim*kdim)
      dimension x(jdim*kdim),    y(jdim*kdim),      turmu(jdim*kdim)
      dimension vort(jdim*kdim), turre(jdim*kdim),  fmu(jdim*kdim)
      dimension vk(jdim*kdim),   ve(jdim*kdim),     pk(jdim*kdim*4)
      dimension spectxi(jdim*kdim*3),  specteta(jdim*kdim*3)
      dimension precon(jdim*kdim*8),   tmp(jdim*kdim*4)
c
      dimension uu(jdim*kdim),    ccx(jdim*kdim)
      dimension vv(jdim*kdim),    ccy(jdim*kdim)
      dimension coef2x(jdim*kdim),coef2y(jdim*kdim)
      dimension coef4x(jdim*kdim),coef4y(jdim*kdim)
c
      dimension f0(maxj),work2(maxj,4)
      common/worksp/f0,work2
c
c
c     -first update pressure and soundspeed
c     -then update boundaries
c     -then follow the same pattern as end of integrat.f
c       ... namely all that contained in master_integrat.inc
        
      do ii=nblkstart,nblkend
        call calcps(ii,jmax(ii),kmax(ii),q(lqptr(ii)),
     &        press(lgptr(ii)),sndsp(lgptr(ii)),
     &        precon(lprecptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &        jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &        kbegin(ii)-nsd1(ii),kend(ii)+nsd3(ii))
      enddo
c       
c     -calculate the circulation 
      if(fsmach.le.1.0) call bccirc                              
c
      do 702 ii=nblkstart,nblkend
        do 701 iside=1,4
          if (ibctype(ii,iside).gt.0) then
            if (ibctype(ii,iside).le.4) then
              call bcupdate(ii,jmax(ii),kmax(ii),q(lqptr(ii)),
     &              press(lgptr(ii)),sndsp(lgptr(ii)),
     &              turre(lgptr(ii)),vk(lgptr(ii)),ve(lgptr(ii)),
     &              xy(lqptr(ii)),xit(lgptr(ii)),ett(lgptr(ii)),
     &              xyj(lgptr(ii)),x(lgptr(ii)),y(lgptr(ii)),f0,
     &              jbegin(ii),jend(ii),kbegin(ii),kend(ii),
     &              jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii),
     &              ibctype(ii,iside),ibcdir(ii,iside),iside)
            endif
          endif
 701    continue
 702  continue
c       
c     Stan D.R/Luis M.  29/09/98
c     -bcavg modified to include turmu as variable ... 
c      see note in etaexpl for details 

c     -- mods by m. nemec, sept 2001 --
c     -- commented out routines bcsng2v_mod and fixteavg2 routines for
c     viscous flow, further comments below --

c     mn: to get agreement with optima2d for c-grids add +1 and -1 to
c     jminbnd and jmaxbnd in bcavg call...
      do ii=nblkstart,nblkend
         do nside1=1,3,2
            if (ibctype(ii,nside1).eq.5) then
c     write (*,*) 'average bc block side',ii,nside1
               in = lblkpt(ii,nside1)
               nside2=lsidept(ii,nside1)
               call bcavg(nside1, nside2, jmax(ii), kmax(ii),
     &              q(lqptr(ii)), xyj(lgptr(ii)), press(lgptr(ii)),
     &              turre(lgptr(ii)), turmu(lgptr(ii)), vk(lgptr(ii)),
     &              ve(lgptr(ii)), jminbnd(ii), jmaxbnd(ii),
     &              kminbnd(ii), kmaxbnd(ii), jmax(in), kmax(in),
     &              q(lqptr(in)), xyj(lgptr(in)), press(lgptr(in)),
     &              turre(lgptr(in)), turmu(lgptr(in)), vk(lgptr(in)),
     &              ve(lgptr(in)), jminbnd(in), jmaxbnd(in),
     &              kminbnd(in), kmaxbnd(in), .true.)
            endif
         end do
      end do
c
c
c
c     tecopy was before halo ---- put below
c     copy final results to halo of neighbors
c     do not copy halo for bcavg condition!!!!! (ibctype=5)
c     -----------------------------------------------------
c
      do 975 ii=nblkstart,nblkend
      do 975 nside1=2,4,2
        i2 = lblkpt(ii,nside1)
        if (i2.ne.0) then
c          write(50,*) 'doing block',ii,i2
c         -final copy must include the entire side not just low --- up
c          special halo routine including jacobians !!!!!!
c
          nside2=lsidept(ii,nside1)
          call halo4j(ii,nhalo,nside1,nside2,
     &      jmax(ii),kmax(ii),q(lqptr(ii)),xyj(lgptr(ii)),
     &      jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &      kminbnd(ii),kmaxbnd(ii),
     &      jmax(i2),kmax(i2),q(lqptr(i2)),xyj(lgptr(i2)),
     &      jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &      kminbnd(i2),kmaxbnd(i2))

        endif
  975 continue
c
c     -modification by S.D. 07/07/98
c     -now that the halo columns have been copied, do the same for
c      the halo rows if nhalo=2
      do 976 ii=nblkstart,nblkend
      do 976 nside1=1,3,2
        i2 = lblkpt(ii,nside1)
        if(i2.ne.0 .and. (ibctype(ii,nside1).ne.5 .or. nhalo.ge.2)) then
          nside2=lsidept(ii,nside1)
          call halo4j(ii,nhalo,nside1,nside2,
     &          jmax(ii),kmax(ii),q(lqptr(ii)),xyj(lgptr(ii)),
     &          jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &          kminbnd(ii)-nsd1(ii),kmaxbnd(ii)+nsd3(ii),
     &          jmax(i2),kmax(i2),q(lqptr(i2)),xyj(lgptr(i2)),
     &          jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &          kminbnd(i2)-nsd1(i2),kmaxbnd(i2)+nsd3(i2))
        endif
 976  continue
c
c
c     S.D.R.
c     -fixte.f copies upper and lower-surface trailing edge data
c      to downstream block 
c     -i.e. for a 3-blk case downstream blocks would be blk 1 and blk 3.
c     -triple value point .... one for upper & lower surface in blk2
c      which are copied to blks 1 & 3 by fixte.f 
c      Those two points in blocks 1 & 3 then averaged by fixteavg2.f.
c
c      write(6,*) 'before mg'
c      write(6,66) ncopy,nsing,nsng,nteavg
c      write(6,*)  'lvl=',lvl,mglev,mglev*lvl,mglev*(lvl-1)
c      call flush(6)
      if (mg .or. gseq) then
c       note: divide operation higher priority than multiplication
c             -so its done first by the compiler
c             -note: can't do ncopy * (lvl/mglev) because term
c                    in brackets < 1 and thus is stored as 0 (integer)
        ncopystart = ncopy  /mglev * (lvl-1) + 1
        ncopyend   = ncopy  /mglev * lvl
        nsingstart = nsing  /mglev * (lvl-1) + 1
        nsingend   = nsing  /mglev * lvl
        nsngstart  = nsng   /mglev * (lvl-1) + 1
        nsngend    = nsng   /mglev * lvl
        nteavgstart= nteavg /mglev * (lvl-1) + 1
        nteavgend  = nteavg /mglev * lvl
        n6sngstart = n6sng  /mglev * (lvl-1) + 1
        n6sngend   = n6sng  /mglev * lvl
      else
        ncopystart = 1
        ncopyend   = ncopy
        nsingstart = 1
        nsingend   = nsing
        nsngstart  = 1
        nsngend    = nsng
        nteavgstart= 1
        nteavgend  = nteavg
        n6sngstart = 1
        n6sngend   = n6sng
      endif
c
c      write(6,66) ncopystart,ncopyend,nsingstart,nsingend
c      write(6,66) nsngstart,nsngend,nteavgstart,nteavgend
c      write(6,66) n6sng,n6sngstart,n6sngend
c      call flush(6)
c 66   format(4i6)
      if (sngvalte) then
c       -for single-value trailing edge apply fixteavg2 to trailing-edge
c        data on element before transfer over to downstream blocks.
c       -note this loop affects all stagnation points, including those
c        near the leading edge, so those values are averaged too.
c        If you want to avoid that do 1,ncopy/2 for leading edge
c        and ncopy/2+1,ncopy for trailing edge
        do 879 ii=ncopystart,ncopyend,2
          iblk1=ibte1(ii)
          iblk2=ibte1(ii+1)
          call fixteavg2(jmax(iblk1),kmax(iblk1),
     &          q(lqptr(iblk1)),xyj(lgptr(iblk1)),
     &          jmax(iblk2),kmax(iblk2),
     &          q(lqptr(iblk2)),xyj(lgptr(iblk2)),
     &          ijte1(ii),ikte1(ii),ijte1(ii+1),ikte1(ii+1))
 879    continue
      endif
c
c     -avg of points away from sing (inviscid cases only).
      if (.not.mg .and. .not.sngvalte) then
        do 890 ii=nsingstart,nsingend
c         -use fixteavg2.f to average leading-edge singular point
c          for inviscid flows
cmn          iblk1=ibs1(ii)
cmn          iblk2=ibs2(ii)
cmn          write (*,*) ii,ibs1(ii),ibs2(ii)
cmn          write (*,*) js1(ii),ks1(ii),js2(ii),ks2(ii)
cmn          call fixteavg2(jmax(iblk1),kmax(iblk1),
cmn     &          q(lqptr(iblk1)),xyj(lgptr(iblk1)),
cmn     &          jmax(iblk2),kmax(iblk2),
cmn     &          q(lqptr(iblk2)),xyj(lgptr(iblk2)),
cmn     &          js1(ii),ks1(ii),js2(ii),ks2(ii))
c
c
c         -this is the original method used by Tom Nelson
c          (explained in manual)
          call bcsing(jmax(ibs1(ii)),kmax(ibs1(ii)),
     &          q(lqptr(ibs1(ii))),xyj(lgptr(ibs1(ii))),
     &          js1(ii),ks1(ii),javg1(ii), kavg1(ii),
     &          jmax(ibs2(ii)),kmax(ibs2(ii)),
     &          q(lqptr(ibs2(ii))),xyj(lgptr(ibs2(ii))),
     &          js2(ii),ks2(ii),javg2(ii),kavg2(ii))
 890    continue
      endif


c     -- copy values to 'downstream' blocks --
c     -- involves leading and trailing edges --
c     write (*,*) 'ncopystart,ncopyend',ncopystart,ncopyend
      do 880 ii=ncopystart,ncopyend
c     write (*,*) ibte1(ii), ibte2(ii)
c     write (*,*) ijte1(ii),ikte1(ii),ijte2(ii),ikte2(ii)
        iblk1=ibte1(ii)
        iblk2=ibte2(ii)
c       ibte1      : is block on element surface
c       ibte2      : is downstream block
c       itej1,itek1: trailing edge node on element belonging blk ibte1
c       itej2,itek2: downstream trailing edge node belonging blk ibte2
c
        call fixte(jmax(iblk1),kmax(iblk1),
     &        ijte1(ii),ikte1(ii),
     &        q(lqptr(iblk1)),xyj(lgptr(iblk1)),
     &        press(lgptr(iblk1)),sndsp(lgptr(iblk1)),
     &        turre(lgptr(iblk1)),
     &        vk(lgptr(iblk1)),ve(lgptr(iblk1)),
     &        jmax(iblk2),kmax(iblk2),
     &        ijte2(ii),ikte2(ii),
     &        q(lqptr(iblk2)),xyj(lgptr(iblk2)),
     &        press(lgptr(iblk2)),sndsp(lgptr(iblk2)),
     &        turre(lgptr(iblk2)),
     &        vk(lgptr(iblk2)),ve(lgptr(iblk2)),.true.)
 880  continue

c     -- treatment of viscous singular pt version 2 --
c     !! mods by m. nemec !!
c     the routine bcsng2v_mod is not used. this routine just averaged
c     the density and pressure at the leading edge.
c     for linearization reasons, a dual value leading edge is used for
c     viscous flows. uncomment from 'c mn start' to 'c mn end' lines to
c     get  back original code.
c     mn start
c     if (viscous) then
c     write (*,*) 'nsngstart,nsngend',nsngstart,nsngend
c     -affects leading edge stagnation point
c     -if (sngvalte=true) this is redundant
c     do 892 ii=nsngstart,nsngend
c     write (*,*) ibsng1(ii),ibsng2(ii)
c     write (*,*) jsng1(ii),ksng1(ii),jpsng1(ii),kpsng1(ii)
c     write (*,*) jsng2(ii),ksng2(ii),jpsng2(ii),kpsng2(ii)
c     call bcsng2v_mod(jmax(ibsng1(ii)),kmax(ibsng1(ii)),
c     &          q(lqptr(ibsng1(ii))),xyj(lgptr(ibsng1(ii))),
c     &          jsng1(ii),ksng1(ii),jpsng1(ii),kpsng1(ii),
c     &          jmax(ibsng2(ii)),kmax(ibsng2(ii)),
c     &          q(lqptr(ibsng2(ii))),xyj(lgptr(ibsng2(ii))),
c     &          jsng2(ii),ksng2(ii),jpsng2(ii),kpsng2(ii))
c     892    continue
c     endif
c     mn end      

c     -- this is the original tornado routine, as explained in the
c     manual version 3.0. as explained above, this routine is replaced
c     by bcsng2v_mod --
c     if (viscous) then
c     -affects leading edge stagnation point
c     -if (sngvalte=true) this is redundant
c     do 892 ii=nsngstart,nsngend
c     call bcsng2v(jmax(ibsng1(ii)),kmax(ibsng1(ii)),
c     &          q(lqptr(ibsng1(ii))),xyj(lgptr(ibsng1(ii))),
c     &          jsng1(ii),ksng1(ii),jpsng1(ii),kpsng1(ii),
c     &          jmax(ibsng2(ii)),kmax(ibsng2(ii)),
c     &          q(lqptr(ibsng2(ii))),xyj(lgptr(ibsng2(ii))),
c     &          jsng2(ii),ksng2(ii),jpsng2(ii),kpsng2(ii))
c     892    continue
c     endif

c     S.D.R
c     -fixteavg2 averages trailing edge values in downstream blocks.
c     
c     ibteavg1       : is upper-downstream block
c     ibteavg2       : is lower-downstream block
c     jteavg1,kteavg1: downstream trailing edge belonging blk ibteavg1
c     itej2,itek2    : downstream trailing edge belonging blk ibteavg2
c     
c     -- mn: next loop is only active for viscous flows --
c     -- see routine autote2.f --
c     -- !! sept 21/ 01 m. nemec mods!!
c     -- omit averaging the trailing edge value, just use dual value --
c     -- to get back original code uncomment from 'c mn start' to 'c mn
c     end' -- 
c     mn start
c     if (.not.sngvalte) then
c     write (*,*) 'nteavgstart,nteavgend',nteavgstart,nteavgend
c     do 885 ii=nteavgstart,nteavgend
c     iblk1=ibteavg1(ii)
c     iblk2=ibteavg2(ii)
c     write (*,*) ' ibteavg1, ibteavg2',ibteavg1(ii),ibteavg2(ii)
c     write (*,*) jteavg1(ii),kteavg1(ii),jteavg2(ii),kteavg2(ii)
c     call fixteavg2(jmax(iblk1),kmax(iblk1),
c     &          q(lqptr(iblk1)),xyj(lgptr(iblk1)),
c     &          jmax(iblk2),kmax(iblk2),
c     &          q(lqptr(iblk2)),xyj(lgptr(iblk2)),
c     &         jteavg1(ii),kteavg1(ii),jteavg2(ii),kteavg2(ii))
c     885    continue
c     endif
c     mn end

c      if (lvl.eq.1) then
      do 895 ii=n6sngstart,n6sngend
c        write(6,*) 'Starting averaging of 6-block singular point'
c        write(6,886) ibs6(1,ii),ibs6(2,ii),ibs6(3,ii),ibs6(4,ii),
c     &               ibs6(5,ii),ibs6(6,ii)
c 886    format(6i4)
        call bcsing6(
     1        jmax(ibs6(1,ii)),kmax(ibs6(1,ii)),q(lqptr(ibs6(1,ii))),
     &        xyj(lgptr(ibs6(1,ii))),js6(1,ii),ks6(1,ii),
     &        javg6(1,ii),kavg6(1,ii),
     2        jmax(ibs6(2,ii)),kmax(ibs6(2,ii)),q(lqptr(ibs6(2,ii))),
     &        xyj(lgptr(ibs6(2,ii))),js6(2,ii),ks6(2,ii),
     &        javg6(2,ii),kavg6(2,ii),
     3        jmax(ibs6(3,ii)),kmax(ibs6(3,ii)),q(lqptr(ibs6(3,ii))),
     &        xyj(lgptr(ibs6(3,ii))),js6(3,ii),ks6(3,ii),
     &        javg6(3,ii),kavg6(3,ii),
     4        jmax(ibs6(4,ii)),kmax(ibs6(4,ii)),q(lqptr(ibs6(4,ii))),
     &        xyj(lgptr(ibs6(4,ii))),js6(4,ii),ks6(4,ii),
     &        javg6(4,ii),kavg6(4,ii),
     5        jmax(ibs6(5,ii)),kmax(ibs6(5,ii)),q(lqptr(ibs6(5,ii))),
     &        xyj(lgptr(ibs6(5,ii))),js6(5,ii),ks6(5,ii),
     &        javg6(5,ii),kavg6(5,ii),
     6        jmax(ibs6(6,ii)),kmax(ibs6(6,ii)),q(lqptr(ibs6(6,ii))),
     &        xyj(lgptr(ibs6(6,ii))),js6(6,ii),ks6(6,ii),
     &        javg6(6,ii),kavg6(6,ii))
 895  continue
c      endif

csdr  -see mod. note before first call to calcps.f above.
c      write(6,*) 'before calcps at end of master_integrat'
c      call flushit(6)
      do 950 ii=nblkstart,nblkend
        call calcps(ii,jmax(ii),kmax(ii),q(lqptr(ii)),press(lgptr(ii)),
     &              sndsp(lgptr(ii)),precon(lprecptr(ii)),
     &              xy(lqptr(ii)),xyj(lgptr(ii)),
     &              jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &              kbegin(ii)-nsd1(ii),kend(ii)+nsd3(ii))
  950 continue
c      write(6,*) 'after calcps at end of master_integrat'
c      call flushit(6)
c
c
c     ***********************************************************
c     ***********************************************************
c     Note: (luis) testing grid_seq for SA model
      if(turbulnt.and.iturb.lt.4) then
        do 58 ii=nblkstart,nblkend
        do 58 nside1=2,4,2
          i2 = lblkpt(ii,nside1)
          if (i2.ne.0) then
            nside2 = lsidept(ii,nside1)
            call interf(nhalo,nside1,nside2,
     &            jmax(ii),kmax(ii),1,turre(lgptr(ii)),
     &            jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii),
     &            jmax(i2),kmax(i2),turre(lgptr(i2)),
     &            jminbnd(i2),jmaxbnd(i2),kminbnd(i2),kmaxbnd(i2))
            call halocp(ii,nhalo,1,nside1,nside2,
     &            jmax(ii),kmax(ii),turre(lgptr(ii)),
     &            jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &            kminbnd(ii),kmaxbnd(ii),
     &            jmax(i2),kmax(i2),turre(lgptr(i2)),
     &            jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &            kminbnd(i2),kmaxbnd(i2))
          endif
 58     continue
        do 59 ii=nblkstart,nblkend
        do 59 nside1=1,3,2
          i2 = lblkpt(ii,nside1)
          if (i2.ne.0.and.ibctype(ii,nside1).ne.5) then
            nside2 = lsidept(ii,nside1)
            call interf(nhalo,nside1,nside2,
     &            jmax(ii),kmax(ii),1,turre(lgptr(ii)),
     &            jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii),
     &            jmax(i2),kmax(i2),turre(lgptr(i2)),
     &            jminbnd(i2),jmaxbnd(i2),kminbnd(i2),kmaxbnd(i2))
            call halocp(ii,nhalo,1,nside1,nside2,
     &            jmax(ii),kmax(ii),turre(lgptr(ii)),
     &            jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &            kminbnd(ii)-nsd1(ii),kmaxbnd(ii)+nsd3(ii),
     &            jmax(i2),kmax(i2),turre(lgptr(i2)),
     &            jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &            kminbnd(i2)-nsd1(i2),kmaxbnd(i2)+nsd3(i2))
          endif
 59     continue
      endif
c
      return
      end
