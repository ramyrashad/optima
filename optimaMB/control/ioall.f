c ********************************************************************
c **************** input/output routine ******************************
c ********************************************************************
c calling routines: main, integrat, initia
      subroutine ioall(junit,mglev,iwhat,jdim,kdim,q,qos,press,
     >                 sndsp,turmu,fmu,vort,turre,turos,vk,ve,
     $                 xy,xyj,x,y,coef2x,coef4x,coef2y,coef4y)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer strlen,junit,jdim,kdim,namelen,lentmp
      integer ifoil,i,ii,ibn,iseg,ib,is,nunit,jc,kc,jf,kf,n,jj,kk
      integer nstart,j,k,ioerr,jtmp,ktmp,iside,nf,nend,ios,iwhat,icp
      integer iblock,iadv,ie,i0,jjmp,mglev,ntmp

      character command*40,filena2*40
      character*2 leorte,corner
c
      double precision q(jdim*kdim*4),turmu(jdim*kdim),vort(jdim*kdim)
      double precision turre(jdim*kdim),vk(jdim*kdim),ve(jdim*kdim)
      double precision press(jdim*kdim),sndsp(jdim*kdim),fmu(jdim*kdim)
      double precision xy(jdim*kdim*4),xyj(jdim*kdim),turos(jdim*kdim)
      double precision x(jdim*kdim),y(jdim*kdim),qos(jdim*kdim*4)
      double precision coef2x(jdim*kdim),coef4x(jdim*kdim)
      double precision coef2y(jdim*kdim),coef4y(jdim*kdim),dval

      double precision dtiseq(-5:20),dtmins(-5:20),dtspiseq(-5:20)
      double precision vlx(-5:20),vnx(-5:20),vly(-5:20),vny(-5:20)
      integer iends(-5:20),jacdtseq(-5:20),isbiter(-5:20,2)      
      integer isequal
      common/mesup/ dtiseq, dtmins,dtspiseq, vlx, vnx, vly, vny, 
     &        isequal, iends, jacdtseq, isbiter
c
c
c     -must do this in order ... cannot go 23,24,25, and then 27
      go to (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
     &       21,22,23,24,25,26) iwhat
c
c     **************************************************************
c     summary of ioall
c     
c     number  description
c     ------  -----------
c       1     optimization: read target cp distribution 'cptar'
c       2     optimization: read b-spline and design vars. info file
c       3     read grid from unit 8
c       4     read restart soln from unit 4 and turb from unit 2
c       5     nothing
c       6     output surface cp distribution on unit 20
c       7     optimization: output of target pressure distribution
c       8     nothing
c       9     grid sequencing input
c      10     output q file for restart on unit 4
c      11     final output of forces on unit 6
c      12     final output of total forces on unit 21 for easy viewing
c      13     nothing
c      14     output viscous data on unit 14, y+ u+ cf
c      15     nothing
c      16     output grid in plot3d format
c      18     Open tubulence model convergence history file on unit 18 
c      19     Open meanflow convergence history file on unit 19
c      20     Spalart-Allmaras convergence history on unit 18
c      21     Read in connectivity files etc.
c      22     output metrics unit 32
c      23     output viscous data to unit 33
c      24     output disipation to unit 36
c      25     output rhs residual to unit 37
c      26     output sorted segment data describing elements to file
c     **************************************************************
c
    1 continue
c
c     -- read target pressure distribution 'cptar' --
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+7) = '.cptar'
      open ( unit=n_all, file=filena, status='unknown' )
c
      ifoil = 1
      i = 1
      do ii = 1, 9999
         read (n_all,*,end=100) xcp_tar(i,ifoil), cp_tar(i,ifoil)
         if ( abs(xcp_tar(i,ifoil)-999.0) .lt. 1.d-12 ) then
c     write (*,*) 'done',i,ifoil
            xcp_tar(i,ifoil) = 0.0
            cp_tar(i,ifoil) = 0.0
            icpn(ifoil) = i-1
            ifoil = ifoil + 1
            i = 0
         end if
         i = i+1
      end do
 100  close(n_all)

c      write (*,*) 'ioall: target cp'
c      do ifoil = 1, nfoils
c      write (*,*) 'Target pressure for airfoil: ',ifoil
c      do i = 1,icpn(ifoil)
c      write (*,*) xcp_tar(i,ifoil), cp_tar(i,ifoil)
c      end do
c      end do

c     -- check if xcp_tar agrees with grid and in future interpolate
c     values --
      write (*,*) 'checking xcp'
      do ifoil = 1,nfoils
         ibn = 1
         do iseg = 1,nsegments(ifoil)
            ib = isegblk(iseg,ifoil)
            is = isegside(iseg,ifoil)
            call checkxcp(ibn, jmax(ib), kmax(ib), jminbnd(ib),
     &           jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), ifoil, is,
     &           x(lgptr(ib)))
         end do

         write (*,*) icpn(ifoil),ibn-1

         if ( icpn(ifoil).ne.ibn-1 ) then
            write (*,*) 'problem with icpn'
            stop
         end if
      end do
      write (*,*) 'done xcp'
      go to 10000

c     **************************************************************

    2 continue

c     -- read b-spline info file -- 

      do ifoil = 1,maxfoil
         nbseg(ifoil) = 0
         do iseg = 1,maxbseg
            nbcp(ifoil,iseg) = 0
         end do
      end do

c     -- design variables file --
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+5) = '.dvs'
      open (unit=n_all, file=filena, status='old', form='formatted')

c     -- read design variables --
c     -- ndv: number of design variables --
c     -- nbdv: number of b-spline design variables --
c     -- ntdv: number of translational design variables --
c     -- ngdv = nbdv + ntdv --
      read (n_all, *) ndv, nbdv, ntdv
      do i = 1,ndv
         read (n_all, *) ifoil, iseg, icp, dval
         idvf(i) = ifoil
         idvs(i) = iseg
         idvi(i) = icp
         dvs(i)  = dval
      end do
      ngdv = nbdv + ntdv

      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+6) = '.fhis'
      open(unit=n_fhis, file=filena, form='formatted')
  
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+5) = '.bsp'
      open(unit=n_all, file=filena, status='old', form='formatted')

      read (n_all, *) jbsord
      if (jbsord.gt.ibsord) stop 'ioall: jbsord > ibsord !!'
c     
      do ii = 1,nfoils
         read (n_all, *) ifoil, nbseg(ifoil), ibap(ifoil)
c     write (*,*) ifoil, nbseg(ifoil), ibap(ifoil)
         if (nbseg(ifoil) .ne. 0) then
            do iseg = 1,nbseg(ifoil)
               read (n_all, *) nbcp(ifoil,iseg), isurf(ifoil,iseg)
               read (n_all, *) ibstart(ifoil,iseg), ibend(ifoil,iseg)
               
               if (nbcp(ifoil,iseg).gt.ibsnc) stop
     &              'ioall: nbcp > ibsnc !!' 

               do i = 1,nbcp(ifoil,iseg)
                  read (n_all, *) bcp(i,1,ifoil,iseg),
     &                 bcp(i,2,ifoil,iseg)

c     -- store original control points in bcpo --
                  bcpo(i,1,ifoil,iseg) = bcp(i,1,ifoil,iseg)
                  bcpo(i,2,ifoil,iseg) = bcp(i,2,ifoil,iseg)
               end do
c     
c     -- read initial parameter vector --
               if ( (ibend(ifoil,iseg)-ibstart(ifoil,iseg)+1).gt.
     &              ibody )stop 'ioall: ibody too small'
               do i = 1,ibend(ifoil,iseg)-ibstart(ifoil,iseg)+1
                  read (n_all, *) bt(i,ifoil,iseg)
c     write (*,*) i,bt(i,ifoil,iseg)
               end do
c     
c     -- read initial knot vector --
               do i = 1,jbsord+nbcp(ifoil,iseg)
                  read (n_all, *) bknot(i,ifoil,iseg)
c     write (*,*) i, bknot(i,ifoil,iseg)
               end do
            end do
         end if
c     write (*,*) 'finished foil', ifoil
      end do
      close(n_all)

c     -- multi-point optimization where alpha may (or may not) be a
c     design variable -- 
      ii = 0
      do i=1, mpopt
         if ( dvalfa(i) ) then
            ii=ii+1
            dvs(ngdv+ii) = alphas(i)
c     -- idvi is used as a pointer to the location in dvs array --
            idvi(ngdv+i)  = ngdv+ii
            ndv = ndv + 1
         end if
         idvf(ngdv+i) = -1
         idvs(ngdv+i) = -1
      end do

c     -- read constraints --
      ios = 0
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+5) = '.oc'
      open(unit=n_all,file=filena,iostat=ios,err=200,status='old')

      read(n_all,*)
      read(n_all,*) ntcon, ngapc, novc
      read(n_all,*)      
      do i=1,ntcon
         read(n_all,*) itcf(i), ctx(i), cthtar(i)
      end do

      read(n_all,*)
      do i=1,ngapc
         read(n_all,*) ifgapc(i), gapl(i), gapu(i)
      end do
      
      read(n_all,*)
      do i=1,novc
         read(n_all,*) ifovc(i), ovl(i), ovu(i)
      end do

      if (ntdv.eq.0 .and. novc.ne.0) then
         stop '!!ioall.f: novc is not zero but ntdv is zero!!'
      end if

      namelen = strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+7) = '.ochis'
      open(unit=n_con, file=filena, status='new', form='formatted')

      close(n_all)

 200  continue

      if (ios .ne. 0) then
         ntcon = 0
         ngapc = 0
         novc  = 0
         write (n_out,210)
 210     format (3x,'Input file .oc not found!',3x/, 
     &        'No optimization contraints specified!')
      end if
      
      if (nbdv.gt.0) then
         write (n_out,220)
 220     format (/3x,'Airfoil control points:')
         do ifoil = 1,nfoils
            do iseg = 1,nbseg(ifoil)
               if ( nbcp(ifoil,iseg) .ne. 0 ) then
                  write (n_out,224) ifoil,iseg
                  do i = 1,nbcp(ifoil,iseg)
                     write (n_out,230) i, bcp(i,1,ifoil,iseg),
     &                    bcp(i,2,ifoil,iseg)
                  end do
               end if
            end do
         end do
 224     format(/3x,'Element:',i3,3x,'B-Spline Segment:',i3/)
 230     format(i4, 2e14.6)
      end if

      write (n_out,240)
 240  format (/3x,'Design variables:'/)
      do i=1,ndv
         write (n_out,250) i, dvs(i)
      end do
 250  format(i4, e14.6)

      ii = 0
      do i=1, mpopt      
         if ( dvalfa(i) ) then
            ii = ii + 1
            write (n_out,252) i
            write (n_out,254) dvs(ngdv+ii), ngdv+ii
         end if
      end do 
 252  format (/3x,'Angle of attack is a design variable for',
     &     ' design point', i3)
 254  format (3x,'Initial AOA:',f6.2,1x,'DVS index:',i4)

      write (n_out,260) ndv
 260  format (/3x,'Number of design variables:', i4/)

      write (n_out,270) 
 270  format (3x,'Weight factor WFL at each design point:')
      do i=1,mpopt
         write (n_out,280) i,wfls(i)
      end do
 280  format (8x,i3,f6.2)

      go to 10000

c     **************************************************************
c       read geometry:  plot3d multi-grid format, whole grid
c     **************************************************************
    3 continue
         namelen =  strlen(grid_file_prefix)

         filena = grid_file_prefix
         filena(namelen+1:namelen+3) = '.g'
         namelen = namelen + 2
         if (iread.eq.1) then
            open(unit=n_all,file=filena,status='old',form='formatted')
         elseif (iread.eq.2) then
            open(unit=n_all,file=filena,status='old',form='unformatted')
         endif
c     -just linking output gridfile to input gridfile (not necessary)
         lentmp = strlen(output_file_prefix)
         filena2 = output_file_prefix
         filena2(lentmp+1:lentmp+3)= '.g'
         command = 'ln -s '
         command(7:6+namelen) = filena
         lentmp = namelen+6+1
         namelen = strlen(filena2)
         command(lentmp+1:lentmp+namelen) = filena2
         call system(command)

      if (iread.eq.1) then
        read(n_all,351) nblks
        read(n_all,351) (jbmax(i), kbmax(i),i=1,nblks)
        call setptr
c       -note: halo points are used
        do 350 i=1,nblks
          jminbnd(i) = nhalo + 1
          kminbnd(i) = nhalo + 1
          jmaxbnd(i) = jbmax(i) + nhalo
          kmaxbnd(i) = kbmax(i) + nhalo
          i0 = lgptr(i) + nhalo*(jbmax(i) + 2*nhalo) + nhalo - 1
          jjmp = jbmax(i) + 2*nhalo
          read(n_all,352)((x(i0+j+k*jjmp),j=1,jbmax(i)),k=0,kbmax(i)-1),
     &               ((y(i0+j+k*jjmp),j=1,jbmax(i)),k=0,kbmax(i)-1)
 350    continue
 351    format(2i5)
 352    format(5e14.7)

      elseif(iread.eq.2)then
        read(n_all) nblks
        read(n_all) (jbmax(i), kbmax(i),i=1,nblks)

        pblk(1) = 1
        do i=1,4
          pblk(i+1)=pblk(i)+nblks
        end do
c     
        nblkstot=nblks         
        if (mg .or. gseq) then
          nblkstot = nblks * mglev
          do 345 ii = 1,mglev-1
            call set_level(ii)
            do 344 n = nblkstart,nblkend
              ntmp = n + nblks
              jbmax(ntmp)=(jbmax(n)+1)/2
              kbmax(ntmp)=(kbmax(n)+1)/2
 344        continue
 345      continue
        endif

        if (nblks .gt. maxblk) stop 'Error: nblks > maxblk'  
        if (nblks .gt. mxbfine) stop 'Error: nblks > mxbfine'

        call setptr
        write(n_out,*)
     &       '----------------------------------------------------'
        write(n_out,374) nhalo
 374    format(1x,6hNHALO=,i2)
        do 375 i=1,nblks
          jminbnd(i) = nhalo + 1
          kminbnd(i) = nhalo + 1
          jmaxbnd(i) = jbmax(i) + nhalo
          kmaxbnd(i) = kbmax(i) + nhalo
c         -note: halo points are used
          i0 = lgptr(i) + nhalo*(jbmax(i) + 2*nhalo) + nhalo
          jjmp = jbmax(i) + 2*nhalo
c          if (i.gt.1) write(n_out,*)
c          write(n_out,353) i,nblks
c          write(n_out,354) jbmax(i),kbmax(i)
c          write(n_out,355) i0,jjmp
c 353      format('     reading block # ',i2,'  of ',i2)
c 354      format('     grid size:  j = ',i3,',   k = ',i3)
c 355      format('     pointers :  1st point = ',i6,',   j-jump =',i4)
          read(n_all) ((x(i0+(j-1)+k*jjmp),j=1,jbmax(i)),k=0,kbmax(i)-1)
     &              , ((y(i0+(j-1)+k*jjmp),j=1,jbmax(i)),k=0,kbmax(i)-1)
 375    continue

c        write(n_out,*)
c     &       '----------------------------------------------------'
      endif

      close(n_all)

      if (mg .or. gseq) then
c       -setting boundary indeces for all sub-level grids.        
        do ii=2,mglev
          call set_level(ii)
          do n = nblkstart,nblkend
            jminbnd(n) = nhalo + 1
            kminbnd(n) = nhalo + 1
            jmaxbnd(n) = jbmax(n) + nhalo
            kmaxbnd(n) = kbmax(n) + nhalo
          enddo
        enddo
c     
        do ii = 1,mglev-1
          call set_level(ii)
c         -n refers to fine grid and ntmp refers to coarse grid
          do n = nblkstart,nblkend
            ntmp = n + nblks
            jf=jbmax(n)+2*nhalo
            kf=kbmax(n)+2*nhalo
            jc=jbmax(ntmp)+2*nhalo
            kc=kbmax(ntmp)+2*nhalo
            call mkcoarse(ntmp,jf,kf,x(lgptr(n)),y(lgptr(n)),
     &                         jc,kc,x(lgptr(ntmp)),y(lgptr(ntmp)))
          enddo
        enddo
c
        do ii = mglev,2,-1
          call set_level(ii)
          if (ii.eq.4) then
c            nunit=48
            nunit=n_all 
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+4) = '.g4'
            open(unit=nunit,file=filena,form='unformatted')
          elseif (ii.eq.3) then
c            nunit=47
            nunit=n_all  
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+4) = '.g3'
            open(unit=nunit,file=filena,form='unformatted')
          elseif (ii.eq.2) then
c            nunit=46
            nunit=n_all 
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+4) = '.g2'
            open(unit=nunit,file=filena,form='unformatted')
          endif
          write(nunit) nblks
          write(nunit) (jbmax(i), kbmax(i),i=nblkstart,nblkend)
          do i=nblkstart,nblkend
            call writegrid(nunit,x(lgptr(i)),y(lgptr(i)),
     &            jbmax(i)+2*nhalo,kbmax(i)+2*nhalo,
     &            jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
          enddo
          close(nunit)
        enddo
      endif

      go to 10000
c
c     **************************************************************
c     Read in a binary restart file from unit 3 or 9.
c     The q read in is not yet scaled by the metric jacobian xyj,
c     the rescaling is done in initia.
c     Compatible with plot3d
c     **************************************************************
    4 continue

      if (unsteady .and. IMARCH.eq.2) then
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+4) = '.q2'
         open(unit=n_all,file=filena,status='old',
     &        form='unformatted')

         call set_level(1)
     
         read(n_all) nblks
         read(n_all) (jbmax(i),kbmax(i),i=nblkstart,nblkend)
         do i=nblkstart,nblkend
           read(n_all) fsmtem,alptem,retem,fsmtem
           call readq(n_all,qos(lqptr(i)),jmax(i),kmax(i),
     &           jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
         end do
c     
         read(n_all) istart
         close(n_all)
      end if

      if (grdseq_rest) then
         namelen =  strlen(grid_file_prefix)
         filena = grid_file_prefix
         filena(namelen+1:namelen+3) = '.q'
         open(unit=n_all,file=filena,status='old',form='unformatted')
      else

         namelen =  strlen(restart_file_prefix)
         namelen =  strlen(restart_file_prefix)
         filena = restart_file_prefix
         filena(namelen+1:namelen+3) = '.q'
         open(unit=n_all,file=filena,status='old',
     &        form='unformatted')
c     else
c     if (mpit.eq.1) then
c     namelen =  strlen(restart_file_prefix)
c     namelen =  strlen(restart_file_prefix)
c     filena = restart_file_prefix
c     filena(namelen+1:namelen+3) = '.q'
c     open(unit=n_all,file=filena,status='old',
c     &                 form='unformatted')
c     else if (mpit.eq.2) then
c     namelen =  strlen(restart_file_prefix)
c     namelen =  strlen(restart_file_prefix)
c     filena = restart_file_prefix
c     filena(namelen+1:namelen+4) = '.q2'
c     open(unit=n_all,file=filena,status='old',
c     &                 form='unformatted')
c     else
c     stop 'IOALL: too many mpit'
c     end if
c     end if
      endif
      
      if (grdseq_rest) then
         call set_level(mglev)
      else
         call set_level(1)
      endif
      read(n_all) nblks
      read(n_all) (jbmax(i),kbmax(i),i=nblkstart,nblkend)
      do i=nblkstart,nblkend
         read(n_all) fsmtem,alptem,retem,fsmtem
         call readq(n_all,q(lqptr(i)),jmax(i),kmax(i),
     &        jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
      end do
c     
      read(n_all) istart
      close(n_all)

c     -- read in the turbulence model data from unit n_all --
      if (turbulnt) then

         if (unsteady .and. IMARCH.eq.2) then
            namelen =  strlen(restart_file_prefix)
            filena = restart_file_prefix
            filena(namelen+1:namelen+4) = '.t2'
            open(unit=n_all,file=filena,status='old',form='unformatted')

            read(n_all) nblks
            read(n_all) (jbmax(i),kbmax(i),i=nblkstart,nblkend)
            do i=nblkstart,nblkend
               read(n_all) fsmtem,alptem,retem,fsmtem
               call readarr4(n_all,vk(lgptr(i)),ve(lgptr(i)),
     &              turos(lgptr(i)),turmu(lgptr(i)),jmax(i),kmax(i),            
     &              jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
            end do
            close(n_all)
         end if  ! unsteady

         if (grdseq_rest) then
            namelen =  strlen(grid_file_prefix)
            filena = grid_file_prefix
         else
            namelen =  strlen(restart_file_prefix)
            filena = restart_file_prefix
         endif

         if (mpopt.eq.1) then
            filena(namelen+1:namelen+3) = '.t'
            open(unit=n_all,file=filena,status='old',form='unformatted')
         else
            if (mpit.eq.1) then
c               write (*,*) 'reading t'
               filena(namelen+1:namelen+3) = '.t'
               open(unit=n_all,file=filena,status='old',form
     &              ='unformatted')
            else if (mpit.eq.2) then
c               write (*,*) 'reading t2'
               filena(namelen+1:namelen+4) = '.t2'
               open(unit=n_all,file=filena,status='old',form
     &              ='unformatted')
            else
               stop 'IOALL: too many mpit'
            end if
         end if

         read(n_all) nblks
         read(n_all) (jbmax(i),kbmax(i),i=nblkstart,nblkend)
         do i=nblkstart,nblkend
            read(n_all) fsmtem,alptem,retem,fsmtem
            call readarr4(n_all,vk(lgptr(i)),ve(lgptr(i)),
     &           turre(lgptr(i)),turmu(lgptr(i)),
     &           jmax(i),kmax(i),
     &           jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
         end do
         close(n_all)
      end if

      go to 10000

c     **************************************************************
    5 continue

c     -- read in optima2D --

      open(unit=n_all,file='optima2d.q',form='unformatted')

      read(n_all) nblks
      write (*,*) nblks
      read(n_all) (jbmax(i),kbmax(i),i=1,3)
      write (*,*) jbmax
      write (*,*) kbmax
      read(n_all) fsmtem,alptem,retem,fsmtem
      do i=1,3
         call readq(n_all,q(lqptr(i)),jmax(i),kmax(i),
     &        jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
      end do
      close(n_all)

      open(unit=n_all,file='optima2d.t',form='unformatted')

      read(n_all) nblks
      read(n_all) (jbmax(i),kbmax(i),i=1,3)
      read(n_all) fsmtem,alptem,retem,fsmtem
      do i=1,3
         call readar(n_all, turre(lgptr(i)), jmax(i), kmax(i),
     &        jminbnd(i), jmaxbnd(i), kminbnd(i), kmaxbnd(i))
      end do
      close(n_all)

      go to 10000
c     **************************************************************
c     -- coefficient of pressure data written to output unit 20 --
    6 continue

      write(n_cp, 664)
 664  format('#',1x, 23hCoefficient of Pressure )
      write(n_cp, 665) numiter
 665  format('# iteration # = ',i5)
c     
      do ifoil=1,nfoils
         write(n_cp,666)
 666     format
     &        ('#-----------------------------------------------------')
         write(n_cp,667) ifoil
 667     format('# cp distribution for element #',i2)
         write(n_cp,668)
 668     format('#',8x,'x',18x,'Cp',18x,'y')
         write(n_cp,666)

         do iseg=1,nsegments(ifoil)
            ib = isegblk(iseg,ifoil)
            is = isegside(iseg,ifoil)
            call writecp(nhalo, jbmax(ib), kbmax(ib), is,
     &           press(lgptr(ib)), x(lgptr(ib)), y(lgptr(ib)),
     &           xyj(lgptr(ib)))
         end do
         write(n_cp,*)
      end do
      rewind(n_cp)

      go to 10000
c     
c     
c     **************************************************************
    7 continue

c     -- output of target pressure distribution --
c     -- used to setup optimization runs --
c     -- coefficient of pressure data written to n_all --
      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+7) = '.cptar'
      open(unit=n_all,file=filena,status='new',form='formatted')

      do ifoil = 1,nfoils
         do iseg = 1,nsegments(ifoil)
            ib = isegblk(iseg,ifoil)
            is = isegside(iseg,ifoil)
            call writecptar(nhalo, jbmax(ib), kbmax(ib),is,
     &           press(lgptr(ib)), x(lgptr(ib)),
     &           xyj(lgptr(ib)), .true., n_all)
         end do
         write(n_all,700) 999.0, 999.0
      end do
 700  format(2x,f5.1,19x,f5.1)

      close(n_all)
      go to 10000

c     *****************************************************************
    8 continue

c     -- read polar file for sweeps (angle of attack or Mach number --
      ios = 0
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+7) = '.pol'
      open (unit=n_all, file=filena, iostat=ios, err=850, status='old')

      read (n_all,*) ipolar

      if (ipolar.gt.100) stop 'ioall: ipolar > 100'

      do i=1,ipolar
         read(n_all,*) polar(i)
      end do

      close (n_all)

      write (n_out,810)
      do i=1,ipolar
         write(n_out,820) i, polar(i)
      end do      

      write (n_polar,800) 
 800  format('VARIABLES= "No" "M" "ALPHA" "C_D" "C_L" "C_M" "RESID"')

 810  format(3x,'Polar run for the following angles of attack:')
 820  format(3x,i4,f10.2)
 

 850  if (ios .ne. 0) then
         write (n_out,860)
         stop
      end if

 860  format (3x,'Input file .pol not found!')

      go to 10000
c     **************************************************************
c
    9 continue
c
c     -grid sequencing input
c
      if(isequal .ge. 1)  then
c
      write(n_out,*)
      write(n_out,*)
     >'------------------------------------------------------'
      write(n_out,*)
     >'|            Grid and Time Step Sequencing           |'
      write(n_out,*)
     >'------------------------------------------------------'
      if (mg .or. gseq) then
c        is=mglev-isequal+1
c        ie=mglev
        is=mglev
        ie=mglev-isequal+1
      iadv=-1
      else
        is=1
        ie=isequal
      iadv=1
      endif
c     
      do ii =is,ie,iadv
         dtmins(ii)=0.
         read(*,*) iends(ii),dtiseq(ii),jacdtseq(ii),
     &        dtspiseq(ii),isbiter(ii,1),isbiter(ii,2),
     &        vlx(ii),vnx(ii),vly(ii),vny(ii)
c     
         write(n_out,921) ii
 921     format(' | Grid ',i2,44x,1h|)
c     
         write(n_out,924) iends(ii),jacdtseq(ii)
 924     format(2h |,5x,11hsteps     =,i6,',',4x,11hjacdt     =,i6,8x,
     &        1h|)
c     
         write(n_out,925) isbiter(ii,1),isbiter(ii,2)
         if (idmodel.eq.2) then
            write(n_out,927) vlx(ii),vnx(ii)
            write(n_out,928) vly(ii),vny(ii)
         endif
 922     format(2h |,52x,1h|)
 925     format(' |     isbiter_1 = ',i5,',    isbiter_2 = ',i5,8x,1h|)
 927     format(' |     vlxi  = ',f5.2,',    vnxi  = ',f5.2,16x,1h|)
 928     format(' |     vleta = ',f5.2,',    vneta = ',f5.2,16x,1h|)
c     
         write(n_out,926) dtiseq(ii),dtspiseq(ii)
 926     format(' |     dt    = ',f5.2,',    dtsp  = ',f5.2,16x,1h|)
         write(n_out,922)
      end do
      write(n_out,*)
     >'------------------------------------------------------'
         else
            write(n_out,*)'error in input,  isequal < 1'
         endif
c
c
      go to 10000
c
c
c     **************************************************************
 10   continue

c     -- store a binary solution/restart file --

      if (unsteady) then

         write(n_q2) nblks
         write(n_q2) (jbmax(i),kbmax(i),i=1,nblks)
         do i=1,nblks
            write(n_q2) fsmach, alpha, re*fsmach, fsmach
            call writeq(n_q2,qos(lqptr(i)),xyj(lgptr(i)),jmax(i),
     &           kmax(i),jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
         end do
         
         write(n_q2) numiter-1

         rewind(n_q2)

      end if

      write(n_q) nblks
      write(n_q) (jbmax(i),kbmax(i),i=1,nblks)
      do i=1,nblks
         write(n_q) fsmach, alpha, re*fsmach, fsmach
         call writeq(n_q, q(lqptr(i)), xyj(lgptr(i)), jmax(i), kmax(i),
     &        jminbnd(i), jmaxbnd(i), kminbnd(i), kmaxbnd(i))
      end do

      write(n_q) numiter

      rewind(n_q)
      go to 10000

c     **************************************************************
 11   continue

c     -- output of forces per element --

      if (ioptm .ne. 2) then
         write(n_eld,1140) ('~',i=1,54)
         write(n_eld,1139)
         write(n_eld,1111) clit
         write(n_eld,1112) cdit
         write(n_eld,1113) cmit
 1111 format(' |  pressure  cl = ',e16.8,'                   |')
 1112 format(' |            cd = ',e16.8,'                   |')
 1113 format(' |            cm = ',e16.8,'                   |')

      if (viscous) then
         write(n_eld,1139)
         write(n_eld,1114) clvt
         write(n_eld,1115) cdvt
         write(n_eld,1116) cmvt
 1114    format(' |  friction  cl = ',e16.8,'                   |')
 1115    format(' |            cd = ',e16.8,'                   |')
 1116    format(' |            cm = ',e16.8,'                   |')
         write(n_eld,1139)
         write(n_eld,1117) clt
         write(n_eld,1118) cdt
         write(n_eld,1119) cmt
 1117    format(' |  total     cl = ',e16.8,'                   |')
 1118    format(' |            cd = ',e16.8,'                   |')
 1119    format(' |            cm = ',e16.8,'                   |')
      endif

      if (nfoils.gt.1) then
         write(n_eld,1139)
         write(n_eld,1138)
         do ii=1,nfoils
            write(n_eld,1121) ii
 1121       format(' |  element # ',i2,
     &           '                                      |')
            write(n_eld,1122) clfoil(ii)
            write(n_eld,1123) cdfoil(ii)
            write(n_eld,1124) cmfoil(ii)
 1122       format(' |            cl = ',e16.8,'                   |')
 1123       format(' |            cd = ',e16.8,'                   |')
 1124       format(' |            cm = ',e16.8,'                   |')
         end do
      endif
      write(n_eld,1139)
      write(n_eld,1140) ('~',i=1,54)

      if (viscous) then
         write(n_eld,*)
         write(n_eld,*)
         write(n_eld,*)
         write(n_eld,1140) ('~',i=1,70)
         write(n_eld,1130) junit
         do ii=1,nfoils
            write(n_eld,1137)
            write(n_eld,1133) ii
            write(n_eld,1134) clp(ii),cdp(ii),cmp(ii)
            write(n_eld,1135) clf(ii),cdf(ii),cmf(ii)
            write(n_eld,1136) clfoil(ii),cdfoil(ii),cmfoil(ii)
         end do
         write(n_eld,1140) ('~',i=1,70)
      endif

 1130 format(1x,8h|  Grid ,i1,60x,1h|)
 1133 format(1x,13h|  element # ,i2,54x,1h|)
 1134 format(1x,1h|,6x,14hPressure  Cl= ,f9.6,2x,4hCd= ,f11.8,2x,
     &     4hCm=,f11.8,5x,1h|)
 1135 format(1x,1h|,6x,14hFriction  Cl= ,f9.6,2x,4hCd= ,f11.8,2x,
     &     4hCm=,f11.8,5x,1h|)
 1136 format(1x,1h|,6x,14hTotal     Cl= ,f9.6,2x,4hCd= ,f11.8,2x,
     &     4hCm=,f11.8,5x,1h|)
 1137 format(1x,1h|,68x,1h|)
 1138 format(1x,26h|  elemental coefficients:,27x,1h|)
 1139 format(1x,1h|,52x,1h|)
 1140 format(1x,78a)

      rewind(n_eld)

      else                      !ioptm=2

        write(n_eld,1145) alpha, (clp(ii), ii=1,nfoils), (cdp(ii),
     &        ii=1,nfoils), (cmp(ii), ii=1,nfoils), (clf(ii),
     &        ii=1,nfoils), (cdf(ii),ii=1,nfoils), (cmf(ii),
     &        ii=1,nfoils), (clfoil(ii),ii=1,nfoils), (cdfoil(ii),
     &        ii=1,nfoils), (cmfoil(ii),ii=1,nfoils) 

      end if

 1145 format(f10.3,100e15.7)

      goto 10000
c     
c     **************************************************************
   12 continue

c     -- output final force integration to file for easy access --

      write(n_ld,1200) junit,clit,cdit,cmit
      if (viscous) then
        write(n_ld,1201) clvt,cdvt,cmvt
        write(n_ld,1202) clt,cdt,cmt
        write(n_ld,1203) clt/cdt
      endif
      write(n_ld,*)
      write(n_ld,*)

 1200 format(5hGrid ,i1,2x,14hPressure  Cl= ,f9.6,2x,4hCd= ,f11.8,2x,
     &     4hCm= ,f11.8)
 1201  format(8x,14hFriction  Cl= ,f9.6,2x,4hCd= ,f11.8,2x,4hCm= ,f11.8)
 1202  format(8x,14hTotal     Cl= ,f9.6,2x,4hCd= ,f11.8,2x,4hCm= ,f11.8)
 1203  format(8x,14hRatio  Cl/Cd= ,f9.6)

      call flush(n_ld)

      goto 10000
c     **************************************************************
   13 continue

c     -- write out cp and cf history files --
      if (junit.eq.1) then
         write (n_cfhis,1302)
         write (n_cphis,1304)
      end if
 1302 format('VARIABLES="x" "C_f" "j"')
 1304 format('VARIABLES="x" "C_p"')

      do ifoil = 1,nfoils
         write (n_cphis,1310) alpha
         write (n_cfhis,1310) alpha
         do iseg = 1,nsegments(ifoil)
            ib = isegblk(iseg,ifoil)
            is = isegside(iseg,ifoil)

            call writecptar(nhalo, jbmax(ib), kbmax(ib),is,
     &           press(lgptr(ib)), x(lgptr(ib)),
     &           xyj(lgptr(ib)), .false., n_cphis)

c     -- note that viscout is being called with no jacobian in Q --
c     -- cf is ok , but yplus and uplus should not be used --
            call viscout (jmax(ib), kmax(ib), is, ibcdir(ib,is),
     &           q(lqptr(ib)),x(lgptr(ib)),
     &           y(lgptr(ib)), xy(lqptr(ib)), xyj(lgptr(ib)),
     &           fmu(lgptr(ib)), ifoil, ib,
     &           jminbnd(ib), jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib),
     &           0, n_cfhis)
         end do
      end do

 1310 format('ZONE T=" AOA ',f6.3,'"')

      goto 10000
c     **************************************************************
 14   continue
c     -- viscous data --

      write(n_cf,141) ('#',i=1,78)
 141  format(78a)
      write(n_cf,142)
 142  format('#   viscous output')
      write(n_cf,141) ('#',i=1,78)
      write(n_cf,143)
 143  format('#variables=')
      write(n_cf,144)
 144  format('#ifoil',x,'blk',3x,'i',4x,'j',6x,'x',
     &     11x,'y',10x,'cf',8x,'uplus(1)',4x,'yplus(1)')

      do ifoil=1,nfoils
         write(n_cf,146) ifoil
 146     format('#zone,t="airfoil',i3,'"')
         do iseg=1,nsegments(ifoil)
            ib = isegblk(iseg,ifoil)
            is = isegside(iseg,ifoil)
            call viscout(jmax(ib),kmax(ib),is,ibcdir(ib,is),
     &           q(lqptr(ib)),
     &           x(lgptr(ib)),y(lgptr(ib)),xy(lqptr(ib)),xyj(lgptr(ib)),
     &           fmu(lgptr(ib)),ifoil,ib,
     &           jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib),n_cf,0)
         end do
         write(n_cf,*)
      end do

      rewind(n_cf)
      goto 10000
c     ******************************************************************
   15 continue

      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+7) = '.grid'
      open ( unit=n_all, file='out.grid', status='unknown',
     &     form='unformatted' )

      write(n_all) nblks
      write(n_all) (jbmax(i), kbmax(i),i=nblkstart,nblkend)
      do i=nblkstart,nblkend
         call writegrid(n_all,x(lgptr(i)),y(lgptr(i)),
     &        jbmax(i)+2*nhalo,kbmax(i)+2*nhalo,
     &        jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
      enddo
      
      close (n_all)

      goto 10000
c     **************************************************************
   16 continue

c     -- output polar info, tecplot format --
      write (n_polar,1600) junit, fsmach, alpha, cdt, clt, cmt, resid
 1600 format (i3, 2f8.4, 3e15.7, e11.3)

      go to 10000
c     **************************************************************
   17 continue

c     -- header for eld file if ioptm=2 --
      write (n_eld,1710)
      write (n_eld,1720) nfoils, 9*nfoils+1
      write (n_eld,1730)
 1710 format('# Force output for each element')
 1720 format('# Number of airfoils, NFOILS =',i2/,
     &     '# This file has 9xNFOILS+1 columns =',i3)  
 1730 format('# The order of variables is as follows:',
     &     /'# ALPHA, nfoils x CLp, nfoils x CDp, nfoils x CMp,',
     &     /'# nfoils x CLf, nfoils x CDf, nfoils x CMf,',
     &     /'# nfoils x CLt, nfoils x CDt, nfoils x CMt,')

      go to 10000
c     **************************************************************
   18 continue
cmnc
cmnc     -header for history file:
cmn      if (restart) then
cmnc       *********Convergence History **********
cmn        namelen =  strlen(restart_file_prefix)
cmn        filena_restart = restart_file_prefix
cmn        filena_restart(namelen+1:namelen+5) = '.his'
cmnc     
cmn        namelen =  strlen(output_file_prefix)
cmn        filena = output_file_prefix
cmn        filena(namelen+1:namelen+5) = '.his'
cmnc     
cmn        command = 'cp '
cmn        namelen = strlen(filena_restart)
cmn        command(4:3+namelen) = filena_restart
cmnc       -length of 'cp ' is 3, length of filena_restart is namelen
cmnc       -add 1 for blank space at end of 'cp filena_restart' 
cmn        lentmp = namelen+3+1
cmn        namelen = strlen(filena)
cmn        command(lentmp+1:lentmp+namelen) = filena
cmn        call system(command)
cmnc
cmn      else
cmn        namelen =  strlen(output_file_prefix)
cmn        filena = output_file_prefix
cmn        filena(namelen+1:namelen+5) = '.his'
cmn      endif
cmn
cmn      open(unit=19,file=filena,status='unknown',access='append')
cmn
      go to 10000
c     
c     **************************************************************
 19   continue
c     -- output convergence history to unit n_his --
      write(n_his,1900) numiter, totime1, resid, maxres(3), maxres(1),
     &     maxres(2), residmx, clt, cdt
      call flush(n_his)
 1900 format(i5,e12.4,e13.5,3i4,e10.2,e16.8,e16.8)
      go to 10000
c     
c     **************************************************************
 20   continue
c     -output turbulence convergence history to unit 18
      write(n_this,2010) numiter, tresid, mxtur(3), mxtur(1),
     &     mxtur(2), tresmx
 2010 format(I7,1x,e15.8,3I6,1x,e15.8)
      go to 10000
c     **************************************************************
c
c     Read in connectivity file
c     lblkpt(i,j) gives the connecting block for block i side j
c     lsidept(i,j) gives the connecting side for block i side j
c     idir(i,j) gives the orientation of the neighboring block
c     ibctype(i,j) give the boundary condition type = 0  interface
c                                                     1  wall
c                                                     2  farfield
c                                                     3  outflow xi min
c                                                     4  outflow xi max
c                                                     5  avg between blocks
c     ibcdir(i,j)     gives the direction for integration for solid bc's
c              =  n   direction of parametric coordinate
c              = -n   opposite direction of parametric coordinate
c                     where n is the element number
c     jtranlo(i,j)    j point for transition
c     jtranup(i,j)    j point for transition
c     **************************************************************
 21   continue

      ios = 0
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+5) = '.con'
      open(unit=n_all,file=filena,iostat=ios,err=51,status='old')
      read(n_all,*)
      read(n_all,*)
c     -note: transition points inc .con file should NOT include
c     halo points
      do kk=1,nblks
         do jj=1,4
            read(n_all,*) ktmp,jtmp,lblkpt(kk,jj),lsidept(kk,jj),
     &           idir(kk,jj),ibctype(kk,jj),ibcdir(kk,jj),
     &           jtranlo(jj,kk),jtranup(jj,kk)
            if (jtranlo(jj,kk).eq.0) then
c     -this is to remain compatible with old version             
               if (jj.eq.1) jtranlo(jj,kk)=-99
               if (jj.eq.3) then
                  jtranlo(jj,kk)=jtranup(jj,kk)
                  jtranup(jj,kk)=-99
               endif
            endif
            if((ktmp.ne.kk).or.(jtmp.ne.jj)) then
               write(n_out,*) 'connectivity read error'
               write(n_out,*) ' kk=',kk,' jj=',jj
               write(n_out,*) ' ktmp=',ktmp,' jtmp=',jtmp
               stop
            endif
         end do
      end do
      close(n_all)

      if (mg .or. gseq) then 
         do ii=2,mglev
            call set_level(ii)
            do n=nblkstart,nblkend
               do jj=1,4
c     -here ntmp refers to fine grid and n to coarse grid
                  ntmp = n - nblks
                  if(lblkpt(ntmp,jj).eq.0) then
                     lblkpt(n,jj) = 0
                  else
                     lblkpt(n,jj) = lblkpt(ntmp,jj)+nblks
                  endif
                  lsidept(n,jj) = lsidept(ntmp,jj) 
                  idir(n,jj)    = idir(ntmp,jj)
                  ibctype(n,jj) = ibctype(ntmp,jj)
                  ibcdir(n,jj)  = ibcdir(ntmp,jj)
                  jtranlo(jj,n) = jtranlo(jj,ntmp)
                  jtranup(jj,n) = jtranup(jj,ntmp)  
               enddo
            enddo
         enddo
      endif
c
 51   continue
      if (ios.ne.0) then
        write(n_out,*)'** error **'
        write(n_out,*)'file "tornado.con" not found!'
        stop
      endif
c
c     **************************************************************
c     Read in stagnation point specification file
c     
c     stgblk(ii)   is the block number containing stagnation point # ii
c     corner       is the corner of the block that is the stagnation
c                  point # ii as described in 'tornado.stg' and is one of
c                                'ul' = upper left
c                                'll' = lower left
c                                'ur' = upper right
c                                'lr' = lower right
c     leorte       is the stagnation point type for point # ii as
c                  described in 'tornado.stg' and is either
c                                'le' = leading edge, or
c                                'te' = trailing edge
c     
c     the character strings corner and leorte are converted into integer
c     descriptors as follows:
c     
c     stgcnr(ii)   is the corner of the block that is stagnation point
c                  number ii and is denoted as corner n, where corner n
c                  is the point that seperates sides n and (n-1) :
c                                1 = lower right
c                                2 = lower left
c                                3 = upper left
c                                4 = upper right
c     stgtyp(ii)   is the stagnation point type for point # ii,
c                  defined as :
c                                1 = leading edge
c                               -1 = trailing edge
c     **************************************************************
      ii = 1
      ioerr = 0
      ios = 0
      filena = grid_file_prefix
      filena(namelen+1:namelen+5) = '.stg'
      open(unit=n_all,file=filena,iostat=ios,err=55,status='old')
      read(n_all,*)
      read(n_all,*)
      read(n_all,*)
   54 read(n_all,58,end=57)istgblk(ii),corner,leorte
c
      if (leorte.eq.'LE') istgtyp(ii) = 1
      if (leorte.eq.'TE') istgtyp(ii) = -1
      if (corner.eq.'LR') istgcnr(ii) = 1
      if (corner.eq.'LL') istgcnr(ii) = 2
      if (corner.eq.'UL') istgcnr(ii) = 3
      if (corner.eq.'UR') istgcnr(ii) = 4
c     
      if (istgblk(ii).gt.nblks) then
        write(n_out,*)'** error **'
        write(n_out,*)'incorrect format in file "tornado.stg"'
        write(n_out,*)'block number exceeds total number in grid!'
        ioerr = 1
      elseif(istgblk(ii).le.0) then
        write(n_out,*)'** error **'
        write(n_out,*)'incorrect format in file "tornado.stg"'
        write(n_out,*)'block number specified is <= zero!'
        ioerr = 2
      endif
      if (istgcnr(ii).lt.1.or.istgcnr(ii).gt.4) then
        write(n_out,*)'** error **'
        write(n_out,*)'incorrect format in file "tornado.stg"'
        write(n_out,*)'corner type must appear in columns 12 & 13, and'
        write(n_out,*)
     &       'be in upper case (i.e., "ul","ll","ur", or "lr").'
        ioerr = 3
      endif
      if (istgtyp(ii).ne.-1.and.istgtyp(ii).ne.1) then
        write(n_out,*)'** error **'
        write(n_out,*)'incorrect format in file "tornado.stg"'
        write(n_out,*)'stagnation type must appear in columns 22 & 23,'
        write(n_out,*)'and be either "le" or "te".'
        ioerr = 4
      endif
      if (ioerr.ne.0) stop
      ii = ii + 1
      goto 54
c     
 55   continue
      if (ios.ne.0) then
        write(n_out,*)'** error **'
        write(n_out,*)'file ',filena,' not found!'
        stop
      endif
c     
 57   continue
      close(n_all)
      nstg = ii - 1
 58   format(t2,i2,t12,a2,t22,a2)
c
      if (mg .or. gseq) then
        nstgtot = nstg * mglev
        do k=2,mglev
          nstart = nstgtot/mglev*(k-1)+1
          nend   = nstgtot/mglev*k
c         -n refers to coarse grid and nf refers to fine grid          
          do n = nstart,nend
            nf = n - (k-1)*nstg
            istgblk(n) = istgblk(nf)+(k-1)*nblks
            istgtyp(n) = istgtyp(nf)
            istgcnr(n) = istgcnr(nf)
          enddo
        enddo
      endif
        
c     -reorder the stagnation point data so that le's fall on either the
c      ul (upper left)  or ll (lower left) corners and te's on
c      ur or lr corners (it makes coding simpler later!)
      call orderstg
c
c     *************************************************************
c     -adjust confile if wind tunnel or avg. psi bc's options are active
c
      if (psiavg.or.poutflow) call autocon

      if (viscoutflow) call outflowbnd(jdim,kdim,x)

c     *************************************************************
c     -adjust confile to set transition as specified in 'tornado.f05'
c
      if (turbulnt .and. iturb.ne.3) call autotran( 1, isequal, jdim,
     &     kdim, x, y)

c     write to file (if indicated):
      if (autowrite) then
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+10) = '.auto.con'
         open(unit=n_all,file=filena)
         write(n_all,*)
     &        ' Block Connectivities      Bnd Cond  Transition'
         write(n_all,*) ' blk  side blk  side dir   bc  bcdir jtlo jtup'
         do kk=1,nblkstot
            do jj=1,4
               write(n_all,959) kk,jj,
     &              lblkpt(kk,jj),lsidept(kk,jj),idir(kk,jj),
     &              ibctype(kk,jj),ibcdir(kk,jj),jtranlo(jj,kk),
     &              jtranup(jj,kk)
            end do
         end do
         close(unit=n_all)
 959     format(9i5)
      endif

c
c     **************************************************************
c     -read of jkhalo information for # of halo rows/cols on
c     each side of each block
c     
      ios = 0
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+5) = '.hal'
      open(unit=n_all,file=filena,iostat=ios,err=81,status='old')
      write(n_out,*)'halo file "',filena,'" exists -> reading!'
      read(n_all,*)
      read(n_all,*)
      do kk=1,nblks
         do jj=1,4
            read(n_all,*) ktmp,jtmp,jkhalo(kk,jj)
         end do
      end do
      close(n_all)

 81   continue

c     -generate halo file automatically (if not present):
      if (ios.ne.0) then
        call autohal
c
c       -write to file  (if indicated):
        if (autowrite) then
           namelen =  strlen(output_file_prefix)
           filena = output_file_prefix
           filena(namelen+1:namelen+10) = '.auto.hal'
           open(unit=n_all,file=filena)
           write(n_all,*)'|   Blocks |#Halo'
           write(n_all,*)'| Blk  Side| Pts'
           do iblock=1,nblkstot
              do iside=1,4
                 write(n_all,982) iblock,iside,jkhalo(iblock,iside)
              end do
           end do
           close(unit=n_all)
 982       format(3i5)
        endif
      endif
c     
c     **************************************************************
c     Read of trailing edge data for copying
c     
c     Read of singular point data for copy between blocks
c     note: this is needed because the point in the adjacent block
c     which is in the halo of a block with a singular point
c     as a corner will not be on the surface !!!!
c     
c     Include singular points in copy file
c     copy from solid wall bc's to non-solid wall blocks
c     note: the metrics may change between blocks !!!!!!
c
      ios = 0
      nsing = 0
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+5) = '.cop'
      open(unit=n_all,file=filena,iostat=ios,err=69,status='old')
      write(n_out,*)'copy file "',filena,'" exists -> reading!'
      read(n_all,*)
      read(n_all,*) ncopy
      do kk=1,ncopy
         read(n_all,*) ibte1(kk),ijte1(kk),ikte1(kk),
     &        ibte2(kk),ijte2(kk),ikte2(kk)
      end do

c     -specification of singular point on the boundary for bcsing
      if (.not.viscous) then
         read(n_all,*)
         read(n_all,*) nsing
         do kk=1,nsing
            read(n_all,*) ibs1(kk),js1(kk),ks1(kk),ibs2(kk),js2(kk),
     &           ks2(kk),javg1(kk),kavg1(kk),javg2(kk),kavg2(kk)
         end do
      endif
      close(n_all)

 69   continue
c     -generate copy file automatically (if not present):
      if (ios.ne.0) then
         call autocop(mglev)
c     
c     -write to file (if indicated):
         if (autowrite) then
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+10) = '.auto.cop'
            open(unit=n_all,file=filena)
c     
            write(n_all,*) 'Trailing edge copy data, from 1 to 2:'
            write(n_all,970) ncopy
 970        format(i5,5x,' block1, j1, k1, block2, j2, k2')
            do kk=1,ncopy
               write(n_all,971) ibte1(kk),ijte1(kk),ikte1(kk),
     &              ibte2(kk),ijte2(kk),ikte2(kk)
            end do
 971        format(6i5)

            write(n_all,*) 'Inviscid singular point specification'
            write(n_all,974) nsing
 974        format(i5,5x,
     &           'nsing * b1,j1,k1,b2,j2,k2,jav1,kav1,jav2,kav2')
            if (.not.viscous) then
               do kk=1,nsing
                  write(n_all,975) ibs1(kk),js1(kk),ks1(kk),
     &                 ibs2(kk),js2(kk),ks2(kk),
     &                 javg1(kk),kavg1(kk),javg2(kk),kavg2(kk)
 975              format(10i5)
               end do
            endif
            close(n_all)
         endif
      endif
c     
      do kk=1,ncopy
c     -add nhalo to idecies
         ijte1(kk)=ijte1(kk)+nhalo
         ikte1(kk)=ikte1(kk)+nhalo
         ijte2(kk)=ijte2(kk)+nhalo
         ikte2(kk)=ikte2(kk)+nhalo
      end do
      if (.not.viscous) then
         do kk=1,nsing
c     -add nhalo to idecies
            js1(kk)=js1(kk)+nhalo
            ks1(kk)=ks1(kk)+nhalo
            js2(kk)=js2(kk)+nhalo
            ks2(kk)=ks2(kk)+nhalo
            javg1(kk)=javg1(kk)+nhalo
            kavg1(kk)=kavg1(kk)+nhalo
            javg2(kk)=javg2(kk)+nhalo
            kavg2(kk)=kavg2(kk)+nhalo
         end do
      endif
c     
c     **************************************************************
c     -read of data for specification of pinf for inflow
c     
      ios = 0
      if(poutflow) then
         namelen =  strlen(grid_file_prefix)
         filena = grid_file_prefix
         filena(namelen+1:namelen+5) = '.pfl'
         open(unit=n_all,file=filena,iostat=ios,err=90,status='old')
         write(n_out,*)'p.inf. file "',filena,'" exists -> reading!'
         read(n_all,*)
         read(n_all,*) npavg
         do ii=1,npavg
            read(n_all,*) npblk(ii), npj(ii), npk(ii)
         end do
         close(n_all)
      endif
c     
 90   continue
c
c     generate pfl file automatically (if not present):
      if (ios.ne.0) then
        call autopfl
c     
c     write to file (if indicated):
        if (autowrite) then
           namelen =  strlen(output_file_prefix)
           filena = output_file_prefix
           filena(namelen+1:namelen+10) = '.auto.pfl'
           open(unit=n_all,file=filena)
           write(n_all,*)
     &          'specification of points used to get inflow pressure'
           write(n_all,985) npavg
 985       format(i5,5x,'number of points to avg for inlet p.')
           do ii=1,npavg
              write(n_all,*) npblk(ii),npj(ii),npk(ii),ii
 986          format(3i5,5x,'block, j and, k for point #',i2)
           end do
           close(n_all)
        endif
      endif

c     -- read data for singular pt treatment (for viscous flow) --
      ios = 0
      nsng = 0
      if (viscous) then
         namelen =  strlen(grid_file_prefix)
         filena = grid_file_prefix
         filena(namelen+1:namelen+5) = '.sng'
         open(unit=n_all,file=filena,iostat=ios,err=92,status='old')
         write(n_out,*)'singular file "',filena,'" exists -> reading!'
         read(n_all,*)
         read(n_all,*)
         read(n_all,*) nsng
         do ii=1,nsng
            read(n_all,*)ibsng1(ii),jsng1(ii),ksng1(ii),jpsng1(ii)
     &           ,kpsng1(ii),ibsng2(ii),jsng2(ii),ksng2(ii),jpsng2(ii)
     &           ,kpsng2(ii)
         end do
         close(n_all)

 92     continue

c     -generate pfl file automatically (if not present):
        if (ios.ne.0) then
           call autosng

c     -write to file (if indicated):
           if (autowrite) then
              namelen =  strlen(output_file_prefix)
              filena = output_file_prefix
              filena(namelen+1:namelen+10) = '.auto.sng'
              open(unit=n_all,file=filena)
              write(n_all,*) 'file to specify singular pt treatment', 
     &             'for viscous flow' 
              write(n_all,*)
     &             'blk1,jsng1,ksng1,jpsng1,kpsng1,blk2,jsng2,ksng2', 
     &             'jpsng2,kpsng2' 
              write(n_all,988) nsng
 988          format(i5)
              do ii=1,nsng
                 write(n_all,992)ibsng1(ii),jsng1(ii),ksng1(ii),
     &                jpsng1(ii),kpsng1(ii),
     &                ibsng2(ii),jsng2(ii),ksng2(ii),
     &                jpsng2(ii),kpsng2(ii)
 992             format(10i5)
              end do
              close(n_all)
           endif
        endif
      endif

c     **************************************************************
c     -read data for trailing edge point copies (sharp only)
      ios = 0
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+5) = '.te2'
      open(unit=n_all,file=filena,iostat=ios,err=94,status='old')
      write(n_out,*)'te2 file "',filena,'" exists -> reading!'
      read(n_all,*)
      read(n_all,*)
      read(n_all,*) nteavg
      do ii=1,nteavg
         read(n_all,*) ibteavg1(ii), jteavg1(ii), kteavg1(ii),
     &        ibteavg2(ii), jteavg2(ii), kteavg2(ii)
      end do
      close(n_all)

 94   continue

c     -generate pfl file automatically (if not present):
      if (ios.ne.0) then
         call autote2(mglev)

c     -write to file (if indicated):
         if (autowrite) then
            namelen =  strlen(output_file_prefix)
            filena = output_file_prefix
            filena(namelen+1:namelen+10) = '.auto.te2'
            open(unit=n_all,file=filena)
            write(n_all,*)'Specification of (sharp) t.e. point averages'
            write(n_all,*)'#TEavg * block1, j1, k1, block2, j2, k2'
            write(n_all,994) nteavg
 994        format(i5,5x,'# T.E. averages')
            do ii=1,nteavg
               write(n_all,995) ibteavg1(ii),jteavg1(ii),kteavg1(ii),
     &              ibteavg2(ii),jteavg2(ii),kteavg2(ii)
 995           format(6i5,5x,'blk1, j1, k1, blk2, j2, k2')
            end do
            close(n_all)
         endif
      endif

      do ii=1,nteavg
c     -add nhalo to indices
         jteavg1(ii)=jteavg1(ii)+nhalo
         kteavg1(ii)=kteavg1(ii)+nhalo
         jteavg2(ii)=jteavg2(ii)+nhalo
         kteavg2(ii)=kteavg2(ii)+nhalo
      end do

      ios = 0
c     -Read data for averaging 6-block singular points
      n6sng = 0
      namelen =  strlen(grid_file_prefix)
      filena = grid_file_prefix
      filena(namelen+1:namelen+6) = '.sng6'
      open(unit=n_all,file=filena,status='old',err=97)
      write(n_out,*) 'Reading sng6 file'
      read(n_all,*)
      read(n_all,*)
      read(n_all,*) n6sng
      do jj=1,n6sng
         do ii=1,6
            read(n_all,*) ibs6(ii,jj),js6(ii,jj),ks6(ii,jj),
     &           javg6(ii,jj),kavg6(ii,jj)
c     -these points do not contain halo points, add nhalo
            js6(ii,jj)   = js6(ii,jj)   + nhalo
            ks6(ii,jj)   = ks6(ii,jj)   + nhalo
            javg6(ii,jj) = javg6(ii,jj) + nhalo
            kavg6(ii,jj) = kavg6(ii,jj) + nhalo
         end do
      end do
c     write(n_out,*) ' Number of 6-block singular points=',n6sng
      close(n_all)

 97   goto 10000

c     **************************************************************
 22   continue
c     
c     -output metrics to unit 32 in plot3d multi-block format
c     
c     #ifdef YMP
c     call asnunit(32, '-F f77 -N ieee_dp', ier02)
c     #endif
      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+9) = '.metrics'
      open(unit=n_all,file=filena,status='unknown',form='unformatted')
c     
      write(n_all) nblks
      write(n_all) (jbmax(i),kbmax(i),i=1,nblks)
      do i=1,nblks
         write(n_all) fsmach, alpha, re*fsmach, fsmach
         call writearr(n_all,xy(lqptr(i)),jmax(i),kmax(i),jminbnd(i),
     &        jmaxbnd(i),kminbnd(i),kmaxbnd(i))
      end do
      close(n_all)
      go to 10000
c
c     **************************************************************
 23   continue
c     
c     -output viscous in plot3d multi-block whole format.
c     
c     #ifdef YMP
c     call asnunit(33, '-F f77 -N ieee_dp', ier02)
c     #endif
c      namelen =  strlen(output_file_prefix)
c      filena = output_file_prefix
c      filena(namelen+1:namelen+3) = '.t'
c      open(unit=33,file=filena,status='unknown',form='unformatted')


      if (unsteady) then

         write(n_t2) nblks
         write(n_t2) (jbmax(i),kbmax(i),i=1,nblks)
         do i=1,nblks
            write(n_t2) fsmach, alpha, re*fsmach, fsmach
            call writearr4(n_t2, vk(lgptr(i)), ve(lgptr(i)),
     &           turos(lgptr(i)), turmu(lgptr(i)), jmax(i),kmax(i),
     &           jminbnd(i), jmaxbnd(i), kminbnd(i), kmaxbnd(i))
         end do
         rewind(n_t2)

      end if   !unsteady

      
      write(n_t) nblks
      write(n_t) (jbmax(i),kbmax(i),i=1,nblks)
      do i=1,nblks
         write(n_t) fsmach, alpha, re*fsmach, fsmach
         call writearr4(n_t, vk(lgptr(i)), ve(lgptr(i)),
     &        turre(lgptr(i)), turmu(lgptr(i)), jmax(i),kmax(i),
     &        jminbnd(i), jmaxbnd(i), kminbnd(i), kmaxbnd(i))
      end do
      rewind(n_t)

      go to 10000

c     *****************************************************************

 24   continue
c     
c     -output dissipation in plot3d multi-block whole format.
c     
c     #ifdef YMP
c     call asnunit(36, '-F f77 -N ieee_dp', ier02)
c     #endif
      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+9) = '.discoef'
      open(unit=n_all,file=filena,status='unknown',form='unformatted')
c     
      write(n_all) nblks
      write(n_all) (jbmax(i),kbmax(i),i=1,nblks)
      do i=1,nblks
         write(n_all) fsmach, alpha, re*fsmach, fsmach
         call writearr4(n_all, coef2x(lgptr(i)), coef4x(lgptr(i)),
     &        coef2y(lgptr(i)), coef4y(lgptr(i)), jmax(i), kmax(i), 
     &        jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
      end do
      close(n_all)
      go to 10000
c 
c     **************************************************************
 25   continue
c     
c     -output residual in plot3d multi-block whole format.
c     -note: the residual array s is passed to this routine in array q
c     
c     #ifdef YMP
c     call asnunit(37, '-F f77 -N ieee_dp', ier02)
c     #endif
      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+10) = '.residual'
      open(unit=n_all,file=filena,status='unknown',form='unformatted')
c     
      write(n_all) nblks
      write(n_all) (jbmax(i),kbmax(i),i=1,nblks)
      do i=1,nblks
         write(n_all) fsmach, alpha, re*fsmach, fsmach
         call writearr(n_all,q(lqptr(i)),jmax(i),kmax(i),
     &        jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
      end do
      close(n_all)
      go to 10000
c     
c     **************************************************************
 26   continue
c     
c     -output sorted segment data describing elements to file
c     -write to file (if indicated):
      if (autowrite) then
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+12) = '.auto.foils'
         open(unit=n_all,file=filena)
         write(n_all,*) 'segment (block/side) list for elements'
         write(n_all,2610) nfoils
         do ifoil=1,nfoils
            write(n_all,2620) ifoil,nsegments(ifoil)
            do iseg=1,nsegments(ifoil)
               write(n_all,2630) iseg, isegblk(iseg,ifoil),
     &              isegside(iseg,ifoil)
            end do
         end do
         close(n_all)
      endif
 2610 format(' number of elements =',i2)
 2620 format(' number of segments for foil #',i2,' = ',i2)
 2630 format(' segment #',i2,':   block = ',i2,',  side = ',i1)
      go to 10000
c     
c     **************************************************************
10000 continue
c     
      return
      end                       !ioall

C ********************************************************************
C ***        subroutine to read 4 arrays                           ***
C ********************************************************************
      subroutine readar(lu,array1, jdim,kdim,jbegin,jend,kbegin,kend)

      implicit none

      integer lu,jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision array1(jdim,kdim)

      read(lu)  ((array1(j,k),j=jbegin,jend),k=kbegin,kend)

      return 
      end 
