      subroutine trip(JDIM,A,B,C,F,Q,S,J1,J2)                           
      DIMENSION A(JDIM),B(JDIM),C(JDIM),F(JDIM),Q(JDIM),S(JDIM)         
      JA = J1 + 1                                                       
      FN = F(J2)                                                        
C                                                                       
C         FORWARD ELIMINATION SWEEP                                     
C                                                                       
      Q(J1) = -C(J1)/B(J1)                                              
      F(J1) = F(J1)/B(J1)                                               
      S(J1) = - A(J1)/B(J1)                                             
      DO 10 J=JA,J2                                                     
         P =1./( B(J) + A(J)*Q(J-1))                                    
         Q(J) = - C(J)*P                                                
         F(J) = ( F(J) - A(J)*F(J-1))*P                                 
         S(J) = - A(J)*S(J-1)*P                                         
   10 CONTINUE                                                          
C                                                                       
C         BACKWARD PASS                                                 
C                                                                       
      JJ = J1 + J2                                                      
      Q(J2) = 0.                                                        
      S(J2) = 1.                                                        
      DO 11 I=JA,J2                                                     
         J = JJ - I                                                     
         S(J) = S(J) + Q(J)*S(J+1)                                      
         Q(J) = F(J) + Q(J)*Q(J+1)                                      
11    continue                                                          
      F(J2) = ( FN - C(J2)*Q(J1) - A(J2)*Q(J2-1))/(C(J2)*S(J1) +        
     1     A(J2)*S(J2-1)  +B(J2))                                       
C                                                                       
C         BACKWARD ELIMINATION PASS                                     
C                                                                       
      DO 12 I=JA,J2                                                     
         J = JJ -I                                                      
         F(J) = F(J2)*S(J) + Q(J)                                       
12    continue                                                          
      RETURN                                                            
      END                                                               
