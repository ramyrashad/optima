c     ------------------------------------------------------------------
c     -- solve flowfield --
c     -- 1. approximate-factorization solver --
c     -- 2. newton-krylov solver --
c     -- m. nemec, may 2001 --
c     ------------------------------------------------------------------
      subroutine flow_solve (ifirst, nmax, indx, q, qps,qos,xy,xyj,x,y,
     &     turmu,vort,turre,turps,turos,fmu,press,precon,sndsp,xit,ett, 
     &     ds,vk,ve,coef2x,coef4x, coef2y, coef4y, s, uu, vv, ccx, ccy,
     &     spectxi, specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja, icol)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"
#include "../include/scratch.inc"
#include "../include/turb.inc"

      integer ifirst,nmax,ib,lg,ip,jdim,kdim,junit,j,ntvar,mglev,ifoil
      integer ibn,iseg,is

      double precision q(*),             press(*),       sndsp(*)
      double precision s(*),             xy(*),          xyj(*)
      double precision xit(*),           ett(*),         ds(*)
      double precision x(*),             y(*),           turmu(*)
      double precision vort(*),          turre(*),       fmu(*)
      double precision vk(*),            ve(*),          tmp(*)
      double precision uu(*),            vv(*),          ccx(*)
      double precision ccy(*),           coef2x(*),      coef2y(*)
      double precision coef4x(*),        coef4y(*),      precon(*)
      double precision spectxi(*),       specteta(*)
      double precision wk1(*)
cmt
      double precision qps(*), qos(*), turps(*), turos(*)

      integer ia(*), ja(*), ipa(*), jpa(*), icol(*), indx(*)

      double precision dtiseq(-5:20),dtmins(-5:20),dtspiseq(-5:20)
      double precision vlx(-5:20),vnx(-5:20),vly(-5:20),vny(-5:20)
      integer iends(-5:20),jacdtseq(-5:20),isbiter(-5:20,2)      
      integer isequal
      common/mesup/ dtiseq, dtmins,dtspiseq, vlx, vnx, vly, vny, 
     &        isequal, iends, jacdtseq, isbiter


      if (iends(1).gt.0) then

c     ---------------
c     -- AF solver --
c     ---------------

         write (n_out,10) fsmach, alpha
 10      format (/3x,'Computing flow solution: M =',f6.3,', AOA =',f7.3)
         write (n_out,20) 
 20      format (3x,'<< Starting A-F iterations >>')
         call flush(n_out)

         
         call afsolve(ifirst, q,qos, xy, xyj, x, y,turmu,vort,
     &        turre,turos,fmu, precon, press, sndsp, xit, ett,
     &        ds, vk, ve,coef2x, coef4x, coef2y, coef4y, s, uu, vv, ccx,
     &        ccy,spectxi,specteta, tmp, wk1, wk1(maxjk*4+1),
     &        wk1(2*maxjk*4+1),nmax)

c     -- detection of negative Jacobian for optimization runs --
         if (badjac) return         

         write (n_out,30) resid, totime1, numiter
 30      format (3x,'A-F residual:',e10.3,3x,'Run-time:',f7.2,'s',3x,
     &        'Iterations:',i7 )
         call flush(n_out)

      else

c     --------------------------------
c     -- no AF solver initialization--
c     --------------------------------

c     - initialize variables
         call initia(1,ifirst,maxj,maxk,q,qos,press,sndsp,s,xy,xyj,
     &        xit,ett,ds,x,y,turmu,fmu,vort,turre,turos,vk,ve,coef2x,
     &        coef4x,coef2y,coef4y,precon,nmax)

c     -calculate grid aspect ratios
         call asprat(x,y,.true.)

         if (restart) iend = istart
          
         if ( viscous .and. turbulnt ) then
     
c     -- update distances for the s-a model (array smin) --
            call autostan( 1, 1, maxj, maxk, x, y, d, d(1,2), .false.)
            do ib=1,nblks
               lg = lgptr(ib)
               call caldist(jmax(ib), kmax(ib), x(lg), y(lg), smin(lg),
     &              jlow(ib), jup(ib), klow(ib), kup(ib),icoord,xcoord,
     &              ycoord)
            end do


         end if   !turbulnt?

      end if


c     ----------------
c     -- N-K solver --
c     ----------------

      if ( (unsteady .and. nkiends .gt. 0) 
     &     .or. (.not.unsteady .and.nkits.gt.0) ) then

cmpr  call hallo_q in case we used afsolve before calling nksolve
         if (.not. unsteady .and. iends(1).gt.0) then   
            call halo_q(1,nblks,maxj,maxk,q,turre)
         end if

         dt = 1.0d0
         dt2 = dtiseq(1)

         if (.not.unsteady) print *,'Starting N-K iterations'

         write (n_out,40)
 40      format (3x,'<< Starting N-K iterations >>')

         call nksolve( nmax, ntvar, q, qps, qos, xy, xyj,x,
     &        y,turmu,vort,turre,turps,turos,fmu,precon,press,sndsp,
     &        xit, ett, ds, vk, ve, coef2x, coef4x, coef2y, coef4y, s,
     &        uu,vv, ccx, ccy, spectxi, specteta, tmp, indx, ipa, jpa,
     &        ia, ja, icol)

      end if

      write (n_out,50) resid, totime1
 50   format (3x,'Flow solve converged to',e9.2,' in',e11.4,
     &     ' seconds.')
      call flush(n_out)

c     -----------------------------
c     -- final flow solve output --
c     -----------------------------

      jdim = maxj
      kdim = maxk

c     -- store force and moment coefficients
      ip = 12
      junit=1

      if (nkits.eq.0) then
         do j=1,6
            backspace(n_ld)
         end do
      end if
      call ioall(junit, mglev, ip, jdim, kdim, q,qos, press,
     &     sndsp, turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &     coef2x, coef4x, coef2y, coef4y)

c     -- store elemental force and moment coefficients --
      ip = 11
      junit = 1
      call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,
     &     sndsp, turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &     coef2x, coef4x, coef2y, coef4y)

c     -- write out formatted cp data --
      ip = 6
      call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,
     &     sndsp, turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &     coef2x, coef4x, coef2y, coef4y)

c     -- viscous parameter output --
      if(viscous) then
         ip = 14
         call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,sndsp, 
     &        turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &        coef2x, coef4x, coef2y, coef4y)
      endif

c     -- storing of binary solution data --
      if (store) then
         ip = 10
         call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,sndsp, 
     &        turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &        coef2x, coef4x, coef2y, coef4y)
      endif

c     -- output metrics --
      if (writemets) then
         ip = 22
         call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,sndsp, 
     &        turmu, fmu, vort, turre,turos, vk, ve, xy, xyj, x, y,
     &        coef2x, coef4x, coef2y, coef4y)
      endif

c     --output turbulence quantities --
      if (viscous.and.turbulnt) then
         ip = 23
         call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,sndsp, 
     &        turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y, 
     &        coef2x, coef4x, coef2y, coef4y)
      endif

c     -- output dissipation quantities --
      if (writedisp) then
         ip = 24
         call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,sndsp, 
     &        turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &        coef2x, coef4x, coef2y, coef4y)
      endif
      
c     -- write out target cp distribution --
      if (ioptm.eq.1) then
         ip = 7
         junit = 0
         call ioall(junit, mglev, ip, jdim, kdim, q, qos,press,sndsp, 
     &        turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &        coef2x, coef4x, coef2y, coef4y)
      else 
c     -- remove jacobian scaling from q vector --
         if (unsteady) then
            do ib = 1,nblks
             call qmuj( jmax(ib),kmax(ib),qos(lqptr(ib)),xyj(lgptr(ib)),
     &              jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &              kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
            end do
         end if
         do ib = 1,nblks
            call qmuj( jmax(ib), kmax(ib), q(lqptr(ib)), xyj(lgptr(ib)),
     &           jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &           kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
         end do

c     -- extract cp distribution for inverse design --
         if (iobjf.eq.1 .and. ioptm.gt.2 .and. ioptm.ne.6 ) then
            do ifoil = 1,nfoils
               ibn = 1
               do iseg = 1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  call getcp(ibn, jmax(ib), kmax(ib), jminbnd(ib),
     &                 jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), ifoil, is,
     &                 press(lgptr(ib)), xyj(lgptr(ib)))
               end do
            end do
         end if

         restart = .false.
      end if

      return
      end                       !flow_solve
