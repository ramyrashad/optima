c----------------------------------------------------------------------
c     -- differentiation of production and destruction terms for the 
c     modified S-A model -- 
c     written by: marian nemec
c     modified by Markus Rumpfkeil
c     modified by Ramy Rashad (August 2010)
c     date: April. 2006
c----------------------------------------------------------------------

      subroutine dsa_pd(jdim, kdim, q, xy, xyj, x, y, fmu, bm2, bm1, db,
     &      bp1, bp2, chi, dchi,fv1, dfv1,  wk1, wk2, wk3, vort, dprod,
     &      ddest, dsxip, dsxim, dsetap, dsetam)

      implicit none
      
#include "../include/arcom.inc"
#include "../include/sam.inc"

      integer jdim, kdim,j,k,jp1,jm1,kp1,km1,n

      double precision cb,cw,g,g6,rinv,fw,r,ts,ts2,fv2,tnu,t1,ri
      double precision ujm1,ujp1,ukm1,ukp1,vjm1,vjp1,vkm1,vkp1,e_dfw

      double precision q(jdim,kdim,5), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim), fmu(jdim,kdim)
      double precision chi(jdim,kdim), dchi(5,jdim,kdim), fv1(jdim,kdim)
      double precision dfv1(jdim,kdim), chi2

      double precision bm2(5,jdim,kdim), bm1(5,jdim,kdim)
      double precision db(5,jdim,kdim), bp1(5,jdim,kdim)
      double precision bp2(5,jdim,kdim),fv3(jdim,kdim)

      double precision wk1(jdim,kdim,3), wk2(jdim,kdim,3)
      double precision wk3(jdim,kdim), vort(jdim,kdim)

      double precision dprod(5,jdim,kdim), ddest(5,jdim,kdim) 
      double precision dsxip(5,jdim,kdim), dsxim(5,jdim,kdim)
      double precision dsetap(5,jdim,kdim), dsetam(5,jdim,kdim)
      double precision ft2store(jdim,kdim)

      double precision ft2, t2, t3, t4

c     -- local arrays --
      double precision dchifv1(5),dfv2(5),dfv3(5),dts(5)
      double precision dr(5),dg(5),dfw(5)
      double precision dft2(5)

c     -- Initialize 'fv3' array !!!!

      do j = 1,jdim
         do k = 1, kdim
            fv3(j,k) = 0.0
         end do
      end do

c     -- calculate vorticity without absolute value and cut-off --
      do k = klow,kup
        kp1 = k+1
        km1 = k-1
        do j = jlow,jup
          jp1 = jplus(j)
          jm1 = jminus(j)
          ujm1 = q(jm1,k,2)/q(jm1,k,1)
          ujp1 = q(jp1,k,2)/q(jp1,k,1)
          ukm1 = q(j,km1,2)/q(j,km1,1)
          ukp1 = q(j,kp1,2)/q(j,kp1,1)
          vjm1 = q(jm1,k,3)/q(jm1,k,1)
          vjp1 = q(jp1,k,3)/q(jp1,k,1)
          vkm1 = q(j,km1,3)/q(j,km1,1)
          vkp1 = q(j,kp1,3)/q(j,kp1,1)

          vort(j,k) = 0.5d0*( (vjp1-vjm1)*xy(j,k,1) + (vkp1-vkm1)
     &          *xy(j,k,3) - (ujp1-ujm1)*xy(j,k,2) - (ukp1-ukm1)
     &          *xy(j,k,4) )
        end do
      end do

c     -- off - diagonal entries only from vorticity --
c     -- wk1 holds d(u)/d(Q^) --
c     -- wk2 holds d(v)/d(Q^) --
      if (.not. frozen .or. ( frozen .and. ifrz_what.eq.2 ) ) then
         do k = kbegin,kend
            do j = jbegin,jend      

               ri = 1.d0/q(j,k,1)

               wk1(j,k,1) = - 0.5d0*q(j,k,2)*ri**2
               wk1(j,k,2) = 0.5d0*ri
               wk1(j,k,3) = 0.d0             

               wk2(j,k,1) = - 0.5d0*q(j,k,3)*ri**2
               wk2(j,k,2) = 0.d0
               wk2(j,k,3) = 0.5d0*ri
               
            end do
         end do
      else
         do n=1,3 
            do k = kbegin,kend
               do j = jbegin,jend 
                  wk1(j,k,n) = 0.d0
                  wk2(j,k,n) = 0.d0
               end do
            end do
         end do
      end if

c     -- d(S)/d(Q^) --
c     -- special treatment due to absolute value and constant in
c     vorticity expression -- 
        do k = klow,kup
          kp1 = k+1
          km1 = k-1
          do j = jlow,jup 
             if ( dabs(vort(j,k)) .le. 8.5d-10 ) then
                do n = 1,3
                   dsxip(n,j,k) = 0.d0
                   dsxim(n,j,k) = 0.d0
                   dsetap(n,j,k)= 0.d0
                   dsetam(n,j,k)= 0.d0
                end do
             else if ( vort(j,k) .lt. - 8.5d-10 ) then
                jp1 = jplus(j)
                jm1 = jminus(j)
                do n = 1,3
                   dsxip(n,j,k) =-wk2(jp1,k,n)*xy(j,k,1)+
     &                            wk1(jp1,k,n)*xy(j,k,2)
                   dsxim(n,j,k) = wk2(jm1,k,n)*xy(j,k,1)-
     &                            wk1(jm1,k,n)*xy(j,k,2)
                   dsetap(n,j,k)=-wk2(j,kp1,n)*xy(j,k,3)+
     &                            wk1(j,kp1,n)*xy(j,k,4)
                   dsetam(n,j,k)= wk2(j,km1,n)*xy(j,k,3)-
     &                            wk1(j,km1,n)*xy(j,k,4)                   
                end do
             else        
                jp1 = jplus(j)
                jm1 = jminus(j)
                do n = 1,3
                   dsxip(n,j,k) = wk2(jp1,k,n)*xy(j,k,1)-
     &                            wk1(jp1,k,n)*xy(j,k,2)
                   dsxim(n,j,k) =-wk2(jm1,k,n)*xy(j,k,1)+
     &                            wk1(jm1,k,n)*xy(j,k,2)
                   dsetap(n,j,k)= wk2(j,kp1,n)*xy(j,k,3)-
     &                            wk1(j,kp1,n)*xy(j,k,4)
                   dsetam(n,j,k)=-wk2(j,km1,n)*xy(j,k,3)+
     &                            wk1(j,km1,n)*xy(j,k,4)
                end do
             end if ! dabs(vort)
          end do ! j
       end do ! k

c     -- production and destruction terms --
c     -- note: storing factors for the off-diagonal terms from vorticity
c     due to the destruction term in wk3 --

      cb = -cb1/re
      cw = cw1/re
      e_dfw = -7.d0/6.d0

      do k = klow,kup
        do j = jlow,jup

          tnu = q(j,k,5)*xyj(j,k)

          if (.not. originalSA) then
          
c         -- t1 = 1/(1+chi/cv2) --
             t1   = 1.d0/( 1.d0+chi(j,k)/cv2 )
             fv2 = t1**3
c         -- d(fv2)/d(Q^) --
             do n = 1,5
                dfv2(n) = -3.d0*(t1**4)*dchi(n,j,k)/cv2          
             end do
             fv3(j,k)=(1+chi(j,k)*fv1(j,k))*(1-fv2)/chi(j,k)

c         -- d(chi.fv1)/d(Q^) --
             do n = 1,5
                dchifv1(n)=( chi(j,k)*dfv1(j,k) + fv1(j,k) )*dchi(n,j,k)
             end do
c         -- d(fv3)/d(Q^) --
             do n = 1,5
                dfv3(n)=( ( dchifv1(n)*(1-fv2) - (1+chi(j,k)*fv1(j,k))*
     &               dfv2(n) )*chi(j,k) - (1+chi(j,k)*fv1(j,k))*
     &               (1-fv2)*dchi(n,j,k)  ) / chi(j,k)**2  
             end do
          
c         -- t1 = 1/(k**2.d**2) --
             t1 = (akarman*smin(j,k))**(-2)

c         -- S_tilde --
             ts = max(dabs(vort(j,k)),8.5d-10)*re*fv3(j,k) + tnu*t1*fv2

c         -- d(s_tilde)/d(Q^) --
c         -- vorticity (S) only contributes off-diagonal entries --
             do n = 1,5
                dts(n)=t1*tnu*dfv2(n)+
     &               max(dabs(vort(j,k)),8.5d-10)*re*dfv3(n)
             end do
             dts(5) = dts(5) + t1*fv2*xyj(j,k)

          else ! (originalSA)
          
c         -- t1 = 1/(1+chi*fv1) --
             t1   = 1.d0/( 1.d0+chi(j,k)*fv1(j,k) )
             fv2  = 1.d0 - chi(j,k)*t1

c         -- d(chi.fv1)/d(Q^) --
             do n = 1,5
                dchifv1(n) = (chi(j,k)*dfv1(j,k) + fv1(j,k))*dchi(n,j,k)
             end do

c         -- d(fv2)/d(Q^) --
             do n = 1,5
                dfv2(n) = - ( (1.d0 + chi(j,k)*fv1(j,k))*dchi(n,j,k) -
     &               chi(j,k)*dchifv1(n) )*t1*t1 
             end do
          
c         -- t1 = 1/(k**2.d**2) --
             t1 = 1.d0/(akarman*smin(j,k))**2

c         -- S_tilde --
             ts = max(dabs(vort(j,k)),8.5d-10 )*re + tnu*t1*fv2

c         -- d(s_tilde)/d(Q^) --
c         -- vorticity (S) only contributes off-diagonal entries --
             do n = 1,5
                dts(n) = t1*tnu*dfv2(n)
             end do
             dts(5) = dts(5) + t1*fv2*xyj(j,k)

          end if ! (.not.originalSA)

c         -- d(ft2)/d(Q^)          
          if (transitionalFlow) then
             chi2 = chi(j,k)*chi(j,k)
             ft2 = ct3*exp(-ct4*chi2)
             do n = 1,5
                dft2(n) =-ct4*2.d0*ft2*dchi(n,j,k)*chi(j,k)
             end do 
          else
             do n=1,5
                dft2(n) = 0.d0
             end do
          end if 

          r = tnu*t1/ts          

          if (dabs(r).ge.10.d0) then

            fw = const
c        -- dfw/d(Q^) --
            do n = 1,5
               dfw(n) = 0.d0
            end do
            wk3(j,k) = 0.d0

         else

            g = r + cw2*(r**6-r)
            fw = g*( (1.d0+cw3_6)/(g**6+cw3_6) )**expon 
            
c        -- dr/d(Q^) --
            ts2 = ts**2
            do n = 1,5
               dr(n) = - tnu*t1*dts(n)/ts2
            end do
            dr(5) = dr(5) + t1*xyj(j,k)/ts
            wk3(j,k) = - tnu*t1/ts2

c        -- dg/d(Q^) --
            t1 = 1.d0 + cw2*(6.d0*r**5 - 1.d0)
            do n = 1,5
              dg(n) = dr(n)*t1
            end do
            wk3(j,k) = wk3(j,k)*t1

c        -- dfw/d(Q^) --
            g6 = g**6
            t1 = const*( (g6+cw3_6)**(-expon) - g6*(g6+cw3_6)**e_dfw )
            do n = 1,5
               dfw(n) = dg(n)*t1
!               if (j.eq.250 .and. k.eq.10) then
!                  write(scr_unit,*) 'dgn = ', dg(n)
!                  write(scr_unit,*) 't1 = ', t1
!               end if 
            end do
            wk3(j,k) = wk3(j,k)*t1

         endif
                    
cjd
c     -- node j,k production and destruction terms --
c     -----------------------------------------------

         ! ______________________________________
         ! Transitional flow using trip functions
         if (transitionalFlow) then

c           For transition we retain the ft2 term in the differentiation
c           since it is not zero

c           -- node j,k production terms --
            
            ! d(prod)/dQ = Term_1 + Term_2 + Term_3
            ! where: Term_1 =  cb * (1-ft2)  * nu_tilde * d(S_tilde)/dQ
            !        Term_2 =  cb * (1-ft2)  * S_tilde  * d(nu_tilde)/dQ
            !        Term_3 = -cb * nu_tilde * S_tilda  * d(ft2)/dQ

            t1 = cb*q(j,k,5)
            do n = 1,4
               dprod(n,j,k)=t1*((1-ft2)*dts(n)-ts*dft2(n))
            end do

            ! RR: The following is what J. Driver used. I believe
            ! that it is incorrect and I'll replace it for
            ! now. Perhaps it could be verified with a complex step
            ! derivative.

!               dprod(5,j,k)=cb*((1-ft2)*(q(j,k,5)*dts(5)+ts)
!     &              - q(j,k,5)*(ft2*dts(5)-ts*dft2(5)))

            ! RR: This is the correct analytical dprod(5)/dq(5):

            dprod(5,j,k)=cb*((1-ft2)*(q(j,k,5)*dts(5) + ts)
     &           - q(j,k,5)*ts*dft2(5))
            
c           -- node j,k destruction terms --

            rinv = 1.d0/xyj(j,k)
            t1 = (tnu/smin(j,k))**2
            t2 = (1.d0/smin(j,k))**2
            t3 = 1.d0/(akarman*smin(j,k))**2
            t4 = q(j,k,5)**2
            do n = 1,4
               ddest(n,j,k) = xyj(j,k)*t4*t2*(cw*dfw(n)
     &              +(cb/(akarman**2))*dft2(n))
            end do
            ddest(5,j,k)= xyj(j,k)*(cw*t2)*
     &           (fw*2.d0*q(j,k,5)+ t4*dfw(5))+
     &           xyj(j,k)*(cb*t3)*(ft2*2.d0*q(j,k,5)+ 
     &           t4*dft2(5))

c           NOTE: cb = -cb1/re
c           NOTE: cw = +cw1/re

c           -- store ft2 vector for jacobian arrays
            ft2store(j,k) = ft2
         
         ! _______________________________________________
         ! Fully turbulent flow: ft1 and ft2 are both zero
         else

            t1 = cb*q(j,k,5)
            do n = 1,5
               dprod(n,j,k) = t1*dts(n)
            end do
            dprod(5,j,k) = dprod(5,j,k) + cb*ts
            
            rinv = 1.d0/xyj(j,k)
            t1 = (tnu/smin(j,k))**2         

!            if (j.eq.250 .and. k.eq.10) then
!               write(scr_unit,*) 'tnu  = ', tnu
!               write(scr_unit,*) 'smin =', smin(j,k)
!               write(scr_unit,*) 'rinv =', rinv
!               write(scr_unit,*) 'cw   =', cw
!               write(scr_unit,*) 't1   =', t1
!            end if 
            do n = 1,5 
               ddest(n,j,k) = rinv*cw*t1*dfw(n)
!               if (j.eq.250 .and. k.eq.10) then
!                  write(scr_unit,*) 'dfw = ', dfw(n)
!                  write(scr_unit,*) 'ddest = ', ddest(n,j,k)
!               end if 
            end do
            ddest(5,j,k) = ddest(5,j,k) + 
     &           rinv*cw*fw*2.d0*xyj(j,k)*t1/tnu

         end if ! (transitionalFlow)

         wk3(j,k) = wk3(j,k)*rinv*cw*t1
cjd/

      end do
      end do

c     -- add result to jacobian arrays --


      do k = klow,kup
         do j = jlow,jup

            !_______________________________
            ! Add off-diagonal contributions

            ! Compute different "t1" multipliers based on the 
            ! following two input parameters:
            ! 1) transitional/fully-turbulent flow, AND
            ! 2) original/ashford SA model

            ! For the original SA model:
            if (transitionalFlow) then 
               t1 = re*(q(j,k,5)*cb*(1-ft2store(j,k)) + wk3(j,k))
            else
               t1 = re*(q(j,k,5)*cb + wk3(j,k))
            end if
            ! For the ashford modification, multiply t1 by fv3:
            if (.not.originalSA) then
               t1 = fv3(j,k)*t1
            end if

            do n = 1,3
               bm2(n,j,k) = bm2(n,j,k) + dsxim(n,j,k)*t1 
               bm1(n,j,k) = bm1(n,j,k) + dsetam(n,j,k)*t1
               bp1(n,j,k) = bp1(n,j,k) + dsetap(n,j,k)*t1
               bp2(n,j,k) = bp2(n,j,k) + dsxip(n,j,k)*t1
            end do

            ! __________________________
            ! Add diagonal contributions
            do n = 1,5              
!               if (j.eq.250 .and. k.eq.10) then
!                  write(scr_unit,*) 'n =', n
!                  write(scr_unit,*) 'db = ', db(n,j,k)
!                  write(scr_unit,*) 'dprod = ', dprod(n,j,k)
!                  write(scr_unit,*) 'dfw = ', dfw(n)
!                  write(scr_unit,*) 'ddest = ', ddest(n,j,k)
!               end if 
               db(n,j,k)  = db(n,j,k) + dprod(n,j,k)+ ddest(n,j,k) 
            end do

         end do ! k-loop
      end do ! j-loop

      return                                                            
      end                       !dsa_pd
