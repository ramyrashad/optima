c---------------------------------------------------------------------
c     -- This program will sort a 4x4 matrix
c     -- Row-swap based on the strongest diagonal entry
c     -- Based on a subroutine for 3d solver by J.Nichols
c     -- Written by: Howard Buckley July 2010
c---------------------------------------------------------------------
      subroutine ibc_swap (j, jdim, k, kdim, cc, c1, c2, xyj)

      implicit none

#include "../include/arcom.inc"

      integer 
     &     n3, n1, row, col, count, row1, col1, j, k, ix, iy, b1,
     &     cswap(4,4), colsum(4), rowsum(4), indx(jdim,kdim), 
     &     iex(jdim,kdim,4), itmp, jdim, kdim

      double precision 
     &     cc(4,4), c1(4,4), c2(4,4), tol, xyj(jdim,kdim)

      tol = 1.0d-2

c     -- Convert cc matrix to an integer matrix (1 to 4)
c     Rank the numbers in each column (1 is the largest value)

      do col = 1,4
         do row = 1,4
            count = 4
            do n1 = 1,4
               if (dabs(cc(row,col)).ge.dabs(cc(n1,col)).and.
     &                                            (n1.ne.row)) then
                  count = count - 1
               end if
            end do
            if (dabs(cc(row,col)/xyj(j,k)).lt.tol) then
               cswap(row,col) = 0
            else
               cswap(row,col) = count
            end if
         end do
      end do

c     -- check if row entries are equal --
      do col = 1,4
         do row = 1,4
            do n1 = 1,4
               if ((cswap(row,col).eq.cswap(n1,col)).and.
     &              (n1.ne.row).and.(cswap(n1,col).ne.0)) then
                  cswap(n1,col) = cswap(n1,col) + 1
               end if
            end do
         end do
      end do

c     -- Create a column sum vector
      do col = 1,4
         count = 0
         do row = 1,4
            count = count + cswap(row,col)
         end do
         colsum(col) = count
      end do

c     -- Create a row sum vector
      do row = 1,4
         count = 0
         do col = 1,4
            count = count + cswap(row,col)
         end do
         rowsum(row) = count
      end do
      
c$$$      if ((j.eq.1).and.(k.eq.1).and.(m.eq.1)) then
c$$$         write(*,*)'after forming integer matrix'
c$$$         do col = 1,5
c$$$            do row = 1,5
c$$$               write(*,*)'row,col,cswap',row,col,cswap(row,col)
c$$$            end do
c$$$            write(*,*)'colsum',colsum(col)
c$$$         end do
c$$$c         stop
c$$$      end if

      
c     -- Begin sort alogorithm --
c     -- Swap rows with only one entry -- using column sum
      do col = 1,4
         if (colsum(col).eq.1) then
            if (cswap(col,col).eq.0) then
               do row = 1,4
                  if (cswap(row,col).eq.1) then
                     ix = row
                     iy = col
                     do n3 =1,4
                        b1 = cc(ix,n3)
                        cc(ix,n3) = cc(iy,n3)
                        cc(iy,n3) = b1
                        b1 = c1(ix,n3)
                        c1(ix,n3) = c1(iy,n3)
                        c1(iy,n3) = b1
                        b1 = c2(ix,n3)
                        c2(ix,n3) = c2(iy,n3)
                        c2(iy,n3) = b1
                        b1 = cswap(ix,n3)
                        cswap(ix,n3) = cswap(iy,n3)
                        cswap(iy,n3) = b1
                     end do
                     itmp = iex(j,k,ix)
                     iex(j,k,ix) = iex(j,k,iy)
                     iex(j,k,iy) = itmp
                  end if
               end do
            end if
         end if
      end do

c     -- Swap rows with only one entry -- using row sum
      do row = 1,4
         if (rowsum(row).eq.1) then
            if (cswap(row,row).eq.0) then
               do col = 1,4
                  if (cswap(row,col).eq.1) then
                     ix = row
                     iy = col
                     do n3 =1,4
                        b1 = cc(ix,n3)
                        cc(ix,n3) = cc(iy,n3)
                        cc(iy,n3) = b1
                        b1 = c1(ix,n3)
                        c1(ix,n3) = c1(iy,n3)
                        c1(iy,n3) = b1
                        b1 = c2(ix,n3)
                        c2(ix,n3) = c2(iy,n3)
                        c2(iy,n3) = b1
                        b1 = cswap(ix,n3)
                        cswap(ix,n3) = cswap(iy,n3)
                        cswap(iy,n3) = b1
                     end do
                     itmp = iex(j,k,ix)
                     iex(j,k,ix) = iex(j,k,iy)
                     iex(j,k,iy) = itmp
                  end if
               end do
            end if
         end if
      end do

c$$$      if ((j.eq.1).and.(k.eq.1).and.(m.eq.1)) then     
c$$$         write(*,*)'after moving single rows'
c$$$         do col = 1,5
c$$$            do row = 1,5
c$$$               write(*,*)'row,col,cswap',row,col,cswap(row,col)
c$$$            end do
c$$$            write(*,*)'colsum',colsum(col)
c$$$         end do
c$$$c         stop
c$$$      end if


      do col = 1,4
         if (cswap(col,col).eq.0) then
            do row = 1,4
               if ((cswap(row,col).ne.0).and.(cswap(col,row).ne.0)) then
                  ix = row
                  iy = col
                  do n3 =1,4
                     b1 = cc(ix,n3)
                     cc(ix,n3) = cc(iy,n3)
                     cc(iy,n3) = b1
                     b1 = c1(ix,n3)
                     c1(ix,n3) = c1(iy,n3)
                     c1(iy,n3) = b1
                     b1 = c2(ix,n3)
                     c2(ix,n3) = c2(iy,n3)
                     c2(iy,n3) = b1
                     b1 = cswap(ix,n3)
                     cswap(ix,n3) = cswap(iy,n3)
                     cswap(iy,n3) = b1
                  end do
                  itmp = iex(j,k,ix)
                  iex(j,k,ix) = iex(j,k,iy)
                  iex(j,k,iy) = itmp
                  goto 1
               end if
            end do
c     -- if no solution is found, print matrix to screen --
            write(*,*)'no solution ibc_swap',j,k
            write(*,*)'after sort'       
            do row1 = 1,4
               do col1 = 1,4
                  write(*,*)'row,col,cswap',row1,col1,cswap(row1,col1)
               end do
               write(*,*)'colsum',colsum(row1)
            end do

         end if
 1       continue
      end do

c      if ((j.eq.1).and.(k.eq.1).and.(m.eq.1)) then  
c$$$         write(*,*)'after sort'       
c$$$         do col = 1,5
c$$$            do row = 1,5
c$$$               write(*,*)'row,col,cswap',row,col,cswap(row,col)
c$$$            end do
c$$$            write(*,*)'colsum',colsum(col)
c$$$         end do
c         stop
c      end if

      return
      end                       !ibc_swap

