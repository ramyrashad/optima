c     -- output grid, block-by-block --
c     -- m. nemec, may 2001 --

      subroutine writeGRD (junit, jdim, kdim, x, y)

      implicit none

      integer junit, jdim, kdim, j, k

      double precision x(jdim,kdim), y(jdim,kdim)

      write(junit) ((x(j,k),j=1,jdim),k=1,kdim),
     &     ((y(j,k),j=1,jdim),k=1,kdim) 

      return 
      end                       !writeGRD
