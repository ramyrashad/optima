program ut_hybropt
#ifdef _MPI_VERSION
   use mpi
#endif

   implicit none

   integer :: ii, jj, ndv, nfnc, nnzLinG, nnzGrad, ppc, popsize, &
              mycommid, nProc, ierr, myrank, hybr_meth, COMM_CURRENT, &
              ranseed, selmethod, mingen, maxgen, &
              datevals(3), sn_maj_lim, flow_sol_lim, sample_offset, &
              opt_def_unit, myrank_glob, i, opt_info_unit, &
              opt_his_unit, file_stat, optf_output, dpfile_unit, &
              sampl_seq_unit

   integer, allocatable :: &
      iLfun(:), jLvar(:), iGfun(:), jGvar(:), &
      commarr(:)

   double precision :: infBnd, prob_pert, prob_mut, penalty, &
                       prob_avg, xbest, xavg, xpert, xmut, &
                       feas_tol, opt_tol, lb_tol

   double precision, allocatable :: &
      X(:), Xlow(:), Xupp(:), Xmul(:), Fnc(:), Flow(:), Fupp(:), &
      Fmul(:), LinG(:), Grad(:), Xso(:)

   character(len=128) :: sobol_file

   logical usedb, file_exists, cig, csampl

   external :: hybrOptA, husrfun    

   !-- INPUT PARAMETERS --

   infBnd = 1.1d20
   ranseed = 729126
   hybr_meth = 2
   xmut =  (4.0d0 / 16.0d0) * 100.0d0
   xpert = (4.0d0 / 16.0d0) * 100.0d0
   popsize = 100
   xbest = (2.0d0 / 16.0d0) * 100.0d0
   xavg  = (6.0d0 / 16.0d0) * 100.0d0
   sn_maj_lim = 1000
   flow_sol_lim = 2000
   ppc = 1
   usedb = .false.
   sample_offset = 0
   mingen = 1
   maxgen = 100000
   selmethod = 2
   prob_pert = 0.5d0
   prob_mut = 0.5d0
   prob_avg = 1.0d0
   penalty = 1.0d2
   lb_tol = 1.0d0
   cig = .false.
   optf_output = 131
   opt_info_unit = 1234
   opt_his_unit = 1235
   opt_def_unit = 1236
   dpfile_unit = 1237
   sampl_seq_unit = 1238
   sobol_file = 'new-joe-kuo-6.21201'
   csampl = .false.

   !-- GET RANSEED --
!   open (unit=234423, file='rand', status='old', form='formatted', iostat=file_stat)
!   read (234423, *) ranseed
!   close(234423)

   !-- INITIALIZE MPI --

   call mpi_init(ierr)

   COMM_CURRENT = MPI_COMM_WORLD
   call mpi_comm_size(COMM_CURRENT, nProc, ierr)
   call mpi_comm_rank(COMM_CURRENT, myrank, ierr)

   myrank_glob = myrank

   allocate(commarr(popsize))
   mycommid = floor(myrank / dble(ppc)) + 1
   do ii = 1, popsize
      call mpi_comm_split(COMM_CURRENT, mycommid, &
                          ii, commarr(ii), ierr)
   end do
   do ii = 1, popsize
      call mpi_comm_rank(commarr(ii), myrank, ierr)
      call mpi_comm_size(commarr(ii), nProc, ierr)
   end do
   COMM_CURRENT = commarr(mycommid)

   !-- OPTIMIZATION PROBLEM --

   ndv = 2
   nfnc = 4
   nnzLinG = 4
   nnzGrad = 2

   allocate(X(ndv), Xlow(ndv), Xupp(ndv), Xmul(ndv), &
            Fnc(nfnc), Flow(nfnc), Fupp(nfnc), Fmul(nfnc), &
            iLfun(nnzLinG), jLvar(nnzLinG), LinG(nnzLinG), &
            iGfun(nnzGrad), jGvar(nnzGrad), Grad(nnzGrad), &
            Xso(ndv))

   X(:)    =  1.0d0
   Xlow(1) =  0.0d0
   Xupp(1) =  1.0d0
   Xlow(2) =  0.0d0
   Xupp(2) =  1.0d0
   Xmul(:) =  0.0d0

   do i = 1, ndv
      Xso(i) = i
   end do

   Flow(:) = -infBnd
   Fupp(:) =  infBnd
   Fmul(:) =  0.0d0

   iLfun(1) = 2
   jLvar(1) = 1
   LinG(1) =  1.0d0
   Flow(2) = 0.3d0
   Fupp(2) = 0.7d0

   iLfun(2) = 3
   jLvar(2) = 2
   LinG(2) = 1.0d0
   Flow(3) = -infBnd
   Fupp(3) = 0.8d0

   iLfun(3) = 4
   jLvar(3) = 1
   LinG(3) = 0.5d0
   iLfun(4) = 4
   jLvar(4) = 2
   LinG(4) = 1.0d0
   Flow(4) = 0.45d0
   Fupp(4) = infBnd

#if 0
   iLfun(2) = 3
   jLvar(2) = 1
   LinG(2) =  0.5d0
   iLfun(3) = 3
   jLvar(3) = 2
   LinG(3) =  1.0d0
   Flow(3) = 0.45d0
   Fupp(3) = infBnd

   iLfun(4) = 4
   jLvar(4) = 2
   LinG(4) =  1.0d0
   Flow(4) = -infBnd
   Fupp(4) = 0.8d0

   iLfun(5) = 5
   jLvar(5) = 1
   LinG(5) =  0.5d0
   iLfun(6) = 5
   jLvar(6) = 2
   LinG(6) =  1.0d0
   Flow(5) = -infBnd
   Fupp(5) = 0.75d0

   iLfun(7) = 6
   jLvar(7) = 1
   LinG(7) =  3.0d0
   iLfun(8) = 6
   jLvar(8) = 2
   LinG(8) =  1.0d0
   Flow(6) = -infBnd
   Fupp(6) = 2.0d0
#endif

   iGfun(1) = 1
   iGfun(2) = 1
   jGvar(1) = 1
   jGvar(2) = 2

   !-- WRITE OPTIMIZATION DEFINITION FILE --

   inquire(file='optimization.def', exist=file_exists)
   call mpi_barrier(MPI_COMM_WORLD, ierr)

   if (myrank_glob == 0 .and. (file_exists)) then
      write(*,*) 'INFO: Optimization problem already defined'
   endif 

   if (myrank_glob == 0 .and. (.not. file_exists)) then

      write(*,*) 'INFO: Creating optimization definition file'
      open (unit=opt_def_unit, file='optimization.def', &
            status='new', form='formatted', iostat=file_stat)
      if (file_stat /= 0) then
         write(*,*) myrank_glob, &
             ': Error in Optimize:initHybrOptimize :: ', &
             'error opening optimization.dat file '
        call mpi_abort(COMM_CURRENT,0,ierr)

      endif

      !-- Write nX, nFnc, nnzLinG, nnzGrad
      write(opt_def_unit, *) ndv, nfnc, nnzLinG, nnzGrad

      !-- Write Xlow, X, Xupp, Xmul, Xso
      do i = 1, ndv
         write(opt_def_unit, *) Xlow(i), X(i), Xupp(i), Xmul(i), Xso(i)
      end do

      !-- Write Flow, Fnc, Fupp, Fmul
      do i = 1, nfnc
         write(opt_def_unit, *) Flow(i), Fnc(i), Fupp(i), Fmul(i)
      end do

      !-- Write iLfun, jLvar, LinG
      do i = 1, nnzLinG
         write(opt_def_unit, *) iLfun(i), jLvar(i), LinG(i)
      end do

      !-- Write iGfun, jGvar
      do i = 1, nnzGrad
         write(opt_def_unit, *) iGfun(i), jGvar(i)
      end do
         
      close(opt_def_unit)

   endif

   !-- CALL OPTIMIZER --

   call hybrOptA(husrfun, popsize, ppc, commarr, mycommid, &
                 hybr_meth, sobol_file, usedb, mingen, maxgen, ranseed, &
                 selmethod, prob_pert, prob_mut, prob_avg, &
                 penalty, xbest, xavg, xpert, xmut, &
                 sn_maj_lim, flow_sol_lim, sample_offset, &
                 cig, optf_output, opt_info_unit, opt_his_unit, &
                 opt_def_unit, dpfile_unit, MPI_COMM_WORLD, lb_tol, &
                 sampl_seq_unit, csampl)

   call mpi_finalize(ierr)

end program

subroutine husrfun(dv, nvar, f, nF, G, lenG, comm, csz, cid, &
                   fsn_base, statt, needF, needG)
#ifdef _MPI_VERSION
      use mpi
#endif

    implicit none

    integer :: nvar, nF, lenG, csz, cid, fsn_base, statt, &
               COMM_CURRENT, nProc, myrank, ierr
    logical :: needF, needG
    integer :: comm(csz)
    double precision :: dv(nvar), f(nF), G(lenG)

    f(1) = dv(1)**2 - dv(2)**2
    G(1) =  2*dv(1)
    G(2) = -2*dv(2)

!    f(1) = 0.00025d0 * dv(1)**2 + 0.00025d0 * dv(2)**2 - &
!           cos(dv(1)) * cos(dv(2)/sqrt(2.0d0)) + 1
!    G(1) = 0.0005d0 * dv(1) + sin(dv(1))*cos(dv(2)/sqrt(2.0d0))
!    G(2) = 0.0005d0 * dv(2) + cos(dv(1))*sin(dv(2)/sqrt(2.0d0))/sqrt(2.0d0)

!    call sleep(8-cid+1)
 
end subroutine

