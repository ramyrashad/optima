      subroutine bcfarbody(jdim,kdim,q,kbc,xy,xit,ett,xyj,x,y)
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kbc,4)                                           
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c                                                                       
c ====================================================================
c ---         Boundary conditions for airfoil topology             ---  
c ---                  c  or  o mesh type                          ---  
c ====================================================================
c                                                                       
c  -far field bc for o mesh outer boundary and c mesh k = kmax          
c  -far field characteristic like bc                          
c                                                                       
c.....................................................................  
c  Far field circulation based on potential vortex added to reduce      
c  the dependency on outer boundary location.  works well.  for         
c  instance, a naca0012 at m=0.63 a=2. shows no dependency on ob from   
c  4 - 96 chords.                                                       
c.....................................................................  
c                                                                       
      gi = 1./gamma                                                 
      gm1i = 1./gami                                                
c                                                                    
      alphar = alpha*pi/180.                                         
      cosang = cos( alphar )                                         
      sinang = sin( alphar )                                         
      ainf = sqrt(gamma*pinf/rhoinf)                                 
      hstfs = 1./gami + 0.5*fsmach**2                                
c                                                                       
c.....................................................................  
c.....................................................................  
c                                                                       
      k = 1                                                          
c      kq = kbc                                                          
      kq = 1
         do 60 j = jtail1,jtail2                                                
c                                                                          
c           -reset free stream values with circulation correction        
c            xa = x(j,k) - chord/4.                                         
c            ya = y(j,k)                                                    
c            radius = sqrt(xa**2+ya**2)                                     
c            angl = atan2(ya,xa)                                            
c            cjam = cos(angl)                                               
c            sjam = sin(angl)                                               
c            qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))    
c            uf = uinf + qcirc*sjam
c            vf = vinf - qcirc*cjam
            uf = uinf                                          
            vf = vinf
            af2 = gami*(hstfs - 0.5*(uf**2+vf**2))                         
            af = sqrt(af2)                                                 
c                                                                          
c.....   ..........................................................  
c           choose a reference frame in terms of normal and          
c           tangential components                                    
c.....   ..........................................................  
c                                                                          
c                                                                          
c           -metric terms                                        
            snorm = 1./sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                        
            xy3h = xy(j,k,3)*snorm                                            
            xy4h = xy(j,k,4)*snorm                                            
c                                                                          
c.....   .................................................................  
c           -check for inflow or outflow                          
c                for inflow : three variables are specified  r1 and        
c                        qt  ( q_tangential)  and   s~ r**gamma/p  (~entrop
c                        with one var. computed.  r2                       
c                for outflow : one variable is fixed r1  with three compute
c                        r2, qt and s                                      
c                                                                          
c                compute riemann invariants                                
c                fix          r1 = qn - 2.*a/(gamma-1)  at free stream     
c                compute      r2 = qn + 2.*a/(gamma-1)  from interior      
c                             extrapolation of flow variables              
c                                                                          
c.....   ................................................................  
c           -get extrapolated variables                                
c.....   ................................................................  
c                                                                          
             rhoext = q(j,kq+1,1)*xyj(j,k+1)                                   
             rjm1   = 1./q(j,kq+1,1)                                           
             uext   = q(j,kq+1,2)*rjm1                                         
             vext   = q(j,kq+1,3)*rjm1                                         
             eext   = q(j,kq+1,4)*xyj(j,k+1)                                   
             pext   = gami*(eext - 0.5*rhoext*(uext**2+vext**2))               
c                                                                          
c.....   ................................................................  
c           -set riemann invariants                                  
c.....   ................................................................  
c                                                                          
             r1 = xy3h*uf + xy4h*vf - 2.*af*gm1i                               
             r2 = xy3h*uext + xy4h*vext +2.*sqrt(gamma*pext/rhoext)*gm1i      
c                                                                              
             qn = (r1 + r2)*0.5                                                
             cspe = (r2 - r1)*gami*0.25                                        
             c2 = cspe**2                                                      
c                                                                          
c.....   ................................................................  
c           -set other fixed or extrapolated variables               
c.....   ................................................................  
c                                                                          
             if(qn .le. 0.0)then                                          
                qt = xy4h*uf - xy3h*vf                                       
                entro = gamma                                                
             else                                                         
                qt = xy4h*uext - xy3h*vext                                   
                entro = rhoext**gamma/pext                                   
             endif                                                        
c                                                                          
c.....   ................................................................  
c           -compute flow variables                                   
c.....   ................................................................  
c                                                                          
             u = xy3h*qn + xy4h*qt                                             
             v = xy4h*qn - xy3h*qt                                             
c                                                                              
             q(j,kq,1) = (c2*entro*gi)**gm1i                                   
             pres = c2*q(j,kq,1)*gi                                            
c                                                                          
c           -add jacobian                                             
             rjj = 1./xyj(j,kq)                                             
             q(j,kq,1) = q(j,kq,1)*rjj                                     
             q(j,kq,2) = q(j,kq,1)*u                                       
             q(j,kq,3) = q(j,kq,1)*v                                       
             q(j,kq,4) = pres*gm1i*rjj + 0.5*q(j,kq,1)*(u**2+v**2)         
60       continue                                                          
c
      return                                                            
      end                                                               
