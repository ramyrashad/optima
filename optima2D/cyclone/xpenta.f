      subroutine xpenta(jdim,kdim,a,b,c,d,e,x,y,f,jl,ju,kl,ku)         
      double precision ld,ld1,ld2,ldi                                              
      dimension a(jdim,kdim),b(jdim,kdim),c(jdim,kdim),d(jdim,kdim)
      dimension e(jdim,kdim),x(jdim,kdim),y(jdim,kdim),f(jdim,kdim)    
c                                                                       
c     ! start forward generation process and sweep                          
c                                                                       
      j = jl                                                          
      do 1 k = kl,ku                                                  
         ld = c(j,k)                                                     
         ldi = 1./ld                                                     
         f(j,k) = f(j,k)*ldi                                             
         x(j,k) = d(j,k)*ldi                                             
         y(j,k) = e(j,k)*ldi                                             
1     continue                                                        
c                                                                     
      j = jl+1                                                        
      do 2 k = kl,ku                                                  
         ld1 = b(j,k)                                                    
         ld = c(j,k) - ld1*x(j-1,k)                                      
         ldi = 1./ld                                                     
         f(j,k) = (f(j,k) - ld1*f(j-1,k))*ldi                            
         x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                            
         y(j,k) = e(j,k)*ldi                                             
2     continue                                                        
c                                                                       
c        do 3 j = jl+2,ju-2
c           do 11 k = kl,ku                                         
c              ld2 = a(j,k)                                            
c              ld1 = b(j,k) - ld2*x(j-2,k)                             
c              ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
c              ldi = 1./ld                                             
c              f(j,k) = (f(j,k) - ld2*f(j-2,k) - ld1*f(j-1,k))*ldi     
c              x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                    
c              y(j,k) = e(j,k)*ldi                                     
c 11        continue                                                
c 3    continue                                                   
      do 3 k = kl,ku                                         
      do 3 j = jl+2,ju-2
         ld2 = a(j,k)                                            
         ld1 = b(j,k) - ld2*x(j-2,k)                             
         ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
         ldi = 1./ld                                             
         f(j,k) = (f(j,k) - ld2*f(j-2,k) - ld1*f(j-1,k))*ldi     
         x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                    
         y(j,k) = e(j,k)*ldi                                     
 3    continue                                                   
                                                                       
      j = ju-1                                                        
      do 12 k = kl,ku                                                 
         ld2 = a(j,k)                                                    
         ld1 = b(j,k) - ld2*x(j-2,k)                                     
         ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))                     
         ldi = 1./ld                                                     
         f(j,k) = (f(j,k) - ld2*f(j-2,k) - ld1*f(j-1,k))*ldi             
         x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                            
12    continue                                                        
c                                                                       
      j = ju                                                  
      do 13 k = kl,ku                                         
         ld2 = a(j,k)                                            
         ld1 = b(j,k) - ld2*x(j-2,k)                             
         ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
         ldi = 1./ld                                             
         f(j,k) = (f(j,k) - ld2*f(j-2,k) - ld1*f(j-1,k))*ldi     
13    continue                                                
c                                                                       
c     !  back sweep solution                                         
c                                                                       
      do 14 k = kl,ku                                                 
         f(ju,k) = f(ju,k)                                               
         f(ju-1,k) = f(ju-1,k) - x(ju-1,k)*f(ju,k)                       
14    continue                                                        
c                                                                       
c        do 4 j = 2,ju-jl                                                
c           jx = ju-j                                                    
c           do 15 k = kl,ku                                              
c           f(jx,k) = f(jx,k) - x(jx,k)*f(jx+1,k) - y(jx,k)*f(jx+2,k)   
c15         continue                                                     
c4       continue                                                        
      do 4 k = kl,ku                                              
      do 4 j = 2,ju-jl                                                
         jx = ju-j                                                    
         f(jx,k) = f(jx,k) - x(jx,k)*f(jx+1,k) - y(jx,k)*f(jx+2,k)    
4     continue                                                        
c                                                                       
      return                                                          
      end                                                             
