c----------------------------------------------------------------------
c     -- sensitivity gradient calculation --
c     -- uses matrix approach --
c     
c     written by: marian nemec
c     date: oct. 2000
c----------------------------------------------------------------------
      subroutine sensit (grad, jdim, kdim, ndim, indx, icol, iex, q, cp,
     &     xy, xyj, x, y, cp_tar, bap, bcp, bt, bknot, dvs, idv, ia, ja,
     &     ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat, turmu, fmu,
     &     vort, press, sndsp, tmet, ds, spect, uu, vv, ccx, ccy, coef4,
     &     coef2, precon, gam, s, rhs, sol, dOdQ, dQdX, dRdX, bcf,
     &     work1, xys, xyjs, xs, ys, turres, xOrig, yOrig, dx, dy, obj0)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim, kdim, ndim

      integer idv(nc+mpopt), indx(jdim,kdim), icol(9), iex(jdim,kdim,4)
      integer ia(jdim*kdim*ndim+2),ipa(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer jpa(jdim*kdim*ndim*ndim*5+1),iat(jdim*kdim*ndim+2)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)

      integer iatmp(maxjk*nblk+2), ipatmp(maxjk*nblk+2)
      integer jatmp(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1) 
      integer jpatmp(maxjk*nblk2*5+1) 

c     -- ILU arrays --
      integer ierr, nz, its_nk_val
      integer,dimension(:),allocatable :: levs,jw
      double precision alu(iwk)
      integer jlu(iwk),ju(maxjk*nblk)

c     -- gmres work array --
      integer iwk_gmres
      double precision,allocatable :: wk_gmres(:)

      double precision grad(nc+mpopt), cp(jdim,kdim)
      double precision cp_tar(jbody,2), dvs(nc+mpopt), bap(jbody,2)
      double precision bcp(nc,2), bt(jbody), bknot(jbsord+nc)

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision xOrig(jdim,kdim), yOrig(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)

      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)
      double precision qtmp(jdim,kdim,ndim),stmp(jdim,kdim,ndim)

      double precision s(jdim,kdim,ndim), rhs(jdim*kdim*ndim+1)
      double precision sol(jdim*kdim*ndim+1)
      double precision dOdQ(jdim,kdim,ndim), dQdX(jdim,kdim,ndim)
      double precision dRdX(jdim,kdim,ndim), bcf(jdim,kdim,4)

      double precision xys(jdim,kdim,4), xyjs(jdim,kdim), xs(jdim,kdim)
      double precision ys(jdim,kdim), turres(jdim,kdim)

c     -- work arrays: temp storage --
c     -- compatible with cyclone --
      double precision work1(maxjk,25)

      double precision dRdXna, dOdQna, dQdXna, resultnorm,dQdXnain, obj0
      double precision multipl(jdim*kdim*ndim+1),reslt(jdim*kdim*ndim+1)

      external framux_o2

csi   variables used for storing rhs values for esdirk time marching
      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim)
      double precision a_jk(6,6)
csi

c     -- time vector arrays --
      double precision dtvec(maxj*maxk*nblk)

c     -- timing variables --
      real*4 time1, time2, t, tarray(2)

c      integer itest(6)
      
      logical jac_tmp, prec_tmp, imps, scalena

cc     c     -- finite diff jacobian --
c      integer ia2(maxjk*nblk+1), ja2(maxjk*nblk2*9)
c      double precision as2(maxjk*nblk2*9)
c      integer ic(maxj*maxk*nblk+1), jc(maxj*maxk*nblk2*9)
c      double precision cs(maxj*maxk*nblk2*9)
c      double precision dd(maxj*maxk*nblk)
c      logical vals
cc     c     ---------------------------

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c   size of vectors grad (the gradient of the objective function with respect 
c     to each of the design variables), idv, and dvs (the vector of the desing
c     variables) have been increased to ensure sufficient
c     space is available for all angles of attack to be used as design variables.
c
c   dodqna is the partial derivative of the objective function with respect to the angle
c      of attack, added as a flow variable for variable angle of attack problems.
c   dqdxna is the derivative of the angle of attack with respect to the 
c      design variables, for variable angle of attack problems.
c   drdxna is the partial derivative of the new residual equation (cl - cl*) with respect
c      to the design variables for variable angle of attack problems.
c   scalena is a variable which controls the scaling of the rhs equation that was
c      added for variable angle of attack problems.
c   rhsnamult is the resulting scaling that is applied to the rhs equation.
c   clopt (in arcom.inc) controls whether or not a variable angle of attack problem
c      is being solved.  Set to TRUE for variable angle of attack in input file.  
c      Default value is FALSE.

      na = jdim*kdim*ndim
      nas = jdim*kdim*ndim*ndim*9
      nap = jdim*kdim*ndim*ndim*5

      if (clopt) then
         na = na+1
         nap = nap+1
         nas = jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1
      end if

cmpr  Allocate some of the big arrays dynamically
      iwk_gmres=(na+1)*(IM_GMRES+2)+(IM_GMRES+1)*IM_GMRES
      allocate(levs(iwk),jw(3*maxjk*nblk),wk_gmres(iwk_gmres))


      jac_tmp = jac_mat
      prec_tmp = prec_mat

      jac_mat = .true.
      prec_mat = .true.

c     -- copy pointer arrays --
      do j=1,na+1
        iatmp(j) = ia(j)
        ipatmp(j) = ipa(j)
      end do
      do j = 1,nas
        jatmp(j) = ja(j)
      end do
      do j = 1,nap
        jpatmp(j) = jpa(j)
      end do 

      write (*,10)
 10   format (/3x,
     &      '<< Solving sensitivity equation to compute gradient. >>',
     &      /3x,'<< Using matrix approach. >>' )

c     -- determine dJ/dQ --
      call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
      call dOBJdQ (jdim, kdim, ndim, x, y, xy, xyj, q, dOdQ, cp_tar,
     &      cp, press, obj0)

      if (clopt) then
         call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
         call dOBJdQna (jdim, kdim, ndim, x, y, xy, xyj, q, dOdQna,
     &        cp_tar,cp, press, obj0)
      end if

c     -- determine dQdX --
c     -- form LHS: preconditioner and Jacobian -- 
c     -- arrays ia, ja, as store O(2) Jacobian --
c     -- arrays ipa, jpa, pa store O(1) preconditioner --

c     -- put in jacobian --
      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
            q(j,k,n) = q(j,k,n)/xyj(j,k)
          end do
        end do
      end do 

c     -- ensure tmet is zero --
      do k = 1,kend
        do j = 1,jend
          tmet(j,k) = 0.d0
        end do
      end do

c     -- calculate pressure and sound speed --
      call calcps (jdim, kdim, q, press, sndsp, work1, xy, xyj)

c     -- compute generalized distance function --
      if (viscous .and. turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kend
          do j = jbegin,jend
            j_point=j
            if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
            xpt1 = x(j_point,kbegin)
            ypt1 = y(j_point,kbegin)
            smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
          end do
        end do
      end if

c     -- lhs: dR/dQ --
      imps=.true.
      call get_lhs (jdim, kdim, ndim, indx, icol, iex, ia, ja, as,
     &      ipa, jpa, pa, xy, xyj, x, y, q, press, sndsp, fmu, turmu,
     &      uu, vv, ccx, ccy, coef2, coef4, tmet, precon, bcf, work1,
     &      ast, pdc,a_jktmp,imps)

c     -- order jacobian array --
      call csrcsc (na, 1, 1, as, ja, ia, ast, jat, iat)
      call csrcsc (na, 1, 1, ast, jat, iat, as, ja, ia)
      if (icg.eq.0) write (opt_unit,20) ia(na+1) - ia(1)
 20   format (3x,'Non-zeros in Jacobian matrix:',i8)

c     -- finite difference jacobian --

c      call fd_drdq (jdim, kdim, ndim, indx, q, press, sndsp, xy,
c     &      xyj, fmu, x, y, vort, tmet, turmu, ia2, ja2, as2, work1) 
c
c 200  format (1h ,34(1h-),' row',i5,1x,34(1h-),/
c     *      3('  columns :     values   * ') )
cc     -- xiiiiiihhhhhhdddddddddddd-*- --
c 201  format(3(1h ,i6,5h   : ,D12.5,3h * ) )
c
c      nzmax = maxj*maxk*nblk2*9
c      call aplsb (na,na,as,ja,ia,-1.d0,as2,ja2,ia2,cs,jc,ic,nzmax,ierr)
c
c      write (*,*) 'IERR',ierr
c
c      call retmx (na,cs,jc,ic,dd)
c
c      n = 0
c      vmax = 0.d0
c      do j=1,na
c        if (dabs(dd(j)).gt.1.d-4) then
c          do nn = 1,ndim
c            do kk = 2,kup
c              do jj = 2,jup
c                if ( (( indx(jj,kk) - 1 )*ndim + nn).eq.j ) then
c                  write (*,333) jj,kk,nn,j,dd(j)
c                  if (dabs(dd(j)).gt.vmax) then
c                    vmax=dd(j)
c                    jvmax = jj
c                    kvmax = kk
c                  end if
c                  goto 222
c                end if
c              end do
c            end do
c          end do
c        end if
c 222    continue
c      end do
c 333  format (4i8,e12.3)
c
c      jvmax = 20
c      kvmax = 2
c
c      i1 = ( indx(jvmax,kvmax) - 1 )*ndim + 1
c      i2 = ( indx(jvmax,kvmax) - 1 )*ndim + ndim
c      
c      write (*,*) 'node jj kk',jvmax,kvmax,vmax
c
c      do i=i1, i2
c        write(87,200) i
c        k1=ia(i)
c        k2 = ia(i+1)-1
c          write (87,201) (ja(k),as(k),k=k1,k2)
c      end do
c
c      do i=i1, i2
c        write(86,200) i
c        k1=ia2(i)
c        k2 = ia2(i+1)-1
c          write (86,201) (ja2(k),as2(k),k=k1,k2)
c      end do
c
c      stop
cc     ---

c     -- order preconditioner array --
      call csrcsc (na, 1, 1, pa, jpa, ipa, pat, jpt, ipt)
      call csrcsc (na, 1, 1, pat, jpt, ipt, pa, jpa, ipa)
      if (icg.eq.0) write (opt_unit,30) ipa(na+1) - ipa(1)
 30   format (3x,'Non-zeros in preconditioner:',i8)

c     -- take out jacobian --
      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
            q(j,k,n) = q(j,k,n)*xyj(j,k)
          end do
        end do
      end do           

c     -- storing grid and metrics is done for speed --
      do n = 1,4
        do k=kbegin,kend
          do j=jbegin,jend
            xys(j,k,n) = xy(j,k,n)
          end do
        end do
      end do
              
      do k=kbegin,kend
        do j=jbegin,jend
          xs(j,k) = x(j,k)
          ys(j,k) = y(j,k)
          xyjs(j,k) = xyj(j,k)
        end do
      end do

c      itest(1) = 1
c      itest(2) = 1
c      itest(3) = 2
c      itest(4) = 1
c      itest(5) = 2
c      itest(6) = 3

c     -- solve sensitivity equation for each design variable --
      do in = 1,ndv
c      do iii = 1,6
c         in = itest(iii)
c     -- form rhs => dR/dX -- 
c     -- sol and rhs used as work arrays --
        call dResdX ( in, jdim, kdim, ndim, q, xy, xyj, x, y, bap, bcp,
     &        bt, bknot, dvs, idv, dRdX, sol, rhs, precon, coef2, coef4,
     &        uu, vv, ccx, ccy, ds, press, tmet,spect, gam, turmu, fmu,
     &        vort, sndsp, work1, xys, xyjs, xs, ys, turres,
     &        xOrig, yOrig, dx, dy, -1.d0, dRdXna)

c     -- diagonal scaling for all b.c. equations --
c     -- next 'if' statement is used to ensure that scale_bc does not  
c     rescale the jacobian --
        if (in .gt. 1) jac_mat = .false.
c     if (iii .gt. 1) jac_mat = .false.
        scalena = .false.
        if (clopt) scalena = .true. 
        call scale_bc (1, jdim, kdim, ndim, indx, iex, ia, ja, as, ipa,
     &       jpa, pa, dRdX, bcf, work1, rhsmultna, scalena)
        if (in .gt. 1) jac_mat = .true.
c     if (iii .gt. 1) jac_mat = .true.

c     -- ILU --
        if ( prec_mat ) call prec_ilu (jdim, kdim, ndim, ilu_meth, lfil,
     &        opt_unit, ipa, jpa, pa, alu, jlu, ju, levs, jw,work1)

c     -- gmres --
        ipar(1) = 0
        ipar(2) = 2
        ipar(3) = 1
        ipar(4) = iwk_gmres
        ipar(5) = im_gmres
        ipar(6) = iter_gmres

c     -- inner iterations convergence tolerance -- 
        fpar(1) = eps_gmres
        fpar(2) = 1.d-14
        fpar(11) = 0.0

        do n = 1,ndim
          do k = 1,kend
            do j = 1,jend
              jk = ( indx(j,k) - 1 )*ndim + n
              rhs(jk) = dRdX(j,k,n)
            end do
          end do
        end do 
        if (clopt) rhs(na) = dRdXna*rhsmultna

        write (*,55) ipar(5)
 55     format (/3x,'Starting GMRES(',i2,') ...')

        ierr = 0
        t=etime(tarray)
        time=tarray(1)
        call run_gmres (jdim, kdim, ndim, indx, iex, q, xy, xyj, x, y,
     &        precon, coef2, coef4, uu, vv, ccx, ccy, ds, press, tmet,
     &        spect, gam, turmu, fmu, vort, sndsp, s, bcf, 0, rhs,
     &        sol, as, ja, ia, alu, jlu, ju, work1(1,1), work1(1,6),
     &        wk_gmres, work1(1,11), 6, gmres_unit, framux_o2,rhsnamult, 
     &        D_hat,exprhs, a_jk, its_nk_val,dtvec)
        t=etime(tarray)
        time=tarray(1)-time

        write (*,60) im_gmres, ipar(1), time
        write (*,61) ipar(7), fpar(5)
        write (opt_unit,60) im_gmres, ipar(1), time
        write (opt_unit,61) ipar(7), fpar(5)

 60     format (3x,'GMRES(',i2,') return code:',i4,2x,'Run-time:',
     &        f7.2,'s')
 61     format (3x,'Number of inner iterations',i5,' and residual',
     &        e12.4)

        if ( ipar(1).lt.0 ) then 
          write (*,*) '!!SENSITIVITY: gmres convergence problems!!'
          stop
        end if

        do n = 1,ndim
          do k = 1,kend
            do j = 1,jend
              jk = ( indx(j,k) - 1 )*ndim + n
              dQdX(j,k,n) = sol(jk)
            end do
          end do
        end do
        if (clopt) dQdXna = sol(na)

c     -- for viscous flow, zero out the sensitivity vector at the
c     boundary  for the momentum equation --
      if (viscous) then
        k = 1
        do n = 2,3
          do j = jtail1,jtail2
            dQdX(j,k,n) = 0.d0
          end do
        end do 
      end if

c     -- product of objective function and state vector sensitivity -- 
        grad(in) = 0.d0
        do n = 1,ndim
          do k = 1,kend
            do j = 1,jend
              grad(in) = grad(in) + dOdQ(j,k,n)*dQdX(j,k,n)
            end do
          end do
        end do
        if (clopt) grad(in) = grad(in) + dOdQna*1.d2 * dQdXna

c     -- dJ/dX --
        call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
        call dObjdX (in, dOdX, jdim, kdim, dvs, idv, x, y, bap, bcp,
     &        bt, bknot, q, press, xy, xyj, xys, xyjs, xs, ys,
     |        xOrig, yOrig, dx, dy, -1.d0, obj0)

        grad(in) = grad(in) + dOdX

        if (in.eq.1) prec_mat = .false.
      end do

      do j=1,na+1
        ia(j) = iatmp(j)
        ipa(j) = ipatmp(j) 
      end do
      do j = 1,nas
        ja(j) = jatmp(j)
      end do
      do j = 1,nap
        jpa(j) = jpatmp(j) 
      end do   

      jac_mat = jac_tmp
      prec_mat = prec_tmp

cmpr  Deallocate memory
      deallocate(levs,jw,wk_gmres)

      return 
      end                       !sensit
