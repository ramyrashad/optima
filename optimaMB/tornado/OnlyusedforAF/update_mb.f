c
c UPDATING the state variables
c
      tmaxk=0.0
      tmaxe=0.0
      residk=0.0
      reside=0.0
      negnk=0.0
      negne=0.0
      do k=klow,kup
         do j=jlow,jup
            num=0
            if ((vk(j,k)+dk(j,k)).lt.0.0) negnk=negnk+1
            do while (((vk(j,k)+dk(j,k)).le.0.0).and.(num.le.10))
                num=num+1
c                dk(j,k)=dk(j,k)/2.0
                dk(j,k)=0.0
            enddo
            if ((vk(j,k)+dk(j,k)).gt.0.0) vk(j,k)=vk(j,k)+dk(j,k)
            tmaxk=max(tmaxk,vk(j,k))
            residk=residk+(dk(j,k))**2
            num=0
            if ((ve(j,k)+de(j,k)).lt.0.0) negne=negne+1
            do while (((ve(j,k)+de(j,k)).le.0.0).and.(num.le.10))
                num=num+1
c                de(j,k)=de(j,k)/2.0
                de(j,k)=0.0
            enddo
            if ((ve(j,k)+de(j,k)).gt.0.0) ve(j,k)=ve(j,k)+de(j,k)
            tmaxe=max(tmaxe,de(j,k))
            reside=reside+(de(j,k))**2
         enddo
      enddo
c
c reporting convergence rate
c
      if (mod(numiter, 100) .eq. 1) then
        residk=sqrt(residk)/float((jmax-1)*(kmax-1))
        reside=sqrt(reside)/float((jmax-1)*(kmax-1))
        write (21,'(i5,a,e10.4,a,e10.4,a,e10.4,a,e10.4,a,i5,a,i5)')
     $    numiter,' rk=',residk,' re=',reside,' mk=',tmaxk,
     $    'me=',tmaxe,' nk=',negnk,' ne=',negne
        call flushit(21)
      endif
