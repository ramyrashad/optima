      subroutine splintder(jdim,xa,ya,y2a,x,dy)
c
c     Written by Stan De Rango 23/06/97
c
c     -this subroutine returns the first derivative, dy, of the 
c      splined function at location x.
      implicit none
      integer j,jhi,jlo,jdim
      double precision a,b,h,x,dy,xa(jdim),ya(jdim),y2a(jdim)
c
      jlo=1
      jhi=jdim
 1    if (jhi-jlo.gt.1) then
         j=(jhi+jlo)/2
         if(xa(j).gt.x) then
            jhi=j
         else
            jlo=j
         endif
         goto 1
      endif
c
      h=xa(jhi)-xa(jlo)
      if (h.eq.0.d0) pause 'bad xa input in splintder'
      a=(xa(jhi)-x)/h
      b=(x-xa(jlo))/h
c
      dy= (ya(jhi)-ya(jlo))/h - (3.d0*a**2-1.d0)*h*y2a(jlo)/6.d0 + 
     &    (3.d0*b**2-1.d0)*h*y2a(jhi)/6.d0
      return
      end
