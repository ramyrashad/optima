C
      SUBROUTINE init_fit(nchrom,nobfun,fit,i_mnx,fit_init,
     $     rand_init_fit)
      dimension fit(nobfun,nchrom)
      real*8 rand
      logical rand_init_fit
      integer seed

      if(.not.rand_init_fit)then
         DO NC=1,NCHROM
            DO NO=1,NOBFUN
               FIT(NO,NC) = fit_init
            ENDDO
         ENDDO
      else
         DO NC=1,NCHROM
            DO NO=1,NOBFUN
               CALL RAN3(1,RAND)
               FIT(NO,NC) = rand
            ENDDO
         ENDDO
      endif


      RETURN
      END
