       SUBROUTINE INTQ(LMAXP,XC,YC,ZC,QC,LMAX0,XF,YF,ZF,QF)             
C                                                                       
C===============================================================        
c      PARAMETER (maxj=497)                                             
#include "../include/parm.inc"
       DIMENSION XF(2),YF(2),ZF(2)                                      
       DIMENSION XC(2),YC(2),ZC(2)                                      
       DIMENSION QF(2),QC(2)                                            
       DIMENSION S(MAXJ)                                                
       DIMENSION SS(MAXJ)                                               
                                                                        
       S(1)  = 0.0                                                      
       DO 10 L=2,LMAX0                                                  
       S(L)=S(L-1) +                                                    
     >  SQRT( (XF(L)-XF(L-1))**2 + (YF(L)-YF(L-1))**2                   
     >      + (ZF(L)-ZF(L-1))**2 )                                      
10     CONTINUE                                                         
                                                                        
                                                                        
       IF(S(LMAX0).GT. 1.E-20)THEN                                      
                                                                        
                                                                        
       SS(1)  = 0.0                                                     
       DO 20 L=2,LMAXP                                                  
       SS(L)=SS(L-1) +                                                  
     >  SQRT( (XC(L)-XC(L-1))**2 + (YC(L)-YC(L-1))**2                   
     >      + (ZC(L)-ZC(L-1))**2 )                                      
20     CONTINUE                                                         
                                                                        
       SS(LMAXP) = S(LMAX0)                                             
                                                                        
                                                                        
       CALL CSPLIN(SS,QC,LMAXP,S,QF,LMAX0)                              
                                                                        
       ELSE                                                             
C                                                                       
C.... SINGULARITY DETECTED                                              
C                                                                       
       SUM = 0.0                                                        
       DO 30 L=1,LMAXP                                                  
        SUM = SUM + QC(L)                                               
 30    CONTINUE                                                         
        SUM = SUM/FLOAT(LMAXP)                                          
       DO 40 L=1,LMAX0                                                  
        QF(L) = SUM                                                     
 40    CONTINUE                                                         
                                                                        
       ENDIF                                                            
                                                                        
                                                                        
                                                                        
       RETURN                                                           
       END                                                              
