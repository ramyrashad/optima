      subroutine xpenta2(jdim,kdim,a,b,c,d,e,x,y,f,jl,ju,kl,ku)        
      double precision ld,ld1,ld2,ldi                                              
      dimension a(jdim,kdim),b(jdim,kdim),c(jdim,kdim),d(jdim,kdim)   
      dimension e(jdim,kdim),x(jdim,kdim),y(jdim,kdim),f(jdim,kdim,2)  
c                                                                       
c     ! start forward generation process and sweep                          
c                                                                       
      j = jl                                                          
      do 1 k = kl,ku                                                  
         ld = c(j,k)                                                     
         ldi = 1./ld                                                     
         f(j,k,1) = f(j,k,1)*ldi                                         
         f(j,k,2) = f(j,k,2)*ldi                                         
         x(j,k) = d(j,k)*ldi                                             
         y(j,k) = e(j,k)*ldi                                             
1     continue                                                        
c                                                                     
      j = jl+1                                                        
      do 2 k = kl,ku                                                  
         ld1 = b(j,k)                                                    
         ld = c(j,k) - ld1*x(j-1,k)                                      
         ldi = 1./ld                                                     
         f(j,k,1) = (f(j,k,1) - ld1*f(j-1,k,1))*ldi                      
         f(j,k,2) = (f(j,k,2) - ld1*f(j-1,k,2))*ldi                      
         x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                            
         y(j,k) = e(j,k)*ldi                                             
2     continue                                                        
c                                                                       
c            do 3 j = jl+2,ju-2                                          
c                do 11 k = kl,ku                                         
c                ld2 = a(j,k)                                            
c                ld1 = b(j,k) - ld2*x(j-2,k)                             
c                ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
c                ldi = 1./ld                                             
c                f(j,k,1) = (f(j,k,1) - ld2*f(j-2,k,1)                   
c     *                      - ld1*f(j-1,k,1))*ldi                       
c                f(j,k,2) = (f(j,k,2) - ld2*f(j-2,k,2)                   
c     *                      - ld1*f(j-1,k,2))*ldi                       
c                x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                    
c                y(j,k) = e(j,k)*ldi                                     
c11              continue                                                
c3            continue                                                   
      do 3 k = kl,ku                                         
      do 3 j = jl+2,ju-2                                          
         ld2 = a(j,k)                                            
         ld1 = b(j,k) - ld2*x(j-2,k)                             
         ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
         ldi = 1./ld                                             
         f(j,k,1) = (f(j,k,1) - ld2*f(j-2,k,1) - ld1*f(j-1,k,1))*ldi
         f(j,k,2) = (f(j,k,2) - ld2*f(j-2,k,2) - ld1*f(j-1,k,2))*ldi
         x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                    
         y(j,k) = e(j,k)*ldi                                     
 3    continue                                                   
c                                                                       
      j = ju-1                                                        
      do 12 k = kl,ku                                                 
         ld2 = a(j,k)                                                    
         ld1 = b(j,k) - ld2*x(j-2,k)                                     
         ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))                     
         ldi = 1./ld                                                     
         f(j,k,1) = (f(j,k,1) - ld2*f(j-2,k,1) - ld1*f(j-1,k,1))*ldi     
         f(j,k,2) = (f(j,k,2) - ld2*f(j-2,k,2) - ld1*f(j-1,k,2))*ldi     
         x(j,k) = (d(j,k) - ld1*y(j-1,k))*ldi                            
12    continue                                                        
c                                                                       
      j = ju                                                  
      do 13 k = kl,ku                                         
         ld2 = a(j,k)                                            
         ld1 = b(j,k) - ld2*x(j-2,k)                             
         ld = c(j,k) - (ld2*y(j-2,k) + ld1*x(j-1,k))             
         ldi = 1./ld                                             
         f(j,k,1) = (f(j,k,1) - ld2*f(j-2,k,1) - ld1*f(j-1,k,1))*ldi
         f(j,k,2) = (f(j,k,2) - ld2*f(j-2,k,2) - ld1*f(j-1,k,2))*ldi
13    continue                                                
c                                                                       
c     !  back sweep solution                                         
c                                                                       
      do 14 k = kl,ku                                                 
         f(ju,k,1) = f(ju,k,1)                                           
         f(ju,k,2) = f(ju,k,2)                                           
         f(ju-1,k,1) = f(ju-1,k,1) - x(ju-1,k)*f(ju,k,1)                 
         f(ju-1,k,2) = f(ju-1,k,2) - x(ju-1,k)*f(ju,k,2)                 
14    continue                                                        
c                                                                       
c      do 4 j = 2,ju-jl                                                
c         jx = ju-j                                                    
c      do 15 k = kl,ku                                              
c         f(jx,k,1) = f(jx,k,1) - x(jx,k)*f(jx+1,k,1) -                
c     *               y(jx,k)*f(jx+2,k,1)                              
c         f(jx,k,2) = f(jx,k,2) - x(jx,k)*f(jx+1,k,2) -                
c     *               y(jx,k)*f(jx+2,k,2)                              
c15    continue
c4     continue                                                        
      do 4 k = kl,ku                                              
      do 4 j = 2,ju-jl                                                
        jx = ju-j                                                    
        f(jx,k,1) =f(jx,k,1) - x(jx,k)*f(jx+1,k,1) - y(jx,k)*f(jx+2,k,1)
        f(jx,k,2) =f(jx,k,2) - x(jx,k)*f(jx+1,k,2) - y(jx,k)*f(jx+2,k,2)
4     continue                                                        
c                                                                       
      return                                                          
      end                                                             
