      subroutine clcd( jdim,kdim,q,press,x,y,xy,xyj,
     *         nscal, cl, cd, cm, clvv, cdvv, cmvv)
c
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim)
c
c
      dimension z(maxj),work(maxj,29)
      common/worksp/z,work
c
c     the parameter nscal determines whether the q variables are scaled
c     by the metric jacobians xyj.
c
c     routine supplies force and moment coeffs.  on airfoil surface
c
      cpc = 2.d0/(gamma*fsmach**2)
c
      cl = 0.
      cd = 0.
      cm = 0.
      clvv = 0.
      cdvv = 0.
      cmvv = 0.
c
c  set limits of integration
c
      j1 = jlow
      j2 = jup
      jtp = jlow
      if(.not.periodic)then
      j1 = jtail1
      j2 = jtail2
      jtp = jtail1+1
      endif
c
c     compute cp at grid points and store in z array
c
      do 10 j=j1,j2
         pp = press(j,1)
         if( nscal .eq.0) pp = pp*xyj(j,1)
         z(j) = (pp*gamma -1.d0)*cpc
10    continue
c
c     compute normal force coefficient and chord directed force coeff
c     chord taken as one in all cases
c
c
      cn = 0.
      cc = 0.
      cmle = 0.
c
c     For periodic flows:
c     -going from interval contained between (jmax,1), then (1,2)
c      and ending on (jmax-1,jmax). Note: jlow=1,jup=jmax,jmax=jmaxold-1
c
      do 11 j=jtp,j2
        jm1 = jminus(j)
        cpav = (z(j) + z(jm1))*.5
        cn = cn - cpav*(x(j,1) - x(jm1,1))
        cc = cc + cpav*(y(j,1) - y(jm1,1))
        cmle = cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))
c
c        Tornado does it this way.
c         cmle = cmle + cpav*
c     &    (  (x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1)) +
c     &       (y(j,1)+y(jm1,1))*.5*(y(j,1) -y(jm1,1))   )
c
 11   continue
      cl = cn*cos(alpha*pi/180.)-cc*sin(alpha*pi/180.)
      cd = cn*sin(alpha*pi/180.) + cc*cos(alpha*pi/180.)
      cmqc = cmle +.25*cn
      cm = cmqc
c      write (*,*) 'cl,cd',cl,cd
c
      if(viscous)then
c        write (*,*)  ' in viscous'
c
c     viscous coefficent of friction calculation
c     taken from p. buning
c
c     calculate the skin friction coefficient
c
c      c  = tau   /            2
c       f      w / 1/2*rho   *u
c                         inf  inf
c
c      tau  = mu*(du/dy-dv/dx)
c         w                   w
c
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc.,
c     york, 1974, p. 50, but use freestream values instead of edge values.
c
c
c     for calculating cf, we need the coefficient of viscosity, mu.  use
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1, rhoinf=1.
c
      cinf  = 1.
      alngth= 1.
c     re already has fsmach scaling
      amu   = rhoinf*alngth/re
      uinf2 = fsmach**2
c
      k= 1
      ja = jlow
      jb = jup
c
      if(.not.periodic)then
        j = jtail1
        jp1 = j+1
        uxi = q(jp1,k,2)/q(jp1,k,1)-q(j,k,2)/q(j,k,1)
        vxi = q(jp1,k,3)/q(jp1,k,1)-q(j,k,3)/q(j,k,1)
c        uxi=0.d0
c        vxi=0.d0
c       -second-order biased
        ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)
     *        -.5*q(j,k+2,2)/q(j,k+2,1)
        veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)
     *        -.5*q(j,k+2,3)/q(j,k+2,1)
c
        xix = xy(j,k,1)
        xiy = xy(j,k,2)
        etax = xy(j,k,3)
        etay = xy(j,k,4)
        tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
        z(j)= tauw/(.5*rhoinf*uinf2)
c
        j = jtail2
        jm1 = j-1
        uxi = q(j,k,2)/q(j,k,1)-q(jm1,k,2)/q(jm1,k,1)
        vxi = q(j,k,3)/q(j,k,1)-q(jm1,k,3)/q(jm1,k,1)
c        uxi=0.d0
c        vxi=0.d0
        ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)
     *        -.5*q(j,k+2,2)/q(j,k+2,1)
        veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)
     *        -.5*q(j,k+2,3)/q(j,k+2,1)
c
        xix = xy(j,k,1)
        xiy = xy(j,k,2)
        etax = xy(j,k,3)
        etay = xy(j,k,4)
        tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
        z(j)= tauw/(.5*rhoinf*uinf2)
c
c       -set new limits
        ja = jtail1+1
        jb = jtail2-1
      endif
c
      do 110 j = ja,jb
        jp1 = jplus(j)
        jm1 = jminus(j)
        uxi = .5*(q(jp1,k,2)/q(jp1,k,1)-q(jm1,k,2)/q(jm1,k,1))
        vxi = .5*(q(jp1,k,3)/q(jp1,k,1)-q(jm1,k,3)/q(jm1,k,1))
c        uxi=0.d0
c        vxi=0.d0
        ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)
     *        -.5*q(j,k+2,2)/q(j,k+2,1)
        veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)
     *        -.5*q(j,k+2,3)/q(j,k+2,1)

        xix = xy(j,k,1)
        xiy = xy(j,k,2)
        etax = xy(j,k,3)
        etay = xy(j,k,4)

        tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
        z(j)= tauw/(.5*rhoinf*uinf2)
c
 110  continue
c      write (*,*) 'done 110'
      if (sngvalte) then
c       -average trailing edge value
        z(j1)=.5d0*(z(j1)+z(j2))
        z(j2)=z(j1)
      endif
c
c     Compute viscous normal and axial forces
c
c     -compute normal force coefficient and chord directed force coeff
c     -chord taken as one in all cases
c
      cnv = 0.
      ccv = 0.
      cmlev = 0.
      do 111 j=jtp,j2
         jm1 = jminus(j)
         cfav = (z(j) + z(jm1))*.5
         ccv = ccv + cfav*(x(j,1) - x(jm1,1))
         cnv = cnv + cfav*(y(j,1) - y(jm1,1))
         cmlev = cmlev + cfav*
     *    (  (x(j,1)+x(jm1,1))*.5*(y(j,1) -y(jm1,1)) -
     *       (y(j,1)+y(jm1,1))*.5*(x(j,1) -x(jm1,1))   )
111   continue
c      write (*,*) ' done 111'
      clvv = cnv*cos(alpha*pi/180.)-ccv*sin(alpha*pi/180.)
      cdvv = cnv*sin(alpha*pi/180.) + ccv*cos(alpha*pi/180.)
      cmqcv = cmlev +.25*cnv
      cmvv = cmqcv
c      write (*,*) 'clvv,cdvv',clvv,cdvv
c
      endif

c
      return
      end
