c     ***************************************************
c             do xi derivative contribution first
c     ***************************************************
      zz=1.d0/12.d0
      do 900 k=klow,kup
      do 900 j=jlow+1,jup-1
        jp2=j+2
        jp1=j+1
        jm1=j-1
        jm2=j-2
        ujm2=workq(jm2,k,2)
        ujm1=workq(jm1,k,2)
        ujp1=workq(jp1,k,2)
        ujp2=workq(jp2,k,2)
c     
        vjm2=workq(jm2,k,3)
        vjm1=workq(jm1,k,3)
        vjp1=workq(jp1,k,3)
        vjp2=workq(jp2,k,3)
c     
c        vorticity(j,k)=0.d0
        vorticity(j,k)=zz*(
     &     (-vjp2+8.d0*(vjp1-vjm1)+vjm2)*xy(j,k,1)
     &    -(-ujp2+8.d0*(ujp1-ujm1)+ujm2)*xy(j,k,2))
 900  continue
c
      zz=1.d0/6.d0
      j=jlow
      jm1=j-1
      jp1=j+1
      jp2=j+2
      do j=klow,kup
         ujm1=workq(jm1,k,2)
         ujk =workq(j,k,2)
         ujp1=workq(jp1,k,2)
         ujp2=workq(jp2,k,2)
         vjm1=workq(jm1,k,3)
         vjk =workq(j,k,3)
         vjp1=workq(jp1,k,3)
         vjp2=workq(jp2,k,3)
c
c        vorticity(j,k)=0.d0
         vorticity(j,k)=zz*(
     &     (-2.d0*vjm1-3.d0*vjk+6.d0*vjp1-vjp2)*xy(j,k,1)
     &    -(-2.d0*ujm1-3.d0*ujk+6.d0*ujp1-ujp2)*xy(j,k,2))
      enddo
c
      j=jup
      jp1=j+1
      jm1=j-1
      jm2=j-2
      do k=klow,kup
         ujm2=workq(jm2,k,2)
         ujm1=workq(jm1,k,2)
         ujk =workq(j,k,2)
         ujp1=workq(jp1,k,2)
         vjm2=workq(jm2,k,3)
         vjm1=workq(jm1,k,3)
         vjk =workq(j,k,3)
         vjp1=workq(jp1,k,3)
c
c        vorticity(j,k)=0.d0
         vorticity(j,k)=zz*(
     &     (vjm2-6.d0*vjm1+3.d0*vjk+2.d0*vjp1)*xy(j,k,1)
     &    -(ujm2-6.d0*ujm1+3.d0*ujk+2.d0*ujp1)*xy(j,k,2))
      enddo
c
c
c     ***************************************************
c            do eta derivative contribution now
c     ***************************************************
      zz=1.d0/12.d0
      do 955 k=klow+1,kup-1
        kp2=k+2
        kp1=k+1
        km1=k-1
        km2=k-2
        do 950 j=jlow,jup
          ukm2=workq(j,km2,2)
          ukm1=workq(j,km1,2)
          ukp1=workq(j,kp1,2)
          ukp2=workq(j,kp2,2)
c     
          vkm2=workq(j,km2,3)
          vkm1=workq(j,km1,3)
          vkp1=workq(j,kp1,3)
          vkp2=workq(j,kp2,3)
c     
          vorticity(j,k)=abs(vorticity(j,k) + zz*(
     &          +(-vkp2+8.d0*(vkp1-vkm1)+vkm2)*xy(j,k,3)
     &          -(-ukp2+8.d0*(ukp1-ukm1)+ukm2)*xy(j,k,4)))
 950    continue
 955  continue

      zz=1.d0/6.d0
      k=klow
      km1=k-1
      kp1=k+1
      kp2=k+2
      do j=jlow,jup
         ukm1=workq(j,km1,2)
         ujk =workq(j,k,2)
         ukp1=workq(j,kp1,2)
         ukp2=workq(j,kp2,2)
         vkm1=workq(j,km1,3)
         vjk =workq(j,k,3)
         vkp1=workq(j,kp1,3)
         vkp2=workq(j,kp2,3)
c
         vorticity(j,k)=abs(vorticity(j,k) + zz*(
     &     (-2.d0*vkm1-3.d0*vjk+6.d0*vkp1-vkp2)*xy(j,k,3)
     &    -(-2.d0*ukm1-3.d0*ujk+6.d0*ukp1-ukp2)*xy(j,k,4)))
      enddo
c
      k=kup
      kp1=k+1
      km1=k-1
      km2=k-2
      do j=jlow,jup
         ukm2=workq(j,km2,2)
         ukm1=workq(j,km1,2)
         ujk =workq(j,k,2)
         ukp1=workq(j,kp1,2)
         vkm2=workq(j,km2,3)
         vkm1=workq(j,km1,3)
         vjk =workq(j,k,3)
         vkp1=workq(j,kp1,3)
c
         vorticity(j,k)=abs(vorticity(j,k) + zz*(
     &     (vkm2-6.d0*vkm1+3.d0*vjk+2.d0*vkp1)*xy(j,k,3)
     &    -(ukm2-6.d0*ukm1+3.d0*ujk+2.d0*ukp1)*xy(j,k,4)))
      enddo
c
      k=1
      kp1=k+1
      kp2=k+2
      kp3=k+3
      zz=1.d0/6.d0
c     -we only need vorticity on airfoil surface at transition points
      do j=jtranlo,jtranup,jtranup-jtranlo
         jp1=j+1
         jm1=j-1
         ujk=workq(j,k,2)
         ujm1=workq(jm1,k,2)
         ujp1=workq(jp1,k,2)
         ukp1=workq(j,kp1,2)
         ukp2=workq(j,kp2,2)
         ukp3=workq(j,kp3,2)
         vjk=workq(j,k,3)
         vjm1=workq(jm1,k,3)
         vjp1=workq(jp1,k,3)
         vkp1=workq(j,kp1,3)
         vkp2=workq(j,kp2,3)
         vkp3=workq(j,kp3,3)
c
         vorticity(j,k)=abs(
c          -second-order streamwise and third-order normal
c          -streamwise term is zero after strtit iterations anyway.
     &     0.5d0*(vjp1-vjm1)*xy(j,k,1)+
     &     zz*(-11.d0*vjk+18.d0*vkp1-9.d0*vkp2+2.d0*vkp3)*xy(j,k,3)-
     &     0.5d0*(ujp1-ujm1)*xy(j,k,2)-
     &     zz*(-11.d0*ujk+18.d0*ukp1-9.d0*ukp2+2.d0*ukp3)*xy(j,k,4))
c           second-order both ways         
c     &     (vjp1-vjm1)*xy(j,k,1)+(-3.d0*vjk+4.d0*vkp1-vkp2)*xy(j,k,3)
c     &    -(ujp1-ujm1)*xy(j,k,2)-(-3.d0*ujk+4.d0*ukp1-ukp2)*xy(j,k,4)))
      enddo
