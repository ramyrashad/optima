c ********************************************************************
c ***************** initialize variables and data ********************
c ********************************************************************
c calling routine:  main
c
      subroutine initia(mglev,ifirst,jdim,kdim,q,qos,press,sndsp,s,xy,
     *                  xyj,xit,ett,ds,x,y,turmu,fmu,vort,turre,turos,
     &                  vk,ve,coef2x,coef4x,coef2y,coef4y,precon,nmax)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"
c                                                                       
      double precision q(maxjkq),precon(maxjk*8),qos(maxjkq)
      double precision press(maxjk),coef2y(maxjk),coef4y(maxjk)              
      double precision s(maxjkq),xy(maxjkq),xyj(maxjk)           
      double precision xit(maxjk),ett(maxjk),ds(maxjk),fmu(maxjk)             
      double precision x(maxjk),y(maxjk),turmu(maxjk),turos(maxjk)       
      double precision vort(maxjk),turre(maxjk),vk(maxjk),ve(maxjk)
      double precision coef2x(maxjk),coef4x(maxjk),sndsp(maxjk)
      integer mglev,ifirst,jdim,kdim,nmax,ii,nside1,iblk1,iblk2,i,i2,ip
      integer junit,nside2,ib
      double precision cosang,sinang
      
c
c*********************************************************************
c               compute various constants                               
c*********************************************************************

c     write (*,*) 'initia: ifirst',ifirst

      gamma = 1.4 
      gami = gamma -1.0
      einf   = 1.0/(gamma*gami) + 0.5*fsmach*fsmach
      rhoinf = 1.0               
      pinf   = 1.0/gamma 
      pout   = pinf

      cosang = cos(pi*alpha/180.0)
      sinang = sin(pi*alpha/180.0) 
      uinf   = fsmach*cosang    
      vinf   = fsmach*sinang  
 
      re = re_input / fsmach  
      if (re.ne.0.0 .and. ifirst.eq. 1) then 
         write(n_out,*)
         write(n_out,3) re   
         write(n_out,*)
 3       format( 1x, 36hRe scaled by freestream, reset to :    ,e14.3)
      end if 

      if (mg .or. gseq) then
         iblk1=1
         iblk2=nblkstot
      else
         iblk1=1
         iblk2=nblks
      end if

      if (ifirst.eq.1) then                                       
c     -first time initia called
c     -initialize to free stream                              
c     -load free stream at angle of attack alpha              
         if (zerostart .and. .not. restart) then
            do i=iblk1,iblk2
               call zerost( q(lqptr(i)), jbmax(i)+2*nhalo,
     &              kbmax(i)+2*nhalo, rhoinf,einf)
            end do
         else if (.not. restart) then
            do i=iblk1,iblk2
              call frstrm(q(lqptr(i)),jmax(i),kmax(i),nmax, 
     &          rhoinf,uinf,vinf,einf,turre(lgptr(i)),retinf,jminbnd(i),
     &          jmaxbnd(i),kminbnd(i),kmaxbnd(i),i)
            end do

            call halo_q(iblk1,iblk2,jdim,kdim,q,turre)

         end if
      end if
  
c     -- set up variables, indecies, and compute metrics
c     -- this must be done prior to reading the q file
c     -- note: copy metrics for those blocks with interface conditions,
c     do not copy for avg or other bc's.

      call setup( coef2x, coef2y, vk, ve, s, xy, xyj, xit,
     &     ett, ds, x, y, turmu, fmu, vort)

c     -- detection of negative Jacobian for optimization runs --
      if (badjac) return

      do ii=iblk1,iblk2
         do nside1=2,4,2
            i2 = lblkpt(ii,nside1)
            if(i2.ne.0) then
               nside2= lsidept(ii,nside1)
               call fixmets(nhalo,nside1,nside2,
     &              jmax(ii),kmax(ii),xy(lqptr(ii)),xyj(lgptr(ii)),
     &              jbegin2(ii),jend2(ii),kminbnd(ii),kmaxbnd(ii),
     &              jmax(i2),kmax(i2),xy(lqptr(i2)),xyj(lgptr(i2)),
     &              jbegin2(i2),jend2(i2),kminbnd(i2),kmaxbnd(i2))
            endif
         end do
      end do

c     -- copy halo rows of metrics in eta direction --
      do ii=iblk1,iblk2
         do nside1=1,3,2
            i2 = lblkpt(ii,nside1)
            if(i2.ne.0) then
               nside2= lsidept(ii,nside1)
               call fixmets(nhalo,nside1,nside2,
     &              jmax(ii),kmax(ii),xy(lqptr(ii)),xyj(lgptr(ii)),
     &              jminbnd(ii),jmaxbnd(ii),kbegin2(ii),kend2(ii),
     &              jmax(i2),kmax(i2),xy(lqptr(i2)),xyj(lgptr(i2)),
     &              jminbnd(i2),jmaxbnd(i2),kbegin2(i2),kend2(i2))
            endif
         end do
      end do
      

c ********************************************************************
c       option to restart previously stored solution from tape3
c       variables read in without jacobian scaling                
c ********************************************************************
      if (restart .and. ifirst.eq.1) then

cMPR     set everything to 10^4 to avoid problems with zero entries for blunt trailing edges
cMPR     No worries - the really needed values will overwrite this quick fix
         do i = 1,maxjkq
            q(i) = 1.0d4
         end do

         if (unsteady) then
            do i = 1,maxjkq
               qos(i) = 1.0d4
            end do
         end if

         ip = 4
         call ioall(junit,mglev,ip,jdim,kdim,q,qos,press,
     &        sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,y,
     &        coef2x,coef4x,coef2y,coef4y)

c$$$c        -- set LL and UL corner of Q-vector to 10^4 for blunt trailing edge
c$$$         do i=1,nblks
c$$$            if (ibctype(i,2) .eq. 1) then
c$$$               ib = i
c$$$               call fix_q(ib,nhalo,jmax(ib),kmax(ib),nmax,q(lqptr(ib)))              
c$$$               if (unsteady) call fix_q(ib,nhalo,jmax(ib),kmax(ib),nmax,
c$$$     &              qos(lqptr(ib)))
c$$$
c$$$               ib = lblkpt(i,3)
c$$$               if (ib.ne.0) then
c$$$                  call fix_q(ib,nhalo,jmax(ib),kmax(ib),
c$$$     &                 nmax,q(lqptr(ib)))
c$$$                  if (unsteady) call fix_q(ib,nhalo,jmax(ib),kmax(ib),
c$$$     &                 nmax,qos(lqptr(ib)))
c$$$               endif
c$$$
c$$$               ib = lblkpt(i,1)
c$$$               if (ib.ne.0) then
c$$$                  call fix_q(ib,nhalo,jmax(ib),kmax(ib),
c$$$     &                 nmax,q(lqptr(ib)))
c$$$                  if (unsteady) call fix_q(ib,nhalo,jmax(ib),kmax(ib),
c$$$     &                 nmax,qos(lqptr(ib)))
c$$$               endif
c$$$            endif
c$$$         enddo
                
         call halo_q(iblk1,iblk2,jdim,kdim,q,turre)
         
         if (unsteady) then
            call halo_q(iblk1,iblk2,jdim,kdim,qos,turos)
         end if
      endif                                                             

c ********************************************************************
c     scale variables by jacobian stored by xymets in xyj(j,k)
c ********************************************************************
      if (unsteady) then
         do ii=iblk1,iblk2
            call scjac(ii,jmax(ii),kmax(ii),qos(lqptr(ii)),
     &           xyj(lgptr(ii)),
     &           jbegin2(ii),jend2(ii),kbegin2(ii),kend2(ii))
         end do
      end if

      do ii=iblk1,iblk2
         call scjac(ii,jmax(ii),kmax(ii),q(lqptr(ii)),xyj(lgptr(ii)),
     &        jbegin2(ii),jend2(ii),kbegin2(ii),kend2(ii))
      end do


c     ********************************************************************
c     initialize press,sndsp
c     
c     ********************************************************************
      do ii=iblk1,iblk2
         call calcps(ii,jmax(ii),kmax(ii),q(lqptr(ii)),
     &        press(lgptr(ii)),sndsp(lgptr(ii)),
     &        precon(lprecptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &        jbegin2(ii),jend2(ii),kbegin2(ii),kend2(ii))
      end do     

c     -- logic to warm start optimization run -- 
c     -- this avoids scaling updates in bcbody.f --
c     -- also prevents turre resets in spalart_allmaras.f --
      if ( ioptm.gt.1 .and. ifirst.ne.1 ) restart = .true.

      return                                                            
      end                       !initia
