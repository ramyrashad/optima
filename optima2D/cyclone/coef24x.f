c ##########################
c ##                      ##
c ##  subroutine coef24x  ##
c ##                      ##
c ##########################
c
      subroutine coef24x (jdim,kdim,coef2x,coef4x,pcoef,
     &                                         spect,coef2,coef4)
c
c*****************************************************************
c   coef2x comes in as coefx (pressure gradiant coefficient)
c   coef4x comes in as specx (spectral radius)
c*****************************************************************
cCO   coef2x(j,k) returns d^(2)_{j+1/2, k}
cCO   coef4x(j,k) returns d^(4)_{j+1/2, k}

      use disscon_vars
#include "../include/arcom.inc"
c
      dimension coef2x(jdim,kdim),coef4x(jdim,kdim)
      dimension pcoef(jdim,kdim),spect(jdim,kdim)
      dimension coef2(jdim,kdim),coef4(jdim,kdim)
c
c
      do 2 k = kbegin,kend
      do 2 j = jbegin,jend
         pcoef(j,k) = coef2x(j,k)
         spect(j,k) = coef4x(j,k)
         spectx_dc(j,k) = coef4x(j,k)
 2    continue
c
      eps4x = dis4x
      eps2x = dis2x
c
c
      if (prec.eq.0) then
         
c----    form 2nd and 4th order dissipation coefficients in x ----
         do 20 k = kbegin,kend
         do 20 j = jbegin,jup
            jpl = jplus(j)
            c2 = eps2x*(pcoef(jpl,k)*spect(jpl,k)+pcoef(j,k)*spect(j,k))
            c4 = eps4x*(spect(jpl,k) + spect(j,k))
            c4 = c4 - min(c4,c2)
            coef2x(j,k) = c2
            coef4x(j,k) = c4
 20      continue
              
      else
c----    form 2nd and 4th order dissipation coefficients in x ----
c
c        Why the two sets of coefficients/switches?
c        Well, Dave's version of preconditioning does not compute Gamma
c        at the half points.  So when we decided to do so, only the right
c        hand side was changed.  So the way stepf2dx.f and stepf2dy.f 
c        wants the coefficients computed is the old way.  
c        Rather than change those routines as well, I just made two 
c        sets of coefficients ...
c        one set for the RHS and the other for the LHS.
c        If you look at matrix dissipation, only one set is used but you
c        need to modify stepf2dx.f and stepf2dy.f accordingly.
c
         do 40 k = klow,kup
         do 40 j = jbegin,jend
            coef2(j,k)=eps2x*pcoef(j,k)*spect(j,k)
            coef4(j,k)=eps4x*spect(j,k)
 40      continue
c
         do 55 k=klow,kup
            do 50 j=jbegin,jup
               jp1=jplus(j)
               c2=coef2(j,k)+coef2(jp1,k)
               c4=coef4(j,k)+coef4(jp1,k)
               c4 = c4 - min(c4,c2)
               coef4(j,k)=coef4(j,k)-min(coef2(j,k),coef4(j,k))
               coef2x(j,k) = c2
               coef4x(j,k) = c4
 50         continue
            coef4(jend,k)=coef4(jend,k)-min(coef2(jend,k),coef4(jend,k))
 55      continue
      endif
c
      return
      end

