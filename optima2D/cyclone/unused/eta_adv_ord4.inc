c        **********************
c        Advective Terms in Eta
c        **********************

         zz=1.d0/6.d0
         do k=klow+1,kup-1
         km2=k-2
         km1=k-1
         kp1=k+1
         kp2=k+2
         do j=jlow,jup
            sgnu = sign(1.,vv(j,k))
            app  = .5d0*(1.+sgnu)
            apm  = .5d0*(1.-sgnu)
c
            tm2=turre(j,km2)
            tm1=turre(j,km1)
            tt =turre(j,k)
            tp1=turre(j,kp1)
            tp2=turre(j,kp2)
c
            fy(j,k) = fy(j,k) - vv(j,k)*(
c
c          -second-order upwinding
c     &      app*(tm2-4.d0*tm1+3.d0*tt)
c     &     +apm*(-3.d0*tt+4.d0*tp1-tp2))*0.5d0
c
c          -third-order biased upwinding
     &     app*(tm2 - 6.d0*tm1 + 3.d0*tt + 2.d0*tp1)
     &    + apm*(-2.d0*tm1 - 3.d0*tt + 6.d0*tp1 -tp2))*zz
c
            by(j,k)   = by(j,k)   - vv(j,k)*app
            cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
            dy(j,k)   = dy(j,k)   + vv(j,k)*apm
         enddo
         enddo

         k=klow
         do j=jlow,jup

            tm1=turre(j,k-1)
            tt =turre(j,k)
            tp1=turre(j,k+1)
            tp2=turre(j,k+2)

            sgnu = sign(1.,vv(j,k))
            app  = .5*(1.+sgnu)
            apm  = .5*(1.-sgnu)
c            app=0.
c            apm=1.
            fy(j,k) = fy(j,k) - vv(j,k)*(
c           -first-order / third-order mix upwinding
c     &            app*(-3.d0*tm1+4.d0*tt-tp1)*.5d0
     &            app*(tt-tm1)
     &          + apm*(tp1-tt))
c     &          + apm*(-2.d0*tm1 - 3.d0*tt + 6.d0*tp1 -tp2)*zz)
c
            by(j,k)   = by(j,k)   - vv(j,k)*app
            cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
            dy(j,k)   = dy(j,k)   + vv(j,k)*apm
         enddo
c
c
         k=kup
         do j=jlow,jup

            tm2=turre(j,k-2)
            tm1=turre(j,k-1)
            tt =turre(j,k)
            tp1=turre(j,k+1)

            sgnu = sign(1.,vv(j,k))
            app  = .5*(1.+sgnu)
            apm  = .5*(1.-sgnu)
c            app  = 1.
c            apm  = 0.
            fy(j,k) = fy(j,k) - vv(j,k)*(
c           -first-order / third-order mix upwinding
c     &             app*(tm2 - 6.d0*tm1 + 3.d0*tt + 2.d0*tp1)*zz
     &             app*(tt-tm1)
     &           + apm*(tp1-tt))
c     &           + apm*(tm1-4.d0*tt+3.d0*tp1)*.5d0)
c
            by(j,k)   = by(j,k)   - vv(j,k)*app
            cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
            dy(j,k)   = dy(j,k)   + vv(j,k)*apm
         enddo
c
