c----------------------------------------------------------------------
c     -- construct dR/dQ and preconditioner --
c     -- m. nemec, july 2001
c----------------------------------------------------------------------
      subroutine get_lhs ( nmax, q, press, sndsp, xy, xyj, precon, xit,
     &     ett, fmu, x, y, turmu, vort, turre, vk, ve, coef4x, coef4y,
     &     uu, vv, ccx, ccy, coef2x, coef2y, indx, pdc, pa, ipa, as, ia,
     &     icol, iex, bcf, ntvar, precmat, jacmat, imps)  

      implicit none

#include "../include/parms.inc"

      double precision q(*),       press(*),     sndsp(*)
      double precision xy(*),      xyj(*),       precon(*)
      double precision xit(*),     ett(*),       fmu(*)
      double precision x(*),       y(*),         turmu(*)
      double precision vort(*),    turre(*),     vk(*) 
      double precision ve(*),      coef4x(*),    coef4y(*)
      double precision uu(*),      vv(*),        ccx(*)
      double precision ccy(*),     coef2x(*),    coef2y(*)
      double precision pa(*),      as(*),        pdc
      integer bcf(maxjb,4,4,mxbfine),icol(*),ia(*),j,jj,ii,jcol    
      integer iex(maxjb,4,4,mxbfine),ipa(*),indx(*),jdel,nmax,ntvar
      logical precmat,jacmat,imps

c     -- zero sparse jacobian matrix as --
      if ( jacmat ) then
         do j = 1,maxjk*jacb2*9
            as(j) = 0.0
         end do
      end if

c     -- zero preconditioner matrix pa --
      if ( precmat ) then
         do j = 1,maxjk*jacb2*5
            pa(j) = 0.0
         end do 
      end if

c     write (*,*) 'going drdq'
c     call flush (6)
c     -- form second order flow jacobian and preconditioner --
      call dRdQ ( nmax, q, press, sndsp, xy, xyj, precon, xit,
     &     ett, fmu, x, y, turmu, vort, turre, vk, ve, coef4x, coef4y,
     &     uu, vv, ccx, ccy, coef2x, coef2y, indx, pdc, pa, as, icol,
     &     iex, bcf, precmat, jacmat, imps)
c     write (*,*) 'done drdq'
c     call flush (6)

      if ( jacmat ) then
c     -- sort jacobian array --
         jcol = 1
         do ii = 1,ntvar
            jj = ( ii-1 )*icol(9)
            jdel = ia(ii+1) - ia(ii)
            do j = jj+1,jj+jdel
               as(jcol) = as(j)
               jcol = jcol + 1
            end do
         end do
      end if

      if ( precmat ) then
c     -- sort preconditioner array --
         jcol = 1
         do ii = 1,ntvar
            jj = ( ii-1 )*icol(5)
            jdel = ipa(ii+1) - ipa(ii)
            do j = jj+1,jj+jdel
               pa(jcol) = pa(j)
               jcol = jcol + 1
            end do
         end do
      end if

      return	
      end                       !get_lhs
