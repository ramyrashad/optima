c ********************************************************************
c ** LU Factorization --- for pentadiagonal systems                 **
c ** x sweep routine                                                **
c ** Crout's method of factorization applied to pentadiagonals      **
c ********************************************************************
      subroutine xludcmp(dm2,dm1,dd,dp1,dp2,jmax,kmax,
     &                   jlow,jup,klow,kup)
c ********************************************************************
c Variables:   dm2: vector 2 below diagonal
c              dm1: vector 1 below diagonal    
c              dd : vector of diagonal entries
c              dp1: vector 1 above diagonal
c              dp2: vector 2 above diagonal
c ********************************************************************
      dimension dm2(jmax,kmax), dm1(jmax,kmax), dd(jmax,kmax)
      dimension dp1(jmax,kmax), dp2(jmax,kmax) 
      do 199 k=klow,kup
c jlow
         j = jlow
         dm1(j+1,k) = dm1(j+1,k)/dd(j,k)
         dm2(j+2,k) = dm2(j+2,k)/dd(j,k)
c jlow+1        
         j = jlow + 1
         dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j-1,k)
         dm1(j+1,k) = (dm1(j+1,k)-dm2(j+1,k)*dp1(j-1,k))/dd(j,k)
         dm2(j+2,k) = dm2(j+2,k)/dd(j,k)
c ** Main Loop *******************************************************
         do 120 j = jlow+2,jup-2
c                                                        /* beta's  */
           dp1(j-1,k) = dp1(j-1,k) - dm1(j-1,k)*dp2(j-2,k)
           dd(j,k) = dd(j,k)-dm2(j,k)*dp2(j-2,k)-dm1(j,k)*dp1(j-1,k)
c                                                        /* alpha's */
           dm1(j+1,k) = (dm1(j+1,k)-dm2(j+1,k)*dp1(j-1,k))/dd(j,k)
           dm2(j+2,k) = dm2(j+2,k)/dd(j,k)
  120    continue
c ** Main Loop *******************************************************
c jup-1
         j = jup - 1
         dp1(j-1,k) = dp1(j-1,k) - dm1(j-1,k)*dp2(j-2,k)
         dd(j,k) = dd(j,k)-dm2(j,k)*dp2(j-2,k)-dm1(j,k)*dp1(j-1,k)
         dm1(j+1,k) = (dm1(j+1,k)-dm2(j+1,k)*dp1(j-1,k))/dd(j,k)
c jup
         j = jup 
         dp1(j-1,k) = dp1(j-1,k) - dm1(j-1,k)*dp2(j-2,k)
         dd(j,k) = dd(j,k)-dm2(j,k)*dp2(j-2,k)-dm1(j,k)*dp1(j-1,k)
  199 continue
      return
      end
