      subroutine dcldq( jdim,kdim,q,press,x,y,xy,xyj,                    
     *         nscal, dcltdq)                     
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim)                         
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension dpdq(jdim,4), dzdq(jdim,4), dclidq(jdim,4)
      dimension dccdq(jdim,4),dcndq(jdim,4)
      dimension dcnvdq(jdim,kdim,4),dccvdq(jdim,kdim,4)
      dimension dcltdq(jdim,kdim,4),dclvvdq(jdim,kdim,4)

      dimension dcfavjm1dq(jdim,kdim,4), dcfavjdq(jdim,kdim,4)
      dimension dcfavjp1dq(jdim,kdim,4), dcfavjp2dq(jdim,kdim,4)
c                                                                       
      double precision dcldqFD(jdim,kdim,4)
      double precision qtemp(jdim,kdim,4)
c                                                                       
      dimension z(maxj),work(maxj,29)
      common/worksp/z,work

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c  dpdq(j,n) - partial derivative of pressure on the surface of the airfoil with
c    respect to the flow variable q(j,1,n).
c  dzdq(j,n) - partial derivative of z (from clcd.f) with
c    respect to the flow variable q(j,1,n).
c  dclidq(j,n) - partial derivative of coefficient of lift due to pressure wrt
c    flow variable q(j,1,n).
c  dccdq(j,n) - partial derivative of chord directed force coefficient due to 
c    pressure wrt flow variable q(j,1,n).
c  dcndq(j,n) - partial derivative of normal force coefficient due to 
c    pressure wrt flow variable q(j,1,n).
c  dcltdq(j,k,n) - partial derivative of total coefficient of lift wrt flow
c    variable q(j,k,n), calculated analytically.
c  dclvvdq(j,k,n) - partial derivative of viscous coefficient of lift wrt flow
c    variable q(j,k,n).
c  dcfavjm1dq(j,k,n), dcfavjdq(j,k,n), dcfavjp1(j,k,n), and dcfavjp2(j,k,n) are
c    the partial derivatives of cfav (from clcd.f) taken at j-1, j, j+1, and j+2 
c    respectively wrt the flow variable q(j,k,n).
c  dcldqFD(j,k,n) - partial derivative of coefficient of lift wrt flow variable
c    q(j,k,n), calculated by finite differences.  Used for testing and 
c    comparison.
c

c                                                                       
c     the parameter nscal determines whether the q variables are scaled 
c     by the metric jacobians xyj.                                      
c 

      if( nscal .eq.0) then
         do n = 1,4
            do k = 1,kend
               do j = 1,jend
                  qtemp(j,k,n) = q(j,k,n)
                  q(j,k,n) = q(j,k,n)*xyj(j,k)
clb               this gives q without the jacobian xyj.  This is Q instead of Q^.  
               end do
            end do
         end do
      end if

      
c                        
      cpc = 2.d0/(gamma*fsmach**2)                             
c                                                                       
      do j=1,jdim
         do n=1,4
            dpdq(j,n)=0.d0
            dcndq(j,n)=0.d0
            dccdq(j,n)=0.d0
            do k=1,kdim
               dcnvdq(j,k,n)=0.d0
               dccvdq(j,k,n)=0.d0
               dclvvdq(j,k,n)=0.d0

               dcfavjm1dq(j,k,n) = 0.d0
               dcfavjdq(j,k,n) = 0.d0
               dcfavjp1dq(j,k,n) = 0.d0
               dcfavjp2dq(j,k,n) = 0.d0
            end do
         end do
      end do
c                                                                       
c  set limits of integration                                            
c                                                                       
      j1 = jlow                                                         
      j2 = jup                                                          
      jtp = jlow                                                        
      if(.not.periodic)then                                             
         j1 = jtail1                                                       
         j2 = jtail2                                                       
         jtp = jtail1+1                                                    
      endif                                                             
c                                                                       
c     compute cp at grid points and store in z array     
c                                                                       
      do j=j1,j2                                                     
clb      dpdq is the derivative of p wrt q.  Second index is n.
         dpdq(j,1)=0.5*gami*(q(j,1,2)**2+q(j,1,3)**2)
     &               /q(j,1,1)**2
         dpdq(j,2)=-gami*q(j,1,2)/q(j,1,1)
         dpdq(j,3)=-gami*q(j,1,3)/q(j,1,1)
         dpdq(j,4)=gami

         do n=1,4
            dzdq(j,n)=dpdq(j,n)*gamma*cpc
         end do
      end do
c                                                                       
c     compute normal force coefficient and chord directed force coeff     
c     chord taken as one in all cases
c     
c                                                                       
c      cn = 0.                                                           
c      cc = 0.                                                           
c    
c     For periodic flows:
c     -going from interval contained between (jmax,1), then (1,2)
c      and ending on (jmax-1,jmax). Note: jlow=1,jup=jmax,jmax=jmaxold-1
c
      do j=jtp,j2-1                                                    
c        jm1 = jminus(j)                                                
c        cpav = (z(j) + z(jm1))*.5   
         do n=1,4
            dcndq(j,n) = -dzdq(j,n)*0.5*((x(j,1)-x(j-1,1))
     &           +(x(j+1,1)-x(j,1)))                         
            dccdq(j,n) = dzdq(j,n)*0.5*((y(j,1)-y(j-1,1))
     &           +(y(j+1,1)-y(j,1))) 
            dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &                     -sin(alpha*pi/180.d0)*dccdq(j,n)
         end do
c        cmle = cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))    
c
c        Tornado does it this way.
c         cmle = cmle + cpav*
c     &    (  (x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1)) +                  
c     &       (y(j,1)+y(jm1,1))*.5*(y(j,1) -y(jm1,1))   )                
c
      end do
      j=j1
      do n=1,4
         dcndq(j,n) = -dzdq(j,n)*0.5*(x(j+1,1)-x(j,1))
         dccdq(j,n) = dzdq(j,n)*0.5*(y(j+1,1)-y(j,1))
         dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &        -sin(alpha*pi/180.d0)*dccdq(j,n)
      end do
      j=j2
      do n=1,4
         dcndq(j,n) = -dzdq(j,n)*0.5*(x(j,1)-x(j-1,1))
         dccdq(j,n) = dzdq(j,n)*0.5*(y(j,1)-y(j-1,1))
         dclidq(j,n) = cos(alpha*pi/180.d0)*dcndq(j,n)
     &        -sin(alpha*pi/180.d0)*dccdq(j,n)
      end do
      
                                                                       
      if (viscous) then                                                 
c        write (*,*)  ' in viscous'
c                                                                       
c     viscous coefficent of friction calculation                           
c     taken from p. buning                                               
c                                                                       
c     calculate the skin friction coefficient                             
c                                                                       
c      c  = tau   /            2                                        
c       f      w / 1/2*rho   *u                                         
c                         inf  inf                                      
c                                                                       
c      tau  = mu*(du/dy-dv/dx)                                          
c         w                   w                                         
c                                                                       
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc., 
c     york, 1974, p. 50, but use freestream values instead of edge values.
c                                                                       
c                                                                       
c     for calculating cf, we need the coefficient of viscosity, mu.  use  
c     re = (rhoinf*uinf*length scale)/mu.  also assume cinf=1, rhoinf=1.  
c     
         cinf  = 1.                                                        
         alngth= 1.                                                        
c     re already has fsmach scaling                                       
         amu   = rhoinf*alngth/re                                          
         uinf2 = fsmach**2                                                 
c     
         k = 1                                                              
         ja = jlow                                                         
         jb = jup                                                          
c     
         if(.not.periodic)then 

clb   ********************************************************************                                         
            j = jtail1                                                  
            jp1 = j+1
            
            xixj = xy(j,k,1)                                             
            xiyj = xy(j,k,2)                                             
            etaxj = xy(j,k,3)                                            
            etayj = xy(j,k,4)
            xixjp1 = xy(jp1,k,1)
            xiyjp1 = xy(jp1,k,2)
            
            dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &           -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &           +q(j,k,2)/(q(j,k,1)**2)*xiyj
     &           +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &           -q(j,k,3)/(q(j,k,1)**2)*xixj
     &           -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
            dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &           -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
            
            dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &           -1.d0/q(j,k,1)*xiyj
     &           -1.5d0/q(j,k,1)*etayj
            dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
            
            dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &           +1.d0/q(j,k,1)*xixj
     &           +1.5d0/q(j,k,1)*etaxj
            dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
            
            dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)*etayj
     &           +2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
            
            dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
            
            dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
            
            dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)*etayj
     &           -.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
            
            dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
            
            dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj


clb   ********************************************************************
            
            j=jtail1+1
            jp1 = j+1
            jm1 = j-1
            
            xixjm1 = xy(jm1,k,1)
            xiyjm1 = xy(jm1,k,2)
            xixj = xy(j,k,1)                                             
            xiyj = xy(j,k,2)                                             
            etaxj = xy(j,k,3)                                            
            etayj = xy(j,k,4)
            xixjp1 = xy(jp1,k,1)
            xiyjp1 = xy(jp1,k,2)
            
            dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &           -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &           -q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &           +q(j,k,3)/(q(j,k,1)**2)*xixjm1
            dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &           -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &           +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &           -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
            dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &           -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
            
            dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj
     &           +1.d0/q(j,k,1)*xiyjm1
            dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &           -1.5d0/q(j,k,1)*etayj
            dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
            
            dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj
     &           -1.d0/q(j,k,1)*xixjm1
            dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &           +1.5d0/q(j,k,1)*etaxj
            dcfavjp2dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
            
            dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)*etayj
     &           +2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
            dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)*etayj
     &           +2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
            
            dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
            dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
            
            dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
            dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
            
            dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)*etayj
     &           -.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
            dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)*etayj
     &           -.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
        
            dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
            dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
            
            dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
            dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj


clb   ********************************************************************     
            j = jtail2-1 
            jp1 = j+1
            jm1 = j-1
            
            xixjm1 = xy(jm1,k,1)
            xiyjm1 = xy(jm1,k,2)
            xixj = xy(j,k,1)                                             
            xiyj = xy(j,k,2)                                             
            etaxj = xy(j,k,3)                                            
            etayj = xy(j,k,4)
            xixjp1 = xy(jp1,k,1)
            xiyjp1 = xy(jp1,k,2)
            
            dcfavjp1dq(j,k,1) = q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &       -q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &           +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &           -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
            dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &           -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &           -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &           +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
            dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &           +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
            
            dcfavjp1dq(j,k,2) = -1.d0/q(j,k,1)*xiyjp1
     &           -1.5d0/q(j,k,1)*etayj
            dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj+.5d0/q(j,k,1)*xiyjm1
            dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
            
            dcfavjp1dq(j,k,3) = 1.d0/q(j,k,1)*xixjp1
     &           +1.5d0/q(j,k,1)*etaxj
            dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj-.5d0/q(j,k,1)*xixjm1
            dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
            
            dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)*etayj
     &           +2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
            dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)*etayj
     &           +2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj

            dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
            dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
            
            dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
            dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
            
            dcfavjp1dq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)*etayj
     &           -.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
            dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)*etayj
     &           -.5d0*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
            
            dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
            dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
            
            dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
            dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj

clb   ********************************************************************
            
            j = jtail2                                                  
            jp1 = j+1
            jm1 = j-1
            
            xixjm1 = xy(jm1,k,1)
            xiyjm1 = xy(jm1,k,2)
            xixj = xy(j,k,1)                                             
            xiyj = xy(j,k,2)                                             
            etaxj = xy(j,k,3)                                            
            etayj = xy(j,k,4)
            xixjp1 = xy(jp1,k,1)
            xiyjp1 = xy(jp1,k,2)
            
            dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &           +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
            dcfavjdq(j,k,1) = -q(j,k,2)/(q(j,k,1)**2)*xiyj
     &           +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &           +q(j,k,3)/(q(j,k,1)**2)*xixj
     &           -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &           -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &           +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
            
            dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
            dcfavjdq(j,k,2) = 1.d0/q(j,k,1)*xiyj
     &           -1.5d0/q(j,k,1)*etayj
     &           +.5d0/q(j,k,1)*xiyjm1
            
            dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
            dcfavjdq(j,k,3) = -1.d0/q(j,k,1)*xixj
     &           +1.5d0/q(j,k,1)*etaxj
     &           -.5d0/q(j,k,1)*xixjm1
            
            dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)*etayj
     &           +2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
            dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
            dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
            
            dcfavjdq(j,k+2,1) = .5d0*q(j,k+2,2)/(q(j,k+2,1)**2)*etayj
     &           -.5d0*q(j,k+2,3)/q(j,k+2,1)**2*etaxj
            dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
            dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
            

clb   ********************************************************************                            
c                                                                       
c     -set new limits                                                       
            ja = jtail1+2                                               
            jb = jtail2-2                                               
         endif                                                          
c                                                                       
         do j = ja,jb                                            
            jp1 = j+1                                            
            jm1 = j-1 
            jm2 = j-2
            
            xixjm1 = xy(jm1,k,1)
            xiyjm1 = xy(jm1,k,2)
            xixj = xy(j,k,1)                                             
            xiyj = xy(j,k,2)                                             
            etaxj = xy(j,k,3)                                            
            etayj = xy(j,k,4)
            xixjp1 = xy(jp1,k,1)
            xiyjp1 = xy(jp1,k,2)
            
clb   For tidier calculations, use cfav derivatives first
            dcfavjm1dq(j,k,1) = -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &           +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
            dcfavjdq(j,k,1) = 1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &           -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
     &           -.5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjm1
     &           +.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjm1
            dcfavjp1dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &           -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
     &           +1.5d0*q(j,k,2)/(q(j,k,1)**2)*etayj
     &           -1.5d0*q(j,k,3)/(q(j,k,1)**2)*etaxj
            dcfavjp2dq(j,k,1) = .5d0*q(j,k,2)/(q(j,k,1)**2)*xiyjp1
     &           -.5d0*q(j,k,3)/(q(j,k,1)**2)*xixjp1
            
            dcfavjm1dq(j,k,2) = .5d0/q(j,k,1)*xiyjm1
            dcfavjdq(j,k,2) = -1.5d0/q(j,k,1)*etayj+.5d0/q(j,k,1)*xiyjm1
            dcfavjp1dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
     &           -1.5d0/q(j,k,1)*etayj
            dcfavjp2dq(j,k,2) = -.5d0/q(j,k,1)*xiyjp1
        
            dcfavjm1dq(j,k,3) = -.5d0/q(j,k,1)*xixjm1
            dcfavjdq(j,k,3) = 1.5d0/q(j,k,1)*etaxj-.5d0/q(j,k,1)*xixjm1
            dcfavjp1dq(j,k,3) = .5d0/q(j,k,1)*xixjp1
     &           +1.5d0/q(j,k,1)*etaxj
            dcfavjp2dq(j,k,3) = -.5d0/q(j,k,1)*xixjp1

            dcfavjdq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)*etayj
     &           +2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
            dcfavjp1dq(j,k+1,1) = -2.d0*q(j,k+1,2)/(q(j,k+1,1)**2)*etayj
     &           +2.d0*q(j,k+1,3)/(q(j,k+1,1)**2)*etaxj
            
            dcfavjdq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
            dcfavjp1dq(j,k+1,2) = 2.d0/q(j,k+1,1)*etayj
            
            dcfavjdq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj
            dcfavjp1dq(j,k+1,3) = -2.d0/q(j,k+1,1)*etaxj

            dcfavjdq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)*etayj
     &           -0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
            dcfavjp1dq(j,k+2,1) = 0.5*q(j,k+2,2)/(q(j,k+2,1)**2)*etayj
     &           -0.5*q(j,k+2,3)/(q(j,k+2,1)**2)*etaxj
            
            dcfavjdq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
            dcfavjp1dq(j,k+2,2) = -.5d0/q(j,k+2,1)*etayj
            
            dcfavjdq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj
            dcfavjp1dq(j,k+2,3) = .5d0/q(j,k+2,1)*etaxj


c     
         end do
c      write (*,*) 'done 110'
clb      if (sngvalte) then
clbc       -average trailing edge value
clb        z(j1)=.5d0*(z(j1)+z(j2))
clb        z(j2)=z(j1)
clb      endif
c                                                                       
c     Compute viscous normal and axial forces                             
c                                                                       
c     -compute normal force coefficient and chord directed force coeff     
c     -chord taken as one in all cases                                      
c                                                                       
                      
         do j=j1,j2
            do n = 1,3
               do k = 1,3
                  dcnvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                 dcfavjm1dq(j,k,n)*(y(j-1,1)-y(j-2,1))
     &                 + dcfavjdq(j,k,n)*(y(j,1)-y(j-1,1))
     &                 + dcfavjp1dq(j,k,n)*(y(j+1,1)-y(j,1))
     &                 + dcfavjp2dq(j,k,n)*(y(j+2,1)-y(j+1,1)))
                  dccvdq(j,k,n) = amu/(rhoinf*uinf2)*(
     &                 dcfavjm1dq(j,k,n)*(x(j-1,1)-x(j-2,1))
     &                 + dcfavjdq(j,k,n)*(x(j,1)-x(j-1,1))
     &                 + dcfavjp1dq(j,k,n)*(x(j+1,1)-x(j,1))
     &                 + dcfavjp2dq(j,k,n)*(x(j+2,1)-x(j+1,1)))

                  dclvvdq(j,k,n)=dcnvdq(j,k,n)*cos(alpha*pi/180.d0)
     &              -dccvdq(j,k,n)*sin(alpha*pi/180.d0)
               end do
            end do
         end do
      end if
      
      do j=j1,j2                                                   
         do k=1,3
            do n=1,4
               if (k .eq. 1) dcltdq(j,k,n) = dclidq(j,n)
               if (viscous) then
                  if (k.eq.1) dcltdq(j,k,n)=dcltdq(j,k,n)+dclvvdq(j,k,n)
                  if (k.ne.1) dcltdq(j,k,n)=dclvvdq(j,k,n)
               endif
               dcltdq(j,k,n)=dcltdq(j,k,n)*xyj(j,k)
clb        need to multiply by xyj to get dcldq^ rather than dcldq.
            end do
         end do
      end do
c     
      


clbclb   TEMPORARY CALCULATION OF DCLDQ BY FINITE DIFFERENCING
clb
clbclb      nscal=1
clb      if( nscal .eq.0) then
clb         do n = 1,4
clb            do k = 1,kend
clb               do j = 1,jend
clb                  q(j,k,n) = qtemp(j,k,n)
clb                  dcldqFD(j,k,n) = 0.d0
clb               end do
clb            end do
clb         end do
clb      end if 
clb 
clb       if (.not. viscous) then
clb          do n = 1,4
clb             do j = jtail1,jtail2
clb                tmp = q(j,1,n)
clb                stepsize = fd_eta*tmp
clb                
clb               if ( dabs(stepsize) .lt. 1.d-9 ) then
clb                  stepsize = 1.d-9*dsign(1.d0,stepsize)
clbc     write (opt_unit,20) 
clbc     write (opt_unit,30) j, k, n, tmp, stepsize
clb               end if
clb
clb               q(j,1,n) = q(j,1,n) + stepsize
clb               stepsize = q(j,1,n) - tmp
clb               pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
clb     &              **2)/q(j,1,1) )
clb               press_tmp = press(j,1)
clb               press(j,1) = pp
clb
clb               call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
clb     &              cli, cdi, cmi, clv, cdv, cmv)
clb               cdtp = cdi + cdv
clb               cltp = cli + clv
clb               objp = cltp
clb
clbclb               write(*,*) 'cltp',cltp
clb
clb               press(j,1) = press_tmp
clb               q(j,1,n) = tmp - stepsize
clb               pp = gami*( q(j,1,4) - 0.5d0*( q(j,1,2)**2 + q(j,1,3)
clb     &              **2)/q(j,1,1) )
clb               press_tmp = press(j,1)
clb               press(j,1) = pp
clb
clb               call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
clb     &              cli, cdi, cmi, clv, cdv, cmv)
clb               cdtm = cdi + cdv
clb               cltm = cli + clv
clb               objm = cltm
clbclb               write(*,*) 'cltm',cltm
clb               
clb               press(j,1) = press_tmp
clb               q(j,1,n) = tmp  
clb               
clb               dcldqFD(j,1,n) = ( objp - objm ) / (2.d0
clb     &              *stepsize)
clb            end do
clb         end do
clb      else
clbc     -- viscous flow --
clb         k=1
clb         do n = 1,4!,3
clbclb            write(*,*) 'n: 1,4,3', n
clb            do j = jtail1,jtail2
clb               tmp = q(j,k,n)
clb               stepsize = fd_eta*tmp
clb               
clb               if ( dabs(stepsize) .lt. 1.d-9 ) then
clb                  stepsize = 1.d-9*dsign(1.d0,stepsize)
clbc     write (opt_unit,20) 
clbc     write (opt_unit,30) j, k, n, tmp, stepsize
clb               end if
clb
clb               q(j,k,n) = q(j,k,n) + stepsize
clb               stepsize = q(j,k,n) - tmp
clb               pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
clb     &              **2 )/q(j,k,1) )
clb               
clb               press_tmp = press(j,k)
clb               press(j,k) = pp
clb               call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
clb     &              cli, cdi, cmi, clv, cdv, cmv)
clb               cdtp = cdi + cdv
clb               cltp = cli + clv
clbclb               write(*,*) 'cltp',cltp
clb               objp =cltp
clb               press(j,k) = press_tmp
clb               
clb               q(j,k,n) = tmp - stepsize
clb               pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k,3)
clb     &              **2 )/q(j,k,1) )
clb               press_tmp = press(j,k)
clb               press(j,k) = pp
clb               call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
clb     &              cli, cdi, cmi, clv, cdv, cmv)
clb               cdtm = cdi + cdv
clb               cltm = cli + clv
clbclb               write(*,*) 'cltm',cltm
clb               objm = cltm
clb               press(j,k) = press_tmp
clb               q(j,k,n) = tmp  
clb
clb               dcldqFD(j,k,n) = ( objp - objm ) / (2.d0
clb     &              *stepsize)
clb            end do
clb         end do                       
clb
clb         do n = 1,4
clb            do k = 2,3
clb               do j = jtail1,jtail2
clb                  tmp = q(j,k,n)
clb                  stepsize = fd_eta*tmp
clb                  
clb                  if ( dabs(stepsize) .lt. 1.d-9 ) then
clb                     stepsize = 1.d-9*dsign(1.d0,stepsize)
clbc     write (opt_unit,20) 
clbc     write (opt_unit,30) j, k, n, tmp, stepsize
clb                  end if
clb
clb                  q(j,k,n) = q(j,k,n) + stepsize
clb                  stepsize = q(j,k,n) - tmp
clb                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
clb     &                 q(j,k,3)**2 )/q(j,k,1) )
clb                  
clb                  press_tmp = press(j,k)
clb                  press(j,k) = pp
clb                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
clb     &                 cli, cdi, cmi, clv, cdv, cmv)
clb                  cdtp = cdi + cdv
clb                  cltp = cli + clv
clbclb                  write(*,*) 'cltp',cltp
clb                  objp = cltp
clb                  press(j,k) = press_tmp
clb                  
clb                  q(j,k,n) = tmp - stepsize
clb                  pp = gami*( q(j,k,4) - 0.5d0*( q(j,k,2)**2 + q(j,k
clb     &                 ,3)**2 )/q(j,k,1) )
clb                  press_tmp = press(j,k)
clb                  press(j,k) = pp
clb                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
clb     &                 cli, cdi, cmi, clv, cdv, cmv)
clb                  cdtm = cdi + cdv
clb                  cltm = cli + clv
clbclb                  write(*,*) 'cltm',cltm
clb                  objm = cltm
clb                  press(j,k) = press_tmp
clb                  q(j,k,n) = tmp  
clb                  
clb                  dcldqFD(j,k,n) = ( objp - objm ) / (2.d0
clb     &                 *stepsize)
clb
clb               end do
clb            end do           
clb         end do
clb      end if
clb
clb      write(*,*) 'j,   k,  n,  dcldq_anal,   dcltdq_FD'
clb
clb      do k=1,3
clb         do j=jtail1,jtail2
clb            do n=1,4
clbclb               write(*,710) j, k, n, dcltdq(j,k,n), dcldqFD(j,k,n)
clb               write(*,*) j, k, dcltdq(j,k,n), dcldqFD(j,k,n)     
clb            end do
clb         end do
clb      end do
clb 710  format (i3, 3x, i2, 3x, i2, 3x, d20.12, 3x, d20.12)
clb      stop
clb
clb      k=1
clb      write(*,*) 'k=', k
clb      do n=1,4
clb         write(*,*) 'n=', n
clb         do j=jtail1,jtail2
clb            write(*,*) dclidq(j,n)*xyj(j,k), dclvvdq(j,k,n)*xyj(j,k), 
clb     &           dcldqFD(j,k,n)
clb         end do
clb      end do
clb      do k=2,3
clb         write(*,*) 'k=', k
clb         do n=1,4
clb            write(*,*) 'n=', n
clb            do j=jtail1,jtail2
clb               write(*,*) dclvvdq(j,k,n)*xyj(j,k), dcldqFD(j,k,n)
clb            end do
clb         end do
clb      end do
clb      stop
clb
clbclb   END CALCULATION OF DCLDQ BY FINITE DIFFERENCING

      if( nscal .eq.0) then
         do n = 1,4
            do k = 1,kend
               do j = 1,jend
                  q(j,k,n) = qtemp(j,k,n)
               end do
            end do
         end do
      end if
c                                                                       
      return                                                            
      end                                                               
