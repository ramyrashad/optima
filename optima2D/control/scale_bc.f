c----------------------------------------------------------------------
c     -- jacobian scaling for b.c. blocks --

c     written by: marian nemec
c     date: july 2000
c----------------------------------------------------------------------
      subroutine scale_bc (igoto, jdim, kdim, ndim, indx, iex, iat,
     &     jat, ast, ipt, jpt, pat, rhs, rhs0, bcf, diag, rhsnamult, 
     &     scalena)

      use disscon_vars
#include "../include/arcom.inc"

      integer jdim, kdim, ndim, len, j, jj, k, n

      integer indx(jdim,kdim), iex(jdim,kdim,4)
clb
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer jpt(jdim*kdim*ndim*ndim*5+1)
      integer iat(jdim*kdim*ndim+2), ipt(jdim*kdim*ndim+2)

      integer idiag(maxjk*nblk+1)

      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)
clb
      double precision diag(jdim*kdim*ndim+1), tmp(4), tmp0(4)
      double precision rhs(jdim,kdim,ndim), bcf(jdim,kdim,4)
      double precision rhsnamult, rhs0(jdim,kdim,ndim)
      logical scalena
      
      if (periodic) then
         na = (jdim-1)*kdim*ndim 
         jsta=jbegin
         jsto=jend
      else
         na = jdim*kdim*ndim
         jsta=jbegin+1
         jsto=jend-1
      end if

      if (clalpha2 .or. clopt) na=na+1
         
      goto (100, 200) igoto

 100  continue

      if (prec_mat .and. scalena) then
c     -- check for zeros on diagonal of jacobian --
         call getdia (na,na,0,pat,jpt,ipt,len,diag,idiag,0)
         do j = 1,jend
            do k = 1,kend
               do n = 1,ndim
                  jk = ( indx(j,k) - 1 )*ndim + n
                  if ( dabs(diag(jk)) .lt. 1.d-10 ) then
                     write (*,10) j, k, n, jk, diag(jk)
                  end if
               end do
            end do
         end do
         if (clopt .and. dabs(diag(na)) .lt. 1.d-10 ) then
            write(*,10) jdim, kdim, ndim, na, diag(na)
         end if
      end if
c     ------------------------------------------------------------------
c     -- shuffle rhs --
c     -- do not do this if solving the adjoint equation --

c     -- 1. airfoil body --
      k = kbegin
      do j = jtail1, jtail2
        do n = 1,4
          tmp0(n) = rhs0(j,k,iex(j,k,n))
          tmp(n) = rhs(j,k,iex(j,k,n))
        end do
        do n = 1,4
          rhs0(j,k,n) = tmp0(n)
          rhs(j,k,n) = tmp(n)
        end do
      end do

c     -- 2. far-field --
      k = kend
      do j = jsta,jsto
        do n=1,4
          tmp0(n) = rhs0(j,k,iex(j,k,n))
          tmp(n) = rhs(j,k,iex(j,k,n))
        end do
        do n = 1,4
          rhs0(j,k,n) = tmp0(n)
          rhs(j,k,n) = tmp(n)
        end do
      end do

      if(.not.periodic) then

      j = jbegin
      do k = 1,kend
        do n = 1,4  
          tmp0(n) = rhs0(j,k,iex(j,k,n))    
          tmp(n) = rhs(j,k,iex(j,k,n))
        end do
        do n = 1,4
          rhs0(j,k,n) = tmp0(n)
          rhs(j,k,n) = tmp(n)
        end do
      end do

      j = jend
      do k = 1,kend
        do n = 1,4
          tmp0(n) = rhs0(j,k,iex(j,k,n))
          tmp(n) = rhs(j,k,iex(j,k,n))
        end do
        do n = 1,4
          rhs0(j,k,n) = tmp0(n)
          rhs(j,k,n) = tmp(n)
        end do
      end do 

      end if

c     ------------------------------------------------------------------

c     -- scale rhs --
c     -- 1. airfoil body --
      k = kbegin
      do n = 1,4
        do j = jtail1, jtail2
          if (dissCon) then
             rhs0(j,k,n) = rhs0(j,k,n)*bcf(j,k,n)
             rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
          else
             rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
          end if
        end do
      end do

c     -- 2. far-field --
      k = kend
      do n=1,4
        do j = jsta,jsto
          if (dissCon) then
             rhs0(j,k,n) = rhs0(j,k,n)*bcf(j,k,n)
             rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
          else
             rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
          end if
        end do
      end do 

      if(.not.periodic) then

      j = jbegin
      do n = 1,4
        do k = 1,kend
          if (dissCon) then
             rhs0(j,k,n) = rhs0(j,k,n)*bcf(j,k,n)
             rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
          else
             rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
          end if
        end do
      end do

      j = jend
      do n = 1,4
        do k = 1,kend
          if (dissCon) then
             rhs0(j,k,n) = rhs0(j,k,n)*bcf(j,k,n)
             rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
          else
             rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
          end if             
        end do
      end do

      end if

c     -- 3. wake-cut --
c      k=1
c      n=4
c      do j = jbegin+1,jtail1-1
c        rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
c      end do
c      do j = jtail2+1,jend-1
c        rhs(j,k,n) = rhs(j,k,n)*bcf(j,k,n)
c      end do

c     ------------------------------------------------------------------

c     -- scale jacobian matrix --
      if ( jac_mat) then
c     -- 1. airfoil body --
        k = kbegin
        do j = jtail1, jtail2
          do n = 1,4
            jk = ( indx(j,k) - 1 )*ndim + n
            dv = bcf(j,k,n)
            jb = iat(jk)
            je = iat(jk+1) - 1
            do ii = jb,je
              ast(ii) = ast(ii)*dv
            end do
          end do
        end do

c     -- 2. far-field --
        k = kend
        do j = jsta,jsto
          do n=1,4
            jk = ( indx(j,k) - 1 )*ndim + n
            dv = bcf(j,k,n)
            jb = iat(jk)
            je = iat(jk+1) - 1
            do ii = jb,je
              ast(ii) = ast(ii)*dv
            end do 
          end do
        end do 

        if(.not.periodic) then

        j = jbegin
        do k = 1,kend
          do n = 1,4
            jk = ( indx(j,k) - 1 )*ndim + n
            dv = bcf(j,k,n)
            jb = iat(jk)
            je = iat(jk+1) - 1
            do ii = jb,je
              ast(ii) = ast(ii)*dv
            end do
          end do
        end do

        j = jend
        do k = 1,kend
          do n = 1,4
            jk = ( indx(j,k) - 1 )*ndim + n
            dv = bcf(j,k,n)
            jb = iat(jk)
            je = iat(jk+1) - 1
            do ii = jb,je
              ast(ii) = ast(ii)*dv
            end do
          end do
        end do

      end if

c     -- 3. wake-cut --
c        k = kend
c        n=4
c        do j = jbegin+1,jtail1-1
c            jk = ( indx(j,k) - 1 )*ndim + n
c            dv = bcf(j,k,n)
c            jb = iat(jk)
c            je = iat(jk+1) - 1
c            do ii = jb,je
c              ast(ii) = ast(ii)*dv
c            end do 
c        end do
c        do j = jtail2+1,jend-1
c            jk = ( indx(j,k) - 1 )*ndim + n
c            dv = bcf(j,k,n)
c            jb = iat(jk)
c            je = iat(jk+1) - 1
c            do ii = jb,je
c              ast(ii) = ast(ii)*dv
c            end do 
c        end do
        rhsnamult = 1.d0

      end if !jac_mat
c     ------------------------------------------------------------------

c     -- scale preconditioner --
      if ( prec_mat ) then
c     -- 1. airfoil body --
        k = kbegin
        do j = jtail1, jtail2
          do n = 1,4
            jk = ( indx(j,k) - 1 )*ndim + n
            dv = bcf(j,k,n)
            jpb = ipt(jk)
            jpe = ipt(jk+1) - 1
            do ii = jpb,jpe
              pat(ii) = pat(ii)*dv
            end do
          end do
        end do

c     -- 2. far-field --
        k = kend
        do j = jsta,jsto
          do n=1,4
            jk = ( indx(j,k) - 1 )*ndim + n
            dv = bcf(j,k,n)
            jpb = ipt(jk)
            jpe = ipt(jk+1) - 1
            do ii = jpb,jpe
              pat(ii) = pat(ii)*dv
            end do   
          end do
        end do  

        if(.not.periodic) then

        j = jbegin
        do k = 1,kend
          do n = 1,4
            jk = ( indx(j,k) - 1 )*ndim + n
            dv = bcf(j,k,n)
            jpb = ipt(jk)
            jpe = ipt(jk+1) - 1
            do ii = jpb,jpe
              pat(ii) = pat(ii)*dv
            end do 
          end do
        end do

        j = jend
        do k = 1,kend
          do n = 1,4
            jk = ( indx(j,k) - 1 )*ndim + n
            dv = bcf(j,k,n)
            jpb = ipt(jk)
            jpe = ipt(jk+1) - 1
            do ii = jpb,jpe
              pat(ii) = pat(ii)*dv
            end do 
          end do
        end do

        end if

c     -- 3. wake-cut --
c        k = kend
c        n = 4
c        do j = jbegin+1,jtail1-1
c            jk = ( indx(j,k) - 1 )*ndim + n
c            dv = bcf(j,k,n)
c            jb = ipt(jk)
c            je = ipt(jk+1) - 1
c            do ii = jb,je
c              pat(ii) = pat(ii)*dv
c            end do 
c        end do
c        do j = jtail2+1,jend-1
c            jk = ( indx(j,k) - 1 )*ndim + n
c            dv = bcf(j,k,n)
c            jb = ipt(jk)
c            je = ipt(jk+1) - 1
c            do ii = jb,je
c              pat(ii) = pat(ii)*dv
c            end do 
c        end do
       
       if (clopt .and. scalena) then
           jk = na
           dv = 1.d0/diag(jk)
           
           rhsnamult = dv

           jb = iat(jk)
           je = iat(jk+1) - 1
           jpb = ipt(jk)
           jpe = ipt(jk+1) - 1
           if(jac_mat) then
              do ii = jb,je
                 ast(ii) = ast(ii)*dv
              end do
           end if
           do ii = jpb,jpe
           end do

        else

           rhsnamult = 1.d0

        end if

      else

         rhsnamult = 1.d0

      end if !prec_mat
c     ------------------------------------------------------------------
      goto 1000

 200  continue

c     -- scaling for adjoint problem --

c     -- check for zeros on diagonal of jacobian --
      call getdia (na,na,0,ast,jat,iat,len,diag,idiag,0)
      do j = 1,jend
        do k = 1,kend
          do n = 1,ndim
            jk = ( indx(j,k) - 1 )*ndim + n
            if ( dabs(diag(jk)) .lt. 1.d-10 ) then
              write (*,10) j, k, n, jk, diag(jk)
              stop
            end if
          end do
        end do
      end do
      if (clopt .and. dabs(diag(na)) .lt. 1.d-10 ) then
         write(*,10) jdim, kdim, ndim, na, diag(na)
         stop
      end if

c     -- scale jacobians and rhs at b.c. blocks --
c     -- 1. airfoil body --
      k = kbegin
      do j = jtail1, jtail2
        do n = 1,ndim
          jk = ( indx(j,k) - 1 )*ndim + n
          dv = 1.d0/diag(jk)

          rhs(j,k,n) = rhs(j,k,n)*dv

          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do
        end do
      end do

c     -- 2. far-field --
      k = kend
      do j = jsta,jsto
        do n=1,ndim
          jk = ( indx(j,k) - 1 )*ndim + n
          dv = 1.d0/diag(jk)

          rhs(j,k,n) = rhs(j,k,n)*dv

          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do 
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do   
        end do
      end do  

      if(.not.periodic) then

      j = jbegin
      do k = 1,kend
        do n = 1,ndim
          jk = ( indx(j,k) - 1 )*ndim + n
          dv = 1.d0/diag(jk)

          rhs(j,k,n) = rhs(j,k,n)*dv

          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do 
        end do
      end do

      j = jend
      do k = 1,kend
        do n = 1,ndim
          jk = ( indx(j,k) - 1 )*ndim + n
          dv = 1.d0/diag(jk)

          rhs(j,k,n) = rhs(j,k,n)*dv

          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do 
        end do
      end do

      end if

c     -- 3. wake-cut --
c      k = kbegin
c      n = 4
c      do j = jbegin+1,jtail1-1
c        jk = ( indx(j,k) - 1 )ndim + n
c        dv = 1.d0/diag(jk)
c 
c        rhs(j,k,n) = rhs(j,k,n)*dv
c 
c        jb = iat(jk)
c        je = iat(jk+1) - 1
c        jpb = ipt(jk)
c        jpe = ipt(jk+1) - 1
c        do ii = jb,je
c          ast(ii) = ast(ii)*dv
c        end do
c        do ii = jpb,jpe
c          pat(ii) = pat(ii)*dv
c        end do 
c      end do
c 
c      do j = jtail2+1,jend-1
c        jk = ( indx(j,k) - 1 )*ndim + n
c        dv = 1.d0/diag(jk)
c 
c        rhs(j,k,n) = rhs(j,k,n)*dv
c 
c        jb = iat(jk)
c        je = iat(jk+1) - 1
c        jpb = ipt(jk)
c        jpe = ipt(jk+1) - 1
c        do ii = jb,je
c          ast(ii) = ast(ii)*dv
c        end do
c        do ii = jpb,jpe
c          pat(ii) = pat(ii)*dv
c        end do 
c      end do

clb    equation with variable angle of attack flow solver
clb    increased jacobian size
      if (clopt .and. scalena) then
        jk = na
        dv = 1.d0/diag(jk)
        
        rhsnamult = dv

        jb = iat(jk)
        je = iat(jk+1) - 1
        jpb = ipt(jk)
        jpe = ipt(jk+1) - 1
        do ii = jb,je
           ast(ii) = ast(ii)*dv
        end do
        do ii = jpb,jpe
           pat(ii) = pat(ii)*dv
        end do 
      else
         rhsnamult = 1.d0
      end if
clb


 10   format ('SCALE_JAC: ',3i5,i8,e12.4)

      goto 1000

 1000 continue

      return
      end                       !scale_bc
