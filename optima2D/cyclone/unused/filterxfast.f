c ##########################
c ##                      ##
c ##  subroutine filterx  ##
c ##                      ##
c ##########################
c
      subroutine filterx (jdim,kdim,q,s,xyj,coef2,coef4,work)            
c
c******************************************************************
c   this subroutine follows very faithfully t. pulliam notes, p.31
c
c   fourth order smoothing, added explicitly to rhs                     
c   second order near shocks with pressure grd coeff.                   
c                                                                       
c  xi direction                                                         
c                                                                       
c   start differences each variable separately                          
c******************************************************************
c                                                                       
      include '../include/arcom.inc'
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension work(jdim,kdim,3)                                       
      do 15 n = 1,4                                                     
c
c
c---- 1st-order forward difference ----
      do 16 k =klow,kup                                                
      do 16 j =jlow,jup                                                
         jpl = jplus(j)                                                    
         work(j,k,1) = q(jpl,k,n)*xyj(jpl,k) - q(j,k,n)*xyj(j,k)           
 16   continue                                                          
c  -- c mesh bc --
      if (.not.periodic) then                                          
         j1 = jbegin                                                    
         j2 = jend                                                      
         do 76 k = klow,kup                                             
            work(j1,k,1) = q(j1+1,k,n)*xyj(j1+1,k) -                       
     >                     q(j1,k,n)*xyj(j1,k)                           
            work(j2,k,1) = work(j2-1,k,1)                                  
 76      continue                                                       
      endif                                                          
c
c
c---- apply cent-dif to 1st-order forward ----
      do 17 k =klow,kup                                                
      do 17 j =jlow,jup                                                
         jpl = jplus(j)                                                    
         jmi = jminus(j)                                                   
         work(j,k,2) = work(jpl,k,1)-2.*work(j,k,1)+work(jmi,k,1)     
 17   continue                                                          
c  -- c mesh bc --
      if (.not.periodic) then                                          
         j1 = jbegin                                                    
         j2 = jend                                                      
         do 77 k =klow,kup                                             
            work(j1,k,2) = q(j1+2,k,n)*xyj(j1+2,k) -                       
     >           2.*q(j1+1,k,n)*xyj(j1+1,k) + q(j1,k,n)*xyj(j1,k)         
            work(j2,k,2) = 0.                                              
 77      continue                                                       
      endif                                                          
c
c
c---- form dissipation term before last differenciation ----
      do 19 k =klow,kup                                                
      do 19 j =jlow,jup                                                
         work(j,k,3) = (coef2(j,k)*work(j,k,1)-coef4(j,k)*work(j,k,2))   
 19   continue                                                          
c  -- c mesh bc --
      if (.not.periodic) then                                          
         j1 = jbegin                                                    
         j2 = jend                                                      
         do 79 k = klow,kup                                             
         work(j1,k,3) = (coef2(j1,k)*work(j1,k,1) -                     
     >                   coef4(j1,k)*work(j1,k,2))                      
         work(j2,k,3) = 0.                                              
79       continue                                                       
         endif                                                          
c
c
c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)                                           
      do 20 k = klow,kup                                                
      do 20 j = jlow,jup                                                
            jmi = jminus(j)                                                   
            s(j,k,n) = s(j,k,n) + (work(j,k,3) - work(jmi,k,3))*dtd           
 20      continue                                                          
c                                                                       
 15   continue                                                          
c
c
      return                                                            
      end                                                               
