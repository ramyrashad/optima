      
      subroutine usrfun
     &   ( Status, n, sn_x,
     &     needF, nF, F,
     &     needG, lenG, G,
     &     cu, lencu, iu, leniu, ru, lenru )

#ifdef _MPI_VERSION
      use mpi
#endif
      implicit
     &     none

#include "../include/mpi_info.inc"
#include "../include/arcom.inc"
#include "../include/optcom.inc"





      !-- Declare all variables

      integer
     &     jdim, kdim, ndim, ifirst, indx(maxj,maxk), icol(9), 
     &     iex(maxjk*4), idv(ibsnc), iren(maxjk), rStat, ip,
     &     ia(maxjk*nblk+2), ifun, iter, nflag, mp, varStat, ruIndex,
     &     ja(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1), 
     &     ipa(maxjk*nblk+2), jpa(maxjk*nblk2*5+1),
     &     iat(maxjk*nblk+2),
     &     jat(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1),
     &     ipt(maxjk*nblk+2), jpt(maxjk*nblk2*5+1), itfirst,
     &     Status, needF, needG, nF, n, lenG, lencu, leniu, lenru,
     &     iu(leniu), i, j, k, l, m, EOF, d10, d1, namelen, sendProc

      double precision
     &     q(maxjk,nblk), cp(maxjk), grad(ibsnc),  
     &     xy(maxjk,4), xyj(maxjk), x(maxj,maxk), y(maxj,maxk),
     &     dx(maxj*maxk*maxincr), dy(maxj*maxk*maxincr), tcon(20),
     &     lambda(2*maxj*maxk*maxincr), vort(maxj,maxk), gCons(20),
     &     turmu(maxj,maxk),  fmu(maxj,maxk), snobj_alpha,
     &     cp_tar(ibody,2), dvs(ibsnc), bt(ibody), bknot(ibskt),
     &     bap(ibody,2), bcp(ibsnc,2), work1(maxjk,100),
     &     as(maxj*maxk*nblk*(nblk*9+1)+maxj*nblk*3+1), 
     &     ast(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1),
     &     pa(maxjk*nblk2*5+1), 
     &     pat(maxjk*nblk2*5+1), cp_his(ibody,300,nopc),
     &     ac_his(ibody,300), opwk(maxjk,8), 
     &     xsave(maxj,maxk), ysave(maxj,maxk), dvscale(ibsnc), 
     &     qtmp(maxjk,nblk,nopc), obj0s(mpopt), obj0, objfun,
     &     lcon, lcons(nopc), dCldx(ibsnc), dCldxs(nopc,ibsnc),
     &     qwarm(warmset,maxj,maxk,nblk,nopc),offdp(noffcon),
     &     dgCdxs(maxtcon,ndv), offCs(50), doffCdxs(50,50), offC,
     &     F(nF), G(lenG), sn_x(n), ru(lenru), diff, doffCdx(50)
 

      character
     &     command*60, cu(lencu)*8

*     ==================================================================
*     Computes the nonlinear objective and constraint terms for the 
*     given optimization problem.
*
*     The triples (g(k),iGfun(k),jGvar(k)), k = 1,2,...,neG, define
*     the sparsity pattern and values of the nonlinear elements
*     of the Jacobian.
*     ==================================================================
      integer
     &     neG, Out, Obj
      double precision
     &     sum
*     ------------------------------------------------------------------
      double precision     two,         three
      parameter           (two = 2.0d+0,three = 3.0d+0)
      double precision     four
      parameter           (four = 4.0d+0)
*     ------------------------------------------------------------------

      !-- If this is the last call to the 'usrfun' subroutine from SNOPT,
      !-- there is no need to do anything, so exit immediately

      if (Status.ge.2) then
         write(scr_unit,9)
 9       format(/3x,'SNOPT is finished SNOPTing...terminating
     & optimization')
         return
      end if

      write(scr_unit,10)
 10   format(
     & 3x,'Beginning function and gradient evaluations in usrfun
     & subroutine')

      !-- Read restart file

      rewind(72)

      read(72) ifirst, ifun, ico, rhoinf, pinf, rest_iter
      read(72) (dvscale(i),i = 1,ibsnc)
      read(72) (alphas(i),i = 1,nopc)
      read(72) (alphas_init(i),i = 1,nopc)
      read(72) (obj0s(i),i = 1,mpopt)
      read(72) (((((qwarm(i,j,k,l,m),i=1,warmset),j=1,maxj),
     &        k=1,maxk),l=1,nblk),m=1,mpopt)    
      read(72) xsave
      read(72) ysave

      !-- Increment restart counter 

      if (opt_restart) rest_iter = rest_iter + 1

      !-- Read variables 'F' and 'G' from file containing the function
      !-- and gradient values used by SNOPT

      rStat = 0

      call snRestart(rStat,F,G,EOF,nF,lenG,ifun,Status)

      !-- If function and gradient values were read from file then skip 
      !-- evaluations in usrfun and give SNOPT the stored values of
      !-- F and G
      
      if (EOF.eq.0) then

         goto 1001
         
      end if
         
      !-- Read in Optima2D variables from SNOPT user variable 'ru'

      varStat = 1

      call varTransfer(varStat,jdim,kdim,ndim,ifirst,ifun,itfirst,
     &      x,y,idv,bap,bcp,bt,bknot,indx,icol,iex,cp,xy,xyj,cp_tar,
     &      fmu,vort,turmu,ia,ja,ipa,jpa,iat,jat,ipt,jpt,as,ast,pa,
     &      pat,ru,ruIndex)

      !-- If this is the 1st time entering 'usrfun' then do a few things:

      if (Status.eq.1) then 

         !-- Initial usrfun counter
         ifun = 0

         !-- Unpack 'dvs' amd dvscale' from user work array 'ru' so 
         !-- they can be used by 'usrfun'

         i = ruIndex

         do j=1,ibsnc
            dvs(j) = ru(i)
            dvscale(j) = ru(ibsnc+i)
            i = i+1
         end do

      end if

      if ((Status.eq.1).or.(opt_restart)) then

         !-- Set file prefixes for a restart
         !-- For the inital run before any restarts, the output and 
         !-- restart file prefixes are 'rest00' and rest00'

#ifdef _MPI_VERSION
         if (rank==0) then
#endif

            !-- Update the output file prefix to reflect the current 
            !-- value of the restart counter

            d10  = rest_iter / 10
            d1   = rest_iter - (d10*10)

            output_file_prefix = 'rest'
            namelen =  len_trim(output_file_prefix)
            output_file_prefix(namelen+1:namelen+2)
     &          = char(d10+48)//char(d1+48)

            !-- Update the restart file prefix to reflect the previous 
            !-- value of the restart counter

            d10  = (rest_iter-1) / 10
            d1   = (rest_iter-1) - (d10*10)

            restart_file_prefix = 'rest'
            namelen =  len_trim(restart_file_prefix)
            restart_file_prefix(namelen+1:namelen+2)
     &          = char(d10+48)//char(d1+48)

            opt_restart = .true.
            obj_restart = .true.

            rewind(prefix_unit)
            write(prefix_unit,*)output_file_prefix
            write(prefix_unit,*)restart_file_prefix
            write(prefix_unit,*)opt_restart
            write(prefix_unit,*)obj_restart
         
            opt_restart = .false.
            obj_restart = .false.

#ifdef _MPI_VERSION
         end if
#endif

      end if

      !-- Increment counter that tracks how many times we have entered
      !-- the 'usrfun' subroutine

      ifun=ifun+1

      
      Out = 15

      Obj = 1                  ! Objective row of F

#ifdef _MPI_VERSION

      !-- Force all processors to wait here until everyone has arrived

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      !-- If the design point assigned to this processor is an 
      !-- 'on-design' point, evaluate its objective function and gradient
      !-- wrt design variables at current design iteration

#ifdef _MPI_VERSION
      if (off_flags(rank+1).eq.(.false.)) then
         if (opt_meth .eq. 11) then
            print *, rank_glob+1, 'INFO: Evaluating on-design point'
         end if
#else
      if (off_flags(1).eq.(.false.)) then
#endif   
         call snObj (objfun, jdim, kdim, ndim, ifirst, itfirst, sn_x, 
     &     iex, q, cp, y, xy, xyj, x, dx, dy, lambda, fmu, vort, turmu,
     &     idv, indx, icol, cp_tar,  bap, bcp, bt, bknot, dvs, cp_his, 
     &     ac_his, ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, 
     &     pat, xsave, ysave, work1, opwk, grad, dvscale, Status, qwarm,
     &     ifun, obj0s, snobj_alpha, needF, needG, lcon, dCldx)   

#ifdef _MPI_VERSION
         if (opt_meth .eq. 11) then
            print *, rank_glob+1, 'INFO: Calculated objective: ', objfun
         end if
#endif

         !-- If flow solver convergence problems or a negative Jacobian
         !-- conditions was detected at the current value of the design
         !-- variables, return to SNOPT and ask for better design 
         !-- variables

         if ((Status.eq.-1) .or. (Status.eq.-2)) then

            !-- Set function and gradient values to zero

            F = 0.0

            do i=1,lenG
               G(i) = 0.0
            end do

            goto 1000
            
         end if

      end if

      !-- If the design point assigned to this processor is an 
      !-- 'off-design' point, evaluate its constraint function and 
      !-- gradient wrt design variables at current design iteration

#ifdef _MPI_VERSION
      if (off_flags(rank+1).eq.(.true.)) then
         if (opt_meth .eq. 11) then
            print *, rank_glob+1, 'INFO: Evaluating off-design point'
         end if
#else
      if (off_flags(1).eq.(.true.)) then
#endif

         call offCon(offC, jdim, kdim, ndim, ifirst, itfirst, sn_x, 
     &     iex, q, cp, y, xy, xyj, x, dx, dy, lambda, fmu, vort, turmu,
     &     idv, indx, icol, cp_tar,  bap, bcp, bt, bknot, dvs, cp_his, 
     &     ac_his, ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, 
     &     pat, xsave, ysave, work1, opwk, doffCdx, dvscale, Status, 
     &     qwarm, ifun, obj0s, objfun, grad, snobj_alpha, needF, needG,
     &     lcon, dCldx)

         if (opt_meth .eq. 11) then
#ifdef _MPI_VERSION
         print *, rank_glob+1, 'INFO: Done evaluating off-design point'
#else
         print *, 'INFO: Done evaluating off-design point'
#endif
         end if

         !-- If flow solver convergence problems or a negative Jacobian
         !-- conditions was detected at the current value of the design
         !-- variables, return to SNOPT and ask for better design 
         !-- variables

         if ((Status.eq.-1) .or. (Status.eq.-2)) then

            !-- Set function and gradient values to zero

            F = 0.0

            do i=1,lenG
               G(i) = 0.0
            end do
            
            goto 1000
            
         end if

      end if

      !-- Scale the composite objective function gradient

      do i=1,ndv
         grad(i) = grad(i)*dvscale(i)
      end do      

      if (noffcon.gt.0) then

#ifdef _MPI_VERSION

         !-- Send off-design constraint function values to processor 
         !-- zero and store in array 'offCs'

         if (off_flags(rank+1).and.rank.ne.0) then
            
            call MPI_SEND(offC, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)            

         end if

         if (rank==0) then

            j = 1 !-- Initialize 'offCs' array counter

            if (off_flags(rank+1)) then
               offCs(1) = offC
               j = j + 1
            end if

            !-- Receive off-design constraint values, but only from
            !-- off-design points

            do i = 2, mpopt

               if (off_flags(i)) then

                  sendProc = i-1

                  call MPI_RECV(offC, 1, MPI_DOUBLE_PRECISION, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

                  offCs(j) = offC

                  j = j + 1 !-- Increment 'offCs' array counter
     
               end if

            end do
      
         end if

         !-- The storage of off-design constraints from all processors 
         !-- done on processor zero overwrites the off-design constraint 
         !-- for processor zero, so restore the correct off-design
         !-- constraint value on processor zero

         if ((rank.eq.0).and.(off_flags(rank+1))) offC = offCs(1)

         !-- Broadcast 'offCs' array to all processors

         call MPI_BCAST(offCs,mpopt,MPI_DOUBLE_PRECISION,0,
     &    COMM_CURRENT,ierr) 

#else

         if (off_flags(1)) then
            
            offCs(1) = offC

         end if

#endif

         !-- Write off-design performance values to data file

#ifdef _MPI_VERSION
         if (rank.eq.0 .and. opt_meth.ne.11) then
#endif

            do i=1,noffcon
               offdp(i) = offCs(i)
            end do

            open(unit=5440,file='off-design-performance.dat',
     &               status='unknown')

            EOF = 0
               
            do while (EOF.eq.0)

               read(5440,5444, IOSTAT = EOF)

            end do

            write(5440,5444) ifun, offdp
 5444       format(i4,50(3x,f12.9)) 

            close(5440)

#ifdef _MPI_VERSION               
         end if
#endif
     
#ifdef _MPI_VERSION

         !-- Send off-design constraint function gradients to processor 
         !-- zero and store in array 'doffCdxs'

         if (off_flags(rank+1)) then
            
            call MPI_SEND(doffCdx, 50, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)            

         end if

         if (rank==0) then

            j = 1 !-- Initialize 'offCs' array counter

            !-- Receive off-design constraint values, but only from
            !-- off-design points

            do i = 1, mpopt

               if (off_flags(i)) then

                  sendProc = i-1

                  call MPI_RECV(doffCdx, 50, MPI_DOUBLE_PRECISION, 
     &            sendProc, 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)
                  
                  do k=1,ndv
                     doffCdxs(j,k) = doffCdx(k)
                  end do

                  j = j + 1 !-- Increment 'doffCdxs' array counter
     
               end if

            end do
      
         end if

         !-- The storage of constraint grads from all processors 
         !-- done on processor zero overwrites the constraint grad
         !-- for processor zero, so restore the correct off-design
         !-- constraint gradient on processor zero

         if ((rank.eq.0).and.(off_flags(rank+1))) then

            do i=1,ndv
               doffCdx(i) = doffCdxs(1,i)
            end do

         end if

         !-- Broadcast 'doffCdxs' array to all processors

         call MPI_BCAST(doffCdxs,2500,MPI_DOUBLE_PRECISION,0,
     &                    COMM_CURRENT,ierr)      
       

#else

         if (off_flags(1)) then

            do k=1,ndv
               doffCdxs(1,k) = doffCdx(k)
            end do
                 
         end if

#endif

      end if

#ifdef _MPI_VERSION

      !-- Force all processors to wait here until the on-design and 
      !-- off-design points have been evaluated

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      !-- Scale the gradients of the off-design constraint 
      !-- functions
     
      do i=1,noffcon
         do j=1,ndv
            doffCdxs(i,j) = doffCdxs(i,j)*dvscale(j)
         end do
      end do

      !-- Store and broadcast lift constraint function and gradient
      !-- values

      if (nlcdm.gt.0) then

#ifdef _MPI_VERSION

         !-- Send lift constraint function values to processor
         !-- zero and store in array 'lcons'

         if (objfuncs(rank+1).eq.19.and.rank.ne.0) then

            call MPI_SEND(lcon, 1, MPI_DOUBLE_PRECISION, 0, 1,
     &                 COMM_CURRENT, ierr)

         end if

         if (rank==0) then

            j = 1 !-- Initialize 'lcons' array counter

            if (objfuncs(1).eq.19) then
               lcons(1) = lcon
               j = j + 1
            end if

            !-- Receive lift constraint values

            do i = 2, mpopt

               if (objfuncs(i).eq.19) then

                  sendProc = i-1

                  call MPI_RECV(lcon, 1, MPI_DOUBLE_PRECISION, sendProc
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

                  lcons(j) = lcon

                  j = j + 1 !-- Increment 'offCs' array counter

               end if

            end do

         end if

         !-- The storage of lift constraints from all processors
         !-- done on processor zero overwrites the lift constraint
         !-- for processor zero, so restore the correct lift
         !-- constraint value on processor zero

         if ((rank.eq.0).and.(objfuncs(1).eq.19)) lcon = lcons(1)

         !-- Broadcast 'lcons' array to all processors

         call MPI_BCAST(lcons,mpopt,MPI_DOUBLE_PRECISION,0,
     &    COMM_CURRENT,ierr)

#else

         if (objfuncs(1).eq.19) then

            lcons(1) = lcon

         end if

#endif

#ifdef _MPI_VERSION
         !-- Send lift constraint  gradients to processor
         !-- zero and store in array 'dCldxs'

         if (objfuncs(rank+1).eq.19.and.rank.ne.0) then

            call MPI_SEND(dCldx, 50, MPI_DOUBLE_PRECISION, 0, 1,
     &                 COMM_CURRENT, ierr)

         end if

         if (rank==0) then

            j = 1 !-- Initialize 'dCldxs' array counter

            if (objfuncs(1).eq.19) then
               do k=1,ndv
                  dCldxs(j,k) = dCldx(k)
               end do
               j = j + 1
            end if

            !-- Receive lift constraint gradients

            do i = 2, mpopt

               if (objfuncs(i).eq.19) then

                  sendProc = i-1

                  call MPI_RECV(dCldx, 50, MPI_DOUBLE_PRECISION,
     &            sendProc, 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

                  do k=1,ndv
                     dCldxs(j,k) = dCldx(k)
                  end do

                  j = j + 1 !-- Increment 'doffCdxs' array counter

               end if

            end do

         end if

         !-- The storage of lift constraint grads from all processors
         !-- done on processor zero overwrites the lift constraint grad
         !-- for processor zero, so restore the correct lift
         !-- constraint gradient on processor zero

         if ((rank.eq.0).and.(objfuncs(1).eq.19)) then

            do i=1,ndv
               dCldx(i) = dCldxs(1,i)
            end do

         end if

         !-- Broadcast 'dCldxs' array to all processors

         call MPI_BCAST(dCldxs,2500,MPI_DOUBLE_PRECISION,0,
     &                    COMM_CURRENT,ierr)

#else

         if (objfuncs(1).eq.19) then

            do k=1,ndv
               dCldxs(1,k) = dCldx(k)
            end do

         end if

#endif

         !-- Scale the lift constraint gradients
         do i=1,nlcdm
            do j=1,ndv
               dCldxs(i,j) = dCldxs(i,j)*dvscale(j)
            end do
         end do

      end if

#ifdef _MPI_VERSION

      !-- Force all processors to wait here until the lift
      !-- constraints have been stored and broadcast

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      !-- Evaluate geometric constraints and their gradients wrt
      !-- design variables at current design iteration

      if (ngcon.ge.1) then

         call geoCon(sn_x, gCons, dgCdxs, Status, jdim, kdim, ndim, 
     &     dvs, idv, x, y, bap, bcp, bt, bknot, dvscale, 
     &     dx, dy, xsave, ysave, ifun)

         !-- Scale the gradients of the geometric constraint functions
     
         do i=1,ntcon+nrtcon+1

            do j=1,ngdv
               dgCdxs(i,j) = dgCdxs(i,j)*dvscale(j)
            end do

         end do

      end if

#ifdef _MPI_VERSION

      !-- Force all processors to wait here until the geometric 
      !-- constraints have been evaluated

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      if (needF .gt. 0) then

         !-- Assign the objective function value to vector 'F'

         F(Obj) = objfun

         !-- Assign off-design contraint function values to vector 'F'

         do i=2,noffcon+1
            
            F(i) = offCs(i-1)

         end do

         !-- Assign geometric contraint function values to vector 'F'

         if (ngcon.ge.1) then

            do i=noffcon+2,noffcon+ngcon+1
    
               F(i) = gCons(i-noffcon-1)

            end do
         
         end if

         !-- Assign lift constraint function values to vector 'F'

         if (nlcdm.gt.0) then

            do i=noffcon+ngcon+2,noffcon+ngcon+nlcdm+1

               F(i) = lcons(i-noffcon-ngcon-1)

            end do

         end if

      end if

      !-- Populate 'G' array with known derivatives evaluated at current 
      !-- design iteration

      if (needG .gt. 0) then

         neG = 0

         !-- Populate 'G' with the top row of the Jacobian which 
         !-- corresponds to the composite gradient of the objective 
         !-- function

         do i = 1, ndv

            neG        =  neG + 1
            G(neG)     =  grad(i)
            !iGfun(neG) =  Obj
            !jGvar(neG) =  i
        
         end do

         !-- Now populate 'G' with the ith row of the Jacobian which
         !-- corresponds to the gradient of the (i-1)th constraint
         !-- function
         
         if (nf.gt.1) then

            !-- Add off-design constraint gradients
           
            do i = 1,noffcon

               do j = 1, ndv

                  neG        =  neG + 1
                  G(neG)     =  doffCdxs(i, j)
                  !iGfun(neG) =  i
                  !jGvar(neG) =  j
        
               end do
           
            end do

            !-- Add geometric constraint gradients

            do i = 1,ngcon

               do j = 1, ndv

                  neG        =  neG + 1
                  G(neG)     =  dgCdxs(i, j)
                  !iGfun(neG) =  i
                  !jGvar(neG) =  j
        
               end do

            end do

            !-- Add lift constraint gradients

            do i = 1,nlcdm

               do j = 1, ndv

                  neG        =  neG + 1
                  G(neG)     =  dCldxs(i, j)
                  !iGfun(neG) =  i
                  !jGvar(neG) =  j

               end do

            end do
 
         end if ! (nf.gt.1)
        
      end if ! (needG .gt. 0)

 1000 continue

      !-- Replace the current grid file with the grid file corresponding
      !-- to this design iteration

#ifdef _MPI_VERSION
      if (rank.eq.0) then
#endif        
         ip=15
         call ioall (ip,0,jdim,kdim,q,work1(1,1),cp,work1(1,5),
     &        work1(1,6),work1(1,7),work1(1,8),xy,xyj,x,y, obj0s)
#ifdef _MPI_VERSION
      end if
#endif

      !-- Write function, constraint, and Status values to file before 
      !-- exiting usrfun
      
      rStat = 1

      call snRestart(rStat,F,G,EOF,nF,lenG,ifun,Status)

      varStat = 0

      call varTransfer(varStat,jdim,kdim,ndim,ifirst,ifun,itfirst,
     &      x,y,idv,bap,bcp,bt,bknot,indx,icol,iex,cp,xy,xyj,cp_tar,
     &      fmu,vort,turmu,ia,ja,ipa,jpa,iat,jat,ipt,jpt,as,ast,pa,
     &      pat,ru,ruIndex)

      !-- Write restart file

      rewind(72)

      write(72) ifirst, ifun, ico, rhoinf, pinf, rest_iter
      write(72) (dvscale(i),i = 1,ibsnc)
      write(72) (alphas(i),i = 1,nopc)
      write(72) (alphas_init(i),i = 1,nopc)
      write(72) (obj0s(i),i = 1,mpopt)
      write(72) (((((qwarm(i,j,k,l,m),i=1,warmset),j=1,maxj),
     &        k=1,maxk),l=1,nblk),m=1,mpopt)    
      write(72) xsave
      write(72) ysave

 1001 continue

#ifdef _MPI_VERSION

      !-- Force all processors to arrive here before returning to SNOPT

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      write(scr_unit,1002)
 1002 format(/3x,
     &'Exiting usrfun subroutine',//3x, 
     &'===========================================================',/)
      return

      end ! subroutine usrfun
