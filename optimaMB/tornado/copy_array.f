c----------------------------------------------------------------------
c     -- copy array q1 to array q2 --
c----------------------------------------------------------------------

      subroutine copy_array(jdim,kdim,ndim,q1,q2)

      implicit none
      integer jdim,kdim,ndim,j,k,n
      double precision q1(jdim,kdim,ndim), q2(jdim,kdim,ndim)

      do n=1,ndim
         do k=1,kdim
            do j=1,jdim
               q2(j,k,n)=q1(j,k,n)
            end do
         end do
      end do

      return
      end                       !copy_array


      subroutine copy_arrayes(jdim,kdim,ndim,q1,maxstage1,jstage1,
     &     q2,maxstage2,jstage2)
cmpr  copy jstage1 from variable q1 to jstage2 from variable q2
      implicit none
      integer jdim,kdim,ndim,j,k,n,jstage1,jstage2,maxstage1,maxstage2
      double precision q1(jdim,kdim,ndim,maxstage1)
      double precision q2(jdim,kdim,ndim,maxstage2)

      do n=1,ndim
         do k=1,kdim
            do j=1,jdim
               q2(j,k,n,jstage2)=q1(j,k,n,jstage1)
            end do
         end do
      end do

      return
      end                       !copy_arrayes


      subroutine copy_arraytes(jdim,kdim,q1,maxstage1,jstage1,
     &     q2,maxstage2,jstage2)
cmpr  copy jstage1 from variable q1 to jstage2 from variable q2
      implicit none
      integer jdim,kdim,j,k,n,jstage1,jstage2,maxstage1,maxstage2
      double precision q1(jdim,kdim,maxstage1)
      double precision q2(jdim,kdim,maxstage2)
     
      do k=1,kdim
         do j=1,jdim
            q2(j,k,jstage2)=q1(j,k,jstage1)
         end do
      end do     

      return
      end                       !copy_arraytes
