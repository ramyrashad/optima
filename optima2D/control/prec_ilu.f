c----------------------------------------------------------------------
c     -- ILU factorization for gmres preconditioner --
c     
c     written by: marian nemec
c     date: august 2000
c----------------------------------------------------------------------
      subroutine prec_ilu (jdim, kdim, ndim, ilu_meth, lfil, iout, ipa,
     &     jpa, pa, alu, jlu, ju, levs, jw, wk)

      implicit none

#include "../include/arcom.inc"

      integer jdim, kdim, ndim, ilu_meth, lfil, na, iout, ierr, nz
      integer jw(3*maxjk*nblk)
      integer jlu(iwk), ju(maxjk*nblk), levs(iwk) 
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)

      double precision alu(iwk), wk(jdim*kdim*ndim+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)

      real*4 etime, time, t, tarray(2)
      external etime

      na = jmax*kdim*ndim
      if (clalpha2 .or. clopt) then
         na=na+1
      endif


c     -- first order preconditioner matrix --
      if (ilu_meth.eq.1) then
c     -- sparskit ilu(0) preconditioner --
        ierr = 0
        t=etime(tarray)
        time=tarray(1)
        call ilu0 (na, pa, jpa, ipa, alu, jlu, ju, jw, ierr)
        t=etime(tarray)
        time=tarray(1)-time
        nz = ju(na) - 2
        write (iout,40) ierr, time
      else if (ilu_meth.eq.2) then            
c     -- sparskit iluk(p) preconditioner --
        ierr = 0
        t=etime(tarray)
        time=tarray(1)
        call iluk (na, pa, jpa, ipa, lfil, alu, jlu, ju, levs, iwk,
     &        wk, jw, ierr)
        t=etime(tarray)
        time=tarray(1)-time
        nz=ju(na) - 2
        write (iout,42) lfil, ierr, time
      else if (ilu_meth.eq.3) then 
c     -- probe ilu(p) preconditioner --
        ierr = 0
        t=etime(tarray)
        time=tarray(1)
        call ilup (na, lfil, pa, jpa, ipa, alu, jlu, ju, wk, levs, 
     &        jw, jw(na+1), jw(2*na+1), ierr, nz)
        t=etime(tarray)
        time=tarray(1)-time
        write (iout,44) lfil, ierr, time
      else
        write (*,*) 'PREC_ILU: ilu_meth wrong value'
        stop
      end if

 40   format (3x,'ILU(0) error flag and time:',i3,f10.3)
 42   format (3x,'ILUK(',i2,') error flag and time:',i3,f10.3)
 44   format (3x,'ILU(',i2,')PROBE error flag and time:',i3,f10.3)

      if (ierr.ne.0) then
        write (*,*) 'PREC_ILU: preconditioner ierr problem!!',ierr 
      end if
      
  
      write (iout,50) nz
 50   format(3x,'Non-zeros after ILU:',i8)

      return
      end                       !prec_ilu
