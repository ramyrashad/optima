************************************************************************
      !-- Program name: offCon
      !-- Written by: Howard Buckley
      !-- Date: March 2009
      !-- 
      !-- This subroutine evaluates off-design constraints and their 
      !-- gradients wrt the current value of the design variables
************************************************************************


      subroutine offCon 
     &     (offC, jdim, kdim, ndim, ifirst, itfirst, sn_x, 
     &     iex, q, cp, y, xy, xyj, x, dx, dy, lambda, fmu, vort, turmu,
     &     idv, indx, icol, cp_tar,  bap, bcp, bt, bknot, dvs, cp_his, 
     &     ac_his, ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, 
     &     pat, xsave, ysave, work1, opwk, doffCdx, dvscale, Status, 
     &     qwarm, ifun, obj0s, objfun, grad, snobj_alpha, needF, needG,
     &     lcon, dCldx)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      
      !-- Declare Optima2D variables

      integer
     &     i, j, k, l, jdim, kdim, ndim, idv(ibsnc),ifirst, mp,
     &     ifun, itfirst, indx(jdim,kdim), icol(9), badrank,
     &     iex(jdim,kdim,4), ia(jdim*kdim*ndim+2),
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1),
     &     iat(jdim*kdim*ndim+2), ifun_tmp, needF, needG,
     &     jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1),
     &     gradtemp, sendProc, iter, d100, d10, d1, ndvtot

      double precision
     &     dvs(nc+mpopt), dvscale(ibsnc), bap(jbody,2),
     &     x(jdim,kdim), y(jdim,kdim), xsave(jdim,kdim), 
     &     ysave(jdim,kdim), bcp(nc,2), bt(jbody), obj1,
     &     bknot(jbsord+nc), tcon(ntcon), dCdx(ntcon, ndv),
     &     q(jdim,kdim,ndim), xy(jdim,kdim,4), lcon, dCldx(ibsnc),
     &     xyj(jdim,kdim), xys(jdim,kdim,4), xyjs(jdim,kdim),
     &     xs(jdim,kdim), ys(jdim,kdim), dx(jdim*kdim*incr), 
     &     dy(jdim*kdim*incr), diff, grad(ibsnc), cp(jdim,kdim),
     &     fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim),
     &     as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), pat(jdim*kdim*ndim*ndim*5+1),
     &     cp_tar(jbody,2), obj0, lambda(2*jdim*kdim*incr), telapse,
     &     cp_his(jbody,300,mpopt), ac_his(jbody,300),
     &     s_vector(ndv), resids(mpopt), work1(jdim*kdim,100), 
     &     opwk(jdim*kdim,8), obj0s(mpopt), doffCdalpha,
     &     obj, FP, FMIN, ALFA, AT, AP, GSQ, DG, DG1,
     &     DP, STEP, ACC, DAL, U1, U2, U3, U4, EPS, XSQ,
     &     dv_scal(ibsnc), test, dv_scalMag, wobj, wobj1, wobj2, objfun,
     &     sdMag, numer, denom, compObj, compObj1, compObj2,
     &     compGrad(ibsnc),
     &     wgrad(ibsnc), offC, doffCdx(50), atmp, objs(mpopt),
     &     dpgrad(50),
     &     theta1, theta2, slope, pobj, grads(20,ibsnc),
     &     tmp, stmp, gtmp(ibsnc), gtemp(ibsnc), fd_temp,
     &     alphawarm, walfaGrad, U, V, P, a, Mach, MaxMach,
     &     qs(jdim,kdim,4), L2doffCdx, snobj_alpha,
     &     aleft, dleft, fleft, aright, dright, fright,       
     &     garbage, dvs_avg, gmin(ibsnc), dgTest, gminMag,
     &     qwarm(warmset,maxj,maxk,nblk,nopc), dCldalpha,     
     &     q_init(warmset,jdim,kdim,ndim,mpopt),
     &     cd(mpopt),cl(mpopt),cm(mpopt), qtmp(maxj,maxk,nblk,mpopt)

      Logical 
     &     Rsw, badjacs(mpopt), succfs_send, succfs_recv

      character 
     &     command*60, filename*40

      !-- Declare SNOPT variables

      integer 
     &     Status 
      
      double precision 
     &     sn_x(nc+mpopt)

      !-- Declare local variables

      integer 
     &     n, i_badsolve

      !-----------------------------------------------------------------

      !-- Format statements:

 300  format (i3,i4,    2e22.15, 3e16.8, f10.4, f12.4)
 305  format (i3,i4,i3, 2e22.15, 3e16.8, f10.4, f12.4)

 500  format (4x,i5,3x,i6,2x,d15.8,2x,d15.8)
 510  format(3x,'Flow solver convergence problems! Resid:',e12.4)
 520  format(3x,'Restoring last converged solution')
 550  format (i4,5e16.8)
 600  format (3e24.16)
 700  format (i4, e16.8, 4e15.6, e12.4, e10.2)
 702  format (i4, e16.8, 4e15.6, e12.4, e10.2, f7.1)
 705  format (i4, 2e16.8, f7.1)
 710  format ( /3x, 'ICO:', i4, 3x, 'OBJ.:',
     &     e12.4, 3x, 'GRAD:', e12.4)
 711  format ( /3x, 'ICO:', i4, 3x, 'OBJ.:',
     &     e12.4, 3x, 'GRAD:', e12.4, 3x, 'PT:',i4)

 720  format (i4, 32e16.8)   
 721  format (a4, 32e16.8)

 800  format (2i4, 2e16.8)
 810  format (i4, 30e15.6)
 815  format (i4,i3,30e15.6)

 890  format ('#*NAME=MAIN')
 895  format ('#*EOR')
 896  format (2e18.8)
 900  format (200e16.6)
 910  format (2e24.16)
 920  format (i5,e24.16)

#ifdef _MPI_VERSION

      !-- Force all processors to wait here until everyone has arrived

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      if (Status.eq.1) then
         
         write(scr_unit,930)sc_method
 930     format(/3x,'Scaling Option [',i1,'] is applied to SNOPT design
     & variables')
         
      end if

      !-- Store total number of design variables 

      ndvtot = ndv

      
      !-- Set the Optima2D design variable array 'dvs' equal to the 
      !-- current values of the SNOPT design variables

      do i=1,ndv
         dvs(i) = sn_x(i)
      enddo

      !-- Unscale the design variables

      do i=1,ndv
         dvs(i) = dvs(i)*dvscale(i)
      enddo

      !-- If this is the first iteration then save some stuff


      if (Status .eq. 1 .and. ico .eq. 0) then

         !-- Save initial grid 

         do k = 1,kdim
            do j = 1,jdim
               xsave(j,k) = x(j,k)
               ysave(j,k) = y(j,k)
            end do
         end do

         !-- Save airfoil x coordinates

         i = 1
         do j = jtail1, jtail2
            ac_his(i,1) = x(j,1)
            i = i + 1
         enddo

         !-- save initial angle of attack values

         alphas_init = alphas

      endif


      !-- Perturb original grid using the current design variables

      if (ngdv.gt.0) then

         !-- Restore original grid

         do k = 1,kdim
            do j = 1,jdim
               x(j,k) = xsave(j,k)
               y(j,k) = ysave(j,k)
            end do
         end do

         call regrid( -1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

      end if

      !-- Write current airfoil geometry and control points to file 

#ifdef _MPI_VERSION
      if (rank.eq.0) then
#endif
         if (acout) rewind (ac_unit)
         if (bcout) rewind (bc_unit)
         do j = jtail1,jtail2
            if (acout) write (ac_unit,600) x(j,1), y(j,1)
         end do
         if (acout) call flush(ac_unit)
         do i = 1,nc
            if (bcout) write (bc_unit,600) bcp(i,1), bcp(i,2)
         end do
         if (bcout) call flush(bc_unit)
#ifdef _MPI_VERSION
      end if
#endif

      !-- Store airfoil shape
      
      i = 1
      do j = jtail1, jtail2      
         ac_his(i,ifun)   = x(j,1)
         ac_his(i,ifun+1) = y(j,1)
         i = i + 1
      end do
 
      !-- Write current design variables to file

#ifdef _MPI_VERSION
      if (rank.eq.0) then
#endif
         write (dvhis_unit,720) ifun, (dvs(j),j=1,ndv)
         call flush(dvhis_unit)

         !-- If this is the first iteration then write current airfoil 
         !-- coordinates to file

         if (Status .eq. 1) then

            if (bestout) write (n_best,890)
            do j = jtail1, jtail2
               if (bestout) write (n_best,896) x(j,1), y(j,1)
            end do
            if (bestout) write (n_best,895)
            if (bestout) call flush(n_best)
         
         end if 
#ifdef _MPI_VERSION         
      end if
#endif

      !-- Set design point operating conditions according to processor
      !-- rank. i.e. the processor with rank N will handle computation
      !-- of design point N+1

#ifdef _MPI_VERSION
      mp = rank+1
#else
      mp = 1
#endif

      !-- Define number of design variables at design point

      if ( dvalfa(mp) ) then
         ndv = ngdv+1
      else
         ndv = ngdv
      end if
         
      !-- Set operating conditions for current design point

      if (opt_meth.ne.4) fsmach = fsmachs(mp)
      re_in = reno(mp)
      cl_tar = cltars(mp)
      cd_tar = cdtars(mp)
      wfl = wfls(mp)
      obj_func = objfuncs(mp)
      clalpha = clalphas(mp)
      clalpha2 = clalpha2s(mp)
      clopt = clopts(mp)
      obj0 = obj0s(mp)

      !-- adjust angle of attack --

      if (.not. clopt) then
         if ( dvalfa(mp) ) then
            atmp = dvs(ngdv+1)
            dvs(ngdv+1) = dvs(idv(ngdv+mp))
            alpha = dvs(ngdv+1)
         else if (warm_start) then
            alpha = alphas(mp)
         else
            alpha = alphas_init(mp)
         end if
      else if (warm_start) then
         alpha = alphas(mp)
      else
         alpha = alphas_init(mp)
      end if

      frozen = .false.       !flag used for freezing dissipation

      !-- Restore last converged solution --
      if ((Status .ne. 1).and.(warm_start)) then
         if (ifirst.eq.0) then
            do n=1,4
               do k=1,kdim
                  do j=1,jdim
                     q(j,k,n)=qwarm(1,j,k,n,mp)
                  end do
               end do
            end do      
            !-- Update turbulent viscosity --
            if (viscous .and. turbulnt .and. itmodel.eq.2) then
               do k = 1,kdim
                  do j = 1,jdim
                     turre(j,k) = qwarm(1,j,k,5,mp)
                  end do
               end do
            end if
         end if
      end if

      !-- Evaluate off-design constraint function

      call CalcObj(offC, jdim, kdim, ndim, ifirst, indx, icol, iex,
     &        q, cp, xy, xyj, x, y, cp_tar, fmu, vort, turmu, work1,
     &        ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat, 
     &        itfirst, obj0, mp, ifun, obj1, lcon )

#ifdef _MPI_VERSION

      !-- Force all processors to pause here until the objective and
      !-- constraint functions at every design point have been calc'd

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif
    
      !-- Check flow convergence
      succfs_send = .true.
      if (Status.eq.1) then
         if (opt_meth .eq. 11) then
            !-- Check for convergence problems on each proc individually
            if (resid .gt. 1.0d-8) then
               succfs_send = .false.
            endif

#ifdef _MPI_VERSION
            !-- Notify root that some proc could not converge
            !-- Logical OR operation on all processors
            call mpi_reduce(succfs_send, succfs_recv, 1, MPI_LOGICAL,
     &                      MPI_LOR, 0, COMM_CURRENT, ierr)

            !-- If you are root, set Status = -2
            if ((rank .eq. 0) .and. (.not. succfs_recv)) then
               Status = -2
            endif

            !-- Broadcast Status to all procs
            call mpi_bcast(Status, 1, MPI_INTEGER, 0,COMM_CURRENT, ierr)
#endif

            if (Status .eq. -2) then
               return
            end if
         else
            if (resid .gt. 1.d-8 ) then

               write (scr_unit,10)
 10            format
     &     (/3x,'Flow convergence problems on first design iteration!!',
     &      /3x,'Terminating optimization')
               write (scr_unit,15) mp
 15            format(3x,'Design Point: ')
               stop

            end if
         end if
      end if

      !-- If negative Jacobian or flow solve connvergence problems are
      !-- encountered at any design point, ensure that all processors
      !-- return to SNOPT with 'Status' flag set to -1. This tells SNOPT
      !-- that the objective function is not defined at the current 
      !-- value of the design variables.
      !--
      !-- First collect the values of badjac and resid from each 
      !-- processor and store in arrays on processor 0

      !-- Collect badjac values and store in array badjacs: 

#ifdef _MPI_VERSION        

      if (rank==0) then

         badjacs(1) = badjac
  
      else
            
         call MPI_SEND(badjac, 1, MPI_LOGICAL, 0, 1, 
     &                 COMM_CURRENT, ierr)            

      end if

      if (rank==0) then

         do i = 2, mpopt
            
            sendProc = i-1
            call MPI_RECV(badjac, 1, MPI_LOGICAL, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

            badjacs(i) = badjac
            
         end do
         
      end if

      !-- Broadcast the array of negative jacobian values to all 
      !-- processors

      call MPI_BCAST(badjacs,mpopt,MPI_LOGICAL,0,COMM_CURRENT,
     &     ierr)      

#else

      badjacs(1) = badjac

#endif

      !-- Collect resid values and store in array resids:

#ifdef _MPI_VERSION
     
      if (rank==0) then

         resids(1) = resid
  
      else
            
         call MPI_SEND(resid, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                 COMM_CURRENT, ierr)            

      end if

      if (rank==0) then

         do i = 2, mpopt
            
            sendProc = i-1

            call MPI_RECV(resid, 1, MPI_DOUBLE_PRECISION, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

            resids(i) = resid
            
         end do
         
      end if

      !-- Broadcast the array of residual values to all 
      !-- processors

      call MPI_BCAST(resids,mpopt,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)  

#else

      resids(1) = resid

#endif

      !-- Scan 'badjacs' and 'resids' arrays to determine if a 
      !-- negative Jacobian or flow solve convergence problem was
      !-- encountered at any design point

      do i = 1, mpopt

         !-- If a negative Jacobian value was detected at any design 
         !-- point then set 'badjac' equal to 'TRUE'

         if (badjacs(i)) then

            badrank = i-1             
            badjac = .true.

            goto 123

         end if

         !-- If flow solver problems were encountered at any design
         !-- point then set resid to the residual value at the first 
         !-- design point where a flow solve convergence problem was
         !-- encountered


         if (resids(i).gt. 1.d-8) then
            
            badrank = i-1 
            resid = resids(i)

            goto 123

         end if

      end do

 123  continue
         
      !-- If negative Jacobian detected, restore last converged solution
      !-- then return to SNOPT and ask for better design variables, 
      !-- i.e. set 'Status' equal to -1

      if (badjac) then

         Status = -1
         write (scr_unit,905) badrank
         write (scr_unit,520)
         badjac = .false.

         !-- Decrement iteration counter

         ifun = ifun - 1

         !-- Adjust angle of attack

         if ( dvalfa(mp) ) then
            dvs(idv(ngdv+mp)) = dvs(ngdv+1)
            dvs(ngdv+1) = atmp
         end if
         
         !-- Adjust number of design variables
         
         ndv = ndvtot

         !-- Restore last converged solution --
   
         if ((ifirst.eq.0).and.(warm_start)) then
            do n=1,4
               do k=1,kdim
                  do j=1,jdim
                     q(j,k,n)=qwarm(1,j,k,n,mp)
                  end do
               end do
            end do      
            !-- Update turbulent viscosity --
            if (viscous .and. turbulnt .and. itmodel.eq.2) then
               do k = 1,kdim
                  do j = 1,jdim
                     turre(j,k) = qwarm(1,j,k,5,mp)
                  end do
               end do
            end if
         end if
   
         return
         
      end if

 905  format
     & (/3x,'Negative Jacobian detected on process [',i3,' ]', 
     &      /3x,'Returning to SNOPT with Status equal to -1')

      !-- If flow solver convergence problems detected, restore last 
      !-- converged solution then return to SNOPT and ask for better 
      !-- design variables, i.e. set 'Status' equal to -1

      if (resid .gt. 1.d-8) then

         Status = -1
         write (scr_unit,906) badrank
         write (scr_unit,907) resid
         write (scr_unit,520)

         !-- Decrement iteration counter
         
         ifun = ifun - 1

         !-- Adjust angle of attack
         
         if ( dvalfa(mp) ) then
            dvs(idv(ngdv+mp)) = dvs(ngdv+1)
            dvs(ngdv+1) = atmp
         end if

         !-- Adjust number of design variables
         
         ndv = ndvtot

         !-- Restore last converged solution --
   
         if ((ifirst.eq.0).and.(warm_start)) then
            do n=1,4
               do k=1,kdim
                  do j=1,jdim
                     q(j,k,n)=qwarm(1,j,k,n,mp)
                  end do
               end do
            end do      
            !-- Update turbulent viscosity --
            if (viscous .and. turbulnt .and. itmodel.eq.2) then
               do k = 1,kdim
                  do j = 1,jdim
                     turre(j,k) = qwarm(1,j,k,5,mp)
                  end do
               end do
            end if
         end if

         return
         
      end if ! (resid .gt. 1.d-8)

 906  format 
     & (/3x,'Flow solver convergence problems on process [',i3,' ]',
     &      /3x,'Returning to SNOPT with Status equal to -1')

 907  format(3x,'Residual: ',e12.4)

#ifdef _MPI_VERSION
    
      !-- Send final angle of attack from this flow solution to array 
      !-- 'alphas' on processor 0 for warm-starting solution of next 
      !-- iteration

      if (rank==0) then

         if (clopt) then
         
            alphas(1) = alpha

         end if

         do i = 2, mpopt

            if (clopts(i)) then

               sendProc = i-1

               call MPI_RECV(alpha, 1, MPI_DOUBLE_PRECISION,  
     &            sendProc, 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)
               
               alphas(i) = alpha
               
            end if

         end do
            
      else

         if (clopt) then
            
            call MPI_SEND(alpha, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                    COMM_CURRENT, ierr)            
            
         end if 

      end if ! (rank==0)

      !-- Broadcast 'alphas' to all processors

      call MPI_BCAST(alphas,mpopt,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)    

#else

      if (clopt) then
      
         alphas(1) = alpha

      end if

#endif      

      !-- Set the AOA restart array with AOA values from this 
      !-- iteration

      alphas_restart = alphas

      write(scr_unit,21) 
 21   format(/3x,'Set the AOA restart values to the AOA values',/3x,
     &           'from this iteration',/)

      !-- The storage of AOA's from all processors done on processor
      !-- zero overwrites the AOA for processor zero, so restore the
      !-- correct AOA on processor zero

#ifdef _MPI_VERSION

      if ((rank.eq.0).and.(clopt)) alpha = alphas(1)

#else

      if (clopt) alpha = alphas(1)

#endif

#ifdef _MPI_VERSION

      !-- The following call to MPI_REDUCE used in the calc of the
      !-- composite objective function is not applicable to off-design
      !-- constraints, but is included here and supplied with dummy
      !-- values so that all processors are following parallel paths
      !-- (more or less)

      wmpo(mp) = 0.0

      !-- Apply the design point weighting to the computed objective
      !-- function (**Dummy value of 'wobj' passed to MPI_REDUCE**)

      wobj = 0.0 !wmpo(mp)*objfun
      wobj1 = 0.0
      wobj2 = 0.0


      !-- Use MPI_REDUCE to compute the composite normalized 
      !-- designer-priority-weighted objective 
      !-- function and store on processor 0

      call MPI_REDUCE(wobj,compObj,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,
     &     COMM_CURRENT, ierr)

      !-- Use MPI_REDUCE to compute the composite un-normalized 
      !-- designer-priority-weighted objctive 
      !-- function and store on processor 0

      call MPI_REDUCE(wobj1,compObj1,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,
     &     COMM_CURRENT, ierr)

      !-- Use MPI_REDUCE to compute the composite un-normalized 
      !-- objective function without designer-priority weighting
      !-- and store on processor 0

      call MPI_REDUCE(wobj2,compObj2,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,
     &     COMM_CURRENT, ierr)

      !-- Broadcast the composite objective function value to all
      !-- processors

      call MPI_BCAST(compObj,1,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr) 

#else

      wmpo(mp) = 0.0
      wobj = 0.0
      wobj1 = 0.0
      wobj2 = 0.0
      compObj = 0.0
      compObj1 = 0.0
      compObj2 = 0.0

#endif

      !-- The composite objective function value is passed back to
      !-- the 'usrfun' subroutine as variable 'objfun

      objfun = compObj
      
      !-- If logging of grid or solution history is enabled, then
      !-- write these values to file now (H.Buckley).

      if((grid_his).or.(sol_his)) then

         !-- Add jacobian scaling...don't ask why!!!!           
         do n = 1,ndim
            do j = 1,jmax
               do k = 1,kmax
                  q(j,k,n) = (q(j,k,n))/(xyj(j,k))
               end do
            end do
         end do

         call history(x, y, q, q, ifun, turmu, xy, xyj, jdim, 
     &                      kdim, mp )

         !-- Remove jacobian scaling...pretend it was never there!!!!
            
         do n = 1,ndim
            do j = 1,jmax
               do k = 1,kmax
                  q(j,k,n) = (q(j,k,n))*(xyj(j,k))
               end do
            end do
         end do

      endif

      !-- Store initial objective function values in 'obj0s' array

      if (itfirst == 1) then

#ifdef _MPI_VERSION

         if (rank==0) then

            obj0s(1) = obj0
            
            do i = 2, mpopt

               sendProc = i-1

               call MPI_RECV(obj0, 1, MPI_DOUBLE_PRECISION,  
     &            sendProc, 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)
               
               obj0s(i) = obj0
                           
               !-- Reset the initial objective function value for proc 0
               !-- This step required because 'obj0' on proc 0 is 
               !-- repeatedly overwritten with the values of obj0 from
               !-- the sending processors.

               obj0 = obj0s(1)

            end do

            !-- After all initial objective function values have been
            !-- stored in 'obj0s' write 'obj0s' to .obj file
            if (rank.eq.0) then
               do i = 1, mpopt
                  write(obj_unit,401) obj0s(i)
 401              format(e40.34)
               end do
            end if

         else
            
            call MPI_SEND(obj0, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                    COMM_CURRENT, ierr)            
            
         end if ! (rank==0)

         !-- Broadcast 'obj0s' to all processors

         call MPI_BCAST(obj0s,mpopt,MPI_DOUBLE_PRECISION,0,
     &        COMM_CURRENT,ierr)    

#else

      obj0s(1) = obj0

      do i = 1, mpopt
         write(obj_unit,401) obj0s(i)
 401     format(e40.34)
      end do

#endif

      end if ! (itfirst == 1)
    
      !-- Save converged flow solution for warm starts 

      do n=1,ndim
         do k=1,kdim
            do j=1,jdim
               qwarm(1,j,k,n,mp)=q(j,k,n)
            end do
         end do
      end do
            
      ifirst = 0

      !-- Check trailing edge angle

      theta1 = atan2d( y(jtail2,1)-y(jtail2-1,1), x(jtail2,1) -
     &        x(jtail2-1,1) )
         
      theta2 = atan2d( y(jtail1,1)-y(jtail1+1,1), x(jtail1,1) -
     &        x(jtail1+1,1))

      slope = 0.5d0*(theta1+theta2)
         
      !-- Compute gradient of constraint function

      if (coef_frz) frozen = .true.
      
      if (needG.gt.0) then

         !-- If OBJ_FUNC=19 for lift-constrained drag min, then 2
         !-- gradients must be evaluated; 1 for the drag objective and
         !-- 1 for the lift constraint.         
         if (obj_func.eq.19) then
            !-- Evaluate gradient of objective J = Cd
            lcdm_grad = 1
            call CalcGrad (offC,doffcdx,jdim,kdim, ndim, ifirst, q, cp, 
     &        xy, xyj, x, y, xsave, ysave, dx, dy, lambda, fmu, vort, 
     &        turmu,cp_tar, bap, bcp, bt, bknot, dvs, idv, indx, icol, 
     &        iex,work1, ia, ja, ipa,jpa, iat, jat, ipt, jpt, as, ast, 
     &        pa, pat, opwk, obj0, mp, ifun)
            !-- Evaluate gradient of lift constraint C = Cl
            lcdm_grad = 2
            call CalcGrad (lcon,dCldx,jdim, kdim, ndim, ifirst, q, cp, 
     &        xy, xyj, x, y, xsave, ysave, dx, dy, lambda, fmu, vort, 
     &        turmu,cp_tar, bap, bcp, bt, bknot, dvs, idv, indx, icol, 
     &        iex,work1, ia, ja, ipa,jpa, iat, jat, ipt, jpt, as, ast, 
     &        pa, pat, opwk, obj0, mp, ifun)

            !-- Put AOA gradient component in correct location in 
            !-- 'dCldx' vector

            if (dvalfa(mp)) then             
               dCldalpha = dCldx(ngdv+1)               
               dCldx(ngdv+1) = 0.0
               dCldx(idv(ngdv+mp)) = dCldalpha
            end if
         else
            lcdm_grad = 0
            call CalcGrad (offC,doffCdx,jdim,kdim, ndim, ifirst, q, cp, 
     &        xy, xyj, x, y, xsave, ysave, dx, dy, lambda, fmu, vort, 
     &        turmu,cp_tar, bap, bcp, bt, bknot, dvs, idv, indx, icol, 
     &        iex,work1, ia, ja, ipa,jpa, iat, jat, ipt, jpt, as, ast, 
     &        pa, pat, opwk, obj0, mp, ifun)
         end if ! obj_func.eq.19
      end if ! needG.gt.0


#ifdef _MPI_VERSION

      !-- Force all processors to pause here until the constraint
      !-- function gradient at every off-design point has been calculated

      call MPI_BARRIER(COMM_CURRENT, ierr)

#endif

      !-- Save the un-weighted design point gradient vector and apply 
      !-- design variable scaling
      do i=1,ndv
         dpgrad(i) = doffCdx(i)*dvscale(i)
      end do

      !-- Put AOA gradient component in correct location in 'doffCdx'
      !-- vector

      ndv = ndvtot !-- Reset ndv to total # of design variables

      if (dvalfa(mp)) then

         doffCdalpha = doffCdx(ngdv+1)
         
         doffCdx(ngdv+1) = 0.0

         doffCdx(idv(ngdv+mp)) = doffCdalpha

      end if

      !-- Calculate the L2 norm fo the gradient associated with design
      !-- point assigned to this processor

      L2doffCdx = 0.0
      do i=1,ndv
         L2doffCdx = L2doffCdx + doffCdx(i)*doffCdx(i)
      end do
      L2doffCdx = dsqrt(L2doffCdx)

      !-- The following call to MPI_REDUCE used in the calculation of the
      !-- composite objective function gradient is not applicable to 
      !-- off-design constraints, but is included here and supplied with 
      !-- dummy values so that all processors are following parallel 
      !-- paths (more or less)

      do i=1,ndv
         grad(i)=0.0
      end do

      !-- Initialize weighted gradient to zero

      do i=1,ndv
         wgrad(i)=0.0
      end do

      !-- Apply the design point weighting to the geometric design
      !-- variable components of the gradient vector

      do i=1,ngdv
         wgrad(i)= wmpo(mp)*grad(i)
      end do

#ifdef _MPI_VERSION

      !-- Use MPI_REDUCE to sum the weighted geometric design
      !-- variable components of the gradient vector from each processor
      !-- and store the result on processor 0

      call MPI_REDUCE(wgrad,compGrad,50,MPI_DOUBLE_PRECISION,MPI_SUM,
     &    0, COMM_CURRENT, ierr)

      !-- Add angle-of-attack gradient components to composite gradient
      !-- vector
      
      if (dvalfa(mp)) then

         walfaGrad =  wmpo(mp)*grad(ngdv+1)

         if (rank==0) then

            compGrad(idv(ngdv+mp)) = walfaGrad

         else
            
            call MPI_SEND(walfaGrad, 1, MPI_DOUBLE_PRECISION, 0, 1, 
     &                    COMM_CURRENT, ierr)            

         end if

      end if

      if (rank==0) then

         do mp = 2, mpopt

            sendProc = mp-1

            if (dvalfa(mp)) then

              call MPI_RECV(walfaGrad, 1, MPI_DOUBLE_PRECISION, sendProc 
     &                , 1, COMM_CURRENT, MPI_STATUS_IGNORE, ierr)

              compGrad(idv(ngdv+mp)) = walfaGrad

            end if

         end do
         
         mp = 1

      end if

      !-- Broadcast the composite gradient vector to all processors

      call MPI_BCAST(compGrad,50,MPI_DOUBLE_PRECISION,0,
     &                COMM_CURRENT, ierr)

#else

      do i=1,ndv 
         compGrad(i) = wgrad(i)
      end do
         
      if (dvalfa(mp)) then

         walfaGrad =  wmpo(mp)*grad(ngdv+1)

         compGrad(idv(ngdv+mp)) = walfaGrad

      end if

#endif
      
      !-- The composite gradient of the objective function is passed
      !-- back to the 'usrfun' subroutine as variable 'grad'

      grad = compGrad

      !-- Calculate the L2 norm fo the composite gradient

#ifdef _MPI_VERSION

      !-- Only done on processor 0

      if(rank.eq.0) then

         cgradmag = 0.0
         do i=1,ndv
            cgradmag = cgradmag + compGrad(i)*compGrad(i)
         end do
         cgradmag = dsqrt(cgradmag)

      end if

      !-- Broadcast the L2 norm of the composite gradient to all
      !-- processors
      
      call MPI_BCAST(cgradmag,1,MPI_DOUBLE_PRECISION,0,COMM_CURRENT,
     &     ierr)

#else

         cgradmag = 0.0
         do i=1,ndv
            cgradmag = cgradmag + compGrad(i)*compGrad(i)
         end do
         cgradmag = dsqrt(cgradmag)

#endif

      !-- Increment design iteration counter

      ico = ico+1

      !-- re-adjust design variable vector --
      if ( dvalfa(mp) ) then
         dvs(idv(ngdv+mp)) = dvs(ngdv+1)
         dvs(ngdv+1) = atmp
      end if

         
      !-- The variable 'iter' in Optima2D is the search direction
      !-- counter.
      !-- Set search direction counter equal to 'icg'

      iter = icg

      !-- Write off-design constraint function and gradient results
      !-- to the repsective design point .ohis file

      if (mpopt.gt.1) then
c     start change by huafei
         cd(mp)=cdt
         cl(mp)=clt
         cm(mp)=cmt
c     end change by huafei
         if (ohisout) write (n_mpo,700) ifun, offC, L2doffCdx, clt, cdt,
     &           cmt, alpha, resid
         if (ohisout) call flush(n_mpo)
      end if

#ifdef _MPI_VERSION

      !-- Pause until all processors are caught up

      call MPI_BARRIER(COMM_CURRENT,ierr)

#endif

      !-- Write objective function and gradient results for the
      !-- current design point to standard output

      if (mpopt .eq. 1) then
         write (scr_unit,710) ico, offC, L2doffCdx
      else
         write (scr_unit,711) ico, offC, L2doffCdx, mp
      end if
      call flush(6)


      !-- Write composite objective function and gradient results 
      !-- to the main .ohis file
      !-- Only done by processor 0

      if (mpopt.gt.1) then
#ifdef _MPI_VERSION
         if(rank.eq.0) then
#endif
            write (ohis_unit,705)  
     &                ifun, compObj, cgradmag, slope       
            call flush(ohis_unit)
#ifdef _MPI_VERSION
         end if
#endif
      else
         cl(1)=clt
         cd(1)=cdt
         cm(1)=cmt
#ifdef _MPI_VERSION
         if(rank==0) then
#endif
            write (ohis_unit,702) ico, offC, L2doffCdx, clt, cdt, 
     &              cmt, alpha, resid, slope
            call flush(ohis_unit)
#ifdef _MPI_VERSION
         end if
#endif
      end if

      !-- Write the composite gradient vector to the .gvhis file
      !-- Only done by processor 0
#ifdef _MPI_VERSION
      if(rank.eq.0)then
#endif
         write (gvhis_unit,810) ifun, (compGrad(i),i=1,ndv)
         call flush(gvhis_unit)
#ifdef _MPI_VERSION
      end if
#endif

      !-- Write the un-weighted design point gradient vector to 
      !-- a processor-specific .gvhis file.
      !-- Done by all processors
      if (gvhisout) write (dpgrad_unit,810) ifun, (dpgrad(i),i=1,ndv)
      if (gvhisout) call flush(dpgrad_unit)

      !-- After completing the constraint function/gradient calculation
      !-- for the first time, set 'ifirst' equal to zero so that
      !-- flow solves are warm-started for all subsequent design 
      !-- iterations. Also set 'itfirst' equal to zero. 'itfirst' is
      !-- the Optima2D equivalent of 'Status' in SNOPT. 'itfirst' and
      !-- 'Status' are both set to one for the first iteration and then
      !-- set to zero thereafter.

      if (Status .eq. 1) then
         ifirst = 0
         itfirst = 0
      end if

      return

      end ! offCon
