c----------------------------------------------------------------------
c     -- derivative of residuals at boundary nodes --
c     -- includes farfield residual --      
c     -- based on vbcrhs and bcrhs (m. nemec) --
c     -- l. billing,  may 2005 --
c----------------------------------------------------------------------
      subroutine drdalpha (jdim, kdim, ndim, x, y, xy, xyj, q, press, 
     &     drdalph)

#include "../include/arcom.inc"

      double precision hinf, xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim)
      double precision q(jdim,kdim,ndim), press(jdim,kdim)
      double precision drdalph(jdim,kdim,ndim)

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c  alphar - angle of attack in radians.
c  drdalph - partial derivative of residual with respect to angle of attack.

c  Limitations - only written for circulation correction false.

      alphar = alpha*pi/180.d0     
      hstfs  = 1.d0/gami + 0.5d0*fsmach**2.d0

      if (circul) then
         write(scr_unit,*) 
     &   'Getting drdalpha with circulation correction on'
         write(scr_unit,*) 'Currently unable to do so - ERROR'
         stop
      end if

clb   -- setting derivative to zero for all locations --      
      do j = 1,jdim
         do k = 1,kdim
            do n = 1,ndim
               drdalph(j,k,n) = 0.d0
            end do
         end do
      end do

c     -----------------------------------------------------------------
c     -- outer-boundary  k = kmax --
c     -----------------------------------------------------------------
      do j = jbegin+1,jend-1

         k = kend
c     -- metrics terms --
         par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
         xy3 = xy(j,kend,3)/par
         xy4 = xy(j,kend,4)/par
         
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr  = gami*(q(j,k,4) - 0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/
     &        q(j,k,1))*xyj(j,k)
         a   = dsqrt(gamma*pr/rho)                    
         vn1 = xy3*u + xy4*v
         vt1 = xy4*u - xy3*v
         a1  = a
         rho1= rho
         pr1 = pr
c     -- reset free stream values with circulation correction --
clb         if (circul) then
clb            xa = x(j,k) - chord/4.
clb            ya = y(j,k)
clb            radius = dsqrt(xa**2+ya**2)
clb            angl = atan2(ya,xa)
clb            cjam = cos(angl)
clb            sjam = sin(angl)
clb            qcirc = circb/( radius* (1.- 
clb     &           (fsmach*sin(angl-alphar))**2))
clb            uf = uinf + qcirc*sjam
clb            vf = vinf - qcirc*cjam
clb            af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
clb            af = dsqrt(af2)
clb         else
         uf = uinf
         vf = vinf
         af = dsqrt(gamma*pinf/rhoinf)
clb         endif
c     
         vninf = xy3*uf + xy4*vf
         vtinf = xy4*uf - xy3*vf
         ainf = af
         
         drdalph(j,k,1) = pi/180.d0*(xy3*vinf-xy4*uinf)
clb         drdalph(j,k,1) = (xy3*vinf-xy4*uinf)
         drdalph(j,k,2) = 0.d0
         
c     -- inflow --
         if (vn1.le.0.d0) then
            drdalph(j,k,3) = 0.d0
            drdalph(j,k,4) = pi/180.d0*(xy4*vinf+xy3*uinf)
clb            drdalph(j,k,4) = (xy4*vinf+xy3*uinf)
            
c     -- outflow --
         else
            drdalph(j,k,3) = 0.d0
            drdalph(j,k,4) = 0.d0
         endif
      end do

      if (.not. viscous) then

c     -----------------------------------------------------------------
c     -- inflow boundary  j = 1 --
c     -----------------------------------------------------------------
         do k = kbegin,kend

c     -- metrics terms --
            par = dsqrt(xy(jbegin,k,1)**2+(xy(jbegin,k,2)**2))
            xy1 = xy(jbegin,k,1)/par
            xy2 = xy(jbegin,k,2)/par
            
            j = jbegin
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5d0*(q(j,k,2)**2 +
     &           q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
            a   = dsqrt(gamma*pr/rho)                    
            vn1 = xy1*u + xy2*v
            vt1 =-xy2*u + xy1*v
            a1  = a
            rho1= rho
            pr1 = pr
c     -- reset free stream values with circulation correction --
clb            if (circul) then
clb               xa = x(j,k) - chord/4.
clb               ya = y(j,k)
clb               radius = dsqrt(xa**2+ya**2)
clb               angl = atan2(ya,xa)
clb               cjam = cos(angl)
clb               sjam = sin(angl)
clb               qcirc = circb/( radius* (1.- 
clb     &              (fsmach*sin(angl-alphar))**2))
clb               uf = uinf + qcirc*sjam
clb               vf = vinf - qcirc*cjam
clb               af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
clb               af = dsqrt(af2)        
clb            else
            uf = uinf
            vf = vinf
            af = dsqrt(gamma*pinf/rhoinf)
clb            endif
            vninf = xy1*uf + xy2*vf
            vtinf =-xy2*uf + xy1*vf
            ainf = af

            drdalph(jbegin,k,1) = pi/180.d0*(xy1*vinf-xy2*uinf)
clb            write(*,*) jbegin, k, '1', drdalph(jbegin,k,1)
clb            drdalph(jbegin,k,1) = (xy1*vinf-xy2*uinf)
            drdalph(jbegin,k,2) = 0.d0
c     -- inflow --
            if (-vn1.le.0.d0) then
               drdalph(jbegin,k,3) = 0.d0
               drdalph(jbegin,k,4) = -pi/180.d0*(xy2*vinf+xy1*uinf)
clb               write(*,*) jbegin, k, '4', drdalph(jbegin,k,1)
clb               drdalph(jbegin,k,4) = -(xy2*vinf+xy1*uinf)
c     -- outflow --
            else
               drdalph(jbegin,k,3) = 0.d0
               drdalph(jbegin,k,4) = 0.d0
clb               write(*,*) 'both 3 and 4 = 0'
            endif
         end do

c     -----------------------------------------------------------------
c     -- outflow boundary  j = jend --
c     -----------------------------------------------------------------
         do k = kbegin,kend
            
c     -- metrics terms --
            par = dsqrt(xy(jend,k,1)**2+(xy(jend,k,2)**2))
            xy1 = xy(jend,k,1)/par
            xy2 = xy(jend,k,2)/par
            
            j = jend
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            pr  = gami*(q(j,k,4) - 0.5d0*(q(j,k,2)**2 +
     &           q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
            a   = dsqrt(gamma*pr/rho)                    
            vn1 = xy1*u + xy2*v
            vt1 =-xy2*u + xy1*v
            a1  = a
            rho1= rho
            pr1 = pr
c     -- reset free stream values with circulation correction --
clb            if (circul) then
clb               xa = x(j,k) - chord/4.
clb               ya = y(j,k)
clb               radius = dsqrt(xa**2+ya**2)
clb               angl = atan2(ya,xa)
clb               cjam = cos(angl)
clb               sjam = sin(angl)
clb               qcirc = circb/( radius* 
clb     &              (1.- (fsmach*sin(angl-alphar))**2))
clb               uf = uinf + qcirc*sjam
clb               vf = vinf - qcirc*cjam
clb               af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
clb               af = dsqrt(af2)        
clb            else
            uf = uinf
            vf = vinf
            af = dsqrt(gamma*pinf/rhoinf)
clb            endif
            vninf = xy1*uf + xy2*vf
            vtinf =-xy2*uf + xy1*vf
            ainf = af
            
            drdalph(jend,k,1) = pi/180.d0*(xy1*vinf-xy2*uinf)
clb            drdalph(jend,k,1) = (xy1*vinf-xy2*uinf)
            drdalph(jend,k,2) = 0.d0
c     -- inflow --
            if (vn1.le.0.d0) then
               drdalph(jend,k,3) = 0.d0
               drdalph(jend,k,4) = -pi/180.d0*(xy2*vinf+xy1*uinf)
clb               drdalph(jend,k,4) = -(xy2*vinf+xy1*uinf)
c     -- outflow --
            else
               drdalph(jend,k,3) = 0.d0
               drdalph(jend,k,4) = 0.d0
            endif
         end do
      end if 
      
      return 
      end                       !drdalpha
      
