      subroutine etaimpl(jdim,kdim,q,s,press,sndsp,turmu,fmu,vort,x,y,  
     >   xy,xyj,ett,ds,vv,ccy,coef2y,coef4y,spect,lu,precon,akk)
c                                                                       
c     main integration calls
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim*kdim*4)                                          
      dimension press(jdim*kdim),sndsp(jdim*kdim)                       
      dimension s(jdim,kdim,4),xy(jdim*kdim*4),xyj(jdim*kdim)           
      dimension ett(jdim*kdim),ds(jdim*kdim)                            
      dimension x(jdim*kdim),y(jdim*kdim),turmu(jdim*kdim)              
      dimension vort(jdim*kdim),fmu(jdim*kdim)                          
c                                                                       
      dimension spect(jdim,kdim,3),precon(jdim,kdim,6)  
      dimension vv(jdim*kdim),ccy(jdim*kdim)                            
      dimension coef2y(jdim*kdim),coef4y(jdim*kdim)
      double precision lu(jdim,kdim,4,5)
c
      common/scratch/ a,b,c,d,e,work1
      common/newwork/ dj,ddj

      dimension a(maxpen,4),b(maxpen,16),c(maxpen,16)
      dimension d(maxjk,16),e(maxpen,4),work1(maxjk,10)
      dimension dj(maxpen,3,4),ddj(maxpen,3,4)
      double precision akk
c
c                                                                       
c<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  
c<<<                     implicit operators                        >>>  
c<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  
c                                                                       
c
c     Scalar Pentadiagonal Algorithm
c     ------------------------------                                  
c
      if(meth.eq.1 .or. meth.eq.3 .or. meth.eq.4 .or. meth.eq.5)then  
c       -viscous options 
        if( .not.viscous .or. ivis.ne.1)then                       
c         -inviscid implicit operator                            ...  
          if((.not.cmesh .and. prec.eq.0) .and. orderxy )then
            ixx = 1                                               
            ixy = 2                                               
            iyx = 3                                               
            iyy = 4                                               
            call ninver(jdim,kdim,s,xy,ixx,ixy,iyx,iyy)           
          elseif(cmesh .or. .not.orderxy .or. prec.gt.0)then      
            ix = 3                                                
            iy = 4                                                
            call tkinv(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy,precon) 
          endif                                                           
c
c
c         -eta diagonal integration
c       
          if (meth.eq.4 .or. meth.eq.5) then
            call stepf2dylu(jdim,kdim,q,turmu,fmu,s,press,sndsp,    
     &            xy,xyj,ds,coef2y,coef4y,vv,ccy,work1,spect,lu,akk)
            if (meth.eq.4) then
              do 25 n=1,4
                call yludcmp3(lu(1,1,n,2),lu(1,1,n,3),lu(1,1,n,4),
     &                jdim,kdim,jlow,jup,klow,kup)
c     
                call ylubcsb3(lu(1,1,n,2),lu(1,1,n,3),lu(1,1,n,4),
     &                s(1,1,n),jdim,kdim,jlow,jup,klow,kup)
 25           continue
            else
              do 30 n=1,4
                call yludcmp(lu(1,1,n,1),lu(1,1,n,2),lu(1,1,n,3),
     &                lu(1,1,n,4),lu(1,1,n,5),
     &                jdim,kdim,jlow,jup,klow,kup)
             
                call ylubcsb(lu(1,1,n,1),lu(1,1,n,2),lu(1,1,n,3),
     &                lu(1,1,n,4),lu(1,1,n,5),s(1,1,n),
     &                jdim,kdim,jlow,jup,klow,kup)
 30           continue
            endif
          else
            call stepf2dy(jdim,kdim,q,turmu,fmu,s,press,sndsp,xy,
     &            xyj,x,y,ds,coef2y,coef4y,vv,ccy,work1,spect,precon,
     &            akk)
          endif
c         ..........................................................  
c         ...  t    s  matrix multiply to close diagonalization  ...  
c         ...   eta     3, 4  for eta metrics                    ...  
c         ..........................................................  
c                                                                      
          if(cmesh .or. orderxy)then                                 
            ix = 3                                                
            iy = 4                                                
            call tk(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy,precon)  
          endif                                                      
c                                                                      
cdu       -convert rhs back to conservative variables
          if (prec.gt.0) call dum(jdim,kdim,q,xyj,sndsp,precon,s)
c
        elseif(ivis.eq.1)then                                      

c
c         -viscous block operator in eta
c          -note this logic dependent on order of impilcit integration
c                                                                       
c          .........................................................  
c          ...  t   s  matrix multiply to close diagonalization  ...  
c          ...   xi       1, 2  for xi  metrics                  ...  
c          .........................................................  
c                                                                       
          if(.not.cmesh .and. orderxy)then                      
            ix = 1                                                
            iy = 2                                                
            call tk(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy)             
          endif                                                 
c                                                                       
c         -block tridiagonal in eta
          call stepy(jdim,kdim,q,s,press,sndsp,turmu,fmu,       
     &          ds,xy,xyj,ett,coef2y,coef4y,a,b,c,d,e,dj,ddj)    
          if(.not.cmesh .and. .not.orderxy)then                 
            ix = 1                                                
            iy = 2                                                
            call tkinv(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy)          
          endif                                                 
c                                                                       
        endif                                                     
c     
      elseif(meth.eq.2) then              
c                                                                       
c       -block tridiagonal implicit
c        - xi - eta order of inversions
c        - implicit block eta operator
c                                                                       
        call stepy(jdim,kdim,q,s,press,sndsp,turmu,fmu,    
     &        ds,xy,xyj,ett,coef2y,coef4y,a,b,c,d,e,dj,ddj)  
c                                                                       
      endif                                                          
c                                                                       
      return                                             
      end                                                
