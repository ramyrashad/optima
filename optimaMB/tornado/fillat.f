c     ------------------------------------------------------------------
c     -- fill jacobian matrix and preconditioner: s-a terms --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine fillat (jmax, kmax, nmax, ibc1, ibc2, ibc3, ibc4, 
     &     kminbnd, kmaxbnd, jminbnd, jmaxbnd, 
     &     indx, icol, as, pa, bm2, bm1, db,
     &     bp1, bp2, jacmat, precmat)

      implicit none
   
      integer jmax, kmax, nmax, kminbnd, kmaxbnd, jminbnd, jmaxbnd
      integer ibc1, ibc2, ibc3, ibc4
      integer indx(jmax,kmax),icol(*),js,je,ks,ke,j,k,m,n,ii,ij
      
      double precision bm2(5,jmax,kmax), bm1(5,jmax,kmax), as(*), pa(*)
      double precision db(5,jmax,kmax),  bp1(5,jmax,kmax)
      double precision bp2(5,jmax,kmax)

      logical  jacmat, precmat

c     -- preconditioner --
      if ( precmat ) then 

         if (ibc2.NE.0) then
            js = jminbnd + 1
         else
            js = jminbnd
         end if

         if (ibc4.NE.0) then
            je = jmaxbnd - 1
         else
            je = jmaxbnd
         end if 
         ks = kminbnd+1
         ke = kmaxbnd-1

         n  = 5 
         do k = ks,ke
            do j = js,je
               ij = ( indx(j,k) - 1 )*nmax + n
               ii = ( ij - 1 )*icol(5)
               do m = 1, nmax
                  pa(ii+        m) = pa(ii+        m) + bm2(m,j,k)
                  pa(ii+icol(1)+m) = pa(ii+icol(1)+m) + bm1(m,j,k)
                  pa(ii+icol(2)+m) = pa(ii+icol(2)+m) + db(m,j,k)
                  pa(ii+icol(3)+m) = pa(ii+icol(3)+m) + bp1(m,j,k)
                  pa(ii+icol(4)+m) = pa(ii+icol(4)+m) + bp2(m,j,k)
               end do
            end do
         end do

      end if  !precmat

c     -- second order jacobian --
      if ( jacmat ) then

         n  = 5

c     -- interior points --
         if (ibc2.EQ.0) then
            js = jminbnd
         else
            js = jminbnd + 2
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd - 2
         end if          
         ks = kminbnd + 2
         ke = kmaxbnd - 2


         do k = ks,ke
            do j = js,je
               ij = ( indx(j,k) - 1 )*nmax + n
               ii = ( ij - 1 )*icol(9)
               do m = 1,nmax
                  as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,j,k)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,j,k)  
                  as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,j,k)
                  as(ii+icol(7)+m) = as(ii+icol(7)+m) + bp2(m,j,k)
               end do
            end do
         end do

c     -- first interior boundary nodes --
         if (ibc2.EQ.0) then
            js = jminbnd
         else
            js = jminbnd + 2
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd - 2
         end if
         k = kminbnd+1

         do j = js,je
            ij = ( indx(j,k) - 1 )*nmax + n
            ii = ( ij - 1 )*icol(9)
            do m = 1,nmax
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,j,k)  
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
            end do
         end do

         k = kmaxbnd-1
         do j = js,je
            ij = ( indx(j,k) - 1 )*nmax + n
            ii = ( ij - 1 )*icol(9)
            do m = 1,nmax
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,j,k)  
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
            end do
         end do

         if (ibc2.NE.0) then
            j   = jminbnd + 1
            ks  = kminbnd + 2
            ke  = kmaxbnd - 2

            do k = ks,ke
               ij = ( indx(j,k) - 1 )*nmax + n
               ii = ( ij - 1 )*icol(9)
               do m = 1,nmax
                  as(ii+        m) = as(ii+        m) + bm2(m,j,k)
                  as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,j,k)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,j,k)  
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,j,k)
                  as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
               end do
            end do

            k = kminbnd + 1
            ij = ( indx(j,k) - 1 )*nmax + n
            ii = ( ij - 1 )*icol(9)
            do m = 1,nmax
               as(ii+        m) = as(ii+        m) + bm2(m,j,k)
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,j,k)
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + db(m,j,k)  
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + bp1(m,j,k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp2(m,j,k)
            end do

            k = kmaxbnd - 1
            ij = ( indx(j,k) - 1 )*nmax + n
            ii = ( ij - 1 )*icol(9)
            do m = 1,nmax
               as(ii+        m) = as(ii+        m) + bm2(m,j,k)
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,j,k)  
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,j,k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp2(m,j,k)
            end do
         end if

         if (ibc4.NE.0) then
            j   = jmaxbnd - 1
            ks  = kminbnd + 2
            ke  = kmaxbnd - 2

            do k = ks,ke
               ij = ( indx(j,k) - 1 )*nmax + n
               ii = ( ij - 1 )*icol(9)
               do m = 1,nmax
                  as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,j,k)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,j,k)  
                  as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,j,k)
                  as(ii+icol(7)+m) = as(ii+icol(7)+m) + bp2(m,j,k)
               end do
            end do

            k = kminbnd + 1
            ij = ( indx(j,k) - 1 )*nmax + n
            ii = ( ij - 1 )*icol(9)
            do m = 1,nmax
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + bm1(m,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,j,k)  
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + bp1(m,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
            end do

            k = kmaxbnd - 1
            ij = ( indx(j,k) - 1 )*nmax + n
            ii = ( ij - 1 )*icol(9)
            do m = 1,nmax
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm2(m,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + bm1(m,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,j,k)  
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp2(m,j,k)
            end do
         end if

      end if  !jacmat

      return                                                            
      end                       !fillat
