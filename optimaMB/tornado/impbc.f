c-----------------------------------------------------------------------
c     -- implicit boundary conditions --
c     -- ibcfar => farfield b.c. --      
c     -- ibcib => inviscid solid wall b.c. --
c     -- loosely based on probe (a. pueyo) --
c     -- m. nemec, july 2001 --
c-----------------------------------------------------------------------
      subroutine impbc ( imps, nmax, indx, q, xy, xyj, press, pa, as,
     &     icol, bcf, iex, precmat, jacmat)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"

      integer nmax, icol(*),ib,lg,lq,is,k1,k2,j1,j2
      double precision q(*),xy(*),xyj(*),press(*),pa(*),as(*)
      double precision bcf(maxjb,4,4,mxbfine)
      integer iex(maxjb,4,4,mxbfine),indx(*)
      logical precmat,jacmat,imps

      do ib = 1,nblks
         lq = lqptr(ib)
         lg = lgptr(ib)
         do is = 1,4
            if (ibctype(ib,is).eq.1) then
c     -- airfoil body --
               if (.not. viscous) then
c     -- inviscid flow --
                  if (is.eq.1) then
                     call ibcib( imps, ib, is, jmax(ib), kmax(ib),
     &                    jminbnd(ib), jmaxbnd(ib), kminbnd(ib),
     &                    kminbnd(ib)+1, kminbnd(ib)+2, indx(lg), q(lq),
     &                    xy(lq), xyj(lg), pa, as, icol, iex, bcf,
     &                    gamma, gami, fsmach, maxjb, mxbfine, precmat,
     &                    jacmat)
                  else if (is.eq.3) then
                     call ibcib( imps, ib, is, jmax(ib), kmax(ib),
     &                    jminbnd(ib), jmaxbnd(ib), kmaxbnd(ib),
     &                    kmaxbnd(ib)-1, kmaxbnd(ib)-2, indx(lg), q(lq),
     &                    xy(lq), xyj(lg), pa, as, icol, iex, bcf,
     &                    gamma, gami, fsmach, maxjb, mxbfine, precmat,
     &                    jacmat)
                  else
                     stop 'impbc: problems with body side'
                  end if
               else             !viscous flow
                  if (imps) then
                     if (is.eq.1) then
                        k1 = kminbnd(ib)
                        k2 = k1+1
                        call ibcvb(ib, k1, k2, jminbnd(ib), jmaxbnd(ib),
     &                       jmax(ib), kmax(ib), nmax, q(lq), xyj(lg),
     &                       press(lg), as, pa, icol, indx(lg), jacmat,
     &                       precmat)
c     write (*,*) 'ibcvb1'
                     else if (is.eq.3) then
                        k1 = kmaxbnd(ib)
                        k2 = k1-1
                        call ibcvb(ib, k1, k2, jminbnd(ib), jmaxbnd(ib),
     &                       jmax(ib), kmax(ib), nmax, q(lq), xyj(lg),
     &                       press(lg), as, pa, icol, indx(lg), jacmat,
     &                       precmat)
c     write (*,*) 'ibcvb3'
                     else if (is.eq.2) then
                        j1 = jminbnd(ib)
                        j2 = j1+1
                        call ibcvb24(ib,j1,j2, kminbnd(ib), kmaxbnd(ib),
     &                       jmax(ib), kmax(ib), nmax, q(lq), xyj(lg),
     &                       press(lg), as, pa, icol, indx(lg), jacmat,
     &                       precmat)
                     else
                        stop 'impbc: problems with body side'
                     end if
                  end if
               end if
               
            else if (ibctype(ib,is).eq.2) then
c     -- farfield boundary --
c     -- stored in ibcfar.f --
               if (is.eq.1) then
                  k1 = kminbnd(ib) 
                  k2 = kminbnd(ib) + 1
                  call ibcfs13( imps, ib, is, jmax(ib), kmax(ib), nmax,
     &                 jminbnd(ib), jmaxbnd(ib), k1, k2, indx(lg),
     &                 q(lq), xy(lq), xyj(lg), -1.0, pa, as, icol,
     &                 bcf, iex, gamma, gami, maxjb, mxbfine, precmat,
     &                 jacmat)
c     write (*,*) 'ibcfs13'
               else if (is.eq.2) then 
                  j1 = jminbnd(ib) 
                  j2 = jminbnd(ib) + 1
                  call ibcfs24( imps, ib, is, jmax(ib), kmax(ib), nmax,
     &                 j1, j2, kminbnd(ib)+1, kmaxbnd(ib)-1, indx(lg),
     &                 q(lq), xy(lq), xyj(lg), -1.0, pa, as, icol, bcf,
     &                 iex, gamma, gami, maxjb, mxbfine, precmat,
     &                 jacmat)
c     write (*,*) 'ibcfs24'
               else if (is.eq.3) then
                  k1 = kmaxbnd(ib) 
                  k2 = kmaxbnd(ib) - 1
                  call ibcfs13( imps, ib, is, jmax(ib), kmax(ib), nmax,
     &                 jminbnd(ib), jmaxbnd(ib), k1, k2, indx(lg),
     &                 q(lq), xy(lq), xyj(lg), 1.0, pa, as, icol, bcf,
     &                 iex, gamma, gami, maxjb, mxbfine, precmat,
     &                 jacmat)
c     write (*,*) 'ibcfs13'
               else if (is.eq.4) then
                  j1 = jmaxbnd(ib) 
                  j2 = jmaxbnd(ib) - 1
                  call ibcfs24( imps, ib, is, jmax(ib), kmax(ib), nmax,
     &                 j1, j2, kminbnd(ib)+1, kmaxbnd(ib)-1,indx(lg),
     &                 q(lq), xy(lq), xyj(lg), 1.0, pa, as, icol, bcf,
     &                 iex, gamma, gami, maxjb, mxbfine, precmat,
     &                 jacmat)
c     write (*,*) 'ibcfs24'
               end if

            else if (ibctype(ib,is).eq.3) then
c     -- viscous outflow at j=jmin side 2 --
               j1 = jminbnd(ib)
               j2 = j1+1
c               write (*,*) 'ibcvout3'
               call ibcvout(ib, is, imps, j1, j2, kminbnd(ib)+1,
     &              kmaxbnd(ib)-1, jmax(ib), kmax(ib), nmax, q(lq),
     &              xyj(lg), press(lg),indx(lg), as, pa, icol, gami,
     &              jacmat, precmat, bcf, iex)

            else if (ibctype(ib,is).eq.4) then
c     -- viscous outflow at j=jmax side 4 --
c               write (*,*) 'ibcvout4'
               j1 = jmaxbnd(ib)
               j2 = j1-1
               call ibcvout(ib, is, imps, j1, j2, kminbnd(ib)+1,
     &              kmaxbnd(ib)-1, jmax(ib), kmax(ib), nmax, q(lq),
     &              xyj(lg), press(lg),indx(lg), as, pa, icol, gami,
     &              jacmat, precmat, bcf, iex)
            end if
         end do
      end do

      return
      end                       ! impbc
