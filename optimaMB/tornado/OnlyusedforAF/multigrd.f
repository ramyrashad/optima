c ********************************************************************
c ********************* main integration routine *********************
c ********************************************************************
c calling routine:  main
c
      subroutine multigrd(lvl,mglev,lev_top,lev_bot,npts2,
     &      jdimen,kdimen,isbiter,
     &      q,q0,qp,pk,s,press,
     &      sndsp,turmu,fmu,vort,turre,vk,ve,
     &      x,y,xy,xyj,xit,ett,ds,
     &      uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &      spectxi,specteta,precon,tmp)
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer   fblk,cblk
      dimension npts2(maxblk),svmaxres(3)
      dimension q(maxjkq),    press(maxjk),  sndsp(maxjk)
      dimension s(maxjkq),    xy(maxjkq),    xyj(maxjk)
      dimension xit(maxjk),   ett(maxjk),    ds(maxjk)
      dimension x(maxjk),     y(maxjk),      turmu(maxjk)
      dimension vort(maxjk),  turre(maxjk),  fmu(maxjk)
      dimension q0(maxjkq),   qp(maxjkq),    pk(maxjkq)
      dimension vk(maxjk),    ve(maxjk),     isbiter(-5:20,2)
      dimension spectxi(maxjk*3),  specteta(maxjk*3)
      dimension precon(maxjk*8),   tmp(maxjkq)

      dimension uu(maxjk),    ccx(maxjk)
      dimension vv(maxjk),    ccy(maxjk)
      dimension coef2x(maxjk),coef2y(maxjk)
      dimension coef4x(maxjk),coef4y(maxjk)

      dimension work2(maxj,5)
      common/worksp/work2
c
c
c      write(6,*) 'numiter',numiter
c      write(6,*) 'Level in Multigrid=',lvl,lev_bot,lev_top
c      call flush(6)
c
c      if (idmodel.eq.2) then
c        if (lvl.ne.lev_top) then
c          vlximg =0.1
c          vletamg=0.1
c          vnximg =0.1
c          vnetamg=0.1
c        else
c          vlximg =vlxi
c          vletamg=vleta
c          vnximg =vnxi
c          vnetamg=vneta
c        endif
c      endif
c
      if (lvl.eq.lev_bot) then
        call set_level(lvl)
c
c       -initializing qp from q0 (restricted q)
c        - qp(i)=q0(i)        
c
        do ii=nblkstart,nblkend
           call copy_array(jmax(ii), kmax(ii), 4, q0(lqptr(ii)),
     &          qp(lqptr(ii)))
          call resets(jmax(ii),kmax(ii),s(lqptr(ii)))
        enddo
c
        do ii=nblkstart,nblkend
          call calcps(ii,jmax(ii),kmax(ii),qp(lqptr(ii)),
     &          press(lgptr(ii)),sndsp(lgptr(ii)),
     &          precon(lprecptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &          jlow(ii)-nsd2(ii),jup(ii)+nsd4(ii),
     &          klow(ii)-nsd1(ii),kup(ii)+nsd3(ii))
        enddo
c
        do ii=1,isbiter(lvl,1)
          call integrat(lvl,lev_top,mglev,
     &          npts2,jdimen,kdimen,qp,pk,s,press,
     &          sndsp,turmu,fmu,vort,turre,vk,ve,
     &          x,y,xy,xyj,xit,ett,ds,
     &          uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &          spectxi,specteta,precon,tmp)
c          write(6,189) lvl,resid,maxres(3),maxres(1),maxres(2),
c     &          residmx,clt,cdt
        enddo
 189    format(i3,e13.5,3i4,e12.4,e18.10,e19.11)
c
c       -residual variables will be over written so save them here
c        if (lvl.eq.lev_top) call save_resid(svresid,svresidmx,svmaxres)
      elseif (lvl.lt.lev_bot) then
c          
        call set_level(lvl)
c
c       -initializing q from q0 (restricted q)
        do ii=nblkstart,nblkend
           call copy_array(jmax(ii), kmax(ii), 4, q0(lqptr(ii)),
     &          q(lqptr(ii)))
          call resets(jmax(ii),kmax(ii),s(lqptr(ii)))
        enddo
c     
        if (w_cycle .and. lvl.ne.lev_top) then
          nwc=2
        else
          nwc=1
        endif
c
        do 444 iwc=1,nwc
          do ii=nblkstart,nblkend
            call calcps(ii,jmax(ii),kmax(ii),q(lqptr(ii)),
     &            press(lgptr(ii)),sndsp(lgptr(ii)),
     &            precon(lprecptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &            jlow(ii)-nsd2(ii),jup(ii)+nsd4(ii),
     &            klow(ii)-nsd1(ii),kup(ii)+nsd3(ii))
          enddo
c
          do ii=1,isbiter(lvl,1)
            call integrat(lvl,lev_top,mglev,
     &            npts2,jdimen,kdimen,q,pk,s,press,
     &            sndsp,turmu,fmu,vort,turre,vk,ve,
     &            x,y,xy,xyj,xit,ett,ds,
     &            uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &            spectxi,specteta,precon,tmp)
c            write(6,189) lvl,resid,maxres(3),maxres(1),maxres(2),
c     &            residmx,clt,cdt
          enddo
c
c
c         -residual variables will be over written so save them here
c          if(lvl.eq.lev_top) call save_resid(svresid,svresidmx,svmaxres)
c
c         -restricting q (q --> q0)
          do iblk=nblkstart,nblkend
            fblk=iblk
            cblk=iblk + nblks
            call restrict (fblk,cblk,
     &           jmax(fblk),kmax(fblk),q(lqptr(fblk)),xyj(lgptr(fblk)),
     &           turre(lgptr(fblk)),turmu(lgptr(fblk)),
     &           jmax(cblk),kmax(cblk),q0(lqptr(cblk)),xyj(lgptr(cblk)),
     &           turre(lgptr(cblk)),turmu(lgptr(cblk)))
          enddo
c
c          write(6,*) 'after restrict   level=',lvl
c          call flush(6)
          call set_level(lvl+1)
c
          if (turbulnt) then
            do 58 ii=nblkstart,nblkend
            do 58 nside1=2,4,2
              i2 = lblkpt(ii,nside1)
              if (i2.ne.0) then
                nside2 = lsidept(ii,nside1)
                call interf(nhalo,nside1,nside2,
     &                jmax(ii),kmax(ii),1,turmu(lgptr(ii)),
     &                jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii),
     &                jmax(i2),kmax(i2),turmu(lgptr(i2)),
     &                jminbnd(i2),jmaxbnd(i2),kminbnd(i2),kmaxbnd(i2))
                call halocp(ii,nhalo,1,nside1,nside2,
     &                jmax(ii),kmax(ii),turmu(lgptr(ii)),
     &                jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &                kminbnd(ii),kmaxbnd(ii),
     &                jmax(i2),kmax(i2),turmu(lgptr(i2)),
     &                jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &                kminbnd(i2),kmaxbnd(i2))
c              elseif (i2.ne.0 .and. ibctype(ii,nside1).eq.5) then
c                call bcavgtur(nside1,nside2,
c     &                jmax(ii),kmax(ii),q(lqptr(ii)),xyj(lgptr(ii)),
c     &                turmu(lgptr(ii)),turmu(lgptr(ii)),
c     &                vk(lgptr(ii)),ve(lgptr(ii)),
c     &                jbegin(ii),jend(ii),kbegin(ii),kend(ii),
c     &                jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii),
c     &                jmax(in),kmax(in),q(lqptr(in)),xyj(lgptr(in)),
c     &                turmu(lgptr(in)),turmu(lgptr(in)),
c     &                vk(lgptr(in)),ve(lgptr(in)),
c     &                jbegin(in),jend(in),kbegin(in),kend(in),
c     &                jminbnd(in),jmaxbnd(in),kminbnd(in),kmaxbnd(in))
              endif
 58         continue
            do 59 ii=nblkstart,nblkend
            do 59 nside1=1,3,2
              i2 = lblkpt(ii,nside1)
              if (i2.ne.0.and.ibctype(ii,nside1).ne.5) then
                nside2 = lsidept(ii,nside1)
                call interf(nhalo,nside1,nside2,
     &                jmax(ii),kmax(ii),1,turmu(lgptr(ii)),
     &                jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii),
     &                jmax(i2),kmax(i2),turmu(lgptr(i2)),
     &                jminbnd(i2),jmaxbnd(i2),kminbnd(i2),kmaxbnd(i2))
                call halocp(ii,nhalo,1,nside1,nside2,
     &                jmax(ii),kmax(ii),turmu(lgptr(ii)),
     &                jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &                kminbnd(ii)-nsd1(ii),kmaxbnd(ii)+nsd3(ii),
     &                jmax(i2),kmax(i2),turmu(lgptr(i2)),
     &                jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &                kminbnd(i2)-nsd1(i2),kmaxbnd(i2)+nsd3(i2))
              endif
 59           continue
          endif
c
          call mg_bcupdate(lvl+1,mglev,
     &          npts2,jdimen,kdimen,q0,pk,s,press,
     &          sndsp,turmu,fmu,vort,turre,vk,ve,
     &          x,y,xy,xyj,xit,ett,ds,
     &          uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &          spectxi,specteta,precon,tmp)
c
c          write(6,*) 'after mg_bcupdate'
c          call flush(6)
c
c         -forming the forcing function on coarse grid
c         note: lvl is fine grid level
          call mk_pk(lvl,mglev,npts2,
     &          jdimen,kdimen,q,q0,pk,s,press,
     &          sndsp,turmu,fmu,vort,turre,vk,ve,
     &          x,y,xy,xyj,xit,ett,ds,
     &          uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &          spectxi,specteta,precon,tmp)
c          write(6,*) 'after mk_pk   level=',lvl
c          call flush(6)
c
c         -call multigrid recursively ... going on coarser grid
c          write(6,*) ' Recursive call to multigrid ..level',lvl
          call multigrd(lvl+1,mglev,lev_top,lev_bot,npts2,
     &          jdimen,kdimen,isbiter,
     &          q,q0,qp,pk,s,press,
     &          sndsp,turmu,fmu,vort,turre,vk,ve,
     &          x,y,xy,xyj,xit,ett,ds,
     &          uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &          spectxi,specteta,precon,tmp)
c
c         -updating the fine qp ... lvl is fine grid
          call mk_qp (lvl,jdimen,kdimen,qp,q,q0,s,xyj,turre)
c
c
c          write(6,*) 'after mk_qp in multigrid   level=',lvl
c          call flush(6)
c
          call mg_bcupdate(lvl,mglev,
     &          npts2,jdimen,kdimen,qp,pk,s,press,
     &          sndsp,turmu,fmu,vort,turre,vk,ve,
     &          x,y,xy,xyj,xit,ett,ds,
     &          uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &          spectxi,specteta,precon,tmp)
c
c          write(6,*) 'after mg_bcupdate in multigrid',lvl
c          call flush(6)
c          stop
c          -done in master_integrat.inc in mb_bcupdate.f
c          do ii=nblkstart,nblkend
c            call calcps(ii,jmax(ii),kmax(ii),qp(lqptr(ii)),
c     &            press(lgptr(ii)),sndsp(lgptr(ii)),
c     &            precon(lprecptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
c     &            jlow(ii)-nsd2(ii),jup(ii)+nsd4(ii),
c     &            klow(ii)-nsd1(ii),kup(ii)+nsd3(ii))
c          enddo
c
          do ii=1,isbiter(lvl,2)
            call integrat(lvl,lev_top,mglev,
     &            npts2,jdimen,kdimen,qp,pk,s,press,
     &            sndsp,turmu,fmu,vort,turre,vk,ve,
     &            x,y,xy,xyj,xit,ett,ds,
     &            uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &            spectxi,specteta,precon,tmp)
          enddo
c        write(6,*) 'finished multigrd.f ... lvl=',lvl
c        call flush(6)
c
          if (w_cycle .and. iwc.eq.1) then
            do ii=nblkstart,nblkend
               call copy_array(jmax(ii), kmax(ii), 4, qp(lqptr(ii)),
     &              q(lqptr(ii)))
            enddo
          endif
 444    continue
      endif
c
 999  continue
c     -reset saved residual values from fine grid.
c      if (lvl.eq.lev_top) then
c        resid = svresid
c        do i=1,3
c          maxres(i) = svmaxres(i)
c        enddo
c        residmx = svresidmx 
c      endif
c 
c
      return
      end
