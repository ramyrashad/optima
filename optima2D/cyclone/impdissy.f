      subroutine impdissy(jdim,kdim,xyj,coef2,coef4,a,b,c,d,e)          
c
#include "../include/arcom.inc"
c                                                                       
      dimension xyj(jdim,kdim)                                          
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension a(jdim,kdim,4),b(jdim,kdim,4,4),c(jdim,kdim,4,4),       
     >          d(jdim,kdim,4,4),e(jdim,kdim,4)                         
c                                                                       
c                                                                       
c                                                                       
      dtd   = dt * thetadt/(1. + phidt)                                 
      smudt = smuim*dt* thetadt/(1. + phidt)                           
c                                                                       
c                                                                       
c  eta inversion part of diagonal scheme                                
c                                                                       
c  all 4 equations have the same terms                                  
c                                                                       
c                                                                       
c     if(meth.eq.1 .or. meth .eq. 2 .or. meth.eq.3 .or. meth.eq.4 .or. meth.eq.5)then
      if (meth.le.5) then
c                                                                       
         if (idmodel.ne.3) then
          k =   klow                                                    
          km1 = k-1                                                     
          do 422 j = jlow,jup                                           
            c2m = coef2(j,km1)*dtd                                      
            c4m = coef4(j,km1)*dtd                                      
              c2 = coef2(j,k)*dtd                                       
              c4 = coef4(j,k)*dtd                                       
            diag1 =      0.                                             
            a(j,k,1) = a(j,k,1) + diag1                                 
            a(j,k,2) = a(j,k,2) + diag1                                 
            a(j,k,3) = a(j,k,3) + diag1                                 
            a(j,k,4) = a(j,k,4) + diag1                                 
            diag2 =      (c2m + c4 + c4m)*xyj(j,k-1)                    
            b(j,k,1,1) = b(j,k,1,1) - diag2                             
            b(j,k,2,2) = b(j,k,2,2) - diag2                             
            b(j,k,3,3) = b(j,k,3,3) - diag2                             
            b(j,k,4,4) = b(j,k,4,4) - diag2                             
            diag3 =      xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                 
            c(j,k,1,1) = c(j,k,1,1) + diag3                             
            c(j,k,2,2) = c(j,k,2,2) + diag3                             
            c(j,k,3,3) = c(j,k,3,3) + diag3                             
            c(j,k,4,4) = c(j,k,4,4) + diag3                             
            diag4 =      (c2 + 3.*c4 +c4m)*xyj(j,k+1)                   
            d(j,k,1,1) = d(j,k,1,1) - diag4                             
            d(j,k,2,2) = d(j,k,2,2) - diag4                             
            d(j,k,3,3) = d(j,k,3,3) - diag4                             
            d(j,k,4,4) = d(j,k,4,4) - diag4                             
            diag5 =      xyj(j,k+2)*c4                                  
            e(j,k,1) = e(j,k,1) + diag5                                 
            e(j,k,2) = e(j,k,2) + diag5                                 
            e(j,k,3) = e(j,k,3) + diag5                                 
            e(j,k,4) = e(j,k,4) + diag5                                 
422       continue                                                      
c                                                                       
          k = kup                                                       
          km1 = k-1                                                     
          do 426 j = jlow,jup                                           
            c2m = coef2(j,km1)*dtd                                      
            c4m = coef4(j,km1)*dtd                                      
              c2 = coef2(j,k)*dtd                                       
              c4 = coef4(j,k)*dtd                                       
            diag1 =       xyj(j,k-2)*c4m                                
            a(j,k,1) = a(j,k,1) + diag1                                 
            a(j,k,2) = a(j,k,2) + diag1                                 
            a(j,k,3) = a(j,k,3) + diag1                                 
            a(j,k,4) = a(j,k,4) + diag1                                 
            diag2 =       (c2m + 3.*c4m + c4)*xyj(j,k-1)                
            b(j,k,1,1) = b(j,k,1,1) - diag2                             
            b(j,k,2,2) = b(j,k,2,2) - diag2                             
            b(j,k,3,3) = b(j,k,3,3) - diag2                             
            b(j,k,4,4) = b(j,k,4,4) - diag2                             
            diag3 =       xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)                
            c(j,k,1,1) = c(j,k,1,1) + diag3                             
            c(j,k,2,2) = c(j,k,2,2) + diag3                             
            c(j,k,3,3) = c(j,k,3,3) + diag3                             
            c(j,k,4,4) = c(j,k,4,4) + diag3                             
            diag4 =       (c2 + c4 + c4m)*xyj(j,k+1)                    
            d(j,k,1,1) = d(j,k,1,1) - diag4                             
            d(j,k,2,2) = d(j,k,2,2) - diag4                             
            d(j,k,3,3) = d(j,k,3,3) - diag4                             
            d(j,k,4,4) = d(j,k,4,4) - diag4                             
            diag5 =       0.                                            
            e(j,k,1) = e(j,k,1) + diag5                                 
            e(j,k,2) = e(j,k,2) + diag5                                 
            e(j,k,3) = e(j,k,3) + diag5                                 
            e(j,k,4) = e(j,k,4) + diag5                                 
426       continue                                                      
c                                                                       
c   fourth order in interior                                            
c                                                                       
        do 428 k = klow+1,kup-1                                         
          km1 = k-1                                                     
          do 430 j = jlow,jup                                           
            c2m = coef2(j,km1)*dtd                                      
            c4m = coef4(j,km1)*dtd                                      
              c2 = coef2(j,k)*dtd                                       
              c4 = coef4(j,k)*dtd                                       
            diag1 =        xyj(j,k-2)*c4m                               
            a(j,k,1) = a(j,k,1) + diag1                                 
            a(j,k,2) = a(j,k,2) + diag1                                 
            a(j,k,3) = a(j,k,3) + diag1                                 
            a(j,k,4) = a(j,k,4) + diag1                                 
            diag2 =      (c2m + 3.*c4m + c4)*xyj(j,k-1)                 
            b(j,k,1,1) = b(j,k,1,1) - diag2                             
            b(j,k,2,2) = b(j,k,2,2) - diag2                             
            b(j,k,3,3) = b(j,k,3,3) - diag2                             
            b(j,k,4,4) = b(j,k,4,4) - diag2                             
            diag3 =       xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)                
            c(j,k,1,1) = c(j,k,1,1) + diag3                             
            c(j,k,2,2) = c(j,k,2,2) + diag3                             
            c(j,k,3,3) = c(j,k,3,3) + diag3                             
            c(j,k,4,4) = c(j,k,4,4) + diag3                             
            diag4 =      (c2 + 3.*c4 + c4m)*xyj(j,k+1)                  
            d(j,k,1,1) = d(j,k,1,1) - diag4                             
            d(j,k,2,2) = d(j,k,2,2) - diag4                             
            d(j,k,3,3) = d(j,k,3,3) - diag4                             
            d(j,k,4,4) = d(j,k,4,4) - diag4                             
            diag5 =       xyj(j,k+2)*c4                                 
            e(j,k,1) = e(j,k,1) + diag5                                 
            e(j,k,2) = e(j,k,2) + diag5                                 
            e(j,k,3) = e(j,k,3) + diag5                                 
            e(j,k,4) = e(j,k,4) + diag5                                 
430       continue                                                      
428     continue                                                        
c
      else
c ***********************************************************************
c **                    Constant coefficient Diss.                     **
c ***********************************************************************
c constant coefficient 4th dissipation                                 
          k = klow                                                      
          do 1422 j = jlow,jup                                          
             rjjj = smudt*coef4(j,k)                                     
             diag1 =       0.                                            
             a(j,k,1) = a(j,k,1) + diag1                                 
             a(j,k,2) = a(j,k,2) + diag1                                 
             a(j,k,3) = a(j,k,3) + diag1                                 
             a(j,k,4) = a(j,k,4) + diag1                                 
             diag2 =      rjjj*xyj(j,k-1)                                
             b(j,k,1,1) = b(j,k,1,1) - diag2                             
             b(j,k,2,2) = b(j,k,2,2) - diag2                             
             b(j,k,3,3) = b(j,k,3,3) - diag2                             
             b(j,k,4,4) = b(j,k,4,4) - diag2                             
             diag3 =      3.*rjjj*xyj(j,k)                               
             c(j,k,1,1) = c(j,k,1,1) + diag3                             
             c(j,k,2,2) = c(j,k,2,2) + diag3                             
             c(j,k,3,3) = c(j,k,3,3) + diag3                             
             c(j,k,4,4) = c(j,k,4,4) + diag3                             
             diag4 =      3.*rjjj*xyj(j,k+1)                             
             d(j,k,1,1) = d(j,k,1,1) - diag4                             
             d(j,k,2,2) = d(j,k,2,2) - diag4                             
             d(j,k,3,3) = d(j,k,3,3) - diag4                             
             d(j,k,4,4) = d(j,k,4,4) - diag4                             
             diag5 =      xyj(j,k+2)*rjjj                                
             e(j,k,1) = e(j,k,1) + diag5                                 
             e(j,k,2) = e(j,k,2) + diag5                                 
             e(j,k,3) = e(j,k,3) + diag5                                 
             e(j,k,4) = e(j,k,4) + diag5                                 
1422      continue                                                     
c                                                                       
          k = kup                                                       
          do 1426 j = jlow,jup                                          
            rjjj = smudt*coef4(j,k)                                     
            diag1 =        xyj(j,k-2)*rjjj                              
            a(j,k,1) = a(j,k,1) + diag1                                 
            a(j,k,2) = a(j,k,2) + diag1                                 
            a(j,k,3) = a(j,k,3) + diag1                                 
            a(j,k,4) = a(j,k,4) + diag1                                 
            diag2 =      3.*rjjj*xyj(j,k-1)                             
            b(j,k,1,1) = b(j,k,1,1) - diag2                             
            b(j,k,2,2) = b(j,k,2,2) - diag2                             
            b(j,k,3,3) = b(j,k,3,3) - diag2                             
            b(j,k,4,4) = b(j,k,4,4) - diag2                             
            diag3 =       3.*rjjj*xyj(j,k)                              
            c(j,k,1,1) = c(j,k,1,1) + diag3                             
            c(j,k,2,2) = c(j,k,2,2) + diag3                             
            c(j,k,3,3) = c(j,k,3,3) + diag3                             
            c(j,k,4,4) = c(j,k,4,4) + diag3                             
            diag4 =      rjjj*xyj(j,k+1)                                
            d(j,k,1,1) = d(j,k,1,1) - diag4                             
            d(j,k,2,2) = d(j,k,2,2) - diag4                             
            d(j,k,3,3) = d(j,k,3,3) - diag4                             
            d(j,k,4,4) = d(j,k,4,4) - diag4                             
            diag5 =       0.                                            
            e(j,k,1) = e(j,k,1) + diag5                                 
            e(j,k,2) = e(j,k,2) + diag5                                 
            e(j,k,3) = e(j,k,3) + diag5                                 
            e(j,k,4) = e(j,k,4) + diag5                                 
1426      continue                                                     
c                                                                       
c   fourth order in interior                                            
c                                                                       
        do 1428 k = klow+1,kup-1                                        
          do 1430 j = jlow,jup                                          
            rjjj = smudt*coef4(j,k)                                     
            diag1 =        xyj(j,k-2)*rjjj                              
            a(j,k,1) = a(j,k,1) + diag1                                 
            a(j,k,2) = a(j,k,2) + diag1                                 
            a(j,k,3) = a(j,k,3) + diag1                                 
            a(j,k,4) = a(j,k,4) + diag1                                 
            diag2 =      4.*rjjj*xyj(j,k-1)                             
            b(j,k,1,1) = b(j,k,1,1) - diag2                             
            b(j,k,2,2) = b(j,k,2,2) - diag2                             
            b(j,k,3,3) = b(j,k,3,3) - diag2                             
            b(j,k,4,4) = b(j,k,4,4) - diag2                             
            diag3 =       6.*rjjj*xyj(j,k)                              
            c(j,k,1,1) = c(j,k,1,1) + diag3                             
            c(j,k,2,2) = c(j,k,2,2) + diag3                             
            c(j,k,3,3) = c(j,k,3,3) + diag3                             
            c(j,k,4,4) = c(j,k,4,4) + diag3                             
            diag4 =      4.*rjjj*xyj(j,k+1)                             
            d(j,k,1,1) = d(j,k,1,1) - diag4                             
            d(j,k,2,2) = d(j,k,2,2) - diag4                             
            d(j,k,3,3) = d(j,k,3,3) - diag4                             
            d(j,k,4,4) = d(j,k,4,4) - diag4                             
            diag5 =       xyj(j,k+2)*rjjj                               
            e(j,k,1) = e(j,k,1) + diag5                                 
            e(j,k,2) = e(j,k,2) + diag5                                 
            e(j,k,3) = e(j,k,3) + diag5                                 
            e(j,k,4) = e(j,k,4) + diag5                                 
1430       continue                                                     
1428     continue                                                       
         endif
      endif
      return                                                            
      end                                                               
