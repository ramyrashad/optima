c     ------------------------------------------------------------------
c     -- compute objective function value --
c     -- m. nemec, aug. 2001 --
c     ------------------------------------------------------------------
      function CalcObj( ifirst, nmax, indx, q,qps,qos,xy,xyj,x,y,turmu,
     &     vort,turre,turps,turos,fmu,press,precon,sndsp,xit,ett,ds,vk,
     &     ve,coef2x,coef4x,coef2y,coef4y,s, uu, vv, ccx, ccy, spectxi,
     &     specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja, icol)

      implicit none

#include "../include/parms.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer ifirst, nmax, ntvar,ifoil,i
      double precision q(*),             press(*),       sndsp(*)
      double precision s(*),             xy(*),          xyj(*)
      double precision xit(*),           ett(*),         ds(*)
      double precision x(*),             y(*),           turmu(*)
      double precision vort(*),          turre(*),       fmu(*)
      double precision vk(*),            ve(*),          tmp(*)
      double precision uu(*),            vv(*),          ccx(*)
      double precision ccy(*),           coef2x(*),      coef2y(*)
      double precision coef4x(*),        coef4y(*),      precon(*)
      double precision spectxi(*),       specteta(*),    wk1(*)
      double precision qps(*),           qos(*)
      double precision turps(*),         turos(*)

      integer ia(*), ja(*), ipa(*), jpa(*), icol(*), indx(*)

      double precision CalcObj,rld,fcd,fcl,thc,goc

      call flow_solve (ifirst, nmax, indx, q,qps,qos, xy, xyj, x, y,
     &        turmu, vort,turre,turps,turos,fmu, press,precon,sndsp,xit,
     &        ett,ds, vk, ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,
     &        ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,
     &        ia, ja, icol)

      rewind (n_his)
      rewind (n_this)

c     -- detection of negative Jacobian for optimization runs --
      if (badjac) return

      if (iobjf .eq. 1) then
c     -- inverse design --

         CalcObj = 0.0
         do ifoil = 1,nfoils
            do i = 1, icpn(ifoil)
               CalcObj = CalcObj + 0.5*( cpi(i,ifoil) -
     &              cp_tar(i,ifoil) )**2
            end do
         end do

      else if (iobjf .eq. 2) then
c     -- weighted objective function for lift and drag optimization --

         call constraints(thc,goc)

c     -- objective --
         fcl = ( 1.0 - clt/cl_tar )**2

         if (cdt.gt.cd_tar) then
            CalcObj = wfd*( 1.0 - cdt/cd_tar )**2
         else
            CalcObj = 0.0
         end if
         if ( wfd.gt.1.e-15 ) fcd = CalcObj/wfd
         
         write (n_out,100) thc, goc
         call flush(n_out)

         CalcObj = CalcObj + wfl*fcl + thc + goc

      else if (iobjf .eq. 3) then
c     -- lift to drag ratio objective --

         call constraints(thc,goc)
            
         rld = clt/cdt

         write (n_out,100) thc, goc         
         call flush(n_out)
         CalcObj = wfl*( 1.0 - rld/cl_tar )**2 + thc + goc
      end if

 100  format(3x,'Thickness penalty:',e12.4,' Gap-Overlap penalty:',
     &     e12.4)

      return
      end                       !CalcObj
