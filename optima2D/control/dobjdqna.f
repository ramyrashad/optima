c----------------------------------------------------------------------
c     -- subroutine to calculate the derivative of the objective
c     function with respect to the state vector q using
c     finite differences for the case when alpha varies in the
c     flow solver. (q(na) = alpha) --
c     
c     written by: laura billing, july 2005
c     modified from dOBJdQ
c----------------------------------------------------------------------
      subroutine dOBJdQna (jdim, kdim, ndim, x, y, xy, xyj, q, dOdQna,
     &      cp_tar, cpa, press, obj0)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim, kdim, ndim, i, j, jj, k, n, na, diff_flag

      double precision q(jdim,kdim,ndim),xyj(jdim,kdim),xy(jdim,kdim,4)
      double precision x(jdim,kdim), y(jdim,kdim)
      double precision cp_tar(jbody,2), cpa(jdim,kdim), press(jdim,kdim)
      double precision presstemp(jdim,kdim), dobjdcd, dobjdcl

      double precision dOdQna, dcddalph, dcdvvdalph, cn, cc, cnv, ccv
      double precision uxi, vxi, ueta, veta, xix, xiy, etax, etay
      double precision tauw, dcldalph, dclvvdalph, dcdtdalph, dcltdalph

      double precision z(jdim)
      double precision obj0

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c  dOdQna - partial derivative of objective function wrt angle of attack, for 
c    variable angle of attack problems.
c  dcddalph - partial derivative of coefficient of drag due to pressure wrt 
c    angle of attack.
c  dcdvvdalph - partial derivative of coefficient of drag due to friction wrt 
c    angle of attack.
c  cn, cnv - normal force coefficients due to pressure and friction respectively.
c  cc, ccv - chord directed force coefficients due to pressure and friction 
c    respectively.
c  uxi, vxi - partial derivatives of u wrt xi, v wrt xi.  Similarly for ueta,
c    veta, xix (xi wrt x), xiy, etax, etay.
c

      dOdQna = 0.d0

c     -- note 1: q does not contain jacobian --


c      iflag = 0
c      if (obj_func.eq.6) then
c         obj_func=3
c         iflag=1
c      end if

      if ( obj_func.eq.8) then
c     -- drag minimization with variable angle of attack --

clb     CALCULATING DCDDALPHA ANALYTICALLY

c     the parameter nscal determines whether the q variables are scaled 
c     by the metric jacobians xyj.   

         nscal = 1
         cpc = 2.d0/(gamma*fsmach**2)                             
c                                                                                                                             
         dcddalph = 0.d0
         dcdvvdalph = 0.d0
c                                                                       
c  set limits of integration                                            
c                                                                       
         j1 = jlow                                                      
         j2 = jup                                                       
         jtp = jlow                                                     
         if(.not.periodic)then                                          
            j1 = jtail1                                                 
            j2 = jtail2                                                 
            jtp = jtail1+1                                              
         endif                                                          
c                                                                       
c     compute cp at grid points and store in z array     
c                                                                       
         do 10 j=j1,j2                                                  
            pp = press(j,1)                                             
            if( nscal .eq.0) pp = pp*xyj(j,1)                           
            z(j) = (pp*gamma -1.d0)*cpc                                 
 10      continue
         cn = 0.                                                        
         cc = 0.                                                        
         cmle = 0.
         do 11 j=jtp,j2                                                 
            jm1 = jminus(j)                                             
            cpav = (z(j) + z(jm1))*.5                                   
            cn = cn - cpav*(x(j,1) - x(jm1,1))                          
            cc = cc + cpav*(y(j,1) - y(jm1,1))                          
            cmle = cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))  
 11      continue

         dcddalph = pi/180.d0*(cn*cos(alpha*pi/180.d0) 
     &        - cc*sin(alpha*pi/180.d0))
c                                                                       
         if(viscous)then                                                
            cinf  = 1.                                                  
            alngth= 1.                                                  
c     re already has fsmach scaling                                       
            amu   = rhoinf*alngth/re                                    
            uinf2 = fsmach**2                                           
c     
            k= 1                                                        
            ja = jlow                                                   
            jb = jup                                                    
c                                                                       
            if(.not.periodic)then                                       
               j = jtail1                                               
               jp1 = j+1                                                
               uxi = q(jp1,k,2)/q(jp1,k,1)-q(j,k,2)/q(j,k,1)            
               vxi = q(jp1,k,3)/q(jp1,k,1)-q(j,k,3)/q(j,k,1)            
               ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)    
     *              -.5*q(j,k+2,2)/q(j,k+2,1)                           
               veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)    
     *              -.5*q(j,k+2,3)/q(j,k+2,1)
c     
               xix = xy(j,k,1)                                          
               xiy = xy(j,k,2)                                          
               etax = xy(j,k,3)                                         
               etay = xy(j,k,4)                                         
               tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))      
               z(j)= tauw/(.5*rhoinf*uinf2)                             
c     
               j = jtail2                                               
               jm1 = j-1                                                
               uxi = q(j,k,2)/q(j,k,1)-q(jm1,k,2)/q(jm1,k,1)            
               vxi = q(j,k,3)/q(j,k,1)-q(jm1,k,3)/q(jm1,k,1)            
               ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)    
     *              -.5*q(j,k+2,2)/q(j,k+2,1)                           
               veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)    
     *              -.5*q(j,k+2,3)/q(j,k+2,1)                           
c     
               xix = xy(j,k,1)
               xiy = xy(j,k,2)
               etax = xy(j,k,3)
               etay = xy(j,k,4)
               tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))      
               z(j)= tauw/(.5*rhoinf*uinf2)
c                                                                       
c     -set new limits                                                       
               ja = jtail1+1                                            
               jb = jtail2-1                                            
            endif                                                       
c     
            do 110 j = ja,jb                                            
               jp1 = jplus(j)                                           
               jm1 = jminus(j)                                          
               uxi = .5*(q(jp1,k,2)/q(jp1,k,1)-q(jm1,k,2)/q(jm1,k,1))   
               vxi = .5*(q(jp1,k,3)/q(jp1,k,1)-q(jm1,k,3)/q(jm1,k,1))   
               ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)    
     *              -.5*q(j,k+2,2)/q(j,k+2,1)                           
               veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)    
     *              -.5*q(j,k+2,3)/q(j,k+2,1)                           
               xix = xy(j,k,1)                                          
               xiy = xy(j,k,2)                                          
               etax = xy(j,k,3)                                         
               etay = xy(j,k,4)                                         
               tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))      
               z(j)= tauw/(.5*rhoinf*uinf2)                             
c                                                                       
 110        continue
c     write (*,*) 'done 110'
            if (sngvalte) then
c     -average trailing edge value
               z(j1)=.5d0*(z(j1)+z(j2))
               z(j2)=z(j1)
            endif

            cnv = 0.                                                    
            ccv = 0.                                                    
            cmlev = 0.                                                  
            do 111 j=jtp,j2                                             
               jm1 = jminus(j)                                          
               cfav = (z(j) + z(jm1))*.5                               
               ccv = ccv + cfav*(x(j,1) - x(jm1,1))                     
               cnv = cnv + cfav*(y(j,1) - y(jm1,1))                     
               cmlev = cmlev + cfav*                                    
     *              (  (x(j,1)+x(jm1,1))*.5*(y(j,1) -y(jm1,1)) -        
     *              (y(j,1)+y(jm1,1))*.5*(x(j,1) -x(jm1,1))   )         
 111        continue              

            dcdvvdalph = pi/180.d0*(cnv*cos(alpha*pi/180.d0) 
     &           -ccv*sin(alpha*pi/180.d0))
         
         endif
         
         dOdQna = (dcddalph+dcdvvdalph)/obj0
         
clb    END CALCULATING DCDDALPHA ANALYTICALLY

      !-- Off-design constraint function 16: Maximum Mach number 
      !-- Max Mach number is not dependent on angle of attack
      !-- Therefore dOdQna = 0.0

      else if (obj_func.eq.16) then

         dOdQna = 0.0

      !-- Off-design constraint function 17: Maximum Mach number with
      !-- KS function   
      !-- Max Mach number is not dependent on angle of attack
      !-- Therefore dOdQna = 0.0

      else if (obj_func.eq.17) then

         dOdQna = 0.0

      else if (obj_func.eq.20.or.obj_func.eq.21) then

         !-- Objective function J = 1/R (Inverse aircraft range factor):
         !-- R = (Mach)*(Cl/Cd)
         !-- J = Cd/(Mach*Cl)
         !-- 
         !-- OR
         !--
         !-- Objective function J = Cd/Cl (OBJ_FUNC.eq.21)
         !-- Inverse aircraft endurance factor

         !-- Objective function J = 1/R (Inverse aircraft range factor):
         !-- R = (Mach)*(Cl/Cd)
         !-- J = Cd/(Mach*Cl)
         
         diff_flag = 0

         if (diff_flag.eq.0) then

            !-- Add +ve perturbation to alpha
            tmp = alpha
            stepsize = fd_eta*alpha

            alpha = alpha + stepsize
            stepsize = alpha - tmp


            !-- Calculate dOdX at (+) --
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &           cdi, cmi, clv, cdv, cmv) 
            cdtp = cdi + cdv
            cltp = cli + clv

            if (obj_func.eq.20) then
               objp = cdtp/(cltp*fsmach)
               objp = objp/obj0
            else if (obj_func.eq.21) then
               objp = cdtp/cltp
               objp = objp/obj0
            end if
         
            !-- Add -ve perturbation to alpha
            alpha = tmp - stepsize

            !-- Calculate dOdX at (-) --
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &           cdi, cmi, clv, cdv, cmv) 
            cdtm = cdi + cdv
            cltm = cli + clv

            if (obj_func.eq.20) then
               objm = cdtm/(cltm*fsmach)
               objm = objm/obj0
            else if (obj_func.eq.21) then
               objm = cdtm/cltm
               objm = objm/obj0
            end if

            dOdQna = ( objp - objm )/(2.d0*stepsize)
            
            !-- Set alpha back to unperturbed value
            alpha = tmp  
      
         else if (diff_flag.eq.1) then


            !-- dJ/d(alpha) = [dJ/d(Cd)]*[d(Cd)/d(alpha)] 
            !--                   + [dJ/d(Cl)]*[d(Cl)/d(alpha)]

            !-- Calculate [dJ/d(Cd)] and  [dJ/d(Cl)]

            do n = 1,4
               do k = 2,3
                  do j = jtail1,jtail2

                     press(j,k) = gami*(q(j,k,4) - 0.5d0*( q(j,k,2)**2 +
     &                    q(j,k,3)**2 )/q(j,k,1) )
                     
                  end do
               end do
            end do

            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1,
     &           cli, cdi, cmi, clv, cdv, cmv)
            cdt = cdi + cdv
            clt = cli + clv
            
            dobjdcd = 1.d0/(fsmach*clt)
            dobjdcl = -cdt/(fsmach*(clt**2))
            
            !-- Calculating [d(Cd)/d(alpha)] and [d(Cl)/d(alpha)]
            !-- analytically

c     the parameter nscal determines whether the q variables are scaled 
c     by the metric jacobians xyj.   

            nscal = 1
            cpc = 2.d0/(gamma*fsmach**2)                             
c     
            dcddalph = 0.d0
            dcdvvdalph = 0.d0
            dcldalph = 0.d0
            dclvvdalph = 0.d0
c     
c  set limits of integration                                            
c                                                                       
            j1 = jlow   
            j2 = jup          
            jtp = jlow    
            if(.not.periodic)then  
               j1 = jtail1 
               j2 = jtail2
               jtp = jtail1+1 
            endif   
c                                                                       
c     compute cp at grid points and store in z array     
c                                                                       
            do 1000 j=j1,j2      
               pp = press(j,1)   
               if( nscal .eq.0) pp = pp*xyj(j,1) 
               z(j) = (pp*gamma -1.d0)*cpc 
 1000       continue
            cn = 0.     
            cc = 0.   
            cmle = 0.
            do 1100 j=jtp,j2                                    
               jm1 = jminus(j)  
               cpav = (z(j) + z(jm1))*.5  
               cn = cn - cpav*(x(j,1) - x(jm1,1))
               cc = cc + cpav*(y(j,1) - y(jm1,1)) 
               cmle=cmle + cpav*(x(j,1)+x(jm1,1))*.5*(x(j,1) -x(jm1,1))
 1100       continue

            dcddalph = pi/180.d0*(cn*cos(alpha*pi/180.d0) 
     &           - cc*sin(alpha*pi/180.d0))

            dcldalph = pi/180.d0*(-cn*sin(alpha*pi/180.d0) 
     &           + cc*cos(alpha*pi/180.d0))
c                                                                       
            if(viscous)then 
               cinf  = 1.
               alngth= 1. 
c     re already has fsmach scaling   
               amu   = rhoinf*alngth/re  
               uinf2 = fsmach**2  
c     
               k= 1  
               ja = jlow  
               jb = jup  
c                                                                       
               if(.not.periodic)then  
                  j = jtail1  
                  jp1 = j+1          
                  uxi = q(jp1,k,2)/q(jp1,k,1)-q(j,k,2)/q(j,k,1)
                  vxi = q(jp1,k,3)/q(jp1,k,1)-q(j,k,3)/q(j,k,1)
                  ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)
     *                 -.5*q(j,k+2,2)/q(j,k+2,1)   
                  veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)
     *                 -.5*q(j,k+2,3)/q(j,k+2,1)
c     
                  xix = xy(j,k,1)    
                  xiy = xy(j,k,2) 
                  etax = xy(j,k,3)  
                  etay = xy(j,k,4)  
                  tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))
                  z(j)= tauw/(.5*rhoinf*uinf2)     
c     
                  j = jtail2  
                  jm1 = j-1 
                  uxi = q(j,k,2)/q(j,k,1)-q(jm1,k,2)/q(jm1,k,1) 
                  vxi = q(j,k,3)/q(j,k,1)-q(jm1,k,3)/q(jm1,k,1)
                  ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)
     *                 -.5*q(j,k+2,2)/q(j,k+2,1)   
                  veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)
     *                 -.5*q(j,k+2,3)/q(j,k+2,1)  
c     
                  xix = xy(j,k,1)
                  xiy = xy(j,k,2)
                  etax = xy(j,k,3)
                  etay = xy(j,k,4)
                  tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax)) 
                  z(j)= tauw/(.5*rhoinf*uinf2)
c                                                                       
c     -set new limits                                                       
                  ja = jtail1+1  
                  jb = jtail2-1   
               endif     
c     
               do 11000 j = ja,jb
                  jp1 = jplus(j)                                           
                  jm1 = jminus(j)                                          
                  uxi = .5*(q(jp1,k,2)/q(jp1,k,1)-q(jm1,k,2)/q(jm1,k,1))   
                  vxi = .5*(q(jp1,k,3)/q(jp1,k,1)-q(jm1,k,3)/q(jm1,k,1))   
                  ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k+1,2)/q(j,k+1,1)    
     *                 -.5*q(j,k+2,2)/q(j,k+2,1)                           
                  veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k+1,3)/q(j,k+1,1)    
     *                 -.5*q(j,k+2,3)/q(j,k+2,1)                           
                  xix = xy(j,k,1)                                          
                  xiy = xy(j,k,2)                                          
                  etax = xy(j,k,3)                                         
                  etay = xy(j,k,4)                                         
                  tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))      
                  z(j)= tauw/(.5*rhoinf*uinf2)                             
c                                                                       
11000          continue
c     write (*,*) 'done 110'
               if (sngvalte) then
c     -average trailing edge value
                  z(j1)=.5d0*(z(j1)+z(j2))
                  z(j2)=z(j1)
               endif
               
               cnv = 0.                                                    
               ccv = 0.                                                    
               cmlev = 0.                                                  
               do 11100 j=jtp,j2 
                  jm1 = jminus(j)                                          
                  cfav = (z(j) + z(jm1))*.5                               
                  ccv = ccv + cfav*(x(j,1) - x(jm1,1))                     
                  cnv = cnv + cfav*(y(j,1) - y(jm1,1))                     
                  cmlev = cmlev + cfav*                                    
     *                 (  (x(j,1)+x(jm1,1))*.5*(y(j,1) -y(jm1,1)) -        
     *                 (y(j,1)+y(jm1,1))*.5*(x(j,1) -x(jm1,1))   )         
11100          continue              
               
               dcdvvdalph = pi/180.d0*(cnv*cos(alpha*pi/180.d0) 
     &              -ccv*sin(alpha*pi/180.d0))
               dclvvdalph = pi/180.d0*(-cnv*sin(alpha*pi/180.d0) 
     &              +ccv*cos(alpha*pi/180.d0))
               dcdtdalph = dcddalph+dcdvvdalph
               dcltdalph = dcldalph+dclvvdalph          
               
            endif
            
            dOdQna = dobjdcd*dcdtdalph/obj0 + dobjdcl*dcltdalph/obj0
            !dOdQna = dOdQna/obj0
            
         end if ! (diff_flag.eq.1)

      else
         write(scr_unit,*) 
     &   'called dobjdqna with wrong objective function'
         stop

      end if

      return
      end                       !dobjdqna
