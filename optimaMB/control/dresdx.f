c     ------------------------------------------------------------------
c     -- calculate the sensitivity of the rhs residual dR/dX where Xs
c     are the design variables --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine dResdX ( in, q, press, sndsp, dRdX, xy, xyj, xit, ett,
     &     ds, x, y, turmu, vort, turre,fmu, vk, ve, tmp, uu, vv, ccx,
     &     ccy, coef2x, coef2y, coef4x,coef4y, precon, spectxi,
     &     specteta, dRsadX, xs, ys, Rm, Rp)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/turb.inc"
#include "../include/scratch.inc"

      integer in,ib,ifoil,i,nside1,nside2,lq,lg,i2
      double precision q(*),         press(*),     sndsp(*)
      double precision dRdX(*),      xy(*),        xyj(*)
      double precision xit(*),       ett(*),       ds(*)
      double precision x(*),         y(*),         turmu(*)
      double precision vort(*),      turre(*),     fmu(*)
      double precision vk(*),        ve(*),        tmp(*)
      double precision uu(*),        vv(*),        ccx(*)
      double precision ccy(*),       coef2x(*),    coef2y(*)
      double precision coef4x(*),    coef4y(*),    precon(*)
      double precision spectxi(*),   specteta(*),  dRsadX(*)

      double precision xs(*), ys(*)
      double precision Rm(*), Rp(*), Rsap(maxjk), Rsam(maxjk)

      double precision exprhs(maxjkq),dhatq(6*maxjkq)  
      double precision exprsa(maxjk),dhatt(6*maxjk)  

c     -- locals --
      double precision bapf(ibody,2)
      double precision dt_tmp,dvtmp,stepsize,dxx,cosang,sinang

      dt_tmp = dt
      dt = 1.d0

c     -- using cenral-differences to evaluate rhs sensitivity --
c     -- second order accurate --
      dvtmp = dvs(in)
      stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
      if ( abs(stepsize).lt.1.e-10 ) stepsize = 1.e-10*sign(1.0,
     &     stepsize)
c     -- calculate rhs at (+) --

      dvs(in) = dvs(in) + stepsize
      stepsize = dvs(in) - dvtmp

      if ( in.le.ngdv ) then
c     -- regrid domain --
c     -- store bap --            
         ifoil = idvf(in)
         do i = 1, ibap(ifoil) 
            bapf(i,1) = bap(i,1,ifoil)
            bapf(i,2) = bap(i,2,ifoil)
         end do
         call regrid ( (in), x, y)
c     -- restore bap --
         ifoil = idvf(in)  
         do i = 1, ibap(ifoil) 
            bap(i,1,ifoil) = bapf(i,1)
            bap(i,2,ifoil) = bapf(i,2)
         end do

c     -- recalculate metrics --
         do ib = 1,nblks 
            call xymets(ib, nblks, nhalo, jmax(ib), kmax(ib),
     &           x(lgptr(ib)), y(lgptr(ib)), xy(lqptr(ib)),
     &           xit(lgptr(ib)), ett(lgptr(ib)), xyj(lgptr(ib)),  
c     &           nsd1(ib), nsd2(ib),nsd3(ib), nsd4(ib), 
     &           jbegin(ib),
     &           jend(ib), kbegin(ib),kend(ib), jminbnd(ib),
     &           jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib))
         end do

c     -- copy column of metrics for halos --
         do ib = 1,nblks
            do nside1 = 2,4,2
               i2 = lblkpt(ib,nside1)
               if (i2.ne.0) then
                  nside2 = lsidept(ib,nside1)
                  call fixmets( nhalo, nside1, nside2, jmax(ib),
     &                 kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &                 jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &                 kminbnd(ib), kmaxbnd(ib), jmax(i2), kmax(i2),
     &                 xy(lqptr(i2)), xyj(lgptr(i2)),
     &                 jbegin(i2)-nsd2(i2), jend(i2)+nsd4(i2),
     &                 kminbnd(i2), kmaxbnd(i2))
               endif
            end do
         end do

c     -- halo row metrics copy --
         do ib = 1, nblks
            do nside1 = 1,3,2
               i2 = lblkpt(ib,nside1)
               if (i2.ne.0 .and. 
     &              (ibctype(ib,nside1).ne.5 .or. nhalo.ge.2)) then
                  nside2 = lsidept(ib,nside1)
                  call fixmets( nhalo, nside1, nside2, jmax(ib),
     &                 kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &                 jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &                 kminbnd(ib)-nsd1(ib), kmaxbnd(ib)+nsd3(ib),
     &                 jmax(i2), kmax(i2), xy(lqptr(i2)),
     &                 xyj(lgptr(i2)), jbegin(i2)-nsd2(i2),
     &                 jend(i2)+nsd4(i2), kminbnd(i2)-nsd1(i2),
     &                 kmaxbnd(i2)+nsd3(i2) )
               endif
            end do
         end do
      else                      ! alpha is a design variable
         alpha = dvs(in)
         cosang = cos(pi*alpha/180.0)
         sinang = sin(pi*alpha/180.0) 
         uinf   = fsmach*cosang    
         vinf   = fsmach*sinang
      end if

c     -- recalculate rhs of cyclone --
      do ib = 1,nblks
         call scjac( ib, jmax(ib), kmax(ib), q(lqptr(ib)), 
     &        xyj(lgptr(ib)), jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
      end do

      do ib=1,nblks
         call calcps(ib,jmax(ib),kmax(ib),q(lqptr(ib)),
     &        press(lgptr(ib)),sndsp(lgptr(ib)),
     &        precon(lprecptr(ib)),xy(lqptr(ib)),xyj(lgptr(ib)),
     &        jbegin(ib)-nsd2(ib),jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib),kend(ib)+nsd3(ib))
      end do

      if (viscous .and. turbulnt) then
c     -- update distances for the s-a model (array smin) --
         call autostan( 1, 1, maxj, maxk, x, y, d, d(1,2), .false.)
         do ib=1,nblks
            lg = lgptr(ib)
            call caldist(jmax(ib), kmax(ib), x(lg), y(lg), smin(lg),
     &           jlow(ib), jup(ib), klow(ib), kup(ib), icoord, xcoord,
     &           ycoord)
         end do
      end if

c     -- current implementation could be improved since perturbations
c     at the body only influence few blocks in the grid --
      call get_rhs(q,q,q, press, sndsp, Rp, xy, xyj, xit, ett, ds, x, y,
     &     turmu,vort,turre,turre,turre,fmu,vk,ve,tmp, uu, vv, ccx, ccy,
     &     coef2x, coef2y, coef4x, coef4y, precon, spectxi, specteta,
     &     Rsap,exprhs,dhatq,exprsa,dhatt,.false.)

c     -- remove jacobian scaling from q vector --
      do ib = 1,nblks
         call qmuj( jmax(ib), kmax(ib), q(lqptr(ib)), xyj(lgptr(ib)),
     &        jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
      end do

c     -- calculate rhs at (-) --
      dvs(in) = dvtmp - stepsize

      if ( in.le.ngdv ) then
c     -- restore grid --
         do ib = 1,nblks
            lg = lgptr(ib)
            call copy_array( jmax(ib), kmax(ib), 1, xs(lg), x(lg))
            call copy_array( jmax(ib), kmax(ib), 1, ys(lg), y(lg))
         end do
c     -- regrid domain --
         call regrid ( (in), x, y)
c     -- restore bap --
         ifoil = idvf(in)  
         do i = 1, ibap(ifoil) 
            bap(i,1,ifoil) = bapf(i,1)
            bap(i,2,ifoil) = bapf(i,2)
         end do

c     -- recalculate metrics --
         do ib = 1,nblks 
            call xymets(ib, nblks, nhalo, jmax(ib), kmax(ib),
     &           x(lgptr(ib)), y(lgptr(ib)), xy(lqptr(ib)),
     &           xit(lgptr(ib)), ett(lgptr(ib)), xyj(lgptr(ib)),
c     &           nsd1(ib), nsd2(ib), nsd3(ib), nsd4(ib), 
     &       jbegin(ib),
     &           jend(ib), kbegin(ib), kend(ib), jminbnd(ib),
     &           jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib))
         end do

c     -- copy column of metrics for halos --
         do ib = 1,nblks
            do nside1 = 2,4,2
               i2 = lblkpt(ib,nside1)
               if (i2.ne.0) then
                  nside2 = lsidept(ib,nside1)
                  call fixmets( nhalo, nside1, nside2, jmax(ib),
     &                 kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &                 jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &                 kminbnd(ib), kmaxbnd(ib), jmax(i2), kmax(i2),
     &                 xy(lqptr(i2)), xyj(lgptr(i2)),
     &                 jbegin(i2)-nsd2(i2), jend(i2)+nsd4(i2),
     &                 kminbnd(i2), kmaxbnd(i2))
               endif
            end do
         end do

c     -- halo row metrics copy --
         do ib = 1, nblks
            do nside1 = 1,3,2
               i2 = lblkpt(ib,nside1)
               if (i2.ne.0 .and. 
     &              (ibctype(ib,nside1).ne.5 .or. nhalo.ge.2)) then
                  nside2 = lsidept(ib,nside1)
                  call fixmets( nhalo, nside1, nside2, jmax(ib),
     &                 kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &                 jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &                 kminbnd(ib)-nsd1(ib), kmaxbnd(ib)+nsd3(ib),
     &                 jmax(i2), kmax(i2), xy(lqptr(i2)),
     &                 xyj(lgptr(i2)), jbegin(i2)-nsd2(i2),
     &                 jend(i2)+nsd4(i2), kminbnd(i2)-nsd1(i2),
     &                 kmaxbnd(i2)+nsd3(i2) )
               endif
            end do
         end do      
      else                      ! alpha is a design variable
         alpha = dvs(in)
         cosang = cos(pi*alpha/180.0)
         sinang = sin(pi*alpha/180.0) 
         uinf   = fsmach*cosang    
         vinf   = fsmach*sinang
      end if

c     -- recalculate rhs of cyclone --
      do ib = 1,nblks
         call scjac( ib, jmax(ib), kmax(ib), q(lqptr(ib)), 
     &        xyj(lgptr(ib)), jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
      end do

      do ib=1,nblks
         call calcps(ib,jmax(ib),kmax(ib),q(lqptr(ib)),
     &        press(lgptr(ib)),sndsp(lgptr(ib)),
     &        precon(lprecptr(ib)),xy(lqptr(ib)),xyj(lgptr(ib)),
     &        jbegin(ib)-nsd2(ib),jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib),kend(ib)+nsd3(ib))
      end do

      if (viscous .and. turbulnt) then
c     -- update distances for the s-a model (array smin) --
         call autostan( 1, 1, maxj, maxk, x, y, d, d(1,2), .false.)
         do ib=1,nblks
            lg = lgptr(ib)
            call caldist(jmax(ib), kmax(ib), x(lg), y(lg), smin(lg),
     &           jlow(ib), jup(ib), klow(ib), kup(ib), icoord, xcoord,
     &           ycoord)
         end do
      end if

c     -- current implementation could be improved since perturbations
c     at the body only influence few blocks in the grid --
      call get_rhs(q,q,q, press, sndsp, Rm, xy, xyj, xit, ett, ds, x, y,
     &     turmu,vort,turre,turre,turre,fmu,vk,ve,tmp,uu,vv, ccx, ccy,
     &     coef2x, coef2y, coef4x, coef4y, precon, spectxi, specteta,
     &     Rsam,exprhs,dhatq,exprsa,dhatt,.false.)
     
c     -- remove jacobian scaling from q vector --
      do ib = 1,nblks
         call qmuj( jmax(ib), kmax(ib), q(lqptr(ib)), xyj(lgptr(ib)),
     &        jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &        kbegin(ib)-nsd1(ib), kend(ib)+nsd3(ib))
      end do

c     -- evaluate sensitivity of the flow residual --
      dxx = 1.d0/(2.d0*stepsize)
      do ib = 1,nblks
         lq = lqptr(ib)
         lg = lgptr(ib)
         call fdrdx(dxx, jminbnd(ib), jmaxbnd(ib), kminbnd(ib),
     &        kmaxbnd(ib), jmax(ib), kmax(ib), Rp(lq), Rm(lq), dRdX(lq),
     &        Rsap(lg), Rsam(lg), dRsadX(lg), viscous, turbulnt)
      end do

      dvs(in) = dvtmp
      dt = dt_tmp

      return
      end                       !drdx
