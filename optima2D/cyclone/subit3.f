      subroutine subit3(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy,lu)
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),s(jdim,kdim,4)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),sndsp(jdim,kdim)
      double precision    lu(jdim,kdim,4,5)

c
      if (ix .eq. 1) then
         call tkinv(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy)
         if (meth.eq.4) then
            do 125 n=1,4
               call xlubcsb3(lu(1,1,n,2),lu(1,1,n,3),lu(1,1,n,4),
     *                      s(1,1,n),jdim,kdim,jlow,jup,klow,kup)   
 125        continue
         else
            do 130 n=1,4
               call xlubcsb(lu(1,1,n,1),lu(1,1,n,2),lu(1,1,n,3),
     *                      lu(1,1,n,4),lu(1,1,n,5),s(1,1,n),jdim,
     *                      kdim,jlow,jup,klow,kup)
 130        continue
         endif
         if (cmesh) call tk(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy)
      else
         if ( .not. cmesh) then 
            ixx = 1                                               
            ixy = 2                                               
            iyx = 3                                               
            iyy = 4                                               
            call ninver(jdim,kdim,s,xy,ixx,ixy,iyx,iyy)
         else 
            call tkinv(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy)
         endif
c
         if (meth.eq.4) then
            do 150 n=1,4
               call ylubcsb3(lu(1,1,n,2),lu(1,1,n,3),lu(1,1,n,4),
     *                      s(1,1,n),jdim,kdim,jlow,jup,klow,kup)
 150        continue
         else
            do 155 n=1,4
               call ylubcsb(lu(1,1,n,1),lu(1,1,n,2),lu(1,1,n,3),
     *                      lu(1,1,n,4),lu(1,1,n,5),s(1,1,n),jdim,
     *                      kdim,jlow,jup,klow,kup)
 155        continue
         endif
         call tk(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy)
      endif
c
      return
      end
