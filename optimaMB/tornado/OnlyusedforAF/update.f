C ********************************************************************
C **************** SOLUTION UPDATE ROUTINE  **************************
C ** update the interior, interfaces and halo points                **
C ** the data for the boundaries will be incorrect but that doesn't **
C ** matter because it is overwritten by the bc routines.           **
C ********************************************************************
      subroutine update(jdim,kdim,q,s,jlow,jup,klow,kup)
c
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4)
c
      phic = phidt/(1.+phidt)                                        
c
c     update solution interior only
c     update the boundary conditions later
c
      do 5 n = 1,4      
      do 5 k = klow,kup    
      do 5 j = jlow,jup   
        q(j,k,n) = q(j,k,n)+s(j,k,n) 
c       -initialize rhs for next iteration (if phidt.ne.0)
        s(j,k,n) = phic * s(j,k,n) 
 5    continue
c                                    
      return                     
      end                       
