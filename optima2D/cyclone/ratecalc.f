      subroutine ratecalc(istart,numiter,resid)                       

#include "../include/units.inc"

      data resold,resnew/1.,0./                                       
                                                                       
c                 compute convergence rate                              
c                 set initial value at 50 steps                         
                                                                       
      expon = 0.01                                                   
c                                                                       
      if(numiter-istart+1 .eq. 50) then                              
        resnew = resid                                           
c              expon is 1/(number of iterations in interval)
        expon = 0.02                                             
      endif                                                          
c                                                                       
c                 every 100 steps compute convergence rate              
c                                                                       
      if( mod( numiter - istart + 1, 100) .eq. 0) then               
        resold = resnew                                             
        resnew = resid                                              
        ratio  = resnew/resold                                      
        rate = abs(ratio)**expon                                    
        write (out_unit,*)
        write (out_unit,*)
     &        '++++++++++++++++++++++++++++++++++++++++++++++++++++++'  
        write (out_unit,*)
     &        '|                                                    |'  
      write (out_unit,99) numiter,rate
 99   format (26h | Convergence rate at n =,I6,2x,2his,f10.7,8x,1h|)
      write (out_unit,*)
     &      '|                                                    |'  
      write (out_unit,*)
     &      '++++++++++++++++++++++++++++++++++++++++++++++++++++++'  
      write (out_unit,*)
      endif                                                          
c                                                                       
      return                                                         
      end                                                            
