          FUNCTION TIMER_SECONDS(DUMMY)
C
C  CPU TIMING FUNCTION 
C
C   IN THIS CASE FOR AN ASEMBLY FOR AN IRIS2500T
C
C  REPLACE THIS WITH WHATEVER WORKS ON YOUR MACHINE
C
C  FUNCTION SHOULD RETURN IN SECONDS THE CURRENT ELAPSED CPU TIME
C
C  THE DUMMY ARGUMENT IS ADDED BECAUSE SOME SYSTEM OT INTRINSIC
C  CALLS REQUIRE ONE.  FOR EXAMPLE, ON CRAY MACHINES SECONDIS A
C  BUILTIN REFERENCED AS:   TIME = SECOND(DUMMY)
C
C  IRIS2500T uses asembly code clock.as
C
c      real*4 tt(2)
c      integer isec
C
c      timer_seconds = sngl(etime(tt))
cccc      call ftime(isec)
cccc      timer_seconds = isec
C
      REAL*4 TARRAY(2), TMP
C
C   Note that we only count the user time(!)
C
      TMP     = ETIME(TARRAY)
      timer_seconds  = TARRAY(1)
      RETURN
      END

