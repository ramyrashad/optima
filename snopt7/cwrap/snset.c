#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "snoptch.h"

/* *************************************************************** */

int csnset( snProblem *prob, char Option[], int userCall )
{
  int    iPrint, iSumm, iExit = 0;
  int    lenopt = strlen(Option);

  iPrint = (prob->print_file_unit && userCall) ? prob->print_file_unit : 0;
  iSumm  = (prob->sumry_file_unit && userCall) ? prob->sumry_file_unit : 0;

  snset_( Option, &iPrint, &iSumm, &iExit,
	  prob->cw, &(prob->length_cw),
	  prob->iw, &(prob->length_iw),
	  prob->rw, &(prob->length_rw),
	  lenopt,
	  prob->length_cw );

  return iExit;
}

/* *************************************************************** */

int snSetOptionString( snProblem *prob,  char Option[] )
{
  int userCall = 1, iExit;
  csnset( prob, Option, userCall );

  return iExit;
}

/* *************************************************************** */

int csnseti( snProblem *prob, char Option[], int iOption, int userCall )
{
  int        iPrint, iSumm, iExit = 0;
  int        lenopt = strlen(Option);

  iPrint = (prob->print_file_unit && userCall) ? prob->print_file_unit : 0;
  iSumm  = (prob->sumry_file_unit && userCall) ? prob->sumry_file_unit : 0;

  snseti_( Option, &iOption, &iPrint, &iSumm, &iExit,
	   prob->cw, &(prob->length_cw),
	   prob->iw, &(prob->length_iw),
	   prob->rw, &(prob->length_rw),
	   lenopt,
	   prob->length_cw );

  return iExit;
}

/* *************************************************************** */

int snSetOptionInteger( snProblem *prob, char Option[], int iOption )
{
  int userCall = 1, iExit;
  csnseti( prob, Option, iOption, userCall );

  return iExit;
}

/* *************************************************************** */

int csnsetd( snProblem *prob, char Option[],  double dOption, int userCall )
{
  int        iPrint, iSumm, iExit = 0;
  int        lenOpt = strlen(Option);

  iPrint = (prob->print_file_unit && userCall) ? prob->print_file_unit : 0;
  iSumm  = (prob->sumry_file_unit && userCall) ? prob->sumry_file_unit : 0;

  snsetr_( Option, &dOption, &iPrint, &iSumm, &iExit,
	   prob->cw, &(prob->length_cw),
	   prob->iw, &(prob->length_iw),
	   prob->rw, &(prob->length_rw),
	   lenOpt,
	   prob->length_cw );

  return iExit;
}

/* *************************************************************** */

int snSetOptionDouble( snProblem *prob, char Option[], double dOption )
{
  int userCall = 1, iExit;
  csnsetd( prob, Option, dOption, userCall );

  return iExit;
}

/* *************************************************************** */

int snSetObjectiveAdd( snProblem* prob, double objective_add )
{
  prob->option.objective_add = objective_add;
  return SN_OK;
}

/* *************************************************************** */

int snSetObjectiveFunction( snProblem* prob, snObjectiveFunc func )
{
  prob->objective_func = func;
  return SN_OK;
}
/* *************************************************************** */

int snSetUserFunction( snProblem* prob, snUserFunc func )
{
  prob->user_func = func;
  return SN_OK;
}
/* *************************************************************** */

int snSetConstraintFunction( snProblem* prob, snConstraintFunc func )
{
  prob->constraint_func = func;
  return SN_OK;
}

/* *************************************************************** */

int snSetProblemName( snProblem* prob, char *ProblemName )
{
  prob->option.ProblemName = ProblemName;
  return SN_OK;
}

/* *************************************************************** */

int snSetVariableBounds( snProblem* prob, int var, double lower, double upper )
{
  assert( var >= 0 && var < prob->dimension.n );

  prob->bu[ var ] = upper;
  prob->bl[ var ] = lower;

  return SN_OK;
}

/* *************************************************************** */

int snSetConstraintBounds( snProblem* prob, int con, double lower, double upper )
{
  assert( con >= 0 && con < prob->dimension.m );

  prob->bu[ con + prob->dimension.n ] = upper;
  prob->bl[ con + prob->dimension.n ] = lower;

  return SN_OK;
}

/* *************************************************************** */

int snSetNonzero( snProblem *prob, int nz, double Jcol, int indJ )
{

  assert( nz >= 0 && nz < prob->dimension.ne );

  prob->Jcol[ nz ] = Jcol;
  prob->indJ[ nz ] = indJ;

  return SN_OK;
}

/* *************************************************************** */

int snSetNonzeroColumnPointer( snProblem* prob, int col, int locJ )
{
  assert( col >= 0 && col < (prob->dimension.n + 1) );

  prob->locJ[ col ] = locJ;

  return SN_OK;
}

/* *************************************************************** */

int snSetVariable( snProblem* prob, int var, int hs, double xs )
{

  assert( var >= 0 && var < prob->dimension.n );

  prob->hs[ var ] = hs;
  prob->xs[ var ] = xs;

  return SN_OK;
}

/* *************************************************************** */

int snSetConstraint( snProblem* prob, int con, int hs, double xs )
{

  assert( con >= 0 && con < prob->dimension.m );

  prob->hs[ con + prob->dimension.n ] = hs;
  prob->xs[ con + prob->dimension.n ] = xs;

  return SN_OK;
}

/* *************************************************************** */

int snSetMultiplier( snProblem* prob, int con, double pi )
{
  assert( con >= 0 && con < prob->dimension.m );

  prob->pi[ con ] = pi;

  return SN_OK;
}

/* *************************************************************** */

int snSetPrintFileName( snProblem* prob, char* print_file_name )
{
  int str_len;

  if ( prob->option.sumry_file_name != NULL )
    if ( strcmp( print_file_name , prob->option.sumry_file_name ) == 0 ) {
      snSetError( prob, SN_FILE_ERROR,
		  "specified print file identical to summary file." );
      return SN_ERROR;
    }

  /* free if memory has already been assigned allocated */
  if ( prob->option.print_file_name != NULL )
    free( prob->option.print_file_name );

  str_len = strlen( print_file_name ) + 1;
  prob->option.print_file_name = (char*)malloc( sizeof(char) * str_len );
  strcpy( prob->option.print_file_name , print_file_name );

  return SN_OK;
}

/* *************************************************************** */

int snSetSummaryFileName( snProblem* prob, char* sumry_file_name )
{
  int str_len;

  if ( prob->option.print_file_name != NULL )
    if ( strcmp( sumry_file_name , prob->option.print_file_name ) == 0 ) {
      snSetError( prob, SN_FILE_ERROR,
		  "specified summary file identical to print file." );
      return SN_ERROR;
    }

  /* free if memory has already been assigned allocated */
  if ( prob->option.sumry_file_name != NULL )
    free( prob->option.sumry_file_name );

  str_len = strlen( sumry_file_name ) + 1;
  prob->option.sumry_file_name = (char*)malloc( sizeof(char) * str_len );
  strcpy( prob->option.sumry_file_name , sumry_file_name );

  return SN_OK;
}
