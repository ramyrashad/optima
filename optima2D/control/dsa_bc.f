c----------------------------------------------------------------------
c     -- differentiation of boundary conditions for the S-A model --
c     written by: marian nemec
c     date: jan. 2001
c----------------------------------------------------------------------

      subroutine dsa_bc (jdim,kdim,ndim,indx,icol,q,xy,xyj,pa,as)

      use disscon_vars

#include "../include/arcom.inc"

      integer jdim, kdim, ndim, indx(jdim,kdim), icol(9),jsta,jsto

clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
clb
      double precision q(jdim,kdim,5), xy(jdim,kdim,4),xyj(jdim,kdim)

c     -----------------------------------------------------------------
c     -- airfoil body => nu_tilde = 0 --
c     -----------------------------------------------------------------
      k = 1
      n1 = 5
      n2 = 5
      do j = jtail1, jtail2
        ij = ( indx(j,k)-1)*ndim + n1
        ii = ( ij - 1 )*icol(9)
        if (clopt) ii = ( ij - 1 )*(icol(9)+1)
        ip = ( ij - 1 )*icol(5)
        
        if (dissCon) then
           as(ii+n2) = xyj(j,k)*(1.d0 + lamDissT)
           pa(ip+n2) = xyj(j,k)*(1.d0 + lamDissT)
        else
           as(ii+n2) = xyj(j,k)
           pa(ip+n2) = xyj(j,k)
        end if
           
      end do

c     -----------------------------------------------------------------
c     -- outer-boundary  k = kmax --
c     -----------------------------------------------------------------

      if(.not.periodic) then
         jsta=jbegin+1
         jsto=jend-1
      else
         jsta=jbegin
         jsto=jend
      end if

      do j = jsta,jsto
        par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
        xy3 = xy(j,kend,3)/par
        xy4 = xy(j,kend,4)/par

        u   = q(j,kend,2)/q(j,kend,1)
        v   = q(j,kend,3)/q(j,kend,1)
        vn = xy3*u + xy4*v

        k = kend
        n1 = 5
        n2 = 5

        if (vn.le.0.d0) then
c     -- inflow --
          ij = ( indx(j,k)-1)*ndim + n1
          ii = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          ip = ( ij - 1 )*icol(5)

          as(ii+icol(1)+n2) = xyj(j,k)
          pa(ip+icol(1)+n2) = xyj(j,k)
        else
c     -- outflow --          
          ij = ( indx(j,k)-1)*ndim + n1
          ii = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          ip = ( ij - 1 )*icol(5)

          as(ii+        n2) = -xyj(j,kup)
          as(ii+icol(1)+n2) = xyj(j,kend)

          pa(ip+        n2) = -xyj(j,kup)
          pa(ip+icol(1)+n2) = xyj(j,kend)
        endif
      end do

c     -----------------------------------------------------------------
c     -- j = 1 and j = jend boundary --
c     -----------------------------------------------------------------
      if(.not.periodic) then

      do k = kbegin,kend
        n1 = 5
        n2 = 5
        j = 1
        ij = ( indx(j,k)-1)*ndim + n1
        ii = ( ij - 1 )*icol(9)
        if (clopt) ii = ( ij - 1 )*(icol(9)+1)
        ip = ( ij - 1 )*icol(5)

        as(ii+        n2) = xyj(j,k)
        as(ii+icol(1)+n2) = -xyj(jplus(j),k)
        pa(ip+        n2) = xyj(j,k)
        pa(ip+icol(1)+n2) = -xyj(jplus(j),k)

        j = jend
        ij = ( indx(j,k)-1)*ndim + n1
        ii = ( ij - 1 )*icol(9)
        if (clopt) ii = ( ij - 1 )*(icol(9)+1)
        ip = ( ij - 1 )*icol(5)

        as(ii+        n2) = -xyj(jminus(j),k)
        as(ii+icol(1)+n2) = xyj(j,k)
        pa(ip+        n2) = -xyj(jminus(j),k)
        pa(ip+icol(1)+n2) = xyj(j,k)

      end do

      end if  !not periodic

c     -----------------------------------------------------------------
c     -- wake-cut --
c     -----------------------------------------------------------------
      if(.not.periodic) then

      k = 1
      n = 5
      do j  = jbegin+1,jtail1-1
        ii = ( indx(j,k) - 1 )*ndim + n
        jj = ( ii - 1 )*icol(9)
        if (clopt) jj = ( ii - 1 )*(icol(9)+1)
        jf = jmax-j+1
        as(jj+        n) =   1.d0*xyj(j,1)
        as(jj+icol(1)+n) = - 0.5d0*xyj(j,2)
        as(jj+icol(2)+n) = - 0.5d0*xyj(jf,2)

        ip = ( ii - 1 )*icol(5)
        pa(ip+        n) =   1.d0*xyj(j,1)
        pa(ip+icol(1)+n) = - 0.5d0*xyj(j,2)
        pa(ip+icol(2)+n) = - 0.5d0*xyj(jf,2)
      end do

c     -- same for top of wake cut --
      do j  = jtail2+1,jend-1
        ii = ( indx(j,k) - 1 )*ndim + n
        jj = ( ii - 1 )*icol(9)
        if (clopt) jj = ( ii - 1 )*(icol(9)+1)
        jf = jmax-j+1
        as(jj+        n) =   1.d0*xyj(j,1)
        as(jj+icol(1)+n) = - 0.5d0*xyj(j,2)
        as(jj+icol(2)+n) = - 0.5d0*xyj(jf,2)

        ip = ( ii - 1 )*icol(5)
        pa(ip+        n) =   1.d0*xyj(j,1)
        pa(ip+icol(1)+n) = - 0.5d0*xyj(j,2)
        pa(ip+icol(2)+n) = - 0.5d0*xyj(jf,2)
      end do

      end if  !not periodic

      return
      end                       !dsa_bc
