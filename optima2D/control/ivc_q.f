c----------------------------------------------------------------------
c     -- implicit  viscous cross terms  --
c     -- differentiation of the viscous flux vector w.r.t. Q^ --
c     -- written by:  markus rumpfkeil  --
c     -- date: jan. 2006
c     Note: we need -Re^(-1)*d_xi Ehat and -Re^(-1)*d_eta Fhat
c----------------------------------------------------------------------

      subroutine ivc_q (jdim,kdim,ndim,xy,xyj,q,fmu,turmu,press,dmul,  
     &     dmut,dj,dk,dpj,dmj,dpk,dmk)

      implicit none

#include "../include/arcom.inc"
#include "../include/visc.inc"

      integer jdim, kdim,ndim,j,k,n,jp1,jm1,kp1,km1,kmm

      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision fmu(jdim,kdim),turmu(jdim,kdim)
      double precision dj(ndim,3,jdim,kdim), dk(ndim,3,jdim,kdim) 
      double precision dpj(ndim,3,jdim,kdim), dmj(ndim,3,jdim,kdim) 
      double precision dpk(ndim,3,jdim,kdim), dmk(ndim,3,jdim,kdim)
      double precision q2(jdim,kdim,4), turmtmp(jdim,kdim)
      double precision press(jdim,kdim)

      double precision dmul(ndim,jdim,kdim),dmut(5,jdim,kdim)
      double precision dmuttmp(5,jdim,kdim)

      double precision a1(jdim,kdim), a2(jdim,kdim), a3(jdim,kdim)
      double precision a4(jdim,kdim), a5(jdim,kdim)

      double precision hre,hfmu,t1,t2,t3,t4,r1,ah1,ah2,ah3,ah4,ah5
      double precision uxi,vxi,c2xi,ueta,veta,c2eta
      double precision vflux1,vflux2,vflux3,rrm1,rrp1,S1,S2

c     prlam   =  laminar prandtl number  = .72                      
c     prturb  =  turbulent prandtl number = .90                     
c     prlinv  =  1./(laminar prandtl number)                        
c     prtinv  =  1./(turbulent prandtl number)                      
c     f13     =  1/3                                                
c     f43     =  4/3                                                
c     hre     =  1/2 * reynolds number                           
c     fmu     =  laminar viscosity
c     turmu   =  turbulent viscosity      

      hre = 1.d0/(2.d0*re)
 
c  for cmesh logic apply cross terms at k = 1 along wake cut with       
c                                                                       
c                    k+1 = 2, k-1 = 1, k = 1                            
                                                           
      kmm = 1                                                        
      if(cmesh .and. wake) kmm = 0      

c     mu, turmu at node points for cross terms for more compactness
                                                                       
      do 305 j = jbegin,jend 
      do 305 n=1,5
      turmtmp(j,1) = 0.0d0 
      dmuttmp(n,j,1) = 0.0d0 
      do 305 k = klow,kend                                            
      turmtmp(j,k) = 0.5d0*(turmu(j,k)+turmu(j,k-kmm)) 
      dmuttmp(n,j,k) =0.5d0*(dmut(n,j,k)+dmut(n,j,k-kmm))
  305 continue  


c---------------------------------------------------------------------  
c       (e_eta)_xi    terms                      
c---------------------------------------------------------------------  
 

c     -- calculate alfas and useful variables --
      do k = kbegin,kend                                              
        do j = jbegin,jend 
c     t1 = xi_x * eta_x
c     t2 = xi_x * eta_y
c     t3 = xi_y * eta_x
c     t4 = xi_y * eta_y

          t1      = xy(j,k,1)*xy(j,k,3)                               
          t2      = xy(j,k,1)*xy(j,k,4)                               
          t3      = xy(j,k,2)*xy(j,k,3)                               
          t4      = xy(j,k,2)*xy(j,k,4)                                
          r1      = hre/xyj(j,k)
          
          a1(j,k) = r1*( t1     +   t4    ) 
          a2(j,k) = r1*( f43*t1 +   t4    ) 
          a3(j,k) = r1*( t1     +   f43*t4)                        
          a4(j,k) = r1*(-f23*t2 +   t3    )
          a5(j,k) = r1*( t2     -   f23*t3)

          r1   = 1.d0/q(j,k,1)                                        
          q2(j,k,1) = r1                                           
          q2(j,k,2) = r1*q(j,k,2)                                  
          q2(j,k,3) = r1*q(j,k,3)                                  
          q2(j,k,4) = r1*q(j,k,4) - (q2(j,k,2)**2 +q2(j,k,3)**2)
        end do
      end do



c     -- entries for viscous Jacobian, treating mu as constant -- 
      do k = klow,kup
        kp1 = k+1                                                       
        km1 = k-kmm
        do j = jbegin,jend

c     -- laminar + turbulent viscosity at nodes --
          t1 = fmu(j,k) + turmtmp(j,k)
c     -- mu_l/Pr_l + mu_t/Pr_t --
          t2 = fmu(j,k)*prlinv + turmtmp(j,k)*prtinv

c     -- evaluate alfas at k   factor 1/2 already in hre--
          ah1 =  a1(j,k) *t2*gamma
          ah2 =  a2(j,k) *t1
          ah3 =  a3(j,k) *t1
          ah4 =  a4(j,k) *t1
          ah5 =  a5(j,k) *t1
                                  
          ueta = q2(j,kp1,2)-q2(j,km1,2)  
          veta = q2(j,kp1,3)-q2(j,km1,3)
          S1   = ah2*ueta+ah4*veta
          S2   = ah5*ueta+ah3*veta
       
          dmk (1,1,j,k) = -(ah2*q2(j,km1,2)+ah4*q2(j,km1,3))*q2(j,km1,1)
          dpk (1,1,j,k) =  (ah2*q2(j,kp1,2)+ah4*q2(j,kp1,3))*q2(j,kp1,1)
          dmk (2,1,j,k) =  ah2*q2(j,km1,1)                              
          dpk (2,1,j,k) = -ah2*q2(j,kp1,1)
          dmk (3,1,j,k) =  ah4*q2(j,km1,1)
          dpk (3,1,j,k) = -ah4*q2(j,kp1,1)
          dmk (4,1,j,k) = 0.d0
          dpk (4,1,j,k) = 0.d0
          dmk (1,2,j,k) = -(ah5*q2(j,km1,2)+ah3*q2(j,km1,3))*q2(j,km1,1)
          dpk (1,2,j,k) =  (ah5*q2(j,kp1,2)+ah3*q2(j,kp1,3))*q2(j,kp1,1)
          dmk (2,2,j,k) =  ah5*q2(j,km1,1)                              
          dpk (2,2,j,k) = -ah5*q2(j,kp1,1)                            
          dmk (3,2,j,k) =  ah3*q2(j,km1,1)                              
          dpk (3,2,j,k) = -ah3*q2(j,kp1,1)                            
          dmk (4,2,j,k) = 0.d0                                          
          dpk (4,2,j,k) = 0.d0   
                                     
          dmk (1,3,j,k) =-ah1*q2(j,km1,4)*q2(j,km1,1)
     &         + q2(j,k,2)*dmk(1,1,j,k) + q2(j,k,3)*dmk(1,2,j,k)
          dk  (1,3,j,k) = (S1*q2(j,k,2)+S2*q2(j,k,3))*q2(j,k,1)
          dpk (1,3,j,k) = ah1*q2(j,kp1,4)*q2(j,kp1,1) 
     &         + q2(j,k,2)*dpk(1,1,j,k) + q2(j,k,3)*dpk(1,2,j,k)    

          dmk (2,3,j,k) =-ah1*q2(j,km1,2)*q2(j,km1,1)+q2(j,k,2)*
     &         dmk(2,1,j,k)+q2(j,k,3)*dmk(2,2,j,k) 
          dk  (2,3,j,k) = -S1*q2(j,k,1)
          dpk (2,3,j,k) = ah1*q2(j,kp1,2)*q2(j,kp1,1)+q2(j,k,2)*
     &         dpk(2,1,j,k)+q2(j,k,3)*dpk(2,2,j,k)
     
          dmk (3,3,j,k) =-ah1*q2(j,km1,3)*q2(j,km1,1)+q2(j,k,2)*
     &         dmk(3,1,j,k)+q2(j,k,3)*dmk(3,2,j,k) 
          dk  (3,3,j,k) = -S2*q2(j,k,1)
          dpk (3,3,j,k) = ah1*q2(j,kp1,3)*q2(j,kp1,1)+q2(j,k,2)*
     &         dpk(3,1,j,k)+q2(j,k,3)*dpk(3,2,j,k) 
     
          dmk (4,3,j,k) =  ah1*q2(j,km1,1)                              
          dpk (4,3,j,k) = -ah1*q2(j,kp1,1)                            
        end do
      end do

      if (turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kup
          do j = jlow,jup
            do n = 1,3   
              dk(5,n,j,k) = 0.d0
              dpk(5,n,j,k) = 0.d0
              dmk(5,n,j,k) = 0.d0
            end do
          end do
        end do
      end if

c     -- compute derivative terms due to laminar and turbulent (sa)
c     viscosity: dmu is negative to be consistent with inviscid terms --
      do k = klow,kup
        kp1 = k+1
        km1 = k-kmm  
        do j = jbegin,jend

c     -- calculate viscous flux vector without viscosity terms --
c     -- factor 1/2 already in hre--
          rrm1  = 1./q(j,km1,1)        
          rrp1  = 1./q(j,kp1,1)

          ueta  = q2(j,kp1,2)-q2(j,km1,2)  
          veta  = q2(j,kp1,3)-q2(j,km1,3)
                                                  
          c2eta = gamma*(press(j,kp1)*rrp1 - press(j,km1)*rrm1)/gami

          vflux1 = a2(j,k)*ueta + a4(j,k)*veta
          vflux2 = a5(j,k)*ueta + a3(j,k)*veta
          vflux3 = q2(j,k,2)*vflux1 + q2(j,k,3)*vflux2 

c     -- dmu/dQ terms factor 0.5d0 from taking central difference--
          do n = 1,ndim
            dmk(n,1,j,k) = dmk(n,1,j,k) - 0.5d0*vflux1*(dmul(n,j,km1)+
     &            dmuttmp(n,j,k))  
            dmk(n,2,j,k) = dmk(n,2,j,k) - 0.5d0*vflux2*(dmul(n,j,km1)+
     &            dmuttmp(n,j,k)) 
            dmk(n,3,j,k) = dmk(n,3,j,k) - 0.5d0*(vflux3*(dmul(n,j,km1)+
     &            dmuttmp(n,j,km1))+a1(j,km1)*c2eta*(dmul(n,j,km1)*
     &            prlinv+dmuttmp(n,j,km1)*prtinv) )  

            dpk(n,1,j,k) = dpk(n,1,j,k) - 0.5d0*vflux1*(dmul(n,j,kp1) +
     &            dmuttmp(n,j,kp1)) 
            dpk(n,2,j,k) = dpk(n,2,j,k) - 0.5d0*vflux2*(dmul(n,j,kp1) +
     &            dmuttmp(n,j,kp1)) 
            dpk(n,3,j,k) = dpk(n,3,j,k) - 0.5d0*(vflux3*(dmul(n,j,kp1)+
     &            dmuttmp(n,j,kp1))+a1(j,kp1)*c2eta*(dmul(n,j,kp1)* 
     &            prlinv+dmuttmp(n,j,kp1)*prtinv))

            if (n.eq.5) then
              if (abs(dk(5,3,j,k)) .lt. 5.d-16) then
                dk(5,1,j,k) = 0.d0
                dk(5,2,j,k) = 0.d0
                dk(5,3,j,k) = 0.d0
              end if
              if (abs(dpk(5,3,j,k)) .lt. 5.d-16) then
                dpk(5,1,j,k) = 0.d0
                dpk(5,2,j,k) = 0.d0
                dpk(5,3,j,k) = 0.d0
              end if
              if (abs(dmk(5,3,j,k)) .lt. 5.d-16) then
                dmk(5,1,j,k) = 0.d0
                dmk(5,2,j,k) = 0.d0
                dmk(5,3,j,k) = 0.d0
              end if
            end if

          end do
        end do
      end do

c---------------------------------------------------------------------  
c      (f_xi)_eta    terms                      
c---------------------------------------------------------------------  
 


c     -- entries for viscous Jacobian, treating mu as constant -- 
      do j = jlow,jup
        jp1 = jplus(j)
        jm1 = jminus(j)
        do k = kbegin,kend

c     -- laminar + turbulent viscosity at nodes --
          t1 = fmu(j,k) + turmtmp(j,k)
c     -- mu_l/Pr_l + mu_t/Pr_t --
          t2 = fmu(j,k)*prlinv + turmtmp(j,k)*prtinv

c     -- evaluate alfas at j --
          ah1 =  a1(j,k) *t2*gamma
          ah2 =  a2(j,k) *t1
          ah3 =  a3(j,k) *t1
          ah4 =  a4(j,k) *t1
          ah5 =  a5(j,k) *t1
                                  
          uxi = q2(jp1,k,2)-q2(jm1,k,2)  
          vxi = q2(jp1,k,3)-q2(jm1,k,3)
          S1  = ah2*uxi+ah5*vxi
          S2  = ah4*uxi+ah3*vxi

          dmj (1,1,j,k) = -(ah2*q2(jm1,k,2)+ah5*q2(jm1,k,3))*q2(jm1,k,1)
          dpj (1,1,j,k) =  (ah2*q2(jp1,k,2)+ah5*q2(jp1,k,3))*q2(jp1,k,1)
          dmj (2,1,j,k) =  ah2*q2(jm1,k,1)                              
          dpj (2,1,j,k) = -ah2*q2(jp1,k,1)
          dmj (3,1,j,k) =  ah5*q2(jm1,k,1)
          dpj (3,1,j,k) = -ah5*q2(jp1,k,1)
          dmj (4,1,j,k) = 0.d0
          dpj (4,1,j,k) = 0.d0
          dmj (1,2,j,k) = -(ah4*q2(jm1,k,2)+ah3*q2(jm1,k,3))*q2(jm1,k,1)
          dpj (1,2,j,k) =  (ah4*q2(jp1,k,2)+ah3*q2(jp1,k,3))*q2(jp1,k,1)
          dmj (2,2,j,k) =  ah4*q2(jm1,k,1)                              
          dpj (2,2,j,k) = -ah4*q2(jp1,k,1)                            
          dmj (3,2,j,k) =  ah3*q2(jm1,k,1)                              
          dpj (3,2,j,k) = -ah3*q2(jp1,k,1)                            
          dmj (4,2,j,k) = 0.d0                                          
          dpj (4,2,j,k) = 0.d0   
                                     
          dmj (1,3,j,k) =-ah1*q2(jm1,k,4)*q2(jm1,k,1)
     &         + q2(j,k,2)*dmj(1,1,j,k) + q2(j,k,3)*dmj(1,2,j,k)
          dj  (1,3,j,k) = (S1*q2(j,k,2)+S2*q2(j,k,3))*q2(j,k,1)
          dpj (1,3,j,k) = ah1*q2(jp1,k,4)*q2(jp1,k,1) 
     &         + q2(j,k,2)*dpj(1,1,j,k) + q2(j,k,3)*dpj(1,2,j,k)    

          dmj (2,3,j,k) =-ah1*q2(jm1,k,2)*q2(jm1,k,1)+q2(j,k,2)*
     &         dmj(2,1,j,k)+q2(j,k,3)*dmj(2,2,j,k) 
          dj  (2,3,j,k) = -S1*q2(j,k,1)
          dpj (2,3,j,k) = ah1*q2(jp1,k,2)*q2(jp1,k,1)+q2(j,k,2)*
     &         dpj(2,1,j,k)+q2(j,k,3)*dpj(2,2,j,k)
     
          dmj (3,3,j,k) =-ah1*q2(jm1,k,3)*q2(jm1,k,1)+q2(j,k,2)*
     &         dmj(3,1,j,k)+q2(j,k,3)*dmj(3,2,j,k) 
          dj  (3,3,j,k) = -S2*q2(j,k,1)
          dpj (3,3,j,k) = ah1*q2(jp1,k,3)*q2(jp1,k,1)+q2(j,k,2)*
     &         dpj(3,1,j,k)+q2(j,k,3)*dpj(3,2,j,k) 
     
          dmj (4,3,j,k) =  ah1*q2(jm1,k,1)                              
          dpj (4,3,j,k) = -ah1*q2(jp1,k,1)   
                                     
        end do
      end do

      if (turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kup
          do j = jlow,jup
            do n = 1,3              
              dj(5,n,j,k) = 0.d0
              dpj(5,n,j,k) = 0.d0
              dmj(5,n,j,k) = 0.d0
            end do
          end do
        end do
      end if

c     -- compute derivative terms due to laminar and turbulent (sa)
c     viscosity: dmu is negative to be consistent with inviscid terms --
      do j = jlow,jup
        jp1 = jplus(j)
        jm1 = jminus(j)
        do k = kbegin,kend

c     -- calculate viscous flux vector without viscosity terms --
          rrm1  = 1./q(jm1,k,1)          
          rrp1  = 1./q(jp1,k,1) 

          uxi  = q2(jp1,k,2)-q2(jm1,k,2)  
          vxi  = q2(jp1,k,3)-q2(jm1,k,3)
                                                           
          c2xi = gamma*(press(jp1,k)*rrp1  - press(jm1,k)*rrm1)/gami

          vflux1 = a2(j,k)*uxi + a5(j,k)*vxi
          vflux2 = a4(j,k)*uxi + a3(j,k)*vxi
          vflux3 = q2(j,k,2)*vflux1 + q2(j,k,3)*vflux2

c     -- dmu/dQ terms factor 0.5d0 from taking central difference--
          do n = 1,ndim
            dmj(n,1,j,k) = dmj(n,1,j,k) - 0.5d0*vflux1*(dmul(n,jm1,k) +
     &           dmuttmp(n,jm1,k))  
            dmj(n,2,j,k) = dmj(n,2,j,k) - 0.5d0*vflux2*(dmul(n,jm1,k) +
     &           dmuttmp(n,jm1,k)) 
            dmj(n,3,j,k) = dmj(n,3,j,k) - 0.5d0*(vflux3*(dmul(n,jm1,k)+
     &           dmuttmp(n,jm1,k))+a1(jm1,k)*c2xi*(dmul(n,jm1,k)*prlinv+
     &           dmuttmp(n,jm1,k)*prtinv) )  

            dpj(n,1,j,k) = dpj(n,1,j,k) - 0.5d0*vflux1*(dmul(n,jp1,k) +
     &           dmuttmp(n,jp1,k)) 
            dpj(n,2,j,k) = dpj(n,2,j,k) - 0.5d0*vflux2*(dmul(n,jp1,k) +
     &           dmuttmp(n,jp1,k)) 
            dpj(n,3,j,k) = dpj(n,3,j,k) - 0.5d0*( vflux3*(dmul(n,jp1,k)+
     &           dmuttmp(n,jp1,k))+a1(jp1,k)*c2xi*(dmul(n,jp1,k)*prlinv+ 
     &           dmuttmp(n,jp1,k)*prtinv))

            if (n.eq.5) then
              if (abs(dj(5,3,j,k)) .lt. 5.d-16) then
                dj(5,1,j,k) = 0.d0
                dj(5,2,j,k) = 0.d0
                dj(5,3,j,k) = 0.d0
              end if
              if (abs(dpj(5,3,j,k)) .lt. 5.d-16) then
                dpj(5,1,j,k) = 0.d0
                dpj(5,2,j,k) = 0.d0
                dpj(5,3,j,k) = 0.d0
              end if
              if (abs(dmj(5,3,j,k)) .lt. 5.d-16) then
                dmj(5,1,j,k) = 0.d0
                dmj(5,2,j,k) = 0.d0
                dmj(5,3,j,k) = 0.d0
              end if            
            end if

          end do
        end do
      end do

      return                                                            
      end                       !ivc_q
