C #########################
C ##                     ##
C ##  SUBROUTINE SWSIGN  ##
C ##                     ##
C #########################
C
      SUBROUTINE SWSIGN (JDIM,KDIM,NDIM,VAR,NB,NE,JB,JE,KB,KE)           
C
      DIMENSION VAR(JDIM,KDIM,NDIM)                                     
C
C                                                                       
C---- FORM JACOBIAN ----
      DO 30 N=NB,NE                                                     
      DO 30 K=KB,KE                                                     
      DO 30 J=JB,JE                                                     
            VAR(J,K,N) = -1.d0*VAR(J,K,N)
 30   CONTINUE                                                          
C
C                                                                       
      RETURN                                                            
      END                                                               
