c     ------------------------------------------------------------------
c     -- linearization of the inverse design objective function with
c     respect to the q vector --
c     -- m. nemec, oct. 2001 --
c     -- called from: dobjdq.f --
c     ------------------------------------------------------------------
      subroutine dides( ic, ifoil, is, js, je, ks, ke, jmax, kmax,
     &     xyj, q, dOdQ, gami, fsmach, viscous, cpi, cp_tar)

      implicit none
      
#include "../include/parms.inc"
#include "../include/optcom.inc"

      integer ic, ifoil, is, js, je, ks, ke, jmax, kmax, j, k
      double precision xyj(jmax,kmax), q(jmax,kmax,4), dOdQ(jmax,kmax,4)
      double precision cp_tar(ibody,maxfoil+1), cpi(ibody,maxfoil)
      double precision gami,fsmach
      logical   viscous

      if (is.eq.1) then

         k = ks

         if (.not. viscous) then
            do j = js, je
               dOdQ(j,k,1) = ( xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami/q(j,k,1)**2*( q(j,k,2)**2 +
     &              q(j,k,3)**2)/(fsmach*fsmach) )

               dOdQ(j,k,2) = ( - xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*q(j,k,2)/q(j,k,1)*2.0
     &              /(fsmach*fsmach) )

               dOdQ(j,k,3) = ( - xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami*q(j,k,3)/q(j,k,1)*2.0
     &              /(fsmach*fsmach))

               dOdQ(j,k,4) = (xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*2.0/(fsmach*fsmach) )

               ic = ic+1
            end do
         else                   !viscous
            do j = js,je
               dOdQ(j,k,1) = ( xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami/q(j,k,1)**2*( q(j,k,2)**2 +
     &              q(j,k,3)**2)/(fsmach*fsmach) )

               dOdQ(j,k,4) = (xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*2.0/(fsmach*fsmach) )

               ic = ic+1
            end do
         end if

      else if (is.eq.2) then
         j = js

         if (.not. viscous) then
            do k = ks, ke
               dOdQ(j,k,1) = ( xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami/q(j,k,1)**2*( q(j,k,2)**2 +
     &              q(j,k,3)**2)/(fsmach*fsmach) )

               dOdQ(j,k,2) = ( - xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*q(j,k,2)/q(j,k,1)*2.0
     &              /(fsmach*fsmach) )

               dOdQ(j,k,3) = ( - xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami*q(j,k,3)/q(j,k,1)*2.0
     &              /(fsmach*fsmach))

               dOdQ(j,k,4) = (xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*2.0/(fsmach*fsmach) )

               ic = ic+1
            end do
         else                   !viscous
            do k = ks,ke
               dOdQ(j,k,1) = ( xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami/q(j,k,1)**2*( q(j,k,2)**2 +
     &              q(j,k,3)**2)/(fsmach*fsmach) )

               dOdQ(j,k,4) = (xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*2.0/(fsmach*fsmach) )

               ic = ic+1
            end do
         end if

      else if (is.eq.3) then 

         k = ke

         if (.not. viscous) then
            do j = je, js, -1
               dOdQ(j,k,1) = ( xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami/q(j,k,1)**2*( q(j,k,2)**2 +
     &              q(j,k,3)**2)/(fsmach*fsmach) )

               dOdQ(j,k,2) = ( - xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*q(j,k,2)/q(j,k,1)*2.0
     &              /(fsmach*fsmach) )

               dOdQ(j,k,3) = ( - xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami *q(j,k,3)/q(j,k,1)*2.0
     &              /(fsmach*fsmach))

               dOdQ(j,k,4) = (xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*2.0/(fsmach*fsmach) )
               ic = ic+1
            end do
         else                   !viscous
            do j = je, js, -1
               dOdQ(j,k,1) = ( xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil))*gami/q(j,k,1)**2*( q(j,k,2)**2 +
     &              q(j,k,3)**2)/(fsmach*fsmach) )

               dOdQ(j,k,4) = (xyj(j,k)*( cpi(ic,ifoil) -
     &              cp_tar(ic,ifoil) )*gami*2.0/(fsmach*fsmach) )

               ic = ic+1
            end do
         end if
      else
         
         write (*,*) 'dides: not set for side 4'
         stop

      end if

      return
      end                       ! dides
