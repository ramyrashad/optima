c     CUSP Dissipation
c     -- calculation of limited average --
c     Written by: Marian Nemec
c     Date: April 1998
c     Mods: March 2000

      subroutine dclay (jdim,kdim,q,xyj,x,y,tmp,wq,p,swr,swr2)

#include "../include/arcom.inc"

      integer j,k,n

      dimension q(jdim,kdim,4),xyj(jdim,kdim),x(jdim,kdim)
      dimension y(jdim,kdim),tmp(jdim,kdim,4),swr2(jdim,kdim,4)
      dimension wq(2,jdim,kdim,4),p(jdim,kdim),swr(jdim,kdim)

      double precision sw,qjp1,qj

      if ( limiter.gt.3 ) then
c     -- using full limiting --
        do n = 1,4
          call dclimy (jdim,kdim,q,xyj,x,y,tmp,p,(n),swr)
          do k = kbegin,kup
            do j = jlow,jup
              swr2(j,k,n) = swr(j,k)
            end do
          end do
        end do

        do n = 1,4
          do k = klow,kup-1
            do j = jlow,jup
              qkp1 = q(j,k+1,n)*xyj(j,k+1)
              qk = q(j,k,n)*xyj(j,k)
              sw = swr2(j,k,n)
              sw = 2.5d-1*sw*( tmp(j,k+1,n) + tmp(j,k-1,n) )
              wq(1,j,k,n) = qkp1 - sw
              wq(2,j,k,n) = qk + sw
            end do
          end do
        end do

c     -- boundary nodes --

        k = kbegin
        do n = 1,4
          do j = jlow,jup
            qkp1 = q(j,kbegin+1,n)*xyj(j,kbegin+1)
            qk = q(j,kbegin,n)*xyj(j,kbegin)
            sw = swr2(j,k,n)
            sw = 2.5d-1*sw*( tmp(j,k+1,n) + tmp(j,k,n) )
            wq(1,j,k,n) = qkp1 - sw
            wq(2,j,k,n) = qk + sw
          end do
        end do 

        k = kup
        do n = 1,4
          do j = jlow,jup
            qkp1 = q(j,kend,n)*xyj(j,kend)
            qk = q(j,kup,n)*xyj(j,kup)
            sw = swr2(j,k,n)
            sw = 2.5d-1*sw*( tmp(j,k,n) + tmp(j,k-1,n) ) 
            wq(1,j,k,n) = qkp1 - sw
            wq(2,j,k,n) = qk + sw
          end do
        end do
      else
c     -- Nemec-Zingg limiter or O(1) or O(3) dissipation --
        n = 1
        call dclimy (jdim,kdim,q,xyj,x,y,tmp,p,(n),swr)

        do n = 1,4
          do k = klow,kup-1
            do j = jlow,jup
              qkp1 = q(j,k+1,n)*xyj(j,k+1)
              qk = q(j,k,n)*xyj(j,k)
              sw = swr(j,k)
              sw = 2.5d-1*sw*( tmp(j,k+1,n) + tmp(j,k-1,n) )
              wq(1,j,k,n) = qkp1 - sw
              wq(2,j,k,n) = qk + sw
            end do
          end do
        end do

c     -- boundary nodes --

        k = kbegin
        do n = 1,4
          do j = jlow,jup
            qkp1 = q(j,kbegin+1,n)*xyj(j,kbegin+1)
            qk = q(j,kbegin,n)*xyj(j,kbegin)
            sw = swr(j,k)
            sw = 2.5d-1*sw*( tmp(j,k+1,n) + tmp(j,k,n) )
            wq(1,j,k,n) = qkp1 - sw
            wq(2,j,k,n) = qk + sw
          end do
        end do

        k = kup
        do n = 1,4
          do j = jlow,jup
            qkp1 = q(j,kend,n)*xyj(j,kend)
            qk = q(j,kup,n)*xyj(j,kup)
            sw = swr(j,k)
            sw = 2.5d-1*sw*( tmp(j,k,n) + tmp(j,k-1,n) )
            wq(1,j,k,n) = qkp1 - sw
            wq(2,j,k,n) = qk + sw
          end do
        end do

      end if

      return
      end                       !dclay
