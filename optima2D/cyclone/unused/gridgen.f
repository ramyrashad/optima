      parameter(jmax=331,kmax=51)
      dimension x(jmax,kmax),y(jmax,kmax)
c
      dx=10./float(jmax-1)
      dy=10./float(kmax-1)
      do 10 k=1,kmax
      do 10 j=1,jmax
         x(j,k)=-5.+dx*float(j-1)
         y(j,k)=-5.+dy*float(k-1)
 10   continue
      open(unit=8,file='first.g')
c
      write(8) jmax,kmax
      write(8)  ( ( x(j,k),j=1,jmax) ,k=1,kmax) ,                      
     &             ( ( y(j,k),j=1,jmax) ,k=1,kmax)
c
      close(unit=8)
      stop
      end
