      subroutine xi_adv(jdim,kdim,uu,bx,cx,dx,fy)
c     calling subroutine: spalart
c
#include "../include/arcom.inc"
c
      dimension uu(jdim,kdim),fy(jdim,kdim)
      dimension bx(kdim,jdim),cx(kdim,jdim),dx(kdim,jdim)
c
c     *********************
c     Advective terms in xi
c     *********************
c     
c     -first-order upwinding/downwinding
      do k=klow,kup
      do j=jlow,jup
         sgnu = sign(1.,uu(j,k))
         app  = .5*(1.+sgnu)
         apm  = .5*(1.-sgnu)
         fy(j,k)= fy(j,k) - 
     &        uu(j,k)*(app*(turre(j,k)-turre(j-1,k))
     &        +apm*(turre(j+1,k)-turre(j,k)) )
         bx(k,j)   = bx(k,j)   - uu(j,k)*app
         cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
         dx(k,j)   = dx(k,j)   + uu(j,k)*apm
      enddo
      enddo
c
      return
      end
