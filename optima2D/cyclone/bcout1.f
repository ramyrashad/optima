      subroutine bcout1(jdim,kdim,q,jbc,xy,xit,ett,xyj,x,y)             
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jbc,kdim,4)                                           
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c                                                                       
c---------------------------------------------------------------------  
c---------------------------------------------------------------------  
c---         boundary conditions for airfoil topology              ---  
c---                  c  or  o mesh type                           ---  
c---------------------------------------------------------------------  
c---------------------------------------------------------------------  
c                                                                       
c  bc for c mesh j = 1, j = jmax outflow boundaries                     
c                                                                       
c********************************************************************** 
c********************************************************************** 
c                                                                       
c.....................................................................  
c  far field circulation based on potential vortex added to reduce      
c  the dependency on outer boundary location.  works well.  for         
c  instance, a naca0012 at m=0.63 a=2. shows no dependency on ob from   
c  4 - 96 chords.                                                       
c.....................................................................  
c                                                                       
          gi = 1./gamma                                                 
          gm1i = 1./gami                                                
c                                                                       
         alphar = alpha*pi/180.                                         
         cosang = cos( alphar )                                         
         sinang = sin( alphar )                                         
         ainf = sqrt(gamma*pinf/rhoinf)                                 
         hstfs = 1./gami + 0.5*fsmach**2                                
c                                                                       
c.....................................................................  
c---------------------------------------------------------------------  
c               outflow for 'c' mesh                                    
c---------------------------------------------------------------------  
c                                                                       
c  for viscous flow do not use characteristic conditions                
c                                                                       
c                                                                       
      if(.not.periodic .and. .not.viscous)then                          
      j = 1                                                             
      jq = 1                                                            
c                                                                       
      sign = -1.                                                        
      jadd = 1                                                          
c                                                                       
c.....................................................................  
c               check for supersonic free stream                        
c.....................................................................  
c                                                                       
      if(fsmach.gt.1.)go to 75                                          
c                                                                       
72    continue                                                          
c                                                                       
      do 74 k = kbegin,kend                                             
c                                                                       
c   reset free stream values with circulation correction                
c                                                                       
        xa = x(j,k) - chord/4.                                          
        ya = y(j,k)                                                     
        radius = sqrt(xa**2+ya**2)                                      
        angl = atan2(ya,xa)                                             
        cjam = cos(angl)                                                
        sjam = sin(angl)                                                
        qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))     
        uf = uinf + qcirc*sjam                                          
        vf = vinf - qcirc*cjam                                          
        af2 = gami*(hstfs - 0.5*(uf**2+vf**2))                          
        af = sqrt(af2)                                                  
c                                                                       
c               choose a reference frame in terms of normal and         
c               tangential components                                   
c                                                                       
c               metric terms                                            
c                                                                       
      snorm = 1./sqrt(xy(j,k,1)**2+xy(j,k,2)**2)                        
      xy1h = xy(j,k,1)*snorm                                            
      xy2h = xy(j,k,2)*snorm                                            
c                                                                       
      xyjm1 = xyj(j+jadd,k)                                             
      rhoext = q(jq+jadd,k,1)*xyjm1                                     
      rjm1 = 1./q(jq+jadd,k,1)                                          
      uext = q(jq+jadd,k,2)*rjm1                                        
      vext = q(jq+jadd,k,3)*rjm1                                        
      eext = q(jq+jadd,k,4)*xyjm1                                       
      pext = gami*(eext - 0.5*rhoext*(uext**2+vext**2))                 
c                                                                       
      rfix = xy1h*uf + xy2h*vf - sign * 2.*af*gm1i                      
      rext = xy1h*uext + xy2h*vext +                                    
     1     sign * 2.*sqrt(gamma*pext/rhoext)*gm1i                       
c                                                                       
c                                                                       
c              compute flow variables based on above calc               
c                                                                       
      qn = (rfix + rext)*0.5                                            
      cspe = sign * (rext - rfix)*gami*0.25                             
      c2 = cspe**2                                                      
c                                                                       
           if(sign*qn .le. 0.0)then                                     
           qt = (-xy2h*uf + xy1h*vf)                                    
           entro = gamma                                                
           else                                                         
           qt = (-xy2h*uext + xy1h*vext)                                
           entro = rhoext**gamma/pext                                   
           endif                                                        
c                                                                       
c             compute flow variables                                    
c                                                                       
      u = xy1h*qn - xy2h*qt                                             
      v = xy2h*qn + xy1h*qt                                             
      q(jq,k,1) = (c2*entro*gi)**gm1i                                   
      pres = c2*q(jq,k,1)*gi                                            
c                                                                       
c              add jacobian                                             
c                                                                       
          rj = 1./xyj(j,k)                                              
          q(jq,k,1) = q(jq,k,1)*rj                                      
          q(jq,k,2) = q(jq,k,1)*u                                       
          q(jq,k,3) = q(jq,k,1)*v                                       
          q(jq,k,4) = pres*gm1i*rj + 0.5*q(jq,k,1)*(u**2+v**2)          
74    continue                                                          
79    continue                                                          
c                                                                       
      go to 89                                                          
c                                                                       
c.....................................................................  
c                supersonic logic                                       
c.....................................................................  
c                                                                       
75     continue                                                         
      do 84 k = kbegin,kend                                             
c                                                                       
c                choose a reference frame in terms of normal and        
c                tangential components                                  
c                                                                       
      rhoinv = 1./q(jq,k,1)                                             
      u = q(jq,k,2)*rhoinv                                              
      v = q(jq,k,3)*rhoinv                                              
c                                                                       
c                metric terms                                           
c                                                                       
      snorm = 1./sqrt(xy(j,k,1)**2+xy(j,k,2)**2)                        
      xy1h = xy(j,k,1)*snorm                                            
      xy2h = xy(j,k,2)*snorm                                            
      qn = sign*(xy1h*u + xy2h*v)                                       
c                                                                       
          if(qn.ge.0.)then                                              
          rmet = xyj(j+jadd,k)/xyj(j,k)                                 
          do 86 n = 1,4                                                 
86        q(jq,k,n) = q(jq+jadd,k,n)*rmet                               
          endif                                                         
c                                                                       
84    continue                                                          
89    continue                                                          
c                                                                       
c                                                                       
      elseif(.not.periodic .and. viscous)then                           
c                                                                       
c   extrapolation on viscous outflow with pressure extrapolated         
c                                                                       
          do 250 k = kbegin,kend                                        
             j  = jbegin                                                
             jq = jbegin                                                
             rrj = xyj(j+1,k)/xyj(j,k)                                  
             q(jq,k,1) = q(jq+1,k,1)*rrj                                
             q(jq,k,2) = q(jq+1,k,2)*rrj                                
             q(jq,k,3) = q(jq+1,k,3)*rrj                                
             ppp = gami*(q(jq+1,k,4) -                                  
     *              0.5*(q(jq+1,k,2)**2+q(jq+1,k,3)**2)/q(jq+1,k,1) )   
             q(jq,k,4) = ppp/gami*rrj +                                 
     *                 0.5*(q(jq,k,2)**2+q(jq,k,3)**2)/q(jq,k,1)        
250       continue                                                      
c                                                                       
       endif                                                            
c                                                                       
       return                                                           
       end                                                              
