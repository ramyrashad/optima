C***********************************************************************
C************* EVALUATION OF RIGHT HAND SIDE ( EXPLICIT PART ) *********
C***********************************************************************
      subroutine rhsy(jdim,kdim,q,s,press,xy,xyj,ett,flux,
     &      jbegin,jend,kbegin,kend,klow,kup)

      implicit none
c                                                                       
#include "../include/common.inc"
c     
      integer jdim,kdim,jbegin,jend,kbegin,kend,klow,kup,j,k,n,km1,kp1
      double precision q(jdim,kdim,4),press(jdim,kdim),s(jdim,kdim,4)          
      double precision xy(jdim,kdim,4),xyj(jdim,kdim),ett(jdim,kdim),r03 
      double precision flux(jdim,kdim,4),r1,r2,r0,rr,u,v,qs,pressj,r02 
                                                       
c     central differencing used in eta direction                       
      do 20 k=kbegin,kend                                               
      do 20 j=jbegin,jend                                                  
        r1 = xy(j,k,3)                                                 
        r2 = xy(j,k,4)                                                 
        r0 = ett(j,k)                                                  
c       rr is j/rho                                                     
        rr = 1./q(j,k,1)                                               
        u = q(j,k,2)*rr                                                
        v = q(j,k,3)*rr                                                
c                                                                       
c       r1, r2 are d eta/dx and  d eta/dy     
c       qs is cap-v ( contravariant velocities )        
        qs = r0 +r1*u + r2*v                                           
c                                                                       
c       pressj is pressure / j                                          
        pressj= press(j,k)                                             
c                                                                       
        flux(j,k,1)= q(j,k,1)*qs                                       
        flux(j,k,2)= q(j,k,2)*qs + r1*pressj                           
        flux(j,k,3)= q(j,k,3)*qs + r2*pressj                           
        flux(j,k,4) = qs*( q(j,k,4) + pressj) - pressj*r0              
 20   continue                                                          
c                                                                       
      if (iord.eq.4) then
        r0 = -dt/(12.d0*(1.d0+phidt))
        r02= 2.d0*r0
        r03 = -.5*dt / (1. + phidt)
        do 30 n =1,4
        do 30 k =klow+1,kup-1                                      
        do 30 j =jbegin,jend                                    
          s(j,k,n) = s(j,k,n) + r0*(flux(j,k-2,n)
     &          +8.d0*(-flux(j,k-1,n)+flux(j,k+1,n))-flux(j,k+2,n))
 30     continue
c
        do 31 n=1,4
        do 31 j=jbegin,jend
          k=klow
c         -second order
c          s(j,k,n) = s(j,k,n) + r03*(flux(j,k+1,n) - flux(j,k-1,n))  
c         -third order
          s(j,k,n) =s(j,k,n) + r02*(-2.d0*flux(j,k-1,n)
     &          -3.d0*flux(j,k,n)+6.d0*flux(j,k+1,n)-flux(j,k+2,n))
c
          k=kup
c         -second order         
c          s(j,k,n) = s(j,k,n) + r03*(flux(j,k+1,n) - flux(j,k-1,n)) 
c         -third order
          s(j,k,n) =s(j,k,n) + r02*(flux(j,k-2,n)
     &          -6.d0*flux(j,k-1,n)+3.d0*flux(j,k,n)+2.d0*flux(j,k+1,n))
 31     continue
      elseif (iord.eq.2) then
        r0 = -0.5d0*dt / (1.d0 + phidt)
c       central difference f                                         
        do 130 n=1,4
        do 130 k=klow,kup
          kp1=k+1
          km1=k-1
          do 129 j=jbegin,jend
            s(j,k,n) = r0*( flux(j,kp1,n) - flux(j,km1,n)) + s(j,k,n)
 129      continue
 130    continue
      endif
c                                                                       
      return                                                            
      end                                                               
