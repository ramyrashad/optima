C **************************************************************
C **  Routine to copy q and turre to halo pts   **************** 
C **  Markus Rumpfkeil, June 2006               ****************  
C ************************************************************** 

      SUBROUTINE halo_q(iblk1,iblk2,jdim,kdim,q,turre)

      implicit none
 
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer iblk1,iblk2,jdim,kdim,ii,nbor,nside1,nside2
      double precision q(jdim*kdim*4), turre(jdim*kdim)                  
     
      DO ii=iblk1,iblk2
         DO nside1=2,4,2
            nbor = lblkpt(ii,nside1)
            nside2 = lsidept(ii,nside1)
            IF(nbor.NE.0) THEN
               CALL halocp(ii,nhalo,4,nside1,nside2,
     &              jmax(ii),kmax(ii),q(lqptr(ii)),
     &              jbegin2(ii),jend2(ii),
     &              kminbnd(ii),kmaxbnd(ii),
     &              jmax(nbor),kmax(nbor),q(lqptr(nbor)),
     &              jbegin2(nbor),jend2(nbor),
     &              kminbnd(nbor),kmaxbnd(nbor))
c     -- halo column copy for turre vector --
               IF (turbulnt .AND. iturb.EQ.3) THEN
                  CALL halocp(ii,nhalo,1,nside1,nside2,
     &                 jmax(ii),kmax(ii),turre(lgptr(ii)),
     &                 jbegin2(ii),jend2(ii),
     &                 kminbnd(ii),kmaxbnd(ii),
     &                 jmax(nbor),kmax(nbor),turre(lgptr(nbor)),
     &                 jbegin2(nbor),jend2(nbor),
     &                 kminbnd(nbor),kmaxbnd(nbor))
               ENDIF
            ENDIF
         ENDDO
      ENDDO
c
c     -now that the halo columns have been copied, do the same for
c      the halo rows (same as in master_integrat.inc)
      DO ii=iblk1,iblk2
         DO nside1=1,3,2
            nbor = lblkpt(ii,nside1)
            nside2 = lsidept(ii,nside1)
            IF (nbor.NE.0 .AND. nhalo.GT.1) THEN
               CALL halocp(ii,nhalo,4,nside1,nside2,
     &              jmax(ii),kmax(ii),q(lqptr(ii)),
     &              jbegin2(ii),jend2(ii),
     &              kbegin2(ii),kend2(ii),
     &              jmax(nbor),kmax(nbor),q(lqptr(nbor)),
     &              jbegin2(nbor),jend2(nbor),
     &              kbegin2(nbor),kend2(nbor))
c     -- halo column copy for turre vector --
               IF (turbulnt .AND. iturb.EQ.3) THEN
                  call halocp(ii,nhalo,1,nside1,nside2,
     &                 jmax(ii),kmax(ii),turre(lgptr(ii)),
     &                 jbegin2(ii),jend2(ii),
     &                 kbegin2(ii),kend2(ii),
     &                 jmax(nbor),kmax(nbor),turre(lgptr(nbor)),
     &                 jbegin2(nbor),jend2(nbor),
     &                 kbegin2(nbor),kend2(nbor))
               ENDIF
            ENDIF
         ENDDO
      ENDDO

      RETURN
      END
