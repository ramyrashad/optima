C ********************************************************************
C ********************* METRIC CALCULATIONS **************************
C ********************************************************************
c calling routine: xymets
c
c     This subroutine calculates the jacobian based on the cell area.
c     It was implemented because the conventional jacobian calculation
c     yielded negative jacobians in the 3rd or 4th coarse grid when
c     grid sequencing or multigriding

      subroutine calcmet2(iblk,jdim,kdim,x,y,xy,xyj,
     &                    jbegin,jend,kbegin,kend)

      implicit none
c                                                                       
#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/units.inc"
c 
      integer iblk,jdim,kdim,jbegin,jend,kbegin,kend
      integer j,k,jlow,jup,klow,kup,kp1,jp1,jm1,km1
      double precision xy(jdim,kdim,4),xyj(jdim,kdim)
      double precision tmp,vectorAx,vectorAy,vectorBx,vectorBy,product 
      double precision x(jdim,kdim),y(jdim,kdim),dotp
      double precision area(maxjb,maxkb)
c
#ifndef YMP
      double precision magA,magB
#else
      real   magA,magB
#endif
c
c     -- compute area of each cell in a block --
c
      jlow=jbegin+1
      jup=jend-1
      klow=kbegin+1
      kup=kend-1
c
      do k=kbegin,kup
        kp1=k+1
        do j=jbegin,jup
          jp1=j+1
          vectorAx = x(jp1,kp1)-x(j,k)
          vectorAy = y(jp1,kp1)-y(j,k)
          vectorBx = x(j,kp1)-x(jp1,k)
          vectorBy = y(j,kp1)-y(jp1,k)

          magA = sqrt(vectorAx**2+ vectorAy**2)
          magB = sqrt(vectorBx**2+ vectorBy**2)

          product = magA * magB 
          
          dotp = vectorAx*vectorBx + vectorAy*vectorBy
          
          area(j,k) = 0.5d0*product* sin(cos(dotp/product))
c          area(j,k) = 0.5d0*product* dsin(dacos(dotp/product))
          if (area(j,k).le.0.) then
            write(n_out,*) 'Block',iblk
            write(n_out,*) 'Cell Area(',j,k,')=',area(j,k)
            stop
          endif
        enddo
      enddo
c
c     ---- Jacobians according to cell area ----
c
c
c     -- interior nodes --

      do k=klow,kup
        km1=k-1
        do j=jlow,jup
          jm1=j-1
c         -take average of four cell areas around node (j,k)
          tmp = 0.25d0*(area(jm1,km1) + area(jm1,k) 
     $                    + area(j,k) + area(j,km1))
c         -form jacobian (i.e. inverse of average cell area.)
          xyj(j,k)=1.d0/tmp
        enddo
      enddo
c
c     --- block boundaries ---

c     -- side 1 --
      k = kbegin

      do j=jlow,jup
        xyj(j,k) = 1.d0/(0.5d0*(area(j,k) + area(j-1,k)))
      enddo
      
      j = jbegin
      xyj(j,k) = 1.d0/area(j,k)
      j = jend
      xyj(j,k) = 1.d0/area(j-1,k)
c
c      
c     -- side 2 --
      j = jbegin
      do k=klow,kup
        xyj(j,k) = 1.d0/(0.5d0*(area(j,k) + area(j,k-1)))
      enddo
c
c
c     -- side 3 --
      k = kend
      km1=k-1
      do j=jlow,jup
        xyj(j,k) = 1.d0/(0.5d0*(area(j-1,km1) + area(j,km1)))
      enddo
c     
      j = jbegin
      xyj(j,k) = 1.d0/area(j,km1)
      j = jend
      xyj(j,k) = 1.d0/area(j-1,km1)
c
c
c     -- side 4 --
      j = jend
      jm1=j-1
      do k=klow,kup
        xyj(j,k) = 1.d0/(0.5d0*(area(jm1,k) + area(jm1,k-1)))
      enddo
c
c---- end of Jacobian calculation according to cell area ----
      
c     -- calculate metrics --
      do k=kbegin,kend
      do j=jbegin,jend
c     - xix = xy1, xiy = xy2, etax = xy3, etay = xy4
        xy(j,k,1)=xy(j,k,1)*xyj(j,k)
        xy(j,k,2)=-xy(j,k,2)*xyj(j,k)
        xy(j,k,3)=-xy(j,k,3)*xyj(j,k)
        xy(j,k,4)=xy(j,k,4)*xyj(j,k)
      enddo
      enddo
c
      return                                                            
      end                                                               
