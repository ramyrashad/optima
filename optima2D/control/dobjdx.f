c----------------------------------------------------------------------
c     -- gradient calculation: sensitivity of the objective 
c     function wrt the design variables --
c     
c     written by: marian nemec
c     date: sept. 2000
c----------------------------------------------------------------------

      subroutine dObjdX (in, dOdX, jdim, kdim, dvs, idv, x, y, bap, bcp,
     &      bt, bknot, q, press, xy, xyj, xys, xyjs, xs, ys,
     &      xOrig, yOrig, dx, dy, given_stepsize, obj0)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
      
      !-- Declare all variables

      integer 
     &     i, j, k, n, jdim, kdim, ndim, idv(nc+mpopt), jMaxMach,
     &     in, kMaxMach, mp

      double precision 
     &     t3,a3,rt3, obj0, q(jdim,kdim,4), xy(jdim,kdim,4), 
     &     xyj(jdim,kdim), c_upp, c_low,
     &     x(jdim,kdim), y(jdim,kdim), press(jdim,kdim),
     &     xOrig(jdim,kdim), yOrig(jdim,kdim),
     &     dx(jdim*kdim*incr), dy(jdim*kdim*incr),
     &     dvs(nc+mpopt), bap(jbody,2), bcp(nc,2),
     &     bt(jbody), bknot(jbsord+nc),
     &     MaxMachp, MaxMachm, aMaxMach, uMaxMach, vMaxMach,
     &     pMaxMach, ks, ksMachp, ksMachm,
     &     xys(jdim,kdim,4), xyjs(jdim,kdim), xs(jdim,kdim),
     &     ys(jdim,kdim), R,
     &     sndsp(jdim,kdim), precon(jdim,kdim,6), 
     &     given_stepsize, stepsize, cdtp, cdtm, cltp, cltm, objp, objm,
     &     dOdX, tmp, rc_con, cmtp, cmtm, maxmach


#ifdef _MPI_VERSION
      mp = rank+1
#else
      mp = 1
#endif

      !-- Initialize dOdX     
      dOdX = 0.d0

      if ( obj_func.eq.1 ) then
        
         dOdX = 0.d0

      else if ( obj_func.eq.2 ) then

         if (.not.cdf) then
c     -- use forward difference formula --
            tmp = dvs(in)
            stepsize = fd_eta*dvs(in)

c           Fix the stepsize to be what was given (Chad Oldfield)
            if (given_stepsize > 0.d0) stepsize = given_stepsize

            dvs(in) = dvs(in) + stepsize
            stepsize = dvs(in) - tmp

c     -- regrid domain --
            call regrid ( in, jdim, kdim, dvs, idv, x, y, bap, bcp,
     &           bt, bknot, xOrig, yOrig, dx, dy, .false.)

c     -- calculate dOdX at (+) --
c     -- note that metrics have not been recalculated!! --
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli, cdi,
     &           cmi, clv, cdv, cmv) 
            cdtp = cdi + cdv
c     -- evaluate sensitivity of the objective function --
            dOdX = ( cdtp - cdt )/stepsize/obj0
            dvs(in) = tmp

         else
c     -- use cenral-differences --
c     -- second order accurate --
            tmp = dvs(in)
            stepsize = fd_eta*dvs(in)

c           Fix the stepsize to be what was given (Chad Oldfield)
            if (given_stepsize > 0.d0) stepsize = given_stepsize

            dvs(in) = dvs(in) + stepsize
            stepsize = dvs(in) - tmp

            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- calculate dOdX at (+) --
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &           cdi, cmi, clv, cdv, cmv)  
            cdtp = cdi + cdv
            dvs(in) = tmp - stepsize

            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- calculate dOdX at (-) --
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &           cdi, cmi, clv, cdv, cmv) 
            cdtm = cdi + cdv
            dOdX = ( cdtp - cdtm )/ (2.d0*stepsize) /obj0
            dvs(in) = tmp        
         end if

c     -----------------------------------------------------------------
      else if ( obj_func.eq.3 ) then
c     -- Obj = wfd*( cd - cd_tar )**2 + wfl*( cl - cl_tar )**2
c     -- use second order accurate central-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)

c        Fix the stepsize to be what was given (Chad Oldfield)
         if (given_stepsize > 0.d0) stepsize = given_stepsize

         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &            bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         end if           

         if ( in.eq.ndv) alpha = dvs(in)

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv

         if ( in.eq.ndv) alpha = tmp

         objp = wfd*( cdtp-cd_tar )**2 + wfl*( cltp-cl_tar )**2

         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &            bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         end if

         if ( in.eq.ndv) alpha = dvs(in)

c     -- calculate dOdX at (-) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &          cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv

         if ( in.eq.ndv) alpha = tmp

         objm = wfd*( cdtp-cd_tar )**2 + wfl*( cltp-cl_tar )**2
          
         dOdX = ( objp - objm )/ (2.d0*stepsize) /obj0
         dvs(in) = tmp        

c     -----------------------------------------------------------------
      else if ( obj_func.eq.4 ) then
c     -- Obj = max( Cl )
c     -- use second order accurate cenral-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)

c        Fix the stepsize to be what was given (Chad Oldfield)
         if (given_stepsize > 0.d0) stepsize = given_stepsize

         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if           

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cltp = cli + clv

         if (in.gt.ngdv) alpha = tmp

         objp = -cltp 

         dvs(in) = tmp - stepsize
        
         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
        else
           alpha = dvs(in)
        end if

c     -- calculate dOdX at (-) --
        call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &       cdi, cmi, clv, cdv, cmv) 
        cltp = cli + clv
        
        if (in.gt.ngdv) alpha = tmp

        objm = -cltp
        
        dOdX = ( objp - objm )/ (2.d0*stepsize)/obj0
        dvs(in) = tmp        

      else if ( obj_func.eq.5 ) then
c     -----------------------------------------------------------------
c     -- Obj = max (Cl/Cd) --
c     -- use second order accurate cenral-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)

c        Fix the stepsize to be what was given (Chad Oldfield)
         if (given_stepsize > 0.d0) stepsize = given_stepsize

         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if           

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv

         if (in.gt.ngdv) alpha = tmp

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3, .false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

         objp = (cdtp/cltp)/obj0 + t3 + rt3 + a3 + rc_con

         dvs(in) = tmp - stepsize
         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c     -- calculate dOdX at (-) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv
         
         if (in.gt.ngdv) alpha = tmp

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3, .false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

         objm = (cdtp/cltp)/obj0 + t3 + rt3 + a3 + rc_con
        
         dOdX = ( objp - objm )/ (2.d0*stepsize)
         dvs(in) = tmp 
c     -----------------------------------------------------------------
      else if ( obj_func.eq.6 ) then
c     -- Obj = wfd*( cd - cd_tar )**2 + wfl*( cl - cl_tar )**2
c     -- use second order accurate cenral-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)

c        Fix the stepsize to be what was given (Chad Oldfield)
         if (given_stepsize > 0.d0) stepsize = given_stepsize

         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c1112         if ( in.eq.ndv .and. dvalfa(in) ) alpha = dvs(in)

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv

c1112         if ( in.eq.ndv .and. dvalfa(in) ) alpha = tmp

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3, .false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

         if (cdtp.gt.cd_tar) then
            objp = wfd*( 1.d0 - cdtp/cd_tar )**2
         else
            objp = 0.0 
         end if

         if (use_quad_penalty_meth) then
            objp = (objp + wfl*( 1.d0 - cltp/cl_tar )**2)/obj0
     |      + t3 + rt3 + a3 + rc_con
         else
            objp = (objp + wfl*( 1.d0 - cltp/cl_tar )**2)/obj0
         endif

         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c1112          if ( in.eq.ndv .and. dvalfa(in) ) alpha = dvs(in)

c     -- calculate dOdX at (-) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtm = cdi + cdv
         cltm = cli + clv
         
c1112          if ( in.eq.ndv .and. dvalfa(in) ) alpha = tmp

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3, .false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

         if (cdtm.gt.cd_tar) then
            objm = wfd*( 1.d0 - cdtm/cd_tar )**2
         else
            objm = 0.0
         end if

         if (use_quad_penalty_meth) then
            objm = (objm + wfl*( 1.d0 - cltm/cl_tar )**2)/obj0
     |      + t3 + rt3 + a3 + rc_con
         else
            objm = (objm + wfl*( 1.d0 - cltm/cl_tar )**2)/obj0
         endif

         dOdX = ( objp - objm )/ (2.d0*stepsize)
         dvs(in) = tmp  
         if ( in.gt.ngdv ) alpha = tmp
c     if (mpopt.eq.1) write (opt_unit,*) in,dOdX

c     -----------------------------------------------------------------
      else if ( obj_func.eq.7 ) then
c     -- Obj = wfd*( cm - cd_tar )**2 + wfl*( cl - cl_tar )**2
c     -- use second order accurate cenral-differences --

         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)

c        Fix the stepsize to be what was given (Chad Oldfield)
         if (given_stepsize > 0.d0) stepsize = given_stepsize

         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp
         
         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if           

c1112         if ( in.eq.ndv .and. dvalfa(in) ) alpha = dvs(in)

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cmtp = cmi + cmv
         cltp = cli + clv

c1112         if ( in.eq.ndv .and. dvalfa(in) ) alpha = tmp

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3, .false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

         objp = wfd*( 1.d0 - cmtp/cd_tar )**2
         objp = (objp + wfl*( 1.d0 - cltp/cl_tar )**2)/obj0
     |      + t3 + rt3 + a3 + rc_con
         
         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c1112          if ( in.eq.ndv .and. dvalfa(in) ) alpha = dvs(in)

c     -- calculate dOdX at (-) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cmtp = cmi + cmv
         cltp = cli + clv

c1112          if ( in.eq.ndv .and. dvalfa(in) ) alpha = tmp
         
c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3, .false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim, x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

         objm = wfd*( 1.d0 - cmtp/cd_tar )**2
         objm = (objm + wfl*( 1.d0 - cltp/cl_tar )**2)/obj0
     |      + t3 + rt3 + a3 + rc_con
         
         dOdX = ( objp - objm )/ (2.d0*stepsize)
         dvs(in) = tmp  
         if ( in.gt.ngdv ) alpha = tmp

c     -----------------------------------------------------------------
      else if ( obj_func.eq.8.or.lcdm_grad.eq.1 ) then
c     -- Obj = cd
c     -- use second order accurate central-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)
         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3, .false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

         if (use_quad_penalty_meth) then

            objp = cdtp/obj0 + t3 + rt3 + a3 + rc_con

         else

            objp = cdtp/obj0

         end if
         
         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy, .false.)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c     -- calculate dOdX at (-) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtm = cdi + cdv
         cltm = cli + clv

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3, .false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

         if (use_quad_penalty_meth) then

            objm = cdtm/obj0 + t3 + rt3 + a3 + rc_con

         else

            objm = cdtm/obj0

         end if

         dOdX = ( objp - objm )/(2.d0*stepsize)
         dvs(in) = tmp  
         if ( in.gt.ngdv ) alpha = tmp

      else if ( obj_func.eq.15.or.lcdm_grad.eq.2 ) then

         !-- finite difference derivative calculation of dJ/dX for 
         !-- off-design constraint function J = Lift

c     -- use second order accurate central-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)
         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv

         if (obj_func.eq.19.and.lcdm_grad.eq.2) then
            objp = cltp/1.d0
         else
            objp = cltp/obj0
         end if
         
         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c     -- calculate dOdX at (-) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtm = cdi + cdv
         cltm = cli + clv

         if (obj_func.eq.19.and.lcdm_grad.eq.2) then
            objm = cltm/1.d0
         else
            objm = cltm/obj0
         end if

         dOdX = ( objp - objm )/(2.d0*stepsize)

         !-- Get bounds on off-design constraint
         if (off_flags(mp)) then
            c_upp = c_upp1(mp)
            c_low = c_low1(mp)
         end if
         !-- Scale by constraint bound
         if (scale_con) dOdX = dOdX/c_low

         dvs(in) = tmp  
         if ( in.gt.ngdv ) alpha = tmp

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3,.false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)


      else if ( obj_func.eq.16 ) then

         !-- finite difference derivative calculation of dJ/dX for 
         !-- off-design constraint fucntion J = Max Mach number

c     -- use second order accurate central-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)

c     -- calculate dOdX at (+) --

         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

         call getMaxMach(q,jdim,kdim,ndim,MaxMachp,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)
 
         objp = MaxMachp/obj0

c     -- calculate dOdX at (-) --
         
         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

         call getMaxMach(q,jdim,kdim,ndim,MaxMachm,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)
 
         objm = MaxMachm/obj0

         !-- Calculate finite differenced gradient

         dOdX = ( objp - objm )/(2.d0*stepsize)
         dvs(in) = tmp  
         if ( in.gt.ngdv ) alpha = tmp

         dOdX = 0.d0

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

      else if ( obj_func.eq.17 ) then

         !-- The KS Max Mach objective function has no direct 
         !-- sensitivity to changes in the shape of the airfoil
         !-- (via design variables X). Therefore,for the sake of
         !-- computational efficiency, skip the finite difference
         !-- calculations here. But, if for some reason, you want
         !-- to do them anyway, just remove the goto statement below:
         
         goto 10

         !-- finite difference derivative calculation of dJ/dX for 
         !-- off-design constraint fucntion J = KS Max Mach number

c     -- use second order accurate central-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)

c     -- calculate dOdX at (+) --

         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if
         call KSmaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach,ks,ksMachp)
 
         objp = ksMachp/obj0

c     -- calculate dOdX at (-) --
         
         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

         call KSmaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach,ks,ksMachm)
 
         objm = ksMachm/obj0

         !-- Calculate finite differenced gradient

         dOdX = ( objp - objm )/(2.d0*stepsize)
         dvs(in) = tmp  
         if ( in.gt.ngdv ) alpha = tmp

         !-- Get bounds on off-design constraint
         if (off_flags(mp)) then
            c_upp = c_upp1(mp)
            c_low = c_low1(mp)
         end if
         !-- Scale by constraint bound
         if (scale_con) dOdX = dOdX/c_upp


 10      dOdX = 0.d0

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)
        
      else if ( obj_func.eq.18 ) then

         !-- finite difference derivative calculation of dJ/dX for 
         !-- objetive function J = 1/Lift

c     -- use second order accurate central-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)
         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv

         objp = (1.d0/cltp)/obj0
         
         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else
            alpha = dvs(in)
         end if

c     -- calculate dOdX at (-) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtm = cdi + cdv
         cltm = cli + clv

         objm = (1.d0/cltm)/obj0

         dOdX = ( objp - objm )/(2.d0*stepsize)
         dvs(in) = tmp  
         if ( in.gt.ngdv ) alpha = tmp

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3,.false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

      else if (obj_func.eq.20.or.obj_func.eq.21) then

 1000    continue
       
         !-- finite difference derivative calculation of dJ/dX for 
         !-- Objective function J = 1/R (Inverse aircraft range factor):
         !-- R = (Mach)*(Cl/Cd)
         !-- J = Cd/(Mach*Cl)

c     -- use second order accurate central-differences --
         tmp = dvs(in)
         stepsize = fd_eta*dvs(in)
c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.d-10 ) stepsize = 1.d-10*sign(1.d0,
     &        stepsize)
         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - tmp

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else if ( in.eq.ngdv+1 ) then ! either AOA or Mach # is a DV
            if (.not.dvmach(mp)) then ! dvs(ngdv+1) is an AOA DV
               alpha = dvs(in)
            else if (dvmach(mp)) then ! dvs(ngdv+1) is a Mach # DV
               fsmach = dvs(in)
            end if
         else if (in.eq.ngdv+2) then ! Both AOA and Mach # are DV's
            fsmach = dvs(in) ! dvs(ngdv+2) is a Mach # DV
         end if

c     -- calculate dOdX at (+) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtp = cdi + cdv
         cltp = cli + clv

         R = fsmach*(cltp/cdtp)

         if (obj_func.eq.20) then
            objp = 1.d0/R
            objp = objp/obj0
         else if (obj_func.eq.21) then
            objp = cdtp/cltp
            objp = objp/obj0
         end if
       
         dvs(in) = tmp - stepsize

         if ( in.le.ngdv ) then
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &           bknot, xOrig, yOrig, dx, dy)
c     -- recalculate metrics --
            call xidif (jdim,kdim,x,y,xy)
            call etadif (jdim,kdim,x,y,xy)
            call calcmet (jdim,kdim,xy,xyj)
         else if ( in.eq.ngdv+1 ) then ! either AOA or Mach # is a DV
            if (.not.dvmach(mp)) then ! dvs(ngdv+1) is an AOA DV
               alpha = dvs(in)
            else if (dvmach(mp)) then ! dvs(ngdv+1) is a Mach # DV
               fsmach = dvs(in)
            end if
         else if (in.eq.ngdv+2) then ! Both AOA and Mach # are DV's
            fsmach = dvs(in) ! dvs(ngdv+2) is a Mach # DV
         end if

c     -- calculate dOdX at (-) --
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 1, cli,
     &        cdi, cmi, clv, cdv, cmv) 
         cdtm = cdi + cdv
         cltm = cli + clv

         R = fsmach*(cltm/cdtm)

         if (obj_func.eq.20) then
            objm = 1.d0/R
            objm = objm/obj0
         else if (obj_func.eq.21) then
            objm = cdtm/cltm
            objm = objm/obj0
         end if

         dOdX = ( objp - objm )/(2.d0*stepsize)

         dvs(in) = tmp  

         if ( in.eq.ngdv+1 ) then
            if (.not.dvmach(mp)) then  
               alpha = tmp
            else if (dvmach(mp)) then 
               fsmach = tmp
            end if

         else if ( (in.eq.ngdv+2).and.(dvmach(mp)) ) then
            fsmach = tmp
         end if

c     -- calculate constraints (thicknes, range thickness, area)
c     and store them in t3, rt3, and a3 respectively --
         call CalcTCon(jdim,kdim,y,t3,.false.)
         call CalcRTCon(jdim,kdim,y,rt3)
         call CalcACon(jdim, kdim,x,y,a3)
         call CalcRCCon(jdim,kdim,x,y,rc_con)

      end if ! obj_func.eq.1

c     -- restore original grid --
      if ( obj_func.ne.1 .and. in.le.ngdv) then
         do n = 1,4
            do k=kbegin,kend
               do j=jbegin,jend
                  xy(j,k,n) = xys(j,k,n)
               end do
            end do
         end do
        
         do k=kbegin,kend
            do j=jbegin,jend
               x(j,k) = xs(j,k)
               y(j,k) = ys(j,k)
               xyj(j,k) = xyjs(j,k)
            end do
         end do

      end if

      return 
      end                       !dobjdx
