c *******************************************************************
c ** subroutine to automatically adjust the `JTLO' and `JTUP'      **
c ** arrays so that transition occurs where the user specifies     **
c ** (in terms of percentage chord top/bottom for each airfoil)    **
c *******************************************************************
c calling routine: ioall
c
c
c
      subroutine autotran(lvl,isequal,jdim,kdim,x,y)

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

      integer lvl,isequal,jdim,kdim,ifoil,ii,nstgstart,nstgend,nn
      integer iblklo,iblkup,iblock,iblkte,i0u,jjmpu,i0l,jj,j0,jhigh
      integer jo,iside,iswitch,iblktran,jtran,nextblk,iloflag,iupflag
      integer jle,iblkle,jjmp,i0,jjmpl
      double precision x(jdim*kdim),y(jdim*kdim)
      integer ielflag(maxfoil),iturbflag(maxfoil)
      double precision xteu,xtel,yteu,ytel,xp,yp,gradc,x1,y1,xtran,xloc
      double precision xq,xle,yle,xte,yte,dist,dist2,gradp
c
c
      ifoil = 0
      do 10 ii=1,maxfoil
        ielflag(ii) = 0
        iturbflag(ii) = 0
   10 continue
c
c main loop
c
      if (mg .or. gseq) then
        nstgstart = nstgtot/isequal*(lvl-1)+1
        nstgend   = nstgtot/isequal*lvl
      else
        nstgstart = 1
        nstgend   = nstg
      endif
c
c      write(n_out,777) nstgstart,nstgend,lvl,nstgtot,nstg
c 777  format('nstg',5i6)
c
      do 900 ii=nstgstart,nstgend
c
c determine the element number
c
        if (istgcnr(ii).eq.1.and.istgtyp(ii).eq.-1)
     &      ifoil = abs(ibcdir(istgblk(ii),1))
        if (istgcnr(ii).eq.4.and.istgtyp(ii).eq.-1)
     &      ifoil = abs(ibcdir(istgblk(ii),3))
c
        if (ifoil.ne.0.and.
     &      translo(ifoil).eq.0.d0.and.transup(ifoil).eq.0.d0) then
          if (iturbflag(ifoil).ne.1) then
            write(n_out,*)
            write(n_out,15) ifoil
   15       format(' no transition specified on foil #',i2)
            write(n_out,*) '  -> fully turbulent!'
            iturbflag(ifoil) = 1
          endif
          goto 900
        endif
c
c find upper and lower blocks that have sides 1 and 3 (respectively)
c along the airfoil surface and terminate with the t.e. point.
c ensure that this is done only once for each element!!
c
        iblklo = 0
        iblkup = 0
        if (istgtyp(ii).eq.-1.and.ielflag(ifoil).eq.0) then
          ielflag(ifoil) = 1
          if (istgcnr(ii).eq.1) then
            iblkup = istgblk(ii)
            iblock = lblkpt(iblkup,4)
            iblock = lblkpt(iblock,1)
            if (ibctype(iblock,2).eq.1) then
              iblkte = iblock
              iblock = lblkpt(iblock,1)
            endif
            iblklo = lblkpt(iblock,2)
          elseif (istgcnr(ii).eq.4) then
            iblklo = istgblk(ii)
            iblock = lblkpt(iblklo,4)
            iblock = lblkpt(iblock,3)
            if (ibctype(iblock,2).eq.1) then
              iblkte = iblock
              iblock = lblkpt(iblock,3)
            endif
            iblkup = lblkpt(iblock,2)
          endif
c
c check to see if this is a c-mesh
c
          if (ifoil.eq.1.and.iblklo.le.0) then
            cmesh = .true.
            iblklo = 0
          elseif (ifoil.eq.1.and.iblkup.le.0) then
            cmesh = .true.
            iblkup = 0
          endif
c
c find trailing edge coordinates (or average for a blunt t.e.).
c
          xteu = 0.d0
          xtel = 0.d0
          yteu = 0.d0
          ytel = 0.d0
          if (iblkup.gt.0) then
            i0u = lgptr(iblkup)+nhalo*(jbmax(iblkup)+2*nhalo)+nhalo-1
            jjmpu = jbmax(iblkup) + 2*nhalo
            xteu = x(i0u+jbmax(iblkup))
            yteu = y(i0u+jbmax(iblkup))
          endif
          if (iblklo.gt.0) then
            i0l = lgptr(iblklo)+nhalo*(jbmax(iblklo)+2*nhalo)+nhalo-1
            jjmpl = jbmax(iblklo) + 2*nhalo
            xtel = x(i0l+jbmax(iblklo)+(kbmax(iblklo)-1)*jjmpl)
            ytel = y(i0l+jbmax(iblklo)+(kbmax(iblklo)-1)*jjmpl)
          endif
          if (iblkup.le.0) then
            xte = xtel
            yte = ytel
          elseif (iblklo.le.0) then
            xte = xteu
            yte = ytel
          elseif (iblkup.gt.0.and.iblklo.gt.0) then
            xte = 0.5*(xteu + xtel)
            yte = 0.5*(yteu + ytel)
          endif
c
c find the leading edge point (i.e., the point furthest from the t.e.).
c
c check upper surface (stagnation point to trailing edge) :
c
          dist2 = 0.d0
          dist = 0.d0
          if (iblkup.gt.0) then
            iblock = iblkup
   50       continue
              i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
              jjmp = jbmax(iblock) + 2*nhalo
              do 60 jj=jbmax(iblock),1,-1
                x1 = x(i0 + jj)
                y1 = y(i0 + jj)
                dist2 = (xte - x1)**2 + (yte - y1)**2
                if (dist2.gt.dist) then
                  iblkle = iblock
                  jle = jj
                  xle = x1
                  yle = y1
                  dist = dist2
                  iupflag = 1
                  iloflag = 0
                endif
   60         continue
              nextblk = lblkpt(iblock,2)
            if (ibctype(nextblk,1).eq.1) then
              iblock = nextblk
              goto 50
            endif
          endif
c
c          write(n_out,*) 'iupflag,iloflag',iupflag,iloflag
c check lower surface (stagnation point to trailing edge) :
c
          if (iblklo.gt.0) then
            iblock = iblklo
   70       continue
              i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
              jjmp = jbmax(iblock) + 2*nhalo
              do 80 jj=jbmax(iblock),1,-1
                x1 = x(i0 + jj + (kbmax(iblock)-1)*jjmp)
                y1 = y(i0 + jj + (kbmax(iblock)-1)*jjmp)
                dist2 = (xte - x1)**2 + (yte - y1)**2
                if (dist2.gt.dist) then
                  iblkle = iblock
                  jle = jj
                  xle = x1
                  yle = y1
                  dist = dist2
                  iupflag = 0
                  iloflag = 1
                endif
   80         continue
              nextblk = lblkpt(iblock,2)
            if (ibctype(nextblk,3).eq.1) then
              iblock = nextblk
              goto 70
            endif
          endif
c
c          write(n_out,*) 'iupflag,iloflag',iupflag,iloflag
c          call flushit(6)
c
          gradc = (yte - yle)/(xte - xle)
          if (abs(gradc).gt.1e-09) gradp = -1.d0/gradc
c
c set transition point on upper surface :
c
c locate grid point closest to specified upper transition point
c
          dist = 1.d0
          iblock = iblkup
   90     continue
            j0 = 1
            if (iblock.eq.iblkle) j0 = jle
            if (cmesh) j0 = jle
            i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
            jjmp = jbmax(iblock) + 2*nhalo
            do 100 jj=jbmax(iblock),j0,-1
              xp = x(i0 + jj)
              yp = y(i0 + jj)
              if (abs(gradc).gt.1e-09) then
                xq = (yp - yte + gradc*xte - gradp*xp)/(gradc - gradp)
              else
                xq = xp
              endif
              xloc = abs((xle - xq)/(xle - xte))
              dist2 = abs(transup(ifoil)-xloc)
              if (dist2.lt.dist) then
                iblktran = iblock
                jtran = jj
                xtran = xq
                dist = dist2
              endif
  100       continue
            nextblk = lblkpt(iblock,2)
          if (ibctype(nextblk,1).eq.1.and.iblock.ne.iblkle) then
            iblock = nextblk
            goto 90
          endif
c
c if the leading edge is on the other side of the stagnation point
c (i.e., the lower surface), check aft of the stagnation point on
c the lower surface until the l.e. is reached.
c
          if ((iloflag.eq.1).and.(.not.cmesh)) then
            iblock = lblkpt(nextblk,1)
            iblock = lblkpt(iblock,4)
  110       continue
              j0 = jbmax(iblock)
              if (iblock.eq.iblkle) j0 = jle
              i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
              jjmp = jbmax(iblock) + 2*nhalo
              do 120 jj=1,j0
                xp = x(i0 + jj + (kbmax(iblock)-1)*jjmp)
                yp = y(i0 + jj + (kbmax(iblock)-1)*jjmp)
                if (abs(gradc).gt.1e-09) then
                  xq = (yp - yte + gradc*xte - gradp*xp)/(gradc - gradp)
                else
                  xq = xp
                endif
                xloc = (xle - xq)/(xle - xte)
                dist2 = abs(transup(ifoil)-xloc)
                if (dist2.lt.dist) then
                  iblktran = iblock
                  jtran = jj
                  xtran = xq
                  dist = dist2
                endif
  120         continue
            if (iblock.ne.iblkle) then
              iblock = lblkpt(iblock,4)
              goto 110
            endif
          endif
c
c move point by 2 grid points to allow for turbulence model ramp-in
c
          if (iloflag.eq.1.and.iblktran.eq.iblkle) then
            jtran = jtran + 2
            if (jtran.gt.jle) jtran = jle
          else
            if (jtran.ge.3) then
              jtran = jtran - 2
              if (iblktran.eq.iblkle.and.jtran.lt.jle) jtran = jle
            else
              nextblk = lblkpt(iblktran,2)
              if (ibctype(nextblk,1).eq.1) then
                iblktran = nextblk
                jtran = jbmax(nextblk) - 2 + jtran
                if (iblktran.eq.iblkle.and.jtran.lt.jle) jtran = jle
              else
                nextblk = lblkpt(nextblk,1)
                nextblk = lblkpt(nextblk,4)
                iblktran = nextblk
                jtran = 3 - jtran
                if (iblktran.eq.iblkle.and.jtran.gt.jle) jtran = jle
              endif
            endif
          endif
c
c set j point for transition in block "iblktran"
c
          iswitch = 0
          jtran = jtran + nhalo
          if (iloflag.eq.1.and.iblktran.eq.iblkle) then
            jtranlo(3,iblktran) = jtran
            iswitch = 1
          else
            jtranup(1,iblktran) = jtran
            jtranlo(1,iblktran) = 0
          endif
c
c set j limits in all blocks upstream of transition point
c
          nextblk = lblkpt(iblktran,2)
          if (ibctype(nextblk,1).eq.1.and.iswitch.eq.0) then
  140       continue
              jtranup(1,nextblk) = 2*nhalo + jbmax(nextblk)
              jtranlo(1,nextblk) = 0
              nextblk = lblkpt(nextblk,2)
            if (ibctype(nextblk,1).eq.1) goto 140
          endif
c
          write(n_out,*)
          write(n_out,145)ifoil
  145     format(' transition specified on foil #',i2)
          write(n_out,146)transup(ifoil)*100.d0
  146     format('   at ',f6.2,'% chord on the upper surface.')
c
c set transition point on lower surface :
c
c locate grid point closest to specified lower transition point
c
c          write(n_out,*) 'iblktran_up',iblktran
c          call flushit(6)
          jtran = 0
          dist = 1.d0
          iblock = iblklo
          iside = 3
          if (iblklo.le.0) then
            iblock = iblkup
            iside = 1
          endif
  150     continue
            j0 = 1
            if (iblock.eq.iblkle) j0 = jle
            if (iblklo.le.0) jo = jle
            i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
            jjmp = jbmax(iblock) + 2*nhalo
            if (cmesh) then
              jhigh = j0
              j0 = 1
            else
              jhigh = jbmax(iblock)
            endif
            do 160 jj=jhigh,j0,-1
              if (cmesh) then
                xp = x(i0 + jj)
                yp = y(i0 + jj)
              else
                xp = x(i0 + jj + (kbmax(iblock)-1)*jjmp)
                yp = y(i0 + jj + (kbmax(iblock)-1)*jjmp)
              endif
              if (abs(gradc).gt.1e-09) then
                xq = (yp - yte + gradc*xte - gradp*xp)/(gradc - gradp)
              else
                xq = xp
              endif
              xloc = abs((xle - xq)/(xle - xte))
              dist2 = abs(translo(ifoil)-xloc)
              if (dist2.lt.dist) then
                iblktran = iblock
                jtran = jj
                xtran = xq
                dist = dist2
              endif
  160       continue
            nextblk = lblkpt(iblock,2)
          if (ibctype(nextblk,iside).eq.1.and.iblock.ne.iblkle) then
            iblock = nextblk
            goto 150
          endif
c          write(n_out,*) 'iblktran_lo',iblktran
c          call flushit(6)
c
c if the leading edge is on the other side of the stagnation point
c (i.e., the upper surface), check aft of the stagnation point on
c the upper surface until the l.e. is reached.
c
          if ((iupflag.eq.1).and.(.not.cmesh)) then
            iblock = lblkpt(nextblk,3)
            iblock = lblkpt(iblock,4)
            nn=0
  170       continue
            nn=nn+1
              j0 = jbmax(iblock)
              if (iblock.eq.iblkle) j0 = jle
              i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
              jjmp = jbmax(iblock) + 2*nhalo
              do 180 jj=1,j0
                xp = x(i0 + jj)
                yp = y(i0 + jj)
                if (abs(gradc).gt.1e-09) then
                  xq = (yp - yte + gradc*xte - gradp*xp)/(gradc - gradp)
                else
                  xq = xp
                endif
                xloc = (xle - xq)/(xle - xte)
                dist2 = abs(translo(ifoil)-xloc)
                if (dist2.lt.dist) then
                  iblktran = iblock
                  jtran = jj
                  xtran = xq
                  dist = dist2
                endif
  180         continue
            if (iblock.ne.iblkle) then
              iblock = lblkpt(iblock,4)
              goto 170
            endif
          endif
c
c move point by 2 grid points to allow for turbulence model ramp-in
c
c          write(n_out,*) 'iblktran,iblkle',iblktran
c          stop
          if (iupflag.eq.1.and.iblktran.eq.iblkle) then
            jtran = jtran + 2
            if (jtran.gt.jle) jtran = jle
          else
            if (jtran.ge.3) then
              jtran = jtran - 2
              if (iblktran.eq.iblkle.and.jtran.lt.jle) jtran = jle
            else
              nextblk = lblkpt(iblktran,2)
              if (ibctype(nextblk,3).eq.1) then
                iblktran = nextblk
                jtran = jbmax(nextblk) - 2 + jtran
                if (iblktran.eq.iblkle.and.jtran.lt.jle) jtran = jle
              else
                nextblk = lblkpt(nextblk,3)
                nextblk = lblkpt(nextblk,4)
                iblktran = nextblk
                jtran = 3 - jtran
                if (iblktran.eq.iblkle.and.jtran.gt.jle) jtran = jle
              endif
            endif
          endif
c
c set j point for transition in block "iblktran"
c
          iswitch = 0
          jtran = jtran + nhalo
          if (cmesh) then
            jtranlo(1,iblktran) = jtran
          else
            if (iupflag.eq.1.and.iblktran.eq.iblkle) then
              jtranlo(1,iblktran) = jtran
              iswitch = 1
            else
              jtranup(3,iblktran) = jtran
              if (jtranlo(3,iblktran).eq.-99) jtranlo(3,iblktran) = 0
            endif
          endif
c
c set j limits in all blocks upstream of transition point
c
          nextblk = lblkpt(iblktran,2)
          if (ibctype(nextblk,3).eq.1.and.iswitch.eq.0) then
  190       continue
              jtranup(3,nextblk) = 2*nhalo + jbmax(nextblk)
              if (jtranlo(3,nextblk).eq.-99) jtranlo(3,nextblk) = 0
              nextblk = lblkpt(nextblk,2)
            if (ibctype(nextblk,3).eq.1) goto 190
          endif
c
          write(n_out,195)translo(ifoil)*100.d0
  195     format('   at ',f6.2,'% chord on the lower surface.')
c
        endif
c
  900 continue
c
c
c
      return
      end
