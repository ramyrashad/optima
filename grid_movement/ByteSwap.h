#ifndef BYTESWAP_H
#define BYTESWAP_H
#include <algorithm>  //required for std::swap from Big to Little endian
#include <cmath>
#define ByteSwap5(x) ByteSwap((unsigned char *) &x,sizeof(x))

// when reading fortran unformatted files on Linux-alpha
using namespace std;

void ByteSwap (unsigned char * b, int n);

#endif   // BYTESWAP_H
