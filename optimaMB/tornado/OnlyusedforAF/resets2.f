c ********************************************************************
c ********        Routine to reset halo points in S        ***********
c ********************************************************************
c
      subroutine resets2(iblk,jdim,kdim,s)
c
#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
c
      dimension s(jdim,kdim,4)   
c
      do 9 n=1,4                                                        
c     do 9 k=1,kminbnd(iblk)-1                                             
      do 9 k=1,nhalo
      do 9 j=1,jdim                                                     
        s(j,k,n) = 0.                                                  
    9 continue                                                          
c
      do 99 n=1,4                                                        
c     do 99 k=kmaxbnd(iblk)+1,kdim
      do 99 k=kdim-nhalo+1,kdim
      do 99 j=1,jdim                                                     
        s(j,k,n) = 0.                                                  
 99   continue                                                          
c
      do 8 n=1,4                                                        
      do 8 k=1,kdim
      do 8 j=1,nhalo 
c     do 8 j=1,jminbnd(iblk)-1                                        
        s(j,k,n) = 0.                                                  
    8 continue                                                          
c
      do 88 n=1,4                                                        
      do 88 k=1,kdim
      do 88 j=jdim-nhalo+1,jdim
c     do 88 j=jmaxbnd(iblk)+1,jdim
        s(j,k,n) = 0.                                                  
 88   continue                                                          
c
      return
      end
