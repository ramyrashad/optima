************************************************************************
      !-- Program name: addDissx
      !-- Written by: Howard Buckley
      !-- Date: April 2010
      !-- 
      !-- If dissipation-based continuation is to be used as a
      !-- globalization method for N-K, then add additional 
      !-- dissipation to inviscid flux derivative dE/d_xi
************************************************************************

      subroutine  addDissx(jdim,kdim,q,s,xyj)

#ifdef _MPI_VERSION
      use mpi
#endif


      use disscon_vars
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      !-- Declare variables

      integer
     &     j, jdim, j1, j2, jpl, jmi, k, kdim, n

      double precision
     &     cf2xdc(jdim,kdim), c2, dtd,
     &     work(jdim,kdim,3), q(jdim,kdim,4), xyj(jdim,kdim),
     &     tmp(jdim,kdim,4), s(jdim,kdim,4)

      !-- Initialize 'tmp' array                  

      do 3 n=1,4
      do 3 k=1,kdim
      do 3 j=1,jdim
         tmp(j,k,n)=0.0
 3    continue

      !-- Calculate a second-difference scalar dissipation coefficient
      !-- governed by the continuation parameter 'lamDiss'

      do k = kbegin,kend
        do j = jbegin,jup
          jpl = jplus(j) 
          c2 = lamDiss*(spectx_dc(jpl,k)+spectx_dc(j,k))
          cf2xdc(j,k) = c2
        end do
      end do

      !-- Calculate the additional dissipation term to be added to
      !-- the inviscid flux derivative

      do n = 1,4 !-- 1st-order forward difference

        do k =klow,kup 
          do j =jlow,jup
            jpl=jplus(j)
            work(j,k,1) = q(jpl,k,n)*xyj(jpl,k) - q(j,k,n)*xyj(j,k)
          end do
        end do
   
        !-- c mesh bc 
        if (.not.periodic) then
          j1 = jbegin
          j2 = jend
          do k = klow,kup
             work(j1,k,1) = q(j1+1,k,n)*xyj(j1+1,k) -
     &                     q(j1,k,n)*xyj(j1,k)
             work(j2,k,1) = work(j2-1,k,1)
          end do
        endif

        !-- Form dissipation term 
        do k =klow,kup
          do j =jlow,jup
            work(j,k,2) = cf2xdc(j,k)*work(j,k,1)
          end do
        end do

        !-- c mesh bc 
        if (.not.periodic) then
           j1 = jbegin
           j2 = jend
           do k = klow,kup
              work(j1,k,2) = cf2xdc(j1,k)*work(j1,k,1) 
              work(j2,k,2) = 0.d0
           end do
        endif

        !-- Do a backwards difference on the dissipation term
        dtd = dt / (1. + phidt)
        do k = klow,kup
          do j = jlow,jup
            jmi=jminus(j)
            tmp(j,k,n) = (work(j,k,2) - work(jmi,k,2))*dtd
          end do
        end do

      end do ! n=1,4

      !-- Add dissipation term to dE/d_xi

      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
           s(j,k,n)=s(j,k,n)+tmp(j,k,n)
 200  continue

      return
      end
