c ********************************************************************
c ************  calculate pressure and sound speed *******************
c ********************************************************************
      subroutine calcps(iblk,jdim,kdim,q,press,sndsp,precon,xy,xyj,
     &                  jbegin,jend,kbegin,kend)

      implicit none
c                                                                       
#include "../include/common.inc"
c
      integer iblk,jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      double precision precon(jdim,kdim,8),xy(jdim,kdim,4)
      double precision xyj(jdim,kdim),du1,du2,du3,dlimit,u,v,vel2,ux,uy
      double precision dul,duw,dlim,epsilon
c                                                                       
c  calculate pressure and sound speed and store                         
c                                                                       
c  note pressure has jacobian                                           
c       sound speed does not have jacobian                              

        do k = kbegin,kend
           do j = jbegin,jend

c           if (abs(q(j,k,1)) .lt. 1.e-10) then
c              write (*,*) 'density',iblk,j,k
c           end if
              if (q(j,k,1).eq.0.d0) then
                 write(*,*) 'calcps: divide by zero',iblk,j,k,q(j,k,1)
                 stop
              endif

              press(j,k) = gami*(q(j,k,4) -
     *             0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))

c     BEGIN DEBUG: Trap negative pressure
              if (press(j,k) .lt. 0.d0) then
                 write(*,*) 'calcps: neg press',iblk,j-2,k-2,
     &                press(j,k),q(j,k,1),q(j,k,2),q(j,k,3),q(j,k,4)
                 stop
              endif
c     END DEBUG



c$$$              if (press(j,k).lt.0.d0) then
c$$$                 press(j,k)=abs(press(j,k))
c$$$              endif
c$$$              if (q(j,k,1).lt.0.d0) then
c$$$                 q(j,k,1)=abs(q(j,k,1))
c$$$              endif





              sndsp(j,k) = gamma*press(j,k)/q(j,k,1)

c     BEGIN DEBUG: Trap floating point error
              if (sndsp(j,k) .lt.0.d0) then
                 write(*,*) 'calcps: neg sndsp^2',iblk,j-2,k-2
              endif
c     END DEBUG

              sndsp(j,k) = SQRT(sndsp(j,k))

           end do
        end do



c
csd   PRECONDITIONING STUFF                                        
csd   precon(j,k,   1) = epsilon
csd                 2) = (u^2+v^2)/2
csd                 3) = c^2
csd                 4) = lower limit for epsilon
csd                 5) = Ax
csd                 6) = Bx
csd                 7) = Ay
csd                 8) = By where lambda_i = Ai +/- Bi
csd
        if (prec.gt.0) then
           du1=0.0
           du2=0.0
           du3=0.0
c
           if (prec.eq.1) then
              du1=1.0
              dlimit=1.0
           elseif (prec.eq.2) then
              du2=1.0
              dlimit=1.0e-2
           else
              du3=1.0
              dlimit=prphi*fsmach**2
           endif
c
           do 1000 k=kbegin,kend
              do 1000 j=jbegin,jend
c
                 u=q(j,k,2)/q(j,k,1)
                 v=q(j,k,3)/q(j,k,1)
                 vel2=u**2+v**2

                 precon(j,k,2)=.5d0*vel2
                 precon(j,k,3)=sndsp(j,k)**2
c
c vel2 now mach**2
                 vel2=vel2/precon(j,k,3)
c
c contravariant velocities
                 ux=xy(j,k,1)*u+xy(j,k,2)*v
                 uy=xy(j,k,3)*u+xy(j,k,4)*v
c
                 dul=xy(j,k,1)**2+xy(j,k,2)**2
                 duw=xy(j,k,3)**2+xy(j,k,4)**2
c
                 dlim=max(dlimit,prxi*rhoinf*sqrt(max(duw,dul))
     +                /(xyj(j,k)*re*q(j,k,1)*fsmach))
c
c for prec=1 epsilon=1
c for prec=2 epsilon=local mach  => accoeding to Dave ... this is obsolete
c for prec=3 epsilon=(local mach)**2
c
                 epsilon=du1+du2*sqrt(vel2)+du3*vel2
                 epsilon=min(1.0,max(epsilon,dlim))
c
                 precon(j,k,1)=epsilon
                 precon(j,k,4)=dlimit
c     
c forming first part of preconditioned eigenvalue
                 precon(j,k,5)=.5d0*(1.0+epsilon)*ux
                 precon(j,k,7)=.5d0*(1.0+epsilon)*uy
c
c forming second part of preconditioned eigenvalue
c
                 precon(j,k,6)=sqrt((.5d0*(1.d0-epsilon)*ux)**2+
     +                epsilon*(dul)*precon(j,k,3))
                 precon(j,k,8)=sqrt((.5d0*(1.d0-epsilon)*uy)**2+
     +                epsilon*(duw)*precon(j,k,3))
 1000      continue
        endif

        return                                                          
        end                                                             


