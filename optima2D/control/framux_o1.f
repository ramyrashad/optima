c-----------------------------------------------------------------------
c     -- framux_o1: first-order accurate Frechet matrix A multiply
c     vector X.  This subroutine evaluates a matrix-vector product
c     using forward differences: v2 = A*v1 --
c     
c     Author : A. Pueyo, Mods: M. Nemec
c     Date: Dec. 1995, Sept. 2000
c-----------------------------------------------------------------------
      subroutine framux_o1 (na, jdim, kdim, ndim, indx, iex, v1, v2, qr,
     &     xy, xyj, x, y, precon, coef2, coef4,uu, vv, ccx, ccy, ds,
     &     press, tmet, spect, gam, turmu, fmu, vort, sndsp, sr, bcf,
     &     q, qnow, qold, s, ifirst, as, work,rhsnamult,D_hat,exprhs,
     &     a_jk,dtvec)

      use disscon_vars
      implicit none
c     
#include "../include/arcom.inc"
      
      
      integer jdim,kdim,ndim,na,n,ifirst,j,k,i,jk
      integer indx(jdim,kdim), iex(jdim,kdim,4), nscal

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision eps, v1(na), v2(na), v1_n, eps1
      double precision qr(jdim,kdim,ndim), sr(jdim,kdim,ndim)
      double precision q(jdim,kdim,ndim), s(jdim,kdim,ndim)
      double precision s0(jdim,kdim,ndim), fac
      double precision qold(jdim,kdim,ndim), qnow(jdim,kdim,ndim)

      double precision bcf(jdim,kdim,4), xy(jdim,kdim,4), alphatemp
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)

      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)
      double precision work(jdim,kdim,4), cleq, cleqr

      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer jpa(jdim*kdim*ndim*ndim*5+1)
      integer ia(jdim*kdim*ndim+2),ipa(jdim*kdim*ndim+2)

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100)

      logical imps, jac_tmp, prec_tmp

      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim)
      double precision a_jk(6,6),rhsnamult,rhsnamult2
c     -- JG: time vector in matrix-vector product --
      double precision dtvec(*)

clb   The variable qr is the current flow variables and sr is the 
clb   residual based on those flow variables.
clb   The variable q is the offset flow variables and s is the 
clb   residual based on those flow variables.
      
      jac_tmp = jac_mat
      prec_tmp = prec_mat

      jac_mat = .false.
      prec_mat = .false.

      if (ifirst.eq.1) then
        do j = 1,na
          v2(j) = 0.d0
        end do
      else
        v1_n = 0.d0
        do i = 1,na
          v1_n = v1_n + v1(i)**2
        end do
        v1_n = dsqrt(v1_n)
        eps  = dsqrt(2.2d-16) / v1_n
        eps1 = 1.d0/eps
        
c     -- evaluate vector --
        do n = 1,ndim
          do k = 1,kmax
            do j = 1,jmax
              jk = ( indx(j,k)-1 )*ndim + n
              q(j,k,n) = qr(j,k,n) + eps*v1(jk)
            end do
          end do
        end do

        if (clalpha2) then
           alphatemp=alpha
           alpha = alpha + eps*v1(na)/1.d2
           uinf = fsmach*cos(alpha*pi/180.d0)
           vinf = fsmach*sin(alpha*pi/180.d0)
        end if

        call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- compute laminar viscosity based on sutherland's law --
        if (viscous) call fmun(jdim, kdim, q, press, fmu, sndsp)


c     -- function evaluation --
        call get_rhs (jdim, kdim, ndim, q, qnow, qold, xy, xyj, x, y, 
     &       precon, coef2, coef4, uu, vv, ccx, ccy, ds, press, tmet, 
     &       spect, gam, turmu, fmu, vort, sndsp, s, s0, work1, D_hat, 
     &       exprhs, a_jk, .false.)

        !-- If dissipation-based continuation is to be used as a
        !-- globalization method for N-K, then add additional 
        !-- dissipation to inviscid flux derivatives on RHS.

        if (dissCon) then

           call addDissx(jdim,kdim,q,s,xyj)
           call addDissy(jdim,kdim,q,s,xyj) 
       
        end if
        
        call scale_bc (1, jdim, kdim, ndim, indx, iex, ia, ja, as, ipa,
     &        jpa, as, s, s0, bcf, as, rhsnamult2, .false.)

c     -- evaluate difference: S = -R(Q); reverse the sign of s-sr --
        do n = 1,ndim
          do k = 1,kmax
            do j = 1,jmax
              jk = ( indx(j,k)-1 )*ndim + n
              v2(jk) = -eps1 * (s(j,k,n) - sr(j,k,n))
c     JG: add the time step pointwise times v1
              v2(jk) = v2(jk) + v1(jk)*dtvec(jk)
            end do
          end do
        end do

clb   -- Adding equation for cl due to variable angle of attack logic --
        if (clalpha2) then
            
clb   Adding in equation due to cl term
           nscal = 0
           call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
     &          cli, cdi, cmi, clv, cdv, cmv) 
           clt = cli + clv
           cleq=-(clt-clinput)*rhsnamult

           alpha = alphatemp
           uinf = fsmach*cos(alpha*pi/180.d0)
           vinf = fsmach*sin(alpha*pi/180.d0)
           call calcps (jdim, kdim, qr, press, sndsp, 
     &          precon, xy, xyj)
           call clcd (jdim, kdim, qr, press, x, y, xy, xyj, nscal,
     &          cli, cdi, cmi, clv, cdv, cmv) 
           clt = cli + clv
           cleqr=-(clt-clinput)*rhsnamult
         
           v2(na) = - eps1 * (cleq - cleqr)
c     JG: add the time step pointwise times v1
           v2(na) = v2(na)+ v1(na)*dtvec(na)
           
        end if
clb
      end if


      jac_mat = jac_tmp
      prec_mat = prec_tmp
      
      return
      end                       !framux_o1
