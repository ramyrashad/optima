c     -- Re-plot the axis, grid  --
c     Re-plot airfoil, control points 
c     based on GUI interface by  po-yang Jay Liu 
c     m. nemec,  may 2000
      subroutine graphit ( relsize, ipslu, idev, x_org, y_org, x_scale,
     &      y_scale, bap, bcp, jbody, nc, iorder )

      implicit none

      double precision x_scale, x_org, y_scale, y_org, relsize

      integer ipslu, idev, jbody, nc, iorder
      double precision bap(jbody,2), bcp(nc,2)

c     -- cursor position and boundary (relative position, not abs) --
      double precision x_right, x_left, y_up, y_down
      data x_right, x_left, y_up, y_down
     &   /   1.1,   -0.1,    0.2,   -0.2/

c     -- initializing the displaying window --
      call plopen ( relsize, ipslu, idev )
      call drawtoscreen
      call newfactors ( x_scale, y_scale )

c     -- draw grid and axis in black --
      call newcolorname ('black')
      call gridaxis (x_org, y_org, x_scale, y_scale, x_left, x_right,
     &      y_down, y_up)
      
c     -- display caption for spline info --
      call plchar(x_left, -0.41, 0.02, 'Order of B-spline   :', 0.0,-1)
      call plchar(x_left, -0.45, 0.02, '# of data points    :', 0.0,-1)
      call plchar(x_left, -0.49, 0.02, '# of control points :', 0.0,-1)

c     -- draw airfoil --
      call newcolorname ('red')
      call xyline (jbody,bap(1,1),bap(1,2),0.0,1.0,0.0,1.0,1)

c     -- draw control points --
      call newcolorname ('yellow')
      call xyline (nc,bcp(1,1),bcp(1,2),0.0,1.0,0.0,1.0,1)
      call xysymb (nc,bcp(1,1),bcp(1,2),0.0,1.0,0.0,1.0,0.01,5)

c     -- display spline info --
      call newcolorname('black')
      call plnumb(0.15 + (2)*0.15, -0.41, 0.02, real(iorder), 0.0, -1)
      call plnumb(0.15 + (2)*0.15, -0.45, 0.02, real(jbody), 0.0, -1)
      call plnumb(0.15 + (2)*0.15, -0.49, 0.02, real(nc), 0.0, -1)

      call plflush

      end                       !graphit

c     *******************************************
c     ** Set up the Grid and Axis with offsets **
c     *******************************************
      subroutine gridaxis  (x_org, y_org, x_scale, y_scale, 
     &      x_left, x_right, y_down, y_up)

      implicit none

      double precision  x_org, y_org, x_scale, y_scale, 
     &      x_left, x_right, y_down, y_up
      integer  mask0,  mask1,  mask2,  mask3,  mask4
      parameter (mask0 = -1    ,  mask1 = -21846,  mask2 = -30584,
     &      mask3 = -32640,  mask4 = -32768 )

      x_org = 0.6 - x_left*x_scale
      y_org = 3.0 - y_down*y_scale

      call plotabs ( x_org,y_org,-3)

c     -- dot-pattern masks for plgrid, newpat, etc. --
c     mask0:  _________________________   (solid)
c     1:  .........................          
c     2:  . . . . . . . . . . . . .          
c     3:  .   .   .   .   .   .   .          
c     4:  .       .       .       .          

      call plgrid(x_left, y_down, int((x_right-x_left)/0.025),0.025,
     &      int((y_up-y_down)/0.025), 0.025, mask3 )
      call plgrid(x_left, y_down, int((x_right-x_left)/0.05), 0.05,
     &      int((y_up-y_down)/0.05), 0.05, mask2)
      call plgrid(x_left, y_down, int((x_right-x_left)/0.1), 0.1, 
     &      int((y_up-y_down)/0.1), 0.1, mask1)

c     -- top axis --
      call xaxis(x_left, y_up, x_right-x_left, 0.1, x_left, 0.1,
     &      -0.01, 2)
c     -- middle axis --
      call xaxis(x_left, 0.0, x_right-x_left, 0.1, x_left, 0.1, 0.01, 2)
c     -- bottom axis --
      call xaxis(x_left, y_down, x_right-x_left, 0.1, x_left, 0.1,
     &      0.01, 2)
c     -- left axis --
      call yaxis(0.0 , y_down,  y_down-y_up, 0.1, y_down, 0.1, 0.01, 2)
c     -- right axis --
      call yaxis(1.0, y_down, y_down-y_up, 0.1, y_down, 0.1, -0.01, 2)

      end                       !gridaxis

