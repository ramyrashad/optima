c ##########################
c ##                      ##
c ##  subroutine filty    ##
c ##                      ##
c ##########################
c
      subroutine filty (jdim,kdim,q,s,xyj,coef2,coef4,sndsp,work,
     >                    precon,tmp,tmp2)            
c
c********************************************************************
c                                                                       
c    fourth order smoothing, added explicitly to rhs                     
c second order near shocks with pressure grd coeff.                   
c                                                                       
c               eta direction
c                                                                       
c   start differences each variable separatly                           
c********************************************************************
c
      include '../include/arcom.inc'
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)
      dimension work(jdim,kdim,4),precon(jdim,kdim,6)
      dimension tmp(jdim,kdim,4),tmp2(jdim,kdim,4)
c
      do 39 n = 1,4                                                     
c----    1st-order forward difference ----
         do 35 k = kbegin,kup                                              
         kpl = k+1
         do 35 j = jlow,jup                                                
            tmp(j,k,n) = q(j,kpl,n)*xyj(j,kpl) - q(j,k,n)*xyj(j,k)           
 35      continue                                                          
c        
c        
c----    for fourth order: apply cent-dif to 1st-order forward ----
         do 36 k = klow,kup-1                                              
         kpl = k+1                                                         
         kmi = k-1                                                         
         do 36 j = jlow,jup                                                
            tmp2(j,k,n) = tmp(j,kpl,n) - 2.d0* tmp(j,k,n) + tmp(j,kmi,n)     
 36      continue                                                          
c  --    next points to boundaries: rows 2 & kmax-1 --
         kb = kbegin
         do 37 j = jlow,jup                                                
         tmp2(j,kbegin,n) = q(j,kb+2,n)*xyj(j,kb+2) -                          
     >                2.d0*q(j,kb+1,n)*xyj(j,kb+1) + q(j,kb,n)*xyj(j,kb)
         tmp2(j,kup,n) = tmp2(j,kup-1,n) - tmp2(j,kup,n)                   
 37      continue                                                          
 39   continue                                                          
c                                                                       
c
      call predis2(jdim,kdim,jlow,jup,kbegin,kup,
     &                       0,0,q,xyj,sndsp,precon,tmp,work)
      call predis2(jdim,kdim,jlow,jup,kbegin,kup,
     &                       0,1,q,xyj,sndsp,precon,tmp,work)
c
      do 91 n=1,4
      do 91 k=kbegin,kup
      do 91 j=jlow,jup
         work(j,k,n)=coef2(j,k)*work(j,k,n)+coef2(j,k+1)*tmp(j,k,n)
 91   continue
c
      call predis2(jdim,kdim,jlow,jup,kbegin,kup,
     &                       0,0,q,xyj,sndsp,precon,tmp2,tmp)
      call predis2(jdim,kdim,jlow,jup,kbegin,kup,
     &                       0,1,q,xyj,sndsp,precon,tmp2,tmp)
c      
      do 92 n=1,4
      do 92 k=kbegin,kup
      do 92 j=jlow,jup
         work(j,k,n)=work(j,k,n)-
     &             (coef4(j,k)*tmp(j,k,n)+coef4(j,k+1)*tmp2(j,k,n))
 92   continue
c
c
c
c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)                                           
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
           s(j,k,n)=s(j,k,n) + (work(j,k,n) - work(j,k-1,n))*dtd
 200  continue
      return                                                            
      end                                                               
