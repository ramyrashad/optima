c----------------------------------------------------------------------
c     -- adjust leading edge and trailing edge blocks --
c     -- m. nemec --
c---------------------------------------------------------------------
      subroutine modle ( j1, j2, js, js2, je, je2, ji, ks, kf, ki, jdle, 
     &     kdle, xle, yle, jdim, kdim, x, y, seg)

      integer jdim, kdim, jdle, kdle, i, j, k, ks, kf, ki

      double precision x(jdim,kdim), y(jdim,kdim)
      double precision xle(jdle,kdle), yle(jdle,kdle)
      double precision dx, dy, tmp, seg(jdim*kdim)
      logical mody, modx
      
c      write (*,*) 'loop', ks, kf, ki

      if (kdle.ne.kdim) then
         write (*,*) 'modle: ** problem with block dimensions **'
         stop
      end if

      do k = ks, kf, ki
         
         dy = yle(j1,k) - y(j2,k)
         dx = xle(j1,k) - x(j2,k)

         mody = .false.
         modx = .false.

         if ( abs( dy ) .gt. 1.e-15 ) mody=.true.
         if ( abs( dx ) .gt. 1.e-15 ) modx=.true.

         if ( mody .or. modx ) then
c     -- calculate arclength for j-line at location k --
            i = 1
            seg(i) = 0.d0
            do j = js2, je2, ji
               tmp = sqrt( (x(j,k) - x(j+1,k) )**2 + ( y(j,k) -
     &              y(j+1,k) )**2 )
               i = i+1
               seg(i) = seg(i-1) + tmp
            end do

c     -- adjust grid --
            if ( mody ) then
               i = 1
               do j = js, je, ji
                  y(j,k) = y(j,k) + dy/2.0*( 1.0 + cos(4.0*atan(1.0)
     &                 *seg(i)/seg(jdim)) )
c     y(j,k) = y(j,k) + dy*( 1.0 - seg(i)/seg(jdim) )
                  i = i+1
               end do
            end if

            if ( modx ) then
               i = 1
               do j = js, je, ji
                  x(j,k) = x(j,k) + dx/2.0*( 1.0 + cos(4.0*atan(1.0)
     &                 *seg(i)/seg(jdim)) )
c     x(j,k) = x(j,k) + dx*( 1.0 - seg(i)/seg(jdim) )
                  i = i+1
               end do
            end if

         end if

      end do

      return
      end                       ! modle
