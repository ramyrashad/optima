      subroutine vismaty(JDIM,KDIM,Q,PRESS,XY,XYJ,TURMU,FMU,A,B,C,D,E,  
     >                   DJ,DDJ)                                        
c
#include "../include/arcom.inc"
c                                                                       
      DIMENSION Q(JDIM,KDIM,4),XYJ(JDIM,KDIM),XY(JDIM,KDIM,4)           
      DIMENSION TURMU(JDIM,KDIM),PRESS(JDIM,KDIM),FMU(JDIM,KDIM)        
c                                                                       
      DIMENSION A(JDIM,KDIM,4),B(JDIM,KDIM,4,4),C(JDIM,KDIM,4,4),       
     >          D(JDIM,KDIM,4,4),E(JDIM,KDIM,4)                         
      dimension dj(jdim,kdim,3,4),ddj(jdim,kdim,3,4)                    
C                                                                       
c   note  :: A and E used as temporaries                                
C                                                                       
C                                                                       
C                                                                       
      DATA PR,PRTR,FRT/.72,.8,1.333333/                                 
C                                                                       
C                                                                       
      HD = DT * .5 * THETADT/(1. + PHIDT)                               
      HRE = HD/RE                                                       
      GPR = GAMMA/PR                                                    
C                                                                       
      DO 10 K = KBEGIN,KEND                                             
         DO 10 J = JLOW,JUP                                             
C                                                                       
C    SUTHERLAND EQUATION                                                
C                                                                       
C                                                                       
            RINV   = 1./Q(J,K,1)                                        
            EXS    = XY(J,K,3)**2                                       
            EYS    = XY(J,K,4)**2                                       
            EXY    = XY(J,K,3)*XY(J,K,4)*.3333333                       
            DJAC   = HRE/XYJ(J,K)                                       
C                                                                       
            E( J, K,1) = DJAC*( FRT*EXS + EYS)                          
            E( J, K,2) = DJAC*EXY                                       
            E( J, K,3) = DJAC*( EXS + FRT*EYS)                          
            E( J, K,4) = GPR*DJAC*( EXS + EYS)                          
            A( J, K,1) = RINV                                           
            A( J, K,2) = RINV*Q(J,K,2)                                  
            A( J, K,3) = RINV*Q(J,K,3)                                  
            A( J, K,4) = RINV*Q(J,K,4)                                  
     >                   - ( A(J,K,2)**2 + A(J,K,3)**2 )                
C                                                                       
10    CONTINUE                                                          
C                                                                       
      DO 20 K = KBEGIN,KUP                                              
         KP = K + 1                                                     
      DO 20 J = JLOW,JUP                                                
C                                                                       
c    Turmu is at j,k+1/2                                                
c                                                                       
            TURM   = TURMU(J,K)                                         
c  average fmu to j,k+1/2 point                                         
            FMUM   = 0.5*(FMU(J,KP)   + FMU(J,K)  )                     
            VNU    = FMUM + TURM                                        
            GKAP   = FMUM + PRTR*TURM                                   
c                                                                       
            CC1 = E( J, K,1) + E( J, KP,1)                              
            CC2 = E( J, K,2) + E( J, KP,2)                              
            CC3 = E( J, K,3) + E( J, KP,3)                              
            CC4 = E( J, K,4) + E( J, KP,4)                              
            CC1 = CC1*VNU                                               
            CC2 = CC2*VNU                                               
            CC3 = CC3*VNU                                               
            CC4 = CC4*GKAP                                              
C                                                                       
            dj( J, K,1,1) =                                             
     >         - ( CC1*A(J,K,2) + CC2*A(J,K,3))*A(J,K,1)                
            ddj( J, K,1,1) =                                            
     >         - ( CC1*A(J,KP,2) + CC2*A(J,KP,3))*A(J,KP,1)             
            dj( J, K,1,2) = CC1*A( J, K,1)                              
            ddj( J, K,1,2) = CC1*A( J, KP,1)                            
            dj( J, K,1,3) = CC2*A( J, K,1)                              
            ddj( J, K,1,3) = CC2*A( J, KP,1)                            
            dj( J, K,1,4) = 0.                                          
            ddj( J, K,1,4) = 0.                                         
            dj( J, K,2,1) =                                             
     >        - ( CC2*A(J,K,2) + CC3*A(J,K,3))*A(J,K,1)                 
            ddj( J, K,2,1) =                                            
     >        - ( CC2*A(J,KP,2) + CC3*A(J,KP,3))*A(J,KP,1)              
            dj( J, K,2,2) = CC2*A( J, K,1)                              
            ddj( J, K,2,2) = CC2*A( J, KP,1)                            
            dj( J, K,2,3) = CC3*A( J, K,1)                              
            ddj( J, K,2,3) = CC3*A( J, KP,1)                            
            dj( J, K,2,4) = 0.                                          
            ddj( J, K,2,4) = 0.                                         
C                                                                       
            dj( J, K,3,1) = -(CC4*A(J,K,4) + CC1*A(J,K,2)**2 +          
     >                     2.*CC2*A(J,K,2)*A(J,K,3) +                   
     >                     CC3*A(J,K,3)**2)*A(J,K,1)                    
            ddj( J, K,3,1) = -(CC4*A(J,KP,4) + CC1*A(J,KP,2)**2 +       
     >                     2.*CC2*A(J,KP,2)*A(J,KP,3) +                 
     >                     CC3*A(J,KP,3)**2)*A(J,KP,1)                  
            dj( J, K,3,2) = -CC4*A(J,K,2)*A(J,K,1) - dj(J,K,1,1)        
            ddj( J, K,3,2) = -CC4*A(J,KP,2)*A(J,KP,1) - ddj(J,K,1,1)    
            dj( J, K,3,3) = -CC4*A(J,K,3)*A(J,K,1) - dj(J,K,2,1)        
            ddj( J, K,3,3) = -CC4*A(J,KP,3)*A(J,KP,1) - ddj(J,K,2,1)    
            dj( J, K,3,4) = CC4*A( J, K,1)                              
            ddj( J, K,3,4) = CC4*A( J, KP,1)                            
C                                                                       
   20 CONTINUE                                                          
C                                                                       
      DO 30 K=KLOW,KUP                                                  
      KR = K-1                                                          
      DO 30 N=2,4                                                       
      DO 30 M=1,4                                                       
         DO 30 J = JLOW,JUP                                             
            B(J,K,N,M) = B(J,K,N,M) - dj(J, KR,N-1,M)                   
            C(J,K,N,M) = C(J,K,N,M) + dj(J, K,N-1,M) +ddj(J, KR,N-1,M)  
            D(J,K,N,M) = D(J,K,N,M) - ddj(J, K,N-1,M)                   
   30 CONTINUE                                                          
C                                                                       
C                                                                       
      RETURN                                                            
      END                                                               
