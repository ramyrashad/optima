      subroutine fixteavg2(jmax1,kmax1,q1,xyj1,jmax2,kmax2,q2,xyj2,
     &                     j1,k1,j2,k2)
#include "../include/common.inc"
c
      dimension q1(jmax1,kmax1,4), xyj1(jmax1,kmax1)
      dimension q2(jmax2,kmax2,4), xyj2(jmax2,kmax2)
c
      rr1 = 1./xyj1(j1,k1)
      rr2 = 1./xyj2(j2,k2)
c
      qsav1 = 0.5*(q1(j1,k1,1)*xyj1(j1,k1)+q2(j2,k2,1)*xyj2(j2,k2))
      qsav2 = 0.5*(q1(j1,k1,2)*xyj1(j1,k1)+q2(j2,k2,2)*xyj2(j2,k2))
      qsav3 = 0.5*(q1(j1,k1,3)*xyj1(j1,k1)+q2(j2,k2,3)*xyj2(j2,k2))
c
      ppp1 = gami*(q1(j1,k1,4)-0.5*
     &         (q1(j1,k1,2)**2+q1(j1,k1,3)**2)/q1(j1,k1,1))*xyj1(j1,k1)
      ppp2 = gami*(q2(j2,k2,4)-0.5*
     *         (q2(j2,k2,2)**2+q2(j2,k2,3)**2)/q2(j2,k2,1))*xyj2(j2,k2)
      psav = 0.5*(ppp1+ppp2)

      q1(j1,k1,1) = qsav1*rr1
      q2(j2,k2,1) = qsav1*rr2
c
      q1(j1,k1,2) = qsav2*rr1
      q2(j2,k2,2) = qsav2*rr2
c
      q1(j1,k1,3) = qsav3*rr1
      q2(j2,k2,3) = qsav3*rr2
c
      q1(j1,k1,4) = psav/gami*rr1 +
     *          0.5*(q1(j1,k1,2)**2+q1(j1,k1,3)**2)/q1(j1,k1,1)
      q2(j2,k2,4) = psav/gami*rr2 +
     *          0.5*(q2(j2,k2,2)**2+q2(j2,k2,3)**2)/q2(j2,k2,1)
      return
      end
