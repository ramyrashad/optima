c----------------------------------------------------------------------
c     -- the non-linear (JST) artificial dissipation coefficients are
c        added to the Jacobian matrix and preconditioner. --
c     -- based on impdissx from probe
c     -- modified by marian nemec, may 2000
c----------------------------------------------------------------------
      subroutine fillasdx(jdim, kdim, ndim, xyj, coef, coef2, coef4,
     &     indx, icol, as, pa, pdc)

      use disscon_vars
      implicit none

#include "../include/arcom.inc"

      integer j, k, ii, jdim, kdim, ndim, jp1, jm1, jp2, jm2, n, jj
      integer indx(jdim,kdim), icol(9),jsta,jsto

      double precision xyj(jdim,kdim), coef2(jdim,kdim)
      double precision coef(jdim,kdim), coef4(jdim,kdim)
clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1), fac
clb

      double precision c2m, c4m, c2, c4, pdc, c2dc
      double precision diag1, diag2, diag3, diag4, diag5

c     -----------------------------------------------------------------
      
      if ( prec_mat ) then

c     -- dissipation for first-order preconditioner --
        do k = kbegin,kend
          do j = jbegin,jend
            coef(j,k) = coef2(j,k) + pdc*coef4(j,k)
          end do
        end do

        !-- If dissipation-based continuation is being used as a
        !-- globalization method for N-K, then calculated a
        !-- dissipation coefficient based on the continuation
        !-- parameter 'lamDiss' and add it to the flow Jacobian
        !-- preconditioner
        
        if (dissCon) then
          do k = kbegin,kend
            do j = jbegin,jup
              jp1 = jplus(j) 
              c2dc = lamDiss*(spectx_dc(jp1,k)
     &                        +spectx_dc(j,k))
              coef(j,k) = coef(j,k) + c2dc         
            end do
          end do
        end if
       
        do j = jlow,jup
          do k = klow,kup
            jp1 = jplus(j)
            jm1 = jminus(j)
            c2 = coef(j,k)
            c2m = coef(jm1,k)
            diag1 = xyj(jm1,k)*c2m
            diag2 = xyj(j,k)  *(c2m + c2 )
            diag3 = xyj(jp1,k)*c2
            
            do n =1,4
              ii = ( indx(j,k) - 1 )*ndim + n
              jj = ( ii - 1 )*icol(5)
              pa(jj+n)    = pa(jj+n)    - diag1
              pa(jj+icol(2)+n) = pa(jj+icol(2)+n) + diag2
              pa(jj+icol(4)+n) = pa(jj+icol(4)+n) - diag3
            end do                                         
          end do
        end do

      end if

c     -----------------------------------------------------------------

      if ( jac_mat ) then
c     -- dissipation for jacobian --
c     -- interior nodes (j=3 to jend-2) --

         if(.not.periodic) then
            jsta=jlow+1
            jsto=jup-1
         else
            jsta=jlow
            jsto=jup
         end if

        do j =jsta,jsto                                             

          jp1 = jplus(j)
          jm1 = jminus(j)
          jp2 = jplus(jp1)
          jm2 = jminus(jm1)
          do k =klow+1,kup-1                                         
            c2m = coef2(jm1,k)                                       
            c4m = coef4(jm1,k)                                       
            c2  = coef2(j,k)                                          
            c4  = coef4(j,k)                                         
            diag1 = xyj(jm2,k)*c4m
            diag2 = xyj(jm1,k)*(c2m + 3.*c4m + c4)
            diag3 = xyj(j,k)  *(c2m+3.*c4m+c2+3.*c4)
            diag4 = xyj(jp1,k)*(c2 + 3.*c4 + c4m)
            diag5 = xyj(jp2,k)*c4
            do n =1,4
              ii = ( indx(j,k) - 1 )*ndim + n
              jj = ( ii - 1 )*icol(9)
              if (clopt) jj = ( ii - 1 )*(icol(9)+1)
              as(jj+        n) =                    diag1
              as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
              as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag3
              as(jj+icol(7)+n) = as(jj+icol(7)+n) - diag4
              as(jj+icol(8)+n) =                    diag5
            end do
          end do
        end do

        if(.not.periodic) then

        j = jlow
        jm1 = jminus(j)
        jp1 = jplus(j)
        jp2 = jplus(jp1)
        do k =klow+1,kup-1                                         
          c2m = coef2(jm1,k)                                       
          c4m = coef4(jm1,k)                                       
          c2  = coef2(j,k)                                          
          c4  = coef4(j,k)                                         
          diag2 = (c2m + c4m + c4)*xyj(jm1,k)                    
          diag3 = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                 
          diag4 = (c2 + 3.*c4 + c4m)*xyj(jp1,k)                  
          diag5 = xyj(jp2,k)*c4  
          do n =1,4
            ii = ( indx(j,k) - 1 )*ndim + n
            jj = ( ii - 1 )*icol(9)
            if (clopt) jj = ( ii - 1 )*(icol(9)+1)
            as(jj+        n) = as(jj+n)         - diag2
            as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag3
            as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
            as(jj+icol(7)+n) =                    diag5
          end do
        end do

        j = jup
        jm1 = jminus(j)
        jm2 = jminus(jm1)
        jp1 = jplus(j)
        do k =klow+1,kup-1                                         
          c2m = coef2(jm1,k)                                       
          c4m = coef4(jm1,k)                                       
          c2  = coef2(j,k)                                          
          c4  = coef4(j,k)    
          diag1 =      xyj(jm2,k)*c4m                                 
          diag2 =      (c2m + 3.*c4m + c4)*xyj(jm1,k)                 
          diag3 =      xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)                 
          diag4 =      (c2 + c4 + c4m)*xyj(jp1,k) 
          do n =1,4
            ii = ( indx(j,k) - 1 )*ndim + n
            jj = ( ii - 1 )*icol(9)
            if (clopt) jj = ( ii - 1 )*(icol(9)+1)
            as(jj+        n) =                    diag1
            as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
            as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag3
            as(jj+icol(7)+n) = as(jj+icol(7)+n) - diag4
          end do
        end do

        end if

        k = klow

        do j =jsta,jsto                                             

          jp1 = jplus(j)
          jm1 = jminus(j)
          jp2 = jplus(jp1)
          jm2 = jminus(jm1)
          c2m = coef2(jm1,k)                                       
          c4m = coef4(jm1,k)                                       
          c2  = coef2(j,k)                                          
          c4  = coef4(j,k)                                         
          diag1 = xyj(jm2,k)*c4m
          diag2 = xyj(jm1,k)*(c2m + 3.*c4m + c4)
          diag3 = xyj(j,k)  *(c2m+3.*c4m+c2+3.*c4)
          diag4 = xyj(jp1,k)*(c2 + 3.*c4 + c4m)
          diag5 = xyj(jp2,k)*c4
          do n =1,4
            ii = ( indx(j,k) - 1 )*ndim + n
            jj = ( ii - 1 )*icol(9)
            if (clopt) jj = ( ii - 1 )*(icol(9)+1)
            as(jj+        n) =                    diag1
            as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
            as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag3
            as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
            as(jj+icol(7)+n) =                    diag5
          end do
        end do

        k = kup

        do j =jsta,jsto                                            

          jp1 = jplus(j)
          jm1 = jminus(j)
          jp2 = jplus(jp1)
          jm2 = jminus(jm1)
          c2m = coef2(jm1,k)                                       
          c4m = coef4(jm1,k)                                       
          c2  = coef2(j,k)                                          
          c4  = coef4(j,k)                                         
          diag1 = xyj(jm2,k)*c4m
          diag2 = xyj(jm1,k)*(c2m + 3.*c4m + c4)
          diag3 = xyj(j,k)  *(c2m+3.*c4m+c2+3.*c4)
          diag4 = xyj(jp1,k)*(c2 + 3.*c4 + c4m)
          diag5 = xyj(jp2,k)*c4
          do n =1,4
            ii = ( indx(j,k) - 1 )*ndim + n
            jj = ( ii - 1 )*icol(9)
            if (clopt) jj = ( ii - 1 )*(icol(9)+1)
            as(jj+        n) =                    diag1
            as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
            as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag3
            as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
            as(jj+icol(7)+n) =                    diag5
          end do
        end do

        if(.not.periodic) then

        j = jlow
        k = klow
        jp1 = jplus(j)
        jm1 = jminus(j)
        jp2 = jplus(jp1)
        c2m = coef2(jm1,k)                                       
        c4m = coef4(jm1,k)                                       
        c2  = coef2(j,k)                                          
        c4  = coef4(j,k)                                         
        diag2 = (c2m + c4m + c4)*xyj(jm1,k)                    
        diag3 = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                 
        diag4 = (c2 + 3.*c4 + c4m)*xyj(jp1,k)                  
        diag5 = xyj(jp2,k)*c4
        do n =1,4
          ii = ( indx(j,k) - 1 )*ndim + n
          jj = ( ii - 1 )*icol(9)
          if (clopt) jj = ( ii - 1 )*(icol(9)+1)
          as(jj+        n) = as(jj+        n) - diag2
          as(jj+icol(2)+n) = as(jj+icol(2)+n) + diag3
          as(jj+icol(5)+n) = as(jj+icol(5)+n) - diag4
          as(jj+icol(6)+n) =                    diag5
        end do

        j = jlow
        k = kup
        jp1 = jplus(j)
        jm1 = jminus(j)
        jp2 = jplus(jp1)
        c2m = coef2(jm1,k)                                       
        c4m = coef4(jm1,k)                                       
        c2  = coef2(j,k)                                          
        c4  = coef4(j,k)                                         
        diag2 = (c2m + c4m + c4)*xyj(jm1,k)                    
        diag3 = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                 
        diag4 = (c2 + 3.*c4 + c4m)*xyj(jp1,k)                  
        diag5 = xyj(jp2,k)*c4      
        do n =1,4
          ii = ( indx(j,k) - 1 )*ndim + n
          jj = ( ii - 1 )*icol(9)
          if (clopt) jj = ( ii - 1 )*(icol(9)+1)
          as(jj+        n) = as(jj+        n) - diag2
          as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag3
          as(jj+icol(5)+n) = as(jj+icol(5)+n) - diag4
          as(jj+icol(6)+n) =                    diag5
        end do

        j = jup
        k = klow
        jp1 = jplus(j)
        jm1 = jminus(j)
        jm2 = jminus(jm1)
        c2m = coef2(jm1,k)                                       
        c4m = coef4(jm1,k)                                       
        c2  = coef2(j,k)                                          
        c4  = coef4(j,k)
        diag1 =      xyj(jm2,k)*c4m                                 
        diag2 =      (c2m + 3.*c4m + c4)*xyj(jm1,k)                 
        diag3 =      xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)                 
        diag4 =      (c2 + c4 + c4m)*xyj(jp1,k)
        do n =1,4
          ii = ( indx(j,k) - 1 )*ndim + n
          jj = ( ii - 1 )*icol(9)
          if (clopt) jj = ( ii - 1 )*(icol(9)+1)
          as(jj+        n) =                    diag1
          as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
          as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag3
          as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
        end do
        
        j = jup
        k = kup
        jp1 = jplus(j)
        jm1 = jminus(j)
        jm2 = jminus(jm1)
        c2m = coef2(jm1,k)                                       
        c4m = coef4(jm1,k)                                       
        c2  = coef2(j,k)                                          
        c4  = coef4(j,k)                                         
        diag1 =      xyj(jm2,k)*c4m                                 
        diag2 =      (c2m + 3.*c4m + c4)*xyj(jm1,k)                 
        diag3 =      xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)                 
        diag4 =      (c2 + c4 + c4m)*xyj(jp1,k)
        do n =1,4
          ii = ( indx(j,k) - 1 )*ndim + n
          jj = ( ii - 1 )*icol(9)
          if (clopt) jj = ( ii - 1 )*(icol(9)+1)
          as(jj+        n) =                    diag1
          as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
          as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag3
          as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
        end do
        
        end if

      end if

      return
      end                       ! fillasdx
