c ###########################
c ##                       ##
c ##  subroutine impdissx  ##
c ##                       ##
c ###########################
c
      subroutine impdissx (jdim,kdim,xyj,coef2,coef4,a,b,c,d,e)          
c
c*********************************************************************
c    this subroutine calculates the non-linear artificial dissipation
c coefficients and stores them at the proper block.
c*********************************************************************
c
#include "../include/arcom.inc"
c                                                                       
      dimension xyj(jdim,kdim)                                          
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension a(jdim,kdim,4),b(jdim,kdim,4,4),c(jdim,kdim,4,4),       
     >          d(jdim,kdim,4,4),e(jdim,kdim,4)                         
c                                                                       
c
c                                 
      dtd   = dt * thetadt / (1.+phidt)                                 
      smudt = smuim*dt* thetadt/(1. + phidt)
c                                                                       
c                                                                       
c---- limits ----
      jjl = jlow
      jju = jup
      if (.not.periodic) then                                           
        jjl = jlow+1                                                 
        jju = jup-1                                                  
      endif
c
c
      if (idmodel.ne.3) then
c
c     scalar dissipation
c     -- interior nodes --
         do 209 n =1,4
         do 210 j =jjl,jju                                             
            jp1 = jplus(j)                                                 
            jm1 = jminus(j)                                                
            jp2 = jplus(jp1)                                               
            jm2 = jminus(jm1)
            do 212 k =klow,kup                                         
               c2m = coef2(jm1,k) *dtd                                      
               c4m = coef4(jm1,k) *dtd                                      
               c2  = coef2(j,k)   *dtd                                       
               c4  = coef4(j,k)   *dtd                                       
               diag1 =      xyj(jm2,k)*c4m                                 
               a(j,k,n)   = diag1                                 
               diag2 =      xyj(jm1,k)*(c2m + 3.*c4m + c4)
               b(j,k,n,n) = b(j,k,n,n) - diag2                             
               diag3 =      xyj(j,k)  *(c2m+3.*c4m+c2+3.*c4)                 
               c(j,k,n,n) = c(j,k,n,n) + diag3                             
               diag4 =      xyj(jp1,k)*(c2 + 3.*c4 + c4m)
               d(j,k,n,n) = d(j,k,n,n) - diag4                             
               diag5 =      xyj(jp2,k)*c4                                  
               e(j,k,n)   = diag5                                 
 212        continue                                                    
 210     continue                                                      
 209     continue
c        
c        
c----    first interior node in j for non-periodic b.c. ----
         if (.not.periodic)then                                       
            do 219 n =1,4
            j = jlow                                                       
            jp1 = jplus(j)                                                 
            jm1 = jminus(j)                                                
            jp2 = jplus(jp1)                                               
            do 220 k =klow,kup                                         
               c2m = coef2(jm1,k)*dtd                                      
               c4m = coef4(jm1,k)*dtd                                      
               c2  = coef2(j,k)*dtd                                       
               c4  = coef4(j,k)*dtd                                       
               diag1 =       0.                                            
               a(j,k,n) = diag1                                 
               diag2 =      (c2m + c4m + c4)*xyj(jm1,k)                    
               b(j,k,n,n) = b(j,k,n,n) - diag2                             
               diag3 =      xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                 
               c(j,k,n,n) = c(j,k,n,n) + diag3                             
               diag4 =      (c2 + 3.*c4 + c4m)*xyj(jp1,k)                  
               d(j,k,n,n) = d(j,k,n,n) - diag4                             
               diag5 =      xyj(jp2,k)*c4                                  
               e(j,k,n) = diag5                                 
 220        continue                                                    
 219        continue
c        
c        
c----    last interior node in j for non-periodic b.c. ----
            do 221 n =1,4
            j = jup                                                        
            jp1 = jplus(j)                                                 
            jm1 = jminus(j)                                                
            jm2 = jminus(jm1)                                              
            do 222 k  =klow,kup                                         
               c2m = coef2(jm1,k)*dtd                                      
               c4m = coef4(jm1,k)*dtd                                      
               c2 = coef2(j,k)*dtd                                       
               c4 = coef4(j,k)*dtd                                       
               diag1 =      xyj(jm2,k)*c4m                                 
               a(j,k,n) = diag1                                 
               diag2 =      (c2m + 3.*c4m + c4)*xyj(jm1,k)                 
               b(j,k,n,n) = b(j,k,n,n) - diag2                             
               diag3 =      xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)                 
               c(j,k,n,n) = c(j,k,n,n) + diag3                             
               diag4 =      (c2 + c4 + c4m)*xyj(jp1,k)                     
               d(j,k,n,n) = d(j,k,n,n) - diag4
               diag5 =      0.                                             
               e(j,k,n) = diag5                                 
 222        continue                                                    
 221        continue
c                                                                          
         endif  
c
      else
c ***********************************************************************
c **                    Constant coefficient Diss.                     **
c ***********************************************************************
c constant coefficient 4th dissipation                                 
c                                                                       
         do 1212 k = klow,kup                                        
            do 1210 j = jjl,jju                                            
               jp1 = jplus(j)                                                 
               jm1 = jminus(j)                                                
               jp2 = jplus(jp1)                                               
               jm2 = jminus(jm1)                                              
               rjjj = smudt*coef4(j,k)                                     
               diag1 =       xyj(jm2,k)*rjjj                               
               a(j,k,1) = a(j,k,1) + diag1                                 
               a(j,k,2) = a(j,k,2) + diag1                                 
               a(j,k,3) = a(j,k,3) + diag1                                 
               a(j,k,4) = a(j,k,4) + diag1                                 
               diag2 =      4.*xyj(jm1,k)*rjjj                             
               b(j,k,1,1) = b(j,k,1,1) - diag2                             
               b(j,k,2,2) = b(j,k,2,2) - diag2                             
               b(j,k,3,3) = b(j,k,3,3) - diag2                             
               b(j,k,4,4) = b(j,k,4,4) - diag2                             
               diag3 =      6.*xyj(j,k)*rjjj                               
               c(j,k,1,1) = c(j,k,1,1) + diag3                             
               c(j,k,2,2) = c(j,k,2,2) + diag3                             
               c(j,k,3,3) = c(j,k,3,3) + diag3                             
               c(j,k,4,4) = c(j,k,4,4) + diag3                             
               diag4 =      4*xyj(jp1,k)*rjjj                              
               d(j,k,1,1) = d(j,k,1,1) - diag4                             
               d(j,k,2,2) = d(j,k,2,2) - diag4                             
               d(j,k,3,3) = d(j,k,3,3) - diag4                             
               d(j,k,4,4) = d(j,k,4,4) - diag4                             
               diag5 =      xyj(jp2,k)*rjjj                                
               e(j,k,1) = e(j,k,1) + diag5                                 
               e(j,k,2) = e(j,k,2) + diag5                                 
               e(j,k,3) = e(j,k,3) + diag5                                 
               e(j,k,4) = e(j,k,4) + diag5                                 
1210        continue                                                     
1212     continue                                                   
c                                                                       
         if(.not.periodic)then                                       
            j = jlow                                                       
            jp1 = jplus(j)                                                 
            jm1 = jminus(j)                                                
            jp2 = jplus(jp1)                                               
            do 1220 k = klow,kup                                        
               rjjj = smudt*coef4(j,k)                                     
               diag1 =       0.                                            
               a(j,k,1) = a(j,k,1) + diag1                                 
               a(j,k,2) = a(j,k,2) + diag1                                 
               a(j,k,3) = a(j,k,3) + diag1                                 
               a(j,k,4) = a(j,k,4) + diag1                                 
               diag2 =      rjjj*xyj(jm1,k)                                
               b(j,k,1,1) = b(j,k,1,1) - diag2                             
               b(j,k,2,2) = b(j,k,2,2) - diag2                             
               b(j,k,3,3) = b(j,k,3,3) - diag2                             
               b(j,k,4,4) = b(j,k,4,4) - diag2                             
               diag3 =      3.*xyj(j,k)*rjjj                               
               c(j,k,1,1) = c(j,k,1,1) + diag3                             
               c(j,k,2,2) = c(j,k,2,2) + diag3                             
               c(j,k,3,3) = c(j,k,3,3) + diag3                             
               c(j,k,4,4) = c(j,k,4,4) + diag3                             
               diag4 =      3.*rjjj*xyj(jp1,k)                             
               d(j,k,1,1) = d(j,k,1,1) - diag4                             
               d(j,k,2,2) = d(j,k,2,2) - diag4                             
               d(j,k,3,3) = d(j,k,3,3) - diag4                             
               d(j,k,4,4) = d(j,k,4,4) - diag4                             
               diag5 =      xyj(jp2,k)*rjjj                                
               e(j,k,1) = e(j,k,1) + diag5                                 
               e(j,k,2) = e(j,k,2) + diag5                                 
               e(j,k,3) = e(j,k,3) + diag5                                 
               e(j,k,4) = e(j,k,4) + diag5                                 
1220        continue                                                   
c                                                                       
            j = jup                                                        
            jp1 = jplus(j)                                                 
            jm1 = jminus(j)                                                
            jm2 = jminus(jm1)                                              
            do 1222 k = klow,kup                                        
               rjjj = smudt*coef4(j,k)                                     
               diag1 =       xyj(jm2,k)*rjjj                               
               a(j,k,1) = a(j,k,1) + diag1                                 
               a(j,k,2) = a(j,k,2) + diag1                                 
               a(j,k,3) = a(j,k,3) + diag1                                 
               a(j,k,4) = a(j,k,4) + diag1                                 
               diag2 =      3.*rjjj*xyj(jm1,k)                             
               b(j,k,1,1) = b(j,k,1,1) - diag2                             
               b(j,k,2,2) = b(j,k,2,2) - diag2                             
               b(j,k,3,3) = b(j,k,3,3) - diag2                             
               b(j,k,4,4) = b(j,k,4,4) - diag2                             
               diag3 =      3.*xyj(j,k)*rjjj                               
               c(j,k,1,1) = c(j,k,1,1) + diag3                             
               c(j,k,2,2) = c(j,k,2,2) + diag3                             
               c(j,k,3,3) = c(j,k,3,3) + diag3                             
               c(j,k,4,4) = c(j,k,4,4) + diag3                             
               diag4 =      3.*rjjj*xyj(jp1,k)                             
               d(j,k,1,1) = d(j,k,1,1) - diag4                             
               d(j,k,2,2) = d(j,k,2,2) - diag4                             
               d(j,k,3,3) = d(j,k,3,3) - diag4                             
               d(j,k,4,4) = d(j,k,4,4) - diag4                             
               diag5 =      0.                                             
               e(j,k,1) = e(j,k,1) + diag5                                 
               e(j,k,2) = e(j,k,2) + diag5                                 
               e(j,k,3) = e(j,k,3) + diag5                                 
               e(j,k,4) = e(j,k,4) + diag5                                 
1222        continue                                                   
         endif                                                       
      endif
      return                                                          
      end                                                             
