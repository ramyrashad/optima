#ifndef TAGSORT_H
#define TAGSORT_H

#include <vector>
#include <cmath>

using namespace std;

void tagSort (vector <int>& dataArr, vector <int>& tagArr, 
		     const int& size);

void tagSort(vector <int>& dataArr, int *tagArr, const int* size);

void tagSort(const int* dataArr, int *tagArr, const int& size);

void swap2 ( int* x, int* y );


#endif
