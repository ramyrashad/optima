c----------------------------------------------------------------------
c     -- subroutine to ouput target pressure distributions for inverse
c     design --
c----------------------------------------------------------------------
c     calling routine: ioall

      subroutine writecptar(nhalo, jdim, kdim,iside, press, x,
     &     xyj, flag, n_unit)

      implicit none

#include "../include/common.inc"
#include "../include/units.inc"

      integer nhalo,jdim,kdim,iside,n_unit,j,k
      double precision cpc,pp,cp
      double precision press(jdim+2*nhalo,kdim+2*nhalo)
      double precision x(jdim+2*nhalo,kdim+2*nhalo)
      double precision xyj(jdim+2*nhalo,kdim+2*nhalo)

      logical   flag

      cpc = 2.0/(gamma*fsmach*fsmach)

c     side # 1
      if (iside.eq.1) then
         k = nhalo + 1
         do j=nhalo+1,nhalo + jdim
            pp = press(j,k)*xyj(j,k)
            cp = (pp*gamma -1.0)*cpc
            if (flag) then
               write(n_unit,100) x(j,k), cp
            else
               write(n_unit,200) x(j,k), cp
            end if
         end do

c     side # 2
      elseif (iside.eq.2) then
         j = nhalo + 1
         do k=nhalo + kdim,nhalo + 1,-1
            pp = press(j,k)*xyj(j,k)
            cp = (pp*gamma -1.0)*cpc
            if (flag) then
               write(n_unit,100) x(j,k), cp
            else
               write(n_unit,200) x(j,k), cp
            end if
         end do

c     side # 3
      elseif (iside.eq.3) then
         k = nhalo + kdim
         do j=nhalo + jdim,nhalo + 1,-1
            pp = press(j,k)*xyj(j,k)
            cp = (pp*gamma -1.0)*cpc
            if (flag) then
               write(n_unit,100) x(j,k), cp
            else
               write(n_unit,200) x(j,k), cp
            end if
         end do

c     side # 4
      elseif (iside.eq.4) then
         j = nhalo + jdim
         do k=1 + nhalo,nhalo + kdim 
            pp = press(j,k)*xyj(j,k)
            cp = (pp*gamma -1.0)*cpc
            if (flag) then
               write(n_unit,100) x(j,k), cp
            else
               write(n_unit,200) x(j,k), cp
            end if
         end do
      endif

c     That's all folks!

 100  format(2e24.16)
 200  format(2e16.8)

      return
      end
