c *********************************************************************
c ** This routine automatically generates a list of blocks and sides **
c ** that define the individual elements.   The list defines the     **
c ** segments (i.e., blocks and sides) in such an order so as to     **
c ** define and traverse each element in a clockwise direction.      **
c *********************************************************************
c calling routine: ioall, master
c
c
      subroutine sortfoils(jdim,kdim,x,y)

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

      integer jdim,kdim,imaxblk,ifoil,iblk,iside,iseg,ii,isegment
      integer nlist,imaxseg,jkstart,jkend
      double precision x(jdim*kdim),y(jdim*kdim),xend,yend,xstart,ystart
      integer ilistblk(maxsegs,maxfoil),ilistside(maxsegs,maxfoil)

c
c     -zero data
      imaxblk = 0
      nfoils = 0
      do 10 ifoil=1,maxfoil
        nsegments(ifoil) = 0
 10   continue
c
c     -first find the number of elements or foils (nfoils)
      do 100 iblk=nblkstart,nblkend
      do 100 iside=1,4
        nfoils = max(nfoils,abs(ibcdir(iblk,iside)))
 100  continue
c
c     -find list of blocks and sides that make up each element
      do 225 ifoil =1,nfoils
        do 200 iblk=nblkstart,nblkend
        do 200 iside=1,4
          if (abs(ibcdir(iblk,iside)).eq.ifoil) then
            nsegments(ifoil) = nsegments(ifoil) + 1
            ilistblk(nsegments(ifoil),ifoil) = iblk
            ilistside(nsegments(ifoil),ifoil) = iside
          endif
 200    continue
c        write(*,*) ifoil,nsegments(ifoil)
 225  continue

c     -- Error Check: nfoils and nsegments --
      if (nfoils.gt.maxfoil) stop 'Error: nfoils > maxfoil'
      do ifoil=1,nfoils
         if (nsegments(ifoil).gt.maxsegs) 
     &        stop 'Error: nsegments > maxsegs'
      enddo

c
c
c     MAIN LOOP:
C     SORT the list starting with the largest block number
c
c
      do 900 ifoil=1,nfoils
        imaxseg = 1
        nlist = nsegments(ifoil)
c
c       -find the segment that has the largest block number
c
        do 300 iseg=1,nsegments(ifoil)
          if (ilistblk(iseg,ifoil).gt.ilistblk(imaxseg,ifoil)) then
            imaxseg = iseg
          endif
  300   continue
c
c       -set segment with largest block number as first entry in
c        list of segments.
c
        iblk = ilistblk(imaxseg,ifoil)
        isegblk(1,ifoil) = iblk
        iside = ilistside(imaxseg,ifoil)
        isegside(1,ifoil) = iside
        isegment = 1
c
c       -set starting (x,y) point and ending (x,y) point on 1st segment
        call getbounds(iblk,iside,jkstart,jkend)

        if (iside.eq.2) then
c         -blunt trailing edge ... set xstart and ystart as
c          lower left corner of block
c         -note: original TORNADO did this internally in getbounds
          xstart = x(jkend)
          ystart = y(jkend)
          xend = x(jkstart)
          yend = y(jkstart)
        else
          xstart = x(jkstart)
          ystart = y(jkstart)
          xend = x(jkend)
          yend = y(jkend)
        endif

c       -remove this segment from the unsorted list of segments
        do 400 iseg=imaxseg+1,nsegments(ifoil)
          ilistblk(iseg-1,ifoil) = ilistblk(iseg,ifoil)
          ilistside(iseg-1,ifoil) = ilistside(iseg,ifoil)

  400   continue
        nlist = nlist - 1
c
c       Loop through unsorted entries and add segments to sorted list
c       as they connect
c
  410   continue
c
        do 500 iseg=1,nlist
          call getbounds(ilistblk(iseg,ifoil),ilistside(iseg,ifoil)
     &          ,jkstart,jkend)


c         Add segment to sorted segment list if it connects
          if (   ( abs(x(jkstart)-xend).lt.1.0E-07.and.
     &             abs(y(jkstart)-yend).lt.1.0E-07 ) .or. 
     &       ( ilistside(iseg,ifoil).eq.2.and.abs(x(jkend)-xend)
     &         .lt.1.0E-07.and.abs(y(jkend)-yend).lt.1.0E-07 )  
     &         ) then
            isegment = isegment + 1
            isegblk(isegment,ifoil) = ilistblk(iseg,ifoil)
            isegside(isegment,ifoil) = ilistside(iseg,ifoil)
            if (ilistside(iseg,ifoil).ne.2) then
               xend = x(jkend)
               yend = y(jkend)
            else
               xend = x(jkstart)
               yend = y(jkstart)
            end if

c           -remove connecting segment from unsorted segment list
            do 450 ii = iseg+1,nlist
              ilistblk(ii-1,ifoil) = ilistblk(ii,ifoil)
              ilistside(ii-1,ifoil) = ilistside(ii,ifoil)
 450        continue
            nlist = nlist - 1
c     
            if (nlist.gt.0) goto 410
          endif
 500    continue

c        write(n_out,*) 'ifoil=',ifoil
c        do 550 iseg=1,nsegments(ifoil)
c           write(n_out,*) 'block,side', isegblk(iseg,ifoil),
c     &          isegside(iseg,ifoil)
c 550    continue
c        write(n_out,*) 
c
c Check for errors
c
        if (nlist.ne.0) write(*,*)'ERROR: nlist>0, foil=',ifoil
        if ((xend.ne.xstart).or.(yend.ne.ystart)) write(*,*)
     &    'ERROR: start & ending coordinates not equal, ifoil='
     &    ,ifoil, xstart, xend, ystart, yend

c     -- Error check --
        if (nlist.ne.0) then
           write(*,*) ''
           write(*,*) 'Check the parameters maxj & maxk in parms.inc.'
           write(*,*) 'They define the number of points on the airfoil'
           write(*,*) 'and may need to be increased'
        endif


c That's it for now
c
  900 continue
c
      return
      end
