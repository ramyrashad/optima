!
! ===================================================================
!
module CommonH
!
! -------------------------------------------------------------------
!
! Purpose:
!    Defines global constants and variables
!
! Author:
!    Oleg Chernukhin, May 2010
!
! -------------------------------------------------------------------

use mpi

implicit none

! __________________________
! -- Default variable kinds
   integer, parameter :: int8 = selected_int_kind(8)
   integer, parameter :: dp = kind(1.d0)

! __________________________________
! -- Common constants and variables
   real(kind=dp) :: infBnd = 1.1d20
   integer :: ierr, fsn_base
   integer :: ind_feval, tot_feval
   logical :: useDatabase

! ___________________
! -- MPI information
   integer :: nproc_glob, nproc_loc
   integer :: myrank_glob, myrank_loc
   integer :: mycid
   integer :: ppc, popsize
   integer :: HYBROPT_COMM
   integer, allocatable :: comm(:)

! __________________________________
! -- Common optimization parameters
   integer :: hybropt_method, nX, nFnc, nnzLinG, &
              nnzGrad, iPrintC, iSummC, opt_output
   real(kind=dp) :: penalty
   integer, allocatable :: iLfun(:), jLvar(:), iGfun(:), jGvar(:)
   real(kind=dp), allocatable :: LinG(:), Xlow(:), Xupp(:), Xmul(:), &
                                 Flow(:), Fupp(:), Fmul(:)
   character(len=128) :: info_file, hist_file

! _____________________________________________
! -- Variables for hm load balancing mechanism
   logical,allocatable :: donebuffer(:), doneflag(:)
   integer,allocatable :: done_recv_req(:)
   logical :: termbuffer, decision_term, donemsg, termflag
   integer :: done_tag, term_tag, term_recv_req
   integer :: statmpi_array(MPI_STATUS_SIZE)
   real(kind=dp) :: load_bal_tol

! ___________________________________
! -- Interface to Optima2D/Jetstream
! -- Must match husrfun interface (see optimize subroutine)
   interface
      subroutine call2solver(dv, nvar, f, nF, G, lenG, comm, csz,cid,&
                             fsn_base, stat, needF, needG)
         integer :: nvar, nF, lenG, csz, cid, fsn_base, stat
         logical :: needF, needG
         real(kind(1.d0)) :: dv(nvar), f(nF), G(lenG)
         integer :: comm(csz)
      end subroutine
   end interface

! ______________________________________________________________
! -- Procedure pointer: only works on ifort compiler v11 and up
   procedure(call2solver), pointer :: hu_ptr

! ____________________
! module subroutines
contains
!
! =====================================================================
!
   subroutine hm_load_bal_decide(flag_arr, dec_flag)
!
! ---------------------------------------------------------------------
!
! Purpose:
!    Decides whether to tell the population members that are still
!    doing gradient-based iterations to quit and move on to the next
!    generation. Done for load balancing, only for the hybrid method
!
! ---------------------------------------------------------------------
! _______________________
! variable specification
!

   !-- subroutine arguments
   logical :: flag_arr(popsize)
   logical,intent(out) :: dec_flag

   !-- local variables
   integer :: ii, truecount
   real(kind=dp) :: complete_frac

! _____________________
! begin main execution

   truecount = 0
   do ii = 1, size(flag_arr)
      if (flag_arr(ii)) truecount = truecount + 1
   end do

   complete_frac = dble(truecount) / dble(popsize)

   if (complete_frac >= load_bal_tol) then 
      dec_flag = .true.
   else
      dec_flag = .false.
   endif

   return

end subroutine
!
! ===================================================================
!
end module

