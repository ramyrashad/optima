#include <cmath>
#include <vector>
#include "comprow_double.h"
#include "ilupre_double.h"
#include "cg.h"
#include "Node.h"
#include "grid.h"

#define tolerance 1e-7       // CG tolerance parameter

using namespace std;

extern "C" {
void movegrid_(char *filena, const int& namelen, const int &jtail1, 
	       const int& jtail2, double* x, double* y, const double* by);
}


void movegrid_(char *filena, const int& namelen, const int &jtail1, 
	       const int& jtail2, double* x, double* y, const double* by)
{
  int j, maxit, temp;
  char *filename = new char[namelen+1];
  for (j=0; j<namelen; j++)
    filename[j] = filena[j];
  filename[namelen] = '\0';

	// declare and initialize objects
	vector <Node> node_loc;
	vector <Node*> bnd_nodes(jtail2-jtail1+1); 
	
	// CONJUGATE GRADIENT METHOD CONVERGENCE PARAMETERS
	
	//cout << "Reading in grid ... " << endl;
	read_grid (filename, node_loc, bnd_nodes, jtail1, jtail2);
	//  ofstream fout("grid_out.dat");
	// set new y-coordinates
	
	//cout << "Reading in deformed boundary ... " << endl;
	temp = (int) bnd_nodes.size();
	for (j=0; j<temp; j++)
	  bnd_nodes[j]->set_new_y(by[j]);

	
	// VARIABLES IN SOLVING MATRIX
	// counts the number of nonzeros in the stiffness matrix	
	int num_inn_nodes(0), nonzeros (0);  
	get_num_nonzeros (node_loc, num_inn_nodes, nonzeros);

	double *stiff_val = new double [nonzeros];
	int *colind = new int [nonzeros];
	int *rowptr = new int [num_inn_nodes+1];
	
	// Solution, RHS vectors
	VECTOR_double deltay(num_inn_nodes, 0.0), deltax(num_inn_nodes,0.0),
		Fx(num_inn_nodes,0.0), Fy(num_inn_nodes,0.0);     
	
	//cout << "Forming stiffness matrix ... " << endl;
	calc_stiffness(node_loc, stiff_val, Fx, Fy, colind, rowptr);

	// ***** START SOLVING ... *****
	
	CompRow_Mat_double alpha(num_inn_nodes, num_inn_nodes,
				 nonzeros, stiff_val, rowptr, colind);
	CompRow_ILUPreconditioner_double P(alpha);     // ILU preconditioner
	
	// Solve using the conjugate gradient method
	double tol = tolerance;
	maxit = 1000; 
	
	//cout << "Solving for new nodal coordinates ... " << endl;
	temp = CG(alpha, deltay, Fy, P, maxit, tol);  
	//cout<< "CG flag:  " << num_iter << "     ";
	//cout<< "Iterations performed:  " << maxit << "     ";
	//cout<< "Tolerance achieved:  " << tol << endl;
	if (temp == 1)
	  cerr << "Convergence was not achieved with given " 
	       << "number of iterations " << endl;

	tol = tolerance;	
	temp = CG(alpha, deltax, Fx, P, maxit, tol);
	//cout<< "CG flag:  " << num_iter << "     ";
	//cout<< "Iterations performed:  " << maxit << "     ";
	//cout<< "Tolerance achieved:  " << tol << endl;	
	if (temp == 1)
	  cerr << "Convergence was not achieved with given " 
	       << "number of iterations " << endl;
	
	//cout << "Updating nodal locations ... " << endl;
	update (node_loc, deltax, deltay);

	delete [] stiff_val;  stiff_val = NULL;      
	delete [] colind;     colind = NULL;
	delete [] rowptr;     rowptr = NULL;
	delete [] filename;   filename = NULL;

	write_grid (node_loc, x, y);

	// clear up memory
	temp = (int) node_loc.size();
	for (j=0; j<temp; j++)
	  node_loc[j].clearmem();

	//cout << "Exitting move_grid function ... " << endl; 
}




