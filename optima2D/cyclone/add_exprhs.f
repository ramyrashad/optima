      subroutine add_exprhs(jdim,kdim,s,exprhs)
 
#include "../include/arcom.inc"

      dimension s(jdim,kdim,4)
      double precision exprhs(jdim,kdim,4)

      do 500 n=1,4
      do 500 k=klow,kup
      do 500 j=jlow,jup
         s(j,k,n)=s(j,k,n)+exprhs(j,k,n)
 500  continue

      return
      end
