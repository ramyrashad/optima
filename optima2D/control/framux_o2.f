c-----------------------------------------------------------------------
c     -- framux_o2: second-order accurate Frechet matrix A multiply
c     vector X. This subroutine evaluates a matrix-vector product
c     using centered differences: v2 = A*v1 --
c     
c     Author : A. Pueyo, Mods: M. Nemec
c     Date: Dec. 1995, Mar. 2001
c-----------------------------------------------------------------------
      subroutine framux_o2 (na, jdim, kdim, ndim, indx, iex, v1, v2, qr,
     &      xy, xyj, x, y, precon, coef2, coef4,uu, vv, ccx, ccy, ds,
     &      press, tmet, spect, gam, turmu, fmu, vort, sndsp, sm, bcf,
     &      q, sp, ifirst, as, work, rhsnamult)
c     
#include "../include/arcom.inc"
      
      integer indx(jdim,kdim), iex(jdim,kdim,4)

clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)

      double precision eps, v1(na), v2(na), v1_n, eps1
      double precision qr(jdim,kdim,ndim), sp(jdim,kdim,ndim)
      double precision q(jdim,kdim,ndim), sm(jdim,kdim,ndim)

      double precision bcf(jdim,kdim,4), xy(jdim,kdim,4)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)

      double precision press(jdim,kdim), sndsp(jdim,kdim)

      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)
      double precision work(jdim,kdim,4)

      double precision rhsnamult,rhsnamult2, alphatemp

      logical imps, jac_tmp, prec_tmp, scalena

      write(*,*)'framux_o2 was called which is not supported'
      write(*,*) 'Somebody should fix this. Program will stop for now.'
      stop

      jac_tmp = jac_mat
      prec_tmp = prec_mat

      jac_mat = .false.
      prec_mat = .false.

      alphatemp=alpha

      if (ifirst.eq.1) then
        do j = 1,na
          v2(j) = 0.d0
        end do
      else
        v1_n = 0.d0
        do i = 1,na
          v1_n = v1_n + v1(i)**2
        end do
        v1_n = dsqrt(v1_n)
        eps  = dsqrt(2.2d-16) / v1_n
c        eps  = 1.d-7 / v1_n
        eps1 = 0.5d0/eps
        
c     -- evaluate vector --
        do n = 1,ndim
          do k = 1,kmax
            do j = 1,jmax
              jk = ( indx(j,k)-1 )*ndim + n
              q(j,k,n) = qr(j,k,n) + eps*v1(jk)
              sp(j,k,n) = 0.d0
              sm(j,k,n) = 0.d0
            end do
          end do
        end do

        if (clalpha2 .or. clopt) then 
            alpha = alphatemp + eps*v1(na)!/1.d2
            uinf = fsmach*cos(alpha*pi/180.d0)
            vinf = fsmach*sin(alpha*pi/180.d0)
         end if

         call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- function evaluation --
        call get_rhs (jdim, kdim, ndim, q, xy, xyj, x, y, precon,
     &        coef2, coef4, uu, vv, ccx, ccy, ds, press, tmet, spect,
     &        gam, turmu, fmu, vort, sndsp, sp, work)

c        imps = .false.
c
c        do n = 1,4
c          do k = kbegin,kend
c            do j = jbegin,jend          
c              iex(j,k,n) = n
c            end do
c          end do
c        end do        
c
c        call impbc (jdim, kdim, ndim, indx, icol, iex, q, xy, xyj, x, y,
c     &        as, as, imps, bcf, press)
        scalena = .false.
        call scale_bc (1, jdim, kdim, ndim, indx, iex, ia, ja, as, ipa,
     &        jpa, as, sp, bcf, as, rhsnamult2, scalena)
        

clb   -- Adding equation for cl due to variable angle of attack logic --
         if (clalpha2 .or. clopt) then
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 0,
     &           cli, cdi, cmi, clv, cdv, cmv) 
            clt = cli + clv
            cleqp=-(clt-clinput)*rhsnamult
         end if

c     -- evaluate vector --
        do n = 1,ndim
          do k = 1,kmax
            do j = 1,jmax
              jk = ( indx(j,k)-1 )*ndim + n
              q(j,k,n) = qr(j,k,n) - eps*v1(jk)
            end do
          end do
        end do

        if (clalpha2 .or. clopt) then 
            alpha = alphatemp - eps*v1(na)!/1.d2
            uinf = fsmach*cos(alpha*pi/180.d0)
            vinf = fsmach*sin(alpha*pi/180.d0)
         end if

        call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

c     -- function evaluation --
        call get_rhs (jdim, kdim, ndim, q, xy, xyj, x, y, precon,
     &        coef2, coef4, uu, vv, ccx, ccy, ds, press, tmet, spect,
     &        gam, turmu, fmu, vort, sndsp, sm, work)

c        imps = .false.
c
c        do n = 1,4
c          do k = kbegin,kend
c            do j = jbegin,jend          
c              iex(j,k,n) = n
c            end do
c          end do
c        end do        
c
c        call impbc (jdim, kdim, ndim, indx, icol, iex, q, xy, xyj, x, y,
c     &        as, as, imps, bcf, press)
        scalena = .false.
        call scale_bc (1, jdim, kdim, ndim, indx, iex, ia, ja, as, ipa,
     &        jpa, as, sm, bcf, as, rhsnamult2,scalena)

clb   -- Adding equation for cl due to variable angle of attack logic --
         if (clalpha2 .or. clopt) then
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, 0,
     &           cli, cdi, cmi, clv, cdv, cmv) 
            clt = cli + clv
            cleqm=-(clt-clinput)*rhsnamult
         end if

c     -- evaluate difference: S = -R(Q); reverse the sign of s-sr --
        do n = 1,ndim
          do k = 1,kmax
            do j = 1,jmax
              jk = ( indx(j,k)-1 )*ndim + n
              v2(jk) = -eps1 * (sp(j,k,n) - sm(j,k,n))
            end do
          end do
        end do
      end if

      if (clalpha2) then
         v2(na) = - eps1 * (cleqp - cleqm)

         alpha = alphatemp
         uinf = fsmach*cos(alpha*pi/180.d0)
         vinf = fsmach*sin(alpha*pi/180.d0)
      end if
      
      jac_mat = jac_tmp
      prec_mat = prec_tmp

      return
      end                       !framux_o2
