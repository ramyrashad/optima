      subroutine frechetq(ib,jmax,kmax,nmax,q,turre,indx,
     &     q_temp,wk_temp1,wk_temp2,js,je,ks,ke,xyj)

      implicit none

      integer j,k,n,jk,js,je,ks,ke,ib,jmax,kmax,nmax,jkp1
      integer indx(jmax,kmax)

      double precision q(jmax,kmax,nmax)
      double precision turre(jmax,kmax),xyj(jmax,kmax),a
      double precision q_temp(*),wk_temp1(*),wk_temp2(*)

      do n = 1,4
         do k = ks,ke
            do j = js,je
               jk = ( indx(j,k)-1)*nmax + n
               if (abs(q_temp(jk)) .ge. 5.d-2  .and. 
     &              abs(wk_temp1(jk)-wk_temp2(jk)).gt.1.d-4 ) then 

               write(*,10)ib,j-2,k-2,n,q_temp(jk),
     &              wk_temp1(jk)-wk_temp2(jk),wk_temp1(jk),wk_temp2(jk)
                    q(j,k,n)   = q_temp(jk)

               endif
            enddo
         enddo
      enddo

      if (nmax.eq.5) then 
         n = 5
         do k = ks,ke
            do j = js,je
               jk = ( indx(j,k)-1)*nmax + n

               if (abs(q_temp(jk)) .ge. 5.d-2  .and. 
     &              abs(wk_temp1(jk)-wk_temp2(jk)).gt.1.d-4 ) then

               write(*,10)ib,j-2,k-2,n,q_temp(jk),
     &             wk_temp1(jk)-wk_temp2(jk),wk_temp1(jk),wk_temp2(jk) 

                  turre(j,k)=q_temp(jk)
              
               endif
            enddo
         enddo
      endif

 10   format(1x,i3,1x,i3,1x,i3,1x,i3,4x,e16.8,e16.8,e16.8,e16.8)
 
      return                     
      end                       !frechetq
