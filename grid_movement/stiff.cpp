#ifndef STIFF_H
#include "stiff.h"
#endif

/*
  Purpose:  form the element stiffness matrix
  Element type:  quadrilateral
*/


void get_K (const vector<double*>& vertices, 
	    double** K, const double& elemQo, double& meshQ)
{
  int i;
  double Q ;   // Q = distortion measurw = elemQ/elemQo
  double area;  // area of quad
 
  if (meshQ == -1) // 1st iteration
    Q = 1;
  else
    {
      get_Q (vertices, Q);  
      if (Q > meshQ)
	meshQ = Q;
      Q = pow(Q/elemQo, PowerLaw);
    }


  // Young's modulus = 1/(area of quad)
  area_quad (vertices, area);
  // Now calculate Young's modulus and store in area
  area = Q/( area * (1.0+poisson)*(1.0-2.0*poisson) );
  // set up material matrix, assume plane strain
  i = 3;
  vector < vector <double> > D (i, vector<double> (i,0.0) );    
  D[0][0] = area*(1.0-poisson);
  D[0][1] = area*poisson;
  D[1][0] = D[0][1];
  D[1][1] = D[0][0];
  D[2][2] = area*(0.5-poisson);
    
  
  // form dN at each integration (Gauss quadrature) point, 
  // dNz_(1..4), dNh_(1..4)
  vector<vector<double> > dNa(2, vector<double>(4,0.0));   
  vector<vector<double> > dNb(2, vector<double>(4,0.0));   
  vector<vector<double> > dNc(2, vector<double>(4,0.0));   
  vector<vector<double> > dNd(2, vector<double>(4,0.0));   
  form_dN (dNa, dNb, dNc, dNd);

  double *J = new double[4];   // Jacobian matrix
  double detJ (0);                // determinant of Jacobian
 

  // calculate the integral values at points a, b, c ,d 
  vector < vector <double> > B (3, vector<double> (8,0.0) );    

  // initialize the elements in stiffness matrix
  for (i=0; i<8; i++)    
    for (int j=0; j<8; j++)    
      K[i][j] = 0.0;  
  

  // cout << "call 1st B " << endl;
  calc_B (vertices, dNa, J, B, detJ);
  calc_I (B, D, K, detJ);
  
  //  cout << "call 2nd B " << endl;
  calc_B (vertices, dNb, J, B, detJ);
  calc_I (B, D, K, detJ);

  // cout << "call 3rd B " << endl;
  calc_B (vertices, dNc, J, B, detJ);
  calc_I (B, D, K, detJ);
   
  //cout << "call 4th B " << endl;
  calc_B (vertices, dNd, J, B, detJ);
  calc_I (B, D, K, detJ);
 
  delete [] J;

  return;
}


void form_dN ( vector <vector<double> >& dNa,  vector <vector<double> >& dNb, 
	       vector <vector <double> >& dNc,  vector <vector <double> >& dNd)
{
  dNa[0][0] = -0.25*(Gauss+1);        dNa[0][1] = -dNa[0][0];
  dNa[0][2] = 0.25*(1-Gauss);         dNa[0][3] = -dNa[0][2];
  dNa[1][0] = dNa[0][0];              dNa[1][1] = dNa[0][3];
  dNa[1][2] = dNa[0][2];              dNa[1][3] = dNa[0][1];
  
  dNb[0][0] = dNa[0][0];     dNb[0][1] = dNa[0][1];
  dNb[0][2] = dNa[0][2];     dNb[0][3] = dNa[0][3];
  dNb[1][0] = dNa[0][3];     dNb[1][1] = dNa[0][0];
  dNb[1][2] = dNa[0][1];     dNb[1][3] = dNa[0][2];

  dNc[0][0] = dNa[0][3];     dNc[0][1] = dNa[0][2];
  dNc[0][2] = dNa[0][1];     dNc[0][3] = dNa[0][0];
  dNc[1][0] = dNa[0][3];     dNc[1][1] = dNa[0][0];
  dNc[1][2] = dNa[0][1];     dNc[1][3] = dNa[0][2];

  dNd[0][0] = dNa[0][3];     dNd[0][1] = dNa[0][2];
  dNd[0][2] = dNa[0][1];     dNd[0][3] = dNa[0][0];
  dNd[1][0] = dNa[0][0];     dNd[1][1] = dNa[0][3];
  dNd[1][2] = dNa[0][2];     dNd[1][3] = dNa[0][1];

  return;
}

void calc_B (const vector <double*>& vertices, 
	     const vector <vector <double> >& dN, 
	     double* J, vector < vector <double> >& B, double& detJ)
{
  int i, j , k, m=0;

  // determine the Jacobian matrix
  for (i=0; i<2; i++)
    {
      for (j=0; j<2; j++)     
	{
	  J[m] = 0.0;
	  for (k=0; k<4; k++) 
	    J[m] += dN[i][k] * vertices[k][j]; 
	  m++;
	}
    }
	   
  detJ =  J[0]*J[3] - J[1]*J[2];
  if (detJ < 0.0)
    {
      cerr << "negative Jacobian encountered" << endl;
      exit (9);
    }

  double * invJ  = new double [4];
  invJ[0] = J[3]/detJ;        invJ[1] = -J[1]/detJ;   
  invJ[2] = -J[2]/detJ;       invJ[3] = J[0]/detJ;

  B[0][0] = invJ[0]*dN[0][0] + invJ[1]*dN[1][0];
  B[0][2] = invJ[0]*dN[0][1] + invJ[1]*dN[1][1];
  B[0][4] = invJ[0]*dN[0][2] + invJ[1]*dN[1][2];
  B[0][6] = invJ[0]*dN[0][3] + invJ[1]*dN[1][3];

  B[1][1] = invJ[2]*dN[0][0] + invJ[3]*dN[1][0];
  B[1][3] = invJ[2]*dN[0][1] + invJ[3]*dN[1][1];
  B[1][5] = invJ[2]*dN[0][2] + invJ[3]*dN[1][2];
  B[1][7] = invJ[2]*dN[0][3] + invJ[3]*dN[1][3];

  B[2][0] = B[1][1];          B[2][1] = B[0][0];
  B[2][2] = B[1][3];          B[2][3] = B[0][2];
  B[2][4] = B[1][5];          B[2][5] = B[0][4];
  B[2][6] = B[1][7];          B[2][7] = B[0][6];

  delete [] invJ;   invJ = NULL;

  return;
}


void calc_I (const vector < vector <double> >& B, 
	     const vector < vector <double> >& D, 
	     double** I, const double& detJ)
{
 
  vector < vector <double> > tempI (8, vector <double> (3, 0.0));
  int i, j, k;   // counter
  double Kij;

  // calculate tempI = transpose(B) * D
  for (i=0; i<8; i++) 
    for (j=0; j<3; j++) 
      for (k=0; k<3; k++) 
	tempI[i][j] += B[k][i]*D[k][j]; 

  // calculate I = tempI*B
  for (i=0; i<8; i++)    
    {
      for (j=0; j<8; j++)    
	{
	  Kij = 0.0;
	  for (k=0; k<3; k++)   
	    Kij += tempI[i][k]*B[k][j]; 
	  I[i][j] +=  Kij * detJ;
	}
    }
  
  // cout << "  end " << endl;

  return;

}


void get_Q (const vector<double*>& vertices, double& Q)
{

  //  Q (element quality) is calculated using norm-2 

 
  /*   3         c         2
       888888888888888888888
       8              /    8
       8          /        8
       d  8       /  q        8  b
       8    /              8
       8 /                 8
       888888888888888888888
       0       a           1

       if a, b, c, d are vectors and a+b+c+d = 0
       p = b+c   and  q = a+b
  */
  
  // length of edges & diagonals of quadrilateral
  double* len = new double [6];    
  for (int i=0; i<6; i++)
    len[i] = 0.0;

  calc_lengths(vertices, len);  //a

  //cout << "in get_Q, after calac_llength " << endl;
  double area = 0.0;   
  
  // calc area of T013
  area_tri (len[5], len[0], len[3], area);
  

  area = 0.125*(len[0]+len[3]+len[5])*len[0]*len[3]*len[5]/(area*area);
  Q = area*area;
  
  // calc area of T123
  area_tri (len[5], len[2], len[1], area);
  area = 0.125*(len[5]+len[1]+len[2])*len[2]*len[1]*len[5]/(area*area);
  Q += area*area;
  
  // calc area of T012
  area_tri (len[4], len[0], len[1], area);
  area = 0.125*(len[0]+len[1]+len[4])*len[0]*len[1]*len[4]/(area*area);
  Q += area*area;
  
  // calc area of T023
  area_tri (len[4], len[2], len[3], area);
  area = 0.125*(len[2]+len[3]+len[4])*len[2]*len[3]*len[4]/(area*area);
  Q += area*area;
  
  Q = sqrt(Q);

  delete [] len;  len = NULL;
  return;
}


// calculates the area of a triangle given the lengths of 3 sides
void area_tri (double& a, double& b, double& c, double& area) 
{

  double **len = new double* [3];  // lengths array
  // sort the lengths of the triangle
  if (a>b)  {
    if (b>c)  {
      len[0]=&a;    len[1]=&b;     len[2]=&c; }
    else if (a>c)  {
      len[0]=&a; 	  len[1]=&c;     len[2]=&b; }
    else {
      len[0]=&c;    len[1]=&a;     len[2]=&b; }  } 
  else  {
    if (a>c)  {
      len[0]=&b;	  len[1]=&a;     len[2]=&c; }
    else if (b>c)  {
      len[0]=&b; 	  len[1]=&c;     len[2]=&a; }
    else {
      len[0]=&c;    len[1]=&b;     len[2]=&a; }  } 
  
  // calculate the area using Kahan's formula
  if ( ( *len[2]-(*len[0] - *len[1]) ) < 0.0 )
    cerr << "Data are not side-lengths of a real triangle!!" << endl;
  else
    {
      area = 0.25*sqrt( ( *len[0] + (*len[1] + *len[2]) )*
			( *len[2] - (*len[0] - *len[1]) )*
			( *len[2] + (*len[0] - *len[1]) )*
			( *len[0] + (*len[1] - *len[2]) ) );
    }
  
  delete [] len;   len = NULL;

  return;
}


void calc_lengths (const vector<double*>& vertices, double* len)
{
 
  double dx, dy;
  dx = vertices[0][0] - vertices[1][0];  
  dy = vertices[0][1] - vertices[1][1];
  len[0] = sqrt( dx*dx + dy*dy );

  dx = vertices[1][0] - vertices[2][0] ;  
  dy = vertices[1][1] - vertices[2][1];
  len[1] = sqrt( dx*dx + dy*dy );

  dx = vertices[2][0] - vertices[3][0] ;  
  dy = vertices[2][1] - vertices[3][1];
  len[2] = sqrt( dx*dx + dy*dy );

  dx = vertices[3][0] - vertices[0][0] ;  
  dy = vertices[3][1] - vertices[0][1];
  len[3] = sqrt( dx*dx + dy*dy );

  dx = vertices[0][0] - vertices[2][0] ;  
  dy = vertices[0][1] - vertices[2][1];
  len[4] = sqrt( dx*dx + dy*dy );
  
  dx = vertices[1][0] - vertices[3][0] ;  
  dy = vertices[1][1] - vertices[3][1];
  len[5] = sqrt( dx*dx + dy*dy );

  return;
  
}


// calculates the area of a quadrilateral by cross products of the 
// sides
// The corners should be specified in counterclockwise order to get
// positive area
void area_quad (const vector<double*>& vertices, double& a)
{

  a = 0.5 * ( (vertices[2][0] - vertices[0][0]) * 
		 (vertices[3][1] - vertices[1][1]) -
		 (vertices[3][0] - vertices[1][0]) *
		 (vertices[2][1] - vertices[0][1]) );
  return;
}
