c ##########################
c ##                      ##
c ##  subroutine filterx  ##
c ##                      ##
c ##########################
c
      subroutine filterx3 (jdim,kdim,q,s,xyj,coef2,coef4,sndsp,work,
     >                    precon,tmp)            
c
c******************************************************************
c   this subroutine follows very faithfully t. pulliam notes, p.31
c
c   fourth order smoothing, added explicitly to rhs                     
c   second order near shocks with pressure grd coeff.                   
c                                                                       
c  xi direction                                                         
c                                                                       
c   start differences each variable separately                          
c******************************************************************
c                                                                       
      include '../include/arcom.inc'
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)          
      dimension work(jdim,kdim,3),precon(jdim,kdim,6),tmp(jdim,kdim,4)          
      dimension tmp2(maxj,maxk,4)
      do 15 n = 1,4                                                     
c
c
c---- 1st-order forward difference ----
      do 16 k =klow,kup                                                
      do 16 j =jlow,jup                                                
         work(j,k,1) = q(j+1,k,n)*xyj(j+1,k) - q(j,k,n)*xyj(j,k)           
 16   continue                                                          
c  -- c mesh bc --
      if (.not.periodic) then                                          
         j1 = jbegin                                                    
         j2 = jend                                                      
         do 76 k = klow,kup                                             
            work(j1,k,1) = q(j1+1,k,n)*xyj(j1+1,k) -                       
     >                     q(j1,k,n)*xyj(j1,k)                           
            work(j2,k,1) = work(j2-1,k,1)                                  
 76      continue                                                       
      endif                                                          
c
c
c---- apply cent-dif to 1st-order forward ----
      do 17 k =klow,kup                                                
      do 17 j =jlow,jup                                                
         work(j,k,2) = work(j+1,k,1)-2.*work(j,k,1)+work(j-1,k,1)     
 17   continue                                                          
c  -- c mesh bc --
      if (.not.periodic) then                                          
         j1 = jbegin                                                    
         j2 = jend                                                      
         do 77 k =klow,kup                                             
            work(j1,k,2) = q(j1+2,k,n)*xyj(j1+2,k) -                       
     >           2.*q(j1+1,k,n)*xyj(j1+1,k) + q(j1,k,n)*xyj(j1,k)         
            work(j2,k,2) = 0.                                              
 77      continue                                                       
      endif                                                          
c
c
c---- form dissipation term before last differenciation ----
      do 19 k =klow,kup                                                
      do 19 j =jlow,jup                                                
         tmp(j,k,n) = (coef2(j,k)*work(j,k,1)-coef4(j,k)*work(j,k,2))   
         tmp2(j,k,n) = tmp(j,k,n)
 19   continue                                                          
c  -- c mesh bc --
      if (.not.periodic) then                                          
         j1 = jbegin                                                    
         j2 = jend                                                      
         do 79 k = klow,kup                                             
         tmp(j1,k,n) = (coef2(j1,k)*work(j1,k,1) -                     
     >                   coef4(j1,k)*work(j1,k,2))                      
         tmp(j2,k,n) = 0.                                              
         tmp2(j1,k,n) = tmp(j1,k,n)
         tmp2(j2,k,n) = tmp(j2,k,n)
79       continue                                                       
         endif                                                          
c
c
 15   continue                                                          
c
cdu   precondition the dissipation operator
c                                                          
      dtd = dt / (1. + phidt)                                           
      if (prec.gt.0) then
         call predis2(jdim,kdim,0,0,q,xyj,sndsp,precon,tmp)
         do 90 n=1,4
         do 90 k=klow,kup
         do 90 j=jlow,jup
           s(j,k,n)=s(j,k,n)+(tmp(j,k,n) - tmp(j-1,k,n))*dtd
 90      continue
         do 100 n=1,4
         do 100 k=kbegin,kend
         do 100 j=jbegin,jend       
            tmp(j,k,n)=tmp2(j,k,n)
 100     continue
         call predis2(jdim,kdim,1,0,q,xyj,sndsp,precon,tmp)
      endif
c
c
c
c---- last differenciation and add in dissipation ----
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
           s(j,k,n)=s(j,k,n)+(tmp(j,k,n) - tmp(j-1,k,n))*dtd
 200  continue
c
      return                                                            
      end                                                               
