c     -- parameters for b-splines --
c     -- maximum number of b-spline segments = > maxbseg --
      integer ibsnc, ibskt, ibody, maxbseg
c     -- max. number of knots -> ibskt = ibsord + ibsnc --
      parameter (ibody=500, ibsnc=120, ibskt=145, maxbseg=12)
