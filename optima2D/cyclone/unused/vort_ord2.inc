      do k=klow,kup
         kp1=k+1
         km1=k-1
         do j=jlow,jup
           jp1=j+1
           jm1=j-1
           ujm1=workq(jm1,k,2)
           ujp1=workq(jp1,k,2)
           ukm1=workq(j,km1,2)
           ukp1=workq(j,kp1,2)
           vjm1=workq(jm1,k,3)
           vjp1=workq(jp1,k,3)
           vkm1=workq(j,km1,3)
           vkp1=workq(j,kp1,3)
c
           vorticity(j,k)=max(0.5d0*abs(
     &            (vjp1-vjm1)*xy(j,k,1)+(vkp1-vkm1)*xy(j,k,3)
     &           -(ujp1-ujm1)*xy(j,k,2)-(ukp1-ukm1)*xy(j,k,4)),
     &            8.5d-10)
         enddo
      enddo
      k=1
      kp1=k+1
      kp2=k+2
c     -we only need vorticity on airfoil surface at transition points
c      do j=jlow,jup
      do j=jtranlo,jtranup,jtranup-jtranlo
         jp1=j+1
         jm1=j-1
         ujk=workq(j,k,2)
         ujm1=workq(jm1,k,2)
         ujp1=workq(jp1,k,2)
         ukp1=workq(j,kp1,2)
         ukp2=workq(j,kp2,2)
         vjk=workq(j,k,3)
         vjm1=workq(jm1,k,3)
         vjp1=workq(jp1,k,3)
         vkp1=workq(j,kp1,3)
         vkp2=workq(j,kp2,3)
c
         vorticity(j,k)=max(0.5d0*abs(
     &     (vjp1-vjm1)*xy(j,k,1)+(-3.d0*vjk+4.d0*vkp1-vkp2)*xy(j,k,3)
     &    -(ujp1-ujm1)*xy(j,k,2)-(-3.d0*ujk+4.d0*ukp1-ukp2)*xy(j,k,4)),
     &     8.5d-10)
      enddo
