      SUBROUTINE calc_exprhs(jdim,kdim,nmax,jlow,jup,klow,kup,
     &     dhatq,exprhs,dhatt,exprsa)

      implicit none
 
#include "../include/common.inc"
#include "../include/parms.inc"

      INTEGER jdim,kdim,jlow,jup,klow,kup,j,k,n,i_stage,nmax
      DOUBLE PRECISION exprhs(jdim,kdim,4),exprsa(jdim,kdim),coefdhat
      DOUBLE PRECISION dhatq(jdim,kdim,4,6),dhatt(jdim,kdim,6)

      DO n = 1,4
         DO k = 1,kdim
            DO j = 1,jdim
               exprhs(j,k,n) = 0.d0
            ENDDO
         ENDDO
      ENDDO

      IF (jstage.GT.1) THEN

         DO i_stage = 1,jstage-1
            coefdhat = ajk(jstage,i_stage)/ajk(jstage,jstage)
            DO n = 1,4
               DO k = klow,kup
                  DO j = jlow,jup                  
                     exprhs(j,k,n) = exprhs(j,k,n)+
     &                    coefdhat*dhatq(j,k,n,i_stage)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO

      ENDIF


      if (nmax .eq. 5) then
    
         DO k = 1,kdim
            DO j = 1,jdim
               exprsa(j,k) = 0.d0
            ENDDO
         ENDDO    

         IF (jstage.GT.1)THEN

            DO i_stage = 1,jstage-1
               coefdhat = ajk(jstage,i_stage)/ajk(jstage,jstage)         
               DO k = klow,kup
                  DO j = jlow,jup                  
                     exprsa(j,k) = exprsa(j,k)+
     &                    coefdhat*dhatt(j,k,i_stage)
                  ENDDO
               ENDDO
            ENDDO
         
         ENDIF

      end if

      RETURN
      END
