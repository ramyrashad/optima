c ##########################
c ##                      ##
c ##  subroutine filtery  ##
c ##                      ##
c ##########################
c
      subroutine filtery2 (jdim,kdim,q,s,xyj,gam,sndsp,precon,
c     >                                  work,coef2,coef4,tmp)       
     >                                  work,coef2,coef4,tmp,tmp2)
c
c********************************************************************
c                                                                       
c      fourth difference smoothing, added explicitly to rhs            
c      second difference near shocks with pressure grd coeff.          
c                                                                       
c                     eta direction
c                                                                       
c********************************************************************
c
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)
      dimension work(jdim,kdim,4),precon(jdim,kdim,6)
c      dimension tmp(4,jdim,kdim),tmp2(4,maxj,maxk),gam(4,4,jdim,kdim)
      dimension tmp(4,jdim,kdim),tmp2(4,jdim,kdim),gam(4,4,jdim,kdim)
c
      do 39 n = 1,4                                                     
c----    1st-order forward difference ----
         do 35 k = kbegin,kup                                         
         kpl = k+1
         do 35 j = jlow,jup                                            
            tmp(n,j,k) = q(j,kpl,n)*xyj(j,kpl) - q(j,k,n)*xyj(j,k)      
 35      continue                                                    
c        
c        
c----    for fourth order: apply cent-dif to 1st-order forward ----
         do 36 k = klow,kup-1                                         
         kpl = k+1                                                         
         kmi = k-1                                                         
         do 36 j = jlow,jup                                                
            tmp2(n,j,k) = tmp(n,j,kpl) - 2.d0* tmp(n,j,k) + tmp(n,j,kmi)     
 36      continue                                                          
c  --    next points to boundaries: rows 2 & kmax-1 --
         kb = kbegin
         do 37 j = jlow,jup                                                
         tmp2(n,j,kbegin) = q(j,kb+2,n)*xyj(j,kb+2) -                          
     >                2.d0*q(j,kb+1,n)*xyj(j,kb+1) + q(j,kb,n)*xyj(j,kb)
         tmp2(n,j,kup) = tmp(n,j,kup-1) - tmp(n,j,kup)                   
 37      continue                                                          
 39   continue                                                          
c                                                                       
c
c
      if (cmesh) then
         do 19 k = kbegin,kend                                            
         do 19 j = jbegin,jend       
            u=q(j,k,2)/q(j,k,1)
            v=q(j,k,3)/q(j,k,1)
            snd2=1./sndsp(j,k)**2
            epsinv=1./precon(j,k,1)
            alp=epsinv-1.
            beta=alp*gami*precon(j,k,2)*snd2
c           
            gam(1,1,j,k)=beta+1.
            gam(2,1,j,k)=beta*u
            gam(3,1,j,k)=beta*v
            gam(4,1,j,k)=precon(j,k,2)*(beta+alp)
c           
            rc=gami*u*snd2
            gam(1,2,j,k)=-rc*alp
            gam(2,2,j,k)=-rc*alp*u + 1.
            gam(3,2,j,k)=-rc*v*alp
            gam(4,2,j,k)=-u*(beta + alp)
c           
            rc=gami*v*snd2
            gam(1,3,j,k)=-rc*alp
            gam(2,3,j,k)=-rc*u*alp
            gam(3,3,j,k)=-rc*v*alp + 1.
            gam(4,3,j,k)=-v*(beta + alp)
c           
            rc=gami*snd2
            gam(1,4,j,k)=rc*alp
            gam(2,4,j,k)=rc*alp*u
            gam(3,4,j,k)=rc*alp*v
            gam(4,4,j,k)=beta + epsinv
c        
c
c
c            u=q(j,k,2)/q(j,k,1)
c            v=q(j,k,3)/q(j,k,1)
c            snd2=1./sndsp(j,k)**2
c            beta=gami*precon(j,k,2)*snd2
c            epsinv=1./precon(j,k,1)
c            alp=epsinv-1.
cc           calc gamma*M inverse
cc           
c            gam(1,1,j,k)=beta*alp+1.
c            gam(2,1,j,k)=beta*alp*u
c            gam(3,1,j,k)=beta*alp*v
c            gam(4,1,j,k)=precon(j,k,2)*(alp*(beta+1.))
cc           
c            rc=gami*u*snd2
c            gam(1,2,j,k)=-rc*alp
c            gam(2,2,j,k)=-rc*alp*u + 1.
c            gam(3,2,j,k)=-rc*v*alp
c            gam(4,2,j,k)=-u*alp*(beta + 1.)
cc           
c            rc=gami*v*snd2
c            gam(1,3,j,k)=-rc*alp
c            gam(2,3,j,k)=-rc*u*alp
c            gam(3,3,j,k)=-rc*v*alp + 1.
c            gam(4,3,j,k)=-v*alp*(beta + 1.)
cc           
c            rc=gami*snd2
c            gam(1,4,j,k)=rc*alp
c            gam(2,4,j,k)=rc*alp*u
c            gam(3,4,j,k)=rc*alp*v
c            gam(4,4,j,k)=beta*alp + epsinv
 19      continue
      endif
c
c     -next 9 lines for debugging purposes when epsilon=1
c              ... i.e. gam should be an identity matrix
c      do k = kbegin,kend                                            
c      k=5
c      do j = jbegin,jend       
c      norm=0.d0
c      write(90,*) 'j=',j
c      do n=1,4
c         write(90,900) (gam(i,n,j,k), i=1,4)
c 900     format(4f10.2)
c         norm = norm + gam(i,n,j,k)
c       if (i.ne.n .and. gam(i,n,j,k).ne.0.) print *,j,k,gam(i,n,j,k) 
c       if (i.eq.n .and. gam(i,n,j,k).ne.1.) print *,j,k,gam(i,n,j,k) 
c      enddo
c      enddo
c      print *,j,k,norm
c      call flush(6)
cc      if (norm.ne.4.) print *,j,k,norm
c      enddo
c      enddo
c      stop
c
c     -See note in filterx2.f 
      do 50 k=kbegin,kup
         kk=k+1
         do 49 j=jlow,jup
            do 25 n=1,4
               sum=0.
               do 20 i=1,4
                  sum=sum+(coef2(j,k)*gam(n,i,j,k)+
     &                         coef2(j,kk)*gam(n,i,j,kk))*tmp(i,j,k)
 20            continue
c           
               do 21 i=1,4
                  sum=sum-(coef4(j,k)*gam(n,i,j,k)+
     &                        coef4(j,kk)*gam(n,i,j,kk))*tmp2(i,j,k)
 21            continue
               work(j,k,n)=sum
 25         continue
 49      continue
 50   continue
c
c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)                                           
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
           s(j,k,n)=s(j,k,n) + (work(j,k,n) - work(j,k-1,n))*dtd
 200  continue
      return                                                            
      end                                                               
