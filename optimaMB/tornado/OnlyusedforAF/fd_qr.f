c     ------------------------------------------------------------------
c     -- reset q --
c     -- m. nemec, sept. 2001 --
c     ------------------------------------------------------------------
      subroutine fd_qr( jo, ko, no, jmax, kmax, nmax, q, turre, tmpv)

      dimension q(jmax,kmax,4), turre(jmax,kmax) 

      if (no.le.4) then
         q(jo,ko,no) = tmpv
      else
         turre(jo,ko) = tmpv
      end if

      return
      end                       !fd_qr
