//*****************************************************************
// Iterative template routine -- CG
//
// CG solves the symmetric positive definite linear
// system Ax=b using the Conjugate Gradient method.
//
// CG follows the algorithm described on p. 15 in the 
// SIAM Templates book.
//
// The return value indicates convergence within max_iter (input)
// iterations (0), or no convergence within max_iter iterations (1).
//
// Upon successful return, output arguments have the following values:
//  
//        x  --  approximate solution to Ax = b
// max_iter  --  the number of iterations performed before the
//               tolerance was reached
//      tol  --  the residual after the final iteration
//  
//*****************************************************************

#include "/Applications/sparselib_1_6/include/comprow_double.h"
#include "/Applications/sparselib_1_6/include/icpre_double.h"
#include "/Applications/sparselib_1_6/include/ilupre_double.h"
#include "/Applications/sparselib_1_6/include/diagpre_double.h"

#include <iostream>
//#include "/Applications/sparselib_1_6/include/vector_double.h"

using namespace std;
//template < class Matrix, class Vector, class Preconditioner, class Real >
//int 
int CG(const CompRow_Mat_double &A, VECTOR_double &x, const VECTOR_double &b,
   const DiagPreconditioner_double &M, int &max_iter, double &tol)
{

  cout << "A:" << endl << (CompRow_Mat_double)A << endl;
  cout << "tol:" << endl << (double)tol << endl;
  
  //<< "x:" << endl << x << endl << "b:" << endl << b;
  //cout << "M:" << endl << M << endl << "maxitr:" << endl << max_iter << endl << "tol:" << endl << tol << endl;
  
  double resid;
  VECTOR_double p, z, q;
  VECTOR_double alpha(1), beta(1), rho(1), rho_1(1);

  double normb = norm(b);
  VECTOR_double r = b - A*x;


  cout << "r: " << endl << r << endl;
	
  if (normb == 0.0) 
    normb = 1;
  
  if ((resid = norm(r) / normb) <= tol) {
    tol = resid;
    max_iter = 0;
    return 0;
  }

  for (int i = 1; i <= max_iter; i++) {
    z = M.solve(r);
    
	rho(0) = dot(r, z);
    
    if (i == 1)
      p = z;
    else {
      beta(0) = rho(0) / rho_1(0);
      p = z + beta(0) * p;
    }
    
    q = A*p;
	
//	cout << "q: " << endl << q << endl;
//	cout << "z: " << endl << z << endl;
	
    alpha(0) = rho(0) / dot(p, q);
    
    x += alpha(0) * p;
    r -= alpha(0) * q;
    
    if ((resid = norm(r) / normb) <= tol) {
      tol = resid;
      max_iter = i;
      return 0;     
    }

    rho_1(0) = rho(0);
  }
  
  tol = resid;
  return 1;
}

