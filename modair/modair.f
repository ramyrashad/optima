      program modair
c----------------------------------------------------------------------
c     -- airfoil and grid setup for optimization --
c     -- read a hygrid or amber2D grid and bspline airfoil --
c     -- define design variables --
c     -- modify airfoil shape for inverse design --
c
c     written by: marian nemec 
c     date: april 2000
c     mods: dec. 2000
c
c     -- no transfer of this program without explicit approval --
c----------------------------------------------------------------------
      implicit none

#include "parm.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer jmax, kmax, jtail1, jtail2
      common/grid/ jmax, kmax, jtail1, jtail2

      integer input_unit, grid_unit, bsp_unit, new_grid_unit
      integer dvs_unit, btec_unit, etec_unit, gridb_unit, gridc_unit 
      common/units/ input_unit, grid_unit, new_grid_unit, dvs_unit,
     &      bsp_unit, btec_unit, etec_unit, gridb_unit, gridc_unit 

      integer idv(ibsnc)
      double precision bap(ibody,2), bcp(ibsnc,2), bt(ibody)
      double precision bknot(ibskt), x(maxj,maxk), y(maxj,maxk)
      double precision dvs(ibsnc)

      double precision work(ibody,10)

      input_unit     = 10
      grid_unit      = 11
      new_grid_unit  = 12      
      dvs_unit       = 13
      bsp_unit       = 14
      btec_unit      = 15
      etec_unit      = 16
      gridb_unit     = 17
      gridc_unit     = 18

      call input

      call modify (bap, bcp, bt, bknot, x, y, dvs, idv, work)

      stop
      end                       !modair
c-----------------------------------------------------------------------
      subroutine input 

      implicit none

#include "parm.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer jmax, kmax, jtail1, jtail2
      common/grid/ jmax, kmax, jtail1, jtail2

      integer input_unit, grid_unit, bsp_unit, new_grid_unit
      integer dvs_unit, btec_unit, etec_unit, gridb_unit, gridc_unit 
      common/units/ input_unit, grid_unit, new_grid_unit, dvs_unit,
     &      bsp_unit, btec_unit, etec_unit, gridb_unit, gridc_unit  

c     -- input parameters --

      namelist/inputs/ jmax, kmax, jtail1, jtail2, nc, iorder, maxiter,
     &      tol, tecplot

      open ( unit=input_unit, file='grid.inp', status='unknown')

      read(input_unit,nml=inputs)

      write(*,10) jmax, kmax, jtail1, jtail2
      write(*,210) maxj, maxk

      jbody = jtail2 - jtail1 + 1

      if ( jmax.gt.maxj .or. kmax.gt.maxk .or. nc.gt.ibsnc .or.
     &      iorder.gt.ibsord ) then
        write (*,*) '** Some input prameters greater than parm.inc!! **'
        stop
      end if

 10   format (/1x,'JMAX:',i5,' KMAX:',i5,' JTAIL1:',i5,' JTAIL2:',i5)
 210  format (/1x,'In modair/parm.in: MAXJ:',i5,' MAXK:',i5)
      return
      end                       !input
c-----------------------------------------------------------------------
      subroutine modify( bap, bcp, bt, bknot, x, y, dvs, idv, work)

      implicit none

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot      

      integer jmax, kmax, jtail1, jtail2
      common/ grid/ jmax, kmax, jtail1, jtail2

      integer input_unit, grid_unit, bsp_unit, new_grid_unit
      integer dvs_unit, btec_unit, etec_unit, gridb_unit, gridc_unit 
      common/units/ input_unit, grid_unit, new_grid_unit, dvs_unit,
     &      bsp_unit, btec_unit, etec_unit, gridb_unit, gridc_unit  

      integer j, k, imod, idv(nc), ndv, jmax2, kmax2, ne,idvi,idvf,iskp
      integer ivar(3)
      logical errf

      double precision bap(jbody,2), bcp(nc,2), bt(jbody)
      double precision bknot(nc+iorder), x(jmax,kmax), y(jmax,kmax)
      double precision dvs(nc), tmp1

      double precision work(jbody,10) 

c     -- graphics variables --
      integer ipslu, idev
      data    ipslu, idev /0, 1/

      double precision relsize
      data relsize /0.64/       ! + = landscape, - = portrait

      double precision x_scale, y_scale, x_org, y_org
      data x_scale, y_scale, x_org, y_org
     &   /     8.0,     8.0,   0.0,   0.0 /	

      logical cont, moda, answer

c     -- initialize graphics --
      call plinitialize	
      call plopen ( relsize, ipslu, idev )
      call drawtoscreen
      call newfactors ( x_scale, y_scale )

c     -- read grid --
      open ( unit=grid_unit, file='grid.g', status='unknown', 
     &      form='unformatted')
      read(grid_unit) jmax2, kmax2
      if ( jmax.ne.jmax2 .or. kmax.ne.kmax2) then
        write (*,*) ' Check jmax or kmax in input file!! '
        stop
      end if
      read(grid_unit) ((x(j,k),j=1,jmax) ,k=1,kmax),
     &                ((y(j,k),j=1,jmax) ,k=1,kmax)

c     -- copy grid body coordinates to bap array --
      k = 1
      do j = jtail1, jtail2
        bap(k,1) = x(j,1)
        bap(k,2) = y(j,1)
        k = k + 1
      end do

c     -- approximate airfoil with b-spline --
      call bspline (bap(1,1), bap(1,2), bcp, bt, bknot, work(1,1),
     &      work(1,2), work(1,3), work(1,4), work(1,5), work(1,6),
     &      work(1,7), work(1,8), work(1,9))

      call graphit ( relsize, ipslu, idev, x_org, y_org, x_scale,
     &      y_scale, bap, bcp, jbody, nc, iorder )

      ndv = 0
c     -- adjust grid due to errors in b-spline --
      call newgrid (ndv, dvs, idv, x, y, bap, bcp, bt, bknot)
      
c     -- select design variables --
      write (*,9) 
 9    format (/1x,'Current airfoil control points are:')
      do j = 1,nc
        write (*,100) j,bcp(j,1),bcp(j,2)
      end do

      write (*,10) 
 10   format (/1x,
     &      'Define control points to be used as design variables:',
     &      /1x,'Enter Control Point index OR', /1x,
     &      'Enter start end increment Control Point index OR', /1x,
     &      'Enter -1 to EXIT')

      ndv = 0
      ivar(1) = -1
      ivar(2) = -1
      ivar(3) = -1 

c     -- main loop for design variables --
 1000 continue

      ndv = ndv + 1
      
      write (*,11)
 11   format (/1x,'Input design variable(s)   i>  ',$)
      ne = 3
      call readi( ne, ivar, errf)

      if (errf) then
        write (*,*) 'Your input is wrong => Try again!!'
        ndv = ndv - 1
        errf = .false.
        goto 1000
      end if

c     -- exit condition --
      if (ivar(1).eq.-1) goto 1100 

c     -- ne holds the number of input integers --
      if (ne .eq. 1) then
c     -- one control point entered --
        idv(ndv) = ivar(1)
        dvs(ndv) = bcp(idv(ndv),2)
        ivar(1) = -1
        goto 1000
        
      else if (ne .eq. 2) then
c     -- input doesn't make sense --
        write (*,*) 'You must enter increment => Try again!'
        ndv = ndv - 1
        goto 1000

      else if (ne .eq. 3) then
c     -- triplet entered --
        idvi = ivar(1)
        idvf = ivar(2)
        iskp = ivar(3)

        cont = .true.
        do while (cont)
          idv(ndv) = idvi
          dvs(ndv) = bcp(idv(ndv),2)
          idvi = idvi + iskp
          ndv = ndv + 1
          if (idvi.gt.idvf) then
            ndv = ndv - 1
            cont = .false.
          end if
        end do
        ivar(1) = -1
        ivar(2) = -1
        ivar(3) = -1

        goto 1000

      else
        write (*,*) 'ne value is wrong => program stopped'
        stop
      end if

 1100 continue 

      ndv = ndv - 1

      if (ndv.ne.0) then
        write (*,12) ndv
 12     format (/1x,'The selected',i4,' design variable(s) are:')
        do j = 1,ndv
          write (*,200) j, idv(j), dvs(j)
        end do
        call askl('Modify shape of the airfoil?^',moda)
      else
        write (*,13) 
 13     format (/1x,'No design variables selected.')
        moda = .false.
      end if

      if (moda) then
c     -- modify shape for inverse design --
        cont = .true.
        do while (cont)
          write (*,14)
 14       format (1x,'Current list of design variables:')
          do j = 1,ndv
            write (*,200) j, idv(j), dvs(j)
          end do  

 15       continue

          call aski('Modify which design variable (999 exit)?^',imod)

          if ( imod.eq.999 ) go to 20
          write (*,17) dvs(imod)
 17       format (3x,'Current value of the design variable is',e13.6)
          tmp1 = dvs(imod)
          call askr('Enter new y-coordinate^',dvs(imod))

          write (*,18) ( dvs(imod) - tmp1 )/ tmp1*1.d2
 18       format (3x,'The % change for the d.v. is:', f6.2 )

          call askl('Is this acceptable?^',answer)
          if (.not. answer) dvs(imod) = tmp1
          go to 15

 20       continue

c     -- generate new grid --
          call newgrid( ndv, dvs, idv, x, y, bap, bcp, bt, bknot)

          call graphit ( relsize, ipslu, idev, x_org, y_org, x_scale,
     &          y_scale, bap, bcp, jbody, nc, iorder )

          call askl('Change design variable(s)?^',answer)
          if ( .not.answer ) cont=.false. 
          
        end do
      end if

      call output( ndv, dvs, idv, x, y, bcp, bt, bknot)

      call plend
      call plclose

 100  format(i4,2e14.5)
 200  format(2i4,e14.5)

      close (grid_unit)

      stop
      end                       !modify
c----------------------------------------------------------------------
      subroutine output(ndv, dvs, idv, x, y, bcp, bt, bknot)

      implicit none

#include "parm.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer jmax, kmax, jtail1, jtail2
      common/grid/ jmax, kmax, jtail1, jtail2

      integer input_unit, grid_unit, bsp_unit, new_grid_unit
      integer dvs_unit, btec_unit, etec_unit, gridb_unit, gridc_unit 
      common/units/ input_unit, grid_unit, new_grid_unit, dvs_unit,
     &      bsp_unit, btec_unit, etec_unit, gridb_unit, gridc_unit 

      integer j, k, idv(nc), ndv, ndatb, ndatc

      double precision bcp(nc,2), bt(jbody), dvs(nc)
      double precision bknot(nc+iorder), x(jmax,kmax), y(jmax,kmax)
      double precision tb(ibody), tc(ibody)

      open ( unit=new_grid_unit, file='grid-new.g', status='unknown', 
     &      form='unformatted')
      open (unit=bsp_unit, file='grid-new.bsp', status='unknown')
      open (unit=gridb_unit,file='gridb-new.bsp')
      open (unit=gridc_unit,file='gridc-new.bsp')
      open (unit=dvs_unit, file='grid-new.dvs', status='unknown')

c     -- output new grid --
      write(new_grid_unit) jmax, kmax
      write(new_grid_unit) ((x(j,k),j=1,jmax), k=1,kmax),
     &      ((y(j,k),j=1,jmax), k=1,kmax)

c     -- output bspline control file --
      write (bsp_unit,100) iorder
      write (bsp_unit,100) nc
      write (bsp_unit,100) jbody
      write (bsp_unit,110) ( bcp(j,1), bcp(j,2), j=1, nc )
      write (bsp_unit,120) ( bt(j), j=1, jbody )
      write (bsp_unit,120) ( bknot(j), j=1, iorder+nc )

c     -- output parameter vectors for grids 'b' and 'c' --
      j=1
      do k=1,jbody,2
        tb(j) = bt(k)
        j = j+1
      end do
      ndatb = j-1
      
      write (gridb_unit,100) iorder
      write (gridb_unit,100) nc
      write (gridb_unit,100) ndatb
      write (gridb_unit,110) ( bcp(j,1), bcp(j,2), j=1, nc )
      write (gridb_unit,120) ( tb(j), j=1, ndatb )
      write (gridb_unit,120) ( bknot(j), j=1, iorder+nc )

      j=1
      do k=1,ndatb,2
        tc(j) = tb(k)
        j = j+1
      end do
      ndatc = j-1
      
      write (gridc_unit,100) iorder
      write (gridc_unit,100) nc
      write (gridc_unit,100) ndatc
      write (gridc_unit,110) ( bcp(j,1), bcp(j,2), j=1, nc )
      write (gridc_unit,120) ( tc(j), j=1, ndatc )
      write (gridc_unit,120) ( bknot(j), j=1, iorder+nc )

c     -- output design variables --
      write (dvs_unit,130) ( idv(j), dvs(j), j=1, ndv )

 100  format (i5)
 110  format (2e24.16)
 120  format (e24.16)
 130  format(i5,e24.16)

      close (new_grid_unit)
      close (bsp_unit)
      close (dvs_unit)
      close (gridb_unit)
      close (gridc_unit)

      return
      end                       ! output
