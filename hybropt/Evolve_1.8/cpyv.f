c======================================================================
      subroutine cpyv(m,a,b)

C     ARC2D V3.00 by Thomas H. Pulliam
C     NASA Ames Research Center
C     Copyright 1992 
C     Restricted to United States (NO Foreign Dissemination!!)

      dimension a(m),b(m)
      do i= 1,m
         b(i)= a(i)
      enddo

      return
      end
