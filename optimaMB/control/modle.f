c     ------------------------------------------------------------------
c     -- modify l. e. grid blocks for given airfoil --
c     -- m. nemec, may 2001 --
c     ------------------------------------------------------------------
      subroutine modle (ks, kf, ki, jlte, jlte2, js, je, ji, js2, je2,
     &     jmax, kmax, xlte, ylte, jmax2, kmax2, x, y, seg, pi)

      implicit none

      integer ks, kf, ki, jlte, jlte2, js, je, ji, js2, je2, jmax, kmax
      integer jmax2, kmax2,j,k,i,iiend
      double precision xlte(jmax,kmax), ylte(jmax,kmax)
      double precision x(jmax2,kmax2),  y(jmax2,kmax2)
      double precision seg(jmax2*kmax2),dx,dy,tmp,pi

      logical mody, modx
      
c      write (*,*) 'loop', ks, kf, ki

      if (kmax.ne.kmax2) then
         write (*,*) 'modle: ** problem with block dimensions **'
         stop
      end if

      do k = ks, kf, ki
         
         dy = ylte(jlte,k) - y(jlte2,k)
         dx = xlte(jlte,k) - x(jlte2,k)

         mody = .false.
         modx = .false.

         if ( abs( dy ) .gt. 1.e-15 ) mody=.true.
         if ( abs( dx ) .gt. 1.e-15 ) modx=.true.

         if ( mody .or. modx ) then
c     -- calculate arclength for j-line at location k --
c     write (*,*) 'modle: modx, mody:', modx, mody

            i = 1
            seg(i) = 0.0d0
            do j = js, je, ji
               tmp = sqrt( (x(j,k) - x(j+1,k) )**2 + ( y(j,k) -
     &              y(j+1,k) )**2 )
               i = i+1
               seg(i) = seg(i-1) + tmp
            end do
            iiend = i
c     -- adjust grid --
            if ( mody ) then
               i = 1
               do j = js2, je2, ji
                  y(j,k) = y(j,k) + dy/2.0d0*( 1.0d0 + cos(pi*seg(i)
     &                 /seg(iiend)) )
c     write (*,*) j,seg(i)/seg(iiend),dy
c                  y(j,k) = y(j,k) + dy*( 1.0 - seg(i)/seg(iiend) )
                  i = i+1
               end do
            end if

            if ( modx ) then
               i = 1
               do j = js2, je2, ji
                  x(j,k) = x(j,k) + dx/2.0*( 1.0 + cos(pi*seg(i)
     &                 /seg(iiend)) )
c                  x(j,k) = x(j,k) + dx*( 1.0 - seg(i)/seg(iiend) )
                  i = i+1
               end do
            end if

         end if

      end do

      return
      end                       ! modle
