C ##########################
C ##                      ##
C ##  SUBROUTINE FILLMAT  ##
C ##                      ##
C ##########################
C
      SUBROUTINE FILLMAT (JDIM,KDIM,Q,XMET,YMET,TMET,D)                  
C
C**********************************************************************
C     This subroutine constructs the Flux Jacobian matrices A or B at 
C  each node. Stored at D().
C**********************************************************************
C
#include "../include/arcom.inc"
C                                                                       
      DIMENSION Q(JDIM,KDIM,4)                                          
      DIMENSION XMET(JDIM,KDIM),YMET(JDIM,KDIM),TMET(JDIM,KDIM)         
      DIMENSION D(JDIM,KDIM,4,4)                                        
C
C                                                                       
      HD = .5 * DT * THETADT/(1.+PHIDT)                               
C                                                                       
      DO 100 K = KBEGIN,KEND                                            
      DO 100 J=JBEGIN,JEND                                              
         R1 = XMET(J,K)*HD                                              
         R2 = YMET(J,K)*HD                                              
         R0 = TMET(J,K)*HD                                              
         RR = 1. / Q(J,K,1)                                             
         U = Q(J,K,2)*RR                                                
         V = Q(J,K,3)*RR                                                
         UT = U*U + V*V                                                 
         C1 = GAMI *UT*.5                                               
         C2 = Q(J,K,4)*RR*GAMMA                                         
         QS = U*R1 + V*R2                                               
         D(J,K,1,1) = R0                                                
         D(J,K,1,2) = R1                                                
         D(J,K,1,3) = R2                                                
         D(J,K,1,4) = 0.                                                
         D(J,K,2,1) = ( -U*U +C1)*R1   -U*V*R2                          
         D(J,K,2,2) = - ( GAMMA -3.)*U*R1 + V *R2 + R0                  
         D(J,K,2,3) = - GAMI*V*R1 + U*R2                                
         D(J,K,2,4) = GAMI*R1                                           
         D(J,K,3,1) = - U*V*R1 + ( -V*V +C1)*R2                         
         D(J,K,3,2) = V*R1 - GAMI*U*R2                                  
         D(J,K,3,3) = U*R1 +(3.-GAMMA)*V*R2 + R0                        
         D(J,K,3,4) = GAMI*R2                                           
         D(J,K,4,1) = ( - C2 + C1*2.)*QS                                
         D(J,K,4,2) = ( C2 -C1)*R1 - GAMI*U*QS                          
         D(J,K,4,3) = ( C2 - C1)*R2 - GAMI*V*QS                         
         D(J,K,4,4) = GAMMA*QS + R0                                     
100   CONTINUE                                                          
C
C                                                                      
      RETURN                                                            
      END                                                               
