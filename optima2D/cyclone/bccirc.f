

      subroutine bccirc
c     
#include "../include/arcom.inc"
c     
c---------------------------------------------------------------------
c---------------------------------------------------------------------
c---  boundary conditions for airfoil topology              ---  
c---  c  or  o mesh type                           ---  
c---------------------------------------------------------------------
c---------------------------------------------------------------------
c     
c     compute variables for circulation correction for far field logic.
c     
c     
c**********************************************************************
c**********************************************************************
c     
c.....................................................................
c     far field circulation based on potential vortex added to reduce
c     
c     the dependency on outer boundary location.  works well.  for
c     
c     instance, a naca0012 at m=0.63 a=2. shows no dependency on ob from
c     
c     4 - 96 chords.
c     
c.....................................................................
c     
      circb = 0.                                                    
      beta = sqrt(1.-fsmach**2)                                     
      chord = 1.                                                    
      if(.not.clalpha)then                                                   
        circb = 0.25*chord*clt*beta*fsmach/pi 
      else
         circb = 0.25*chord*clinput*beta*fsmach/pi           
      endif                                                                                                     
c     
      return                                                           
      end                                                              
