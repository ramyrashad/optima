c----------------------------------------------------------------------
c     -- generate b-spline for given airfoil segment --
c     -- m. nemec --
c----------------------------------------------------------------------
      subroutine genBSP(ifoil, iseg, is, ap, cp, t, knot, work)
      
      implicit none

#include "mxfoil.inc"
#include "bsp.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      integer ifoil, iseg, is, i, j
      double precision ap(jbody,2), cp(nc,2), t(jbody), knot(nc+iorder)
      double precision work(ibody,10)

      j = 1
      do i = is,jbody+is-1
         ap(j,1) = bap(i,1,ifoil)
         ap(j,2) = bap(i,2,ifoil)
         j = j+1
      end do

      call bspline (ap(1,1), ap(1,2), cp, t, knot,
     &     work(1,1),work(1,2), work(1,3), work(1,4), work(1,5),
     &     work(1,6),work(1,7), work(1,8), work(1,9))

      j = 1
      do i = is,jbody+is-1
         bap(i,1,ifoil) = ap(j,1)
         bap(i,2,ifoil) = ap(j,2)
         bt(j,ifoil,iseg) = t(j)
         j = j+1
      end do

      do i = 1,nc
         bcp(i,1,ifoil,iseg) = cp(i,1)
         bcp(i,2,ifoil,iseg) = cp(i,2)
      end do

      do i = 1,nc+iorder
         bknot(i,ifoil,iseg) =  knot(i)
      end do

      return 
      end                       !genBSP
