c     ------------------------------------------------------------------
c     -- add column of jacobian matrix to sparse arrays --
c     -- m. nemec, sept. 2001 --
c     ------------------------------------------------------------------
      subroutine fd_mat( nz, ic, jo, ko, no, js, je, ks, ke, jmax, kmax,
     &     nmax, indx, jmxc, kmxc, indxc, cdrdq, dsadq, jpf, pf)

      dimension indx(jmax,kmax), indxc(jmxc,kmxc), cdrdq(jmax,kmax,4)
      dimension jpf(*), pf(*), dsadq(jmax,kmax)

      ic = ( indxc(jo,ko) - 1 )*nmax + no
      
c      if (jo.eq.6 .and. ko.eq.9 .and. no.eq.3) write (*,*) ic, indxc(jo
c     &     ,ko), nmax, no

      do j=js,je
         do k=ks,ke
            do n = 1,4
c               if (ic.eq.1611) write (*,*) j,k,n,cdrdq(j,k,n)
c               if (ic.eq.1611) write (*,*) indx(j,k)
               if (abs(cdrdq(j,k,n)).gt.1.e-9) then
                  ir = ( indx(j,k) - 1 )*nmax + n
                  nz = nz+1
                  jpf(nz) = ir
                  pf(nz) = cdrdq(j,k,n)
               end if
            end do
            
            if (nmax.eq.5) then
               if (abs(dsadq(j,k)).gt.1.e-9) then
                  ir = ( indx(j,k) - 1 )*5 + 5
                  nz = nz+1
                  jpf(nz) = ir
                  pf(nz) = dsadq(j,k)
               end if
            end if

         end do
      end do

c      write (*,*) 'done fd_mat'
      return
      end                       ! fd_mat
