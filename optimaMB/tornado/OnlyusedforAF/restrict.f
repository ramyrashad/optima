*********************************************************************
**  subroutine to restrict q from a fine block to a coarse block   **
**  Note:  turre is also restricted when required                  **
*********************************************************************
c     Calling subroutine: multigrid.f

      subroutine restrict (fblk,cblk,jdimf,kdimf,qf,xyjf,
     &                               turref,turmuf,
     &                               jdimc,kdimc,qc,xyjc,
     &                               turrec,turmuc)
c
#include "../include/common.inc"
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
c
      integer fblk,cblk,jdimf,kdimf,jdimc,kdimc
      dimension qf(jdimf,kdimf,4),xyjf(jdimf,kdimf)
      dimension qc(jdimc,kdimc,4),xyjc(jdimc,kdimc)
      dimension turref(jdimf,kdimf),turrec(jdimc,kdimc)
      dimension turmuf(jdimf,kdimf),turmuc(jdimc,kdimc)
c
      jmnc=jminbnd(cblk)
      jmxc=jmaxbnd(cblk)
      kmnc=kminbnd(cblk)
      kmxc=kmaxbnd(cblk)
c
      jmnf=jminbnd(fblk)
      jmxf=jmaxbnd(fblk)
      kmnf=kminbnd(fblk)
      kmxf=kmaxbnd(fblk)
c
c     -remove the jacobian from fine q
      do 10 n = 1,4
      do 10 k = kmnf,kmxf
      do 10 j = jmnf,jmxf
         qf(j,k,n) = qf(j,k,n)*xyjf(j,k)
 10   continue

c     -copying q to the coarse block

      do 20 n  = 1,4
      do 20 kc = kmnc,kmxc
      do 20 jc = jmnc,jmxc
c       jf = (jc-(nhalo+1))*2 + (nhalo+1)
c       kf = (kc-(nhalo+1))*2 + (nhalo+1)
        jf = 2*jc - jmnc
        kf = 2*kc - kmnc
c       copy value at node:
        qc(jc,kc,n) = qf(jf,kf,n) 
 20   continue
c
c     -put the jacobian back in q

c     fine q:
      do 30 n = 1,4
      do 30 k = kmnf,kmxf
      do 30 j = jmnf,jmxf
         qf(j,k,n) = qf(j,k,n)/xyjf(j,k)
 30   continue
c
c     coarse q:
      do 40 n = 1,4
      do 40 k = kmnc,kmxc
      do 40 j = jmnc,jmxc
         qc(j,k,n) = qc(j,k,n)/xyjc(j,k)
 40   continue


c     -restricting viscous variables when applicable
      if(turbulnt) then
c        -simple injection
c        do 50 kc = kmnc,kmxc
c        do 50 jc = jmnc,jmxc
c          jf = 2*jc - jmnc
c          kf = 2*kc - kmnc
cc          copy value at node:
c          turrec(jc,kc) = turref(jf,kf) 
c 50     continue

c       -we need turmu at the half nodes
        do 60 kc = kmnc,kmxc-1
        do 60 jc = jmnc,jmxc
          jf = 2*jc - jmnc
          kf = 2*kc - kmnc
          turmuc(jc,kc)=0.5d0*(turmuf(jf,kf)+turmuf(jf,kf+1))
 60     continue
      end if
c
      return
      end
