c ########################
c ##                    ##
c ##  subroutine tkinv  ##
c ##                    ##
c ########################
c
      subroutine tkinv (jdim,kdim,q,sndsp,s,xyj,xy,ix,iy,precon)  
c
c*******************************************************************   
c  ix = 1, iy = 2  xi  metrics                                          
c  ix = 3, iy = 4  eta metrics                                          
c*******************************************************************   
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),s(jdim,kdim,4),sndsp(jdim,kdim)          
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),precon(jdim,kdim,6)  
c                                                                       
      bt = 1./sqrt(2.)                                                  
c                                                                       
c     t inverse matrix multiply for diagonal algorithm                     
c      k                                                                   
c
      if (prec.eq.0) then
          do 1 k = klow, kup                                                
          do 1 j = jlow,jup                                                
             rx        = xy(j,k,ix)                                         
             ry        = xy(j,k,iy)                                         
             rrxy      = 1./sqrt(rx**2+ry**2)                               
             rx        = rx*rrxy                                            
             ry        = ry*rrxy                                            
c
             rho       = q(j,k,1)*xyj(j,k)                               
             rhoinv    = 1./rho                                          
             rhoinvr   = rhoinv*xyj(j,k)                                 
             u = q(j,k,2)*rhoinvr                                        
             v = q(j,k,3)*rhoinvr                                        
             snr   = 1./sndsp(j, k)                                      
             bet0  = bt*rhoinv*snr                                       
c                                                                          
                term1 = gami*( u*s(j,k,2) + v*s(j,k,3) - s(j,k,4)        
     >                  - 0.5*(u**2+v**2)*s(j,k,1) )                     
c                                                                        
                s1    =  s(j,k,1) + term1*snr**2                         
                s2    = ( (- ry*u + rx*v)*s(j,k,1)                       
     >                  + ry*s(j,k,2) - rx*s(j,k,3) )*rhoinv             
c                                                                        
                term1 =  term1*bet0                                      
                term2 =  ((rx*u + ry*v)*s(j,k,1)                         
     >                  - rx*s(j,k,2) - ry*s(j,k,3) )*bt*rhoinv          
c                                                                        
                s(j,k,1) = s1                                            
                s(j,k,2) = s2                                            
                s(j,k,3) = -(term1 + term2)                              
                s(j,k,4) = -(term1 - term2)                              
c                                                                          
1        continue                                                        
      else
c     use the preconditioned tkinv
c 
         do 1000 k=klow,kup
         do 1000 j=jlow,jup
            dkx=xy(j,k,ix)
            dky=xy(j,k,iy)
c           
            u=q(j,k,2)/q(j,k,1)
            v=q(j,k,3)/q(j,k,1)
            uk=dkx*u+dky*v
c           
            dkb=dsqrt(dkx**2+dky**2)
            dkx=dkx/dkb
            dky=dky/dkb
c           
            du1=dkb*sndsp(j,k)
            du2=(uk-precon(j,k,ix+2)+precon(j,k,iy+2))
            du3=(uk-precon(j,k,ix+2)-precon(j,k,iy+2))
c           
            alp=1.d0/(2.d0*precon(j,k,iy+2))
            a1=du1*s(j,k,1)
            a2=dkx*s(j,k,2) + dky*s(j,k,3)
c
            t1=s(j,k,4)
            t2=dky*s(j,k,2) - dkx*s(j,k,3)
            t3=(a1 + du2*a2)*alp
            t4=(a1 + du3*a2)*alp
c           
            s(j,k,1)=t1
            s(j,k,2)=t2
            s(j,k,3)=t3
            s(j,k,4)=t4
 1000    continue
      endif
      return                                                          
      end                                                             
