#ifndef SOLVERS_H
#define SOLVERS_H

#include <cmath>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

void jacobi(int*& rowptr, int*& colind, double*& val, double*& du,
	    double*& F, const int& nrows, double& res);

int pcg (int*& rowptr, int*& colind, double*& val, double* du,
	    double* F, const int& nrows, double& tol, int& maxit);

void dot(double*& vec1, double*& vec2, double& ans, const int& nrows);

void norm(double*& vec, const int& nrows, double& ans);

void matrix_times_vector (int*& rowptr, int*& colind, double*& val, 
	double*& vec, double*& sol, const int& nrows);

int ILU0 (int*& rowptr, int*& colind, double*& val, double*& LUval,
	   	const int& nrows, int*& uptr);

void LUsol(const int& nrows, double*& vec, double*& rhs, double*& LUval,
           int*& rowptr, int*& colind, int*& uptr);


#endif   // SOLVERS_H
