      subroutine fr_test(ifirst,ntvar,nmax,qps,q,qos,press,sndsp,
     &     s,rsa,xy,xyj,xit,ett,ds,x,y,turmu,vort,turps,
     &     turre,turos,fmu,vk,ve,tmp, uu,vv,ccx,ccy,
     &     coef2x,coef2y,coef4x,coef4y,precon,spectxi,
     &     specteta,pa,jpa,ipa,
     &     indx,iex,icol,bcf,exprhs,dhatq,exprsa,dhatt,dtvc,as,ja,ia)     

      implicit none

#include "../include/units.inc"
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"

      integer ifirst, ntvar, nmax, j, i, ib, lg, lq, is, ii, iii
      integer indx(*),iex(maxjb,4,4,mxbfine),icol(*),im,ip,iim,iip
      integer ipa(*),jpa(*),ia(*),ja(*),index1,jk

      logical precmat, jacmat

      double precision q(*),      press(*),    sndsp(*)
      double precision s(*),      xy(*),       xyj(*)
      double precision xit(*),     ett(*),      ds(*)
      double precision x(*),       y(*),        turmu(*)
      double precision vort(*),    turre(*),   fmu(*)
      double precision vk(*),      ve(*),       tmp(*)
      double precision uu(*),      vv(*),       ccx(*)
      double precision ccy(*),     coef2x(*),   coef2y(*)
      double precision coef4x(*),  coef4y(*),   precon(*)
      double precision spectxi(*), specteta(*), rsa(*)
      double precision v1_n,eps,eps1,xyjtmp(maxjk)
      double precision qps(*), qos(*), turps(*),turos(*)
      double precision exprhs(*),dhatq(*),exprsa(*),dhatt(*)
      double precision dtvc(*),as(*),pa(*),bcf(maxjb,4,4,mxbfine)
      double precision temp
      double precision ones(ntvar),dummy(maxjk),qdummy(maxjkq)
      double precision q_temp(ntvar),wk_temp1(ntvar),wk_temp2(ntvar)


      double precision wk_temp3(ntvar)


c     -- Random Seed Variables
      integer, dimension(1) :: old, seed
      integer :: k
      double precision :: harvest

c     seed an arbitrary vector of ones and a temporary work array

c$$$      seed = 12345
c$$$      call random_seed
c$$$      call random_seed(size=k)
c$$$      call random_seed(get=old)
c$$$      call random_number(harvest)
c$$$      call random_seed(get=old)
c$$$      call random_seed(put=seed)
c$$$      call random_seed(get=old)
c$$$
c$$$      do i=1,ntvar
c$$$         call random_number(harvest)
c$$$         ones(i)=0.2d0*harvest+0.9d0        ! -- random perturbation array
c$$$         wk_temp1(i)=0.d0        ! -- zero work arrays
c$$$         wk_temp2(i)=0.d0
c$$$      end do

      write(*,*)'beginning of fr_test'

      do ii=1,maxjk
         dummy(ii)=0.d0
      enddo
      do ii=1,maxjkq
         qdummy(ii)=0.d0
      enddo
      do ii=1,ntvar
         q_temp(ii)=0.d0
      enddo


c>>>>>>>>>>>>>>>>>
c              figure out index location
      ib = 6
      do  j=jminbnd(ib),jmaxbnd(ib)
c         k=kmaxbnd(ib)
         k=20
c$$$      do  k=kminbnd(ib),kmaxbnd(ib)
c$$$         j=jmaxbnd(ib)
         do jk=1,4

            iii=lgptr(ib)+(j-1)+(k-1)*jmax(ib)
            ii = ( indx(iii) - 1 )*nmax + jk

            iip=lgptr(ib)+(j-1+1)+(k-1)*jmax(ib)
            ip = ( indx(iip) - 1 )*nmax + jk

            iim=lgptr(ib)+(j-1-1)+(k-1)*jmax(ib)
            im = ( indx(iim) - 1 )*nmax + jk

            write(*,*)'perturb node',ib,j,k,jk

c<<<<<<<<<<<<<<<<<

cc      do ii=1,ntvar

            do i=1,ntvar         
               ones(i)=0.d0        
               wk_temp1(i)=0.d0       
               wk_temp2(i)=0.d0
            end do

            ones(ii)=1.d0*max(abs(q(ii)),1.d-14)

c         print *,'Perturb node',ii

            ifirst = 0

            call framux(ifirst,ntvar,nmax,q,qps,qos,press,sndsp,
     &           s,rsa,xy,xyj,xit,ett,ds,x,y,turmu,vort,turre,
     &           turps,turos,fmu,vk,ve,tmp, uu,vv,ccx,ccy,
     &           coef2x,coef2y,coef4x,coef4y,precon,spectxi,
     &           specteta,indx,iex,bcf,ones,wk_temp1,
     &           exprhs,dhatq,exprsa,dhatt,dtvc)

            call amux (ntvar,ones,wk_temp2,as,ja,ia)

            call amux (ntvar,ones,wk_temp3,pa,jpa,ipa)

            do i=1,ntvar        ! calculates the difference
               if (dabs(wk_temp2(i)).gt.1.d-8) then
                  q_temp(i)=(wk_temp1(i)-wk_temp2(i))/dabs(wk_temp2(i))
               else
c     print *, 'Small!',i,wk_temp2(i)
                  q_temp(i)=dabs(wk_temp1(i)-wk_temp2(i))
               end if
            end do

            write(*,*)'node',wk_temp1(ii),wk_temp2(ii),wk_temp3(ii)
            write(*,*)'plus',wk_temp1(ip),wk_temp2(ip),wk_temp3(ip)
            write(*,*)'minus',wk_temp1(im),wk_temp2(im),wk_temp3(im)
            write(*,*)'----'




c$$$            do i=1,nblks
c$$$               call frechetq(i,jmax(i),kmax(i),nmax,qdummy(lqptr(i)),
c$$$     &              dummy(lgptr(i)),indx(lgptr(i)),q_temp,wk_temp1,
c$$$     &              wk_temp2,jminbnd(i),jmaxbnd(i),kminbnd(i),
c$$$     &              kmaxbnd(i),xyj(lgptr(i)))
c$$$            end do

cc         end do
         end do
      end do

c     output

      do i=1,maxjk              ! ignores the jacobian matrix
         xyjtmp(i) = 1.d0
      enddo


      open ( unit=n_q, status='unknown' ) ! outputs solution vector 
      write(n_q) nblks
      write(n_q) (jbmax(i),kbmax(i),i=1,nblks)
      do i=1,nblks
         write(n_q) fsmach, alpha, re*fsmach, fsmach
         call writeq(n_q, qdummy(lqptr(i)), xyjtmp(lgptr(i)), jmax(i), 
     &        kmax(i),jminbnd(i), jmaxbnd(i), kminbnd(i), kmaxbnd(i))
      end do
      write(n_q) numiter
      rewind(n_q)

      if (turbulnt) then        ! outputs turbulence vector 
         open ( unit=n_t, status='unknown' )
         write(n_t) nblks
         write(n_t) (jbmax(i),kbmax(i),i=1,nblks)
         do i=1,nblks
            write(n_t) fsmach, alpha, re*fsmach, fsmach
            call writearr4(n_t, dummy(lgptr(i)), dummy(lgptr(i)),
     &           dummy(lgptr(i)), dummy(lgptr(i)), jmax(i),kmax(i),
     &           jminbnd(i), jmaxbnd(i), kminbnd(i), kmaxbnd(i))
         end do
         rewind(n_t)
      endif

 

      stop 'done with fr_test'

      return
      end           !fr_test
