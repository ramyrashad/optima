c----------------------------------------------------------------------
c     -- implicit boundary conditions --
c     -- impfar = farfield b.c. --      
c     -- impbody = solid wall b.c. --
c     -- based on probe (a. pueyo) --
c     -- m. nemec,  may 2000 --
c----------------------------------------------------------------------
      subroutine impbc (jdim, kdim, ndim, indx, icol, iex, q, xy, xyj,
     &      x, y, as, pa, imps, bcf, press)
      use disscon_vars
#include "../include/arcom.inc"

      integer jdim, kdim, ndim
      integer indx(jdim,kdim), icol(9), iex(jdim,kdim,4)

      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim), press(jdim,kdim)
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision bcf(jdim,kdim,4)

      logical imps

      if (viscous) then
c     -- viscous flow --
        call vimpfar (jdim, kdim, ndim, indx, icol, iex, q, xy, xyj, x,
     &        y, as, pa, imps, bcf, press)
        call vimpbody (jdim, kdim, ndim, indx, icol, iex, q, xy, xyj,
     &        as, pa, imps, bcf, press)
      else
c     -- inviscid flow --
         call impfar (jdim, kdim, indx, iex, q, xy, xyj, x, y, as, pa,
     &        imps, bcf)
         call impbody (jdim, kdim, indx, iex, q, xy, xyj, as, pa, imps,
     &        bcf)
      end if

c     -- explicit treatment --
c     -- bottom of wake cut - first three equations --
      if (imps .and. .not. periodic) then
         k = 1
         do j  = jbegin+1,jtail1-1
            do n = 1,3                 
               ii = ( indx(j,k) - 1 )*ndim + n
               jj = ( ii - 1 )*icol(9)
               if (clopt) jj = ( ii - 1 )*(icol(9)+1)
               jf = jmax-j+1
               as(jj+        n) =   1.d0*xyj(j,1)
               as(jj+icol(1)+n) = - 0.5d0*xyj(j,2)
               as(jj+icol(2)+n) = - 0.5d0*xyj(jf,2)
               
               jp = ( ii - 1 )*icol(5)
               pa(jp+        n) =   1.d0*xyj(j,1)
               pa(jp+icol(1)+n) = - 0.5d0*xyj(j,2)
               pa(jp+icol(2)+n) = - 0.5d0*xyj(jf,2)
            end do
         end do
      
c     -- LHS: fourth equation --
         call fourth (jbegin+1, jtail1-1, jdim, kdim, ndim, indx, icol,
     &        q, xyj, as, pa, bcf)

c     -- same for top of wake cut --
         do j  = jtail2+1,jend-1
            do n = 1,3
               ii = ( indx(j,k) - 1 )*ndim + n
               jj = ( ii - 1 )*icol(9)
               if (clopt) jj = ( ii - 1 )*(icol(9)+1)
               jf = jmax-j+1
               as(jj+        n) =   1.d0*xyj(j,1)
               as(jj+icol(1)+n) = - 0.5d0*xyj(j,2)
               as(jj+icol(2)+n) = - 0.5d0*xyj(jf,2)
               
               jp = ( ii - 1 )*icol(5)
               pa(jp+        n) =   1.d0*xyj(j,1)
               pa(jp+icol(1)+n) = - 0.5d0*xyj(j,2)
               pa(jp+icol(2)+n) = - 0.5d0*xyj(jf,2)
            end do
         end do
        
         call fourth (jtail2+1, jend-1, jdim, kdim, ndim, indx, icol, q,
     &        xyj, as, pa, bcf)
      end if

      return
      end                       ! impbc
c-----------------------------------------------------------------------      
      subroutine fourth(j1, j2, jdim, kdim, ndim, indx, icol, q, xyj,
     &      as, pa, bcf)
c-----------------------------------------------------------------------
      use disscon_vars
#include "../include/arcom.inc"

      integer j1, j2, jdim, kdim, ndim, indx(jdim,kdim), icol(9)
      double precision q(jdim,kdim,4), xyj(jdim,kdim)
clb
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
clb
      double precision bcf(jdim,kdim,4)

      do j = j1,j2

        k = 1
        ii = ( indx(j,k) - 1 )*ndim + 4
        jj = ( ii-1 )*icol(9)
        if (clopt) jj = ( ii - 1 )*(icol(9)+1)
        jp = ( ii-1 )*icol(5)

c     -- matrix for j,1 --
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        as(jj+1) = 0.5d0*(u**2+v**2)*gami*xyj(j,1)
        as(jj+2) = -u*gami*xyj(j,1)
        as(jj+3) = -v*gami*xyj(j,1)
        as(jj+4) = gami*xyj(j,1)
        
        pa(jp+1) = 0.5d0*(u**2+v**2)*gami*xyj(j,1)
        pa(jp+2) = -u*gami*xyj(j,1)
        pa(jp+3) = -v*gami*xyj(j,1)
        pa(jp+4) = gami*xyj(j,1)
        
c     -- matrix for j,2 --
        k = 2
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)       
        as(jj+icol(1)+1) = -.5d0*.5d0*(u**2+v**2)*gami*xyj(j,2)
        as(jj+icol(1)+2) = -.5d0*-u*gami*xyj(j,2)
        as(jj+icol(1)+3) = -.5d0*-v*gami*xyj(j,2)
        as(jj+icol(1)+4) = -.5d0*gami*xyj(j,2)
        
        pa(jp+icol(1)+1) = -.5d0*.5d0*(u**2+v**2)*gami*xyj(j,2)
        pa(jp+icol(1)+2) = -.5d0*-u*gami*xyj(j,2)
        pa(jp+icol(1)+3) = -.5d0*-v*gami*xyj(j,2)
        pa(jp+icol(1)+4) = -.5d0*gami*xyj(j,2)
                   
c     -- matrix for jf,2 --
        k = 2
        jf = jmax-j+1
        
        rho = q(jf,k,1)*xyj(jf,k)
        u   = q(jf,k,2)/q(jf,k,1)
        v   = q(jf,k,3)/q(jf,k,1)       
        as(jj+icol(2)+1) = -.5d0*.5d0*(u**2+v**2)*gami*xyj(jf,2)
        as(jj+icol(2)+2) = -.5d0*-u*gami*xyj(jf,2)
        as(jj+icol(2)+3) = -.5d0*-v*gami*xyj(jf,2)
        as(jj+icol(2)+4) = -.5d0*gami*xyj(jf,2)
        
        pa(jp+icol(2)+1) = -.5d0*.5d0*(u**2+v**2)*gami*xyj(jf,2)
        pa(jp+icol(2)+2) = -.5d0*-u*gami*xyj(jf,2)
        pa(jp+icol(2)+3) = -.5d0*-v*gami*xyj(jf,2)
        pa(jp+icol(2)+4) = -.5d0*gami*xyj(jf,2)
      
c     -- scaling --
        bcf(j,1,4) = 1.d0/as(jj+1)
      end do

      return
      end                       !fourth
