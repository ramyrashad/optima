      program asoms
c----------------------------------------------------------------------
c     -- Aerodynamic Shape Optimization Multi-block Setup --
c
c     -- read an amber2D single or multi-block grid --
c     -- define design variables --
c     -- modify airfoil shape for inverse design --
c     
c     written by: marian nemec 
c     date: april 2001
c     
c     -- no transfer of this program without explicit approval --
c----------------------------------------------------------------------
      implicit none

#include "mxjk.inc"
#include "units.inc"

      integer maxjk
      parameter (maxjk=maxj*maxk)
      double precision x(maxjk), y(maxjk)

      input_unit     = 10
      grid_unit      = 11
      new_grid_unit  = 12      
      dvs_unit       = 13
      bsp_unit       = 14
      btec_unit      = 15
      etec_unit      = 16
      con_unit       = 17
      stg_unit       = 18

      call input(x, y)

c     -- get list of blocks and sides for each element --
      call sortfoils(x, y)

c     -- get airfoil coordinates from grid --
      call setup(x, y)

c     -- b-spline airfoils and adjust grid -- 
      call modify(x, y)

      call output(x, y)

      end                       !asoms
