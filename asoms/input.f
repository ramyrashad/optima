c-----------------------------------------------------------------------
c     -- input all parameters --
c     -- m. nemec --
c-----------------------------------------------------------------------

      subroutine input(x, y)

      implicit none

#include "mxblk.inc"
#include "mxfoil.inc"
#include "mxseg.inc"
#include "mxjk.inc"
#include "bsord.inc"      
#include "bsp.inc"
#include "units.inc"

      integer maxjk
      parameter (maxjk=maxj*maxk)
      double precision x(maxjk), y(maxjk) 

      integer i, j, k, ltmp, ktmp, jtmp, ip1, i0, jjmp, ioerr
      integer jtranlo, jtranup, ios

      double precision xstart, xend 

      integer nblks, jbmax(maxblk), kbmax(maxblk)
      common/mblock/ nblks, jbmax, kbmax

      integer lgptr(maxblk)
      common/ptrs/ lgptr

      integer lblkpt(maxblk,4), lsidept(maxblk,4), idir(maxblk,4)
      integer ibctype(maxblk,4), ibcdir(maxblk,4)
      common/connect/ lblkpt, lsidept, idir, ibctype, ibcdir

      integer nfoils, nsegments(maxfoil), isegblk(maxsegs,maxfoil)
      integer isegside(maxsegs,maxfoil)
      common/sortedfoils/ nsegments, isegblk, isegside, nfoils
      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

c     stagnation point data as read in from 'grid.stg'         
      character*2 leorte, corner
      integer nstg, istgblk(maxblk), istgtyp(maxblk), istgcnr(maxblk)
      common/stgpt/nstg, istgblk, istgtyp, istgcnr

c     -- input parameters --

      namelist/inputs/ iorder, maxiter, tol, tecplot

      open ( unit=input_unit, file='grid.inp', status='unknown')

      read(input_unit,nml=inputs)

      if ( iorder.gt.ibsord ) then
         write (*,*) '** iorder greater than ibsord!! **'
         stop
      end if

      read(input_unit,*)
      read(input_unit,*) nfoils, nbs
      read(input_unit,*)

      write (*,10) nfoils
 10   format (/3x,'Number of airfoils:',i3)
      write (*,20) nbs
 20   format (3x,'Total number of b-spline curves:',i3)

      if ( nfoils.gt.maxfoil ) then
         write (*,*) '** nfoils greater than maxfoil!! **'
         stop
      end if

      if ( nbs.gt.maxbseg ) then
         write (*,*) '** nbs greater than mmaxbseg!! **'
         stop
      end if

      do i = 1,maxfoil
         nbseg(i) = 0
      end do

c     -- reading free-format parameters --
      do i=1,nbs
c     -- j = ifoil --
c     -- i0 = number of control points --
c     -- k = airfoil surface --
         read(input_unit,*) j, i0, k, xstart, xend
         nbseg(j) = nbseg(j) + 1
         nbcp(j,nbseg(j)) = i0
         isurf(j,nbseg(j)) = k
         bstart(j,nbseg(j)) = xstart
         bend(j,nbseg(j)) = xend
      end do

      open (unit=grid_unit, file='grid.g', status='unknown',
     &     form='unformatted')

      read(grid_unit) nblks
      read(grid_unit) (jbmax(i), kbmax(i),i=1,nblks)

      lgptr(1) = 1
      do i=1,nblks-1
        ltmp = jbmax(i)*kbmax(i)
        ip1 = i + 1
        lgptr(ip1) = lgptr(i) + ltmp
      end do

      do i=1,nblks
         i0 = lgptr(i)
         jjmp = jbmax(i)

c         if (i.gt.1) write(*,*)
c          write(*,30) i,nblks
c          write(*,40) jbmax(i),kbmax(i)
c          write(*,50) i0,jjmp
c 30       format('     reading block # ',i2,'  of ',i2)
c 40       format('     grid size:  j = ',i3,',   k = ',i3)
c 50       format('     pointers :  1st point = ',i6,',   j-jump =',i4)

          read(grid_unit) ((x(i0+(j-1)+k*jjmp),j=1,jbmax(i)),
     &         k=0,kbmax(i)-1),((y(i0+(j-1)+k*jjmp),j=1,jbmax(i)),
     &         k=0,kbmax(i)-1)

      end do

      ios = 0
      open(unit=con_unit,file='grid.con',iostat=ios,err=60,status='old')
      read(con_unit,*)
      read(con_unit,*)

      do k=1,nblks
         do j=1,4
            read(con_unit,*) ktmp, jtmp, lblkpt(k,j), lsidept(k,j),
     &           idir(k,j), ibctype(k,j), ibcdir(k,j),
     &           jtranlo, jtranup

            if((ktmp.ne.k).or.(jtmp.ne.j)) then
               write(*,*) 'connectivity read error'
               write(*,*) ' k=',k,' j=',j
               write(*,*) ' ktmp=',ktmp,' jtmp=',jtmp
               stop
            endif
         end do
      end do
      close(unit=con_unit)

 60   continue

      if (ios.ne.0) then
         write(*,*)'** error **'
         write(*,*)'file "grid.con" not found!'
         stop
      endif

c     -- read in stagnation point specification file --

      i = 1
      ioerr = 0
      ios = 0

      open(unit=stg_unit,file='grid.stg',iostat=ios,err=70,status='old')

      read(stg_unit,*)
      read(stg_unit,*)
      read(stg_unit,*)
 80   read(stg_unit,100,end=90) istgblk(i), corner, leorte

      if (leorte.eq.'LE') istgtyp(i) = 1
      if (leorte.eq.'TE') istgtyp(i) = -1
      if (corner.eq.'LR') istgcnr(i) = 1
      if (corner.eq.'LL') istgcnr(i) = 2
      if (corner.eq.'UL') istgcnr(i) = 3
      if (corner.eq.'UR') istgcnr(i) = 4

      if (istgblk(i).gt.nblks) then
        write(*,*) '** error **'
        write(*,*) 'incorrect format in file "tornado.stg"'
        write(*,*) 'block number exceeds total number in grid!'
        ioerr = 1
      elseif(istgblk(i).le.0) then
        write(*,*) '** error **'
        write(*,*) 'incorrect format in file "tornado.stg"'
        write(*,*) 'block number specified is <= zero!'
        ioerr = 2
      endif
      if (istgcnr(i).lt.1.or.istgcnr(i).gt.4) then
        write(*,*) '** error **'
        write(*,*) 'incorrect format in file "tornado.stg"'
        write(*,*) 'corner type must appear in columns 12 & 13, and'
        write(*,*) 'be in upper case (i.e., "ul","ll","ur", or "lr").'
        ioerr = 3
      endif
      if (istgtyp(i).ne.-1.and.istgtyp(i).ne.1) then
        write(*,*) '** error **'
        write(*,*) 'incorrect format in file "tornado.stg"'
        write(*,*) 'stagnation type must appear in columns 22 & 23,'
        write(*,*) 'and be either "le" or "te".'
        ioerr = 4
      endif

      if (ioerr.ne.0) stop
      i = i + 1
      goto 80
c     
 70   continue

      if (ios.ne.0) then
        write(*,*) '** error **'
        write(*,*) 'file not found!'
        stop
      endif
c     
 90   continue
      close(stg_unit)

      nstg = i - 1

 100   format(t2,i2,t12,a2,t22,a2)
        
c     -- reorder the stagnation point data so that le's fall on either
c     the ul (upper left)  or ll (lower left) corners and te's on
c     ur or lr corners (it makes coding simpler later!) --
      call orderstg

      write (*,110) nstg
 110  format (/3x,'Number of stagnation points:',i3)
      write (*,120) 
 120  format (/3x,'Block #',3x,'Corner',3x,'Type')
      do i = 1,nstg
         write (*,130) istgblk(i), istgcnr(i), istgtyp(i) 
      end do
 130  format (4x,i3,7x,i3,5x,i3)

c     -- tecplot output --
      if (tecplot) then
         open (unit=btec_unit,file='airfit.tec')
         write (btec_unit,140)
 140     format ('VARIABLES= x, y')
      end if

      return
      end                       !input

