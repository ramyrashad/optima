C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C   RRRRR   W     W  BBBB   SSSS   EEEEEE  TTTTTTT 
C   R    R  W  W  W  B   B S       E          T    
C   RRRRR   W W W W  BBBB   SSSS   EEEEE      T    
C   R    R  WW   WW  B   B      S  E          T    
C   R    R  W     W  BBBB   SSSS   EEEEEE     T    
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE RWBEST(IPOP,ndim,nchrom,nobfun,ngene,pop,fit,irank,
     $     NPI,IPARM,NPR,PARMR,NPL,PARML,fitmin,fitmax,
     $     io_34,fit_min_max,ga_file_prefix,
     $     arc2d_io,field_pop,field_fit,field_log,
     $     field_ngene,field_nobfun)
      LOGICAL FILEX,io_34,parml,fit_min_max,arc2d_io
      character*80 ga_file_prefix
      integer strlen
      character*80 filena
      character*40 field_pop(ngene),field_fit(nobfun),field_log,
     $     field_ngene,field_nobfun
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** THIS SUBROUTINE READS AND WRITES THE best chromo info   C
C              USING THE FILE NAME                                     C
C              accum.dat.   ***                                        C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      dimension fit(nobfun,ndim), pop(ngene,ndim)
      dimension irank(nchrom)
      DIMENSION IPARM(20,nchrom), PARMR(20,nchrom), PARML(10,nchrom)
      dimension fitmin(10),fitmax(10)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
c$$$      INQUIRE (FILE='accum.dat',EXIST=FILEX)
c$$$      IF (.NOT.FILEX) THEN
c$$$         OPEN (50,FILE='accum.error',STATUS='UNKNOWN',
c$$$     $        FORM='FORMATTED')
c$$$         WRITE (50,2001)
c$$$         close(50)
c$$$         WRITE (*,2001)
c$$$         if(io_34)WRITE (34,2001)
c$$$ 2001    FORMAT ('RWBEST: CANNOT FIND accum.dat',
c$$$     1        ', EXECUTION TERMINATED'/)
c$$$         STOP
c$$$      ENDIF
C
C-----------------------------------------------------------------------
C          *** OPEN FILE AND EXECUTE READ ***
C
      namelen =  strlen(ga_file_prefix)
      filena = ga_file_prefix
      filena(namelen+1:namelen+7) = '.accum'
c$$$      OPEN (4,FILE='accum.dat',STATUS='UNKNOWN',FORM='FORMATTED')
      OPEN (4,FILE=filena,STATUS='UNKNOWN',FORM='FORMATTED')
      IND = 1
      DO NC=1,NCHROM
         IF(IRANK(NC).EQ.1)THEN
            if(arc2d_io)then
               WRITE (4,'(2(8X,I6))') IPOP,NC
               WRITE (4,'(8X,I6)') NGENE
               WRITE (4,'(3((2X,E24.16)))')(POP(NG,NC),NG=1,NGENE)
               WRITE (4,'(8X,I6)') NOBFUN
               WRITE (4,'(3((2X,E24.16)))')(FIT(NO,NC),NO=1,NOBFUN)
               WRITE (4,'(8x,i6)') NPI
               IF (NPI.GT.0) THEN
                  WRITE (4,'(3((2x,i8)))') (IPARM(NN,nc),NN=1,NPI)
               ENDIF
               WRITE (4,'(8x,i6)') NPR
               IF (NPR.GT.0) THEN
                  WRITE (4,'(3((2x,e24.16)))') (PARMR(NN,nc),NN=1,NPR)
               ENDIF
               WRITE (4,'(8x,i6)') NPL
               IF (NPL.GT.0) THEN
                  WRITE (4,'(8((2x,L8)))') (PARML(NN,nc),NN=1,NPL)
               ENDIF
               if(fit_min_max)then
                  WRITE (4,'(3((2x,e24.16)))') (fitmin(NN),NN=1,Nobfun)
                  WRITE (4,'(3((2x,e24.16)))') (fitmax(NN),NN=1,Nobfun)
               endif
            else
               WRITE (4,'(2(8X,I6))') IPOP,NC
               Write(4,'(2x,a40,i8)')field_ngene,ngene
               do ng = 1,ngene
                  write(4,'(2x,a40,e16.8)')field_pop(ng),pop(ng,nc)
               enddo
               write(4,'(2x,a40,i8)')field_nobfun,nobfun
               do nf = 1,nobfun
                  write(4,'(2x,a40,e16.8)')field_fit(nf),fit(nf,nc)
               enddo
            endif
            go to 1000
         ENDIF
      ENDDO
 1000 CLOSE (4)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
