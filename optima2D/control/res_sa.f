c----------------------------------------------------------------------
c     -- modified S-A model residual equations --
c     written by: marian nemec
c     modified by Markus Rumpfkeil
c     modified by Ramy Rashad
c     date: jan. 2000
c----------------------------------------------------------------------

      subroutine res_sa(jdim, kdim, q, rsa, rsa0, x, y, xy, xyj, uu, vv,
     &      fmu, vort, workq, work)

      use disscon_vars

      implicit none

#include "../include/arcom.inc"
#include "../include/sam.inc"

      integer jdim,kdim,j,k,jp1,jm1,jj,kp1,km1,jsta,jsto,n

      double precision rho,u,v,t0,t1,t2,tnum,tnu,tnup,ts,chi,chi2,chi3
      double precision fv1,fv2,fv3,fw,r,g,rinv,pr,ds,trep,trem,vn
      double precision xy1p,xy2p,xy1m,xy2m,xy3p,xy4p,xy3m,xy4m,ttp,ttm
      double precision xy3,xy4,par,cam,cap,cdm,cdp,cnud,rsa_norm

      double precision q(jdim,kdim,5), rsa(jdim,kdim,5)
      double precision rsa0(jdim,kdim,5), rsa_dbc
      double precision xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision fmu(jdim,kdim), uu(jdim,kdim), vv(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim), vort(jdim,kdim)
      double precision workq(jdim,kdim,3), work(jdim,kdim)

cjd
      double precision delta_u(jdim,kdim), trip(jdim,kdim)
      double precision omega_tlo, omega_tup
      double precision g_t, factor, ft1, ft2
cjd/

c     -- frozen jacobian arrays --
      double precision c2xf(maxj,maxk), c4xf(maxj,maxk)
      double precision c2yf(maxj,maxk), c4yf(maxj,maxk)
      double precision fmuf(maxj,maxk), turmuf(maxj,maxk)
      double precision vortf(maxj,maxk), sapdf(maxj,maxk)
      double precision sadx(maxj,maxk), sady(maxj,maxk)
      double precision suuf(maxj,maxk), svvf(maxj,maxk)
      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &      sapdf, sadx, sady, suuf, svvf 

      ! _____________________
      ! Function declarations
      double precision linearInterp

      reinv = 1.d0/re
      resiginv = sigmainv/re

      do k=kbegin,kend
        do j=jbegin,jend
          rho = q(j,k,1)*xyj(j,k)
          u   = q(j,k,2)/q(j,k,1)
          v   = q(j,k,3)/q(j,k,1)
          
          workq(j,k,1)=rho
          workq(j,k,2)=u
          workq(j,k,3)=v
        end do
      end do

c     -- calculate vorticity at node j,k --
      call vort_o2 (jdim, kdim, workq, xy, vort)

      if (frozen .and. ( ifrz_what.eq.1 .or. ifrz_what.eq.3)) then
         do k = 1,kend
            do j = 1,jend
               vort(j,k) = vortf(j,k)
            end do
         end do
      end if  

c     -- advective terms in xi direction --
      do k = klow,kup
        do j = jlow,jup
          t0 = dsign(1.d0, uu(j,k))
          t1 = 0.5d0*(1.d0 + t0)
          t2 = 0.5d0*(1.d0 - t0)

          tnum = q(jminus(j),k,5)*xyj(jminus(j),k)
          tnu  = q(j,k,5)*xyj(j,k)
          tnup = q(jplus(j),k,5)*xyj(jplus(j),k)

c     -- S-A residual: storing -R(Q) for Newton's method --
          if (dissCon) then   
             
             !-- Calculate an additional dissipation term to add to
             !-- the advective term of the turbulence model residual
             !-- equation
             rsa_dbc = 0.5*dabs(uu(j,k))/xyj(j,k)*(-tnum + 2*tnu - tnup)
             
             rsa0(j,k,5) = - uu(j,k)/xyj(j,k)*( t1*( tnu - tnum ) +
     &          t2*( tnup - tnu ) )
             rsa(j,k,5) = -(
     &            ( uu(j,k)/xyj(j,k)*( t1*( tnu - tnum ) +
     &          t2*( tnup - tnu ) ) ) + lamDissT*rsa_dbc )

          else
             rsa(j,k,5) = - uu(j,k)/xyj(j,k)*( t1*( tnu - tnum ) +
     &          t2*( tnup - tnu ) )
          end if
        end do
      end do



c     -- advective terms in eta direction --
      do k = klow,kup
        do j = jlow,jup
          t0 = dsign(1.d0, vv(j,k))
          t1 = 0.5d0*(1.d0 + t0)
          t2 = 0.5d0*(1.d0 - t0)

          tnum = q(j,k-1,5)*xyj(j,k-1)
          tnu  = q(j,k,5)*xyj(j,k)
          tnup = q(j,k+1,5)*xyj(j,k+1)

c     -- S-A residual: storing -R(Q) for Newton's method --
          if (dissCon) then
           
             !-- Calculate an additional dissipation term to add to
             !-- the advective term of the turbulence model residual
             !-- equation
             rsa_dbc = 0.5*dabs(vv(j,k))/xyj(j,k)*(-tnum + 2*tnu - tnup)

             rsa0(j,k,5) = rsa0(j,k,5) - vv(j,k)/xyj(j,k)*
     &        ( t1*( tnu - tnum) + t2*( tnup - tnu ) )
             rsa(j,k,5) = rsa(j,k,5) - (
     &            ( vv(j,k)/xyj(j,k)*( t1*( tnu - tnum ) +
     &          t2*( tnup - tnu ) ) ) + lamDissT*rsa_dbc )
          else
             rsa(j,k,5) = rsa(j,k,5) - vv(j,k)/xyj(j,k)*
     &        ( t1*( tnu - tnum) + t2*( tnup - tnu ) )
          end if

        end do
      end do

c     *************************************************
c       Preliminary Calculations Before  Model
c       Copied from Spalart.f(Cyclone) with changes
c     *************************************************

      do k=klow,kup
        do j=jlow,jmid                  
          delta_u(j,k)=max(1.d-50,
     &          sqrt((workq(j,k,2)-workq(jtranlo,1,2))**2
     &          +(workq(j,k,3)-workq(jtranlo,1,3))**2))
        enddo
        do j=jmid+1,jup
          delta_u(j,k)=max(1.d-50,
     &          sqrt((workq(j,k,2)-workq(jtranup,1,2))**2
     &          +(workq(j,k,3)-workq(jtranup,1,3))**2))
        enddo
      enddo

      ! ___________________________________________________
      ! interpolate for the vorticity at the trip locations
      omega_tlo = linearInterp(x(jtranlo  ,1), vort(jtranlo  ,1),
     &                         x(jtranlo-1,1), vort(jtranlo-1,1),
     &                         translo)
      omega_tup = linearInterp(x(jtranup  ,1), vort(jtranup  ,1),
     &                         x(jtranup+1,1), vort(jtranup+1,1),
     &                         transup)
      omega_tlo=max(1.d-50,omega_tlo)       
      omega_tup=max(1.d-50,omega_tup)

      do k=klow,kup
         do  j=jlow,jmid                  
             g_t=min(0.1,delta_u(j,k)/(omega_tlo*delta_xlo))
             factor=(ct2*(omega_tlo/delta_u(j,k))**2
     &          *(smin(j,k)**2+(g_t*d_trip(j,k))**2))
             if (factor.le.103.0) then
c-----------------------------------------------------------
                if (.not.transitionalFlow .or. flagTPsAsZero) then
                   ft1 = 0.d0 ! since approaching exp(-infinity)
                else
                   ft1=ct1*g_t*exp(-factor)
                endif
c----------------------------------------------------------
                trip(j,k)=ft1*re*delta_u(j,k)**2
             else
                trip(j,k)=0.d0
             endif
                         
         enddo

         do j=jmid+1,jup
             g_t=min(0.1,delta_u(j,k)/(omega_tup*delta_xup))
             factor=(ct2*(omega_tup/delta_u(j,k))**2
     &          *(smin(j,k)**2+(g_t*d_trip(j,k))**2))
             if (factor.le.103.0) then

c-----------------------------------------------------------
                if (.not.transitionalFlow .or. flagTPsAsZero) then
                   ft1 = 0.d0 ! since approaching exp(-infinity)
                else
                   ft1=ct1*g_t*exp(-factor)
                endif
c----------------------------------------------------------
 
                 trip(j,k)=ft1*re*delta_u(j,k)**2
                                 
             else
                 trip(j,k)=0.d0
             endif
          enddo
       enddo


c     -- calculate production and destruction terms --
      do k = klow,kup
        do j = jlow,jup
c     -- nu_tilde --
          tnu = q(j,k,5)*xyj(j,k)
c     -- chi = = tnu*rho/mul --
          chi = tnu*q(j,k,1)*xyj(j,k)/fmu(j,k)
cjd
c     -- chi**2
          chi2 = chi*chi
cjd/
c     -- chi**3 --
          chi3 = chi**3
c     -- t1 = 1/(k**2.d**2)
          t1 = (akarman*smin(j,k))**(-2)
          fv1 = chi3/(chi3+cv1_3)
          if (.not. originalSA) then
             fv2 = (1.d0+chi/cv2)**(-3)
             fv3=(1+chi*fv1)*(1-fv2)/chi
c         -- S_tilde --
             ts = vort(j,k)*re*fv3 + tnu*t1*fv2
          else             
             fv2 = 1.d0 - chi/(1+chi*fv1)
             ts = vort(j,k)*re + tnu*t1*fv2
          end if
          
c     if (j.eq.110 .and. k.eq.4) write (*,*) 'ts_rsa',ts 
          r = tnu*t1/ts

          if (dabs(r).ge.10.d0) then
            fw = const
          else
            g = r + cw2*(r**6-r)
            fw = g*( (1.d0+cw3_6)/(g**6+cw3_6) )**expon
          endif

          rinv = 1.d0/xyj(j,k)

          t1 = tnu/smin(j,k)

cjd
          if (.not.transitionalFlow .or. flagTPsAsZero) then
             !-- no trip terms
             !-- production term
             pr = rinv*cb1*reinv*ts*tnu
             !-- destruction term
             ds = rinv*cw1*reinv*fw*t1*t1

          else 
             !-- include trip terms
             ft2 = ct3*exp(-ct4*chi2)
             !-- production term
             pr  = rinv*(cb1*reinv*ts*tnu*(1 - ft2)
     &            + trip(j,k))
             !-- destruction term
             ds = rinv*reinv*t1*t1*(cw1*fw-cb1*ft2/(akarman**2))
          endif
cjd/

          ! Note that the trip term for transition has been incorporated
          ! into the production term

          if (dissCon) then
             rsa0(j,k,5) = rsa0(j,k,5) + pr - ds
             rsa(j,k,5) = rsa(j,k,5) + (pr - ds)
          else
             rsa(j,k,5) = rsa(j,k,5) + pr - ds
          end if      
        end do
      end do

c     -- work = nu_laminar + nu_turbulent --
      do k = kbegin,kend
        do j = jbegin,jend
          work(j,k) = fmu(j,k)/( q(j,k,1)*xyj(j,k) ) + q(j,k,5)*xyj(j,k)
        end do
      end do


c     -- diffusion terms in xi direction --
      do k = klow,kup
        do j = jlow,jup
          jp1 = jplus(j)
          jm1 = jminus(j)

          xy1p = 0.5d0*( xy(j,k,1) + xy(jp1,k,1) )
          xy2p = 0.5d0*( xy(j,k,2) + xy(jp1,k,2) )
          ttp  = ( xy1p*xy(j,k,1) + xy2p*xy(j,k,2) )
          
          xy1m = 0.5d0*( xy(j,k,1) + xy(jm1,k,1) )
          xy2m = 0.5d0*( xy(j,k,2) + xy(jm1,k,2) )
          ttm  = ( xy1m*xy(j,k,1) + xy2m*xy(j,k,2) )
          
          rinv = 1.d0/xyj(j,k)

          cnud = cb2*resiginv*work(j,k)*rinv
          
          cdp       =    ttp*cnud
          cdm       =    ttm*cnud

          trem = 0.5d0*( work(jm1,k) + work(j,k) )
          trep = 0.5d0*( work(j,k) + work(jp1,k) )

          cap = ttp*trep*(1.0+cb2)*resiginv*rinv
          cam = ttm*trem*(1.0+cb2)*resiginv*rinv

          tnum = q(jm1,k,5)*xyj(jm1,k)
          tnu  = q(j,k,5)*xyj(j,k)
          tnup = q(jp1,k,5)*xyj(jp1,k)

          if (dissCon) then
             rsa0(j,k,5) = rsa0(j,k,5) + cap*(tnup - tnu) - cam*( tnu -
     &          tnum ) - cdp*( tnup - tnu ) + cdm*( tnu - tnum )
             rsa(j,k,5) = rsa(j,k,5) + 
     &            ( cap*( tnup - tnu ) - cam*( tnu -
     &          tnum ) - cdp*( tnup - tnu ) + cdm*( tnu - tnum ) )
          else 
             rsa(j,k,5) = rsa(j,k,5) + cap*( tnup - tnu ) - cam*( tnu -
     &          tnum ) - cdp*( tnup - tnu ) + cdm*( tnu - tnum )
          end if
        end do
      end do   

c     -- diffusion terms in eta direction --
      do k = klow,kup
        kp1 = k+1
        km1 = k-1
        do j = jlow,jup
          xy3p = 0.5d0*( xy(j,k,3) + xy(j,kp1,3) )
          xy4p = 0.5d0*( xy(j,k,4) + xy(j,kp1,4) )
          ttp  = ( xy3p*xy(j,k,3) + xy4p*xy(j,k,4) )
          
          xy3m = 0.5d0*( xy(j,k,3) + xy(j,km1,3) )
          xy4m = 0.5d0*( xy(j,k,4) + xy(j,km1,4) )
          ttm  = ( xy3m*xy(j,k,3) + xy4m*xy(j,k,4) )

          rinv = 1.d0/xyj(j,k)

          cnud = cb2*resiginv*work(j,k)*rinv
          
          cdp = ttp*cnud
          cdm = ttm*cnud
          
          trem = 0.5d0*(work(j,km1)+work(j,k))
          trep = 0.5d0*(work(j,k)+work(j,kp1))
          
          cap  =  ttp*trep*(1.0+cb2)*resiginv*rinv
          cam  =  ttm*trem*(1.0+cb2)*resiginv*rinv
          
          tnum = q(j,km1,5)*xyj(j,km1)
          tnu  = q(j,k,5)*xyj(j,k)
          tnup = q(j,kp1,5)*xyj(j,kp1)

          if (dissCon) then
             rsa0(j,k,5) = rsa0(j,k,5) + cap*(tnup - tnu) - cam*( tnu -
     &            tnum ) - cdp*( tnup - tnu ) + cdm*( tnu - tnum )
             rsa(j,k,5) = rsa(j,k,5) + 
     &            ( cap*( tnup - tnu ) - cam*( tnu -
     &            tnum ) - cdp*( tnup - tnu ) + cdm*( tnu - tnum ) )
          else
             rsa(j,k,5) = rsa(j,k,5) + cap*( tnup - tnu ) - cam*( tnu -
     &            tnum ) - cdp*( tnup - tnu ) + cdm*( tnu - tnum )
          end if
        end do
      end do

c     -- boundary conditions --

c     -----------------------------------------------------------------
c     -- airfoil body => J^(-1).nu_tilde = 0 --
c     -----------------------------------------------------------------
      
      do j = jtail1, jtail2
         if (.not. sbbc .or. (sbbc.and.(j.lt.jsls.or.j.gt.jsle)) ) then
            if (dissCon) then
               rsa0(j,kbegin,5) = - q(j,kbegin,5)*xyj(j,kbegin)
               rsa(j,kbegin,5) = 
     &              -(  q(j,kbegin,5)*xyj(j,kbegin)
     &              + lamDissT*(q(j,kbegin,5)*xyj(j,kbegin) - 1.d0) )
            else
               rsa(j,kbegin,5) = - q(j,kbegin,5)*xyj(j,kbegin)
            end if
         else
            rsa(j,kbegin,5) = - ( q(j,kbegin,5)*xyj(j,kbegin)- retinf )
         end if
      end do

c     -----------------------------------------------------------------
c     -- outer-boundary  k = kmax --
c     -----------------------------------------------------------------

      if(.not.periodic) then
         jsta=jbegin+1
         jsto=jend-1
      else
         jsta=jbegin
         jsto=jend
      end if

      do j = jsta,jsto
c     -- metrics terms --
        par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
        xy3 = xy(j,kend,3)/par
        xy4 = xy(j,kend,4)/par

        u   = q(j,kend,2)/q(j,kend,1)
        v   = q(j,kend,3)/q(j,kend,1)
        vn = xy3*u + xy4*v

        if (vn.le.0.d0) then
c     -- inflow --
          if (dissCon) then 
             rsa0(j,kend,5) = -( q(j,kend,5)*xyj(j,kend) - retinf )
             rsa(j,kend,5) = -( q(j,kend,5)*xyj(j,kend) - retinf )
          else
             rsa(j,kend,5) = -( q(j,kend,5)*xyj(j,kend) - retinf )
          end if
        else
c     -- outflow -- 
          if (dissCon) then 
             rsa0(j,kend,5) = 
     &            -( q(j,kend,5)*xyj(j,kend) - q(j,kup,5)*xyj(j,kup) ) 
             rsa(j,kend,5) = 
     &            -( q(j,kend,5)*xyj(j,kend) - q(j,kup,5)*xyj(j,kup) )
          else
             rsa(j,kend,5) = 
     &            -( q(j,kend,5)*xyj(j,kend) - q(j,kup,5)*xyj(j,kup) )
          end if
        endif
      end do

      if(.not.periodic) then

c     -----------------------------------------------------------------
c     -- j = 1 and j = jend boundary --
c     -----------------------------------------------------------------
      do k = kbegin,kend
        if (dissCon) then 
           rsa0(1,k,5) = -( q(1,k,5)*xyj(1,k) - q(2,k,5)*xyj(2,k) )
           rsa0(jend,k,5) = -( q(jend,k,5)*xyj(jend,k) - q(jup,k,5)
     &        *xyj(jup,k) )
           rsa(1,k,5) = -( q(1,k,5)*xyj(1,k) - q(2,k,5)*xyj(2,k) )
           rsa(jend,k,5) = -( q(jend,k,5)*xyj(jend,k) - q(jup,k,5)
     &        *xyj(jup,k) )
        else
           rsa(1,k,5) = -( q(1,k,5)*xyj(1,k) - q(2,k,5)*xyj(2,k) )
           rsa(jend,k,5) = -( q(jend,k,5)*xyj(jend,k) - q(jup,k,5)
     &        *xyj(jup,k) )
        end if
      end do

c     -----------------------------------------------------------------
c     -- wake-cut --
c     -----------------------------------------------------------------
      do j = jbegin+1,jtail1-1
        jj = jmax - j + 1
        if (dissCon) then
           rsa0(j,1,5)  = -( q(j,1,5)*xyj(j,1) - 0.5d0*(q(j,2,5)
     &          *xyj(j,2) + q(jj,2,5)*xyj(jj,2) ) )  
           rsa0(jj,1,5)  = -( q(jj,1,5)*xyj(jj,1) - 0.5d0*(q(j,2,5)
     &          *xyj(j,2) + q(jj,2,5)*xyj(jj,2) ) )
           rsa(j,1,5)  = -( q(j,1,5)*xyj(j,1) - 0.5d0*(q(j,2,5)
     &          *xyj(j,2) + q(jj,2,5)*xyj(jj,2) ) )  
           rsa(jj,1,5)  = -( q(jj,1,5)*xyj(jj,1) - 0.5d0*(q(j,2,5)
     &          *xyj(j,2) + q(jj,2,5)*xyj(jj,2) ) )
        else
           rsa(j,1,5)  = -( q(j,1,5)*xyj(j,1) - 0.5d0*(q(j,2,5)
     &          *xyj(j,2) + q(jj,2,5)*xyj(jj,2) ) )  
           rsa(jj,1,5)  = -( q(jj,1,5)*xyj(jj,1) - 0.5d0*(q(j,2,5)
     &          *xyj(j,2) + q(jj,2,5)*xyj(jj,2) ) )
        end if
      end do

      end if !not periodic

      return
      end                       !res_sa
