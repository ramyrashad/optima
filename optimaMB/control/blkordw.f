c     ------------------------------------------------------------------
c     -- double bandwidth node order: wake blocks for c-mesh  --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine blkordw (js, je, ks, ke, ksn, ken, jmax, kmax,
     &     indx, jmaxn, kmaxn, indxn, lastblk)

      implicit none
      integer js, je, ks, ke, ksn, ken, jmax, kmax,jmaxn, kmaxn,lastblk
      integer kc,j,k
      integer indx(jmax,kmax), indxn(jmaxn,kmaxn)

      kc = 0

      do j = js,je,1
         
         do k = ke,ks,-1
            kc = kc+1
            indx(j,k) = kc + lastblk
c     write (*,*) j,k,indx(j,k) 
         end do

         lastblk = indx(j,ks)
         kc = 0
c     write (*,*) 'lastblk',lastblk

         do k = ksn,ken,1
            kc = kc+1
            indxn(jmax-j+1,k) = kc + lastblk
c     write (*,*) j,k,jmax-j+1,indxn(jmax-j+1,k)
         end do
         
         lastblk = indxn(jmax-j+1,ken)
         kc = 0
c     write (*,*) 
      end do

      return
      end                       ! blkordw
