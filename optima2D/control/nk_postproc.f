      subroutine nk_postproc(jdim,kdim,ndim,qp,q,qold)
c
#include "../include/arcom.inc"
c
      double precision qp(jdim,kdim,ndim),q(jdim,kdim,ndim)
      double precision qold(jdim,kdim,ndim),qtmp

      if (jesdirk.eq.3 .or. jesdirk.eq.4)then

         do 7 k=kbegin,kend
         do 7 j=jbegin,jend
         do 7 n=1,ndim
            qold(j,k,n)=q(j,k,n)
            q(j,k,n)=qp(j,k,n)
c           and use constant extrapolation for the next physical time step qp=q
 7       continue


      else
c      
         do 8 k=kbegin,kend
         do 8 j=jbegin,jend
         do 8 n=1,4
            qtmp=qold(j,k,n)
            qold(j,k,n)=q(j,k,n)
            q(j,k,n)=qp(j,k,n)
c           quadratic extrapolation for the next physical time step
            qp(j,k,n)=3*q(j,k,n)-3*qold(j,k,n)+qtmp
 8       continue

         if (ndim.eq.5) then
            do 9 k=kbegin,kend
            do 9 j=jbegin,jend
               qold(j,k,5)=q(j,k,5)
               q(j,k,5)=qp(j,k,5)
c           and use constant extrapolation for the next physical time step qp=q
 9          continue
         end if

      end if   !jesdirk.eq.3 .or. jesdirk.eq.4

      return
      end

