      subroutine mip2(jdim,kdim,jmax0,kmax0,jmaxp,kmaxp,                
     >                q0,x0,y0,q1,x1,y1,turtmp,conser,ctof,qint)
c
#include "../include/arcom.inc"
c      include 'parm.inc'
      dimension q0(jdim,kdim,4)                                         
      dimension x0(jdim,kdim),y0(jdim,kdim)                             
                                                                        
      dimension q1(jdim,kdim,4),turtmp(jdim,kdim)
      dimension x1(jdim,kdim),y1(jdim,kdim)                             
                                                                        
      dimension xf(maxj),yf(maxj),zf(maxj),xc(maxj),yc(maxj)
      dimension zc(maxj),q01(maxj),q02(maxj),q03(maxj)       
      dimension q04(maxj),q11(maxj),q12(maxj),q13(maxj)      
      dimension q14(maxj),tur1(maxj),tur0(maxj),work(maxj,14) 

      common/worksp/ xf,yf,zf,xc,yc,zc,q01,q02,q03,           
     >               q04,q11,q12,q13,q14,tur1,tur0,work 
                                                                        
      logical   ctof,qint,conser                                        
c                                                                       
c     2d version                                                       
c     Last modified by Stan De Rango April 1/98 to include interpolation
c     of turbulence quantities from coarse to fine grid for Spalart-
c     Allmaras model (ie. turre).
c                                                                       
      ldim  = 1                                                         
      lmax0 = 1                                                         
      lmaxp = 1                                                         
      ndim  = 4                                                         
c                                                                       
      time = 0                                                          
                                                                        
      write (out_unit,*) 'Fine grid dimensions are ',jmax0,kmax0 
      write (out_unit,*) 'Coarse grid dimensions   ',jmaxp,kmaxp 

      if(ctof)then                                                      
c        *************************************************************
c                    coarse to fine grid interpolation         
c        *************************************************************
        write (out_unit,*)
     &        'Starting coarse to fine interpolation processing...'
                                                                        
         call decode(jdim,kdim,ldim,ndim,q1,jmaxp,kmaxp,lmaxp,conser)    
c
c        calculate strides in j                                     
c                                                                       
         jstride = (jmax0-1)/(jmaxp-1)                                   
         write(out_unit,*)' jstride = ',jstride   
c                                                                       
c        put coarse solution on fine mesh (interpolated in k)       
c                                                                       
         if (itmodel.eq.2) then
           do 5 k=kbegin,kend
           do 5 j=jbegin,jend
             turtmp(j,k)=turre(j,k)
 5         continue
         endif
c
c        -interpolate in eta direction for every jstride xi line
         do 50 j=1,jmax0,jstride                                         
            jj = (j-1)/jstride + 1                                          
c                                                                       
            do 30 k=1,kmax0                                                
               xf(k) = x0(j,k)                                  
               yf(k) = y0(j,k)                                
               zf(k) = 0.0                           
 30         continue                                                       
                                                                        
            call intg(kmax0,xf,yf,zf,kmaxp,xc,yc,zc)                       
                                                                        
            do 35 k=1,kmaxp                                                
               q11(k) = q1(jj,k,1)                              
               q12(k) = q1(jj,k,2)                                
               q13(k) = q1(jj,k,3)                             
               q14(k) = q1(jj,k,4)
               tur1(k)= turtmp(jj,k)
 35         continue                                                       
                                                                        
            call intq(kmaxp,xc,yc,zc,q11,kmax0,xf,yf,zf,q01)               
            call intq(kmaxp,xc,yc,zc,q12,kmax0,xf,yf,zf,q02)               
            call intq(kmaxp,xc,yc,zc,q13,kmax0,xf,yf,zf,q03)               
            call intq(kmaxp,xc,yc,zc,q14,kmax0,xf,yf,zf,q04)               
            if (itmodel.eq.2) 
     &      call intq(kmaxp,xc,yc,zc,tur1,kmax0,xf,yf,zf,tur0)   
                                                                        
            do 40 k=1,kmax0                                                
               q0(j,k,1) = q01(k)                                 
               q0(j,k,2) = q02(k)                               
               q0(j,k,3) = q03(k)                        
               q0(j,k,4) = q04(k)
               turre(j,k) = tur0(k)
 40         continue                                                       
 50      continue        
                                                                        
                                                                        
c        -interpolate in xi direction for each eta line
         do 100 k=1,kmax0                                                
            do 60 j=1,jmax0                                                 
               xf(j) = x0(j,k)                                   
               yf(j) = y0(j,k)                          
               zf(j) = 0.0                               
 60         continue
c
            do 70 j=1,jmax0,jstride                                         
               jj = (j-1)/jstride + 1                                    
               xc(jj) = x0(j,k)                        
               yc(jj) = y0(j,k)                  
               zc(jj) = 0.0                            
               q11(jj) = q0(j,k,1)                       
               q12(jj) = q0(j,k,2)                   
               q13(jj) = q0(j,k,3)                 
               q14(jj) = q0(j,k,4)
               tur1(jj)= turre(j,k)
 70         continue                                                        
                                                                        
            call intq(jmaxp,xc,yc,zc,q11,jmax0,xf,yf,zf,q01)               
            call intq(jmaxp,xc,yc,zc,q12,jmax0,xf,yf,zf,q02)               
            call intq(jmaxp,xc,yc,zc,q13,jmax0,xf,yf,zf,q03)               
            call intq(jmaxp,xc,yc,zc,q14,jmax0,xf,yf,zf,q04)               
            if (itmodel.eq.2)
     &      call intq(jmaxp,xc,yc,zc,tur1,jmax0,xf,yf,zf,tur0)
c               
            do 80 j=1,jmax0                                                
               q0(j,k,1) = q01(j)                      
               q0(j,k,2) = q02(j)                      
               q0(j,k,3) = q03(j)                          
               q0(j,k,4) = q04(j)                               
               turre(j,k)= tur0(j)
 80         continue                                                       
100      continue                                                        
c                                                                        
         call code(jdim,kdim,ldim,ndim,q0,jmax0,kmax0,lmax0,conser)      
c                                                                        
      else                                                             
c        *************************************************************
c                   fine to coarse grid interpolation
c        *************************************************************
c                                                                       
c        -calculate strides in j                                     
c
csd      -so say you have only two grids : 331x51 and 166x51
csd       the first time you're in here jmax0=331 and jmaxp=166       
csd       therefore .... jstride= 330/165 = 2

         jstride = (jmax0-1)/(jmaxp-1)                                   
         write (out_unit,*) 'jstride = ',jstride 
822      continue                                                        
                                                                        
         if(qint)                                                        
     >   call decode(jdim,kdim,ldim,ndim,q0,jmax0,kmax0,lmax0,conser)    
                                                                        
         do 400 j=1,jmax0,jstride                                        
            jj = (j-1)/jstride + 1
            do 300 k=1,kmax0
              xf(k) = x0(j,k)                                            
              yf(k) = y0(j,k)                                     
              zf(k) = 0.0                                   
 300        continue
c
            if(qint)then  
              do 310 k=1,kmax0                                              
                 q01(k) = q0(j,k,1)                                
                 q02(k) = q0(j,k,2)                            
                 q03(k) = q0(j,k,3)                               
                 q04(k) = q0(j,k,4)                              
 310          continue                                                      
            endif                                                           
                                                                        
            call intg(kmax0,xf,yf,zf,kmaxp,xc,yc,zc)
                                                                        
            do 350 k=1,kmaxp
              x1(jj,k)  = xc(k)                                 
              y1(jj,k)  = yc(k)                            
 350        continue

                                                                        
            if(qint)then
              call intq(kmax0,xf,yf,zf,q01,kmaxp,xc,yc,zc,q11)              
              call intq(kmax0,xf,yf,zf,q02,kmaxp,xc,yc,zc,q12)              
              call intq(kmax0,xf,yf,zf,q03,kmaxp,xc,yc,zc,q13)              
              call intq(kmax0,xf,yf,zf,q04,kmaxp,xc,yc,zc,q14)              
              do 375 k=1,kmaxp                                              
                 q1(jj,k,1)      = q11(k)
                 q1(jj,k,2)      = q12(k)
                 q1(jj,k,3)      = q13(k)
                 q1(jj,k,4)      = q14(k)          
 375          continue                                                      
            endif                                                           
 400     continue

         if(qint)
     &   call code(jdim,kdim,ldim,ndim,q1,jmaxp,kmaxp,lmaxp,conser)      
                                                                        
      endif                                                            

      return
      end                                                              
