c #########################
c ##                     ##
c ##  subroutine eigval  ##
c ##                     ##
c #########################
c
      subroutine eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,tmet,      
     >                  uu,cc)                                         
c
c     : ix = 1  iy = 2 for xi   direction  metrics                    
c     : ix = 3  iy = 4 for eta  direction  metrics                    
c                                                                       
#include "../include/arcom.inc"
#include "../include/optcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      dimension xy(jdim,kdim,4),tmet(jdim,kdim),xyj(jdim,kdim)
      dimension uu(jdim,kdim),cc(jdim,kdim)
c                                                                       
c     -compute spectral radius scaling
c     Note: 1/q(j,k,1) is j/rho 
      do 100 k =kbegin,kend 
      do 100 j =jbegin,jend   
c        -time metrics put in (sloppy but they work)
         uu(j,k) = tmet(j,k) + 
     &             (q(j,k,2)*xy(j,k,ix)+q(j,k,3)*xy(j,k,iy))/q(j,k,1) 
         cc(j,k) = sndsp(j,k)*sqrt(xy(j,k,ix)**2+xy(j,k,iy)**2)         
  100 continue                                                          
c

      return
      end

