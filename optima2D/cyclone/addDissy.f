************************************************************************
      !-- Program name: addDissy
      !-- Written by: Howard Buckley
      !-- Date: April 2010
      !-- 
      !-- If dissipation-based continuation is to be used as a
      !-- globalization method for N-K, then add additional 
      !-- dissipation to inviscid flux derivative dF/d_eta
************************************************************************

      subroutine  addDissy(jdim,kdim,q,s,xyj)

#ifdef _MPI_VERSION
      use mpi
#endif


      use disscon_vars
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      !-- Declare variables

      integer
     &     j, jdim, j1, j2, jpl, jmi, k, k1, k2, kmi, kpl, kdim, n

      double precision
     &     cf2ydc(jdim,kdim), c2, dtd,
     &     work(jdim,kdim,3), q(jdim,kdim,4), xyj(jdim,kdim),
     &     tmp(jdim,kdim,4), s(jdim,kdim,4)

      !-- Initialize 'tmp' array

      do 3 n=1,4
      do 3 k=1,kdim
      do 3 j=1,jdim
         tmp(j,k,n)=0.0
 3    continue

      !-- Calculate a second-difference scalar dissipation coefficient
      !-- governed by the continuation parameter 'lamDiss'

      do k = kbegin,kup
        kpl = k+1
        do j = jbegin,jend
          c2 = lamDiss*(specty_dc(j,kpl)+specty_dc(j,k))
          cf2ydc(j,k) = c2
        end do
      end do

      !-- Calculate the additional dissipation term to be added to
      !-- the inviscid flux derivative

      do n = 1,4 !-- 1st-order forward difference

        do k =kbegin,kup
        kpl = k+1   
          do j =jlow,jup
            work(j,k,1) = q(j,kpl,n)*xyj(j,kpl) - q(j,k,n)*xyj(j,k)
          end do
        end do

        !-- Form dissipation term 
        do k =kbegin,kup
          do j =jlow,jup
            work(j,k,2) = cf2ydc(j,k)*work(j,k,1)
          end do
        end do

        !-- Do a backwards difference on the dissipation term
        dtd = dt / (1. + phidt)
        do k = klow,kup
          kmi = k-1 
          do j = jlow,jup
            tmp(j,k,n) = (work(j,k,2) - work(j,kmi,2))*dtd
          end do
        end do

      end do ! n=1,4

      !-- Add dissipation term to dE/d_xi

      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
           s(j,k,n)=s(j,k,n)+tmp(j,k,n)
 200  continue

      return
      end
