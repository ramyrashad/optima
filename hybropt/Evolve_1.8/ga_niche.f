      subroutine ga_niche(n,nchrom,nobfun,irank,
     $     fit,fitmax,fitmin,sig_share,i_mnx)

      dimension fit_niche(nchrom),fit(nobfun,nchrom)
      dimension fitmax(10),fitmin(10)
      integer irank(nchrom)
      
      dimension d(nchrom,nchrom),r_m(nchrom),i_front(nchrom)
      dimension f_max(nobfun),f_min(nobfun)

c     Nicheing using false fitness 
c     ala. Marco, Desideri and Lanteri INRIA No 3686, 1999

      m1 = 0
      srank_min = 1.0 ! start with false fitness = 1.0
      do nc = 1,nchrom
         M = 0
         do ncc= 1,nchrom
            if(irank(ncc).eq.nc)then
               M = M+1          ! count of rank ncc chromosomes
               i_front(M) = ncc ! index of rank ncc chromosomes
            endif
         enddo

         do nob = 1,nobfun      ! find min/max over front
c$$$            f_max(nob) = fitmax(nob)
c$$$            f_min(nob) = fitmin(nob)
            f_max(nob) = -99999999.0
            f_min(nob) =  99999999.0
            do i = 1,M
               ii = i_front(i)
               if(f_max(nob).lt.fit(nob,ii))f_max(nob) = fit(nob,ii)
               if(f_min(nob).gt.fit(nob,ii))f_min(nob) = fit(nob,ii)
            enddo
         enddo

         if (M.gt.0)then
            do i = 1,M
               ii = i_front(i)
               do j = 1,M
                  jj = i_front(j)
                  dd = 0.0
                  do nob = 1,nobfun
                     rr = f_max(nob)-f_min(nob)
                     if(rr.gt.0.0)dd = dd + (
     $                    (fit(nob,ii) - fit(nob,jj))/
     $                      rr)**2
                  enddo
                  d(i,j) = sqrt(dd) ! distance function between two indivduals
               enddo
            enddo

            do i = 1,M
               rm = 0
               do j = 1,M
                  sh = 1 - min(d(i,j)/sig_share,1.0)  ! sharing function
                                ! sig_share is maximum phenotypic distance 
                                ! allowed between two individuals to become 
                                ! members of a niche
                  rm = rm + sh
               enddo
               R_M(i) = rm
            enddo

            srank_min0 = srank_min ! original dummy fitness
            do i = 1,M
               ncc = i_front(i)
               fit_niche(ncc) = srank_min/R_M(i)  ! new dummy fitness
               if(fit_niche(ncc) .lt. srank_min0)
     $              srank_min0 = fit_niche(ncc) ! find min for new front dummy fitness
            enddo
            srank_min = srank_min0*0.99 ! reduce by 1% so that next front has all lower values
            m1 = m1 + M 
         endif
         if (m1.ge.nchrom)go to  1
      enddo
 1    continue

      call RANK_GA(N,nchrom,1,irank,fit_niche,i_mnx) ! re- rank based on dummy fitness 

      return
      end


