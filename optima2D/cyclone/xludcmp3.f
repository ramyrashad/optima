c ********************************************************************
c ** LU Factorization --- for tridiagonal systems                   **
c ** x sweep routine                                                **
c ** Crout's method of factorization applied to tridiagonals        **
c ** (The diagonals of U are all 1's, the diagonals of L are not)   **
c ********************************************************************
      subroutine xludcmp3(dm1,dd,dp1,jmax,kmax,jlow,jup,klow,kup)
c ********************************************************************
c Variables:   dm1: vector 1 below diagonal    
c              dd : vector of diagonal entries
c              dp1: vector 1 above diagonal
c ********************************************************************
      dimension dm1(jmax,kmax), dd(jmax,kmax), dp1(jmax,kmax)
c     outer loop over y
      do 199 k=klow,kup
c jlow
         j = jlow
         dm1(j+1,k) = dm1(j+1,k)/dd(j,k)
c jlow+1        
         j = jlow + 1
         dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j-1,k)
         dm1(j+1,k) = dm1(j+1,k)/dd(j,k)
c ** Main Loop *******************************************************
c        inner loop over x 
         do 120 j = jlow+2,jup-2
c                                                        /* beta's  */
           dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j-1,k)
c                                                        /* alpha's */
           dm1(j+1,k) = dm1(j+1,k)/dd(j,k)
  120    continue
c ** main loop *******************************************************
c jup-1
         j = jup - 1
         dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j-1,k)
         dm1(j+1,k) = dm1(j+1,k)/dd(j,k)
c jup
         j = jup 
         dd(j,k) = dd(j,k)-dm1(j,k)*dp1(j-1,k)
  199 continue
      return
      end
