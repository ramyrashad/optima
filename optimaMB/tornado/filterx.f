C ********************************************************************
C ** SECOND -  FOURTH :  ORDER EXPLICIT FILTER ***********************
C ********************************************************************
      SUBROUTINE filterx(n,jdim,kdim,q,xyj,coef2,coef4,work,tmp,
     &     jbegin,jend,klow,kup)

      implicit none

c     fourth order smoothing, added explicitly to rhs
c     second order near shocks with pressure grd coeff.
c     xi direction

#include "../include/common.inc"

      integer n,jdim,kdim,jbegin,jend,klow,kup,jlow,jup,j,k,jpl
      integer j1,j2,jmi
      double precision q(jdim,kdim,4),xyj(jdim,kdim)            
      double precision coef2(jdim,kdim),coef4(jdim,kdim)                       
      double precision work(jdim,kdim,3),tmp(jdim,kdim,4),dtd

      jlow = jbegin+1
      jup  = jend-1

cmt ~~~ (used to be subroutine filterx1) ~~~
cmt ~~~ 1st-order forward difference ~~~

      DO k = klow,kup
         DO j = jlow,jup
            jpl=j+1
            work(j,k,1) = q(jpl,k,n)*xyj(jpl,k) - q(j,k,n)*xyj(j,k)
         ENDDO
      ENDDO

      j1 = jbegin
      j2 = jend
      DO k = klow,kup
         work(j1,k,1) = q(j1+1,k,n)*xyj(j1+1,k)
     &                 -q(j1,k,n)*xyj(j1,k)

         work(j2,k,1) = work(j2-1,k,1)
      ENDDO

cmt ~~~ (used to be subroutine filterx2) ~~~
cmt ~~~ apply cent-dif to 1st-order forward ~~~

      DO k = klow,kup
         DO j = jlow,jup
            jpl=j+1
            jmi=j-1
            work(j,k,2)=work(jpl,k,1)-2.d0*work(j,k,1)+work(jmi,k,1)
         ENDDO
      ENDDO

      j1 = jbegin 
      j2 = jend  
      DO k = klow,kup
         work(j1,k,2) = q(j1+2,k,n)*xyj(j1+2,k)
     &        - 2.d0*q(j1+1,k,n)*xyj(j1+1,k)
     &        + q(j1,k,n)*xyj(j1,k)

         work(j2,k,2) = 0.d0
      ENDDO

cmt ~~~ (used to be subroutine filterx3) ~~~
cmt ~~~ form dissipation term before last differentiation ~~~

      DO k = klow,kup
         DO j = jlow,jup
            work(j,k,3)=(coef2(j,k)*work(j,k,1)-coef4(j,k)*work(j,k,2))
         ENDDO
      ENDDO

      j1 = jbegin
      j2 = jend
      DO k = klow,kup
         work(j1,k,3) = (coef2(j1,k)*work(j1,k,1)
     &        -coef4(j1,k)*work(j1,k,2))
         work(j2,k,3) = 0.d0
      ENDDO

cmt ~~~ (used to be subroutine filterxs) ~~~
cmt ~~~ last differenciation and add in dissipation ~~~

      dtd = dt / (1.d0 + phidt)

      DO k = klow,kup
         DO j = jlow,jup
            tmp(j,k,n) = (work(j,k,3) - work(j-1,k,3))*dtd
         ENDDO
      ENDDO

      RETURN
      END
