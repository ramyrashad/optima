c----------------------------------------------------------------------
c     -- modify grid blocks with sides next to airfoil --
c     -- m. nemec --
c----------------------------------------------------------------------
      subroutine modblk(node, ifoil, jdim, kdim, iside, x, y, xo, yo,
     &     seg, xandy)

      implicit none

#include "mxfoil.inc"
#include "bsp.inc"
      
      integer node, jdim, kdim, iside, ip, j, k, ifoil
      integer js, je, ks, ke, ixy, istep
      double precision x(jdim,kdim), y(jdim,kdim)
      double precision xo(jdim,kdim), yo(jdim,kdim)
      double precision seg(jdim*kdim) 
      logical xandy

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      do j=1,jdim
         do k=1,kdim
            xo(j,k) = x(j,k)
            yo(j,k) = y(j,k)
         end do
      end do

c     -- side # 1 --

      if (iside.eq.1) then

c     -- adjusting normal direction --
         js = 1
         je = jdim
         ks = 1
         ke = kdim
         istep = 1
         ixy = 2
         call adjgrd(node, ip, ifoil, js, je, ks, ke, istep, ixy, jdim,
     &        kdim, y, xo, yo, seg)

c     -- adjusting streamwise direction --
         if (xandy) then
            ixy = 1
            call adjgrd(node, ip, ifoil, js, je, ks, ke, istep, ixy,
     &           jdim, kdim, x, xo, yo, seg)
         end if

         node = ip - 1

c     -- side # 2 --

      else if (iside.eq.2) then
c     j = 1
c     do k=kdim,1,-1
         write (*,*) 'modblk: side 2 error'
         stop
c     end do

c     -- side # 3 --

      else if (iside.eq.3) then

c     -- adjusting normal direction --
         js = jdim
         je = 1
         ks = kdim
         ke = 1
         istep = -1
         ixy = 2
         call adjgrd(node, ip, ifoil, js, je, ks, ke, istep, ixy, jdim,
     &        kdim, y, xo, yo, seg)

c     -- adjusting streamwise direction --
         if (xandy) then
            ixy = 1
            call adjgrd(node, ip, ifoil, js, je, ks, ke, istep, ixy,
     &           jdim, kdim, x, xo, yo, seg)
         end if

         node = ip - 1
         
c     -- side # 4 --

      else if (iside.eq.4) then
c     j = jdim
c     do k=1,kdim 
         write (*,*) 'modblk: side 4 error'
         stop
c     end do

      end if

      return
      end                       !modblk
