      subroutine expmaty(jdim,kdim,q,coef2y,coef4y,sndsp,s,
     >spect,xyj,press,ccy,vv,xy,x,y,work,s1,s2,u,v,phi,h,temp,temp2)
c
#include "../include/arcom.inc"
c
      integer g,t
      dimension q(jdim,kdim,4),coef2y(jdim,kdim),coef4y(jdim,kdim)
      dimension sndsp(jdim,kdim),s(jdim,kdim,4),work(4,jdim,kdim,2)
      dimension spect(jdim,kdim,3),xyj(jdim,kdim),press(jdim,kdim)
      dimension ccy(jdim,kdim),vv(jdim,kdim),xy(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension temp(4,jdim,kdim),temp2(4,jdim,kdim)
      dimension s1(jdim,kdim),s2(jdim,kdim),u(jdim,kdim),v(jdim,kdim)
      dimension phi(jdim,kdim),h(jdim,kdim)
c
      dtd=dt/(1. +phidt)
      do 11 i=1,4
         do 14 k=kbegin,kup
            kp1=k+1
            do 14 j=jlow,jup
               work(i,j,k,1)=q(j,kp1,i)*xyj(j,kp1) - q(j,k,i)*xyj(j,k)
 14      continue
      
         do 15 j=jlow,jup
            work(i,j,kend,1)=work(i,j,kup,1)
 15      continue
      
         do 18 k=klow,kup-1
           kp=k+1
           km=k-1
c          (q(j+2)-3q(j+1)+3q(j)-q(j-1))
           do 18 j=jlow,jup
           work(i,j,k,2)=work(i,j,kp,1)-2.*work(i,j,k,1)+work(i,j,km,1)
 18      continue
c       
c        boundary conditions a la Kyle
         do 20 j=jlow,jup
         work(i,j,kbegin,2)=
     &              q(j,kbegin+2,i)*xyj(j,kbegin+2)-2.*q(j,kbegin+1,i)
     &              *xyj(j,kbegin+1)+q(j,kbegin,i)*xyj(j,kbegin)
         work(i,j,kup,2)=0.0
 20      continue
 11   continue
c
c
c
      do 100 k=kbegin,kend
      do 100 j=jlow,jup
         rhoinv=1.d0/q(j,k,1)
         s1(j,k)=.5d0*(spect(j,k,2)+spect(j,k,3)-2.*spect(j,k,1))
         s2(j,k)=(spect(j,k,2)-spect(j,k,3))/(2.*ccy(j,k))
         u(j,k)=q(j,k,2)*rhoinv
         v(j,k)=q(j,k,3)*rhoinv
         phi(j,k)=.5d0*(u(j,k)**2+v(j,k)**2)         
         h(j,k)=(q(j,k,4)+press(j,k))*rhoinv
 100  continue
c
      if (iord.eq.4) then
        kl=klow+1
        ku=kup-1
      else
        kl=klow
        ku=kup
      endif
c
      if (dis2y.eq.0.) then
        do 103 k=kbegin,kup
        do 103 j=jbegin,jend
        do 103 n=1,4
          temp(n,j,k)=0.
 103    continue
      endif

      do 888 g=0,1
        if (dis2y.eq.0.) then
c         -by-pass second-difference computation
          do 105 k=kbegin,kup
            t=k+g
            do 104 j=jlow,jup
              include 'matloopy2.inc'
 104        continue
 105      continue
        else
          do 112 k=kbegin,kup
            t=k+g
            do 111 j=jlow,jup
              include 'matloopy.inc'
 111        continue
 112      continue
        endif
c     
        if (flbud .and.
     &     (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
          do 322 n=1,4
          do 322 k=kl,ku
            t=k+g
            km1=k-1
            do 240 j=jlow,jup
              zz=dtd*(
     &         coef2y(j,t)*temp (n,j,k)-coef2y(j,t-1)*temp (n,j,km1) -
     &        (coef4y(j,t)*temp2(n,j,k)-coef4y(j,t-1)*temp2(n,j,km1)))
c       
              s(j,k,n)=s(j,k,n) + zz
              if (n.eq.2) budget(j,k,2)=budget(j,k,2)+zz
 240        continue
 322      continue
        else
          do 222 n=1,4
            do 140 k=kl,ku
              t=k+g
              km1=k-1
              do 140 j=jlow,jup
                s(j,k,n)=s(j,k,n) + dtd*( 
     &           coef2y(j,t)*temp (n,j,k)-coef2y(j,t-1)*temp (n,j,km1) -
     &          (coef4y(j,t)*temp2(n,j,k)-coef4y(j,t-1)*temp2(n,j,km1)))
 140        continue
 222      continue
        endif
 888  continue
c
c
      if (iord.eq.4) then
c        Boundary Conditions
         do 2000 n=1,4
            do 2100 j=jlow,jup
             k=klow
c              k=kbegin
             work(n,j,kbegin,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j,k+1,n)*
     &                          xyj(j,k+1)-q(j,k+2,n)*xyj(j,k+2)
c             work(n,j,k,2)=q(j,k,n)*xyj(j,k)-3.d0*(q(j,k+1,n)*xyj(j,k+1)
c     &                     -q(j,k+2,n)*xyj(j,k+2))-q(j,k+3,n)*xyj(j,k+3)
             k=kbegin
c             k=klow
             work(n,j,klow,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j,k+1,n)*
     &                        xyj(j,k+1)-q(j,k+2,n)*xyj(j,k+2)
c             work(n,j,k,2)=q(j,k,n)*xyj(j,k)-3.d0*(q(j,k+1,n)*xyj(j,k+1)
c     &                     -q(j,k+2,n)*xyj(j,k+2))-q(j,k+3,n)*xyj(j,k+3)
             k=kup-1
             work(n,j,k,2)=-q(j,k-1,n)*xyj(j,k-1)+2.d0*q(j,k,n)*xyj(j,k)
     &                     -q(j,k+1,n)*xyj(j,k+1)
c             work(n,j,k,2)=q(j,k+1,n)*xyj(j,k+1)-3.d0*(q(j,k,n)*xyj(j,k)
c     &                     -q(j,k-1,n)*xyj(j,k-1))-q(j,k-2,n)*xyj(j,k-2)
             k=kup
             work(n,j,k,2)=-q(j,k-1,n)*xyj(j,k-1)+2.d0*q(j,k,n)*xyj(j,k)
     &                     -q(j,k+1,n)*xyj(j,k+1)
c             work(n,j,k,2)=q(j,k+1,n)*xyj(j,k+1)-3.d0*(q(j,k,n)*xyj(j,k)
c     &                     -q(j,k-1,n)*xyj(j,k-1))-q(j,k-2,n)*xyj(j,k-2)
 2100       continue
 2000    continue
c
         do 2500 g=0,1
            do 2400 j=jlow,jup
               do 2200 k=kbegin,klow
                  t=k+g
                  include 'matloopy.inc'
 2200          continue
               do 2250 k=kup-1,kup
                  t=k+g
                  include 'matloopy.inc'
 2250          continue
c        
               do 2300 n=1,4
               do 2300 k=klow,kup,kup-klow
                t=k+g
                km1=k-1
                s(j,k,n)=s(j,k,n) + dtd*( 
     &          coef2y(j,t)*temp (n,j,k)-coef2y(j,t-1)*temp (n,j,km1)
     &        -(coef4y(j,t)*temp2(n,j,k)-coef4y(j,t-1)*temp2(n,j,km1)))
 2300          continue
 2400       continue
 2500    continue
      endif
c            
      return
      end






