      subroutine stepf2dxlu(jdim,kdim,q,turmu,fmu,s,press,sndsp,xy,xyj,
     *                    ds,coef2,coef4,uu,cc,work,spect,lu,akk)     
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension ds(jdim,kdim), turmu(jdim, kdim), fmu(jdim,kdim)        
c                                                                       
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension uu(jdim,kdim),cc(jdim,kdim),spect(jdim,kdim,3)
      double precision lu(jdim,kdim,4,5)
c                                                                       
      double precision akk
      dimension work(jdim,kdim,10)                                      
c                                                                       
c  note : d used for vectorization                                      
c                                                                       
      dtd   = dt * thetadt/(1. + phidt)                                 
      hd    = 0.5*dtd                                                   
c                                                                       
c  dissipation coefficients for implicit side                           
c                                                                       
c                                                                       
c   nonlinear coef's contain dt                                         
c   need to add theta scaling                                           
c                                                                       
       smudt = smuim*dt* thetadt/(1. + phidt)                           
c                                                                       
c  xi direction                                                         
c                                                                       
c  fill matrix for inversion for diagonal in xi                         
c                                                                       
c  set switches for eigenvalues                                         
c                                                                       
c  do first two at the same time since they have the same coefficients  
c                                                                       
      if (idmodel.eq.2) call mspectx(jdim,kdim,uu,cc,xyj,xy,spect)
      do 300 n = 2,4                                                 
         if ( n .eq. 2) sn = 0.                                      
         if ( n .eq. 3) sn = 1.                                      
         if ( n .eq. 4) sn = -1.
         nm=n-1
c
      if(idmodel.eq.1) then
         if(meth.eq.4) then 
            do 210 j = jlow,jup                                            
                jp1 = jplus(j)                                                 
                jm1 = jminus(j)                                                
                do 212 k = klow,kup                                         
                   c2m = coef2(jm1,k)*dtd                                      
                   c4m = coef4(jm1,k)*dtd                                      
                   c2 = coef2(j,k)*dtd                                       
                   c4 = coef4(j,k)*dtd
                   c2 = c2 + 2.*c4
                   c2m = c2m + 2.*c4m                                      
                   lu(j,k,n,2) = -c2m*xyj(jm1,k)               
                   lu(j,k,n,3) = xyj(j,k)*(c2m+c2)                
                   lu(j,k,n,4) = -c2*xyj(jp1,k)                
212             continue                                                    
210         continue
         elseif (meth.eq.5) then
            do 215 j = jlow,jup                                            
               jp1 = jplus(j)                                                 
               jm1 = jminus(j)                                                
               jp2 = jplus(jp1)                                               
               jm2 = jminus(jm1)                                              
               do 213 k = klow,kup                                         
                  c2m = coef2(jm1,k)*dtd                                      
                  c4m = coef4(jm1,k)*dtd                                      
                  c2 = coef2(j,k)*dtd                                       
                  c4 = coef4(j,k)*dtd
                  lu(j,k,n,1) =  xyj(jm2,k)*c4m                               
                  lu(j,k,n,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)               
                  lu(j,k,n,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)                
                  lu(j,k,n,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)                
                  lu(j,k,n,5) = xyj(jp2,k)*c4                                 
 213           continue                                                    
 215        continue
                                                                          
            if(.not.periodic)then                                       
               j = jlow                                                       
               jp1 = jplus(j)                                                 
               jm1 = jminus(j)                                                
               jp2 = jplus(jp1)                                               
               do 220 k = klow,kup                                         
                  c2m = coef2(jm1,k)*dtd                                      
                  c4m = coef4(jm1,k)*dtd                                      
                  c2 = coef2(j,k)*dtd                                       
                  c4 = coef4(j,k)*dtd                                       
                  lu(j,k,n,1) =  0.                                           
                  lu(j,k,n,2) = -(c2m + c4m + c4)*xyj(jm1,k)                  
                  lu(j,k,n,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                
                  lu(j,k,n,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)                
                  lu(j,k,n,5) = xyj(jp2,k)*c4                                 
220            continue                                                    
c                                                                          
               j = jup                                                        
               jp1 = jplus(j)                                                 
               jm1 = jminus(j)                                                
               jm2 = jminus(jm1)                                              
               do 222 k = klow,kup                                         
                  c2m = coef2(jm1,k)*dtd                                      
                  c4m = coef4(jm1,k)*dtd                                      
                  c2 = coef2(j,k)*dtd                                       
                  c4 = coef4(j,k)*dtd                                       
                  lu(j,k,n,1) =  xyj(jm2,k)*c4m                               
                  lu(j,k,n,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)               
                  lu(j,k,n,3) = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)                
                  lu(j,k,n,4) = -(c2 + c4 + c4m)*xyj(jp1,k)                   
                  lu(j,k,n,5) = 0.                                            
222            continue                                                    
c           endif from if(not periodic)                                    
            endif                                                       
         endif
      else
c ********************************************************************
c **                       matrix diss model                        **
c ********************************************************************
c                                                                       
         if(meth.eq.4) then 
            do 310 j = jlow,jup                                            
                jp1 = jplus(j)                                                 
                jm1 = jminus(j)                                                
                do 312 k = klow,kup                                         
                   c2m = (coef2(jm1,k)*spect(jm1,k,nm)+coef2(j,k)
     $                  *spect(j,k,nm))*dtd
                   c4m = (coef4(jm1,k)*spect(jm1,k,nm)+coef4(j,k)
     $                  *spect(j,k,nm))*dtd        
                   c2 = (coef2(j,k)*spect(j,k,nm)+coef2(jp1,k)*spect(jp1
     $                  ,k,nm))*dtd
                   c4 = (coef4(j,k)*spect(j,k,nm)+coef4(jp1,k)*spect(jp1
     $                  ,k,nm))*dtd
                   c2 = c2 + 2.*c4
                   c2m = c2m + 2.*c4m                                      
                   lu(j,k,n,2) = -c2m*xyj(jm1,k)               
                   lu(j,k,n,3) = xyj(j,k)*(c2m+c2)                
                   lu(j,k,n,4) = -c2*xyj(jp1,k)                
312             continue                                                    
310         continue
         elseif (meth.eq.5) then
            do 315 k = klow,kup                                         
               do 313 j = jlow,jup                                            
                  jp1 = jplus(j)                                                 
                  jm1 = jminus(j)                                                
                  jp2 = jplus(jp1)                                               
                  jm2 = jminus(jm1)                                              
                  c2m = (coef2(jm1,k)*spect(jm1,k,nm)+coef2(j,k)
     $                 *spect(j,k,nm))*dtd
                  c4m = (coef4(jm1,k)*spect(jm1,k,nm)+coef4(j,k)
     $                 *spect(j,k,nm))*dtd        
                  c2 = (coef2(j,k)*spect(j,k,nm)+coef2(jp1,k)*spect(jp1
     $                 ,k,nm))*dtd
                  c4 = (coef4(j,k)*spect(j,k,nm)+coef4(jp1,k)*spect(jp1
     $                 ,k,nm))*dtd
                  lu(j,k,n,1) =  xyj(jm2,k)*c4m                               
                  lu(j,k,n,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)               
                  lu(j,k,n,3) = xyj(j,k)*(c2m+3.*c4m+c2+3.*c4)                
                  lu(j,k,n,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)                
                  lu(j,k,n,5) = xyj(jp2,k)*c4                                 
 313           continue         
 315        continue
c                                                                          
            if(.not.periodic)then                                       
               j = jlow                                                       
               jp1 = jplus(j)                                                 
               jm1 = jminus(j)                                                
               jp2 = jplus(jp1)                                               
               do 320 k = klow,kup                                         
                  c2m = (coef2(jm1,k)*spect(jm1,k,nm)+coef2(jm1+1,k)
     $                 *spect(jm1+1,k,nm))*dtd    
                  c4m = (coef4(jm1,k)*spect(jm1,k,nm)+coef4(j,k)
     $                 *spect(j,k,nm))*dtd
                  c2 = (coef2(j,k)*spect(j,k,nm)+coef2(jp1,k)*spect(jp1,
     $                 k,nm))*dtd
                  c4 = (coef4(j,k)*spect(j,k,nm)+coef4(jp1,k)*spect(jp1,
     $                 k,nm))*dtd
                  lu(j,k,n,1) =  0.                                           
                  lu(j,k,n,2) = -(c2m + c4m + c4)*xyj(jm1,k)                  
                  lu(j,k,n,3) = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                
                  lu(j,k,n,4) = -(c2 + 3.*c4 + c4m)*xyj(jp1,k)                
                  lu(j,k,n,5) = xyj(jp2,k)*c4                                 
320            continue                                                    
c                                                                          
               j = jup                                                        
               jp1 = jplus(j)                                                 
               jm1 = jminus(j)                                                
               jm2 = jminus(jm1)                                              
               do 322 k = klow,kup                                         
                  c2m = (coef2(jm1,k)*spect(jm1,k,nm)+coef2(j,k)*
     $                  spect(j,k,nm))*dtd
                  c4m = (coef4(jm1,k)*spect(jm1,k,nm)+coef4(j,k)*
     $                  spect(j,k,nm))*dtd
                  c2 = (coef2(j,k)*spect(j,k,nm)+coef2(jp1,k)*
     $                 spect(jp1,k,nm))*dtd
                  c4 = (coef4(j,k)*spect(j,k,nm)+coef4(jp1,k)*
     $                 spect(jp1,k,nm))*dtd
                  lu(j,k,n,1) =  xyj(jm2,k)*c4m                               
                  lu(j,k,n,2) = -(c2m + 3.*c4m + c4)*xyj(jm1,k)               
                  lu(j,k,n,3) = xyj(j,k)*(c2m+3.*c4m+c2+2.*c4)                
                  lu(j,k,n,4) = -(c2 + c4 + c4m)*xyj(jp1,k)                   
                  lu(j,k,n,5) = 0.                                            
322            continue                                                    
c           endif from if(not periodic)                                    
            endif                                                       
         endif
      endif
c                                                                       
c
c
c
c
c
c
c  add in flux jacobians, variable dt and identity                      
c                                                                       
        if(viscous .and. visxi )then                                    
           hre = hd/re                                                       
c                                                                       
c    sutherland equation                                                
c                                                                       
           do 4000 k = klow,kup                                         
           do 4000 j = jbegin,jend                                      
               rinv = 1./q(j,k,1)                                       
c           xi_x**2 + xi_y**2                                           
               ximet = xy(j,k,1)**2 + xy(j,k,2)**2                      
c           turb. viscosity                                             
               djac = hre/xyj(j,k)                                      
               work(j,k,6) = djac*ximet                                 
c           xyj/rho                                                     
               work(j,k,7) = rinv                                       
4000           continue                                                 
c                                                                       
           do 4401 k = klow,kup                                         
           do 4401 j = jlow,jup                                         
cthp-4/29/87  for now laminar flwo in xi viscous cases only             
cthp-4/29/87               turm = turmu(j,k)                            
               turm = 0.0                                               
               vnum1 = 0.5*(fmu(j,k)+fmu(j-1,k)) + turm                 
               vnum  = 0.5*(fmu(j,k)+fmu(j+1,k)) + turm                 
               ctm1 = (work(j-1,k,6)+work(j,k,6))*vnum1                 
               ctp1 = (work(j+1,k,6)+work(j,k,6))*vnum                  
               cttt = ctm1 + ctp1                                       
               lu(j,k,n,2) = lu(j,k,n,2) - work(j-1,k,7)*ctm1           
               lu(j,k,n,3) = lu(j,k,n,3) + work(j,k,7)*cttt             
               lu(j,k,n,4) = lu(j,k,n,4) - work(j+1,k,7)*ctp1           
4401       continue                                                     
c                                                                       
          endif                                                         
c                                                                       
c  end viscous                                                          
c                                                                       
csi      change lhs to add in time terms
         fct=1.d0
         if (ismodel.gt.1) then
            if (jesdirk .eq. 4 .or. jesdirk .eq.3)then
               fct=1.0d0*dt/(akk*dt2)
               do 233 k=klow,kup
               do 233 j=jlow,jup
                  lu(j,k,n,3) = lu(j,k,n,3) + fct
 233           continue
               fct=1.0d0/(akk*dt2)
            else
               fct=1.5d0*dt/dt2
               do 231 k=klow,kup
               do 231 j=jlow,jup
                  lu(j,k,n,3) = lu(j,k,n,3) + fct
 231           continue
               fct=1.d0
               if (ismodel.eq.2)then
                  fct=1.5d0/dt2
               endif
            endif
         endif
c                                                                       
c   add in flux jacobians and variable dt and identity                  
c                                                                       
         do 232 k = klow,kup                                         
         do 232 j = jlow,jup                                            
            lu(j,k,n,2) = lu(j,k,n,2) - (uu(j-1,k)+sn*cc(j-1,k))*hd     
            lu(j,k,n,4) = lu(j,k,n,4) + (uu(j+1,k)+sn*cc(j+1,k))*hd     
            lu(j,k,n,2) = lu(j,k,n,2)*ds(j,k)                           
            lu(j,k,n,3) = lu(j,k,n,3)*ds(j,k)                           
            lu(j,k,n,4) = lu(j,k,n,4)*ds(j,k)                           
            lu(j,k,n,3) = lu(j,k,n,3) + fct
232      continue                                                    
         if (meth.eq.5) then
            do 242 k = klow,kup                                         
               do 240 j = jlow,jup                                            
                   lu(j,k,n,1) = lu(j,k,n,1)*ds(j,k)                           
                   lu(j,k,n,5) = lu(j,k,n,5)*ds(j,k)                           
240            continue
242         continue                                                    
         endif
300   continue
      if (meth.eq.4) then 
         i1=2
         i2=4
      else
         i1=1
         i2=5
      endif
      do 325 i=i1,i2
      do 325 k=klow,kup
      do 325 j=jlow,jup
         lu(j,k,1,i)=lu(j,k,2,i)
325   continue                                                     
c                                                                       
c                                                                       
c                                                                       
c  for n = 1 and 2 do both together                                     
c   invert on first two s elements                                      
c                                                                       
c      if(meth.eq.1 .or. meth.eq.3 .or. meth.eq.4 .or. meth.eq.11 
c     &   .or. meth.eq.12 .or. meth.eq.14 .or. meth.eq.15)then                     
cc                                                                       
cc  for n = 1 and 2 do both together                                     
cc   invert on first two s elements                                      
cc                                                                       
c          if(n.eq.2)then                                                
c            if(.not.periodic)then                                           
c             call xpenta2(jdim,kdim,work(1,1,1),work(1,1,2),work(1,1,3),     
c     *            work(1,1,4),work(1,1,5),work(1,1,7),work(1,1,8),           
c     *            s(1,1,1),jlow,jup,klow,kup)                                
cc                                                                       
c            else                                                     
cc              periodic                                                 
c            call xpentap2(jdim,kdim,work(1,1,1),work(1,1,2),work(1,1,3),    
c     *            work(1,1,4),work(1,1,5),work(1,1,7),work(1,1,8),           
c     *            work(1,1,9),work(1,1,10),s(1,1,1),jlow,jup,klow,kup)       
c            endif                                                    
c          else                                                        
cc                                                                       
cc  n = 3 or 4                                                           
cc                                                                       
c            if(.not.periodic)then                                    
c             call xpenta(jdim,kdim,work(1,1,1),work(1,1,2),work(1,1,3),      
c     *            work(1,1,4),work(1,1,5),work(1,1,7),work(1,1,8),           
c     *            s(1,1,n),jlow,jup,klow,kup)                                
c            else                                                     
cc              periodic                                                 
c             call xpentap(jdim,kdim,work(1,1,1),work(1,1,2),work(1,1,3),     
c     *            work(1,1,4),work(1,1,5),work(1,1,7),work(1,1,8),           
c     *            work(1,1,9),work(1,1,10),s(1,1,n),jlow,jup,klow,kup)       
c            endif                                                    
c          endif                                                           
c      endif                                                           
cc                                                                       
c300       continue                                                      
c         do 325 i=2,4
c         do 325 k=klow,kup
c         do 325 j=jlow,jup
c             lu(j,k,1,i)=lu(j,k,2,i)
c325      continue                                                     
cc                                                                       
cc  end xi direction                                                     
cc                                                                       
        return                                                          
        end                                                             
