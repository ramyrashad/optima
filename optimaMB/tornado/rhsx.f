C ********************************************************************
C ************ EVALUATION OF RIGHT HAND SIDE ( EXPLICIT PART ) *******
C ********************************************************************
c calling routine: xiexpl
c
      subroutine rhsx(jdim,kdim,q,s,press,xy,xyj,xit,flux,
     &                jbegin,jend,jlow,jup,klow,kup)

      implicit none
c                                                                       
#include "../include/common.inc"
c                                                                       
      integer jdim,kdim,jbegin,jend,jlow,jup,klow,kup,j,k,n
      double precision q(jdim,kdim,4),press(jdim,kdim),s(jdim,kdim,4)          
      double precision xy(jdim,kdim,4),xyj(jdim,kdim),xit(jdim,kdim) 
      double precision flux(jdim,kdim,4),r1,r2,r0,rr,u,v,qs,pressj,r02 
c                                      
c     central differencing used in xi  
c     compute flux vectors 
c                        
      do 200 k=klow,kup   
      do 200 j=jbegin,jend 
         r1 = xy(j,k,1)  
         r2 = xy(j,k,2)   
         r0 = xit(j,k)     
c        rr is j/rho                                                     
         rr = 1./q(j,k,1)   
         u = q(j,k,2)*rr     
         v = q(j,k,3)*rr      

c        r1, r2 are d xi /dx and d xi /dy
c        qs is cap-u ( contravariant velocities )  
         qs = r0 +r1*u + r2*v         
c
c        pressj is pressure / j           
         pressj= press(j,k)               
         flux(j,k,1)= q(j,k,1)*qs           
         flux(j,k,2)= q(j,k,2)*qs + r1*pressj
         flux(j,k,3)= q(j,k,3)*qs + r2*pressj 
         flux(j,k,4) = qs*(q(j,k,4) + pressj) - pressj*r0              
200   continue                                  
c                                                 
      if (iord.eq.4) then
        r0 = -dt/(12.d0*(1.d0+phidt))
        r02= 2.d0*r0
        do 300 n =1,4
        do 300 k =klow,kup
        do 300 j =jlow+1,jup-1                                 
          s(j,k,n) = s(j,k,n) + r0*(flux(j-2,k,n) + 
     &          8.d0*(-flux(j-1,k,n)+flux(j+1,k,n))-flux(j+2,k,n))
 300    continue
c
c        rt0 = -.5*dt / (1.+phidt)
c        s(j,k,n) = s(j,k,n) + rt0*( flux(j+1,k,n) - flux(j-1,k,n))
        do 301 n =1,4
        do 301 k =klow,kup
c         -third order
          j=jlow
          s(j,k,n) =s(j,k,n) + r02*(-2.d0*flux(j-1,k,n) -
     &          3.d0*flux(j,k,n) + 6.d0*flux(j+1,k,n) - flux(j+2,k,n))
c              
          j=jup
          s(j,k,n) =s(j,k,n) + r02*(flux(j-2,k,n) - 6.d0*
     >            flux(j-1,k,n) + 3.d0*flux(j,k,n) + 2.d0*flux(j+1,k,n))
 301    continue
      else
c       -second-order central difference
        r0 = - 0.5d0*dt / (1.d0 + phidt)                 
        do 400 n=1,4                                  
        do 400 k=klow,kup                             
        do 400 j=jlow,jup                              
          s(j,k,n) = s(j,k,n) + r0*( flux(j+1,k,n) - flux(j-1,k,n))
 400    continue
      endif
c
      return                                                            
      end                                                               
