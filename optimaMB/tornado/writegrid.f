C ********************************************************************
C ***        subroutine to output grids                            ***
C ********************************************************************
      subroutine writegrid(lu,x,y,jdim,kdim,jbegin,jend,kbegin,kend)

      implicit none 
      integer lu,jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision x(jdim,kdim), y(jdim,kdim)
c
      write(lu) ((x(j,k),j=jbegin,jend),k=kbegin,kend),
     &          ((y(j,k),j=jbegin,jend),k=kbegin,kend)
      return 
      end 
