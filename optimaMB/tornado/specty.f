c ********************************************************************
c *************  compute eigenvalues *********************************
c ********************************************************************
      subroutine specty(jdim,kdim,vv,ccy,xyj,xy,spect,precon,
     &        jbegin,jend,kbegin,kend)

      implicit none

#include "../include/common.inc"

      integer jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision rj,sigy,precon(jdim,kdim,8)
      double precision xyj(jdim,kdim),xy(jdim,kdim,4)
      double precision spect(jdim,kdim),vv(jdim,kdim),ccy(jdim,kdim)

                                                                      
c   compute dissipation scaling                                         
c                                                                       
c      write(6,*) 'prec,ispec=',prec,ispec
      if(ispec.eq.0)then                                                
c        constant                                                            
         do 50 k = kbegin,kend                                             
         do 50 j = jbegin,jend                                             
            rj = 1./xyj(j,k)                                             
            spect(j,k) = rj                                                
50       continue                                                          
      elseif(ispec.eq.1)then                                       
c                                                                       
c  spectral radius                                                      
c                                                                       
         if (prec.eq.0) then
            do 100 k=kbegin,kend                                         
            do 100 j=jbegin,jend                                         
               sigy = abs(vv(j,k)) + ccy(j,k)                               
               rj = 1./xyj(j,k)                                             
               spect(j,k) = sigy*rj                                         
100         continue
         else
            do 1000 k=kbegin,kend
               do 1000 j=jbegin,jend
                  sigy=abs(precon(j,k,7))+precon(j,k,8)
                  rj=1.0/xyj(j,k)
                  spect(j,k)=sigy*rj
 1000       continue
         endif
c                                                                       
c  ispec = 3 use limit specral radii                                    
c                                                                       
      elseif(ispec.eq.3)then                                         
         do 200 k=kbegin,kend                                         
            do 250 j=jbegin,jend                                         
               sigy = abs(vv(j,k)) + ccy(j,k)                               
               rj = 1./xyj(j,k)                                             
               spect(j,k) = sigy                                            
250         continue                                                     
c                                                                     
            do 260 j=jbegin,jend                                         
               if(spect(j,k).lt.21.)spect(j,k)=21.                          
               if(spect(j,k).gt.1000.)spect(j,k)=1000.                      
260         continue                                                     
c                                                                     
            do 270 j=jbegin,jend                                         
               rj = 1./xyj(j,k)                                             
               spect(j,k) = spect(j,k)*rj                                   
270         continue                                                     
200      continue                                                     
c                                                                       
c                                                                       
      elseif(ispec.eq.4)then                                       
c                                                                       
c  spectral radius                                                      
c                                                                       
           do 400 k=kbegin,kend                                         
           do 400 j=jbegin,jend                                         
              sigy = abs(xy(j,k,3)) + abs(xy(j,k,4))                       
              rj = 1./xyj(j,k)                                             
              spect(j,k) = sigy*rj                                         
400        continue                                                     
c                                                                       
      elseif(ispec.eq.-1)then                          
c                                                                       
        do 500 k=kbegin,kend                             
        do 500 j=jbegin,jend                             
           spect(j,k) = 64./xyj(j,k)                        
500     continue                                         
c                                                                       
      endif                                                           
c                                                                       
c                                                                       
                return                                                  
                end                                                     
