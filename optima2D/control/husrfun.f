
      subroutine husrfun(sn_x, n, f, nF, G, lenG, comm, csz, cid,
     &                   fsn_base, statt, needobj, needgrad)
#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      
      !-- Declare all variables
      integer :: n, nF, lenG, csz, cid, fsn_base
      logical :: needobj, needgrad
      double precision :: sn_x(n), f(nF), G(lenG)
      integer :: comm(csz)

            integer
     &     jdim, kdim, ndim, ifirst, indx(maxj,maxk), icol(9),
     &     iex(maxjk*4), idv(ibsnc), iren(maxjk), rStat, ip,
     &     ia(maxjk*nblk+2), ifun, iter, nflag, mp, varStat, ruIndex,
     &     ja(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1),
     &     ipa(maxjk*nblk+2), jpa(maxjk*nblk2*5+1),
     &     iat(maxjk*nblk+2), lencu, leniu, lenru,
     &     jat(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1),
     &     ipt(maxjk*nblk+2), jpt(maxjk*nblk2*5+1), itfirst,
     &     needF, needG, ifunc,iu(1), statt,
     &     i, j, k, l, m, EOF, d10, d1, namelen, sendProc

      double precision
     &     q(maxjk,nblk), qp(maxjk,nblk), qold(maxjk,nblk), cp(maxjk), 
     &     grad(ibsnc),
     &     xy(maxjk,4), xyj(maxjk), x(maxj,maxk), y(maxj,maxk),
     &     dx(maxj*maxk*maxincr), dy(maxj*maxk*maxincr), tcon(20),
     &     lambda(2*maxj*maxk*maxincr), vort(maxj,maxk), gCons(20),
     &     turmu(maxj,maxk),  fmu(maxj,maxk), snobj_alpha,
     &     cp_tar(ibody,2), dvs(ibsnc), bt(ibody), bknot(ibskt),
     &     bap(ibody,2), bcp(ibsnc,2), work1(maxjk,100),
     &     as(maxj*maxk*nblk*(nblk*9+1)+maxj*nblk*3+1),
     &     ast(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1),
     &     pa(maxjk*nblk2*5+1),
     &     pat(maxjk*nblk2*5+1), cp_his(ibody,300,nopc),
     &     ac_his(ibody,300), opwk(maxjk,8),
     &     xsave(maxj,maxk), ysave(maxj,maxk), dvscale(ibsnc),
     &     qtmp(maxjk,nblk,nopc), obj0s(20), obj0, objfun,
     &     qwarm(warmset,maxj,maxk,nblk,nopc),offdp(noffcon),
     &     dgCdxs(maxtcon,ndv), offCs(50), doffCdxs(50,50), offC,
     &     diff, doffCdx(50), D_hat(maxjk,nblk,6), exprhs(maxjk,nblk),
     &     a_jk(6,6)

      character cu(1)*8

      integer needFeval, needGeval
    
#ifdef _MPI_VERSION

      !-- Begin execution

      !-- set statt to 1 if this is the first time we enter husrfun
      if (first_husrfun) then
         if (rank == 0) then
            print *, cid, 'INFO: 1st time entering husrfun'
         endif
         first_husrfun = .false.
         statt = 1
      endif

      myfsn_base = fsn_base
      myfsn_loc = 0

      lencu = 1
      leniu = 1
      lenru = ru_gl_len

      needFeval = 0
      needGeval = 0
      if (needobj) needFeval = 1
      if (needgrad) needGeval = 1

      call usrfun(statt, n, sn_x, needFeval, nF, f, needGeval, lenG, 
     &            G, cu, lencu, iu, leniu, ru_global, lenru)

#else
      stop 'husrfun.f: hybrid opt. requires compilation with MPI.'
#endif
      end

