c     ------------------------------------------------------------------
c     -- form column of jacobian matrix --
c     -- first order forward difference --
c     -- m. nemec, sept. 2001 --
c     ------------------------------------------------------------------
      subroutine fd_sdiff(nmax, ks, ke, js, je, jmax, kmax, cdrdq,
     &     sp, s, dsadq, rsap, rsa, dxx)

      dimension sp(jmax,kmax,4), s(jmax,kmax,4)
      dimension cdrdq(jmax,kmax,4), dsadq(jmax,kmax)
      dimension rsap(jmax,kmax), rsa(jmax,kmax)
      
      do n = 1,4
         do k = ks,ke
            do j = js,je
               cdrdq(j,k,n) = -( sp(j,k,n) - s(j,k,n) )*dxx
            end do
         end do
      end do

      if (nmax.eq.5) then
         do k = ks,ke
            do j = js,je
               dsadq(j,k) = -( rsap(j,k) - rsa(j,k) )*dxx
            end do
         end do
      end if
      
      return 
      end                       !fd_sdiff
