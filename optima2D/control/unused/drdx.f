c----------------------------------------------------------------------
c     -- calculate the sensitivity of the rhs residual dR/dX --
c        where Xs are the design variables -- 

c     written by: marian nemec
c     date: april 2000
c----------------------------------------------------------------------
      subroutine dRdX ( grad, jdim, kdim, q, xy,  xyj, x, y, bap, bcp,
     &      bt, bknot, dvs, idv, index, psi, Rm, Rp, DxR, precon,
     &      coef2, coef4, uu, vv, ccx, ccy, ds, press, xit, spect, gam,
     &      turmu, fmu, vort, sndsp )

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i, j, k, jdim, kdim, idv(nc), index(jdim,kdim)

      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim), psi(jdim*kdim*4)
      double precision grad(nc), dvs(nc), bap(jbody,2)
      double precision bcp(nc,2), bt(jbody), bknot(jbsord+nc)

      double precision Rm(jdim,kdim,4), Rp(jdim,kdim,4)
      double precision DxR(jdim,kdim,4)

      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision xit(jdim,kdim), ds(jdim,kdim)
      double precision turmu(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3)
      double precision uu(jdim,kdim), vv(jdim,kdim), ccx(jdim,kdim)
      double precision ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)

c     -- work arrays --
c     -- compatible with cyclone --
      double precision a(maxpen,4), b(maxpen,16), c(maxpen,16)
      double precision d(maxjk,16), e(maxpen,4), work2(maxjk,10)
      common/scratch/ a,b,c,d,e,work2
      double precision work3(maxj,30)
      common/worksp/ work3

c     -- local variables --
      double precision tmp, stepsize, dt_tmp

      dt_tmp = dt
      dt = -1.d0

c     -- ensure that time metric is set to zero --
      do k=1,kend
        do j=1,jend
          xit(j,k) = 0.d0
        end do
      end do

      if (.not.cdf) then
c     -- using forward differences to evaluate rhs sensitivity --
c     -- first order accurate --
c     -- evaluate rhs of cyclone --
        do n=1,4
          do k=1,kend
            do j=1,jend
              Rm(j,k,n) = 0.d0
              q(j,k,n) = q(j,k,n)/xyj(j,k)
            end do
          end do
        end do

        call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

        call xiexpl ( jdim, kdim, q, Rm, press, sndsp, turmu, fmu, vort,
     &        x, y, xy, xyj, xit, ds, uu, ccx, coef2, coef4, spect,
     &        precon, gam )  

        call etaexpl ( jdim, kdim, q, Rm, press, sndsp, turmu, fmu,
     &        vort, x, y, xy, xyj, xit, ds, uu, vv, ccy, coef2, coef4,
     &        spect, precon, gam)

        call bcrhs ( jdim, kdim, x, y, xy, xyj, q, Rm, press )

        do n=1,4
          do k=1,kend
            do j=1,jend
              q(j,k,n) = q(j,k,n)*xyj(j,k)
            end do
          end do
        end do

c     -- calculate rhs at (+) --
        do i = 1,ndv
          tmp = dvs(i)
          stepsize = fd_eta*dvs(i)
          dvs(i) = dvs(i) + stepsize
          stepsize = dvs(i) - tmp

c     -- regrid domain --
          call regrid ( (i), jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot )
            
c     -- recalculate metrics --
          call xidif (jdim,kdim,x,y,xy)
          call etadif (jdim,kdim,x,y,xy)
          call calcmet (jdim,kdim,xy,xyj)

c     -- recalculate rhs of cyclone --
          do n=1,4
            do k=1,kend
              do j=1,jend
                Rp(j,k,n) = 0.d0
                q(j,k,n) = q(j,k,n)/xyj(j,k)
              end do
            end do
          end do

          call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

          call xiexpl ( jdim, kdim, q, Rp, press, sndsp, turmu, fmu,
     &          vort, x, y, xy, xyj, xit, ds, uu, ccx, coef2, coef4,
     &          spect, precon, gam)

          call etaexpl ( jdim, kdim, q, Rp, press, sndsp, turmu, fmu,
     &          vort, x, y, xy, xyj, xit, ds, uu, vv, ccy, coef2, coef4,
     &          spect, precon, gam)

          call bcrhs ( jdim, kdim, x, y, xy, xyj, q, Rp, press )

          do n=1,4
            do k=1,kend
              do j=1,jend
                q(j,k,n) = q(j,k,n)*xyj(j,k)
              end do
            end do
          end do

c     -- evaluate sensitivity of the flow residual --
          dxx = 1.d0/stepsize
          do n=1,4
            do k = 1,kend
              do j= 1,jend
                DxR(j,k,n) = ( Rp(j,k,n) - Rm(j,k,n) )*dxx
              end do
            end do
          end do

c     write(q_unit) jmaxold,kend 
c     write(q_unit) fsmach,alpha,re*fsmach,totime 
c     write(q_unit) (((DxR(j,k,n),j=1,jmax) ,k=1,kend),n=1,4)
c     write(q_unit) jtail1, numiter 

c     -- calculate gradient vector of the objective function -- 
          grad(i) = 0.d0
          do n = 1,4
            do k = 1,kend
              do j = 1,jend
                ii = ( index(j,k) - 1 )*4 + n
                grad(i) = grad(i) - DxR(j,k,n)*psi(ii)
              end do
            end do
          end do
          write (opt_unit,10) i
          write (opt_unit,20) grad(i)
          dvs(i) = tmp
        end do

      else
c     -- using cenral-differences to evaluate rhs sensitivity --
c     -- second order accurate --
        do i = 1,ndv
          tmp = dvs(i)
          stepsize = fd_eta*dvs(i)
c     -- calculate rhs at (+) --
          dvs(i) = dvs(i) + stepsize
          stepsize = dvs(i) - tmp

c     -- regrid domain --
          call regrid ( (i), jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot )

c     -- recalculate metrics --
          call xidif ( jdim, kdim, x, y, xy)
          call etadif ( jdim, kdim, x, y, xy)
          call calcmet ( jdim, kdim, xy, xyj)

c     -- recalculate rhs of cyclone --
          do n=1,4
            do k=1,kend
              do j=1,jend
                Rp(j,k,n) = 0.d0
                q(j,k,n) = q(j,k,n)/xyj(j,k)
              end do
            end do
          end do

          call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

          call xiexpl ( jdim, kdim, q, Rp, press, sndsp, turmu, fmu,
     &          vort, x, y, xy, xyj, xit, ds, uu, ccx, coef2, coef4,
     &          spect, precon, gam)

          call etaexpl ( jdim, kdim, q, Rp, press, sndsp, turmu, fmu,
     &          vort, x, y, xy, xyj, xit, ds, uu, vv, ccy, coef2, coef4,
     &          spect, precon, gam)

          call bcrhs ( jdim, kdim, x, y, xy, xyj, q, Rp, press )

          do n=1,4
            do k=1,kend
              do j=1,jend
                q(j,k,n) = q(j,k,n)*xyj(j,k)
              end do
            end do
          end do

c     -- calculate rhs at (-) --
          dvs(i) = tmp - stepsize

c     -- regrid domain --
          call regrid ( (i), jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot )

c     -- recalculate metrics --
          call xidif ( jdim, kdim, x, y, xy)
          call etadif ( jdim, kdim, x, y, xy)
          call calcmet ( jdim, kdim, xy, xyj)

c     -- recalculate rhs of cyclone --
          do n=1,4
            do k=1,kend
              do j=1,jend
                Rm(j,k,n) = 0.d0
                q(j,k,n) = q(j,k,n)/xyj(j,k)
              end do
            end do
          end do

          call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

          call xiexpl ( jdim, kdim, q, Rm, press, sndsp, turmu, fmu,
     &          vort, x, y, xy, xyj, xit, ds, uu, ccx, coef2, coef4,
     &          spect, precon, gam)

          call etaexpl ( jdim, kdim, q, Rm, press, sndsp, turmu, fmu,
     &          vort, x, y, xy, xyj, xit, ds, uu, vv, ccy, coef2, coef4,
     &          spect, precon, gam)

          call bcrhs ( jdim, kdim, x, y, xy, xyj, q, Rm, press )

          do n=1,4
            do k=1,kend
              do j=1,jend
                q(j,k,n) = q(j,k,n)*xyj(j,k)
              end do
            end do
          end do

c     -- evaluate sensitivity of the flow residual --
          dxx = 1.d0/(2.d0*stepsize)
          do n=1,4
            do k = 1,kend
              do j= 1,jend
                DxR(j,k,n) = ( Rp(j,k,n) - Rm(j,k,n) )*dxx
              end do
            end do
          end do

c     -- calculate gradient vector of the objective function -- 
          grad(i) = 0.d0
          do n = 1,4
            do k = 1,kend
              do j = 1,jend
                ii = ( index(j,k) - 1 )*4 + n
                grad(i) = grad(i) - DxR(j,k,n)*psi(ii)
              end do
            end do
          end do
          write (opt_unit,10) i
          write (opt_unit,30) grad(i)
          dvs(i) = tmp
        end do       
      end if

c     -- restore original grid --
      i = -1
      call regrid ( (i), jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &      bknot )

c     -- recalculate metrics --
      call xidif ( jdim, kdim, x, y, xy)
      call etadif ( jdim, kdim, x, y, xy)
      call calcmet ( jdim, kdim, xy, xyj)

      dt = dt_tmp

 10   format(/3x, 'Design variable:', i4)
 20   format(3x, 'Adjoint gradient with F. D. dR/dX:', e16.8)
 30   format(3x, 'Adjoint gradient with C. D. dR/dX:', e16.8)

      return
      end                       !drdx
