      subroutine csplin(x,y,n,xnew,ynew,nnew)                           
c                                                                       
c=====================================================================  
c                                                                       
#include "../include/parm.inc"
c                                                                       
      double precision x(n), y(n), xnew(nnew), ynew(nnew)                           
c                                                                       
c ********************************************************************* 
c                                                                       
c     This subroutine produces a monotone cubic spline interpolant   
c     to the data (x(i),y(i)) i=1,....n  and computes values at          
c     the new points xnew(i), i=1,....,nnew. These are returned in       
c     array y(i). The algorithm used is that outlined by Fritsch and     
c     Butland in Siam J. Sci. Stat. Comput., Vol. 5, No. 2, June, 1984.  
c                                                                       
c.....written by Jeff Cordova 10/26/86                                  
c                                                                       
c ********************************************************************* 
c                                                                       
      double precision d(maxj), del(maxj), h(maxj)                                  
c                                                                       
c ********************************************************************* 
c                  spline coefficient calculations                      
c ********************************************************************* 
c                                                                       
c.....mesh spacing and first divided difference                        
c                                                                       
      do 100 i=1,n-1                                                    
        h(i) = x(i+1) - x(i)                                            
 100  continue                                                          
c                                                                       
      do 200 i=1,n-1                                                    
        del(i) = (y(i+1) - y(i)) / h(i)                                 
 200  continue                                                          
c                                                                       
c.....spline coefficients                                              
c                                                                       
c     *** linear interpolation for n=2 case ***                      
c                                                                       
      if (n .eq. 2)  then                                               
        d(1) = del(1)                                                   
        d(n) = del(1)                                                   
        return                                                          
      endif                                                             
c                                                                       
c     *** monotone spline coefficients for n >= 3 case ***           
c                                                                       
c.....first boundary point 
c     (use three point formula altered to be shape preserving)
c                                                                       
      hsum = h(1) + h(2)                                                
      w1   = (h(1) + hsum) / hsum                                       
      w2   = -h(1) / hsum                                               
      d(1) = w1*del(1) + w2*del(2)                                      
      if (pchst(d(1),del(1)) .le. 0.)  then                             
        d(1) = 0.                                                       
      elseif (pchst(del(1),del(2)) .lt. 0.)  then                       
        dmax = 3.*del(1)                                                
        if (abs(d(1)) .gt. abs(dmax))  d(1) = dmax                      
      endif                                                             
c                                                                       
c.....interior points (brodlie modification of butland formula)         
c                                                                       
      const2 = 1.d0 / 3.d0                                                   
      do 300  i=2,n-1                                                   
        top   = del(i-1) * del(i)                                       
        top   = top * .5 * (1. + sign(1.,top))                          
        alpha = const2 * (h(i-1) + 2.*h(i)) / (h(i-1) + h(i))            
        bot   = alpha * del(i) + (1.-alpha) * del(i-1) + 1.e-20         
        d(i)  = top / bot                                               
 300  continue                                                          
c                                                                       
c.....last boundary point 
c     (use three point formula adjusted to be shape preserving)
c                                                                       
      hsum = h(n-2) + h(n-1)                                            
      w1   = -h(n-1) / hsum                                             
      w2   = (h(n-1) + hsum) / hsum                                     
      d(n) = w1*del(n-2) + w2*del(n-1)                                  
      if (pchst(d(n),del(n-1)) .le. 0.)  then                           
        d(n) = 0.                                                       
      elseif ( pchst(del(n-2),del(n-1)) .lt. 0.)  then                  
        dmax = 3.*del(n-1)                                              
        if (abs(d(n)) .gt. abs(dmax))  d(n) = dmax                      
      endif                                                             
c                                                                       
c *******************************************************************   
c                       spline evaluation                               
c *******************************************************************   
c                                                                       
c.....xnew(i) .le. x(n)                                                 
c                                                                       
      iend = 1                                                          
      do 400 j=1,n-1                                                    
        cthree = (d(j) + d(j+1) - 2.*del(j)) / (h(j)*h(j))              
        ctwo   = (3.*del(j) - 2.*d(j) - d(j+1)) / h(j)                  
        ibeg = iend                                                     
        iend = isrchfge(nnew,xnew,1,x(j+1))                             
        do 500 i=ibeg,iend-1                                            
          t =  xnew(i) - x(j)                                           
          ynew(i) = y(j) + t*(d(j) + t*(ctwo + t*cthree))               
 500    continue                                                        
 400  continue                                                          
c                                                                       
c.....xnew(i) .gt. x(n)                                                 
c                                                                       
      do 600 i=iend,nnew                                                
        t =  xnew(i) - x(n-1)                                           
        ynew(i) = y(n-1) + t*(d(n-1) + t*(ctwo + t*cthree))             
 600  continue                                                          
                                                                        
c                                                                       
      return                                                            
      end                                                               
