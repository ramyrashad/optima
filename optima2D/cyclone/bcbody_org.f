      subroutine bcbody(jdim,kdim,q,kbc,xy,xit,ett,xyj,x,y,             
     >                  a,b,c,f,g,z)                                    
c
#include "../include/arcom.inc"
c
c                                                                       
      dimension q(jdim,kbc,4)                                           
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c                                                                       
      dimension a(jdim),b(jdim),c(jdim),f(jdim),g(jdim),z(jdim)         
c                                                                       
c     -boundary conditions for airfoil topology
c     -C  or  O mesh type             
c                                                                       
c     -x coordinates of start and end of body                 
      j1 = jtail1                                                    
      j2 = jtail2                                                    
c                                                                    
c     -y coordinate along the body                            
                                                                     
      k = 1                                                          
c                                                                       
c     foso =0. for first order and =1. for second order accur
c     in the evaluation of dpdeta                            
c                                                                       
      foso = 0.                                                      
c                                                                       
c                                                                       
c.....................................................................  
c          this scaling is used if the calculation is not being restarte
c          it is used to bring the calculation up slowly, rather than   
c          impulsively.  it uses a sixth degree polynomial f            
c          for a smooth transition from scal slightly bigger than 0. at 
c          to first iteration, to scal = 1. after the strtit'th iteratio
c                                                                       
c          the scaling is turned off by restart if this is a restart run
c.....................................................................  
c                                                                       
      t = float(numiter - 1)/ strtit                                       
      if( t .gt.1.) t = 1.                                              
      scal = (10. -15.*t + 6.*t*t) *t**3                                
c                                                                       
c                no scaling if this is a restart                        
c                                                                       
      if( restart ) scal=1.                                             
c                                                                       
c---------------------------------------------------------------------  
c                    solid surface conditions                           
c---------------------------------------------------------------------  
      k=1                                                               
c********************************************************************** 
c********************************************************************** 
c             inviscid bc at surface                                    
c                                                                       
      if( .not.viscous ) then                                           
c                                                                       
c********************************************************************** 
c********************************************************************** 
c                                                                       
c         obtain u and v on body via tangency and extrapolation of ucap 
c         if viscous flow, ucap = 0.0                                   
c                                                                       
c                first order extrapolation on cap-u                     
c                                                                       
c                                                                       
c.....................................................................  
c                inviscid velocity bc at body                           
c.....................................................................  
c                                                                       
         do 30 j=j1, j2                                                 
c                                                                       
        k = 2                                                           
c                                                                       
            xyss = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                      
            xyssr = 1./xyss                                             
            xy3n2 = xy(j,k,3)*xyssr                                     
            xy4n2 = xy(j,k,4)*xyssr                                     
            rr = 1./q(j,k,1)                                            
            u2 = q(j,k,2)*rr                                            
            v2 = q(j,k,3)*rr                                            
c                                                                       
         k = 3                                                          
c                                                                       
            xyss = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                      
            xyssr = 1./xyss                                             
            xy3n3 = xy(j,k,3)*xyssr                                     
            xy4n3 = xy(j,k,4)*xyssr                                     
            rr = 1./q(j,k,1)                                            
            u3 = q(j,k,2)*rr                                            
            v3 = q(j,k,3)*rr                                            
c                                                                       
c  extrapolate tangential component of velocity                         
c                                                                       
            utang = 2.*(xy4n2*u2 - xy3n2*v2) - (xy4n3*u3 - xy3n3*v3)    
c                                                                       
c  set normal component to zer0                                         
c                                                                       
            vnorm = 0.                                                  
c                                                                       
c  note !!! 12/13/84 thp  need to add unsteady terms                    
c                                                                       
          k = 1                                                         
c                                                                       
            xyss = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                      
            xyssr = 1./xyss                                             
            xy3n1 = xy(j,k,3)*xyssr                                     
            xy4n1 = xy(j,k,4)*xyssr                                     
c                                                                       
            u1 =  xy4n1*utang + xy3n1*vnorm                             
            v1 = -xy3n1*utang + xy4n1*vnorm                             
            u1 = ( 1. - scal)*q(j,k,2)/q(j,k,1) + scal*u1               
            v1 = ( 1. - scal)*q(j,k,3)/q(j,k,1) + scal*v1               
c                                                                       
c              q2 and q3 now satisfy tangency... independently of whether
c              density is lagged                                      
c                                                                       
            q(j,k,2) = u1*q(j,k,1)                                      
            q(j,k,3) = v1*q(j,k,1)                                      
c                                                                       
   30    continue                                                       
c                                                                       
         if( periodic )then                                             
c                                                                       
c.....................................................................  
c  at trailing edge do something different                              
c.....................................................................  
c   v /= 0                                                              
c                                                                       
              j = 1                                                     
              k = 1                                                     
c                                                                       
            if(sharp) then                                              
c              stagnation at trailing edge                              
c              note this logic is usually not necessary except when     
c              grids are highly clustered at t. e. and a negative       
c              jacobian occurs (see xymets)                             
                  u1 = 0.0                                              
                  v1 = 0.0                                              
c                                                                       
c                q2 and q3 now satify tangency... independently of wheth
c                density is lagged                                      
c                                                                       
            u1 = ( 1. - scal)*q(j,k,2)/q(j,k,1) + scal*u1               
            v1 = ( 1. - scal)*q(j,k,3)/q(j,k,1) + scal*v1               
            q(j,k,2) = u1*q(j,k,1)                                      
            q(j,k,3) = v1*q(j,k,1)                                      
c                                                                       
            elseif(cusp)then                                            
c              smooth velocities off trailing edge                      
               u1l = q(2,k,2)/q(2,k,1)                                  
               v1l = q(2,k,3)/q(2,k,1)                                  
               u1u = q(jup,k,2)/q(jup,k,1)                              
               v1u = q(jup,k,3)/q(jup,k,1)                              
               u1 = 0.5*(u1l+u1u)                                       
               v1 = 0.5*(v1l+v1u)                                       
c                                                                       
c                                                                       
c                q2 and q3 now satify tangency... independently of wheth
c                density is lagged                                      
c                                                                       
            u1 = ( 1. - scal)*q(j,k,2)/q(j,k,1) + scal*u1               
            v1 = ( 1. - scal)*q(j,k,3)/q(j,k,1) + scal*v1               
            q(j,k,2) = u1*q(j,k,1)                                      
            q(j,k,3) = v1*q(j,k,1)                                      
c                                                                       
c                                                                       
             endif                                                      
         endif                                                          
c                                                                       
c                                                                       
c---------------------------------------------------------------------  
c       satisfy momentum relation for normal pressure derivation        
c---------------------------------------------------------------------  
c                                                                       
         goto 937
      ja = j1                                                           
      jb = j2                                                           
c       for periodic meshes around sharp or cusp trailing edges         
c       we don't want to integrate across t. e.                         
        if(periodic)then                                                
           if(sharp .or. cusp) ja = j1+1                                
        endif                                                           
      k = 1                                                             
      do 40 j=ja,jb                                                     
      jp1 = jplus(j)                                                    
      jm1 = jminus(j)                                                   
      a5 = 0.5                                                          
         if(j.eq.j1)then                                                
            jm1 = j                                                     
            a5 = 1.0                                                    
         elseif(j.eq.j2)then                                            
            jp1 = j                                                     
            a5 = 1.0                                                    
         endif                                                          
         rho = q(j,k,1)*xyj(j,k)                                        
         uu = ( xy(j,k,1)*q(j,k,2) + xy(j,k,2)*q(j,k,3))/q(j,k,1)       
     1    + xit(j,k)                                                    
         uxi = a5*( q(jp1,k,2)/q(jp1,k,1) - q(jm1,k,2)/q(jm1,k,1))      
         vxi = a5*( q(jp1,k,3)/q(jp1,k,1) - q(jm1,k,3)/q(jm1,k,1))      
         ff = rho*uu*( xy(j,k,3)*uxi+xy(j,k,4)*vxi)                     
         c1 = xy(j,k,3)*xy(j,k,1) + xy(j,k,2)*xy(j,k,4)                 
         c2 = xy(j,k,3)**2 + xy(j,k,4)**2                               
         p2 = gami*( q(j,2,4) -.5*(q(j,2,2)**2+q(j,2,3)**2) /q(j,2,1) ) 
         p3 = gami*( q(j,3,4) -.5*(q(j,3,2)**2+q(j,3,3)**2) /q(j,3,1) ) 
         f(j) = -ff+c2*(-(1.+foso)*p2*xyj(j,k+1)+.5*foso*p3*xyj(j,k+2)) 
         a(j) = -.5*c1                                                  
         c(j) = .5*c1                                                   
         b(j) = -(1.+.5*foso)*c2                                        
   40 continue                                                          
c                                                                       
c                   periodic logic                                      
c                                                                       
      if(.not.periodic .or. sharp .or. cusp)then                        
c                                                                       
         jj = jminus(ja)                                                
         pbc = gami *                                                   
     1       ( q(jj,k,4) -.5*( q(jj,k,2)**2 +q(jj,k,3)**2)/q(jj,k,1) )  
         f(ja) = f(ja) - a(ja)*pbc*xyj(jj,k)                            
         jj = jplus(jb)                                                 
         pbc = gami *                                                   
     1       ( q(jj,k,4) -.5*( q(jj,k,2)**2 +q(jj,k,3)**2)/q(jj,k,1) )  
         f(jb) = f(jb) - c(jb)*pbc*xyj(jj,k)                            
         call trib(jdim,a,b,c,z,f,ja,jb)                                
             if(sharp .or. cusp)f(j1) = 0.5*(f(ja)+f(jb))               
c                                                                       
      elseif(periodic)then                                              
c                                                                       
            call trip(jdim,a,b,c,f,z,g,ja,jb)                           
c                                                                       
      endif                                                             
c                                                                       
capc---- pressure interpolation ----
 937  continue
      do 60 j =jtail1,jtail2
        p2 = gami*(q(j,2,4)-.5*(q(j,2,2)**2+q(j,2,3)**2)/q(j,2,1)) 
        p3 = gami*(q(j,3,4)-.5*(q(j,3,2)**2+q(j,3,3)**2)/q(j,3,1)) 
        f(j) = 2.d0*p2*xyj(j,2) - p3*xyj(j,3)
 60   continue
c                                                                       
c                                                                       
c                 hstfs is the stagnation freestream enthalpy           
c                                                                       
         hstfs = 1./gami + 0.5*fsmach**2                                
         do 45 j=j1,j2                                                  
c                                                                       
c                  rescale q2 and q3 to new density such that when      
c                  reforming pressure  from q1 to q4,                   
c                  the normal derivative of pressure is satified        
c                                                                       
c                  enforce constant free stream stagnation enthalpy at b
c                                                                       
            rinver = 1./q(j,k,1)                                        
            u = q(j,k,2)*rinver                                         
            v = q(j,k,3)*rinver                                         
            q(j,k,1) = gamma*f(j)/(gami* (hstfs - 0.5*(u**2 + v**2) ) ) 
            q(j,k,1) = q(j,k,1)/xyj(j,k)                                
            q(j,k,2) = u*q(j,k,1)                                       
            q(j,k,3) = v*q(j,k,1)                                       
            f(j) = f(j)/xyj(j,k)                                        
            q(j,k,4) = f(j)/gami +                                      
     %      .5 * ( q(j,k,2)**2+q(j,k,3)**2) /q(j,k,1)                   
   45    continue                                                       
c                                                                       
c                                                                       
c********************************************************************** 
c********************************************************************** 
c   viscous surface bc                                                  
c                                                                       
       elseif(viscous)then                                              
c                                                                       
c********************************************************************** 
c********************************************************************** 
c                                                                       
         k = 1                                                          
c                                                                       
c.....................................................................  
c                viscous velocity bc at body                            
c.....................................................................  
c                                                                       
         do 35 j=j1, j2                                                 
c                                                                       
c  cap u and cap v = 0.0                                                
c                                                                       
            u1 = ( xy(j,1,4)*(- xit(j,1)) + xy(j,1,2)*ett(j,1))/xyj(j,1)
            v1 = (- xy(j,1,3)*(- xit(j,1)) -xy(j,1,1)*ett(j,1))/xyj(j,1)
            u1 = ( 1. - scal)*q(j,1,2)/q(j,1,1) + scal*u1               
            v1 = ( 1. - scal)*q(j,1,3)/q(j,1,1) + scal*v1               
c                                                                       
c                q2 and q3 now satify tangency... independently of wheth
c                density is lagged                                      
c                                                                       
            q(j,1,2) = u1*q(j,1,1)                                      
            q(j,1,3) = v1*q(j,1,1)                                      
c                                                                       
   35    continue                                                       
c                                                                       
c...................................................................... 
c   surface condition on pressure                                       
c...................................................................... 
          do 47 j = j1,j2                                               
c                                                                       
c  dp/dn = 0.0                                                          
c                                                                       
c                                                                       
c             extrap p                                                  
c                                                                       
      fosop = 0.0                                                       
         p2 = gami*( q(j,2,4) -.5*(q(j,2,2)**2+q(j,2,3)**2) /q(j,2,1) ) 
         p3 = gami*( q(j,3,4) -.5*(q(j,3,2)**2+q(j,3,3)**2) /q(j,3,1) ) 
         f(j)      = ((1.+fosop)*p2*xyj(j,2)                            
     >             - fosop*p3*xyj(j,3))                                 
c                                                                       
c  note f(j) doesn't have jacobian in it                                
c                                                                       
47       continue                                                       
c                                                                       
c  sharp trailing edge                                                  
c                                                                       
       if(periodic)then                                                 
          j = j1                                                        
          jpl = jplus(j)                                                
          jmi = jminus(j)                                               
          if(sharp .or. cusp)f(j) = 0.5*(f(jpl) + f(jmi))               
       endif                                                            
c                                                                       
         do 48 j = j1, j2                                               
c                                                                       
c                                                                       
c                    first order of density extrapolation seems to      
c                    be unstable for viscous calculations.              
c                    zero'th order is used.                             
c                    or constant wall temperature                       
c                                                                       
            rinver = 1./q(j,k,1)                                        
            rj = 1./xyj(j,k)                                            
c                                                                       
                if(wtrat.ne.0.0)then                                    
c                    fix wall temp   tim  barth 10/10/84                
c                                                                       
c           twall is the ratio of twall/tinf                            
c                                                                       
                prs = f(j)*rj                                           
                twall = wtrat/gamma                                     
                q(j,k,1) = prs/twall                                    
                else                                                    
                q(j,k,1) = q(j,k+1,1)*xyj(j,k+1)*rj                     
                endif                                                   
            u = q(j,k,2)*rinver                                         
            v = q(j,k,3)*rinver                                         
            q(j,k,2) = u*q(j,k,1)                                       
            q(j,k,3) = v*q(j,k,1)                                       
            q(j,k,4) = f(j)/gami*rj +                                   
     %         .5 * ( q(j,k,2)**2+q(j,k,3)**2) /q(j,k,1)                
   48    continue                                                       
c                                                                       
c********************************************************************** 
c********************************************************************** 
c    end surface conditions                                             
c                                                                       
        endif                                                           
c                                                                       
       return                                                           
       end                                                              
