c-----------------------------------------------------------------------
c     -- the non-linear (JST) artificial dissipation coefficients are
c     added to the Jacobian matrix and preconditioner: x direction. --
c     -- m. nemec, july 2001 --
c-----------------------------------------------------------------------
      subroutine filladx( pdc, jmax, kmax, nmax, ibc1, ibc2, ibc3, ibc4,
     &     kminbnd, kmaxbnd, jminbnd, jmaxbnd, j1, j2, k1, k2,
     &     indx, xyj, coef2, coef4, pa, as, icol, precmat, jacmat)

      implicit none

      integer jmax,kmax,nmax,ibc1,ibc2,ibc3,ibc4,kminbnd,kmaxbnd
      integer jminbnd,jmaxbnd,j1,j2,k1,k2,indx(jmax,kmax),icol(*),j,k
      integer js,je,ks,ke,n,ii,jj,jm2,jm1,jp1,jp2
      double precision xyj(jmax,kmax),pa(*),as(*),pdc,c2m,c4m,c2,c4
      double precision coef2(jmax,kmax),coef4(jmax,kmax)
      double precision diag1,diag2,diag3,diag4,diag5
      logical precmat,jacmat

c     -- form preconditioner after jacobian: coef2 is overwritten! --

c     -----------------------------------------------------------------
c     -- dissipation for jacobian --
      if (jacmat) then

c     -- interior nodes (two nodes interior from b.c.) --
         ks = kminbnd + 2
         ke = kmaxbnd - 2

         if (ibc2.EQ.0) then
            js = jminbnd
         else
            js = jminbnd +2
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd -2
         end if

         do k =ks,ke                                         
            do j =js,je                                             

               jp1 = j+1
               jm1 = j-1
               jp2 = j+2
               jm2 = j-2

               c2m = coef2(jm1,k)                                       
               c4m = coef4(jm1,k)                                       
               c2  = coef2(j,k)
               c4  = coef4(j,k)                                         

               diag1 = xyj(jm2,k)*c4m
               diag2 = xyj(jm1,k)*(c2m + 3.0*c4m + c4)
               diag3 = xyj(j,k)  *(c2m + 3.0*c4m + c2 + 3.0*c4)
               diag4 = xyj(jp1,k)*(c2  + 3.0*c4  + c4m)
               diag5 = xyj(jp2,k)*c4

               do n =1,4
                  ii = ( indx(j,k) - 1 )*nmax + n
                  jj = ( ii - 1 )*icol(9)
                  as(jj+        n) = as(jj+        n) + diag1
                  as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
                  as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag3
                  as(jj+icol(7)+n) = as(jj+icol(7)+n) - diag4
                  as(jj+icol(8)+n) = as(jj+icol(8)+n) + diag5
               end do
            end do
         end do

c     -- first interior boundary nodes --
         if (ibc2.EQ.0) then
            js = jminbnd
         else
            js = jminbnd + 2
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd - 2
         end if

         k = kminbnd+1
         do j = js,je
            jp1 = j+1
            jm1 = j-1
            jp2 = j+2
            jm2 = j-2                                             

            c2m = coef2(jm1,k)                                       
            c4m = coef4(jm1,k)                                       
            c2  = coef2(j,k)                                          
            c4  = coef4(j,k)
            
            diag1 = xyj(jm2,k)*c4m
            diag2 = xyj(jm1,k)*(c2m + 3.0*c4m + c4)
            diag3 = xyj(j,k)  *(c2m + 3.0*c4m + c2 + 3.0*c4)
            diag4 = xyj(jp1,k)*(c2  + 3.0*c4  + c4m)
            diag5 = xyj(jp2,k)*c4

            do n =1,4
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)
               as(jj+        n) = as(jj+        n) + diag1
               as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag3
               as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
               as(jj+icol(7)+n) = as(jj+icol(7)+n) + diag5
            end do
         end do
         
         k = kmaxbnd-1
         do j = js,je 
            jp1 = j+1
            jm1 = j-1
            jp2 = j+2
            jm2 = j-2     

            c2m = coef2(jm1,k)                                       
            c4m = coef4(jm1,k)                                       
            c2  = coef2(j,k)                                          
            c4  = coef4(j,k)                                         

            diag1 = xyj(jm2,k)*c4m
            diag2 = xyj(jm1,k)*(c2m + 3.0*c4m + c4)
            diag3 = xyj(j,k)  *(c2m + 3.0*c4m + c2 + 3.0*c4)
            diag4 = xyj(jp1,k)*(c2  + 3.0*c4  + c4m)
            diag5 = xyj(jp2,k)*c4

            do n =1,4
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)
               as(jj+        n) = as(jj+        n) + diag1
               as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
               as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag3
               as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
               as(jj+icol(7)+n) = as(jj+icol(7)+n) + diag5
            end do
         end do

         if (ibc2.NE.0) then
            j   = jminbnd + 1
            jm1 = j-1
            jp1 = j+1
            jp2 = j+2
            ks  = kminbnd + 2
            ke  = kmaxbnd - 2

            do k = ks,ke         
               c2m = coef2(jm1,k)                                       
               c4m = coef4(jm1,k)                                       
               c2  = coef2(j,k)
               c4  = coef4(j,k)
               
               diag2 = (c2m + c4m + c4)*xyj(jm1,k)                    
               diag3 = xyj(j,k)*(c2m+2.*c4m+c2+3.*c4)                 
               diag4 = (c2 + 3.*c4 + c4m)*xyj(jp1,k)                  
               diag5 = xyj(jp2,k)*c4  

               do n =1,4
                  ii = ( indx(j,k) - 1 )*nmax + n
                  jj = ( ii - 1 )*icol(9)
                  as(jj+        n) = as(jj+n)         - diag2
                  as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag3
                  as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
                  as(jj+icol(7)+n) = as(jj+icol(7)+n) + diag5
               end do
            end do

            k = kminbnd + 1
            jp1 = j+1
            jm1 = j-1
            jp2 = j+2

            c2m = coef2(jm1,k)                                       
            c4m = coef4(jm1,k)                                       
            c2  = coef2(j,k)                                          
            c4  = coef4(j,k)                                         
            
            diag2 = (c2m + c4m + c4)*xyj(jm1,k)                    
            diag3 = xyj(j,k)*(c2m + 2.0*c4m + c2 + 3.0*c4)
     &           
            diag4 = (c2 + 3.0*c4 + c4m)*xyj(jp1,k)                  
            diag5 = xyj(jp2,k)*c4
            
            do n =1,4
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)
               as(jj+        n) = as(jj+        n) - diag2
               as(jj+icol(2)+n) = as(jj+icol(2)+n) + diag3
               as(jj+icol(5)+n) = as(jj+icol(5)+n) - diag4
               as(jj+icol(6)+n) = as(jj+icol(6)+n) + diag5
            end do

            k = kmaxbnd - 1 
            jp1 = j+1
            jm1 = j-1
            jp2 = j+2

            c2m = coef2(jm1,k)                                       
            c4m = coef4(jm1,k)                                       
            c2  = coef2(j,k)                                          
            c4  = coef4(j,k)                                         

            diag2 = (c2m + c4m + c4)*xyj(jm1,k)                    
            diag3 = xyj(j,k)*(c2m + 2.0*c4m + c2 +3.0*c4)
     &           
            diag4 = (c2 + 3.0*c4 + c4m)*xyj(jp1,k)                  
            diag5 = xyj(jp2,k)*c4      

            do n =1,4
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)
               as(jj+        n) = as(jj+        n) - diag2
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag3
               as(jj+icol(5)+n) = as(jj+icol(5)+n) - diag4
               as(jj+icol(6)+n) = as(jj+icol(6)+n) + diag5
            end do
         end if
         
         if (ibc4.NE.0) then
            j   = jmaxbnd - 1
            jm1 = j-1
            jp1 = j+1
            ks  = kminbnd + 2
            ke  = kmaxbnd - 2
            jm1 = j-1
            jm2 = j-2
            jp1 = j+1
            do k =ks,ke

               c2m = coef2(jm1,k)                                       
               c4m = coef4(jm1,k)                                       
               c2  = coef2(j,k)
     &              
               c4  = coef4(j,k)    

               diag1 = xyj(jm2,k)*c4m                                 
               diag2 = (c2m + 3.0*c4m + c4)*xyj(jm1,k)                 
               diag3 = xyj(j,k)*(c2m + 3.0*c4m + c2 + 2.0*c4)
     &              
               diag4 = (c2 + c4 + c4m)*xyj(jp1,k) 

               do n =1,4
                  ii = ( indx(j,k) - 1 )*nmax + n
                  jj = ( ii - 1 )*icol(9)
                  as(jj+        n) = as(jj+        n) + diag1
                  as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
                  as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag3
                  as(jj+icol(7)+n) = as(jj+icol(7)+n) - diag4
               end do
            end do

            k = kminbnd + 1      
            jp1 = j+1
            jm1 = j-1
            jm2 = j-2

            c2m = coef2(jm1,k)                                       
            c4m = coef4(jm1,k)                                       
            c2  = coef2(j,k)                                          
            c4  = coef4(j,k)

            diag1 = xyj(jm2,k)*c4m                                 
            diag2 = (c2m + 3.0*c4m + c4)*xyj(jm1,k)                 
            diag3 = xyj(j,k)*(c2m + 3.0*c4m + c2 + 2.0*c4)
     &           
            diag4 = (c2 + c4 + c4m)*xyj(jp1,k)

            do n =1,4
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)
               as(jj+        n) = as(jj+        n) + diag1
               as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag3
               as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
            end do

            k = kmaxbnd - 1
            jp1 = j+1
            jm1 = j-1
            jm2 = j-2

            c2m = coef2(jm1,k)                                       
            c4m = coef4(jm1,k)                                       
            c2  = coef2(j,k)                                          
            c4  = coef4(j,k)                                         

            diag1 = xyj(jm2,k)*c4m                                 
            diag2 = (c2m + 3.0*c4m + c4)*xyj(jm1,k)                 
            diag3 = xyj(j,k)*(c2m + 3.0*c4m + c2 + 2.0*c4)
     &           
            diag4 = (c2 + c4 + c4m)*xyj(jp1,k)

            do n =1,4
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)
               as(jj+        n) = as(jj+        n) + diag1
               as(jj+icol(1)+n) = as(jj+icol(1)+n) - diag2
               as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag3
               as(jj+icol(6)+n) = as(jj+icol(6)+n) - diag4
            end do
         end if
      end if
      
c     ------------------------------------------------------------------
c     -- dissipation for first-order preconditioner --
      IF (precmat) THEN
         DO k = k1,k2
            DO j = j1,j2
               coef2(j,k) = coef2(j,k) + pdc*coef4(j,k)
            ENDDO
         ENDDO

         DO j = j1+1,j2-1
            DO k = k1,k2
               jp1 = j + 1
               jm1 = j - 1
               c2  = coef2(j,k)
               c2m = coef2(jm1,k)

               diag1 = xyj(jm1,k)*c2m
               diag2 = xyj(j,k)  *(c2m + c2)
               diag3 = xyj(jp1,k)*c2

               DO n =1,4
                  ii = ( indx(j,k) - 1 )*nmax + n
                  jj = ( ii - 1 )*icol(5)
                  pa(jj+n)         = pa(jj+n)         - diag1
                  pa(jj+icol(2)+n) = pa(jj+icol(2)+n) + diag2
                  pa(jj+icol(4)+n) = pa(jj+icol(4)+n) - diag3
               ENDDO
            ENDDO
         ENDDO

      ENDIF

      return
      end                       ! filladx
