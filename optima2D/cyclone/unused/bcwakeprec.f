      subroutine bcwakeprec(jdim,kdim,q,xy,xit,ett,xyj,x,y,
     &                      sndsp,press) 
c     parameter (maxj=497)                                              
      include '../include/arcom.inc'
c                                                                       
      dimension q(jdim,kdim,4)                                           
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension press(jdim,kdim),sndsp(jdim,kdim)
c                                                                       
c                                                                       
c---------------------------------------------------------------------  
c                permeable boundary condition  (wake)                   
c---------------------------------------------------------------------  
c                                                                       
      if(.not.periodic)then                                             
         js = jtail1 -1                                                 
         if( js.lt.2) go to 350                                         
            do 20 j=jbegin,js                                           
             i = jend +1 - j                                            
             rrj = 1./xyj(j,1)                                          
             rri = 1./xyj(i,1)                                          
c
             qup1=q(i,2,1)*xyj(i,2)
             qlow1=q(j,2,1)*xyj(j,2)
c
             qup2=q(i,2,2)*xyj(i,2)
             qlow2=q(j,2,2)*xyj(j,2)
c
             qup3=q(i,2,3)*xyj(i,2)
             qlow3=q(j,2,3)*xyj(j,2)
c
             qup4=q(i,2,4)*xyj(i,2)
             qlow4=q(j,2,4)*xyj(j,2)
c
             qupu=(qup2**2+qup3**2)/qup1**2
             qlowu=(qlow2**2+qlow3**2)/qlow1**2
c
c           -form average of non-conservative variables
             sav1=.5d0*(qup1+qlow1)
             sav2=.5d0*(qup2/qup1+qlow2/qlow1)
             sav3=.5d0*(qup3/qup1+qlow3/qlow1)
             sav4=.5d0*(qup4+qlow4)
             savu=sav2**2+sav3**2
             dup=gami*(sav4-0.5d0*(savu)*sav1)
             duc2=gamma*dup/sav1
             savc=sqrt(duc2)
c
c           -multiply by M for upper wake line
             du1=(qup1*qup1-qup4/sndsp(i,2))/sndsp(i,2)
             du2=qup2*du1/qup1+qup1*qup2
             du3=qup3*du1/qup1+qup1*qup3
             du4=qup1*((qupu/2.0/sndsp(i,2)+sndsp(i,2)/gami)*qup1)+
     +            qupu-qupu/2.0/sndsp(i,2)**2*qup4
         
c           -multiply by M for lower wake line
             dl1=(qlow1*qlow1-qlow4/sndsp(j,2))/sndsp(j,2)
             dl2=qlow2*du1/qlow1+qlow1*qlow2
             dl3=qlow3*du1/qlow1+qlow1*qlow3
             dl4=qlow1*((qlowu/2.0/sndsp(j,2)+sndsp(j,2)/gami)*qlow1)+
     +            qlowu-qlowu/2.0/sndsp(j,2)**2*qlow4
c
             av1=.5d0*(du1+dl1)
             av2=.5d0*(du2+dl2)
             av3=.5d0*(du3+dl3)
             av4=.5d0*(du4+dl4)
c
c           -multiply by Minv for wake line
             du4=gami*(savu/2.0*av1-sav2*av2-sav3*av3+av4)
             du1=du4/sav1/savc
             du4=du4-duc2*av1
             du2=(av2-sav2*av1)/sav1
             du3=(av3-sav3*av1)/sav1
c
             q(j,1,1) = du1*rrj                                      
             q(i,1,1) = du1*rri                                      
                q(j,1,2) = du2*rrj                                    
                q(i,1,2) = du2*rri                                    
                   q(j,1,3) = du3*rrj                                  
                   q(i,1,3) = du3*rri                                  
                     q(j,1,4) = du4*rrj                                  
                     q(i,1,4) = du4*rri                                  
c                                                                       
   20       continue                                                    
  350    continue                                                       
c                                                                       
      endif                                                             
c                                                                       
       return                                                           
       end                                                              
