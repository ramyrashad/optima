#ifndef FACE_H
#define FACE_H

#include "Node.h"


class Face  
{  
 private:
   int ID;
   int nvertices;
   int *vertexID;
   double area;
   void calc_area(Node** corners);

  public:

  //Default constructor
  Face(); 

  void clearmem();

  //Constructor of triangular face
  Face(Node** corners, const int& faceID, const int& vertices);


  //Constructor of quadrilateral face
  Face (Node** corners, const int& faceID);
   
  ~Face();       // Destructor
  void add_nei(Node** corners);
  // accessor function
  int get_ID() const { return ID;}
  int *get_vertexID() const {return vertexID;}
  int get_nvertices() const {return nvertices;} 
  double get_area() const {return area;}

  friend ostream& operator <<(ostream& out, const Face& a);
 };

#endif















