C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C                                                                      C
C                                                                      C
C          *** GENETIC ALGORITHM (GA) ***                              C
C                                                                      C
C                                                                      C
C          *** THIS COMPUTER PROGRAM USES A GENETIC ALGORITHM          C
C              FOR OPTIMIZATION PROBLEMS ***                           C
C
C                                                                      C
C                                                                      C
C          *** WRITTEN BY:  DR. TERRY L. HOLST                         C
C                           RESEARCH SCIENTIST                         C
C                           DR. THOMAS H. PULLIAM                      C
C                           RESEARCH SCIENTIST                         C
C                           NUMERICAL AEROSPACE SIMULATION (NAS)       C
C                              SYSTEMS DIVISION (CODE IN)              C
C                           MAIL STOP T27B-1                           C
C                           NASA AMES RESEARCH CENTER                  C
C                           MOFFETT FIELD, CALIFORNIA 94035-1000       C
c                  Help:
C                           (650) 604-6417 VOICE                       C
C                           E-MAIL: thomas.h.pulliam@nasa.gov          C
C
C          *** VERSION 0.1, Feb.  2002 ***                             C
C          *** VERSION 0.2, Feb.  2002 ***                             C
C          *** VERSION 0.3, Mar.  2002 ***                             C
C          *** VERSION 0.4, Mar. 19  2002 ***                          C
C          *** VERSION 1.6, Feb. 23  2005 ***                          C
C          *** VERSION 1.8a, May. 14  2008 ***                         C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C

To Make:
use makefile included.

e.g. make -f IFORT.make install

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

Usage:  Evolver_(VERSION) NGA NPOP NCHROM GA_FILE_PREFIX
  or::  -h:help, -v:version, -i inputs 

Where 
     NGA              :  ( 0: to initialize, >0: for current gen  NPOP: to get stats 
     NPOP             :   Total # of generations 
     NCHROM           : # of chromosomes 
     GA_FILE_PREFIX   : Prefix for ga files: uses GA_FILE_PREFIX_ga.i as ga.i input for Efvolver

Evolver_(VERSION) -i returns
    -----------------------------------------------------
    NAMELIST GCONTRL Variables
    -----------------------------------------------------
    
    ngene:          number of gene in chromsome (D=10)
    gene(min/max):  array size=ngene 
                    min/max for genes
    geneprec:       T: use gene precision (D=F)
    igeneprec:      precision for each gene (all D=0)
                    array of size (ngene)  
                    forces each gene to that precision
              e.g 1.23456789 igeneprec=3 => 1.23000000
    
    nobfun:         number of objective functions (D=1)
    
    i_mnx...INTEGER PARAMETER THAT CONTROLS WHETHER THE 
       OPTIMIZATION IS MAXIMIZATION OR MINIMIZATION
       I_MNX=1...MAXIMIZATION
       I_MNX=0...MINIMIZATION (D)
    
    arc2d_io: Switch for chromo formats
          True:  Old format,  no labels
          False: Label format See field defs below (D)
    
    int_real:  fixes data to integers (all D=0)
    
    field_(ngene,pop,nobfun,fit,log): (see input.f for D)
           ngene:  field label for ngene
           pop:    field label for pop (ngene of these)
           nobfun: field label for number of obj funs
           fit:    field label for fit (nobfun of these)
           log:    field for logical swtich T/F
    
    iselect:        selection choice
            1...GREEDY SELECTION+ min/max (D)
            2...TOURNAMENT SELECTION + min/max
            3...GREEDY SELECTION 
            4...TOURNAMENT SELECTION
            5...Selection from the accumulation file
            6...Selection from the accumulation file 
                with endpoints from active file
            7...Simple bin selection ala Holst
            8...New bin selection ala Holst
    Nbin    number of bins for iselect 7,8 (D=5)
    is_bin threshold for nunber of elements in accum file
            before binning starts (D=20)
    eps_domin: epsilon dominance, dominance based on,
    it > or < fit*(1+eps_domin)  (D=0)
    
    ICULL...INTEGER PARAMETER THAT CONTROLS THE CULLING 
        ALGORITHM. THE CULLING ALGORITHM LOGIC DISCARDS 
        ENTRIES IN THE ACCUMULATION FILE WHEN THE THE 
        NUMBER OF ENTRIS IN THE ACCUMULATION FILE EXCEEDS
        ICULL. THIS DISCARDING LOGIC ATTEMPTS TO FORCE 
        EQUAL SPACING ALONG THE PARETO FRONT AND ONLY
        MAKES SENSE WHEN NOBFUN>1 AND IACCUM=1.
        ICULL=0...CULLING LOGIC IS TURNED OFF. (D)
        ICULL=N...CULLING LOGIC IS EXECUTED FOR EVERY
                  GENERATION IN WHICH THE NUMBER OF 
                  ELEMENTS IN THE ACCUMULATION FILE
                  EXCEEDS N. THIS OPTION UTILIZES AN
                  ARC-LENGTH ALGORITHM AND IS ONLY VALID
                  WHEN NOBFUN=2.
        ICULL=-N..CULLING LOGIC IS EXECUTED FOR EVERY
                  GENERATION IN WHICH THE NUMBER OF 
                  ELEMENTS IN THE ACCUMULATION FILE
                  EXCEEDS N. THIS OPTION UTILIZES A
                  BINNING ALGROITHM AND IS VALID FOR
                  ALL VALUES OF NOBFUN (NOBFUN>1).
    
    IACCUM: 1 for accumulation archive process (D=0)
    
    XBEST....PERCENT OF THOSE INDIVIDUALS IN THE 
             CURRENT POPULATION THAT ARE PASSED
             THROUGHT TO THE NEXT GENERATION WITHOUT
             CROSSOVER OF MUTATION (REAL NUMBER 
             BOUNDED BY 0.0 AND 100.0) ***  (D=10)
    XAVG.....PERCENT OF THOSE INDIVIDUALS IN THE 
             NEW POPULATION THAT ARE DETERMINED 
             USING THE RANDOM AVERAGE CROSSOVER 
             OPERATOR (REAL NUMBER BOUNDED BY 0.0 
             AND 100.0) ***   (D=20)
    boxover: T use extended average crosover (D=F)
    prob_avg: prob of applying average to gene (D=1)
    i_avg_cho: 0 for orig average, 1 for new (D=0)
      0: Avg Oper based on gene/gene chromo/chromo 
      1: Avg Oper across chromo gen by gene with prob_avg
    
    XPERT....PERCENT OF THOSE INDIVIDUALS IN THE 
             NEW POPULATION THAT ARE DETERMINED 
             USING THE PERTURBATION MUTATION 
             OPERATOR (REAL NUMBER BOUNDED BY 0.0 
             AND 100.0) ***  (D=30)
    prob_pert: prob of applying pert to gene (D=0.2)
    beta:    perturbation size for genes (D=0.3)
    beta_min,beta_max,n_beta_s,n_beta_i(D=0,beta,300,300)
             Beta varied from beta_max to beta_min
             after n_beta_i function evals linearly 
             for next function evals
    
    XMUT.....PERCENT OF THOSE INDIVIDUALS IN THE 
             NEW POPULATION THAT ARE DETERMINED 
             USING THE RANDOM MUTATION OPERATOR 
             (REAL NUMBER BOUNDED BY 0.0 AND
             100.0) ***  (D=40)
    prob_mut: prob of applying mut to gene (D=0.2)
    
    CBEST = FALSE, NBEST CHROMOSOMES HAVE PARML(1) = F
                SO THAT THE FITNESS IS NOT REEVALUATED
                : I.E. PASSED THROUGH CHROMOSOMES  (D)
    CBEST = TRUE, NBEST CHROMOSOMES HAVE PARML(1) = T
                 SO THAT THE FITNESS IS REEVALUATED
                 : EVEN FOR PASSED THROUGH CHROMOSOMES
     
    init_ga:
         0: Initialize chromosomes with min/max average,
     RANDOM across genes  
         1: Initialize chromosomes with min/max average,
    ll random across genes (D)
         2: Initialize chromosomes latin hypecube,
    ll random across genes 
         <0 and abs(init_ga)>1 ,
    Initialize chromosomes with a uniform distribution,
    cross foreach gene between genemin -- genemax
    NOTE: init_ga<0 MUST HAVE nchrom = abs(init_ga)^ngene
    
    
    compute_pass_thu: 
    COMPUTE_PASS_THU=FALSE, CHROMOS HAVE PARML(1)=F
        SO THAT THE FITNESS IS NOT REEVALUATED if chromo  
        does not change : I.E. PASSED THROUGH CHROMOSOMES
    COMPUTE_PASS_THU = TRUE, FITNESS IS EVALUATED 
                    even if chromo does not change (D=T)
    
    rand_init_fit:T randomize intial fitness values D)
                  F intial fitness values to fit_min/max
     
    
    i_ran_seed: 
      >= 0 seed random number with -ipop-1-i_ran_seed
      < 0  seed random number with i_ran_seed
      Note for init_ga=2 (latin hypercube initial pop
               i_ran_seed used for seed of latin_random
    