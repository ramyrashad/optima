      ! ================================================================
      ! ================================================================
      ! 
      subroutine getCurvilinearVariables(
      ! Input Variables
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
      ! Input/Output Variables
     &     vel, uu, vv, p, rho, a, M, dy)
      ! 
      ! ================================================================
      ! ================================================================
      ! 
      ! Purpose: This subroutine will compute the off wall distance and
      ! flow variables in the curvilinear coorindate system.
      !
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! Declare Input Variables
      integer
     |     jdim,
     |     kdim,
     |     ndim
      double precision
     |     q(jdim,kdim,ndim),
     |     xy(jdim,kdim,4), 
     |     xyj(jdim,kdim),
     |     x(jdim,kdim),
     |     y(jdim,kdim),
     |     fmu(jdim,kdim),
     |     vort(jdim,kdim),
     |     turmu(jdim,kdim)

      ! ______________________________
      ! Declare Input/Output Variables
      double precision
     |     vel(jdim,kdim),
     |     uu(jdim,kdim),
     |     vv(jdim,kdim),
     |     p(jdim,kdim),
     |     rho(jdim,kdim),
     |     a(jdim,kdim),
     |     M(jdim,kdim),
     |     dy(jdim,kdim)

      ! _______________________
      ! Declare Local Variables
      logical
     |     NormalizedByUinf,
     |     TangentialVelocityOnly
      integer
     |     j,
     |     k,
     |     jp1,
     |     jm1
      double precision
     |     t1,
     |     denom,
     |     m1,
     |     m1_tmp,
     |     m2,
     |     piBL,
     |     phi

      ! ____________________________________________________________
      ! Compute and store the necessary flow variables in curvilinear
      ! (x,y) coordinate system

      do j=1,jdim
         do k=1,kdim

            rho(j,k) = q(j,k,1)*xyj(j,k)
            uu(j,k)  = q(j,k,2)/q(j,k,1)
            vv(j,k)  = q(j,k,3)/q(j,k,1)
            t1       = uu(j,k)*uu(j,k) + vv(j,k)*vv(j,k)
            vel(j,k) = dsqrt(t1)
            p(j,k)   = 0.4*(q(j,k,4)*xyj(j,k)-0.5*q(j,k,1)*xyj(j,k)*t1)
            a(j,k)   = dsqrt(1.4*p(j,k)/(q(j,k,1)*xyj(j,k)))
            M(j,k)   = dsqrt(t1)/a(j,k)

            ! ______________________
            ! Find off-wall distance
            jp1 = j + 1
            jm1 = j - 1

            ! find angle between two lines
            ! line 1: from (j,1)   to (j,k)
            ! line 2: from (jm1,1) to (jp1,1)
            piBL=4.d0*atan(1.d0)
            m2 = (y(j,k)-y(j,1)) / abs(x(j,k)-x(j,1))
            if (j.eq.1) then 
               m1 = (y(jp1,1)-y(j,1)) / abs(x(jp1,1)-x(j,1))
            else if (j.eq.jdim) then
               m1 = (y(j,1)-y(jm1,1)) / abs(x(j,1)-x(jm1,1))
            else 
               m1 = (y(jp1,1)-y(jm1,1)) / abs(x(jp1,1)-x(jm1,1))
            end if
            if (m1.gt.m2) then ! make sure m2 is the greater slope
               m1_tmp = m1
               m1 = m2
               m2 = m1_tmp
            end if

            ! check if m2 is parallel to y-axis (i.e. approaching infinity)
            if (m2.gt.1.0E15) then
               if (m1.ge.0d0) then
                  phi = atan(1d0/m1)
               else 
                  phi = atan(-1d0/m1)
               end if 
            else
               phi = atan((m2-m1)/(1+m1*m2))
            end if 
            if (phi.ge.pi/2d0) then
               phi = piBL - phi
            end if 

            if (k.eq.1) then
               dy(j,k) = 0d0
            else
               dy(j,k) = dsqrt((x(j,k)-x(j,1))**2+(y(j,k)-y(j,1))**2)
               !write(34,*) 'dy1  -> ', j, k, dy(j,k)
               !dy(j,k) = sin(phi)*dy(j,k)
               !write(34,*) 'dy2  -> ', j, k, dy(j,k)
            end if

         end do
      end do

      ! Compute tangential component of the edge velocity using dot
      ! product between the cartesian velocity vector and the covariant
      ! basis vector, e1_hat.
      TangentialVelocityOnly = .false.
      if (TangentialVelocityOnly) then
         do j=1,jdim
            do k=1,kdim
               denom = dsqrt(xy(j,k,3)*xy(j,k,3) + xy(j,k,4)*xy(j,k,4))
               vel(j,k) = (xy(j,k,4)*uu(j,k)-xy(j,k,3)*vv(j,k)) / denom
               vel(j,k) = abs(vel(j,k))
            end do
         end do
      end if

      ! Divide by Mach number if you want to normalize velocity
      ! using freestream velocity (as opposed to freestream sound
      ! speed)
      NormalizedByUinf = .false.
      if (NormalizedByUinf) then
         do j=1,jdim
            do k=1,kdim
               uu(j,k) = uu(j,k)/fsmach
               vv(j,k) = vv(j,k)/fsmach
               vel(j,k) = dsqrt(uu(j,k)*uu(j,k)+vv(j,k)*vv(j,k))
            end do
         end do
      end if

      return
      end subroutine getCurvilinearVariables

      ! ================================================================
      ! ================================================================
      ! 
      double precision function linearInterp(
     &     x1, y1, x2, y2, x_target, y_interp)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will linearly interpolate between two
      ! provided data points
      ! 
      ! Inputs:
      !   x1 = x-axis value of first point 
      !   y1 = y-axis value of first point
      !   x2 = x-axis value of second point 
      !   y2 = y-axis value of second point
      !   x_target = x-axis target value for interpolation
      !
      ! Output:
      !   interpolated value is output as function value
      ! 
      ! Usage Example:
      !   y_interpolated = linearInterp(x,y1,x2,y2,x_target)
      ! 
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------
      
      implicit none
      double precision x1, y1, x2, y2, x_target, y_interp
      double precision x_ratio, xtmp, ytmp

      if (abs(x2-x1).lt.1.0E-10) then

         ! No interpolation required since x2 = x1.
         linearInterp = y1

      else

         if (x2 .lt. x1) then
            ! swap points 1 and 2
            xtmp = x1
            ytmp = y1
            x1 = x2
            y1 = y2
            x2 = xtmp
            y2 = ytmp
         end if

         x_ratio = (x_target - x1) / (x2 - x1)

         ! This function returns the interpolated y-value: 
         linearInterp = y1 + x_ratio * (y2 - y1)

      end if

      return
      end function linearInterp


      ! ================================================================
      ! ================================================================
      ! 
      double precision function trapezoidalIntegration(
     &     length,x,f,jstartTI,jendTI,jincTI)
      ! 
      ! ================================================================
      ! ================================================================
      ! 
      ! Purpose: Performs the one-dimensional integration of function f,
      ! given data points: x1,x2,...,xN and f(x1),f(x2),...,f(xN)
      !
      ! ----------------------------------------------------------------

      implicit none
      
      integer
     |     length,              !< length of input vectors x and f
     |     jstartTI,            !< starting index (lower limit of integration)
     |     jendTI,              !< ending index (upper limit of integration)
     |     jincTI,              !< incremental index for do-loop
     |     jm1,                 !< dummy index minus one
     |     j                    !< dummy index
      double precision
     |     x(length),           !< independent variable
     |     f(length),           !< function representing integrand
     |     sumInt               !< summed integral

      sumInt = 0d0
      do j=jstartTI+jincTI,jendTI,jincTI
         jm1 = j - jincTI
         sumInt = sumInt + (x(j)-x(jm1))*(f(j)+f(jm1))
      end do
      trapezoidalIntegration = 0.5d0*sumInt

      return
      end function trapezoidalIntegration

      ! ================================================================
      ! ================================================================
      ! 
      subroutine naturalCubicSpline(
           ! input variables
     &     length,x,f,jstartCS,jendCS,jincCS,
           ! output variables
     &     b, c, d)
      ! 
      ! ================================================================
      ! ================================================================
      ! 
      ! Purpose: Constructs the natural cubic spline interpolant S for the
      !   function f, defined at the given data points: x1,x2,...,xN and
      !   the corresponding f(x1),f(x2),...,f(xN)
      !
      ! Output: a(j), b(j), c(j), and d(j) for j = 1 to N-1.
      !   These coefficients define the piecewise cubic spline
      !   interpolants, such that:
      !   
      !   S(x) = f(j) + b(j)*dx(j) + c(j)*dx(j)^2 + d(j)*dx(j)^3
      !
      !   where dx(j) = x-x(j) and S(x) is valid on the interval
      !   [ x(j) <= x <= x(j+1) ].
      !
      ! Author:    Ramy Rashad, 2011.
      ! Reference: Numerical Analysis (7th Ed.)
      !            by R.L. Burden and J.D. Faires
      ! ----------------------------------------------------------------

      implicit none

      ! N.B. If you decide to include global variables to this
      ! subroutine, watch out for variable name conflicts!
      
      ! declare input variables
      integer
     &     length,              !< length of input vectors x and f
     &     jstartCS,            !< starting index (lower limit of integration)
     &     jendCS,              !< ending index (upper limit of integration)
     &     jincCS               !< incremental index for do-loop
      double precision
     &     x(length),           !< independent variable
     &     f(length)            !< dependent variable 

      ! declare output variables
      double precision
     &     b(length-1),
     &     c(length-1),
     &     d(length-1)
     
      ! declare local variables
      integer
     &     jincBack,            !< backward direction of increment
     &     jm1,                 !< dummy index minus one
     &     jp1,                 !< dummy index plus one
     &     j                    !< dummy index
      double precision
     &     h(length-1),         !< grid spacing
     &     hinv(length-1),      !< inverse of grid spacing
     &     alpha(length-1),
     &     z(length-1),
     &     mu(length-1),
     &     l(length-1)

      ! Step 1: grid spacing
      do j=jstartCS,jendCS-jincCS,jincCS
         jp1 = j + jincCS
         jm1 = j - jincCS
         h(j) = x(jp1)-x(j)
         hinv(j) = 1d0/h(j)
      end do

      ! Step 2: compute coefficient alpha
      do j=jstartCS+jincCS,jendCS-jincCS,jincCS
         jp1 = j + jincCS
         jm1 = j - jincCS
         alpha(j) =
     &        3d0*(f(jp1)-f(j))*hinv(j) - 3d0*(f(j)-f(jm1))*hinv(jm1)
      end do

      ! Steps 3 -> 5 and part of Step 6 solve a tridiagonal linear
      ! system using Crout Factorization.
      
      ! Step 3:
      l(jstartCS) = 1d0
      mu(jstartCS) = 0d0
      z(jstartCS) = 0d0
      ! Step 4:
      do j=jstartCS+jincCS,jendCS-jincCS,jincCS
         l(j) = 2*(x(jp1)-x(jm1)) - h(jm1)*mu(jm1)
         mu(j) = h(j)/l(j)
         z(j) = (alpha(j) - h(jm1)*z(jm1))/l(j)
      end do
      ! Step 5:
      l(jendCS) = 1d0
      z(jendCS) = 0d0
      c(jendCS) = 0d0

      !Step 6:
      jincBack = -jincCS
      do j=jendCS-jincCS,jstartCS,jincBack
         jp1 = j + jincCS
         jm1 = j - jincCS
         c(j) = z(j) - mu(j)*c(jp1)
         b(j) = (f(jp1)-f(j))*hinv(j) - h(j)*(c(jp1)+2d0*c(j))/3d0
         d(j) = (c(jp1)-c(j))*hinv(j)/3d0
      end do

      return
      end subroutine naturalCubicSpline
      

      ! ================================================================
      ! ================================================================
      ! 
      subroutine write_transition(
     &     jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &     dstar, theta, Hfactor, Re_dstar, Re_theta, Pohlhausen, 
     &     polyPh, Hi, Re_theta_CR, MeanPh, Re_theta_TR, jCR, xCR,
     &     sCR, jTR, xTR, sTR, jTRS, xTRS, sTRS) 
      ! 
      ! ================================================================
      ! ================================================================
      ! 
      ! Purpose: This subroutine will output boundary layer properties
      !   and transition criteria related information to two files.
      !   The first being a scratch file for debugging and informational
      !   purposes. The second being a TECPLOT data file for
      !   further post-processing. 
      !
      ! ----------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      ! _______________________
      ! declare input variables
      integer
     &     jdim,
     &     kdim,
     &     ndim,
     &     jCR,
     &     jTR,
     &     jTRS
      double precision
     &     q(jdim,kdim,ndim),
     &     xy(jdim,kdim,4),
     &     xyj(jdim,kdim),
     &     x(jdim,kdim),
     &     y(jdim,kdim),
     &     fmu(jdim,kdim),
     &     vort(jdim,kdim),
     &     turmu(jdim,kdim),
     &     dstar(jdim),
     &     theta(jdim),
     &     Hfactor(jdim),
     &     Re_dstar(jdim),
     &     Re_theta(jdim),
     &     Pohlhausen(jdim),
     &     PolyPh(jdim),
     &     Hi(jdim),
     &     Re_theta_CR(jdim),
     &     MeanPh(jdim),
     &     Re_theta_TR(jdim),
     &     xCR,
     &     sCR,
     &     xTR,
     &     sTR,
     &     xTRS,
     &     sTRS

      ! ______________________________
      ! Declare Input/Output Variables
      ! -- none

      ! ________________________
      ! Declare Local Variables
      logical NormalizedByUinf, NormalizeVelPlot
      integer j
      double precision x_c(jdim) 

      ! _______________________________________________
      ! declare variables for velocity profile plotting
      integer
     &     jj,
     &     jbl,
     &     k,
     &     jbltemp(7)
      double precision
     &     pchordtemp(7)

      ! _____________________________________________________
      ! declare output variables from getCurvilinearVariables
      double precision
     &     vel(jdim,kdim),
     &     uu(jdim,kdim),
     &     vv(jdim,kdim),
     &     p(jdim,kdim),
     &     rho(jdim,kdim),
     &     a(jdim,kdim),
     &     M(jdim,kdim),
     &     dy(jdim,kdim)

      ! Divide by Mach number if you want to normalize velocity
      ! using freestream velocity (as opposed to freestream sound
      ! speed)
      NormalizedByUinf = .true.
      if (NormalizedByUinf) then
         do j=jblStart,jblEnd,jblinc
            if (foundBLEdge(j)) velBLEdge(j) = velBLEdge(j)/fsmach
         end do
      end if

      ! x/c position
      do j=1,jdim
         x_c(j) = x(j,1)
      end do

      ! ----------------------
      ! -- Output to Screen -- 
      ! ----------------------

      write(34,*) 'Grid Details:'
      write(34,*) '------------'
      write(34,*) 'jblLE    = ', jblLE
      write(34,*) 'jblSP    = ', jblSP
      write(34,*) 'jtail1   = ', jtail1
      write(34,*) 'jtail2   = ', jtail2
      write(34,*) 'top surf = ', jtail2-jblLE+1
      write(34,*) 'bot surf = ', jblLE-jtail1+1
      write(34,*) 'jblStart = ', jblStart
      write(34,*) 'jblEnd   = ', jblEnd
      write(34,*) 'kblStart = ', kblStart
      write(34,*) 'kblEnd   = ', kblEnd

      write(34,*)
      write(34,*) 'blEdgeMethod = ', blEdgeMethod
      write(34,*) 'surfaceBL    = ', surfaceBL
      write(34,*) '---------------------------'
      write(34,*)

      write(34,*)
      if (surfaceBL.eq.1) then
         write(34,*) 'Upper Surface Transition Summary:'
         write(34,*) '---------------------------------'
         write (34,*) 'iterTP = ', iterTP
         if (foundCR) write(34,172) jCR, xCR, sCR
         if (foundTR) write(34,173) jTR, xTR, sTR
         if (foundTR) write(34,174) jTRS, xTRS, sTRS
 172     format('jCRup  = ', I3, '    xCRup  = ', f14.5,
     &        '    sCRup  = ', f14.5)
 173     format('jTRup  = ', I3, '    xTRup  = ', f14.5,
     &        '    sTRup  = ', f14.5)
 174     format('jTRSup = ', I3, '    xTRSup = ', f14.5,
     &        '    sTRSup = ', f14.5)
      else if (surfaceBL.eq.2) then
         write(34,*) 'Lower Surface Transition Summary:'
         write(34,*) '---------------------------------'
         write (34,*) 'iterTP = ', iterTP
         if (foundCR) write(34,175) jCR, xCR, sCR
         if (foundTR) write(34,176) jTR, xTR, sTR
         if (foundTR) write(34,177) jTRS, xTRS, sTRS
 175     format('jCRlo  = ', I3, '    xCRlo  = ', f14.5,
     &        '    sCRlo  = ', f14.5)
 176     format('jTRlo  = ', I3, '    xTRlo  = ', f14.5,
     &        '    sTRlo  = ', f14.5)
 177     format('jTRSlo = ', I3, '    xTRSlo = ', f14.5,
     &        '    sTRSlo = ', f14.5)
      end if
      write(34,*)
      write(34,*)

      write(36,906)
 906  format(
     &     'j',T15,   
     &     'x/c',T30,
     &     'foundBLEdge', T45,
     &     'k_deltaBL', T60,
     &     'velBLEdge',T75,
     &     'deltaBL', T90, 
     &     'dstar',T105,
     &     'theta')
      do j=jblStart,jblEnd,jblinc
         write(36,907) j,x_c(j), foundBLEdge(j), k_deltaBL(j),
     &        velBLEdge(j), deltaBL(j), dstar(j),theta(j)
      end do
 907  format(1p,
     &     I3,T15,
     &     e14.7,T30,
     &     L,T45,
     &     I3,T60,
     &     e14.7,T75,
     &     e14.7,T90,
     &     e14.7,T105,
     &     e14.7)
      write(36,*)
      write(36,908)
 908  format(
     &     'j',T15   
     &     'x/c',T30,
     &     'foundBLEdge',T45,
     &     'Hfactor', T60, 
     &     'Hi',T75,
     &     'polyPh',T90,
     &     'Pohlhausen',T105,
     &     'MeanPh')

      do j=jblStart,jblEnd,jblinc
         write(36,909) j, x_c(j), foundBLEdge(j), Hfactor(j), Hi(j),
     &        polyPh(j), Pohlhausen(j), MeanPh(j)
      end do
 909  format(1p,
     &     I3,T15,
     &     e14.7,T30,
     &     L,T45,
     &     e14.7,T60,
     &     e14.7,T75,
     &     e14.7,T90,
     &     e14.7,T105,
     &     e14.7)
      write(36,*)
      ! ----------------------
      write(36,910)
 910  format(
     &     'j',T15,   
     &     'x/c',T30,
     &     'foundBLEdge',T45,
     &     'Re_dstar',T60,
     &     'Re_theta',T75,
     &     'Re_theta_CR',T90,
     &     'Re_theta_TR')
      do j=jblStart,jblEnd,jblinc
         write(36,911) j, x_c(j), foundBLEdge(j), Re_dstar(j),
     &        Re_theta(j), Re_theta_CR(j), Re_theta_TR(j)
      end do
 911  format(1p,
     &     I3,T15,
     &     e14.7,T30,
     &     L,T45,
     &     e14.7,T60,
     &     e14.7,T75,
     &     e14.7,T90,
     &     e14.7)
      write(36,*)
      ! ----------------------
!      write(36,912)
! 912  format(
!     &     'j',T15   
!     &     'x/c',T30,
!     &     'foundBLEdge',T45,
!     &     'Pohlhausen',T60
!     &     'MeanPh',T75
!     &     'Re_Criteria')
!      do j=jblStart,jblEnd,jblinc
!         write(36,913) j, x_c(j), foundBLEdge(j), Pohlhausen(j)
!     &        ,MeanPh(j), Re_theta_TR(j)
!      end do
! 913  format(1p,I3,T15,
!     &     e14.7,T30,
!     &     L,T45,
!     &     e14.7,T60,
!     &     e14.7,T75,
!     &     e14.7)
!      write(36,*)

      ! ----------------------------------
      ! -- Output BL properties to Tecplot
      ! ----------------------------------
      write(blprop_unit,*) 'TITLE="blproperties"'
      write(blprop_unit,*) 'VARIABLES=',
     &     '"j",',
     &     '"k_delta",',
     &     '"x/c",',
     &     '"velBLEdge",',
     &     '"delta",',
     &     '"dstar",',
     &     '"theta",',
     &     '"Hfactor",',
     &     '"Hi",',
     &     '"Pohlhausen",',
     &     '"MeanPh",',
     &     '"Re_dstar",',
     &     '"Re_theta",',
     &     '"Re_theta_CR",',
     &     '"Re_theta_TR"'
      if (blEdgeMethod.eq.1) then
         write(blprop_unit,*) 'ZONE T= "Bernouilli-', iterTP,'"'
      else if (blEdgeMethod.eq.2) then
         write(blprop_unit,*) 'ZONE T= "DiagnosticFcn-', iterTP,'"'
      else if (blEdgeMethod.eq.3) then
         write(blprop_unit,*) 'ZONE T= "VorticityMethod-', iterTP,'"'
      end if
      do j=jblStart,jblEnd,jblinc
         write(blprop_unit,*) j, k_deltaBL(j), x_c(j), velBLEdge(j),
     &        deltaBL(j),dstar(j),theta(j),Hfactor(j),
     &        Hi(j), Pohlhausen(j), MeanPh(j),
     &        Re_dstar(j), Re_theta(j),Re_theta_CR(j), Re_theta_TR(j)
      end do

      
      ! --------------------------------------
      ! -- Output velocity profiles to Tecplot
      ! --------------------------------------
      if (surfaceBL.eq.1 .and. blEdgeMethod.eq.3) then
         ! fixed chord positions for which to output BL properties
         pchordtemp(1) = 0.01
         pchordtemp(2) = 0.05
         pchordtemp(3) = 0.1
         pchordtemp(4) = 0.2
         pchordtemp(5) = 0.4
         pchordtemp(6) = 0.6
         pchordtemp(7) = 0.8

         ! __________________________________________________
         ! Compute variables in curvilinear coordinate system
         call getCurvilinearVariables(
     &        jdim, kdim, ndim, q, xy, xyj, x, y, fmu, vort, turmu,
     &        vel, uu, vv, p, rho, a, M, dy)

         ! Divide by Mach number if you want to normalize velocity
         ! using freestream velocity (as opposed to freestream sound
         ! speed)
         NormalizedByUinf = .true.
         if (NormalizedByUinf) then
            do j=1,jdim
               do k=1,kdim
                  uu(j,k) = uu(j,k)/fsmach
                  vv(j,k) = vv(j,k)/fsmach
                  vel(j,k) = vel(j,k)/fsmach
               end do
            end do
         end if

         ! Tecplot header
         write(blvel_unit,*) 'TITLE="blprimative"'
         write(blvel_unit,*) 'VARIABLES="j","k","yy","vel","uu","vv",',
     &        '"p","rho","a","M","vort","fmu","turmu"'

         ! At each chordwise station (top surface) output BL properties
         do jbl=1,7


            ! RR: VALID FOR TOP SURFACE ONLY
            j = jtail2
            do while (x(j,1) .gt. pchordtemp(jbl))
               j = j - jblinc
            end do
            jbltemp(jbl) = j
            jj = j

            if (foundBLEdge(j)) then

               write (blvel_unit,2710) pchordtemp(jbl)
 2710          format('zone T="',f3.2,' chord"')

               do k = 1,k_deltaBL(j)

                  NormalizeVelPlot = .true.
                  if (NormalizeVelPlot) then
                     ! Normalize the velocity at each node
                     vel(j,k) = vel(j,k)/velBLEdge(j)
                     ! Normalize the off-wall distance at each node
                     dy(j,k) = dy(j,k)/deltaBL(j)
                  end if

                  ! Output to file
                  write (blvel_unit,*) jj, k, dy(jj,k), vel(jj,k),
     &                 uu(jj,k), vv(jj,k), p(jj,k), rho(jj,k), a(jj,k),
     &                 M(jj,k), vort(jj,k), fmu(jj,k), turmu(jj,k)

               end do ! k
            end if

         end do ! jbl

         ! Reset scaling of velBLEdge to a_inf (as oppose to U_inf)
         if (NormalizedByUinf) then
            do j=jblStart,jblEnd,jblinc
               if (foundBLEdge(j)) velBLEdge(j) = velBLEdge(j)*fsmach
            end do
         end if

      end if ! surfaceBL.eq.1

      return
      end subroutine write_transition
