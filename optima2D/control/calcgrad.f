c----------------------------------------------------------------------
c     -- calculate gradient of objective function --

c     written by: marian nemec
c     date: april 2000
c----------------------------------------------------------------------
      subroutine CalcGrad (obj, grad, jdim, kdim, ndim, ifirst, q, cp,
     &      xy, xyj, x, y, xOrig, yOrig, dx, dy, lambda, fmu, vort,
     &      turmu, cp_tar, bap, bcp, bt, bknot, dvs, idv, indx, icol,
     &      iex, work1, ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast,
     &      pa, pat, opwk, obj0, mp, ifun)

#ifdef _MPI_VERSION
      use mpi
#endif

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer jdim, kdim, ndim, ifirst, idv(nc+mpopt), indx(jdim,kdim)
      integer ia(jdim*kdim*ndim+2), ifun
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1), icol(9)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer iex(jdim,kdim,4)

      integer iat(jdim*kdim*ndim+2)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)

      double precision q(jdim,kdim,ndim), cp(jdim,kdim), xy(jdim,kdim,4)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision xOrig(jdim,kdim), yOrig(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision lambda(2*jdim*kdim*incr)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)
      double precision obj, grad(nc+mpopt), cp_tar(jbody,2)
      double precision bap(jbody,2)
      double precision bcp(nc,2), bt(jbody), bknot(jbsord+nc)
      double precision dvs(nc+mpopt)
      double precision obj0

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100), opwk(jdim*kdim,8)

c     Variables for timing flow solve (Chad Oldfield)
      real*4 junk_time, start_time, finish_time, tarray(2), etime
      external etime

c     Start time (Chad Oldfield)
      junk_time = etime(tarray)
      start_time = tarray(1)

      write (opt_unit,10) icg+1
      call flush(opt_unit)
 10   format (/3x,'Gradient evaluation #:', i4)

      if (gradient.eq.0) then
c     -- gradient ala finite differences --
        call fd_grad (obj, grad, jdim, kdim, ndim, ifirst, indx, icol,
     &        iex, q, cp, xy, xyj, x, y, fmu, vort, turmu, cp_tar, bap,
     &        bcp, bt, bknot, dvs, idv, work1, ia, ja, ipa, jpa, iat,
     &        jat, ipt,jpt, as, ast, pa, pat, opwk(1,1), opwk(1,5),
     &        opwk(1,6), opwk(1,7), xOrig, yOrig, dx, dy, obj0, mp,
     &        ifun)

      else if (gradient.eq.1) then
c     -- gradient ala adjoint method --
        if (gmresout) rewind (gmres_unit)
        call adjoint (grad, jdim, kdim, ndim, q, cp, xy, xyj, x, y,
     &        xOrig, yOrig, dx, dy, lambda, fmu,
     &        vort, turmu, cp_tar, bap, bcp, bt, bknot, dvs, idv, ia, 
     &        ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat,
     &        indx, icol, iex, work1(1,1), work1(1,2),  work1(1,3),
     &        work1(1,4),  work1(1,9),  work1(1,14), work1(1,20),
     &        work1(1,26), work1(1,32), opwk(1,1),   opwk(1,5),
     &        opwk(1,6),   opwk(1,7),   opwk(1,8), obj0)

      else if (gradient.eq.2) then
c     -- gradient ala sensitivity method --
        if (gmresout) rewind (gmres_unit)
        call sensit (grad, jdim, kdim, ndim, indx, icol, iex, q, cp, xy,
     &       xyj, x, y, cp_tar, bap, bcp, bt, bknot, dvs, idv, ia, ja,
     &       ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat, turmu, fmu,
     &       vort, work1(1,1), work1(1,2), work1(1,3),  work1(1,4),
     &       work1(1,5),  work1(1,8),  work1(1,9),  work1(1,10), 
     &       work1(1,11), work1(1,12), work1(1,13), work1(1,14),
     &       work1(1,20), work1(1,36), work1(1,41), work1(1,47),
     &       work1(1,53), work1(1,58), work1(1,63), work1(1,68),
     &       work1(1,72), opwk(1,1),   opwk(1,5),   opwk(1,6),
     &       opwk(1,7),   opwk(1,8),xOrig,yOrig,dx,dy,obj0)

      else if (gradient.eq.3) then
c     -- gradient ala matrix-free sensitivity method --
        if (gmresout) rewind (gmres_unit)
        call sensit_mf (grad, jdim, kdim, ndim, indx, icol, iex, q, cp,
     &       xy, xyj, x, y, cp_tar, bap, bcp, bt, bknot, dvs, idv, ia,
     &       ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat,
     &       work1(1,1), work1(1,2), work1(1,3), work1(1,4), turmu, fmu,
     &       vort, work1(1,5), work1(1,8), work1(1,9), work1(1,10),
     &       work1(1,11), work1(1,12), work1(1,13), work1(1,14),
     &       work1(1,20), work1(1,36), work1(1,42), work1(1,48),
     &       work1(1,53), work1(1,58), work1(1,63), work1(1,68),
     &       work1(1,72), work1(1,77), opwk(1,1),   opwk(1,5),
     &       opwk(1,6),   opwk(1,7),   opwk(1,8), xOrig, yOrig, dx, dy,
     &       obj0)

      else if (gradient.eq.4) then
c       Augmented adjoint method used to compute gradient (i.e.
c       explicitly includes grid perturbation terms).  (Chad Oldfield)
        if (gmresout) rewind (gmres_unit)
        call adjoint (grad, jdim, kdim, ndim, q, cp, xy, xyj, x, y,
     &        xOrig, yOrig, dx, dy, lambda, fmu,
     &        vort, turmu, cp_tar, bap, bcp, bt, bknot, dvs, idv, ia, 
     &        ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat,
     &        indx, icol, iex, work1(1,1), work1(1,2),  work1(1,3),
     &        work1(1,4),  work1(1,9),  work1(1,14), work1(1,20),
     &        work1(1,26), work1(1,32), opwk(1,1),   opwk(1,5),
     &        opwk(1,6),   opwk(1,7),   opwk(1,8), obj0, rank, ierr)

      end if

c     Finish time (Chad Oldfield)
      junk_time = etime(tarray)
      finish_time = tarray(1)
      junk_time = finish_time-start_time
      if (fgtout) write(fgt_unit,'(1X,A,f9.2,A,f9.2,A,f9.2)')
     |    'Gradient evaluation: start ', start_time, ', finish ',
     |    finish_time, ', duration ', junk_time

      return
      end                       !CalcGrad
