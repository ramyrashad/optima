      subroutine overlap(JDIM,KDIM,NDIM,VAR,JMAX,JTAIL1)                
C                                                                       
        DIMENSION VAR(JDIM,KDIM,NDIM)                                   
C                                                                       
C  GET K = 1 ;;J = 1 TO JTAIL1-1 POINTS FROM J = JTAIL2+1 TO JMAX       
C                                                                       
C    JTAIL2 = JMAX-JTAIL1+1                                             
C                                                                       
        DO 10 N = 1,NDIM                                                
        DO 10 J = 1,JTAIL1-1                                            
        JJ = JMAX-J+1                                                   
              VAR(J,1,N) = VAR(JJ,1,N)                                  
10      CONTINUE                                                        
C                                                                       
        RETURN                                                          
        END                                                             
