c This has been modified so that all variables are explicitly declared,
c so the "implicit none" statement may be used in subroutines that
c include this file.  (Chad Oldfield)

c     -- common blocks for OPTIMA2D --
c     written by: marian nemec, march 2000

      integer ibsord, ibsnc, ibskt, ibody
c     -- max. number of knots -> ibskt = ibsord + ibsnc --

      parameter (ibsord=25, ibody=500, ibsnc=100, ibskt=125)

c     -- b-spline common block --
c     -- jbsord: order of b-spline, nc: number of control points,
c     jbody: number of points on airfoil, 
c     ndv: number of design variables, including alpha
c     ngdv: number of geometric design variables ---
      integer jbsord, jbody, nc, ndv, ngdv
      common/bspline/ jbsord, jbody, nc, ndv, ngdv

c     -- optimization parameters --

      double precision opt_tol, fd_eta, wfl, wfd, wfactor, gmax
      double precision cd_tar, cl_tar, wtc, cgradmag, dksDenom
c     fs_tol, grad_tol and it_cyc presently not used
      double precision fs_tol(5), grad_tol(5), polar(100), gradmag

C	START CHANGES MADE BY LASLO

      double precision hessian_eta
      integer hesgrad, inithes, scaling, warmset
      logical hessian, new_search, reset_hessian


C	END CHANGES MADE BY LASLO

C       ==== HYBRID OPTIMIZER GLOBAL VARIABLES ====
#ifdef _MPI_VERSION
      integer ru_gl_len 
      logical first_husrfun
      parameter (ru_gl_len = 30000000)
      double precision ru_global(ru_gl_len)
      common/globru/ ru_global, first_husrfun

      integer myfsn_base, myfsn_loc, popsize, procs_per_chromo, 
     & hybr_meth, ranseed, selmethod, mingen, maxgen, sn_maj_lim, 
     & flow_sol_lim, sample_offset, optf_output
      
      double precision prob_pert, prob_mut, prob_avg, xbest, xavg, 
     & xpert, xmut, pen, load_bal_tol

      character sobol_file*128
      logical cig, use_db

      common/hybridopt/ myfsn_base, myfsn_loc, popsize, mingen,
     & procs_per_chromo, hybr_meth, ranseed, selmethod, maxgen,
     & sn_maj_lim, flow_sol_lim, prob_pert, prob_mut, prob_avg,
     & xbest, xavg, xpert, xmut, pen, load_bal_tol, sobol_file, 
     & optf_output, cig, use_db, sample_offset
#endif

C       ==== END OF HYBRID OPTIMIZER OPTIONS ======

c     -- thickness constraints parameters --
c       maxtcon = maximum no. of thickness constraints
c       ntcon   = the number of thickness constraints used
c       ctx(i)  = X location of the ith thickness constraint
c       cty(i)  = Airfoil thickness at the ith constraint
c       cty_tar(i) = Target thickness at the ith constraint
c       jlotx(i) = Node on the lower surface closest to the ith constraint
c       juptx(i) = Node on the upper sufrace closest to the ith constraint
      integer maxtcon, ntcon, ngcon
      parameter (maxtcon=20)
      double precision ctx(maxtcon),cty_tar(maxtcon),cty(maxtcon)
      integer jlotx(maxtcon), juptx(maxtcon)

c     -- area constraints parameters --
c       wac      = weight of the area constraint
c       area     = cross section area of the airfoil
c       areafrac = fraction of the initial area that is acceptable
c       areainit = initial cross section area at the start of the opt. cycle
      double precision wac, area, areafac, areainit, arearef

c     Radius of curvature constraint parameters (Chad Oldfield)
      integer nrc, nrc_max  ! Number of points defining minimum radius
c         of curvature, and maximum allowable
      parameter (nrc_max=20)
      double precision rc_tar(nrc_max), xrc_tar(nrc_max)
c         Minimum radiuii of curvature, and chordwize positions at which
c         these occur.  Note that xrc_tar must be sorted in order of
c         increasing values (and rc_tar must match that ordering)
      double precision wrc_con
c         Weight for the radius of curvature constraint.

      integer nopc
c     nopc: number of operating conditions (multi-point optimization)
c     ATTENTION: IF YOU CHANGE NOPC, YOU NEED TO CHANGE n_mpo(NOPC)
c     and n_mpcp(NOPC) IN FILE UNITS.INC

#ifdef _MPI_VERSION
      parameter (nopc=400)
#else
      parameter (nopc=1)
#endif

c     Explicitly declare array types (Chad Oldfield)
      double precision wmpo(nopc), fsmachs(nopc), cltars(nopc),
     |    reno(nopc), alphas(nopc), cdtars(nopc), wfls(nopc),
     |    wmpo_new(nopc), c_upp1(nopc), c_low1(nopc), c_upp2(nopc),
     |    c_low2(nopc), wmpo1(nopc), wmpo2(nopc), cltars2(nopc),
     | 	  dvmach0

      integer objfuncs(nopc), ndvmach, machdv1_index, machdv2_index

      integer opt_meth, obj_func, opt_iter, gradient, ico, icg, 
     &      ncyc, it_cyc(5), ipolar, nrtcon, num_offpts, noffcon,
     &      sc_method, nlcdm, lcdm_grad

      logical   dvalfa(nopc), clalphas(nopc), clalpha2s(nopc), 
     &          clopts(nopc), off_flags(nopc), dvmach(nopc)

      logical original_grid  ! True if the grid perturbation is to
c         revert to the original grid when computing finite differences
c         This is only effective when incr = 0 (otherwise it
c         acts as though original_grid = true, regardless of the actual
c         value of original_grid).
      logical moveTE !form the cubic polynomial for the TE

c     Grid perturbation (and grid adjoint) parameters
      integer incr, maxincr  ! number of increments in elasticity
c         grid perturbation, and maximum allowed.  Set incr = 0 to use
c         algebraic grid perturbation.

      parameter (maxincr = 3)

      integer gpmaxit, gamaxit  ! Maximum permitted number of iterations
c         for the elasticity grid perturber, and the grid adjoint solver.
      double precision gptol, gatol  ! Residual tolerances
c         for the elasticity grid perturber, and the grid adjoint solver.

c     Objective and constraint function scaling
      logical scale_obj, scale_con

c     Geometric design variable scaling
      double precision gdv_scale

c     AOA design variable scaling
      double precision aoadv_scale   

c     Mach # design variable scaling
      double precision mdv_scale, mdv_low, mdv_upp

c     -- range thickness variables
c     Explicitly declare double precision & integers (Chad Oldfield)
      integer nthc, nsub
      parameter (nthc=10)
      parameter (nsub=100)
c     Explicitly declare array types (Chad Oldfield)
      double precision crthx(nthc), crth(nthc)
      double precision crtxl(nthc), crtxt(nthc), crthtar(nthc),
     |    crtxn(nthc)
      double precision crtxsub(nthc,nsub)!, spectydfd_dc(400,100)
      integer jlortxsub(nthc,nsub), juprtxsub(nthc ,nsub)
      double precision maxrthc, maxrthctemp, maxrthcxloc, maxrthctot
      double precision remxmin,remxmax,remymin,remymax
      integer remnx,remny

      logical opt_restart, obj_restart, cdf, coef_frz, ckfile
      logical warm_start, warm1st 
      logical sol_his, grid_his, flo_his, cpp_his, cf_his
      logical adptKS

c     -- automatic restart variables...they should be self explanatory --
      logical auto_restart
      integer num_restarts, opt_cycle, wt_iter, wt_cycle, rest_iter
      integer wt_cycle_iter
      double precision alphas_init(nopc), alphas_restart(nopc),
     & alphas_new(nopc)
	


chb   -- Variables for use with wt_update subroutine

      double precision obj_offs(20) ! array to store off-design objective function values      
      double precision obj_offtars(20) ! array to store target off-design objective function values 
      double precision cdt0s(20) ! array to hold initial values of drag coefficients
      double precision clt0s(20) ! array to hold initial values of lift coefficients
      logical off_flag, wt_update_flag, wt_update, clmax_flag
      logical use_quad_penalty_meth, wt_aoa, spect_frz

      double precision rhoKS, wt_exp, rKS_used, cllow, clup, mlow, mup
      double precision wtlow, wtup, altlow, altup, wspan, warea, sweep
      double precision Mstar, ref_chord, omga, exp_wt
      integer jmstart, jmend, kmstart, kmend, jminc, kminc, nm, ncl
      integer nwt, nalt, wt_func
      logical ptest, wt_int, dl_int

      logical v_profile, v_profile_upper
      double precision v_profile_x, vp_dist
      integer vp_points
      
c       --- flags to avoid printing out unneccessary data ---
      logical aoaout, fgtout, gmresout, ldout, o2sout, thisout,
     &        acout, bcout, bestout, bspout, dvsout, hestout,
     &        ohisout, cpout, hisout, mout, gvhisout


C	START CHANGES MADE BY LASLO
c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision bstep
      integer mpopt
      common/opt_set/ gptol, gatol, rc_tar, xrc_tar, wrc_con,
     &     fd_eta, opt_tol, wfl, wfd, wfactor, cd_tar,clt0s, cdt0s,
     &     obj_offs, obj_offtars, cgradmag, gmax, dksDenom,
     &     remxmin,remxmax,remymin,remymax,
     &     cl_tar, cty_tar, ctx, cty, wtc, fs_tol, grad_tol, bstep,
     &     polar, wmpo, fsmachs, cltars, cltars2, cdtars, wfls, 
     &     reno, alphas, objfuncs, clalphas, clalpha2s, clopts,
     &     off_flags, alphas_init, alphas_restart, sc_method,
     &     gradmag, opt_meth, obj_func, opt_iter, gradient, ico, icg,
     &     jlotx, juptx, ntcon, ncyc, it_cyc, ipolar, mpopt,
     &     opt_restart, cdf, coef_frz, dvalfa, ckfile, dvmach,
     &     nrtcon, crthx, crth, crtxl, crtxt, crthtar, crtxn,
     &     crtxsub, jlortxsub, juprtxsub, maxrthc, maxrthctemp,
     &     maxrthcxloc, maxrthctot,remnx,remny,dvmach0,ndvmach,
     &	   machdv1_index, machdv2_index, scale_con,
     &     wac,area,areafac,areainit,!spectydfd_dc, arearef,
     &     auto_restart, num_restarts, opt_cycle, wt_cycle,
     &     hessian, hessian_eta, hesgrad, inithes,
     &     new_search, scaling, reset_hessian, warmset, obj_restart,
     &     original_grid,moveTE,incr, nrc, off_flag, num_offpts,
     &     noffcon, nlcdm, lcdm_grad, warm1st, adptKS, spect_frz, 
     &     gdv_scale, aoadv_scale, mdv_scale, mdv_low, mdv_upp,
     &     gpmaxit, gamaxit, scale_obj, wt_update_flag, warm_start,
     &     sol_his, grid_his, flo_his, cpp_his, cf_his,
     &     wt_update, wt_iter, clmax_flag, 
     &     wmpo_new, alphas_new, rest_iter, use_quad_penalty_meth, 
     &     c_upp1, c_low1, c_upp2, c_low2, jmstart, jmend, kmstart, 
     &     kmend, jminc, kminc, rhoKS, wt_cycle_iter, ngcon, wt_aoa,
     &     wt_exp, rKS_used, cllow, clup, mlow, mup, nm, ncl, ptest,
     &     Mstar, wt_int, wtlow, wtup, nwt, altlow, altup, nalt, wspan,
     &     warea, wt_func, sweep, ref_chord, omga, wmpo1, wmpo2, dl_int,
     &     v_profile, v_profile_upper, v_profile_x, vp_dist, vp_points,
     &     aoaout, fgtout, gmresout, ldout, o2sout, thisout, exp_wt,
     &     acout, bcout, bestout, bspout, dvsout, hestout, 
     &     ohisout, cpout, hisout, mout, gvhisout

C	END CHANGES MADE BY LASLO

c     -- ilu and pgmres parameters --
      integer inord, ireord, ilu_meth, im_gmres, iter_gmres, lfil
      double precision pdc, eps_gmres, tol_gmres1, tol_gmres2
      common/opt_adj/ pdc, eps_gmres, inord, ireord, ilu_meth, lfil,
     &      im_gmres, iter_gmres, tol_gmres1, tol_gmres2 
