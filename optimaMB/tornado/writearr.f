C ********************************************************************
C ***        subroutine to output arrays                           ***
C ********************************************************************
      subroutine writearr(lu,array,jdim,kdim,jbegin,jend,kbegin,kend)
      implicit none
      integer lu,jdim,kdim,jbegin,jend,kbegin,kend,j,k,n
      double precision array(jdim,kdim,4)
      write(lu) (((array(j,k,n),j=jbegin,jend),k=kbegin,kend),n=1,4)
      return 
      end 
