c     ------------------------------------------------------------------
c     -- zero adjoint variable for momentum equation at the wall --
c     -- also zero the s-a model contribution at the wall --
c     -- m. nemec, oct. 2001 --
c     -- calling routine: adjoint.f --
c     ------------------------------------------------------------------
      subroutine zrpsi( is, nmax, js, je, ks, ke, jmax, kmax, psi, psit)

      implicit none
      integer is, nmax, js, je, ks, ke, jmax, kmax, j, k, n
      double precision psi(jmax,kmax,4), psit(jmax,kmax)

      if (is.eq.1) then
         k = ks
      else if (is.eq.3) then 
         k = ke
      else if (is.eq.2) then
         j = js
         goto 200
      else
         write(*,*) 'zrpsi: warrning, not tested for side 4'
         js = je
         goto 200
      end if

      do n = 2,3
         do j = js,je
            psi(j,k,n) = 0.d0
         end do
      end do 
      
      if (nmax.eq.5) then
         do j = js,je
            psit(j,k) = 0.d0
         end do
      end if

      return

 200  continue

      do n = 2,3
         do k = ks,ke
            psi(j,k,n) = 0.d0
         end do
      end do 
      
      if (nmax.eq.5) then
         do k = ks,ke
            psit(j,k) = 0.d0
         end do
      end if


      end                       !zrpsi
