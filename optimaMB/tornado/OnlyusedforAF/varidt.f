c ********************************************************************
c ****************** compute variable dt   ***************************
c ****************** result stored in ds   ***************************
c ********************************************************************
c calling routines: main            for all jacdt once
c                   integrat        for jacdt>1 at each iteration
c
      subroutine varidt(iblk,itype,jdim,kdim,q,sndsp,xy,xyj,ds,precon,
     &                  jbegin,jend,kbegin,kend)
c
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),sndsp(jdim,kdim),precon(jdim,kdim,8)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),ds(jdim,kdim)
c
c     computes variable dt based on the maximum eigenvalues
c
c     dtmin/dt because we later use ds*dt
      dtmin0 = dtmin/dt
c
      if(itype.le.1)then
c       -variable time step based on jacobian
        factor=1.0
        if(dtrate.ne.0.0)factor = 1./dtrate
        if(jacdt.eq.0) factor=0.0
        if (dtmin0.eq.0. .and. factor.eq.1.0) then
c         -99% of time dtmin0=0 and factor=1 so reduce computation
c          by eliminating some operations
          do 55 k=kbegin,kend
          do 55 j=jbegin,jend
            sqj = sqrt(xyj(j,k))
            ds(j,k)= 1.0/(1.0+sqj)
 55       continue
        else
          do 65 k=kbegin,kend
          do 65 j=jbegin,jend
            sqj = sqrt(xyj(j,k))
            ds(j,k)=(1.0+dtmin0*sqj)/(1.0+factor*sqj)
 65       continue
        endif
      elseif(itype.eq.2)then
c
c       itype = 2
c       var. dt based on eigenvalues  (coakley type)
c
        if (prec.eq.0) then
          do 100 k=kbegin,kend
          do 100 j=jbegin,jend
c           ri is j/rho                                                    
            ri = 1./q(j,k,1)
            u = q(j,k,2)*ri
            v = q(j,k,3)*ri
c
c           -note xit and ett not in calculation  
c                                                                             
            siga=abs(u*xy(j,k,1)+v*xy(j,k,2)) +sndsp(j,k)*sqrt(xy(j,k,1)
     &            **2+xy(j,k,2)**2)                
            sigb = abs(u*xy(j,k,3) +v*xy(j,k,4)) +sndsp(j,k)*
     &            sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                
            dtx = 1.d0/(siga + 1.d-11)
            dty = 1.d0/(sigb + 1.d-11)
            ds(j,k) = min(dtx,dty)
 100      continue
        else
          dure=re/(rhoinf*dt)
cdu       rescale by the new spectral radius
          do 2000 k=kbegin,kend
          do 2000 j=jbegin,jend                                       
            siga=abs(precon(j,k,5))+precon(j,k,6)
            sigb=abs(precon(j,k,7))+precon(j,k,8)
            dtx = 1.d0/(siga + 1.d-8)
            dty = 1.d0/(sigb + 1.d-8)                              
            dtv=dure*q(j,k,1)*xyj(j,k)/max(
     &            xy(j,k,1)**2+xy(j,k,2)**2,
     &            xy(j,k,3)**2+xy(j,k,4)**2)
c           
            ds(j,k) = min(dtv,max(dtx,dty))                 
 2000     continue
        endif
      elseif(itype.eq.3)then
c
c       itype = 3
c       variable dt based on constant cfl number
c
        do 200 k=kbegin,kend
        do 200 j=jbegin,jend
c         ri is j/rho
          ri = 1./q(j,k,1)
          u = q(j,k,2)*ri
          v = q(j,k,3)*ri
c
c         -note xit and ett not in calculation
c
          sigab = abs(u*xy(j,k,1)+v*xy(j,k,2))
     &          + abs(u*xy(j,k,3)+v*xy(j,k,4))
     &          + sndsp(j,k)*sqrt(xy(j,k,1)**2 + xy(j,k,2)**2
     &          +                 xy(j,k,3)**2 + xy(j,k,4)**2)
c
c         add cut off lower limit on variable dt 5/13/87  thp
c         ds(j,k) = 1./sigab
          ds(j,k) = (1.+dtmin0*sigab)/sigab
 200    continue
c
c
c       lower limit on dt
c
        do 300 k=kbegin,kend
        do 300 j=jbegin,jend
          if(ds(j,k).lt.dtmin0)ds(j,k) = dtmin0
 300    continue
      endif
c
      return
      end
