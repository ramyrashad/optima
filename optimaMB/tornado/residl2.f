c     ------------------------------------------------------------------
c     -- compute residual: L2 norm of rhs --
c     -- calling routines: integrat, nksolve --
c     -- mods: m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine residl2( jmax, kmax, iblk, s, rsa, resid, residmx,
     &     maxres, tresid, tresmx, mxtur, nres, dt, jlow, jup, klow,
     &     kup, nhalo)

      implicit none
      integer jmax, kmax, iblk,nres, jlow, jup, klow, kup, nhalo
      integer nmax,j,k,n,maxres(3),mxtur(3)
      double precision s(jmax,kmax,4),rsa(jmax,kmax)
      double precision resid,residmx,tresid,tresmx,dt,absr,abss

      nmax = 0
      if (nres.eq.5) then
         nmax=4
      else 
         nmax=nres
      end if

      do n=1,nmax
         do k=klow,kup                                                
            do j=jlow,jup
               resid = resid + s(j,k,n)**2
            end do
         end do
      end do

c     -- subtract nhalo from coordinate so history file output is
c     independent of number of haloes used --
      if (nres.eq.5) then
         do k=klow,kup                                                
            do j=jlow,jup
               tresid = tresid + rsa(j,k)**2
               absr = abs( rsa(j,k) )
               if ( absr .gt. tresmx) then
                  tresmx   = absr
                  mxtur(1) = j-nhalo
                  mxtur(2) = k-nhalo
                  mxtur(3) = iblk
               end if
            end do
         end do
      end if

      do k = klow,kup
         do j = jlow,jup                                         
            abss = abs( s(j,k,1)/dt )                             
            if ( abss.gt.residmx ) then
               residmx   = abss
               maxres(1) = j - nhalo
               maxres(2) = k - nhalo                                 
               maxres(3) = iblk
            endif
         end do
      end do

      return                                                         
      end                       !residl2
