      subroutine preproc(jdim,kdim,s,qp,q,qold,a_jk)
c
#include "../include/arcom.inc"
c
      double precision qp(jdim,kdim,4),q(jdim,kdim,4)
      double precision s(jdim,kdim,4),qold(jdim,kdim,4)       
      double precision a_jk(6,6)
c
      dt2=dt
      if (meth.eq.2 .or. meth.eq.3) then
         jsubmx=0
      else
         if (ismodel.gt.1) dt=dtow
      endif
c
      if (unsted .and. (meth.ne.1) .and. 
     &             (meth.ne.4) .and. (meth.ne.5)) then
         phic = phidt/(1.+phidt)                                        
         do 50 n=1,4
         do 50 k=1,kmax
         do 50 j=1,jmax
            s(j,k,n)=phic*(q(j,k,n)-qold(j,k,n))
 50      continue
      endif
c
      do 3 n=1,4
      do 3 k=1,kmax
      do 3 j=1,jmax
          qp(j,k,n)=q(j,k,n)
    3 continue
c
      if (turbulnt.and. itmodel.eq.2 .and. unsted) then
         do k=1,kmax
            do j=1,jmax
               turre1(j,k)=turre(j,k)
            enddo
         enddo
      endif
      do 7 k=1,6
      do 7 j=1,6
         a_jk(j,k)=0.0
 7    continue
c
      if (jesdirk.eq.4)then
         a_jk(1,1)=0.0
         a_jk(2,1)=1.0/4.0
         a_jk(2,2)=1.0/4.0
         a_jk(3,1)=8611.0/62500.0
         a_jk(3,2)=-1743.0/31250.0
         a_jk(3,3)=1.0/4.0
         a_jk(4,1)=5012029.0/34652500.0
         a_jk(4,2)=-654441.0/2922500.0
         a_jk(4,3)=174375.0/388108.0
         a_jk(4,4)=1.0/4.0
         a_jk(5,1)=15267082809.0/155376265600.0
         a_jk(5,2)=-71443401.0/120774400.0
         a_jk(5,3)=730878875.0/902184768.0
         a_jk(5,4)=2285395.0/8070912.0
         a_jk(5,5)=1.0/4.0
         a_jk(6,1)=82889.0/524892.0
         a_jk(6,2)=0.0
         a_jk(6,3)=15625.0/83664.0
         a_jk(6,4)=69875.0/102672.0
         a_jk(6,5)=-2260.0/8211.0
         a_jk(6,6)=1.0/4.0
      elseif(jesdirk.eq.3) then
         a_jk(1,1)=0.0
         a_jk(2,1)=1767732205903.0/4055673282236.0
         a_jk(2,2)=1767732205903.0/4055673282236.0
         a_jk(3,1)=2746238789719.0/10658868560708.0
         a_jk(3,2)=-640167445237.0/6845629431997.0
         a_jk(3,3)=1767732205903.0/4055673282236.0
         a_jk(4,1)=1471266399579.0/7840856788654.0
         a_jk(4,2)=-4482444167858.0/7529755066697.0
         a_jk(4,3)=11266239266428.0/11593286722821.0
         a_jk(4,4)=1767732205903.0/4055673282236.0
      endif

      return
      end

