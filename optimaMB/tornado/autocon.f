c *********************************************************************
c ** This routine automatically adjusts the th CONFILE if either the **
c ** wind tunnel or average psi bc options are active.               **
c *********************************************************************
c calling routine: ioall
c
c
c *********************************************************************
c NOTE:
c
c ibctype(i,j) give the boundary condition type = 0  interface
c                                                 1  wall
c                                                 2  farfield
c                                                 4  outflow xi max
c                                                 5  avg between blocks
c
c Thus for the windtunnel option:
c   BC's on side 4 for all downstream blocks = 4
c   BC's on side 1 for all lower far field boundary blocks = 1
c   BC's on side 3 for all upper far field boundary blocks = 1
c
c and for the Average psi BC option:
c   BC's on all interior psi lines (sides 1 & 3) that are not walls = 5
c *********************************************************************
c
      subroutine autocon

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

      integer nbs,iblock
c
c ** ADJUST CONFILE IF WIND TUNNEL OPTION IS ACTIVE
c
      if (mg .or. gseq) then
        nbs=nblkstot
      else
        nbs=nblks
      endif

      if (poutflow) then
        write(n_out,*)
        write(n_out,*) 'WIND TUNNEL OPTION ACTIVE!'
        write(n_out,*) '  -> Reworking connectivity file.'
        do 100 iblock=1,nbs
          if (ibctype(iblock,4).eq.2) ibctype(iblock,4)=4
          if (ibctype(iblock,1).eq.2) ibctype(iblock,1)=1
          if (ibctype(iblock,3).eq.2) ibctype(iblock,3)=1
  100   continue
      endif
c
c ** ADJUST CONFILE IF AVERAGE PSI BC OPTION IS ACTIVE
c
      if (psiavg) then
        write(n_out,*)
        write(n_out,*) 'AVERAGE PSI BC OPTION ACTIVE!'
        write(n_out,*) '  -> Reworking connectivity file.'
        do 200 iblock=1,nbs
          if (ibctype(iblock,1).eq.0) ibctype(iblock,1)=5
          if (ibctype(iblock,3).eq.0) ibctype(iblock,3)=5
  200   continue
      endif
c
c ** THAT'S IT !
c
      return
      end
