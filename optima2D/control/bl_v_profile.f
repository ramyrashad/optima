************************************************************************
      !-- Program name: bl_v_profile
      !-- Written by: Howard Buckley
      !-- Date: July 2011
      !-- 
      !-- This subroutine calculates interpolated velocity values along
      !-- a vector originating at intersection of the airfoil surface 
      !-- and a given x/c chord location.
************************************************************************

      subroutine bl_v_profile(jdim, kdim, ndim, q, xy, x, y)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
     
      !-- Declare variables

      integer
     &     jdim, kdim, ndim, j, jle, jv, jn, kn, jc1, kc1, jc2, kc2, i,
     &     k, h, inout, hh, jj, kk

      double precision
     &     q(jdim,kdim,ndim), xy(jdim,kdim,4), x(jdim,kdim),
     &     y(jdim,kdim), tmp1, tmp2, xjv, dist, distn, u1, u2, u3, u4,
     &     quad_cell(4,2), quad_cell0(4,2), xv, yv, xv0, yv0, u, m,
     &     y_int, k_int, j_int, dely, a, b, c, d, e, f, c1, c2, c3,
     &     p1, p2, pv, q1, q2, qv, yv1,
     &     vp_normal_vector(1,2), del, delx, xx(5), yy(5)


      !-- Determine index of leading-edge node      
      do j = jtail1, jtail2-1
         if (x(j+1,1).lt.x(j,1)) then
            jle = j+1
         end if
      end do

      !-- Determine index of node on airfoil where velocity profile is
      !-- desired
      tmp1 = 100.0
      if (v_profile_upper) then  !-- vel profile on airfoil upper surf
         do j = jle, jtail2
            tmp2 = abs(x(j,1) - v_profile_x)
            if (tmp2.lt.tmp1) then
               jv = j
               xjv = x(jv,1)
               tmp1 = tmp2
            end if
         end do
      else !-- vel profile on airfoil lower surf
         do j = jtail1, jle
            tmp2 = abs(x(j,1) - v_profile_x)
            if (tmp2.lt.tmp1) then
               jv = j
               xjv = x(jv,1)
               tmp1 = tmp2
            end if
         end do
      end if


      !-- Evaluate velocity at increments of 'del' along the velocity
      !--  profile line
      del = vp_dist/(vp_points - 1)

      do i = 1, vp_points
           
         xv = x(jv,1)
         if (v_profile_upper) then
            yv = y(jv,1) + del*(i-1)
         else
            yv = y(jv,1) - del*(i-1)
         end if
  
         !-- Find index of the node (j,k) nearest to point (xv,yv)
         distn = 1.d20
         do j = 1,jdim
            do k = 1,kdim
               dist = sqrt((x(j,k) - xv)**2 + (y(j,k) - yv)**2)
               if (dist.lt.distn) then
                  jn = j
                  kn = k
                  distn = dist
               end if
            end do
         end do     
 
         !-- Scan cells surrounding nearest node at (jn,kn) to find
         !-- which cell contains point (xv,yv)
         
         if (kn.eq.1) then
            hh = 3
         else if (kn.eq.2) then
            hh = 4
         else if (kn.eq.3) then
            hh = 5
         else if (kn.gt.3) then
            hh = 6
         end if
            
      
         do jj = 1,6
            do kk = 1,hh

               !-- Set indicies of cell to be scanned
               jc1 = jn - (4-jj)
               jc2 = jn - (3-jj)
               kc1 = kn + (3-kk)
               kc2 = kn + (4-kk)
 
               !-- Set coordinates of the cell vertices
               !-- 5th vertex = 1st vertex
               xx(1) = x(jc1,kc1)
               xx(2) = x(jc2,kc1)
               xx(3) = x(jc2,kc2)
               xx(4) = x(jc1,kc2)
               xx(5) = x(jc1,kc1)
               
               yy(1) = y(jc1,kc1)
               yy(2) = y(jc2,kc1)
               yy(3) = y(jc2,kc2)
               yy(4) = y(jc1,kc2)
               yy(5) = y(jc1,kc1)

               !-- Check if point (xv,yv) lies inside this cell
               call pt_in_poly(xv,yv,xx,yy,INOUT)

               if (inout.eq.1) goto 10
               
            end do
         end do 

         write(scr_unit,*)'interpolated point not found in any cell'
         write(scr_unit,*)'nearest node (jn,kn): ', jn,kn

 10      continue
         
         !-- Store the values of velocity in the x-direction  at the 
         !-- corners of the cell
         u1 = q(jc1,kc1,2)/q(jc1,kc1,1)
         u4 = q(jc1,kc2,2)/q(jc1,kc2,1)
         u3 = q(jc2,kc2,2)/q(jc2,kc2,1)
         u2 = q(jc2,kc1,2)/q(jc2,kc1,1)

         !-- Translate quadrilateral cell so corner is at x=0, y=0 
         !-- before mapping to the unit square
         quad_cell(1,1) = x(jc1,kc1)
         quad_cell(1,2) = y(jc1,kc1)
         quad_cell(2,1) = x(jc1,kc2)
         quad_cell(2,2) = y(jc1,kc2)
         quad_cell(3,1) = x(jc2,kc2)
         quad_cell(3,2) = y(jc2,kc2)
         quad_cell(4,1) = x(jc2,kc1)
         quad_cell(4,2) = y(jc2,kc1)

         do h = 1,4
            quad_cell0(h,1) = quad_cell(h,1) - quad_cell(1,1)
            quad_cell0(h,2) = quad_cell(h,2) - quad_cell(1,2)
         end do

         xv0 = xv - quad_cell(1,1)
         yv0 = yv - quad_cell(1,2)

         !-- Map the quadrilateral cell on to the unit square
         a = quad_cell0(4,1)
         b = quad_cell0(2,1)
         c = quad_cell0(3,1) - quad_cell0(4,1) - quad_cell0(2,1)
         d = quad_cell0(4,2)
         e = quad_cell0(2,2)
         f = quad_cell0(3,2) - quad_cell0(4,2) - quad_cell0(2,2)

         c1 = c*d - a*f
         c2 = -c*yv0 + b*d + xv0*f - a*e
         c3 = -yv0*b + e*xv0

         p1 = (-c2 + sqrt(c2**2 - 4*c1*c3))/(2*c1)
         p2 = (-c2 - sqrt(c2**2 - 4*c1*c3))/(2*c1)
         
         q1 = (xv0 - a*p1)/(b + c*p1)
         q2 = (xv0 - a*p2)/(b + c*p2)

         !-- Throw out the root that lies outside the unit square
         if ((p1.lt.0.0).or.(p1.gt.1.0)) then
            pv = p2
            qv = q2
         else
            pv = p1
            qv = q1
         end if

         !-- Use bilinear interpolation to obtain the value of 
         !-- x-direction velocity at location (xv,yv) within the cell
         u = u1*(1.0 - pv)*(1.0 - qv) + 
     &        u2*pv*(1.0 - qv) + u4*(1.0 - pv)*qv + u3*pv*qv

         !-- Translate y-coordinates so that y=0 at the airfoil surface
         yv1 = yv - y(jv,1)

         !-- Write interpolated velocity to file
         write(blvp_unit,*) u, yv1
         
      end do

      return
      
      end

************************************************************************
      !-- Program name: pt_in_poly
      !-- Written by: Howard Buckley
      !-- Date: July 2011
      !--  
      !-- This subroutine checks if point (xv,yv) lies in the cell 
      !-- defined by the vertices contained in arrays xx and yy.
      !--
      !-- Method: 
      !--   A VERTICAL LINE IS DRAWN FROM THE POINT IN QUESTION TO
      !--   +VE INFINITY. IF IT CROSSES THE POLYGON AN ODD NUMBER OF 
      !--   TIMES, THEN THE POINT IS INSIDE OF THE POLYGON. 
************************************************************************

      subroutine pt_in_poly(xv,yv,xx,yy,inout)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
      
      !-- Declare variables

      integer
     &     inout, bc, i

      double precision
     &     xv, yv, xx(5), yy(5), m, x_int, y_int, beta, beta_x, beta_y,
     &     b, vector(2)


      !-- Initialize border crossing counter
      bc = 0

      !-- Test all four sides of the cell for intersections with the 
      !-- vertical line x = xv
      do i = 1,4
 
         !-- Define the line passing through vertices i and i+1
         !-- y = mx + b
         m = (yy(i+1) - yy(i))/(xx(i+1) - xx(i))
         b = yy(i+1) - m*xx(i+1)

         !-- Find the point of intersection between the line passing 
         !-- through vertices i and i+1 and the line x = xv
         x_int = xv
         y_int = m*xv + b

         !-- Define a vector that connects the vertices
         vector(1) = xx(i+1) - xx(i)
         vector(2) = yy(i+1) - yy(i)

         !-- Calc the scalar 'beta' that satisfies the following eq:
         !--    [xx(i),yy(i)] + beta*vector = [x_int,y_int]
         beta_x = (x_int - xx(i))/vector(1)
         beta_y = (y_int - yy(i))/vector(2)
                           
         beta = (beta_x + beta_y)/2
 
         !-- Two criteria for determining if the vertical line has
         !-- crossed a cell border:
         !-- 1) Since the vertical line originates at point (xv,yv), an
         !-- intersection can only occur if y_int > yv.
         !-- 2) The scalar beta must be greater than 0 and less than 1.
         !--    If beta = 0  or beta = 1, then the line passes through
         !--    a vertex.
         if ((y_int.ge.yv).and.(beta.gt.0.d0).and.(beta.lt.1.d0)) then
            bc = bc+1 ! Increment cell border crossing count
         end if
         
         !-- To avoid counting two border crossings if the intersection
         !-- coincides with a vertex, only count it if beta = 0.
         if ((y_int.ge.yv).and.(beta.eq.0.d0)) bc = bc+1 
         
      end do
 
      if (bc.eq.1) then
         inout  = 1 ! Point is inside cell
      else
         inout = 0 ! Point is outside cell
      end if
 
      return
      
      end
