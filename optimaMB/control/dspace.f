c     ------------------------------------------------------------------
c     -- design space map --
c     -- m. nemec, nov. 2001 --
c     ------------------------------------------------------------------
      subroutine dspace(nmax,indx,q,qps,qos,xy,xyj, x, y, turmu, vort,
     &     turre,turps,turos,fmu,press,precon,sndsp,xit,ett,ds, vk, ve,
     &     coef2x, coef4x, coef2y, coef4y, s, uu, vv, ccx, ccy, spectxi,
     &     specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja, icol)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/index.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer strlen,nmax,namelen,ifirst,ib,lg,i,i2,i1,ntvar

      double precision q(*),             press(*),       sndsp(*)
      double precision s(*),             xy(*),          xyj(*)
      double precision xit(*),           ett(*),         ds(*)
      double precision x(*),             y(*),           turmu(*)
      double precision vort(*),          turre(*),       fmu(*)
      double precision vk(*),            ve(*),          tmp(*)
      double precision uu(*),            vv(*),          ccx(*)
      double precision ccy(*),           coef2x(*),      coef2y(*)
      double precision coef4x(*),        coef4y(*),      precon(*)
      double precision spectxi(*),       specteta(*)
      double precision qps(*),           qos(*)
      double precision turps(*),         turos(*)
      double precision wk1(*)

      integer ia(*), ja(*), ipa(*), jpa(*), icol(*), indx(*)

      double precision dvhold1(10), dvhold2(10),obj

c     -- objective function --
      real CalcObj

      double precision xs(maxjk), ys(maxjk)      
      double precision obhis(21,21),clhis(21,21),cdhis(21,21)
      double precision crhis(21,21) 

      logical frozen
      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

      namelen = strlen(output_file_prefix)
      filena  = output_file_prefix
      filena(namelen+1:namelen+4) = '.ds'

      open( unit=n_ds, file=filena, status='unknown')

      ifirst = 1                ! no warm starts

c      dvs(1)    =  0.12020370E+01
c      dvs(2)    =  -0.10051000E+00

      dvs(1)    =  0.1207037000000000E+01
      dvs(2)    = -0.9851000000000000E-01
c      dvs(2)    =  -9.61999999999999E-02


c      dvhold1(1) = 0.12122538E+01
      dvhold1(1) = 0.1205037E+01
      dvhold1(2) = 1.0712
c      dvhold1(3) = 1.0708
c      dvhold1(4) = 1.0705
c      dvhold1(4) = 1.0702
c      dvhold1(5) = 1.068
c      dvhold1(6) = 1.067
c      dvhold1(7) = 1.066
c      dvhold1(8) = 1.065
c      dvhold1(9) = 1.065

      dvhold2(1) = -0.962E-01
      dvhold2(2) = 0.0
c      dvhold2(3) = 0.0
c      dvhold2(4) = 0.0
c      dvhold2(5) = 0.0
c      dvhold2(6) = 0.0
c      dvhold2(7) = 0.0
c      dvhold2(8) = 0.0
c      dvhold2(9) = 0.0

      obj = 1.0
      clt = 1.0
      cdt = 1.0
      grad(1) = 0.0
      grad(2) = 0.0

      do ib = 1,nblks
         lg = lgptr(ib)
         call copy_array( jmax(ib), kmax(ib), 1, x(lg), xs(lg))
         call copy_array( jmax(ib), kmax(ib), 1, y(lg), ys(lg))
      end do

      i = 0
      
      i2=1
c      do i2 = 1,1
         do i1 = 1,1

            do ib = 1,nblks
               lg = lgptr(ib)
               call copy_array( jmax(ib), kmax(ib), 1, xs(lg), x(lg))
               call copy_array( jmax(ib), kmax(ib), 1, ys(lg), y(lg))
            end do

            call setbap(x, y)
            call regrid(-1, x, y)

c     -- output current grid --
            call outgrd( x, y)
            
            frozen = .false.    !flag used for freezing dissipation
           
c     -- compute obj. function --      
            obj = CalcObj( ifirst, nmax, indx, q,qps,qos, xy, xyj, x, y,
     &           turmu,vort,turre,turps,turos,fmu,press,precon,sndsp,
     &           xit,ett,ds,vk,ve,coef2x,coef4x, coef2y, coef4y, s, uu,
     &           vv, ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa,
     &           jpa, ia, ja, icol)

            obhis(i1,i2) = obj
            clhis(i1,i2) = clt
            cdhis(i1,i2) = cdt
            crhis(i1,i2) = clt/cdt

            if (wstart) ifirst = 0 

            frozen = .false.
     
c            call CalcGrad( ifirst, nmax, indx, q, xy, xyj, x, y,
c     &           turmu, vort, turre, fmu, press, precon, sndsp, xit,
c     &           ett, ds, vk, ve, coef2x, coef4x, coef2y, coef4y, s, uu,
c     &           vv, ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa, 
c     &           jpa, ia, ja, icol, xs, ys)
            
            i = i+1
c            write (n_ohis,100) i, dvs(1), dvs(2), obj, grad(1), grad(2)
            write (n_ohis,100) i, dvs(1), dvs(2), obj, clhis(i1,i2), 
     &           cdhis(i1,i2)

            dvs(1) = dvhold1(i1)
            dvs(2) = dvhold2(i1)


         end do


 100  format (i4,5e30.16)
 150  format (e15.6)
 200  format (5e15.6,';')

      close (n_ds)

      return
      end                       ! dspace


