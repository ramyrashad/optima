      function epsil(fmx,fmin,dfm,npt,fpcc,icc,ncall)                   
c                                                                       
c     This subroutine applies a newton-raphson root-finding          
c     technique to find a value of epsilon for a particular use      
c     of the exponential streching transformation.                   
c                                                                       
c     fmx  : is total arc length along coordinate                            
c     fmin : is starting value of arc length   such as 0.0                  
c     dfm  : is specified initial increment of arc length                    
c     npt  : is number of points along coordinate                            
c     fpcc : is iterative error bound, e.g.o( 0.00002)                      
c     icc  : is maximum number of iterations                                 
c     ncall:    if ncall=1 initial guess for eps is used                   
c               if ncall .gt. 1, previous eps used as initial guess        
c                                                                       
c
#include "../include/units.inc" 

      fmxl=fmx                                                          
      fminl=fmin                                                        
      dfml=dfm                                                          
      fpccl=fpcc                                                        
      iccl=icc                                                          
c                                                                       
c                                                                       
1     fnptm2=npt-2                                                      
      if(ncall.eq.1)  eps=(fmxl/dfml)**(1.0/fnptm2)-1.0                 
c                                                                       
      do 3 nit=1,iccl                                                   
         ep1=eps+1.0                                                       
         ep1tn=ep1**fnptm2                                                 
         reps=1.0/eps                                                      
         dfmoe=dfml*reps                                                   
         f=fmxl-fminl-dfmoe*(ep1tn*ep1-1.0)                                
         if(abs(f).lt.fpccl)  go to 4                                      
         dfmoe2=dfmoe*reps                                                 
         fpn=dfmoe2*(1.0+ep1tn*(eps*fnptm2-1.0))                           
         eps=eps+f/fpn                                                     
3     continue                                                          
c                                                                       
5     epsil=eps                                                         
      write(out_unit,100)                                                      
      return                                                            
c                                                                       
4     epsil=eps                                                         
      write(out_unit,101)  epsil,f,nit                                       
      return                                                            
c                                                                       
100   format(/42h exceeded max. no. of iterations in epsil.)            
101   format(/7h epsil=,f12.5,5x,7h and f=,f12.5,5x,7h after ,i3,     
     *  12h iterations.)                                              
c                                                                       
      end                                                               
