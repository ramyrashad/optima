      subroutine predis(jdim,kdim,q,s,xyj,sndsp,precon,tmp,
     &           jlow,jup,klow,kup)

      implicit none

#include "../include/common.inc"

cdu  this routine multiplies the vector tmp by the preconditioner
cdu  in conversative variables

      integer jdim,kdim,jlow,jup,klow,kup,j,k,i
      double precision q(jdim,kdim,4),xyj(jdim,kdim),sndsp(jdim,kdim)
      double precision precon(jdim,kdim,8),tmp(jdim,kdim,4)
      double precision s(jdim,kdim,4),rho,u,v,c,t1,t2,t3,t4
c
c     note: gami=gamma - 1
c
      if (prec.gt.0) then
         do 1000 k=klow,kup
            do 1000 j=jlow,jup
c        
               rho=q(j,k,1)*xyj(j,k)
               u=q(j,k,2)/q(j,k,1)
               v=q(j,k,3)/q(j,k,1)
cdu            c=dsqrt(precon(j,k,3))
               c=sndsp(j,k)
c        
c        multiply by M inverse
c        
               t4=gami*(precon(j,k,2)*tmp(j,k,1)-u*tmp(j,k,2)-
     +              v*tmp(j,k,3)+tmp(j,k,4))
               t1=t4/rho/c
               t4=t4-precon(j,k,3)*tmp(j,k,1)
               t2=(tmp(j,k,2)-u*tmp(j,k,1))/rho
               t3=(tmp(j,k,3)-v*tmp(j,k,1))/rho
c        
c        multiply by Gamma
c        
               t1=t1/precon(j,k,1)
c        
c        multiply by M
c        
               tmp(j,k,1)=(rho*t1-t4/c)/c
               tmp(j,k,2)=u*tmp(j,k,1)+rho*t2
               tmp(j,k,3)=v*tmp(j,k,1)+rho*t3
               tmp(j,k,4)=rho*((precon(j,k,2)/c+c/gami)*t1+
     +              u*t2+v*t3)-precon(j,k,2)/precon(j,k,3)*t4
c        
 1000    continue
      endif
c
c
      do 200 i=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
           s(j,k,i)=s(j,k,i)+tmp(j,k,i)
 200  continue
      return
      end
