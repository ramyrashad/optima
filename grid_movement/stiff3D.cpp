#ifndef STIFF3D_H
#include "stiff3D.h"
#endif

/*
  Purpose:  form the element stiffness matrix
  Element type:  hexahedral
*/


void get_K_hex (const vector <double*>& vertices, double** K, 
		double* J, double* invJ, vector < vector<double> >& dN,
		vector < vector<double> >& B, 
		vector< vector<double> >& tempI, const double& elemQo, 
		double& meshQ)
{
  int i, j;   // counter

  double Q; // Q = measure of element quality, 
  double volume;

  // cout << " set up material matrix, assume plane strain " << endl;
  // get_Q_hex (vertices, Q);  
  // Q = 10.0/( Q*(1.0+nu)*(1.0-2.0*nu) );
  
  if (meshQ==-1)  // 1st increment
    Q = 1.0;
  else
    {
      get_Q_hex(vertices, Q);
      if (Q > meshQ)
	meshQ = Q;
      Q = pow(Q/elemQo, 2);   // 2 is the power law model
    }

  volume_hex (vertices, volume);
  volume = Q/( volume *(1.0+nu)*(1.0-2.0*nu) );

      /*
    ofstream fout("vol.dat", ios::app);
    fout << Q << endl;
    fout.close();
    */
    //cout <<" finish get_Q " << Q <<endl;//      exit (9) ; 
  //Q = 10000000;
   // Q = 1.0/( Q*(1.0+nu)*(1.0-2.0*nu) );
    
  double* D = new double [3];
  D[0] = volume*(1.0-nu);
  D[1] = volume*nu;
  D[2] = volume*(0.5-nu);

  //cout <<" initialize the elements in stiffness matrix " << endl;
  for (i=0; i<24; i++)    
    for (j=0; j<24; j++)    
      K[i][j] = 0.0;  

  double detJ (0);                // determinant of Jacobian
    
  for (i=0; i<8; i++)  // 8= number of integration point
    {
      // cout << "form _dN " << endl;
      form_dN (dN, i);

      //cout << "get_B " << endl;
      get_B (vertices, dN, J, invJ, B, detJ);
      if (detJ < 0)
	{
	  for (j=0; j<8; j++)
	    cout << vertices[j][0] << "  " << vertices[j][1]<< "   "
		 << vertices[j][2] <<endl;
	  cerr << "negative determinant " << detJ << endl;
	  exit (8);
	}
      /*      ofstream fo ("test.dat");
      for (int ii=0; ii<6; ii++)
	for (int jj=0; jj<24; jj++)
	  fo << ii << "  " << jj << "  " << B[ii][jj] << endl;
      exit (9);
      */
      // cout << i << "  calc_K now " << endl;
      calc_K (B, D, K, detJ, tempI);
    }
  /*
  ofstream fout ("K.test1.dat", ios::app);
  for (i=0; i<24; i++)    
    for (j=0; j<24; j++)    
      fout << K[i][j] << endl;
  fout.close();
  */
  delete [] D;     D = NULL;
  
  return;
}


void form_dN ( vector <vector<double> >& dN, const int& gaussPT)
{

  double zeta, eta, mu;   // local coordinate: zeta, eta, and mu

  switch (gaussPT)
    {
    case 0:
      zeta = -Gauss;  eta = -Gauss; mu = -Gauss;
      break;
    case 1:
      zeta = Gauss;  eta = -Gauss; mu = -Gauss;
      break;
    case 2:
      zeta = Gauss;  eta = Gauss; mu = -Gauss;
      break;
    case 3:
      zeta = -Gauss;  eta = Gauss; mu = -Gauss;
      break;
    case 4:
      zeta = -Gauss;  eta = -Gauss; mu = Gauss;
      break;
    case 5:
      zeta = Gauss;  eta = -Gauss; mu = Gauss;
      break;
    case 6:
      zeta = Gauss;  eta = Gauss; mu = Gauss;
      break;
    case 7:
      zeta = -Gauss;  eta = Gauss; mu = Gauss;
      break;
    default:
      cerr << "Unknown coordinate position " << endl;
      exit (10);
    }

  dN[0][0] = -0.125 * ( 1.0 - mu - eta + eta*mu );
  dN[0][1] = -dN[0][0];
  dN[0][2] = 0.125 * ( 1.0 - mu + eta - eta*mu );
  dN[0][3] = -dN[0][2];
  dN[0][4] = -0.125 * ( 1.0 + mu - eta - eta*mu );
  dN[0][5] = -dN[0][4];
  dN[0][6] = 0.125 * ( 1.0 + mu + eta + eta*mu );
  dN[0][7] = -dN[0][6];


  dN[1][0] = -0.125 * ( 1.0 - mu - zeta + zeta*mu );
  dN[1][1] = -0.125 * ( 1.0 - mu + zeta - zeta*mu );
  dN[1][2] = -dN[1][1];
  dN[1][3] = -dN[1][0];
  dN[1][4] = -0.125 * ( 1.0 + mu - zeta - zeta*mu );
  dN[1][5] = -0.125 * ( 1.0 + mu + zeta + zeta*mu );
  dN[1][6] = -dN[1][5];
  dN[1][7] = -dN[1][4];


  dN[2][0] = -0.125 * ( 1.0 - eta - zeta + zeta*eta );
  dN[2][1] = -0.125 * ( 1.0 - eta + zeta - zeta*eta );
  dN[2][2] = -0.125 * ( 1.0 + eta + zeta + zeta*eta );
  dN[2][3] = -0.125 * ( 1.0 + eta - zeta - zeta*eta );
  dN[2][4] = -dN[2][0];
  dN[2][5] = -dN[2][1];
  dN[2][6] = -dN[2][2];
  dN[2][7] = -dN[2][3];

  return;
}


void get_B (const vector <double*>& vertices, 
	     const vector <vector <double> >& dN, double* J, 
	    double* invJ, vector < vector <double> >& B, double& detJ)
{
  int i, j , k, m=0;

  /* for (k=0; k<8; k++) 
    for (i=0; i<3; i++) 
      cout << vertices[k][i] << endl;
      exit(9);*/

  // determine the Jacobian matrix
  for (i=0; i<3; i++)
    {
      for (j=0; j<3; j++)     
	{
	  J[m] = 0.0;
	  for (k=0; k<8; k++) 
	    J[m] += dN[i][k] * (vertices[k][j]); 
	  m++;
	}
     }
  //  cout << "finish determining Jacobian in get_B" << endl;
  //  detJ =  J[0]*(J[4]*J[8] - J[5]*J[7]) -
  //J[1]*(J[3]*J[8] - J[5]*J[6]) + J[2]*(J[3]*J[7] - J[4]*J[6]);
  //cout << "det J Here " << detJ;

  //for (i=0; i<9; i++)
  //cout << i << "  " << J[i] << endl;
  //exit (9);
  

//cout <<"calculate the inverse of jacobian and determinant of Jacobian"<<endl;
  //  double * invJ  = new double [9];
  //cout << "just before invert" << endl;
  invert_3x3 (J, invJ, detJ);
  
  //  cout << "detJ " << detJ << endl;

  j = 0;
  for (i=0; i<24; i+=3)  // 24 = number of gauss pt * dof
    {
      k = i+1;
      m = i+2;

      B[0][i] = invJ[0]*dN[0][j] + invJ[1]*dN[1][j] + 
	invJ[2]*dN[2][j];
      B[1][k] = invJ[3]*dN[0][j] + invJ[4]*dN[1][j] + 
	invJ[5]*dN[2][j];
      B[2][m] = invJ[6]*dN[0][j] + invJ[7]*dN[1][j] + 
	invJ[8]*dN[2][j];
      
      B[3][i] =  B[1][k];       B[3][k] =  B[0][i];
      B[4][k] =  B[2][m];       B[4][m] =  B[1][k];
      B[5][i] =  B[2][m];       B[5][m] =  B[0][i];
      
      j++;
    }

    //cout << "exit get_B " << endl;
  return;
}


// inverts a 3x3 matrix, elements of the matrix is in a 1D array
// returns the inverted matrix in 1D array + determinant of 
// original matrix
void invert_3x3 (const double* A, double *invA, double& detA)
{

  invA[0] = A[4]*A[8] - A[5]*A[7];
  invA[1] = A[2]*A[7] - A[1]*A[8];
  invA[2] = A[1]*A[5] - A[2]*A[4];
  invA[3] = A[5]*A[6] - A[8]*A[3];
  invA[4] = A[0]*A[8] - A[6]*A[2];
  invA[5] = A[2]*A[3] - A[5]*A[0];
  invA[6] = A[3]*A[7] - A[6]*A[4];
  invA[7] = A[1]*A[6] - A[7]*A[0];
  invA[8] = A[4]*A[0] - A[1]*A[3];

  detA = A[0]*invA[0] + A[1]*invA[3] + A[2]*invA[6];

  for (int i=0; i<9; i++)
    invA[i] = invA[i]/detA;

  return;
}


void calc_K (const vector < vector <double> >& B, 
	     const double* D, double** K, const double& detJ,
	     vector < vector <double> >& tempI)
{
 
  int i, j, k;   // counter
  double Kij;

  // calculate tempI = transpose(B) * D
  for (i=0; i<24; i+=3)
    {
      tempI[i][0] = B[0][i] * D[0];
      tempI[i][1] = B[0][i] * D[1];
      tempI[i][2] =  tempI[i][1];
      tempI[i][3] = B[3][i] * D[2];
      tempI[i][5] = B[5][i] * D[2];

      j = i+1;
      tempI[j][0] = B[1][j] * D[1];
      tempI[j][1] = B[1][j] * D[0];
      tempI[j][2] =  tempI[j][0];
      tempI[j][3] = B[3][j] * D[2];
      tempI[j][4] = B[4][j] * D[2];

      j = i+2;
      tempI[j][0] = B[2][j] * D[1];
      tempI[j][1] = tempI[j][0]; 
      tempI[j][2] = B[2][j] * D[0];
      tempI[j][4] = B[4][j] * D[2];
      tempI[j][5] = B[5][j] * D[2];
    }


  // calculate K = tempI * B
   for (i=0; i<24; i++)    
     {
       for (j=0; j<24; j++)    
	 {
	   Kij = 0.0;
	   for (k=0; k<6; k++)   
	     Kij += tempI[i][k]*B[k][j]; 
	   K[i][j] +=  Kij * detJ;
	 }
     }
  
   // cout << "  end " << endl;

  return;

}


// calculate element quality of a hexahedron
void get_Q_hex (const vector <double*>& vertices, double& Q)
{

  //  Q (element quality) is calculated using norm-2 

  /*
          /\
         / ' \
      W /  '   \ 
       /  u'     \    V
      /    '        \ 
     /   '    '        \
    /  ' v       w'      \
   /...................'... \
                U
 */
  
  double aspect;
  
  // getting aspect ratio of 8 sub tets
  aspect_tet (vertices[0], vertices[2],
	      vertices[5], vertices[1],
	      aspect);
  Q = aspect * aspect;
  aspect_tet (vertices[7], vertices[2],
	      vertices[6], vertices[5],
	      aspect);
  Q = Q + aspect * aspect;
  aspect_tet (vertices[3], vertices[6],
	      vertices[7], vertices[4],
	      aspect);
  Q = Q + aspect * aspect;
  aspect_tet (vertices[0], vertices[7],
	      vertices[4], vertices[5],
	      aspect);
  Q = Q + aspect * aspect;
  aspect_tet (vertices[0], vertices[3],
	      vertices[4], vertices[1],
	      aspect);
  Q = Q + aspect * aspect;
  aspect_tet (vertices[5], vertices[4],
	      vertices[6], vertices[1],
	      aspect);
  Q = Q + aspect * aspect;
  aspect_tet (vertices[3], vertices[2],
	      vertices[6], vertices[1],
	      aspect);
  Q = Q + aspect * aspect;
  aspect_tet (vertices[0], vertices[3],
	      vertices[7], vertices[2],
	      aspect);
  Q = Q + aspect * aspect;

  Q = sqrt (Q);

  return;
}


// computes the aspect ratio of a tetrahedron in 3D.
void aspect_tet (const double* a, const double* b, const double* c,
		 const double* d, double& Q)
{
  double volume, area;  // volume and surface area of tetrahedral

  volume_tet(a, b, c, d, volume, area);
  
  // finds the surface area of the tetrahedron
  area_tet (a, b, c, d, area);
  // finds the maximum edge length in the tetrahedron
  max_length_tet (a, b, c, d, Q);

  Q = Q*area/(3.0*volume);

  return;
}

void volume_tet(const double* a, const double* b, const double* c,
		 const double* d, double& volume, double& area)
{
  // area is just a temporary variable 

  /*  
      modified from:
      http://orion.math.iastate.edu/burkardt/c_src/geometryc/geometryc.c
      Author:    John Burkardt   16 April 1999
      Parameters:
      Input: four corners of the tetrahedron arranging counterclockwise
      looking from the last node
      volume is:
                   1    1    1    1
      (1/6) * det  a0   b0   c0   d0
                   a1   b1   c1   d1
                   a2   b2   c2   d2
      Output, volume of tetrahedron
  */

  // computes the volume of a tetrahedron in 3D.
  //computes the determinant of a 4 by 4 matrix.
  // the variable area is used as a temporary variable

  area = c[1]*d[2] - d[1]*c[2];
  volume = area * (b[0] - a[0]);
  area = b[1]*d[2] - b[2]*d[1];
  volume += area * (a[0] - c[0]);
  area = b[1]*c[2] - b[2]*c[1];
  volume += area * (d[0] - a[0]);
  area = a[1]*d[2] - a[2]*d[1];
  volume += area * (c[0] - b[0]);
  area = a[1]*c[2] - a[2]*c[1];
  volume += area * (b[0] - d[0]);
  area = a[1]*b[2] - a[2]*b[1];
  volume += area * (d[0] - c[0]);
  
  // cout << "volume in stiff3D is " << volume << endl;
  /*  if (volume < 0.0)
    {
      cerr << "Error in nodal numbering of the tetrahedra: "
	   << "negative volume encountered! " << endl;
      exit (88);
    }
    else*/
    volume = fabs(volume);

  return;
}



void max_length_tet (const double* x1, const double* x2, const double* x3,
		 const double* x4, double& maxL)
{
  double len = 0.0;
  calc_len (x1, x2, maxL);
  calc_len (x2, x3, len);
  if (len > maxL)
    maxL = len;
  calc_len (x3, x1, len);
  if (len > maxL)
    maxL = len;
  calc_len (x4, x3, len);
  if (len > maxL)
    maxL = len;
  calc_len (x2, x4, len);
  if (len > maxL)
    maxL = len;
  calc_len (x1, x4, len);
  if (len > maxL)
    maxL = len;

  return;
}


void calc_len (const double* x1, const double* x2, double& len)
{
  len = sqrt ( (x1[0]-x2[0])*(x1[0]-x2[0]) + 
	       (x1[1]-x2[1])*(x1[1]-x2[1]) +
	       (x1[2]-x2[2])*(x1[2]-x2[2]) );

  return;
}


// computes the magnitude of the result of cross product of 
// two vectors in 3D.
void mag_cross_vec (const double* x0, const double* x1,  
		    const double* x2, double& mag)
{
  /*  
      modified from:
      http://orion.math.iastate.edu/burkardt/f_src/geometry/geometry.f90
      Author:    John Burkardt   16 April 1999

      x3 = ( y1 - y0 ) * ( z2 - z0 ) - ( z1 - z0 ) * ( y2 - y0 )
      y3 = ( z1 - z0 ) * ( x2 - x0 ) - ( x1 - x0 ) * ( z2 - z0 )
      z3 = ( x1 - x0 ) * ( y2 - y0 ) - ( y1 - y0 ) * ( x2 - x0 )
      Parameters:
      Input,  the coordinates of three points.  The basis point is (X0,Y0,Z0).
      Output, magnitude of the cross product vector.
  */

  double *vec_prod = new double[3];
  cross_vec3D (x0, x1, x2, vec_prod);
  mag = sqrt( vec_prod[0]*vec_prod[0] + vec_prod[1]*vec_prod[1] +
	      vec_prod[2]*vec_prod[2] );

  delete [] vec_prod;   vec_prod = NULL;

  return;
}


// performs vector cross product in 3D given 3 points
void cross_vec3D (const double* x0, const double* x1,  
		    const double* x2, double* prod)
{

  prod[0] = ( x1[1] - x0[1] ) * ( x2[2] - x0[2] )
    - ( x1[2] - x0[2] ) * ( x2[1] - x0[1] );

  prod[1] = ( x1[2] - x0[2]) * ( x2[0] - x0[0] ) 
    - ( x1[0] - x0[0] ) * ( x2[2] - x0[2] );

  prod[2] = ( x1[0] - x0[0] ) * ( x2[1] - x0[1] ) 
    - ( x1[1] - x0[1] ) * ( x2[0] - x0[0] );

  return;
}

// calculates the area of a quadrilateral by cross products of the 
// sides
void area_quad (const double* a1, const double* a2, const double* a3,
		const double* a4, double& area)
{
  /* The corners should be specified in clockwise or counterclockwise
     order
  */
  double tmp;  // result of cross product

  mag_cross_vec (a1, a2, a3, area);

  mag_cross_vec (a3, a4, a1, tmp);

  area = 0.5 * (area + tmp);

  return;
}


// calculates the surface area of a tetrahedron
void area_tet (const double* a1, const double* a2, const double* a3,
		const double* a4, double& area)
{
  double tmp;
  mag_cross_vec (a1, a2, a3, tmp);
  area = tmp;
  mag_cross_vec (a1, a2, a4, tmp);
  area += tmp;
  mag_cross_vec (a3, a2, a4, tmp);
  area += tmp;
  mag_cross_vec (a3, a1, a4, tmp);
  area += tmp;

  area = 0.5*area;

  return;
}



void volume_hex (const vector <double*>& corners, double& volume)
{
  // divide the hexahedron into 5 tetrahedrons
  // --> volume = sum of volumes of the tets
  // input corners to volume_tet function are arranged
  // counter clockwise (looking from the 4th corner)

  double temp, temp2;
  /*
  volume_tet (corners[0], corners[2], corners[3], corners[7], volume, temp2);
  volume_tet (corners[2], corners[5], corners[6], corners[7], temp, temp2);
  volume += temp;
  volume_tet (corners[0], corners[5], corners[7], corners[4], temp, temp2);
   volume += temp;
  volume_tet (corners[0], corners[1], corners[2], corners[5], temp, temp2);
  volume += temp;
  volume_tet (corners[0], corners[2], corners[7], corners[5], temp, temp2);
   volume += temp;
   // cout << "5. testing volume "<< volume << endl;  
   */
  volume_tet (corners[2], corners[6], corners[3], corners[1], volume, temp2);
  volume_tet (corners[1], corners[6], corners[4], corners[5], temp, temp2);
  volume += temp;
  volume_tet (corners[3], corners[1], corners[4], corners[0], temp, temp2);
  volume += temp;
  volume_tet (corners[3], corners[6], corners[7], corners[4], temp, temp2);
  volume += temp;
  // problem tet
  volume_tet (corners[3], corners[6], corners[4], corners[1], temp, temp2);
  volume += temp;
   
  // cout << "org volume " << volume << endl;*/
 
  volume = volume / 6.0; // remember to take out / 6.0 in volume_tet!!!!!!!

  return;
}



void get_K_Tet(const vector <double*>& vertices, double** K)
{

  int i, j;
  
  // initialize the elements in stiffness matrix
  for (i=0; i<12; i++)    
    for (j=0; j<12; j++)    
      K[i][j] = 0.0;  

  double* temp = new double[12];
  // transpose(B) * E 
  vector < vector <double> > BTE (12, vector<double> (6,0.0) );    

  double z1 = vertices[3][2]-vertices[2][2];
  double z2 = vertices[3][2]-vertices[1][2];
  double z3 = vertices[2][2]-vertices[1][2];
  temp[0] = vertices[1][1]*z1 - vertices[2][1]*z2 + vertices[3][1]*z3;
  temp[1] = -vertices[1][0]*z1 + vertices[2][0]*z2 - vertices[3][0]*z3;

  z2 = vertices[3][2]-vertices[0][2];
  z3 = vertices[2][2]-vertices[0][2];
  temp[2] = -vertices[0][1]*z1 + vertices[2][1]*z2 - vertices[3][1]*z3;
  temp[3] = vertices[0][0]*z1 - vertices[2][0]*z2 + vertices[3][0]*z3;

  z1 = vertices[3][2]-vertices[1][2];
  z3 = vertices[1][2]-vertices[0][2];
  temp[4] = vertices[0][1]*z1 - vertices[1][1]*z2 + vertices[3][1]*z3;
  temp[5] = -vertices[1][0]*z1 + vertices[2][0]*z2 - vertices[3][0]*z3;

  z1 = vertices[2][2]-vertices[1][2];
  z2 = vertices[2][2]-vertices[0][2];
  temp[6] = -vertices[0][1]*z1 + vertices[1][1]*z2 - vertices[2][1]*z3;
  temp[7] = vertices[0][0]*z1 - vertices[1][0]*z2 + vertices[2][0]*z3;

  z1 = vertices[3][1]-vertices[2][1];
  z2 = vertices[3][1]-vertices[1][1];
  z3 = vertices[2][1]-vertices[1][1];
  temp[8] = vertices[1][0]*z1 - vertices[2][0]*z2 + vertices[3][0]*z3;
  z2 = vertices[3][1]-vertices[0][1];
  z3 = vertices[2][1]-vertices[0][1];
  temp[9] = -vertices[0][0]*z1 + vertices[2][0]*z2 - vertices[3][0]*z3;
  z1 = vertices[3][1]-vertices[1][1];
  z3 = vertices[1][1]-vertices[0][1];
  temp[10] = vertices[0][0]*z1 - vertices[1][0]*z2 + vertices[3][0]*z3;
  z1 = vertices[2][1]-vertices[1][1];
  z2 = vertices[2][1]-vertices[0][1];
  temp[11] = -vertices[0][0]*z1 + vertices[1][0]*z2 - vertices[2][0]*z3;

  // z1 = volume,   z2 = temporary variable
  volume_tet(vertices[0], vertices[1], vertices[2], vertices[3], z1, z2);
  // finds the surface area of the tetrahedron, stored in z2
  area_tet (vertices[0], vertices[1], vertices[2], vertices[3], z2);
  // finds the maximum edge length in the tetrahedron, stored in z3
  max_length_tet (vertices[0], vertices[1], vertices[2], vertices[3], z3);

  z3 = z3*z2/(3.0*z1);   // element quality

  z3 = z3/( (1.0+nu)*(1.0-2.0*nu) );
  
   // set up material matrix, assume plane strain
  double* D = new double[3];
  D[0] = z3*(1.0-nu);
  D[1] = z3*nu;
  D[2] = z3*(0.5-nu);
  // Q holds the volume of tet

  /*  calc_B (vertices, B);

  // Ke = area*depth*BT*D*B
  // depth = 1
  // BT = transpose(B)
  // B = 1/Jacobian[ ]  where jacobian = 2*area

  calc_Ke (B, K, D);
   for (i=0; i<6; i++)    
    for (j=0; j<6; j++)    
      K[i][j] = K[i][j]/(4.0*area*area);  
  */
   delete [] D;   D = NULL;

  return;

}


void get_BTE (const double* temp, const double* D, vector< vector<double> >& BTE)
{
  int i;
 
  for (i=0; i<12; i+=4)
    {
      BTE[i][0] = temp[i]*D[0];  BTE[i][1] = temp[i]*D[1];  BTE[i][2] = temp[i]*D[1];  
      BTE[i][3] = temp[4]*D[2];  BTE[i][5] = temp[8]*D[2];  
    }
  BTE[0][0] = temp[0]*D[0];  BTE[0][1] = temp[0]*D[1];  BTE[0][2] = temp[0]*D[1];  
  BTE[0][3] = temp[4]*D[2];  BTE[0][5] = temp[8]*D[2];  

  BTE[1][0] = temp[4]*D[1];  BTE[1][1] = temp[4]*D[0];  BTE[1][2] = temp[4]*D[1];  
  BTE[1][3] = temp[0]*D[2];  BTE[1][4] = temp[8]*D[2];  

  BTE[2][0] = temp[8]*D[1];  BTE[2][1] = temp[8]*D[1];  BTE[2][2] = temp[8]*D[0];  
  BTE[2][4] = temp[4]*D[2];  BTE[2][5] = temp[0]*D[2];  

  BTE[3][0] = temp[1]*D[0];  BTE[3][1] = temp[1]*D[1];  BTE[3][2] = temp[1]*D[1];  
  BTE[3][3] = temp[5]*D[2];  BTE[3][5] = temp[9]*D[2];  

  BTE[4][0] = temp[5]*D[1];  BTE[4][1] = temp[5]*D[0];  BTE[4][2] = temp[5]*D[1];  
  BTE[4][3] = temp[1]*D[2];  BTE[4][4] = temp[9]*D[2];  

  BTE[5][0] = temp[9]*D[1];  BTE[5][1] = temp[9]*D[1];  BTE[5][2] = temp[9]*D[0];  
  BTE[5][4] = temp[5]*D[2];  BTE[5][5] = temp[1]*D[2];  

  BTE[6][0] = temp[2]*D[0];  BTE[6][1] = temp[2]*D[1];  BTE[6][2] = temp[2]*D[1];  
  BTE[6][3] = temp[6]*D[2];  BTE[6][5] = temp[10]*D[2];  

  BTE[7][0] = temp[6]*D[1];  BTE[7][1] = temp[6]*D[0];  BTE[7][2] = temp[6]*D[1];  
  BTE[7][3] = temp[2]*D[2];  BTE[7][4] = temp[10]*D[2];  

  BTE[8][0] = temp[10]*D[1];  BTE[8][1] = temp[10]*D[1];  BTE[8][2] = temp[10]*D[0];  
  BTE[8][4] = temp[6]*D[2];  BTE[8][5] = temp[2]*D[2];  

  BTE[9][0] = temp[3]*D[0];  BTE[9][1] = temp[3]*D[1];  BTE[9][2] = temp[3]*D[1];  
  BTE[9][3] = temp[7]*D[2];  BTE[9][5] = temp[11]*D[2];  

  BTE[10][0] = temp[7]*D[1];  BTE[10][1] = temp[7]*D[0];  BTE[10][2] = temp[7]*D[1];  
  BTE[10][3] = temp[3]*D[2];  BTE[10][4] = temp[11]*D[2];  

  BTE[11][0] = temp[11]*D[1];  BTE[11][1] = temp[11]*D[1];  BTE[11][2] = temp[11]*D[0];  
  BTE[11][4] = temp[7]*D[2];  BTE[11][5] = temp[3]*D[2];  

  return;
}
