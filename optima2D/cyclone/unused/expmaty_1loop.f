      subroutine expmaty(jdim,kdim,q,coef2y,coef4y,sndsp,s,
     >spect,xyj,press,ccy,vv,xy,x,y,work,s1,s2,u2,v2,phi2,h,temp,temp2)
c
      include '../include/arcom.inc'
c
      integer g,t
      dimension q(jdim,kdim,4),coef2y(jdim,kdim),coef4y(jdim,kdim)
      dimension sndsp(jdim,kdim),s(jdim,kdim,4),work(4,jdim,kdim,2)
      dimension spect(jdim,kdim,3),xyj(jdim,kdim),press(jdim,kdim)
      dimension ccy(jdim,kdim),vv(jdim,kdim),xy(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension temp(jdim,kdim,4),temp2(4,jdim,kdim)
      dimension s1(jdim,kdim),s2(jdim,kdim),u2(jdim,kdim),v2(jdim,kdim)
      dimension phi2(jdim,kdim),h(jdim,kdim),fluxjac(4,4)
c
      dtd=dt/(1. +phidt)
      do 11 i=1,4
         do 14 k=kbegin,kup
            kp1=k+1
            do 14 j=jlow,jup
               work(i,j,k,1)=q(j,kp1,i)*xyj(j,kp1) - q(j,k,i)*xyj(j,k)
 14      continue
      
         do 15 j=jlow,jup
            work(i,j,kend,1)=work(i,j,kup,1)
 15      continue
      
         do 18 k=klow,kup-1
           kp=k+1
           km=k-1
c          (q(j+2)-3q(j+1)+3q(j)-q(j-1))
           do 18 j=jlow,jup
           work(i,j,k,2)=work(i,j,kp,1)-2.*work(i,j,k,1)+work(i,j,km,1)
 18      continue
c       
c   boundary conditions a la Kyle
         do 20 j=jlow,jup
         work(i,j,kbegin,2)=
     &              q(j,kbegin+2,i)*xyj(j,kbegin+2)-2.*q(j,kbegin+1,i)
     &              *xyj(j,kbegin+1)+q(j,kbegin,i)*xyj(j,kbegin)
         work(i,j,kup,2)=0.0
 20      continue
 11   continue
c
c
c
c     note: no time metrics are used.
      do 100 k=kbegin,kup
         kp1=k+1
         do 100 j=jlow,jup
            rho= q(j,k,1)*xyj(j,k)
            rhop= q(j,kp1,1)*xyj(j,kp1)
            rq=1.d0/q(j,k,1)
            rqp=1.d0/q(j,kp1,1)
            rr = 1.d0/rho                                             
            rrp = 1.d0/rhop
            u = q(j,k,2)*rq                                                
            v = q(j,k,3)*rq                                                
            up = q(j,kp1,2)*rqp
            vp = q(j,kp1,3)*rqp  
            p  =gami*(q(j,k,4)*xyj(j,k) - 0.5d0*rho*(u**2+v**2))
            pp =gami*(q(j,kp1,4)*xyj(j,kp1) - 0.5d0*rhop*(up**2+vp**2))
c           
c           -calc Roe's average using primitive variables
c            (see pg 465 of Hirsh Vol.2)
            he = gamma*pp*rrp/gami + 0.5d0*(up**2+vp**2)
            hi = gamma*p*rr/gami + 0.5d0*(u**2+v**2)
c           
            sre    = dsqrt(rhop)
            sri    = dsqrt(rho)
            denom  = 1.d0/(sre + sri)
            rhoa   = sre*sri
            ua     = (up*sre + u*sri)*denom
            va     = (vp*sre + v*sri)*denom
            ha     = (he*sre + hi*sri)*denom
            ca2    = gami*(ha - 0.5d0*(ua**2 + va**2))
            pa     = rhoa*ca2/gamma
            ea     = pa/gami +  0.5d0*rhoa*(ua**2+va**2)
c            rhoa=.5d0*(rho+rhop)
c            ua  =.5d0*(u+up)
c            va  =.5d0*(v+vp)
c            ea  =.5d0*(q(j,k,4)*xyj(j,k)+q(j,kp1,4)*xyj(j,kp1))
c           
            r1 = (xy(j,k,3)+xy(j,kp1,3))*.5d0 
            r2 = (xy(j,k,4)+xy(j,kp1,4))*.5d0                               
            ut = ua*ua + va*va
            phi = gami*ut*.5d0                                               
            c2 = ea*gamma/rhoa                                         
            thet = ua*r1 + va*r2
c                  
            fluxjac(1,1) = 0.d0                                             
            fluxjac(2,1) = (-ua*ua +phi)*r1   -ua*va*r2                      
            fluxjac(3,1) = - ua*va*r1 + (-va*va +phi)*r2                    
            fluxjac(4,1) = ( -c2 + phi*2.d0)*thet                         
c
            fluxjac(1,2) = r1                                                
            fluxjac(2,2) = -(gamma-3.d0)*ua*r1 + va*r2                  
            fluxjac(3,2) = va*r1 - gami*ua*r2                             
            fluxjac(4,2) = ( c2 - phi)*r1 - gami*ua*thet                    
c
            fluxjac(1,3) = r2                                                
            fluxjac(2,3) = -gami*va*r1 + ua*r2                            
            fluxjac(3,3) = ua*r1 +(3.d0-gamma)*va*r2                        
            fluxjac(4,3) = ( c2 - phi)*r2 - gami*va*thet                    
c     
            fluxjac(1,4) = 0.d0                                             
            fluxjac(2,4) = gami*r1     
            fluxjac(3,4) = gami*r2     
            fluxjac(4,4) = gamma*thet
 100  continue
c
      do 38 k = kbegin,kup                                              
      do 38 j = jlow,jup                                                
         zjac=.5d0*(xyj(j,k)+xyj(j,k+1))
         do n=1,4
           sum=0.d0
           do i=1,4
             sum=sum + fluxjac(n,i)*(coef2y(j,k)*work(i,j,k,1) - 
     &                               coef4y(j,k)*work(i,j,k,2))
           enddo
           temp(j,k,n)=sum/zjac
         enddo
 38   continue                                                          
c
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
         s(j,k,n)=s(j,k,n) + dtd*(temp(j,k,n)-temp(j,k-1,n))
 200  continue
c
c       do 100 k=kbegin,kend
c       do 100 j=jlow,jup
c          s1(j,k)=.5d0*(spect(j,k,2)+spect(j,k,3)-2.*spect(j,k,1))
c          s2(j,k)=(spect(j,k,2)-spect(j,k,3))/
c     &                                    (2.*ccy(j,k))
cc     &                                    (2.*sndsp(j,k)*ccy(j,k))
c          u(j,k)=q(j,k,2)/q(j,k,1)
c          v(j,k)=q(j,k,3)/q(j,k,1)
c          phi(j,k)=.5d0*(u(j,k)**2+v(j,k)**2)         
c          h(j,k)=(q(j,k,4)+press(j,k))/q(j,k,1)
c 100   continue
c
c       do 888 g=0,1
c          do 111 k=kbegin,kup
c             t=k+g
c             do 111 j=jlow,jup
c                include 'matloopy.inc'
c 111      continue
cc
cc
c          do 222 n=1,4
c             do 140 k=klow,kup
c                t=k+g
c                do 140 j=jlow,jup
c                s(j,k,n)=s(j,k,n) + dtd*( 
c     &          coef2y(j,t)*temp(n,j,k)-coef2y(j,t-1)*temp(n,j,k-1)
c     &        - (coef4y(j,t)*temp2(n,j,k)-coef4y(j,t-1)*temp2(n,j,k-1)))
c            
c 140         continue
c 222      continue
c 888  continue
c     
      return
      end






