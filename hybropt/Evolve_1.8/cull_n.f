C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C                   CCCC  U    U  L      L         N    N              C
C                  C      U    U  L      L         NN   N              C
C                  C      U    U  L      L         N N  N              C
C                  C      U    U  L      L         N  N N              C
C                   CCCC   UUUU   LLLLL  LLLLL_____N   NN              C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE CULL_N(ndim,NDUM,NACC,GENES,FITACC,I_ACC,NGENE,NOBFUN,
     1               ICULL,I_MINMAX)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** THIS SUBROUTINE PERFORMS CULLING OF THE                 C
C              ACCUMULATION FILE USING AN N-DIMENSIONAL                C
C              BINNING ALGROITHM. IT IS IMPLEMENTED WHEN               C
C              IACCUM=1 (ACCUMULATION FILE IS ACTIVE), NOBFUN>1        C
C              (THERE ARE AT LEAST TWO OBJECTIVES), ICULL<0 AND        C
C              NACC.GE.IABS(ICULL) (THE CURRENT NUMBER OF              C
C              ELEMENTS IN THE ACCUMULATION FILE EXCEEDS THE           C
C              USER-SPECIFIED ICULL LIMIT). THIS ALGRORITHM WILL       C
C              BE IMPLEMENTED WITH ENDPOINT RETENTION IF NDUM IS       C
C              POSITIVE AND WITHOUT ENPOINT RETENTION IF NDUM IS       C
C              NEGATIVE ***                                            C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C          *** FORMAL PARAMETER DEFINITIONS ***
C
C       NDUM...IABS(NDUM) IS THE NUMBER OF INTERVALS THAT EACH 
C              OBJECTIVE DIRECTION IS DIVIDED INTO. WHEN NDUM IS 
C              POSITIVE, THE ACCUMULATION FILE ENDPOINTS ARE 
C              SELECTED FIRST (ENDPOINT RETENTION OPTION). WHEN
C              NDUM IS NEGATIVE THE ENDPOINT RETENTION OPTION IS 
C              NOT USED.
C       NACC...NUMBER OF ELMENTS IN THE CURRENT ACCUMULATION FILE 
C              (PARETO FRONT)
C     NOBFUN...NUMBER OF OBJECTIVE FUNCTIONS
C  FITACC(,)...DOUBLY-SUBSCRIPTED ARRAY CONTAINING NOBFUN FITNESS
C              VALUES FOR EACH ELEMENT IN THE ACCUMULATION FILE
C      NGENE...NUMBER OF GENES ASSOCIATED WITH EACH CHROMOSOME
C   GENES(,)...DOUBLY-SBSCRIPTED ARRAY CONTAINING NGENE GENES
C              FOR EACH ELEMENT IN THE ACCUMULATION FILE
C   ICULL......INTEGER PARAMETER THAT CONTROLS THE CULLING 
C              ALGORITHM. THE CULLING ALGORITHM LOGIC DISCARDS 
C              ENTRIES IN THE ACCUMULATION FILE WHEN THE THE 
C              NUMBER OF ENTRIS IN THE ACCUMULATION FILE EXCEEDS
C              ICULL. THIS DISCARDING LOGIC ATTEMPTS TO FORCE 
C              EQUAL SPACING ALONG THE PARETO FRONT AND ONLY
C              MAKES SENSE WHEN NOBFUN>1 AND IACCUM=1.
C              ICULL=0...CULLING LOGIC IS TURNED OFF.
C              ICULL=N...CULLING LOGIC IS EXECUTED FOR EVERY
C                        GENERATION IN WHICH THE NUMBER OF 
C                        ELEMENTS IN THE ACCUMULATION FILE
C                        EXCEEDS N. THIS OPTION UTILIZES AN
C                        ARC-LENGTH ALGORITHM AND IS ONLY VALID
C                        WHEN NOBFUN=2.
C              ICULL=-N..CULLING LOGIC IS EXECUTED FOR EVERY
C                        GENERATION IN WHICH THE NUMBER OF 
C                        ELEMENTS IN THE ACCUMULATION FILE
C                        EXCEEDS N. THIS OPTION UTILIZES A
C                        BINNING ALGROITHM AND IS VALID FOR
C                        ALL VALUES OF NOBFUN (NOBFUN>1).
C   I_MINMAX...INTEGER PARAMETER THAT CONTROLS WHETHER THE 
C              OPTIMIZATION IS MAXIMIZATION OR MINIMIZATION
C              I_MINMAX=1...MAXIMIZATION
C              I_MINMAX=0...MINIMIZATION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C
      DIMENSION         FITACC(nobfun,ndim)
      DIMENSION         GENES(nacc,ndim)
      dimension         I_ACC(2,ndim)
      DIMENSION         FITMAX(nobfun),           FITMIN(nobfun)
      DIMENSION         IBIN(ndim)
      DIMENSION         IB(nobfun)
      DIMENSION         NBIN_LIST(1000),      NBIN_NUM(1000),
     1                  NFIL(1000)
      DIMENSION         NAMAX(nobfun),            NAMIN(nobfun)
C
      DIMENSION         ZZZ(nacc,ndim),     i_zzzacc(2,ndim)
      DIMENSION         ZZZFIT(nobfun,ndim)
      real*8 rand
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** SET INITIAL PARAMETERS ***
C
      NINT = ABS(NDUM)
      IDUM = 1
C
C-----------------------------------------------------------------------
C          *** COMPUTE MAX AND MIN VALUES IN THE ACCUMULATION FILE
C              FOR EACH OBJECTIVE-SPACE DIMENSION ***
C
      DO NO=1,NOBFUN
         FITMAX(NO) = -1.0E30
         FITMIN(NO) =  1.0E30
      ENDDO
      DO NA=1,NACC
         DO NO=1,NOBFUN
            IF (FITACC(NO,NA).GT.FITMAX(NO)) THEN
               FITMAX(NO) = FITACC(NO,NA)
               NAMAX(NO)  = NA
            ENDIF
            IF (FITACC(NO,NA).LT.FITMIN(NO)) THEN
               FITMIN(NO) = FITACC(NO,NA)
               NAMIN(NO)  = NA
            ENDIF
         ENDDO
      ENDDO
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      DO NO=1,NOBFUN
C      WRITE (34,531) NO,FITMAX(NO),FITMIN(NO),NAMAX(NO),NAMIN(NO)
C  531 FORMAT ('CULL_N: NO,FITMAX,FITMIN,NAMAX,NAMIN =',I6,2F22.14,2I5)
C      ENDDO
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** PERFORM SELECTION ***
C              IBIN()...ARRAY CONTAINING A BIN NUMBER FOR EACH ENTRY
C                       IN THE ACCUMULATION FILE. THERE ARE NACC
C                       ELEMENTS IN IBIN.
C              NBIN.....NUMBER OF ACTIVE BINS, I.E., NUMBER OF BINS
C                       THAT CONTAIN AT LEAST ONE ELEMENT
C              NBIN_LIST()..ARRAY CONTAINING THE LIST OF ALL UNIQUE 
C                       BIN NUMBERS. THERE ARE NBIN ELEMENTS IN 
C                       NBIN_LIST.
C              NFIL()...COUNTER CONTAINING CURRENT NUMBER OF ELEMENTS
C                       THAT HAVE BEEN SELECTED FROM EACH ACTIVE
C                       BIN. THERE ARE NBIN ELEMENTS IN NFIL.
C              NINT.....NUMBER OF INTERVALS THAT EACH OBJECTIVE
C                       DIMENSION IS DIVIDED INTO
C              NMAX.....MAXIMUM NUMBER OF ELEMENTS TO BE TAKEN FROM 
C                       EACH BIN (CAN BE EXCEEDED BY ONE)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** COMPUTE BIN LOCATION FOR EACH ENTRY IN THE
C              ACCUMULATION FILE (PARETO FRONT) ***
C
      NBIN = 0
      DO NBI=1,1000
         NBIN_NUM(NBI) = 0
      ENDDO
      DO NC=1,NACC
         DO NO=1,NOBFUN
            IB(NO) = INT((FITACC(NO,NC)-FITMIN(NO))/
     1                    (FITMAX(NO)-FITMIN(NO))*FLOAT(NINT))+1
            IF (IB(NO).GT.NINT) IB(NO) = NINT
         ENDDO
         IBIN(NC) = IB(1)
         DO NO=2,NOBFUN
            IBIN(NC) = IBIN(NC)+(IB(NO)-1)*NINT**(NO-1)
         ENDDO
         IF (NBIN.EQ.0) THEN
            NBIN            = NBIN+1
            NBIN_LIST(NBIN) = IBIN(NC)
            NBIN_NUM(NBIN)  = NBIN_NUM(NBIN)+1
            GO TO 1000
         ENDIF
         DO NBI=1,NBIN
            IF (IBIN(NC).EQ.NBIN_LIST(NBI)) THEN
               NBIN_NUM(NBI) = NBIN_NUM(NBI)+1
               GO TO 1000
            ENDIF
         ENDDO
         NBIN            = NBIN+1
         NBIN_LIST(NBIN) = IBIN(NC)
         NBIN_NUM(NBIN)  = NBIN_NUM(NBIN)+1
 1000    CONTINUE
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      DO NBI=1,NBIN
C      WRITE (34,532) NC,NBI,NBIN,IBIN(NC),NBIN_LIST(NBI),NBIN_NUM(NBI)
C  532 FORMAT ('CULL_N: NC,NBI,NBIN,IBIN,NBIN_LIST,NBIN_NUM =',
C     1        3I4,4X,I4,4X,2I4)
C      ENDDO
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ENDDO
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      DO NC=1,NACC
C      WRITE (34,330) NC,IBIN(NC),(FITACC(NO,NC),NO=1,NOBFUN)
C  330 FORMAT ('CULL_N:NC,IBIN,FITACC=',2I5,3X,10F12.6)
C      ENDDO
C      DO NBI=1,NBIN
C      WRITE (34,332) NBI,NBIN_LIST(NBI),NBIN_NUM(NBI)
C  332 FORMAT ('CULL_N: NBI,NBIN_LIST,NBIN_NUM =',3I6)
C      ENDDO
C      NSUM = 0
C      DO NBI=1,NBIN
C      NSUM = NSUM+NBIN_NUM(NBI)
C      ENDDO
C      WRITE (34,331) NACC,NBIN,NSUM
C  331 FORMAT ('CULL_N: NACC,NBIN,NSUM=',3I5)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C          *** SELECT THE NEW CHROMOSOMES SO THAT EACH BIN
C              HAS EQUAL REPRESENTATION. IF NDUM IS POSITIVE
C              USE ACCUMULCATION FILE ENDPOINT RETENTION. THIS
C              PART OF LOGIC SELECTS NMAX ELEMENTS FROM EACH
C              ACTIVE BIN ***
C
c$$$C      NMAX = NCHROM/NBIN
C      NMAX = NACC/NBIN
C      NMAX = INT(0.9*FLOAT(IABS(ICULL)))/NBIN
      NMAX = IABS(ICULL)/NBIN
      NSUM = 0
      DO NBI=1,NBIN
         NADD = NMAX
         IF (NBIN_NUM(NBI).LT.NMAX) NADD = NBIN_NUM(NBI)
         NSUM = NSUM+NADD
      ENDDO
C
      DO NBI=1,NBIN
         NFIL(NBI) = 0
      ENDDO
      DO NC=1,NSUM
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (34,431) NC,NMAX
C  431 FORMAT ('CULL_N: NC,NMAX =',2I6)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 3000    CONTINUE
         IF (NDUM.GT.0.AND.NC.LE.NOBFUN) THEN
            IF (I_MINMAX.EQ.1)  THEN 
               IR = NAMAX(NC)
            ELSE
               IR = NAMIN(NC)
            ENDIF
         ELSE
            CALL RAN3(IDUM,RAND)
            IR = INT(RAND*FLOAT(NACC))+1
         ENDIF
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (34,432) NC,IR,(NFIL(NBI),NBI=1,NBIN)
C  432 FORMAT ('CULL_N:NC,IR,NFIL =',2I4,4X,40I2)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         IF (IBIN(IR).NE.0) THEN
            DO NBI=1,NBIN
               IF (IBIN(IR).EQ.NBIN_LIST(NBI)) THEN
                  IF (NFIL(NBI).LT.NMAX.AND.
     1                NFIL(NBI).LT.NBIN_NUM(NBI)) THEN
                     NFIL(NBI) = NFIL(NBI)+1
                     DO NG=1,NGENE
                        ZZZ(NG,NC) = GENES(NG,IR)
                     ENDDO
                     DO NO=1,NOBFUN
                        ZZZFIT(NO,NC) = FITACC(NO,IR)
                     ENDDO
                     i_zzzacc(1,nc) = I_ACC(1,ir)
                     i_zzzacc(2,nc) = I_ACC(2,ir)
                     GO TO 2000
                  ELSE
                     GO TO 3000
                  ENDIF
               ENDIF
            ENDDO
            IBIN(IR) = 0
         ELSE
            GO TO 3000
         ENDIF
 2000    CONTINUE
      ENDDO
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C           *** MOVE SELECTED GENE AND FITNESS VALUES TO GENES 
C               AND FITACC ARRAYS ***
C
      DO NC=1,NSUM
         DO NG=1,NGENE
            GENES(NG,NC) = ZZZ(NG,NC)
         ENDDO
         DO NO=1,NOBFUN
            FITACC(NO,NC) = ZZZFIT(NO,NC)
         ENDDO
         i_acc(1,nc) = I_ZZZACC(1,ir)
         i_acc(2,nc) = I_ZZZACC(2,ir)
      ENDDO
      NACC = NSUM
C
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      DO NC=1,NACC
C      WRITE (34,430) NC,(FITACC(NO,NC),NO=1,NOBFUN)
C  430 FORMAT ('CULL_N:NC,FITACC=',I5,10F12.6)
C      ENDDO
C      WRITE (34,431) NACC,NBIN
C  431 FORMAT ('CULL_N: NACC,NBIN=',2I5)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C           *** SELECTION COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END


