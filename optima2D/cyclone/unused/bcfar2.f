      subroutine bcfar2(jdim,kdim,q,press,xy,xit,ett,xyj,x,y,j1,j2,
     &                  kbc,idir)              
c
      include '../include/arcom.inc'
c                                                                       
      dimension duqinf(4),duqext(4),duqbar(4),duqdif(4),duqinfp(4)
      dimension duqextp(4)
      dimension q(jdim,kdim,4),press(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c
      dimension vinff(4),vint(4),vinfp(4),vintp(4),qbar(4),qdif(4)
      dimension vec(4),xx(4,4),tmp(4)
c     -far field bc for o mesh outer boundary and c mesh k = kmax          
c                                                                       
c   .....................................................................  
c     Far field circulation based on potential vortex added to reduce      
c     the dependency on outer boundary location.  works well.  for         
c     instance, a naca0012 at m=0.63 a=2. shows no dependency on ob from   
c     4 - 96 chords.                                                       
c   .....................................................................  
c                                                                       
      gi = 1.d0/gamma                                                 
      gm1i = 1.d0/gami                                                
c                                                                    
      alphar = alpha*pi/180.d0
      cosang = cos( alphar )                                         
      sinang = sin( alphar )                                         
      ainf = dsqrt(gamma*pinf/rhoinf)                                 
      hstfs = 1.d0/gami + 0.5d0*fsmach**2                                
c                                                                       
c                                                                       
      k = kbc
      kq= kbc
c     for Vn to point outward at farfield, sgn should be +1
c      sgn=1.d0
      sgn=-sign(1.d0,float(idir))
      kadd = sign(1,idir)
c
      if (prec.eq.0) then
         do 60 j = j1,j2                                                
c                                                                          
c           -reset free stream values with circulation correction                
c           -this if statement is here so that when I test for uniform
c            flow convergence I don't get a floating point exception when
c            using this routine for the body too.
c           -circul should be set to false when testing uniform flow.
            if (circul) then
               xa = x(j,k) - chord/4.d0
               ya = y(j,k)                                                    
               radius = dsqrt(xa**2+ya**2)                                     
               angl = atan2(ya,xa)                                            
               cjam = cos(angl)                                               
               sjam = sin(angl)                                               
               qcirc =circb/(radius*(1.d0-(fsmach*sin(angl-alphar))**2))    
            else
               qcirc=0.d0
               sjam=0.d0
               cjam=0.d0
            endif
            uf = uinf + qcirc*sjam                                         
            vf = vinf - qcirc*cjam                                         
            af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))                         
            af = dsqrt(af2)        
c                                                                          
c           -metrics for normals
            rx=xy(j,k,3)
            ry=xy(j,k,4)
            rxy2=1.d0/dsqrt(rx**2+ry**2)
c            print *,'rxy2=',j,rxy2
            rx=rx*rxy2*sgn
            ry=ry*rxy2*sgn
c
c           -get extrapolated variables                                
            if (iord.eq.4) then
               kq=k+kadd
               xyj2 = xyj(j,kq)                                             
               rho2 = q(j,kq,1)*xyj2                                     
               rinv = 1.d0/q(j,kq,1)                                          
               u2 = q(j,kq,2)*rinv                                        
               v2 = q(j,kq,3)*rinv
               e2 = q(j,kq,4)*xyj2
               p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c
               kq=k+2*kadd
               xyj3 = xyj(j,kq)                                             
               rho3 = q(j,kq,1)*xyj3
               rinv = 1.d0/q(j,kq,1)                                          
               u3 = q(j,kq,2)*rinv                                        
               v3 = q(j,kq,3)*rinv                                       
               e3 = q(j,kq,4)*xyj3
               p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c              
               kq=k+3*kadd
               xyj4 = xyj(j,kq)                                             
               rho4 = q(j,kq,1)*xyj4
               rinv = 1.d0/q(j,kq,1)                                          
               u4 = q(j,kq,2)*rinv                                        
               v4 = q(j,kq,3)*rinv                                       
               e4 = q(j,kq,4)*xyj4
               p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c              
c               kq=k+4*kadd
c               xyj5 = xyj(j,kq)                                             
c               rho5 = q(j,kq,1)*xyj5
c               rinv = 1.d0/q(j,kq,1)                                          
c               u5 = q(j,kq,2)*rinv                                        
c               v5 = q(j,kq,3)*rinv                                       
c               e5 = q(j,kq,4)*xyj5
c               p5 = gami*(e5 - 0.5d0*rho5*(u5**2+v5**2))
c              
c              -3rd order extrapolation
c               rhoext= 4.d0*(rho2+rho4) - 6.d0*rho3 -rho5
c               uext= 4.d0*(u2+u4) -6.d0*u3 -u5
c               vext= 4.d0*(v2+v4) -6.d0*v3 -v5
c               pext= 4.d0*(p2+p4) -6.d0*p3 -p5
c              -2nd order extrapolation
               rhoext= 3.d0*(rho2 - rho3) + rho4
               uext= 3.d0*(u2 - u3) + u4
               vext= 3.d0*(v2 - v3) + v4
               pext= 3.d0*(p2 - p3) + p4
c              -1st order extrapolation
c               rhoext= 2.d0*rho2 - rho3
c               uext= 2.d0*u2 - u3
c               vext= 2.d0*v2 - v3
c               pext= 2.d0*p2 - p3
c              -Zeroeth order extrapolation
c               rhoext= rho2
c               uext= u2
c               vext= v2
c               pext= p2
            else
               kq=k+kadd
               xyj2 = xyj(j,kq)                                             
               rho2 = q(j,kq,1)*xyj2                                     
               rinv = 1.d0/q(j,kq,1)                                          
               u2 = q(j,kq,2)*rinv                                        
               v2 = q(j,kq,3)*rinv
               e2 = q(j,kq,4)*xyj2
               p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c
               kq=k+2*kadd
               xyj3 = xyj(j,kq)                                             
               rho3 = q(j,kq,1)*xyj3
               rinv = 1.d0/q(j,kq,1)                                          
               u3 = q(j,kq,2)*rinv                                        
               v3 = q(j,kq,3)*rinv                                       
               e3 = q(j,kq,4)*xyj3
               p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c
c              -1st order extrapolation
c               rhoext= 2.d0*rho2 - rho3
c               uext= 2.d0*u2 - u3
c               vext= 2.d0*v2 - v3
c               pext= 2.d0*p2 - p3
c              -Zeroth-order extrapolation
               rhoext= rho2
               uext= u2
               vext= v2
               pext= p2
            endif
            entext=pext/rhoext**gamma
            hext= gamma*pext/rhoext/gami + 0.5d0*(uext**2+vext**2)
            a2ext= gamma*pext/rhoext
c                                                                          
c           -calc conserved and primitive variables for both
c            infinity and interior values
c
c           -conserved variables at inf
c           -note that entinf=1/gamma so  rhoinf=(ainf**2/(gamma*entinf))**gm1i
c                                               =(ainf**2)**gm1i
            vinff(1)= af2**gm1i
            vinff(2)= vinff(1)*uf
            vinff(3)= vinff(1)*vf
            pf     = vinff(1)*af2*gi
            vinff(4)= pf*gm1i +0.5d0*(vinff(2)**2+vinff(3)**2)/vinff(1)
c
c           -primitive variables at inf
            vinfp(1)= vinff(1)
            vinfp(2)= uf
            vinfp(3)= vf
            vinfp(4)= pf
c
c           -conserved variables extrapolated from interior
c           -formula used for vint(1) here is equivalent to rhoext
c            vint(1)= (a2ext/(gamma*entext))**gm1i
c            write(6,*) 'numiter,j',numiter,j
c            write(6,*) rhoext,vint(1),dabs(rhoext-vint(1))
            vint(1)= rhoext
            vint(2)= vint(1)*uext
            vint(3)= vint(1)*vext
            vint(4)= pext*gm1i + 0.5d0*(vint(2)**2+vint(3)**2)/vint(1)
c
c           -primitve variables extrapolated from interior
            vintp(1)= vint(1)
            vintp(2)= uext
            vintp(3)= vext
            vintp(4)= pext
c
c
c                                             _ _
c           u_b = 0.5*(uinf+uext) - 0.5*( sgn(A(n))*(uinf-uext) )
c                    _ _                               
c           with sgn(A(n)) = X sgn(eigenvalues of A) X^(-1)
c                                _ 
c           X the eigenvectors,  n the normal (normalized),
c           _
c           A = n A + n B at Roe state
c                x     y
c
c
c           -form simple average (conservative variables)
            qbar(1)= 0.5d0*(vinff(1) + vint(1))
            qbar(2)= 0.5d0*(vinff(2) + vint(2))
            qbar(3)= 0.5d0*(vinff(3) + vint(3))
            qbar(4)= 0.5d0*(vinff(4) + vint(4))
c
c           -form difference (primitive variables)
            qdif(1)= (vinfp(1) - vintp(1))
            qdif(2)= (vinfp(2) - vintp(2))
            qdif(3)= (vinfp(3) - vintp(3))
            qdif(4)= (vinfp(4) - vintp(4))
c            qdif(1)= 0.5d0*(vinfp(1) - vintp(1))
c            qdif(2)= 0.5d0*(vinfp(2) - vintp(2))
c            qdif(3)= 0.5d0*(vinfp(3) - vintp(3))
c            qdif(4)= 0.5d0*(vinfp(4) - vintp(4))
c
c           -calc Roe's average using primitive variables
c            (see pg 465 of Hirsh Vol.2)
            he = gamma*vinfp(4)/gami/vinfp(1) + 
     &                                   0.5d0*(vinfp(2)**2+vinfp(3)**2)
            hi = gamma*vintp(4)/gami/vintp(1) + 
     &                                   0.5d0*(vintp(2)**2+vintp(3)**2)
c
            sre    = dsqrt(vinfp(1))
            sri    = dsqrt(vintp(1))
            denom  = sre + sri
            rhoa   = sre*sri
            ua     = (vinfp(2)*sre + vintp(2)*sri)/denom
            va     = (vinfp(3)*sre + vintp(3)*sri)/denom
            ha     = (he*sre + hi*sri)/denom
            ca     = dsqrt( gami*(ha - 0.5d0*(ua**2 + va**2)) )
c
c
c           -see Hirsh pg 180 Vol.2 for L^{-1} and X
            vcap=xy(j,k,3)*ua + xy(j,k,4)*va
c            vcap=rx*ua + ry*va
            denom=1.d0/ca
            denom2=denom**2
            t3= 0.5d0*rhoa*(rx*qdif(2)+ry*qdif(3))*denom
            t4= qdif(4)*denom2
            vec(1) = qdif(1) - t4
            vec(2) = -rhoa*(rx*qdif(3) - ry*qdif(2))
            vec(3) = 0.5d0*t4+t3
            vec(4) = 0.5d0*t4-t3
c
c            write(99,999) numiter,j,vcap,ca,vcap+ca,vcap-ca
c 999        format (2I6,4e15.4)
            vec(1) = sign(1.d0,vcap)*vec(1)
            vec(2) = sign(1.d0,vcap)*vec(2)
            vec(3) = sign(1.d0,vcap+ca)*vec(3)
            vec(4) = sign(1.d0,vcap-ca)*vec(4)
c
c           -multiply be right eigenvectors
            vtot2 = .5d0*(ua**2+va**2)
            xx(1,1)= 1.d0
            xx(1,2)= 0.d0
            xx(1,3)= 1.d0
            xx(1,4)= 1.d0
            xx(2,1)= ua
            xx(2,2)= ry
            xx(2,3)= ua+rx*ca
            xx(2,4)= ua-rx*ca
            xx(3,1)= va
            xx(3,2)= -rx
            xx(3,3)= va+ry*ca
            xx(3,4)= va-ry*ca
            xx(4,1)= vtot2
            xx(4,2)= ry*ua - rx*va
            xx(4,3)= ha + vcap*ca
            xx(4,4)= ha - vcap*ca
c
            do 25 n=1,4
               tmp(n)=0.d0
               do 20 i=1,4
                  tmp(n)=tmp(n)+xx(n,i)*vec(i)
 20            continue
               tmp(n)=qbar(n)-0.5d0*tmp(n)
 25         continue
c
c           -add jacobian                                             
            rjj = 1.d0/xyj(j,k)                                             
            q(j,k,1) = tmp(1)*rjj
            q(j,k,2) = tmp(2)*rjj                             
            q(j,k,3) = tmp(3)*rjj
            q(j,k,4) = tmp(4)*rjj
60       continue                                                          
      else
c
         if (prec.ne.3) then
            print *,'BCFAR only written for prec=3'
            stop
         endif
c
c
cdu      -use the preconditioned characteristics to solve the bc's
c
         dulimit=prphi*fsmach**2
c
         do 1000 j=j1,j2
c     
c          -do circulation correction
            xa = x(j,k) - chord/4.                                         
            ya = y(j,k)                                                    
            radius = dsqrt(xa**2+ya**2)                                     
            angl = atan2(ya,xa)                                            
            cjam = cos(angl)                                               
            sjam = sin(angl)                                               
            qcirc = circb/(radius* (1.d0-(fsmach*SIN(ANGL-ALPHAR))**2))    
            uf = uinf + qcirc*sjam                                         
            vf = vinf - qcirc*cjam                                         
            af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))                         
            af = dsqrt(af2)                                                 
c
c          -calculate values for duqinf
            duqinf(1)=af2**gm1i
            duqinf(2)=duqinf(1)*uf
            duqinf(3)=duqinf(1)*vf
            dupinf=duqinf(1)*af2*gi
            duqinf(4)=dupinf*gm1i+0.5d0*(uf**2+vf**2)*duqinf(1)
c
c       -calculate values for duqext (no Jacobian in these)
c         if ((j.lt.(j2-3)).and.(j.gt.(j1+3))) then
c            duqext(1)=2.0*q(j,kq-1,1)*xyj(j,k-1)-q(j,kq-2,1)*xyj(j,k-2)
c            duqext(2)=2.0*q(j,kq-1,2)*xyj(j,k-1)-q(j,kq-2,2)*xyj(j,k-2)
c            duqext(3)=2.0*q(j,kq-1,3)*xyj(j,k-1)-q(j,kq-2,3)*xyj(j,k-2)
c            duqext(4)=2.0*q(j,kq-1,4)*xyj(j,k-1)-q(j,kq-2,4)*xyj(j,k-2)
c         else
            duqext(1)=q(j,kq-1,1)*xyj(j,k-1)
            duqext(2)=q(j,kq-1,2)*xyj(j,k-1)
            duqext(3)=q(j,kq-1,3)*xyj(j,k-1)
            duqext(4)=q(j,kq-1,4)*xyj(j,k-1)
c         endif
c
csdc           modification by Stan: 16/01/98
csdc           primitive variable at infinity.
csd            duqinfp(1)=duqinf(1)
csd            duqinfp(2)=uf
csd            duqinfp(3)=vf
csd            duqinfp(4)=dupinf
csdc
csdc           primitive variables extrapolated from interior.
csd            duqextp(1)=duqext(1)
csd            duqextp(2)=duqext(2)/duqext(1)
csd            duqextp(3)=duqext(3)/duqext(1)
csd            duqextp(4)=gami*(duqext(4)-.5d0*duqext(1)*
csd     &                          (duqextp(2)**2+duqextp(3)**2))
csdc           end of modification by Stan.
c
c          -form simple average
            duqbar(1)=0.5d0*(duqinf(1)+duqext(1))
            duqbar(2)=0.5d0*(duqinf(2)+duqext(2))
            duqbar(3)=0.5d0*(duqinf(3)+duqext(3))
            duqbar(4)=0.5d0*(duqinf(4)+duqext(4))
c
c          -form difference
            duqdif(1)=0.5d0*(duqinf(1)-duqext(1))
            duqdif(2)=0.5d0*(duqinf(2)-duqext(2))
            duqdif(3)=0.5d0*(duqinf(3)-duqext(3))
            duqdif(4)=0.5d0*(duqinf(4)-duqext(4))
c
csdc           Stan: 16/01/98
csd                  I found no difference in drag results when
csd                  using Roe's average or a simple average of the
csd                  properties at the mean state.  Hence Dave's
csd                  version remains.
csd
csdc           -calc Roe's average using primitive variables
csdc            (see pg 465 of Hirsh Vol.2)
csd            he = gamma*duqinfp(4)/gami/duqinfp(1) + 
csd     &                               0.5d0*(duqinfp(2)**2+duqinfp(3)**2)
csd            hi = gamma*duqextp(4)/gami/duqextp(1) + 
csd     &                               0.5d0*(duqextp(2)**2+duqextp(3)**2)
csdc
csd            sre    = dsqrt(duqinfp(1))
csd            sri    = dsqrt(duqextp(1))
csd            denom  = sre + sri
csd            rhoa   = sre*sri
csd            ua     = (duqinfp(2)*sre + duqextp(2)*sri)/denom
csd            va     = (duqinfp(3)*sre + duqextp(3)*sri)/denom
csd            ha     = (he*sre + hi*sri)/denom
csd            ca     = dsqrt( gami*(ha - 0.5d0*(ua**2 + va**2)) )
csdc
csd            durho=rhoa
csd            duu=ua
csd            duv=va
csd            duuv2=duu**2+duv**2
csd            duc2=ca**2
csd            duc=ca
csd            duma2=duuv2/duc2
c
c          -calculate properties at mean state
            durho=duqbar(1)
            duu=duqbar(2)/durho
            duv=duqbar(3)/durho
            duuv2=duu**2+duv**2
            dup=gami*(duqbar(4)-0.5d0*(duuv2)*durho)
            duc2=gamma*dup/durho
            duc=dsqrt(duc2)
            duma2=duuv2/duc2
c
            duw=xy(j,k,1)**2+xy(j,k,2)**2
            dul=xy(j,k,3)**2+xy(j,k,4)**2
c
            dulim=max(dulimit,prxi*rhoinf*dsqrt(max(duw,dul))
     +           /re/durho/fsmach)
            due=min(1.d0,max(duma2,dulim))
c            due=1.d0
c
            dukx=xy(j,k,3)
            duky=xy(j,k,4)
            duuk=dukx*duu+duky*duv
c
            dua=(1.d0+due)/2.0*duuk
            dub=dsqrt(((1.d0-due)*duuk/2.0)**2+due*dul*duc2)
c
            dul=dsqrt(dul)
            dukx=dukx/dul
            duky=duky/dul
c
c          -multiply by Minv
            du4=gami*(duuv2/2.0*duqdif(1)-duu*duqdif(2)-
     +           duv*duqdif(3)+duqdif(4))
            du1=du4/durho/duc
            du4=du4-duc2*duqdif(1)
            du2=(duqdif(2)-duu*duqdif(1))/durho
            du3=(duqdif(3)-duv*duqdif(1))/durho
c
c          -multiply by Tkinv
            dut1=du4
            dut2=duky*du2-dukx*du3
            dut3=(dul*duc*du1+(duuk-dua+dub)*
     +           (dukx*du2+duky*du3))/dub/2.d0
            dut4=(dul*duc*du1+(duuk-dua-dub)*
     +           (dukx*du2+duky*du3))/dub/2.d0
c
c          -multiply by sign(Lambda)
            du1=sign(1.d0,duuk)*dut1
            du2=sign(1.d0,duuk)*dut2
            du3=sign(1.d0,dua+dub)*dut3
            du4=sign(1.d0,dua-dub)*dut4
c
c          -multiply by Tk
            dut1=((dua+dub-duuk)*du3+(duuk-dua+dub)*du4)/dul/duc
            dut2=duky*du2+dukx*(du3-du4)
            dut3=-dukx*du2+duky*(du3-du4)
            dut4=du1
c
c          -multiply by M
            du1=(durho*dut1-dut4/duc)/duc
            du2=duu*du1+durho*dut2
            du3=duv*du1+durho*dut3
            du4=durho*((duuv2/2.d0/duc+duc/gami)*dut1+
     +           duu*dut2+duv*dut3)-duuv2/2.d0/duc2*dut4
c
c          -calculate the boundry Q
            dut1=duqbar(1)-du1
            dut2=duqbar(2)-du2
            dut3=duqbar(3)-du3
            dut4=duqbar(4)-du4
c
c          -apply
            q(j,kq,1)=dut1/xyj(j,k)
            q(j,kq,2)=dut2/xyj(j,k)
            q(j,kq,3)=dut3/xyj(j,k)
            q(j,kq,4)=dut4/xyj(j,k)
 1000    continue
      endif
c
65    continue                                                          
      return                                                            
      end                                                               
