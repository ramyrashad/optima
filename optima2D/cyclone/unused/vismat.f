      subroutine vismat(klin,jdim,kdim,q,press,turmu,fmu,xy,xyj,a,b,c)  
c     parameter (maxj=497)                                              
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),turmu(jdim,kdim)        
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),fmu(jdim,kdim)           
c                                                                       
      dimension a(jdim,4,4), b(jdim,4,4), c(jdim,4,4)                   
c                                                                       
      common/worksp/rr(maxj,3),u(maxj,3),v(maxj,3),                     
     * c1(maxj,3),c2(maxj,3),c3(maxj,3),c4(maxj,3),tc(maxj,3),          
     * dkr(maxj, 2,4,4), dk(maxj, 2,4,4), work(maxj,4)                  
c                                                                       
c                                                                       
      data pr,prtr,frt/.72,.8,1.333333/                                 
c                                                                       
c                                                                       
c     the results of this routine require a stencil of 3 points in      
c     the k direction, however vectorization requires that flmaty2      
c     ( which calls this routine ) get a j direction line per call.     
c     this means that there is triple computation of the quantities     
c     in the first two loops, and double computation in the third.      
c     this recomputation can be removed by more coding.   - jtb 4/15/83 
c                                                                       
      hd = dt * .5 * thetadt/(1. + phidt)                               
      hre = hd/re                                                       
      gpr = gamma/pr                                                    
c                                                                       
c    sutherland equation                                                
c                                                                       
      do 5 k = klin - 1, klin + 1                                       
         kcnt = 2 + k - klin                                            
         do 5 j = jlow, jup                                             
c                                                                       
c                                                                       
5     continue                                                          
c                                                                       
      do 10 k = klin - 1, klin + 1                                      
         kcnt = 2 + k - klin                                            
         do 10 j = jlow, jup                                            
c                                                                       
            r1 = 1./q(j,k,1)                                            
            exs = xy(j,k,3)**2                                          
            eys = xy(j,k,4)**2                                          
            exy = xy(j,k,3)*xy(j,k,4)*.3333333                          
            turm = turmu(j,k)                                           
            vnu = fmu( j, k) + turm                                     
            gkap = fmu( j, k) + prtr*turm                               
            djac = hre/xyj(j,k)                                         
            vnujac = vnu*djac                                           
c                                                                       
            c1( j, kcnt) = vnujac*( frt*exs + eys)                      
            c2( j, kcnt) = vnujac*exy                                   
            c3( j, kcnt) = vnujac*( exs + frt*eys)                      
            c4( j, kcnt) = gpr*djac*( exs + eys)*gkap                   
            rr( j, kcnt) = r1                                           
            u( j, kcnt) = r1*q(j,k,2)                                   
            v( j, kcnt) = r1*q(j,k,3)                                   
            tc( j, kcnt) =                                              
     1         q(j,k,4)*r1 - ( u( j, kcnt)**2 + v( j, kcnt)**2)         
c                                                                       
10    continue                                                          
c                                                                       
      do 20 k = 1, 2                                                    
         kp = k + 1                                                     
         kr = k                                                         
         do 20 j = jlow, jup                                            
c                                                                       
            cc1 = c1( j, kr) + c1( j, kp)                               
            cc2 = c2( j, kr)+ c2( j, kp)                                
            cc3 =  c3( j, kr)+ c3( j, kp)                               
            cc4 = c4( j, kr)+ c4( j, kp)                                
c                                                                       
            dkr( j, k,2,1) =                                            
     1         - ( cc1*u( j, kr) + cc2*v( j, kr)) *rr( j, kr)           
            dk( j, k,2,1) = -( cc1*u( j, kp) + cc2*v( j, kp))*rr( j, kp)
            dkr( j, k,2,2) = cc1*rr( j, kr)                             
            dk( j, k,2,2) = cc1*rr( j, kp)                              
            dkr( j, k,2,3) = cc2*rr( j, kr)                             
            dk( j, k,2,3) = cc2*rr( j, kp)                              
            dkr( j, k,2,4) = 0.                                         
            dk( j, k,2,4) = 0.                                          
c                                                                       
            dkr( j, k,3,1) =                                            
     1         - ( cc2*u( j, kr) + cc3*v( j, kr))*rr( j, kr)            
            dk( j, k,3,1) = -( cc2*u( j, kp) + cc3*v( j, kp))*rr( j, kp)
            dkr( j, k,3,2) = cc2*rr( j, kr)                             
            dk( j, k,3,2) = cc2*rr( j, kp)                              
            dkr( j, k,3,3) = cc3*rr( j, kr)                             
            dk( j, k,3,3) = cc3*rr( j, kp)                              
            dkr( j, k,3,4) = 0.                                         
            dk( j, k,3,4) = 0.                                          
c                                                                       
            dkr( j, k,4,1) = -( cc4*tc( j, kr) +cc1*u( j, kr)**2 +      
     1         2.*cc2*u( j, kr)*v( j, kr) +                             
     2         cc3*v( j, kr)**2)*rr( j, kr)                             
            dk( j, k,4,1) = - (cc4*tc( j, kp) + cc1*u( j, kp)**2 +      
     1        2.*cc2*u( j, kp)*v( j, kp) + cc3*v( j, kp)**2) *rr( j, kp)
            dkr( j, k,4,2) = -cc4*u( j, kr)*rr( j, kr) - dkr( j, k,2,1) 
            dk( j, k,4,2) = -cc4*u( j, kp)*rr( j, kp) - dk( j, k,2,1)   
            dkr( j, k,4,3) = -cc4*v( j, kr)*rr( j, kr) - dkr( j, k,3,1) 
            dk( j, k,4,3)= -cc4*v( j, kp)*rr( j, kp) -dk( j, k,3,1)     
            dkr( j, k,4,4) = cc4*rr( j, kr)                             
            dk( j, k,4,4) = cc4 *rr( j, kp)                             
c                                                                       
   20 continue                                                          
c                                                                       
      k = 1                                                             
      kp = k+1                                                          
      do 30 n=2,4                                                       
      do 30 m=1,4                                                       
         do 30 j = jlow, jup                                            
            a(j,n,m) = a(j,n,m) - dkr(j, k,n,m)                         
            b(j,n,m) = b(j,n,m)   +dkr(j, kp,n,m) +dk(j, k,n,m)         
            c(j,n,m) = c(j,n,m) - dk(j, kp,n,m)                         
   30 continue                                                          
c                                                                       
c                                                                       
      return                                                            
      end                                                               
