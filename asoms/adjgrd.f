c----------------------------------------------------------------------
c     -- adjust grid in either x or y direction --
c     -- note: g is the x or y coordinate array to be modified --
c     --       ixy = 1 denotes x direction --
c     --           = 2 denotes y direction --
c     -- m. nemec --
c----------------------------------------------------------------------
      subroutine adjgrd(node, ip,  ifoil, js, je, ks, ke, istep, ixy,
     &     jdim, kdim, g, xo, yo, seg)

      implicit none
      
#include "mxfoil.inc"
#include "bsp.inc"

      integer node, ifoil, js, je, ks, ke, istep, ixy, jdim, kdim
      integer i, ip, j, k

      double precision g(jdim,kdim), xo(jdim,kdim), yo(jdim,kdim)
      double precision delta , seg(jdim*kdim), tmp 

      integer nbs, nbseg(maxfoil), nbcp(maxfoil,maxbseg)
      integer isurf(maxfoil,maxbseg), ibap(maxfoil)
      integer ibstart(maxfoil,maxbseg), ibend(maxfoil,maxbseg)
      double precision bstart(maxfoil,maxbseg), bend(maxfoil,maxbseg)
      double precision bap(ibody,2,maxfoil), bt(ibody,maxfoil,maxbseg)
      double precision bcp(ibsnc,2,maxfoil,maxbseg) 
      double precision bknot(ibskt,maxfoil,maxbseg)
      common/bsinfo/ bstart, bend, bap, bt, bcp, bknot, nbcp, isurf,
     &     ibap, ibstart, ibend, nbseg, nbs

      ip = node
      do j = js, je, istep

         delta = bap(ip,ixy,ifoil) - g(j,ks)
         ip = ip + 1
         if ( abs( delta ) .gt. 1.e-15 ) then
c     -- calculate arclength for k-line at location j --
            i = 1
            seg(i) = 0.d0
            do k = ks+istep, ke, istep 
               tmp = sqrt( (xo(j,k) - xo(j,k-istep) )**2 + ( yo(j,k) -
     &              yo(j,k-istep) )**2 )
               i = i+1
               seg(i) = seg(i-1) + tmp
            end do
            i = 1
            do k = ks, ke, istep
               g(j,k) = g(j,k) + delta/2.0*( 1.0 + cos(4.0*atan(1.0) 
     &              *seg(i)/seg(kdim)) )
c     g(j,k) = g(j,k) + delta*( 1.0 - seg(i)/seg(kdim) )
               i = i+1
            end do
         end if
      end do

      return
      end                       !adjgrd
