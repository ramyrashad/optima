c ********************************************************************
c **  routine to initialize some viscous parameters and time metrics**
c **  called from subroutine setup                                  **
c ********************************************************************
c
      subroutine initstuf(jdim,kdim,turmu,vort,fmu,
     &      coef2x,coef2y,vk,ve,xit,ett,ds)

      implicit none
      integer jdim,kdim,j,k
      double precision turmu(jdim,kdim),vort(jdim,kdim), fmu(jdim,kdim)
      double precision xit(jdim,kdim),ett(jdim,kdim),ds(jdim,kdim)
      double precision coef2x(jdim,kdim),coef2y(jdim,kdim)
      double precision vk(jdim,kdim),ve(jdim,kdim)
c
c     -variables like coef2* and vk,ve are defined here to avoid
c      NAN's if -trapuv (debugging) flag is used
      do 5 k=1,kdim      
      do 5 j=1,jdim       
        turmu(j,k) = 0.    
        vort(j,k)  = 0.     
        fmu(j,k)   = 1.      
        coef2x(j,k)=0.
        coef2y(j,k)=0.
        vk(j,k)=0.
        ve(j,k)=0.
        xit(j,k) = 0.                                                  
        ett(j,k) = 0.                                                  
        ds(j,k)  = 1.0                                                 
    5 continue                   
c
      return
      end
