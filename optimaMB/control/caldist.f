
!=======================================================================

      subroutine caldist(jmax, kmax, x, y, smin, jlow, jup, klow, kup,
     &     icoord, xcoord, ycoord)

!-----------------------------------------------------------------------
!     | purpose: 
!     |    compute generalized distance function
!     |
!     | inputs: ?
!     | outputs: ?
!     | calling routine: ?
!     | written: M. Nemec, October 2001
!     | modifications: M. Tabesh, June 2007
!-----------------------------------------------------------------------

      implicit none

#include "../include/parms.inc"
  
      integer jmax, kmax,jlow,jup,klow,kup,icoord,j,k,ic
      double precision smin(jmax,kmax),xcoord(maxjfoil),ycoord(maxjfoil)
      double precision x(jmax,kmax),y(jmax,kmax),ss,dmin

      do k = klow,kup
         do j = jlow,jup
            dmin = 1.d6
            do ic = 1,icoord
               ss = sqrt((x(j,k)-xcoord(ic))**2+(y(j,k)-ycoord(ic))**2)
               if (ss.lt.dmin) dmin=ss
            end do
            smin(j,k)=dmin
!     cmt: to avoid delta x=0 & delta y=0 ===> smin=0
            if(smin(j,k)==0.d0)then ! 
               smin(j,k)=1.d-18
            endif
         end do
      end do

      return
      end                       !caldist
