C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C                  SSSS    OOOO   RRRRR   TTTTTTT   2222               C
C                 S       O    O  R    R     T     2    2              C
C                  SSSS   O    O  RRRRR      T         2               C
C                      S  O    O  R    R     T       2                 C
C                  SSSS    OOOO   R    R     T     222222              C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE SORT2(NOBFUN,NCHROM,ICCOUNT,FITSAV,ISAV)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THE SUBROUTINE REMOVES DUPLICATES FROM THE LIST        C
C               FITSAV AS WELL AS VALUES AT -99.0, THE VALUE           C
C               USED TO INITIALIZE THE LIST. THE PARAMETER             C
C               ICCOUNT IS THE NUMBER OF ELEMENTS IN THE               C
C               FITSAV LIST COMPUTED BY THIS SUBROUTINE ***            C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
C
      DIMENSION FITSAV(nobfun,nchrom*10)
      DIMENSION FITDUM(nobfun,nchrom*10)
      DIMENSION ISAV(2,nchrom*10)
      DIMENSION IDUM(2,nchrom*10)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
c$$$      NCHROMM = NCHROM-1
c$$$      ICCOUNT = 0
      ICCOUNT = 1
c     load first elements
      DO NO = 1,NOBFUN
         FITDUM(NO,1) = FITSAV(NO,1)
      ENDDO
      IDUM(1,1) = ISAV(1,1)
      IDUM(2,1) = ISAV(2,1)
      
c$$$      DO NC=1,NCHROMM
      DO NC=2,NCHROM
         IOCOUNT = 0
         DO NO=1,NOBFUN
            IF (FITSAV(NO,NC).EQ.FITSAV(NO,NC-1)) THEN
               IOCOUNT = IOCOUNT+1
            ENDIF
         ENDDO 
         IF (IOCOUNT.LT.NOBFUN.AND.FITSAV(1,NC).NE.-99.0) THEN
            ICCOUNT = ICCOUNT+1
            DO NO=1,NOBFUN
               FITDUM(NO,ICCOUNT) = FITSAV(NO,NC)
            ENDDO
            IDUM(1,ICCOUNT) = ISAV(1,NC)
            IDUM(2,ICCOUNT) = ISAV(2,NC)
         ENDIF
      ENDDO
      DO NC=1,ICCOUNT
         DO NO=1,NOBFUN
            FITSAV(NO,NC) = FITDUM(NO,NC)
         ENDDO
         ISAV(1,NC) = IDUM(1,NC)
         ISAV(2,NC) = IDUM(2,NC)
      ENDDO
C
C          *** SORT COMPLETED ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END
