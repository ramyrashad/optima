             do 112 k=klow,kup
                do 112 j=jbegin,jup
                   t=j+g
                   a1=xy(t,k,1)
                   a2=xy(t,k,2)
                   cs=sndsp(t,k)**2
c
                   s1=(spect(t,k,2)+spect(t,k,3)-2.*spect(t,k,1))/2.
                   s2=(spect(t,k,2)-spect(t,k,3))/
     &                                       (2.*sndsp(t,k)*ccx(t,k))
                   u=q(t,k,2)/q(t,k,1)
                   v=q(t,k,3)/q(t,k,1)
                   phi=(u**2+v**2)/2
                   dot1=phi*work(j,k,1,i)
     &                   -u*work(j,k,2,i)
     &                   -v*work(j,k,3,i)
     &                  + work(j,k,4,i)
                   dot2=-uu(j,k)*work(j,k,1,i)
     &                       + a1*work(j,k,2,i)
     &                  + a2*work(j,k,3,i)
c
                   fact1=(s1*dot1*(gami/cs)+s2*dot2)
                   fact2=((s1*dot2)/(a1**2+a2**2)+s2*gami*dot1)
c     
cold                    h=(q(t,k,3)/q(t,k,1))+press(t,k)*xyj(t,k)
c                   h=(q(t,k,4)+press(t,k))/q(t,k,1)
                   temp(j,k,1)=spect(j,k,1)*work(j,k,1,i)
     &                         +fact1
                   temp(j,k,2)=spect(j,k,1)*work(j,k,2,i)
     &                         +fact1*u
     &                         +fact2*a1
                   temp(j,k,3)=spect(j,k,1)*work(j,k,3,i)
     &                         +fact1*v
     &                         +fact2*a2
	           temp(j,k,4)=(abs(uu(j,k))+ccx(j,k))*
     &                         work(j,k,4,i)/xyj(j,k)
c                   temp(j,k,4)=spect(j,k,1)*work(j,k,4,i)
c     &                         +fact1*h
c     &                         +fact2*uu(j,k)
 112         continue
