      subroutine expmaty(jdim,kdim,q,coef2y,coef4y,sndsp,s,
     >spect,xyj,press,ccy,vv,xy,x,y,work,s1,s2,u,v,phi,h,temp,temp2,
     >specthalf)
c
      include '../include/arcom.inc'
c
      integer g,t
      dimension q(jdim,kdim,4),coef2y(jdim,kdim),coef4y(jdim,kdim)
      dimension sndsp(jdim,kdim),s(jdim,kdim,4),work(4,jdim,kdim,2)
      dimension spect(jdim,kdim,3),xyj(jdim,kdim),press(jdim,kdim)
      dimension ccy(jdim,kdim),vv(jdim,kdim),xy(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim),specthalf(jdim,kdim,3)
      dimension temp(4,jdim,kdim),temp2(4,jdim,kdim)
      dimension s1(jdim,kdim),s2(jdim,kdim),u(jdim,kdim),v(jdim,kdim)
      dimension phi(jdim,kdim),h(jdim,kdim),up(4),upp(4)
c
       dtd=dt/(1. +phidt)
       do 11 i=1,4
          do 14 k=kbegin,kup
             kp1=k+1
             do 14 j=jlow,jup
                work(i,j,k,1)=q(j,kp1,i)*xyj(j,kp1) - q(j,k,i)*xyj(j,k)
 14       continue

          do 15 j=jlow,jup
             work(i,j,kend,1)=work(i,j,kup,1)
 15       continue

          do 18 k=klow,kup-1
            kp=k+1
            km=k-1
c           (q(j+2)-3q(j+1)+3q(j)-q(j-1))
            do 18 j=jlow,jup
            work(i,j,k,2)=work(i,j,kp,1)-2.*work(i,j,k,1)+work(i,j,km,1)
 18       continue
c       
c   boundary conditions a la Kyle
          do 20 j=jlow,jup
          work(i,j,kbegin,2)=
     &               q(j,kbegin+2,i)*xyj(j,kbegin+2)-2.*q(j,kbegin+1,i)
     &               *xyj(j,kbegin+1)+q(j,kbegin,i)*xyj(j,kbegin)
          work(i,j,kup,2)=0.0
 20       continue
 11    continue
c
c
      do 50 n=1,3
      do 50 k=kbegin,kup
      do 50 j=jlow,jup
        specthalf(j,k,n)=.5d0*(spect(j,k,n)+spect(k+1,k,n))
 50   continue

      do 51 k=kbegin,kup
        kp1=k+1
        do 51 j=jlow,jup
          up(1)=q(j,k,1)*xyj(j,k)
          upp(1)=q(j,kp1,1)*xyj(j,kp1)
c     
          up(2)=q(j,k,2)/q(j,k,1)
          upp(2)=q(j,kp1,2)/q(j,kp1,1)
c     
          up(3)=q(j,k,3)/q(j,k,1)
          upp(3)=q(j,kp1,3)/q(j,kp1,1)
c     
          up(4)=gami*(q(j,k,4)*xyj(j,k) - 
     &                               0.5d0*up(1)*(up(2)**2+up(3)**2))
          upp(4)=gami*(q(j,kp1,4)*xyj(j,kp1) - 
     &                               0.5d0*upp(1)*(upp(2)**2+upp(3)**2))
 51   continue
c
c
       do 100 k=kbegin,kend
       do 100 j=jlow,jup
c           -calc Roe's average using primitive variables
c            (see pg 465 of Hirsh Vol.2)
         he = gamma*upp(4)/gami/upp(1) + 
     &                                0.5d0*(upp(2)**2+upp(3)**2)
         hi = gamma*up(4)/gami/up(1) + 
     &                                0.5d0*(up(2)**2+up(3)**2)
c        
         sre    = dsqrt(upp(1))
         sri    = dsqrt(up(1))
         denom  = sre + sri
         rhoa   = sre*sri
         ua     = (upp(2)*sre + up(2)*sri)/denom
         va     = (upp(3)*sre + up(3)*sri)/denom
         ha     = (he*sre + hi*sri)/denom
         ca     = dsqrt( gami*(ha - 0.5d0*(ua**2 + va**2)) )

          s1(j,k)=.5d0*(specthalf(j,k,2)+specthalf(j,k,3)
     &                                    -2.*specthalf(j,k,1))
          s2(j,k)=(specthalf(j,k,2)-specthalf(j,k,3))/
     &                                    (2.*ca*ccy(j,k))
          u(j,k)=ua
          v(j,k)=va
          phi(j,k)=.5d0*(u(j,k)**2+v(j,k)**2)         
          h(j,k)=ha
 100   continue
c
       do 888 g=0,0
          do 111 k=kbegin,kup
             t=k+g
             do 111 j=jlow,jup
                include 'matloopy.inc'
 111      continue
c
c
          do 222 n=1,4
             do 140 k=klow,kup
                t=k+g
                do 140 j=jlow,jup
                s(j,k,n)=s(j,k,n) + dtd*( 
     &          coef2y(j,t)*temp(n,j,k)-coef2y(j,t-1)*temp(n,j,k-1)
     &        - (coef4y(j,t)*temp2(n,j,k)-coef4y(j,t-1)*temp2(n,j,k-1)))
            
 140         continue
 222      continue
 888  continue
c     
      return
      end






