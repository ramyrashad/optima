      subroutine frstrm(q,jmax,kmax,nmax,rhoinf,uinf,vinf,einf,turre,
     &                  retinf,js,je,ks,ke,ib)

      implicit none
C ********************************************************************
C **     routine to set freestream values of rho, rho*u, rho*v, e   **
C ********************************************************************
c calling routine:  initia

#include "../include/parms.inc"
#include "../include/mbcom.inc"
      integer jmax,kmax,nmax,js,je,ks,ke,ib,j,k
      double precision rhoinf,uinf,vinf,einf,retinf
      double precision q(jmax,kmax,4),turre(jmax,kmax)
cmt
c      do 32 k = ks, ke                                                  
c      do 32 j = js, je                                                   
      do 32 k = 1, kmax
      do 32 j = 1, jmax
        q(j,k,1) = rhoinf                                              
        q(j,k,2) = rhoinf*uinf                                         
        q(j,k,3) = rhoinf*vinf                                         
        q(j,k,4) = einf                                                
   32 continue 

      if (nmax.eq.5) then
      
         do k = ks, ke   
            do j = js, je
              
               if ( (ibctype(ib,1).eq.1 .and. k .eq. ks) .or.
     &              (ibctype(ib,2).eq.1 .and. j .eq. js) .or.
     &              (ibctype(ib,3).eq.1 .and. k .eq. ke) .or.
     &              (ibctype(ib,4).eq.1 .and. j .eq. je) ) then
c              -- airfoil body --     
                  turre(j,k) = 1.0d-14                   
                  
               else
c              -- other domain --
                  turre(j,k) = 1.0d0
                  
               end if               

            end do
         end do

      end if                    !nmax==5


      
      return
      end
