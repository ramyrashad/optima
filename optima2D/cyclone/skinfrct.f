      subroutine skinfrct(jdim,kdim,q,x,y,xy,cf)
c
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),xy(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim),cf(jdim)
c                                                                       
c
c
c     viscous coefficent of friction calculation        
c     taken from P. Buning                                               
c                                                                    
c     calculate the skin friction coefficient  (w => wall) 
c                                                                    
c      c  = tau   /  q    
c       f      w      inf 
c     
c      where
c     
c            tau  = mu*(du/dy-dv/dx)                                     
c               w                   w
c     
c            q    =  1/2 * rhoinf * uinf * uinf
c             inf
c                                                                    
c            du/dy = (du/dxi * dxi/dy) + (du/deta * deta/dy) 
c            dv/dx = (dv/dxi * dxi/dx) + (dv/deta * deta/dx) 
c     
c     (definition from f.m. white, viscous fluid flow, mcgraw-hill, inc., 
c     york, 1974, p. 50, but use freestream values instead of edge values.
c                                                                    
c                                                                    
c     for calculating cf, we need the coefficient of viscosity, mu.  use  
c     re = (rhoinf*uinf*length scale)/mu = (rhoinf*cinf*length scale)/mu
c          ... also assume cinf=1, rhoinf=1.  

      cinf  = 1.d0
      alngth= 1.d0
c     re already has fsmach scaling                                       
      amu   = rhoinf*cinf*alngth/re
      uinf2 = fsmach**2
c                                                                          
      k= 1                                                              
      j1 = jlow
      j2 = jup
c                                                                       
      if(.not.periodic)then                                          
        j1 = jtail1                                               
        j2 = jtail2                                               
      endif                                                          
c
c
c     -compute Cf at each node location
      if (iord.eq.4) then
c                                                                       
        tmp=1.d0/12.d0
        do 110 j = j1,j2                                            
          jp1 = jplus(j)                                              
          jp2 = jplus(jp1)                                              
          jm1 = jminus(j)                                             
          jm2 = jminus(jm1)
c
c         Note: for viscous flows u and v = 0 on body ...
c               consequently, the xi direction derivatives can be skipped.
c               -only during first strtit iterations is u and v non-zero
c                but we dont't care about cf then. 
c
csdc         xi direction    
csd          rm2=1.d0/q(jm2,k,1)
csd          rm1=1.d0/q(jm1,k,1)
csd          rp1=1.d0/q(jp1,k,1)
csd          rp2=1.d0/q(jp2,k,1)
csdc
csd          um2=q(jm2,k,2)*rm2
csd          um1=q(jm1,k,2)*rm1
csd          up1=q(jp1,k,2)*rp1
csd          up2=q(jp2,k,2)*rp2
csdc
csd          vm2=q(jm2,k,3)*rm2
csd          vm1=q(jm1,k,3)*rm1
csd          vp1=q(jp1,k,3)*rp1
csd          vp2=q(jp2,k,3)*rp2
csdc
csd          uxi = tmp*(um2 + 8.d0*(-um1+up1) - up2)
csd          vxi = tmp*(vm2 + 8.d0*(-vm1+vp1) - vp2)
          uxi=0.d0
          vxi=0.d0
c
c
c         eta direction
          r=1.d0/q(j,k,1)
          rp1=1.d0/q(j,k+1,1)
          rp2=1.d0/q(j,k+2,1)
          rp3=1.d0/q(j,k+3,1)
          rp4=1.d0/q(j,k+4,1)
c
          u  =q(j,k,2)*r
          up1=q(j,k+1,2)*rp1
          up2=q(j,k+2,2)*rp2
          up3=q(j,k+3,2)*rp3
          up4=q(j,k+4,2)*rp4
c
          v  =q(j,k,3)*r
          vp1=q(j,k+1,3)*rp1
          vp2=q(j,k+2,3)*rp2
          vp3=q(j,k+3,3)*rp3
          vp4=q(j,k+4,3)*rp4
c     
          ueta= tmp*(-25.d0*u +48.d0*up1-36.d0*up2+16.d0*up3-3.d0*up4) 
          veta= tmp*(-25.d0*v +48.d0*vp1-36.d0*vp2+16.d0*vp3-3.d0*vp4) 
c
c         metric derivatives
          xix = xy(j,k,1)                                             
          xiy = xy(j,k,2)                                             
          etax = xy(j,k,3)                                            
          etay = xy(j,k,4)                                            
c
          tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))         
c          jj=j-j1+1
c         print *,'tauw=',jj,sn(jj)/sn(jd),tauw
          cf(j) = tauw/(.5d0*rhoinf*uinf2)
 110    continue                                                    
        if (sngvalte) then
c         -average trailing edge value
          cf(j1)=.5d0*(cf(j1)+cf(j2))
          cf(j2)=cf(j1)
        endif
        do 109 j=j1,j2
          write(cf_unit,777) x(j,k),cf(j),y(j,k)
 777      format (3f20.12)
 109    continue


      elseif (iord.eq.2) then
c                                                                       
        if (.not. periodic) then
          j = j1                                                  
          jp1 = j+1                                                   
c
csdc         xi direction
csd          u=q(j,k,2)/q(j,k,1)
csd          up1=q(jp1,k,2)/q(jp1,k,1)
csd          v=q(j,k,3)/q(j,k,1)
csd          vp1=q(jp1,k,3)/q(jp1,k,1)
csdc
csd          uxi = up1-u
csd          vxi = vp1-v
          uxi=0.d0
          vxi=0.d0
c
c         eta direction
          up1=q(j,k+1,2)/q(j,k+1,1)
          up2=q(j,k+2,2)/q(j,k+2,1)
          vp1=q(j,k+1,3)/q(j,k+1,1)
          vp2=q(j,k+2,3)/q(j,k+2,1)
c
          ueta= .5d0*(-3.d0*u + 4.d0*up1 - up2)
          veta= .5d0*(-3.d0*v + 4.d0*vp1 - vp2)
c
          xix = xy(j,k,1)                                             
          xiy = xy(j,k,2)                                             
          etax = xy(j,k,3)                                            
          etay = xy(j,k,4)                                            
          tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))         
          cf(j) = tauw/(.5*rhoinf*uinf2)                                
          ja=j1+1
          jb=j2-1
        endif
c                                                                       
        do 111 j = ja,jb
          jp1 = jplus(j)                                              
          jm1 = jminus(j)                                             
c
c         xi direction
          um1=q(jm1,k,2)/q(jm1,k,1)
          up1=q(jp1,k,2)/q(jp1,k,1)
          vm1=q(jm1,k,3)/q(jm1,k,1)
          vp1=q(jp1,k,3)/q(jp1,k,1)
c
c          uxi = .5d0*(up1-um1)
c          vxi = .5d0*(vp1-vm1)
          uxi=0.d0
          vxi=0.d0
c
c         eta direction
          u  =q(j,k,2)/q(j,k,1)
          up1=q(j,k+1,2)/q(j,k+1,1)
          up2=q(j,k+2,2)/q(j,k+2,1)
          v  =q(j,k,3)/q(j,k,1)
          vp1=q(j,k+1,3)/q(j,k+1,1)
          vp2=q(j,k+2,3)/q(j,k+2,1)
c
          ueta= .5d0*(-3.d0*u + 4.d0*up1 - up2)
          veta= .5d0*(-3.d0*v + 4.d0*vp1 - vp2)
c
c
          xix = xy(j,k,1)                                             
          xiy = xy(j,k,2)                                             
          etax = xy(j,k,3)                                            
          etay = xy(j,k,4)                                            
          tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))         
          cf(j) = tauw/(.5*rhoinf*uinf2)                                
c          write(99,444) j,u,up1,up2,ueta
c 444      format(i4,4e15.6)
 111    continue                                                    

        if (.not.periodic) then
          j = jtail2                                                  
          jm1 = j-1
c
          
c         xi direction
          um1=q(jm1,k,2)/q(jm1,k,1)
          u  =q(j,k,2)/q(j,k,1)
          vm1=q(jm1,k,3)/q(jm1,k,1)
          v  =q(j,k,3)/q(j,k,1)
c
c          uxi = u-um1
c          vxi = v-vm1
          uxi=0.d0
          vxi=0.d0
c
c         eta direction
          up1=q(j,k+1,2)/q(j,k+1,1)
          up2=q(j,k+2,2)/q(j,k+2,1)
          vp1=q(j,k+1,3)/q(j,k+1,1)
          vp2=q(j,k+2,3)/q(j,k+2,1)
c
          ueta= .5d0*(-3.d0*u + 4.d0*up1 - up2)
          veta= .5d0*(-3.d0*v + 4.d0*vp1 - vp2)
c
c     
          xix = xy(j,k,1)                                             
          xiy = xy(j,k,2)                                             
          etax = xy(j,k,3)                                            
          etay = xy(j,k,4)                                            
          tauw= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))         
          cf(j) = tauw/(.5*rhoinf*uinf2)                                
        endif
        if (sngvalte) then
c         -average trailing edge value
          cf(j1)=.5d0*(cf(j1)+cf(j2))
          cf(j2)=cf(j1)
        endif
        do 119 j=j1,j2
          write(cf_unit,777) x(j,k),cf(j),y(j,k)
 119    continue
      endif
c
      return
      end
