c----------------------------------------------------------------------
c     -- regrid: grid perturbation --
c     -- m. nemec, may 2001 --
c----------------------------------------------------------------------

      subroutine regrid (jdv, x, y)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
c     -- new common block for blunt t.e. --
#include "../include/bteinfo.inc"

      integer jdv,ifoil,ii,iseg,i,is,ie,nps,ncp,j,it,i2,nside1,nside2
      integer xory
      double precision dx,dy,bcp_tmp,xte,yte
      double precision x(maxjk), y(maxjk)
      double precision ap(ibody,2), cp(ibsnc,2), t(ibody), bktmp(ibskt)

c     -- locals --
c     dimension bapf(ibody,2)
      logical flag, gogo

      if ( jdv.eq.-1 ) then
c     -- regrid the whole domain --
         
c     -- reset bcp and bap array --
         do ifoil=1,nfoils
            do ii = 1, ibap(ifoil) 
               bap(ii,1,ifoil) = bapo(ii,1,ifoil)
               bap(ii,2,ifoil) = bapo(ii,2,ifoil)
            end do
            do iseg=1,nbseg(ifoil)
               do ii = 1,nbcp(ifoil,iseg)
                  bcp(ii,1,ifoil,iseg) = bcpo(ii,1,ifoil,iseg)
                  bcp(ii,2,ifoil,iseg) = bcpo(ii,2,ifoil,iseg)
               end do
            end do
         end do

         do ifoil=1,nfoils
            if (nbdv.ne.0) then !b-sline design variables
               do iseg=1,nbseg(ifoil)
                  xory=2
                  if (isurf(ifoil,iseg) .eq. 3) xory=1   !blunt trailing edge              
                  do i = 1,nbdv
                     if ( idvf(i).eq.ifoil .and. idvs(i).eq.iseg ) then
                        bcp(idvi(i),xory,ifoil,iseg) = dvs(i)
                     end if
                  end do

                  is = ibstart(ifoil,iseg)
                  ie = ibend(ifoil,iseg) 
                  nps = ie - is + 1
                  ncp = nbcp(ifoil,iseg)

                  j = 1
                  do i = is,ie
                     t(j) = bt(j,ifoil,iseg)
                     j = j+1
                  end do

                  do i = 1,ncp
                     cp(i,1) = bcp(i,xory,ifoil,iseg)
                  end do

                  do i = 1,ncp+jbsord
                     bktmp(i) = bknot(i,ifoil,iseg)
                  end do
                  
c     -- generate new airfoil surface and store in array bap --
                  call new_body(is, ie, nps, ncp, ifoil, bktmp, t,
     &                 ap, cp, xory)
               end do

               if ( nbseg(ifoil).ne.0 ) then
c     -- modify all blocks next to airfoil --
c     -- only modify grid in y direction => flag = false --
                  flag = .false.
                  call modall (ifoil, flag, x, y, .false.)
               end if
            end if

c     -- gap and overlap design variables --
            tdelx = 0.0
            tdely = 0.0
            gogo = .false.
            do it=1,ntdv
c     -- check if current element has gap/overlap variables --
               if ( idvf(nbdv+it).eq.ifoil ) then
c     -- trailing edge for element at lower surface --
                  xte = bap(1,1,ifoil)
                  yte = bap(1,2,ifoil)
c     -- check for blunt trailing edge --
                  if (bte(ifoil).eq..true.) then
                     xte = bap(ite(ifoil),1,ifoil)
                     yte = bap(ite(ifoil),2,ifoil)
                  endif

c     -- check for horizontal design variable --
                  if (idvs(nbdv+it).eq.1) then
                     gogo = .true.
                     tdelx = dvs(nbdv+it) - xte
                  else if (idvs(nbdv+it).eq.2) then
                     gogo = .true.
                     tdely = dvs(nbdv+it) - yte 
                  else
                     stop 'regrid: unexpected value in dvs'
                  end if
               end if
            end do

c     -- modify all blocks next to airfoil --
c     -- only modify grid in x and y directions => flag = true --
            if (gogo) then
c     -- translate airfoil body --
               do ii = 1, ibap(ifoil) 
                  bap(ii,1,ifoil) = bap(ii,1,ifoil) + tdelx
                  bap(ii,2,ifoil) = bap(ii,2,ifoil) + tdely
               end do

c     -- translate bspline control points --
               do iseg=1,nbseg(ifoil)
                  do ii = 1,nbcp(ifoil,iseg)
                     bcp(ii,1,ifoil,iseg) = bcp(ii,1,ifoil,iseg) + tdelx
                     bcp(ii,2,ifoil,iseg) = bcp(ii,2,ifoil,iseg) + tdely
                  end do
               end do

               flag = .true.
               call modall (ifoil, flag, x, y, .true.)
            end if
         end do                 ! nfoils

      else
c     ------------------------------------------------------------------
c     -- regrid for selected design variable --

         if (jdv.le.nbdv) then

            ifoil = idvf(jdv)
            iseg = idvs(jdv)

            xory=2
            if (isegside(iseg,ifoil).eq. 2) xory=1 !blunt trailing edge

            bcp_tmp = bcp(idvi(jdv),2,ifoil,iseg)
            bcp(idvi(jdv),xory,ifoil,iseg) = dvs(jdv) + tdely

            is = ibstart(ifoil,iseg)
            ie = ibend(ifoil,iseg) 
            nps = ie - is + 1
            ncp = nbcp(ifoil,iseg)

            j = 1
            do i = is,ie
               t(j) = bt(j,ifoil,iseg)
               j = j+1
            end do

            do i = 1,ncp
               cp(i,1) = bcp(i,xory,ifoil,iseg)
            end do

            do i = 1,ncp+jbsord
               bktmp(i) = bknot(i,ifoil,iseg)
            end do

c     -- store bap --
c     do i = 1, ibap(ifoil) 
c     bapf(i,1) = bap(i,1,ifoil)
c     bapf(i,2) = bap(i,2,ifoil)
c     end do   

c     -- generate new airfoil surface and store in array bap --
            call new_body(is, ie, nps, ncp, ifoil, bktmp, t,ap,cp,xory)

c     -- modify all blocks next to airfoil --
c     -- only modify grid in y direction => flag = false --
            flag = .false.
            call modall (ifoil, flag, x, y, .false.)

c     -- restore original control point --
            bcp(idvi(jdv),2,ifoil,iseg) = bcp_tmp

c     -- restore bap --
c     do i = 1, ibap(ifoil) 
c     bap(i,1,ifoil) = bapf(i,1)
c     bap(i,2,ifoil) = bapf(i,2)
c     end do
            
         else
c     -- gap/overlap design variables -- 
            ifoil = idvf(jdv)
c     -- trailing edge for element at lower surface --
            xte = bap(1,1,ifoil)
            yte = bap(1,2,ifoil)
c     -- check for blunt trailing-edge --
            if (bte(ifoil).eq..true.) then
               xte = bap(ite(ifoil),1,ifoil)
               yte = bap(ite(ifoil),2,ifoil)
            endif

            dx = 0.d0
            dy = 0.d0

c     -- check for horizontal design variable --
            if ( idvs(jdv).eq.1 ) then
               dx = dvs(jdv) - xte
            else if (idvs(jdv).eq.2) then
               dy = dvs(jdv) - yte
            else
               stop 'regrid: unexpected value in dvs'
            end if

c     -- translate airfoil body --
            do ii = 1, ibap(ifoil) 
               bap(ii,1,ifoil) = bap(ii,1,ifoil) + dx
               bap(ii,2,ifoil) = bap(ii,2,ifoil) + dy
            end do           

            flag = .true.
            call modall (ifoil, flag, x, y, .true.)

c     do ii = 1, ibap(ifoil) 
c     bap(ii,1,ifoil) = bap(ii,1,ifoil) - dx
c     bap(ii,2,ifoil) = bap(ii,2,ifoil) - dy
c     end do  
            
         end if
      end if                    !jdv

c     -- copy grid halo points --
      do ii=1,nblks
         do nside1=2,4,2
            i2 = lblkpt(ii,nside1)
            if (i2.ne.0) then
               nside2= lsidept(ii,nside1)
               call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),x(lgptr(ii)),
     &              jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &              kminbnd(ii),kmaxbnd(ii),
     &              jmax(i2),kmax(i2),x(lgptr(i2)),
     &              jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &              kminbnd(i2),kmaxbnd(i2))
               call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),y(lgptr(ii)),
     &              jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &              kminbnd(ii),kmaxbnd(ii),
     &              jmax(i2),kmax(i2),y(lgptr(i2)),
     &              jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &              kminbnd(i2),kmaxbnd(i2))
            endif
         end do
      end do

      do ii=1,nblks
         do nside1=1,3,2
            i2 = lblkpt(ii,nside1)
            if (i2.ne.0 .and. 
     &           (ibctype(ii,nside1).ne.5 .or. nhalo.ge.2)) then
               nside2= lsidept(ii,nside1)
               call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),x(lgptr(ii)),
     &              jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &              kminbnd(ii)-nsd1(ii),kmaxbnd(ii)+nsd3(ii),
     &              jmax(i2),kmax(i2),x(lgptr(i2)),
     &              jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &              kminbnd(i2)-nsd1(i2),kmaxbnd(i2)+nsd3(i2))
               call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),y(lgptr(ii)),
     &              jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &              kminbnd(ii)-nsd1(ii),kmaxbnd(ii)+nsd3(ii),
     &              jmax(i2),kmax(i2),y(lgptr(i2)),
     &              jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &              kminbnd(i2)-nsd1(i2),kmaxbnd(i2)+nsd3(i2))
            endif
         end do
      end do

c     write (*,*) 'done regrid'

      return
      end                       !regrid
