C ********************************************************************
C ***        subroutine to output q                                ***
C ********************************************************************
      subroutine writeq2(junit,q,xyj,jdim,kdim,j1,j2,k1,k2)
      dimension q(jdim,kdim,4), xyj(jdim,kdim)
      write(junit) (((q(j,k,n),j=j1,j2),k=k1,k2),n=1,4)
c
      return 
      end
