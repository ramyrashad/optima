      subroutine heap_add(iarr,j,ptr,er)
c
      integer ptr(iarr),j,ii,jj,itmp
      double precision er(iarr),dabii,dabjj
c
      jj=j
 10   ii=jj/2
      if (ii.eq.0) goto 999
      if (ptr(ii).eq.0 .or. ptr(jj).eq.0) goto 999
c
      dabii=dabs(er(ptr(ii)))
      dabjj=dabs(er(ptr(jj)))
c
      if (dabii.lt.dabjj) then
         itmp=ptr(ii)
         ptr(ii)=ptr(jj)
         ptr(jj)=itmp
         jj=ii
         goto 10
      else
         goto 999
      endif
c
 999  return
      end
