!
! =====================================================================
!
module Snopt_Mod
!
! ---------------------------------------------------------------------
! Purpose: Contains neccessary subroutines to run SNOPT optimization,
!          whether for optimizing or satisfying constraints only
!
! Author:  Oleg Chernukhin, Sept 2010
! ---------------------------------------------------------------------

use CommonH

implicit none
public

! _____________________________________________
! -- Module variables, constants and structures

   !-- SNOPT interfaces
   interface
      subroutine snInit(iPrint, iSumm, cw, lencw, iw, leniw, rw, lenrw)
         integer :: iPrint, iSumm, lencw, leniw, lenrw
         character(len=8) :: cw(lencw)
         integer          :: iw(leniw)
         real(kind(1.d0)) :: rw(lenrw)
      end subroutine snInit
   end interface

   interface
      subroutine snSet(buffer, iPrint, iSumm, Errors, &
                       cw, lencw, iw, leniw, rw, lenrw)
         character(len=*) :: buffer
         integer :: iPrint, iSumm, Errors, lencw, leniw, lenrw
         character(len=8) :: cw(lencw)
         integer          :: iw(leniw)
         real(kind(1.d0)) :: rw(lenrw)
      end subroutine snSet
   end interface

   interface
      subroutine snSeti(buffer, ivalue, iPrint, iSumm, Errors, &
                        cw, lencw, iw, leniw, rw, lenrw)
         character(len=*) :: buffer
         integer :: ivalue, iPrint, iSumm, Errors, lencw, leniw, lenrw
         character(len=8) :: cw(lencw)
         integer          :: iw(leniw)
         real(kind(1.d0)) :: rw(lenrw)
     end subroutine snSeti
   end interface

   interface
      subroutine snSetr(buffer, rvalue, iPrint, iSumm, Errors, &
                        cw, lencw, iw, leniw, rw, lenrw)
         character(len=*) :: buffer
         integer :: iPrint, iSumm, Errors, lencw, leniw, lenrw
         character(len=8) :: cw(lencw)
         integer          :: iw(leniw)
         real(kind(1.d0)) :: rvalue, rw(lenrw)
     end subroutine snSetr
   end interface

   interface
      subroutine snOptA(Start, nF, n, nxname, nFname, ObjAdd, ObjRow, &
                        ProbI,usrfun,iAfun,jAvar,lenA,neA,A, &
                        iGfun, jGvar, lenG, neG, xlow, xupp, xnames, &
                        Flow, Fupp, Fnames, x, xstate, xmul, &
                        F, Fstate, Fmul, INFO, mincw, miniw, minrw, &
                        nS, nInf, sInf, cu, lencu,iu, leniu, ru, &
                        lenru, cw, lencw, iw, leniw, rw, lenrw)
         interface
            subroutine usrfun(Stat, nvar,dv,needf, nF, f, needG, &
                              lenG, G, cu, lencu, iu, leniu, ru, &
                              lenru)
               integer :: nvar, needf, nF, needG, lenG, lencu, &
                          leniu, lenru, Stat
               character(len=8) :: cu(lencu)
               integer          :: iu(leniu)
               real(kind(1.d0)) :: dv(nvar), f(nF), G(lenG), ru(lenru)
            end subroutine
         end interface
         character(len=8) :: &
            ProbI, cu(lencu), cw(lencw), Fnames(nFname), xnames(nxname)
         integer :: &
            INFO, lenA, lencu, lencw, lenG, leniu, &
            leniw, lenru, lenrw, mincw, miniw, minrw, n, neA, neG, nF,&
            nFname, nInf, nS, nxname, ObjRow, Start, iAfun(lenA), &
            iGfun(lenG), iu(leniu), iw(leniw), jAvar(lenA), &
            jGvar(lenG), xstate(n), Fstate(nF)
         real(kind(1.d0)) :: &
            ObjAdd, sInf, A(lenA), F(nF), Fmul(nF), Flow(nF), &
            Fupp(nF), ru(lenru), rw(lenrw), x(n), xlow(n), xmul(n), &
            xupp(n)
      end subroutine snOptA
   end interface

! ___________________
! module subroutines
contains
!
! =====================================================================
!
subroutine runSNOPT(popX, popFnc, maj_its_lim, printout, snopt_status,&
                    usrfun)
! ---------------------------------------------------------------------
!
! Purpose:
!    Sets up and runs SNOPT. This subroutine is used for multiple
!    purposes. It can be used for a) performing full optimization,
!    b) only finding a feasible point, c) only satisfying linear 
!    constraints.
!
! ---------------------------------------------------------------------
! ____________________________________
! variable specification

   !-- subroutine arguments
   integer :: maj_its_lim, snopt_status
   real(kind=dp) :: popX(popsize, nX), popFnc(popsize, nFnc)
   integer :: printout
   interface
      subroutine usrfun(Stat, nvar,dv,needf, nF, f, needG, &
                        lenG, G, cu, lencu, iu, leniu, ru, &
                        lenru)
         integer :: nvar, needf, nF, needG, lenG, lencu, &
                    leniu, lenru, Stat
         character(len=8) :: cu(lencu)
         integer          :: iu(leniu)
         real(kind(1.d0)) :: dv(nvar), f(nF), G(lenG), ru(lenru)
      end subroutine
   end interface

   !-- local variables
   integer :: Start, ObjRow, nxname, nFname, &
              lencw, leniw, lenrw, &
              ltmpcw, ltmpiw, ltmprw, &
              mincw, miniw, minrw, &
              nS, nInf, neA, neG, &
              Stat, info, iPrint, iSumm, &
              needF, needG, ii, min_its_lim, &
              maj_pr_level, min_pr_level
   integer :: Xstate(nX), Fstate(nFnc)
   real(kind=dp) :: X(nX), Fnc(nFnc), ObjAdd, sInf, sobpt(nX), &
                    Grad(nnzGrad), feas_tol, opt_tol, &
                    idle_time, tot_time
   character(len=8), allocatable :: cw(:), tmpcw(:)
   integer, allocatable :: iw(:), tmpiw(:)
   real(kind=dp), allocatable :: rw(:), tmprw(:)
   character(len=8) :: xnames(1), Fnames(1), Prob

! ____________________________________
! begin main execution

   !-- Fill X and nFnc
   do ii = 1, nX
      X(ii) = popX(mycid, ii)
   end do
   do ii = 1, nFnc
      Fnc(ii) = popFnc(mycid, ii)
   end do

   !-- Initialize local variables
   Start = 0
   nxname = 1
   nFname = 1
   ObjRow = 1
   ObjAdd = 0.0d0
   needF = 1 
   needG = 1
   neA = nnzLinG
   neG = nnzGrad
   min_its_lim = 1500
   feas_tol = 1.0d-6
   Prob = 'Probname'
   maj_pr_level = ishft(ibclr(ibclr(printout, 7), 8), -1)
   min_pr_level = 0
   if (btest(printout, 7)) min_pr_level = 1
   if (btest(printout, 8)) min_pr_level = 10

   !-- For Jetstream use 1e-8, for Optima2D use 1e-6
   opt_tol = 1.0d-6
!   opt_tol = 1.0d-8

   !-- Allocate temporary arrays
   ltmpcw = 500
   ltmpiw = 500
   ltmprw = 500
   allocate(tmpcw(ltmpcw),tmpiw(ltmpiw),tmprw(ltmprw))

   !-- Initialize SNOPT and set some of the options
   if (myrank_loc == 0) then

      if (opt_output > 1) then
         iPrint = iPrintC
         iSumm = iSummC
      else
         iPrint = -1
         iSumm = -1
      endif
   else
      !-- Non-root procs do not print anything out
      iPrint = -1
      iSumm = -1
   endif

   !-- Initialize SNOPT
   call snInit(iPrint, iSumm, tmpcw, ltmpcw, tmpiw, &
               ltmpiw, tmprw,ltmprw)

   !-- Set SNOPT options
   ierr = 0

   call snSeti('Major print level', maj_pr_level, &
        iPrint, iSumm, ierr, tmpcw, ltmpcw, tmpiw, ltmpiw, &
        tmprw, ltmprw)

   call snSeti('Minor print level', min_pr_level, &
        iPrint, iSumm, ierr, tmpcw, ltmpcw, tmpiw, ltmpiw, &
        tmprw, ltmprw)   

   call snSet('Hessian full memory', &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSet('LU rook pivoting', &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSeti('Superbasics limit', nX+1, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSeti('Iterations limit', 1000000, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSeti('Major iterations limit', maj_its_lim, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSeti('Minor iterations limit', min_its_lim, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)      

   call snSetr('Major optimality tolerance', opt_tol, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSetr('Major feasibility tolerance', feas_tol, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   !-- Find the neccessary sizes for cw, iw, and rw
   call snMemA(info, nFnc, nX, 1, 1, nnzLinG, nnzGrad, &
               lencw, leniw, lenrw, &
               tmpcw, ltmpcw, tmpiw, ltmpiw, tmprw,ltmprw)
   if (info /= 104) then
      write(*,*) myrank_glob, &
          ': Error in Hybropt_Mod:runSNOPT :: ', &
          'problem calling snMemA'
     call mpi_abort(HYBROPT_COMM,0,ierr)
   endif   
   info = 0

   !-- Let SNOPT know the storage will be increased
   ierr = 0
   call snSeti('Total character workspace', lencw, &
        iPrint, iSumm, ierr, tmpcw, ltmpcw, tmpiw, ltmpiw, &
        tmprw, ltmprw)

   call snSeti('Total integer workspace',leniw, &
        iPrint, iSumm, ierr, tmpcw, ltmpcw, tmpiw, ltmpiw, &
        tmprw, ltmprw)

   call snSeti('Total real workspace',lenrw, &
        iPrint, iSumm, ierr, tmpcw, ltmpcw, tmpiw, ltmpiw, &
        tmprw, ltmprw)     
      
   if (ierr > 0) then
     write(*,*) myrank_glob, &
          ': Error in Hybopt_Mod:runSNOPT :: ', &
          'error setting SNOPT options'
     call mpi_abort(HYBROPT_COMM,0,ierr)
   end if

   !-- Broadcast neccessary sizes for cw, iw, and rw
   call MPI_Bcast(lencw, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call MPI_Bcast(leniw, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call MPI_Bcast(lenrw, 1, MPI_INTEGER, 0, comm(mycid), ierr)

   !-- Allocate arrays
   allocate(cw(lencw), iw(leniw), rw(lenrw))

   !-- Copy values from tmp arrays
   cw(1:ltmpcw) = tmpcw(1:ltmpcw)
   iw(1:ltmpiw) = tmpiw(1:ltmpiw)
   rw(1:ltmprw) = tmprw(1:ltmprw)

   ! set variables that are never changed / used
   Xstate(:) = 0
   Fstate(:) = 0

   ! root enters snOptA, other procs enter usrfun
   if (myrank_loc == 0) then

#if 1
      !-- Initiate load balancing mechanism
      if (hybropt_method == 3 .and. load_bal_tol < 1.0d0) then 

         if (myrank_glob == 0) then
            do ii = 2, popsize
               !-- Start non-blocking receive operation for 'done' flag
               call mpi_irecv(donebuffer(ii), 1, MPI_LOGICAL, &
                              (ii-1)*ppc, done_tag, HYBROPT_COMM, &
                              done_recv_req(ii), ierr)
               if (ierr .ne. MPI_SUCCESS) then
                  write(*,*) 'ERROR: MPI_IRECV(done) in Snopt_Mod'
                  call mpi_abort(HYBROPT_COMM, 0, ierr)
               endif
            end do
         else
            !-- Start non-blocking receive for 'terminate' flag
            call mpi_irecv(termbuffer, 1, MPI_LOGICAL, 0, term_tag, &
                           HYBROPT_COMM, term_recv_req, ierr)
            if (ierr .ne. MPI_SUCCESS) then
                write(*,*) 'ERROR: MPI_IRECV(done) in Snopt_Mod'
                call mpi_abort(HYBROPT_COMM, 0, ierr)
            endif
         endif
      endif
#endif

      call snOptA(Start, & ! Start = 0 always
                  nFnc, & ! number of problem functions in F(x)
                  nX, & ! number of design variables
                  nxname, & ! nxname = 1 always
                  nFname, & ! nFname = 1 always
                  ObjAdd, & ! ObjAdd = 0.0d0 always
                  ObjRow, & ! ObjRow = 1 always
                  Prob, & ! Problem name
                  usrfun, & ! usrfun subroutine handle
                  iLfun, & ! iAfun function/constraint index in A
                  jLvar, & ! jAvar variable index in A
                  nnzLinG, & ! lenA
                  neA, & ! always same as lenA
                  LinG, & ! linear matrix A
                  iGfun, & ! i-coords in non-linear matrix G
                  jGvar, & ! j-coords in non-linear matrix G
                  nnzGrad, & ! lenG
                  neG, & ! always same as lenG
                  Xlow, & ! vector of lower bounds on dv's
                  Xupp, & ! vector of upper bounds on dv's
                  xnames, & ! array of length 1 always
                  Flow, & ! always negative infinity
                  Fupp, & ! always positive infinity
                  Fnames, & ! array of length 1 always
                  X, & ! initial guess
                  Xstate, & ! always array of zeros
                  Xmul, & ! 
                  Fnc, & ! function/constraint
                  Fstate, & ! always zeros
                  Fmul, & ! always zeros
                  snopt_status, & ! status of optimization process
                  mincw, & ! size of cw workspace needed 
                  miniw, & ! size of iw workspace needed
                  minrw, & ! size of rw workspace needed
                  nS, & ! final number of superbasic variables
                  nInf, & ! number of infeasibilities
                  sInf, & ! sum of infeasibilities
                  cw, & 
                  lencw, &
                  iw, & 
                  leniw, &
                  rw, &
                  lenrw, &
                  cw, &
                  lencw, &
                  iw, &
                  leniw, &
                  rw, &
                  lenrw)

#if 1
   idle_time = mpi_wtime()

   !-- Finalize load-balancing mechanism
   if (hybropt_method == 3 .and. load_bal_tol < 1.0d0) then
      if (myrank_glob == 0) then
         !-- If root exited SNOPT and others are still running,
         !-- enter loop until decision to terminate has been reached
         if (.not. decision_term) then
            do
               !call sleep(1) !-- Wait a second...
               doneflag(1) = .true. !-- Root is done
               !-- Check who else is done
               do ii = 2, popsize
                  call mpi_test(done_recv_req(ii), doneflag(ii), &
                                statmpi_array, ierr)
               end do

               !-- Decide if we need to send terminate signal
               call hm_load_bal_decide(doneflag, decision_term)

               if (decision_term) then
                  !-- Send terminate signal to others
                  do ii = 2, popsize
                     call mpi_send(decision_term, 1, MPI_LOGICAL, &
                                   (ii-1)*ppc, term_tag, HYBROPT_COMM, &
                                   ierr)
                  end do
                  !-- Exit the loop
                  exit
               endif
            end do
         endif

         !-- Complete non-blocking 'done' receive before proceeding
         do ii = 2, popsize
            call mpi_wait(done_recv_req(ii), statmpi_array, ierr)
         end do
       
         !-- Re-set doneflag
         doneflag(:) = .false.
      else !-- Other local roots
         !-- Send done message to global root
         donemsg = .true.
         call mpi_send(donemsg, 1, MPI_LOGICAL, 0, done_tag, &
                       HYBROPT_COMM, ierr)

         !-- Complete non-blocking 'terminate' receive before proceeding
         call mpi_wait(term_recv_req, statmpi_array, ierr)

         !-- Re-set donemsg
         donemsg = .false.
      endif

      !-- Re-set decision_term flag
      decision_term = .false.
   endif
#endif

   else
      !-- Non-root processes still enter snOptA, but with a different
      !-- usrfun (say 'usrfun_test'). If the problem is setup OK, 
      !-- this usrfun will be entered, stat will be set to -2, and 
      !-- we exit SNOPT problem. 
      !
      !-- Once we know the problem setup is OK, we enter the provided
      !-- usrfun, where the root is waiting for us.
      !--
      !-- If the problem setup is not OK, SNOPT exists immediately,
      !-- without going to 'usrfun_test'. We find out about this from
      !-- stat, and avoid entering usrfun, where the root WILL NOT
      !-- be waiting for us.
      !--
      !-- This modification avoids bringing down the whole system.

      call snOptA(Start, nFnc, nX, nxname, nFname, ObjAdd, ObjRow, & 
                  Prob, usrfun_test, iLfun, jLvar, nnzLinG, neA, &
                  LinG, iGfun, jGvar, nnzGrad, neG, Xlow, &
                  Xupp, xnames, Flow, Fupp, Fnames, X, Xstate, &
                  Xmul, Fnc, Fstate, Fmul, snopt_status, &
                  mincw, miniw, minrw, nS, nInf, sInf, cw, lencw, &
                  iw, leniw, rw, lenrw, cw, lencw, iw, leniw, &
                  rw, lenrw)

      if (snopt_status == 71) then
         !-- Problem setup is OK

         do
            call usrfun(Stat, & ! determines when SNOPT is done mainly
                        nX, & ! number of design variables
                        X, & ! vector of design variables
                        needF, & 
                        nFnc, & 
                        Fnc, &
                        needG, & 
                        nnzGrad, & 
                        Grad, &  
                        cw, & 
                        lencw, &
                        iw, &
                        leniw, &
                        rw, &
                        lenrw)
            if (Stat >= 2 .or. Stat <= -2) then
               exit
            endif
         end do
      endif

      idle_time = mpi_wtime()

   endif

   !-- Store the results in popX and popFnc
   if (myrank_loc == 0) then
      popFnc(mycid, :) = Fnc(:)
      popX(mycid, :) = X(:)
   endif

   !-- Broadcast the results to all local procs
   call mpi_bcast(popFnc(mycid,:), nFnc, MPI_DOUBLE_PRECISION, 0, &
                  comm(mycid), ierr)
   call mpi_bcast(popX(mycid,:), nX, MPI_DOUBLE_PRECISION, 0, &
                  comm(mycid), ierr)
   call mpi_bcast(snopt_status, 1, MPI_INTEGER, 0, &
                  comm(mycid), ierr)

#if 0
   idle_time = mpi_wtime() - idle_time
   print *, myrank_glob, 'INFO: Idle time = ', idle_time
!   call mpi_reduce(idle_time, tot_time, 1, MPI_DOUBLE_PRECISION, &
!                   MPI_SUM, 0, HYBROPT_COMM, ierr)
!   if (myrank_glob == 0) print *, mycid, 'INFO: Total idle time = ', tot_time
#endif

#if 0
   !-- Deallocate arrays
   if(allocated(tmpcw)) deallocate(tmpcw)
   if(allocated(tmpiw)) deallocate(tmpiw)
   if(allocated(tmprw)) deallocate(tmprw)
   if(allocated(cw)) deallocate(cw)
   if(allocated(iw)) deallocate(iw)
   if(allocated(rw)) deallocate(rw)
   if(allocated(Xstate)) deallocate(Xstate)
   if(allocated(Fstate)) deallocate(Fstate)
   if(allocated(Grad)) deallocate(Grad)
#endif

   return

end subroutine
!
! =====================================================================
!
subroutine usrfun_test(stat, nvar, dv, needf, nF, f, needG, lenG, G,&
                       cu, lencu, iu, leniu, ru, lenru)
! ---------------------------------------------------------------------
!
! Purpose:
!    This subroutine was designed to avoid crashing the program if
!    there is a problem with satisfying linear constraints. 
!
!    By virtue of entering this subroutine, we conclude that SNOPT
!    does not have complaints about the problem setup. All we do here
!    is set stat to -2 and return.
!
! ---------------------------------------------------------------------
! ___________________________________
! variable specification

   !-- subroutine arguments
   integer, intent(in) :: nvar, needf, nF, needG, lenG, lencu, leniu, &
                          lenru
   real(kind=dp), intent(in) :: dv(nvar)
   character(len=8), intent(inout) :: cu(lencu)
   integer, intent(inout) :: stat, iu(leniu)
   real(kind=dp), intent(inout) :: f(nF), G(lenG), ru(lenru)

! ___________________________________
! begin main execution

   stat = -2
   return

end subroutine
!
! =====================================================================
!
subroutine runSnoptMLE(theta, usrfun)
!
! ---------------------------------------------------------------------
!  
! Purpose: Performs Maximum Likelihood Estimator (MLE) optimization
!          problem, using SNOPT. This is done for constructing 
!          Kriging response surfaces.
!
! ---------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   real(kind=dp) :: theta
   interface
      subroutine usrfun(Stat, nvar,dv,needf, nF, f, needG, &
                        lenG, G, cu, lencu, iu, leniu, ru, &
                        lenru)
         integer :: nvar, needf, nF, needG, lenG, lencu, &
                    leniu, lenru, Stat
         character(len=8) :: cu(lencu)
         integer          :: iu(leniu)
         real(kind(1.d0)) :: dv(nvar), f(nF), G(lenG), ru(lenru)
      end subroutine
   end interface

   !-- local variables
   integer :: Start, ObjRow, nxname, nFname, &
              lencw, leniw, lenrw, &
              ltmpcw, ltmpiw, ltmprw, &
              mincw, miniw, minrw, &
              nS, nInf, neA, neG, nxmle, nfmle, &
              Stat, info, iPrint, iSumm, &
              needF, needG, ii, nnzLinGMLE, nnzGradMLE, &
              snopt_status
   real(kind=dp) :: X(1), Fnc(1), ObjAdd, sInf
   character(len=8), allocatable :: cw(:), tmpcw(:)
   integer, allocatable :: iw(:), tmpiw(:)
   real(kind=dp), allocatable :: rw(:), tmprw(:)
   character(len=8) :: xnames(1), Fnames(1), ProbName
   integer :: iGfunMLE(1), jGvarMLE(1), iLfunMLE(1), jLvarMLE(1), &
              Xstate(1), Fstate(1)
   real(kind=dp) :: Grad(1), LinGMLE(1), XuppMLE(1), XlowMLE(1), &
                    FuppMLE(1), FlowMLE(1), XmulMLE(1), FmulMLE(1)

! _____________________
! begin main execution

   X(1) = theta
   
   Start = 0
   nxname = 1
   nFname = 1
   ObjRow = 1
   ObjAdd = 0.0d0
   needF = 1
   needG = 1
   nnzLinGMLE = 1
   neA = 0
   nnzGradMLE = 1
   neG = 1
   nfmle = 1
   nxmle = 1
   ProbName = 'mleprobl'
   XlowMLE(1) = 1.0d-6
   XuppMLE(1) = infBnd
   iGfunMLE(1) = 1
   jGvarMLE(1) = 1

   !-- Allocate temporary arrays
   ltmpcw = 500
   ltmpiw = 500
   ltmprw = 500
   allocate(tmpcw(ltmpcw),tmpiw(ltmpiw),tmprw(ltmprw))

   iPrint = -1
   iSumm = -1
   !iPrint = iPrintC
   !iSumm = iSummC

   !-- Initialize SNOPT
   call snInit(iPrint, iSumm, tmpcw,ltmpcw,tmpiw,ltmpiw, tmprw,ltmprw)

   !-- Set SNOPT options
   ierr = 0
   call snSet('Hessian full memory', &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSet('LU rook pivoting', &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSetr('Major optimality tolerance', 1.0d-8, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSeti('Superbasics limit', 2, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSeti('Iterations limit', 1000000, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSeti('Major iterations limit', 1000, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   call snSeti('Minor iterations limit',1500, &
        iPrint, iSumm, ierr,tmpcw,ltmpcw,tmpiw,ltmpiw, &
        tmprw,ltmprw)

   !-- Find the neccessary sizes for cw, iw, and rw
   call snMemA(info, nfmle, nxmle, 1, 1, nnzLinGMLE, nnzGradMLE, &
               lencw, leniw, lenrw, &
               tmpcw, ltmpcw, tmpiw, ltmpiw, tmprw,ltmprw)
   if (info /= 104) then
      write(*,*) myrank_glob, &
          ': Error in Hybropt_Mod:runSNOPT :: ', &
          'problem calling snMemA'
     call mpi_abort(HYBROPT_COMM,0,ierr)
   endif
   info = 0

   lencw = (leniw + lenrw)*2

   !-- Let SNOPT know the storage will be increased
   ierr = 0
   call snSeti('Total character workspace', lencw, &
        iPrint, iSumm, ierr, tmpcw, ltmpcw, tmpiw, ltmpiw, &
        tmprw, ltmprw)

   call snSeti('Total integer workspace',leniw, &
        iPrint, iSumm, ierr, tmpcw, ltmpcw, tmpiw, ltmpiw, &
        tmprw, ltmprw)

   call snSeti('Total real workspace',lenrw, &
        iPrint, iSumm, ierr, tmpcw, ltmpcw, tmpiw, ltmpiw, &
        tmprw, ltmprw)

   if (ierr > 0) then
     write(*,*) myrank_glob, &
          ': Error in Hybopt_Mod:runSNOPT :: ', &
          'error setting SNOPT options'
     call mpi_abort(HYBROPT_COMM,0,ierr)
   end if

   !-- Broadcast neccessary sizes for cw, iw, and rw
   call MPI_Bcast(lencw, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call MPI_Bcast(leniw, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call MPI_Bcast(lenrw, 1, MPI_INTEGER, 0, comm(mycid), ierr)

   ! allocate arrays
   allocate(cw(lencw), iw(leniw), rw(lenrw))

   ! copy values from tmp arrays
   cw(1:ltmpcw) = tmpcw(1:ltmpcw)
   iw(1:ltmpiw) = tmpiw(1:ltmpiw)
   rw(1:ltmprw) = tmprw(1:ltmprw)

   ! set variables that are never changed / used
   Xstate(:) = 0
   Fstate(:) = 0

   call snOptA(Start, & ! Start = 0 always
               nfmle, & ! number of problem functions in F(x)
               nxmle, & ! number of design variables
               nxname, & ! nxname = 1 always
               nFname, & ! nFname = 1 always
               ObjAdd, & ! ObjAdd = 0.0d0 always
               ObjRow, & ! ObjRow = 1 always
               ProbName, & ! Problem name
               usrfun, & ! usrfun subroutine handle
               iLfunMLE, & ! iAfun function/constraint index in A
               jLvarMLE, & ! jAvar variable index in A
               nnzLinGMLE, & ! lenA
               neA, & 
               LinGMLE, & ! linear matrix A
               iGfunMLE, & ! i-coords in non-linear matrix G
               jGvarMLE, & ! j-coords in non-linear matrix G
               nnzGradMLE, & ! lenG
               neG, & ! always same as lenG
               XlowMLE, & ! vector of lower bounds on dv's
               XuppMLE, & ! vector of upper bounds on dv's
               xnames, & ! array of length 1 always
               FlowMLE, & ! always negative infinity
               FuppMLE, & ! always positive infinity
               Fnames, & ! array of length 1 always
               X, & ! initial guess
               Xstate, & ! always array of zeros
               XmulMLE, & ! 
               Fnc, & ! function/constraint
               Fstate, & ! always zeros
               FmulMLE, & ! always zeros
               snopt_status, & ! status of optimization process
               mincw, & ! size of cw workspace needed 
               miniw, & ! size of iw workspace needed
               minrw, & ! size of rw workspace needed
               nS, & ! final number of superbasic variables
               nInf, & ! number of infeasibilities
               sInf, & ! sum of infeasibilities
               cw, &
               lencw, &
               iw, &
               leniw, &
               rw, &
               lenrw, &
               cw, &
               lencw, &
               iw, &
               leniw, &
               rw, &
               lenrw)

   !-- Deallocate arrays
   deallocate(tmpcw, tmpiw, tmprw, cw, iw, rw)

end subroutine
!
! =====================================================================
!
end module

