           jte=jtail1
           xte=x(jte,1)
           yte=y(jte,1)
           dist=0.d0
           k=kbegin
           do j=jtail1,jtail2
             dx=x(j,k)-xte
             dy=y(j,k)-yte
             dist2=sqrt(dx**2+dy**2)
             if (dist2.gt.dist) then
               dist=dist2
               jle=j
               xle=x(j,k)
             endif
           enddo
c
           scal_beg=0.d0
           scal_end=1.d0
c
           xt=max(transup-.1d0*ramptran,0.d0)
           dx=ramptran
           a=-2.d0/dx**3
           b= 3.d0/dx**2
 222       do 477 k=kbegin,kend
c             xt=x(jrampup1,k)
c             dx=x(jrampup2+1,k)-xt
c             a12=dx**2
c             a11=dx*a12
c             a21=3.d0*a12
c             a22=2.d0*dx
c             det=a11*a22-a12*a21
c             a=-(-a22*scal_end + a12*scal_beg)/det
c             b=-(a21*scal_end - a11*scal_beg)/det
             do 476 j=jle,jup
               xx=x(j,k)
               if (xx.lt.xt) then
                 scal=0.d0
               elseif (xx.ge.xt+dx) then
                 scal=1.d0
               else
                 scal=(a*(xx-xt) + b)*(xx-xt)**2
               endif
               turmu(j,k)=scal*turmu(j,k)
c               if (k.eq.kbegin) write(99,556) j,xx,scal
 556           format(i4,2f20.8)
 476         continue
 477       continue

           xt=max(translo-.1d0*ramptran,0.d0)
           dx=ramptran
           a=-2.d0/dx**3
           b= 3.d0/dx**2
           do 479 k=kbegin,kend
c             a12=dx**2
c             a11=dx*a12
c             a21=3.d0*a12
c             a22=2.d0*dx
c             det=a11*a22-a12*a21
c             a=-(-a22*scal_end + a12*scal_beg)/det
c             b=-(a21*scal_end - a11*scal_beg)/det
             do 478 j=jlow,jle-1
               xx=x(j,k)
               if (xx.lt.xt) then
                 scal=0.d0
               elseif (xx.ge.xt+dx) then
                 scal=1.d0
               else
                 scal=(a*(xx-xt) + b)*(xx-xt)**2
               endif
c               turmu(j,k)=scal*turmu(jramplo2-1,k)
               turmu(j,k)=scal*turmu(j,k)
 478         continue
 479       continue
