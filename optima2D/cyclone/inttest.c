#include <signal.h>

struct sigaction new_handler;
struct sigaction old_handler;

int stop_test_set = 0;

int abort_signal = 0;

void sighandle(int stupid) {
  abort_signal = 1;
  if (stop_test_set) {
    sigaction(SIGINT, &old_handler,0);
    stop_test_set = 0;
  }
}

void init_ist_() {
  abort_signal = 0;
  new_handler.sa_flags = 0;
  new_handler.sa_handler = sighandle;
  sigaction(SIGINT,&new_handler,&old_handler);
  stop_test_set = 1;
}

void rm_ist_() {
  if (stop_test_set) {
    sigaction(SIGINT, &old_handler,0);
    stop_test_set = 0;
  }
}

void check_stop_(int *stop_now) {
  if (abort_signal) {
    *stop_now = 1;
  }
  else {
    *stop_now = 0;
  }
}

  
