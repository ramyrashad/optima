      subroutine xi_adv(jmax,kmax,jlow,jup,klow,kup,
     &                                             uu,turre,bx,cx,dx,fy)
c     calling subroutine: spalart_allmaras
c
      dimension uu(jmax,kmax),fy(jmax,kmax),turre(jmax,kmax)
      dimension bx(kmax,jmax),cx(kmax,jmax),dx(kmax,jmax)
c
c     *********************
c     Advective terms in xi
c     *********************
c     
c     -first-order upwinding/downwinding
      do k=klow,kup
      do j=jlow,jup
         sgnu = sign(1.,uu(j,k))
         app  = .5*(1.+sgnu)
         apm  = .5*(1.-sgnu)
         fy(j,k)= fy(j,k) - 
     &        uu(j,k)*(app*(turre(j,k)  -turre(j-1,k))
     &                +apm*(turre(j+1,k)-turre(j,k)) )
         bx(k,j)   = bx(k,j)   - uu(j,k)*app
         cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
         dx(k,j)   = dx(k,j)   + uu(j,k)*apm
      enddo
      enddo

      return
      end
