      subroutine blomax(jdim,kdim,q,press,vort,turmu,fmu,xy,xyj)         
c     parameter (maxj=497, maxk=97)                                     
      include '../include/arcom.inc'
c                                                                       
c                                                                       
      dimension q(jdim,kdim,4),turmu(jdim,kdim),vort(jdim,kdim)         
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)         
      dimension fmu(jdim,kdim)                                          
c                                                                       
      common/worksp/snor(maxj),tmo(maxj),tmi(maxj),uu(maxj),            
     *     tas(maxj),work(maxj,87)                                      
c                                                                       
      common/turout/tauh(maxj), ustar(maxj),                            
     >              yplus(maxj, maxk), uplus(maxj, maxk)                
c                                                                       
                                                                        
c                                                                       
      data f27 /1.6/                                                    
      data fk,fkk,ydumf /0.4,0.0168,1.0/                                
      data fkleb /0.3/                                                  
      data fmutm/14./                                                   
c                                                                       
c  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
c  ******************  turmu comes in with vorticity *****************  
c  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
c                                                                       
c                                                                       
      if(turbulnt) then                                                 
c                                                                       
         kedge = .75*kend                                               
c                                                                       
c  note xyj(j,k) stores transform jacobian by which dep vars are divided
c                                                                       
      do 80 j = jlow,jup                                                
c                                                                       
c     find vorticity tas(k) and total velocity utot(k)                  
c                                                                       
        do 11 k = kbegin,kup                                            
         utot       = q(j,k,2)**2 + q(j,k,3)**2                         
         utot       = sqrt(utot)/q(j,k,1)                               
         uu(k)      = utot                                              
         tas(k)     = vort(j,k)                                         
         snor(k)    = 0.0                                               
         turmu(j,k) = 0.0                                               
   11   continue                                                        
c                                                                       
c      compute ra                                                       
c                                                                       
c                                                                       
        k=kbegin                                                        
        wmu = .5*( fmu(j,k) + fmu(j,k+1))                               
        tau = abs(tas(k))                                               
        rhoav = 0.5*(xyj(j,k)*q(j,k,1) + xyj(j,k+1)*q(j,k+1,1))         
        ra = sqrt( re*rhoav*tau/wmu)/26.                                
        if(.not.cmesh .and. .not.periodic)then                          
        if(j .lt. jtail1 .or. j .gt. jtail2) ra = 1000.*ra              
        endif                                                           
c                                                                       
c      compute normal distance snor(k) and y*dudy                       
c                                                                       
        snor(1) = 0.                                                    
        ydum = 0.0                                                      
        umin = uu(1)                                                    
        ydus = 0.0                                                      
        do 20 k = klow,kup                                              
         if(uu(k) .lt. umin) umin = uu(k)                               
         scis = abs(xy(j,k-1,3)*xy(j,k,3)+xy(j,k-1,4)*xy(j,k,4))        
         scal = 1.0/sqrt(scis)                                          
         snor(k) = snor(k-1) + scal                                     
         if(k .gt. 2) go to 18                                          
           km2 = 1                                                      
           ydum = 1.e-3                                                 
           um = ydum                                                    
           ym = 0.5*snor(2)                                             
   18   continue                                                        
        snora = 0.5*(snor(k) + snor(k-1))                               
        ydu = snora*abs(tas(k-1))*(1.-exp(-ra*snora))  
        if(k .gt. kedge) go to 20                                       
          if(ydu .lt. ydum) go to 20                                    
            km2 = k - 1                                                 
            ydum = ydu                                                  
            um = 0.5*(uu(k-1) + uu(k))                                  
            ym = snora                                                  
   20   continue                                                        
c                                                                       
c     interpolate to find ym, ydum, and um                              
c                                                                       
        if(km2 .lt. 2 .or. km2 .gt. kedge-1) go to 22                   
          ym3 = 0.5*(snor(km2+1) + snor(km2+2))                         
          ym1 = 0.5*(snor (km2-1) + snor (km2))                         
          ydum1 = ym1*abs(tas(km2-1))*(1.0 - exp(-ra*ym1))              
          ydum3 = ym3*abs(tas(km2+1))*(1.0 - exp(-ra*ym3))              
          c2 = ydum - ydum1                                             
          c3 = ydum3 - ydum                                             
          dy1 = ym - ym1                                                
          dy3 = ym3 - ym                                                
          am = (dy3*dy3*c2 + dy1*dy1*c3)/(dy1*dy3*(dy1+dy3))            
          bm = (dy1*c3 - dy3*c2)/(dy1*dy3*(dy1 + dy3))                  
          if(bm .ge. 0.) go to 22                                       
            ymm = ym - 0.5*am/bm                                        
            ydu  = ydum - 0.25*am*am/bm                                 
            ymm = ym - 0.5*am/bm                                        
            if(ydu .lt. ydum .or. ymm .lt. ym1 .or. ymm .gt. ym3)       
     1       go to 22                                                   
               ydum = ydu                                               
               ym = ymm                                                 
               if(ym .gt. snor(km2+1)) km2 = km2 + 1                    
               if(ym .lt. snor(km2)) km2 = km2 - 1                      
               um= ((snor(km2+1) - ym)*uu(km2)+(ym-snor(km2))*uu(km2+1))
     1            /(snor(km2+1) - snor(km2))                            
   22 continue                                                          
c                                                                       
c      compute outer eddy viscosity                                     
c                                                                       
        do 25 k=1,kedge                                                 
           snor(k) = 0.5*(snor(k) + snor(k+1))                          
           rhoav = 0.5*(xyj(j,k)*q(j,k,1) + xyj(j,k+1)*q(j,k+1,1))      
           ffc = fkk*f27*re*rhoav                                       
           tmo(k) = ffc*ym*ydum                                         
           ffcwk = ydumf*ydumf*ffc                                      
           udiff = abs(um - umin)                                       
           if(ydum .gt. udiff*ydumf) tmo(k) = ffcwk*ym*udiff*udiff/ydum 
           fia = fkleb*snor(k)/ym                                       
           if(fia .gt. 1.e5) fia = 1.e5                                 
           fi = 1.0 + 5.5*fia**6                                        
           tmo(k) = tmo(k)/fi                                           
           tmo(k) = abs(tmo(k))                                         
   25   continue                                                        
c                                                                       
c      compute inner eddy viscosity                                     
c                                                                       
        do 30 k=1,kedge                                                 
         tau = abs(tas(k))                                              
         rhoav = 0.5*(xyj(j,k)*q(j,k,1) + xyj(j,k+1)*q(j,k+1,1))        
         tmi(k) =rhoav*re*tau*(fk*snor(k)*(1.-exp(-ra*snor(k))))**2     
         tmi(k) = abs(tmi(k))                                           
   30   continue                                                        
c                                                                       
c      load viscosity coeffs. into array, use inner value until         
c      match point is reached                                           
c                                                                       
        k = 1                                                           
   40   turmu(j,k) = tmi(k)                                             
        k = k +1                                                        
        if( k.gt. kedge) go to 10                                       
          if( tmi(k) .le. tmo(k)) go to 40                              
   41     turmu(j,k) = tmo(k)                                           
          k = k +1                                                      
          if( k.le. kedge) go to 41                                     
   10   continue                                                        
c                                                                       
80    continue                                                          
c                                                                       
c  transition model truned off for now                                  
c  transition not computed but fixed from input in terms of % chord     
c                                                                       
       if(translo.ne.0.0)then                                           
c                                                                       
c  zero turmu from jtranlo to jtranup                                   
c                                                                       
c    for cmesh logic shift jtranlo and jtranup bu jtail1-1              
c                                                                       
       jtlo = jtranlo                                                   
       jtup = jtranup                                                   
         if(cmesh)then                                                  
           jtlo = jtranlo-jtail1+1                                      
           jtup = jtranup-jtail1+1                                      
         endif                                                          
c                                                                       
         do 455 j = jtlo,jtup                                           
         do 455 k = kbegin,kend                                         
          turmu(j,k) = 0.                                               
455      continue                                                       
c                                                                       
c  thp 4/2/86  put in linear ramp for transition                        
c                                                                       
         if(jtup-jtlo .gt. 4)then                                       
         do 466 k = kbegin,kend                                         
           turmu(jtlo,k) = 0.5*turmu(jtlo-1,k)                          
           turmu(jtup,k) = 0.5*turmu(jtup+1,k)                          
466       continue                                                      
         endif                                                          
c                                                                       
      endif                                                             
c                                                                       
c  if not turbulnt set turmu = 0.0                                      
c                                                                       
      else                                                              
          do 800 k = kbegin,kend                                        
          do 800 j = jbegin,jend                                        
          turmu(j,k) = 0.0                                              
800       continue                                                      
c                                                                       
      endif                                                             
c                                                                       
      if(.not.bcairf)then                                               
c                                                                       
c  for flat plate set eddy viscosity to zero                            
c                                                                       
         do 885 k = kbegin, kend                                        
         do 885 j = jbegin, jtail1                                      
           turmu(j, k) = 0.0                                            
885      continue                                                       
      endif                                                             
c                                                                       
 
      end                                                             
