#ifndef TAGSORT_H
#include "tagSort.h"
#endif

void tagSort(vector <int>& dataArr, vector <int>& tagArr, const int& 
size)
{
    int k, indexOfMin, pass;

    //for (k = 0; k < size; k++)
    //    tagArr[k] = k;

    for (pass = 0; pass < size - 1; pass++)
    {
        indexOfMin = pass;

        for (k = pass + 1; k < size; k++)
            if (dataArr[ tagArr[k] ] < dataArr[ tagArr[indexOfMin] ])
                indexOfMin = k;

        if (indexOfMin != pass)
            swap2 ( &tagArr[pass], &tagArr[indexOfMin] );
    }
}// end tagSort( )


void tagSort(vector <int>& dataArr, int *tagArr, const int* size)
{
    int k, indexOfMin, pass;

    for (pass = 0; pass < *size - 1; pass++)
    {
        indexOfMin = pass;

        for (k = pass + 1; k < *size; k++)
            if (dataArr[ tagArr[k] ] < dataArr[ tagArr[indexOfMin] ])
                indexOfMin = k;

        if (indexOfMin != pass)
            swap2 ( &tagArr[pass], &tagArr[indexOfMin] );
    }
}// end tagSort( )



void tagSort(const int* dataArr, int *tagArr, const int& size)
{
    int k, indexOfMin, pass;
    for (k = 0; k < size; k++)
        tagArr[k] = k;

    for (pass = 0; pass < size - 1; pass++)
    {
        indexOfMin = pass;

        for (k = pass + 1; k < size; k++)
	  if (dataArr[ tagArr[k] ] < dataArr[tagArr[indexOfMin]])
                indexOfMin = k;

        if (indexOfMin != pass)
            swap2 ( &tagArr[pass], &tagArr[indexOfMin] );
    }
}// end tagSort( )



// swap function defined for integers
void swap2 ( int* x, int* y )
{
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;
}

