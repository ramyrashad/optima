#ifndef TEC_H
#include "write_tecplot.h"
#endif

void write_tecplot (const vector <Node>& nodes, const vector <Face>& elems)
{
  int i, j;

  i = nodes.size();
  j = elems.size();

  ofstream fout ("gridout_tec.dat");
  ofstream fout2 ("gridin_tec.dat");
  fout << "VARIABLES = \"X\", \"Y\"" << endl;
  fout << "ZONE F = FEPOINT, ET = QUADRILATERAL,N= " << i << " ,E= " 
       << j << endl;
  fout2 << "VARIABLES = \"X\", \"Y\"" << endl;
  fout2 << "ZONE F = FEPOINT, ET = QUADRILATERAL,N= " << i << " ,E= " 
	<< j << endl;

  double* xy;
  int* ind;
  int n, ii;

  for (j=0; j<i; j++)
    {
      xy = nodes[j].get_coord();
      fout << setw(15) << xy[0] << setw(15) << xy[1] << endl;
      xy = nodes[j].get_xyz();
      fout2 << setw(15) << xy[0] << setw(15) << xy[1] << endl;
      
    }

  i = elems.size();
  for (j=0; j<i; j++)
    {
      ind = elems[j].get_vertexID();
      n = elems[j].get_nvertices();
      
      for (ii=0; ii<n; ii++)
	{
	  fout << setw(8) << ind[ii]+1;
	  fout2 << setw(8) << ind[ii]+1;
	}
      if (n==3)
	{
	  fout << setw(8) << ind[0]+1;
	  fout2 << setw(8) << ind[0]+1;
	}
      fout << endl;
      fout2 << endl;
    }

  fout.close();
  fout2.close();
  xy = NULL;
  ind = NULL;
  
  return;
}


void write_tecplot (const vector <Node>& nodes, const vector <Vol>& elems)
{
  int i, j;

  i = nodes.size();
  j = elems.size();

  ofstream fout ("gridout_tec.dat");
  ofstream fout2 ("gridin_tec.dat");
  fout << "VARIABLES = \"X\", \"Y\", \"Z\" " << endl;
  fout << "ZONE F = FEPOINT, ET = TETRAHEDRON, N= " << i << " ,E= " 
       << j << endl;
  fout2 << "VARIABLES = \"X\", \"Y\", \"Z\" " << endl;
  fout2 << "ZONE F = FEPOINT, ET = TETRAHEDRON, N= " << i << " ,E= " 
	<< j << endl;

  double* xy;
  int* ind;
  int n, ii;

  for (j=0; j<i; j++)
    {
      xy = nodes[j].get_coord();
      fout << setw(15) << xy[0] << setw(15) << xy[1] 
	   << setw(15) << xy[2] << endl;
      xy = nodes[j].get_xyz();
      fout2 << setw(15) << xy[0] << setw(15) << xy[1] 
	    << setw(15) << xy[2] << endl;
    }

  i = elems.size();
  for (j=0; j<i; j++)
    {
      ind = elems[j].get_vertexID();
      n = elems[j].get_nvertices();
      
      for (ii=0; ii<n; ii++)
	{
	  fout << setw(8) << ind[ii]+1;
	  fout2 << setw(8) << ind[ii]+1;
	}
      fout << endl;
      fout2 << endl;
    }

  fout.close();
  fout2.close();
  xy = NULL;
  ind = NULL;
  
  return;
}
