      subroutine bcplate(jdim,kdim,q,press,sndsp,xy,xit,ett,xyj,x,y)    
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)        
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c                                                                       
      dimension a(maxj),b(maxj),c(maxj),f(maxj),g(maxj)
      dimension z(maxj),work(maxj,24)
      common/worksp/a,b,c,f,g,z,work
c                                                                       
      t = float(numiter -1)/ strtit
      if( t .gt.1.) t = 1.                                              
      scal = (10. -15.*t + 6.*t*t) *t**3                                
c                                                                       
c                no scaling if this is a restart                        
c                                                                       
      if( restart ) scal=1.                                             
c                                                                       
c                                                                       
c                                                                       
      foso = 0.                                                         
      k=1                                                               
c                                                                       
c                viscous velocity bc at body                            
c                                                                       
         do 10 j=jtail1, jtail2                                         
c                                                                       
            u1 = ( 1. - scal)*q(j,1,2)/q(j,1,1)                         
            v1 = ( 1. - scal)*q(j,1,3)/q(j,1,1)                         
            q(j,1,2) = u1*q(j,1,1)                                      
            q(j,1,3) = v1*q(j,1,1)                                      
c                                                                       
   10    continue                                                       
c                                                                       
c                                                                       
c                   symmetry condition forward of plate                 
c                   cartesian v = 0.0, extrap rho,u,e                   
c                                                                       
      fosor = 0.0                                                       
      fosou = 0.0                                                       
      fosop = 0.0                                                       
      do 20 j=1,jtail1-1                                                
        k   = 1                                                         
        kk  = 2                                                         
        kkk = 3                                                         
        p2 = gami*( q(j,2,4) -.5*(q(j,2,2)**2+q(j,2,3)**2) /q(j,2,1) )  
        p3 = gami*( q(j,3,4) -.5*(q(j,3,2)**2+q(j,3,3)**2) /q(j,3,1) )  
        q(j,k,3)   = 0.                                                 
        q(j,k,1)   = ((1.+fosor)*q(j,kk,1)*xyj(j,kk)                    
     >             - fosor*q(j,kkk,1)*xyj(j,kkk))/xyj(j,k)              
        q(j,k,2)     = ((1.+fosou)*q(j,kk,2)/q(j,kk,1)                  
     >             - fosou*q(j,kkk,2)/q(j,kkk,1))*q(j,k,1)              
        p1      = ((1.+fosop)*p2*xyj(j,kk)                              
     >             - fosop*p3*xyj(j,kkk))/xyj(j,k)                      
        q(j,k,4)   = p1/gami + .5*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)    
 20   continue                                                          
c                                                                       
c             extrap p                                                  
c                                                                       
      fosop = 0.0                                                       
      k  = 1                                                            
      kk = 2                                                            
      kkk= 3                                                            
      do 40 j=jtail1,jmax                                               
         p2 = gami*( q(j,2,4) -.5*(q(j,2,2)**2+q(j,2,3)**2) /q(j,2,1) ) 
         p3 = gami*( q(j,3,4) -.5*(q(j,3,2)**2+q(j,3,3)**2) /q(j,3,1) ) 
         f(j)      = ((1.+fosop)*p2*xyj(j,kk)                           
     >             - fosop*p3*xyj(j,kkk))/xyj(j,k)                      
   40 continue                                                          
      k   = 1                                                           
      kk  = 2                                                           
      kkk = 3                                                           
      foso2 = 0.0                                                       
      fosor = 0.                                                        
      do 15 j=jtail1,jmax                                               
        if(wtrat .eq. 0.0)then                                          
         q(j,k,1)   = ((1.+fosor)*q(j,kk,1)*xyj(j,kk)                   
     >             - fosor*q(j,kkk,1)*xyj(j,kkk))/xyj(j,k)              
        else                                                            
         twall = wtrat/gamma                                            
         p1 = gami*( q(j,1,4) -.5*(q(j,1,2)**2+q(j,1,3)**2) /q(j,1,1) ) 
         q(j,k,1)   =   p1/twall                                        
        endif                                                           
  15  continue                                                          
      do 50 j = jtail1, jmax                                            
c                                                                       
        rinver = 1./q(j,k,1)                                            
        rj = 1./xyj(j,k)                                                
        u = q(j,k,2)*rinver                                             
        v = q(j,k,3)*rinver                                             
        q(j,k,1) = ((1.+foso2)*q(j,kk,1)*xyj(j,kk)                      
     >             - foso2*q(j,kkk,1)*xyj(j,kkk))/xyj(j,k)              
        q(j,k,2) = u*q(j,k,1)                                           
        q(j,k,3) = v*q(j,k,1)                                           
c        q(j,k,4) = ((1.+foso2)*q(j,kk,4)*xyj(j,kk)                     
c     >             - foso2*q(j,kkk,4)*xyj(j,kkk))/xyj(j,k)             
        q(j,k,4) = f(j)/gami +                                          
     >       .5 * ( q(j,k,2)**2+q(j,k,3)**2) /q(j,k,1)                  
   50    continue                                                       
c                                                                       
c        *****   far-field stuff  *****                                 
c                                                                       
c             top boundary                                              
c                                                                       
c                                                                       
         pi = 4.*atan(1.)                                               
         rinf = 1.                                                      
         uinf = fsmach*cos(alpha*pi/180.)                               
         vinf = fsmach*sin(alpha*pi/180.)                               
         einf = 1./(gamma*gami) + .5*fsmach**2                          
         pinf = 1./gamma                                                
         ainf = sqrt(gamma*pinf/rinf)                                   
         hstfs = 1./gami + 0.5*fsmach**2                                
         gm1i  = 1./gami                                                
         gi    = 1./gamma                                               
         k = kmax                                                       
         if(fsmach.lt.1.0)then                                          
c                                                                       
c                         subsonic freestream                           
c                                                                       
      do 60 j = 2,jm                                                    
         uf = uinf                                                      
         vf = vinf                                                      
         af2 = gami*(hstfs - .5*(uf**2+vf**2))                          
         af = sqrt(af2)                                                 
c                                                                       
c              choose a reference frame in terms of normal and          
c              tangential components                                    
c                                                                       
c                   metric terms                                        
c                                                                       
      snorm = 1./sqrt(xy(j,k,3)**2+xy(j,k,4)**2)                        
      xy3h = xy(j,k,3)*snorm                                            
      xy4h = xy(j,k,4)*snorm                                            
c                                                                       
c                  check for inflow or outflow                          
c             for inflow : three variables are specified  r1 and        
c                     qt  ( q_tangential)  and   s~ r**gamma/p  (~entrop
c                     with one var. computed.  r2                       
c             for outflow : one variable is fixed r1  with three compute
c                     r2, qt and s                                      
c                                                                       
c             compute riemann invariants                                
c             fix          r1 = qn - 2.*a/(gamma-1)  at free stream     
c             compute      r2 = qn + 2.*a/(gamma-1)  from interior      
c                          extrapolation of flow variables              
c                                                                       
c             get extrapolated variables                                
c                                                                       
      rhoext = q(j,k-1,1)*xyj(j,k-1)                                    
      rjm1   = 1./q(j,k-1,1)                                            
      uext   = q(j,k-1,2)*rjm1                                          
      vext   = q(j,k-1,3)*rjm1                                          
      eext   = q(j,k-1,4)*xyj(j,k-1)                                    
      pext   = gami*(eext - 0.5*rhoext*(uext**2+vext**2))               
c                                                                       
c               set riemann invariants                                  
c                                                                       
      r1 = xy3h*uf + xy4h*vf - 2.*af*gm1i                               
      r2 = xy3h*uext + xy4h*vext + 2.*sqrt(gamma*pext/rhoext)*gm1i      
c                                                                       
      qn = (r1 + r2)*0.5                                                
      cspe = (r2 - r1)*gami*0.25                                        
      c2 = cspe**2                                                      
c                                                                       
c               set other fixed or extrapolated variables               
c                                                                       
           if(qn .le. 0.0)then                                          
           qt = xy4h*uf - xy3h*vf                                       
           entro = gamma                                                
           else                                                         
           qt = xy4h*uext - xy3h*vext                                   
           entro = rhoext**gamma/pext                                   
           endif                                                        
c                                                                       
c              compute flow variables                                   
c                                                                       
      u = xy3h*qn + xy4h*qt                                             
      v = xy4h*qn - xy3h*qt                                             
c                                                                       
      q(j,k,1) = (c2*entro*gi)**gm1i                                    
      pres = c2*q(j,k,1)*gi                                             
c                                                                       
c              add jacobian                                             
c                                                                       
          rjj = 1./xyj(j,k)                                             
          q(j,k,1) = q(j,k,1)*rjj                                       
          q(j,k,2) = q(j,k,1)*u                                         
          q(j,k,3) = q(j,k,1)*v                                         
          q(j,k,4) = pres*gm1i*rjj + 0.5*q(j,k,1)*(u**2+v**2)           
60    continue                                                          
      else                                                              
c                                                                       
c                         supersonic freestream                         
c                                                                       
      k = kmax                                                          
      do 70 j = 2,jm                                                    
c                                                                       
c               choose a reference frame in terms of normal and         
c               tangential components                                   
c                                                                       
c                                                                       
c               metric terms                                            
c                                                                       
c          use extrapolated qn to determine inflow/outflow              
c                                                                       
      rhoinv = 1./q(j,k-1,1)                                            
      u = q(j,k-1,2)*rhoinv                                             
      v = q(j,k-1,3)*rhoinv                                             
      snorm = 1./sqrt(xy(j,k-1,3)**2+xy(j,k-1,4)**2)                    
      xy3h = xy(j,k-1,3)*snorm                                          
      xy4h = xy(j,k-1,4)*snorm                                          
      qn1ext = xy3h*u + xy4h*v                                          
      rhoinv = 1./q(j,k-2,1)                                            
      u = q(j,k-2,2)*rhoinv                                             
      v = q(j,k-2,3)*rhoinv                                             
      snorm = 1./sqrt(xy(j,k-2,3)**2+xy(j,k-2,4)**2)                    
      xy3h = xy(j,k-2,3)*snorm                                          
      xy4h = xy(j,k-2,4)*snorm                                          
      qn2ext = xy3h*u + xy4h*v                                          
ctjb      qnext = 2.*qnext1 - qnext2                                    
      qnext = qn1ext                                                    
c                                                                       
           if(qnext.gt.0.)then                                          
           rmet1 = xyj(j,k-1)/xyj(j,k)                                  
           rmet2 = xyj(j,k-2)/xyj(j,k)                                  
           do 65 n = 1,4                                                
             q(j,k,n) = 2.*q(j,k-1,n)*rmet1-q(j,k-2,n)*rmet2            
65         continue                                                     
           endif                                                        
70    continue                                                          
      endif                                                             
c                                                                       
c                  front boundary                                       
c                                                                       
          if ( fsmach .lt. 1.)then                                      
          j  = 1                                                        
          do 80 k = 1,kmax                                              
c                                                                       
c  extrapolate pressure                                                 
c                                                                       
          p2 = gami*(q(2,k,4) -.5*(q(2,k,2)**2+q(2,k,3)**2)/q(2,k,1))   
          p1 = p2*xyj(2,k)/xyj(j,k)                                     
          q(j,k,4) = p1/gami +                                          
     >       .5 * ( q(j,k,2)**2+q(j,k,3)**2) /q(j,k,1)                  
80        continue                                                      
         endif                                                          
c                                                                       
c                                                                       
c           back boundary                                               
c                                                                       
c                                                                       
c        extrapolate density,momentums,fix static pressure              
c                                                                       
            j=jmax                                                      
            do 200 k=2,kmax                                             
            rmet = xyj(j-1,k)/xyj(j,k)                                  
              do 200 n = 1,4                                            
               q(j,k,n) = q(j-1,k,n)*rmet                               
200            continue                                                 
          if(fsmach.lt. 1.0)then                                        
          do 201 k = 2, kmax                                            
c          p2 = gami*(q(jmax-1,k,4) -.5*(q(jmax-1,k,2)**2+              
c     *            q(jmax-1,k,3)**2)/q(jmax-1,k,1))                     
c          p1 = p2*xyj(jmax-1,k)/xyj(j,k)                               
c          q(j,k,4) = p1/gami +                                         
c     >       .5 * ( q(j,k,2)**2+q(j,k,3)**2) /q(j,k,1)                 
              q(j,k,4) = 1./gamma/gami/xyj(j,k)                         
     >       + .5*(q(j,k,2)*q(j,k,2) + q(j,k,3)*q(j,k,3))/q(j,k,1)      
201            continue                                                 
          endif                                                         
c                                                                       
      return                                                            
      end                                                               
