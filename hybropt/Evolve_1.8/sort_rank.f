C     
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      SUBROUTINE SORT_rank(irank,NOBFUN,NCHROM,FIT,ngene,pop,debug,
     $     i_mnx)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTINE PERFORMS A SORT BASED ON THE FIRST     C
C               FITNESS VALUE ***                                      C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      logical debug
      DIMENSION FIT(nobfun,nchrom),pop(ngene,nchrom),irank(nchrom)
C
      dimension ZZZ(ngene,nchrom),ZZZFIT(nobfun,nchrom),irzzz(nchrom)
C-----------------------------------------------------------------------
      if(debug)write(49,*)' In sort_rank'
C-----------------------------------------------------------------------
C
c    re-rank based on select chromosomes      
c
      call RANK_1(nchrom,nobfun,fit,irank,i_mnx)

      if(debug)then
         write(49,*)'-----------  Irank/fit before sort -----------'
         do nc = 1,nchrom
            write(49,'(i5,3(2x,e24.16))')irank(nc),
     $           (fit(no,nc),no=1,nobfun)
         enddo
      endif
C
      ITMAX  = 45
      icount = 0
      DO IT=1,ITMAX
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.IT) THEN
               icount = icount + 1
               DO NG=1,NGENE
                  ZZZ(NG,ICOUNT) = POP(NG,NC)
               ENDDO
               DO NO=1,NOBFUN
                  ZZZFIT(NO,ICOUNT) = FIT(NO,NC)
               ENDDO
               irzzz(icount) = irank(nc)
               IF (ICOUNT.GT.NCHROM) GO TO 1000
            ENDIF
         ENDDO
      ENDDO
 1000    CONTINUE

C
C Load selection into pop and fit (note fitness valid here, but not ofter operations)
      do nc = 1,nchrom
         do ng = 1,ngene
            pop(ng,nc) = zzz(ng,nc)
         enddo
         do no = 1,nobfun
            fit(no,nc) = zzzfit(no,nc)
         enddo
         irank(nc) = irzzz(nc)
      enddo
         
C          *** SORT COMPLETED ***
C-----------------------------------------------------------------------
      if(debug)then
         write(49,*)'-----------  Irank/fit after sort -----------'
         do nc = 1,nchrom
            write(49,'(i5,3(2x,e24.16))')irank(nc),
     $           (fit(no,nc),no=1,nobfun)
         enddo
      endif
C-----------------------------------------------------------------------
C
      RETURN
      END
