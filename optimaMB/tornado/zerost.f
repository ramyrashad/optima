      subroutine zerost(q,jmax,kmax,rhoinf,einf)

      implicit none
C ********************************************************************
C **     routine to set freestream values of rho, e                 **
C **     and set U & V to zero                                      **
C ********************************************************************
c calling routine:  initia
c
      double precision jmax,kmax,rhoinf,einf
      integer j,k
      double precision q(jmax,kmax,4)
c
      DO 32 K=1,KMAX                                                    
      DO 32 J=1,JMAX                                                    
        Q(J,K,1) = RHOINF                                              
        Q(J,K,2) = 0.0
        Q(J,K,3) = 0.0
        Q(J,K,4) = EINF                                                
   32 CONTINUE                                                          
      return
      end
