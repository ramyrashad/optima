c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c
c  Interface to Evolver: Genetic Algorithm
c
c  Evolver written by:
c     T. Holst
c     T. Pulliam
c     See Evolver.f for further information
c
c  This interface is written by: 
c     O. Chernukhin, July 2010
c
c  Modifications to Evolver by O. Chernukhin:
c     rank.f : removed some output statements
c     
c
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      subroutine evolvint (nFnc, ngene, genemin, genemax, 
     &                     popX, popFnc, ipop, i_ran_seed, 
     &                     nchrom, npop, iselect,  
     &                     prob_pert, prob_mut, prob_avg,  
     &                     xbest, xavg, xpert, xmut, calcFit) 

      implicit none

      !_______________________________
      !-- Variable declarations
      
      !-- Input parameters      
      external calcFit

      integer 
     &   nFnc, !-- One objective + number of constraints
     &   ipop, !-- Starting generation number (>0)
     &   i_ran_seed, !-- Random seed
     &   nchrom, !-- Population size
     &   ngene, !-- Number of design variables
     &   npop, !-- Maximum number of generations
     &   iselect !-- Selection algorithm

      double precision 
     &   prob_pert, !-- Probability of perturbation mutation
     &   prob_mut, !-- Probability of mutation mutation
     &   prob_avg, !-- Probability of crossover mutation
     &   xbest, !-- Passthrough percentage
     &   xavg, !-- Crossover percentage
     &   xpert, !-- Perturbation mutation percentage
     &   xmut !-- Pure mutation percentage

      double precision
     &   genemin(ngene), !-- Xlow vector
     &   genemax(ngene), !-- Xupp vector     
     &   popX(nchrom, ngene),
     &   popFnc(nchrom, nFnc)

      !-- Other parameters
      integer nobfun, nout, npl, npi, npr, iaccum, i_mnx, init_ga,
     &        itran, icull, nbin_cull, i_avg_cho, max_acc_dim, nacc,
     &        i_acc(2, nchrom), ncfitmin(1), ncfitmax(1),  
     &        ncrmsmin, ncrmsmax, nfuneval, nbest, navg, npert, nmut,
     &        nbin, is_bin, n_beta_s, n_beta_i
 
      logical beta_sin, debug, scale_genes, trans_minmax, io_34,
     &        fit_min_max, geneprec, arc2d_io, rand_init_fit, cbest,
     &        compute_pass_thu, box_over, compute_fit(nchrom)

      double precision fit_init, eps_domin, sig_share, beta,
     &                 beta_min, beta_max
 
      !-- Local variables 
      integer idum, ii, jj, kk, mm, imove
      integer irank(nchrom)
      double precision fit(1, nchrom), pop(ngene, nchrom), fitmin(1),
     &                 fitmax(1), fitavg(1), fitrmsmin(1), 
     &                 fitrmsmax(1), pop_acc(ngene, 10000), 
     &                 fit_acc(1, 10000)
    
      double precision rand, tmpcv

      !_________________________________ 
      !-- Begin execution

      !-- Provide default values for all parameters
      nobfun = 1 !-- we are not concerned with multiple objectives
      nfuneval = 0
      beta_sin = .false. !-- don't know
      nout = 0 !-- don't know
      npl = 1 !-- apparently something to do with ARC2D_IO
      npi = 0 !-- ARC2D_IO?
      npr = 0 !-- ARC2D_IO?
      iaccum = 0 !-- don't know
      i_mnx = 0 !-- Always a minimization problem
      fit_init = 1.0d30
      init_ga = 1 !-- Should never come into play
      debug = .false. 
      itran = 0 !-- Don't know
      scale_genes = .false. !-- Always false
      trans_minmax = .false. !-- Always false 
      icull = 0 !-- Only for multi-objective
      nbin_cull = 20 !-- Don't know
      nbin = 1 !-- Only for multi-objective
      is_bin = 1 !-- Only for multi-objective
      io_34 = .false. !-- Don't know
      fit_min_max = .false. !-- Don't know
      geneprec = .false. !-- Allows to restrict gene precision
      eps_domin = 0.0d0 !-- Only for multi-objective
      arc2d_io = .false. !-- ARC2D_IO
      rand_init_fit = .false. !-- Should never be used
      i_avg_cho = 0 !-- Don't know
      cbest = .false. !-- Doesn't matter
      compute_pass_thu = .true. !-- Doesn't matter 
      sig_share = 0.0d0 !-- Don't know
      box_over = .false. !-- Don't know
      max_acc_dim = 10000
      nacc = 0
      fitmin(1) = 0.0d0
      fitmax(1) = 0.0d0
      fitavg(1) = 0.0d0
      ncfitmin(1) = 0.0d0
      ncfitmax(1) = 0.0d0
      fitrmsmin(1) = 0.0d0
      fitrmsmax(1) = 0.0d0
      beta = 0.3d0
      beta_min = 0.3d0
      beta_max = 0.3d00
      n_beta_s = 300
      n_beta_i = 300

      !-- Initialize the random number generator
      if ( i_ran_seed .ge. 0) then
         idum = - ipop - 1 - i_ran_seed
      else
         idum = i_ran_seed
      endif
      call ran3(idum, rand)     

      !-- Calculate fitness 
      call calcfit(ngene, popX, nchrom, popFnc, nFnc, fit(1,:))

      !-- Initialize population (i.e. transpose popX)
      do ii = 1, ngene
         do jj = 1, nchrom
            pop(ii, jj) = popX(jj, ii)
         end do
      end do
      
      !-- Calculate statistics
      call stats(imove, nchrom, ngene, nobfun, pop, fit, fit_init, 
     &           i_mnx, fitmin, fitmax, fitavg, ncfitmin, ncfitmax,
     &           fitrmsmin, fitrmsmax)

      !-- Rank chromosomes
      call rank(ipop, nchrom, ngene, nobfun, pop, fit, irank, 
     &          max_acc_dim, nacc, pop_acc, fit_acc, i_acc, iaccum,
     &          i_mnx, fit_init, eps_domin, debug)

      !-- Select chromosomes
      call SELECT(ipop, nchrom, ngene, nobfun, pop, fit, irank,
     &            iselect, max_acc_dim, pop_acc, fit_acc, nacc, i_mnx,
     &            fit_init, nbin, is_bin, fitmin, fitmax, fitavg,
     &            ncfitmin, ncfitmax, ncrmsmin, ncrmsmax, fitrmsmin,
     &            fitrmsmax, debug)

      !-- SORT_rank
      call SORT_rank(irank, nobfun, nchrom, fit, ngene, pop,
     &               debug, i_mnx)

      !-- Mutation/Crossover operators
      call mut_cross(ipop, nchrom, ngene, nobfun, pop, fit, nfuneval,
     &               beta, beta_sin, fit_init, genemin, genemax,
     &               debug, io_34, beta_min, beta_max, n_beta_s,
     &               n_beta_i, box_over, prob_pert, prob_mut, prob_avg, 
     &               i_avg_cho, xbest, xavg, xpert, xmut, nbest, 
     &               navg, npert, nmut, compute_fit, cbest)

      !-- Write new design variable to array and return 
      do ii = 1, ngene
         do jj = 1, nchrom
            popX(jj, ii) = pop(ii, jj)
         end do
      end do

      return 

      end subroutine
