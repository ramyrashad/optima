c---- add_dJdG ---------------------------------------------------------
c Computes the derivative of the objective function, J, with respect to
c the grid node locations, G, and adds it to A.
c
c Here, J is considered to be defined as
c   J = J(Q, G, X)
c where Q, G, and X are independant variables.  Therefore, dJ/dG is
c evaluated with Q and X held constant (i.e. not with Q hat held constant)
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions.
c press: pressure (without the inverse of the metric jacobian included)
c Q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c xy, xyj: metrics, and metric jacobian
c x, y: x- and y-coordinates of the grid nodes
c obj0: initial objective function value
c
c Returns:
c A: with the derivative of the objective function with respect to the
c   grid added to it.
c
c Chad Oldfield, September 2005.
c-----------------------------------------------------------------------
      subroutine add_dJdG(jdim, kdim, press, Q, xy, xyj, x, y, A, obj0,
     &     den,dfdxflag)
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim
      double precision press(jdim,kdim), Q(jdim,kdim,4),
     |    xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision A(jdim,kdim,2), obj0

c     Local variables
      double precision radians, cos_alpha, sin_alpha, den
c         Conversion from degrees to radians, the sine and cosine of the
c         angle of attack.
      double precision c_cc, c_cn, c_cm  ! The constants by which
c         dcc/dG, dcn/dG, and dcm/dG are multiplied (for viscous forces)
      integer i, j, subi, max_subi
c         i, j are iterators.
c         subi, max_subi are used to find the index of the thickest part
c         of the airfoil, for floating thickness constraints.
      double precision rc_con, xa, ya, xb, yb, xc, yc, r, rmin,
     |    num1, num2, num3, deninv
c         rc_con: radius of curvature constraint
c         xa, ya, xb, yb, xc, yc: 3 adjacent points on the airfoil
c           used to define a circle, for radius of curvature constraints
c         r: the radius of curvature
c         rmin: the radius of curvature
      double precision temp
      logical constrained  ! True if constraints are used for the given
c         objective function
      logical dfdxflag

c     Variables related to angle of attack
      radians = pi/180.0
      cos_alpha = cos(alpha*radians)
      sin_alpha = sin(alpha*radians)

c     Differentiate the objective function without constraints
      if (obj_func .eq. 0) then
c        Mean drag  minimization
         c_cn = sin_alpha/den
         c_cc = cos_alpha/den         
         c_cm = 0.d0

         if (unsted .and. dfdxflag) then 
            call add_dcdG(jdim, kdim, press, Q, xy, xyj, x, y, c_cc, 
     &           c_cn, c_cm, A)
            constrained = .true.
         end if
      else if (obj_func == 1) then
c       Inverse design (match target pressure distribution).
c       Cp is independent of G, so dJ/dG = 0
         if (unsted) constrained = .true. !then unsteady
      elseif (obj_func == 2) then
c       Not active.
      elseif (obj_func == 3) then
c       Not active.
      elseif (obj_func == 4) then
c       Not active.
      elseif (obj_func == 5) then
c       Maximize lift to drag ratio.
         if (.not.unsted) den = 1.d0
         temp = 1.d0/obj0
         c_cn = temp * (sin_alpha/clt/den - cos_alpha*cdt/clt**2/den)
         c_cc = temp * (cos_alpha/clt/den + sin_alpha*cdt/clt**2/den)
         c_cm = 0.d0

         if (dfdxflag .or. .not.unsted) then
            call add_dcdG(jdim, kdim, press, Q, xy, xyj, x, y,c_cc,c_cn,
     |           c_cm, A)
            constrained = .true.
         end if

      elseif (obj_func == 6) then
c       Lift-constrained drag minimization.
        temp = 2.d0 * wfl * (1.d0 - clt/cl_tar)/(-cl_tar) / obj0
        c_cn = temp * cos_alpha
        c_cc = -temp * sin_alpha
        if (cdt > cd_tar) then
          temp = 2.d0 * wfd * (1.d0 - cdt/cd_tar)/(-cd_tar) / obj0
          c_cn = c_cn + temp * sin_alpha
          c_cc = c_cc + temp * cos_alpha
        endif
        c_cm = 0.d0
        call add_dcdG(jdim, kdim, press, Q, xy, xyj, x, y, c_cc, c_cn,
     |      c_cm, A)
        constrained = .true.
      elseif (obj_func == 7) then
c       Maximize lift subject to a moment constraint.
        c_cm = 2.d0*wfd*(1.d0-cmt/cd_tar)/(-cd_tar)/obj0
        temp = 2.d0*wfl*(1.d0-clt/cl_tar)/(-cl_tar)/obj0
        c_cc = -temp*sin_alpha
        c_cn = temp*cos_alpha + 0.25d0*c_cm
        call add_dcdG(jdim, kdim, press, Q, xy, xyj, x, y, c_cc, c_cn,
     |      c_cm, A)
        constrained = .true.
      elseif (obj_func == 8) then
c       Lift-constrained drag minimization. (lift constraint treated in flow
c       solver)
        temp = 1.d0 / obj0
        c_cn = temp * sin_alpha
        c_cc = temp * cos_alpha
        c_cm = 0.d0
        call add_dcdG(jdim, kdim, press, Q, xy, xyj, x, y, c_cc, c_cn,
     |      c_cm, A)
        constrained = .true.
      else if (obj_func .eq. 9) then
c        Inverse remote design gridshape
c        Cp is independent of G, so dJ/dG = 0.
         constrained = .true.
      else if (obj_func .eq. 12) then
c        Inverse remote design boxshape
c        Cp is independent of G, so dJ/dG = 0.
         constrained = .true.
      endif

c     Differentiate the constraints
      if (constrained) then
c       Thickness constraints
        do i = 1, ntcon
          if (cty(i) < cty_tar(i)) then
            temp = wtc*2.d0*(1.d0-cty(i)/cty_tar(i))/(-cty_tar(i));
            A(juptx(i),1,2) = A(juptx(i),1,2) + temp;
            A(jlotx(i),1,2) = A(jlotx(i),1,2) - temp;
          endif
        end do

c       Area constraints
        if (area < areafac*areainit) then
          temp = wac*(1.d0-area/(areafac*areainit))/(-areafac*areainit)
          A(jtail1,1,1) = A(jtail1,1,1)-temp*(y(jtail1+1,1)-y(jtail1,1))
          A(jtail1,1,2) = A(jtail1,1,2)+temp*(x(jtail1+1,1)-x(jtail1,1))
          do j = jtail1+1, jtail2-1
            A(j,1,1) = A(j,1,1) + temp*(y(j-1,1) - y(j+1,1))
            A(j,1,2) = A(j,1,2) + temp*(x(j+1,1) - x(j-1,1))
          end do
          A(jtail2,1,1) = A(jtail2,1,1)+temp*(y(jtail2,1)+y(jtail2-1,1))
          A(jtail2,1,2) = A(jtail2,1,2)+temp*(x(jtail2,1)-x(jtail2-1,1))
        endif

c       Floating thickness constraints
        maxrthc = 0.0
        maxrthctemp = 0.0
        do i=1,nrtcon
c         First find the thickest x-location in the range...
          do subi = 1, crtxn(i)+1
            maxrthctemp = y(juprtxsub(i,subi),1)-y(jlortxsub(i,subi),1)
            if (maxrthc .lt. maxrthctemp) then
              maxrthc = maxrthctemp
              max_subi = subi
            end if
          end do
c         ... then differentiate
          if (maxrthc .lt. crthtar(i) ) then
            temp = wtc*2.d0*( 1.0 - maxrthc/crthtar(i))/(-crthtar(i))
            A(juprtxsub(i,max_subi),1,2) = A(juprtxsub(i,max_subi),1,2)
     |          + temp;
            A(jlortxsub(i,max_subi),1,2) = A(jlortxsub(i,max_subi),1,2)
     |          - temp;
          end if
        end do

c       Radius of curvature constraints
        if (wrc_con .ne. 0.d0) then
          xb = x(jtail1  ,1)
          yb = y(jtail1  ,1)
          xc = x(jtail1+1,1)
          yc = y(jtail1+1,1)
          do j = jtail1+1, jtail2-1
c           Compute the radius of curvature
            xa = xb
            ya = yb
            xb = xc
            yb = yc
            xc = x(j+1,1)
            yc = y(j+1,1)
            num1 = ((xc - xb)**2 + (yc - yb)**2)
            num2 = ((xc - xa)**2 + (yc - ya)**2)
            num3 = ((xa - xb)**2 + (ya - yb)**2)
            deninv = 1.d0/(xa*yb-xa*yc-xb*ya+xb*yc+xc*ya-xc*yb)
            r = sqrt(0.25d0 * num1 * num2 * num3 * deninv**2)

c           Interpolate to find the minimum radius of curvature.
c           xrc_tar is assumed to be sorted in order of increasing
c           values.
            i = 1
            do while ((xrc_tar(i+1) < xb) .and. (i+1 < nrc))
              i = i+1
            enddo
            rmin = (xb - xrc_tar(i))*(rc_tar(i+1)-rc_tar(i))
     |          /(xrc_tar(i+1)-xrc_tar(i)) + rc_tar(i)

c           Differentiate if the radius of curvature is less than the
c           target.
            if (r < rmin) then
              temp = wrc_con * (r/rmin - 1.d0) / (rmin * r) * 0.25d0
c             So d(rc_con)/dG = temp * d(num1*num2*num3*deninv**2)/dG
              A(j-1, 1, 1) = A(j-1, 1, 1) + temp *
     |            (- num1*(2.d0*(xc-xa))*num3*deninv**2
     |             + num1*num2*(2.d0*(xa-xb))*deninv**2
     |             + num1*num2*num3*deninv**3*(-2.d0)*(yb - yc) )
              A(j-1, 1, 2) = A(j-1, 1, 2) + temp *
     |            (- num1*(2.d0*(yc-ya))*num3*deninv**2
     |             + num1*num2*(2.d0*(ya-yb))*deninv**2
     |             + num1*num2*num3*deninv**3*(-2.d0)*(-xb + xc) )
              A(j  , 1, 1) = A(j  , 1, 1) + temp *
     |            (- (2.d0*(xc-xb))*num2*num3*deninv**2
     |             - num1*num2*(2.d0*(xa-xb))*deninv**2
     |             + num1*num2*num3*deninv**3*(-2.d0)*(-ya + yc) )
              A(j  , 1, 2) = A(j  , 1, 2) + temp *
     |            (- (2.d0*(yc-yb))*num2*num3*deninv**2
     |             - num1*num2*(2.d0*(ya-yb))*deninv**2
     |             + num1*num2*num3*deninv**3*(-2.d0)*(xa - xc) )
              A(j+1, 1, 1) = A(j+1, 1, 1) + temp *
     |            (  (2.d0*(xc-xb))*num2*num3*deninv**2
     |             + num1*(2.d0*(xc-xa))*num3*deninv**2
     |             + num1*num2*num3*deninv**3*(-2.d0)*(ya - yb) )
              A(j+1, 1, 2) = A(j+1, 1, 2) + temp *
     |            (  (2.d0*(yc-yb))*num2*num3*deninv**2
     |             + num1*(2.d0*(yc-ya))*num3*deninv**2
     |             + num1*num2*num3*deninv**3*(-2.d0)*(-xa + xb) )
            endif
          enddo
        endif
      endif
      end
c---- add_dJdG ---------------------------------------------------------

c---- add_dcdG ---------------------------------------------------------
c Computes the derivatives of the normal and chordwize forces on the
c airfoil, cn and cc, and the moment on the airfoil (at the leading
c edge), cm, with respect to the grid node locations, G.  These
c are multiplied by constants c_cn and c_cc, and added to A.
c
c Note: For viscous flows, this assumes that the no slip condition
c is enforced exactly, and so du/dxi = dv/dxi = 0 (and thus some terms
c in the derivative can be omitted).
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c press: pressure (without the inverse of the metric jacobian included)
c Q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c xy, xyj: metrics, and metric jacobian
c x, y: x- and y-coordinates of the grid nodes
c c_cc, c_cn, c_cm: the constants by which dcc/dG, dcn/dG, and dcm/dG
c   are multiplied.
c
c Returns:
c A: with c_cc * dcc/dG + c_cn * dcn/dG + c_cm * dcm/dG added to it.
c
c Chad Oldfield, September 2005
c-----------------------------------------------------------------------
      subroutine add_dcdG (jdim, kdim, press, Q, xy, xyj, x, y,
     |    c_cc, c_cn, c_cm, A)
      implicit none

#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim
      double precision press(jdim,kdim), Q(jdim,kdim,4),
     |    xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision c_cc, c_cn, c_cm
      double precision A(jdim, kdim, 2)

c     Local variables
      integer j, jm1, jp1
      double precision z_jm1, z_j, z_jp1  ! The value of z (as used in
c         clcd.f) at nodes j-1, j, and j+1
      double precision u_eta, v_eta, xi_x, xi_y, eta_x, eta_y, mJ
c         du/deta, dv/deta, and metrics at node j
      double precision u_eta_jp1, v_eta_jp1, xi_x_jp1, xi_y_jp1,
     |    eta_x_jp1, eta_y_jp1
c         du/deta, dv/deta, and metrics at node j+1
      double precision alngth, dcc_x, dcn_y, dcc_z, dcn_z, dcm_x, dcm_y,
     |    dcm_z, d_xi(2), d_eta(2)
c         Temporary variables for the viscous forces.
      double precision temp, const

c     Inviscid forces (due to pressure)
      const = 1.d0/(gamma*fsmach**2)
      z_j = (press(jtail1,1)*gamma-1.d0)
      z_jp1 = (press(jtail1+1,1)*gamma-1.d0)
      A(jtail1,1,1) = A(jtail1,1,1) + const*(c_cn*(z_jp1 + z_j)
     |    - c_cm*x(jtail1,1)*(z_jp1+z_j))
      A(jtail1,1,2) = A(jtail1,1,2) - const*c_cc*(z_jp1 + z_j)
      do j = jtail1+1, jtail2-1
        z_jm1 = z_j
        z_j = z_jp1
        z_jp1 = (press(j+1,1)*gamma-1.d0)
        A(j,1,1) = A(j,1,1) + const*(c_cn*(z_jp1 - z_jm1)
     |      + c_cm*x(j,1)*(z_jm1-z_jp1))
        A(j,1,2) = A(j,1,2) + const*c_cc*(z_jm1 - z_jp1)
      end do
      z_jm1 = z_j
      z_j = z_jp1
      A(jtail2,1,1) = A(jtail2,1,1) - const*(c_cn*(z_j + z_jm1)
     |    + c_cm*x(jtail2,1)*(z_j+z_jm1))
      A(jtail2,1,2) = A(jtail2,1,2) + const*c_cc*(z_j + z_jm1)

c     Viscous forces (due to wall shear stress)
      if (viscous) then
        alngth = 1.d0
        const = alngth / (Re*fsmach**2)

c       Startup: compute force coefficient, z_jp1, at node jtail1
        jp1 = jtail1
        u_eta_jp1= -1.5d0*Q(jp1,1,2)/Q(jp1,1,1)
     |      + 2.d0*Q(jp1,2,2)/Q(jp1,2,1) - 0.5d0*Q(jp1,3,2)/Q(jp1,3,1)
        v_eta_jp1= -1.5d0*Q(jp1,1,3)/Q(jp1,1,1)
     |      + 2.d0*Q(jp1,2,3)/Q(jp1,2,1) - 0.5d0*Q(jp1,3,3)/Q(jp1,3,1)
        xi_x_jp1 = xy(jp1,1,1)
        xi_y_jp1 = xy(jp1,1,2)
        eta_x_jp1 = xy(jp1,1,3)
        eta_y_jp1 = xy(jp1,1,4)
        z_jp1 = (u_eta_jp1*eta_y_jp1 - v_eta_jp1*eta_x_jp1)
        z_j = 0.d0  ! Just to have something to pass to z_jm1

        do j = jtail1, jtail2
          jp1 = j+1
          jm1 = j-1

c         Pass down previously computed values
          u_eta = u_eta_jp1
          v_eta = v_eta_jp1
          xi_x = xi_x_jp1
          xi_y = xi_y_jp1
          eta_x = eta_x_jp1
          eta_y = eta_y_jp1
          mJ = xyj(j,1)
          z_jm1 = z_j
          z_j = z_jp1
          
c         Compute force coefficient, z_jp1, at node j+1
          u_eta_jp1= -1.5d0*Q(jp1,1,2)/Q(jp1,1,1)
     |        + 2.d0*Q(jp1,2,2)/Q(jp1,2,1) - 0.5d0*Q(jp1,3,2)/Q(jp1,3,1)
          v_eta_jp1= -1.5d0*Q(jp1,1,3)/Q(jp1,1,1)
     |        + 2.d0*Q(jp1,2,3)/Q(jp1,2,1) - 0.5d0*Q(jp1,3,3)/Q(jp1,3,1)
          xi_x_jp1 = xy(jp1,1,1)
          xi_y_jp1 = xy(jp1,1,2)
          eta_x_jp1 = xy(jp1,1,3)
          eta_y_jp1 = xy(jp1,1,4)
          z_jp1 = (u_eta_jp1*eta_y_jp1 - v_eta_jp1*eta_x_jp1)

c         Treat the nodes at the trailing edge differently...
          if (j == jtail1) then
            dcc_x = -z_j - z_jp1
            dcn_y = dcc_x
            dcc_z = x(jp1,1) - x(j,1)
            dcn_z = y(jp1,1) - y(j,1)
            dcm_x = y(jp1,1)*(z_j + z_jp1)
            dcm_y = -x(jp1,1)*(z_j + z_jp1)
            dcm_z = x(j,1)*y(jp1,1) - x(jp1,1)*y(j,1)
          elseif (j == jtail2) then
            dcc_x = z_j + z_jm1
            dcn_y = dcc_x
            dcc_z = x(j,1) - x(jm1,1)
            dcn_z = y(j,1) - y(jm1,1)
            dcm_x = -y(jm1,1)*(z_j + z_jm1)
            dcm_y = x(jm1,1)*(z_j + z_jm1)
            dcm_z = x(jm1,1)*y(j,1) - x(j,1)*y(jm1,1)
          else  ! General node
            dcc_x = z_jm1 - z_jp1
            dcn_y = dcc_x
            dcc_z = x(jp1,1) - x(jm1,1)
            dcn_z = y(jp1,1) - y(jm1,1)
            dcm_x = y(jp1,1)*(z_jp1+z_j) - y(jm1,1)*(z_j+z_jm1)
            dcm_y = x(jm1,1)*(z_j+z_jm1) - x(jp1,1)*(z_jp1+z_j)
            dcm_z = x(jm1,1)*y(j,1) - x(j,1)*y(jm1,1)
     |          + x(j,1)*y(jp1,1) - x(jp1,1)*y(j,1)
          endif

c         cc and cn depend on G explicitly...
          A(j,1,1) = A(j,1,1) + const*(c_cc*dcc_x + c_cm*dcm_x)
          A(j,1,2) = A(j,1,2) + const*(c_cn*dcn_y + c_cm*dcm_y)
c         ... and through z
          temp = const*(c_cc*dcc_z + c_cn*dcn_z + c_cm*dcm_z)
          d_xi(1) = temp * (u_eta*(-eta_y*xi_x+mJ) + v_eta*eta_x*xi_x)
          d_xi(2) = temp * (u_eta*(-eta_y*xi_y) + v_eta*(eta_x*xi_y+mJ))
          call add_delta_xi_I(jdim, kdim, j, 1, d_xi, A)
          d_eta(1) = temp * (u_eta*(-eta_y*eta_x) + v_eta*eta_x**2)
          d_eta(2) = temp * (u_eta*(-eta_y**2) + v_eta*eta_x*eta_y)
          call add_delta_eta_I(jdim, kdim, j, 1, d_eta, A)
        end do
      endif
      
      end
c---- add_dcdG ---------------------------------------------------------
