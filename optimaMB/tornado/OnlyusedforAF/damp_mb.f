c
c     calculates : 1) cross-diffusion term of the w equation 
c                     derived from the epsilon equation
c                  2) f1 function that switches between the 
c                     two models
c                  3) the differential equation constants which
c                     are made up of the f1 function
c
c?? must be the right smin used here
c?? wilcox model
c      sigmak_w=0.5
c??
c
      if (bnd(1).or.bnd(3)) then
      do k=klow,kup
         do j=jlow,jup
c
            g11=xy(j,k,1)**2+xy(j,k,2)**2
            g22=xy(j,k,3)**2+xy(j,k,4)**2
            g12=xy(j,k,1)*xy(j,k,3)+xy(j,k,2)*xy(j,k,4)
c
            dk_dxi=0.5*(vk(j+1,k)-vk(j-1,k))
            dk_deta=0.5*(vk(j,k+1)-vk(j,k-1))
c
            de_dxi=0.5*(ve(j+1,k)-ve(j-1,k))
            de_deta=0.5*(ve(j,k+1)-ve(j,k-1))
c
            cross_diff=2.*q(j,k,1)*sigmae_e/ve(j,k)*
     $           (g11*dk_dxi*de_dxi+g22*dk_deta*de_deta
     $           +g12*(dk_dxi*de_deta+dk_deta*de_dxi))
c
            cross_diff=max(cross_diff,1.e-20)
c
            a1=sqrt(vk(j,k))/(0.09*ve(j,k)*smin(j,k))
            a2=500.*fnu(j,k)/(smin(j,k)**2*ve(j,k))
            a3=4.*q(j,k,1)*sigmae_e*vk(j,k)/(cross_diff*smin(j,k)**2)
            arg1=min(max(a1,a2),a3)
            arg2=max(2*a1,a2)
            f_1(j,k)=tanh(arg1**4)
            f_2(j,k)=tanh(arg2**2)
c
            sigma_k(j,k)=f_1(j,k)*sigmak_w+(1.-f_1(j,k))*sigmak_e
            sigma_e(j,k)=f_1(j,k)*sigmae_w+(1.-f_1(j,k))*sigmae_e
            beta(j,k)=f_1(j,k)*beta_w+(1.-f_1(j,k))*beta_e
            betas(j,k)=f_1(j,k)*betas_w+(1.-f_1(j,k))*betas_e
            gam(j,k)=f_1(j,k)*gamma_w+(1.-f_1(j,k))*gamma_e
            cdiff(j,k)=(1.-f_1(j,k))*sigmae_e
         enddo
      enddo
c?? added this bit of code
      k=kend
      do j=jlow,jup
         f_1(j,k)=f_1(j,k-1)
         f_2(j,k)=f_2(j,k-1)
         sigma_k(j,k)=sigma_k(j,k-1)
         sigma_e(j,k)=sigma_e(j,k-1)
         beta(j,k)=beta(j,k-1)
         betas(j,k)=betas(j,k-1)
         gam(j,k)=gam(j,k-1)
         cdiff(j,k)=cdiff(j,k-1)
      enddo         
      k=kbegin
      do j=jlow,jup
         f_1(j,k)=f_1(j,k+1)
         f_2(j,k)=f_2(j,k+1)
         sigma_k(j,k)=sigma_k(j,k+1)
         sigma_e(j,k)=sigma_e(j,k+1)
         beta(j,k)=beta(j,k+1)
         betas(j,k)=betas(j,k+1)
         gam(j,k)=gam(j,k+1)
         cdiff(j,k)=cdiff(j,k+1)
      enddo         
      j=jend
      do k=kbegin,kend
         f_1(j,k)=f_1(j-1,k)
         f_2(j,k)=f_2(j-1,k)
         sigma_k(j,k)=sigma_k(j-1,k)
         sigma_e(j,k)=sigma_e(j-1,k)
         beta(j,k)=beta(j-1,k)
         betas(j,k)=betas(j-1,k)
         gam(j,k)=gam(j-1,k)
         cdiff(j,k)=cdiff(j-1,k)
      enddo         
      j=jbegin
      do k=kbegin,kend
         f_1(j,k)=f_1(j+1,k)
         f_2(j,k)=f_2(j+1,k)
         sigma_k(j,k)=sigma_k(j+1,k)
         sigma_e(j,k)=sigma_e(j+1,k)
         beta(j,k)=beta(j+1,k)
         betas(j,k)=betas(j+1,k)
         gam(j,k)=gam(j+1,k)
         cdiff(j,k)=cdiff(j+1,k)
      enddo         
c??      
      else
      do k=kbegin,kend
         do j=jbegin,jend
            f_1(j,k)=0.0
            f_2(j,k)=0.0
            sigma_k(j,k)=sigmak_e
            sigma_e(j,k)=sigmae_e
            beta(j,k)=beta_e
            betas(j,k)=betas_e
            gam(j,k)=gamma_e
            cdiff(j,k)=sigmae_e
         enddo
      enddo
      endif
      midk=int(real(kup)/2.)
      if (bnd(1)) then
      do j=jlow,jup
         wilcox=.false.
         bradshaw=.false.
         do k=midk,klow,-1
           if (f_1(j,k).gt.0.999) wilcox=.true.
           if (f_2(j,k).gt.0.999) bradshaw=.true.
           if (wilcox) f_1(j,k)=1.0
           if (bradshaw) f_2(j,k)=1.0
        enddo
      enddo
      endif
      if (bnd(3)) then
      do j=jlow,jup
         wilcox=.false.
         bradshaw=.false.
         do k=midk,kup
           if (f_1(j,k).gt.0.999) wilcox=.true.
           if (f_2(j,k).gt.0.999) bradshaw=.true.
           if (wilcox) f_1(j,k)=1.0
           if (bradshaw) f_2(j,k)=1.0
        enddo
      enddo
      endif
c?? wilcox model
c      do k=kbegin,kend
c        do j=jbegin,jend
c            f_1(j,k)=1.0
c            f_2(j,k)=0.0
c            sigma_k(j,k)=sigmak_w
c            sigma_e(j,k)=sigmae_w
c            beta(j,k)=beta_w
c            betas(j,k)=betas_w
c            gam(j,k)=gamma_w
c            cdiff(j,k)=0.0
c        enddo
c      enddo
c??
c
c      

