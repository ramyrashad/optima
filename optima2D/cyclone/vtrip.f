      subroutine vtrip(JDIM,KDIM,A,B,C,Q,S,FN,F,J1,J2,K1,K2)            
      DIMENSION A(JDIM,KDIM),B(JDIM,KDIM),C(JDIM,KDIM),F(JDIM,KDIM),    
     >          Q(JDIM,KDIM),S(JDIM,KDIM),FN(KDIM)                      
      JA = J1 + 1                                                       
C                                                                       
C         FORWARD ELIMINATION SWEEP                                     
C                                                                       
      DO 1 K = K1,K2                                                    
      FN(K) = F(J2,K)                                                   
      Q(J1,K) = -C(J1,K)/B(J1,K)                                        
      F(J1,K) = F(J1,K)/B(J1,K)                                         
      S(J1,K) = - A(J1,K)/B(J1,K)                                       
1     CONTINUE                                                          
C                                                                       
      DO 10 J=JA,J2                                                     
         DO 2 K = K1,K2                                                 
         P =1./( B(J,K) + A(J,K)*Q(J-1,K))                              
         Q(J,K) = - C(J,K)*P                                            
         F(J,K) = ( F(J,K) - A(J,K)*F(J-1,K))*P                         
         S(J,K) = - A(J,K)*S(J-1,K)*P                                   
2        CONTINUE                                                       
   10 CONTINUE                                                          
C                                                                       
C         BACKWARD PASS                                                 
C                                                                       
      JJ = J1 + J2                                                      
      DO 3 K = K1,K2                                                    
      Q(J2,K) = 0.                                                      
      S(J2,K) = 1.                                                      
3     CONTINUE                                                          
C                                                                       
      DO 11 I=JA,J2                                                     
         J = JJ - I                                                     
         DO 4 K = K1,K2                                                 
         S(J,K) = S(J,K) + Q(J,K)*S(J+1,K)                              
         Q(J,K) = F(J,K) + Q(J,K)*Q(J+1,K)                              
4        CONTINUE                                                       
11    continue                                                          
      DO 5 K = K1,K2                                                    
      F(J2,K) = ( FN(K) - C(J2,K)*Q(J1,K) - A(J2,K)*Q(J2-1,K))/         
     1          ( C(J2,K)*S(J1,K) + A(J2,K)*S(J2-1,K)  +B(J2,K))        
5     CONTINUE                                                          
C                                                                       
C         BACKWARD ELIMINATION PASS                                     
C                                                                       
      DO 12 I=JA,J2                                                     
         J = JJ -I                                                      
         DO 6 K = K1,K2                                                 
         F(J,K) = F(J2,K)*S(J,K) + Q(J,K)                               
6        CONTINUE                                                       
12    continue                                                          
      RETURN                                                            
      END                                                               
