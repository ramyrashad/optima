c     -----------------------------------------------------------------
c     -- setup of preconditioner and jacobian pointer arrays ipa and jpa
c     based on the ordering array indx --
c     -- m. nemec, aug. 2001 --
c     -----------------------------------------------------------------
cmt ~~~ ximesh is replaced by ibctype ~~~
      SUBROUTINE setgraph( nmax, ntvar, indx, icol, ipa, jpa, ia, ja,
     &     viscous)

      implicit none

#include "../include/parms.inc"
#include "../include/units.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"

      integer nmax,ntvar,i,js,jsb,je,jeb,ib,is,j,k,ks,ke,iblk,jcol
      integer itmp,ii,jj,nlines
      integer indx(*), icol(*), ipa(*), jpa(*), ia(*), ja(*)
      logical   viscous

c     -- plot variables --
      logical pltout
      character*1 title
      character*2 munt 
      integer lines(50)

      DO i = 1,maxjk*jacb+1
        ia(i) = 0
        ipa(i) = 0
      ENDDO
      DO i = 1,maxjk*jacb2*9
        ja(i) = 0
      ENDDO
      DO i = 1,maxjk*jacb2*5
        jpa(i) = 0
      ENDDO

c     -- setup column indx used for constructing sparse arrays --
c     -- icol is needed to handle both 4x4 and 5x5 jacobian blocks --
      icol(1) = nmax
      DO i = 2,9
        icol(i) = icol(i-1) + nmax
      ENDDO
c
c     -----------------------------------------------------------------
c     -- preconditioner (first order jac.) & second order jacobian --
c     -- setup sparse matricies ipa and jpa for preconditioner  --

cmt ~~~ I replaced the ximesh stuff with ibctype to get rid of extra parameters
      DO i = 1,nblks
c     -- interior nodes -- 
c     -- following logic is used to distinguish interior interfaces --
         IF(ibctype(i,2).EQ.0)THEN
            js  = jminbnd(i)
            jsb = js
         ELSE
            js  = jminbnd(i) + 1
            jsb = jminbnd(i) + 2
         ENDIF
         IF(ibctype(i,4).EQ.0)THEN
            je  = jmaxbnd(i)
            jeb = je
         ELSE
            je  = jmaxbnd(i) - 1
            jeb = jmaxbnd(i) - 2
         ENDIF

         CALL pa_int( indx(lgptr(i)), jmax(i), kmax(i), nmax, js,
     &        je, kminbnd(i)+1, kmaxbnd(i)-1, jpa, ipa, icol)

         CALL as_int( indx(lgptr(i)), jmax(i), kmax(i), nmax, jsb,
     &        jeb, kminbnd(i)+2, kmaxbnd(i)-2, ja, ia, icol)
      ENDDO

c     -- block boundaries --
      DO ib=1,nblks
         DO is=1,4
            CALL pa_bc( ib, is, nmax, indx, jpa, ipa, icol)
            CALL as_bc( ib, is, nmax, indx, ja, ia, icol)

            IF (ibctype(ib,is).NE.0) THEN
c     -- first interior nodes dissipation stencils for jacobian matrix --

cmt ~~~ side 1 ~~~
               IF (is.EQ.1) THEN

                  k  = kminbnd(ib)+1

                  IF (ibctype(ib,2).EQ.0) THEN
                     js = jminbnd(ib)
                  ELSE
                     js = jminbnd(ib)+2
                  ENDIF

                  IF (ibctype(ib,4).EQ.0) THEN
                     je = jmaxbnd(ib)
                  ELSE
                     je = jmaxbnd(ib)-2
                  ENDIF
                        
                  CALL as_fin1( k, js, je, nmax, indx(lgptr(ib)),
     &                 jmax(ib), kmax(ib), ja, ia, icol)

c     -- special dissipation stencils at interior block corners --
                  IF (ibctype(ib,2).NE.0) THEN
                     k = kminbnd(ib)+1
                     j = jminbnd(ib)+1
                     CALL as_ic1( j, k, nmax, indx(lgptr(ib)),
     &                    jmax(ib),kmax(ib), ja, ia, icol)
                  ENDIF

                  IF (ibctype(ib,4).NE.0) THEN
                     k = kminbnd(ib)+1
                     j = jmaxbnd(ib)-1
                     CALL as_ic2( j, k, nmax, indx(lgptr(ib)),
     &                    jmax(ib),kmax(ib), ja, ia, icol)
                  ENDIF

               ELSEIF (is.EQ.2) THEN
cmt ~~~ side 2 ~~~
                  j  = jminbnd(ib)+1
                  ks = kminbnd(ib)+2
                  ke = kmaxbnd(ib)-2
                  CALL as_fin2( j, ks, ke, nmax, indx(lgptr(ib)),
     &                 jmax(ib), kmax(ib), ja, ia, icol)

               ELSEIF (is.EQ.3) THEN
cmt ~~~ side 3 ~~~
                  k  = kmaxbnd(ib)-1

                  IF (ibctype(ib,2).EQ.0) THEN
                     js = jminbnd(ib)
                  ELSE
                     js = jminbnd(ib)+2
                  ENDIF

                  IF (ibctype(ib,4).EQ.0) THEN
                     je = jmaxbnd(ib)
                  ELSE
                     je = jmaxbnd(ib)-2
                  ENDIF

                  CALL as_fin3( k, js, je, nmax, indx(lgptr(ib)),
     &                 jmax(ib), kmax(ib), ja, ia, icol)

c     -- special dissipation stencils at interior block corners --
                  IF (ibctype(ib,2).NE.0) THEN
                     k = kmaxbnd(ib)-1
                     j = jminbnd(ib)+1
                     CALL as_ic3( j, k, nmax, indx(lgptr(ib)),
     &                    jmax(ib),kmax(ib), ja, ia, icol)
                  ENDIF

                  IF (ibctype(ib,4).NE.0) THEN
                     k = kmaxbnd(ib)-1                  
                     j = jmaxbnd(ib)-1
                     CALL as_ic4( j, k, nmax, indx(lgptr(ib)),
     &                    jmax(ib),kmax(ib), ja, ia, icol)
                  ENDIF

               ELSE
cmt ~~~ side 4 ~~~
                  j  = jmaxbnd(ib)-1
                  ks = kminbnd(ib)+2
                  ke = kmaxbnd(ib)-2
                  CALL as_fin4( j, ks, ke, nmax, indx(lgptr(ib)),
     &                 jmax(ib), kmax(ib), ja, ia, icol)
               ENDIF
            ENDIF
         ENDDO
      ENDDO

      IF (.NOT. viscous) THEN
c     -- leading edge singular points for inviscid flow --         
         do i = 1,nsing
            CALL pa_sng( js1(i), ks1(i), js2(i), ks2(i), javg1(i),
     &           kavg1(i), javg2(i), kavg2(i), indx(lgptr(ibs1(i))),
     &           jmax(ibs1(i)), kmax(ibs1(i)), indx(lgptr(ibs2(i))),
     &           jmax(ibs2(i)), kmax(ibs2(i)), nmax, jpa, ipa, icol)

            CALL as_sng( js1(i), ks1(i), js2(i), ks2(i), javg1(i),
     &           kavg1(i), javg2(i), kavg2(i), indx(lgptr(ibs1(i))),
     &           jmax(ibs1(i)), kmax(ibs1(i)), indx(lgptr(ibs2(i))),
     &           jmax(ibs2(i)), kmax(ibs2(i)), nmax, ja, ia, icol)
         ENDDO
      ENDIF

c     -- copy points (leading and trailing edge points) --
      DO i=1,ncopy
         CALL pa_cop( ijte1(i), ikte1(i), ijte2(i), ikte2(i),
     &        indx(lgptr(ibte1(i))), jmax(ibte1(i)), kmax(ibte1(i)),
     &        indx(lgptr(ibte2(i))), jmax(ibte2(i)), kmax(ibte2(i)),
     &        nmax, jpa, ipa, icol)
            
         CALL as_cop( ijte1(i), ikte1(i), ijte2(i), ikte2(i),
     &        indx(lgptr(ibte1(i))), jmax(ibte1(i)), kmax(ibte1(i)),
     &        indx(lgptr(ibte2(i))), jmax(ibte2(i)), kmax(ibte2(i)),
     &        nmax, ja, ia, icol)
      ENDDO

c     -- remove zeros --
      ntvar = 0
      DO iblk = 1,nblks
         ntvar = ntvar + jbmax(iblk)*kbmax(iblk)
      ENDDO
      ntvar = ntvar*nmax

      jcol = 1
      itmp = ipa(1)
      ipa(1) = 1
      DO ii = 1,ntvar
         jj = (ii-1)*icol(5)
         DO j = jj+1,jj+itmp
            jpa(jcol) = jpa(j)
            jcol = jcol + 1
         ENDDO
         itmp = ipa(ii+1)
         ipa(ii+1) = jcol
      ENDDO

      jcol = 1
      itmp = ia(1)
      ia(1) = 1
      DO ii = 1,ntvar
         jj = (ii-1)*icol(9)
         DO j = jj+1,jj+itmp
            ja(jcol) = ja(j)
            jcol = jcol + 1
         ENDDO
         itmp = ia(ii+1)
         ia(ii+1) = jcol
      ENDDO

c     -----------------------------------------------------------------

      pltout = .FALSE.
      IF (pltout) THEN
         munt = 'in'
         title = ' '      
c         nlines = ntvar/4 - 1
c         lines(1) = 4
c         do i=2,nlines
c            lines(i) = 4 + lines(i-1)
c         end do
         nlines = 0

         OPEN( unit=n_all, file='fjac.ps', status='unknown')
         CALL pspltm (ntvar, ntvar, 0, jpa, ipa, title, 0, 7.0, munt,
     &        nlines, lines, n_all)
         CLOSE(n_all)
         STOP 'setgraph ps output'
      ENDIF

      RETURN
      END                       !setgraph
