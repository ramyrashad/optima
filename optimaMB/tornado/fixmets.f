c ********************************************************************
c **       Routine to copy data from the interior of block2         **
c **       to the halo points of block 1                            **
c **                                                                **
c **       Range of copy is max(begin's) to min(end's)              **
c ********************************************************************
c calling routine: initia
c Notes on metrics:   xy(j,k,n)     n=1   XIx 
c                                   n=2   XIy
c                                   n=3   ETAx
c                                   n=4   ETAy
c When copying metrics, the sign may be reversed for the opposite side
c to the one being copied.
c
c Example:       blk1 side1 from blk2 side1 
c   both sides are xi with eta opposite so change sign on eta metrics
c   direction of 1 is opposite so change sign on xi metrics too
c
      subroutine fixmets(nhalo,nside1,nside2,
     &  jdim1,kdim1,xy1,xyj1,
     &  jbegin1,jend1,kbegin1,kend1,
     &  jdim2,kdim2,xy2,xyj2,
     &  jbegin2,jend2,kbegin2,kend2)

      implicit none
      integer nhalo,nside1,nside2,jdim1,kdim1,jdim2,kdim2
      integer jbegin1,jend1,kbegin1,kend1,jbegin2,jend2,kbegin2,kend2
      integer jb1,je1,jb2,je2,kb1,ke1,kb2,ke2,ii,k1h,k2i,j1,j2,j1h,j2i
      integer jj,k1,k2,kk
      double precision xisign,etasign
      double precision xy1(jdim1,kdim1,4),xyj1(jdim1,kdim1)
      double precision xy2(jdim2,kdim2,4),xyj2(jdim2,kdim2)
c
c
c     -indices for begining and end of blocks
c      -note jbegin and jend extend full scope of block including nhalo
      jb1=jbegin1+nhalo
      je1=jend1-nhalo
      jb2=jbegin2+nhalo
      je2=jend2-nhalo
c      -note kbegin and kend extend full scope of block including nhalo
c       only when copying halo rows, during halo column copy ... not so
      kb1=kbegin1+nhalo
      ke1=kend1-nhalo
      kb2=kbegin2+nhalo
      ke2=kend2-nhalo

      do 99 ii=0,nhalo-1
c ********************************************************************
c **  block 1   :      side 1 or 3                                  **
c ********************************************************************
      if(nside1.eq.1.or.nside1.eq.3) then
        if (nside1.eq.1) then
          k1h = kbegin1 + ii
        else
          k1h = kend1 - ii
        endif
        if (nside2.eq.1.or.nside2.eq.3) then
c         -block 1 - side 1 or 3 connected to block 2 - side 1 or 3 
          if(nside2.eq.1) then
            k2i = kb2 + nhalo - ii
          else
            k2i = ke2 - nhalo + ii
          endif
          if(nside1.eq.nside2) then
            xisign  = -1.
            etasign = -1.
            j1 = max(jbegin1,jdim2+1-jend2)
            j2 = min(jend1,jdim2+1-jbegin2)
            do 110 jj=j1,j2
              j1h = jj
              j2i = jdim2 + 1 - jj
              xyj1(j1h,k1h) = xyj2(j2i,k2i)
              xy1(j1h,k1h,1) = xisign  * xy2(j2i,k2i,1)
              xy1(j1h,k1h,2) = xisign  * xy2(j2i,k2i,2)
              xy1(j1h,k1h,3) = etasign * xy2(j2i,k2i,3)
              xy1(j1h,k1h,4) = etasign * xy2(j2i,k2i,4)
 110        continue
          else
c            xisign  = 1.
c            etasign = 1.
            j1 = max(jbegin1,jbegin2)
            j2 = min(jend1,jend2)
            do 115 jj=j1,j2 
              j1h = jj
              j2i = jj
              xyj1(j1h,k1h) = xyj2(j2i,k2i)
              xy1(j1h,k1h,1) = xy2(j2i,k2i,1)
              xy1(j1h,k1h,2) = xy2(j2i,k2i,2)
              xy1(j1h,k1h,3) = xy2(j2i,k2i,3)
              xy1(j1h,k1h,4) = xy2(j2i,k2i,4)
 115        continue
          endif
        elseif(nside2.eq.2.or.nside2.eq.4)then
c         -block 1 - side 1 or 3 connected to block 2 - side 2 or 4 
          stop ' fixmets error'
        endif
c ********************************************************************
c **  block 1   :      side 2 or 4                                  **
c ********************************************************************
      elseif(nside1.eq.2.or.nside1.eq.4) then
        if(nside1.eq.2) then
          j1h = jbegin1 + ii
        else 
          j1h = jend1 - ii
        endif 
        if(nside2.eq.2.or.nside2.eq.4)then
csdr      -if nhalo=2 then second halo column is done first
          if(nside2.eq.2) then
            j2i = jb2 + nhalo - ii
          else
            j2i = je2 - nhalo + ii
          endif
          if(nside1.eq.nside2) then
            xisign = -1.
            etasign= -1.
            k1 = max(kbegin1,kdim2+1-kend2)
            k2 = min(kend1,kdim2+1-kbegin2)
            do 210 kk=k1,k2
              k1h = kk
              k2i = kdim2 + 1 - kk
              xyj1(j1h,k1h) = xyj2(j2i,k2i)
              xy1(j1h,k1h,1) = xisign  * xy2(j2i,k2i,1)
              xy1(j1h,k1h,2) = xisign  * xy2(j2i,k2i,2)
              xy1(j1h,k1h,3) = etasign * xy2(j2i,k2i,3)
              xy1(j1h,k1h,4) = etasign * xy2(j2i,k2i,4)
 210        continue
          else
c            xisign = 1.
c            etasign= 1.
            k1 = max(kbegin1,kbegin2)
            k2 = min(kend1,kend2)
            do 215 kk=k1,k2
              k1h = kk
              k2i = kk
              xyj1(j1h,k1h) = xyj2(j2i,k2i)
              xy1(j1h,k1h,1) = xy2(j2i,k2i,1)
              xy1(j1h,k1h,2) = xy2(j2i,k2i,2)
              xy1(j1h,k1h,3) = xy2(j2i,k2i,3)
              xy1(j1h,k1h,4) = xy2(j2i,k2i,4)
 215        continue
          endif
        elseif(nside2.eq.1.or.nside2.eq.3)then
          stop ' fixmets error'
        endif
      else
        stop ' error in fixmets, nside1'
      endif
 99   continue
      return
      end
