c-----------------------------------------------------------------------
c     -- direct problem (flow solve or sensitivity) jacobian and rhs
c     scaling for body and farfield b.c. --
c     -- m. nemec, sept. 2001 --
c     -- called from nksolve.f --
c-----------------------------------------------------------------------
      subroutine sdirect( ib, is, jmax, kmax, nmax, ks, ke, js, je,
     &     precmat, jacmat, iex, bcf, indx, s, pa, as, ipa, ia, maxjb,
     &     mxbfine)

      implicit none
      integer ib, is, jmax, kmax, nmax, ks, ke, js, je, maxjb, mxbfine
      integer iex(maxjb,4,4,mxbfine), indx(jmax,kmax), ipa(*), ia(*)
      integer j,k,n,jk,jpb,jpe,ii
      double precision bcf(maxjb,4,4,mxbfine),s(jmax,kmax,4),as(*),pa(*)
      logical   precmat,jacmat

c     -- local array --
      double precision tmp(maxjb,4)

c     -- shuffle and scale s vector --
c     -- do not do this if solving the adjoint equation --

      if (is.eq.1) then
         k = ks
         do n = 1,4
            do j = js, je
               tmp(j,n) = s(j,k,iex(j,n,is,ib))
            end do
         end do
         do n = 1,4
            do j = js, je
               s(j,k,n) = tmp(j,n)*bcf(j,n,is,ib)
            end do
         end do

      else if (is.eq.2) then
         j = js
         do n = 1,4
            do k = ks+1, ke-1
               tmp(k,n) = s(j,k,iex(k,n,is,ib))
            end do
         end do
         do n = 1,4
            do k = ks+1, ke-1
               s(j,k,n) = tmp(k,n)*bcf(k,n,is,ib)
            end do
         end do 

      else if (is.eq.3) then
         k = ke
         do n = 1,4
            do j = js, je
               tmp(j,n) = s(j,k,iex(j,n,is,ib))
            end do
         end do
         do n = 1,4
            do j = js, je 
               s(j,k,n) = tmp(j,n)*bcf(j,n,is,ib)
            end do
         end do 

      else if (is.eq.4) then
         j = je
         do n = 1,4
            do k = ks+1, ke-1
               tmp(k,n) = s(j,k,iex(k,n,is,ib))
            end do
         end do
         do n = 1,4
            do k = ks+1, ke-1
               s(j,k,n) = tmp(k,n)*bcf(k,n,is,ib)
            end do
         end do 
      end if

c     -- scale preconditioner --
      if ( precmat ) then

         if (is.eq.1) then

            k = ks
            do j = js, je
               do n = 1,4
                  jk = ( indx(j,k) - 1 )*nmax + n
                  jpb = ipa(jk)
                  jpe = ipa(jk+1) - 1
                  do ii = jpb,jpe
                     pa(ii) = pa(ii)*bcf(j,n,is,ib)
                  end do
               end do
            end do

         else if (is.eq.2) then

            j = js
            do k = ks+1, ke-1
               do n = 1,4
                  jk = ( indx(j,k) - 1 )*nmax + n
                  jpb = ipa(jk)
                  jpe = ipa(jk+1) - 1
                  do ii = jpb,jpe
                     pa(ii) = pa(ii)*bcf(k,n,is,ib)
                  end do
               end do
            end do
            
         else if (is.eq.3) then

            k = ke
            do j = js, je
               do n = 1,4
                  jk = ( indx(j,k) - 1 )*nmax + n
                  jpb = ipa(jk)
                  jpe = ipa(jk+1) - 1
                  do ii = jpb,jpe
                     pa(ii) = pa(ii)*bcf(j,n,is,ib)
                  end do
               end do
            end do

         else if (is.eq.4) then

            j = je
            do k = ks+1, ke-1
               do n = 1,4
                  jk = ( indx(j,k) - 1 )*nmax + n
                  jpb = ipa(jk)
                  jpe = ipa(jk+1) - 1
                  do ii = jpb,jpe
                     pa(ii) = pa(ii)*bcf(k,n,is,ib)
                  end do
               end do
            end do 

         end if      

      end if

c     -- scale jacobian --
      if (jacmat) then

         if (is.eq.1) then

            k = ks
            do j = js, je
               do n = 1,4
                  jk = ( indx(j,k) - 1 )*nmax + n
                  jpb = ia(jk)
                  jpe = ia(jk+1) - 1
                  do ii = jpb,jpe
                     as(ii) = as(ii)*bcf(j,n,is,ib)
                  end do
               end do
            end do

         else if (is.eq.2) then

            j = js
            do k = ks+1, ke-1
               do n = 1,4
                  jk = ( indx(j,k) - 1 )*nmax + n
                  jpb = ia(jk)
                  jpe = ia(jk+1) - 1
                  do ii = jpb,jpe
                     as(ii) = as(ii)*bcf(k,n,is,ib)
                  end do
               end do
            end do
            
         else if (is.eq.3) then

            k = ke
            do j = js, je
               do n = 1,4
                  jk = ( indx(j,k) - 1 )*nmax + n
                  jpb = ia(jk)
                  jpe = ia(jk+1) - 1
                  do ii = jpb,jpe
                     as(ii) = as(ii)*bcf(j,n,is,ib)
                  end do
               end do
            end do

         else if (is.eq.4) then

            j = je
            do k = ks+1, ke-1
               do n = 1,4
                  jk = ( indx(j,k) - 1 )*nmax + n
                  jpb = ia(jk)
                  jpe = ia(jk+1) - 1
                  do ii = jpb,jpe
                     as(ii) = as(ii)*bcf(k,n,is,ib)
                  end do
               end do
            end do 

         end if      

      end if
      
      return
      end                       !sdirect
