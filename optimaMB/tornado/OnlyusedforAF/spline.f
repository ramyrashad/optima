      subroutine spline(jdim,x,y,yp1,ypn,r,a,b,c)
c
c     Written by Stan De Rango 23/06/97
c
      implicit none
      integer j,jdim
      double precision yp1,ypn,x(jdim),y(jdim)
      double precision a(jdim),b(jdim),c(jdim),r(jdim)
c
      do 5 j=2,jdim-1
         a(j)=(-x(j-1)+x(j))/6.d0
         b(j)=(-x(j-1)+x(j+1))/3.d0
         c(j)=(-x(j)+x(j+1))/6.d0
         r(j)=(-y(j)+y(j+1))/(-x(j)+x(j+1)) -
     &                         (-y(j-1)+y(j))/(-x(j-1)+x(j))
 5    continue
c
      j=2
      r(j)=r(j)-a(j)*yp1
c
      j=jdim-1
      r(j)=r(j)-c(j)*ypn
c
      call thomas(2,jdim-1,a,b,c,r)
c
      j=1
      r(j)=yp1
c
      j=jdim
      r(j)=ypn
c
      return
      end
      
