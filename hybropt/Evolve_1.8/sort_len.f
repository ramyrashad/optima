      subroutine sort_len(nchrom,nobfun,fit,fit_len,ind_fit,i_mnx)

      dimension fit(nobfun,nchrom),fit_len(nchrom)
      dimension ind_fit(nchrom)
c tmps
      dimension f_len(nchrom),f(nobfun,nchrom)

      call setv(nchrom,0.0,f_len)


      do nc = 1,nchrom
         ind_fit(nc) = nc
         do no = 1,nobfun
            f_len(nc) = fit(no,nc)**2 + f_len(nc)
         enddo
      enddo

      if(i_mnx .eq.1)then
         do ncc=1,nchrom
            do nc=1,nchrom-1
               if (f_len(nc) .lt. f_len(nc+1) )then
                  fd            = f_len(nc)
                  id            = ind_fit(nc)
                  f_len(nc)     = f_len(nc+1)
                  ind_fit(nc)   = ind_fit(nc+1)
                  f_len(nc+1)   = fd
                  ind_fit(nc+1) = id
               endif
            enddo
         enddo
      else
         do ncc=1,nchrom
            do nc=1,nchrom-1
               if (f_len(nc) .gt. f_len(nc+1) )then
                  fd            = f_len(nc)
                  id            = ind_fit(nc)
                  f_len(nc)     = f_len(nc+1)
                  ind_fit(nc)   = ind_fit(nc+1)
                  f_len(nc+1)   = fd
                  ind_fit(nc+1) = id
               endif
            enddo
         enddo
      endif

c$$$% sort fit;
      call cpyv(nobfun*nchrom,fit,f)

      do no = 1,nobfun
         do nc = 1,nchrom
            fit(no,nc) = f(no,ind_fit(nc))
         enddo
      enddo

      return
      end
