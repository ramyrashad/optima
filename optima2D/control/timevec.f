c----------------------------------------------------------------------
c     -- construct time vector for startup --
c
c     written by: john gatsis
c     date: april 2005
c     modified by Markus Rumpfkeil
c----------------------------------------------------------------------

      subroutine timevec (jdim,kdim,ndim,na,xyj,indx,q,s,ipa,jpa,pa,
     &      nki,residfl,rhsstore,dtvec)
      
#include "../include/arcom.inc"

c     -- input --

      integer jdim,kdim,ndim,nki, iadd,indx(jdim,kdim)
      double precision xyj(jdim,kdim)
  
      double precision q(jdim,kdim,ndim),s(jdim,kdim,ndim),rhsstore

      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)

c     -- output --

      double precision dtvec(*)

c     -- local --

      integer unstable,n,ir,idiag
      double precision delta_ref, delta_min,alphat,residfl
      double precision tau,r,Jn,Sn,dnut,nut,tflow,tturb
      logical start

c     -------------------------
c     -- reference time step --
c     -------------------------
c     reference flow conditions:
c     
c     (1) Not Turbulent (viscious and inviscid)
c     (2) Viscous and Turbulent (Spalart-Allmaras)
c
c     Note: For unsteady run we don't need small timesteps 
c     in order to converge
c     --------------------------------------------


      if (.not.turbulnt) then
c     Not Turbulent (viscious and inviscid)
        
         delta_min = 1.0d1
         alphat    = 1.0d0
         beta      = 1.0d0

         if (.not. unsted .and. nki.le.3) then
            delta_ref = 1.0d0
            start = .true.
         else if (.not. unsted .and. nki.le.6 .and. nki .gt.3) then
            delta_ref = 1.0d1
            start = .true.
         else
            delta_ref = max(alphat/residfl**beta,delta_min)
            start = .false.
         end if
         
      else
c     Viscous and Turbulent
     
         if (fsmach .lt. 0.4d0) then
c     % subsonic case
c     % M=0.30, AOA=0.0, Re=3e6
            delta_min = 2.0d1
            alphat = 1.0d0
            beta = 1.0d0
            r = 0.4d0
            tau = 2.0d0
            
            if (.not. unsted .and. nki.le.10) then
               delta_ref = delta_min
               start = .true.
            else if (.not. unsted .and. nki.le.15) then
               delta_ref = 2*delta_min
               start = .true.
            else if (.not. unsted .and. nki.le.20) then
               delta_ref = 3*delta_min
               start = .true.
            else if (.not. unsted .and. nki.le.25) then
               delta_ref = 5*delta_min
               start = .true.
            else
               delta_ref = max(alphat/residfl**(beta),delta_min)
               start = .false.
            end if
            
         else
c     % transonic case
c     % M=0.729, AOA=2.31, Re=6.5e6

            delta_min = 2.0d1
            alphat = 1.0d-1
            beta = 1.0d0
            r = 0.4d0
            tau = 2.0d0
            
            if (.not. unsted .and. nki.le.10) then
               delta_ref = 1.0d0
               start = .true.
            else if (.not. unsted .and. nki.le.20) then
               delta_ref = 1.0d1
               start = .true.
c$$$            else if (.not. unsted .and. nki.le.20) then
c$$$               delta_ref = 2.0d1
c$$$               start = .true.
            else if (.not. unsted .and. nki.le.25) then
               delta_ref = 1.0d2
               start = .true.
            else
               delta_ref = max(alphat/residfl**(beta),delta_min)
               start = .false.
            end if

         end if                 ! fsmach .lt. 0.5d0
         
      end if                    ! turbulnt?

c     ---------------
c     -- time step --
c     ---------------

      unstable = 0
      
      do j=jlow,jup
         do k=klow,kup
            
            jk = (indx(j,k)-1)*ndim               
c     mean flow
            tflow = delta_ref / (1.0d0 + dsqrt(xyj(j,k)))
            
            do n=1,4                  
               dtvec(jk+n) = 1.0d0/tflow
            end do
               
c     turbulence
            
            if (viscous .and. turbulnt .and. itmodel.eq.2) then
                         
               ir = jk + 5
               idiag = 0
               do n = ipa(ir),ipa(ir+1)-1
                  if ( jpa(n).eq.ir ) then
                     idiag = n
                  end if
               end do
               
               Jn = pa(idiag)
               
               Sn = s(j,k,5)
               
               nut = q(j,k,5)*xyj(j,k)
               
               dnut = Sn / Jn
               
               if (dabs(dnut).gt.r*max(nut,1.0d0)) then ! estimated -ve update

                  tturb = Sn/r/max(nut,1.0d0) - Jn
                  tturb = dabs(1.0d0/tturb)
                     
                  unstable = unstable + 1
                  
               else             ! proceed with scaled mean flow timestep
                  
                  tturb = tau * tflow
                  
               end if
               
               dtvec(jk+5) = 1.0d0/tturb
               
            end if              ! turbulnt
            
         end do
      end do

      if (clalpha2) then
         if (resid .gt. 1.d-3) then
            Jn = pa(na)               
            Sn = rhsstore                            
            dnut = Sn / Jn              
            tturb = Sn/r - Jn
            dtvec(na)=dabs(1.0d0/tturb)
         else
            dtvec(na)=1.0d0/delta_ref
         end if
      end if

c     ------------------------------
c     -- report average timesteps --
c     ------------------------------


c     turbulent

c$$$      if (viscous .and. turbulnt .and. itmodel.eq.2) then        
c$$$
c$$$         if (start) then
c$$$            
c$$$            write (*,92) nki,delta_min,alphat/residfl**(beta),delta_ref
c$$$     &           ,delta_ref*tau,unstable
c$$$ 92         format('START time (nki,min,aresb,dtref,dtturb,unstbl)',
c$$$     &           I4,4E9.2,I4)
c$$$
c$$$         else if (delta_min.gt.alphat/residfl*(beta)) then
c$$$            
c$$$            write (*,93) nki,delta_min,alphat/residfl**(beta),delta_ref
c$$$     &           ,delta_ref*tau,unstable
c$$$ 93         format('MINIM time (nki,min,aresb,dtref,dtturb,unstbl)',
c$$$     &           I4,4E9.2,I4)
c$$$            
c$$$         else
c$$$            
c$$$            write (*,94) nki,delta_min,alphat/residfl**(beta),delta_ref
c$$$     &           ,delta_ref*tau,unstable
c$$$ 94         format('ACCEL time (nki,min,aresb,dtref,dtturb,unstbl)',
c$$$     &           I4,4E9.2,I4)
c$$$            
c$$$         end if
c$$$         
c$$$c     inviscid
c$$$
c$$$      else
c$$$         
c$$$         if (start) then
c$$$            
c$$$            write (*,95) nki,delta_min,alphat/residfl**(beta),delta_ref
c$$$ 95         format('START time (nki,min,aresb,dtref)',
c$$$     &           I4,3E9.2)
c$$$            
c$$$         else if (delta_min.gt.alphat/residfl*(beta)) then
c$$$
c$$$            write (*,96) nki,delta_min,alphat/residfl**(beta),delta_ref
c$$$ 96         format('MINIM time (nki,min,aresb,dtref)',
c$$$     &           I4,3E9.2)
c$$$          
c$$$         else
c$$$            
c$$$            write (*,97) nki,delta_min,alphat/residfl**(beta),delta_ref
c$$$ 97         format('ACCEL time (nki,min,aresb,dtref)',
c$$$     &           I4,3E9.2)
c$$$
c$$$         end if
c$$$
c$$$      end if

      return
      end                       !timevec
