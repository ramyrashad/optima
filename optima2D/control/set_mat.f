c----------------------------------------------------------------------
c     -- setup sparse jacobian and preconditioner matrices --
c     
c     written by: marian nemec
c     date: july 2000
c     modified by: laura billing
c     date: june 2005
c----------------------------------------------------------------------
      subroutine set_mat (jdim, kdim, ndim, jt1, jt2, indx, icol, ia,
     &      ja, ipa, jpa)

      implicit none
      
#include "../include/arcom.inc"      

      integer jdim, kdim, ndim, jmaxtmp,jptwo(0:jdim),jmtwo(0:jdim)

      integer i, j, k, m, n, jk, ii, jj, jp2, jp1, jm1, jm2
      integer kp2, kp1, km1, km2, jt1, jt2, jt, jkt, jcol, na

      integer jsta,jsto
      
      integer indx(jdim,kdim), icol(9)
clb
      integer ia(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)

      integer jtemp(jdim*kdim*ndim), onoff(jdim*kdim*ndim)

clb   size of ia, ja, ipa, jpa increased to make space for terms introduced
clb   due to varying angle of attack in the flow solver.

      double precision tmp

c     ------------------------
c     -- plot variables --      
      character*10 title
      character*2 munt 
      integer lines(1)
      munt = 'in'
c     ------------------------

      if (.not.periodic) then
         jmaxtmp = jdim
         jmax=jdim
      else
         jmaxtmp = jdim-1
         jmax=jdim-1
      end if
      
      kmax = kdim

      do j=1,jdim*kdim*ndim
         jtemp(j)=0
         onoff(j)=0
      end do

c                                                                       
c     set periodic index                                         
c
      jm = jmaxtmp-1                                                       
      km = kmax-1                                                       
      jbegin = 1                                                        
      jend = jmaxtmp                                                       
      kbegin = 1                                                        
      kend = kmax                                                       
      klow = 2                                                          
      kup = km                                                          
      do j = 1, jdim                                                 
         jplus(j) = j+1                                                 
         jminus(j) = j-1 
         jptwo(j)= j+2
         jmtwo(j)= j-2
      end do
c                                                                       
      if (.not.periodic) then                                        
         jplus(0)= 1
         jplus(jmaxtmp) = jmaxtmp                                             
         jminus(1) = 1                                                  
         jlow = 2                                                       
         jup = jmaxtmp-1 

         jptwo(jmaxtmp-1)=jmaxtmp
         jptwo(jmaxtmp)=jmaxtmp
         jmtwo(2)=1
         jmtwo(1)=1
      else 
         jplus(0)= 1
         jplus(jmaxtmp) = 1
         jplus(jmaxtmp+1) = 2                                                
         jminus(1) = jmaxtmp 
         jminus(0) = jmaxtmp-1
         jlow = 1                                                       
         jup = jmaxtmp                                                    
         jtail1 = 1                                                     
         jtail2 = jmaxtmp  
         
         
         jptwo(jmaxtmp-1)=1
         jptwo(jmaxtmp)=2
         jptwo(jmaxtmp+1)=3
         jmtwo(2)=jmaxtmp
         jmtwo(1)=jmaxtmp-1

         jptwo(0)=2
         jmtwo(0)=jmaxtmp-2
      endif

      jt1=jtail1
      jt2=jtail2
                          
      na = jmaxtmp*kmax*ndim

      do i = 1,na+2
        ia(i) = 0
        ipa(i) = 0
      end do

      do i = 1,na*(9*ndim+1)+jmax*ndim*3+1
        ja(i) = 0
      end do

      do i = 1,na*5*ndim+1
        jpa(i) = 0
      end do

c     -- setup column indx used for constructing sparse arrays --
      icol(1) = ndim
      do i = 2,9
        icol(i) = icol(i-1) + ndim
      end do

      if(.not.periodic) then
         jsta=jbegin+2
         jsto=jend-2
      else
         jsta=jbegin
         jsto=jend
      end if

c     -----------------------------------------------------------------
c     -- second order jacobian --
c     -- setup sparse matrix indx ia and ja --
c     -- all interior nodes where central-differencing is applied --
      do j = jsta,jsto
        do k = kbegin+2,kend-2
          do n = 1,ndim

            jk = ( indx(j,k) - 1 )*ndim + n
            jj = ( jk - 1 )*icol(9)

            if (clopt) jj = ( jk - 1 )*(icol(9)+1)

            jm2 = ( indx(jmtwo(j),k) - 1 )*ndim
            jm1 = ( indx(jminus(j),k) - 1 )*ndim
            jp1 = ( indx(jplus(j),k) - 1 )*ndim
            jp2 = ( indx(jptwo(j),k) - 1 )*ndim

            km2 = ( indx(j,k-2) - 1 )*ndim
            km1 = ( indx(j,k-1) - 1 )*ndim
            kp1 = ( indx(j,k+1) - 1 )*ndim
            kp2 = ( indx(j,k+2) - 1 )*ndim

            do m = 1,ndim
              ja(jj+        m) = jm2 + m 
              ja(jj+icol(1)+m) = jm1 + m  
              ja(jj+icol(2)+m) = km2 + m
              ja(jj+icol(3)+m) = km1 + m
              ja(jj+icol(4)+m) = jk  - n + m
              ja(jj+icol(5)+m) = kp1 + m
              ja(jj+icol(6)+m) = kp2 + m
              ja(jj+icol(7)+m) = jp1 + m
              ja(jj+icol(8)+m) = jp2 + m
            end do
            ia(jk) = 9*ndim
          end do
        end do
      end do

      if(.not.periodic) then

c     -- jbegin far-field boundary --
      j=jbegin
      do k = kbegin,kend
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)
          jp1 = ( indx(jplus(j),k) - 1 )*ndim

          if (clopt) jj = ( jk - 1 )*(icol(9)+1)

          do m = 1,ndim
            ja(jj+        m)   = jk  - n + m
            ja(jj+icol(1)+m) = jp1 + m
          end do
          ia(jk) = 2*ndim

          if (clopt) then
             ja(jj+icol(1)+ndim+1) = na+1
clb             write(*,*) 'jbegin bndy ja indx', jj+icol(1)+ndim+1
             ia(jk) = 2*ndim+1
          end if

        end do
      end do

c     -- jend far-field boundary --
      j=jend
      do k = kbegin,kend
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)
          jm1 = ( indx(jminus(j),k) - 1 )*ndim
          
          if (clopt) jj = ( jk - 1 )*(icol(9)+1)

          do m = 1,ndim
            ja(jj+icol(1)+m) = jk  - n + m
            ja(jj+        m) = jm1 + m
          end do
          ia(jk) = 2*ndim

          if (clopt) then
             ja(jj+icol(1)+ndim+1) = na+1
clb             write(*,*) 'jend bndy ja indx', jj+icol(1)+ndim+1
             ia(jk) = 2*ndim+1
          end if

        end do
      end do

      end if  !not periodic

c     -- kend far-field boundary
      k=kend
      do j=jlow,jup
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)
          km1 = ( indx(j,k-1) - 1 )*ndim
            
          if (clopt) jj = ( jk - 1 )*(icol(9)+1)

          do m = 1,ndim
            ja(jj+icol(1)+m) = jk  - n + m
            ja(jj+        m) = km1 + m
          end do
          ia(jk) = 2*ndim

          if (clopt) then
             ja(jj+icol(1)+ndim+1) = na+1
             ia(jk) = 2*ndim+1
          end if

        end do
      end do

      if(.not.periodic) then

c     -- explicit wake cut, 'bottom' section --
      k = kbegin
      do j = jlow,jt1-1
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)
          kp1 = ( indx(j,k+1) - 1 )*ndim
          jt  = jmaxtmp-j+1
          jkt = ( indx(jt,k+1) - 1 )*ndim

          if (clopt) jj = ( jk - 1 )*(icol(9)+1)
          do m = 1,ndim
            ja(jj+        m) = jk - n + m
            ja(jj+icol(1)+m) = kp1 + m
            ja(jj+icol(2)+m) = jkt + m
          end do
          ia(jk) = 3*ndim  
        end do
      end do

      end if  !not periodic

c     -- airfoil body --
      k = kbegin
      if (.not. viscous) then       
        do j = jt1,jt2
          do n = 1,ndim

            jk  = ( indx(j,k) - 1 )*ndim + n
            jj  = ( jk - 1 )*icol(9)
            kp1 = ( indx(j,k+1) - 1 )*ndim
            kp2 = ( indx(j,k+2) - 1 )*ndim

            if (clopt) jj = ( jk - 1 )*(icol(9)+1)

            do m = 1,ndim
              ja(jj+        m) = jk - n + m
              ja(jj+icol(1)+m) = kp1 + m
              ja(jj+icol(2)+m) = kp2 + m
            end do
            ia(jk) = 3*ndim 
          end do
        end do
      else
        do j = jt1,jt2
          do n = 1,ndim

            jk  = ( indx(j,k) - 1 )*ndim + n
            jj  = ( jk - 1 )*icol(9)
            kp1 = ( indx(j,k+1) - 1 )*ndim

            if (clopt) jj = ( jk - 1 )*(icol(9)+1)

            do m = 1,ndim
              ja(jj+        m) = jk - n + m
              ja(jj+icol(1)+m) = kp1 + m
            end do
            ia(jk) = 2*ndim  
          end do
        end do
      end if

      if(.not.periodic) then

c     -- explicit wake cut, top section --
      k = kbegin  
      do j = jt2+1,jup
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)
          kp1 = ( indx(j,k+1) - 1 )*ndim
          jt = jmaxtmp-j+1
          jkt = ( indx(jt,k+1) - 1 )*ndim

          if (clopt) jj = ( jk - 1 )*(icol(9)+1)

          do m = 1,ndim
            ja(jj+        m) = jk - n + m
            ja(jj+icol(1)+m) = kp1 + m
            ja(jj+icol(2)+m) = jkt + m
          end do
          ia(jk) = 3*ndim
        end do
      end do

      end if  ! not periodic

      if(.not.periodic) then

c     -- j first interior point dissipation stencil --
      j = jlow
      do k = kbegin+2,kend-2
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)

          if (clopt) jj = ( jk - 1 )*(icol(9)+1)

          jm1 = ( indx(jminus(j),k) - 1 )*ndim
          jp1 = ( indx(jplus(j),k) - 1 )*ndim
          jp2 = ( indx(jptwo(j),k) - 1 )*ndim

          km2 = ( indx(j,k-2) - 1 )*ndim
          km1 = ( indx(j,k-1) - 1 )*ndim
          kp1 = ( indx(j,k+1) - 1 )*ndim
          kp2 = ( indx(j,k+2) - 1 )*ndim

          do m = 1,ndim
            ja(jj+        m) = jm1 + m  
            ja(jj+icol(1)+m) = km2 + m
            ja(jj+icol(2)+m) = km1 + m
            ja(jj+icol(3)+m) = jk  - n + m
            ja(jj+icol(4)+m) = kp1 + m
            ja(jj+icol(5)+m) = kp2 + m
            ja(jj+icol(6)+m) = jp1 + m
            ja(jj+icol(7)+m) = jp2 + m
          end do
          ia(jk) = 8*ndim
        end do
      end do

c     -- j last interior point dissipation stencil --
      j=jup
      do k = kbegin+2,kend-2
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)

          if (clopt) jj = ( jk - 1 )*(icol(9)+1)

          jm2 = ( indx(jmtwo(j),k) - 1 )*ndim
          jm1 = ( indx(jminus(j),k) - 1 )*ndim
          jp1 = ( indx(jplus(j),k) - 1 )*ndim

          km2 = ( indx(j,k-2) - 1 )*ndim
          km1 = ( indx(j,k-1) - 1 )*ndim
          kp1 = ( indx(j,k+1) - 1 )*ndim
          kp2 = ( indx(j,k+2) - 1 )*ndim

          do m = 1,ndim
            ja(jj+        m) = jm2 + m
            ja(jj+icol(1)+m) = jm1 + m
            ja(jj+icol(2)+m) = km2 + m
            ja(jj+icol(3)+m) = km1 + m
            ja(jj+icol(4)+m) = jk  - n + m
            ja(jj+icol(5)+m) = kp1 + m
            ja(jj+icol(6)+m) = kp2 + m
            ja(jj+icol(7)+m) = jp1 + m
          end do
          ia(jk) = 8*ndim
        end do
      end do

      end if  !not periodic

c     -- k first interior point dissipation stencil --
      k=klow
      do j = jsta,jsto
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)

          if (clopt) jj = ( jk - 1 )*(icol(9)+1)

          jm2 = ( indx(jmtwo(j),k) - 1 )*ndim
          jm1 = ( indx(jminus(j),k) - 1 )*ndim
          jp1 = ( indx(jplus(j),k) - 1 )*ndim
          jp2 = ( indx(jptwo(j),k) - 1 )*ndim

          km1 = ( indx(j,k-1) - 1 )*ndim
          kp1 = ( indx(j,k+1) - 1 )*ndim
          kp2 = ( indx(j,k+2) - 1 )*ndim

          do m = 1,ndim
            ja(jj+        m) = jm2 + m
            ja(jj+icol(1)+m) = jm1 + m
            ja(jj+icol(2)+m) = km1 + m
            ja(jj+icol(3)+m) = jk  - n + m
            ja(jj+icol(4)+m) = kp1 + m
            ja(jj+icol(5)+m) = kp2 + m
            ja(jj+icol(6)+m) = jp1 + m
            ja(jj+icol(7)+m) = jp2 + m
          end do
          ia(jk) = 8*ndim
        end do
      end do

c     -- k last interior point dissipation stencil --
      k=kup
      do j = jsta,jsto
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(9)

          if (clopt) jj = ( jk - 1 )*(icol(9)+1)

          jm2 = ( indx(jmtwo(j),k) - 1 )*ndim
          jm1 = ( indx(jminus(j),k) - 1 )*ndim
          jp1 = ( indx(jplus(j),k) - 1 )*ndim
          jp2 = ( indx(jptwo(j),k) - 1 )*ndim

          km2 = ( indx(j,k-2) - 1 )*ndim
          km1 = ( indx(j,k-1) - 1 )*ndim
          kp1 = ( indx(j,k+1) - 1 )*ndim

          do m = 1,ndim
            ja(jj+        m) = jm2 + m
            ja(jj+icol(1)+m) = jm1 + m
            ja(jj+icol(2)+m) = km2 + m
            ja(jj+icol(3)+m) = km1 + m
            ja(jj+icol(4)+m) = jk  - n + m
            ja(jj+icol(5)+m) = kp1 + m
            ja(jj+icol(6)+m) = jp1 + m
            ja(jj+icol(7)+m) = jp2 + m
          end do
          ia(jk) = 8*ndim
        end do
      end do

      if(.not.periodic) then

c     -- first interior points in j and k directions for dissipation --
      j = jlow
      k = klow
      do n = 1,ndim

        jk  = ( indx(j,k) - 1 )*ndim + n
        jj  = ( jk - 1 )*icol(9)

        if (clopt) jj = ( jk - 1 )*(icol(9)+1)

        jm1 = ( indx(jminus(j),k) - 1 )*ndim
        jp1 = ( indx(jplus(j),k) - 1 )*ndim
        jp2 = ( indx(jptwo(j),k) - 1 )*ndim

        km1 = ( indx(j,k-1) - 1 )*ndim
        kp1 = ( indx(j,k+1) - 1 )*ndim
        kp2 = ( indx(j,k+2) - 1 )*ndim

        do m = 1,ndim
          ja(jj+        m) = jm1 + m  
          ja(jj+icol(1)+m) = km1 + m
          ja(jj+icol(2)+m) = jk  - n + m
          ja(jj+icol(3)+m) = kp1 + m
          ja(jj+icol(4)+m) = kp2 + m
          ja(jj+icol(5)+m) = jp1 + m
          ja(jj+icol(6)+m) = jp2 + m
        end do
        ia(jk) = 7*ndim
      end do

      j = jlow
      k = kup
      do n = 1,ndim

        jk  = ( indx(j,k) - 1 )*ndim + n
        jj  = ( jk - 1 )*icol(9)

        if (clopt) jj = ( jk - 1 )*(icol(9)+1)

        jm1 = ( indx(jminus(j),k) - 1 )*ndim
        jp1 = ( indx(jplus(j),k) - 1 )*ndim
        jp2 = ( indx(jptwo(j),k) - 1 )*ndim

        km2 = ( indx(j,k-2) - 1 )*ndim
        km1 = ( indx(j,k-1) - 1 )*ndim
        kp1 = ( indx(j,k+1) - 1 )*ndim

        do m = 1,ndim
          ja(jj+        m) = jm1 + m
          ja(jj+icol(1)+m) = km2 + m
          ja(jj+icol(2)+m) = km1 + m
          ja(jj+icol(3)+m) = jk  - n + m
          ja(jj+icol(4)+m) = kp1 + m
          ja(jj+icol(5)+m) = jp1 + m
          ja(jj+icol(6)+m) = jp2 + m
        end do
        ia(jk) = 7*ndim
      end do

      j=jup
      k = klow
      do n = 1,ndim

        jk  = ( indx(j,k) - 1 )*ndim + n
        jj  = ( jk - 1 )*icol(9)

        if (clopt) jj = ( jk - 1 )*(icol(9)+1)

        jm2 = ( indx(jmtwo(j),k) - 1 )*ndim
        jm1 = ( indx(jminus(j),k) - 1 )*ndim
        jp1 = ( indx(jplus(j),k) - 1 )*ndim
        
        km1 = ( indx(j,k-1) - 1 )*ndim
        kp1 = ( indx(j,k+1) - 1 )*ndim
        kp2 = ( indx(j,k+2) - 1 )*ndim

        do m = 1,ndim
          ja(jj+        m) = jm2 + m
          ja(jj+icol(1)+m) = jm1 + m
          ja(jj+icol(2)+m) = km1 + m
          ja(jj+icol(3)+m) = jk  - n + m
          ja(jj+icol(4)+m) = kp1 + m
          ja(jj+icol(5)+m) = kp2 + m
          ja(jj+icol(6)+m) = jp1 + m
        end do
        ia(jk) = 7*ndim
      end do

      j = jup
      k = kup
      do n = 1,ndim

        jk  = ( indx(j,k) - 1 )*ndim + n
        jj  = ( jk - 1 )*icol(9)

        if (clopt) jj = ( jk - 1 )*(icol(9)+1)

        jm2 = ( indx(jmtwo(j),k) - 1 )*ndim
        jm1 = ( indx(jminus(j),k) - 1 )*ndim
        jp1 = ( indx(jplus(j),k) - 1 )*ndim
        
        km2 = ( indx(j,k-2) - 1 )*ndim
        km1 = ( indx(j,k-1) - 1 )*ndim
        kp1 = ( indx(j,k+1) - 1 )*ndim

        do m = 1,ndim
          ja(jj+        m) = jm2 + m
          ja(jj+icol(1)+m) = jm1 + m
          ja(jj+icol(2)+m) = km2 + m
          ja(jj+icol(3)+m) = km1 + m
          ja(jj+icol(4)+m) = jk  - n + m
          ja(jj+icol(5)+m) = kp1 + m
          ja(jj+icol(6)+m) = jp1 + m
        end do
        ia(jk) = 7*ndim
      end do

      end if  !not periodic




      jcol = 1
      tmp = ia(1)
      ia(1) = 1
      do ii = 1,na
        jj = (ii-1)*icol(9)
        if (clopt) jj = (ii-1)*(icol(9)+1)
        do j = jj+1,jj+tmp
          ja(jcol) = ja(j)
          jcol = jcol + 1
        end do
        tmp = ia(ii+1)
        ia(ii+1) = jcol
      end do

clb   Modifications for constant cl, changing alpha flow solves
      if (clopt) then
c     -- airfoil surface - derivative of cl with respect to q
         do k=1,3
            do j=jtail1,jtail2
               do n = 1,4
                  jk  = ( indx(j,k) - 1 )*ndim + n
                  jtemp(jk) = jk
                  onoff(jk) = 1
                  
               end do
            end do
         end do
         do j = 1,jdim*kdim*ndim
            if (onoff(j).eq.1) then
               ja(jcol) = jtemp(j)
clb               write(*,*) 'airfoil surface jcol(final) set mat',jcol
               jcol = jcol+1
            endif
         end do
c     -- adding term for dcldalpha
         ja(jcol) = na + 1
         jcol = jcol + 1
         ia(na+2) = jcol
       
       
clb
clb         write(*,*) 'jcol max',jcol
clb         stop
clb         write(*,*) 'index of dcldalpha'
clb         na=na+1
clb         write(*,*) 'na increased'
      endif

clb      open(unit=2005,file='test_unit',status='new',
clb     &     form='formatted')
clb 
clb      call pspltm (na, na, 0, ja, ia, title, 0, 7.0, munt, 0,
clb     &        lines,  2005)
clb
clb      write(*,*) 'SET_MAT.f'
clb      stop
clb      if (clopt) na = na-1

c     -----------------------------------------------------------------
c     -- first order jacobian --

c     -- setup sparse matrix indx ipa and jpa for preconditioner  --
c     -- interior nodes --
      do k = klow,kup
        do j = jlow,jup
          do n = 1,ndim

            jk = ( indx(j,k) - 1 )*ndim + n
            jj = ( jk - 1 )*icol(5)

            jm1 = ( indx(jminus(j),k) - 1 )*ndim
            jp1 = ( indx(jplus(j),k) - 1 )*ndim
            km1 = ( indx(j,k-1) - 1 )*ndim
            kp1 = ( indx(j,k+1) - 1 )*ndim

            do m = 1,ndim
              jpa(jj+        m) = jm1 + m 
              jpa(jj+icol(1)+m) = km1 + m
              jpa(jj+icol(2)+m) = jk  - n + m
              jpa(jj+icol(3)+m) = kp1 + m
              jpa(jj+icol(4)+m) = jp1 + m
            end do
            ipa(jk) = 5*ndim
          end do
        end do
      end do

c     -- B.C. body surface --
      k = kbegin
      if (.not. viscous) then
        do j = jt1,jt2
          do n = 1,ndim

            jk = ( indx(j,k) - 1 )*ndim + n
            jj = ( jk - 1 )*icol(5)

            kp1 = ( indx(j,k+1) - 1 )*ndim
            kp2 = ( indx(j,k+2) - 1 )*ndim
            
            do m = 1,ndim
              jpa(jj+        m) = jk - n + m
              jpa(jj+icol(1)+m) = kp1 + m
              jpa(jj+icol(2)+m) = kp2 + m
            end do
            ipa(jk) = 3*ndim
          end do
        end do
      else
        do j = jt1,jt2
          do n = 1,ndim

            jk = ( indx(j,k) - 1 )*ndim + n
            jj = ( jk - 1 )*icol(5)
            kp1 = ( indx(j,k+1) - 1 )*ndim
            
            do m = 1,ndim
              jpa(jj+        m) = jk - n + m
              jpa(jj+icol(1)+m) = kp1 + m
            end do
            ipa(jk) = 2*ndim
          end do
        end do
      end if

      if(.not.periodic) then
      
c     -- B.C. wake 1 (bottom) --
      k = kbegin
      do j = jlow,jt1-1
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(5)
          kp1 = ( indx(j,k+1) - 1 )*ndim
          jt  = jmaxtmp-j+1
          jkt = ( indx(jt,k+1) - 1 )*ndim

          do m = 1,ndim
            jpa(jj+        m) = jk - n + m
            jpa(jj+icol(1)+m) = kp1 + m
            jpa(jj+icol(2)+m) = jkt + m
          end do
          ipa(jk) = 3*ndim
        end do
      end do

c     -- B.C. wake 2 (top) --
      do j = jt2+1,jup
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(5)
          kp1 = ( indx(j,k+1) - 1 )*ndim
          jt = jmaxtmp-j+1
          jkt = ( indx(jt,k+1) - 1 )*ndim

          do m = 1,ndim
            jpa(jj+        m) = jk - n + m
            jpa(jj+icol(1)+m) = kp1 + m
            jpa(jj+icol(2)+m) = jkt + m
          end do
          ipa(jk) = 3*ndim
        end do
      end do

      end if  !not periodic

c     -- kend far-field boundary
      k=kend
      do j=jlow,jup
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(5) 
          km1 = ( indx(j,k-1) - 1 )*ndim

          do m = 1,ndim
            jpa(jj+icol(1)+m) = jk  - n + m
            jpa(jj+        m)   = km1 + m
          end do
          ipa(jk) = 2*ndim
        end do
      end do

      if(.not.periodic) then

c     -- jbegin far-field boundary --
      j=jbegin
      do k = kbegin,kend
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(5)
          jp1 = ( indx(jplus(j),k) - 1 )*ndim

          do m = 1,ndim
            jpa(jj+        m) = jk  - n + m
            jpa(jj+icol(1)+m) = jp1 + m
          end do
          ipa(jk) = 2*ndim
        end do
      end do

c     -- jend far-field boundary --
      j=jend
      do k = kbegin,kend
        do n = 1,ndim

          jk  = ( indx(j,k) - 1 )*ndim + n
          jj  = ( jk - 1 )*icol(5)
          jm1 = ( indx(jminus(j),k) - 1 )*ndim

          do m = 1,ndim
            jpa(jj+icol(1)+m) = jk  - n + m
            jpa(jj+        m) = jm1 + m
          end do
          ipa(jk) = 2*ndim
        end do
      end do

      end if  !not periodic
      
c     -- remove zeros --
      jcol = 1
      tmp = ipa(1)
      ipa(1) = 1
      do ii = 1,na
        jj = (ii-1)*icol(5)
        do j = jj+1,jj+tmp
          jpa(jcol) = jpa(j)
          jcol = jcol + 1
        end do
        tmp = ipa(ii+1)
        ipa(ii+1) = jcol
      end do

clb   Modifications for constant cl, changing alpha flow solves
      if (clalpha2) then
         ipa(na+2)=ipa(na+1)+1
         jpa(jcol)=na+1
         na=na+1
      endif

c     -----------------------------------------------------------------
      
c$$$      open( unit=n_best, file='fjac.ps', status='unknown')
c$$$         
c$$$        
c$$$      call pspltm (na, na, 0, jpa, ipa, title, 0, 7.0, munt, 0,
c$$$     &        lines, n_best)
c$$$
c$$$      close(n_best)
c$$$      stop

      return
      end                       !set_mat
