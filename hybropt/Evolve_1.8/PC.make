# Makefile

SHELL=/bin/sh

TARGET = Evolver_1.8
VERSION = Evolve_1.8

FOR= pgf90
FORFLAGS=	-fastsse -r8
LNKSW = -Bstatic
LIBS = -L$(HOME)/lib -llatin_random

TARFILES = $(VERSION)/*.make $(VERSION)/*.f $(VERSION)/test*
TARPATH = ../

OBJS =	Evolver.o initga.o input.o stats.o rwchromo.o \
	solout.o ran3.o sort.o sort2.o rwpareto.o rank.o \
	strlen.o string_codes.o GA_version.o \
	get_params.o rank_ga.o ga_niche.o select.o mut_cross.o \
	sort_par.o sort_rank.o rank_1.o second.o \
	transform.o bin.o bin2.o cull_n.o rwbest.o init_param.o \
	ga_precision.o ga_int_conv.o init_fit.o

$(TARGET):	$(OBJS)
		$(FOR) -o $(TARGET) $(LNKSW) $(FORFLAGS) $(OBJS) $(LIBS)
		/bin/rm -f *.o

install:	$(TARGET)
		mv $(TARGET) $(HOME)/bin/.

clean:
	/bin/rm -f *~ "#"* *.l *.T *.o


install_all:	$(TARGET)	
		cp $(TARGET) $(HOME)/MYscratch/bin_sli/.
		cp $(TARGET) $(HOME)/bin/.
		\rm $(TARGET)

.f.o:
		$(FOR) $(FORFLAGS) -c $*.f 

archive:	clean
		cd $(TARPATH);	tar cvf $(TARGET).tar $(TARFILES); gzip $(TARGET).tar

