c ********************************************************************
c *********************   eta implicit routine   *********************
c ********************************************************************
c
c     main integration calls
c
      subroutine etaimpl(jdim,kdim,q,s,press,sndsp,turmu,fmu,vort,x,y,
     &   xy,xyj,ett,ds,vv,ccy,coef2y,coef4y,spect,precon)
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
c
      dimension q(jdim*kdim*4),spect(jdim*kdim*3),precon(jdim*kdim*8)
      dimension press(jdim*kdim),sndsp(jdim*kdim)
      dimension s(jdim*kdim*4),xy(jdim*kdim*4),xyj(jdim*kdim)
      dimension ett(jdim*kdim),ds(jdim*kdim)
      dimension x(jdim*kdim),y(jdim*kdim),turmu(jdim*kdim)
      dimension vort(jdim*kdim),fmu(jdim*kdim)
      dimension vv(jdim*kdim),ccy(jdim*kdim)
      dimension coef2y(jdim*kdim),coef4y(jdim*kdim)
c
c
#include "../include/scratch.inc"
c
c     ***************************************************************
c     **                  implicit operators                       **
c     ***************************************************************
c
c     -scalar pentadiagonal algorithm
c
c     -for multi-block use tkinv ... not ninver
      ix = 3
      iy = 4
      do 50 ii=nblkstart,nblkend
        call tkinv(ix,iy,jmax(ii),kmax(ii),
     &        q(lqptr(ii)),sndsp(lgptr(ii)),s(lqptr(ii)),
     &        xyj(lgptr(ii)),xy(lqptr(ii)),precon(lprecptr(ii)),
     &        jminbnd(ii),jmaxbnd(ii),kbegin(ii),kend(ii))
c     &        kbegin(ii)-nsd1(ii),kend(ii)+nsd3(ii))
 50   continue
c      
c      
c     -increase matrix diss. limiters on LHS to improve robustness.
      if (.not.matcharc2d) then
c        if (idmodel.eq.2 .and. vleta.lt.0.3) then
        if (idmodel.eq.2 .and. vleta.lt.0.3 
     &                                     .and. visceig.eq.1.) then
          vltmp=vleta
          vleta=0.3
          do 105 ii=nblkstart,nblkend
            lg=lgptr(ii)
            lq=lqptr(ii)
            call mspecty(jmax(ii),kmax(ii),vv(lg),ccy(lg),
     &            xyj(lg),xy(lq),spect(lspptr(ii)),
     &            jminbnd(ii),jmaxbnd(ii),kbegin(ii),kend(ii))
 105      continue
          vleta=vltmp
        endif
      endif
c     
      do 120 ii=nblkstart,nblkend
        call stepf2dy(jmax(ii),kmax(ii),
     &        q(lqptr(ii)),turmu(lgptr(ii)),fmu(lgptr(ii)),
     &        s(lqptr(ii)),press(lgptr(ii)),sndsp(lgptr(ii)),
     &        xy(lqptr(ii)),xyj(lgptr(ii)),ds(lgptr(ii)),
     &        coef2y(lgptr(ii)),coef4y(lgptr(ii)),
     &        vv(lgptr(ii)),ccy(lgptr(ii)),
     &        work1,spect(lspptr(ii)),precon(lprecptr(ii)),
     &        jminbnd(ii),jmaxbnd(ii),
     &        kbegin(ii),kend(ii),klow(ii),kup(ii),
     &        eta1mesh(ii),eta2mesh(ii))
 120  continue
c     
c     -close out with eigenvector matrix multiplication
      ix = 3
      iy = 4
      do 300 ii=nblkstart,nblkend
        call tk(ix,iy,jmax(ii),kmax(ii),
     &        q(lqptr(ii)),sndsp(lgptr(ii)),s(lqptr(ii)),
     &        xyj(lgptr(ii)),xy(lqptr(ii)),precon(lprecptr(ii)),
     &        jminbnd(ii),jmaxbnd(ii),kbegin(ii),kend(ii))
c     &        jbegin(ii),jend(ii),
c     &        kbegin(ii)-nsd1(ii),kend(ii)+nsd3(ii))
 300  continue
c      
c     
      if (prec.gt.0) then
c       -convert rhs back to conservative variables
        do 310 ii=nblkstart,nblkend
          call dum(jmax(ii),kmax(ii),q(lqptr(ii)),
     &          xyj(lgptr(ii)),sndsp(lgptr(ii)),
     &          precon(lprecptr(ii)),s(lqptr(ii)),
     &          jminbnd(ii),jmaxbnd(ii),klow(ii),kup(ii))
 310    continue
      endif
c
c     write(6,*) ' finished etaimpl'
c     call flushit(6)
      return
      end
