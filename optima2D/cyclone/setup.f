      subroutine setup(jdim,kdim,s,xy,xyj,xit,ett,ds,x,y,               
     *                 turmu,fmu,vort) 
c
c     calling subroutine 'initia'
c
#include "../include/arcom.inc"
c                                                                       
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension xit(jdim,kdim),ett(jdim,kdim),ds(jdim,kdim)             
      dimension x(jdim,kdim),y(jdim,kdim),turmu(jdim,kdim)              
      dimension vort(jdim,kdim),fmu(jdim,kdim)
      
      integer jj
      double precision d1, d2
      ! added for transition point placement
      logical foundjtup, foundjtlo
      integer jte
      double precision dist, distx, disty, dist2, xte, yte

c                                                                       
c     jtail2 = jmax  for periodic meshes                               
      if (periodic) jtail2 = jmax                                     
c      
      if (x(1,kmax).ne.x(jmax,kmax) .or. y(1,kmax).ne.y(jmax,kmax))
     &   goto 15
c                                                                       
c     reset periodic jmax and set overlap switch                  
c     
      jmaxold = jmax                                                    
      jmax = jmaxold-1                                                  
      jtail2 = jmax                                                     
      write(out_unit,997)
      write (out_unit,*) '| jmax and jtail2 reset to',jmax,jtail2 
      write(out_unit,997)
 15   continue                                                          
c                                                                       
c     set periodic index                                         
c                                                                       
      jm = jmax-1                                                       
      km = kmax-1                                                       
      jbegin = 1                                                        
      jend = jmax                                                       
      kbegin = 1                                                        
      kend = kmax                                                       
      klow = 2                                                          
      kup = km                                                          
      do 10 j = 1, jmax                                                 
         jplus(j) = j+1                                                 
         jminus(j) = j-1                                                
10    continue                                                          
c                                                                       
      if (.not.periodic) then                                        
         jplus(0)= 1
         jplus(jmax) = jmax                                             
         jminus(1) = 1                                                  
         jlow = 2                                                       
         jup = jmax-1                                                   
      else                                                              
         jplus(0)= 1
         jplus(jmax) = 1                                                
         jminus(1) = jmax                                               
         jlow = 1                                                       
         jup = jmax                                                     
         jtail1 = 1                                                     
         jtail2 = jmax                                                  
         jminus(jmaxold)=jmax
         jplus(jmaxold)=jplus(1)
      endif                                                             
c                  

      if (.not.postProcessOnly) then

         do k=1,kmax                                                  
            do j=1,jmax
               vort( j, k)  = 0.
               fmu( j, k)   = 1.
            end do
         end do
c        
         if (itmodel.eq.1 .or. (itmodel.eq.2 .and. .not.restart)) then
            do k=1,kmax
               do j=1,jmax
                  turmu(j,k)=0.d0
               end do
            end do
         endif

      endif ! (.not.postProcessOnly)
     
      ! ______________________
      ! find leading edge node
      jte=jtail1
      xte=x(jte,1)
      yte=y(jte,1)
      do j=jtail1,jtail2
         distx=x(j,1)-xte
         disty=y(j,1)-yte
         dist2=sqrt(distx**2+disty**2)
         if (dist2.gt.dist) then
            dist=dist2
            jLEsetup=j
         endif
      enddo


      if (transitionalFlow) then

         ! _______________________________________________
         ! Initialize transition point related information
         call transitionPointInit(jdim,kdim,x,y)

c        --------------------------------------------------
c        For Baldwin-Lomax Model ONLY (itmodel.eq.1)
c        - ramp in transition 
c        --------------------------------------------------
         k = 1                                                          

         if (itmodel.eq.1 .and. ramptran.ne.0.) then 
            if (ramptran.lt.0.) then
c              -for linear ramp starting at specified transition pnt.
c              ---------------
               rmpds=abs(ramptran)
               jrampup=jtranup
               jramplo=jtranlo
c              
c              -determine node range of ramp in for upper surface
               do 454 j=jtranup,jend
                  if (x(j,k)-x(jtranup,k) .gt. rmpds) then
                     jrampup=j
                     goto 455
                  endif
 454           continue
 455           continue
c              
c              -ensure that ramp spans at least 2 nodes.
               if (jrampup.eq.jtranup) jrampup=jtranup+1

c              -determine node range of ramp in for lower surface
               do 456 j=jtranlo,jbegin,-1
                  if (x(j,k)-x(jtranlo,k) .gt. rmpds) then
                     jramplo=j
                     goto 457
                  endif
 456           continue
 457           continue
c              
c              -ensure that ramp spans at least 2 nodes.
               if (jramplo.eq.jtranlo) jramplo=jtranlo-1
               jramplo1=jramplo
               jrampup1=jrampup
c              
            else
c              ---------------
c              -for cubic ramp
c              ---------------
c              -start ramp about 1/4*ramptran before specified trans. pnt.
c              -if jrampup1,jrampup2,jramplo1,jramplo2 are > 0 then use
c              these use input values.                
c              if (jrampup1 .gt. 0) then
c              ramptran=x(jrampup2,k)-x(jrampup1,k)
c              goto 993
c              endif

               jte=jtail1
               xte=x(jte,1)
               yte=y(jte,1)
               dist=0.d0
               k=kbegin
               do j=jtail1,jtail2
                  dx=x(j,k)-xte
                  dy=y(j,k)-yte
                  dist2=sqrt(dx**2+dy**2)
                  if (dist2.gt.dist) then
                     dist=dist2
                     jle=j
                     xle=x(j,k)
                  endif
               enddo
c              
c              
               zz=.1d0
c              -Note: Nodes affected are actually jrampup1+1 to jrampup2-1
c              and jramplo1-1 to jramplo2+1
c              -ie. the code scales all eddy viscosities at nodes
c              in between boundaries computed here.
c              
c              -determine node range of ramp in for upper surface
               jrampup1=jle
               do 555 j=jle,jtail2
                  if (x(j,k).lt.(transup - zz*ramptran)) then
                     jrampup1=j
                  endif
 555           continue
c              
               jrampup2=jtail2
               do 654 j=jtail2,jrampup1,-1
c                 write(99,444) j,x(j,k),x(j,k)-x(jtranup1,k),ramptran
c                 444            format(i4,3f15.8)
c                 call flush(99)
c                 if (x(j,k)-x(jrampup1,k) .gt. ramptran) then
                  if (x(j,k).gt.(transup + (1.d0-zz)*ramptran)) then
                     jrampup2=j
                  endif
 654           continue
c              
c              -ensure that ramp spans at least 2 nodes.
               if (jrampup2.eq.jrampup1) jrampup2=jrampup1+1
c              
c              
c              
c              -determine node range of ramp in for lower surface
               jramplo1=jle
               do 557 j=jle,jtail1,-1
                  if (x(j,k) .lt. (translo - zz*ramptran)) then
                     jramplo1=j
                  endif
 557           continue
c              
               jramplo2=jtail1
               do 656 j=jtail1,jramplo1
c                 if (x(j,k)-x(jramplo1,k) .gt. ramptran) then
                  if (x(j,k).gt.(translo + (1.d0-zz)*ramptran)) then
                     jramplo2=j
                  endif
 656           continue
c              
c              -ensure that ramp spans at least 2 nodes.
               if (jramplo2.eq.jramplo1) jramplo2=jramplo1-1
c              
            endif ! if (ramptran.lt.0.) 
         endif ! if (ramptran.ne.0. .and. itmodel.eq.1)
         !-- End of Section for Baldwin-Lomax Model ONLY -- 

         if (itmodel.eq.1) then
            if (ramptran.lt.0.) then
               write(out_unit,996) jtranlo-jramplo+1,-ramptran*100.
               write(out_unit,995)
 995           format(1x,1h|,57x,1h|)
 996           format(1x,39H| Lower transition linearly ramped over
     &              ,i3,1x,6hnodes.,1x,1h(,f4.2,2h%),1x,1H|)
            elseif (ramptran.gt.0.) then
               write(out_unit,896) jramplo1-jramplo2+1,ramptran*100.
               write(out_unit,897) jramplo1,jramplo2
               write(out_unit,995)
 896           format(1x,38H| Lower transition ramped (cubic) over,i3
     &              ,1x,6hnodes.,1x,1h(,f4.2,2h%),2x,1H|)
 897           format(1x,1h|,19x,10Hjramplo1 =,i4,4x,10Hjramplo2 =,i4
     &              ,6x,1H|)
            endif            ! if (ramptran.lt.0.)
         endif               ! if (itmodel.eq.1)
c        
         if (itmodel.eq.1) then
            if (ramptran.lt.0.) then
               write(out_unit,994) jrampup-jtranup+1,-ramptran*100.
 994           format(1x,39H| Upper transition linearly ramped over
     &              ,i3,1x,6hnodes.,1x,1h(,f4.2,2h%),1x,1H|)
            elseif (ramptran.gt.0.) then
               write(out_unit,894) jrampup2-jrampup1+1,ramptran*100.
               write(out_unit,895) jrampup1,jrampup2
 894           format(1x,38H| Upper transition ramped (cubic) over,i3
     &              ,1x,6hnodes.,1x,1h(,f4.2,2h%),2x,1H|)
 895           format(1x,1h|,19x,10Hjrampup1 =,i4,4x,10Hjrampup2 =,i4
     &              ,6x,1H|)
            endif            ! if (ramptran.lt.0.)
         endif               ! if (itmodel.eq.1)
c        
 777     continue
         write(out_unit,997)
         write(out_unit,*)

      else
         ! ______________________________________________________
         ! fully-turbulent flow (i.e. transitionalFlow = .false.)
         ! here we set transition nodes to leading edge nodes,
         ! however, in the SA turbulence model, ft1 and ft2 are
         ! set to zero if and when transitionalFLow = .false.
         jtranup = jLEsetup
         jtranlo = jLEsetup
         jmid    = jLEsetup
      endif ! transitionalFlow

      ! ______________________________________________________
      ! Store transition information for transition prediction
      xTransForced(1) = transup
      xTransForced(2) = translo
      jTransForced(1) = jtranup
      jTransForced(2) = jtranlo
      ! ____________________________
      ! Write transition information 
      write(out_unit,997)
 997     format(2x,57('-'))
c        
      write (out_unit,*)
     &        '| Transition on upper surface at x/c = ', transup, ' |'
      write (out_unit,*)
     &        '| Transition on lower surface at x/c = ', translo, ' |'
c        
c     
c     -initialize time metrics and variable dt                          
      do 8 k=1,kmax
      do 8 j=1,jmax
         xit(j,k) = 0.d0
         ett(j,k) = 0.d0
         ds(j,k)  = 1.0d0
8     continue
c                                                                       
c                                                                       
c     metric calculation                                     

c     -- compute xi derivatives of x, y --
      
      call xidif(jdim,kdim,x,y,xy)                                     

c     -- compute  eta derivatives of x, y --
      call etadif(jdim,kdim,x,y,xy)                                 

c     -- form metrics and jacobian --
      if (jacarea) then
c     -- calc. jacobian using cell areas --
        call calcmet2(jdim,kdim,xy,x,y,xyj,s)
      else
c     -- calc. jacobian using metric terms --
        call calcmet(jdim,kdim,xy,xyj)
c     -- negative jacobian detected --
        if (badjac) return
      endif

c     -- test the invariants of the transformation --
c     call testmet(jdim,kdim,xy,xyj)
c     stop
c     
      do n=1,4
        do k=1,kmax
          do j=1,jmax
            s(j,k,n) = 0.d0
          end do
        end do
      end do

      return                                                            
      end                       !setup     
