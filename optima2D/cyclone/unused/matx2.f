c ##########################
c ##                      ##
c ##  subroutine matx     ##
c ##                      ##
c ##########################
c
      subroutine matx2 (jdim,kdim,q,s,xy,xyj,gam,sndsp,uu,ccx,
     &                           coef2,coef4,work,tmp,tmp2)            
c
c******************************************************************
c   this subroutine follows very faithfully t. pulliam notes, p.31
c
c   fourth order smoothing, added explicitly to rhs                     
c   second order near shocks with pressure grd coeff.                   
c                                                                       
c  xi direction                                                         
c                                                                       
c   start differences each variable separately                          
c******************************************************************
c                                                                       
      include '../include/arcom.inc'
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)          
      dimension work(jdim,kdim,4),uu(jdim,kdim),ccx(jdim,kdim)
      dimension tmp(4,jdim,kdim),tmp2(4,jdim,kdim),gam(4,4,jdim,kdim)
      dimension vec(4),tinv(4,4),t(4,4),xy(jdim,kdim,4),zvec(4)
      dimension coef2h(maxj,maxk),coef4h(maxj,maxk),zjach(maxj,maxk)
c
      gi = 1.d0/gamma                                                 
      gm1i = 1.d0/gami                                                
c
      do 10 n = 1,4                                                     
c----    1st-order forward difference ----
         do 15 k =klow,kup                                                
         do 15 j =jlow,jup                                                
            tmp(n,j,k) = q(j+1,k,n)*xyj(j+1,k) - q(j,k,n)*xyj(j,k)           
 15      continue
c  --    c mesh bc --
         if (.not.periodic) then                                          
            j1 = jbegin                                                    
            j2 = jend                                                      
            do 16 k = klow,kup                                             
               tmp(n,j1,k) = q(j1+1,k,n)*xyj(j1+1,k) -                       
     >                        q(j1,k,n)*xyj(j1,k)                           
               tmp(n,j2,k) = tmp(n,j2-1,k)                                  
 16         continue                                                       
         endif                                                          
c        
c        
c----    apply cent-dif to 1st-order forward ----
         do 17 k =klow,kup                                                
         do 17 j =jlow,jup                                                
            tmp2(n,j,k) = tmp(n,j+1,k)-2.d0*tmp(n,j,k)+tmp(n,j-1,k)     
 17      continue                                                          
c  --    c mesh bc --
         if (.not.periodic) then                                          
            j1 = jbegin                                                    
            j2 = jend                                                      
            do 18 k =klow,kup                                             
               tmp2(n,j1,k) = q(j1+2,k,n)*xyj(j1+2,k) -                   
     >              2.d0*q(j1+1,k,n)*xyj(j1+1,k) + q(j1,k,n)*xyj(j1,k)    
               tmp2(n,j2,k) = 0.                                   
 18         continue
     $              
         endif                                                          
 10   continue                                                          
c
c
      do k=kbegin,kend
      do j=jbegin,jup
        coef2h(j,k)=coef2(j,k)+coef2(j+1,k)
        coef4h(j,k)=coef4(j,k)+coef4(j+1,k)
        zjach(j,k)=xyj(j,k)+xyj(j+1,k)
      enddo
      enddo
c
      do k=klow,kup
      do j=jbegin,jup
      do i=1,4
        tmp(i,j,k)=(coef2h(j,k)*tmp(i,j,k)-
     &                   coef4h(j,k)*tmp2(i,j,k))*zjach(j,k)
      enddo
      enddo
      enddo

      do 19 k = kbegin,kend                                            
      do 19 j = jbegin,jup
c
        jp1=j+1
        do i=1,4
          zvec(i)=tmp(i,j,k)
        enddo
c        -need to compute  J^{-1} * T * |eigenvalue matrix| * T^{-1}
c
          rho=q(j,k,1)*xyj(j,k)+q(jp1,k,1)*xyj(jp1,k)
          rhoinv=1.d0/rho
          u  =q(j,k,2)/q(j,k,1) + q(jp1,k,2)/q(jp1,k,1)
          v  =q(j,k,3)/q(j,k,1) + q(jp1,k,3)/q(jp1,k,1)
          c  =sndsp(j,k)+sndsp(jp1,k)
          snr=1.d0/c
c
c         *******************************************************
c         -multiply by Tkinv
          bt=1.d0/sqrt(2.d0)
          ucap=(xy(j,k,1)+q(j,k,2) + xy(j,k,2)*q(j,k,3))/q(j,k,1)
          ucap=ucap + 
     &     (xy(jp1,k,1)*q(jp1,k,2) + xy(jp1,k,2)*q(jp1,k,3))/q(jp1,k,1)
c          ucap=rx*u + ry*v
          vtot2=0.5d0*(u**2+v**2)
          beta=bt*rhoinv*snr
          ryu=ry*u
          rxu=rx*u
          ryv=ry*v
          rxv=rx*v
          
          t1= gami*(u*zvec(2) + v*zvec(3) - zvec(4)-vtot2*zvec(1))
          
          vec(1) =zvec(1) + t1*snr**2
          vec(2) =((-ryu+rxv)*zvec(1)+ry*zvec(2)-rx*zvec(3))*rhoinv
          
          t1= t1*beta
          t2= ((rxu+ryv)*zvec(1) - rx*zvec(2)-ry*zvec(3))*bt*rhoinv
          
          vec(3) = -(t1+t2)
          vec(4) = -(t1-t2)
c         *******************************************************
c         
c         -multipy by absolute of eigenvalue matrix
          t1=abs(ucap)
          vec(1) = t1*vec(1)
          vec(2) = t1*vec(2)
          ccxh=ccx(j,k)+ccx(jp1,k)
          vec(3) = abs(ucap+ccxh)*vec(3)
          vec(4) = abs(ucap-ccxh)*vec(4)
c         
c         *******************************************************
c         -multiply by Tk
          alp=rho*bt*snr
          t1= alp*(vec(3)+vec(4))
          t2= rho*bt*(vec(3)-vec(4))
          t3= rho*vec(2)
          t4= alp*t1
          work(j,k,1)= vec(1) + t1
          work(j,k,2)= vec(1)*u + ry*t3 + u*t4 + rx*t2
          work(j,k,3)= vec(1)*v - rx*t3 + v*t4 + ry*t2
          work(j,k,4)= vtot2*vec(1) + (ryu - rxv)*t3 +
     &             (vtot2+ca**2*gm1i)*t1 + (rxu + ryv)*t2
 19   continue
c

c
c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
         s(j,k,n)=s(j,k,n) + (work(j,k,n) - work(j-1,k,n))*dtd
 200  continue
c
      return                                                            
      end                                                               
