c-------------------------------------------------------------------c
c---- subroutine to set the forcing function of the coarse level ---c
c-------------------------------------------------------------------c
c---- calling subroutine: mk_pk

      subroutine form_pk (jdim,kdim,pk,r)
c
      dimension r(jdim,kdim,4),pk(jdim,kdim,4)
c
      do 5 n = 1,4
      do 5 k = 1,kdim
      do 5 j = 1,jdim
        pk(j,k,n) = - r(j,k,n)
c        write(66,*) n,j,k,pk(j,k,n)
 5    continue
c
      return
      end

