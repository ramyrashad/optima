
#define ThreeD // 3D problem (cannot use numbers as 1st letter)

#include <cmath>
#include <vector>
#include "comprow_double.h"
#include "ilupre_double.h"
#include "cg.h"
#include "Node.h"
#include "grid.h"
#include "bicg.h"


using namespace std;

extern "C" {
  void movegrid3dmb_(char *filena, const int& namelen, double* x, double* y, 
		   double* z, const int* ibap, const int* lgptr, const int& nblk,
		   const int& nfoils, const int& nhalo);
}
// bap is the array of nodes on airfoil (has "wall" boundary condition)
// ibap is the array of number of nodes on each airfoil 
// nhalo is the halo (ghost) points


void movegrid3dmb_(char *filena, const int& namelen, double* x, double* y, 
		 double* z, const int* ibap, const int* lgptr, const int& nblk,
		 const int& nfoils, const int& nhalo)
{

  int j;
  char *filename = new char[namelen+1];
  for (j=0; j<namelen; j++)
    filename[j] = filena[j];
  filename[namelen] = '\0';

	// declare and initialize objects
  int tot_node (0);	// total number of nodes
  int *jmax = new int [nblk];
  int *kmax = new int [nblk];
  

  for (j=0; j<nfoils; j++)
    tot_node += ibap[j];   // total number of nodes on airfoil surface

	vector <Node> node_loc;
	vector <Node*> bnd_nodes(tot_node);
	int moving_bnd_nodes (0);	
	
	// CONJUGATE GRADIENT METHOD CONVERGENCE PARAMETERS
	double tolerance = 1.0e-5;
	int maxit = 500; 
	
	// SPRING STIFFNESS CALCULATION VARIABLES
	double phi = 1.;  
	
	read_grid_mb (filename, node_loc, tot_node, moving_bnd_nodes, 
		      bnd_nodes, ibap, nfoils, nblk, jmax, kmax);

  	deform_airfoil (filename, bnd_nodes, moving_bnd_nodes);
	
	// VARIABLES IN SOLVING MATRIX
	int num_inn_nodes(0), nonzeros (0);  
	// counts the number of nonzeros in the stiffness matrix	
	get_num_nonzeros (node_loc, tot_node, num_inn_nodes, nonzeros);

	double *stiff_val = new double [nonzeros];
	int *colind = new int [nonzeros];
	int *rowptr = new int [num_inn_nodes+1];
	
	// Solution, RHS vectors
	VECTOR_double deltay(num_inn_nodes, 0.0), deltax(num_inn_nodes,0.0),
		Fx(num_inn_nodes,0.0), Fy(num_inn_nodes,0.0);     
	
	//cout << "Forming stiffness matrix ... " << endl;
	calc_stiffness(node_loc, tot_node, stiff_val, phi, Fx, 
		       Fy, colind, rowptr);
	// ***** START SOLVING ... *****
	
	CompRow_Mat_double alpha(num_inn_nodes, num_inn_nodes, nonzeros, 
				 stiff_val, rowptr, colind);
	CompRow_ILUPreconditioner_double P(alpha);     // ILU preconditioner
	
	// Solve using the conjugate gradient method
	double tol = tolerance;
	int num_iter = 0;

	int maxitxy = maxit;

	num_iter = CG(alpha, deltax, Fx, P, maxitxy, tol);
	cout<< "CG flag:  " << num_iter << "     ";
	cout<< "Iterations performed x-dir:  " << maxitxy << "     ";
	cout<< "Tolerance achieved:  " << tol << endl;	


	tol = tolerance;
	maxitxy = maxit;	
	num_iter = CG(alpha, deltay, Fy, P, maxitxy, tol);  
	cout<< "CG flag:  " << num_iter << "     ";
	cout<< "Iterations performed y-dir:  " << maxitxy << "     ";
	cout<< "Tolerance achieved:  " << tol << endl;
	
	//cout << "Updating nodal locations ... " << endl;
	update (node_loc, tot_node, deltax, deltay);
	
	delete [] stiff_val;
	delete [] colind;
	delete [] rowptr;
	cout <<"before write gridMB" << endl;
	write_gridMB (node_loc, nblk, jmax, kmax, x, y, nhalo, lgptr);

	delete [] jmax;
	delete [] kmax;
	//delete filename;

	// clear all dynamically allocated memories associated with object Node
	for (j=0; j<moving_bnd_nodes; j++)
	  node_loc[j].clearmem();

	cout << "exitting movegridMB " << endl;
	return;
}













