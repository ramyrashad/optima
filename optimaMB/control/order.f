c----------------------------------------------------------------------
c     -- set initial node order --
c     -- reorder nodes if required  --
c     -- m. nemec, june 2001 --
c----------------------------------------------------------------------
cmt ~~~ ximesh is replaced by ibctype ~~~
      subroutine order (inord, ireord, x, y, indx, viscous)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

      double precision x(*), y(*)
      logical   viscous

c     -- local variables --
      integer jsize,na,lastblk,i,ib,is,inord,ireord,ii,j1,j2,j3,nnz
      integer jn,nw,nbi,nbj,nlines,iout,j,k,js,je,ks,ke,ip,ibn,ib2,is2
      integer iprec,nmax,ierr
      parameter (jsize=20)
      integer ipa(maxjk+1), jpa(maxjk*jsize), icol(9), indx(*)
      integer iwa(maxjk+1), jwa(maxjk*jsize), irenu(maxjk)
      double precision as(maxjk*jsize)

      logical tecout, pltout
      character*1 title
      character*2 munt 
      integer lines(100)
      
      na = 0
      lastblk = 0

c     -- ip is a pointer for icol to distinguish between A1 and A2
c     preconditioners --
      ip = 0

      if (inord.eq.1) then
c     write (*,*) '!! INORD = 1 !!'

c     -- natural ordering => k inside j loop --
         do ib = 1,nblks
            call blkord ( lastblk, jmax(ib), kmax(ib), indx(lgptr(ib)),
     &           jminbnd(ib), jmaxbnd(ib), 1, kminbnd(ib), kmaxbnd(ib),
     &           nhalo)
            na = na + jbmax(ib)*kbmax(ib)
         end do

      else if (inord.eq.2) then
c     write (*,*) '!! INORD = 2 !!'

c     -- reverse (upstream) natural ordering --
         do ib = nblks,1,-1
            call blkord ( lastblk, jmax(ib), kmax(ib), indx(lgptr(ib)),
     &           jmaxbnd(ib), jminbnd(ib), -1, kminbnd(ib), kmaxbnd(ib),
     &           nhalo)
            na = na + jbmax(ib)*kbmax(ib)
         end do

      else if (inord.eq.3) then
c     write (*,*) '!! INORD = 3 !!'

c     -- 3-block c-grid: double bandwidth --
         ib  = 1
         ibn = 3
         call blkordw( jminbnd(ib), jmaxbnd(ib), kminbnd(ib),
     &        kmaxbnd(ib), kminbnd(ibn), kmaxbnd(ibn), jmax(ib),
     &        kmax(ib), indx(lgptr(ib)), jmax(ibn), kmax(ibn),
     &        indx(lgptr(ibn)), lastblk)

         ib = 2
         call blkordc( jminbnd(ib), kminbnd(ib), 
     &        kmaxbnd(ib), jmax(ib), kmax(ib), indx(lgptr(ib)), lastblk)

         do ib = 1,nblks
            na = na + jbmax(ib)*kbmax(ib)
         end do

      else if (inord.eq.4) then

c     write (*,*) '!! INORD = 4 !!'

c     -- downstream across the 'wake-cuts' h-grid ordering --
         ib  = 1
         ibn = 4
         call blkord2 (jminbnd(ib), jmaxbnd(ib), 1, kminbnd(ib),
     &        kmaxbnd(ib), kminbnd(ibn), kmaxbnd(ibn), jmax(ib),
     &        kmax(ib), indx(lgptr(ib)), jmax(ibn), kmax(ibn),
     &        indx(lgptr(ibn)), lastblk)

         ib  = 2
         ibn = 5
         call blkord2 (jminbnd(ib), jmaxbnd(ib), 1, kminbnd(ib),
     &        kmaxbnd(ib), kminbnd(ibn), kmaxbnd(ibn), jmax(ib),
     &        kmax(ib), indx(lgptr(ib)), jmax(ibn), kmax(ibn),
     &        indx(lgptr(ibn)), lastblk)

         ib  = 3
         ibn = 6
         call blkord2 (jminbnd(ib), jmaxbnd(ib), 1, kminbnd(ib),
     &        kmaxbnd(ib), kminbnd(ibn), kmaxbnd(ibn), jmax(ib),
     &        kmax(ib), indx(lgptr(ib)), jmax(ibn), kmax(ibn),
     &        indx(lgptr(ibn)), lastblk)

         do ib = 1,nblks
            na = na + jbmax(ib)*kbmax(ib)
         end do

      else if (inord.eq.5) then

c     -- upstream across the 'wake-cuts' h-grid ordering --
c     write (*,*) '!! INORD = 5 !!'

         ib  = 3
         ibn = 6
         call blkord2 (jmaxbnd(ib), jminbnd(ib), -1, kminbnd(ib),
     &        kmaxbnd(ib), kminbnd(ibn), kmaxbnd(ibn), jmax(ib),
     &        kmax(ib), indx(lgptr(ib)), jmax(ibn), kmax(ibn),
     &        indx(lgptr(ibn)), lastblk)

         ib  = 2
         ibn = 5
         call blkord2 (jmaxbnd(ib), jminbnd(ib), -1, kminbnd(ib),
     &        kmaxbnd(ib), kminbnd(ibn), kmaxbnd(ibn), jmax(ib),
     &        kmax(ib), indx(lgptr(ib)), jmax(ibn), kmax(ibn),
     &        indx(lgptr(ibn)), lastblk)

         ib  = 1
         ibn = 4
         call blkord2 (jmaxbnd(ib), jminbnd(ib), -1, kminbnd(ib),
     &        kmaxbnd(ib), kminbnd(ibn), kmaxbnd(ibn), jmax(ib),
     &        kmax(ib), indx(lgptr(ib)), jmax(ibn), kmax(ibn),
     &        indx(lgptr(ibn)), lastblk)

         do ib = 1,nblks
            na = na + jbmax(ib)*kbmax(ib)
         end do
         
      else
         stop 'inord wrong value'
      end if

c     -- copy ordering to halos  --
c     -- use routine halcpi since indx is an array of integers --
      do ib=1,nblks
          do is=2,4,2
            ib2 = lblkpt(ib,is)
            is2 = lsidept(ib,is)
            if ( ib2.ne.0 ) then
               call halcpi( ib, nhalo, 1, is, is2, jmax(ib), kmax(ib),
     &              indx(lgptr(ib)), jbegin2(ib), jend2(ib), 
     &              kminbnd(ib), kmaxbnd(ib), jmax(ib2),
     &              kmax(ib2), indx(lgptr(ib2)), jbegin2(ib2),
     &              jend2(ib2), kminbnd(ib2), kmaxbnd(ib2))
            endif
         end do
      end do

c     -- tecplot output of initial node order --
      tecout = .false.
      if (tecout) then
         open ( unit=n_all, file='node.tec', status='unknown' )
         write (n_all,*) 'VARIABLES = "X","Y","N"'
         do ib = 1,nblks
            write (n_all,100) jbmax(ib),kbmax(ib)
            call writeord( n_all, x(lgptr(ib)), y(lgptr(ib)),
     &           jmax(ib), kmax(ib), jminbnd(ib), jmaxbnd(ib),
     &           kminbnd(ib), kmaxbnd(ib), indx(lgptr(ib)))
         end do
 100     format('ZONE I=',i3,', J=',i3,',F=BLOCK')
         close(n_all)
      end if

      iprec = 1

      if (ireord.gt.0) then

c     write (*,*) '!! IREORD = ',ireord
c     -----------------------------------------------------------------
c     -- first order jacobian --
c     -- setup sparse matricies ipa and jpa for preconditioner  --

         if (iprec.eq.1) then
c     -- first order jacobian stencil --
            ip = 5

            nmax = 1
            icol(1) = nmax
            do i = 2,4
               icol(i) = icol(i-1) + nmax
            end do
            icol(5) = 10

            do i = 1,maxjk*jsize
               jpa(i) = 0
            end do
            do i = 1,maxjk+1
               ipa(i) = 0
            end do

c     -- interior nodes -- 
            DO i = 1,nblks
               CALL pa_int(indx(lgptr(i)),jmax(i),kmax(i),nmax,
     &              jlow(i),jup(i),klow(i),kup(i),jpa,ipa,icol)
            ENDDO

c     -- block boundaries --
            do ib=1,nblks
               do is=1,4
                  call pa_bc( ib, is, nmax, indx, jpa, ipa, icol)
               end do
            end do

            if (.not. viscous) then
c     -- leading edge singular points for inviscid flow --
               do i = 1,nsing
                  call pa_sng( js1(i), ks1(i), js2(i), ks2(i), javg1(i),
     &                 kavg1(i), javg2(i), kavg2(i),
     &                 indx(lgptr(ibs1(i))), jmax(ibs1(i)),
     &                 kmax(ibs1(i)), indx(lgptr(ibs2(i))),
     &                 jmax(ibs2(i)), kmax(ibs2(i)), nmax, jpa, ipa,
     &                 icol)
               end do
            end if

c     -- copy points (leading and trailing edge points) --
            do i=1,ncopy
               call pa_cop( ijte1(i), ikte1(i), ijte2(i), ikte2(i),
     &              indx(lgptr(ibte1(i))), jmax(ibte1(i)), 
     &              kmax(ibte1(i)), indx(lgptr(ibte2(i))),
     &              jmax(ibte2(i)), kmax(ibte2(i)), nmax, jpa, ipa,
     &              icol)
            end do

         else if (iprec.eq.2) then

            ip = 9

c     -- initial ordering for jacobian matrix --
            nmax = 1
            icol(1) = nmax
            do i = 2,8
               icol(i) = icol(i-1) + nmax
            end do
            icol(9) = 15

            do i = 1,maxjk*jsize
               jpa(i) = 0
            end do
            do i = 1,maxjk+1
               ipa(i) = 0
            end do

c     -- interior nodes -- 
            DO i = 1,nblks
c     -- following logic is used to distinguish interior interfaces --
               IF (ibctype(i,2).EQ.0) THEN
                  js = jminbnd(i)
               ELSE
                  js = jminbnd(i) + 2
               ENDIF

               IF (ibctype(i,4).EQ.0) THEN
                  je = jmaxbnd(i)
               ELSE
                  je = jmaxbnd(i) - 2
               ENDIF

               CALL as_int( indx(lgptr(i)), jmax(i), kmax(i), nmax,
     &              js, je, kminbnd(i)+2, kmaxbnd(i)-2, jpa, ipa,
     &              icol)
            ENDDO

c     -- block boundaries --
            DO ib=1,nblks
               DO is=1,4
                  CALL as_bc( ib, is, nmax, indx, jpa, ipa, icol)

                  IF ( ibctype(ib,is).NE.0 ) THEN
c     -- first interior nodes dissipation stencils for jacobian --
                     IF (is.EQ.1) THEN

                        k  = kminbnd(ib)+1

                        IF (ibctype(ib,2).EQ.0) THEN
                           js = jminbnd(ib)
                        ELSE
                           js = jminbnd(ib)+2
                        ENDIF

                        IF (ibctype(ib,4).EQ.0) THEN
                           je = jmaxbnd(ib)
                        ELSE
                           je = jmaxbnd(ib)-2
                        ENDIF
                        
                        CALL as_fin1( k, js, je, nmax, indx(lgptr(ib)),
     &                       jmax(ib), kmax(ib), jpa, ipa, icol)

c     -- special dissipation stencils at interior block corners --
                        IF (ibctype(ib,2).NE.0) THEN
                           k = kminbnd(ib)+1
                           j = jminbnd(ib)+1
                           CALL as_ic1( j, k, nmax, indx(lgptr(ib)),
     &                          jmax(ib),kmax(ib), jpa, ipa, icol)
                        ENDIF

                        IF (ibctype(ib,4).NE.0) THEN
                           k = kminbnd(ib)+1
                           j = jmaxbnd(ib)-1
                           CALL as_ic2( j, k, nmax, indx(lgptr(ib)),
     &                          jmax(ib),kmax(ib), jpa, ipa, icol)
                        ENDIF

                     ELSEIF (is.EQ.2) THEN
                        j  = jminbnd(ib)+1
                        ks = kminbnd(ib)+2
                        ke = kmaxbnd(ib)-2
                        CALL as_fin2( j, ks, ke, nmax, indx(lgptr(ib)),
     &                       jmax(ib), kmax(ib), jpa, ipa, icol)

                     ELSEIF (is.EQ.3) THEN

                        k  = kmaxbnd(ib)-1

                        IF (ibctype(ib,2).EQ.0) THEN
                           js = jminbnd(ib)
                        ELSE
                           js = jminbnd(ib)+2
                        ENDIF

                        IF (ibctype(ib,4).EQ.0) THEN
                           je = jmaxbnd(ib)
                        ELSE
                           je = jmaxbnd(ib)-2
                        ENDIF

                        CALL as_fin3( k, js, je, nmax, indx(lgptr(ib)),
     &                       jmax(ib), kmax(ib), jpa, ipa, icol)

c     -- special dissipation stencils at interior block corners --
                        if (ibctype(ib,2).NE.0) THEN
                           k = kmaxbnd(ib)-1
                           j = jminbnd(ib)+1
                           CALL as_ic3( j, k, nmax, indx(lgptr(ib)),
     &                          jmax(ib),kmax(ib), jpa, ipa, icol)
                        ENDIF

                        IF (ibctype(ib,4).NE.0) THEN
                           k = kmaxbnd(ib)-1                  
                           j = jmaxbnd(ib)-1
                           CALL as_ic4( j, k, nmax, indx(lgptr(ib)),
     &                          jmax(ib),kmax(ib), jpa, ipa, icol)
                        ENDIF

                     ELSE
                        j  = jmaxbnd(ib)-1
                        ks = kminbnd(ib)+2
                        ke = kmaxbnd(ib)-2
                        CALL as_fin4( j, ks, ke, nmax, indx(lgptr(ib)),
     &                       jmax(ib), kmax(ib), jpa, ipa, icol)
                     ENDIF
                  ENDIF
               ENDDO
            ENDDO

            if (.not. viscous) then
c     -- leading edge singular points for inviscid flow --
               do i = 1,nsing
                  call as_sng( js1(i), ks1(i), js2(i), ks2(i), javg1(i),
     &                 kavg1(i), javg2(i), kavg2(i),
     &                 indx(lgptr(ibs1(i))), jmax(ibs1(i)),
     &                 kmax(ibs1(i)), indx(lgptr(ibs2(i))),
     &                 jmax(ibs2(i)), kmax(ibs2(i)), nmax, jpa, ipa,
     &                 icol)
               end do
            end if

c     -- copy points (leading and trailing edge points) --
            do i=1,ncopy
               call as_cop( ijte1(i), ikte1(i), ijte2(i), ikte2(i),
     &              indx(lgptr(ibte1(i))), jmax(ibte1(i)),
     &              kmax(ibte1(i)), indx(lgptr(ibte2(i))),
     &              jmax(ibte2(i)),kmax(ibte2(i)), nmax, jpa, ipa, icol)
            end do
         end if

c     -----------------------------------------------------------------
         pltout = .false.
         if (pltout) then
c     -- remove zeros --
            nbi = 1
            j2 = ipa(1)
            ipa(1) = 1
            do ii = 1,na
               nbj = (ii-1)*icol(ip)
               do j1 = nbj+1,nbj+j2
                  jpa(nbi) = jpa(j1)
                  nbi = nbi+1
               end do
               j2 = ipa(ii+1)
               ipa(ii+1) = nbi
            end do      

c     -- plot output --
            munt = 'in'
            title = ' '
            nlines = na/5 - 1
            lines(1) = 5
            do i=2,nlines
               lines(i) = 5 + lines(i-1)
            end do
            nlines = 0

c     -- order within rows --
            call csrcsc (na,0,1,as,jpa,ipa,as,jwa,iwa)
            call csrcsc (na,0,1,as,jwa,iwa,as,jpa,ipa)

            open( unit=n_all, file='jac_iord.ps', status='unknown')

            call pspltm( na, na, 0, jpa, ipa, title, 0, 7.0, munt,
     &           nlines,lines, n_all)

            close (n_all)
            stop 'order: plotted initial node order jacobian'
         end if

c     -- make graph symmetric for reordering --
         iout = n_out
         jn = 0
         do i = 1,na
            nbi = (i-1)*icol(ip)
            do 10 j1 = 1,ipa(i)
               j = jpa(nbi+j1)
               nbj = (j-1)*icol(ip)
               do j2 = 1,ipa(j)
                  j3 = jpa(nbj+j2)
                  if (j3.eq.i) goto 10
               end do
               ipa(j) = ipa(j)+1
               if ( ipa(j) .gt. icol(ip) ) then
                  write (*,*) 'SYMGRAPH: problem at row', j,ipa(j)
                  stop
               end if
               jpa(nbj+ipa(j)) = i
               jn = jn + 1
 10         continue
         end do

         write (iout,120) jn
 120     format (3x,'Added',i6,' blocks to make graph symmetric.')

c     -- remove zeros --
         nbi = 1
         j2 = ipa(1)
         ipa(1) = 1
         do ii = 1,na
            nbj = (ii-1)*icol(ip)
            do j1 = nbj+1,nbj+j2
               jpa(nbi) = jpa(j1)
               nbi = nbi+1
            end do
            j2 = ipa(ii+1)
            ipa(ii+1) = nbi
         end do

c     -- order within rows --
         call csrcsc (na,0,1,as,jpa,ipa,as,jwa,iwa)
         call csrcsc (na,0,1,as,jwa,iwa,as,jpa,ipa)

         pltout = .false.
         if (pltout) then
            munt = 'in'
            title = ' '
            nlines = na/4 - 1
            lines(1) = 4
            do i=2,nlines
               lines(i) = 4 + lines(i-1)
            end do
            nlines = 0

            open( unit=n_all, file='jac_sym.ps', status='unknown')
            call pspltm (na, na, 0, jpa, ipa, title, 0, 7.0, munt,
     &           nlines,lines, n_all)
            close(n_all)
         end if
         nnz = ipa(na+1)-1

c     -- irenu array will hold the reodering on return from cmk --
         do i = 1,maxjk+1
            iwa(i) = 0
         end do
         do i = 1,maxjk*jsize
            jwa(i) = 0
         end do
         nw = maxjk*jsize

c     -- reorder --
         call cmk (ireord, nnz, na, irenu, jpa, ipa, iwa, iout, nw, jwa,
     &        ierr)
         
         if (ierr.ne.0) then
            write (*,*) 'ORDER: ERROR IN ORDERING !! '
            write (*,*) 'error code = ',ierr
            write (*,*) 'program terminated'
            write (*,*) '----------------------------'
            stop
         endif

c     -- set new indx array --
         do ib = 1,nblks
            call newindx( jmax(ib), kmax(ib), indx(lgptr(ib)), irenu,
     &           jminbnd(ib), jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib))
         end do

c     -- copy ordering to halos  --
c     -- use routine halcpi since indx is an array of integers --
         do ib=1,nblks
            do is=2,4,2
               ib2 = lblkpt(ib,is)
               is2 = lsidept(ib,is)
               if ( ib2.ne.0 ) then
                  call halcpi( ib, nhalo, 1, is, is2, jmax(ib),
     &                 kmax(ib), indx(lgptr(ib)), jbegin2(ib),
     &                 jend2(ib), kminbnd(ib), kmaxbnd(ib),
     &                 jmax(ib2),kmax(ib2), indx(lgptr(ib2)),
     &                 jbegin2(ib2), jend2(ib2),
     &                 kminbnd(ib2), kmaxbnd(ib2))
               endif
            end do
         end do

c     -- tecplot output of final node order --
         tecout = .false.
         if (tecout) then
            open ( unit=n_all, file='node-f.tec', status='unknown' )
            write (n_all,*) 'VARIABLES = "X","Y","N"'
            do ib = 1,nblks
               write (n_all,100) jbmax(ib),kbmax(ib)
               call writeord( n_all, x(lgptr(ib)), y(lgptr(ib)),
     &              jmax(ib), kmax(ib), jminbnd(ib), jmaxbnd(ib),
     &              kminbnd(ib), kmaxbnd(ib), indx(lgptr(ib)))
            end do
            close(n_all)
         end if

c     -- plot reordered Jacobian matrix --
         pltout = .false.
         if (pltout) then
            do i = 1,maxjk*5
               jpa(i) = 0
            end do
            do i = 1,maxjk+1
               ipa(i) = 0
            end do

c     -----------------------------------------------------------------
c     -- first order jacobian --
c     -- setup sparse matricies ipa and jpa for preconditioner  --

c     -- interior nodes -- 
            DO i = 1,nblks
               CALL pa_int(indx(lgptr(i)),jmax(i),kmax(i),nmax,
     &              jlow(i),jup(i),klow(i),kup(i),jpa,ipa,icol)
            ENDDO      

c     -- block boundaries --
            do ib=1,nblks
               do is=1,4
                  call pa_bc( ib, is, nmax, indx, jpa, ipa, icol)
               end do
            end do

            if (.not. viscous) then
c     -- leading edge singular points for inviscid flow --
               do i = 1,nsing
                  call pa_sng( js1(i), ks1(i), js2(i), ks2(i), javg1(i),
     &                 kavg1(i), javg2(i), kavg2(i),
     &                 indx(lgptr(ibs1(i))), jmax(ibs1(i)),
     &                 kmax(ibs1(i)), indx(lgptr(ibs2(i))),
     &                 jmax(ibs2(i)), kmax(ibs2(i)), nmax, jpa, ipa,
     &                 icol)
               end do
            end if

c     -- copy points (leading and trailing edge points) --
            do i=1,ncopy
               call pa_cop( ijte1(i), ikte1(i), ijte2(i), ikte2(i),
     &              indx(lgptr(ibte1(i))), jmax(ibte1(i)), kmax(ibte1(i)
     &              ),indx(lgptr(ibte2(i))), jmax(ibte2(i)),
     &              kmax(ibte2(i)),nmax, jpa, ipa, icol)
            end do

c     -----------------------------------------------------------------
c     -- remove zeros --
            nbi = 1
            j2 = ipa(1)
            ipa(1) = 1
            do ii = 1,na
               nbj = (ii-1)*icol(ip)
               do j1 = nbj+1,nbj+j2
                  jpa(nbi) = jpa(j1)
                  nbi = nbi+1
               end do
               j2 = ipa(ii+1)
               ipa(ii+1) = nbi
            end do      

c     -- order within rows --
            call csrcsc (na,0,1,as,jpa,ipa,as,jwa,iwa)
            call csrcsc (na,0,1,as,jwa,iwa,as,jpa,ipa)

c     -- plot output --
            munt = 'in'
            title = ' '      
            nlines = 0

            open( unit=n_all, file='jac_ro.ps', status='unknown')
            call pspltm (na, na, 0, jpa, ipa, title, 0, 7.0, munt,
     &           nlines,lines, n_all)
            close(n_all)
         end if
      end if

      return
      end                       !order
