c-----------------------------------------------------------------------
c     Program name: getMaxMach
c     Written By: Howard Buckley, March 2009
c     
c     This subroutine scans the current flow solution 'q' and stores the
c     largest value of Mach number.
c
c-----------------------------------------------------------------------
      	
      subroutine getMaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)

#ifdef _MPI_VERSION
      use mpi
#endif


      !-- Declare variables

      implicit none
								
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
		
      integer 	
     &     j, k, jdim, kdim, ndim, jMaxMach, kMaxMach

      double precision 
     &     q(jdim,kdim,ndim), U, V, P, a, Mach, MaxMach,
     &     aMaxMach, uMaxMach, vMaxMach, pMaxMach

      !-- Calculate Mach number at each node. Store the largest
      !-- value found in 'MaxMach'

      MaxMach = 0.0
      jMaxMach = 0
      kMaxMach = 0

      do j = jmstart, jmend, jminc !-- Increment j by jminc

         do k = kmstart, kmend, kminc !-- Increment kminc by 2

            !-- Calculate velocities in x and y directions
            
            U = q(j,k,2)/q(j,k,1)
            V = q(j,k,3)/q(j,k,1)

            !-- Calculate pressure

            P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))

            !-- Calculate sound speed
            
            a = sqrt(1.4*P/q(j,k,1))

            !-- Calculate Mach number

            Mach = sqrt(U**2 + V**2)/a

            if (Mach.gt.MaxMach) then

               MaxMach = Mach
               jMaxMach = j
               kMaxMach = k
               aMaxMach = a
               uMaxMach = U
               vMaxMach = V
               pMaxMach = P

            end if

         end do

      end do

      return

      end   
