      subroutine mspectx(jdim,kdim,q,uu,ccx,xyj,xy,spect,sndsp)
c
#include "../include/arcom.inc"
c
      dimension xyj(jdim,kdim),xy(jdim,kdim,4)
      dimension q(jdim,kdim,4),sndsp(jdim,kdim)
      dimension spect(jdim,kdim,3),uu(jdim,kdim),ccx(jdim,kdim)
      double precision temp,rj
c                                                                       
c     compute dissipation scaling                                     
c     -spectral radius                                                 
c                                                                       
      if (mscal) then
        do 110 k=kbegin,kend                                         
        do 110 j=jbegin,jend                                         
          rj =1.d0/xyj(j,k)
          rhoinv=1.d0/q(j,k,1)
          u=q(j,k,2)*rhoinv
          v=q(j,k,3)*rhoinv
          zmach=sqrt(u**2+v**2)/sndsp(j,k)
          if (zmach.gt.1.d0) zmach=1.d0
          temp=(abs(uu(j,k))+ccx(j,k))*rj*zmach
          spect(j,k,1)=max(abs(uu(j,k))*rj,vlxi*temp)
          spect(j,k,2)=max(abs(uu(j,k)+ccx(j,k))*rj,vnxi*temp)
          spect(j,k,3)=max(abs(uu(j,k)-ccx(j,k))*rj,vnxi*temp)
 110    continue                                                     
      else
        do 100 k=kbegin,kend                                         
        do 100 j=jbegin,jend                                         
          rj =1.d0/xyj(j,k)
          temp=(abs(uu(j,k))+ccx(j,k))*rj
          spect(j,k,1)=max(abs(uu(j,k))*rj,vlxi*temp)
          spect(j,k,2)=max(abs(uu(j,k)+ccx(j,k))*rj,vnxi*temp)
          spect(j,k,3)=max(abs(uu(j,k)-ccx(j,k))*rj,vnxi*temp)
 100    continue                                                     
      endif
      
      return                                                  
      end                                                     


