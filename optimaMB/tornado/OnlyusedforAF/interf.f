c ********************************************************************
c **       Routine to apply interface conditions                    **
c **       the update is applied to blocks 1 and 2 using the        **
c **       results from both block 1 and block 2                    **
c **                                                                **
c **       Also, the halo points of block 1 are filled with         **
c **       data from block 2.                                       **
c **                                                                **
c **                                                                **
c **                                                                **
c ********************************************************************
c calling routines: integrat,xiexpl,etaexpl,xiimpl,etaimpl
c
c When copying data, the sign does not change.
c
c
      subroutine interf(nhalo,nside1,nside2,
     &  jdim1,kdim1,ndim,s1,
     &  jbegin1,jend1,kbegin1,kend1,
     &  jdim2,kdim2,s2,
     &  jbegin2,jend2,kbegin2,kend2)
c
      dimension s1(jdim1,kdim1,ndim)
      dimension s2(jdim2,kdim2,ndim)
c
c     -form indices for begining and end of blocks
c      -note jbegin and jend extend full scope of block including nhalo
c      jb1=jbegin1+nhalo
c      je1=jend1-nhalo
c      jb2=jbegin2+nhalo
c      je2=jend2-nhalo
      jb1=jbegin1
      je1=jend1
      jb2=jbegin2
      je2=jend2
c
c
c ********************************************************************
c **  block 1   :      side 1 or 3                                  **
c ********************************************************************
      if(nside1.eq.1.or.nside1.eq.3) then
        if (nside1.eq.1) then
          k1 = kbegin1
        else
          k1 = kend1
        endif
        if (nside2.eq.1.or.nside2.eq.3) then
c         -block 1 - side 1 or 3 connected to block 2 - side 1 or 3 
          if(nside2.eq.1) then
            k2 = kbegin2
          else
            k2 = kend2
          endif
          if(nside1.eq.nside2) then
            js = max(jb1,jdim2+1-je2)
            je = min(je1,jdim2+1-jb2)
            do 111 n=1,ndim
              do 110 jj=js,je
                j1 = jj
                j2 = jdim2 + 1 - jj
                stmp = 0.5*(s1(j1,k1,n)+s2(j2,k2,n))
                s1(j1,k1,n) = stmp
                s2(j2,k2,n) = stmp
 110          continue
 111        continue
          else
            js = max(jb1,jb2)
            je = min(je1,je2)
            do 116 n=1,ndim
              do 115 jj=js,je 
                stmp = 0.5*(s1(jj,k1,n)+s2(jj,k2,n))
                s1(jj,k1,n) = stmp
                s2(jj,k2,n) = stmp
 115          continue
 116        continue
          endif
        elseif(nside2.eq.2.or.nside2.eq.4)then
c         -block 1 - side 1 or 3 connected to block 2 - side 2 or 4 
          stop ' interf error'
        endif
c ********************************************************************
c **  block 1   :      side 2 or 4                                  **
c ********************************************************************
      elseif(nside1.eq.2.or.nside1.eq.4) then
        if(nside1.eq.2) then
          j1 = jb1
        else 
          j1 = je1
        endif 
        if(nside2.eq.2.or.nside2.eq.4)then
          if(nside2.eq.2) then
            j2 = jb2
          else
            j2 = je2
          endif
          if(nside1.eq.nside2) then
c            ks = max(kbegin1,kdim2+1-kend2)
c            ke = min(kend1,kdim2+1-kbegin2)
            ks = max(kbegin1+1,kdim2+1-kend2+1)
            ke = min(kend1-1,kdim2+1-kbegin2-1)
            do 211 n=1,ndim
              do 210 kk=ks,ke
                k1 = kk
                k2 = kdim2 + 1 - kk
                stmp = 0.5*(s1(j1,k1,n)+s2(j2,k2,n))
                s1(j1,k1,n) = stmp
                s2(j2,k2,n) = stmp
 210          continue
 211        continue
          else
c            ks = max(kbegin1,kbegin2)
c            ke = min(kend1,kend2)
            ks = max(kbegin1+1,kbegin2+1)
            ke = min(kend1-1,kend2-1)
            do 217 n=1,ndim
              do 215 kk=ks,ke
                stmp = 0.5*(s1(j1,kk,n)+s2(j2,kk,n))
                s1(j1,kk,n) = stmp
                s2(j2,kk,n) = stmp
 215          continue
 217        continue
          endif
        elseif(nside2.eq.1.or.nside2.eq.3)then
          stop ' interf error'
        endif
      else
        stop ' error in interf, nside1'
      endif
c
      return
      end
