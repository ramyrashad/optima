c
      do k=klow,kup
         do j=jlow,jup
            p2(j,k)=ux(j,k)+vy(j,k)
            p1(j,k)=2.0*(ux(j,k)**2+vy(j,k)**2)+(uy(j,k)+vx(j,k))**2
     $           -2./3.*(p2(j,k))**2
         enddo
      enddo
      if (first) then
      do k=klow,kup
      do j=jlow,jup
        smin(j,k)=10000.
      enddo
      enddo
c
      if (bnd(1)) then
         do k=klow,kup
            do j=jlow,jup
               j_point=j
               xpt1=x(j_point,kbegin)
               ypt1=y(j_point,kbegin)
               smin1 = sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
               smin(j,k)=min(smin1,smin(j,k))
               taw=sqrt(abs(uy(j_point,kbegin)-vx(j_point,kbegin))*re)
               yplus(j,k)=smin1*taw
            enddo
         enddo
      else
         do k=klow,kup
            do j=jlow,jup
c               smin1(j,k)=10000.0
               yplus(j,k)=10000.0
            enddo
         enddo
      endif
      if (bnd(2)) then
         do j=jlow,jup
            do k=klow,kup
               k_point=k
               xpt1=x(jbegin,k_point)
               ypt1=y(jbegin,k_point)
               smin2 = sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
               smin(j,k) = min(smin2,smin(j,k))
               taw=sqrt(abs(uy(jbegin,k_point)-vx(jbegin,k_point))*re)
               yplus(j,k)=min(yplus(j,k),smin2*taw)
            enddo
         enddo
      else
         do k=klow,kup
            do j=jlow,jup
c               smin2(j,k)=10000.0
               yplus(j,k)=min(yplus(j,k),10000.0)
            enddo
         enddo
      endif
      if (bnd(3)) then
         do k=klow,kup
            do j=jlow,jup
               j_point=j
               xpt1=x(j_point,kend)
               ypt1=y(j_point,kend)
               smin3 = sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
               smin(j,k) = min(smin3,smin(j,k))
               taw=sqrt(abs(uy(j_point,kend)-vx(j_point,kend))*re)
               yplus(j,k)=min(yplus(j,k),smin3*taw)
            enddo
         enddo
      else
         do k=klow,kup
            do j=jlow,jup
c               smin3(j,k)=10000.0
               yplus(j,k)=min(10000.0,yplus(j,k))
            enddo
         enddo
      endif
      if (bnd(4)) then
         do j=jlow,jup
            do k=klow,kup
               k_point=k
               xpt1=x(jend,k_point)
               ypt1=y(jend,k_point)
               smin4 = sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
               smin(j,k) = min(smin4,smin(j,k))
               taw=sqrt(abs(uy(jend,k_point)-vx(jend,k_point))*re)
               yplus(j,k)=min(yplus(j,k),smin4*taw)
            enddo
         enddo
      else
         do k=klow,kup
            do j=jlow,jup
c               smin4(j,k)=10000.0
               yplus(j,k)=min(10000.0,yplus(j,k))
            enddo
         enddo
      endif
      trip_present(iblk)=.false.
      do k=klow,kup
         do j=jlow,jup
c            temp_1=min(smin1(j,k),smin3(j,k))
c            temp_2=min(smin2(j,k),smin4(j,k))
c            smin(j,k)=min(temp_1,temp_2)
            if ((jtranup1.gt.0).or.(jtranlo1.gt.0)) then
c               if (jtranup1.ne.jend) then
                  trip_present(iblk)=.true.
                  smin1=10000.
                  smin3=10000.
                  if (bnd(1)) then
                    xpt1=x(j,kbegin)
                    ypt1=y(j,kbegin)
                    smin1 = sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
                  endif
                  if (bnd(3)) then
                    xpt1=x(j,kend)
                    ypt1=y(j,kend)
                    smin3 = sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
                  endif
                  if (smin1.le.smin3) then
                     kwall(j,k)=kbegin
                     jtranlo(j,k)=jtranlo1
                     jtranup(j,k)=jtranup1
                  endif
c               endif
            endif
c     
            if (jtranup3.gt.0) then
               if (jtranup3.ne.jend) then
                  trip_present(iblk)=.true.
                  smin1=10000.
                  smin3=10000.
                  if (bnd(1)) then
                    xpt1=x(j,kbegin)
                    ypt1=y(j,kbegin)
                    smin1 = sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
                  endif
                  if (bnd(3)) then
                    xpt1=x(j,kend)
                    ypt1=y(j,kend)
                    smin3 = sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
                  endif
                  if (smin3.le.smin1) then
                     kwall(j,k)=kend
                     jtranlo(j,k)=jtranlo3
                     jtranup(j,k)=jtranup3
                  elseif (jtranup1.le.0) then
                     kwall(j,k)=kend
                     jtranlo(j,k)=jtranlo3
                     jtranup(j,k)=jtranup3
                  endif
               endif
            endif
         enddo
      enddo
      multi_trans(iblk)=.false.
c      if (jtranlo1.gt.0) multi_trans(iblk)=.true.
      if (multi_trans(iblk).and.trip_present(iblk)) then
        do j=jlow,jup
          k=kwall(j,kbegin)
          chord=0.0
          fmin=100.0
          if ((jtranlo(j,k).gt.0).or.(jtranup(j,k).gt.0)) then
            if (jtranlo(j,k).le.0) jtranlo(j,k)=2
            if (jtranup(j,k).le.0) jtranup(j,k)=2
            do jj=jtranlo(j,k),jtranup(j,k)-1
              chord=chord+sqrt(
     &              (x(jj,k)-x(jj+1,k))**2 + (y(jj,k)-y(jj+1,k))**2)
            enddo
            chord_2=.5d0*chord
            do jj=jtranlo(j,k),jtranup(j,k)-1
              chord=chord+sqrt(
     &              (x(jj,k)-x(jj+1,k))**2 + (y(jj,k)-y(jj+1,k))**2)
              if (abs(chord-chord_2).lt.fmin) then
                fmin=abs(chord-chord_2)
                jmid=jj+1
              endif
            enddo
          endif
        enddo
      endif
      endif
