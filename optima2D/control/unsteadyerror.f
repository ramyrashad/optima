      subroutine unsteadyerror(jdim, kdim, ndim,ifirst,indx,icol, 
     &      iex,ia,ja,as,ipa,jpa, pa, iat, jat, ast, ipt, jpt, pat, q,
     &      qp, qold, press, xy, xyj, x, y, fmu, vort, turmu, work1,
     &      D_hat,exprhs,a_jk)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim, kdim, ndim,icol(9),na, ifirst,stage,jsta,jsto,ip
      integer indx(jdim,kdim),iex(jdim*kdim*ndim),stopstage,stopstage2
      integer count,count2,nout,skipend,i,j,k,div,countloop
      integer nk_iendstmp,nk_skiptmp

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4), xyjtmp
      double precision qp(jdim,kdim,ndim),qold(jdim,kdim,ndim)
      double precision press(jdim,kdim),sndsp(jdim,kdim),tmet(jdim,kdim)  
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)

c     -- sparse adjoint arrays --
      integer ia(jdim*kdim*ndim+2),iat(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1) 

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100)

csi
      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim)
      double precision a_jk(6,6)
csi

      double precision psip(jdim,kdim,ndim),psi(jdim,kdim,ndim)
      double precision psiold(jdim,kdim,ndim),fracdt,dtasave(tdim)
      double precision,dimension(:,:,:,:),allocatable :: psies

      double precision cdmean,clmean,win(tdim),fobjc,fobjf
      double precision ns(sdim,2),J1(tdim),J2(tdim),totadjcorr
      double precision deltat,deltattmp,scalef,deltatbig,partadjcorr
      complex*16,dimension(:,:,:,:),allocatable :: dpod

      double precision uu(jdim,kdim),vv(jdim,kdim),po(tdim,sdim)
      double precision ccx(jdim,kdim),ccy(jdim,kdim),adjcorrarr(tdim) 
      double precision coef4(jdim,kdim), coef2(jdim,kdim),timeadjcorr
      double precision precon(jdim,kdim,6), spect(jdim,kdim,3)
      double precision ds(jdim,kdim), s(jdim,kdim,ndim)
      double precision bcf(jdim,kdim,4), gam(jdim,kdim,16)

      real*4 etime, t, tp, tarray(2)
      external etime
      logical doloop,adapted

      double precision dtiseq(20),dtmins(20),dtow2(20)
      integer jmxi(20),kmxi(20),jskipi(20),iends(20)
      integer isequal,iseqlev

      common/mesup/ dtiseq,dtmins,dtow2,isequal,iseqlev,
     &      jmxi,kmxi,jskipi,iends

cmpr  Allocate some of the big arrays dynamically
      allocate(psies(jdim,kdim,ndim,6))
      allocate(dpod(Nobs,tdim,sdim,3))

c     Divide time interval into div segments for refinement
      div=2
      
c     Set some variables
      errest=.true.
      doloop=.true.
      countloop=1
      gamma=1.4d0
      gami=0.4d0    
      scalef=1.d0
      t=0.0

c     Calculate the number of outputs
      deltat=noutevery*dtiseq(1)
      deltatbig=nouteverybig*dtbig
      skipend=nk_skip/nouteverybig     
      nout=nk_iends/noutevery+skipend

c     set up initial time step distribution
      do i=1,tdim
         dta(i)=0.d0
      end do
      do i=1,nk_skip
         dta(i)=dtbig
      end do
      do i=nk_skip+1,nk_skip+nk_iends
         dta(i)=dt2
      end do

      
      do while(doloop)

      totadjcorr=0.d0
      partadjcorr=0.d0

c     Solve unsteady flow problem on coarse grid

      write(*,*)  
      write(*,*)
      write(*,58) countloop
      write(*,*) '---------------------------------------------'

      if (erradap) then
         write(*,*) 'Time step distribution:'
         write(*,*) (dta(i),i=1,nout)
      end if

  
      ifirst = 1                ! indicates first call to flow solve if =1
      restart = .true.

      call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &     ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat,
     &     q, qp, qold, press, xy, xyj, x, y, fmu, vort, turmu,work1, 
     &     D_hat,exprhs,a_jk)
      

      if (obj_func .eq. 0) then
c     --  Calculate mean drag  --
            
         call calcobjair0(fobjc,jdim,kdim,nout,skipend,x,y,xy,xyj)

         write(*,*) 'Mean drag value on coarse grid:',fobjc 

      else if (obj_func .eq. 5) then
c     --  Calculate (mean drag / mean lift)  --
            
         call calcobjair5(fobjc,jdim,kdim,nout,skipend,x,y,xy,xyj,
     &        cdmean,clmean,scalef)

         write(*,*) 'Mean drag/mean lift value on coarse grid:',fobjc 

      end if ! obj_func 


      tp=t
      t=etime(tarray)
      write (*,60) t-tp

      

c     Calculate unsteady adjoint on coarse grid

      write(*,*)
      write(*,59) countloop
      write(*,*) '--------------------------------------------'

      na = jmax*kdim*ndim

      if (jesdirk.eq.4 .or. jesdirk.eq.3) then
c     Evolve psi for ESDIRK4:
c     -- initialize psies and psites to zero for all stages

         if(jesdirk.eq.4)then
            jstagemx=6
         elseif(jesdirk.eq.3)then
            jstagemx=4
         endif 

         do i=1,ndim
            do k = 1,kdim
               do j = 1,jdim
                  do stage=1,jstagemx               
                     psies(j,k,i,stage)=0.d0
                  end do
               end do
            end do
         end do
         
         do count = nout,1,-1

            if (count.ne.0) then
               stopstage=2
            else
               stopstage=jstagemx
            end if


            do jstage = jstagemx,stopstage,-1

               call unsteadyadjoint(jdim,kdim,ndim,indx,icol,psi,psies, 
     &         psiold,iex,ia,ja,as,ipa,jpa,pa,iat,jat,ast,ipt,jpt,pat,q,
     &         press,sndsp,xy,xyj,x,y,uu,vv,ccx,ccy,coef2,coef4,tmet,
     &         precon,bcf,fmu,vort,turmu,work1,a_jk,count,nout,skipend,
     &         po,dpod,win,ns,J1,J2,deltat,deltatbig,deltattmp,scalef,
     &         jsta,jsto,na,cdmean,clmean,.true.)

            end do !loop over stages

         end do !loop over time steps

      else if (jesdirk.eq.1) then                 
c     Evolve psi for BDF2:  
c     -- set psi^(N+1)=0 and psi^(N+2)=0

         do i=1,ndim
            do k = 1,kdim
               do j = 1,jdim                  
                  psi(j,k,i)=0.d0
                  psiold(j,k,i)=0.d0               
               end do
            end do
         end do

         jstagemx=1
         jstage=1
      
         do count = nout, 1, -1

            call unsteadyadjoint(jdim,kdim,ndim,indx,icol,psi,psies, 
     &      psiold,iex,ia,ja,as,ipa,jpa,pa,iat,jat,ast,ipt,jpt,pat,q,
     &      press,sndsp,xy,xyj,x,y,uu,vv,ccx,ccy,coef2,coef4,tmet,
     &      precon,bcf,fmu,vort,turmu,work1,a_jk,count,nout,skipend,
     &      po,dpod,win,ns,J1,J2,deltat,deltatbig,deltattmp,scalef,jsta,
     &      jsto,na,cdmean,clmean,.true.)

         end do !loop over time steps

      end if !jesdirk?



      tp=t
      t=etime(tarray)
      write (*,60) t-tp




      if (SIMIN_RES.gt.1.d-14) then

         write(*,*)
         write(*,*) 'Calculate adjoint correction due to partial'
     &        // ' convergence'
         write(*,*) '-------------------------------------------'
     &        // '------------'

c     Calculate adjoint error correction: psi^T * rhs
         call calcadjcorr(jdim,kdim,ndim,q,qp,qold,psip,press, 
     &        sndsp,tmet,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,
     &        a_jk,uu,vv,ccx,ccy,coef4,coef2,precon,spect,ds,s,bcf,gam,
     &        nout,1,adjcorrarr,partadjcorr)

         write(*,*)'Partial convergence adjoint correction:',partadjcorr
         write(*,*)'Corrected objective function:', fobjc+partadjcorr

         tp=t
         t=etime(tarray)
         write (*,60) t-tp

      end if !SIMIN_RES.lt.1.d-14



      write(*,*)
      write(*,*) 'Interpolate to finer grid'
      write(*,*) '-------------------------'
      

      call interp2fine(jdim,kdim,ndim,q,qp,qold,psi,psip,psiold,
     &     xyj,turmu,div,nout)

      tp=t
      t=etime(tarray)
      write (*,60) t-tp







      write(*,*)
      write(*,*) 'Calculate adjoint correction on interpolated'
     &        // ' finer grid'
      write(*,*) '--------------------------------------------'
     &        // '-----------'

c     set up equally spaced time step distribution on finer grid

      do i=1,nout
         dtasave(i)=dta(i)
      end do

      do i=nk_skip+nk_iends,nk_skip+1,-1
         fracdt=dta(i)/dble(div)
         do count2=0,div-1
            dta(div*i-count2)=fracdt
         end do
      end do
      do i=nk_skip,1,-1
         fracdt=dta(i)/dble(div)
         do count2=0,div-1
            dta(div*i-count2)=fracdt
         end do
      end do

      nk_skip=div*nk_skip
      nk_iends=div*nk_iends

      deltat=deltat/dble(div)
      deltatbig=deltatbig/dble(div)
      skipend=nk_skip/nouteverybig     
      nout=nk_iends/noutevery+skipend


c     Evaluate objective function on interpolated finer grid

      if (obj_func .eq. 0) then
c     --  Calculate mean drag  --
            
         call calcobjair0(fobjf,jdim,kdim,nout,skipend,x,y,xy,xyj)

         write(*,*) 'Mean drag value on interpolated finer grid:',fobjf 

      else if (obj_func .eq. 5) then
c     --  Calculate (mean drag / mean lift)  --
            
         call calcobjair5(fobjf,jdim,kdim,nout,skipend,x,y,xy,xyj,
     &        cdmean,clmean,scalef)

         write(*,*) 'Mean drag/mean lift value on interpolated'
     &            //' finer grid:',fobjf 

      end if ! obj_func 


c     Calculate adjoint error correction on interpolated finer grid: psi^T * rhs

      call calcadjcorr(jdim,kdim,ndim,q,qp,qold,psip,press, 
     &     sndsp,tmet,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,
     &     a_jk,uu,vv,ccx,ccy,coef4,coef2,precon,spect,ds,s,bcf,gam,
     &     nout,div,adjcorrarr,totadjcorr)

      timeadjcorr=totadjcorr-partadjcorr
      if (SIMIN_RES.gt.1.d-14) then         
         write(*,*) 'Time adjoint correction:',timeadjcorr         
      end if
      write(*,*) 'Total adjoint correction:',totadjcorr

      tp=t
      t=etime(tarray)
      write (*,60) t-tp


      write(*,*)
      write(*,*) 'Total corrected objective' 
     & // ' function:',fobjf+totadjcorr


      if (erradap) then      
c     adapt time step sizes

         nk_skip=nk_skip/div
         nk_iends=nk_iends/div

         deltat=deltat*dble(div)
         deltatbig=deltatbig*dble(div)
         skipend=nk_skip/nouteverybig     
         nout=nk_iends/noutevery+skipend
         
         nk_skiptmp= nk_skip
         nk_iendstmp=nk_iends

         adapted=.false.
         count=1
         do i=1,nk_skiptmp
            if (dabs(adjcorrarr(i)-partadjcorr) .gt. 
     &           dabs(timeadjcorr)/dble(nout)) then
               adapted=.true.
               fracdt=dtasave(i)/dble(div)
               do count2=1,div
                  dta(count)=fracdt
                  count=count+1
               end do
            else
               dta(count)=dtasave(i)
               count=count+1
            end if
         end do
         nk_skip=count-1
         skipend=nk_skip/nouteverybig           
         do i=nk_skiptmp+1,nk_skiptmp+nk_iendstmp
            if (dabs(adjcorrarr(i)-partadjcorr) .gt. 
     &           dabs(timeadjcorr)/dble(nout)) then
               adapted=.true.
               fracdt=dtasave(i)/dble(div)
               do count2=1,div
                  dta(count)=fracdt
                  count=count+1
               end do
            else
               dta(count)=dtasave(i)
               count=count+1
            end if
         end do
         nk_iends=count-1-nk_skip
         nout=nk_iends/noutevery+skipend

      end if  !erradap

        doloop=(erradap.and.dabs(timeadjcorr).gt.erradaptol.and.adapted)
        countloop=countloop+1

      end do !do while(doloop)

      if (erradap.and.dabs(timeadjcorr).lt.erradaptol) then
         write(*,*)
         write(*,*) 'Desired error tolerance for adaptation achieved.'
      else if (erradap.and..not.adapted) then
         write(*,*)
         write(*,*) 'No time step required adaptation.'
      end if


      if (.not.erradap) then

         write(*,*)
         write(*,*) 'Actually solve unsteady flow problem on finer grid'
         write(*,*) '--------------------------------------------------'

         SIMIN_RES=1.d-14
         ifirst = 1             ! indicates first call to flow solve if =1
         restart = .true.

         call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &        ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat,
     &        q, qp, qold, press, xy, xyj, x, y, fmu, vort, turmu,work1, 
     &        D_hat,exprhs,a_jk)
      

         if (obj_func .eq. 0) then
c        --  Calculate mean drag  --
            
            call calcobjair0(fobjf,jdim,kdim,nout,skipend,x,y,xy,xyj)
            
            write(*,*) 'Mean drag value on finer grid:',fobjf 

         else if (obj_func .eq. 5) then
c        --  Calculate (mean drag / mean lift)  --
            
            call calcobjair5(fobjf,jdim,kdim,nout,skipend,x,y,xy,xyj,
     &           cdmean,clmean,scalef)
            
            write(*,*) 'Mean drag/mean lift value on finer grid:',fobjf 

         end if                 ! obj_func

         tp=t
         t=etime(tarray)
         write (*,60) t-tp

      end if !.not. erradap
      
      
cmpr  Deallocate memory
      write(*,*)
      deallocate(psies,dpod)

 58   format (' Solve unsteady flow problem on coarse grid',i3)
 59   format (' Calculate unsteady adjoint on coarse grid',i3)
 60   format (' Run-time:',f7.2,'s')

      return
      end !unsteadyerror






      subroutine calcadjcorr(jdim,kdim,ndim,q,qp,qold,psip,press, 
     &     sndsp,tmet,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,
     &     a_jk,uu,vv,ccx,ccy,coef4,coef2,precon,spect,ds,s,bcf,gam,
     &     nout,div,adjcorr,totadjcorr)

      implicit none

#include "../include/arcom.inc"

      integer jdim, kdim, ndim,na,ip
      integer count,count2,nout,i,j,k,j_point,div

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4), xyjtmp
      double precision qp(jdim,kdim,ndim),qold(jdim,kdim,ndim)
      double precision press(jdim,kdim),sndsp(jdim,kdim),tmet(jdim,kdim)  
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100)

csi
      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim)
      double precision a_jk(6,6),xpt1,ypt1
csi

      double precision psip(jdim,kdim,ndim),adjcorr(tdim),totadjcorr

      double precision uu(jdim,kdim),vv(jdim,kdim)
      double precision ccx(jdim,kdim),ccy(jdim,kdim) 
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), spect(jdim,kdim,3)
      double precision ds(jdim,kdim), s(jdim,kdim,ndim)
      double precision bcf(jdim,kdim,4), gam(jdim,kdim,16)


      count2 = 1
      adjcorr(count2)=0.d0


c     -- compute generalized distance function --
      if (viscous .and. turbulnt .and. itmodel.eq.2) then
         do k = kbegin,kend
            do j = jbegin,jend
               j_point=j
               if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
               xpt1 = x(j_point,kbegin)
               ypt1 = y(j_point,kbegin)
               smin(j,k)=dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
            end do
         end do
      end if


      if (jesdirk.eq.4 .or. jesdirk.eq.3) then
c     Error correction for ESDIRK4: 

         write(*,*) 'Error correction for ESDIRK not implemented yet'
         stop

      else if (jesdirk.eq.1) then
c     Error correction for BDF2:


c     -- read flow field from restart files
         ip = 4 
         call ioall(ip,0,jdim,kdim,q,qold,press,sndsp,turmu,
     &        fmu,vort,xy,xyj,x,y )

c     -- put in jacobian Q->Qhat
         do k=1,kmax
            do j=1,jmax
               xyjtmp=xyj(j,k)
               q(j,k,5) = turre(j,k)/xyjtmp
               qold(j,k,5) = turold(j,k)/xyjtmp
               do i = 1,4
                  q(j,k,i) = q(j,k,i)/xyjtmp
                  qold(j,k,i) = qold(j,k,i)/xyjtmp
               end do
            end do
         end do


         do count = 1,nout

c        -- read flow field at this time step --
            call iomarkus(3,count,jdim,kdim,qp,xyj,turmu)

c        -- put in jacobian Q->Qhat and ensure tmet is zero--
            do k = 1,kdim
               do j = 1,jdim            
                  tmet(j,k) = 0.d0
                  xyjtmp=xyj(j,k)
                  qp(j,k,5) = turre(j,k)/xyjtmp
                  do i = 1,4
                     qp(j,k,i) = qp(j,k,i)/xyjtmp
                  end do
               end do
            end do 

c        -- calculate pressure and sound speed --
            call calcps (jdim, kdim, qp, press, sndsp, precon, xy, xyj)

c        -- compute laminar viscosity based on sutherland's law --
            if (viscous) call fmun(jdim, kdim, qp, press, fmu, sndsp)

            dt2=dta(count)

c        -- evaluate residual on fine grid with interpolated quantities
            call get_rhs(jdim, kdim, ndim, qp, q, qold, xy, xyj, 
     &        x, y, precon, coef2, coef4, uu, vv, ccx, ccy, 
     &        ds, press, tmet, spect, gam, turmu, fmu, vort, 
     &        sndsp, s, work1, D_hat, exprhs,a_jk,.false.)

c        -- read adjoint field at this time step --
            call iomarkus(3,5000+count,jdim,kdim,psip,xyj,turmu)
            do k = 1,kdim       
               do j = 1,jdim
                  psip(j,k,5)=turre(j,k)
               end do
            end do 

c        -- store psi^T * rhs in adjcorr(count2), note that rhs is negative            
            do i=1,ndim   
               do k = 1,kdim       
                  do j = 1,jdim                   
                    adjcorr(count2)=adjcorr(count2)-psip(j,k,i)*s(j,k,i)
                  end do
               end do
            end do

c        -- add adjcorr(count2) to total error and increase count2 by one
            if (mod(count,div).eq.0) then
               totadjcorr=totadjcorr+adjcorr(count2)
               count2 = count2+1
               adjcorr(count2)=0.d0
            end if

c        -- copy q to qold and qp to q
            do i=1,ndim   
               do k = 1,kdim       
                  do j = 1,jdim 
                     qold(j,k,i)=q(j,k,i)
                     q(j,k,i)=qp(j,k,i)                                                 
                  end do
               end do
            end do

         end do   !loop over time steps      

      end if   !jesdirk?

      return
      end !calcadjcorr





      subroutine interp2fine(jdim,kdim,ndim,q,qp,qold,psi,psip,psiold,
     &     xyj,turmu,div,nout)

      implicit none

#include "../include/arcom.inc"

      integer jdim, kdim, ndim
      integer count,count2,nout,i,j,k,div

      double precision q(jdim,kdim,ndim),xyjout(jdim,kdim)
      double precision qp(jdim,kdim,ndim),qold(jdim,kdim,ndim)
      double precision psip(jdim,kdim,ndim),psi(jdim,kdim,ndim)
      double precision psiold(jdim,kdim,ndim),fac,fac1,xyj(jdim,kdim)
      double precision turmu(jdim,kdim)


      if (jesdirk.eq.4 .or. jesdirk.eq.3) then
c     Interpolate for ESDIRK4: 

         write(*,*) 'Interpolation for ESDIRK not implemented yet'
         stop

      else if (jesdirk.eq.1) then
c     Interpolate for BDF2:

c     -- read flow and adjoint field at last time step --
         call iomarkus(3,nout,jdim,kdim,qp,xyj,turmu)
         do k = 1,kdim       
            do j = 1,jdim
               qp(j,k,5)=turre(j,k)
            end do
         end do           
         call iomarkus(3,5000+nout,jdim,kdim,psip,xyj,turmu)
         do k = 1,kdim       
            do j = 1,jdim
               psip(j,k,5)=turre(j,k)
            end do
         end do 

         do count = nout,1,-1

c        -- write flow and adjoint field at this time step*div --
            numiter=iend+div*count
            do k = 1,kdim       
               do j = 1,jdim
                  turre(j,k)=qp(j,k,5)
                  xyjout(j,k)=1.0d0
               end do
            end do 
            call iomarkus(1,div*count,jdim,kdim,qp,xyjout,turmu)
            do k = 1,kdim       
               do j = 1,jdim
                  turre(j,k)=psip(j,k,5)
                  xyjout(j,k)=1.0d0
               end do
            end do 
            call iomarkus(1,5000+div*count,jdim,kdim,psip,xyjout,turmu)


c        -- read flow and adjoint field at previous time step --            
            call iomarkus(3,count-1,jdim,kdim,qold,xyj,turmu)
            do k = 1,kdim       
               do j = 1,jdim
                  qold(j,k,5)=turre(j,k)
               end do
            end do 
            if (count.gt.1) then
cmpr        there is no psi value before the first time step
               call iomarkus(3,5000+count-1,jdim,kdim,psiold,xyj,turmu)
               do k = 1,kdim       
                  do j = 1,jdim
                     psiold(j,k,5)=turre(j,k)
                  end do
               end do
            end if

            do count2=div-1,1,-1

               fac=dble(count2)/dble(div)
               fac1=1.d0-fac

c           -- linear interpolation
               if (count.gt.1) then
                  
                  do i=1,ndim   
                     do k = 1,kdim       
                        do j = 1,jdim 
                           q(j,k,i)=fac*qp(j,k,i)+fac1*qold(j,k,i)
                           psi(j,k,i)=fac*psip(j,k,i)+fac1*psiold(j,k,i)
                        end do
                     end do
                  end do
               else 
c           -- linear extrapolation for psi for coarse first time step
cmpr           use always two closest psi values even if they are interpolated  
                  do i=1,ndim   
                     do k = 1,kdim       
                        do j = 1,jdim 
                           q(j,k,i)=fac*qp(j,k,i)+fac1*qold(j,k,i)
                           psi(j,k,i)=2.d0*psip(j,k,i)-psi(j,k,i)
                        end do
                     end do
                  end do
               end if
               
c        -- write interpolated flow and adjoint field at this time step*div-count2 --
               numiter=iend+div*count-count2
               do k = 1,kdim       
                  do j = 1,jdim
                     turre(j,k)=q(j,k,5)
                     xyjout(j,k)=1.0d0
                  end do
               end do 
               call iomarkus(1,div*count-count2,jdim,kdim,q,
     &              xyjout,turmu)
               do k = 1,kdim       
                  do j = 1,jdim
                     turre(j,k)=psi(j,k,5)
                     xyjout(j,k)=1.0d0
                  end do
               end do
               call iomarkus(1,5000+div*count-count2,jdim,kdim,psi,
     &              xyjout,turmu)

               if (count.eq.1 .and. count2.ne.1) then
cmpr           switch psip and psi which means that the linear extrapolation 
cmpr           formula is still valid
                  do i=1,ndim   
                     do k = 1,kdim       
                        do j = 1,jdim 
                           psiold(j,k,i)=psip(j,k,i)
                           psip(j,k,i)=psi(j,k,i)
                           psi(j,k,i)=psiold(j,k,i)
                        end do
                     end do
                  end do
               end if

            end do !count2


            if (count .gt. 1) then
c           -- copy qold to qp and psiold to psip
               do i=1,ndim   
                  do k = 1,kdim       
                     do j = 1,jdim 
                        qp(j,k,i)=qold(j,k,i)
                        psip(j,k,i)=psiold(j,k,i)
                     end do
                  end do
               end do
            end if


         end do  !loop over time steps      

      end if   !jesdirk?


      return
      end !interp2fine
