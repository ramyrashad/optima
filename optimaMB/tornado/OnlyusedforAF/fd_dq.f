c     ------------------------------------------------------------------
c     -- perturb q for finite difference jacobian approximation --
c     -- m. nemec, sept. 2001 --
c     ------------------------------------------------------------------
      subroutine fd_dq( jo, ko, no, jmax, kmax, nmax, q, turre, xyj,
     &     tmpv, stepsize)

      dimension q(jmax,kmax,4), turre(jmax,kmax), xyj(jmax,kmax)

      fds = 1.e-7

      if (no.le.4) then
         tmpv = q(jo,ko,no)
         stepsize = fds*q(jo,ko,no)
         if ( abs(stepsize) .lt. 1.e-9 ) then
            stepsize = 1.e-9*sign(1.0,stepsize)
         end if

         q(jo,ko,no) = q(jo,ko,no) + stepsize
         stepsize = q(jo,ko,no) - tmpv
      else
         tmpv = turre(jo,ko)
         stepsize = fds*turre(jo,ko)/xyj(jo,ko)
         if ( abs(stepsize) .lt. 1.e-9 ) then
            stepsize = 1.e-9*sign(1.0,stepsize)
         end if

         tn = turre(jo,ko)/xyj(jo,ko) + stepsize
         stepsize = tn - turre(jo,ko)/xyj(jo,ko)
         turre(jo,ko) = tn*xyj(jo,ko)

      end if
      return 
      end                       !fd_dq
