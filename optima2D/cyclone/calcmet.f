      subroutine calcmet(jdim,kdim,xy,xyj)                      
c                                                                       
#include "../include/arcom.inc"
c                                                                       
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
c                                                                       
c     form jacobian
c                  
                                                   
      do 30 k=kbegin,kend                                               
      do 30 j=jbegin,jend                                               
c        calculating the inverse of the Jacobaian J^{-1}.
         dinv = ( xy(j,k,4) * xy(j,k,1) - xy(j,k,2) * xy(j,k,3) )       
         if(dinv .le. 0.0) then
            write (scr_unit,*) 'negative jacobian( ',j,k,' )= ', dinv
            badjac = .true.
            return
         end if
c        storing the Jacobian "J" in xyj.
         xyj(j,k) = 1./dinv                                             
c         write(72,72) j,k,xyj(j,k)
 30   continue                                                          
 72   format(2I4,e25.12)
c                                                                       
c  calculate metrics                                                    
c                                                                       
      do 32 k=kbegin,kend                                               
      do 32 j=jbegin,jend                                               
         dinv = xyj(j,k)                                                
c                                                                       
c xix = xy1, xiy=xy2,etax=xy3, etay = xy4                               
c                                                                       
         xy(j,k,1) =   xy(j,k,1)*dinv                                   
         xy(j,k,2) = - xy(j,k,2)*dinv                                   
         xy(j,k,3) = - xy(j,k,3)*dinv                                   
         xy(j,k,4) =   xy(j,k,4)*dinv                                   
c         write(89,*) j,k,dinv
32    continue                                                          
c      do 5 n=1,4
c      do 5 k=1,kend
c      do 5 j=1,jend
c         write(80,71) j,k,xy(j,k,n)
c 5    continue
c 71   format(2I4,e25.12)
c                                                                       
c         k=1
c         print *,'j=jtail2'
c         print *,xy(jtail1,k,1),xy(jtail2,k,1)
c         print *,xy(jtail1,k,2),xy(jtail2,k,2)
c         print *,xy(jtail1,k,3),xy(jtail2,k,3)
c         print *,xy(jtail1,k,4),xy(jtail2,k,4)
c         print *,xyj(jtail1,k),xyj(jtail2,k)
c         print *,'j=jtail2+1'
c         print *,xy(jtail1-1,k,1),xy(jtail2+1,k,1)
c         print *,xy(jtail1-1,k,2),xy(jtail2+1,k,2)
c         print *,xy(jtail1-1,k,3),xy(jtail2+1,k,3)
c         print *,xy(jtail1-1,k,4),xy(jtail2+1,k,4)
c         print *,xyj(jtail1-1,k),xyj(jtail2+1,k)
c         print *,'j=jtail2+2'
c         print *,xy(jtail1-2,k,1),xy(jtail2+2,k,1)
c         print *,xy(jtail1-2,k,2),xy(jtail2+2,k,2)
c         print *,xy(jtail1-2,k,3),xy(jtail2+2,k,3)
c         print *,xy(jtail1-2,k,4),xy(jtail2+2,k,4)
c         print *,xyj(jtail1-2,k),xyj(jtail2+2,k)
c   for `o' mesh and overlapped 1 and jmaxold define jmaxold point      
c                                                                       
      if(periodic .and. jmax.ne.jmaxold)then                            
c                                                                       
c  special logic for overlap o grid case                                
c  set jmaxold point from j = 1 point                                   
c                                                                       
      do 910 k = 1,kmax                                                 
      xy(jmaxold,k,1) = xy(1,k,1)                                       
      xy(jmaxold,k,2) = xy(1,k,2)                                       
      xy(jmaxold,k,3) = xy(1,k,3)                                       
      xy(jmaxold,k,4) = xy(1,k,4)                                       
      xyj(jmaxold,k) = xyj(1,k)                                         
910    continue                                                         
c                                                                       
      endif                                                             
c                                                                       
      return                                                            
      end                                                               
