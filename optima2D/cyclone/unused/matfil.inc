      jp2=j+2
      jp1=j+1
      jm1=j-1
      jm2=j-2
      rho = q(j,k,1)*xyj(j,k)                                        
      uu = xy(j,k,1)*u(j,k) + xy(j,k,2)*v(j,k) + xit(j,k)
      uxi= (u(jm2,k) - 8.d0*(u(jm1,k) - u(jp1,k)) - u(jp2,k))*tp
      vxi= (v(jm2,k) - 8.d0*(v(jm1,k) - v(jp1,k)) - v(jp2,k))*tp
c     
      ff = rho*uu*( xy(j,k,3)*uxi+xy(j,k,4)*vxi)                     
      c1 = xy(j,k,3)*xy(j,k,1) + xy(j,k,2)*xy(j,k,4)                 
      c2 = xy(j,k,3)**2 + xy(j,k,4)**2                               
c
c     calculate pressure values
      p2 =gami*(q(j,2,4)-
     &                 .5d0*(q(j,2,2)**2+q(j,2,3)**2)/q(j,2,1))*xyj(j,2)
c      p3 =gami*(q(j,3,4)-
c     &                 .5d0*(q(j,3,2)**2+q(j,3,3)**2)/q(j,3,1))*xyj(j,3) 
c      p4 =gami*(q(j,4,4)-
c     &                 .5d0*(q(j,4,2)**2+q(j,4,3)**2)/q(j,4,1))*xyj(j,4) 
c      p5 =gami*(q(j,5,4)-
c     &                 .5d0*(q(j,5,2)**2+q(j,5,3)**2)/q(j,5,1))*xyj(j,5) 
c
c     For (dp/dn = 0) use following two lines
c     Use 1st order difference of dp/dn (pressure).
      f(j)=-ff-c2*p2
      c(j)=-c2
c
c     Use 2nd order difference of dp/dn (pressure).
c      f(j)=-ff-.5d0*c2*(4.d0*p2 - p3)
c      c(j)=-.5d0*3.d0*c2
c
c     Use 3rd order difference of dp/dn (pressure).
c      f(j)=-ff-c2*tp2*(18.d0*p2 - 9.d0*p3 + 2.d0*p4)
c      c(j)=-11.d0*tp2*c2
c
c     Use 4th order difference of dp/dn (pressure).
c      f(j)=-ff-c2*tp*(48.d0*p2 - 36.d0*p3 + 16*p4 - 3.d0*p5)
c      c(j)=-25.d0*tp*c2
c
      a(j)=c1*tp                                                  
      b(j)=-8.d0*tp*c1                                        
      d(j)=-b(j)
      e(j)=-a(j)
