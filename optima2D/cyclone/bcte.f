      subroutine bcte(jdim,kdim,q,xyj)             
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),xyj(jdim,kdim)
c                                                                       
c
c     -average trailing edge values
c                                                                       
      gm1i=1.d0/gami
c                                                                       
      j = jtail1
      i = jtail2
      kk=1
      rrj = 1.d0/xyj(j,kk)                                          
      rri = 1.d0/xyj(i,kk)
c     
      rhou=q(i,kk,1)*xyj(i,kk)
      rhol=q(j,kk,1)*xyj(j,kk)
      qsav = 0.5d0*(rhou+rhol)
      q(j,kk,1) = qsav*rrj                                      
      q(i,kk,1) = qsav*rri                                      
c     
      uu=q(i,kk,2)*xyj(i,kk)
      ul=q(j,kk,2)*xyj(j,kk)
      qsav = 0.5d0*(uu+ul)
      q(j,kk,2) = qsav*rrj                                    
      q(i,kk,2) = qsav*rri
c     
      vu=q(i,kk,3)*xyj(i,kk)
      vl=q(j,kk,3)*xyj(j,kk)
      qsav = 0.5d0*(vu+vl)
      q(j,kk,3) = qsav*rrj                                  
      q(i,kk,3) = qsav*rri                                  
c     
c     average pressure at trailing edge              
c     
c      psav = 0.5d0*(press(j,kk)*xyj(j,kk) + press(i,kk)*xyj(i,kk))
      pup = gami*(q(i,kk,4)-0.5*
     &         (q(i,kk,2)**2+q(i,kk,3)**2)/q(i,kk,1))*xyj(i,kk)
      plo = gami*(q(j,kk,4)-0.5*
     &         (q(j,kk,2)**2+q(j,kk,3)**2)/q(j,kk,1))*xyj(j,kk)
      psav = 0.5d0*(plo+pup)
c     
      q(j,kk,4) = psav*gm1i*rrj +                                 
     *      0.5d0*(q(j,kk,2)**2+q(j,kk,3)**2)/q(j,kk,1)          
      q(i,kk,4) = psav*gm1i*rri +                                 
     *      0.5d0*(q(i,kk,2)**2+q(i,kk,3)**2)/q(i,kk,1)          
c     
      return                                                           
      end                                                              
      
