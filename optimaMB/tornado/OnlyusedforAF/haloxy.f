c ********************************************************************
c **       Routine to copy data from the interior of block2         **
c **       to the halo points of block 1                            **
c **       For arrays dimensioned (jdim,kdim,4)                     **
c **       Includes rescaling by jacobian                           **
c ********************************************************************
c
c calling routine: setup
c
      subroutine haloxy(nside1,nside2,jkhalo,
     &  jdim1,kdim1,x1,y1,
     &  jbegin1,jend1,kbegin1,kend1,jlow1,jup1,klow1,kup1,
     &  jdim2,kdim2,x2,y2,
     &  jbegin2,jend2,kbegin2,kend2,jlow2,jup2,klow2,kup2)
c
      dimension x1(jdim1,kdim1),y1(jdim1,kdim1)
      dimension x2(jdim2,kdim2),y2(jdim2,kdim2)
c
c
      do 99 ii = 0,jkhalo-1
c ********************************************************************
c **  block 1   :      side 1 or 3                                  **
c ********************************************************************
      if(nside1.eq.1.or.nside1.eq.3) then
        if (nside1.eq.1) then
          k1h = kbegin1 + ii
        else
          k1h = kend1 - ii
        endif
c *** block 1 - side 1 or 3 connected to block 2 - side 1 or 3 
        if (nside2.eq.1.or.nside2.eq.3) then
          if(nside2.eq.1) then
            k2i = klow2 + 1 - ii
          else
            k2i = kup2 - 1 + ii
          endif
          if(nside1.eq.nside2) then
            j1 = max(jbegin1,jdim2+1-jend2)
            j2 = min(jend1,jdim2+1-jbegin2)
            do 110 jj=j1,j2
              j1h = jj
              j2i = jdim2 + 1 - jj
              x1(j1h,k1h) = x2(j2i,k2i)
              y1(j1h,k1h) = y2(j2i,k2i)
  110       continue
          else
            j1 = max(jbegin1,jbegin2)
            j2 = min(jend1,jend2)
            do 115 jj=j1,j2 
              j1h = jj
              j2i = jj
              x1(j1h,k1h) = x2(j2i,k2i)
              y1(j1h,k1h) = y2(j2i,k2i)
  115       continue
          endif
c *** block 1 - side 1 or 3 connected to block 2 - side 2 or 4 
        elseif(nside2.eq.2.or.nside2.eq.4)then
          if(nside2.eq.2) then 
            j2i = jlow2 + 1 - ii
          else
            j2i = jup2 - 1 + ii
          endif
          if ( (nside1.eq.1.and.nside2.eq.2)
     &     .or.(nside1.eq.3.and.nside2.eq.4) )  then
            j1 = max(jbegin1,kbegin2)
            j2 = min(jend1,kend2)
            do 120 jj=j1,j2
              j1h = jj
              k2i = jj
              x1(j1h,k1h) = x2(j2i,k2i)
              y1(j1h,k1h) = y2(j2i,k2i)
  120       continue
          else
            j1 = max(jbegin1,kdim2+1-kend2)
            j2 = min(jend1,kdim2+1-kbegin2)
            do 125 jj=j1,j2
              j1h = jj
              k2i = kdim2 + 1 - jj 
              x1(j1h,k1h) = x2(j2i,k2i)
              y1(j1h,k1h) = y2(j2i,k2i)
  125       continue
          endif
        endif
c ********************************************************************
c **  block 1   :      side 2 or 4                                  **
c ********************************************************************
      elseif(nside1.eq.2.or.nside1.eq.4) then
        if(nside1.eq.2) then
          j1h = jbegin1 + ii
        else 
          j1h = jend1 - ii
        endif 
        if(nside2.eq.2.or.nside2.eq.4)then
          if(nside2.eq.2) then
            j2i = jlow2 + 1 - ii
          else
            j2i = jup2 - 1 + ii
          endif
          if(nside1.eq.nside2) then
            k1 = max(kbegin1,kdim2+1-kend2)
            k2 = min(kend1,kdim2+1-kbegin2)
            do 210 kk=k1,k2
              k1h = kk
              k2i = kdim2 + 1 - kk
              x1(j1h,k1h) = x2(j2i,k2i)
              y1(j1h,k1h) = y2(j2i,k2i)
  210       continue
          else
            k1 = max(kbegin1,kbegin2)
            k2 = min(kend1,kend2)
            do 215 kk=k1,k2
              k1h = kk
              k2i = kk
              x1(j1h,k1h) = x2(j2i,k2i)
              y1(j1h,k1h) = y2(j2i,k2i)
  215       continue
          endif
        elseif(nside2.eq.1.or.nside2.eq.3)then
          if(nside2.eq.1) then 
            k2i = klow2 + 1 - ii
          else
            k2i = kup2 - 1 + ii
          endif
          if ( (nside1.eq.2.and.nside2.eq.1)
     &     .or.(nside1.eq.4.and.nside2.eq.3) )  then
            k1 = max(kbegin1,jbegin2)
            k2 = min(kend1,jend2)
            do 220 kk=k1,k2
              k1h = kk
              j2i = kk
              x1(j1h,k1h) = x2(j2i,k2i)
              y1(j1h,k1h) = y2(j2i,k2i)
  220       continue
          else
            k1 = max(kbegin1,jdim2+1-jend2)
            k2 = min(kend1,jdim2+1-jbegin2)
            do 225 kk=k1,k2
              k1h = kk
              j2i = jdim2 + 1 - kk
              x1(j1h,k1h) = x2(j2i,k2i)
              y1(j1h,k1h) = y2(j2i,k2i)
  225       continue
          endif
        else
          stop ' haloxy error'
        endif
      else
        stop ' error in haloxy, nside1'
      endif
   99 continue

      return
      end
