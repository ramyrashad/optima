c This has been modified so that all variables are explicitly declared,
c so the "implicit none" statement may be used in subroutines that
c include this file.  (Chad Oldfield)

#include "../include/indx.inc"
#include "../include/units.inc"

      logical cmesh, orderxy, periodic, wake, circul, sharp, cusp
      logical bcairf
c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision dswall, sobmax, tinf, wtrat, circb, chord
      integer jtail1, jtail2, jsls, jsle
      common/geom/                                                      
     &      periodic,    cmesh,       orderxy,     wake,      jtail1,
     &      jtail2,      bcairf,      circul,      dswall,    sobmax,
     &      sharp,       cusp,        tinf,        wtrat,     circb,
     &      chord, jsls, jsle

      logical writeresid, sv_grdseq, flbud, timing, writeturb, jacarea
      logical grdseq_rest,ibc,sknfrc,frozen,outtime,sbbc,errest,erradap
      logical gnuplot_res, postProcessOnly, flowAnalysisOnly

c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision dtrate, pi, strtit, smu,smuim,dis2x,dis2y,gami,
     |    dis4x, dis4y, dt, phidt, thetadt, fsmach, alpha, dt2, gamma,
     |    dtmin, dtow, foso, scaledq, omegaa, omegaf, omega,dta(tdim),
     |    simin_res, dtbig, dtsmall, sbeta, sdelta, xobs, yobs, dtold,
     |    sumdt,erradaptol
      integer meth, ispec, jacdt, idmodel, iord, integ, nnit, nq, ncp,
     |    ifrz_what, noutevery, nouteverybig,klineconst,jobs,kobs,
     |    Nobs,outFWH
      common/parm/            
     &      dtrate,      pi,          strtit,      smu,     smuim,
     &      dis2x,       dis2y,       dis4x,       dis4y,   dt,
     &      phidt,       thetadt,     fsmach,      alpha,   dt2,
     &      gamma,       gami,        dtmin,       dtow,    foso,    
     &      scaledq,     omegaa,      omegaf,      omega,   simin_res,   
     &      dtbig,       dtsmall,     sbeta,       sdelta,  dtold,
     &      xobs,        yobs,        erradaptol,  sumdt,        
     &      meth,        ispec,       jacdt,       idmodel, dta,
     &      timing,      iord,        ibc,         integ,   nnit,
     &      nq,          ncp,         ifrz_what,   sv_grdseq,
     &      grdseq_rest, flbud,       writeturb,   jacarea, erradap,
     &      writeresid,  sknfrc,      frozen,      noutevery, 
     &      outtime,     nouteverybig,klineconst,  sbbc,    errest,
     &	    jobs,        kobs,        Nobs,        outFWH, 
     &      gnuplot_res, postProcessOnly, flowAnalysisOnly

      double precision res_spl
      integer maxres(2)     
      logical restart, store, badjac
c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision resid, resid0, residmx, totime1
      integer numiter, istart, iend, iread
      common/swtc/               
     &     resid,       residmx,      totime1,    res_spl,    maxres,
     &     numiter,     istart,       iend,       iread,      restart,
     &     store,       badjac,	      resid0
     
      logical turbulnt, viscous, visxi, viseta, viscross, originalSA
c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision re, pchord, retinf, ramptran, re_in,
     |   transup, translo, transup_initGuess, translo_initGuess 
      integer ivis, jtranup, jtranlo, jramplo1, jrampup1, jramplo2,
     |    jrampup2
      common/visc/                                                      
     &      viscous,    turbulnt,    re,           ivis,       visxi,
     &      viseta,     viscross,    pchord,       transup,    translo,
     &      transup_initGuess, translo_initGuess,
     &      jtranup,    jtranlo,     jramplo1,     jrampup1,   jramplo2,
     &      jrampup2,   retinf,      ramptran,     re_in,    originalSA
     
c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision clt, cdt, cmt, cli, cdi, cmi, clv, cdv, cmv
      common/forces/                                                    
     &      clt,       cdt,         cmt,         cli,        cdi,
     &      cmi,       clv,         cdv,         cmv

c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision rhoinf, uinf, vinf, einf, pinf
      common/infinty/                                                   
     &      rhoinf,    uinf,        vinf,        einf,      pinf
     
      logical clalpha, clalpha2, clopt
c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision relaxcl, clinput, cltol
      integer isw, iclfreq, iclstrt
      common/clalp/                                                    
     &      relaxcl,   clinput,     cltol,       isw,
     &      iclfreq,   iclstrt,     clalpha,     clalpha2,   clopt   
     
      integer prec
      double precision prxi,prphi
      common/precon/prxi,prphi,prec

      logical unsted
c     Explicitly declare double precision & integers (Chad Oldfield)
      integer jsubit, jsubmx, iplu0, iplu1, ismodel, ispmod,jesdirk,
     &      jstage,jstagemx
      common/sub/
     &      jsubit,jsubmx,iplu0,iplu1,ismodel,ispmod,unsted,jesdirk,
     &      jstage,jstagemx
     
      double precision turre(maxj,maxk),turre1(maxj,maxk)
      double precision turold(maxj,maxk),smin(maxj,maxk)
      double precision d_trip(maxj,maxk),res_spl2
c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision jLEsetup, delta_xlo, delta_xup
      integer jmid, ispbc, itmodel
      common/turb/
     &      turre,turre1,turold,
     &      smin,d_trip,res_spl2,
     &      jLEsetup,delta_xlo,delta_xup,jmid,ispbc,itmodel

c     -- matrix dissipation common block --
      double precision vnxi,vlxi,vneta,vleta
      common/matrix/vnxi,vlxi,vneta,vleta

c     -- cusp dissipation common block --
      integer limiter
      double precision epz,epv
      common/limiters/epz,epv,limiter

c     -- flux budgets --
cRR:       double precision budget(mbudj,mbudk,3)
      double precision budget(maxj,maxk,3)
      common/flxbud/budget
     
      double precision min_res, af_minr      
c     Explicitly declare double precision & integers (Chad Oldfield)
      double precision spmin_res
      common/conver/min_res, af_minr, spmin_res
     
      double precision visceig
      logical sngvalte, mscal, zeroturre, zerote
      common/newvar/visceig,sngvalte,mscal,zeroturre,zerote

      character*40 output_file_prefix, grid_file_prefix
      character*40 output_file_prefix_old,output_file_prefix0
      character*40 output_file_prefix0_old, dirname 
      character*40 filena, restart_file_prefix, restart_file_prefix0
      character*40 output_file_prefix_backup
      common/filen/
     &      output_file_prefix, grid_file_prefix,
     &      restart_file_prefix, filena, restart_file_prefix0,
     &      output_file_prefix_old, output_file_prefix0,
     &      output_file_prefix0_old, dirname,
     &      output_file_prefix_backup

      integer nk_its, nk_ilu, nk_lfil, nk_iends, nk_skip
      integer nk_pfrz, nk_imgmr, nk_itgmr, ipar(16)
      double precision nk_pdc, fpar(16)
      logical prec_mat, jac_mat
      common/nkdata/ nk_pdc, fpar, nk_its, nk_ilu, nk_lfil, nk_imgmr,
     &      nk_pfrz, nk_itgmr, ipar, prec_mat, jac_mat, nk_iends,
     &      nk_skip

c     -- transition related INPUT parameters --
      logical
     &     transitionalFlow,    !< flag TRUE if transitional flow (false = fully turbulent)
     &     freeTransition,      !< flag TRUE if predicting transition (false = fixed trans pts)
     &     TPGuessOveride,      !< flag TRUE if you want to set your own initial guesses
     &     bypassFinalTPRun,    !< flag TRUE if you want to skip the final flow solve with final TPs
     &     earlyTP,             !< flag TRUE if only a partial AF convergence req'd for TP
     &     applyTPShift,        !< flag TRUE if shifting predicted TP downstream by a % of Re_theta
     &     write_blprop,        !< flag TRUE to output BL properties to tecplot file
     &     write_blvel,         !< flag TRUE to output BL velocity profiles to tecplot file
     &     write_tp             !< flag TRUE to output transition prediction info to file
      integer
     &     transPredictMethod,  !< selects transition prediction methodology
     &     blEdgeMethod,        !< selects method for finding the BL edge/thickness
     &     maxIterTP,           !< max number of iterations for transition prediction
     &     jConvTolTP           !< convergence tolerance based on number of streamwise nodes
      double precision
     &     Tu,                  !< free-stream turbulence intensity (ex: for Tu = 0.1% enter 0.001)
     &     Ncrit,               !< critical N-factor for e^N method transition prediction (MATTC)
     &     underRelaxTP,        !< under-relaxation factor for TP movement
     &     earlyResidTP,        !< AF residual for early transition prediction (see earlyTP)
     &     percentTPShift,      !< percentage of Re_theta by which predicted points are shifted downstream
     &     aftLimitTP,          !< furthest aft chord position allowed for transition points 
     &     xConvTolTP           !< convergence tolerance based on x/c distance
      

c     -- transition related GLOBAL variables --
      logical
     &     finalTP,             !< flag TRUE if all transition points are converged
     &     foundBLEdge(maxj),   !< flag TRUE if BL edge was successfully found
     &     foundCR,             !< flag TRUE if critical point successfully found
     &     foundTR,             !< flag TRUE if transition point successfully found
     &     convergedTP(2),      !< flag TRUE if transition prediction routine has converged
     &     convergedTPOld(2),   !< flag TRUE if transition prediction routine has converged
     &     checkForFullyLam(2),
     &     checkForVanishingBubble(2),
     &     flagTPsAsZero
      integer
     &     k_deltaBL(maxj),     !< lower off-wall index of the two nodes which bound the BL edge
     &     jblStart, jblEnd,    !< start/end j index for transition prediction
     &     kblStart, kblEnd,    !< start/end k index for calculation of BL properties
     &     jblinc,              !< increment for j-index based on surface (top/bottom)
     &     jblLE,               !< j-index of leading edge node  
     &     jblSP,               !< j-index of stagnation point node
     &     surfaceBL,           !< surface index: 1=upper, 2=lower
     &     jTransForced(2),     !< j-index of fixed transition points
     &     jTransPredict(2),    !< j-indices of predicted transition points
     &     errorTrackerTP,      !< tracks the error flag (overwritten for each surface)
     &     errorFlagTP(2),      !< stores the error flag (1=upper, 2=lower)
     &     iterTP               !< max number of iterations for transition prediction
      double precision
     &     xTransForced(2),     !< position of fixed transition points
     &     deltaBL(maxj),       !< boundary layer thickness (off-wall height)
     &     velBLEdge(maxj),     !< magnitude of velocity at boundary layer edge
     &     Re_theta_laminar(maxj), !< container to store the laminar Re_theta for each surface
     &     xTransPredictNoShift(2) !< x/c locations of precited transition point with no shift

c     -- transition related common block declaration --
      common/transition/                                                      
     &     transitionalFlow, freeTransition, TPGuessOveride,
     &     bypassFinalTPRun, write_blprop, write_blvel, write_tp,
     &     transPredictMethod, blEdgeMethod,
     &     earlyTP, applyTPShift, finalTP,
     &     foundBLEdge, foundCR, foundTR,
     &     convergedTP, xConvTolTP, jConvTolTP,
     &     convergedTPOld, checkForFullyLam, checkForVanishingBubble,
     &     flagTPsAsZero,
     &     k_deltaBL, jblStart, jblEnd,
     &     kblStart, kblEnd, jblinc, jblLE, jblSP, surfaceBL,
     &     jTransForced, jTransPredict,
     &     errorTrackerTP, errorFlagTP, iterTP, maxIterTP,
     &     Tu,Ncrit,earlyResidTP,percentTPShift,underRelaxTP,
     &     aftLimitTP, xTransForced, deltaBL, velBLEdge,
     &     Re_theta_laminar, xTransPredictNoShift
