c        **********************
c        Advective Terms in Eta
c        **********************
c     
c        -first-order upwinding/downwinding        
         do k=klow,kup
         do j=jlow,jup
            sgnu = sign(1.,vv(j,k))
            app  = .5*(1.+sgnu)
            apm  = .5*(1.-sgnu)
            fy(j,k) = fy(j,k) - 
     &           vv(j,k)*( app*(turre(j,k)-turre(j,k-1))
     &           +apm*(turre(j,k+1)-turre(j,k)) )
            by(j,k)   = by(j,k)   - vv(j,k)*app
            cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
            dy(j,k)   = dy(j,k)   + vv(j,k)*apm
         enddo
         enddo
c
