        subroutine xpentap2(JDIM,KDIM,a,b,c,d,e,u1,u2,u3,u4,f,          
     *                      jl,ju,kl,ku)                                
c       PARAMETER (maxj=497)                                            
#include "../include/parm.inc"
        double precision ld,ld1,ld2,ldi,ldd,lu                                      
        dimension a(JDIM,KDIM),b(JDIM,KDIM),c(JDIM,KDIM),d(JDIM,KDIM)   
        dimension e(JDIM,KDIM),f(JDIM,KDIM,2),u4(JDIM,KDIM)             
        dimension u1(JDIM,KDIM),u2(JDIM,KDIM),u3(JDIM,KDIM)             
c                                                                       
        COMMON/WORKSP/lda(MAXJ),ldb(MAXJ),lus(MAXJ),                    
     *         lds(MAXJ),WORK(MAXJ,26)
        REAL LDA,LDB,LUS,LDS                                            
c                                                                       
c        ! start forward generation process and sweep                   
c                                                                       
        i = jl                                                          
        do 10 k = kl,ku                                                 
        ld = c(i,k)                                                     
        ldi = 1./ld                                                     
        u1(i,k) = d(i,k)*ldi                                            
        u2(i,k) = e(i,k)*ldi                                            
        u3(i,k) = a(i,k)*ldi                                            
        u4(i,k) = b(i,k)*ldi                                            
        f(i,k,1) = f(i,k,1)*ldi                                         
        f(i,k,2) = f(i,k,2)*ldi                                         
10      continue                                                        
c                                                                       
        i = jl+1                                                        
        do 20 k = kl,ku                                                 
        ld1 = b(i,k)                                                    
        ld = c(i,k) - ld1*u1(i-1,k)                                     
        ldi = 1./ld                                                     
        u1(i,k) = (d(i,k) - ld1*u2(i-1,k))*ldi                          
        u2(i,k) = e(i,k)*ldi                                            
        u3(i,k) = -ld1*u3(i-1,k)*ldi                                    
        u4(i,k) = (a(i,k) -ld1*u4(i-1,k))*ldi                           
        f(i,k,1) = (f(i,k,1) - ld1*f(i-1,k,1))*ldi                      
        f(i,k,2) = (f(i,k,2) - ld1*f(i-1,k,2))*ldi                      
20      continue                                                        
c                                                                       
          do 1 i = jl+2,ju-4                                            
                do 11 k = kl,ku                                         
                ld2 = a(i,k)                                            
                ld1 = b(i,k) - ld2*u1(i-2,k)                            
                ld = c(i,k) - (ld2*u2(i-2,k) + ld1*u1(i-1,k))           
                ldi = 1./ld                                             
                u1(i,k) = (d(i,k) - ld1*u2(i-1,k))*ldi                  
                u2(i,k) = e(i,k)*ldi                                    
                u3(i,k) = (-ld2*u3(i-2,k) - ld1*u3(i-1,k))*ldi          
                u4(i,k) = (-ld2*u4(i-2,k) - ld1*u4(i-1,k))*ldi          
                f(i,k,1) = (f(i,k,1) - ld2*f(i-2,k,1) -                 
     *                     ld1*f(i-1,k,1))*ldi                          
                f(i,k,2) = (f(i,k,2) - ld2*f(i-2,k,2) -                 
     *                      ld1*f(i-1,k,2))*ldi                         
11              continue                                                
1           continue                                                    
c                                                                       
                i = ju-3                                                
                do 12 k = kl,ku                                         
                ld2 = a(i,k)                                            
                ld1 = b(i,k) - ld2*u1(i-2,k)                            
                ld = c(i,k) - (ld2*u2(i-2,k) + ld1*u1(i-1,k))           
                ldi = 1./ld                                             
                u1(i,k) = (d(i,k) - ld1*u2(i-1,k))*ldi                  
                u3(i,k) = (e(i,k) - ld2*u3(i-2,k) - ld1*u3(i-1,k))*ldi  
                u4(i,k) = (-ld2*u4(i-2,k) - ld1*u4(i-1,k))*ldi          
                f(i,k,1) = (f(i,k,1) - ld2*f(i-2,k,1) -                 
     *                     ld1*f(i-1,k,1))*ldi                          
                f(i,k,2) = (f(i,k,2) - ld2*f(i-2,k,2) -                 
     *                     ld1*f(i-1,k,2))*ldi                          
12              continue                                                
c                                                                       
        i = ju-2                                                        
        do 22 k = kl,ku                                                 
        ld2 = a(i,k)                                                    
        ld1 = b(i,k) - ld2*u1(i-2,k)                                    
        ld = c(i,k) - (ld2*u2(i-2,k) + ld1*u1(i-1,k))                   
        ldi = 1./ld                                                     
        u3(i,k) = (d(i,k) - ld2*u3(i-2,k) - ld1*u3(i-1,k))*ldi          
        u4(i,k) = (e(i,k) - ld2*u4(i-2,k) - ld1*u4(i-1,k))*ldi          
        f(i,k,1) = (f(i,k,1) - ld2*f(i-2,k,1) - ld1*f(i-1,k,1))*ldi     
        f(i,k,2) = (f(i,k,2) - ld2*f(i-2,k,2) - ld1*f(i-1,k,2))*ldi     
22      continue                                                        
c                                                                       
        i = ju-1                                                        
        do 32 k = kl,ku                                                 
        lda(k) = e(i,k)                                                 
        ldb(k) = -lda(k)*u1(jl,k)                                       
        f(i,k,1) = f(i,k,1) - lda(k)*f(jl,k,1) - ldb(k)*f(jl+1,k,1)     
        f(i,k,2) = f(i,k,2) - lda(k)*f(jl,k,2) - ldb(k)*f(jl+1,k,2)     
        lds(k) = lda(k)*u3(jl,k) + ldb(k)*u3(jl+1,k)                    
        u4(i,k) = lda(k)*u4(jl,k) + ldb(k)*u4(jl+1,k)                   
32      continue                                                        
                                                                        
        do 3 m = jl+2,ju-4                                              
           do 14 k = kl,ku                                              
           ldd = -lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)                   
           lda(k) = ldb(k)                                              
           ldb(k) = ldd                                                 
           lds(k) = lds(k) + ldd*u3(m,k)                                
           u4(i,k) = u4(i,k) + ldd*u4(m,k)                              
           f(i,k,1) = f(i,k,1) - ldd*f(m,k,1)                           
           f(i,k,2) = f(i,k,2) - ldd*f(m,k,2)                           
14         continue                                                     
3       continue                                                        
c                                                                       
        m = ju-3                                                        
        do 15 k = kl,ku                                                 
        ldd = a(i,k) - lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)              
        lda(k) = ldb(k)                                                 
        ldb(k) = ldd                                                    
        lds(k) = lds(k) + ldd*u3(m,k)                                   
        u4(i,k) = u4(i,k) + ldd*u4(m,k)                                 
        f(i,k,1) = f(i,k,1) - ldd*f(m,k,1)                              
        f(i,k,2) = f(i,k,2) - ldd*f(m,k,2)                              
15      continue                                                        
        m = ju-2                                                        
        do 25 k = kl,ku                                                 
        ldd = b(i,k) - lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)              
        lda(k) = ldb(k)                                                 
        ldb(k) = ldd                                                    
        lds(k) = lds(k)+ ldd*u3(m,k)                                    
        u4(i,k) = u4(i,k) + ldd*u4(m,k)                                 
        f(i,k,1) = f(i,k,1) - ldd*f(m,k,1)                              
        f(i,k,2) = f(i,k,2) - ldd*f(m,k,2)                              
25      continue                                                        
c                                                                       
        do 35 k = kl,ku                                                 
        lds(k) = c(i,k) - lds(k)                                        
        ldi = 1./lds(k)                                                 
        u4(i,k) = (d(i,k) - u4(i,k))*ldi                                
        f(i,k,1) = f(i,k,1)*ldi                                         
        f(i,k,2) = f(i,k,2)*ldi                                         
35      continue                                                        
c                                                                       
c    this breakup of k loops is very important                          
c                                                                       
        i = ju                                                          
        do 16 k = kl,ku                                                 
        lda(k) = d(i,k)                                                 
        ldb(k) = e(i,k) - lda(k)*u1(jl,k)                               
        lus(k) = lda(k)*u3(jl,k) + ldb(k)*u3(jl+1,k)                    
        lds(k) = lda(k)*u4(jl,k) + ldb(k)*u4(jl+1,k)                    
        f(i,k,1) = f(i,k,1) - lda(k)*f(jl,k,1) - ldb(k)*f(jl+1,k,1)     
        f(i,k,2) = f(i,k,2) - lda(k)*f(jl,k,2) - ldb(k)*f(jl+1,k,2)     
c                                                                       
16      continue                                                        
c                                                                       
        do 4 m = jl+2,ju-3                                              
           do 17 k = kl,ku                                              
           ldd = -lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)                   
           lda(k) = ldb(k)                                              
           ldb(k) = ldd                                                 
           lus(k) = lus(k) + ldd*u3(m,k)                                
           lds(k) = lds(k) + ldd*u4(m,k)                                
           f(i,k,1) = f(i,k,1) - ldd*f(m,k,1)                           
           f(i,k,2) = f(i,k,2) - ldd*f(m,k,2)                           
17         continue                                                     
4       continue                                                        
c                                                                       
        m = ju-2                                                        
        do 18 k = kl,ku                                                 
        ldd = a(i,k) - lda(k)*u2(m-2,k) - ldb(k)*u1(m-1,k)              
        lda(k) = ldb(k)                                                 
        ldb(k) = ldd                                                    
        lus(k) = lus(k) + ldd*u3(m,k)                                   
        lds(k) = lds(k) + ldd*u4(m,k)                                   
        f(i,k,1) = f(i,k,1) - ldd*f(m,k,1)                              
        f(i,k,2) = f(i,k,2) - ldd*f(m,k,2)                              
18      continue                                                        
        m = ju-1                                                        
        do 28 k = kl,ku                                                 
        ldd = b(i,k) - lus(k)                                           
        f(i,k,1) = f(i,k,1) - ldd*f(m,k,1)                              
        f(i,k,2) = f(i,k,2) - ldd*f(m,k,2)                              
        lds(k) = c(i,k) - ldd*u4(m,k) - lds(k)                          
        ldi = 1./lds(k)                                                 
        f(i,k,1) = f(i,k,1)*ldi                                         
        f(i,k,2) = f(i,k,2)*ldi                                         
28      continue                                                        
c                                                                       
c        !  back sweep solution                                         
c                                                                       
      do 19 k = kl,ku                                                   
      f(ju,k,1) = f(ju,k,1)                                             
      f(ju,k,2) = f(ju,k,2)                                             
      f(ju-1,k,1) = f(ju-1,k,1) - u4(ju-1,k)*f(ju,k,1)                  
      f(ju-1,k,2) = f(ju-1,k,2) - u4(ju-1,k)*f(ju,k,2)                  
      f(ju-2,k,1) = f(ju-2,k,1) - u4(ju-2,k)*f(ju,k,1) -                
     *              u3(ju-2,k)*f(ju-1,k,1)                              
      f(ju-2,k,2) = f(ju-2,k,2) - u4(ju-2,k)*f(ju,k,2) -                
     *              u3(ju-2,k)*f(ju-1,k,2)                              
      f(ju-3,k,1) = f(ju-3,k,1) - u4(ju-3,k)*f(ju,k,1) -                
     *              u3(ju-3,k)*f(ju-1,k,1)- u1(ju-3,k)*f(ju-2,k,1)      
      f(ju-3,k,2) = f(ju-3,k,2) - u4(ju-3,k)*f(ju,k,2) -                
     *              u3(ju-3,k)*f(ju-1,k,2)- u1(ju-3,k)*f(ju-2,k,2)      
19      continue                                                        
c                                                                       
        do 2 i = 4,ju-jl                                                
        ix = ju-i                                                       
          do 50 k = kl,ku                                               
          f(ix,k,1) = f(ix,k,1) - u4(ix,k)*f(ju,k,1) -                  
     *                u3(ix,k)*f(ju-1,k,1)- u1(ix,k)*f(ix+1,k,1) -      
     *                u2(ix,k)*f(ix+2,k,1)                              
          f(ix,k,2) = f(ix,k,2) - u4(ix,k)*f(ju,k,2) -                  
     *                u3(ix,k)*f(ju-1,k,2)- u1(ix,k)*f(ix+1,k,2) -      
     *                u2(ix,k)*f(ix+2,k,2)                              
50        continue                                                      
2       continue                                                        
c                                                                       
        return                                                          
        end                                                             
