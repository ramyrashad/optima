      subroutine integrat(jdim,kdim,qp,q,qold,s,press, 
     &     D_hat, exprhs, akk,
     &     sndsp,turmu,fmu,vort,x,y,xy,xyj,xit,ett,ds,spectx,
     &     specty,uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &     lux,luy,precon,gam)
c
c     main integration calls                                            
c 
      implicit none

#include "../include/arcom.inc"

      integer jdim,kdim,ip,ix,iy,n,j,k
      double precision fct,fct1                              
      double precision qp(jdim,kdim,4),q(jdim,kdim,4),qold(jdim,kdim,4)  
      double precision press(jdim,kdim),sndsp(jdim,kdim),sndsp_norm                       
      double precision s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      double precision xit(jdim,kdim),ett(jdim,kdim),ds(jdim,kdim)             
      double precision x(jdim,kdim),y(jdim,kdim),turmu(jdim,kdim)              
      double precision vort(jdim,kdim),fmu(jdim,kdim)
      double precision precon(jdim,kdim,6),gam(jdim,kdim,16)
c                                                                       
      double precision spectx(jdim,kdim,3),specty(jdim,kdim,3)            
      double precision uu(jdim,kdim),ccx(jdim,kdim),s0(jdim,kdim,4)                            
      double precision vv(jdim,kdim),ccy(jdim,kdim)                            
      double precision coef2x(jdim,kdim),coef2y(jdim,kdim)                     
      double precision coef4x(jdim,kdim),coef4y(jdim,kdim)
      double precision lux(jdim,kdim,4,5),luy(jdim,kdim,4,5)
      double precision time1,time2,t,tarray(2),etime
c                                                                       
      double precision a0(maxj),b0(maxj),c0(maxj),f0(maxj),g0(maxj)
      double precision z0(maxj),qbcj(3,maxj,4),qbck(maxj,3,4)
      common/worksp/a0,b0,c0,f0,g0,z0,qbcj,qbck
c
      common/scratch/ a,b,c,d,e,work1

c     !!!!!! note : this use of d and e  array in equivalence to save 
c                storage is very dangerous.  in cases where d is        
c                used, such as the stepx and stepy routines values      
c                residing in equivalenced arrays may be bad.            

c
      double precision a(maxpen,4),b(maxpen,16),c(maxpen,16)
      double precision d(maxjk,16),e(maxpen,4),work1(maxjk,10)
c
      double precision D_hat(jdim,kdim,4,6)
      double precision exprhs(jdim,kdim,4)
      double precision akk
c
      double precision S_temp(jdim,kdim,4)
csisisisi
c    store the right hand side and solve implicitly several times
c    maybe just twice
c    see what happens
c    if the answer is always the same then i dont know what the prob
c    could be

c     save totime1
c     save totime2
c     t=etime(tarray)
c     time1=tarray(1)
c     time2=tarray(2)

csi   if this is the first subiteration some values have to be set
      if (jsubit.eq.0 .and. unsted )then
         call reset_qp(jdim,kdim,4,qp,q)
         call calcps(jdim,kdim,qp,press,sndsp,precon,xy,xyj)
         call bcupdate(jdim,kdim,qp,press,sndsp,xy,xit,ett,xyj,x,y,   
     &                 a0,b0,c0,f0,g0,z0,qbcj,qbck)                 
      endif
      if (jstage.eq.1 .and. (jesdirk.eq.4 .or. jesdirk.eq.3))then
         call reset_Dhat(jdim,kdim,D_hat)
         goto 998
      endif
      if(jacdt.gt.1)
     &     call varidt(jacdt,jdim,kdim,qp,sndsp,xy,xyj,ds,precon)      
c     
c     -compute laminar viscosity based on sutherlands law
      if(viscous) call fmun(jdim,kdim,qp,press,fmu,sndsp)
c     
c     
cmpr      call reset_s(jdim,kdim,s)
      if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5) then
c     -Subiteration component for explicit side
c     -implemented by Stan De Rango  (Aug 12,1994)
         call subit(jdim,kdim,qp,q,qold,s,akk)
csi    add explicit terms for esdirk
         if (jesdirk.eq.4 .or. jesdirk.eq.3) then 
            call add_exprhs(jdim,kdim,s,exprhs)
         endif
      endif
c     
c     
c*********************************************************************  
c***                     EXPLICIT  RHS                             ***  
c*********************************************************************  
c                                                                       
c note: order of explicit operators for vorticity calculation must be 
c        xi then eta                                                   
c  
         
      call xiexpl(jdim,kdim,qp,s,s0,press,sndsp,turmu,fmu,vort,x,y,xy,
     &     xyj,xit,ds,uu,ccx,coef2x,coef4x,spectx,precon,gam)
  
c                                                                       
c                                                                       
      if(viscous .and. viscross) then                                   
c        -viscous cross terms
         call visrhsnc(jdim,kdim,qp,press,s,turmu,fmu,xy,xyj,        
     >                 work1(1,1), work1(1,2), work1(1,3), work1(1,4),
     >                 work1(1,5), work1(1,6), work1(1,7),             
     >                 work1(1,8), work1(1,9), d)                       
      endif                                                             
c                                                                       
c
      call etaexpl(jdim,kdim,qp,s,s0,press,sndsp,turmu,fmu,vort,x,y,
     &             xy,xyj,ett,ds,uu,vv,ccy,coef2y,coef4y,specty,precon,
     &             gam)

c
c 777  t=etime(tarray)
c      time1=tarray(1)-time1
c      time2=tarray(2)-time2
c      totime1=totime1 + time1
c      totime2=totime2 + time1 + time2
c      if (timing) then
c        write(time_unit,*) numiter,totime1,totime2
c     if (numiter.eq.iend) write(time_unit,*)'average=',
c     totime1/dble(numiter)
c        call flush(time_unit)
c      endif

c     -compute l2 and max norms of residual
      if (viscous .and. turbulnt .and. itmodel .eq. 2) then
c     -- total residual --
         call residl2(jdim,kdim,4,s,4,0.d0)
c         call residinf(jdim,kdim,4,s,4)
c     -- quit if residual becomes too high or nan --
         if (resid .gt. 1.0e3) then
            write(*,*) 'integrat.f : residual is too high or 
     &           nan, exit'
            write(*,*) 'resid',resid
            resid = 1000.0
            return
         end if
      else
c     -- density residual --
         call residl2(jdim,kdim,4,s,1,0.d0)
      end if
c
c     write residual to res_unit
      if(writeresid) then
        if (mod(numiter,100).eq.1 .or. numiter.eq.iend 
     &           .or. resid.lt.min_res) then
          ip = 21
          call ioall(ip,0,jdim,kdim,s,qold,press,sndsp,turmu,fmu,
     &      vort,xy,xyj,x,y)
        endif
      endif
c
c     -scale with variable dt
      call scaledt(jdim,kdim,s,ds)                                  
c

c
c*********************************************************************  
c***                     IMPLICIT  LHS                             ***  
c*********************************************************************  
      
c      if (jstage.ne.1 .or. (jesdirk.ne.4 .and. jesdirk.ne.3))then
      if (meth.eq.4 .or. meth.eq.5) then
c     
c     note that the following is valid only for orderxy=true.
         if (jsubit.eq.0) then
c     
c     implicit xi rhs  - xy order
            call xiimpl(jdim,kdim,qp,s,press,sndsp,turmu,fmu,
     &           vort,x,y,xy,xyj,xit,ds,uu,ccx,coef2x,coef4x,
     &           spectx,lux,precon,akk) 
c     
c     implicit eta rhs
            call etaimpl(jdim,kdim,qp,s,press,sndsp,turmu,fmu,
     &           vort,x,y,xy,xyj,ett,ds,vv,ccy,coef2y,coef4y,
     &           specty,luy,precon,akk)
         else
            ix = 1                                                    
            iy = 2
            call subit3(jdim,kdim,qp,sndsp,s,xyj,xy,ix,iy,lux)
c     
            if (ismodel.gt.1) then
               fct1=1.d0
               if (ismodel.eq.2) fct1=1.5d0/dt2
               fct=1.5d0*dt/dt2 
               do 50 n=1,4
               do 50 k=klow,kup
               do 50 j=jlow,jup
                  s(j,k,n)=s(j,k,n)*(fct1 + fct*ds(j,k))
 50            continue
            endif
c     
            ix = 3                                                    
            iy = 4
            call subit3(jdim,kdim,qp,sndsp,s,xyj,xy,ix,iy,luy)
         endif
      else ! not meth 4 or 5:
c     if(orderxy)then                                                 
c     implicit xi rhs  - xy order
         call xiimpl(jdim,kdim,qp,s,press,sndsp,turmu,fmu,vort,x,y,
     &        xy,xyj,xit,ds,uu,ccx,coef2x,coef4x,spectx,lux,
     &        precon,akk)        
c     
c     implicit eta rhs   
         call etaimpl(jdim,kdim,qp,s,press,sndsp,turmu,fmu,vort,x,y,
     &        xy,xyj,ett,ds,vv,ccy,coef2y,coef4y,specty,luy,
     &        precon,akk)

c     
c     if(.not.orderxy)then                                           
c     implicit xi rhs  - yx order
c     call xiimpl(jdim,kdim,qp,s,press,sndsp,turmu,fmu,vort,x,y,xy, 
c     &      xyj,xit,ds,uu,ccx,coef2x,coef4x,spectx,lux,precon)    

      endif ! (meth.eq.4 .or. meth.eq.5)
c
c<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  
c<<<                    finish integration                         >>>  
c<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  
c                                                                       
c     -update solution vector    
 999  call update(jdim,kdim,qp,s)                              
c                                                                       
c     -update pressure and soundspeed   
      call calcps(jdim,kdim,qp,press,sndsp,precon,xy,xyj)

c     -- check abnormal or nan resid --
      if ( abs( resid - 1000.0) .lt. 0.01 ) return

c*********************************************************************  
c***   10. explicit boundary conditions                            ***  
c*********************************************************************  
c                                                                       
c      iord=2
      if (bcairf) then                                               
         call bcupdate(jdim,kdim,qp,press,sndsp,xy,xit,ett,xyj,x,y,   
     &                 a0,b0,c0,f0,g0,z0,qbcj,qbck)                 
      else                                                  
         call bcplate(jdim,kdim,qp,press,sndsp,xy,xit,ett,xyj,x,y)
      endif
      
c      iord=4
c
c     write turbulence quantities to unit 26
      if(writeturb) then
        if (mod(numiter,500).eq.1 .or. numiter.eq.iend 
     &           .or. resid.lt.min_res) then
          ip = 25
          call ioall(ip,0,jdim,kdim,qp,qold,press,sndsp,turmu,fmu,
     &      vort,xy,xyj,x,y)
        endif
      endif

c      if (mod(numiter-istart+1,10) .eq. 0) then
c        do j=jbegin,jend
c          write(88,*) x(j,kbegin),turmu(j,kbegin)
c        enddo
c        rewind(88)
c      endif
c      do k=1,kend
c      do j=1,jend
c        budget(j,k,1)=vort(j,k)*dt
c      enddo
c      enddo
c       flbud=.true.
c      for examining eddy viscosity
c      write(77) jmax,kmax                                             
c      write(77) fsmach,alpha,re*fsmach,totime                         
c      write(77) (((turmu(j,k),j=1,jmaxold) ,k=1,kmax) ,n=1,4)  
c      write(77) jtail1, numiter                                     
c
c
csi ************************************************************
csi ************************************************************
csi *****   Recalculate rhs after solving for new qp************
csi ******** and store this value into dhat ********************
csi             (only for jesdirk 3 or 4)
csi ************************************************************
 998  jsubit=jsubit+1
      if((jesdirk.eq.4 .or. jesdirk.eq.3))then
         
         call reset_s(jdim,kdim,s)
         call xiexpl(jdim,kdim,qp,s,press,sndsp,turmu,fmu,
     &        vort,x,y,xy,xyj,xit,ds,uu,ccx,coef2x,coef4x,
     &        spectx,precon,gam)         
         call etaexpl(jdim,kdim,qp,s,press,sndsp,turmu,
     &        fmu,vort,x,y,xy,xyj,ett,ds,uu,vv,ccy,coef2y,
     &        coef4y,specty,precon,gam)
         call calc_Dhat(jdim,kdim,s,D_hat)
      endif
      return                                             
      end                                                
