      subroutine bdyspln(jdim,kdim,iside,x,y,sn,xn,yn,x2,y2,
     &      jbegin,jend,kbegin,kend)
c
#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/units.inc"
c
      integer inter
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension cp(jdim),cp2(jdim),sn(jdim),xn(jdim),yn(jdim)
      dimension x2(jdim),y2(jdim)
c
c     -worksp has space for 5*maxj
      dimension zeta(maxj),work(maxj,3)
      common/worksp/z,work
c
c     cp  = used to store both coefficient of pressure and friction
c     xn  = chordwise direction (pos. in direction of LE to TE)
c     yn  = normal to chord
c     sn  = parameter approximating arclength (going from lower TE to
c           upper TE)
c     zeta= dummy variable for parameterization of xn,yn,cp
c     dx  = d(xn)/d(sn)
c     dy  = d(yn)/d(sn)
c     x2,y2,cp2 = double derivative of corresponding variable
c
c     -set limits of integration
      j1 = jbegin
      j2 = jend
      jtp = jbegin + 1
      if (iside.eq.1) k1 = kbegin
      if (iside.eq.3) k1 = kend
      if (iside.eq.2 .or. iside.eq.4) then
        write(n_out,*) 'High-order integration set-up for non-blunt'
        write(n_out,*) 'trailing edges only.'
        stop
      endif
      jd=j2-j1+1
c

c     -spline airfoil surface so you have y',y'',x',and x''  w.r.t s.
c     -here we simply store values of x and y in work arrays xn and yn
      k=k1
      zeta(1)=0.d0
      xn(1)=x(j1,k)
      yn(1)=y(j1,k)
      do 1 j=jtp,j2
        jj=j-jtp+2
        zeta(jj)=dble(jj-1)
        xn(jj)=x(j,k)
        yn(jj)=y(j,k)
 1    continue

c     We begin by splining y & x w.r.t zeta, a dummy variable.
c      
c     ****************************************************      
c     *                  Funct: y(zeta)                  *
c     ****************************************************      
c     Boundary Conditions
      yp1=bc(jd,1,1,zeta,yn)
      ypn=bc(jd,jd,-1,zeta,yn)
c      yp1=0.d0
c      ypn=0.d0
c      
c      print *,'bc of y',yp1,ypn
      call spline(jd,zeta,yn,yp1,ypn,y2,work,work(1,2),work(1,3))
c      
c     ****************************************************      
c     *                  Funct: x(zeta)                  *
c     ****************************************************      
c     Boundary Conditions
      yp1=bc(jd,1,1,zeta,xn)
      ypn=bc(jd,jd,-1,zeta,xn)
c      
c      print *,'bc of x',yp1,ypn
      call spline(jd,zeta,xn,yp1,ypn,x2,work,work(1,2),work(1,3))
c      
c       do 6 j=1,jd
c          call splintder(jd,sn,xn,x2,sn(j),dx)
c          print *,j,sn(j)/sn(jd),dx
c 6     continue
c       stop
c      
c     Now, using y(zeta) & x(zeta), we interpolate as many points as we
c     wish between zeta points to get an accurate calculation of
c     arclength.  The value is stored in sn.  Then we respline y & x,
c     but this time we do w.r.t. sn.
c     The value 'inter' is the number of intervals between two
c     zeta points. So a total of 'inter+1' points discretize the
c     arclength between the two points.
c      
      inter=49
      sn(1)=0.d0
      do 5 j=2,jd
        ds=(zeta(j)-zeta(j-1))/dble(inter)
        sntmp=sn(j-1)
        xp=xn(j-1)
        yp=yn(j-1)
        do 6 k=2,inter+1
          stmp=zeta(j-1)+dble(k-1)*ds
          call splint(jd,zeta,xn,x2,stmp,xtmp)
          call splint(jd,zeta,yn,y2,stmp,ytmp)
          sntmp=sntmp + dsqrt((xtmp-xp)**2 + (ytmp-yp)**2)
          xp=xtmp
          yp=ytmp
 6      continue
        sn(j)=sntmp
 5    continue
c      
c     ****************************************************      
c     *                   Funct: y(sn)                   *
c     ****************************************************      
c     Boundary Conditions
      yp1=bc(jd,1,1,sn,yn)
      ypn=bc(jd,jd,-1,sn,yn)
c      yp1=0.d0
c      ypn=0.d0
c      
c      print *,'bc of y',yp1,ypn
      call spline(jd,sn,yn,yp1,ypn,y2,work,work(1,2),work(1,3))
c      
c     ****************************************************      
c     *                   Funct: x(sn)                   *
c     ****************************************************      
c     Boundary Conditions
      yp1=bc(jd,1,1,sn,xn)
      ypn=bc(jd,jd,-1,sn,xn)
c      
c      print *,'bc of x',yp1,ypn
      call spline(jd,sn,xn,yp1,ypn,x2,work,work(1,2),work(1,3))
c
      return
      end
