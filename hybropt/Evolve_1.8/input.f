C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C               IIIII   NN    N  PPPPP  U    U TTTTTTT                 C
C                 I     N N   N  P    P U    U    T                    C
C                 I     N  N  N  PPPPP  U    U    T                    C
C                 I     N   N N  P      U    U    T                    C
C               IIIII   N    NN  P       UUUU     T                    C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE INPUT(IPOP,i_ran_seed,nchrom,ngene,nout,npop,
     $     beta,beta_sin,nobfun,npi,npr,npl,iparm,parmr,parml,
     $     iaccum,iselect,
     $     beta_min,beta_max,n_beta_s,n_beta_i,i_mnx,fit_init,
     $     init_ga,debug,ga_file_prefix,
     $     itran,scale_genes,trans_minmax,nbin,is_bin,icull,nbin_cull,
     $     io_34,fit_min_max,genemin,genemax,Igeneprec,geneprec,
     $     eps_domin,int_real,arc2d_io,field_pop,field_fit,
     $     field_log,field_ngene,field_nobfun,rand_init_fit,i_avg_cho,
     $     CBEST,  sig_share, box_over, prob_pert, prob_mut, prob_avg,
     $     XBEST,      XAVG,       XPERT,      XMUT,
     $     NBEST,      NAVG,       NPERT,      NMUT, compute_pass_thu)
      LOGICAL INERR,debug,geneprec
      LOGICAL CBEST,scale_genes,trans_minmax,compute_pass_thu
      logical box_over,beta_sin
      logical parml, rand_init_fit
      logical io_34,fit_min_max,arc2d_io
      character*40 field_pop(100),field_fit(100),field_log,
     $     field_ngene,field_nobfun
      real*8 rand
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTING PROVIDES DEFAULT VALUES FOR ALL        C
C               NAMELIST GCONTRL PARAMETERS AND THEN UPDATES           C
C               THEM WITH A NAMELIST READ ***                          C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      dimension genemin(100), genemax(100), Igeneprec(100)
      dimension int_real(100)

C
C-----------------------------------------------------------------------
      character*80 ga_file_prefix
      integer strlen
      character*80 filena
C-----------------------------------------------------------------------
C
      DIMENSION  IPARM(20),  PARMR(20), PARML(10)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      NAMELIST /GCONTRL/BETA,       GENEMIN,    GENEMAX,    IPARM,
     1     IACCUM,     ISELECT,    NGENE,      NOBFUN,
     2     NOUT,       NPI,        NPL,        NPR, 
     3     PARML,      PARMR,      XAVG,       XBEST,
     4     XMUT,       XPERT,      CBEST,      sig_share,
     5     box_over,   i_ran_seed, prob_pert,  prob_mut,   prob_avg,
     6     beta_min,   beta_max,   n_beta_s,   n_beta_i, i_avg_cho,
     $     i_mnx,  beta_sin,  debug,   init_ga,    itran,
     $     scale_genes, trans_minmax, nbin, is_bin, icull, nbin_cull,
     $     io_34, fit_min_max, igeneprec, geneprec, eps_domin, int_real,
     $     arc2d_io, field_pop, field_fit, field_nobfun, field_ngene,
     $     field_log, rand_init_fit, compute_pass_thu
c
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** ESTABLISH DEFAULT VALUES FOR THE NAMELIST 
C              /CONTRL/ PARAMETERS ***
C
      BETA        =  0.3
      GENEMIN(1)  = -0.01
      GENEMIN(2)  = -0.01
      GENEMIN(3)  = -0.01
      GENEMIN(4)  = -0.01
      GENEMIN(5)  = -0.01
      GENEMIN(6)  = -0.01
      GENEMIN(7)  = -0.01
      GENEMIN(8)  = -0.01
      GENEMIN(9)  =  0.0
      GENEMIN(10) = -3.0
      GENEMIN(11) = -0.01
      GENEMIN(12) = -0.01
      GENEMIN(13) = -0.0
      GENEMIN(14) = -0.0
      GENEMIN(15) =  0.0
      GENEMIN(16) =  0.0
      GENEMIN(17) =  0.0
      GENEMIN(18) = -3.0
      GENEMIN(19) = -0.0
      GENEMIN(20) = -0.0
      GENEMAX(1)  =  0.01
      GENEMAX(2)  =  0.01
      GENEMAX(3)  =  0.01
      GENEMAX(4)  =  0.01
      GENEMAX(5)  =  0.01
      GENEMAX(6)  =  0.01
      GENEMAX(7)  =  0.01
      GENEMAX(8)  =  0.01
      GENEMAX(9)  =  3.0
      GENEMAX(10) =  0.0
      GENEMAX(11) =  0.01
      GENEMAX(12) =  0.01
      GENEMAX(13) =  0.0
      GENEMAX(14) =  0.0
      GENEMAX(15) =  0.0
      GENEMAX(16) =  0.0
      GENEMAX(17) =  3.0
      GENEMAX(18) =  0.0
      GENEMAX(19) =  0.0
      GENEMAX(20) =  0.0
      IGENEPREC(1)  =  0
      IGENEPREC(2)  =  0
      IGENEPREC(3)  =  0
      IGENEPREC(4)  =  0
      IGENEPREC(5)  =  0
      IGENEPREC(6)  =  0
      IGENEPREC(7)  =  0
      IGENEPREC(8)  =  0
      IGENEPREC(9)  =  0
      IGENEPREC(10) =  0
      IGENEPREC(11) =  0
      IGENEPREC(12) =  0
      IGENEPREC(13) =  0
      IGENEPREC(14) =  0
      IGENEPREC(15) =  0
      IGENEPREC(16) =  0
      IGENEPREC(17) =  0
      IGENEPREC(18) =  0
      IGENEPREC(19) =  0
      IGENEPREC(20) =  0
      INT_REAL(1)  =  0
      INT_REAL(2)  =  0
      INT_REAL(3)  =  0
      INT_REAL(4)  =  0
      INT_REAL(5)  =  0
      INT_REAL(6)  =  0
      INT_REAL(7)  =  0
      INT_REAL(8)  =  0
      INT_REAL(9)  =  0
      INT_REAL(10) =  0
      INT_REAL(11) =  0
      INT_REAL(12) =  0
      INT_REAL(13) =  0
      INT_REAL(14) =  0
      INT_REAL(15) =  0
      INT_REAL(16) =  0
      INT_REAL(17) =  0
      INT_REAL(18) =  0
      INT_REAL(19) =  0
      INT_REAL(20) =  0
      geneprec      = .false.
      IACCUM      =  0
      ISELECT     =  1
      NGENE       =  10
      NOBFUN      =  1
      NOUT        =  1
      NPI         =  6
      NPL         =  1
      NPR         =  3
      if(ipop.eq.0)then
         do i = 1,20
            IPARM(i)    =  0
c$$$  IPARM(2)    =  10
c$$$  IPARM(3)    =  0
c$$$  IPARM(4)    =  2
c$$$  IPARM(5)    =  2
c$$$  IPARM(6)    =  1
            PARMR(i)    =  0.0
c$$$  PARMR(2)    =  0.0
c$$$  PARMR(3)    =  0.0
         enddo
         do i = 1,10
            PARML(i)    =  .false.
         enddo
         PARML(1)    =  .TRUE.
      endif
      XAVG        =  20.0
      XBEST       =  10.0
      XMUT        =  40.0
      XPERT       =  30.0
      CBEST       = .FALSE.
      sig_share   = 0.0
      box_over    = .false.
      i_ran_seed  = 0
      prob_pert   = 0.2
      prob_mut    = 0.2
      prob_avg    = 1.0
      beta_min    = 0.0
      beta_max    = beta
      n_beta_s    = 300
      n_beta_i    = 300
      i_avg_cho   = 0 ! orig avg oper  1=new'
      i_mnx       = 0  !  maximum problem
      beta_sin    = .false.
      debug       = .false.
      init_ga     = 1  !  random genes between genemin and genemax
      itran       = 0
      scale_genes = .false.
      trans_minmax = .false.
      nbin = 5
      is_bin = 20
      icull  = 0
      nbin_cull = 20
      io_34 = .false.
      fit_min_max = .false.
      eps_domin = 0.0
      arc2d_io = .true.
      field_nobfun = 'nobfun'
      DO NO=1,100
         field_fit(no) = 'fit'
      enddo
      field_log = 'Process_Chromo'
      field_ngene = 'ngene'
      do ng = 1,100
         field_pop(ng) = 'POP'
      enddo
      rand_init_fit = .false.
      compute_pass_thu       = .true.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** READ NEW VALUES FOR NAMELIST PARAMETERS ***
C
      namelen =  strlen(ga_file_prefix)
      filena = ga_file_prefix
      filena(namelen+1:namelen+6) = '_ga.i'
      INQUIRE (FILE=filena, EXIST=INERR)
      IF (.NOT.INERR) THEN
         INQUIRE (FILE='ga.i', EXIST=INERR)
         IF (.NOT.INERR) THEN
            OPEN (50,FILE='ga.error',STATUS='UNKNOWN',FORM='FORMATTED')
            WRITE (50,1002)
            close(50)
            WRITE (*,1002)
c$$$  if(io_34)WRITE (34,1002)
 1002       FORMAT (/'INPUT: CANNOT FIND ga.i, EXECUTION TERMINATED'/)
            STOP
         else
            filena = 'ga.i'
         endif
      ENDIF
      OPEN (51,FILE=filena,STATUS='UNKNOWN',FORM='FORMATTED')
c$$$      OPEN (51,FILE='ga.i',STATUS='UNKNOWN',FORM='FORMATTED')
      READ (51,GCONTRL)
      print *,'Read in ',filena
      CLOSE (51)
C
c error checks
      if((iselect.ge.5 .and. iselect.le.8) .and. iaccum.ne.1)then
         print *,'Error iselect.eq.5 and iaccum.ne.1'
         STOP 'Error iselect.eq.5 and iaccum.ne.1'
      endif
      if((iselect.ge.5 .and. iselect.le.8) .and. .not.cbest)then
         print *,'CBEST and compute_pass_thu needs to be true',
     $        ' for selection from accumlation file, reset to T'
         CBEST = .true.
         compute_pass_thu = .true.
      endif
      if(.not.arc2d_io)then
         npl = 1 
         npi = 0 
         npr = 0
      endif
C-----------------------------------------------------------------------
c  open files
      namelen =  strlen(ga_file_prefix)
      filena = ga_file_prefix
      filena(namelen+1:namelen+8) = '_33.out'
c$$$      filena = 'File33.out'
      open(unit=33,file=filena,form='formatted')
      namelen =  strlen(ga_file_prefix)
      if(io_34)then
         filena = ga_file_prefix
         filena(namelen+1:namelen+8) = '_34.out'
c$$$  filena = 'File34.out'
         open(unit=34,file=filena,form='formatted')
      endif
      if(debug)then
         namelen =  strlen(ga_file_prefix)
         filena = ga_file_prefix
         filena(namelen+1:namelen+6) = '.debug'
         open(unit=49,file=filena,form='formatted')
      endif
c  oputput of rank info
      namelen =  strlen(ga_file_prefix)
      filena = ga_file_prefix
      filena(namelen+1:namelen+6) = '.rank'
      open(unit=52,file=filena,form='formatted')
C-----------------------------------------------------------------------
C          *** PERFORM CONSISTENCY CHECKS ON INPUT ***
C-----------------------------------------------------------------------
C          *** MAKE SURE THAT XBEST+XAVG+XPERT+XMUT IS
C              100.0. OTHERWISE TERMINATE EXECUTION ***
C
      iunit = 0
      if(io_34)iunit = 34
      if(.not.io_34 .and. debug)iunit = 49
      if(iunit.eq.0)go to 410
 409  continue
      IF (IPOP.EQ.0 .or. iunit.eq.49) THEN
         XTOT  = XBEST+XAVG+XPERT+XMUT
         IF (XTOT.NE.100.0) THEN
            WRITE (iunit,201) XBEST,XAVG,XPERT,XMUT
  201       FORMAT (/'INPUT:  INPUT ERROR DETECTED:'/
     1               '        XBEST+XAVG+XPERT+XMUT MUST BE 100.0'/
     2               '        CURRENT VALUES ARE'/
     3               '        XBEST =', F8.4/
     4               '        XAVG  =', F8.4/
     5               '        XPERT =', F8.4/
     6               '        XMUT  =', F8.4/
     7               '        EXECUTION TERMINATED'/)
            STOP
         ENDIF
C
         IF (NGENE.GT.100) THEN
            WRITE (iunit,205) NGENE
  205       FORMAT (/'INPUT: INTEGER INPUT PARAMETER INCONSISTENCY'/
     1               '       NGENE MUST BE LESS THAN OR EQUAL TO 100',
     2               '       DUE TO PROGRAM DIMENSIONS'/
     3               '       NGENE  =',I4/
     4               '       EXECUTION TERMINATED'/)
            STOP
         ENDIF
C 
         IF (NCHROM.GT.2048) THEN
            WRITE (iunit,208) NCHROM
  208       FORMAT (/'INPUT: VALUE OF NCHROM EXCEEDS DIMENSIONS',
     1               '       NCHROM =',I6,
     2               '       EXECUTION TERMINATED'/)
            STOP
         ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** WRITE GA BANNER ***
C
      IF (NOUT.GE.1.AND.IPOP.EQ.0 .or. iunit.eq.49) THEN
         WRITE (iunit,880) 
         WRITE (iunit,880) 
         WRITE (iunit,881) 
         WRITE (iunit,881) 
         WRITE (iunit,882) 
         WRITE (iunit,883) 
         WRITE (iunit,884) 
         WRITE (iunit,885) 
         WRITE (iunit,886) 
         WRITE (iunit,887) 
         WRITE (iunit,888) 
         WRITE (iunit,881) 
         WRITE (iunit,881) 
c$$$         WRITE (iunit,889) 
         WRITE (iunit,881) 
         WRITE (iunit,881) 
         WRITE (iunit,880) 
         WRITE (iunit,880) 
  880    FORMAT (64(1H*))
  881    FORMAT (2H**,60(1H ),2H**)
  882    FORMAT (2H**,10X,
     1    '           GGGGGGG     AAAAAAA          ',10X,2H**)
  883    FORMAT (2H**,10X,
     1    '          G       G   A       A         ',10X,2H**)
  884    FORMAT (2H**,10(1H ),
     1    '          G           A       A         ',10X,2H**)
  885    FORMAT (2H**,10X,
     1    '          G    GGG    AAAAAAAAA         ',10X,2H**)
  886    FORMAT (2H**,10(1H ),
     1    '          G       G   A       A         ',10X,2H**)
  887    FORMAT (2H**,10X,
     1    '          G       G   A       A         ',10X,2H**)
  888    FORMAT (2H**,10X,
     1    '           GGGGGGG    A       A         ',10X,2H**)
  889    FORMAT (2H**,11X,
     1             'GENETIC ALGORITHM OPTIMIZATION (GA)      ',8X,2H**/
     2    2H**,11X,' Version  Evolver_0.6: TLH & THP     03/05/2002  ',
     $        2H**/
     3    2H**,11X,'                                         ',8X,2H**/
     3    2H**,11X,'WRITTEN BY: DR. TERRY L. HOLST           ',8X,2H**/
     3    2H**,11X,'            DR. THOMAS H. PULLIAM        ',8X,2H**/
     4    2H**,11X,'            NASA AMES RESEARCH CENTER    ',8X,2H**/
     5    2H**,11X,'            MOFFETT FIELD, CALIFORNIA    ',8X,2H**)
         call ga_version(iunit)
      ENDIF
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** WRITE UPDATED VALUES OF UNIT 5 PARAMETERS BUT
C              ONLY IF IPOP=0 ***
C
      IF (NOUT.GE.1.AND.IPOP.EQ.0) THEN
         WRITE (iunit,200) IPOP,NPOP,NCHROM
  200    FORMAT (/'GA:  IPOP   =',I6/
     1            '     NPOP   =',I6/
     2            '     NCHROM ='I6/) 
      ENDIF
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** WRITE UPDATED VALUES OF GCONTRL PARAMETERS ***
C
      IF (NOUT.GE.1.AND.IPOP.EQ.0 .or. iunit.eq.49) THEN
         WRITE (iunit,301) 
         WRITE (iunit,302)  BETA
         WRITE (iunit,303) (GENEMIN(NG),NG=1,NGENE)
         WRITE (iunit,304) (GENEMAX(NG),NG=1,NGENE)
         if(geneprec)WRITE (iunit,315) (IGENEPREC(NG),NG=1,NGENE)
         WRITE (iunit,316) (Int_real(NG),NG=1,NGENE)
         if(init_ga.eq.0)write (iunit,357)
         if(init_ga.eq.1)write (iunit,358)
         WRITE (iunit,305)  IACCUM
         IF (NPI.GT.0)   WRITE (iunit,306) (IPARM(NP),NP=1,NPI)
         WRITE (iunit,307)  ISELECT
         WRITE (iunit,308)  NGENE,NOBFUN,NOUT,
     1                   NPI,NPL,NPR
         IF (NPL.GT.0)   WRITE (iunit,309) (PARML(NP),NP=1,NPL)
         IF (NPR.GT.0)   WRITE (iunit,310) (PARMR(NP),NP=1,NPR)
         WRITE (iunit,311)  XAVG,XBEST,XMUT,XPERT
         IF(.NOT.CBEST)WRITE (iunit,313)
         IF(CBEST)WRITE (iunit,314)
         IF(.NOT.COMPUTE_PASS_THU)WRITE (iunit,313)
         IF(COMPUTE_PASS_THU)WRITE (iunit,314)
         if(sig_share.eq.0.0 .and. nobfun.gt.1)write(iunit,321)
         if(sig_share.ne.0.0.and. nobfun.gt.1)write(iunit,322)sig_share
         if(box_over)write(iunit,350)
         if(i_ran_seed.ge.0)write(iunit,351)(-ipop-1-i_ran_seed)
         if(i_ran_seed.lt.0)write(iunit,351)(i_ran_seed)
         if(i_avg_cho.eq.0)write(iunit,378)
         if(i_avg_cho.eq.1)write(iunit,379)
         write(iunit,352)prob_pert, prob_mut,prob_avg
         if(beta_min .ne. 0.0)then
            write(iunit,353)beta_max,beta_min,n_beta_s,n_beta_i
         endif
         if(beta_sin)then
            if(beta_min .eq.0.0 .or. n_beta_i.eq.0)then
               print *,'Beta_max/max need to be set'
               print *,'check n_beta_i (can not be 0)',n_beta_i
               stop
            endif
            write(iunit,359)
         endif
         if(i_mnx.eq.1)WRITE (iunit,354) 
         if(i_mnx.eq.0)WRITE (iunit,355) 
         if(debug)write(iunit,360)
         if(itran.ge.1)write(iunit,370)
         if(scale_genes)write(iunit,371)
         if(trans_minmax)write(iunit,372)
         if(iselect.eq.7.or.iselect.eq.8)write(iunit,373)nbin,is_bin
         if(icull.ne.0)write(iunit,374)icull,nbin_cull
         if(eps_domin.ne.0.0)write(iunit,375)eps_domin
         if(arc2d_io)write(iunit,376)
         if(.not.arc2d_io)write(iunit,377)
         WRITE (iunit,312) 
  301    FORMAT (/'$GCONTRL')
  302    FORMAT ('BETA     = ',F9.7)
  303    FORMAT ('GENEMIN  = ',10F9.5/11X,10F9.5/11X,10F9.5/11X,10F9.5/
     1                     11X,10F9.5/11X,10F9.5/11X,10F9.5/11X,10F9.5)
  304    FORMAT ('GENEMAX  = ',10F9.5/11X,10F9.5/11X,10F9.5/11X,10F9.5/
     1                     11X,10F9.5/11X,10F9.5/11X,10F9.5/11X,10F9.5)
 315     FORMAT ('IGENEPREC  = ',10I3/11X,10I3/11X,10I3/11X,10I3/
     1                     11X,10I3/11X,10I3/11X,10I3/11X,10I3)
 316     FORMAT ('Int_real  = ',10I3/11X,10I3/11X,10I3/11X,10I3/
     1                     11X,10I3/11X,10I3/11X,10I3/11X,10I3)
  305    FORMAT ('IACCUM   = ',I9)
  306    FORMAT ('IPARM    = ',10I9)
  307    FORMAT ('ISELECT  = ',I9)
  308    FORMAT ('NGENE    = ',I9/
     1           'NOBFUN   = ',I9/
     2           'NOUT     = ',I9/
     3           'NPI      = ',I9/
     4           'NPL      = ',I9/
     5           'NPR      = ',I9)
  309    FORMAT ('PARML    = ',10L9)
  310    FORMAT ('PARMR    = ',10F9.5)
  311    FORMAT ('XAVG     = ',F9.5/
     1           'XBEST    = ',F9.5/
     2           'XMUT     = ',F9.5/
     3           'XPERT    = ',F9.5)
  312    FORMAT ('$END'/)
 313     FORMAT('CBEST = FALSE, NBEST CHROMOSOMES HAVE PARML(1) = F',
     $        ' SO THAT THE FITNESS IS NOT REEVALUATED', 
     $        ': I.E. PASSED THROUGH CHROMOSOMES')
 314     FORMAT('CBEST = TRUE, NBEST CHROMOSOMES HAVE PARML(1) = T',
     $        ' SO THAT THE FITNESS IS REEVALUATED', 
     $        ': EVEN FOR PASSED THROUGH CHROMOSOMES')
 317     FORMAT('COMPUTE_PASS_THU = FALSE, CHROMOS HAVE PARML(1) = F',
     $        ' SO THAT THE FITNESS IS NOT REEVALUATED if chromo does ' 
     $        ' not change : I.E. PASSED THROUGH CHROMOSOMES')
 318     FORMAT('COMPUTE_PASS_THU = TRUE, FITNESS IS EVALUATED ',
     $        ' even if chromo does not change')
      ENDIF
 321  FORMAT ('Niche not used')
 322  FORMAT ('Niche used with sig_share = ',f12.4)
 350  format(' Box Over Used')
 351  format(' Random number seed set to ',i10)
 352  format(' Pert, Mut, Avg operator probability =  ',3(2x,f12.4))
 353  format(' Beta varied from ',f8.4,' to ',f8.4,
     $     ' after ',i5,' function evals ',' linearly for next ',
     $     i5,' function evals')
 354  format(' Maximization process')
 355  format(' Minimization process')
 357  format(' Initialize chromosomes with min/max average',
     $     ' + RANDOM across genes ')
 358  format(' Initialize chromosomes with min/max average',
     $     ' all random across genes')
 359  format('Sin variation of beta')
 360  format('Debug Option')
 370  format('Transformation Option USED!!')
 371  format('Scaling used in Transformation Option!!')
 372  format('Scaling used in Transformation for gene min/max!!')
 373  format('Nbin= ',i5,' after is_bin ',i5,' elements in accum file')
 374  format('ICULL= ',i5,' Using ',i5,'  bins')
 375  format('eps_domin= ',f10.4,' eps dominance used')
 376  format('arc2d io format for chromo files USED !!')
 377  format('arc2d io format for chromo files NOT USED !!')
 378  format('Avg Oper based on gene/gene chromo/chromo ')
 379  format('Avg Oper across chromo gen by gene with prob_avg ')
C
C          *** INPUT COMPLETE ***
C-----------------------------------------------------------------------
c  Checks and balances 
      if(beta_min .ne.0.0 .and. beta.ne.beta_max)then
         write(iunit,*)'For Variable Beta Option ',
     $        'Beta reset to beta_max = ',beta_max
      endif

      if(debug .and. iunit.ne.49)then
         iunit = 49
         go to 409
      endif
 410  continue
      if(beta_min .ne.0.0 .and. beta.ne.beta_max)then
         beta = beta_max
      endif
      fit_init = 0.0
      if(i_mnx.eq.0)fit_init = 1.0e30
C-----------------------------------------------------------------------
      
      RETURN
      END
