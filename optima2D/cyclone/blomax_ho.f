      subroutine blomax_ho(jdim,kdim,q,press,vort,turmu,fmu,
     &                     xy,xyj,x,y,rhotmp,snorhalf,uuhalf,snor,uu)  
#include "../include/arcom.inc"
c                                                                       
c                                                                       
      dimension q(jdim,kdim,4),turmu(jdim,kdim),vort(jdim,kdim)         
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension fmu(jdim,kdim),snor(jdim,kdim),uu(jdim,kdim)
      dimension uuhalf(jdim,kdim),rhotmp(jdim,kdim),snorhalf(jdim,kdim)
c                                                                       
      common/worksp/tmo(maxj),tmi(maxj),tas(maxj),work(maxj,27)
c                                                                       
c      common/turout/tauh(maxj), ustar(maxj),                            
c     >              yplus(maxj, maxk), uplus(maxj, maxk)                
c                                                                       
                                                                        
c                                                                       
      data f27 /1.6d0/                                                
      data fk,fkk,ydumf /0.4d0,0.0168d0,1.d0/                           
      data fkleb /0.3d0/                                              
      data fmutm/14.d0/                                              
c                                                                       
      if (numiter.eq.istart) then
        do 2 k=kbegin,kup
        do 2 j=jlow,jup
          turmu(j,k)=0.d0
 2      continue
      endif
c
      kedge = nint(.75d0*dble(kend))
c                                                                       
c  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
c  ******************  turmu comes in with vorticity *****************  
c  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
c
c     compute normal distance snor(k) and total velocity uu(j,k)
      do 120 j=jlow,jup
         snor(j,1)= 0.d0
         uu(j,1)  = sqrt(q(j,1,2)**2+q(j,1,3)**2)/q(j,1,1)
         do 115 k=klow,kup
            uu(j,k)= sqrt(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)
            scis = abs(xy(j,k-1,3)*xy(j,k,3)+xy(j,k-1,4)*xy(j,k,4))        
            scal = 1.d0/sqrt(scis)                                          
            snor(j,k) = snor(j,k-1) + scal
 115     continue
c
c        store snor and uu at half-points
c        -fourth-order at interior
         tmp=1.d0/16.d0
c         do 110 k=klow,kup-1
         do 110 k=klow,kedge
            snorhalf(j,k)= tmp*(-snor(j,k-1) + 
     &                       9.d0*(snor(j,k)+snor(j,k+1)) - snor(j,k+2))
            uuhalf(j,k)  = tmp*(-uu(j,k-1) + 
     &                       9.d0*(uu(j,k)+uu(j,k+1)) - uu(j,k+2))
 110     continue
c        -snor and uu at boundaries
c        -third-order
         k=1
         tmp=1.d0/8.d0
         snorhalf(j,k)=tmp*(3.d0*snor(j,k)+6.d0*snor(j,k+1)-snor(j,k+2))
         uuhalf(j,k)  =tmp*(3.d0*uu(j,k)+6.d0*uu(j,k+1)-uu(j,k+2))
c        -second-order
c         k=kup
c         snorhalf(j,k)= .5d0*(snor(j,k)+snor(j,k+1))
c         uuhalf(j,k)  = .5d0*(uu(j,k)+uu(j,k+1))
 120  continue
c
c     store rho at half-points
      tmp=1.d0/16.d0
c      do 130 k=klow,kup-1
      do 130 k=klow,kedge
      do 130 j=jlow,jup
         rm1=q(j,k-1,1)*xyj(j,k-1)
         r  =q(j,k,1)*xyj(j,k)
         rp1=q(j,k+1,1)*xyj(j,k+1)
         rp2=q(j,k+2,1)*xyj(j,k+2)
c        -fourth-order         
         rhotmp(j,k) = tmp*(-rm1 + 9.d0*(r + rp1) -rp2)
 130  continue
c
c     rho at boundaries
      tmp=1.d0/8.d0
      do 135 j=jlow,jup
         k=1
         r  =q(j,k,1)*xyj(j,k)
         rp1=q(j,k+1,1)*xyj(j,k+1)
         rp2=q(j,k+2,1)*xyj(j,k+2)
c        -third-order
         rhotmp(j,k) = tmp*(3.d0*r + 6.d0*rp1 - rp2)
c        -second-order
c         k=kup
c         rhotmp(j,k) = .5d0*(q(j,k,1)*xyj(j,k)+q(j,k+1,1)*xyj(j,k+1))    
 135  continue
c                                                                       
c     note xyj(j,k) stores transform jacobian by which dep vars are divided
c                                                                       
      ztmp=1.d0/26.d0
      ztmp2=1.d0/8.d0
      do 80 j = jlow,jup                                                
c        find vorticity tas(k) and minimum of uu
         umin=uu(j,1)
         do 11 k = kbegin,kup                                            
            tas(k)     = vort(j,k)                                         
c            turmu(j,k) = 0.d0                                               
            if(uu(j,k) .lt. umin) umin = uu(j,k)            
   11    continue                                                        
c                                                                       
c        compute ra                                                       
c                                                                       
         k=kbegin
c        -third-order extrapolation at k+1/2 node
         wmu = (3.d0*fmu(j,k) + 6.d0*fmu(j,k+1) - fmu(j,k+2))*ztmp2
         tau = abs(tas(k))                                               
         ra = sqrt(re*rhotmp(j,k)*tau/wmu)*ztmp                         
         if (.not.cmesh .and. .not.periodic) then                          
            if(j.lt.jtail1 .or. j.gt.jtail2) ra = 1.d3*ra              
         endif                                                           
c                                                                       
c        compute y*dudy                       
c                                                                       
         ydum = 0.d0                                                      
c         umin = uu(j,1)                                                    
         ydus = 0.d0                                                      
c
         km2 = 1                                                      
         ydum = 1.d-3                                                 
         um = ydum                                                    
         ym = 0.5d0*snor(j,2)                                             
c         ym = snorhalf(j,1)
c
         do 20 k = klow,kedge 
            km1=k-1
c           -use snorhalf(k-1) to be consistant with original cpde which
c            uses snora=.5*(snor(k)+snor(k-1)) ... same for uuhalf(km1)
            snora = snorhalf(j,km1)
            ydu = snora*abs(tas(km1))*(1.d0-exp(-ra*snora))  
            if(ydu.ge.ydum) then
               km2 = k - 1
               ydum = ydu
c               um = 0.5d0*(uu(j,k-1) + uu(j,k))                          
               um = uuhalf(j,km1)
               ym = snora
            endif
 20      continue
c                                                                       
c        interpolate to find ym, ydum, and um                              
c                                                                       
         if(km2 .lt. 2 .or. km2 .gt. kedge-1) go to 22                   
c         ym3 = 0.5d0*(snor(j,km2+1) + snor(j,km2+2))                         
c         ym1 = 0.5d0*(snor (km2-1) + snor (km2))                         
         ym3 = snorhalf(j,km2+1)
         ym1 = snorhalf(j,km2-1)
         ydum1 = ym1*abs(tas(km2-1))*(1.d0 - exp(-ra*ym1))              
         ydum3 = ym3*abs(tas(km2+1))*(1.d0 - exp(-ra*ym3))              
         c2 = ydum - ydum1                                             
         c3 = ydum3 - ydum                                             
         dy1 = ym - ym1                                                
         dy3 = ym3 - ym                                                
         am = (dy3*dy3*c2 + dy1*dy1*c3)/(dy1*dy3*(dy1+dy3))            
         bm = (dy1*c3 - dy3*c2)/(dy1*dy3*(dy1 + dy3))                  
         if(bm .ge. 0.) go to 22                                       
c
         ymm = ym - 0.5d0*am/bm                                        
         ydu  = ydum - 0.25d0*am*am/bm                                 
         if(ydu.lt.ydum .or. ymm.lt.ym1 .or. ymm.gt.ym3) goto 22
c
         ydum = ydu                                               
         ym = ymm                                                 
         if(ym .gt. snor(j,km2+1)) km2 = km2 + 1                    
         if(ym .lt. snor(j,km2)) km2 = km2 - 1                      
         um= ((snor(j,km2+1) - ym)*uu(j,km2) + 
     &      (ym-snor(j,km2))*uu(j,km2+1))/(snor(j,km2+1) - snor(j,km2))
 22      continue
c                                                                       
c        compute outer eddy viscosity                                     
c                                                                       
         do 25 k=kbegin,kedge                                                 
            ffc = fkk*f27*re*rhotmp(j,k)                                       
            tmo(k) = ffc*ym*ydum                                         
            ffcwk = ydumf*ydumf*ffc                                      
            udiff = abs(um - umin)                                       
            if(ydum .gt. udiff*ydumf) tmo(k) = ffcwk*ym*udiff*udiff/ydum 
            fia = fkleb*snorhalf(j,k)/ym                                       
            if(fia .gt. 1.d5) fia = 1.d5                                 
            fi = 1.d0 + 5.5d0*fia**6                                        
            tmo(k) = tmo(k)/fi                                           
            tmo(k) = abs(tmo(k))                                         
 25      continue
c                                                                       
c        compute inner eddy viscosity                                     
c                                                                       
         do 30 k=kbegin,kedge                                                 
            tau = abs(tas(k))                                              
            tmi(k) =rhotmp(j,k)*re*tau*
     &               (fk*snorhalf(j,k)*(1.d0-exp(-ra*snorhalf(j,k))))**2     
            tmi(k) = abs(tmi(k))                                           
 30      continue                                                        
c                                                                       
c        load viscosity coeffs. into array, use inner value until         
c        match point is reached                                           
c                                                                       
         k=1  
 40      turmu(j,k) = tmi(k)
         k=k+1
         do 10 while (k.le.kedge)
           if (tmi(k) .le. tmo(k)) then
             turmu(j,k)=tmi(k)
             k=k+1
           else
             turmu(j,k)=tmo(k)
             kk=k+1
             k=kedge+1
           endif
 10      continue
         do 79 k=kk,kedge
           turmu(j,k)=tmo(k)
 79      continue
c         k = k +1                                                        
c         if( k.gt. kedge) go to 10                                  
c         if( tmi(k) .le. tmo(k)) go to 40                              
c 41      turmu(j,k) = tmo(k)                                           
c         k = k +1                                                      
c         if( k.le. kedge) go to 41                                     
c 10      continue
 80   continue                                                          
c                                                                       
c     transition model turned off for now                              
c     transition not computed but fixed from input in terms of % chord   
c                                                                       
      if(translo.ne.0.0)then                                           
c                                                                       
c        -zero turmu from jtranlo to jtranup                            
c        -for cmesh logic shift jtranlo and jtranup bu jtail1-1         
c                                                                       
         jtlo = jtranlo                                               
         jtup = jtranup                                           
         if(cmesh)then                                                  
           jtlo = jtranlo-jtail1+1                                      
           jtup = jtranup-jtail1+1                                      
         endif                                                          
c                                                                       
         if (ramptran.eq.0.) then
           do 455 k = kbegin,kend
           do 455 j = jtlo,jtup
             turmu(j,k) = 0.d0
 455       continue
c                                                                       
c          -original ramp           
c           thp 4/2/86  put in linear ramp for transition      
           if(jtup-jtlo .gt. 4)then                                   
             do 460 k = kbegin,kend                                  
               turmu(jtlo,k) = 0.5d0*turmu(jtlo-1,k)                
               turmu(jtup,k) = 0.5d0*turmu(jtup+1,k)                
 460         continue
           endif                                                 
c
         elseif (ramptran.gt.0.) then
           include 'ramp.inc'
         endif
      endif                                                             
c                                                                       
      if(.not.bcairf)then                                               
c        -for flat plate set eddy viscosity to zero                
c                                                                       
         do 885 k = kbegin, kend                                        
         do 885 j = jbegin, jtail1                                      
            turmu(j, k) = 0.0                                            
 885     continue                                                       
      endif                                                             
c 
c      j=170
c      do k=kbegin,kup
c         write(94,*) k,turmu(j,k)
c      enddo
      return
      end                                                             
