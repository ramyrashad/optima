c----------------------------------------------------------------------
c     -- initialize indices --
c----------------------------------------------------------------------
c     calling subroutine: optimaMB
c     
      subroutine setgrid( x, y)

      implicit none
c     
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"
c     
      double precision x(maxjk),y(maxjk)
      integer nbs,ii,j,nside1,nside2,i2
c     
c     Notes:  non-periodic meshes only
c     jtail is irrelevent for multi-block
C     Boundary Condition types and Indexing for them.
c     
c     BC type      side                    indices 
c     interface:    
c     1            klow = 2          kbegin = 1
c     2            jlow = 2          jbegin = 1
c     3            kup  = kmax-1     kend   = kmax
c     4            jup  = jmax-1     jend   = jmax
c     
c     outer,wall    1            klow = 3          kbegin = 2
c     or avg        2            jlow = 3          jbegin = 2
c     3            kup  = kmax-2     kend   = kmax-1
c     4            jup  = jmax-2     jend   = jmax-1
c     
c     
c     Control indeces
c     ---------------
c     
c     jmax    ---  total no. pts in xi direction, incl halo 
c     kmax    ---  total no. pts in eta direction, incl halo
c     
c     jbegin  ---  lower value for j calcs on a block 
c     kbegin  ---  lower value for k calcs on a block 
c     
c     jend    ---  upper value for a j calcs on a block 
c     kend    ---  upper value for a k calcs on a block
c     
c     jlow    ---  loop index: value of first interior pt 
c     klow    ---  loop index: value of first interior pt
c     
c     jup     ---  loop index: value of last interior pt  
c     kup     ---  loop index: value of last interior pt 
c     
c     jm      ---  jmax - 1 
c     km      ---  kmax - 1
c     
c     
c     NOTE!!  (S.D.R 30/11/98)
c     -with the addition of arrays nsd1 through nsd4 to manipulate
c     the code so that some routines use more than 1 halo point,
c     the use of jplus and jminus arrays introduces error at
c     points jbegin and jend.  So whereever possible, substitute
c     j+1 for jplus and j-1 for jminus.  Those arrays were really
c     only necessary in the single block version of ARC2D for
c     periodic flows.
c     
c     jplus   ---  index array for j differencing, jplus(j) = j+1  
c     jminus  ---  index array for j differencing, jminus(j) = j-1 
c     except at the ends: jminus(jbegin) = jbegin
c     jplus(jend)    = jend
c     
      if (mg .or. gseq) then
         nbs=nblkstot
      else
         nbs=nblks
      endif

      do 100 ii=1,nbs 
         jmax(ii) = jbmax(ii) + 2*nhalo
         kmax(ii) = kbmax(ii) + 2*nhalo
         jm(ii) = jmax(ii)-1
         km(ii) = kmax(ii)-1 
c     jplus and jminus arrays
         do 25 j = 1, jmax(ii) 
            jplus(j,ii)  = j + 1 
            jminus(j,ii) = j - 1
 25      continue
c     side 1
         if (ibctype(ii,1).eq.5 .and. nhalo.ge.2) then
            nsd1(ii)=nhalo
         else
            nsd1(ii)=0
         endif
c     
         if (ibctype(ii,1).eq.0) then
            kbegin(ii)   = nhalo + 1 - jkhalo(ii,1)
            klow(ii)     = kbegin(ii) + 1
            eta1mesh(ii) = .false.
         else
            kbegin(ii) = nhalo + 1 - jkhalo(ii,1)
            klow(ii)   = kbegin(ii) + 1
            eta1mesh(ii) = .true.  
         endif
c     side 2
         if (ibctype(ii,2).eq.0) then
            jbegin(ii) = nhalo + 1 - jkhalo(ii,2)
            jlow(ii)   = jbegin(ii) + 1
            xi1mesh(ii) = .false.
            nsd2(ii)=nhalo-1
         else
            jbegin(ii) = nhalo + 1 - jkhalo(ii,2)
            jlow(ii)   = jbegin(ii)+1
            xi1mesh(ii) = .true.
            nsd2(ii)=0
         endif
c     side 3
         if (ibctype(ii,3).eq.5 .and. nhalo.ge.2) then
            nsd3(ii)=nhalo
         else
            nsd3(ii)=0
         endif
c     
         if (ibctype(ii,3).eq.0) then
            kend(ii) = kmax(ii) - nhalo + jkhalo(ii,3)
            kup(ii)  = kend(ii) - 1
            eta2mesh(ii) = .false.
         else
            kend(ii) = kmax(ii) - nhalo + jkhalo(ii,3)
            kup(ii)  = kend(ii) - 1
            eta2mesh(ii) = .true. 
         endif
c     side 4
         if (ibctype(ii,4).eq.0) then
            jend(ii) = jmax(ii) - nhalo + jkhalo(ii,4)
            jup(ii)  = jend(ii) - 1
            xi2mesh(ii) = .false.
            nsd4(ii)=nhalo-1
         else
            jend(ii) = jmax(ii) - nhalo + jkhalo(ii,4)
            jup(ii)  = jend(ii) - 1
            xi2mesh(ii) = .true. 
            nsd4(ii)=0
         endif
         jminus(jbegin(ii),ii) = jbegin(ii)
         jplus(jend(ii),ii) = jend(ii)
 100  continue

cmt ~~~ begin2 and end2 parameters (aka "all-the-way"!) ~~~
      DO ii=1,nbs 
c ... side 1 ...
         IF ((ibctype(ii,1).EQ.0).OR.(ibctype(ii,1).EQ.5)) THEN
            kbegin2(ii) = 1
         ELSE
            kbegin2(ii) = kminbnd(ii)
         ENDIF
c ... side 2 ...
         IF ((ibctype(ii,2).EQ.0).OR.(ibctype(ii,2).EQ.5)) THEN
            jbegin2(ii) = 1
         ELSE
            jbegin2(ii) = jminbnd(ii)
         ENDIF
c ... side 3 ...
         IF ((ibctype(ii,3).EQ.0).OR.(ibctype(ii,3).EQ.5)) THEN
            kend2(ii) = kmax(ii)
         ELSE
            kend2(ii) = kmaxbnd(ii)
         ENDIF
c ... side 4 ...
         IF ((ibctype(ii,4).EQ.0).OR.(ibctype(ii,4).EQ.5)) THEN
            jend2(ii) = jmax(ii)
         ELSE
            jend2(ii) = jmaxbnd(ii)
         ENDIF
      ENDDO
cmt ~~~ 

c     -- copy halo points onto neighboring blocks --

      do ii=1,nbs
         do nside1=2,4,2
            i2 = lblkpt(ii,nside1)
            if (i2.ne.0) then
               nside2= lsidept(ii,nside1)
               call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),x(lgptr(ii)),
     &              jbegin2(ii),jend2(ii),kminbnd(ii),kmaxbnd(ii),
     &              jmax(i2),kmax(i2),x(lgptr(i2)),
     &              jbegin2(i2),jend2(i2),kminbnd(i2),kmaxbnd(i2))
               call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),y(lgptr(ii)),
     &              jbegin2(ii),jend2(ii),kminbnd(ii),kmaxbnd(ii),
     &              jmax(i2),kmax(i2),y(lgptr(i2)),
     &              jbegin2(i2),jend2(i2),kminbnd(i2),kmaxbnd(i2))
            endif
         end do
      end do

      do ii=1,nbs
         do nside1=1,3,2
            i2 = lblkpt(ii,nside1)
            if (i2.ne.0 .and. 
     &           (ibctype(ii,nside1).ne.5 .or. nhalo.ge.2)) then
               nside2= lsidept(ii,nside1)
               call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),x(lgptr(ii)),
     &              jminbnd(ii),jmaxbnd(ii),kbegin2(ii),kend2(ii),
     &              jmax(i2),kmax(i2),x(lgptr(i2)),
     &              jminbnd(i2),jmaxbnd(i2),kbegin2(i2),kend2(i2))
               call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),y(lgptr(ii)),
     &              jminbnd(ii),jmaxbnd(ii),kbegin2(ii),kend2(ii),
     &              jmax(i2),kmax(i2),y(lgptr(i2)),
     &              jminbnd(i2),jmaxbnd(i2),kbegin2(i2),kend2(i2))
            endif
         end do
      end do
      return
      end                       !setgrid
