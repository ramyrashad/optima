      subroutine etadif(jdim,kdim,x,y,xy)                               
c                                                                       
#include "../include/arcom.inc"
c                                                                       
      dimension xy(jdim,kdim,4)                                         
      dimension x(jdim,kdim),y(jdim,kdim)                               
c                                                                       
c     xy2 = xeta, xy1 = yeta
c                                                                       
      if (iord.eq.4) then
         tmp=1.d0/12.d0
         tmp2=0.5d0*tmp
         do 11 j=jbegin,jend                                          
            do 10 k=kbegin+2,kend-2                                    
             xy(j,k,2)=tmp*(-x(j,k+2)+8.d0*(x(j,k+1)-x(j,k-1))+x(j,k-2))
             xy(j,k,1)=tmp*(-y(j,k+2)+8.d0*(y(j,k+1)-y(j,k-1))+y(j,k-2))
c            write(98,77) j,k,xy(j,k,2),xy(j,k,1)
10          continue                                                   
c           -second order
c            k = kbegin                                                
c            xy(j,k,2) = ( -3.d0*x(j,k) +4.d0*x(j,k+1) - x(j,k+2))*.5d0 
c            xy(j,k,1) = ( -3.d0*y(j,k) +4.d0*y(j,k+1) -y(j,k+2))*.5d0  
c            k = kend                                                 
c            xy(j,k,2) = ( 3.d0*x(j,k) -4.d0*x(j,k-1) + x(j,k-2))*.5d0 
c            xy(j,k,1) = ( 3.d0*y(j,k) -4.d0*y(j,k-1) + y(j,k-2))*.5d0 
            k = kbegin                                               
cc           -third order
            xy(j,k,2) =2.d0*tmp*(-11.d0*x(j,k) + 18.d0*x(j,k+1) - 
     >                                   9.d0*x(j,k+2) + 2.d0*x(j,k+3))
            xy(j,k,1) =2.d0*tmp*(-11.d0*y(j,k) + 18.d0*y(j,k+1) -
     >                                   9.d0*y(j,k+2) + 2.d0*y(j,k+3)) 
c           -fourth order            
c            xy(j,k,2) =tmp*(-25.d0*x(j,k) + 48.d0*x(j,k+1) - 
c     >                  36.d0*x(j,k+2) + 16.d0*x(j,k+3) - 3.d0*x(j,k+4))
c            xy(j,k,1) =tmp*(-25.d0*y(j,k) + 48.d0*y(j,k+1) -
c     >                  36.d0*y(j,k+2) + 16.d0*y(j,k+3) - 3.d0*y(j,k+4))
c
            k = kbegin+1
c           -second order 
c               xy(j,k,2) = ( x(j,k+1) - x(j,k-1))*.5d0              
c               xy(j,k,1) = ( y(j,k+1) - y(j,k-1))*.5d0                
c            if (j.lt.jtail1 .or. j.gt.jtail2) then
c           -third order
            xy(j,k,2)=2.d0*tmp*(-2.d0*x(j,k-1) - 3.d0*x(j,k) + 
     >                                        6.d0*x(j,k+1) - x(j,k+2)) 
            xy(j,k,1)=2.d0*tmp*(-2.d0*y(j,k-1) - 3.d0*y(j,k) + 
     >                                        6.d0*y(j,k+1) - y(j,k+2))  
c             xy(j,k,2)=tmp*(-5.d0*x(j,k-1) - 2.d0*x(j,k) + 
c     &              6.d0*x(j,k+1) + 2.d0*x(j,k+2) - x(j,k+3))        
c             xy(j,k,1)=tmp*(-5.d0*y(j,k-1) - 2.d0*y(j,k) + 
c     &              6.d0*y(j,k+1) + 2.d0*y(j,k+2) - y(j,k+3))
c            else
c           -fourth order
c            xy(j,k,2)=tmp2*(-6.*x(j,k-1) - 20.*x(j,k) + 36.*x(j,k+1)
c     >                        - 12.*x(j,k+2) + 2.*x(j,k+3))        
c            xy(j,k,1)=tmp2*(-6.*y(j,k-1) - 20.*y(j,k) + 36.*y(j,k+1)
c     >                        - 12.*y(j,k+2) + 2.*y(j,k+3))         
c            endif
c
            k = kend-1                                                  
c           -second order
c               xy(j,k,2) = ( x(j,k+1) - x(j,k-1))*.5d0                  
c               xy(j,k,1) = ( y(j,k+1) - y(j,k-1))*.5d0                  
c           -third order
            xy(j,k,2)=2.d0*tmp*(x(j,k-2) - 6.d0*x(j,k-1)
     >                                    + 3.d0*x(j,k) + 2.d0*x(j,k+1))
            xy(j,k,1)=2.d0*tmp*(y(j,k-2) - 6.d0*y(j,k-1)
     >                                    + 3.d0*y(j,k) + 2.d0*y(j,k+1))
c               xy(j,k,2)=tmp*(x(j,k-3) - 2.d0*x(j,k-2) -
c     &               6.d0*x(j,k-1) + 2.d0*x(j,k) + 5.d0*x(j,k+1))      
c               xy(j,k,1)=tmp*(y(j,k-3) - 2.d0*y(j,k-2) -
c     &               6.d0*y(j,k-1) + 2.d0*y(j,k) + 5.d0*y(j,k+1)) 
c
            k = kend
c           -third order
            xy(j,k,2) =2.d0*tmp*(11.d0*x(j,k) - 18.d0*x(j,k-1) + 
     >                                    9.d0*x(j,k-2) - 2.d0*x(j,k-3))
            xy(j,k,1) =2.d0*tmp*(11.d0*y(j,k) - 18.d0*y(j,k-1) +
     >                                    9.d0*y(j,k-2) - 2.d0*y(j,k-3))
   11    continue
c         k=1
c         do 200 j=jtail1,jtail2
c            xy(j,k,2) = ( -3.d0*x(j,k) +4.d0*x(j,k+1) - x(j,k+2))*.5d0
c            xy(j,k,1) = ( -3.d0*y(j,k) +4.d0*y(j,k+1) - y(j,k+2))*.5d0  
c 200     continue
c         k=2
c         do 201 j=jbegin,jend
c            xy(j,k,2) = ( x(j,k+1) - x(j,k-1))*.5d0                   
c            xy(j,k,1) = ( y(j,k+1) - y(j,k-1))*.5d0                   
c 201     continue
c         
         if (cmesh) then
         do 13 j=jbegin,jtail1
           jj=jend-j+1
           k=kbegin                                               
           xy(jj,k,2)=tmp*(-x(jj,k+2)+8.d0*(x(jj,k+1)-x(j,k+1))
     >                                                     +x(j,k+2))    
           xy(jj,k,1)=tmp*(-y(jj,k+2)+8.d0*(y(jj,k+1)-y(j,k+1))
     >                                                     +y(j,k+2))
           xy(j,k,2)=-xy(jj,k,2)
           xy(j,k,1)=-xy(jj,k,1)
           k=kbegin+1                                               
           xy(j,k,2)=-tmp*(-x(jj,k)+8.d0*(x(j,k-1)-x(j,k+1))+x(j,k+2))  
           xy(j,k,1)=-tmp*(-y(jj,k)+8.d0*(y(j,k-1)-y(j,k+1))+y(j,k+2))
           xy(jj,k,2)=tmp*(-x(jj,k+2)+8.d0*(x(jj,k+1)-x(jj,k-1))+x(j,k)) 
           xy(jj,k,1)=tmp*(-y(jj,k+2)+8.d0*(y(jj,k+1)-y(jj,k-1))+y(j,k))
c          write(98,77) j,k,xy(j,k,2),xy(j,k,1)
 13      continue
         endif
c
c         k=1
c         print *,'j=jtail2'
c         print *,xy(jtail1,k,1),xy(jtail2,k,1)
c         print *,xy(jtail1,k,2),xy(jtail2,k,2)
c         print *,xy(jtail1,k,3),xy(jtail2,k,3)
c         print *,xy(jtail1,k,4),xy(jtail2,k,4)
cc         print *,xyj(jtail1,k),xyj(jtail2,k)
c         print *,'j=jtail2+1'
c         print *,xy(jtail1-1,k,1),xy(jtail2+1,k,1)
c         print *,xy(jtail1-1,k,2),xy(jtail2+1,k,2)
c         print *,xy(jtail1-1,k,3),xy(jtail2+1,k,3)
c         print *,xy(jtail1-1,k,4),xy(jtail2+1,k,4)
cc         print *,xyj(jtail1-1,k),xyj(jtail2+1,k)
c         print *,'j=jtail2+2'
c         print *,xy(jtail1-2,k,1),xy(jtail2+2,k,1)
c         print *,xy(jtail1-2,k,2),xy(jtail2+2,k,2)
c         print *,xy(jtail1-2,k,3),xy(jtail2+2,k,3)
c         print *,xy(jtail1-2,k,4),xy(jtail2+2,k,4)
c         print *,xyj(jtail1-2,k),xyj(jtail2+2,k)
c         print *,'j=jtail2'
c         print *,x(jtail1,k),x(jtail2,k)
c         print *,y(jtail1,k),y(jtail2,k)
c         print *,'j=jtail2+1'
c         print *,x(jtail1-1,k),x(jtail2+1,k)
c         print *,y(jtail1-1,k),y(jtail2+1,k)
c         print *,'j=jtail2+2'
c         print *,x(jtail1-2,k),x(jtail2+2,k)
c         print *,y(jtail1-2,k),y(jtail2+2,k)
      else
         do 21 j=jbegin,jend                                           
            do 20 k=klow,kup                                           
               xy(j,k,2) = ( x(j,k+1) - x(j,k-1))*.5d0                   
               xy(j,k,1) = ( y(j,k+1) - y(j,k-1))*.5d0                   
c              write(98,77) j,k,xy(j,k,2),xy(j,k,1)
20          continue                                                   
            k = kbegin                                                 
            xy(j,k,2) = ( -3.d0*x(j,k) +4.d0*x(j,k+1) - x(j,k+2))*.5d0
            xy(j,k,1) = ( -3.d0*y(j,k) +4.d0*y(j,k+1) - y(j,k+2))*.5d0  
            k = kend                                                   
            xy(j,k,2) = ( 3.d0*x(j,k) -4.d0*x(j,k-1) + x(j,k-2))*.5d0   
            xy(j,k,1) = ( 3.d0*y(j,k) -4.d0*y(j,k-1) + y(j,k-2))*.5d0 
   21    continue                                                     
         if (cmesh) then    
            k = 1                                                    
            do 23 j = 1,jtail1                                         
              jj = jmax-j+1
              xy(j,k,2) = ( x(j,k+1) - x(jj,k+1))*.5d0
              xy(j,k,1) = ( y(j,k+1) - y(jj,k+1))*.5d0
              xy(jj,k,2) = -xy(j,k,2)
              xy(jj,k,1) = -xy(j,k,1)
23          continue                                                   
         endif                                                          
      endif
c
c     for periodic grids with sharp or cusp trailing edges use        
c     first order derivative for eta terms, second order sometimes leads
c     to negative jacobians                                          
      if( periodic )then                                               
         j = 1                                                         
         if( sharp .or. cusp)then                                      
              xy(j,1,2) =  -x(j,1) + x(j,2)                            
              xy(j,1,1) =  -y(j,1) + y(j,2)                            
         endif                                                         
      endif                                                           
c                                                                      
 77   format(2I5,2e25.15)
      return                                                         
      end                                                            
