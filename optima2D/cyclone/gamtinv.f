c ##########################
c ##                      ##
c ##  subroutine gamtinv  ##
c ##                      ##
c ##########################
c
      subroutine gamtinv(jdim,kdim,q,sndsp,s,xyj,xy,ix,iy,uu,precon)  
c
c     This routine multiplies the right hadn side vector by the inverse
c     transfer matrix M,
c     inverse preconditioner, and inverse eigenvalue matrix T_xi.
c
c                       _1      _1      _1
c     ie.  RHS =>   T_xi   Gamma   M_xi    x  RHS.
c
c*******************************************************************   
c  ix = 1, iy = 2  xi  metrics                                          
c  ix = 3, iy = 4  eta metrics                                          
c*******************************************************************   
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),s(jdim,kdim,4),sndsp(jdim,kdim)          
      dimension xy(jdim,kdim,4),xyj(jdim,kdim),precon(jdim,kdim,6)
      dimension uu(jdim,kdim) 
c                                                                       
c     note gami=gamma - 1
c
      do 1000 k=klow,kup
      do 1000 j=jlow,jup
c
         rho=q(j,k,1)*xyj(j,k)
         rhoinv=1.d0/rho
         u=q(j,k,2)/q(j,k,1)
         v=q(j,k,3)/q(j,k,1)
         c2=sndsp(j,k)**2
c
         dkx=xy(j,k,ix)
         dky=xy(j,k,iy)        
c         uk=dkx*u+dky*v
         uk=uu(j,k)
c
         dkb=dsqrt(dkx**2+dky**2)
         dkbinv=1.d0/dkb
         dkx=dkx*dkbinv
         dky=dky*dkbinv
c        
         um=uk-precon(j,k,ix+2)+precon(j,k,iy+2)
         up=uk-precon(j,k,ix+2)-precon(j,k,iy+2)
         uv=uk*dkbinv
c        
         alp=2.d0*precon(j,k,iy+2)
         alp=rhoinv/alp
         beta=precon(j,k,2)*gami
         eps=precon(j,k,1)
         etap=eps*dkb*gami
         etab=eps*dkb*beta
c
         etaps4=etap*s(j,k,4)
         etapu=etap*u
         etapv=etap*v
c
         s1=s(j,k,1)
         s2=s(j,k,2)
         s3=s(j,k,3)
         s4=s(j,k,4)
c
         t1=(beta-c2)*s1 -gami*(u*s2 + v*s3 -s4)
c
         t2=(-u*dky+v*dkx)*s1 + dky*s2 - dkx*s3
c
         t3=(etab - uv*um)*s1 + (-etapu + um*dkx)*s2 +
     &      (-etapv + um*dky)*s3 + etaps4
c
         t4=(etab - uv*up)*s1 + (-etapu + up*dkx)*s2 +
     &      (-etapv + up*dky)*s3 + etaps4
c        
         s(j,k,1)=t1
         s(j,k,2)=t2*rhoinv
         s(j,k,3)=t3*alp
         s(j,k,4)=t4*alp
 1000 continue
      return                                                          
      end                                                             
