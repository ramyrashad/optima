      subroutine filpny(jdim,kdim,q,press,sndsp,coef2,coef4,            
     >                  ett,xy,xyj,turmu,fmu,ds,a,b,c,d,e,dj,ddj,s)       
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),xyj(jdim,kdim),xy(jdim,kdim,4)           
      dimension press(jdim,kdim),sndsp(jdim,kdim),ett(jdim,kdim)        
      dimension turmu(jdim,kdim),fmu(jdim,kdim),s(jdim,kdim,4)        
      dimension ds(jdim,kdim),coef2(jdim,kdim),coef4(jdim,kdim)         
c                                                                       
      dimension a(jdim,kdim,4),b(jdim,kdim,4,4),c(jdim,kdim,4,4) 
      dimension d(jdim,kdim,4,4),e(jdim,kdim,4)                        
c                                                                       
c                                                                       
c                                                                       
c   fill e with flux jacobian                                           
c   penta  note use of c                                                   
      call fillmat (jdim,kdim,q,xy(1,1,3),xy(1,1,4),ett,c)
c                                                                       
c   inviscid flux difference                                            
      do 20 m=1,4                                                       
      do 20 n=1,4                                                       
      do 20 k=klow,kup                                                  
      do 20 j=jlow,jup                                                  
         b(j,k,n,m) = - c(j,k-1,n,m)                                    
         d(j,k,n,m) =   c(j,k+1,n,m)                                    
 20   continue                                                          
      do 21 m=1,4                                                       
      do 21 n=1,4                                                       
      do 21 k=klow,kup                                                  
      do 21 j=jlow,jup                                                  
         c(j,k,n,m) =  0.0                                              
21    continue                                                          
c                                                                       
      if( viscous .and. viseta)                                         
     >call vismaty(jdim,kdim,q,press,xy,xyj,turmu,fmu,a,b,c,d,e,dj,ddj) 
c                                                                       
c  clean out a and e                                                    
c                                                                       
      do 35 n=1,4                                                       
      do 35 k=kbegin,kend                                               
      do 35 j=jbegin,jend                                               
         a(j,k,n) = 0.0                                                 
         e(j,k,n) = 0.0                                                 
 35   continue                                                          
c                                                                       
c                                                                       
c  add implicit dissipation                                             
      call impdissy(jdim,kdim,xyj,coef2,coef4,a,b,c,d,e)                
c
c---- implicit boudary conditions
      if (ibc) then
         if (.not.wake) then
            call imbc(jdim,kdim,q,xy,xyj,c,d,s,press)
            call imfar(jdim,kdim,kend,kend-1,q,xy,
     &                                        xyj,a,b,c,s,press,sndsp)
         else
            call imfar(jdim,kdim,kend,kend-1,q,xy,
     &                                        xyj,a,b,c,s,press,sndsp)
            call imfar(jdim,kdim,kbegin,kbegin+1,q,
     &                                     xy,xyj,e,d,c,s,press,sndsp)
         endif
      endif
c                                                                       
      do 40 m=1,4                                                       
      do 40 n=1,4                                                       
      do 40 k=klow,kup                                                  
      do 40 j=jlow,jup                                                  
         b(j,k,n,m) = b(j,k,n,m)*ds(j,k)                                
         c(j,k,n,m) = c(j,k,n,m)*ds(j,k)                                
         d(j,k,n,m) = d(j,k,n,m)*ds(j,k)                                
 40   continue                                                          
      do 41 n=1,4                                                       
      do 41 k=klow,kup                                                  
      do 41 j=jlow,jup                                                  
         a(j,k,n) = a(j,k,n)*ds(j,k)                                    
         e(j,k,n) = e(j,k,n)*ds(j,k)                                    
 41   continue                                                          
c                                                                       
c                                                                       
c  add identity                                                         
      do 30 n=1,4                                                       
      do 30 k=klow,kup                                                  
      do 30 j=jlow,jup                                                  
         c(j,k,n,n) = c(j,k,n,n) + 1.                                   
 30   continue                                                          
c                                                                       
c                                                                       
      return                                                            
      end                                                               
