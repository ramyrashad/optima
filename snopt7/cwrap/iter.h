typedef struct {

  int num;
  int num_minor;
  int num_objective_function_evals;
  int num_constraint_function_evals;
  int num_superbasics;
  int num_LU_nonzeros;
  int num_BS_columns_swapped;

  double step;
  double merit;
  double feasibility;
  double optimality;
  double penalty;
  double reduced_hessian_condition;

  int flags;

} snMajorIteration;

/* bits for the flag field in snMajorIteration */
#define SN_MJI_FEASIBILE                  (1 <<  0) /* P */
#define SN_MJI_OPTIMAL                    (1 <<  1) /* D */
#define SN_MJI_CENTRAL_DIFF_USED          (1 <<  2) /* c */
#define SN_MJI_VIOLATION_LIMIT_REACHED    (1 <<  3) /* d */
#define SN_MJI_MAJ_STEP_LIMIT_REACHED     (1 <<  4) /* l */
#define SN_MJI_ELASTIC_INFEASIBLE         (1 <<  5) /* i */
#define SN_MJI_HES_UPDATE_EXTRA_FUNC_EVAL (1 <<  6) /* M */
#define SN_MJI_HES_UPDATE_EXTRA_TERM      (1 <<  7) /* m */
#define SN_MJI_HES_RESET                  (1 <<  8) /* R */
#define SN_MJI_HES_RESET_BFGS             (1 <<  9) /* r */
#define SN_MJI_HES_UPDATE_SELF_SCALED     (1 << 10) /* s */
#define SN_MJI_HES_UPDATE_POS_DEF         (1 << 11) /* S */
#define SN_MJI_HES_UNCHANGED              (1 << 12) /* n */
#define SN_MJI_HES_FLUSHED                (1 << 13) /* f */
#define SN_MJI_MINOR_ITER_LIMIT_REACHED   (1 << 14) /* t */
#define SN_MJI_QP_SUBPROB_UNBOUNDED       (1 << 15) /* u */
#define SN_MJI_QP_SUBPROB_WEAK            (1 << 16) /* w */

typedef struct {
  int num;                      /* Itn  */
  int partial_price;            /* pp   */
  int added_superbasic_set;     /* +SBS */
  int removed_superbasic_set;   /* -SBS */
  int removed_basic_set;        /* -BS  */
  int foo_what_to_name_this_guy;/* -B   */ /* ??? */
  int pivot;
  int num_L_nonzeros;
  int num_U_nonzeros;
  int num_compressions;
  int num_infeasibilities;

  double reduced_gradient;      /* dj  */
  double step;
  double sum_infeasiblilities;
  double sum_infeasiblilities_or_objective;

  snMajorIteration* major_iteration;

} snMinorIteration;

#define SN_ITER_BREAK 0
#define SN_ITER_CONT  1

/* prototype for the function that will get called on the completion of
 * every minor iteration and major iteration, respectively.
 * return SN_ITER_BREAK to signal desire to end iterations
 * return SN_ITER_CONT to signal desire to continue iterations
 */
typedef int (*snOnMinorIteration)( snMinorIteration* minor_iter );
typedef int (*snOnMajorIteration)( snMajorIteration* major_iter );
