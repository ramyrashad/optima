************************************************************************
      !-- Program name: prec_test
      !-- Written by: Howard Buckley
      !-- Date: April 2010
      !-- 
      !-- Compare preconditioner 'M' to the flow Jecobian 'A' calulated
      !-- via Frechet derivative
************************************************************************

      subroutine  prec_test(na, jdim, kdim, ndim, indx, iex, qr,
     &     xy, xyj, x, y, precon, coef2, coef4, uu, vv, ccx, ccy, ds,
     &     press, tmet, spect, gam, turmu, fmu, vort, sndsp, sr, bcf,
     &     q, qnow, qold, as, work, rhsnamult, D _hat, exprhs,
     &     a_jk, pa, ipa, jpa)

#ifdef _MPI_VERSION
      use mpi
#endif

      use disscon_vars
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      !-- Declare variables

      integer
     &     jdim, kdim, ndim, n, jk, i, na, indx(jdim,kdim), k, j,
     &     iex(jdim,kdim,4),
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     jpa(jdim*kdim*ndim*ndim*5+1),
     &     ia(jdim*kdim*ndim+2),ipa(jdim*kdim*ndim+2)

      double precision
     &     q(jdim,kdim,ndim), qr(jdim,kdim,ndim), 
     &     vecdiff(jdim,kdim,ndim),  precon(jdim,kdim,6),
     &     Av(na), Mv(na), v(na), s0(jdim,kdim,ndim),
     &     s(jdim,kdim,ndim), sr(jdim,kdim,ndim), v_n, eps, eps1,
     &     xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim), xy(jdim,kdim,4),
     &     coef4(jdim,kdim), coef2(jdim,kdim), ds(jdim,kdim),
     &     uu(jdim,kdim), vv(jdim,kdim), ccx(jdim,kdim), ccy(jdim,kdim),
     &     press(jdim,kdim), tmet(jdim,kdim), spect(jdim,kdim,3),
     &     gam(jdim,kdim,16), turmu(jdim,kdim), fmu(jdim,kdim), 
     &     vort(jdim,kdim), sndsp(jdim,kdim), bcf(jdim,kdim,4),
     &     qold(jdim,kdim,ndim), qnow(jdim,kdim,ndim), 
     &     work(jdim,kdim,4), t, fac, fac_sa,
     &     as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), D_hat(jdim,kdim,ndim,6),
     &     exprhs(jdim,kdim,ndim), a_jk(6,6), rhsnamult, rhsnamult2,
     &     work1(maxjk,39), totime, maxxyj, minxyj, Mv1,
     &     vecdiff_norm_q1,vecdiff_norm_q2, vecdiff_norm_q3,
     &     vecdiff_norm_q4,vecdiff_norm_q5, qdiff(jdim,kdim,ndim)


      !-- Initialize variables
      do n = 1,ndim
         do k = kbegin,kend
            do j = jbegin,jend
               vecdiff(j,k,n) = 0.d0
            end do
         end do
      end do

      vecdiff_norm_q1 = 0.0
      vecdiff_norm_q2 = 0.0
      vecdiff_norm_q3 = 0.0
      vecdiff_norm_q4 = 0.0
      vecdiff_norm_q5 = 0.0


      !-- Create a random perturbation vector 'v'
      call random_seed
      call random_number(v)
      v = -0.25 + 0.5d0*v

      !-- scale random vector by metric jacobian
      do n=1,ndim
        do k=1,kmax
          do j=1,jmax         
            jk = ( indx(j,k)-1 )*ndim + n
            v(jk) = v(jk)/xyj(j,k)
          end do
        end do
      end do

      !-- Calculate perturbation parameter 'eps'
      v_n = 0.d0
      do i = 1,na
        v_n = v_n + v(i)**2
      end do
      v_n = dsqrt(v_n)
      eps  = dsqrt(2.2d-16) / v_n
      !eps = 1e-14
      eps1 = 1.d0/eps

      !-- Perturb current flow variables by eps*v
      Mv1=0.0
      do n = 1,ndim
        do k = 1,kmax
          do j = 1,jmax
            jk = ( indx(j,k)-1 )*ndim + n
            q(j,k,n) = qr(j,k,n) + eps*v(jk)
            qdiff(j,k,n) = eps*v(jk)
          end do
        end do
      end do

      !if (clalpha2) then
      !   alphatemp=alpha
      !   alpha = alpha + eps*v1(na)/1.d2
      !   uinf = fsmach*cos(alpha*pi/180.d0)
      !   vinf = fsmach*sin(alpha*pi/180.d0)
      !end if

      !-- Calculate pressure using perturbed flow variables
      call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
      
      !-- Calculate laminar viscosity using perturbed flow variables
      if (viscous) call fmun(jdim, kdim, q, press, fmu, sndsp)

      !-- Evaluate RHS using perturbed flow variables
      call get_rhs (jdim, kdim, ndim, q, qnow, qold, xy, xyj, x, y, 
     &       precon, coef2, coef4, uu, vv, ccx, ccy, ds, press, tmet, 
     &       spect, gam, turmu, fmu, vort, sndsp, s, s0, work1, D_hat, 
     &       exprhs, a_jk, .false.)

      !-- If dissipation-based continuation is to be used as a
      !-- globalization method for N-K, then add additional 
      !-- dissipation to inviscid flux derivatives on RHS.

      if (dissCon) then
         call addDissx(jdim,kdim,q,s,xyj)
         call addDissy(jdim,kdim,q,s,xyj)       
      end if

      call scale_bc (1, jdim, kdim, ndim, indx, iex, ia, ja, as, ipa,
     &        jpa, as, s, s0, bcf, as, rhsnamult2, .false.)

      !-- RR: debug: Write 'vecdiff' to file in Plot3D format
      call system('rm -rf q-orig.q')
      open(unit=424,file='q-orig.q',status='new',form='unformatted')
      
      totime = 0.0
      write(424) jmaxold,kmax
      write(424) fsmach,alpha,re*fsmach,totime
      write(424) (((sr(j,k,n)*xyj(j,k),j=1,jmaxold) ,k=1,kmax) ,n=1,4)
      write(424) jtail1, numiter, dt2  
      if (itmodel.eq.2) then
        write(424) ((turre(j,k), j=1,jmaxold) ,k=1,kmax)
        write(424) ((turmu(j,k), j=1,jmaxold) ,k=1,kmax)
      endif
      rewind 424

      !-- RR: debug: Write 'vecdiff' to file in Plot3D format
      call system('rm -rf qdiff.q')
      open(unit=425,file='qdiff.q',status='new',form='unformatted')
      
      totime = 0.0
      write(425) jmaxold,kmax
      write(425) fsmach,alpha,re*fsmach,totime
      write(425) (((-eps1*(s(j,k,n)-sr(j,k,n))*xyj(j,k),j=1,jmaxold)
     &,k=1,kmax),n=1,4)
      write(425) jtail1, numiter, dt2  
      if (itmodel.eq.2) then
        write(425) ((turre(j,k), j=1,jmaxold) ,k=1,kmax)
        write(425) ((turmu(j,k), j=1,jmaxold) ,k=1,kmax)
      endif
      rewind 425

      !-- Calculate the matrix-vector product 'Av' using a Frechet 
      !-- derivative
      !-- S  = -R(Q + eps*v)
      !-- SR = -R(Q)
      do n = 1,ndim
        do k = 1,kmax
          do j = 1,jmax
            jk = ( indx(j,k)-1 )*ndim + n
            Av(jk) = -eps1 * (s(j,k,n) - sr(j,k,n)) 
          end do
        end do
      end do

      !-- Calculate the matrix-vector product 'Mv'. AMUX subroutine
      !-- found in SPARSKIT2

      call amux(na, v, Mv, pa, jpa, ipa) 
   
      !-- Take the difference 'Av - Mv' and store in 'vecdiff'
      do n = 1,ndim
        do k = kbegin,kend
          do j = jbegin,jend
            jk = ( indx(j,k)-1)*ndim + n               
            vecdiff(j,k,n) = abs(Av(jk) - Mv(jk))
c$$$            if (n.eq.2.and.vecdiff(j,k,n).gt.1e-1) then
c$$$            !if (n.eq.2)then
c$$$               write(scr_unit,*)'j,k,n',j,k,n
c$$$               write(scr_unit,*)'vecdiff(j,k,n)',vecdiff(j,k,n)
c$$$               write(scr_unit,*)'Av(jk)=',Av(jk)
c$$$               write(scr_unit,*)'Mv(jk)=',Mv(jk)
c$$$               write(scr_unit,*)''
c$$$            end if
            vecdiff_norm_q1 = vecdiff_norm_q1 + vecdiff(j,k,1)**2
            vecdiff_norm_q2 = vecdiff_norm_q2 + vecdiff(j,k,2)**2
            vecdiff_norm_q3 = vecdiff_norm_q3 + vecdiff(j,k,3)**2
            vecdiff_norm_q4 = vecdiff_norm_q4 + vecdiff(j,k,4)**2
            vecdiff_norm_q5 = vecdiff_norm_q5 + vecdiff(j,k,5)**2
          end do
        end do
      end do 
      
      vecdiff_norm_q1 = dsqrt(vecdiff_norm_q1)
      vecdiff_norm_q2 = dsqrt(vecdiff_norm_q2)
      vecdiff_norm_q3 = dsqrt(vecdiff_norm_q3)
      vecdiff_norm_q4 = dsqrt(vecdiff_norm_q4)
      vecdiff_norm_q5 = dsqrt(vecdiff_norm_q5)

      write (*, *) 'RR: hello from prec_test 2'
      

      write(scr_unit,*)'vecdiff_norm_q1=',vecdiff_norm_q1
      write(scr_unit,*)'vecdiff_norm_q2=',vecdiff_norm_q2
      write(scr_unit,*)'vecdiff_norm_q3=',vecdiff_norm_q3
      write(scr_unit,*)'vecdiff_norm_q4=',vecdiff_norm_q4
      write(scr_unit,*)'vecdiff_norm_q5=',vecdiff_norm_q5

      !-- Write 'vecdiff' to file in Plot3D format
      call system('rm -rf vecdiff.q')
      open(unit=324,file='vecdiff.q',status='new',form='unformatted')
      
      totime = 0.0
      write(324) jmaxold,kmax
      write(324) fsmach,alpha,re*fsmach,totime
      write(324) (((vecdiff(j,k,n),j=1,jmaxold) ,k=1,kmax) ,n=1,4)
      write(324) jtail1, numiter, dt2  

      close(324)

      stop
      
      return

      end
      

