snopt-cuter/README   Philip E. Gill and Josh D. Griffin    24 February 2004


===========================================================================
Using the snOpt-CUTEr interface
===========================================================================

1) The SNOPT and CUTEr libraries must be installed before using the
   SNOPT-CUTER interface.

   For SNOPT, the LD_LIBRARY_PATH must define the location of the SNOPT
   libraries. For example, when using bash, the appropriate command is

   % export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SNOPT/lib

   where $SNOPT defines the path to the SNOPT distribution.

   For CUTEr, the following environment variables must be set
   (as prescribed by CUTEr):

     MASTSIF
     SIFDEC
     MYSIFDEC
     CUTER
     MYCUTER

   Furthermore, the following path environment variables should be modified.

   $MYCUTER/bin should be added to PATH.
   $MYCUTER/double/lib should be added to LD_LIBRARY_PATH.

   e.g., when using bash, the appropriate incantations are

   % export PATH=$PATH:$MYCUTER
   % export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MYCUTER/double/lib


2) In the directory snopt-cuter,  configure and make the snopt-cuter
   distribution:

   % ./configure
   % make


3) To run SNOPT on a CUTEr problem, type

   % ./runon-cuter PROB.SIF

   or

   % ./runon-cuter PROB

4) Snopt options may be set by modifying the file 'sncuter.spc' included
   in the distribution.  The options should be set as described in the
   SNOPT documentation.

5) Summary output will be sent to standard output.  A print file will also
   be created with the name 'PROB.out'.

6) To decode a CUTEr problem without running SNOPT, type

   % ./decode-cuter PROB.SIF

   or

   % ./decode-cuter PROB

   The appropriate CUTEr files are generated in the directory snopt-cuter.
