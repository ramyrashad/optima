      integer jmax,kmax,jm,km,jbegin,jend,kbegin,kend
      integer jbegin2,jend2,kbegin2,kend2
      integer jlow,jup,klow,kup,jmaxold,jplus,jminus
      integer nsd1,nsd2,nsd3,nsd4
     
      common/index/                                                     
     1  jmax,       kmax,        jm,          km,                       
     1  jbegin,     jend,        kbegin,      kend,                     
     1  jbegin2,    jend2,       kbegin2,     kend2,                     
     1  jlow,       jup,         klow,        kup,                      
     1  jmaxold,    jplus,       jminus,      nsd1,
     1  nsd2,       nsd3,        nsd4
c
      dimension jmax(maxblk),kmax(maxblk),jm(maxblk),km(maxblk)
      dimension jbegin(maxblk),jend(maxblk),kbegin(maxblk),kend(maxblk)
      dimension jbegin2(maxblk),jend2(maxblk),
     & kbegin2(maxblk),kend2(maxblk)
      dimension jlow(maxblk),jup(maxblk),klow(maxblk),kup(maxblk)
      dimension jmaxold(maxblk),jplus(maxj,maxblk),jminus(maxj,maxblk)
      dimension nsd2(maxblk),nsd4(maxblk)
      dimension nsd1(maxblk),nsd3(maxblk)
c

      integer jminbnd,jmaxbnd,kminbnd,kmaxbnd

      common/boundary/
     1  jminbnd,    jmaxbnd,
     2  kminbnd,    kmaxbnd
c
      dimension jminbnd(maxblk), jmaxbnd(maxblk)
      dimension kminbnd(maxblk), kmaxbnd(maxblk)
c
c     Indices in the common boundary correspond to the indices of 
c     the physical blocks.  Thus they are used for I/O and to apply
c     boundary conditions.
