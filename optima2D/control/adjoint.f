c----------------------------------------------------------------------
c     -- adjoint gradient calculation --
c     
c     Augmented by Chad Oldfield to explicitly include grid
c     sensitivities.  To use this option, set gradient = 4.
c
c     written by: marian nemec
c     date: june 2000
c----------------------------------------------------------------------

      subroutine adjoint (grad, jdim, kdim, ndim, q, cp, xy, xyj, x, y,
     &      xOrig, yOrig, dx, dy, lambda,
     &      fmu, vort, turmu, cp_tar, bap, bcp, bt, bknot, dvs, idv, ia,
     &      ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat, indx,
     &      icol, iex, press, sndsp, tmet, dOdQ, psi, sol, dRdX, rhs,
     &      work1, xys, xyjs, xs, ys, turres, obj0)

#ifdef _MPI_VERSION
      use mpi
#endif

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer jdim, kdim, ndim

      integer idv(nc+mpopt), indx(jdim,kdim), icol(9), its_nk_val
clb
      integer ia(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer iat(jdim*kdim*ndim+2)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)
clb
      integer iatmp(maxjk*nblk+2),  ipatmp(maxjk*nblk+2)
      integer jatmp(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      integer jpatmp(maxjk*nblk2*5+1) 
clb
      integer iex(jdim,kdim,4)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)

      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)
      

c     c     -- finite diff jacobian --
c     integer ia2(maxjk*nblk+1), ja2(maxjk*nblk2*9)
c     double precision as2(maxjk*nblk2*9)
c     integer ic(maxj*maxk*nblk+1), jc(maxj*maxk*nblk2*9)
c     double precision cs(maxj*maxk*nblk2*9)
c     double precision dd(maxj*maxk*nblk)
c     logical vals
c     c     ---------------------------

      double precision q(jdim,kdim,ndim), cp(jdim,kdim), xy(jdim,kdim,4)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision xOrig(jdim,kdim), yOrig(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision lambda(2*jdim*kdim*incr)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)
      double precision press(jdim,kdim),sndsp(jdim,kdim),tmet(jdim,kdim)

      double precision grad(nc+mpopt), cp_tar(jbody,2), bap(jbody,2)
      double precision bcp(nc,2), bt(jbody), bknot(jbsord+nc)
      double precision dvs(nc+mpopt)

      double precision dOdQ(jdim,kdim,ndim), psi(jdim,kdim,ndim)
      double precision dOdQna, psina, dOdQ0(jdim,kdim,ndim)
      double precision obj0
clb
      double precision sol(jdim*kdim*ndim+1), dRdX(jdim,kdim,ndim)
      double precision rhs(jdim*kdim*ndim+1), dRdXna
clb

csi   variables used for storing rhs values for esdirk time marching
      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim)
      double precision a_jk(6,6)
csi

c     -- time vector arrays --
      double precision dtvec(maxj*maxk*nblk)

      double precision xys(jdim,kdim,4), xyjs(jdim,kdim), xs(jdim,kdim)
      double precision ys(jdim,kdim), turres(jdim,kdim) 

c     -- work array from cyclone --
      double precision work1(maxjk,64)

c     -- ILU arrays --
      integer nz
      integer,dimension(:),allocatable :: levs,jw
      double precision alu(iwk)
      integer jlu(iwk),ju(maxjk*nblk)


c     -- gmres work array --
      integer iwk_gmres
      double precision,allocatable :: wk_gmres(:)

      external framux_o2

      logical res_tmp, jac_tmp, prec_tmp, imps

c     -- timing variables --
      real*4 etime, time, t, tarray(2)
      external etime

      logical scalena
      double precision rhsnamult

      character*2 munt

c     Variables for varying finite difference stepsizes in the finite
c     difference calculations of psi * dR/dX and of dJ/dX.
c     (Chad Oldfield)
      logical new_stepsize
      double precision dLdX_fd(ndv), dLdX_a(ndv)
      double precision stepsize
      double precision psi_dRdX, dOdX
      logical do_both, manysteps
      do_both = .false.   ! Set to true to do both the original adjoint
c         calculation and the augmented calculation, and compare the
c         results.
      manysteps = .false.  ! Set to true to try several finite
c         difference step sizes.

c   Comments for variables added L. Billing, July 2006
c   appologies for any confusing explanation - it should be better than
c   no explanation at all.
c
c   size of vectors grad (the gradient of the objective function with 
c   respect to each of the design variables), idv, and dvs (the vector 
c   of the desing variables) have been increased to ensure sufficient
c   space is available for all angles of attack to be used as design 
c   variables.
c
c   dodqna is the derivative of the objective function with respect to 
c   the angle of attack, added as a flow variable for variable angle of 
c   attack problems.
c   psina is the psi variable for the new equation for variable angle of 
c   attack problems.
c   drdxna is the derivative of the new residual equation (cl - cl*) 
c   with respect to the design variables for variable angle of attack 
c   problems.
c   scalena is a variable which controls the scaling of the rhs equation 
c   that was added for variable angle of attack problems.
c   rhsnamult is the resulting scaling that is applied to the rhs 
c   equation.
c   clopt (in arcom.inc) controls whether or not a variable angle of 
c   attack problem is being solved.  Set to TRUE for variable angle of 
c   attack in input file. Default value is FALSE.

      !-- If performing 2 grad calcs for OBJ_FUNC=19 then skip barrier
      !-- on second time around
      if (lcdm_grad.eq.2) goto 5
#ifdef _MPI_VERSION
      call MPI_BARRIER(COMM_CURRENT, ierr) ! syncronize all processors
#endif
 5    continue

      if (lcdm_grad.eq.1) write (scr_unit,6)
#ifdef _MPI_VERSION
      if (lcdm_grad.eq.2) write (scr_unit,7) cltars(rank+1)
#else
      if (lcdm_grad.eq.2) write (scr_unit,7) cltars(1)
#endif
 6    format
     &(/3x,'Evaluate gradient for objective J = Cd')
 7    format
     &(/3x,'Evaluate gradient for lift constraint Cl = ',f7.4)

#ifdef _MPI_VERSION
      write (scr_unit,10),rank
      call flush(6)
10    format 
     &(/3x,'Proc [',i2,'] solving adjoint equation to compute gradient')
#else
      write (scr_unit,10)
      call flush(6)
10    format 
     &(/3x,'Proc [0] solving adjoint equation to compute gradient')
#endif

      !-- Initialize gradient vector

      do i = 1, ndv
        grad(i) = 0.d0
      enddo

      munt = 'in'
      na = jmax*kdim*ndim
      nas = jmax*kdim*ndim*ndim*9
      nap = jmax*kdim*ndim*ndim*5
     
      if (clopt) then
         na=na+1
         nap=nap+1
         nas = jmax*kdim*ndim*(ndim*9+1)+jmax*ndim*3+1
      end if

cmpr  Allocate some of the big arrays dynamically
      iwk_gmres=(na+1)*(IM_GMRES+2)+(IM_GMRES+1)*IM_GMRES
      allocate(levs(iwk),jw(3*maxjk*nblk),wk_gmres(iwk_gmres))

      call flush(6)

      jac_tmp = jac_mat
      prec_tmp = prec_mat

      jac_mat = .true.
      prec_mat = .true.

c     -- put in jacobian --
      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
            q(j,k,n) = q(j,k,n)/xyj(j,k)
          end do
        end do
      end do 

c     -- ensure tmet is zero --
      do k = 1,kend
        do j = 1,jend
          tmet(j,k) = 0.d0
        end do
      end do

c     -- calculate pressure and sound speed --
      call calcps (jdim, kdim, q, press, sndsp, work1, xy, xyj)

c     -- compute generalized distance function --
      if (viscous .and. turbulnt .and. itmodel.eq.2) then
        do k = kbegin,kend
          do j = jbegin,jend
            j_point=j
            if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
            xpt1 = x(j_point,kbegin)
            ypt1 = y(j_point,kbegin)
            smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
          end do
        end do
      end if

c     -- adjoint lhs: dR/dQ --
      imps=.true.
      call get_lhs (jdim, kdim, ndim, indx, icol, iex, ia, ja, as,
     &      ipa, jpa, pa, xy, xyj, x, y, q, press, sndsp, fmu, turmu,
     &      work1(1,1), work1(1,2), work1(1,3), work1(1,4), work1(1,5),
     &      work1(1,6), tmet, work1(1,7), work1(1,13), work1(1,17),
     &      ast, pdc, a_jk,imps)

c     -- transpose jacobian array --
      call csrcsc (na, 1, 1, as, ja, ia, ast, jat, iat)
      if (icg.eq.0) write (opt_unit,20) ia(na+1) - ia(1)
 20   format (3x,'Non-zeros in Jacobian matrix:',i8)
      
c     -- transpose preconditioner array --
      call csrcsc (na, 1, 1, pa, jpa, ipa, pat, jpt, ipt)
      if (icg.eq.0) write (opt_unit,30) ipa(na+1) - ipa(1)
 30   format (3x,'Non-zeros in preconditioner:',i8)

c     -- take out jacobian --      
      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
            q(j,k,n) = q(j,k,n)*xyj(j,k)
          end do
        end do
      end do

c     -- form adjoint rhs: dObj/dQ --
c     -- note for adjoint: do not row shuffle dObj/dQ  --
c     -- pressure has no jacobian -- 
      call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
      if (gradient == 4) then
        call get_dJdQ(jdim, kdim, ndim, Q, cp, cp_tar, xy, xyj, x, y,
     |      dOdQ, obj0)
      else
        call dOBJdQ (jdim, kdim, ndim, x, y, xy, xyj, q, dOdQ, cp_tar,
     &        cp, press, obj0)
      endif
      if (clopt) then
         call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
         call dOBJdQna (jdim, kdim, ndim, x, y, xy, xyj, q, dOdQna,
     &        cp_tar,cp, press, obj0)
      end if

c     -- diagonal scaling for all b.c. equations --
c     -- this operation is performed after taking the transpose,
c     special case for the adjoint problem. --
      scalena = .true.

      call scale_bc (2, jdim, kdim, ndim, indx, iex, iat, jat, ast,
     &    ipt, jpt, pat, dOdQ, dOdQO, work1(1,1), work1(1,5), rhsnamult,
     &    scalena)

c     -- ILU preconditioner --
      call prec_ilu (jdim, kdim, ndim, ilu_meth, lfil, opt_unit, ipt,
     &     jpt, pat, alu, jlu, ju, levs, jw, work1) 
      
c     -- gmres --
      ipar(1) = 0
      ipar(2) = 2
      ipar(3) = 1
      ipar(4) = iwk_gmres
      ipar(5) = im_gmres
      ipar(6) = iter_gmres

c     -- gmres convergence tolerance --
      fpar(1) = eps_gmres
      fpar(2) = 1.d-14
      fpar(11) = 0.0

      write (scr_unit,55) ipar(5)
      call flush(6)
 55   format (/3x,'Starting GMRES(',i2,') ...')

      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
            jk = ( indx(j,k) - 1 )*ndim + n
            rhs(jk) = dOdQ(j,k,n)
clb   Dividing by initial objective function value to normalize J
          end do
        end do
      end do

      if (clopt) rhs(na) = dOdQna*1.d2*rhsnamult
clb   Dividing by initial objective function value to normalize J


      ierr = 0
      t=etime(tarray)
      time=tarray(1)
      call run_gmres (jdim, kdim, ndim,indx,iex,q,qp,qold,xy,xyj,x,y,
     &      work1(1,1), work1(1,7), work1(1,8), work1(1,9),
     &      work1(1,10), work1(1,11), work1(1,12), work1(1,13), press,
     &      tmet, work1(1,14), work1(1,17), work1(1,33), work1(1,34),
     &      work1(1,35), sndsp, work1(1,36), work1(1,40), 0, rhs, sol,
     &      ast, jat, iat, alu, jlu, ju, work1(1,44), work1(1,49),
     &      wk_gmres,work1(1,54),0,gmres_unit,framux_o1,rhsnamult,D_hat,
     &      exprhs, a_jk, its_nk_val,dtvec)

      t=etime(tarray)
      time=tarray(1)-time

      !-- If performing 2 grad calcs for OBJ_FUNC=19 then skip barrier
      !-- on second time around
      if (lcdm_grad.eq.2) goto 59
#ifdef _MPI_VERSION
      call MPI_BARRIER(COMM_CURRENT, ierr) ! syncronize all processors
#endif
 59   continue
      write(scr_unit,60) im_gmres, ipar(1), time
      write(scr_unit,61) ipar(7), fpar(5)
      write (opt_unit,60) im_gmres, ipar(1), time
      write (opt_unit,61) ipar(7), fpar(5)

 60   format (3x,'GMRES(',i2,') return code:',i4,2x,'Run-time:',
     &      f7.2,'s')
 61   format (3x,'Number of inner iterations',i5,' and residual',e12.4)

      if ( ipar(1).lt.0 ) then 
        write (scr_unit,*) '!!ADJOINT: gmres convergence problems!!'
c        stop
      end if

      do n = 1,ndim
        do k = 1,kend
          do j = 1,jend
            jk = ( indx(j,k) - 1 )*ndim + n
            dOdQ(j,k,n) = sol(jk)
          end do
        end do
      end do
      if (clopt) psina = sol(na)
      
c     -- unshuffle adjoint vector --
      do n = 1,4
        do k = 1,kend
          do j = 1,jend
            psi(j,k,iex(j,k,n)) = dOdQ(j,k,n)
          end do
        end do
      end do

      if (viscous .and. turbulnt .and. itmodel .eq.2) then
        n = 5 
        do k = 1,kend
          do j = 1,jend
            psi(j,k,n) = dOdQ(j,k,n)
          end do
        end do
      end if

c     -- for viscous flow, zero out the psi vector at the boundary for
c     the momentum equation --
      if (viscous) then
        k = 1
        do n = 2,3
          do j = jtail1,jtail2
            psi(j,k,n) = 0.d0
          end do
        end do 
        if (turbulnt .and. itmodel .eq.2) then
          do j = jtail1,jtail2
            psi(j,k,5) = 0.d0
          end do
        end if
      end if

c     -- store grid and turre --
c     -- storing grid and metrics is done for speed --
c     -- storing turre is necessary to ensure that the S-A model
c     restarts from the same turre state --
      do n = 1,4
        do k=kbegin,kend
          do j=jbegin,jend
            xys(j,k,n) = xy(j,k,n)
          end do
        end do
      end do
      
      do k=kbegin,kend
        do j=jbegin,jend
          xs(j,k) = x(j,k)
          ys(j,k) = y(j,k)
          xyjs(j,k) = xyj(j,k)
        end do
      end do

      if ((gradient == 4) .or. do_both) then
cCO     Analytically Compute the dL/dX, the derivative of the Lagrangian,
cCO     L, with respect to the design variables, X.
cCO     (dL/dX = the objective function gradient, for those who use the
cCO     chain rule to derive adjoint equations)
        call get_dLdX (jdim, kdim, ndim, q, press, sndsp,
     |      fmu, turmu, xy, xyj, psi, psina, x, y, xOrig, yOrig, dx, dy,
     |      lambda, bt, bknot, idv, dLdX_a, obj0, 1.d0, .false., .true.)
      endif
      if ((gradient .ne. 4) .or. do_both) then
cCO     Compute dL/dX using finite differences
c       -- calculate the sensitivities of the flow residuals --
        do in = 1, ndv

c       -- sol and rhs used as work arrays --
          if (manysteps) then
c           Tell new_stepsize to begin trying several step sizes.
            write(scr_unit,*) 'Finite differencing dR/dX: in = ', in
            stepsize = 0.d0
          else
c           Just use default stepsize selection, and run the finite
c           difference once.
            stepsize = -1.d0
          endif
          do while (new_stepsize(psi_dRdX, stepsize, 1.d-2, 1.d-9))
            call dResdX( in, jdim, kdim, ndim, q, xy,xyj,x,y,bap,bcp,
     &          bt, bknot, dvs, idv, dRdX, sol, rhs, work1(1,1),
     &          work1(1,7),  work1(1,8),  work1(1,9),  work1(1,10),
     &          work1(1,11), work1(1,12), work1(1,13), work1(1,14),
     &          tmet, work1(1,15), work1(1,18), turmu, fmu, vort,
     &          work1(1,34), work1(1,35), xys, xyjs, xs, ys, turres,
     &          xOrig, yOrig, dx, dy, stepsize, dRdXna)

c         -- product of adjoint variable with residual sensitivity -- 
            psi_dRdX = 0.d0
            do n = 1,ndim
              do k = 1,kend
                do j = 1,jend
                  psi_dRdX = psi_dRdX + dRdX(j,k,n)*psi(j,k,n)
                end do
              end do
            end do

            if (clopt) psi_dRdX = psi_dRdX + dRdXna*psina
 
          enddo

c         Add dJ/dX (with Q held constant)
          call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
c          write(scr_unit,*)'returned from calcps'
          if (manysteps) then
c           Tell new_stepsize to begin trying several step sizes.
            write(scr_unit,*) 'Finite differencing dJ/dX: in = ', in
            stepsize = 0.d0
          else
c           Just use default stepsize selection, and run the finite
c           difference once.
            stepsize = -1.d0
          endif

          do while (new_stepsize(dOdX, stepsize, 1.d-1, 1.d-8))

            call dObjdX(in,dOdX,jdim,kdim,dvs,idv,x,y,bap,bcp,
     &            bt, bknot, q, press, xy, xyj, xys, xyjs, xs, ys,
     |            xOrig, yOrig, dx, dy, stepsize, obj0)
          end do

          dLdX_fd(in) = psi_dRdX + dOdX

        end do

        !-- If performing 2 grad calcs for OBJ_FUNC=19, skip barrier
        !-- on second time around
        if (lcdm_grad.eq.2) goto 159
#ifdef _MPI_VERSION
        call MPI_BARRIER(COMM_CURRENT, ierr) 
#endif
 159    continue
      endif

      if (do_both) then
cCO     Write the comparison of finite difference and analytic
cCO     computations of dL/dX to the screen.
        write (scr_unit,*) 
     &      'Comparision of analytic and finite difference',
     |      ' calculation of dL/dX:'
        write (scr_unit,*) '      FD                 analytic',
     |      '            rel dif'
        do in = 1, ndv
          write (scr_unit,'(1X, I3, 2D19.10, 2D12.2)') in, dLdX_fd(in),
     |        dLdX_a(in), (dLdX_a(in)-dLdX_fd(in))/dLdX_fd(in)
        end do
      endif

cCO   Choose either the analytic or the finite difference dLdX
      if (gradient == 4) then
        do in = 1, ndv
          grad(in) = dLdX_a(in)
        enddo
      else
        do in = 1, ndv
          grad(in) = dLdX_fd(in)
        enddo
      end if

      call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

      jac_mat = jac_tmp
      prec_mat = prec_tmp

      call flush(opt_unit)

cmpr  Deallocate the big arrays
      deallocate(levs,jw,wk_gmres)

      return 
      end                       !adjoint

c     -- finite difference jacobian --
c
c      call csrcsc (na, 1, 1, ast, jat, iat, as, ja, ia)
c
c      call fd_drdq (jdim, kdim, ndim, indx, q, press, sndsp, xy,
c     &      xyj, fmu, x, y, vort, tmet, turmu, ia2, ja2, as2, work1) 
c
cc      call csrcsc (na, 1, 1, as2, ja2, ia2, ast, jat, iat)
c
c 200  format (1h ,34(1h-),' row',i5,1x,34(1h-),/
c     *      3('  columns :     values   * ') )
cc     -- xiiiiiihhhhhhdddddddddddd-*- --
c 201  format(3(1h ,i6,5h   : ,D12.5,3h * ) )
c
c      nzmax = maxj*maxk*nblk2*9
c      call aplsb (na,na,as,ja,ia,-1.d0,as2,ja2,ia2,cs,jc,ic,nzmax,ierr)
c
c      write (scr_unit,*) 'IERR',ierr
c
c      call retmx (na,cs,jc,ic,dd)
c
c      n = 0
c      vmax = 0.d0
c      do j=1,na
c        if (dabs(dd(j)).gt.1.d-4) then
c          do nn = 1,ndim
c            do kk = 2,kup
c              do jj = 2,jup
c                if ( (( indx(jj,kk) - 1 )*ndim + nn).eq.j ) then
c                  write (scr_unit,333) jj,kk,nn,j,dd(j)
c                  if (dabs(dd(j)).gt.vmax) then
c                    vmax=dd(j)
c                    jvmax = jj
c                    kvmax = kk
c                  end if
c                  goto 222
c                end if
c              end do
c            end do
c          end do
c        end if
c 222    continue
c      end do
c 333  format (4i8,e12.3)
c
c      i1 = ( indx(jvmax,kvmax) - 1 )*ndim + 1
c      i2 = ( indx(jvmax,kvmax) - 1 )*ndim + ndim
c      
c      write (scr_unit,*) 'node jj kk',jvmax,kvmax,vmax
c
c      do i=i1, i2
c        write(87,200) i
c        k1=ia(i)
c        k2 = ia(i+1)-1
c          write (87,201) (ja(k),as(k),k=k1,k2)
c      end do
c
c      do i=i1, i2
c        write(86,200) i
c        k1=ia2(i)
c        k2 = ia2(i+1)-1
c          write (86,201) (ja2(k),as2(k),k=k1,k2)
c      end do
c
c      stop

c$$$c---- tplot ------------------------------------------------------------
c$$$c Writes data to a tecplot file t.plt, for generating contour plots.
c$$$c The data to be written includes x- and y- values at each node as
c$$$c computed using finite differences, and as computed using analytically.
c$$$c
c$$$c Requries:
c$$$c jdim, kdim; Dimensions of the grid
c$$$c x, y: x- and y- coordinates of the grid node locations
c$$$c fd_x, fd_y: Values computed at each grid node, in the x- and
c$$$c   y-directions, using finite differences
c$$$c a_x, a_y: Values computed at each grid node, in the x- and
c$$$c   y-directions, using an analytic technique
c$$$c
c$$$c Chad Oldfield, November 2005
c$$$c-----------------------------------------------------------------------
c$$$      subroutine tplot(jdim, kdim, x, y, fd_x, fd_y)
c$$$      implicit none
c$$$
c$$$c     Argument data types (See above for descriptions)
c$$$      integer jdim, kdim
c$$$      double precision x(jdim,kdim), y(jdim,kdim)
c$$$      double precision fd_x(jdim,kdim), fd_y(jdim,kdim)
c$$$
c$$$c     Variables for tecplot routines
c$$$      character*1 NULLCHR
c$$$      Integer*4 Debug,III,NPts,NElm
c$$$      Integer*4 TecIni,TecDat,TecZne,TecNod,TecFil,TecEnd
c$$$      Integer*4 VIsDouble
c$$$      integer imax, jmax, kmax, i, j, k
c$$$      NULLCHR = CHAR(0)
c$$$      Debug = 1
c$$$      VIsDouble = 1  ! Data is double precision (0 for single precision)
c$$$      IMax = jdim
c$$$      JMax = kdim
c$$$      KMax = 1
c$$$
c$$$c     Open the file and write the Tecplot data file header information.
c$$$      I = TecIni('SIMPLE DATASET'//NULLCHR,
c$$$     & 'x y lambda_x lambda_y'//NULLCHR,
c$$$     & 't.plt'//NULLCHR,
c$$$     & '.'//NULLCHR,
c$$$     & Debug,
c$$$     & VIsDouble)
c$$$
c$$$c     Write the zone header information.
c$$$      I = TecZne('Lambda'//NULLCHR,
c$$$     & IMax,
c$$$     & JMax,
c$$$     & KMax,
c$$$     & 'BLOCK'//NULLCHR,
c$$$     & CHAR(0))
c$$$
c$$$c     Write out the field data.
c$$$      III = IMax*JMax
c$$$      I = TecDat(III,X,VIsDouble)
c$$$      I = TecDat(III,Y,VIsDouble)
c$$$      I = TecDat(III,fd_X,VIsDouble)
c$$$      I = TecDat(III,fd_Y,VIsDouble)
c$$$      I = TecEnd()
c$$$
c$$$      End
c$$$c---- tplot ------------------------------------------------------------
