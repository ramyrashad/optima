c----------------------------------------------------------------------c
c----       subroutine to calculate the forcing function           ----c
c----------------------------------------------------------------------c
c----  calling routine:  multigrid.f
c
      subroutine mk_pk(lvl,mglev,npts2,
     &                jdim,kdim,q,q0,pk,s,press,
     &                sndsp,turmu,fmu,vort,turre,vk,ve,
     &                x,y,xy,xyj,xit,ett,ds,
     &                uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &                spectxi,specteta,precon,tmp)
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
c
      integer fblk,cblk
      dimension q(jdim*kdim*4),    q0(jdim*kdim*4)
      dimension press(jdim*kdim),  sndsp(jdim*kdim)
      dimension s(jdim*kdim*4),    xy(jdim*kdim*4),   xyj(jdim*kdim)
      dimension xit(jdim*kdim),    ett(jdim*kdim),    ds(jdim*kdim)
      dimension x(jdim*kdim),      y(jdim*kdim),      turmu(jdim*kdim)
      dimension vort(jdim*kdim),   turre(jdim*kdim),  fmu(jdim*kdim)
      dimension vk(jdim*kdim),     ve(jdim*kdim),     pk(jdim*kdim*4)
      dimension spectxi(jdim*kdim*3),  specteta(jdim*kdim*3)
      dimension precon(jdim*kdim*8),   tmp(jdim*kdim*4)
c
      dimension uu(jdim*kdim),    ccx(jdim*kdim)
      dimension vv(jdim*kdim),    ccy(jdim*kdim)
      dimension coef2x(jdim*kdim),coef2y(jdim*kdim)
      dimension coef4x(jdim*kdim),coef4y(jdim*kdim)
c
c
c     -set the residual of the fine grid to zero
      call set_level(lvl)
      do ii=nblkstart,nblkend
        call resets(jmax(ii),kmax(ii),s(lqptr(ii)))
      end do

c     -update pressure of fine grid
      do ii=nblkstart,nblkend
        call calcps(ii,jmax(ii),kmax(ii),q(lqptr(ii)),
     &        press(lgptr(ii)),sndsp(lgptr(ii)),
     &        precon(lprecptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &        jlow(ii)-nsd2(ii),jup(ii)+nsd4(ii),
     &        klow(ii)-nsd1(ii),kup(ii)+nsd3(ii))
      enddo
c
c     -form rhs on fine grid using q (fine grid solution)
      call rhs(lvl,mglev,npts2,
     &      jdim,kdim,q,pk,s,press,
     &      sndsp,turmu,fmu,vort,turre,vk,ve,
     &      x,y,xy,xyj,xit,ett,ds,
     &      uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &      spectxi,specteta,precon,tmp)
c     
c
c     -add pk contribution to fine
      do ii = nblkstart,nblkend
        call add_fine_pk(jmax(ii),kmax(ii),s(lqptr(ii)),pk(lqptr(ii)))
      enddo
c
c     -restrict fine residual
      do iblk = nblkstart,nblkend
         fblk = iblk
         cblk = iblk + nblks
         call rf2rc (fblk,cblk,
     &           jmax(fblk),kmax(fblk),s(lqptr(fblk)),xyj(lgptr(fblk)),
     &           jmax(cblk),kmax(cblk),s(lqptr(cblk)),xyj(lgptr(cblk)))
      enddo

c     -set the blocks of the coarse level
      call set_level(lvl+1)
c
c     -update pressure of coarse grid
      do ii=nblkstart,nblkend
        call calcps(ii,jmax(ii),kmax(ii),q0(lqptr(ii)),
     &        press(lgptr(ii)),sndsp(lgptr(ii)),
     &        precon(lprecptr(ii)),xy(lqptr(ii)),xyj(lgptr(ii)),
     &        jlow(ii)-nsd2(ii),jup(ii)+nsd4(ii),
     &        klow(ii)-nsd1(ii),kup(ii)+nsd3(ii))
      enddo
c
c     -form rhs on coarse grid using q0 
      call rhs(lvl+1,mglev,npts2,
     &      jdim,kdim,q0,pk,s,press,
     &      sndsp,turmu,fmu,vort,turre,vk,ve,
     &      x,y,xy,xyj,xit,ett,ds,
     &      uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &      spectxi,specteta,precon,tmp)
c
c
c     -form pk for the coarse grid
      do ii=nblkstart,nblkend
        call form_pk (jmax(ii),kmax(ii),pk(lqptr(ii)),s(lqptr(ii)))
      enddo
c
c
c     Note: Stan 
c     -no need to do this here, all we really need is to set all
c      halo points to zero.  All interior points will be defined
c      in mk_qp.f when the residual from the coarser level is prolonged.
c     -so call resets2.f in mk_qp.f, its cheaper too.
c
c     -set the residual of the fine grid to zero before leaving
c      call set_level(lvl)
c      do ii=nblkstart,nblkend
c        call resets(jmax(ii),kmax(ii),s(lqptr(ii)))
c      end do
c
      return
      end
