C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C                           BBBB   III  N   N                          C
C                           B   B   I   NN  N                          C
C                           BBBB    I   N N N                          C
C                           B   B   I   N  NN                          C
C                           BBBB   III  N   N                          C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE BIN(ndim,ndum,NACC,NOBFUN,FITACC,NGENE,GENES,
     1               NCHROM,ZZZ,ZZZFIT,zzzrank)
      real*8 rand
      SAVE
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** THIS SUBROUTINE PERFORMS THE SELECTION OPERATION        C
C              USING A BIN SELECTION ALGROITHM ***                     C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      DIMENSION         FITACC(nobfun,ndim)
      DIMENSION         GENES(ngene,ndim)
      DIMENSION         ZZZ(ngene,nchrom)
      DIMENSION         ZZZFIT(nobfun,nchrom),zzzrank(nchrom)
      DIMENSION         S(ndim)
      DIMENSION         IBIN(100),                IBINBD(100)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** 
C-----------------------------------------------------------------------
C          *** SET INITIAL PARAMETERS ***
C
      NBIN = ABS(ndum)
C
C-----------------------------------------------------------------------
C          *** COMPUTE SQUARE OF THE ARC-LENGTH ALONG 
C              PARETO FRONT ***
C
      S(1) = 0.0
      DO NC=2,NACC
         S(NC) = S(NC-1)+SQRT((FITACC(1,NC)-FITACC(1,NC-1))**2
     1         +              (FITACC(2,NC)-FITACC(2,NC-1))**2)
C         S(NC) = S(NC-1)+(FITACC(1,NC)-FITACC(1,NC-1))**2
C     1         +         (FITACC(2,NC)-FITACC(2,NC-1))**2
C         S(NC) = S(NC-1)+((FITACC(1,NC)-FITACC(1,NC-1))**2
C     1         +          (FITACC(2,NC)-FITACC(2,NC-1))**2)**2
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (30,230) NC,FITACC(1,NC),FITACC(2,NC),S(NC)
C  230 FORMAT ('BIN:NC,FITACC1,FITACC2,S=',I5,3F12.6)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ENDDO
      SMAX = S(NACC)
C
C-----------------------------------------------------------------------
C          *** SETUP SBIN AND IBIN PARAMETERS. NOTE THAT 
C              IBIN GOES FROM 1 TO NBIN ***
C              SBIN...REAL PARAMETER CONTAINING THE ARC-LENGTH
C                     USED TO ESTABLISH EACH BIN BOUNDARY
C              IBIN...INTEGER ARRAY CONTAINING THE DESIRED
C                     NUMBER OF ENTRIES TAKEN FROM EACH BIN
C
      SBIN  = SMAX/FLOAT(NBIN)
      NITER = NCHROM-NOBFUN
      IF (ndum.LT.0) NITER = NCHROM
C
      DO NB=1,NBIN
         IBIN(NB) = NITER/NBIN
      ENDDO
C
      ISUM = 0
      DO NB=1,NBIN
         ISUM = ISUM+IBIN(NB)
      ENDDO
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      DO NB=1,NBIN
C      WRITE (30,241) NB,IBIN(NB)
C  241 FORMAT ('BIN1:NB,IBIN=',2I5)
C      ENDDO
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO NB=1,NBIN
         IF (ISUM.LT.NITER) THEN
            CALL RAN3(NB,RAND)
            IR1 = NBIN*RAND+1
            IBIN(IR1) = IBIN(IR1)+1
            ISUM      = ISUM+1
         ENDIF
      ENDDO
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      DO NB=1,NBIN
C      WRITE (30,251) NB,IBIN(NB)
C  251 FORMAT ('BIN2:NB,IBIN=',2I5)
C      ENDDO
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C          *** COMPUTE INTEGER INDICES ASSOCIATED WITH THE BIN
C              BOUNDARIES. IF ndum--THE NUMBER OF BINS--IS 
C              POSTIVE, EXCLUDE THE ACCUMULATION FILE
C              ENDPOINTS. IF ndum IS NEGATIVE, INCLUDE THE
C              ACCUMULATION ENDPOINTS ***
C
      IF (ndum.LT.0) THEN
         IBINBD(1) = 1
         NB        = 2
         DO NC=2,NACC
            IF (S(NC).GE.SBIN*FLOAT(NB-1)) THEN
               IBINBD(NB) = NC
               NB         = NB+1
            ENDIF
         ENDDO
      ENDIF
C
      IF (ndum.GT.0) THEN
         IBINBD(1) = 2
         NB        = 2
         DO NC=2,NACC
            IF (S(NC).GE.SBIN*FLOAT(NB-1)) THEN
               IBINBD(NB) = NC
               NB         = NB+1
            ENDIF
         ENDDO
         IBINBD(NBIN+1) = NACC-1
      ENDIF
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (30,220) NBIN,NACC,NITER,SMAX
C  220 FORMAT ('BIN:NBIN,NACC,NITER,SMAX=',3I5,F12.6)
C      NBINP = NBIN+1
C      DO NB=1,NBINP
C      WRITE (30,221) NB,SBIN,IBIN(NB),IBINBD(NB)
C  221 FORMAT ('BIN:NB,SBIN,IBIN,IBINBD=',I4,F12.6,2I5)
C      ENDDO
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C          *** PERFORM THE SELECTION. NEWLY SELECTED GENES GO 
C              INTO ZZZ AND FITNESS VALUES GO INTO ZZZFIT ***
C
         ICOUNT = 0
         IF (ndum.GT.0) ICOUNT = 2
         DO NB=1,NBIN
            IB = IBIN(NB)
            DO NI=1,IB
               ICOUNT = ICOUNT+1
               CALL RAN3(ICOUNT,RAND)
               IR1 = (IBINBD(NB+1)-IBINBD(NB)+1)*RAND+IBINBD(NB)
CCCCCCCCCCCCCC DEBUG OPTION CCCCC
C      IF (ICOUNT.EQ.19) IR1 = 1
C      IF (ICOUNT.EQ.20) IR1 = NACC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
               DO NG=1,NGENE
                  ZZZ(NG,ICOUNT) = GENES(NG,IR1)
               ENDDO
               DO NO=1,NOBFUN
                  ZZZFIT(NO,ICOUNT) = FITACC(NO,IR1)
               ENDDO
               zzzrank(icount) = 1
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (30,222) NB,NI,ICOUNT,IBINBD(NB+1),IBINBD(NB),IR1
C  222 FORMAT ('BIN:NB,NI,ICOUNT,IBINBD(NB+1),IBINBD(NB),IR1=',6I5)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
            ENDDO
         ENDDO
CCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      DO NC=1,NCHROM
C      WRITE (30,721) NC,ZZZ(1,NC),ZZZ(2,NC)
C  721 FORMAT ('BIN1:NC,ZZZ1/2=',I6,2F24.17)
C      ENDDO
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C          *** FILL IN THE FIRST NOBFUN ENTRIES OF ZZZ AND ZZZFIT
C              IF ndum>0. THIS MAINTAINS THE ACCUMULATION FILE
C              ENDPOINTS. ***
C
      IF (NDUM.GT.0) THEN
         DO NG=1,NGENE
            ZZZ(NG,1) = GENES(NG,1)
         ENDDO
         DO NO=1,NOBFUN
            ZZZFIT(NO,1) = FITACC(NO,1)
         ENDDO
         zzzrank(1) = 1
         DO NG=1,NGENE
            ZZZ(NG,2) = GENES(NG,NACC)
         ENDDO
         DO NO=1,NOBFUN
            ZZZFIT(NO,2) = FITACC(NO,NACC)
         ENDDO
         zzzrank(2) = 1
      ENDIF

CCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      DO NC=1,NCHROM
C      WRITE (30,741) NC,ZZZ(1,NC),ZZZ(2,NC)
C  741 FORMAT ('BIN2:NC,ZZZ1/2=',I6,2F24.17)
C      ENDDO
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C           *** SELECTION COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END

