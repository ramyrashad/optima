// This is a simple class for keeping track of the CPU time spent running 
// different parts of a program.
//
// Chad Oldfield, Novemeber 2005
//
// Modified by Anh Truong, October 2006

#ifndef STOPW_H
#include "stopwatch.h"
#endif


Stopwatch::Stopwatch()
{
  seconds = 1.0/((double) CLOCKS_PER_SEC);
  start = previous = clock();
}

double Stopwatch::Interval()
{
    // Time since last time the stopwatch was accessed
      clock_t temp(previous);
      previous = clock();
      return seconds * (double) (previous - temp);
  
}

double Stopwatch::Elapsed()
{  // Time since stopwatch was most recently constructed or reset
  previous = clock();
  return seconds * (double) (previous - start);
}
