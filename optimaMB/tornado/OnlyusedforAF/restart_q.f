C ********************************************************************  
C **Routine to copy q to halo pts for a restart run ******************  
C ********************************************************************  
c calling routine:  initia
      subroutine restart_q(mglev,iblk1,iblk2,jdim,kdim,q,xyj,press,
     &                     sndsp,turre,turmu,vk,ve)
c  
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
c  
      dimension q(jdim*kdim*4)                   
      dimension xyj(jdim*kdim),turmu(jdim*kdim)
      dimension press(jdim*kdim),sndsp(jdim*kdim)
      dimension turre(jdim*kdim),vk(jdim*kdim),ve(jdim*kdim)
c
      do 200 ii=iblk1,iblk2
      do 200 nside1=2,4,2
        nbor = lblkpt(ii,nside1)
        nside2 = lsidept(ii,nside1)
        if(nbor.ne.0) then
            call halocp(ii,nhalo,4,nside1,nside2,
     &            jmax(ii),kmax(ii),q(lqptr(ii)),
     &            jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &            kminbnd(ii),kmaxbnd(ii),
     &            jmax(nbor),kmax(nbor),q(lqptr(nbor)),
     &            jbegin(nbor)-nsd2(nbor),jend(nbor)+nsd4(nbor),
     &            kminbnd(nbor),kmaxbnd(nbor))
            call halocp(ii,nhalo,1,nside1,nside2,
     &            jmax(ii),kmax(ii),turre(lgptr(ii)),
     &            jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &            kminbnd(ii),kmaxbnd(ii),
     &            jmax(nbor),kmax(nbor),turre(lgptr(nbor)),
     &            jbegin(nbor)-nsd2(nbor),jend(nbor)+nsd4(nbor),
     &            kminbnd(nbor),kmaxbnd(nbor))
            if (iturb.eq.4) then
              call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),vk(lgptr(ii)),
     &              jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &              kminbnd(ii),kmaxbnd(ii),
     &              jmax(nbor),kmax(nbor),ve(lgptr(nbor)),
     &              jbegin(nbor)-nsd2(nbor),jend(nbor)+nsd4(nbor),
     &              kminbnd(nbor),kmaxbnd(nbor))
              call halocp(ii,nhalo,1,nside1,nside2,
     &              jmax(ii),kmax(ii),ve(lgptr(ii)),
     &              jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &              kminbnd(ii),kmaxbnd(ii),
     &              jmax(nbor),kmax(nbor),ve(lgptr(nbor)),
     &              jbegin(nbor)-nsd2(nbor),jend(nbor)+nsd4(nbor),
     &              kminbnd(nbor),kmaxbnd(nbor))
            endif
        endif
 200  continue
c
c     -now that the halo columns have been copied, do the same for
c      the halo rows (same as in master_integrat.inc)
      do 976 ii=iblk1,iblk2
      do 976 nside1=1,3,2
        nbor = lblkpt(ii,nside1)
        nside2 = lsidept(ii,nside1)
        if (nbor.ne.0 .and. nhalo.gt.1) then
          call halocp(ii,nhalo,4,nside1,nside2,
     &          jmax(ii),kmax(ii),q(lqptr(ii)),
     &          jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &          kminbnd(ii)-nsd1(ii),kmaxbnd(ii)+nsd3(ii),
     &          jmax(nbor),kmax(nbor),q(lqptr(nbor)),
     &          jbegin(nbor)-nsd2(nbor),jend(nbor)+nsd4(nbor),
     &          kminbnd(nbor)-nsd1(nbor),kmaxbnd(nbor)+nsd3(nbor))
        endif
 976  continue
c
      lvl=1
      if (mg .or. gseq) then
c       note: divide operation higher priority than multiplication
c             -so its done first by the compiler
c             -note: can't do ncopy * (lvl/mglev) because term
c                    in brackets < 1 and thus is stored as 0 (integer)
        ncopystart = ncopy  /mglev * (lvl-1) + 1
        ncopyend   = ncopy  /mglev * lvl
        nsingstart = nsing  /mglev * (lvl-1) + 1
        nsingend   = nsing  /mglev * lvl
        nsngstart  = nsng   /mglev * (lvl-1) + 1
        nsngend    = nsng   /mglev * lvl
        nteavgstart= nteavg /mglev * (lvl-1) + 1
        nteavgend  = nteavg /mglev * lvl
        n6sngstart = n6sng  /mglev * (lvl-1) + 1
        n6sngend   = n6sng  /mglev * lvl
      else
        ncopystart = 1
        ncopyend   = ncopy
        nsingstart = 1
        nsingend   = nsing
        nsngstart  = 1
        nsngend    = nsng
        nteavgstart= 1
        nteavgend  = nteavg
        n6sngstart = 1
        n6sngend   = n6sng
      endif
      if (sngvalte) then
c       -for single-value trailing edge apply fixteavg2 to trailing-edge
c        data on element before transfer over to downstream blocks.
        do 879 ii=ncopystart,ncopyend,2
          iblk1=ibte1(ii)
          iblk2=ibte1(ii+1)
          call fixteavg2(jmax(iblk1),kmax(iblk1),
     &          q(lqptr(iblk1)),xyj(lgptr(iblk1)),
     &          jmax(iblk2),kmax(iblk2),
     &          q(lqptr(iblk2)),xyj(lgptr(iblk2)),
     &          ijte1(ii),ikte1(ii),ijte1(ii+1),ikte1(ii+1))
 879    continue
      endif
c
      do 80 ii=ncopystart,ncopyend
        ibte1tmp=ibte1(ii)
        ibte2tmp=ibte2(ii)
        call fixte(jmax(ibte1tmp),kmax(ibte1tmp),
     &        ijte1(ii),ikte1(ii),
     &        q(lqptr(ibte1tmp)),xyj(lgptr(ibte1tmp)),
     &        press(lgptr(ibte1tmp)),sndsp(lgptr(ibte1tmp)),
     &        turre(lgptr(ibte1tmp)),
     &        vk(lgptr(ibte1tmp)),ve(lgptr(ibte1tmp)),
     &        jmax(ibte2tmp),kmax(ibte2tmp),
     &        ijte2(ii),ikte2(ii),
     &        q(lqptr(ibte2tmp)),xyj(lgptr(ibte2tmp)),
     &        press(lgptr(ibte2tmp)),sndsp(lgptr(ibte2tmp)),
     &        turre(lgptr(ibte2tmp)),
     &        vk(lgptr(ibte2tmp)),ve(lgptr(ibte2tmp)),.true.)
 80   continue
c
c
c     avg of points away from sing. 
      if (.not.mg) then
        do 890 ii=nsingstart,nsingend
          call bcsing(jmax(ibs1(ii)),kmax(ibs1(ii)),
     &          q(lqptr(ibs1(ii))),xyj(lgptr(ibs1(ii))),
     &          js1(ii),ks1(ii),javg1(ii), kavg1(ii),
     &          jmax(ibs2(ii)),kmax(ibs2(ii)),
     &          q(lqptr(ibs2(ii))),xyj(lgptr(ibs2(ii))),
     &          js2(ii),ks2(ii),javg2(ii), kavg2(ii))
 890    continue
      endif
c
c
c     treatment of viscous singular pt verison 2
      if(viscous) then
        do 892 ii=nsngstart,nsngend
          call bcsng2v(jmax(ibsng1(ii)),kmax(ibsng1(ii)),
     &      q(lqptr(ibsng1(ii))),xyj(lgptr(ibsng1(ii))),
     &      jsng1(ii),ksng1(ii),jpsng1(ii),kpsng1(ii),
     &      jmax(ibsng2(ii)),kmax(ibsng2(ii)),
     &      q(lqptr(ibsng2(ii))),xyj(lgptr(ibsng2(ii))),
     &      jsng2(ii),ksng2(ii),jpsng2(ii),kpsng2(ii))
  892   continue
      endif

      if (.not.sngvalte) then
        do 885 ii=nteavgstart,nteavgend
          iblk1=ibteavg1(ii)
          iblk2=ibteavg2(ii)
          call fixteavg2(jmax(iblk1),kmax(iblk1),
     &          q(lqptr(iblk1)),xyj(lgptr(iblk1)),
     &          jmax(iblk2),kmax(iblk2),
     &          q(lqptr(iblk2)),xyj(lgptr(iblk2)),
     &          jteavg1(ii),kteavg1(ii),jteavg2(ii),kteavg2(ii))
 885    continue
      endif
c
      return                      
      end  
