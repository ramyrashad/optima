c----------------------------------------------------------------------
c     -- residual (function) evaluation => -R(Q) stored in S array --
c   -- calling routines: nksolve and framux
c     -- m. nemec, july 2001 --
c----------------------------------------------------------------------
      subroutine get_rhs(q,qps,qos,press,sndsp,s,xy,xyj,xit,ett,ds,x,
     &     y,turmu,vort,turre,turps,turos,fmu,vk,ve,tmp,uu,vv,ccx,ccy,
     &     coef2x, coef2y, coef4x, coef4y,precon, spectxi, specteta,
     &     rsa,exprhs,dhatq,exprsa,dhatt,getdhatq)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"
#include "../include/turb.inc"

      integer ib,jdim,kdim,nmax,i,iflag,lg,lq,ib1,is1,ib2,is2
      double precision q(*),         press(*),     sndsp(*)
      double precision s(*),         xy(*),        xyj(*)
      double precision xit(*),       ett(*),       ds(*)
      double precision x(*),         y(*),         turmu(*)
      double precision vort(*),      turre(*),     fmu(*)
      double precision vk(*),        ve(*),        tmp(*)
      double precision uu(*),        vv(*),        ccx(*)
      double precision ccy(*),       coef2x(*),    coef2y(*)
      double precision coef4x(*),    coef4y(*),    precon(*)
      double precision spectxi(*),   specteta(*),  rsa(*)
cmt ~~~
      Double Precision qps(*),qos(*),turps(*),turos(*)
      DOUBLE PRECISION exprhs(*),dhatq(*),exprsa(*),dhatt(*)

      logical getdhatq

      jdim = maxj
      kdim = maxk

      if (viscous .and. turbulnt .and. iturb.eq.3) then
         nmax = 5
      else
         nmax = 4
      end if

c     -- zero rhs vector s --
      do i = 1,maxjkq                       
         s(i)=0.d0                      
      end do

      do i = 1,maxjk
         rsa(i)=0.0d0
      end do

      if(viscous) then
         do ib=1,nblks
            call fmun(jmax(ib), kmax(ib), q(lqptr(ib)),
     &           press(lgptr(ib)), fmu(lgptr(ib)), sndsp(lgptr(ib)),
     &           jbegin2(ib), jend2(ib), kbegin2(ib), kend2(ib))
         end do
      end if

c     -- explicit xi rhs: pass entire arrays to xiexpl --
      call xiexpl( jdim, kdim, q, s, press,
     &     sndsp, turmu, fmu, vort, turre, vk, ve, x, y, xy, xyj, xit,
     &     ds, uu, ccx, coef2x, coef4x, spectxi, precon, tmp)

c     -- logic to avoid calling s-a model in etaexpl --        
      iflag = 0
      if (viscous .and. turbulnt .and. iturb.eq.3) then
         turbulnt = .false.
         iflag = 1
         do ib=1,nblks
            lg=lgptr(ib)
            lq=lqptr(ib)
            call calcmut( jmax(ib), kmax(ib), jminbnd(ib), jmaxbnd(ib),
     &           kbegin2(ib), kend2(ib)-1,
     &           q(lq), turmu(lg), turre(lg), fmu(lg), xyj(lg))
         end do
      endif
      
c     -- explicit eta rhs --
      call etaexpl( 1, 1, 1, jdim, kdim, q, s,
     &     press, sndsp, turmu, fmu, vort, turre, vk, ve, x, y, xy, xyj,
     &     ett, ds, vv, ccy, uu, coef2y, coef4y, specteta, precon, tmp)

      if ( iflag.eq.1 ) turbulnt = .true.
      
      if ( turbulnt ) then
         do ib=1,nblks
            lg=lgptr(ib)
            lq=lqptr(ib)
            call res_sa (ib, jmax(ib), kmax(ib), jbegin(ib), jend(ib),
     &           jlow(ib), jup(ib), kbegin(ib), kend(ib), klow(ib),
     &           kup(ib), q(lq), rsa(lg), turre(lg), xy(lq), xyj(lg),
     &           uu(lg), vv(lg), fmu(lg), vort(lg), smin(lg),re)
         end do
      end if

cmt ~~~
      IF(unsteady) THEN 

cmt ~~~ BDF2
         IF(imarch.EQ.2)THEN
            DO ib=1,nblks
               lq = lqptr(ib)
               lg = lgptr(ib)
               CALL nk_subit(qps(lq),q(lq),qos(lq),s(lq),jmax(ib),
     &         kmax(ib),jlow(ib),jup(ib),klow(ib),kup(ib),nmax,
     &         turps(lg),turre(lg),turos(lg),rsa(lg),xyj(lg))
            ENDDO
         ENDIF

cmt ~~~ ESDIRK4
         IF(imarch.EQ.4)THEN
            IF(getdhatq)THEN
               DO ib=1,nblks
                  CALL save_dhatq(jmax(ib),kmax(ib),nmax,
     &                 jlow(ib),jup(ib),klow(ib),kup(ib),
     &                 s(lqptr(ib)),dhatq(ldhatqptr(ib)),
     &                 rsa(lgptr(ib)),dhatt(ldhattptr(ib)))
               ENDDO
            ELSEIF(jstage.GT.1)THEN
               DO ib=1,nblks
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  CALL nk_subit(qps(lq),q(lq),qos(lq),s(lq),jmax(ib),
     &                 kmax(ib),jlow(ib),jup(ib),klow(ib),kup(ib),nmax,
     &                 turps(lg),turre(lg),turos(lg),rsa(lg),xyj(lg))
                  CALL add_exprhs(jmax(ib),kmax(ib),nmax,
     &                 jlow(ib),jup(ib),klow(ib),kup(ib),
     &                 s(lq),rsa(lg),exprhs(lq),exprsa(lg))
               ENDDO
            ENDIF
         ENDIF

      ENDIF
cmt ~~~

c     -- boundary conditions: farfield and body --
      call bcrhs( q, s, xy, xyj, x, y, press, turre, rsa)

c     -- average boundary condition (ibctype=5) --
      do ib1 = 1,nblks
         do is1 = 1,3,2     
            if (ibctype(ib1,is1).eq.5) then
               ib2 = lblkpt(ib1,is1)
               is2 = lsidept(ib1,is1)
               call bcravg( is1, is2, jmax(ib1), kmax(ib1),
     &              q(lqptr(ib1)), xyj(lgptr(ib1)), press(lgptr(ib1)),
     &              s(lqptr(ib1)), jminbnd(ib1), jmaxbnd(ib1),
     &              kminbnd(ib1), kmaxbnd(ib1), jmax(ib2), kmax(ib2),
     &              q(lqptr(ib2)), xyj(lgptr(ib2)), press(lgptr(ib2)),
     &              kminbnd(ib2), kmaxbnd(ib2), turre(lgptr(ib1)),
     &              turre(lgptr(ib2)), rsa(lgptr(ib1)), turbulnt)
            end if
         end do
      end do

      if (.not. viscous) then
c     -- leading edge singular points for inviscid flow --
         do i = 1,nsing
            call rhssng( js1(i), ks1(i), js2(i), ks2(i), javg1(i), 
     &           kavg1(i), javg2(i), kavg2(i), jmax(ibs1(i)),
     &           kmax(ibs1(i)), q(lqptr(ibs1(i))), xyj(lgptr(ibs1(i))),
     &           press(lgptr(ibs1(i))), s(lqptr(ibs1(i))),
     &           jmax(ibs2(i)), kmax(ibs2(i)), q(lqptr(ibs2(i))),
     &           xyj(lgptr(ibs2(i))), press(lgptr(ibs2(i))),
     &           s(lqptr(ibs2(i)))) 
         end do
      end if

c     -- trailing edge (and leading edge) point copies --
      do i=1,ncopy
         call rhscop( ijte1(i), ikte1(i), ijte2(i), ikte2(i),
     &        xyj(lgptr(ibte1(i))), q(lqptr(ibte1(i))), jmax(ibte1(i)),
     &        kmax(ibte1(i)), xyj(lgptr(ibte2(i))), q(lqptr(ibte2(i))),
     &        s(lqptr(ibte2(i))), jmax(ibte2(i)), kmax(ibte2(i)),
     &        turre(lgptr(ibte1(i))), turre(lgptr(ibte2(i))),
     &        rsa(lgptr(ibte2(i))), turbulnt)
      end do

      return
      end                       ! get_rhs
