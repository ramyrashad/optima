      subroutine calcobjair(fobj,xyj,nout,skipend,deltat)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      double precision qtar(maxjkq),q(maxjkq)
      double precision turdummy(maxjk),xyj(maxjk)

      integer ii,j,nout,skipend,ib

      double precision fobj,deltat,thc,goc 
    
      fobj= 0.0d0

 
      do j = 1,maxjkq
         q(j)=0.d0
         qtar(j)=0.d0
      end do
      
      do ii=skipend+1,nout
            
c     -- read target distribution --
         call iomarkus(4,ii,maxj,maxk,qtar,xyj,turdummy)

c     -- read physical flow field  --
         call iomarkus(3,ii,maxj,maxk,q,xyj,turdummy) 

c     -- put in jacobian Q->Qhat --
         do ib=1,nblks
            call scjac(ib, jmax(ib), kmax(ib), q(lqptr(ib)), 
     &           xyj(lgptr(ib)), jbegin2(ib),jend2(ib),
     &           kbegin2(ib), kend2(ib))
         end do
            
         do j = 1,maxjkq
            
            fobj = fobj + 0.5d0*deltat*(q(j)-qtar(j))**2
            
         end do

      end do
         
 

c     -- add constraints

      call constraints(thc,goc)
      
      fobj = fobj + thc + goc
     
 
      return
      end                       !calcobjair





      subroutine calcobjair0(fobj,nout,skipend,x,y,xy,xyj,scalef)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer nout,skipend,ii,ib,lg,lq,nscal

      double precision q(maxjkq),x(*),y(*),xyj(*),xy(*)
      double precision fobj,scalef,cdmean
      double precision press(maxjk),sndsp(maxjk),precon(maxjk*8)

      double precision thc,goc,dummy(maxjk)
      
      fobj= 0.0d0   

c     set everything to 10^4 to avoid problems with zero entries for blunt trailing edges
c     No worries - the really needed values will overwrite this quick fix     
      do ii = 1,maxjkq
         q(ii)=1.d4
      end do   
      cdmean=0.0d0

      do ii=skipend+1,nout

c        -- read physical flow field  --
         call iomarkus(3,ii,maxj,maxk,q,xyj,dummy)

c        -- halo column copy for q vector --
         call halo_q(1,nblks,maxj,maxk,q,dummy)  

c        -- calculate pressure and sound speed --
         do ib=1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call calcps(ib,jmax(ib),kmax(ib),q(lq),press(lg),sndsp(lg),
     &           precon(lprecptr(ib)),xy(lq),xyj(lg),jbegin2(ib),
     &           jend2(ib),kbegin2(ib),kend2(ib))
         end do

c        -- calculate drag at this time step
         nscal=1
         call clcdmb(q,press,x,y,xy,xyj,nscal,.false.)

         cdmean = cdmean + cdt      
      end do

c     -- calculate mean drag
      fobj = cdmean/real(nout-skipend)

c     scaling of the objective function

      if (scalef .eq. 0.d0)  scalef=fobj        
      fobj=fobj/scalef


      if (.not. sbbc) then
c     -- add constraints

         call constraints(thc,goc)
         fobj = fobj + thc + goc

      end if


      return
      end    !calcobjair0




      subroutine calcobjair5(fobj,nout,skipend,x,y,xy,xyj,cdmean,
     &                       clmean,scalef)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer nout,skipend,ii,ib,lg,lq,nscal

      double precision q(maxjkq),x(*),y(*),xyj(*),xy(*)
      double precision fobj,scalef,cdmean,clmean
      double precision press(maxjk),sndsp(maxjk),precon(maxjk*8)

      double precision thc,goc,dummy(maxjk)
      
      fobj= 0.0d0   

c     set everything to 10^4 to avoid problems with zero entries for blunt trailing edges
c     No worries - the really needed values will overwrite this quick fix     
      do ii = 1,maxjkq
         q(ii)=1.d4
      end do   
      cdmean=0.0d0
      clmean=0.0d0

      do ii=skipend+1,nout

c        -- read physical flow field  --
         call iomarkus(3,ii,maxj,maxk,q,xyj,dummy)

c        -- halo column copy for q vector --
         call halo_q(1,nblks,maxj,maxk,q,dummy)  

c        -- calculate pressure and sound speed --
         do ib=1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call calcps(ib,jmax(ib),kmax(ib),q(lq),press(lg),sndsp(lg),
     &           precon(lprecptr(ib)),xy(lq),xyj(lg),jbegin2(ib),
     &           jend2(ib),kbegin2(ib),kend2(ib))
         end do

c        -- calculate lift and drag at this time step
         nscal=1
         call clcdmb(q,press,x,y,xy,xyj,nscal,.false.)

         cdmean = cdmean + cdt
         clmean = clmean + clt           
      
      end do

c     -- calculate mean lift and drag
      cdmean = cdmean/real(nout-skipend)
      clmean = clmean/real(nout-skipend)

c     -- calculate (mean drag / mean lift)
      fobj =  cdmean/clmean

c     scaling of the objective function

      if (scalef .eq. 0.d0)  scalef=fobj        
      fobj=fobj/scalef


      if (.not. sbbc) then
c     -- add constraints

         call constraints(thc,goc)
         fobj = fobj + thc + goc

      end if


      return
      end    !calcobjair5






      subroutine calcobjair8(fobj,nout,skipend,deltat,scalef,x,y)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      double precision q(maxjkq),x(maxjk),y(maxjk)
      double precision dummy(maxjk),xyj(maxjk),cptar(4*maxj)

      integer ii,j,k,nout,skipend,ib,lq,lg,nx,ny

      double precision fobj,deltat,thc,goc,scalef,dfobjdQ(maxjkq)

      double precision xwant(maxj),ywant(maxk)
      double precision xval(4,maxj),pval(4,maxj)
      integer num(4,maxj,3),len(4)
      double precision dpdQx1(maxk,3,4),dpdQy1(maxj,3,4)
      double precision dpdQx2(maxk,3,4),dpdQy2(maxj,3,4)

      fobj= 0.0d0

      nx=NINT((remxmax-remxmin)/remdx+1.d0)
      ny=NINT((remymax-remymin)/remdy+1.d0)
      do j = 1,nx
         xwant(j)=remxmin+remdx*dble(j-1)
      end do
      do k = 1,ny
         ywant(k)=remymin+remdy*dble(k-1)
      end do


      do ii=skipend+1,nout

         do k=1,4
            len(k)=0
         end do

c        -- read physical cptarget distribution --
       
         call iocpmarkus(2,ii,maxj,maxk,dummy,xyj,x,y,cptar)
           
c        -- read physical flow field  --
         call iomarkus(3,ii,maxj,maxk,q,xyj,dummy) 

         do ib = 1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call interpolateboxyx(jmax(ib),kmax(ib),q(lq),ib,
     &           kminbnd(ib),kmaxbnd(ib),jminbnd(ib),jmaxbnd(ib),x(lg),
     &           y(lg),xval,pval,len,num,dpdQx1,dpdQy1,dpdQx2,
     &           dpdQy2,.false.)
         end do

         call interpolatebox(fobj,cptar,xval,pval,len,num,xwant,ywant,
     &        nx,ny,deltat,scalef,dpdQx1,dpdQy1,dpdQx2,dpdQy2,dfobjdQ,
     &        .false.)

      end do

c     scaling of the objective function

      if (scalef .eq. 0.d0)  scalef=fobj         
      fobj=fobj/scalef 

c     -- add constraints

      call constraints(thc,goc)

      fobj = fobj + thc + goc
     
      return
      end    !calcobjair8




      subroutine interpolateboxyx(jdim,kdim,q,ib,ks,ke,js,je,x,y,
     &     xval,pval,len,num,dpdQx1,dpdQy1,dpdQx2,dpdQy2,deriv)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer jdim,kdim,js,je,ks,ke,ib,j,k
      double precision q(jdim,kdim,4)
      double precision x(jdim,kdim),y(jdim,kdim)
      integer jmy,kmy,len(4), num(4,maxj,3), kk
      double precision xdiff,ydiff,cy,by,ay,cx,bx,ax,betastar,dis
      double precision ppl,pne,cpmy,bpmy,apmy,fac1,fac2,fac3
      double precision xval(4,maxj),pval(4,maxj)
      double precision dpdQx1(maxk,3,4),dpdQy1(maxj,3,4)
      double precision dpdQxy(3,4),beta2
      double precision dpdQx2(maxk,3,4),dpdQy2(maxj,3,4)
      double precision inixmin,iniymin,inixmax,iniymax

      logical deriv
     
      inixmin=0.5d0*abs(remxmin)
      iniymin=0.4d0*abs(remymin)
      inixmax=0.4d0*abs(remxmax-1.d0)
      iniymax=0.75d0*abs(remymax)
   
      do j = js+1,je
         kmy=0
         ydiff=iniymin
         do k = ks+1,ke
c     figure out the closest point ABOVE the lower x-arm
            if ( abs(y(j,k)-remymin).lt.ydiff .and. 
     &            (y(j,k)-remymin).gt.0.d0 )  then
               kmy=k
               ydiff= abs(y(j,k)-remymin)
            end if
              
         end do

c     parametrize and use quadratic to calculate xval as well as pval and ptval
         if (kmy .ne.0) then

            len(1)=len(1)+1

            cy=y(j,kmy-1)
            by=0.5d0*(4.d0*y(j,kmy)-3.d0*cy-y(j,kmy+1))
            ay=0.5d0*(cy-2.d0*y(j,kmy)+y(j,kmy+1))

            dis=0.25d0*by**2/ay**2-(cy-remymin)/ay

            betastar=-0.5d0*by/ay-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*by/ay+sqrt(dis)
            end if

            cx=x(j,kmy-1)
            bx=0.5d0*(4.d0*x(j,kmy)-3.d0*cx-x(j,kmy+1))
            ax=0.5d0*(cx-2.d0*x(j,kmy)+x(j,kmy+1))

            xval(1,len(1))=ax*betastar**2+bx*betastar+cx

            ppl=gami*(q(j,kmy+1,4)-0.5d0*(q(j,kmy+1,2)**2+
     &           q(j,kmy+1,3)**2)/q(j,kmy+1,1))
            pne=gami*(q(j,kmy,4)-0.5d0*(q(j,kmy,2)**2+
     &           q(j,kmy,3)**2)/q(j,kmy,1))
           
            cpmy=gami*(q(j,kmy-1,4)-0.5d0*(q(j,kmy-1,2)**2+
     &           q(j,kmy-1,3)**2)/q(j,kmy-1,1))

            bpmy=0.5d0*(4.d0*pne-3.d0*cpmy-ppl)
            apmy=0.5d0*(cpmy-2.d0*pne+ppl)

            pval(1,len(1))=apmy*betastar**2+bpmy*betastar+cpmy

c$$$            print *, 'lowx',len(1),ay*betastar**2+by*betastar+cy,
c$$$     &           xval(1,len(1))

            if (deriv) then

               num(1,len(1),1)=ib
               num(1,len(1),2)=j
               num(1,len(1),3)=kmy

               dpdQxy(1,1)=gami*0.5d0*
     &             ( (q(j,kmy-1,2)**2+q(j,kmy-1,3)**2)/q(j,kmy-1,1)**2 )                                       
               dpdQxy(1,2)=gami*(-q(j,kmy-1,2)/q(j,kmy-1,1))              
               dpdQxy(1,3)=gami*(-q(j,kmy-1,3)/q(j,kmy-1,1))
               dpdQxy(1,4)=gami

               dpdQxy(2,1)=gami*0.5d0*
     &             ( (q(j,kmy,2)**2+q(j,kmy,3)**2)/q(j,kmy,1)**2 )                                       
               dpdQxy(2,2)=gami*(-q(j,kmy,2)/q(j,kmy,1))              
               dpdQxy(2,3)=gami*(-q(j,kmy,3)/q(j,kmy,1))
               dpdQxy(2,4)=gami

               dpdQxy(3,1)=gami*0.5d0*
     &             ( (q(j,kmy+1,2)**2+q(j,kmy+1,3)**2)/q(j,kmy+1,1)**2 )                                       
               dpdQxy(3,2)=gami*(-q(j,kmy+1,2)/q(j,kmy+1,1))              
               dpdQxy(3,3)=gami*(-q(j,kmy+1,3)/q(j,kmy+1,1))
               dpdQxy(3,4)=gami

               beta2 = betastar**2

               fac1 = 0.5*beta2 - 1.5d0*betastar + 1.0d0
               fac2 = -beta2 + 2.0d0*betastar
               fac3 = 0.5d0*beta2 - 0.5d0*betastar

               do kk=1,4

                  dpdQx1(len(1),3,kk)=fac1*dpdQxy(1,kk)                                                      
                  dpdQx1(len(1),2,kk)=fac2*dpdQxy(2,kk)                                  
                  dpdQx1(len(1),1,kk)=fac3*dpdQxy(3,kk)                                      

               end do
                    
            end if

         end if

      end do

      do k = ke,ks+1,-1  
c     from top to bottom since blocks are numbered like that, too
         jmy=0
         xdiff=inixmax
         do j = js+1,je
c     figure out the closest point LEFT of the right y-arm
            if ( abs(x(j,k)-remxmax).lt.xdiff .and. 
     &            (x(j,k)-remxmax).lt.0.d0 )  then
               jmy=j
               xdiff= abs(x(j,k)-remxmax)
            end if
              
         end do

c     parametrize and use quadratic to calculate xval as well as pval and ptval
         if (jmy .ne.0) then

            len(2)=len(2)+1

            cx=x(jmy+1,k)
            bx=0.5d0*(4.d0*x(jmy,k)-3.d0*cx-x(jmy-1,k))
            ax=0.5d0*(cx-2.d0*x(jmy,k)+x(jmy-1,k))           

            dis=0.25d0*bx**2/ax**2-(cx-remxmax)/ax

            betastar=-0.5d0*bx/ax-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*bx/ax+sqrt(dis)
            end if

            cy=y(jmy+1,k)
            by=0.5d0*(4.d0*y(jmy,k)-3.d0*cy-y(jmy-1,k))
            ay=0.5d0*(cy-2.d0*y(jmy,k)+y(jmy-1,k))

            xval(2,len(2))=ay*betastar**2+by*betastar+cy

            ppl=gami*(q(jmy-1,k,4)-0.5d0*(q(jmy-1,k,2)**2+
     &           q(jmy-1,k,3)**2)/q(jmy-1,k,1))
            pne=gami*(q(jmy,k,4)-0.5d0*(q(jmy,k,2)**2+
     &           q(jmy,k,3)**2)/q(jmy,k,1))
           
            cpmy=gami*(q(jmy+1,k,4)-0.5d0*(q(jmy+1,k,2)**2+
     &           q(jmy+1,k,3)**2)/q(jmy+1,k,1))           

            bpmy=0.5d0*(4.d0*pne-3.d0*cpmy-ppl)
            apmy=0.5d0*(cpmy-2.d0*pne+ppl)

            pval(2,len(2))=apmy*betastar**2+bpmy*betastar+cpmy

c$$$            print *, 'righty',len(2),ax*betastar**2+bx*betastar+cx,
c$$$     &           xval(2,len(2))

            if (deriv) then

               num(2,len(2),1)=ib
               num(2,len(2),2)=jmy
               num(2,len(2),3)=k

               dpdQxy(1,1)=gami*0.5d0*
     &             ( (q(jmy-1,k,2)**2+q(jmy-1,k,3)**2)/q(jmy-1,k,1)**2 )                                       
               dpdQxy(1,2)=gami*(-q(jmy-1,k,2)/q(jmy-1,k,1))              
               dpdQxy(1,3)=gami*(-q(jmy-1,k,3)/q(jmy-1,k,1))
               dpdQxy(1,4)=gami

               dpdQxy(2,1)=gami*0.5d0*
     &             ( (q(jmy,k,2)**2+q(jmy,k,3)**2)/q(jmy,k,1)**2 )                                       
               dpdQxy(2,2)=gami*(-q(jmy,k,2)/q(jmy,k,1))              
               dpdQxy(2,3)=gami*(-q(jmy,k,3)/q(jmy,k,1))
               dpdQxy(2,4)=gami

               dpdQxy(3,1)=gami*0.5d0*
     &             ( (q(jmy+1,k,2)**2+q(jmy+1,k,3)**2)/q(jmy+1,k,1)**2 )                                       
               dpdQxy(3,2)=gami*(-q(jmy+1,k,2)/q(jmy+1,k,1))              
               dpdQxy(3,3)=gami*(-q(jmy+1,k,3)/q(jmy+1,k,1))
               dpdQxy(3,4)=gami

               beta2 = betastar**2

               fac1 = 0.5d0*beta2 - 0.5d0*betastar
               fac2 = -beta2 + 2.0d0*betastar
               fac3 = 0.5*beta2 - 1.5d0*betastar + 1.0d0

               do kk=1,4

                  dpdQy1(len(2),3,kk)=fac1*dpdQxy(1,kk)                                                       
                  dpdQy1(len(2),2,kk)=fac2*dpdQxy(2,kk)                                
                  dpdQy1(len(2),1,kk)=fac3*dpdQxy(3,kk)                                       

               end do
                    
            end if

         end if

      end do


      do j = js+1,je
         kmy=0
         ydiff=iniymax
         do k = ks+1,ke
c     figure out the closest point BELOW the upper x-arm
            if ( abs(y(j,k)-remymax).lt.ydiff .and. 
     &            (y(j,k)-remymax).lt.0.d0 )  then
               kmy=k
               ydiff= abs(y(j,k)-remymax)
            end if
              
         end do

c     parametrize and use quadratic to calculate xval as well as pval and ptval
         if (kmy .ne.0) then

            len(3)=len(3)+1

            cy=y(j,kmy+1)
            by=0.5d0*(4.d0*y(j,kmy)-3.d0*cy-y(j,kmy-1))
            ay=0.5d0*(cy-2.d0*y(j,kmy)+y(j,kmy-1))

            dis=0.25d0*by**2/ay**2-(cy-remymax)/ay

            betastar=-0.5d0*by/ay-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*by/ay+sqrt(dis)
            end if

            cx=x(j,kmy+1)
            bx=0.5d0*(4.d0*x(j,kmy)-3.d0*cx-x(j,kmy-1))
            ax=0.5d0*(cx-2.d0*x(j,kmy)+x(j,kmy-1))

            xval(3,len(3))=ax*betastar**2+bx*betastar+cx
  
            ppl=gami*(q(j,kmy-1,4)-0.5d0*(q(j,kmy-1,2)**2+
     &           q(j,kmy-1,3)**2)/q(j,kmy-1,1))
            pne=gami*(q(j,kmy,4)-0.5d0*(q(j,kmy,2)**2+
     &           q(j,kmy,3)**2)/q(j,kmy,1))
           
            cpmy=gami*(q(j,kmy+1,4)-0.5d0*(q(j,kmy+1,2)**2+
     &           q(j,kmy+1,3)**2)/q(j,kmy+1,1))

            bpmy=0.5d0*(4.d0*pne-3.d0*cpmy-ppl)
            apmy=0.5d0*(cpmy-2.d0*pne+ppl)

            pval(3,len(3))=apmy*betastar**2+bpmy*betastar+cpmy

c$$$            print *, 'upx',len(3),ay*betastar**2+by*betastar+cy,
c$$$     &           xval(3,len(3))

            if (deriv) then

               num(3,len(3),1)=ib
               num(3,len(3),2)=j
               num(3,len(3),3)=kmy

               dpdQxy(1,1)=gami*0.5d0*
     &             ( (q(j,kmy-1,2)**2+q(j,kmy-1,3)**2)/q(j,kmy-1,1)**2 )                                       
               dpdQxy(1,2)=gami*(-q(j,kmy-1,2)/q(j,kmy-1,1))              
               dpdQxy(1,3)=gami*(-q(j,kmy-1,3)/q(j,kmy-1,1))
               dpdQxy(1,4)=gami

               dpdQxy(2,1)=gami*0.5d0*
     &             ( (q(j,kmy,2)**2+q(j,kmy,3)**2)/q(j,kmy,1)**2 )                                       
               dpdQxy(2,2)=gami*(-q(j,kmy,2)/q(j,kmy,1))              
               dpdQxy(2,3)=gami*(-q(j,kmy,3)/q(j,kmy,1))
               dpdQxy(2,4)=gami

               dpdQxy(3,1)=gami*0.5d0*
     &             ( (q(j,kmy+1,2)**2+q(j,kmy+1,3)**2)/q(j,kmy+1,1)**2 )                                       
               dpdQxy(3,2)=gami*(-q(j,kmy+1,2)/q(j,kmy+1,1))              
               dpdQxy(3,3)=gami*(-q(j,kmy+1,3)/q(j,kmy+1,1))
               dpdQxy(3,4)=gami

               beta2 = betastar**2

               fac1 = 0.5d0*beta2 - 0.5d0*betastar
               fac2 = -beta2 + 2.0d0*betastar
               fac3 = 0.5*beta2 - 1.5d0*betastar + 1.0d0                              

               do kk=1,4

                  dpdQx2(len(3),3,kk)=fac1*dpdQxy(1,kk)                                                       
                  dpdQx2(len(3),2,kk)=fac2*dpdQxy(2,kk)                                  
                  dpdQx2(len(3),1,kk)=fac3*dpdQxy(3,kk)                                    

               end do
                    
            end if

         end if

      end do

      do k = ke,ks+1,-1   
c     from top to bottom since blocks are numbered like that, too
         jmy=0
         xdiff=inixmin
         do j = js+1,je
c     figure out the closest point RIGHT of the left y-arm
            if ( abs(x(j,k)-remxmin).lt.xdiff .and. 
     &            (x(j,k)-remxmin).gt.0.d0 )  then
               jmy=j
               xdiff= abs(x(j,k)-remxmin)
            end if
              
         end do

c     parametrize and use quadratic to calculate xval as well as pval and ptval
         if (jmy .ne.0) then

            len(4)=len(4)+1

            cx=x(jmy-1,k)
            bx=0.5d0*(4.d0*x(jmy,k)-3.d0*cx-x(jmy+1,k))
            ax=0.5d0*(cx-2.d0*x(jmy,k)+x(jmy+1,k))           

            dis=0.25d0*bx**2/ax**2-(cx-remxmin)/ax

            betastar=-0.5d0*bx/ax-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*bx/ax+sqrt(dis)
            end if

            cy=y(jmy-1,k)
            by=0.5d0*(4.d0*y(jmy,k)-3.d0*cy-y(jmy+1,k))
            ay=0.5d0*(cy-2.d0*y(jmy,k)+y(jmy+1,k))

            xval(4,len(4))=ay*betastar**2+by*betastar+cy

            ppl=gami*(q(jmy+1,k,4)-0.5d0*(q(jmy+1,k,2)**2+
     &           q(jmy+1,k,3)**2)/q(jmy+1,k,1))
            pne=gami*(q(jmy,k,4)-0.5d0*(q(jmy,k,2)**2+
     &           q(jmy,k,3)**2)/q(jmy,k,1))
           
            cpmy=gami*(q(jmy-1,k,4)-0.5d0*(q(jmy-1,k,2)**2+
     &           q(jmy-1,k,3)**2)/q(jmy-1,k,1))           

            bpmy=0.5d0*(4.d0*pne-3.d0*cpmy-ppl)
            apmy=0.5d0*(cpmy-2.d0*pne+ppl)

            pval(4,len(4))=apmy*betastar**2+bpmy*betastar+cpmy

c$$$            print *, 'lefty',len(4),ax*betastar**2+bx*betastar+cx,
c$$$     &           xval(4,len(4))

            if (deriv) then

               num(4,len(4),1)=ib
               num(4,len(4),2)=jmy
               num(4,len(4),3)=k

               dpdQxy(1,1)=gami*0.5d0*
     &             ( (q(jmy-1,k,2)**2+q(jmy-1,k,3)**2)/q(jmy-1,k,1)**2 )                                       
               dpdQxy(1,2)=gami*(-q(jmy-1,k,2)/q(jmy-1,k,1))              
               dpdQxy(1,3)=gami*(-q(jmy-1,k,3)/q(jmy-1,k,1))
               dpdQxy(1,4)=gami

               dpdQxy(2,1)=gami*0.5d0*
     &             ( (q(jmy,k,2)**2+q(jmy,k,3)**2)/q(jmy,k,1)**2 )                                       
               dpdQxy(2,2)=gami*(-q(jmy,k,2)/q(jmy,k,1))              
               dpdQxy(2,3)=gami*(-q(jmy,k,3)/q(jmy,k,1))
               dpdQxy(2,4)=gami

               dpdQxy(3,1)=gami*0.5d0*
     &             ( (q(jmy+1,k,2)**2+q(jmy+1,k,3)**2)/q(jmy+1,k,1)**2 )                                       
               dpdQxy(3,2)=gami*(-q(jmy+1,k,2)/q(jmy+1,k,1))              
               dpdQxy(3,3)=gami*(-q(jmy+1,k,3)/q(jmy+1,k,1))
               dpdQxy(3,4)=gami

               beta2 = betastar**2
                            
               fac1 = 0.5*beta2 - 1.5d0*betastar + 1.0d0
               fac2 = -beta2 + 2.0d0*betastar
               fac3 = 0.5d0*beta2 - 0.5d0*betastar

               do kk=1,4

                  dpdQy2(len(4),3,kk)=fac1*dpdQxy(1,kk)                                                       
                  dpdQy2(len(4),2,kk)=fac2*dpdQxy(2,kk)                                  
                  dpdQy2(len(4),1,kk)=fac3*dpdQxy(3,kk)                                   

               end do
                    
            end if

         end if

      end do
  

      return
      end    !interpolateboxyx





      subroutine interpolatebox(fobj,cptar,xval,pval,len,num,xwant,
     &        ywant,nx,ny,deltat,scalef,dpdQx1,dpdQy1,dpdQx2,dpdQy2,
     &        dfobjdQ,deriv)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc" 

      double precision cptar(4*maxj),dfobjdQ(maxjkq)

      double precision fobj,deltat,scalef

      double precision xwant(maxj),ywant(maxk)
      double precision xval(4,maxj),pval(4,maxj)
      integer my, num(4,maxj,3),len(4),linecount,j,k,nx,ny
      double precision diff,dis,betastar,pre
      double precision ax,bx,cx,apx,bpx,cpx
      double precision ay,by,cy,apy,bpy,cpy
      double precision dpdQx1(maxk,3,4),dpdQy1(maxj,3,4)
      double precision dpdQx2(maxk,3,4),dpdQy2(maxj,3,4)
      integer ib,kk,jj,twodim,threedim,ptr
      integer index1,index2,index3
      double precision fac,fac1,fac2,fac3,beta2
      integer twodimtmp,threedimtmp,ptrtmp,ibtmp

      logical deriv

      linecount=0

c     calc lower x-arm contribution to the objective function

      do k=1,nx
         diff=2.d0
         do j=1,len(1)
c     figure out the closest point LEFT of the desired xvalue
            if ( abs(xwant(k)-xval(1,j)).lt.diff .and. 
     &           (xwant(k)-xval(1,j)).gt.0.d0 )  then
               my = j
               diff = abs(xwant(k)-xval(1,j))
            end if           
         end do 

c     parametrize and use quadratic to interpolate pval and ptval
         cx=xval(1,my+1)
         bx=0.5d0*(4.d0*xval(1,my)-3.d0*cx-xval(1,my-1))
         ax=0.5d0*(cx-2.d0*xval(1,my)+xval(1,my-1))           

         dis=0.25d0*bx**2/ax**2-(cx-xwant(k))/ax
         
         betastar=-0.5d0*bx/ax-sqrt(dis)

         if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
            betastar=-0.5d0*bx/ax+sqrt(dis)
         end if

         cpx=pval(1,my+1)
         bpx=0.5d0*(4.d0*pval(1,my)-3.d0*cpx-pval(1,my-1))
         apx=0.5d0*(cpx-2.d0*pval(1,my)+pval(1,my-1)) 

         pre=apx*betastar**2+bpx*betastar+cpx
         
         linecount=linecount+1

c         print *, linecount,ax*betastar**2+bx*betastar+cx,xwant(k)

         if (.not. deriv) then

            fobj = fobj + 0.5d0*deltat*(pre-cptar(linecount))**2

         else

            fac=deltat*(pre-cptar(linecount))/scalef
            beta2 = betastar**2

            fac1 = 0.5d0*beta2 - 0.5d0*betastar
            fac2 = -beta2 + 2.0d0*betastar
            fac3 = 0.5*beta2 - 1.5d0*betastar + 1.0d0

            ib=num(1,my,1)
            
            twodim=jbmax(ib)+2*nhalo
            threedim=twodim*(kbmax(ib)+2*nhalo)
            ptr=lqptr(ib)

            do kk=1,4
               do jj=1,3

                  if (num(1,my,2).eq.jmaxbnd(ib)) then
c     the right contribution comes from the next block to the right
                     ibtmp=num(1,my+1,1)
                     twodimtmp=jbmax(ibtmp)+2*nhalo
                     threedimtmp=twodimtmp*(kbmax(ibtmp)+2*nhalo)
                     ptrtmp=lqptr(ibtmp)
                     
                     index1=ptr+(num(1,my-1,2)-1)+(num(1,my-1,3)-
     &                    (jj-1))*twodim+(kk-1)*threedim
                     
                     index3=ptrtmp+(num(1,my+1,2)-1)+(num(1,my+1,3)-
     &                    (jj-1))*twodimtmp+(kk-1)*threedimtmp 
                     
                  elseif (num(1,my,2).eq.jminbnd(ib)+1) then
c     the left contribution comes from the previous block to the left

                     ibtmp=num(1,my-1,1)
                     twodimtmp=jbmax(ibtmp)+2*nhalo
                     threedimtmp=twodimtmp*(kbmax(ibtmp)+2*nhalo)
                     ptrtmp=lqptr(ibtmp)
                     
                     index1=ptrtmp+(num(1,my-1,2)-1)+(num(1,my-1,3)-
     &                    (jj-1))*twodimtmp+(kk-1)*threedimtmp
                        
                     index3=ptr+(num(1,my+1,2)-1)+(num(1,my+1,3)-
     &                    (jj-1))*twodim+(kk-1)*threedim
                        
                  else

                     index1=ptr+(num(1,my-1,2)-1)+(num(1,my-1,3)-
     &                    (jj-1))*twodim+(kk-1)*threedim
                     
                     index3=ptr+(num(1,my+1,2)-1)+(num(1,my+1,3)-
     &                    (jj-1))*twodim+(kk-1)*threedim

                  end if
                  
                  index2=ptr+(num(1,my,2)-1)+(num(1,my,3)-(jj-1))*
     &                 twodim+(kk-1)*threedim
                  
                  dfobjdQ(index1)=dfobjdQ(index1)+fac*fac1*
     &                 dpdQx1(my-1,jj,kk)
                  dfobjdQ(index2)=dfobjdQ(index2)+fac*fac2*
     &                 dpdQx1(my,jj,kk)
                  dfobjdQ(index3)=dfobjdQ(index3)+fac*fac3*
     &                 dpdQx1(my+1,jj,kk)

               end do
            end do

         end if

      end do


c     calc right y-arm contribution to the objective function

      do k=1,ny
         diff=2.d0
         do j=1,len(2)
c     figure out the closest point ABOVE the desired yvalue
            if ( abs(ywant(k)-xval(2,j)).lt.diff .and. 
     &           (ywant(k)-xval(2,j)).lt.0.d0 )  then
               my = j
               diff = abs(ywant(k)-xval(2,j))
            end if           
         end do 

c     parametrize and use quadratic to interpolate pval and ptval
         cy=xval(2,my+1)
         by=0.5d0*(4.d0*xval(2,my)-3.d0*cy-xval(2,my-1))
         ay=0.5d0*(cy-2.d0*xval(2,my)+xval(2,my-1))           
         
         dis=0.25d0*by**2/ay**2-(cy-ywant(k))/ay
         
         betastar=-0.5d0*by/ay-sqrt(dis)

         if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
            betastar=-0.5d0*by/ay+sqrt(dis)
         end if
         
         cpy=pval(2,my+1)
         bpy=0.5d0*(4.d0*pval(2,my)-3.d0*cpy-pval(2,my-1))
         apy=0.5d0*(cpy-2.d0*pval(2,my)+pval(2,my-1)) 

         pre=apy*betastar**2+bpy*betastar+cpy
           
         linecount=linecount+1

c         print *, linecount,ay*betastar**2+by*betastar+cy,ywant(k)

         if (.not. deriv) then

            fobj = fobj + 0.5d0*deltat*(pre-cptar(linecount))**2

         else 

            fac=deltat*(pre-cptar(linecount))/scalef
            beta2 = betastar**2

            fac1 = 0.5d0*beta2 - 0.5d0*betastar
            fac2 = -beta2 + 2.0d0*betastar
            fac3 = 0.5*beta2 - 1.5d0*betastar + 1.0d0

            ib=num(2,my,1)
            
            twodim=jbmax(ib)+2*nhalo
            threedim=twodim*(kbmax(ib)+2*nhalo)
            ptr=lqptr(ib)
               
            do kk=1,4
               do jj=1,3
                  
                  if ( num(2,my,3).eq.kmaxbnd(ib) ) then
c     the top contribution comes from the block on top
                     ibtmp=num(2,my-1,1)
                     twodimtmp=jbmax(ibtmp)+2*nhalo
                     threedimtmp=twodimtmp*(kbmax(ibtmp)+2*nhalo)
                     ptrtmp=lqptr(ibtmp)
                     
                     index1=ptr+(num(2,my+1,2)-(jj-1))+
     &                    (num(2,my+1,3)-1)*twodim+(kk-1)*threedim
                     
                     index3=ptrtmp+(num(2,my-1,2)-(jj-1))+
     &                    (num(2,my-1,3)-1)*twodimtmp+(kk-1)*threedimtmp 
                     
                  elseif (num(2,my,3).eq.kminbnd(ib)+1) then
c     the bottom contribution comes from the block on the bottom
                     
                     ibtmp=num(2,my+1,1)
                     twodimtmp=jbmax(ibtmp)+2*nhalo
                     threedimtmp=twodimtmp*(kbmax(ibtmp)+2*nhalo)
                     ptrtmp=lqptr(ibtmp)
                     
                     index1=ptrtmp+(num(2,my+1,2)-(jj-1))+
     &                    (num(2,my+1,3)-1)*twodimtmp+(kk-1)*threedimtmp
                     
                     index3=ptr+(num(2,my-1,2)-(jj-1))+
     &                    (num(2,my-1,3)-1)*twodim+(kk-1)*threedim                    

                  else

                     index1=ptr+(num(2,my+1,2)-(jj-1))+
     &                    (num(2,my+1,3)-1)*twodim+(kk-1)*threedim
                     
                     index3=ptr+(num(2,my-1,2)-(jj-1))+
     &                    (num(2,my-1,3)-1)*twodim+(kk-1)*threedim 
                     
                  end if

                  index2=ptr+(num(2,my,2)-(jj-1))+(num(2,my,3)-1)*
     &                 twodim+(kk-1)*threedim
                  
                  dfobjdQ(index1)=dfobjdQ(index1)+fac*fac3*
     &                 dpdQy1(my+1,jj,kk)
                  dfobjdQ(index2)=dfobjdQ(index2)+fac*fac2*
     &                 dpdQy1(my,jj,kk)
                  dfobjdQ(index3)=dfobjdQ(index3)+fac*fac1*
     &                 dpdQy1(my-1,jj,kk)

               end do
            end do      

         end if
      
      end do



c     calc upper x-arm contribution to the objective function

      do k=1,nx
         diff=2.d0
         do j=1,len(3)
c     figure out the closest point LEFT of the desired xvalue
            if ( abs(xwant(k)-xval(3,j)).lt.diff .and. 
     &           (xwant(k)-xval(3,j)).gt.0.d0 )  then
               my = j
               diff = abs(xwant(k)-xval(3,j))
            end if           
         end do 

c     parametrize and use quadratic to interpolate pval and ptval
         cx=xval(3,my+1)
         bx=0.5d0*(4.d0*xval(3,my)-3.d0*cx-xval(3,my-1))
         ax=0.5d0*(cx-2.d0*xval(3,my)+xval(3,my-1))           

         dis=0.25d0*bx**2/ax**2-(cx-xwant(k))/ax
         
         betastar=-0.5d0*bx/ax-sqrt(dis)

         if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
            betastar=-0.5d0*bx/ax+sqrt(dis)
         end if

         cpx=pval(3,my+1)
         bpx=0.5d0*(4.d0*pval(3,my)-3.d0*cpx-pval(3,my-1))
         apx=0.5d0*(cpx-2.d0*pval(3,my)+pval(3,my-1)) 

         pre=apx*betastar**2+bpx*betastar+cpx
          
         linecount=linecount+1

c         print *, linecount,ax*betastar**2+bx*betastar+cx,xwant(k)

         if (.not. deriv) then

            fobj = fobj + 0.5d0*deltat*(pre-cptar(linecount))**2

         else 

            fac=deltat*(pre-cptar(linecount))/scalef
            beta2 = betastar**2
            
            fac1 = 0.5d0*beta2 - 0.5d0*betastar
            fac2 = -beta2 + 2.0d0*betastar
            fac3 = 0.5*beta2 - 1.5d0*betastar + 1.0d0
            
            ib=num(3,my,1)
            
            twodim=jbmax(ib)+2*nhalo
            threedim=twodim*(kbmax(ib)+2*nhalo)
            ptr=lqptr(ib)
            
            do kk=1,4
               do jj=1,3
                  
                  if (num(3,my,2).eq.jmaxbnd(ib)) then
c     the right contribution comes from the next block to the right
                     ibtmp=num(3,my+1,1)
                     twodimtmp=jbmax(ibtmp)+2*nhalo
                     threedimtmp=twodimtmp*(kbmax(ibtmp)+2*nhalo)
                     ptrtmp=lqptr(ibtmp)
                     
                     index1=ptr+(num(3,my-1,2)-1)+(num(3,my-1,3)-
     &                    (jj-1))*twodim+(kk-1)*threedim
                     
                     index3=ptrtmp+(num(3,my+1,2)-1)+(num(3,my+1,3)-
     &                    (jj-1))*twodimtmp+(kk-1)*threedimtmp 
                     
                  elseif (num(3,my,2).eq.jminbnd(ib)+1) then
c     the left contribution comes from the previous block to the left
                     
                     ibtmp=num(3,my-1,1)
                     twodimtmp=jbmax(ibtmp)+2*nhalo
                     threedimtmp=twodimtmp*(kbmax(ibtmp)+2*nhalo)
                     ptrtmp=lqptr(ibtmp)
                        
                     index1=ptrtmp+(num(3,my-1,2)-1)+(num(3,my-1,3)-
     &                    (jj-1))*twodimtmp+(kk-1)*threedimtmp
                     
                     index3=ptr+(num(3,my+1,2)-1)+(num(3,my+1,3)-
     &                    (jj-1))*twodim+(kk-1)*threedim
                     
                  else
                     
                     index1=ptr+(num(3,my-1,2)-1)+(num(3,my-1,3)-
     &                    (jj-1))*twodim+(kk-1)*threedim
                     
                     index3=ptr+(num(3,my+1,2)-1)+(num(3,my+1,3)-
     &                    (jj-1))*twodim+(kk-1)*threedim

                  end if

                  index2=ptr+(num(3,my,2)-1)+(num(3,my,3)-(jj-1))*
     &                 twodim+(kk-1)*threedim
                  
                  dfobjdQ(index1)=dfobjdQ(index1)+fac*fac1*
     &                 dpdQx2(my-1,jj,kk)
                  dfobjdQ(index2)=dfobjdQ(index2)+fac*fac2*
     &                 dpdQx2(my,jj,kk)
                  dfobjdQ(index3)=dfobjdQ(index3)+fac*fac3*
     &                 dpdQx2(my+1,jj,kk)

               end do
            end do

         end if
         
      end do




c     calc left y-arm contribution to the objective function

      do k=1,ny
         diff=2.d0
         do j=1,len(4)
c     figure out the closest point ABOVE the desired yvalue
            if ( abs(ywant(k)-xval(4,j)).lt.diff .and. 
     &           (ywant(k)-xval(4,j)).lt.0.d0 )  then
               my = j
               diff = abs(ywant(k)-xval(4,j))
            end if           
         end do 

c     parametrize and use quadratic to interpolate pval and ptval
         cy=xval(4,my+1)
         by=0.5d0*(4.d0*xval(4,my)-3.d0*cy-xval(4,my-1))
         ay=0.5d0*(cy-2.d0*xval(4,my)+xval(4,my-1))           
         
         dis=0.25d0*by**2/ay**2-(cy-ywant(k))/ay
         
         betastar=-0.5d0*by/ay-sqrt(dis)

         if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
            betastar=-0.5d0*by/ay+sqrt(dis)
         end if

         cpy=pval(4,my+1)
         bpy=0.5d0*(4.d0*pval(4,my)-3.d0*cpy-pval(4,my-1))
         apy=0.5d0*(cpy-2.d0*pval(4,my)+pval(4,my-1)) 

         pre=apy*betastar**2+bpy*betastar+cpy
         
         linecount=linecount+1

c         print *, linecount,ay*betastar**2+by*betastar+cy,ywant(k)

         if (.not. deriv) then

            fobj = fobj + 0.5d0*deltat*(pre-cptar(linecount))**2

         else 
      
            fac=deltat*(pre-cptar(linecount))/scalef
            beta2 = betastar**2
            
            fac1 = 0.5d0*beta2 - 0.5d0*betastar
            fac2 = -beta2 + 2.0d0*betastar
            fac3 = 0.5*beta2 - 1.5d0*betastar + 1.0d0
            
            ib=num(4,my,1)
            
            twodim=jbmax(ib)+2*nhalo
            threedim=twodim*(kbmax(ib)+2*nhalo)
            ptr=lqptr(ib)


            do kk=1,4
               do jj=1,3
                  
                  if ( num(4,my,3).eq.kmaxbnd(ib) ) then
c     the top contribution comes from the block on top
                     ibtmp=num(4,my-1,1)
                     twodimtmp=jbmax(ibtmp)+2*nhalo
                     threedimtmp=twodimtmp*(kbmax(ibtmp)+2*nhalo)
                     ptrtmp=lqptr(ibtmp)
               
                     index1=ptr+(num(4,my+1,2)-(jj-1))+
     &                    (num(4,my+1,3)-1)*twodim+(kk-1)*threedim
                     
                     index3=ptrtmp+(num(4,my-1,2)-(jj-1))+
     &                    (num(4,my-1,3)-1)*twodimtmp+(kk-1)*threedimtmp 
                     
                  elseif (num(4,my,3).eq.kminbnd(ib)+1) then
c     the bottom contribution comes from the block on the bottom

                     ibtmp=num(4,my+1,1)
                     twodimtmp=jbmax(ibtmp)+2*nhalo
                     threedimtmp=twodimtmp*(kbmax(ibtmp)+2*nhalo)
                     ptrtmp=lqptr(ibtmp)
               
                     index1=ptrtmp+(num(4,my+1,2)-(jj-1))+
     &                    (num(4,my+1,3)-1)*twodimtmp+(kk-1)*threedimtmp
                     
                     index3=ptr+(num(4,my-1,2)-(jj-1))+
     &                    (num(4,my-1,3)-1)*twodim+(kk-1)*threedim                    

                  else
                     
                     index1=ptr+(num(4,my+1,2)-(jj-1))+
     &                    (num(4,my+1,3)-1)*twodim+(kk-1)*threedim
                     
                     index3=ptr+(num(4,my-1,2)-(jj-1))+
     &                    (num(4,my-1,3)-1)*twodim+(kk-1)*threedim 
                     
                  end if

                  index2=ptr+(num(4,my,2)-(jj-1))+(num(4,my,3)-1)*
     &                 twodim+(kk-1)*threedim
                  
                  dfobjdQ(index1)=dfobjdQ(index1)+fac*fac3*
     &                 dpdQy2(my+1,jj,kk)
                  dfobjdQ(index2)=dfobjdQ(index2)+fac*fac2*
     &                 dpdQy2(my,jj,kk)
                  dfobjdQ(index3)=dfobjdQ(index3)+fac*fac1*
     &                 dpdQy2(my-1,jj,kk)

               end do
            end do                           

         end if
         
      end do



      return
      end    !interpolatebox





      subroutine calcobjairnoise(fobj,nout,skipend,deltat,scalef,x,y,
     &     xy,dpod,win,po,ns,deriv)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/index.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer nout,skipend,j,k,i,jdir,kdir,index,jdimtmp,jkdimtmp,ptrtmp
      integer index1,index2,index3,index4,ii,INOUT,jadd,kadd
      integer tc,sc,Nsous,Nsout,strlen,namelen
      double precision fobj,deltat,scalef,x(maxjk),y(maxjk),dummy(maxjk)
      double precision xy(maxjkq),po(tdim,sdim),win(tdim),ns(sdim,2)
      double precision xo(sdim),yo(sdim),q(maxjkq),xyj(maxjk),den
      double precision ps(tdim,sdim),us(tdim,sdim),vs(tdim,sdim)
      double precision rhos(tdim,sdim),xs(sdim),ys(sdim),thc,goc
      double precision pcomp(tdim),po_tar(tdim,sdim),poav(sdim)
      complex*16 dpod(Nobs,tdim,sdim,3)   
      logical deriv

      fobj= 0.0d0

c
c     Calculate normal projections along the FWH control surface and save xs and ys
c     the defining values for the surface.
c 
c     Procedure: Along a constant eta line the normal is given by nabla eta = (etax,etay) and
c     along a constant xi line the normal is given by nabla xi = (xix,xiy).
c
c     Directivity is tricky: The normal has to point away from the surface. I walk a little bit along
c     the normal in question and check whether this point is inside the polygon. If it is I have to change
c     the signs of the normal components.

      sc=0

      do i=1,Nsp

         if (Spvec(i,2) .gt. Spvec(i,3)) then
            jdir=-1
            jadd=-1
         else if (Spvec(i,2) .lt. Spvec(i,3)) then
            jdir=1
            jadd=1
         else
            jdir=2  !cannot be zero, value doesn't matter
            jadd=0
         end if
         if (Spvec(i,4) .gt. Spvec(i,5)) then
            kdir=-1
            kadd=-1
         else if (Spvec(i,4) .lt. Spvec(i,5)) then
            kdir=1
            kadd=1
         else
            kdir=2 !cannot be zero, value doesn't matter
            kadd=0
         end if

c        Start at the intended index for the first block
         if (i .eq. 1) then
            jadd=0
            kadd=0
         end if

         jdimtmp=jmax(Spvec(i,1))
         jkdimtmp=jdimtmp*kmax(Spvec(i,1))
         ptrtmp=lqptr(Spvec(i,1))

         do j=Spvec(i,2)+jadd,Spvec(i,3),jdir
            do k=Spvec(i,4)+kadd,Spvec(i,5),kdir

               sc=sc+1

               index=lgptr(Spvec(i,1)) + (j-1)+ (k-1)*jdimtmp
               index1=ptrtmp+(j-1)+(k-1)*jdimtmp
               index2=index1+(2-1)*jkdimtmp
               index3=index1+(3-1)*jkdimtmp
               index4=index1+(4-1)*jkdimtmp


               xs(sc)=x(index)
               ys(sc)=y(index)

               if (kdir.eq.2) then
                  den=SQRT(xy(index3)**2+xy(index4)**2)
                  ns(sc,1)=xy(index3)/den
                  ns(sc,2)=xy(index4)/den
               else
                  den=SQRT(xy(index1)**2+xy(index2)**2)
                  ns(sc,1)=xy(index1)/den
                  ns(sc,2)=xy(index2)/den
               end if

            end do
         end do

      end do

      Nsous=sc

C     Take care of directivity using subroutine PNPOLY
C     INOUT - THE SIGNAL RETURNED: 
C     -1 IF THE POINT IS OUTSIDE OF THE POLYGON, 
C     0 IF THE POINT IS ON AN EDGE OR AT A VERTEX, 
C     1 IF THE POINT IS INSIDE OF THE POLYGON.

      do i=1,Nsous

         call PNPOLY(xs(i)+1.d-5*ns(i,1),ys(i)+1.d-5*ns(i,2),
     &        xs,ys,Nsous,INOUT)

         if (INOUT .eq. 1) then
            ns(i,1)=-ns(i,1)
            ns(i,2)=-ns(i,2) 
         end if

      end do


c$$$      do i=1,Nsous
c$$$c         print *, ns(i,1),ns(i,2)
c$$$         print *, xs(i),ys(i)
c$$$      end do
c$$$      stop  

c
c     Calculate pressure, velocities, and density along the FWH control surface 
c     
      tc=0

      do ii=skipend+1,nout

c        -- read physical flow field  --
         call iomarkus(3,ii,maxj,maxk,q,xyj,dummy)
 
c        -- halo column copy for q vector --
         call halo_q(1,nblks,maxj,maxk,q,dummy)

         sc=0
         tc=tc+1 

         jdimtmp=jmax(blobs)
         jkdimtmp=jdimtmp*kmax(blobs)
         ptrtmp=lqptr(blobs)
         index1=ptrtmp+(jobs-1)+(kobs-1)*jdimtmp
         index2=index1+(2-1)*jkdimtmp
         index3=index1+(3-1)*jkdimtmp
         index4=index1+(4-1)*jkdimtmp


         pcomp(tc)=gami*(q(index4)-0.5*(q(index2)**2
     &        +q(index3)**2)/q(index1))

         do i=1,Nsp

            if (Spvec(i,2) .gt. Spvec(i,3)) then
               jdir=-1
               jadd=-1
            else if (Spvec(i,2) .lt. Spvec(i,3)) then
               jdir=1
               jadd=1
            else
               jdir=2           !cannot be zero, value doesn't matter
               jadd=0
            end if
            if (Spvec(i,4) .gt. Spvec(i,5)) then
               kdir=-1
               kadd=-1
            else if (Spvec(i,4) .lt. Spvec(i,5)) then
               kdir=1
               kadd=1
            else
               kdir=2           !cannot be zero, value doesn't matter
               kadd=0
            end if

c           Start at the intended index for the first block
            if (i .eq. 1) then
               jadd=0
               kadd=0
            end if

            jdimtmp=jmax(Spvec(i,1))
            jkdimtmp=jdimtmp*kmax(Spvec(i,1))
            ptrtmp=lqptr(Spvec(i,1))

            do j=Spvec(i,2)+jadd,Spvec(i,3),jdir
               do k=Spvec(i,4)+kadd,Spvec(i,5),kdir

                  sc=sc+1

                  index1=ptrtmp+(j-1)+(k-1)*jdimtmp
                  index2=index1+(2-1)*jkdimtmp
                  index3=index1+(3-1)*jkdimtmp
                  index4=index1+(4-1)*jkdimtmp

                  rhos(tc,sc)=q(index1)
                  us(tc,sc)=q(index2)/q(index1)
                  vs(tc,sc)=q(index3)/q(index1)
                  ps(tc,sc)=gami*(q(index4)-0.5*
     &                 (q(index2)**2+q(index3)**2)/q(index1))


               end do
            end do

         end do
      
      end do   !Calculate quantities along the FWH control surface


      Nsout=tc
    

c
c     Set up observer points, we are interested in 
c

      if ( abs(xobs).gt.0.d0 .or. abs(yobs).gt.0.d0 ) 
     &     then

         xo(1)=xobs
         yo(1)=yobs

      else

         do j=1,Nobs
            index=lgptr(blobs)+(jobs+j-1-1)+(kobs-1)*jmax(blobs)
            xo(j)=x(index)
            yo(j)=y(index)
         end do

      end if

c
c     For directivity plots, if outFWH=2
c

      if (outFWH.eq.2) then
         
         Nobs=64

         do j=1,Nobs
            xo(j)=xobs*DCOS(2.d0*4.d0*atan(1.d0)*(j-1)/DBLE(Nobs))
            yo(j)=xobs*DSIN(2.d0*4.d0*atan(1.d0)*(j-1)/DBLE(Nobs))
         end do

         deriv=.false.

      end if


c     Calculate the pressure po in these observer points via the FWH approach
c

      call calcFWH(Nsous,Nsout,xo,yo,po,xs,ys,ns,rhos,us,vs,ps,
     &     deltat,dpod,win,deriv,pcomp)


c     Calculate the objective function
c

      if (iobjf.eq.10) then

         namelen =  strlen(grid_file_prefix)
         filena = grid_file_prefix
         filena(namelen+1:namelen+7) = '.fwhcp'
         open(unit=n_all,file=filena,status='unknown')

         do j=1,Nobs
            do i=1,Nsout
               read (n_all,*) po_tar(i,j)
            end do
         end do
      
         close(n_all)
         
         do j=1,Nobs
            do i=1,Nsout
         
               fobj =  fobj + (po(i,j)-po_tar(i,j))**2
               
            end do
         end do
      
      else if (iobjf.eq.11) then
     
         do j=1,Nobs
            poav(j)=0.d0
            do i=1,Nsout         
               poav(j)=poav(j)+po(i,j)
            end do
            poav(j)=poav(j)/Nsout
         end do
   
         do j=1,Nobs
            do i=1,Nsout
            
               fobj =  fobj + (po(i,j)-poav(j))**2
               
            end do
         end do

      end if
     

c     scaling of the objective function

      if (scalef .eq. 0.d0)  scalef=fobj        
      fobj=fobj/scalef


      if (.not. sbbc) then
c     -- add constraints

         call constraints(thc,goc)
         fobj = fobj + thc + goc

      end if


      return
      end    !calcobjairnoise






      subroutine calcdJdQ1(jdim,kdim,q,qtar,xyj,deltat,dfobjdQ,
     &           ks,ke,js,je)

      implicit none

      integer jdim,kdim,js,je,ks,ke,j,k,n
      double precision q(jdim,kdim,4),qtar(jdim,kdim,4),deltat
      double precision dfobjdQ(jdim,kdim,4),xyj(jdim,kdim)

c      Note: Q is physical thus we have to transform it to the computational space

      do n=1,4
         do k = ks,ke
            do j = js,je
                                               
               dfobjdQ(j,k,n)=(q(j,k,n)/xyj(j,k)-qtar(j,k,n))*deltat / 
     &              xyj(j,k)
               
            end do
         end do
      end do   

      return
      end    !calcdJdQ1





      subroutine calcdJdQ0or5(jmaxt,kmaxt,iside,idir,q,xy,xyj,x,y,c_cc, 
     &                     c_cn,dJdQ,jstart,jstop,kstart,kstop)


      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"

c     Argument data types; 
      integer jmaxt,kmaxt,kstart,kstop,jstart,jstop,iside,idir
      double precision q(jmaxt,kmaxt,4),xy(jmaxt,kmaxt,4)
      double precision xyj(jmaxt,kmaxt),x(jmaxt,kmaxt),y(jmaxt,kmaxt)
      double precision c_cc,c_cn,dJdQ(jmaxt,kmaxt,4)

c     Local Variables
      integer j,k,n,jpl,jmi,jmi2,kpl,kmi,kmi2,j1,j2,j3,jtp,k1,k2,k3,ktp
      integer kadd,jadd
      double precision temp,const,sgn

      logical oddside


c     -if idir=0 don't integrate the pressure (used for inviscid walls
c      in viscous problems)
      if (idir.eq.0) return

      sgn = float(sign(1,idir))
      temp = sgn*gami/fsmach**2

c     -set side type
      if ( iside.eq.1 .or. iside.eq.3 ) then
        oddside = .true.
      else
        oddside = .false.
      endif

c     -set limits of integration
      k1  = kstart
      k2  = kstop
      ktp = kstart + 1
      j1  = jstart
      j2  = jstop
      jtp = jstart + 1
      if (iside.eq.1) k1 = kstart
      if (iside.eq.2) j1 = jstart
      if (iside.eq.3) k1 = kstop
      if (iside.eq.4) j1 = jstop

c     ****************************************
c     Inviscid contribution: pressure integral
c     ****************************************

      if (oddside) then

         do j = jtp,j2
            jmi=j-1
            const = temp*(c_cc*(y(j,k1)-y(jmi,k1)) + 
     |         c_cn*(x(jmi,k1)-x(j,k1)))

            dJdQ(j,k1,1) = dJdQ(j,k1,1) + 0.5d0*const*
     |           (Q(j,k1,2)**2/Q(j,k1,1)**2+Q(j,k1,3)**2/Q(j,k1,1)**2)
            dJdQ(j,k1,2) = dJdQ(j,k1,2) - const*Q(j,k1,2)/Q(j,k1,1)
            dJdQ(j,k1,3) = dJdQ(j,k1,3) - const*Q(j,k1,3)/Q(j,k1,1)
            dJdQ(j,k1,4) = dJdQ(j,k1,4) + const

            dJdQ(jmi,k1,1) = dJdQ(jmi,k1,1) + 0.5d0 * const * 
     |           ( Q(jmi,k1,2)**2/Q(jmi,k1,1)**2 + 
     |             Q(jmi,k1,3)**2/Q(jmi,k1,1)**2 )
            dJdQ(jmi,k1,2) = dJdQ(jmi,k1,2)-const*Q(jmi,k1,2)
     |           /Q(jmi,k1,1)
            dJdQ(jmi,k1,3) = dJdQ(jmi,k1,3)-const*Q(jmi,k1,3)
     |           /Q(jmi,k1,1)
            dJdQ(jmi,k1,4) = dJdQ(jmi,k1,4)+const

         enddo

      else

         do k = ktp,k2
            kmi=k-1
            const = temp*(c_cc*(y(j1,k)-y(j1,kmi)) + 
     |         c_cn*(x(j1,kmi)-x(j1,k)))

            dJdQ(j1,k,1) = dJdQ(j1,k,1) + 0.5d0*const*
     |           (Q(j1,k,2)**2/Q(j1,k,1)**2+Q(j1,k,3)**2/Q(j1,k,1)**2)
            dJdQ(j1,k,2) = dJdQ(j1,k,2) - const*Q(j1,k,2)/Q(j1,k,1)
            dJdQ(j1,k,3) = dJdQ(j1,k,3) - const*Q(j1,k,3)/Q(j1,k,1)
            dJdQ(j1,k,4) = dJdQ(j1,k,4) + const

            dJdQ(j1,kmi,1) = dJdQ(j1,kmi,1) + 0.5d0 * const * 
     |           ( Q(j1,kmi,2)**2/Q(j1,kmi,1)**2 + 
     |             Q(j1,kmi,3)**2/Q(j1,kmi,1)**2 )
            dJdQ(j1,kmi,2) = dJdQ(j1,kmi,2)-const*Q(j1,kmi,2)
     |           /Q(j1,kmi,1)
            dJdQ(j1,kmi,3) = dJdQ(j1,kmi,3)-const*Q(j1,kmi,3)
     |           /Q(j1,kmi,1)
            dJdQ(j1,kmi,4) = dJdQ(j1,kmi,4)+const

         enddo

      end if  !oddside


c     *******************************************
c     Viscous contribution: shear stress integral
c     *******************************************

      if (viscous) then
     
         if(iside.eq.1) then
            kadd = 1
         elseif(iside.eq.3) then
            kadd = -1
         elseif(iside.eq.2) then
            jadd = 1
         else
            jadd = -1
         endif

         if (oddside) then
            k  = k1
            k2 = k1 + kadd
            k3 = k2 + kadd
         else
            j  = j1
            j2 = j1 + jadd
            j3 = j2 + jadd
         end if

         temp = sgn/(re*fsmach**2)

         if (iord.eq.2) then

            if (oddside) then 

               do j = jtp,j2

                  jpl=j+1
                  jmi=j-1
                  jmi2=j-2 
                  const = temp *
     |              (  c_cc*(x(j,k)-x(jmi,k)) + c_cn*(y(j,k)-y(jmi,k)) )

c                 -- xi direction
                  if (j > jtp .and. j < j2) then !central stencil
c                    Interior nodes, xiy*uxi-xix*vxi terms

                     dJdQ(jpl,k,1) = dJdQ(jpl,k,1) - 0.5d0*const*
     |                 (xy(j,k,2)*Q(jpl,k,2)-xy(j,k,1)*Q(jpl,k,3))
     |                 /Q(jpl,k,1)**2
                     dJdQ(jpl,k,2) =dJdQ(jpl,k,2)+0.5d0*const*xy(j,k,2)
     |                 /Q(jpl,k,1)
                     dJdQ(jpl,k,3) =dJdQ(jpl,k,3)-0.5d0*const*xy(j,k,1)
     |                 /Q(jpl,k,1)

                     dJdQ(jmi,k,1) = dJdQ(jmi,k,1) + 0.5d0*const*
     |                 (xy(j,k,2)*Q(jmi,k,2)-xy(j,k,1)*Q(jmi,k,3))
     |                 /Q(jmi,k,1)**2
                     dJdQ(jmi,k,2) =dJdQ(jmi,k,2)-0.5d0*const*xy(j,k,2)
     |                 /Q(jmi,k,1)
                     dJdQ(jmi,k,3) =dJdQ(jmi,k,3)+0.5d0*const*xy(j,k,1)
     |                 /Q(jmi,k,1)

                     dJdQ(j,k,1) = dJdQ(j,k,1)-0.5d0*const*(xy(jmi,k,2)*
     |                 Q(j,k,2)-xy(jmi,k,1)*Q(j,k,3))/Q(j,k,1)**2
                     dJdQ(j,k,2) = dJdQ(j,k,2) + 0.5d0*const*xy(jmi,k,2)
     |                 /Q(j,k,1)
                     dJdQ(j,k,3) = dJdQ(j,k,3) - 0.5d0*const*xy(jmi,k,1)
     |                 /Q(j,k,1)
                  
                     dJdQ(jmi2,k,1)=dJdQ(jmi2,k,1) + 0.5d0*const*
     |                 (xy(jmi,k,2)*Q(jmi2,k,2)-xy(jmi,k,1)*Q(jmi2,k,3))
     |                 /Q(jmi2,k,1)**2
                     dJdQ(jmi2,k,2)=dJdQ(jmi2,k,2)-0.5d0*const*
     |                 xy(jmi,k,2)/Q(jmi2,k,1)
                     dJdQ(jmi2,k,3)=dJdQ(jmi2,k,3)+0.5d0*const*
     |                 xy(jmi,k,1)/Q(jmi2,k,1)

                  else if (j == jtp) then !(use upwind stencil)
c                    Boundary nodes, xiy*uxi-xix*vxi terms 

                     dJdQ(jpl,k,1) = dJdQ(jpl,k,1) - 0.5d0*const*
     |                 (xy(j,k,2)*Q(jpl,k,2)-xy(j,k,1)*Q(jpl,k,3))
     |                 /Q(jpl,k,1)**2
                     dJdQ(jpl,k,2) = dJdQ(jpl,k,2)+0.5d0*const*xy(j,k,2)
     |                 /Q(jpl,k,1)
                     dJdQ(jpl,k,3) = dJdQ(jpl,k,3)-0.5d0*const*xy(j,k,1)
     |                 /Q(jpl,k,1)
                  
                     dJdQ(jmi,k,1) = dJdQ(jmi,k,1) + 0.5d0*const*
     |                 (xy(j,k,2)*Q(jmi,k,2)-xy(j,k,1)*Q(jmi,k,3))
     |                 /Q(jmi,k,1)**2
                     dJdQ(jmi,k,2) = dJdQ(jmi,k,2)-0.5d0*const*xy(j,k,2)
     |                 /Q(jmi,k,1)
                     dJdQ(jmi,k,3) = dJdQ(jmi,k,3)+0.5d0*const*xy(j,k,1)
     |                 /Q(jmi,k,1)
                  
                     dJdQ(j,k,1) = dJdQ(j,k,1) - const*(xy(jmi,k,2)*
     |                 Q(j,k,2)-xy(jmi,k,1)*Q(j,k,3))/Q(j,k,1)**2
                     dJdQ(j,k,2) =dJdQ(j,k,2)+const*xy(jmi,k,2)/Q(j,k,1)
                     dJdQ(j,k,3) =dJdQ(j,k,3)-const*xy(jmi,k,1)/Q(j,k,1)

                     dJdQ(jmi,k,1) = dJdQ(jmi,k,1) + const*
     |                 (xy(jmi,k,2)*Q(jmi,k,2)-xy(jmi,k,1)*Q(jmi,k,3))
     |                 /Q(jmi,k,1)**2
                     dJdQ(jmi,k,2) = dJdQ(jmi,k,2) -const*xy(jmi,k,2)
     |                 /Q(jmi,k,1)
                     dJdQ(jmi,k,3) = dJdQ(jmi,k,3) +const*xy(jmi,k,1)
     |                 /Q(jmi,k,1)

                  else if (j == j2) then !(use upwind stencil)
c                    Boundary nodes, xiy*uxi-xix*vxi terms 

                     dJdQ(j,k,1) = dJdQ(j,k,1) - const*( xy(j,k,2)*
     |                 Q(j,k,2)-xy(j,k,1)*Q(j,k,3))/Q(j,k,1)**2
                     dJdQ(j,k,2) = dJdQ(j,k,2) + const*xy(j,k,2)
     |                 /Q(j,k,1)
                     dJdQ(j,k,3) = dJdQ(j,k,3) - const*xy(j,k,1)
     |                 /Q(j,k,1)
                  
                     dJdQ(jmi,k,1) = dJdQ(jmi,k,1) + const*
     |                 (xy(j,k,2)*Q(jmi,k,2)-xy(j,k,1)*Q(jmi,k,3))
     |                 /Q(jmi,k,1)**2
                     dJdQ(jmi,k,2) = dJdQ(jmi,k,2) - const*xy(j,k,2)
     |                 /Q(jmi,k,1)
                     dJdQ(jmi,k,3) = dJdQ(jmi,k,3) + const*xy(j,k,1)
     |                 /Q(jmi,k,1)

                     dJdQ(j,k,1) = dJdQ(j,k,1)-0.5d0*const*(xy(jmi,k,2)*
     |                 Q(j,k,2)-xy(jmi,k,1)*Q(j,k,3))/Q(j,k,1)**2
                     dJdQ(j,k,2) = dJdQ(j,k,2) + 0.5d0*const*xy(jmi,k,2)
     |                 /Q(j,k,1)
                     dJdQ(j,k,3) = dJdQ(j,k,3) - 0.5d0*const*xy(jmi,k,1)
     |                 /Q(j,k,1)

                     dJdQ(jmi2,k,1)=dJdQ(jmi2,k,1) +0.5d0*const*
     |                 (xy(jmi,k,2)*Q(jmi2,k,2)-xy(jmi,k,1)*Q(jmi2,k,3))
     |                 /Q(jmi2,k,1)**2
                     dJdQ(jmi2,k,2)=dJdQ(jmi2,k,2)-0.5d0*const*
     |                 xy(jmi,k,2)/Q(jmi2,k,1)
                     dJdQ(jmi2,k,3)=dJdQ(jmi2,k,3)+0.5d0*const*
     |                 xy(jmi,k,1)/Q(jmi2,k,1)

                  end if        ! xiy*uxi-xix*vxi terms

c                 -- eta direction

                  const = float(kadd)*const

                  dJdQ(j,k,1) = dJdQ(j,k,1) + 1.5d0*const*
     |              (xy(j,k,4)*Q(j,k,2)-xy(j,k,3)*Q(j,k,3))/Q(j,k,1)**2
                  dJdQ(j,k,2)=dJdQ(j,k,2)-1.5d0*const*xy(j,k,4)/Q(j,k,1)
                  dJdQ(j,k,3)=dJdQ(j,k,3)+1.5d0*const*xy(j,k,3)/Q(j,k,1)
               
                  dJdQ(jmi,k,1) = dJdQ(jmi,k,1) + 1.5d0*const*
     |              (xy(jmi,k,4)*Q(jmi,k,2)-xy(jmi,k,3)*Q(jmi,k,3))
     |              /Q(jmi,k,1)**2
                  dJdQ(jmi,k,2) = dJdQ(jmi,k,2) -1.5d0*const*xy(jmi,k,4)
     |              /Q(jmi,k,1)
                  dJdQ(jmi,k,3) = dJdQ(jmi,k,3) +1.5d0*const*xy(jmi,k,3)
     |              /Q(jmi,k,1)
               
                  dJdQ(j,k2,1) = dJdQ(j,k2,1) - 2.0d0*const*( xy(j,k,4)*
     |              Q(j,k2,2)-xy(j,k,3)*Q(j,k2,3))/Q(j,k2,1)**2
                  dJdQ(j,k2,2) = dJdQ(j,k2,2) + 2.0d0*const*xy(j,k,4)
     |              /Q(j,k2,1)
                  dJdQ(j,k2,3) = dJdQ(j,k2,3) - 2.0d0*const*xy(j,k,3)
     |              /Q(j,k2,1)
               
                  dJdQ(jmi,k2,1) = dJdQ(jmi,k2,1) - 2.0d0*const*
     |              (xy(jmi,k,4)*Q(jmi,k2,2)-xy(jmi,k,3)*Q(jmi,k2,3))
     |              /Q(jmi,k2,1)**2
                  dJdQ(jmi,k2,2) =dJdQ(jmi,k2,2)+2.0d0*const*xy(jmi,k,4)
     |              /Q(jmi,k2,1)
                  dJdQ(jmi,k2,3) =dJdQ(jmi,k2,3)-2.0d0*const*xy(jmi,k,3)
     |              /Q(jmi,k2,1)

                  dJdQ(j,k3,1) = dJdQ(j,k3,1) + 0.5d0*const*( xy(j,k,4)*
     |              Q(j,k3,2)-xy(j,k,3)*Q(j,k3,3))/Q(j,k3,1)**2
                  dJdQ(j,k3,2) = dJdQ(j,k3,2) - 0.5d0*const*xy(j,k,4)
     |              /Q(j,k3,1)
                  dJdQ(j,k3,3) = dJdQ(j,k3,3) + 0.5d0*const*xy(j,k,3)
     |              /Q(j,k3,1)

                  dJdQ(jmi,k3,1) = dJdQ(jmi,k3,1) + 0.5d0*const*
     |              (xy(jmi,k,4)*Q(jmi,k3,2)-xy(jmi,k,3)*Q(jmi,k3,3))
     |              /Q(jmi,k3,1)**2
                  dJdQ(jmi,k3,2) =dJdQ(jmi,k3,2)-0.5d0*const*xy(jmi,k,4)
     |              /Q(jmi,k3,1)
                  dJdQ(jmi,k3,3) =dJdQ(jmi,k3,3)+0.5d0*const*xy(jmi,k,3)
     |              /Q(jmi,k3,1)

               enddo   !loop over j

            else   !not oddside

               do k = ktp,k2

                  kpl=k+1
                  kmi=k-1
                  kmi2=k-2 
                  const = temp *
     |              (  c_cc*(x(j,k)-x(j,kmi)) + c_cn*(y(j,k)-y(j,kmi)) )

c              -- eta direction

                  if (k > ktp .and. k < k2) then !central stencil
c                    Interior nodes, ueta*etay-veta*etax terms

                     dJdQ(j,kpl,1) = dJdQ(j,kpl,1) - 0.5d0*const*
     |                 (xy(j,k,4)*Q(j,kpl,2)-xy(j,k,3)*Q(j,kpl,3))
     |                 /Q(j,kpl,1)**2
                     dJdQ(j,kpl,2) =dJdQ(j,kpl,2)+0.5d0*const*xy(j,k,4)
     |                 /Q(j,kpl,1)
                     dJdQ(j,kpl,3) =dJdQ(j,kpl,3)-0.5d0*const*xy(j,k,3)
     |                 /Q(j,kpl,1)

                     dJdQ(j,kmi,1) = dJdQ(j,kmi,1) + 0.5d0*const*
     |                 (xy(j,k,4)*Q(j,kmi,2)-xy(j,k,3)*Q(j,kmi,3))
     |                 /Q(j,kmi,1)**2
                     dJdQ(j,kmi,2) =dJdQ(j,kmi,2)-0.5d0*const*xy(j,k,4)
     |                 /Q(j,kmi,1)
                     dJdQ(j,kmi,3) =dJdQ(j,kmi,3)+0.5d0*const*xy(j,k,3)
     |                 /Q(j,kmi,1)

                     dJdQ(j,k,1) = dJdQ(j,k,1)-0.5d0*const*(xy(j,kmi,4)*
     |                 Q(j,k,2)-xy(j,kmi,3)*Q(j,k,3))/Q(j,k,1)**2
                     dJdQ(j,k,2) = dJdQ(j,k,2) + 0.5d0*const*xy(j,kmi,4)
     |                 /Q(j,k,1)
                     dJdQ(j,k,3) = dJdQ(j,k,3) - 0.5d0*const*xy(j,kmi,3)
     |                 /Q(j,k,1)
                  
                     dJdQ(j,kmi2,1)=dJdQ(j,kmi2,1) + 0.5d0*const*
     |                 (xy(j,kmi,4)*Q(j,kmi2,2)-xy(j,kmi,3)*Q(j,kmi2,3))
     |                 /Q(j,kmi2,1)**2
                     dJdQ(j,kmi2,2)=dJdQ(j,kmi2,2)-0.5d0*const*
     |                 xy(j,kmi,4)/Q(j,kmi2,1)
                     dJdQ(j,kmi2,3)=dJdQ(j,kmi2,3)+0.5d0*const*
     |                 xy(j,kmi,3)/Q(j,kmi2,1)

                  else if (k == ktp) then !(use upwind stencil)
c                    Boundary nodes, ueta*etay-veta*etax  terms 

                     dJdQ(j,kpl,1) = dJdQ(j,kpl,1) - 0.5d0*const*
     |                 (xy(j,k,4)*Q(j,kpl,2)-xy(j,k,3)*Q(j,kpl,3))
     |                 /Q(j,kpl,1)**2
                     dJdQ(j,kpl,2) = dJdQ(j,kpl,2)+0.5d0*const*xy(j,k,4)
     |                 /Q(j,kpl,1)
                     dJdQ(j,kpl,3) = dJdQ(j,kpl,3)-0.5d0*const*xy(j,k,3)
     |                 /Q(j,kpl,1)
                  
                     dJdQ(j,kmi,1) = dJdQ(j,kmi,1) + 0.5d0*const*
     |                 (xy(j,k,4)*Q(j,kmi,2)-xy(j,k,3)*Q(j,kmi,3))
     |                 /Q(j,kmi,1)**2
                     dJdQ(j,kmi,2) = dJdQ(j,kmi,2)-0.5d0*const*xy(j,k,4)
     |                 /Q(j,kmi,1)
                     dJdQ(j,kmi,3) = dJdQ(j,kmi,3)+0.5d0*const*xy(j,k,3)
     |                 /Q(j,kmi,1)
                  
                     dJdQ(j,k,1) = dJdQ(j,k,1) - const*(xy(j,kmi,4)*
     |                 Q(j,k,2)-xy(j,kmi,3)*Q(j,k,3))/Q(j,k,1)**2
                     dJdQ(j,k,2) =dJdQ(j,k,2)+const*xy(j,kmi,4)/Q(j,k,1)
                     dJdQ(j,k,3) =dJdQ(j,k,3)-const*xy(j,kmi,3)/Q(j,k,1)

                     dJdQ(j,kmi,1) = dJdQ(j,kmi,1) + const*
     |                 (xy(j,kmi,4)*Q(j,kmi,2)-xy(j,kmi,3)*Q(j,kmi,3))
     |                 /Q(j,kmi,1)**2
                     dJdQ(j,kmi,2) = dJdQ(j,kmi,2) -const*xy(j,kmi,4)
     |                 /Q(j,kmi,1)
                     dJdQ(j,kmi,3) = dJdQ(j,kmi,3) +const*xy(j,kmi,3)
     |                 /Q(j,kmi,1)

                  else if (k == k2) then !(use upwind stencil)
c                    Boundary nodes, ueta*etay-veta*etax  terms 

                     dJdQ(j,k,1) = dJdQ(j,k,1) - const*( xy(j,k,4)*
     |                 Q(j,k,2)-xy(j,k,3)*Q(j,k,3))/Q(j,k,1)**2
                     dJdQ(j,k,2) = dJdQ(j,k,2) + const*xy(j,k,4)
     |                 /Q(j,k,1)
                     dJdQ(j,k,3) = dJdQ(j,k,3) - const*xy(j,k,3)
     |                 /Q(j,k,1)
                  
                     dJdQ(j,kmi,1) = dJdQ(j,kmi,1) + const*
     |                 (xy(j,k,4)*Q(j,kmi,2)-xy(j,k,3)*Q(j,kmi,3))
     |                 /Q(j,kmi,1)**2
                     dJdQ(j,kmi,2) = dJdQ(j,kmi,2) - const*xy(j,k,4)
     |                 /Q(j,kmi,1)
                     dJdQ(j,kmi,3) = dJdQ(j,kmi,3) + const*xy(j,k,3)
     |                 /Q(j,kmi,1)

                     dJdQ(j,k,1) = dJdQ(j,k,1)-0.5d0*const*(xy(j,kmi,4)*
     |                 Q(j,k,2)-xy(j,kmi,3)*Q(j,k,3))/Q(j,k,1)**2
                     dJdQ(j,k,2) = dJdQ(j,k,2) + 0.5d0*const*xy(j,kmi,4)
     |                 /Q(j,k,1)
                     dJdQ(j,k,3) = dJdQ(j,k,3) - 0.5d0*const*xy(j,kmi,3)
     |                 /Q(j,k,1)

                     dJdQ(j,kmi2,1)=dJdQ(j,kmi2,1) +0.5d0*const*
     |                 (xy(j,kmi,4)*Q(j,kmi2,2)-xy(j,kmi,3)*Q(j,kmi2,3))
     |                 /Q(j,kmi2,1)**2
                     dJdQ(j,kmi2,2)=dJdQ(j,kmi2,2)-0.5d0*const*
     |                 xy(j,kmi,4)/Q(j,kmi2,1)
                     dJdQ(j,kmi2,3)=dJdQ(j,kmi2,3)+0.5d0*const*
     |                 xy(j,kmi,3)/Q(j,kmi2,1)

                  end if        ! ueta*etay-veta*etax  terms

c              -- xi direction

                  const = float(jadd)*const

                  dJdQ(j,k,1) = dJdQ(j,k,1) + 1.5d0*const*
     |              (xy(j,k,4)*Q(j,k,2)-xy(j,k,3)*Q(j,k,3))/Q(j,k,1)**2
                  dJdQ(j,k,2)=dJdQ(j,k,2)-1.5d0*const*xy(j,k,4)/Q(j,k,1)
                  dJdQ(j,k,3)=dJdQ(j,k,3)+1.5d0*const*xy(j,k,3)/Q(j,k,1)
               
                  dJdQ(j,kmi,1) = dJdQ(j,kmi,1) + 1.5d0*const*
     |              (xy(j,kmi,4)*Q(j,kmi,2)-xy(j,kmi,3)*Q(j,kmi,3))
     |              /Q(j,kmi,1)**2
                  dJdQ(j,kmi,2) = dJdQ(j,kmi,2) -1.5d0*const*xy(j,kmi,4)
     |              /Q(j,kmi,1)
                  dJdQ(j,kmi,3) = dJdQ(j,kmi,3) +1.5d0*const*xy(j,kmi,3)
     |              /Q(j,kmi,1)
               
                  dJdQ(j2,k,1) = dJdQ(j2,k,1) - 2.0d0*const*( xy(j,k,4)*
     |              Q(j2,k,2)-xy(j,k,3)*Q(j2,k,3))/Q(j2,k,1)**2
                  dJdQ(j2,k,2) = dJdQ(j2,k,2) + 2.0d0*const*xy(j,k,4)
     |              /Q(j2,k,1)
                  dJdQ(j2,k,3) = dJdQ(j2,k,3) - 2.0d0*const*xy(j,k,3)
     |              /Q(j2,k,1)
               
                  dJdQ(j2,kmi,1) = dJdQ(j2,kmi,1) - 2.0d0*const*
     |              (xy(j,kmi,4)*Q(j2,kmi,2)-xy(j,kmi,3)*Q(j2,kmi,3))
     |              /Q(j2,kmi,1)**2
                  dJdQ(j2,kmi,2) =dJdQ(j2,kmi,2)+2.0d0*const*xy(j,kmi,4)
     |              /Q(j2,kmi,1)
                  dJdQ(j2,kmi,3) =dJdQ(j2,kmi,3)-2.0d0*const*xy(j,kmi,3)
     |              /Q(j2,kmi,1)

                  dJdQ(j3,k,1) = dJdQ(j3,k,1) + 0.5d0*const*( xy(j,k,4)*
     |              Q(j3,k,2)-xy(j,k,3)*Q(j3,k,3))/Q(j3,k,1)**2
                  dJdQ(j3,k,2) = dJdQ(j3,k,2) - 0.5d0*const*xy(j,k,4)
     |              /Q(j3,k,1)
                  dJdQ(j3,k,3) = dJdQ(j3,k,3) + 0.5d0*const*xy(j,k,3)
     |              /Q(j3,k,1)

                  dJdQ(j3,kmi,1) = dJdQ(j3,kmi,1) + 0.5d0*const*
     |              (xy(j,kmi,4)*Q(j3,kmi,2)-xy(j,kmi,3)*Q(j3,kmi,3))
     |              /Q(j3,kmi,1)**2
                  dJdQ(j3,kmi,2) =dJdQ(j3,kmi,2)-0.5d0*const*xy(j,kmi,4)
     |              /Q(j3,kmi,1)
                  dJdQ(j3,kmi,3) =dJdQ(j3,kmi,3)+0.5d0*const*xy(j,kmi,3)
     |              /Q(j3,kmi,1)

               enddo  !loop over k

            end if  !oddside?

         else  !higher order
            print *, 'Higher order not implemented yet!'
            stop
         end if   !iord.eq.2

      endif    !Viscous contribution

      return
      end    !calcdJdQ0or5







      subroutine calcdJdQnoise(dJdQ,q,count,nout,skipend,deltat,
     &              scalef,dpod,win,po,ns)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/index.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"


      integer j,k,l,n,i,nout,skipend,sc,Nss,Nt,count,tnow,Nth
      integer index1,index2,index3,index4,jdimtmp,jkdimtmp,ptrtmp
      integer jdir,kdir,strlen,namelen,jadd,kadd

      double precision q(maxjkq),omeg(tdim)
      double precision,allocatable :: dpdq(:,:,:,:)
      double precision dJdQ(maxjkq),ns(sdim,2),po_tar(tdim,sdim)

      double precision scalef,deltat,win(tdim),wsave(4*tdim+15)

      double precision dF1dq(sdim,4),dF2dq(sdim,4),fac,QS,ainf
      double precision scal,sum(sdim),dJdQtmp(sdim,4)
      double precision po(tdim,sdim),poav(sdim),dJdp(tdim,sdim)

      complex*16 dpod(Nobs,tdim,sdim,3),tmp(tdim),dpFTdq(tdim,4),facc
      complex*16 facc2(tdim)

      logical odd

      allocate(dpdq(Nobs,tdim,sdim,4))

c
c     calculate dQdq, dF1dq and dF2dq 
c     

cc    calculate some needed quantities

      Nt = nout-skipend
      tnow=count-skipend

      pi=4.d0*atan(1.d0)
      ainf = sqrt(gamma*pinf/rhoinf)
      uinf=fsmach*ainf*dcos(alpha*pi/180.d0)
      vinf=fsmach*ainf*dsin(alpha*pi/180.d0)

      sc=0

      do i=1,Nsp

         if (Spvec(i,2) .gt. Spvec(i,3)) then
            jdir=-1
            jadd=-1
         else if (Spvec(i,2) .lt. Spvec(i,3)) then
            jdir=1
            jadd=1
         else
            jdir=2              !cannot be zero, value doesn't matter
            jadd=0
         end if
         if (Spvec(i,4) .gt. Spvec(i,5)) then
            kdir=-1
            kadd=-1
         else if (Spvec(i,4) .lt. Spvec(i,5)) then
            kdir=1
            kadd=1
         else
            kdir=2              !cannot be zero, value doesn't matter
            kadd=0
         end if

c        Start at the intended index for the first block
         if (i .eq. 1) then
            jadd=0
            kadd=0
         end if

         jdimtmp=jmax(Spvec(i,1))
         jkdimtmp=jdimtmp*kmax(Spvec(i,1))
         ptrtmp=lqptr(Spvec(i,1))

         do j=Spvec(i,2)+jadd,Spvec(i,3),jdir
            do k=Spvec(i,4)+kadd,Spvec(i,5),kdir
               
               sc=sc+1
               
               index1=ptrtmp+(j-1)+(k-1)*jdimtmp
               index2=index1+(2-1)*jkdimtmp
               index3=index1+(3-1)*jkdimtmp
               index4=index1+(4-1)*jkdimtmp
               
               fac= gami*ns(sc,1)
               QS = q(index2)*ns(sc,1)+q(index3)*ns(sc,2)
               
               dF1dq(sc,1) = fac*0.5d0*( (q(index2)/q(index1))**2 + 
     &              (q(index3)/q(index1))**2  ) - 
     &              q(index2) * QS / q(index1)**2 
               dF1dq(sc,2) = -fac*q(index2)/q(index1) + QS/q(index1) + 
     &              (q(index2)/q(index1)-2.d0*uinf)*ns(sc,1)
               dF1dq(sc,3) = -fac*q(index3)/q(index1) + 
     &              (q(index2)/q(index1)-2.d0*uinf)*ns(sc,2)
               dF1dq(sc,4) = fac

               fac= gami*ns(sc,2)
               
               dF2dq(sc,1) = fac*0.5d0*( (q(index2)/q(index1))**2 + 
     &              (q(index3)/q(index1))**2  ) - 
     &              q(index3) * QS / q(index1)**2 
               dF2dq(sc,2) = -fac*q(index2)/q(index1) + 
     &              (q(index3)/q(index1)-2.d0*vinf)*ns(sc,1)
               dF2dq(sc,3) = -fac*q(index3)/q(index1) + QS/q(index1) + 
     &              (q(index3)/q(index1)-2.d0*vinf)*ns(sc,2)
               dF2dq(sc,4) = fac


            end do
         end do
         
      end do

      Nss=sc
       
       
c
c     Assemble dpdq = ifft(dpFTdq)
c   

cc    window function energy preserving factor

      odd=.true.
      if (mod(Nt,2).eq.0) odd=.false.

      if (odd) then
         Nth=(Nt+1)/2;         
      else 
         Nth=Nt/2+1;
      endif
 
cc    window function energy preserving factor

      scal=0.d0
      do l=1,Nt
         scal=scal+win(l)**2     
      end do
      scal=SQRT(scal/Nt)

cc    initialize omega and wsave for FFts
    
      do l=1,Nth
         omeg(l)=2.d0*pi*dble(l-1)/(Nt*deltat)
      end do 

      call zffti(Nt,wsave)

cc    sum to account for subtracting the mean

      do l=1,Nth
         facc2(l)=(0.d0,0.d0)
         do n=1,Nt
            facc2(l)=facc2(l)+CDEXP(-ci*omeg(l)*DBLE(n-1)*deltat)*win(n)
         end do
         facc2(l)=facc2(l)/Nt/scal
      end do  





cc    loop over all observer and spatial source points

      do k=1,Nobs
      do j=1,Nss

cc       account for fourier trafo and assemble dpFTdq
cc       (throw out upper half of frequencies and compensate by doubling the lower half)
cc       also apply window function to source terms Qs,Fs1 and Fs2 after subtracting their mean 
cc       Note that dQdq(j,2)=ns(j,1) and dQdq(j,3)=ns(j,2)

         do l=1,Nth

            do n=1,4
               dpFTdq(l,n)=dpod(k,l,j,2)*dF1dq(j,n) + 
     &                     dpod(k,l,j,3)*dF2dq(j,n) 
            end do             
            dpFTdq(l,2) = dpFTdq(l,2) + dpod(k,l,j,1)*ns(j,1)
            dpFTdq(l,3) = dpFTdq(l,3) + dpod(k,l,j,1)*ns(j,2)

            
            facc=win(tnow)/scal * CDEXP(-ci*omeg(l)*DBLE(tnow-1)*deltat) 
     &          - facc2(l)

            if (odd .or. l.le.Nth-1)   facc=2.d0*facc   !this accounts for doubling the lower half
            
            do n=1,4
               dpFTdq(l,n) = dpFTdq(l,n)*facc
            end do

         end do
 
cc       inverse fourier trafo 

         do n=1,4

            do l=1,Nt   
               tmp(l)=(0.d0,0.d0)
            end do
            
            do l=2,Nth   
               tmp(l)=dpFTdq(l,n)
            end do
            
            call zfftb(Nt,tmp,wsave)
            
            do l=1,Nt   
               dpdq(k,l,j,n)=DREAL(tmp(l))/Nt
            end do

         end do


      end do     ! loop over all spatial source points
      end do     ! loop over all observer points
          



c
c     Calculate dJdQ = dJdp * dpdq
c

      if (iobjf .eq. 10) then

         namelen =  strlen(grid_file_prefix)
         filena = grid_file_prefix
         filena(namelen+1:namelen+7) = '.fwhcp'
         open(unit=n_all,file=filena,status='unknown')
         
         do j=1,Nobs
            do i=1,Nt
               read (n_all,*) po_tar(i,j)
            end do
         end do
      
         close(n_all)

     
         do k=1,Nobs
            
            do l=1,Nt         
               dJdp(l,k)=2.d0 * ( po(l,k)- po_tar(l,k) ) / scalef
            end do

         end do

      else if (iobjf .eq. 11) then
     
         do k=1,Nobs

            poav(k)=0.d0
            do l=1,Nt         
               poav(k)=poav(k)+po(l,k)
            end do
            poav(k)=poav(k)/Nt
            
            do l=1,Nt         
               dJdp(l,k)=2.d0 * ( po(l,k)-poav(k) ) / scalef
            end do

         end do

      end if


      do j=1,Nss
         do n=1,4
            dJdQtmp(j,n)=0.d0
            do k=1,Nobs
               do l=1,Nt
                  dJdQtmp(j,n) = dJdQtmp(j,n)+dJdp(l,k)*dpdq(k,l,j,n)
               end do
            end do
         end do
      end do


c
c     Cast back into (j,k) index form 
c

      sc=0

      do i=1,Nsp

         if (Spvec(i,2) .gt. Spvec(i,3)) then
            jdir=-1
            jadd=-1
         else if (Spvec(i,2) .lt. Spvec(i,3)) then
            jdir=1
            jadd=1
         else
            jdir=2  !cannot be zero, value doesn't matter
            jadd=0
         end if
         if (Spvec(i,4) .gt. Spvec(i,5)) then
            kdir=-1
            kadd=-1
         else if (Spvec(i,4) .lt. Spvec(i,5)) then
            kdir=1
            kadd=1
         else
            kdir=2 !cannot be zero, value doesn't matter
            kadd=0
         end if

c        Start at the intended index for the first block
         if (i .eq. 1) then
            jadd=0
            kadd=0
         end if

         jdimtmp=jmax(Spvec(i,1))
         jkdimtmp=jdimtmp*kmax(Spvec(i,1))
         ptrtmp=lqptr(Spvec(i,1))

         do j=Spvec(i,2)+jadd,Spvec(i,3),jdir
            do k=Spvec(i,4)+kadd,Spvec(i,5),kdir
               
               sc=sc+1
               
               index1=ptrtmp+(j-1)+(k-1)*jdimtmp
               index2=index1+(2-1)*jkdimtmp
               index3=index1+(3-1)*jkdimtmp
               index4=index1+(4-1)*jkdimtmp
               
               dJdQ(index1)=dJdQtmp(sc,1)
               dJdQ(index2)=dJdQtmp(sc,2)
               dJdQ(index3)=dJdQtmp(sc,3)
               dJdQ(index4)=dJdQtmp(sc,4)


            end do
         end do
         
      end do


      deallocate(dpdq)

      return
      end   !calcdJdQnoise











      subroutine calcobjair5fd(fobj,qin,nout,skipend,x,y,xy,xyj,scalef,
     &     count)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer nout,skipend,ii,ib,lg,lq,nscal,count,j

      double precision qin(*),q(maxjkq),x(*),y(*),xyj(*),xy(*)
      double precision fobj,scalef,cdmean,clmean
      double precision press(maxjk),sndsp(maxjk),precon(maxjk*8)

      double precision thc,goc,dummy(maxjk)
      
      fobj= 0.0d0      
      do j = 1,maxjkq
         q(j)=0.d0
      end do
      cdmean=0.0d0
      clmean=0.0d0

      do ii=skipend+1,nout

         if (ii.ne.count) then
c           -- read physical flow field  --
            call iomarkus(3,ii,maxj,maxk,q,xyj,dummy)

c           -- halo column copy for q vector --
            call halo_q(1,nblks,maxj,maxk,q,dummy)
         else
            do j = 1,maxjkq
               q(j)=qin(j)
            end do
         end if 

c        -- calculate pressure and sound speed --
         do ib=1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call calcps(ib,jmax(ib),kmax(ib),q(lq),press(lg),sndsp(lg),
     &           precon(lprecptr(ib)),xy(lq),xyj(lg),jbegin2(ib),
     &           jend2(ib),kbegin2(ib),kend2(ib))
         end do

c        -- calculate lift and drag at this time step
         nscal=1
         call clcdmb(q,press,x,y,xy,xyj,nscal,.false.)

         cdmean = cdmean + cdt
         clmean = clmean + clt           
      
      end do

c     -- calculate mean lift and drag
      cdmean = cdmean/real(nout-skipend)
      clmean = clmean/real(nout-skipend)

c     -- calculate (mean drag / mean lift)
      fobj =  cdmean/clmean

c     scaling of the objective function

      if (scalef .eq. 0.d0)  scalef=fobj        
      fobj=fobj/scalef


      if (.not. sbbc) then
c     -- add constraints

         call constraints(thc,goc)
         fobj = fobj + thc + goc

      end if


      return
      end    !calcobjair5fd




      subroutine calcobjair8fd(fobj,deltat,scalef,x,y,
     &     q,cptar)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      double precision cptar(4*maxj),q(maxjkq),x(maxjk),y(maxjk)
      double precision dfobjdQ(maxjkq)

      integer j,k,ib,lq,lg,nx,ny

      double precision fobj,deltat,thc,goc,scalef

      double precision xwant(maxj),ywant(maxk)
      double precision xval(4,maxj),pval(4,maxj)
      integer num(4,maxj,3),len(4)
      double precision dpdQx1(maxk,3,4),dpdQy1(maxj,3,4)
      double precision dpdQx2(maxk,3,4),dpdQy2(maxj,3,4)

      fobj= 0.0d0

      nx=NINT((remxmax-remxmin)/remdx+1.d0)
      ny=NINT((remymax-remymin)/remdy+1.d0)
      do j = 1,nx
         xwant(j)=remxmin+remdx*dble(j-1)
      end do
      do k = 1,ny
         ywant(k)=remymin+remdy*dble(k-1)
      end do

      do k=1,4
         len(k)=0
      end do

      do ib = 1,nblks
         lq = lqptr(ib)
         lg = lgptr(ib)
         call interpolateboxyx(jmax(ib),kmax(ib),q(lq),ib,
     &        kminbnd(ib),kmaxbnd(ib),jminbnd(ib),jmaxbnd(ib),x(lg),
     &        y(lg),xval,pval,len,num,dpdQx1,dpdQy1,dpdQx2,
     &        dpdQy2,.false.)
      end do

       call interpolatebox(fobj,cptar,xval,pval,len,num,xwant,
     &        ywant,nx,ny,deltat,scalef,dpdQx1,dpdQy1,dpdQx2,dpdQy2,
     &        dfobjdQ,.false.)

c     scaling of the objective function
        
      fobj=fobj/scalef 

c     -- add constraints

      call constraints(thc,goc)

      fobj = fobj + thc + goc

   
 
      return
      end    !calcobjair8fd






      subroutine calcobjairnoisefd(fobj,qin,count,nout,skipend,deltat,
     &     scalef,x,y,ns)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/index.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer nout,skipend,j,k,i,jdir,kdir,index,ii,count
      integer index1,index2,index3,index4,jdimtmp,jkdimtmp,ptrtmp
      integer tc,sc,Nsous,Nsout,strlen,namelen,jadd,kadd
      double precision fobj,deltat,scalef,x(maxjk),y(maxjk),dummy(maxjk)
      double precision po(tdim,sdim),win(tdim),ns(sdim,2)
      double precision xo(sdim),yo(sdim),q(maxjkq),xyj(maxjk),den
      double precision ps(tdim,sdim),us(tdim,sdim),vs(tdim,sdim)
      double precision rhos(tdim,sdim),xs(sdim),ys(sdim),thc,goc
      double precision pcomp(tdim),po_tar(tdim,sdim),poav(sdim)
      double precision qin(maxjkq)
      complex*16,allocatable ::  dpod(:,:,:,:)   
      logical deriv

      allocate(dpod(Nobs,tdim,sdim,3))

      fobj= 0.0d0

c
c     Save xs and ys the defining values for the surface.
c
      sc=0

      do i=1,Nsp

         if (Spvec(i,2) .gt. Spvec(i,3)) then
            jdir=-1
            jadd=-1
         else if (Spvec(i,2) .lt. Spvec(i,3)) then
            jdir=1
            jadd=1
         else
            jdir=2  !cannot be zero, value doesn't matter
            jadd=0
         end if
         if (Spvec(i,4) .gt. Spvec(i,5)) then
            kdir=-1
            kadd=-1
         else if (Spvec(i,4) .lt. Spvec(i,5)) then
            kdir=1
            kadd=1
         else
            kdir=2 !cannot be zero, value doesn't matter
            kadd=0
         end if

c        Start at the intended index for the first block
         if (i .eq. 1) then
            jadd=0
            kadd=0
         end if

         jdimtmp=jmax(Spvec(i,1))
         jkdimtmp=jdimtmp*kmax(Spvec(i,1))
         ptrtmp=lqptr(Spvec(i,1))

         do j=Spvec(i,2)+jadd,Spvec(i,3),jdir
            do k=Spvec(i,4)+kadd,Spvec(i,5),kdir

               sc=sc+1

               index=lgptr(Spvec(i,1))+(j-1)+(k-1)*jdimtmp

               xs(sc)=x(index)
               ys(sc)=y(index)

            end do
         end do

      end do

      Nsous=sc

c
c     Calculate pressure, velocities, and density along the FWH control surface 
c     
      tc=0

      do ii=skipend+1,nout

         if (ii.ne.count) then
c           -- read physical flow field  --
            call iomarkus(3,ii,maxj,maxk,q,xyj,dummy)

c           -- halo column copy for q vector --
            call halo_q(1,nblks,maxj,maxk,q,dummy)
         else
            do j = 1,maxjkq
               q(j)=qin(j)
            end do
         end if 

         sc=0
         tc=tc+1 

         do i=1,Nsp

            if (Spvec(i,2) .gt. Spvec(i,3)) then
               jdir=-1
               jadd=-1
            else if (Spvec(i,2) .lt. Spvec(i,3)) then
               jdir=1
               jadd=1
            else
               jdir=2           !cannot be zero, value doesn't matter
               jadd=0
            end if
            if (Spvec(i,4) .gt. Spvec(i,5)) then
               kdir=-1
               kadd=-1
            else if (Spvec(i,4) .lt. Spvec(i,5)) then
               kdir=1
               kadd=1
            else
               kdir=2           !cannot be zero, value doesn't matter
               kadd=0
            end if

c           Start at the intended index for the first block
            if (i .eq. 1) then
               jadd=0
               kadd=0
            end if

            jdimtmp=jmax(Spvec(i,1))
            jkdimtmp=jdimtmp*kmax(Spvec(i,1))
            ptrtmp=lqptr(Spvec(i,1))

            do j=Spvec(i,2)+jadd,Spvec(i,3),jdir
               do k=Spvec(i,4)+kadd,Spvec(i,5),kdir

                  sc=sc+1

                  index1=ptrtmp+(j-1)+(k-1)*jdimtmp
                  index2=index1+(2-1)*jkdimtmp
                  index3=index1+(3-1)*jkdimtmp
                  index4=index1+(4-1)*jkdimtmp

                  rhos(tc,sc)=q(index1)
                  us(tc,sc)=q(index2)/q(index1)
                  vs(tc,sc)=q(index3)/q(index1)
                  ps(tc,sc)=gami*(q(index4)-0.5*
     &                 (q(index2)**2+q(index3)**2)/q(index1))


               end do
            end do

         end do
      
      end do   !Calculate quantities along the FWH control surface


      Nsout=tc
    

c
c     Set up observer points, we are interested in 
c

      if ( abs(xobs).gt.0.d0 .or. abs(yobs).gt.0.d0 ) 
     &     then

         xo(1)=xobs
         yo(1)=yobs

      else

         do j=1,Nobs
            index=lgptr(blobs)+(jobs+j-1-1)+(kobs-1)*jmax(blobs)
            xo(j)=x(index)
            yo(j)=y(index)
         end do

      end if

c
c     For directivity plots, otherwise make sure it is commented out...
c
c$$$      Nobs=56
c$$$      do j=1,Nobs
c$$$         xo(j)=128.0d0*DCOS(2.d0*4.d0*atan(1.d0)*(j-1)/DBLE(Nobs))
c$$$         yo(j)=128.0d0*DSIN(2.d0*4.d0*atan(1.d0)*(j-1)/DBLE(Nobs))
c$$$      end do


c     Calculate the pressure po in these observer points via the FWH approach
c

      call calcFWH(Nsous,Nsout,xo,yo,po,xs,ys,ns,rhos,us,vs,ps,
     &     deltat,dpod,win,deriv,pcomp)


c
c     Calculate the objective function
c

      if (iobjf.eq.10) then

         namelen =  strlen(grid_file_prefix)
         filena = grid_file_prefix
         filena(namelen+1:namelen+7) = '.fwhcp'
         open(unit=n_all,file=filena,status='unknown')

         do j=1,Nobs
            do i=1,Nsout
               read (n_all,*) po_tar(i,j)
            end do
         end do
      
         close(n_all)
         
         do j=1,Nobs
            do i=1,Nsout
         
               fobj =  fobj + (po(i,j)-po_tar(i,j))**2
               
            end do
         end do
      
      else if (iobjf.eq.11) then
     
         do j=1,Nobs
            poav(j)=0.d0
            do i=1,Nsout         
               poav(j)=poav(j)+po(i,j)
            end do
            poav(j)=poav(j)/Nsout
         end do
   
         do j=1,Nobs
            do i=1,Nsout
            
               fobj =  fobj + (po(i,j)-poav(j))**2

            end do
         end do

      end if


c     scaling of the objective function
     
      fobj=fobj/scalef

      deallocate(dpod)

      return
      end    !calcobjairnoisefd

