C     
c  Evolver
C
      LOGICAL PARML,parml_chm,debug,geneprec
      LOGICAL CBEST,scale_genes,trans_minmax,compute_pass_thu
      logical box_over,beta_sin
      logical io_34,fit_min_max,arc2d_io,rand_init_fit
      real*8 rand
      SAVE
C
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C                                                                      C
C                                                                      C
C          *** GENETIC ALGORITHM (GA) ***                              C
C                                                                      C
C                                                                      C
C          *** THIS COMPUTER PROGRAM USES A GENETIC ALGORITHM          C
C              FOR OPTIMIZATION PROBLEMS ***                           C
C
C                                                                      C
C                                                                      C
C          *** WRITTEN BY:  DR. TERRY L. HOLST                         C
C                           RESEARCH SCIENTIST                         C
C                           DR. THOMAS H. PULLIAM                      C
C                           RESEARCH SCIENTIST                         C
C                           NUMERICAL AEROSPACE SIMULATION (NAS)       C
C                              SYSTEMS DIVISION (CODE IN)              C
C                           MAIL STOP T27B-1                           C
C                           NASA AMES RESEARCH CENTER                  C
C                           MOFFETT FIELD, CALIFORNIA 94035-1000       C
C                           (650) 604-6032 VOICE                       C
C                           (650) 604-1095 FAX                         C
C                           E-MAIL: tholst@mail.arc.nasa.gov           C
C
C          *** VERSION 0.1, Feb.  2002 ***                             C
C          *** VERSION 0.2, Feb.  2002 ***                             C
C          *** VERSION 0.3, Mar.  2002 ***                             C
C          *** VERSION 0.4, Mar. 19  2002 ***                          C
C          *** VERSION 1.6, Feb. 23  2005 ***                          C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C                                                                      C
C          *** CHRONOLOGICAL DEVELOPMENT OF GA ***                     C
C                                                                      C
C          *** VERSION 1.0  (JULY 2000)....THE FIRST                   C
C              PRELIMINARY VERSION OF GA WAS DEVELOPED                 C
C              WAS DEVELOPED TO PERFORM FUNCTION OPTIMIZATIONS         C
C              AND TO DEVELOP THE BASIC FEATURES OF THE                C
C              GENETIC OPTIMIZATION ALGORITHM USING REAL               C
C              NUMBER ENCODING ***                                     C
C          *** VERSION 1.1  (JANUARY 2001)....MULTIPLE                 C
C              OBJECTIVE FUNCTION CAPABILITY LEADING TO THE            C
C              GENERATION OF A PARETO FRONT WAS ADDED IN THIS          C
C              VERSION. GOLDBERG RANKING IS USED. IN ADDITION          C
C              THE LOGIC WAS REWRITTEN IN A MODULAR FASHION            C
C              ALLOWING PARALLEL IMPLEMENTATION ON A                   C
C              DISTRIBUTED SYSTEM ***                                  C
C          *** VERSION 1.2 (APRIL 2001)....ACCUMULATION FILE           C
C              LOGIC ADDED WHICH HELPS IN DEFINING PARETO              C
C              FRONTS FOR MULTI-OBJECTIVE COMPUTATIONS                 C
C                                                                      C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** GA CODE OPERATION ***                                   C
C                                                                      C
C              PARAMETER INPUT FOR THE GA CODE IS                      C
C              ACCOMPLISHED USING ONE INPUT FILE: ga.i                 C
C              (UNIT 51). THIS FILE IS MANDATORY FOR ALL               C
C              CASES AND MUST RESIDE IN THE DIRECTORY                  C
C              FROM WHICH THE GA CODE EXECUTION IS                     C
C              INITIATED (OR, E.G., BE ACCESSIBLE VIA THE              C
C              PROPER PATH NAME IN A SCRIPT FILE). AN                 C
C              ADDITIONAL FILE (chromo.xx) IS REQUIRED                 C
C              FOR EACH CHROMOSOME AND IS USED TO COMMUNICATE          C
C              INFORMATION BACK AND FORTH BETWEEN THE GA               C
C              ROUTINE AND THE FUNCTION EVALUATION ROUTINE(S).         C
C              THE chromo.xx FILES will AUTOMATICALLY BE               C
C              INITIALIZED WHEN IPOP (THE GA GENERATION OR              C
C              ITERATION INDEX) IS SET TO ZERO.                        C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
C
C          *** A COMPLETE ALPHABETIZED LIST OF NAMELIST 
C              INPUT PARAMETERS AND THEIR DEFINITIONS IS
C              AS FOLLOWS ***
C
C     BETA.....REAL NUMBER PARAMETER USED TO CONTROL THE SIZE OF
C              PERTURBATIONS IN THE PERTRUBATION MUTATION 
C              OPERATOR (0.0<BETA<1.0).
C              (DEFAULT = 0.3)
C     GENEMIN..SINGLE SUBSCRIPT REAL NUMBER ARRAY CONTAINING THE
C              MINIMUM VALUES FOR EACH OF THE GENES. 
C     GENEMAX..SINGLE SUBSCRIPT REAL NUMBER ARRAY CONTAINING THE
C              MAXIMUM VALUES FOR EACH OF THE GENES. 
C     IACCUM...INTEGER PARAMETER USED TO DETERMINE WHETHER OR NOT
C              THE ACCUMULATION FILE LOGIC WITHIN SUBROUTINE RANK
C              IS ACTIVATED. THIS LOGIC ONLY MAKES SENSE WHEN 
C              NOBFUN>1 (MULTI-OBJECTIVE CASES).
C              (DEFAULT = 0)
C              IACCUM=0...NO ACCUMULATION FILE
C              IACCUM=1...ACCUMULATION FILE ACTIVATED
C     IPARM....SINGLE SUBSCRIPT INTEGER ARRAY CONTAINING NPI
C              INTEGER VALUES PASSED BETWEEN GA AND THE
C              FUNCTION EVALUATION CODE(S) VIA THE chromo.i
C              FILES. IPARM(1) IS ALWAYS THE NUMBER OF FUNCTION
C              EVALUATIONS. OTHER PARAMETERS ARE CASE DEPENDENT
C              (IPARM(1) = 0)
C     ISELECT..INTEGER PARAMETER USED TO DETERMINE WHICH TYPE
C              OF SELECTION ALGORITHM IS USED.
C              (DEFAULT = 1)
C              ISELECT=1...GREEDY SELECTION.
C                          MULTIPLE PASS SELECTION. IN THIS
C                          APPROACH THE NEXT GENERATION OF
C                          CHROMOSOMES ARE SELECTED FROM THE
C                          PREVIOUS GENERATION BY USING 
C                          MULTIPLE PASSED THROUGH THE 
C                          POPULATION LIST. ON THE FIRST PASS
C                          ALL IRANK=1 CHROMOSOMES ARE INCLUDED
C                          ON THE SECOND PASS ALL IRANK.LE.2
C                          ARE INCLUDED, AND SO ON, UNTIL 
C                          NCHROM CHROMOSOMES ARE SELECTED.
C              ISELECT=2...TOURNAMENT SELECTION. THE FIRST 
C                          NOBFUN CHROMOSOMES SELECTED ARE 
C                          THOSE THAT HAVE THE NOBFUN MAXIMUM
C                          FITNESS VALUES. THE NOBFUN+1 
C                          CHROMOSOME SELECTED IS THE ONE THAT
C                          HAS THE BEST NORMALIZED RMS FITNESS.
C                          THE REMAINING CHROMOSOMES (NOBFUN+2,
C                          NOBFUN+3,...,NCHROM) ARE SELECTED
C                          USING A STANDARD RANDOM TOURNAMENT
C                          PROCESS ***
C     NGENE....NUMBER OF GENES
C              (DEFAULT = 10)
C     NOBFUN...NUMBER OF OBJECTIVE FUNCTIONS
C              (DEFAULT = 1)
C     NOUT.....OUTPUT CONTROL PARAMETER
C              NOUT=0...GA OUTPUT DEACTIVATED
C              NOUT=1...GA OUTPUT ACTIVATED
C              (DEFAULT = 1)
C     NPI......NUMBER OF ACTIVE INTEGER PARAMETERS IN THE
C              IPARM ARRAY
C              (DEFAULT = 1)
C     NPL......NUMBER OF ACTIVE LOGICAL PARAMETERS IN THE
C              PARML ARRAY
C              (DEFAULT = 1)
C     NPR......NUMBER OF ACTIVE INTEGER PARAMETERS IN THE
C              PARMR ARRAY
C              (DEFAULT = 3)
C     PARMR....SINGLE SUBSCRIPT REAL ARRAY CONTAINING NPR
C              REAL NUMBER VALUES PASSED BETWEEN GA AND THE
C              FUNCTION EVALUATION CODE(S) VIA THE chromo.i
C              FILES. PARMR(1) IS ALWAYS THE CPU TIME 
C              ACCUMULATED BY THE CURRENT CHROMOSOME NUMBER.
C              (DEFAULT = 1)
C     PARML....SINGLE SUBSCRIPT LOGICAL ARRAY CONTAINING NPL
C              LOGICAL VALUES PASSED BETWEEN THE GA AND THE
C              FUNCTION EVALUTION CODE(S) VIA THE chromo.i
C              FILES. PARML(1) IS ALWAYS A PARAMETER THAT
C              TELLS THE FUNCTION EVALUATION ROUTINE IF
C              A FUNCTION EVALUATION NEEDS TO BE PERFORMED
C              OR NOT. 
C              PARML(1)=.TRUE....PERFORM FUNCTION EVALUATION
C              PARML(10=.FALSE...DO NOT PERFORM FUNCTION
C                                EVALUATION
C              (DEFAULT = .TRUE.)
C     XBEST....PERCENT OF THOSE INDIVIDUALS IN THE CURRENT
C              POPULATION THAT ARE PASSED THROUGH TO THE 
C              NEXT GENERATION WITHOUT CROSSOVER OF MUTATION 
C              (0.0<XBEST<100.0).
C              (DEFAULT = 10.0)
C     XAVG.....PERCENT OF THOSE INDIVIDUALS IN THE NEW
C              POPULATION THAT ARE DETERMINED USING THE 
C              RANDOM AVERAGE CROSSOVER OPERATOR. 
C              (0.0<XAVG<100.0)
C              (DEFAULT = 20.0)
C     XPERT....PERCENT OF THOSE INDIVIDUALS IN THE NEW
C              POPULATION THAT ARE DETERMINED USING THE
C              PERTURBATION MUTATION OPERATOR. 
C              (0.0<XPERT<100.0)
C              (DEFAULT = 30.0)
C     XMUT.....PERCENT OF THOSE INDIVIDUALS IN THE NEW
C              POPULATION THAT ARE DETERMINED USING THE
C              RANDOM MUTATION OPERATOR. 
C              (0.0<XMUT<100.0)
C              (DEFAULT = 40.0)
C
c     cbest... for the nbest chromosomes the default is to 
c                  set parml(1) = false so that the passed through
c                  chromosomes are not recomputed (this assumes that
c                  the fitness won't change)
c                  if cbest = true it forces parml(1) = true
c                  so that all the chromosomes get recomputed
c                  (default = false)
c
c     sig_share...niching parameter
c                 0.0  :  niching turned off (Default)
c                 > 0.0:  sig_share is maximum phenotypic distance 
c                         allowed between two individuals to become 
c                         members of a niche 
c     
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C
C          *** AN ALPHABETIZED LIST OF SELECTED PROGRAM  
C              VARIABLES THAT ARE NOT DEFINED ABOVE IN THE
C              LIST OF NAMELIST VARIBLES INCLUDE ***
C
C
C      IPOP....GENERATION LEVEL (MAIN DO LOOP ITERATION VARIABLE). 
C              THIS INTEGER PARAMETER IS READ IN THE FIRST
C              FIELD OF A THREE-FIELD FREE-FORMAT READ AT THE
C              BEGINNING OF GA EXECUTION ***
C              IPOP=0.....GENETIC ALGORITHM INITIALIZATION.
C                         WRITE ONE chromo.i FILE FOR EACH 
C                         CHROMOSOME. THE FITNESS VALUES 
C                         WILL ALL BE ZERO (ASSUMES
C                         OPTIMIZATION IS A MAXIMIZATION).
C              IPOP>0.....ADVANCE GENETIC ALGORITHM ONE 
C                         GENERATION. WRITE UPDATED chromo.i 
C                         FILE FOR EACH CHROMOSOME. THE 
C                         GENES WILL BE NEW AND THE FITNESS
C                         VALUES WILL BE OLD.
C              IPOP=NPOP..COMPLETE STATISTICS ON THE FINAL  
C                         GA ITERATION, WRITE SOLUTION AND 
C                         STOP.
C    NCHROM....NUMBER OF CHROMOSOMES (NUMBER OF INDIVIDUALS IN
C              EACH POPULATION. THIS INTEGER PARAMETER IS READ 
C              IN THE THIRD FIELD OF A THREE-FIELD FREE-FORMAT 
C              READ AT THE BEGINNING OF GA EXECUTION ***
C      NPOP....FINAL GENERATION NUMBER FOR THE CURRENT GA 
C              EXECUTION. THIS INTEGER PARAMETER IS READ IN THE
C              SECOND FIELD OF A THREE-FIELD FREE-FORMAT READ 
C              AT THE BEGINNING OF GA EXECUTION ***
c
c     ga_file_prefix....Prefix for chromo.i files, 
c                       i.e. ga_file_prefix.i files (D = crhomo)
C 
C
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C
      dimension pop(100,2000),fit(10,2000)
      dimension iparm(20),parmr(20),parml(10)
      dimension iparm_chm(20,2000),parmr_chm(20,2000),parml_chm(10,2000)
      dimension irank(2000)
      dimension fit_acc(10,20000), i_acc(2,20000)
      dimension pop_acc(100,20000)
      dimension tw(100,100),tu(100,100),tr(100,100)
      dimension genemin(100), genemax(100), Igeneprec(100)
      dimension int_real(100)
      character*40 field_pop(100),field_fit(100),field_log,
     $     field_ngene,field_nobfun

      dimension  FITMAX(10),  FITAVG(10),  FITMIN(10),
     $     NCFITMAX(10),  NCFITMIN(10)
      dimension CPUTI(2000),             NFUN(2000)
      logical compute_fit(2000)

      character*80 ga_file_prefix
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      max_acc_dim = 10000
C-----------------------------------------------------------------------
C          *** READ MAIN PARAMETERS CONTROLING THE GA PROCESS. 
C              WRITE THE PARAMETERS (LATER) BUT ONLY IF IPOP=0 ***
C
      dummy = 0.0
      e_cpu_0 = timer_seconds(dummy)
      nfuneval = 0
      cputime = 0.0

      call get_params(ipop,npop,nchrom,ga_file_prefix)

C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** READ INPUT 
      CALL INPUT(IPOP,i_ran_seed,nchrom,ngene,nout,npop,beta,beta_sin,
     $     nobfun,NPI,NPR,NPL,IPARM,PARMR,PARML,iaccum,iselect,
     $     beta_min,beta_max,n_beta_s,n_beta_i,i_mnx,fit_init,
     $     init_ga,debug,ga_file_prefix,itran,scale_genes,
     $     trans_minmax,nbin,is_bin,icull,nbin_cull,
     $     io_34,fit_min_max,genemin,genemax,Igeneprec,geneprec,
     $     eps_domin,int_real,arc2d_io,field_pop,field_fit,field_log,
     $     field_ngene,field_nobfun,rand_init_fit, i_avg_cho,
     $     CBEST,  sig_share, box_over, prob_pert, prob_mut, prob_avg,
     $     XBEST,      XAVG,       XPERT,      XMUT,
     $     NBEST,      NAVG,       NPERT,      NMUT,  compute_pass_thu)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** INITIALIZE THE RANDOM NUMBER GENERATOR SO THAT
C              EACH TIME RAN3 IS CALLED WITH A UNIQUE SEED (IDUM)
C              A NEW SEQUENCE OF RANDOM NUMBERS IS GENERATED ***
C
      if(i_ran_seed.ge.0)IDUM = -IPOP-1-i_ran_seed
      if(i_ran_seed.lt.0)IDUM = i_ran_seed
      CALL RAN3(IDUM,RAND)

c     ESTABLISH INITIAL POPULATION 
C     DISTRIBUTION. IF IPOP>0, SKIP POPULATION
C     INITIALIZATION ***
C
      if(IPOP.eq.0)then
         CALL INITGA(IPOP,nchrom,ngene,nobfun,pop,fit,i_mnx,
     $        fit_init,init_ga,genemin,genemax,rand_init_fit,
     $        fitavg,fitmin,fitmax,compute_fit,i_ran_seed)
         call ga_int_conv(nchrom,ngene,pop,int_real)
         if(geneprec)call ga_precision(nchrom,ngene,pop,igeneprec,debug)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** WRITE INITIAL SOLUTION STATISTICS ***
C
         CALL SOLOUT(IPOP,nchrom,ngene,nobfun,pop,fit,irank,nout,
     $        i_mnx,io_34,ga_file_prefix,cputime,fitmin,fitmax,nfuneval,
     $        arc2d_io)
      endif
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** ADVANCE FROM ONE GENERATION TO THE NEXT (IPOP>0) ***
C-----------------------------------------------------------------------
C          *** READ chromo.i FILES THAT INCLUDE NEW FITNESS VALUES ***
C
      IF (IPOP.GT.0) THEN
         IRW = 1
         CALL RWCHROMO(nchrom,NGENE,nobfun,POP,FIT,
     $        NPI,IPARM,IPARM_chm,NPR,PARMR,
     $        PARMR_chm,NPL,PARML,PARML_chm,
     $        IPOP,IRW,NOUT,fitmin,fitmax,ga_file_prefix,
     $        cputime,nfuneval,cputi,nfun,nbest,imove,cbest,debug,
     $        io_34,fit_min_max,arc2d_io,field_pop,field_fit,field_log,
     $        field_ngene,field_nobfun,compute_fit,compute_pass_thu)
C
C-----------------------------------------------------------------------
C          *** COMPUTE FITNESS STATISTICS, READ PARETO FRONT
C              ACCUMMULATION FILE (IPOP>1, NOBFUN>1, IACCUM=1),
C              ESTABLISH RANKINGS (IRANK) FOR THE OLD GENERATION,
C              AND FINALLY WRITE THE NEW PARETO FRONT ACCUMULATION
C              FILE (NOBFUN>1, IACCUM=1). WRITE SOLUTION. IF
C              IPOP=NPOP STOP ***
C
         CALL STATS(IMOVE,nchrom,ngene,nobfun,pop,fit,fit_init,i_mnx,
     $        fitmin,fitmax,fitavg,ncfitmin,ncfitmax,
     $        fitrmsmin,fitrmsmax)

         IF (IPOP.GT.1.AND.NOBFUN.GT.1.AND.IACCUM.EQ.1) THEN
            IRW = 1             ! readin accumulation file
            CALL RWPARETO(IPOP,max_acc_dim,NOBFUN,NACC,I_ACC,FIT_ACC,
     $           ngene,pop_acc,IRW,io_34,ga_file_prefix,
     $           arc2d_io,field_pop,field_fit,field_log,
     $           field_ngene,field_nobfun)
         ENDIF

c     ranking, including pareto ranking
         CALL RANK(IPOP,nchrom,ngene,nobfun,pop,fit,irank,
     $        max_acc_dim,nacc,pop_acc,fit_acc,i_acc,iaccum,
     $        i_mnx,fit_init,eps_domin,debug)
         
         IF (NOBFUN.GT.1.AND.IACCUM.EQ.1) THEN
            if(ICULL.ne.0 .and. nacc.gt.icull)
     $           call CULL_N(ndim,nbin_cull,NACC,POP_ACC,FIT_ACC,
     $           I_ACC,NGENE,NOBFUN,ICULL,I_MINMAX)
            IRW = 2             ! output accumulation file
            CALL RWPARETO(IPOP,max_acc_dim,NOBFUN,NACC,I_ACC,FIT_ACC,
     $           ngene,pop_acc,IRW,io_34,ga_file_prefix,
     $           arc2d_io,field_pop,field_fit,field_log,
     $           field_ngene,field_nobfun)
         else
            call rwbest(ipop,ndim,nchrom,nobfun,ngene,pop,fit,irank,
     $           NPI,IPARM_chm,NPR,PARMR_chm,NPL,PARML_chm,
     $           fitmin,fitmax,io_34,fit_min_max,ga_file_prefix,
     $           arc2d_io,field_pop,field_fit,field_log,
     $           field_ngene,field_nobfun)
         ENDIF

         CALL SOLOUT(IPOP,nchrom,ngene,nobfun,pop,fit,irank,nout,
     $        i_mnx,io_34,ga_file_prefix,cputime,fitmin,fitmax,nfuneval,
     $        arc2d_io)

cthp 5/25/2001  Niching code:  re-ranks data based on niching algorithm
c                              should be called at this stage since it
c                              changes irank which is used in select
         if(sig_share.ne.0.0)then
            if(i_mnx.eq.0)then
               Print *,'Niching not working for minimization yet!!'
               STOP 'Niching not working for minimization yet!!'
            endif
            call ga_niche(
     $           ipop,nchrom,nobfun,irank,fit,fitmax,fitmin,sig_share,
     $           i_mnx)
         endif


         IF (IPOP.EQ.NPOP) STOP ' retreat'
C
C-----------------------------------------------------------------------
C     *** SELECT NEXT GENERATION OF CHROMOSOMES ***
C  
        call SELECT(ipop,nchrom,ngene,nobfun,pop,fit,irank,iselect,
     $        max_acc_dim,pop_acc,fit_acc,nacc,i_mnx,fit_init,
     $        nbin,is_bin,
     $        fitmin,fitmax,fitavg,ncfitmin,ncfitmax,ncrmsmin,ncrmsmax,
     $        fitrmsmin,fitrmsmax,debug)
C-----------------------------------------------------------------------
c  sort chromsome based on rank
c

        if(itran.eq.0)
     $       call SORT_rank(irank,NOBFUN,NCHROM,FIT,ngene,pop,
     $       debug,i_mnx)

C-----------------------------------------------------------------------
C          *** OPTION TO TRANSFORM EACH CHROMOSOME (ZZZ) IN GENE
C              SPACE TO MAKE THE MODIFICATION PROCESS, WHICH IS
C              PERFORMED NEXT, MORE EFFICIENT. DO THIS ONLY FOR
C              MULTI-OBJECTIVE CASES (NOBFUN>1) IF ITRAN=1 ***
C
        IF (itran.ne.0 .and. (ipop.ge.itran .AND. NOBFUN.GT.1)) THEN
           NNN   = 0
           ITRAN_S = 2
           CALL TRANSFORM(NNN,NGENE,NCHROM,POP,TW,TU,TR,ITRAN_S,
     $          genemin,genemax,scale_genes,debug)
           if(trans_minmax)then
              ITRAN_S = 1
              NCH   = 1
              CALL TRANSFORM(NNN,NGENE,NCH,GENEMIN,TW,TU,TR,ITRAN_S,
     $             genemin,genemax,scale_genes,debug)
              CALL TRANSFORM(NNN,NGENE,NCH,GENEMAX,TW,TU,TR,ITRAN_S,
     $             genemin,genemax,scale_genes,debug)
           endif
        ENDIF
C
C          *** TRANSFORMATION OPERATION COMPLETE ***
C-----------------------------------------------------------------------
C     *** pass thru mut and pert operators ***
C 
         call mut_cross(ipop,nchrom,ngene,nobfun,pop,fit,nfuneval,
     $       beta,beta_sin,fit_init,genemin,genemax,debug,io_34,
     $       beta_min,beta_max,n_beta_s,n_beta_i,
     $       box_over, prob_pert, prob_mut, prob_avg, i_avg_cho,
     $       XBEST,      XAVG,       XPERT,      XMUT,
     $       NBEST,      NAVG,       NPERT,      NMUT,
     $       compute_fit,cbest)
         call ga_int_conv(nchrom,ngene,pop,int_real)

C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** TRANSFORM EACH GENE VECTOR (I.E., EACH
C              CHROMOSOME) BACK TO THE ORIGINAL 
C              COORDINATE SYSTEM. USE THE INVERSE OF THE 
C              PREVIOUSLY COMPUTED TRANSFORMATION MATRIX.
C              DO THIS ONLY FOR MULTI-OBJECTIVE CASES 
C              (NOBFUN>1) WHEN ITRAN=1 ***
C
         IF (itran.ne.0 .and. (ipop.ge.itran .AND. NOBFUN.GT.1)) THEN
            ITRAN_S = -2
            CALL TRANSFORM(ipop,NGENE,NCHROM,POP,TW,TU,TR,ITRAN_S,
     $          genemin,genemax,scale_genes,debug)
            if(trans_minmax)then
               ITRAN_S = -1
               NCH   =  1
               CALL TRANSFORM(ipop,NGENE,NCH,GENEMIN,TW,TU,TR,ITRAN_S,
     $             genemin,genemax,scale_genes,debug)
               CALL TRANSFORM(ipop,NGENE,NCH,GENEMAX,TW,TU,TR,ITRAN_S,
     $              genemin,genemax,scale_genes,debug)
            endif
         ENDIF
C
C          *** TRANSFORMATION OPERATION COMPLETE ***
C
C  set precision of pop genes
C
      if(geneprec)call ga_precision(nchrom,ngene,pop,igeneprec,debug)

      ENDIF
C
C          *** WRITE chromo.i FILES FOR INITIALIZATION (IPOP=0)
C              OR FOR ANY GENERATION (0<IPOP<NPOP). THE
C              PARML LOGICAL PARAMETER DETERMINES WHETHER A
C              NEW FUNCTION EVALUATION IS REQUIRED OR NOT. IF
C              PARML=T...PERFORM FUNCTION EVALUATION. IF 
C              PARML=F...NO FUNCTION EVALUATION. FOR THOSE
C              NBEST CHROMOSOMES THAT WERE NOT CHANGED DURING
C              THE SELECTION, SET PARML=F ***
C
      if(ipop.eq.0)call init_param(nchrom,NPI,IPARM,IPARM_chm,NPR,PARMR,
     $     PARMR_chm,NPL,PARML,PARML_chm)
      
cthp_2008
c  Since Fitness valus will always be recomputed after this step, reset fit to fit_init
cthp_2008
      call init_fit(nchrom,nobfun,fit,i_mnx,fit_init,.false.)

      IRW = 2
      CALL RWCHROMO(nchrom,NGENE,nobfun,POP,FIT,
     $     NPI,IPARM,IPARM_chm,NPR,PARMR,
     $     PARMR_chm,NPL,PARML,PARML_chm,
     $     IPOP,IRW,NOUT,fitmin,fitmax,ga_file_prefix,
     $     cputime,nfuneval,cputi,nfun,nbest,imove,cbest,debug,
     $     io_34,fit_min_max,arc2d_io,field_pop,field_fit,field_log,
     $     field_ngene,field_nobfun,compute_fit,compute_pass_thu)
C
      e_cpu_1 = timer_seconds(dummy)
      if(io_34)write(34,200)e_cpu_1 - e_cpu_0
 200  format(/' CPU for Processing Chromos = ',e12.6)

C-----------------------------------------------------------------------
C          *** MAIN ROUTINE IS COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      STOP
      END
