c-----------------------------------------------------------------------
c     -- framux: second-order accurate Frechet matrix A multiply
c     vector X.  This subroutine evaluates a matrix-vector product
c     using forward differences: v2 = A*v1 --
c     -- m. nemec, sept. 2001 --
c     -- calling routine: run_gmres.f
c-----------------------------------------------------------------------
      subroutine framux( ifirst, ntvar, nmax,qr,qps,qos,press,sndsp,sr,
     &     rsar,xy,xyj,xit,ett,ds,x,y,turmu,vort,turrer,turps,turos,fmu,
     &     vk, ve, tmp, uu, vv, ccx, ccy,coef2x, coef2y, coef4x, coef4y,
     &     precon, spectxi, specteta,indx, iex, bcf, v1, v2,
     &     exprhs,dhatq,exprsa,dhatt,dtvc,as,ja,ia)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"

      integer ifirst, ntvar, nmax, j, i, ib, lg, lq, is
      double precision qr(*),      press(*),    sndsp(*)
      double precision sr(*),      xy(*),       xyj(*)
      double precision xit(*),     ett(*),      ds(*)
      double precision x(*),       y(*),        turmu(*)
      double precision vort(*),    turrer(*),   fmu(*)
      double precision vk(*),      ve(*),       tmp(*)
      double precision uu(*),      vv(*),       ccx(*)
      double precision ccy(*),     coef2x(*),   coef2y(*)
      double precision coef4x(*),  coef4y(*),   precon(*)
      double precision v1(ntvar),  v2(ntvar)
      double precision spectxi(*), specteta(*), rsar(*)
      integer indx(*),iex(maxjb,4,4,mxbfine)
      
      double precision v1_n,eps,eps1
cmt
      DOUBLE PRECISION qps(*), qos(*), turps(*),turos(*)
      DOUBLE PRECISION exprhs(*),dhatq(*),exprsa(*),dhatt(*)

c     -- time vector in matrix-vector product --
      double precision dtvc(*)
      
      double precision bcf(maxjb,4,4,mxbfine)


c     -- local arrays --
      double precision q(maxjk*4), s(maxjk*4), turre(maxjk), rsa(maxjk)
      double precision asdummy(2),padummy(2),normc,normjac

      double precision as(*),ja(*), v3(ntvar)
      integer ipadummy(2),iadummy(2),ia(*)

      logical precmat, jacmat

      precmat = .false.
      jacmat  = .false.

      if (ifirst.eq.1) then
         do j = 1,ntvar
            v2(j) = 0.0d0
         end do
      else
         v1_n = 0.0d0
         do i = 1,ntvar
            v1_n = v1_n + v1(i)**2
         end do
         v1_n = dsqrt(v1_n)
         eps  = dsqrt(2.2d-16) / v1_n
         eps1 = 1.0d0/eps

         do i = 1,maxjkq
            q(i) = 0.0d0
         end do
         
         do i = 1,maxjk
            turre(i) = 0.0d0
         end do

c     -- perturb q vector by eps --
         do ib=1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            call fr_dq( ntvar, jmax(ib), kmax(ib), nmax, kminbnd(ib),
     &           kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), indx(lg),
     &           q(lq), qr(lq), turre(lg), turrer(lg), xyj(lg), eps, v1)
         end do


c     -- set LL and UL corner of Q-vector to 10^4 for blunt trailing edge
         do i=1,nblks
            if (ibctype(i,2) .eq. 1) then
               ib = i
               call fix_q(ib,nhalo,jmax(ib),kmax(ib),nmax,q(lqptr(ib)))              

               ib = lblkpt(i,3)
               if (ib.ne.0) then
                  call fix_q(ib,nhalo,jmax(ib),kmax(ib),
     &                 nmax,q(lqptr(ib)))
               endif

               ib = lblkpt(i,1)
               if (ib.ne.0) then
                  call fix_q(ib,nhalo,jmax(ib),kmax(ib),
     &                 nmax,q(lqptr(ib)))                  
               endif
            endif
         enddo

c     -- halo column copy for q vector --
         call halo_q(1,nblks,maxj,maxk,q,turre) 

c     -- calculate pressure and sound speed --
         do ib=1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            call calcps( ib, jmax(ib), kmax(ib), q(lq), press(lg),
     &           sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
     &           jbegin2(ib), jend2(ib),
     &           kbegin2(ib), kend2(ib))
         end do

c     -- residual evaluation: -R(Q) stored in S --
cmt ~~~
c         write(*,*)"entered the get_rhs in framux"
         call get_rhs(q,qps,qos,press,sndsp,s,xy,xyj,xit,ett,ds,x,y,
     &        turmu,vort,turre,turps,turos,fmu,vk,ve,tmp,uu,vv,ccx,ccy,
     &        coef2x, coef2y,coef4x, coef4y, precon, spectxi, specteta,
     &        rsa,exprhs,dhatq,exprsa,dhatt,.false.)
c         write(*,*)"finished the get_rhs in framux"

c     -- the arrays as, pa, ipa, ia are not required here --
         do ib=1,nblks
            do is=1,4
               if (ibctype(ib,is).gt.0 .and. ibctype(ib,is).lt.5) 
     &              call sdirect (ib, is, jmax(ib),kmax(ib), nmax,
     &              kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib),
     &              precmat, jacmat, iex, bcf, indx(lgptr(ib)),
     &              s(lqptr(ib)),padummy,asdummy,ipadummy,iadummy, 
     &              maxjb, mxbfine) 
            end do
         end do


         do i = 1,maxjkq
            q(i) = 0.0d0
         end do
         
         do i = 1,maxjk
            turre(i) = 0.0d0
         end do

         eps1 = 1.0d0/(2.0d0*eps)
         eps=-eps
         

c     -- perturb q vector by eps --
         do ib=1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            call fr_dq( ntvar, jmax(ib), kmax(ib), nmax, kminbnd(ib),
     &           kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), indx(lg),
     &           q(lq), qr(lq), turre(lg), turrer(lg), xyj(lg), eps, v1)
         end do


c     -- set LL and UL corner of Q-vector to 10^4 for blunt trailing edge
         do i=1,nblks
            if (ibctype(i,2) .eq. 1) then
               ib = i
               call fix_q(ib,nhalo,jmax(ib),kmax(ib),nmax,q(lqptr(ib)))              

               ib = lblkpt(i,3)
               if (ib.ne.0) then
                  call fix_q(ib,nhalo,jmax(ib),kmax(ib),
     &                 nmax,q(lqptr(ib)))
               endif

               ib = lblkpt(i,1)
               if (ib.ne.0) then
                  call fix_q(ib,nhalo,jmax(ib),kmax(ib),
     &                 nmax,q(lqptr(ib)))                  
               endif
            endif
         enddo

c     -- halo column copy for q vector --
         call halo_q(1,nblks,maxj,maxk,q,turre) 

c     -- calculate pressure and sound speed --
         do ib=1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)
            call calcps( ib, jmax(ib), kmax(ib), q(lq), press(lg),
     &           sndsp(lg), precon(lprecptr(ib)), xy(lq), xyj(lg),
     &           jbegin2(ib), jend2(ib),
     &           kbegin2(ib), kend2(ib))
         end do

c     -- residual evaluation: -R(Q) stored in S --
cmt ~~~
c         write(*,*)"entered the get_rhs in framux"
         call get_rhs(q,qps,qos,press,sndsp,sr,xy,xyj,xit,ett,ds,x,y,
     &        turmu,vort,turre,turps,turos,fmu,vk,ve,tmp,uu,vv,ccx,ccy,
     &        coef2x, coef2y,coef4x, coef4y, precon, spectxi, specteta,
     &        rsar,exprhs, dhatq,exprsa,dhatt,.false.)
c         write(*,*)"finished the get_rhs in framux"

c     -- the arrays as, pa, ipa, ia are not required here --
         do ib=1,nblks
            do is=1,4
               if (ibctype(ib,is).gt.0 .and. ibctype(ib,is).lt.5) 
     &              call sdirect (ib, is, jmax(ib),kmax(ib), nmax,
     &              kminbnd(ib), kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib),
     &              precmat, jacmat, iex, bcf, indx(lgptr(ib)),
     &              sr(lqptr(ib)),padummy,asdummy,ipadummy,iadummy, 
     &              maxjb, mxbfine) 
            end do
         end do





c     -- finite difference matrix-vector product --
         do ib=1,nblks
            lg = lgptr(ib)
            lq = lqptr(ib)      
            call fr_ds( ntvar, jmax(ib), kmax(ib), nmax, kminbnd(ib),
     &           kmaxbnd(ib), jminbnd(ib), jmaxbnd(ib), indx(lg), s(lq),
     &           sr(lq), rsa(lg), rsar(lg), v1, v2, eps1, dtvc)
         end do




         call amux(ntvar, v1, v3, as, ja, ia)

         normjac=0.d0
         normc=0.d0
         do j = 1,ntvar
            normc =  normc+v2(j)**2
            normjac =  normjac+v3(j)**2
         end do
         normc=SQRT(normc)
         normjac=SQRT(normjac)
         print *, normc,normjac

      end if

      return
      end                       !framux








      subroutine fix_q(ib, nhalo, jmax, kmax, nmax, arr)
c     Setting LL and UL corner of Q-vector to 10^4 to avoid floating point error
c     in calcps.f.  This occurs when nhalo=2 and you have blunt t.e.

      implicit none

      integer ib, nhalo, jmax, kmax, nmax, n, j, k
      double precision arr(jmax, kmax, nmax)

      do n=1,nmax
         do j=1,nhalo

c     -- fix lower left corner
            do k=1,nhalo
               if (arr(j,k,n).eq.0.d0) then
                  arr(j,k,n)=1.d4
               endif
            enddo

c     -- fix upper left corner
            do k=kmax-nhalo+1, kmax
               if (arr(j,k,n).eq.0.d0) then
                  arr(j,k,n)=1.d4
               endif
            enddo

         enddo
      enddo

      return
      end




