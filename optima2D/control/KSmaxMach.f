c-----------------------------------------------------------------------
c     Program name: KSmaxMach
c     Written By: Howard Buckley, May 2009
c     
c     This subroutine evaluates the max Mach number in the flow field
c     using the KS function
c
c-----------------------------------------------------------------------

      subroutine KSmaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach,ks,ksMach)

#ifdef _MPI_VERSION
      use mpi
#endif


      !-- Declare variables

      implicit none
								
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
		
      integer 	
     &     i, j, k, jdim, kdim, ndim, jMaxMach, kMaxMach 

      double precision 
     &     q(jdim,kdim,ndim), U, V, P, a, Mach, MaxMach, ks,
     &     aMaxMach, uMaxMach, vMaxMach, pMaxMach, g, ksMach,
     &     rKS, rKSd, rKS1, dksdrho, dksdrhod, dksdrho1,
     &     ksSum1, ksSum2, ksSum3

      !-- Scan the flow field to find the maximum Mach number
 
      call getMaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)

      !-- Calculate the value of constraint 'g' using the maximum Mach
      !-- nummber

      gmax = MaxMach/Mstar - 1

      !-- Evaluate the KS function using the Mach numbers in the region
      !-- of the flow field where the maximum Mach number is expected to
      !-- be found

      ks = 0.0
      rKS_used = 0.0

      do j = jmstart, jmend, jminc !-- Increment j by jminc

         do k = kmstart, kmend, kminc !-- Increment k by kminc

            !-- Evaluate Mach # at (j,k)
            
            U = q(j,k,2)/q(j,k,1)
            V = q(j,k,3)/q(j,k,1)

            P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))
         
            a = sqrt(1.4*P/q(j,k,1))

            Mach = sqrt(U**2 + V**2)/a

            !-- Calculate the value of constraint 'g' using Mach # at
            !-- (j,k)           

            g = Mach/Mstar  - 1

            !-- Calculate the summation term in the KS function

            ks = ks + exp(rhoKS*(g-gmax))

            !-- Store the summation term for use in dobjdq

            dksDenom = ks
                      
         end do

      end do
 
      ks = gmax + (1/rhoKS)*log(ks)

      !-- Store the value of rhoKS used to calculate ks

      rKS_used = rhoKS

      !-- If the Poon/Martins adaptive KS approach is to be used, do
      !-- these additional steps:

      if (adptKS) then

         !-- Compute dks/drho using the nominal value of rhoKS
         
         call dksdr(jdim, kdim, ndim, rhoKS, q, dksdrho, ksSum1, 
     &                  ksSum2, ksSum3) 

         !-- If dksdrho is less than dksdrhod, then the nominal value
         !-- of rhoKS is sufficient, so leave ks as is

         dksdrhod = -1.d-6

         if (dabs(dksdrho).lt.dabs(dksdrhod)) then

            goto 10

         else !-- Recalculate ks with a more aggressive value of rhoKS

            !-- Calculate dks/drho1, the derivative evaluated at
            !-- (rhoKS + 0.001)

            rKS1 = rhoKS + 0.001

            call dksdr(jdim, kdim, ndim, rKS1, q, dksdrho1, ksSum1, 
     &                  ksSum2, ksSum3)
c$$$
c$$$            write(scr_unit,*)'dksdrhod=',dksdrhod
c$$$            write(scr_unit,*)'dksdrho=',dksdrho
c$$$            write(scr_unit,*)'dksdrho1=',dksdrho1
c$$$            write(scr_unit,*)'rKS1=',rKS1
c$$$            write(scr_unit,*)'rhoKS=',rhoKS
           
            rKSd = (log10(dksdrhod/dksdrho)/log10(dksdrho1/dksdrho))
     &                *log10((rKS1)/rhoKS) + log10(rhoKS)
 
            rKSd = 10**(rKSd)
            
            ks = 0.0

            do j = jmstart, jmend, jminc !-- Increment j by jminc

               do k = kmstart, kmend, kminc !-- Increment k by kminc

                  !-- Evaluate Mach # at (j,k)
            
                  U = q(j,k,2)/q(j,k,1)
                  V = q(j,k,3)/q(j,k,1)

                  P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))
         
                  a = sqrt(1.4*P/q(j,k,1))

                  Mach = sqrt(U**2 + V**2)/a

                  !-- Calculate the value of constraint 'g' using Mach # 
                  !-- at (j,k)           

                  g = Mach/1.35  - 1
  
                  !-- Calculate the summation term in the KS function
 
                  ks = ks + exp(rKSd*(g-gmax))

                  !-- Store the summation term for use in dobjdq

                  dksDenom = ks
                      
               end do

            end do

            ks = gmax + (1/rKSd)*log(ks)

            !-- Store the value of rhoKS used to calculate ks

            rKS_used = rKSd

         end if

      end if

      !-- Calculate the KS function's conservative estimate of max Mach #

 10   ksMach = (ks+1)*Mstar

      return

      end   


c-----------------------------------------------------------------------
c     Program name: dksdr
c     Written By: Howard Buckley, April 2010
c     
c     This subroutine calculates dks/drho
c
c-----------------------------------------------------------------------

      subroutine dksdr(jdim, kdim, ndim, rKS, q, dksdrho, ksSum1, 
     &                  ksSum2, ksSum3)

#ifdef _MPI_VERSION
      use mpi
#endif


      !-- Declare variables

      implicit none
								
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
		
      integer 	
     &     i, j, k, jdim, kdim, ndim, diff_flag

      double precision 
     &     q(jdim,kdim,ndim), U, V, P, a, Mach, MaxMach, ks, ksp, ksm,
     &     aMaxMach, uMaxMach, vMaxMach, pMaxMach, g, ksMach,
     &     rKS, rhoKSp, rhoKSm, dksdrho, stepsize, ksSum1, ksSum2,
     &     ksSum3

      ksSum1 = 0.0
      ksSum2 = 0.0
      ksSum3 = 0.0

      !-- Set diff flag for finite diff or analytic calculation of
      !-- dks/drho

      diff_flag = 1 

      if (diff_flag.eq.0) then ! Calculate dks/drho with finite diffs

         !-- Calculate perturbation stepsize

         stepsize = fd_eta*rKS

         if ( dabs(stepsize) .lt. 1.d-9 ) then
            stepsize = 1.d-9*dsign(1.d0,stepsize)
         end if

         !-- Calc ks value at +ve perturbed rhoKS value

         rhoKSp = rKS + stepsize

         ksp = 0.0

         do j = jmstart, jmend, jminc !-- Increment j by jminc

            do k = kmstart, kmend, kminc !-- Increment k by kminc

               !-- Evaluate Mach # at (j,k)
             
               U = q(j,k,2)/q(j,k,1)
               V = q(j,k,3)/q(j,k,1)

               P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))
         
               a = sqrt(1.4*P/q(j,k,1))

               Mach = sqrt(U**2 + V**2)/a

               !-- Calculate the value of constraint 'g' using Mach # at
               !-- (j,k)           

               g = Mach/1.35  - 1
  
               !-- Calculate the summation term in the KS function
 
               ksp = ksp + exp(rhoKSp*(g-gmax))
                      
            end do

         end do

         ksp = gmax + (1/rhoKSp)*log(ksp)

         !-- Calc ks value at -ve perturbed rhoKS value

         rhoKSm = rKS - stepsize

         ksm = 0.0

         do j = jmstart, jmend, jminc !-- Increment j by jminc

            do k = kmstart, kmend, kminc !-- Increment k by kminc

               !-- Evaluate Mach # at (j,k)
            
               U = q(j,k,2)/q(j,k,1)
               V = q(j,k,3)/q(j,k,1)

               P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))
         
               a = sqrt(1.4*P/q(j,k,1))

               Mach = sqrt(U**2 + V**2)/a

               !-- Calculate the value of constraint 'g' using Mach # at
               !-- (j,k)           

               g = Mach/1.35  - 1
  
               !-- Calculate the summation term in the KS function
 
               ksm = ksm + exp(rhoKSm*(g-gmax))
                      
            end do

         end do

         ksm = gmax + (1/rhoKSm)*log(ksm)

         !-- Calculate finite differenced gradient
         
         dksdrho = (ksp - ksm)/(2.d0*stepsize)

      else if (diff_flag.eq.1) then ! Calculate dks/drho analytically     

         do j = jmstart, jmend, jminc !-- Increment j by jminc

            do k = kmstart, kmend, kminc !-- Increment k by kminc

               !-- Evaluate Mach # at (j,k)
            
               U = q(j,k,2)/q(j,k,1)
               V = q(j,k,3)/q(j,k,1)

               P = 0.4*(q(j,k,4)-0.5*q(j,k,1)*(U**2 + V**2))
         
               a = sqrt(1.4*P/q(j,k,1))

               Mach = sqrt(U**2 + V**2)/a

               !-- Calculate the value of constraint 'g' using Mach # at
               !-- (j,k)           

               g = Mach/1.35  - 1
  
               !-- Calculate summation terms used in dks/drho expression
 
               ksSum1 = ksSum1 + exp(rKS*(g-gmax))
               ksSum2 = ksSum2 + (g-gmax)*exp(rKS*(g-gmax))
               ksSum3 = ksSum3 + ((g-gmax)**2)*exp(rKS*(g-gmax))

            end do

         end do

         dksdrho = (1/rKS)*(ksSum2/ksSum1) - (1/rKS**2)*log(ksSum1)

      end if

      return

      end
