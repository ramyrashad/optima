      subroutine reset_Dhat(jdim,kdim,D_hat)
c
#include "../include/arcom.inc"
c 
      double precision D_hat(jdim,kdim,4,6)

      do 3 st=1,5
      do 3 n=1,4
      do 3 k=1,kdim
      do 3 j=1,jdim
         D_hat(j,k,n,st)=0.0d0
 3    continue

      return
      end

