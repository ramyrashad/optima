c     ------------------------------------------------------------------
c                                                                       
c        ****    ******   *******  *  **     **  *******
c       *    *   *     *     *     *  * *   * *  *     *
c      *      *  *     *     *     *  *  * *  *  *     *
c      *      *  ******      *     *  *   *   *  ******* 
c      *      *  *           *     *  *       *  *     *
c       *    *   *           *     *  *       *  *     *
c        ****    *           *     *  *       *  *     *  MB
c                                                                       
c     ------------------------------------------------------------------

      program optimaMB

c     ------------------------------------------------------------------
c     -- aerodynamic shape optimization for multi-block grids --
c     
c     -- written by: marian nemec --
c     -- date: 2001 --
c     -- no transfer of this program without explicit approval --
c     
c     -- parts of this program contain subroutines from or are based 
c     on the following: ARC2D, TORNADO, OPTIMA2D, SPARSKIT and LAPACK --
c     ------------------------------------------------------------------

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

c     -- solver arrays --
      double precision q(maxjkq),        press(maxjk),      sndsp(maxjk)
      double precision s(maxjkq),        xy(maxjk*4),       xyj(maxjk)
      double precision xit(maxjk),       ett(maxjk),        ds(maxjk)
      double precision x(maxjk),         y(maxjk),          turmu(maxjk)
      double precision vort(maxjk),      turre(maxjk),      fmu(maxjk)
      double precision vk(maxjk),        ve(maxjk),         tmp(maxjk*4)
      double precision uu(maxjk),        vv(maxjk),         ccx(maxjk)
      double precision ccy(maxjk),       coef2x(maxjk),    coef2y(maxjk)
      double precision coef4x(maxjk),    coef4y(maxjk),  precon(maxjk*8)
      double precision spectxi(maxjk*3),specteta(maxjk*3),wk1(3*maxjk*4)
cmt
      double precision qps(maxjkq),qos(maxjkq),turps(maxjk),turos(maxjk)

c     -- ntvar: total number of unknows --
c     -- sparse arrays --
c     -- as, ja, ia: second order flux jacobian --
c     -- pa, jpa, ipa: preconditioner --
c     -- icol used for setup of preconditioner and jacobian --
      integer ia(maxjk*jacb+1),  ja(maxjk*jacb2*9), icol(9)
      integer ipa(maxjk*jacb+1), jpa(maxjk*jacb2*5)

c     -- reordering array --
      integer indx(maxjk)

      integer ntvar,nmax,jdim,kdim,mglev,ip,i,ifirst,mlim,nebfgs

      double precision dtiseq(-5:20),dtmins(-5:20),dtspiseq(-5:20)
      double precision vlx(-5:20),vnx(-5:20),vly(-5:20),vny(-5:20)
      integer iends(-5:20),jacdtseq(-5:20),isbiter(-5:20,2)      
      integer isequal
      common/mesup/ dtiseq, dtmins,dtspiseq, vlx, vnx, vly, vny, 
     &        isequal, iends, jacdtseq, isbiter

c     -- initialize total number of unknows --
      ntvar = 0

c     -- input parameters and read isequal --
      call input(isequal,mglev)            

      if (viscous .and. turbulnt .and. iturb.eq.3) then
c     -- turbulent case with S-A model --
c     -- nmax: number of unknows per node --
         nmax = 5
      else
         nmax = 4
      end if

c     -- read grid for initial airfoil geometry --
c     -- either formatted or unformatted, depending upon iread --
c     -- array pointers are calculated --
      jdim = maxj
      kdim = maxk

      if(iread.lt.1) stop 'optimaMB: iread must be > 0'
      ip = 3 
      call ioall (0, mglev, ip, jdim, kdim, q, qos,press,
     &     sndsp, turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &     coef2x, coef4x, coef2y, coef4y)

c     -- read block connectivity and boundary conditions --
      ip = 21 
      call ioall (0, mglev, ip, jdim, kdim, q, qos,press,
     &     sndsp, turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &     coef2x, coef4x, coef2y, coef4y)

c     -- setup grid index --
      call setgrid(x,y)  

      nblkstart = 1 
      nblkend   = nblks
      if (.not.mg .and. .not.gseq) then
         call sortfoils( jdim, kdim, x, y)
      endif

c     -- input grid sequence and time step parameters --
      ip = 9
      call ioall (0, mglev,ip,jdim,kdim,q,qos,press, sndsp, turmu, fmu,
     &     vort, turre, turos, vk, ve, xy, xyj, x, y,coef2x, coef4x, 
     &     coef2y, coef4y)

c     -- optimization run: read b-spline file and design variables --
      if (ioptm .gt. 2 .and. .not. sbbc) then
         ip = 2 
         call ioall (0, mglev,ip,jdim,kdim,q,qos,press,sndsp,turmu,fmu, 
     &        vort, turre, turos, vk, ve, xy, xyj, x, y, coef2x, coef4x,
     &        coef2y, coef4y)

c     -- setup bap array from initial grid --
         call setbap(x, y)

c     -- read target cp distribution --
         if (iobjf .eq. 1 .and. ioptm .ne. 6) then
            ip = 1 
            call ioall (0,mglev,ip,jdim,kdim,q,qos,press,sndsp,turmu, 
     &           fmu,vort, turre, turos, vk, ve, xy,xyj, x, y, coef2x,
     &           coef4x, coef2y, coef4y)
         end if
      end if                    !ioptm

c     -- setup reordering index array for N-K flow solves and
c     optimization runs --
      if ( nkits.gt.0 .or. nkiends.gt.0 ) then

c     -- initial node order and reordering --
         call order( inord, ireord, x, y, indx, viscous)

c     -- setup csr preconditioner and jacobian pointer arrays --
         call setgraph( nmax, ntvar, indx, icol, ipa, jpa, ia, ja,
     &        viscous) 
      end if

      if ( ioptm.eq.1 ) then
c     -- analysis run --
         write (n_out,100)
         call flush(n_out)
         ifirst = 1             ! indicates first call to flow solve

         print*,"Analysis Mode: Flow solve only..."

         call flow_solve (ifirst, nmax, indx, q,qps,qos, xy, xyj, x, y,
     &        turmu, vort,turre,turps,turos,fmu, press,precon,sndsp,xit,
     &        ett,ds, vk, ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,
     &        ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,
     &        ia, ja, icol)

      else if ( ioptm.eq.2 ) then
c     -- analysis run, angle of attack sweep --
         write (n_out,110)
         call flush(n_out)
c     -- read in angles of attack --
         ip = 8
         call ioall (0, mglev, ip, jdim, kdim, q,qos, press, sndsp,  
     &        turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &        coef2x, coef4x, coef2y, coef4y)
         
c     -- write output header for eld file --
         ip = 17
         call ioall (0, mglev, ip, jdim, kdim, q,qos, press, sndsp, 
     &        turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &        coef2x, coef4x, coef2y, coef4y)

         ifirst = 1             ! indicates first call to flow solve

         alpha = polar(1)
         do i=1,ipolar
            targetalpha = polar(i)
            alptem = alpha
            call flow_solve (ifirst, nmax, indx, q,qps,qos,xy,xyj,x, y,
     &           turmu, vort, turre,turps,turos,fmu,press,precon, sndsp,
     &           xit,ett,ds,vk,ve,coef2x,coef4x,coef2y, coef4y, s, uu,
     &           vv, ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa,
     &           jpa, ia, ja, icol)

c     -- output coefficients --
            ip = 16
            call ioall (i, mglev, ip, jdim, kdim, q,qos, press, sndsp, 
     &           turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &           coef2x, coef4x, coef2y, coef4y)

c     -- output cp and cf history --
            ip = 13
            call ioall (i, mglev, ip, jdim, kdim, q,qos,  press, sndsp, 
     &          turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &          coef2x, coef4x, coef2y, coef4y)
            
            ifirst = 0          !warm start
            rewind (n_his)
            rewind (n_this)
         end do

      else if ( ioptm.eq.3 ) then
         write (n_out,120)
         call flush(n_out)

         call bfgs(nmax, indx, q,qps,qos, xy, xyj, x, y, turmu, vort,
     &        turre,turps,turos, fmu, press, precon, sndsp, xit, ett, 
     &        ds, vk, ve,coef2x, coef4x, coef2y, coef4y, s, uu, vv, ccx,
     &        ccy,spectxi, specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja,
     &        icol, nebfgs)
c     -- nebfgs flag states which condition caused the exit --
c     -- nebfgs = 0, algorithm has converged --
c     -- nebfgs = 1, maximum number of function evaluations --
c     -- nebfgs = 2, line search has failed to improve the function --
c     -- nebfgs = 3, search vector was not a descent direction. this can
c     only be caused by roundoff, and may suggest that the convergence
c     criterion is too strict. --
         write (n_out,140) nebfgs

      else if ( ioptm.eq.4 ) then
         write (n_out,130)
         call flush(n_out)

         print*,"Starting design space mapping..."

         call dspace(nmax, indx,q,qps,qos, xy, xyj, x, y, turmu, vort,
     &        turre,turps,turos,fmu,press,precon,sndsp,xit,ett, ds, vk,
     &        ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv, ccx, ccy,
     &        spectxi, specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja,
     &        icol)

      else if (  ioptm.eq.6 ) then

c     -- unsteady optimization --

         write (n_out,160)
         call flush(n_out)

         write (*,*) 'Starting Unsteady BFGS Optimization for Airfoils'

c     -- bfgs optimization --

         if (sbbc) then
c        -- define design variables and angles for suction/blowing BC
            write (*,*) 'Optimize suction/blowing conditions'

            ndv=2  
c            ndv=3
            dvs(1)=omegaa
            dvs(2)=omegaf
c           suction/blowing angle with respect to the wing surface of the closed slot
            dvs(3)=sbeta
         end if
 
c     We specify the number mlim of limited memory corrections stored. 
      
         mlim=50

         call unsteadyoptairfoil (nmax, indx, q, qps, qos, xy, xyj,x,y,
     &        turmu,vort, turre,turps,turos,fmu,press,precon,sndsp,xit,
     &        ett,ds,vk,ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,
     &        ccx, ccy, spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,
     &        ia, ja, icol,mlim)      

      end if

 100  format (3x,'Analysis Run...'/)
 110  format (3x,'Analysis Run: Angle of Attack Sweep...'/)
 120  format (3x,'Optimization with BFGS...'/)
 130  format (3x,'Design Space Map'/)
 140  format (3x,'BFGS error flag:',i3/)
 160  format (3x,'Unsteady BFGS Optimization for Airfoils'/)

      stop
      end                       !optimaMB
