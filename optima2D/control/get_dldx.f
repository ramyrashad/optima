c---- get_dLdX ---------------------------------------------------------
c Given the Lagrange multiplier (adjoint variable) psi, this solves the
c remaining adjoint equations to find the other Lagrange multipliers (if
c the elasticity grid perturbation is used), and computes dL/dX, the
c objective function gradient.
c
c Limitations:
c Does not include viscous terms in the xi-direction, or viscous cross
c   terms.
c Only supports the Spalart-Allmaras turbulence model.
c Does not support calculation of J, the metric Jacobian, using cell
c   areas.
c Supports only a c-mesh.
c Does not support the circulation correction (need to look at angle of
c   attack relaxation and derivatives of the lift coefficient to include
c   it).
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c ndim: number of governing equations
c Q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c press: pressure (without the inverse of the metric jacobian included)
c sndsp: speed of sound
c fmu, turmu: molecular viscosity, turbulence viscosity (turmu is
c   evaluated at half nodes.  i.e. turmu(j,k) corresponds to the
c   turbulence viscosity at (j, k+1/2).
c xy, xyj: metrics, and metric jacobian
c psi: adjoint vector
c psina: adjoint vector element pertaining to the flow solver equation
c   that is added when lift is constrained by the flow solver
c x, y: x- and y-coordinates of the grid nodes
c xOrig, yOrig: x- and y-coordinates of the nodes of the original grid
c dx, dy: The increments in the grid, as computed by the elasticity
c   grid perturbation scheme.
c lambda: The adjoint vectors for the finite element grid perturbation;
c   used for warmstarting.
c bt: B-spline parameter values for each airfoil surface node
c bknot: B-spline knot sequence
c idv: indices of the B-spline control points that relate to design
c   variables.  i.e. The design variable having the index i is the
c   y-coordinate of the control point having the index idv(i).
c obj0: initial objective function value
c den: (for unsteady optimization) denominator for mean lift or drag
c     calculations. Should be set to one for steady flows.
c dfdxflag: (for unsteady optimization) if true constraints are considered
c     for unsteady adjoint
c laststep: (for unsteady optimization) if true this is the last adjoint 
c     calculation in the unsteady adjoint. Should be set to true for steady flows.
c
c Returns:
c dLdX: psi^T * dR/dX, as above
c
c Nomenclature used in comments:
c   Symbol   Meaning
c   ------   -------
c   da/db    derivative (Jacobian matrix) of a with respect to b
c   G        vector of grid node locations (x- & y-coordinates)
c   A        vector of airfoil surface locations (x- & y-coordinates)
c   R        flow residual vector (R = -s in getrhs.f)
c
c Chad Oldfield, June 2005.
c-----------------------------------------------------------------------
      subroutine get_dLdX (jdim, kdim, ndim, q, press, sndsp,
     |    fmu, turmu, xy, xyj, psi, psina, x, y, xOrig, yOrig, dx, dy,
     |    lambda, bt, bknot, idv, dLdX, obj0, den, dfdxflag, laststep)
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim, ndim
      double precision q(jdim,kdim,ndim), press(jdim,kdim),
     |    sndsp(jdim,kdim),
     |    fmu(jdim,kdim), turmu(jdim,kdim),
     |    xyj(jdim,kdim), xy(jdim,kdim,4),
     |    psi(jdim, kdim, ndim), dLdX(ndv), x(jdim,kdim),
     |    y(jdim,kdim), xOrig(jdim,kdim), yOrig(jdim,kdim),
     |    dx(jdim*kdim*incr), dy(jdim*kdim*incr),
     |    lambda(2*jdim*kdim*incr),
     |    bt(jtail1:jtail2), bknot(jbsord+nc), den
      double precision psina, obj0
      integer idv(nc+mpopt)

c     Local variables
      integer i, j, k  ! iterators
      double precision ddG(jdim, kdim, 2)  ! psi^T * dR/dG
      double precision arclength(kdim)  ! Distance along a gridline
c         of constant j running from the airfoil's surface to the
c         far field boundary.
      double precision ddA(jtail1:jtail2,2)  ! ddG * dG/dA
      double precision temp
      integer left  ! B-spline variable, as defined by de Bour
      logical foundLeft  ! True when left has been computed correctly
      double precision B(jbsord)  ! The B-spline basis functions that
c         have an effect on the caluclation of the current airfoil
c         surface node location.  Since each surface node location can
c         depend to only jbsord basis functions, the indexing of B is
c         from 1 to jbsord.  So, B(i) corresponds to the control point
c         having an index left-jbsord+i.
      integer iB_dv(jbsord)  ! The indices of the geometric design
c         variables that correspond to the basis functions.
c         i.e. dv(iB_dv(i)) is multiplied by B(i) when computing an
c         airfoil grid node location.  iB_dv(i) is 0 if it does not
c         correspond to any design variables.
      logical independant  ! True if the jth airfoil surface node
c         location is independant of all design variables.
      double precision sin_alpha, cos_alpha   ! The sine and cosine of
c         the angle of attack.
      double precision radians  ! pi/180, for converting an angle
c         measured in degrees to an angle measured in radians.
      double precision xi_x, xi_y, eta_x, eta_y  ! metrics
      double precision dVndalpha, dVtdalpha  ! Derivatives of the
c         normal and tangential freestream velocity components with
c         respect to the angle of attack.
      external gridadjoint

      logical dfdxflag,laststep

      radians = pi / 180.d0

      if (.not. unsted) laststep=.true.

c     Compute -psi^T * dR/dG (stored in ddG)
      call get_psi_dRdG (jdim, kdim, ndim, Q, press, sndsp,
     |    fmu, turmu, x, y, xy, xyj, psi, psina, ddG)
      do i = 1, 2
        do k = kbegin, kend
          do j = jbegin, jend
            ddG(j,k,i) = -ddG(j,k,i)
          end do
        end do
      end do

c     Add dJ/dG to ddG
      call add_dJdG(jdim,kdim,press,Q,xy,xyj,x,y,ddG,obj0,den,dfdxflag)

c     Initialize dLdX and left
      do i = 1, ndv
        dLdX(i) = 0.d0
      end do
      left = jbsord

c     Multiply ddG * dG/dA.  The result is stored in ddA.
      if (incr == 0) then
c       Algebraic grid perturbation
        do j = jtail1, jtail2
c         Compute arclength along jth grid line (for dG/dA) 
          arclength(kbegin) = 0.d0
          do k = kbegin+1, kend
            arclength(k) = arclength(k-1) + 
     |          sqrt(  (xOrig(j,k)-xOrig(j,k-1))**2
     |               + (yOrig(j,k)-yOrig(j,k-1))**2)
          end do
          
c         Evaluate ddA(j) = ddG * dG/dA, for the jth A
c         Note that the grid perturbation algorithm only allows the grid
c         nodes to move in the y-direction, so the x-components in
c         dG/dA are 0.
          ddA(j,1) = 0.d0
          ddA(j,2) = 0.d0
          temp = 1.d0/arclength(kend)  ! to avoid division in a loop
          do k = kbegin, kend
            ddA(j,2) = ddA(j,2) + ddG(j,k,2) * (1.d0 - arclength(k)
     |          * temp)
          end do
        end do
      else
c       Finite element grid perturbation
         if (laststep) then 
            call gridadjoint(jdim, kdim, jtail1, jtail2, incr, xOrig,
     |           yOrig, dx, dy, ddG, ddA, lambda, gatol, gamaxit)
         end if                 ! laststep

      endif !incr == 0

c     Multiply ddA * dA/dX, one value of A at a time.  The result
c     is stored in dLdX.
      do j = jtail1, jtail2
c       Compute B-spline basis functions (for dA/dX)
c       First compute left such that
c         bknot(left) <= bt(j) <= bknot(left+1)
c       This first checks to see if left is the same as it was for the
c       previous j, as that is likely.
        foundLeft = .false.
        if (bknot(left) .le. bt(j) .and. bt(j) .le. bknot(left+1)) then
          foundLeft = .true.
        else
          left = jbsord
          do while ((.not. foundLeft) .and. left .le. nc)
            if (bknot(left) .le. bt(j) .and. 
     |          bt(j) .le. bknot(left+1)    ) then
              foundLeft = .true.
            else
              left = left + 1
            endif
          end do
        endif

c       If left was not computed correctly, then abort this iteration
        if (.not. foundLeft) then
          write (*,*) 'get_dldx.f: Unable to evaluate B-spline',
     |        ' correctly.'
          cycle
        endif

c       The location of the nodes on the jth grid line depend on only
c       the B-spline basis functions having indices from
c       (left-jbsord+1) to (left).  Determine which geometric design
c       variables correspond to these basis functions (if any).
        independant = .true.
        do i = 1,jbsord
          iB_dv(i) = 0
        end do
        do i = 1,ngdv
          if (idv(i) .ge. left-jbsord+1 .and. idv(i) .le. left) then
            iB_dv(idv(i)-left+jbsord) = i
            independant = .false.
          endif
        end do

c       If the jth airfoil surface node is independant of all design
c       variables, then abort this iteration
        if (independant) cycle

c       Compute the relevant B-spline basis functions
        call Bval(bknot, bt(j), left, B)

c       Evaluate ddA(j) * dA/dX and add it to dLdX
        do i = 1,jbsord
          if (iB_dv(i) .ne. 0) then
            dLdX(iB_dv(i)) = dLdX(iB_dv(i)) + ddA(j,2) * B(i)
          endif
        end do
      end do

c     Add dJdX to dLdX
      call add_dJdX(dLdX, obj0)

c     Add -psi^T * dR/dX to dLdX.  This is nonzero only if the
c     angle of attack (alpha) is a design variable.
      if (ngdv .lt. ndv) then
c       Upstream inflow/outflow boundary
        k = kmax
        sin_alpha = dsin(alpha*radians)
        cos_alpha = dcos(alpha*radians)
        do j = jbegin+1, jend-1
          eta_x = xy(j,k,3)
          eta_y = xy(j,k,4)
          temp = (eta_x**2 + eta_y**2)**-0.5d0
          dVndalpha = (-eta_x*sin_alpha + eta_y*cos_alpha)
     |        *fsmach*temp*radians
          if ((eta_x*q(j,k,2) + eta_y*q(j,k,3))*q(j,k,1) .le. 0.d0) then
c             Note we omit temp, as temp > 0, and multiply by
c             q(j,k,1), as it is faster than division.
c           Inflow  (i.e. normal velocity Vn <= 0)
            dVtdalpha = (-eta_y*sin_alpha - eta_x*cos_alpha)
     |          *fsmach*temp*radians
            dLdX(ndv) = dLdX(ndv)
     |          - psi(j,k,1) * (-dVndalpha)
     |          - psi(j,k,4) * (-dVtdalpha)
          else
c           Outflow  (i.e. normal velocity Vn > 0)
            dLdX(ndv) = dLdX(ndv)
     |          - psi(j,k,1) * (-dVndalpha)
          endif
        end do

c       Outflow boundaries (only for inviscid analysis)
        if (.not. viscous) then
c         Boundary downstream and below airfoil (j = 1)
          j = jbegin
          do k = jbegin, kend
            xi_x = xy(j,k,1)
            xi_y = xy(j,k,2)
            temp = (xi_x**2 + xi_y**2)**-0.5d0
            dVndalpha = (-xi_x*sin_alpha + xi_y*cos_alpha)
     |          *fsmach*temp*radians
            if ((xi_x*q(j,k,2) + xi_y*q(j,k,3))*q(j,k,1) .gt. 0.d0) then
c               Note we omit temp, as temp > 0, and multiply by
c               q(j,k,1), as it is faster than division.
c             Inflow  (i.e. normal velocity Vn <= 0)
              dVtdalpha = (xi_y*sin_alpha + xi_x*cos_alpha)
     |            *fsmach*temp*radians
              dLdX(ndv) = dLdX(ndv)
     |            - psi(j,k,1) * (-dVndalpha)
     |            - psi(j,k,4) * (-dVtdalpha)
            else
c             Outflow  (i.e. normal velocity Vn > 0)
              dLdX(ndv) = dLdX(ndv)
     |            - psi(j,k,1) * (-dVndalpha)
            endif
          end do

c         Boundary downstream and below airfoil (j = jend)
          j = jend
          do k = jbegin, kend
            xi_x = xy(j,k,1)
            xi_y = xy(j,k,2)
            temp = (xi_x**2 + xi_y**2)**-0.5d0
            dVndalpha = (-xi_x*sin_alpha + xi_y*cos_alpha)
     |          *fsmach*temp*radians
            if ((xi_x*q(j,k,2) + xi_y*q(j,k,3))*q(j,k,1) .le. 0.d0) then
c               Note we omit temp, as temp > 0, and multiply by
c               q(j,k,1), as it is faster than division.
c             Inflow  (i.e. normal velocity Vn <= 0)
              dVtdalpha = (xi_y*sin_alpha + xi_x*cos_alpha)
     |            *fsmach*temp*radians
              dLdX(ndv) = dLdX(ndv)
     |            - psi(j,k,1) * (-dVndalpha)
     |            - psi(j,k,4) * (-dVtdalpha)
            else
c             Outflow  (i.e. normal velocity Vn > 0)
              dLdX(ndv) = dLdX(ndv)
     |            - psi(j,k,1) * (-dVndalpha)
            endif
          end do
        endif

c       Add in psi_na * R_na:
c       Differentiate the residual equation that is added if lift is
c       constrained in the flow solver:
c         R_na = Cl - Cl*
c         d(R_na)/dX = [0 0 ... 0 0 d(R_na)/d(alpha)]
c       and
c         d(R_na)/d(alpha) = d(Cl)/d(alpha) = -Cd
        if (clopt) then
          dLdX(ndv) = dLdX(ndv) - psina * cdt
        endif
      endif

      end
c---- get_dLdX ---------------------------------------------------------
