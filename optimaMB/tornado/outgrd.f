c     ------------------------------------------------------------------
c     -- output grid --
c     -- m. nemec, nov. 2001 --
c     ------------------------------------------------------------------
      subroutine outgrd( x, y)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

      integer strlen,namelen,i
      double precision x(*), y(*)

      namelen = strlen(output_file_prefix)
      filena  = output_file_prefix
      filena(namelen+1:namelen+6) = '.grid'

      open ( unit=n_all, file=filena, status='unknown',
     &     form='unformatted' )
      
      write(n_all) nblks
      write(n_all) (jbmax(i), kbmax(i),i=1,nblks)
      do i=1,nblks
         call writegrid(n_all,x(lgptr(i)),y(lgptr(i)),
     &        jbmax(i)+2*nhalo,kbmax(i)+2*nhalo,
     &        jminbnd(i),jmaxbnd(i),kminbnd(i),kmaxbnd(i))
      end do

      close (n_all)

      return
      end                       !outgrd
