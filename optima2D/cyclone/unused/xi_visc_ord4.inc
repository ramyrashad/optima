c        ***********************
c        F_eta_eta Viscous Terms
c        ***********************
c        
         xx=1.d0/8.d0
         yy=1.d0/24.d0
         zz=1.d0/16.d0
         do k=klow,kup
         do j=jlow,jup-1
           jm1 = j-1
           jp1 = j+1
           jp2 = j+2
c
           xy1m1=xy(jm1,k,1)
           xy1  =xy(j,k,1)
           xy1p1=xy(jp1,k,1)
           xy1p2=xy(jp2,k,1)
c     
           xy2m1=xy(jm1,k,2)
           xy2  =xy(j,k,2)
           xy2p1=xy(jp1,k,2)
           xy2p2=xy(jp2,k,2)
c
c          -h stands for half
c          -interpolate metrics to +1/2 (ph) nodes
           xy1ph    = zz*(-xy1m1+9.d0*(xy1+xy1p1)-xy1p2)
           xy2ph    = zz*(-xy2m1+9.d0*(xy2+xy2p1)-xy2p2)
c         
c          note: work(j,k) = fnu(j,k) + turre(j,k)
c          - interpolate work to +1/2 node
           workph(j,k)=zz*(-work(jm1,k)+9.d0*(work(j,k)+work(jp1,k))
     &           - work(jp2,k))

c          -differentiate turre w.r.t xi
           dvph=(turre(jm1,k)+27.d0*(turre(jp1,k)-turre(j,k))-
     &           turre(jp2,k))*yy
c
           tmpx(j,k)=xy1ph*dvph
           tmpy(j,k)=xy2ph*dvph
         enddo
         enddo
c
c        Outflow Boundaries
c        -second-order
c         j=jbegin
c         jp1 = j+1
c         do k=klow,kup
c           xy1  =xy(j,k,1)
c           xy1p1=xy(jp1,k,1)
cc     
c           xy2  =xy(j,k,2)
c           xy2p1=xy(jp1,k,2)
cc     
c           tur  =turre(j,k)
c           turp1=turre(jp1,k)
cc     
c           xy1ph    = 0.5d0*(xy1 + xy1p1)
c           xy2ph    = 0.5d0*(xy2 + xy2p1)
cc     
c           workph(j,k)=0.5d0*(work(j,k)+work(jp1,k))
cc
c           dvph=(turp1 - tur)
c           
c           tmpx(j,k)=xy1ph*dvph
c           tmpy(j,k)=xy2ph*dvph
c         enddo
c         j=jup
c         jp1 = j+1
c         do k=klow,kup
c           xy1  =xy(j,k,1)
c           xy1p1=xy(jp1,k,1)
cc     
c           xy2  =xy(j,k,2)
c           xy2p1=xy(jp1,k,2)
cc     
c           tur  =turre(j,k)
c           turp1=turre(jp1,k)
cc     
c           xy1ph    = 0.5d0*(xy1 + xy1p1)
c           xy2ph    = 0.5d0*(xy2 + xy2p1)
cc     
c           workph(j,k)=0.5d0*(work(j,k)+work(jp1,k))
cc           
c           dvph=(turp1-tur)
c           
c           tmpx(j,k)=xy1ph*dvph
c           tmpy(j,k)=xy2ph*dvph
c         enddo
c
c
c        -third-order
         j=jbegin
         jp1 = j+1
         jp2 = j+2
         jp3 = j+3
         do k=klow,kup
           xy1  =xy(j,k,1)
           xy1p1=xy(jp1,k,1)
           xy1p2=xy(jp2,k,1)
c     
           xy2  =xy(j,k,2)
           xy2p1=xy(jp1,k,2)
           xy2p2=xy(jp2,k,2)
c     
           tur  =turre(j,k)
           turp1=turre(jp1,k)
           turp2=turre(jp2,k)
           turp3=turre(jp3,k)
c     
           xy1ph    = xx*(3.d0*xy1 + 6.d0*xy1p1 - xy1p2)
           xy2ph    = xx*(3.d0*xy2 + 6.d0*xy2p1 - xy2p2)
c     
           workph(j,k)=xx*(3.d0*work(j,k)+6.d0*work(jp1,k)-work(jp2,k))
c
           dvph=yy*(-turp3 + 3.d0*turp2 + 21.d0*turp1 -23.d0*tur)
           
           tmpx(j,k)=xy1ph*dvph
           tmpy(j,k)=xy2ph*dvph
         enddo
c     
         j=jup
         jp1 = j+1
         jm1 = j-1
         jm2 = j-2
         do k=klow,kup
           xy1m1=xy(jm1,k,1)
           xy1  =xy(j,k,1)
           xy1p1=xy(jp1,k,1)
c     
           xy2m1=xy(jm1,k,2)
           xy2  =xy(j,k,2)
           xy2p1=xy(jp1,k,2)
c     
           turm2=turre(jm2,k)
           turm1=turre(jm1,k)
           tur  =turre(j,k)
           turp1=turre(jp1,k)
c     
           xy1ph    = xx*(-xy1m1 + 6.d0*xy1 + 3.d0*xy1p1)
           xy2ph    = xx*(-xy2m1 + 6.d0*xy2 + 3.d0*xy2p1)
c     
           workph(j,k)=xx*(-work(jm1,k)+6.d0*work(j,k)+3.d0*work(jp1,k))
c           
           dvph=yy*(turm2 - 3.d0*turm1 - 21.d0*tur + 23.d0*turp1)
           
           tmpx(j,k)=xy1ph*dvph
           tmpy(j,k)=xy2ph*dvph
         enddo
c
c
c        ***************************************************************
c        ***************************************************************

         do k=klow,kup
           do j=jlow+1,jup-1
             jm2=j-2
             jm1=j-1
             jp1=j+1
c
             txm2= tmpx(jm2,k)
             txm1= tmpx(jm1,k)
             tx  = tmpx(j,k)
             txp1= tmpx(jp1,k)
c
             tym2= tmpy(jm2,k)
             tym1= tmpy(jm1,k)
             ty  = tmpy(j,k)
             typ1= tmpy(jp1,k)
c
             wrkm2= workph(jm2,k)
             wrkm1= workph(jm1,k)
             wrk  = workph(j,k)
             wrkp1= workph(jp1,k)
c
             ztmp=(1.0+cb2)*resiginv
c
             fy(j,k) = fy(j,k) + (xy(j,k,1)*(wrkm2*txm2 -
     &           27.d0*(wrkm1*txm1 - wrk*tx) - wrkp1*txp1)
     &               + xy(j,k,2)*(wrkm2*tym2 -
     &           27.d0*(wrkm1*tym1 - wrk*ty) - wrkp1*typ1))*yy*ztmp
c
             ztmp=cb2*resiginv*work(j,k)
c
             fy(j,k) = fy(j,k) - (xy(j,k,1)*(txm2 -
     &           27.d0*(txm1 - tx) - txp1)
     &               + xy(j,k,2)*(tym2 -
     &           27.d0*(tym1 - ty) - typ1))*yy*ztmp
           enddo
         enddo
c
c        ******************
c        Outflow Boundaries
c        ******************
c
csdc        *******************************************
csdc        Below the Wake-Cut 
csdc
csdc        -second-order
csd         j=jlow
csd         jp1=j-1
csd         do k=klow,kup
csd           txm1= tmpx(jm1,k)
csd           tx  = tmpx(j,k)
csdc     
csd           tym1= tmpy(jm1,k)
csd           ty  = tmpy(j,k)
csdc     
csd           wrkm1= workph(jm1,k)
csd           wrk  = workph(j,k)
csdc     
csd           ztmp=(1.0+cb2)*resiginv
csdc     
csd           fy(j,k) = fy(j,k) + (xy(j,k,1)*(wrk*tx - wrkm1*txm1)
csd     &                       +  xy(j,k,2)*(wrk*ty - wrkm1*tym1))*ztmp
csdc     
csd           ztmp=cb2*resiginv*work(j,k)
csdc
csd           fy(j,k) = fy(j,k) - (xy(j,k,1)*(tx - txm1)
csd     &                        + xy(j,k,2)*(ty - tym1))*ztmp
csd         enddo
csdc
csdc         -third-order
csdc         j=jlow
csdc         jm1=j-1
csdc         jp1=j+1
csdc         jp2=j+2
csdc         do k=klow,kup
csdc           txm1= tmpx(jm1,k)
csdc           tx  = tmpx(j,k)
csdc           txp1= tmpx(jp1,k)
csdc           txp2= tmpx(jp2,k)
csdcc     
csdc           tym1= tmpy(jm1,k)
csdc           ty  = tmpy(j,k)
csdc           typ1= tmpy(jp1,k)
csdc           typ2= tmpy(jp2,k)
csdcc     
csdc           wrkm1= workph(jm1,k)
csdc           wrk  = workph(j,k)
csdc           wrkp1= workph(jp1,k)
csdc           wrkp2= workph(jp2,k)
csdcc     
csdc           ztmp=(1.0+cb2)*resiginv
csdcc     
csdc           fy(j,k) = fy(j,k) + (xy(j,k,1)*(-23.d0*wrkm1*txm1 
csdc     &      + 21.d0*wrk*tx + 3.d0*wrkp1*txp1 - wrkp2*txp2)
csdc     &               + xy(j,k,2)*(-23.d0*wrkm1*tym1 
csdc     &      + 21.d0*wrk*ty + 3.d0*wrkp1*typ1 - wrkp2*typ2))*yy*ztmp
csdcc     
csdc           ztmp=cb2*resiginv*work(j,k)
csdcc
csdc           fy(j,k) = fy(j,k) - (xy(j,k,1)*(-23.d0*txm1 
csdc     &         + 21.d0*tx + 3.d0*txp1 - txp2)
csdc     &               + xy(j,k,2)*(-23.d0*tym1 
csdc     &         + 21.d0*ty + 3.d0*typ1 - typ2))*yy*ztmp
csdc         enddo
csdc
csdc        *******************************************
csdc        Above the Wake-Cut
csdc
csdc        -second-order         
csd         j=jup
csd         jm1=j-1
csd         do j=klow,kup
csd           txm1= tmpx(jm1,k)
csd           tx  = tmpx(j,k)
csdc     
csd           tym1= tmpy(jm1,k)
csd           ty  = tmpy(j,k)
csdc     
csd           wrkm1= workph(jm1,k)
csd           wrk  = workph(j,k)
csdc     
csd           ztmp=(1.0+cb2)*resiginv
csdc     
csd           fy(j,k) =  fy(j,k) + (xy(j,k,1)*(wrk*tx - wrkm1*txm1)
csd     &                        +  xy(j,k,2)*(wrk*ty - wrkm1*tym1))*ztmp
csd
csdc     
csd           ztmp=cb2*resiginv*work(j,k)
csdc
csd           fy(j,k) = fy(j,k) - (xy(j,k,1)*(tx-txm1)
csd     &                       +  xy(j,k,2)*(ty-tym1))*ztmp
csd         enddo
csdc
csdcsd       -third-order ( a little unstable I think)
csdcsd         j=jup
csdcsd         jm1=j-1
csdcsd         jm2=j-2
csdcsd         jm3=j-3
csdcsd         do k=klow,kup
csdcsd           txm3= tmpx(jm3,k)
csdcsd           txm2= tmpx(jm2,k)
csdcsd           txm1= tmpx(jm1,k)
csdcsd           tx  = tmpx(j,k)
csdcsdc     
csdcsd           tym3= tmpy(jm3,k)
csdcsd           tym2= tmpy(jm2,k)
csdcsd           tym1= tmpy(jm1,k)
csdcsd           ty  = tmpy(j,k)
csdcsdc     
csdcsd           wrkm3= workph(jm3,k)
csdcsd           wrkm2= workph(jm2,k)
csdcsd           wrkm1= workph(jm1,k)
csdcsd           wrk  = workph(j,k)
csdcsdc     
csdcsd           ztmp=(1.0+cb2)*resiginv
csdcsdc     
csdcsd           fy(j,k) =  fy(j,k) + yy*ztmp*(xy(j,k,1)*
csdcsd     & (23.d0*wrk*tx - 21.d0*wrkm1*txm1 - 3.d0*wrkm2*txm2 + wrkm3*txm3)
csdcsd     &                        +  xy(j,k,2)*
csdcsd     & (23.d0*wrk*ty - 21.d0*wrkm1*tym1 - 3.d0*wrkm2*tym2 + wrkm3*tym3))
csdcsdc     
csdcsd           ztmp=cb2*resiginv*work(j,k)
csdcsdc
csdcsd           fy(j,k) =  fy(j,k) - yy*ztmp*(xy(j,k,1)*
csdcsd     &           (23.d0*tx - 21.d0*txm1 - 3.d0*txm2 + txm3)
csdcsd     &                        +  xy(j,k,2)*
csdcsd     &           (23.d0*ty - 21.d0*tym1 - 3.d0*tym2 + tym3))
csdcsd         enddo
c
         do k=klow,kup
            do j=jlow,jup,jup-jlow
               jp1 = j+1
               jm1 = j-1
               xy1p     = .5*(xy(j,k,1)+xy(jp1,k,1))
               xy2p     = .5*(xy(j,k,2)+xy(jp1,k,2))
               ttp      =  (xy1p*xy(j,k,1)+xy2p*xy(j,k,2))
               
               xy1m     = .5*(xy(j,k,1)+xy(jm1,k,1))
               xy2m     = .5*(xy(j,k,2)+xy(jm1,k,2))
               ttm      =  (xy1m*xy(j,k,1)+xy2m*xy(j,k,2))
               
               cnud=cb2*resiginv*work(j,k)
               
               cdp       =    ttp*cnud
               cdm       =    ttm*cnud               
c        
               trem =.5*(work(jm1,k)+work(j,k))
               trep =.5*(work(j,k)+work(jp1,k))
c               
               cap  =  ttp*trep*(1.0+cb2)*resiginv
               cam  =  ttm*trem*(1.0+cb2)*resiginv
               
c        
               bx(k,j)   = cdm-cam
               cx(k,j)   = -cdp+cap-cdm+cam
               dx(k,j)   = cdp-cap
               
               fy(j,k)   =  fy(j,k) - bx(k,j)*turre(jm1,k)
     &              - cx(k,j)*turre(j,k  )
     &              - dx(k,j)*turre(jp1,k) 
c               
            enddo
         enddo
c        ***************************************************************
c        ***************************************************************
         do k=klow,kup
           do j=jlow,jup
c             jp2=j+2
             jp1=j+1
             jm1=j-1
c             jm2=j-2
c
c            ************************************
c            -use second-order for implicit side
c             xy1p     = zz*(-xy(jm1,k,1)+9.d0*(
c     &                      xy(j,k,1)+xy(jp1,k,1))-xy(jp2,k,1))
c             xy2p     = zz*(-xy(jm1,k,2)+9.d0*(
c     &                      xy(j,k,2)+xy(jp1,k,2))-xy(jp2,k,2))
             xy1p     = .5*(xy(j,k,1)+xy(jp1,k,1))
             xy2p     = .5*(xy(j,k,2)+xy(jp1,k,2))
             ttp      =  (xy1p*xy(j,k,1)+xy2p*xy(j,k,2))
             
c             xy1m     = zz*(-xy(jm2,k,1)+9.d0*(
c     &                      xy(jm1,k,1)+xy(j,k,1))-xy(jp1,k,1))
c             xy2m     = zz*(-xy(jm2,k,2)+9.d0*(
c     &                      xy(jm1,k,2)+xy(j,k,2))-xy(jp1,k,2))
             xy1m     = .5*(xy(j,k,1)+xy(jm1,k,1))
             xy2m     = .5*(xy(j,k,2)+xy(jm1,k,2))
             ttm      =  (xy1m*xy(j,k,1)+xy2m*xy(j,k,2))
             
             cnud=cb2*resiginv*work(j,k)
             
             cdp       =    ttp*cnud
             cdm       =    ttm*cnud
             
c             trem = zz*(-work(jm2,k)+9.d0*(work(jm1,k)+work(j,k))
c     &                        -work(jp1,k))
c             trep = zz*(-work(jm1,k)+9.d0*(work(j,k)+work(jp1,k))
c     &                        -work(jp2,k))
             trem =.5*(work(jm1,k)+work(j,k))
             trep =.5*(work(j,k)+work(jp1,k))

             cap  =  ttp*trep*(1.0+cb2)*resiginv
             cam  =  ttm*trem*(1.0+cb2)*resiginv
             
             bx(j,k)   = cdm-cam
             cx(j,k)   = -cdp+cap-cdm+cam
             dx(j,k)   = cdp-cap
c             bx(j,k)   = 0.d0
c             cx(j,k)   = 0.d0
c             dx(j,k)   = 0.d0
c            ************************************
           enddo
         enddo
         
