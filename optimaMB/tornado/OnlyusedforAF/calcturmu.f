      subroutine calcturmu(iblk,jmax,kmax,
     &      jbegin,jend,kbegin,kend,
     &      q,turmu,turre,fmu,fnu,xyj,damp1m,rho)
c
c     calling subroutine : etaexpl
c
c     -This routine computes the turbulent eddy viscosity turmu
c      at +1/2 nodes.
c
#include "../include/common.inc"
#include "../include/sam.inc"
c
      dimension q(jmax,kmax,4),turmu(jmax,kmax)
      dimension fnu(jmax,kmax),turre(jmax,kmax),fmu(jmax,kmax)
      dimension xyj(jmax,kmax),rho(jmax,kmax),damp1m(jmax,kmax)
c
      klow=kbegin+1
      kup=kend-1
      cmu     = .09
cmn      cv1_3   = 357.911
c
      if (iturb.eq.2) then
c       -For Baldwin-Barth
        do 5 k=kbegin,kend
        do 5 j=jbegin,jend
          rho(j,k) = q(j,k,1)*xyj(j,k)
 5      continue
        do 6 k=kbegin,kup
        do 6 j=jbegin,jend
          turmu(j,k) = cmu*damp1m(j,k)*
     >          .5*(turre(j,k)*rho(j,k) + turre(j,k+1)*rho(j,k+1))
 6      continue
c
      elseif (iturb.eq.3) then
c
c       *****************************************************
c       -note: fnu contains rho/mu .. ie.inverse of nu=mu/rho
c       *****************************************************

c       -For Spalart-Allmaras
        do 2 k=kbegin,kend
        do 2 j=jbegin,jend
          rho(j,k) = q(j,k,1)*xyj(j,k)
 2      continue

        if (iord.eq.4) then
          zz=1.d0/16.d0
          do 610 k=klow,kup-1
            km1=k-1
            kp1=k+1
            kp2=k+2
            do 600 j=jbegin,jend
              chim1=turre(j,km1)*fnu(j,km1)
              chi=turre(j,k)*fnu(j,k)
              chip1=turre(j,kp1)*fnu(j,kp1)
              chip2=turre(j,kp2)*fnu(j,kp2)
              
              fv1m1=chim1**3/(chim1**3+cv1_3)
              fv1=chi**3/(chi**3+cv1_3)
              fv1p1=chip1**3/(chip1**3+cv1_3)
              fv1p2=chip2**3/(chip2**3+cv1_3)

              fv1_c=zz*(-fv1m1+9.d0*(fv1+fv1p1)-fv1p2)
c
              turwrk_km1=turre(j,km1)*rho(j,km1)
              turwrk_k=turre(j,k)*rho(j,k)
              turwrk_kp1=turre(j,kp1)*rho(j,kp1)
              turwrk_kp2=turre(j,kp2)*rho(j,kp2)
c     
              turmu(j,k) = fv1_c*zz*( 
     &              -turwrk_km1+9.d0*(turwrk_kp1+turwrk_k)-turwrk_kp2)
 600        continue
 610      continue
c     
          k=kbegin
          kp1=k+1
          kp2=k+2
          zz=1.d0/8.d0
          do 620 j=jbegin,jend
            chi=turre(j,k)*fnu(j,k)
            chip1=turre(j,kp1)*fnu(j,kp1)
            chip2=turre(j,kp2)*fnu(j,kp2)
c
            fv1=chi**3/(chi**3+cv1_3)
            fv1p1=chip1**3/(chip1**3+cv1_3)
            fv1p2=chip2**3/(chip2**3+cv1_3)
c     
            fv1_c=zz*(3.d0*fv1 + 6.d0*fv1p1 - fv1p2)
c
            turwrk_k=turre(j,k)*rho(j,k)
            turwrk_kp1=turre(j,kp1)*rho(j,kp1)
            turwrk_kp2=turre(j,kp2)*rho(j,kp2)
c
            turmu(j,k) = fv1_c*zz*( 
     &            3.d0*turwrk_k + 6.d0*turwrk_kp1 - turwrk_kp2)
 620      continue
c
          k=kup
          km1=k-1
          kp1=k+1
          do 630 j=jbegin,jend
c             -second order
csd            chi=turre(j,k)*fnu(j,k)
csd            fv1=chi**3/(chi**3+cv1_3)
csd            chip1=turre(j,kp1)*fnu(j,kp1)
csd            fv1p1=chip1**3/(chip1**3+cv1_3)
csd            fv1_c=0.5d0*(fv1+fv1p1)
csd            turmu(j,k) = fv1_c*.5d0*( 
csd     &            turre(j,k)*rho(j,k) + turre(j,kp1)*rho(j,kp1))
c
c           -third order
            chim1=turre(j,km1)*fnu(j,km1)
            chi=turre(j,k)*fnu(j,k)
            chip1=turre(j,kp1)*fnu(j,kp1)
            
            fv1m1=chim1**3/(chim1**3+cv1_3)
            fv1=chi**3/(chi**3+cv1_3)
            fv1p1=chip1**3/(chip1**3+cv1_3)
            
            fv1_c=zz*(-fv1m1 + 6.d0*fv1 + 3.d0*fv1p1)
c     
            turwrk_km1=turre(j,km1)*rho(j,km1)
            turwrk_k=turre(j,k)*rho(j,k)
            turwrk_kp1=turre(j,kp1)*rho(j,kp1)
c     
            turmu(j,k) = fv1_c*zz*( 
     &            -turwrk_km1+6.d0*turwrk_k + 3.d0*turwrk_kp1)
 630     continue
      else
         do k=kbegin,kup
            kp1=k+1
            do j=jbegin,jend
c     mn: uncomment for original code 
c      chi=turre(j,k)*fnu(j,k)
c      fv1=chi**3/(chi**3+cv1_3)
c      chip1=turre(j,kp1)*fnu(j,kp1)
c      fv1p1=chip1**3/(chip1**3+cv1_3)
c      fv1_c=0.5*(fv1+fv1p1)
c      turmu(j,k) = fv1_c*.5d0*(turre(j,k)*rho(j,k)
c     &              + turre(j,kp1)*rho(j,kp1))
c     mn: end
               chi=turre(j,k)*fnu(j,k)
               fv1=chi**3/(chi**3+cv1_3)
               tur1 = fv1*turre(j,k)*rho(j,k)
               chip1=turre(j,kp1)*fnu(j,kp1)
               fv1p1=chip1**3/(chip1**3+cv1_3)
               tur2 = fv1p1*turre(j,kp1)*rho(j,kp1)

               turmu(j,k) = 0.5*(tur1+tur2)
            end do
         end do
      endif
c     
      endif
      return
      end

