c #########################
c ##                     ##
c ##  subroutine filpnx  ##
c ##                     ##
c #########################
c
      subroutine filpnx(jdim,kdim,q,press,sndsp,coef2,coef4,xit,xy,
     >                  xyj,turmu,fmu,ds,a,b,c,d,e,dj,ddj,s)  
c
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),xyj(jdim,kdim),xy(jdim,kdim,4)           
      dimension press(jdim,kdim),sndsp(jdim,kdim),xit(jdim,kdim)        
      dimension turmu(jdim, kdim),fmu(jdim,kdim),s(jdim,kdim,4)
      dimension ds(jdim,kdim),coef2(jdim,kdim),coef4(jdim,kdim)         
      dimension a(jdim,kdim,4),b(jdim,kdim,4,4),c(jdim,kdim,4,4)
      dimension d(jdim,kdim,4,4),e(jdim,kdim,4)                         
      dimension dj(jdim,kdim,3,4),  ddj(jdim,kdim,3,4)             
c                                                                       
c                                                                       
c
c---- compute xi flux jacobian ha ----
      call fillmat (jdim,kdim,q,xy(1,1,1),xy(1,1,2),xit,c)               
c
c
c---- apply centered differencing to it ----
      do 20 m =1,4                                                       
      do 20 n =1,4 
      do 20 k =klow,kup                                                  
      do 20 j =jlow,jup 
         jpl = jplus(j)                                                    
         jmi = jminus(j)                                                   
            b(j,k,n,m) = - c(jmi,k,n,m)                                    
            d(j,k,n,m) =   c(jpl,k,n,m)                                    
 20   continue                                                          
      do 21 m =1,4                                                       
      do 21 n =1,4                                                       
      do 21 k =klow,kup                                                  
      do 21 j =jlow,jup                                                  
         c(j,k,n,m) =  0.0                                              
 21   continue                                                          
c
c
c---- viscous terms (??? on the xi direction???)                    
      if( viscous .and. visxi)                                          
     >call vismatx(jdim,kdim,q,press,xy,xyj,turmu,fmu,a,b,c,d,e,dj,ddj) 
c
c                                                                      
c---- clean out a and e ----
c     ?? pourquoi ??
      do 35 n =1,4                                                       
      do 35 k =kbegin,kend                                               
      do 35 j =jbegin,jend                                               
         a(j,k,n) = 0.0                                                 
         e(j,k,n) = 0.0                                                 
 35   continue                                                          
c
c
c---- add implicit dissipation ----
      call impdissx (jdim,kdim,xyj,coef2,coef4,a,b,c,d,e)                
c
c---- implicit boudary conditions for outflow farfield
      if (ibc) then
         call imfarout(jdim,kdim,q,xy,xyj,b,c,d,s,press)
      endif      
c
c---- scale with local time step ----
      do 40 m =1,4
      do 40 n =1,4                                                       
      do 40 k =klow,kup                                                  
      do 40 j =jlow,jup                                                  
         b(j,k,n,m) = b(j,k,n,m)*ds(j,k)                                
         c(j,k,n,m) = c(j,k,n,m)*ds(j,k)                                
         d(j,k,n,m) = d(j,k,n,m)*ds(j,k)                                
c         if (j.eq.jlow) b(j,k,n,m)=0.
c         if (j.eq.jup) d(j,k,n,m)=0.
 40   continue                                                          
      do 41 n =1,4                                                       
      do 41 k =klow,kup                                                  
      do 41 j =jlow,jup                                                  
         a(j,k,n) = a(j,k,n)*ds(j,k)                                    
         e(j,k,n) = e(j,k,n)*ds(j,k)                                    
c         if (j.eq.jlow+1) a(j,k,n)=0.
c         if (j.eq.jup-1) e(j,k,n)=0.
 41   continue                                                          
c
c
c---- add identity ----
      do 30 n =1,4                                                       
      do 30 k =klow,kup                                                  
      do 30 j =jlow,jup                                                  
         c(j,k,n,n) = c(j,k,n,n) + 1.                                   
 30   continue                                                          
c                                                                       
c                                                                       
      return
      end
