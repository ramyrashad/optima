c     ------------------------------------------------------------------
c     -- gradient calculation: sensitivity of the objective 
c     function wrt the design variables --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine dObjdX (in, dOdX, x, y, xy, xyj, xs, ys, xit, ett, q,
     &     press)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"     
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer in,ib,i,lg,lq,is,iseg,ifoil,nside1,nside2,i2
      double precision q(*), xy(*), xyj(*), x(*), y(*), press(*)
      double precision xit(*), ett(*), xs(*), ys(*), dOdX, dtmp
      double precision objp,objm,thc,goc,cdtp,cltp,cdtm,cltm,stepsize

c     -- locals --
      double precision bapf(ibody,2)

      if ( iobjf.eq.1 ) then
c     -- inverse design --
         dOdX = 0.0d0

      else if ( iobjf.eq.2 .or. iobjf.eq.3 ) then

c     -- obj = wfd*( 1.0-cd/cd_tar )**2 + wfl*( 1.0-cl/cl_tar )**2
c     -- use second order accurate cenral-differences --

         dtmp = dvs(in)
         stepsize = fd_eta*dvs(in)

c     -- check for tiny stepsize --
         if ( abs(stepsize).lt.1.e-10 ) stepsize = 1.e-10*sign(1.0,
     &        stepsize)

         dvs(in) = dvs(in) + stepsize
         stepsize = dvs(in) - dtmp

         if ( in.le.ngdv ) then
c     -- regrid domain --
c     -- store bap --            
            ifoil = idvf(in)
            do i = 1, ibap(ifoil) 
               bapf(i,1) = bap(i,1,ifoil)
               bapf(i,2) = bap(i,2,ifoil)
            end do
   
            call regrid ( (in), x, y)

c     -- recalculate metrics --
            do ib = 1,nblks 
               call xymets(ib, nblks, nhalo, jmax(ib), kmax(ib),
     &              x(lgptr(ib)), y(lgptr(ib)), xy(lqptr(ib)),
     &              xit(lgptr(ib)), ett(lgptr(ib)), xyj(lgptr(ib)),
c     &              nsd1(ib), nsd2(ib), nsd3(ib), nsd4(ib), 
     &              jbegin(ib),
     &              jend(ib), kbegin(ib), kend(ib), jminbnd(ib), 
     &              jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib))
            end do

c     -- copy column of metrics for halos --
            do ib = 1,nblks
               do nside1 = 2,4,2
                  i2 = lblkpt(ib,nside1)
                  if (i2.ne.0) then
                     nside2 = lsidept(ib,nside1)
                     call fixmets( nhalo, nside1, nside2, jmax(ib), 
     &                    kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &                    jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &                    kminbnd(ib), kmaxbnd(ib), jmax(i2), kmax(i2),
     &                    xy(lqptr(i2)), xyj(lgptr(i2)),
     &                    jbegin(i2)-nsd2(i2), jend(i2)+nsd4(i2),
     &                    kminbnd(i2), kmaxbnd(i2))
                  endif
               end do
            end do

c     -- halo row metrics copy --
            do ib = 1, nblks
               do nside1 = 1,3,2
                  i2 = lblkpt(ib,nside1)
                  if (i2.ne.0 .and. 
     &                 (ibctype(ib,nside1).ne.5 .or. nhalo.ge.2)) then
                     nside2 = lsidept(ib,nside1)
                     call fixmets( nhalo, nside1, nside2, jmax(ib),
     &                    kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &                    jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &                    kminbnd(ib)-nsd1(ib), kmaxbnd(ib)+nsd3(ib),
     &                    jmax(i2),kmax(i2), xy(lqptr(i2)),
     &                    xyj(lgptr(i2)), jbegin(i2)-nsd2(i2),
     &                    jend(i2)+nsd4(i2), kminbnd(i2)-nsd1(i2),
     &                    kmaxbnd(i2)+nsd3(i2) )
                  end if
               end do
            end do
         else
            alpha = dvs(in)
         end if         
  
c     -- calculate dOdX at (+) --
         cltp = 0.0
         cdtp = 0.0
         do ifoil=1,nfoils
            do iseg=1,nsegments(ifoil)
               ib = isegblk(iseg,ifoil)
               is = isegside(iseg,ifoil)
               lg = lgptr(ib)
               lq = lqptr(ib)
               call clcd( jmax(ib), kmax(ib), is, ibcdir(ib,is), q(lq),
     &              press(lg), x(lg), y(lg), xy(lq), xyj(lg), 1, cli,
     &              cdi, cmi, clv, cdv, cmv, jminbnd(ib), jmaxbnd(ib),
     &              kminbnd(ib), kmaxbnd(ib))
               cltp = cltp + cli + clv
               cdtp = cdtp + cdi + cdv
            end do
         end do

         call constraints(thc,goc)

         if (cdtp.gt.cd_tar) then
            objp = wfd*( 1.0 - cdtp/cd_tar )**2
         else
            objp = 0.0 
         end if

         if (iobjf.eq.3) cltp = cltp/cdtp
         objp = objp + wfl*( 1.0 - cltp/cl_tar )**2 + thc + goc

         if ( in.le.ngdv ) then
c     -- restore grid --
            do ib = 1,nblks
               lg = lgptr(ib)
               call copy_array( jmax(ib), kmax(ib), 1, xs(lg), x(lg))
               call copy_array( jmax(ib), kmax(ib), 1, ys(lg), y(lg))
            end do

c     -- restore bap --
            ifoil = idvf(in)  
            do i = 1, ibap(ifoil) 
               bap(i,1,ifoil) = bapf(i,1)
               bap(i,2,ifoil) = bapf(i,2)
            end do
         end if

c     -- calculate rhs at (-) --
         dvs(in) = dtmp - stepsize

         if ( in.le.ngdv ) then
c     -- regrid domain --
            call regrid ( (in), x, y)

c     -- recalculate metrics --
            do ib = 1,nblks 
               call xymets(ib, nblks, nhalo, jmax(ib), kmax(ib), 
     &              x(lgptr(ib)), y(lgptr(ib)), xy(lqptr(ib)),
     &              xit(lgptr(ib)), ett(lgptr(ib)), xyj(lgptr(ib)),
c     &              nsd1(ib), nsd2(ib), nsd3(ib), nsd4(ib), 
     &              jbegin(ib),
     &              jend(ib), kbegin(ib), kend(ib), jminbnd(ib),
     &              jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib))
            end do

c     -- copy column of metrics for halos --
            do ib = 1,nblks
               do nside1 = 2,4,2
                  i2 = lblkpt(ib,nside1)
                  if (i2.ne.0) then
                     nside2 = lsidept(ib,nside1)
                     call fixmets( nhalo, nside1, nside2, jmax(ib), 
     &                    kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &                    jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &                    kminbnd(ib), kmaxbnd(ib), jmax(i2), kmax(i2),
     &                    xy(lqptr(i2)), xyj(lgptr(i2)),
     &                    jbegin(i2)-nsd2(i2), jend(i2)+nsd4(i2),
     &                    kminbnd(i2), kmaxbnd(i2))
                  endif
               end do
            end do
            
c     -- halo row metrics copy --
            do ib = 1, nblks
               do nside1 = 1,3,2
                  i2 = lblkpt(ib,nside1)
                  if (i2.ne.0 .and. 
     &                 (ibctype(ib,nside1).ne.5 .or. nhalo.ge.2)) then
                     nside2 = lsidept(ib,nside1)
                     call fixmets( nhalo, nside1, nside2, jmax(ib),
     &                    kmax(ib), xy(lqptr(ib)), xyj(lgptr(ib)),
     &                    jbegin(ib)-nsd2(ib), jend(ib)+nsd4(ib),
     &                    kminbnd(ib)-nsd1(ib), kmaxbnd(ib)+nsd3(ib),
     &                    jmax(i2), kmax(i2), xy(lqptr(i2)),
     &                    xyj(lgptr(i2)), jbegin(i2)-nsd2(i2),
     &                    jend(i2)+nsd4(i2), kminbnd(i2)-nsd1(i2),
     &                    kmaxbnd(i2)+nsd3(i2) )
                  end if
               end do
            end do      
         else
            alpha = dvs(in)
         end if

c     -- calculate dOdX at (-) --
         cltm = 0.0
         cdtm = 0.0
         do ifoil=1,nfoils
            do iseg=1,nsegments(ifoil)
               ib = isegblk(iseg,ifoil)
               is = isegside(iseg,ifoil)
               lg = lgptr(ib)
               lq = lqptr(ib)
               call clcd( jmax(ib), kmax(ib), is, ibcdir(ib,is), q(lq),
     &              press(lg), x(lg), y(lg), xy(lq), xyj(lg), 1, cli,
     &              cdi, cmi, clv, cdv, cmv, jminbnd(ib), jmaxbnd(ib),
     &              kminbnd(ib), kmaxbnd(ib))
               cltm = cltm + cli + clv
               cdtm = cdtm + cdi + cdv
            end do
         end do

         call constraints(thc,goc)
         
         if (cdtm.gt.cd_tar) then
            objm = wfd*( 1.0 - cdtm/cd_tar )**2
         else
            objm = 0.0
         end if

         if (iobjf.eq.3) cltm = cltm/cdtm
         objm = objm + wfl*( 1.0 - cltm/cl_tar )**2 + thc + goc

         dOdX = ( objp - objm )/ (2.0*stepsize)

         dvs(in) = dtmp  

         if ( in.le.ngdv ) then
c     -- restore bap --
            ifoil = idvf(in)    
            do i = 1, ibap(ifoil) 
               bap(i,1,ifoil) = bapf(i,1)
               bap(i,2,ifoil) = bapf(i,2)
            end do
         else
            alpha = dvs(in)
         end if

      else

         write (*,*) 'dobjdx: not setup!'
         stop
         
      end if

      return 
      end                       !dobjdx
