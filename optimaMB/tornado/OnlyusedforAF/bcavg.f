      subroutine bcavg(nside1,nside2,
     &      jdim1,kdim1,q1,xyj1,press1,turre1,work1,vk1,ve1,
     &      jbegin1,jend1,kbegin1,kend1,
     &      jdim2,kdim2,q2,xyj2,press2,turre2,work2,vk2,ve2,
     &      jbegin2,jend2,kbegin2,kend2,meanflow)
c
c     Calling subroutines : integrat_sub.f, etaexpl.f      
#include "../include/common.inc"
#include "../include/units.inc"
c
      dimension q1(jdim1,kdim1,4), xyj1(jdim1,kdim1)
      dimension turre1(jdim1,kdim1),work1(jdim1,kdim1)
      dimension vk1(jdim1,kdim1),ve1(jdim1,kdim1)
      dimension q2(jdim2,kdim2,4), xyj2(jdim2,kdim2)
      dimension turre2(jdim2,kdim2),work2(jdim2,kdim2)
      dimension vk2(jdim2,kdim2),ve2(jdim2,kdim2)
      dimension press1(jdim1,kdim1),press2(jdim2,kdim2)
      logical meanflow
c
c
c ********************************************************************
c **  Lower-order                                                   **
c ********************************************************************
      if (iord.eq.2) then
        if(nside1.eq.1.or.nside1.eq.3) then
c         **  block 1   :   side 1 or 3  **
          if (nside1.eq.1) then
            k1  = kbegin1 + 1
            k1h = kbegin1
          else
            k1  = kend1 - 1
            k1h = kend1 
          endif
c         *** block 1 - side 1 or 3 connected to block 2 - side 1 or 3 
          if (nside2.eq.1.or.nside2.eq.3) then
            if(nside2.eq.1) then
              k2  = kbegin2 + 1
            else
              k2  = kend2 - 1
            endif
            if(nside1.eq.nside2) then
c             js = max(jbegin1,jdim2+1-jend2)
c             je = min(jend1,jdim2+1-jbegin2)
              js = jbegin1
              je = jend1
              ifct1 = 1
              ifct2 = -1
            else
c             js = max(jbegin1,jbegin2)
c             je = min(jend1,jend2)
              js = jbegin1
              je = jend1
              ifct1 = 0
              ifct2 = 1
            endif
c            write (*,*) 'bcave',js, je, k1h
            if (meanflow) then
              do 110 jj=js,je
                j1  = jj
                j1h = jj
                j2  = ifct1*(jdim2 + 1) + ifct2*jj
                rr = 1./xyj1(j1h,k1h)
c     
                rho1=q1(j1,k1,1)*xyj1(j1,k1)
                rho2=q2(j2,k2,1)*xyj2(j2,k2)
                q1(j1h,k1h,1) = 0.5*(rho1+rho2)*rr
c     
                u1=q1(j1,k1,2)*xyj1(j1,k1)
                u2=q2(j2,k2,2)*xyj2(j2,k2)
                q1(j1h,k1h,2) = 0.5*(u1+u2)*rr
c
                v1=q1(j1,k1,3)*xyj1(j1,k1)
                v2=q2(j2,k2,3)*xyj2(j2,k2)
                q1(j1h,k1h,3) = 0.5*(v1+v2)*rr
c     
                p1 = press1(j1,k1)*xyj1(j1,k1)
                p2 = press2(j2,k2)*xyj2(j2,k2)
                psav = 0.5*(p1+p2)
c
c                write (*,*) j1h,k1h,k1,j2,k2
c                write (*,*) -( press1(j1h,k1h)*xyj1(j1h,k1h)
c     &               - 0.5d0*(press1(j1h,k1)*xyj1(j1h,k1) + press2(j2,k2
c     &               )*xyj2(j2,k2) )),press1(j1h,k1h)*xyj1(j1h,k1h)
c     &               ,press1(j1h,k1)*xyj1(j1h,k1), press2(j2,k2
c     &               )*xyj2(j2,k2)
c                write (*,*) j1h,k1,q1(j1h,k1,1)*xyj(j1h,k1),q1(j1h,k1,2)
c     &               *xyj(j1h,k1),q1(j1h,k1,3)*xyj(j1h,k1),q1(j1h,k1,4)
c     &               *xyj(j1h,k1)
    
                q1(j1h,k1h,4) = psav/gami*rr +
     &             0.5*(q1(j1h,k1h,2)**2+q1(j1h,k1h,3)**2)/q1(j1h,k1h,1)
 110          continue
            endif
c     
            if (turbulnt) then
              do 111 jj=js,je
                j1  = jj
                j1h = jj
                j2  = ifct1*(jdim2 + 1) + ifct2*jj
                turre1(j1h,k1h) = 0.5*(turre1(j1,k1)+turre2(j2,k2))
                vk1(j1h,k1h) = 0.5*(vk1(j1,k1)+vk2(j2,k2))
                ve1(j1h,k1h) = 0.5*(ve1(j1,k1)+ve2(j2,k2))
 111          continue
            endif
          else
            write(n_out,999) nside2
            stop
          endif
        else
          write(n_out,998) nside1
          stop
        endif
c
c
c
c
c
c
c ********************************************************************
c **  Higher-order                                                  **
c ********************************************************************
      elseif (iord.eq.4) then
        tmp=1.d0/6.d0
        if(nside1.eq.1.or.nside1.eq.3) then
c       **  block 1   :   side 1 or 3  **
          if (nside1.eq.1) then
            k1h = kbegin1
            k1a = kbegin1 + 1
            k1b = kbegin1 + 2
          else
            k1h = kend1 
            k1a = kend1 - 1
            k1b = kend1 - 2
          endif
c         *** block 1 - side 1 or 3 connected to block 2 - side 1 or 3 
          if (nside2.eq.1.or.nside2.eq.3) then
            if(nside2.eq.1) then
              k2a  = kbegin2 + 1
              k2b  = kbegin2 + 2
            else
              k2a  = kend2 - 1
              k2b  = kend2 - 2
            endif
            if(nside1.eq.nside2) then
              js = jbegin1
              je = jend1
              ifct1 = 1
              ifct2 = -1
            else
              js = jbegin1
              je = jend1
              ifct1 = 0
              ifct2 = 1
            endif
            if (meanflow) then
              do 210 jj=js,je
                j1  = jj
                j1h = jj
                j2  = ifct1*(jdim2 + 1) + ifct2*jj
                rr = 1./xyj1(j1h,k1h)
c     
                rho1a=q1(j1,k1a,1)*xyj1(j1,k1a)
                rho1b=q1(j1,k1b,1)*xyj1(j1,k1b)
                rho2a=q2(j2,k2a,1)*xyj2(j2,k2a)
                rho2b=q2(j2,k2b,1)*xyj2(j2,k2b)
                qsav=tmp*(-rho1b + 4.d0*(rho1a + rho2a) -rho2b)
                q1(j1h,k1h,1) = qsav*rr
c     
                u1a=q1(j1,k1a,2)*xyj1(j1,k1a)
                u1b=q1(j1,k1b,2)*xyj1(j1,k1b)
                u2a=q2(j2,k2a,2)*xyj2(j2,k2a)
                u2b=q2(j2,k2b,2)*xyj2(j2,k2b)
                qsav=tmp*(-u1b + 4.d0*(u1a+u2a) -u2b)
                q1(j1h,k1h,2) = qsav*rr
c     
                v1a=q1(j1,k1a,3)*xyj1(j1,k1a)
                v1b=q1(j1,k1b,3)*xyj1(j1,k1b)
                v2a=q2(j2,k2a,3)*xyj2(j2,k2a)
                v2b=q2(j2,k2b,3)*xyj2(j2,k2b)
                qsav=tmp*(-v1b + 4.d0*(v1a+v2a) -v2b)
                q1(j1h,k1h,3) = qsav*rr
c     
                p1a=press1(j1,k1a)*xyj1(j1,k1a)
                p1b=press1(j1,k1b)*xyj1(j1,k1b)
                p2a=press2(j2,k2a)*xyj2(j2,k2a)
                p2b=press2(j2,k2b)*xyj2(j2,k2b)
                psav = tmp*(-p1b + 4.d0*(p1a + p2a) -p2b)
c     
                q1(j1h,k1h,4) = psav/gami*rr +
     &             0.5*(q1(j1h,k1h,2)**2+q1(j1h,k1h,3)**2)/q1(j1h,k1h,1)
 210          continue
            endif
c     
            if (turbulnt) then
              do 211 jj=js,je
                j1  = jj
                j1h = jj
                j2  = ifct1*(jdim2 + 1) + ifct2*jj
c                
                tur1a=turre1(j1,k1a)
                tur1b=turre1(j1,k1b)
                tur2a=turre2(j2,k2a)
                tur2b=turre2(j2,k2b)
                turre1(j1h,k1h)= tmp*(-tur1b+4.d0*(tur1a+tur2a)-tur2b)
c     
                v1a=vk1(j1,k1a)
                v1b=vk1(j1,k1b)
                v2a=vk2(j2,k2a)
                v2b=vk2(j2,k2b)
                vk1(j1h,k1h) = tmp*(-v1b+4.d0*(v1a+v2a)-v2b)
                v1a=ve1(j1,k1a)
                v1b=ve1(j1,k1b)
                v2a=ve2(j2,k2a)
                v2b=ve2(j2,k2b)
                ve1(j1h,k1h) = tmp*(-v1b+4.d0*(v1a+v2a)-v2b)
 211          continue
            endif
          else
            write(n_out,999) nside2
            stop
          endif
        else
          write(n_out,998) nside1
          stop
        endif
      endif
c
 998  format('Error in bcavg, nside1=',i2,2x,'nside1 cant= 2 or 4')
 999  format('Error in bcavg, nside2=',i2,2x,'nside2 cant= 2 or 4')
c
      return
      end
