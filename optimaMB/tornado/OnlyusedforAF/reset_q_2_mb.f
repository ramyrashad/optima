c
c resetting the q vector
c
        do k=kbegin,kend
           do j=jbegin,jend
              rho=q(j,k,1)
              u=q(j,k,2)
              v=q(j,k,3)
              pressure=q(j,k,4)
              q(j,k,1)=rho/xyj(j,k)
              q(j,k,2)=q(j,k,1)*u
              q(j,k,3)=q(j,k,1)*v
              q(j,k,4)=(pressure/gami + .5*rho*(u**2+v**2))/xyj(j,k)
              fnu(j,k)=rho*fnu(j,k)*re
           enddo
        enddo
