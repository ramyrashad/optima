C ********************************************************************
C ***        subroutine to read 4 arrays                           ***
C ********************************************************************
      subroutine readarr4(lu,array1,array2,array3,array4,
     &                    jdim,kdim,jbegin,jend,kbegin,kend)

      implicit none

      integer lu,jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision array1(jdim,kdim),array2(jdim,kdim)
      double precision array3(jdim,kdim),array4(jdim,kdim)

      read(lu)  ((array1(j,k),j=jbegin,jend),k=kbegin,kend),
     &          ((array2(j,k),j=jbegin,jend),k=kbegin,kend),
     &          ((array3(j,k),j=jbegin,jend),k=kbegin,kend),
     &          ((array4(j,k),j=jbegin,jend),k=kbegin,kend) 
c
      return 
      end 
