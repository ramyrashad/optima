      subroutine imbc(jdim,kdim,q,xy,xyj,c,d,s,press)
c
c     implicit boundary conditions for airfoil body
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension c(jdim,kdim,4,4),d(jdim,kdim,4,4)
      dimension s(jdim,kdim,4),press(jdim,kdim)
c
c     no variation of metrics w.r.t to time taken into account
c     NOTE: array a and e already set to zero in filpny.f
c
c
      k=kbegin
      k2=k+1
      if (cmesh) then
         jl=jbegin
         ju=jend
      else
         jl=jtail1
         ju=jtail2
      endif
c
      do 1 m=1,4
      do 1 n=1,4
      do 1 j=jlow,jup
         c(j,k,n,m)=0.
         d(j,k,n,m)=0.
c        for cmesh=false you need next line
         if (m.eq.n) c(j,k,n,n)=1.
 1    continue
c
c
c     this if statement relavent for eta sweep later.
      if (.not.cmesh) then
         do 4 n=1,4
            do 2 j=1,jtail1-1
               s(j,k,n)=0.
 2          continue
            do 3 j=jtail2+1,jend
               s(j,k,n)=0.
 3          continue
 4       continue
      endif
c
      do 9 j=jl,ju
         s(j,k,1)=-(q(j,k,1)*xyj(j,k)-q(j,k2,1)*xyj(j,k2))
         s(j,k,2)=-q(j,k,2)*xyj(j,k)
         s(j,k,3)=-q(j,k,3)*xyj(j,k)
         s(j,k,4)=-(press(j,k)*xyj(j,k)-press(j,k2)*xyj(j,k2))
 9    continue
c
      do 10 j=jl,ju
         rr=1./q(j,k,1)
         u=q(j,k,2)*rr
         v=q(j,k,3)*rr
         rr2=1./q(j,k2,1)
         u2=q(j,k2,2)*rr2
         v2=q(j,k2,3)*rr2
c        
         xg=xyj(j,k)*gami
         c(j,k,1,1)=xyj(j,k)
         c(j,k,2,2)=xyj(j,k)
         c(j,k,3,3)=xyj(j,k)
         c(j,k,4,1)=xg*.5*(u**2+v**2)
         c(j,k,4,2)=-xg*u
         c(j,k,4,3)=-xg*v
         c(j,k,4,4)=xg
c        
         xg=xyj(j,k2)*gami
         d(j,k,1,1)=-xyj(j,k2)
         d(j,k,4,1)=-xg*.5*(u2**2+v2**2)
         d(j,k,4,2)=xg*u2                         
         d(j,k,4,3)=xg*v2                         
         d(j,k,4,4)=-xg                                      
c
 10   continue
      return
      end
