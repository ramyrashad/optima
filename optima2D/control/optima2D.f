c----------------------------------------------------------------------
c                                                                       
c        ****    ******   *******  *  **     **  *******
c       *    *   *     *     *     *  * *   * *  *     *
c      *      *  *     *     *     *  *  * *  *  *     *
c      *      *  ******      *     *  *   *   *  ******* 
c      *      *  *           *     *  *       *  *     *
c       *    *   *           *     *  *       *  *     *
c        ****    *           *     *  *       *  *     *  2D
c                                                                       
c----------------------------------------------------------------------

      program optima2D

c----------------------------------------------------------------------
c     -- aerodynamic shape optimization --
c     
c     -- written by: marian nemec --
c     -- date: 2000-2001 --
c     -- no transfer of this program without explicit approval --
c     
c     -- parts of this program contain subroutines from or are based 
c     on the following: ARC2D, CYCLONE, PROBE, HGRID, KSOPT,
c     Numerical Recipes and LAPACK --
c----------------------------------------------------------------------

#ifdef _MPI_VERSION
      use mpi
#endif

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"


      integer ip, jdim, kdim, ndim, ifirst, itfirst, wt_flag, ifunc
      double precision q(maxjk,nblk), cp(maxjk), xy(maxjk,4), xyj(maxjk)
      double precision qp(maxjk,nblk), qold(maxjk,nblk)
      double precision x(maxj,maxk), y(maxj,maxk)
      double precision dx(maxj*maxk*maxincr), dy(maxj*maxk*maxincr)
      double precision lambda(2*maxj*maxk*maxincr)
      double precision fmu(maxj,maxk), vort(maxj,maxk), turmu(maxj,maxk)
      logical wt_update_restart 
      character command*60
csi
      double precision D_hat(maxjk,nblk,6), polars(100,6)
      double precision exprhs(maxjk,nblk)
      double precision a_jk(6,6), fsmach0, re_in0, cl_tar0, H
csi
c     -- grid storage --
      double precision xsave(maxj,maxk), ysave(maxj,maxk)

c     -- solution storage for warm starts during optimization --
      dimension qtmp(maxjk,nblk,nopc)
      
c     -- optimization arrays --
      integer idv(ibsnc), indx(maxj,maxk)
      integer iex(maxjk*4), iren(maxjk), icol(9)
      double precision cp_tar(ibody,2), dvs(ibsnc)
      double precision dvupp(ibsnc), dvlow(ibsnc)
      double precision cp_his(ibody,300,nopc), ac_his(ibody,300)

      integer ndvar, mlim

c     -- b-spline arrays --
      double precision bap(ibody,2), bcp(ibsnc,2), bt(ibody)
      double precision bknot(ibskt)

c     -- sparse adjoint arrays --
      integer ia(maxjk*nblk+2),ipa(maxjk*nblk+2),jpa(maxjk*nblk2*5+1)
      integer ja(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      integer iat(maxjk*nblk+2),ipt(maxjk*nblk+2),jpt(maxjk*nblk2*5+1)
      integer jat(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)

      double precision as(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      double precision ast(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      double precision pa(maxjk*nblk2*5+1),pat(maxjk*nblk2*5+1)

      double precision obj0s(nopc), qwarm(1,maxj,maxk,nblk,nopc)
      
      integer tempvar5, sendProc, commandlen, d100, d10, d1, varStat
      double precision tempvar1, tempvar2, tempvar3, tempvar4(5)

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100), opwk(maxjk,8)
      double precision ru(30000000), dvscale(ibsnc)

      !-- Perform MPI initializaiton so that the MPI libraries may be
      !-- used

#ifdef _MPI_VERSION      

      call MPI_INIT(ierr)

      !-- Identify the processor that is executing this program

      call MPI_COMM_RANK(MPI_COMM_WORLD, rank_glob, ierr)
      call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierr) 

      !-- Determine the number of processors executing the program

      call MPI_COMM_SIZE(MPI_COMM_WORLD, size_glob, ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD, size, ierr)

      !-- Set COMM_CURRENT to MPI_COMM_WORLD for now
      COMM_CURRENT = MPI_COMM_WORLD
      commsgrouped = .false.
         
#endif

      !-- Initialize counter for weight update cycles      
      wt_cycle = 1

      !-- Initialize counter for optimization auto-restarts
 15   opt_cycle = 0

      !-- Initialize counter for continuation restarts
      rest_iter = 1

 20   continue

      !-- Increment counter for optimization auto-restarts

      opt_cycle = opt_cycle + 1

      !-- Increment counter for weight update cycles

      if (wt_update) wt_cycle = wt_cycle + 1

c     -- input parameters --

      call input
      

#ifdef _MPI_VERSION
      call MPI_BARRIER(COMM_CURRENT, ierr)
#endif

      !-- Create a directory for storing grid files and flow solve
      !-- histories at each design iteration 

!#if 0
#ifdef _MPI_VERSION
      if ((rank.eq.0).and.(.not.opt_restart)) then
#else
      if (.not.opt_restart) then
#endif
         command = 'rm -rf history'

         call system(command)

         command = 'mkdir history'

         call system(command)
         
      end if 
!#endif

c     -- define jmaxold; set jdim,kdim --
c     -- for compatibility with O grids and ARC2D code,
c     the gymnastics with jmax, maxj, etc. are complicated --
      jmaxold = jmax                                                    
      jdim    = jmaxold                                                 
      kdim    = kmax   
      ifirst  = 1

clb
      if(opt_cycle.eq.1 .and. .not.opt_restart 
     &     .and. .not. obj_restart) then 
         itfirst = 1
      else
         itfirst = 0
      endif
clb

      if (viscous .and. turbulnt .and. itmodel.eq.2) then
c     -- turbulent case with S-A model --
         ndim = 5
      else
         ndim = 4
      end if

c     -- read grid for initial airfoil geometry --
      ip = 3       
      call ioall ( ip, 0, jdim, kdim, q, qold, cp, work1(1,5),
     &     work1(1,6), work1(1,7), work1(1,8), xy, xyj, x, y, obj0s) 

      if ( klineconst .gt. kmax) then
         klineconst=kmax
      end if

c     angle of the perturbation slot for suction/blowing BC

      sdelta=-datan((y(jsle,1)-y(jsls,1))/(x(jsle,1)-x(jsls,1)))
      omega=0.d0

c     -- input grid sequence and time step parameters --
c     - Note: opt_cycle is added for auto-restart purpose; prevents the
c     grid sequencing stuff from being read more than once...
      if ((opt_cycle.eq.1).and.(.not.wt_update)) then

         ip = 9
         call ioall ( ip, 0, jdim, kdim, q, work1(1,1), cp, work1(1,5),
     &        work1(1,6), work1(1,7), work1(1,8), xy, xyj, x, y, obj0s)          
      endif
 
c     -- setup reordering index array for N-K flow solves and
c     optimization runs --
      if ( nk_its.gt.0 .or.
     &   (.not.flowAnalysisOnly .and. gradient.gt.0)) then

c     -- initial node order and reordering --
         call order (jdim, kdim, jtail1, jtail2, inord, ireord, indx,
     &        ja, ia, jpa, ipa, iren, out_unit)
c     -- setup csr preconditioner and jacobian pointer arrays --
         call set_mat (jdim, kdim, ndim, jtail1, jtail2, indx, icol, ia,
     &        ja, ipa, jpa)
      end if


      ! ----------------------------------------------------------------
      ! Execution Mode for OPTIMA2D (flow solves and optimizations)
      ! ----------------------------------------------------------------

      select case (opt_meth)

      ! ----------------------------------------------------------------
      ! CASE 1: perform a single flow solve (no optimzation)
      ! ----------------------------------------------------------------
      case (1)

         flowAnalysisOnly = .true.

         write(scr_unit,*)
         write(scr_unit,*) 'Starting Analysis Flow Solve'
         write(scr_unit,*) '============================'

         call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &        ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat,
     &        q, qp, qold, cp, xy, xyj, x, y, fmu, vort, turmu, work1,
     &        D_hat,exprhs,a_jk, ifunc)

         !-- Output boundary-layer velocity profile
         if (v_profile) then
            call bl_v_profile(jdim, kdim, ndim, q, xy, x, y)
         end if

      ! ----------------------------------------------------------------
      ! CASE 2: unsteady optimization for airfoils and cylinders
      ! ----------------------------------------------------------------
      case (2)

         flowAnalysisOnly = .false.

c        -- unsteady optimization for Airfoils--

         if (.not. periodic) then 

            write(scr_unit,*)   
            write(scr_unit,*) 
     &           'Starting Unsteady BFGS Optimization for Airfoils'
            write(scr_unit,*) 
     &           '================================================'

            if (.not.sbbc) then
c           -- define shape design variables
               call OptSet(jdim,kdim,cp_tar,dvs,idv,bcp,bap,bt,bknot,
     &           x, y, dx, dy, lambda)

            else
c           -- define design variables and angles for suction/blowing BC

               write(scr_unit,*) 'Optimize suction/blowing conditions'

               ndv=2  
c              ndv=3
               dvs(1)=omegaa
               dvs(2)=omegaf
c              suction/blowing angle with respect to the wing surface of the closed slot
               dvs(3)=sbeta

            end if

         else !periodic

c        -- unsteady optimization for Cylinders--

            write(scr_unit,*) 
            write(scr_unit,*) 
     &           'Starting Unsteady BFGS Optimization for Cylinders'
            write(scr_unit,*) 
     &           '================================================'

c           We specify the dimension ndvar of the sample problem and the number
c           mlim of limited memory corrections stored.
            ndv=2  
            dvs(1)=omegaa
            dvs(2)=omegaf

         end if  !periodic or not

c        We specify the number mlim of limited memory corrections stored.       
         mlim=50
            
         call unsteadyoptimization(jdim,kdim,ndim,ifirst,indx,icol,
     &        iex,ia,ja,as,ipa,jpa,pa,iat,jat, ast, ipt, jpt, pat,q,
     &        qp,qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,
     &        exprhs,a_jk,mlim,dvs,idv,bcp,bap,bt,bknot)

      ! ----------------------------------------------------------------
      ! CASE 3: bfgs optimization
      ! ----------------------------------------------------------------
      case (3)

         flowAnalysisOnly = .false.

         write (scr_unit,122)   
 122     format(/3x,'Starting Steady BFGS Optimization for Airfoils',
     &          /3x,'==============================================',/)

c     -- read target cp distribution, design variables --

         call OptSet(jdim, kdim, cp_tar, dvs, idv, bcp, bap, bt, bknot,
     &        x, y, dx, dy, lambda)

c     -- optimization restart --
         if (opt_restart .or. obj_restart) then
            if (opt_cycle.eq.1) then
               ip = 4                    
               call ioall (ip, 0, jdim, kdim, q, work1(1,1), cp,
     &              work1(1,5), turmu, fmu, vort, xy, xyj, x, y, obj0s) 
            end if
         end if

         ico=0
         icg=0
         call bfgs( jdim, kdim, ndim, ifirst, indx, icol, iex, q,
     &        cp, xy, xyj, x, y, dx, dy, lambda, fmu, vort, turmu,
     &        cp_tar, dvs, idv, bap,bcp,bt, bknot, work1, ia, ja, as,
     &        ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, cp_his,
     &        ac_his, opwk, ifun, iter, opt_tol, nflag, opt_iter, 1,
     &        5.d-16, xsave, ysave, qtmp, itfirst, obj0s)

c     -- spit out the grid whenever BFGS exited cleanly --

#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif
            ip=15
            call ioall (ip,0,jdim,kdim,q,work1(1,1),cp,work1(1,5),
     &        work1(1,6),work1(1,7),work1(1,8),xy,xyj,x,y, obj0s)
#ifdef _MPI_VERSION
         end if
#endif

         write(scr_unit,123) nflag
 123     format( /3x, 'nflag at BFGS exit', i2)

         if (nflag==0) then

            write (scr_unit,124)
 124        format(/3x,'Optimization converged to tolerance',/)

         end if

#ifdef _MPI_VERSION
         !-- Wait until all processors have exited BFGS

         call MPI_BARRIER(COMM_CURRENT, ierr)
#endif

         write(scr_unit,125)
 125     format(/3x,'All processors have exited BFGS',/)

      ! ----------------------------------------------------------------
      ! CASE 4: bfgs optimization with variable angle of attack
      !         performed for M vs Cd at fixed lift
      !         obj_func=6, wd=0.0, wt=0.0
      ! ----------------------------------------------------------------
      case (4)

         flowAnalysisOnly = .false.

         call OptSet(jdim, kdim, cp_tar, dvs, idv, bcp, bap, bt, bknot,
     &        x, y, dx, dy, lambda)

c     -- read in Mach numbers --
         call ioall(2, 0, jdim, kdim, q, qold, press, work1(1,5),
     &        turmu, fmu, vort, xy, xyj, x, y, obj0s)

         do i=1,ipolar
            
            ico=0
            icg=0
            fsmach = polar(i)

            call bfgs( jdim, kdim, ndim, ifirst, indx, icol, iex, q, cp,
     &          xy, xyj, x, y, dx, dy, lambda, fmu, vort, turmu, cp_tar,
     &          dvs, idv, bap, bcp, bt, bknot, work1, ia, ja, as, ipa,
     &          jpa, pa, iat, jat, ast, ipt, jpt, pat, cp_his, ac_his,
     &          opwk, ifun, iter, opt_tol, nflag, opt_iter, 1, 5.d-16,
     &          xsave, ysave, qtmp, itfirst, obj0s)

c     -- check if bfgs converged --
            if (nflag.eq.0) then
               write (opt_unit,*) 'Optimization converged to tolerance'
            end if

c     -- output M vs Cd data --
            call ioall(7, i, jdim, kdim, q, qold, press,
     &           work1(1,5), turmu, fmu, vort, xy, xyj, x, y, obj0s)

            !RR Save output files at each iteration
            !RR namelen = strlen(output_file_prefix0)
            !RR filena = output_file_prefix0
            !RR filena(namelen+1:namelen+3)='.g'
            !RR  
            !RR namelen = strlen(filena)
            !RR command = 'rm -f '
            !RR command(7:6+namelen) = filena
            !RR  
            !RR call system(command)

         end do

      ! ----------------------------------------------------------------
      ! CASE 5: run flow solver with an angle of attack sweep
      ! ----------------------------------------------------------------
      case (5)

         flowAnalysisOnly = .true.

c     -- read in angles of attack --
         call ioall(2, 0, jdim, kdim, q, qold, press, work1(1,5),
     &        turmu, fmu, vort, xy, xyj, x, y, obj0s)

         fsmach = fsmachs(1)
         re_in = reno(1)

         do i=1,ipolar
            alpha = polar(i)

c     -- analysis run --
            call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &           ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat,
     &           q, qp, qold, cp, xy, xyj, x, y, fmu, vort, turmu,work1,
     &           D_hat,exprhs,a_jk, ifunc)

c     -- output aerodynamic coefficients data --
            call ioall(7, i, jdim, kdim, q, qold, press,
     &           work1(1,5), turmu, fmu, vort, xy, xyj, x, y, obj0s)

            ifirst = 1          !cold start each flow solve
            if (hisout) rewind (his_unit)
            if (thisout) rewind (turbhis_unit)

         end do

      ! ----------------------------------------------------------------
      ! CASE 6: run flow solver with variable angle of attack
      !         for M vs Cd at fixed lift
      ! ----------------------------------------------------------------
      case (6)

         flowAnalysisOnly = .true.

c     -- read in Mach numbers --
         call ioall(2, 0, jdim, kdim, q, work1(1,1), press, work1(1,5),
     &        turmu, fmu, vort, xy, xyj, x, y, obj0s)

         do i=1,ipolar
            
            ico=0
            icg=0
            fsmach = polar(i)

c     -- analysis run --
            call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &           ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat,
     &           q, qp, qold, cp, xy, xyj, x, y, fmu, vort, turmu,work1, 
     &           D_hat,exprhs,a_jk, ifunc)

c     -- output M vs Cd data --
            call ioall(7, i, jdim, kdim, q, work1(1,1), press,
     &           work1(1,5), turmu, fmu, vort, xy, xyj, x, y, obj0s)
         end do

      ! ----------------------------------------------------------------
      ! CASE 7: error estimation and/or time step adaptation for
      !         unsteady flows
      ! ----------------------------------------------------------------
      case (7)

         flowAnalysisOnly = .false.

         write(scr_unit,*)
         if (erradap) then
            write(scr_unit,*)'Error estimation and time step adaptation'
     &           // ' for unsteady flows'
            write(scr_unit,*)'========================================='
     &           // '==================='
         else
            write(scr_unit,*)'Only error estimation for unsteady flows'   
            write(scr_unit,*)'========================================'
         end if


         call unsteadyerror(jdim,kdim,ndim,ifirst,indx,icol,
     &        iex,ia,ja,as,ipa,jpa,pa,iat,jat, ast, ipt, jpt, pat,q,
     &        qp,qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,
     &        exprhs,a_jk)

      ! ----------------------------------------------------------------
      ! CASE 8: use SNOPT to perform optimization 
      ! ----------------------------------------------------------------
      case (8)

         flowAnalysisOnly = .false.

         write (scr_unit,126)   
 126     format(/3x,'Starting Steady SNOPT Optimization for Airfoils',
     &          /3x,'===============================================',/)

         !-- Read target cp distribution, design variables 

         call OptSet(jdim, kdim, cp_tar, dvs, idv, bcp, bap, bt, bknot,
     &        x, y, dx, dy, lambda, dvlow, dvupp)

         !-- Optimization restart 

         if (opt_restart .or. obj_restart) then
            ip = 4                    
            call ioall (ip, 0, jdim, kdim, q, work1(1,1), cp,
     &           work1(1,5), turmu, fmu, vort, xy, xyj, x, y, obj0s) 
         end if

         ico=0
         icg=0

         !-- Write the set of Optima2D variables that must be used by 
         !-- the SNOPT external function 'usrfun' to a file. When 
         !-- 'usrfun' is called within SNOPT, it will read in the 
         !-- Optima2D variables from this file.

         if (opt_restart.eq.(.false.)) then
         
            !-- 72 = rest_unit, filename = 'restart'
            rewind(72)

            write(72) ifirst, ifun, ico, rhoinf, pinf, rest_iter
            write(72) (dvscale(i),i = 1,ibsnc)
            write(72) (alphas(i),i = 1,nopc)
            write(72) (alphas_init(i),i = 1,nopc)
            write(72) (obj0s(i),i = 1,mpopt)
            write(72) (((((qwarm(i,j,k,l,m),i=1,warmset),j=1,maxj),
     &        k=1,maxk),l=1,nblk),m=1,mpopt)    
            write(72) xsave!((xsave(j,k),j=1,maxj),k=1,maxk)
            write(72) ysave!((ysave(j,k),j=1,maxj),k=1,maxk)

         end if

         varStat = 0

         !-- Store Optima2D variables in SNOPT user variable 'ru' so
         !-- they can be retrieved in the 'usrfun' subroutine

         call varTransfer(varStat,jdim,kdim,ndim,ifirst,ifun,itfirst,
     &      x,y,idv,bap,bcp,bt,bknot,indx,icol,iex,cp,xy,xyj,cp_tar,
     &      fmu,vort,turmu,ia,ja,ipa,jpa,iat,jat,ipt,jpt,as,ast,pa,
     &      pat,ru,ruIndex)

         call snoptimize(jdim, kdim, ndim, ifirst, indx, icol, iex, q,
     &        cp, xy, xyj, x, y, dx, dy, lambda, fmu, vort, turmu,
     &        cp_tar, dvs, idv, bap,bcp,bt, bknot, work1, ia, ja, as,
     &        ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, cp_his,
     &        ac_his, opwk, ifun, iter, nflag,
     &        xsave, ysave, qtmp, itfirst, obj0s, ru, ruIndex)

      ! ----------------------------------------------------------------
      ! CASE 9: run flow solver to evaluate Cd integral over a
      !         range of Cl and Mach
      ! ----------------------------------------------------------------
      case (9)

         flowAnalysisOnly = .true.

         write (scr_unit,127)   
 127     format(/3x,'Evaluate Cd integral over range of cruise condns',
     &          /3x,'===============================================',/)

         call cdInt (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &     ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, 
     &     qp, qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,
     &     a_jk,ifunc)

      ! ----------------------------------------------------------------
      ! CASE 10: run flow solver with an angle of attack sweep and
      !          with the provided upper and lower lam-turb transition
      !          locations (primarily used for Brican UAV project)
      ! ----------------------------------------------------------------
      case (10)

         flowAnalysisOnly = .true.

         !-- read polar file for sweeps
         read (ip_unit,*) ipolar

c         do i=1,ipolar
c            read(ip_unit,*) polars(i,1), polars(i,2), polars(i,3),
c     &        polars(i,4), polars(i,5)
c         end do  

         do i=1,ipolar
            read(ip_unit,*) polars(i,1)
         end do    

         write (op_unit,201) 
 201     format('VARIABLES= "No" "M" "C_D" "C_L" "C_M" "ALPHA" "RESID"',
     &     '"L_2 GRAD"')

         fsmach0 = fsmachs(1)
         re_in0 = reno(1)
         cl_tar0 = cltars(1)

         do i=1,ipolar
            !re_in =   polars(i,1)
            !alpha =   polars(i,2)
            !fsmach =  polars(i,3)
            !cl_tar =  polars(i,4)
            !transup = polars(i,4)
            !translo = polars(i,5)
            fsmach = polars(i,1)

            H = fsmach/fsmach0 ! Ratio of current mach # to initial
            cl_tar = (H**-2)*cl_tar0
            re_in = H*re_in0
            write(scr_unit,*)'Mach # = ', fsmach
            write(scr_unit,*)'Target lift coefficient = ', cl_tar
            write(scr_unit,*)'Reynolds # = ', re_in


            !-- analysis run
            call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &           ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat,
     &           q, qp, qold, cp, xy, xyj, x, y, fmu, vort, turmu,work1,
     &           D_hat,exprhs,a_jk, ifunc)

            !-- output aerodynamic coefficients data
            call ioall(7, i, jdim, kdim, q, qold, press,
     &           work1(1,5), turmu, fmu, vort, xy, xyj, x, y, obj0s)

            ifirst = 1          !cold start
            if (hisout) rewind (his_unit)
            if (thisout) rewind (turbhis_unit)
         end do

      ! ----------------------------------------------------------------
      ! CASE 11: Hybrid Optimization
      !          Follow the SNOPT procedure as close as possible
      ! ----------------------------------------------------------------
      case (11)

#ifdef _MPI_VERSION
         flowAnalysisOnly = .false.

         !-- Read target cp distribution, design variables 
         call OptSet(jdim, kdim, cp_tar, dvs, idv, bcp, bap, bt, bknot,
     &        x, y, dx, dy, lambda, dvlow, dvupp)
 
         ico=0
         icg=0

         !-- See SNOPT setup above for an explanation of this...
         if (opt_restart.eq.(.false.)) then
            rewind(72)
            write(72) ifirst, ifun, ico, rhoinf, pinf, rest_iter
            write(72) (dvscale(i),i = 1,ibsnc)
            write(72) (alphas(i),i = 1,nopc)
            write(72) (alphas_init(i),i = 1,nopc)
            write(72) (obj0s(i),i = 1,mpopt)
            write(72) (((((qwarm(i,j,k,l,m),i=1,warmset),j=1,maxj),
     &        k=1,maxk),l=1,nblk),m=1,mpopt)
            write(72) xsave!((xsave(j,k),j=1,maxj),k=1,maxk)
            write(72) ysave!((ysave(j,k),j=1,maxj),k=1,maxk)
         end if

         !-- I will store the optima2D variables in a global array
         !-- ru_global, thus avoiding the need to pass ru to the
         !-- hybropt and back
         varStat = 0
         call varTransfer(varStat,jdim,kdim,ndim,ifirst,ifun,itfirst,
     &      x,y,idv,bap,bcp,bt,bknot,indx,icol,iex,cp,xy,xyj,cp_tar,
     &      fmu,vort,turmu,ia,ja,ipa,jpa,iat,jat,ipt,jpt,as,ast,pa,
     &      pat,ru_global,ruIndex)

         !-- Finally, go to the hybridoptimize() subroutine, which will
         !-- set up the optimization problem and call hybropt
         call hybridoptimize(jdim, kdim, ndim, ifirst, indx, icol, iex, 
     &                       q, cp, xy, xyj, x, y, dx, dy, lambda, fmu,
     &                       vort, turmu, cp_tar, dvs, dvlow, dvupp, 
     &                       idv, bap, bcp,
     &                       bt, bknot, work1, ia, ja, as, ipa, jpa, 
     &                       pa, iat, jat, ast, ipt, jpt, pat, cp_his,
     &                       ac_his, opwk, ifun, iter, nflag,
     &                       xsave, ysave, qtmp, itfirst, obj0s, 
     &                       ruIndex)
         
#else
         stop 'Optima2D.f:hybrid optimization must be compiled with MPI'
#endif
      ! ----------------------------------------------------------------
      ! CASE 12: run flow solver with a lift coefficient sweep
      ! ----------------------------------------------------------------
      case (12)

         flowAnalysisOnly = .true.

         !-- read polar file for sweeps
         read (ip_unit,*) ipolar

         do i=1,ipolar
            read(ip_unit,*) polars(i,1), polars(i,2), polars(i,3),
     &        polars(i,4), polars(i,5)
         end do    

         write (op_unit,202) 
 202     format('VARIABLES= "No" "Re" "M" "C_D" "C_L" "C_M" '
     &          '"ALPHA" "RESID" "L2_GRAD "XTRup" "XTRSup" ',
     &          '"XTRlo" "XTRSlo"')

!         fsmach = fsmachs(1)
!         re_in = reno(1)
!         alpha  = alphas(1)

         do i=1,ipolar
            re_in   = polars(i,1)
            alpha   = polars(i,2) ! this is the initial guess for alpha
            cl_tar  = polars(i,3) ! this is the target lift coefficient
            fsmach  = polars(i,4)
            transup = polars(i,5)
            translo = polars(i,6)

            write(scr_unit,*) ' RR: iteration details'
            write(scr_unit,*) ' re_in   = ', re_in
            write(scr_unit,*) ' alpha   = ', alpha
            write(scr_unit,*) ' cl_tar  = ', cl_tar
            write(scr_unit,*) ' fsmach  = ', fsmach
            write(scr_unit,*) ' transup = ', transup
            write(scr_unit,*) ' translo = ', translo

            !-- analysis run
            call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &           ia, ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat,
     &           q, qp, qold, cp, xy, xyj, x, y, fmu, vort, turmu,work1,
     &           D_hat,exprhs,a_jk, ifunc)

            !-- output data
            call ioall(31, i, jdim, kdim, q, qold, press,
     &           work1(1,5), turmu, fmu, vort, xy, xyj, x, y, obj0s)

            ifirst = 1          !cold start
            rewind (his_unit)
            rewind (turbhis_unit)
         end do

      ! ----------------------------------------------------------------
      ! CASE DEFAULT: stop program with error message
      ! ----------------------------------------------------------------
      case default

         write (opt_unit,*) ' OPT_METH assigned wrong value '
         stop ' OPT_METH assigned wrong value '
 
      end select


 10   continue

      if ((auto_restart) .and.
     &     (opt_cycle.lt.num_restarts) .and.
     &     (opt_meth==3) .and.
     &     ((nflag==5).or.(nflag==1))) then
         if (iter==1) then
            write(scr_unit,*) 'First line search did not converge'
            write(scr_unit,*) 
     & 'No reason to continue with automatic restarts'
            write(scr_unit,*) 'No new airfoil stored'
            write(scr_unit,*) 'Exiting optima'
            opt_cycle = num_restarts
         end if
      end if

      if ((auto_restart) .and.
     &     (opt_cycle.lt.num_restarts) .and.
     &     (opt_meth==3) .and.
     &     ((nflag==5).or.(nflag==1))) then
c     -- If doing a BFGS optimization (opt_method=3)
c     -- AND line search stalled (nflag=5)
c     -- AND we want an automatic restart (auto_restart=true)
c     -- AND we haven't reached the maximum number of restarts, then --

         write(scr_unit,*) '------------------------------'
         write(scr_unit,*) '-- AUTOMATICALLY RESTARTING --'
         write(scr_unit,*) '------------------------------'

         do i = 1,mpopt
            alphas(i) = alpha
         enddo

         goto 20                !go back to the restart..

      else if((opt_meth.eq.3) .and.
     &     (opt_cycle.lt.num_restarts) .and.
     &     (wt_update)) then
      !-- If doing a BFGS optimization (opt_method=3)
      !-- AND we haven't reached the maximum number of restarts,
      !-- AND we wish to restart optimization with updated off-design
      !-- weights   

         write(scr_unit,*) 
     &   '-----------------------------------------------------'
         write(scr_unit,*) 
     &   '--  RESTARTING WITH UPDATED OFF-DESIGN WEIGHTINGS  --'
         write(scr_unit,*) 
     &   '-----------------------------------------------------'


         !-- Make copies of .g, .bsp, .dvs, and .obj files for restarting
         !-- the next weight update cycle
#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif
            d100 = (wt_cycle / 100)
            d10  = (wt_cycle / 10) - (d100*10)
            d1   = (wt_cycle / 1) - (d100*100) - (d10*10)

            dirname = char(d100+48)//char(d10+48)//char(d1+48)

            !-- Copy .g file

            namelen = len_trim(grid_file_prefix)
            command = 'cp '
            command(4:namelen+3) = grid_file_prefix
            command(namelen+4:namelen+5) = '.g'
               
            commandlen = namelen+6
            command(commandlen+1:commandlen+3) = dirname
            command(commandlen+4:commandlen+5) = '.g'
            write(scr_unit,*) command

            call system(command)

            !-- Copy .bsp file

            namelen = len_trim(output_file_prefix0)
            command = 'cp '
            command(4:namelen+3) = output_file_prefix0
            command(namelen+4:namelen+7) = '.bsp'
               
            commandlen = namelen+8
            command(commandlen+1:commandlen+3) = dirname
            command(commandlen+4:commandlen+7) = '.bsp'
            write(scr_unit,*) command

            call system(command)

            !-- Copy .dvs file

            namelen = len_trim(output_file_prefix0)
            command = 'cp '
            command(4:namelen+3) = output_file_prefix0
            command(namelen+4:namelen+7) = '.dvs'
               
            commandlen = namelen+8
            command(commandlen+1:commandlen+3) = dirname
            command(commandlen+4:commandlen+7) = '.dvs'
            write(scr_unit,*) command

            call system(command)

            !-- Copy .obj file

            namelen = len_trim(output_file_prefix0)
            command = 'cp '
            command(4:namelen+3) = output_file_prefix0
            command(namelen+4:namelen+7) = '.obj'
               
            commandlen = namelen+8
            command(commandlen+1:commandlen+3) = dirname
            command(commandlen+4:commandlen+7) = '.obj'
            write(scr_unit,*) command

            call system(command)
#ifdef _MPI_VERSION
         end if
#endif

         !-- Close all I/O units

         write(scr_unit,189)
 189     format(3x,'Closing all I/O units before starting the 
     & next weight update cycle',/)
         
         do i = 1, 100

            close(i)

         end do
 
         !-- Build a command that creates a numbered folder 
         !-- corresponing to the weight update cycle
#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif          
            d100 = (wt_cycle / 100)
            d10  = (wt_cycle / 10) - (d100*10)
            d1   = (wt_cycle / 1) - (d100*100) - (d10*10)

            dirname = char(d100+48)//char(d10+48)//char(d1+48)

            command = 'mkdir '
            command(7:39) = dirname
            call system(command)

            !-- Move all output files from the completed weight update
            !-- cycle into the corresponding numbered folder

            command = 'mv rest*'
            command(10:39) = dirname
            write(*,*) command
            call system(command)

            !-- Move the 'history' folder from the completed weight 
            !-- update cycle into the corresponding numbered folder

            command = 'mv history '
            command(12:39) = dirname
            write(*,*) command
            call system(command)

            !-- Create a directory (for the next weight update cycle)  
            !-- for storing grid files and flow solve histories at each 
            !-- design iteration 

            command = 'mkdir history'
            write(*,*) command
            call system(command)
      
#ifdef _MPI_VERSION         
         end if
#endif

#ifdef _MPI_VERSION
         
         !-- Broadcast the numbered folder name to all processors

         call MPI_BCAST(dirname,40,MPI_CHARACTER,0,
     &                   COMM_CURRENT,ierr)
#endif

         goto 15                !go back to the restart..

      else if((opt_meth.eq.3) .and.
     &     (opt_cycle.lt.num_restarts) .and.
     &     ((nflag).eq.(-1))) then

      !-- If doing a BFGS optimization (opt_method=3)
      !-- AND we haven't reached the maximum number of restarts,
      !-- AND we had a line search stall during a weight update 
      !-- cycle
         if (wt_update) then
            write(scr_unit,*) 
     &   '-----------------------------------------------------'
            write(scr_unit,*) 
     &   '--  RESTARTING WITH UPDATED OFF-DESIGN WEIGHTINGS  --'
            write(scr_unit,*) 
     &   '-----------------------------------------------------'
         else
            write(scr_unit,*)
     &   '-------------------------------------------------------'
            write(scr_unit,*) 
     &   '--  RESTARTING WITH UNCHANGED OFF-DESIGN WEIGHTINGS  --'
            write(scr_unit,*) 
     &   '-------------------------------------------------------'
         end if


         !-- Make copies of .g, .bsp, .dvs, and .obj files for restarting
         !-- the next weight update cycle
#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif

            d100 = (wt_cycle / 100)
            d10  = (wt_cycle / 10) - (d100*10)
            d1   = (wt_cycle / 1) - (d100*100) - (d10*10)

            dirname = char(d100+48)//char(d10+48)//char(d1+48)

            !-- Copy .g file

            namelen = len_trim(grid_file_prefix)
            command = 'cp '
            command(4:namelen+3) = grid_file_prefix
            command(namelen+4:namelen+5) = '.g'
               
            commandlen = namelen+6
            command(commandlen+1:commandlen+3) = dirname
            command(commandlen+4:commandlen+5) = '.g'
            write(scr_unit,*) command

            call system(command)

            !-- Copy .bsp file

            namelen = len_trim(output_file_prefix0)
            command = 'cp '
            command(4:namelen+3) = output_file_prefix0
            command(namelen+4:namelen+7) = '.bsp'
               
            commandlen = namelen+8
            command(commandlen+1:commandlen+3) = dirname
            command(commandlen+4:commandlen+7) = '.bsp'
            write(scr_unit,*) command

            call system(command)

            !-- Copy .dvs file

            namelen = len_trim(output_file_prefix0)
            command = 'cp '
            command(4:namelen+3) = output_file_prefix0
            command(namelen+4:namelen+7) = '.dvs'
               
            commandlen = namelen+8
            command(commandlen+1:commandlen+3) = dirname
            command(commandlen+4:commandlen+7) = '.dvs'
            write(scr_unit,*) command

            call system(command)

            !-- Copy .obj file

            namelen = len_trim(output_file_prefix0)
            command = 'cp '
            command(4:namelen+3) = output_file_prefix0
            command(namelen+4:namelen+7) = '.obj'
               
            commandlen = namelen+8
            command(commandlen+1:commandlen+3) = dirname
            command(commandlen+4:commandlen+7) = '.obj'
            write(scr_unit,*) command

            call system(command)
#ifdef _MPI_VERSION
         end if
#endif

         !-- Close all I/O units

         write(scr_unit,189)
         
         do i = 1, 100

            close(i)

         end do
 
         !-- Build a command that creates a numbered folder 
         !-- corresponing to the weight update cycle
#ifdef _MPI_VERSION
         if (rank.eq.0) then
#endif          
            d100 = (wt_cycle / 100)
            d10  = (wt_cycle / 10) - (d100*10)
            d1   = (wt_cycle / 1) - (d100*100) - (d10*10)

            dirname = char(d100+48)//char(d10+48)//char(d1+48)

            command = 'mkdir '
            command(7:39) = dirname
            call system(command)

            !-- Move all output files from the completed weight update
            !-- cycle into the corresponding numbered folder

            command = 'mv rest*'
            command(10:39) = dirname
            write(*,*) command
            call system(command)

            !-- Move the 'history' folder from the completed weight 
            !-- update cycle into the corresponding numbered folder

            command = 'mv history '
            command(12:39) = dirname
            write(*,*) command
            call system(command)

            !-- Create a directory (for the next weight update cycle)  
            !-- for storing grid files and flow solve histories at each 
            !-- design iteration 

            command = 'mkdir history'
            write(*,*) command
            call system(command)
#ifdef _MPI_VERSION               
         end if
#endif
      
#ifdef _MPI_VERSION   
         !-- Broadcast the numbered folder name to all processors

         call MPI_BCAST(dirname,40,MPI_CHARACTER,0,
     &                   COMM_CURRENT,ierr)
#endif

         goto 15                !go back to the restart..

      else
c     -- Otherwise... --
         if (opt_cycle.ge.num_restarts) then
            write(scr_unit,*)
            write(scr_unit,*) 'Max. no. of auto restarts exceeded'
         endif
         write(scr_unit,*)
         write(scr_unit,*) 'End Optimization'
         write(scr_unit,*)
      endif

      !-- Clean up any unfinished business left by MPI before terminating
      !-- Optima2D
#ifdef _MPI_VERSION 
      call MPI_FINALIZE(ierr)
#endif

      stop
      end                       !optima2D
