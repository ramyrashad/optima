c ##########################
c ##                      ##
c ##  subroutine filtery  ##
c ##                      ##
c ##########################
c
      subroutine filtery (jdim,kdim,q,s,s0,xyj,coef2,coef4,sndsp,precon,
     >                    work,tmp)
c
c********************************************************************
c
c    fourth order smoothing, added explicitly to rhs
c second order near shocks with pressure grd coeff.
c
c               eta direction
c
c   start differences each variable separatly
c********************************************************************
      use disscon_vars
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)
      dimension s0(jdim,kdim,4)
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)
      dimension work(jdim,kdim,3),precon(jdim,kdim,6),tmp(jdim,kdim,4)
      double precision tmpresid, workresid, work1resid, work2resid
      double precision coef2resid, coef4resid
            
      do 39 n = 1,4
c
c
c---- 1st-order forward difference ----
      do 35 k = kbegin,kup
      kpl = k+1
      do 35 j = jlow,jup
         work(j,k,1) = q(j,kpl,n)*xyj(j,kpl) - q(j,k,n)*xyj(j,k)
 35   continue
c
c
c---- for fourth order: apply cent-dif to 1st-order forward ----
      do 36 k = klow,kup-1
      kpl = k+1
      kmi = k-1
      do 36 j = jlow,jup
         work(j,k,2) = work(j,kpl,1) - 2.d0* work(j,k,1) + work(j,kmi,1)
 36   continue
c  -- next points to boundaries: rows 2 & kmax-1 --
      kb = kbegin
      do 37 j = jlow,jup
      work(j,kbegin,2) = q(j,kb+2,n)*xyj(j,kb+2) -
     >                2.d0*q(j,kb+1,n)*xyj(j,kb+1) + q(j,kb,n)*xyj(j,kb)
      work(j,kup,2)    = work(j,kup-1,1) - work(j,kup,1)
 37   continue
c
c
      !-- Initialize work array before forming dissipation term
      do 78 k=1,kdim
      do 78 j=1,jdim
            work(j,k,3) = 0.0
 78   continue

c---- form dissipation term before last dif ----
      do 38 k = kbegin,kup
      do 38 j = jlow,jup
         work(j,k,3) = (coef2(j,k)*work(j,k,1)-coef4(j,k)*work(j,k,2))
 38   continue
c
c
      !-- Initialize tmp array before last differentiation      
      do 81 k=1,kdim
      do 81 j=1,jdim
         tmp(j,k,n) = 0.0
 81   continue

c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)
      do 40 k =klow,kup
      do 40 j =jlow,jup
         tmp(j,k,n) = (work(j,k,3) - work(j,k-1,3))*dtd
 40   continue
c
 39   continue
c
cdu   precondition the dissipation operator
c
c      if (prec.gt.0)
c     >call predis(jdim,kdim,q,xyj,sndsp,precon,tmp)
c
      if (dissCon) then
         do 200 n=1,4
         do 200 k=klow,kup
         do 200 j=jlow,jup
            s0(j,k,n)=s0(j,k,n) + tmp(j,k,n)
            s(j,k,n)=s(j,k,n) + tmp(j,k,n)
 200     continue
      else
         do 201 n=1,4
         do 201 k=klow,kup
         do 201 j=jlow,jup
            s(j,k,n)=s(j,k,n) + tmp(j,k,n)
 201     continue
      end if
c
      if (flbud .and.
     &   (mod(numiter-istart+1,100).eq.0 .or. numiter.eq.iend)) then
         n=2
         do 210 k=klow,kup
         do 210 j=jlow,jup
            budget(j,k,2)=budget(j,k,2) + tmp(j,k,n)
 210     continue
      endif
c
      return
      end
