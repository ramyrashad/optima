      subroutine imfar(jdim,kdim,k1,k2,q,xy,xyj,a,b,c,s,press,sndsp)
c
c     implicit boundary conditions for farfield boundaries
#include "../include/arcom.inc"
c
      dimension q(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension c(jdim,kdim,4,4),b(jdim,kdim,4,4),a(jdim,kdim,4)
      dimension tm1(4,4),tm2(4,4)
      dimension s(jdim,kdim,4),press(jdim,kdim),sndsp(jdim,kdim)
c
c     no variation of metrics w.r.t to time taken into account
c     NOTE: array a and e already set to zero in filpny.f
c
      k=k1
      do 2 m=1,4
      do 2 n=1,4
      do 2 j=jlow,jup
         b(j,k,n,m)=0.
         c(j,k,n,m)=0.
 2    continue
c
c      print *,' '
c      print *,'numiter=',numiter
      do 10 j=jlow,jup
         rho=q(j,k,1)*xyj(j,k)
         rhoinv=1./rho
         rhog=rho**gamma
         rr=rhoinv*xyj(j,k)
         u=q(j,k,2)*rr
         v=q(j,k,3)*rr
         xys=sqrt(xy(j,k,3)**2+xy(j,k,4)**2)
         xyx=xy(j,k,3)/xys
         xyy=xy(j,k,4)/xys
         vn=xyx*u+xyy*v
         vt=xyy*u-xyx*v
         p=press(j,k)*xyj(j,k)
         snd=sqrt(gamma*p*rhoinv)
c
         rho2=q(j,k2,1)*xyj(j,k2)
         rhoinv2=1./rho2
         rhog2=rho2**gamma
         rr2=rhoinv2*xyj(j,k2)
         u2=q(j,k2,2)*rr2
         v2=q(j,k2,3)*rr2
         xys2=sqrt(xy(j,k2,3)**2+xy(j,k2,4)**2)
         xyx2=xy(j,k2,3)/xys2
         xyy2=xy(j,k2,4)/xys2
         vn2=xyx2*u2+xyy2*v2
         vt2=xyy2*u2-xyx2*v2
         p2=press(j,k2)*xyj(j,k2)
         snd2=sqrt(gamma*p2*rhoinv2)
c
c        note: entropy_inf=rho_inf**gamma / p_inf = gamma
c              u_inf = freestream mach number * cosang
c              v_inf = freestream mach number * sinang
c              Vn_inf= xyx*u_inf + xyy*v_inf
c              a_inf = 1
c
c
         gg=2./gami
         r1=vn-snd*gg
         r2=vn+snd*gg
         r22=vn2+snd2*gg
c         ainf=.25*(r2-r1)/gg
c         r1inf=xyx*uinf+xyy*vinf-gg*ainf
         r1inf=xyx*uinf+xyy*vinf-gg
         s(j,k,1)=-(r1-r1inf)
         s(j,k,2)=-(r2-r22)
c
         c(j,k,1,1)=snd*rhoinv/gami
         c(j,k,2,1)=-c(j,k,1,1)
         c(j,k,3,1)=-snd**2/rhog
c
         c(j,k,1,2)=xyx
         c(j,k,2,2)=xyx
         c(j,k,4,2)=xyy
c
         c(j,k,1,3)=xyy
         c(j,k,2,3)=xyy
         c(j,k,4,3)=-xyx
c
         c(j,k,1,4)=-gamma*rhoinv/(snd*gami)
         c(j,k,2,4)=-c(j,k,1,4)
         c(j,k,3,4)=1./rhog
c
c        note: first row of b is zero.
         b(j,k,2,1)=snd2*rhoinv2/gami
         b(j,k,2,2)=-xyx2
         b(j,k,2,3)=-xyy2
         b(j,k,2,4)=-gamma*rhoinv2/(snd2*gami)
c
c        the rest are the last 2 rows
         vninf=xyx*uinf+xyy*vinf
         if (vninf.gt.0.) then
c           vn > 0 for outflow. 
c           take info from interior           
            b(j,k,3,1)=snd2**2/rhog2
            b(j,k,4,2)=-xyy2
            b(j,k,4,3)=xyx2
            b(j,k,3,4)=-1./rhog2
            s(j,k,3)=-(p/rhog-p2/rhog2)
            s(j,k,4)=-(vt-vt2)
         else
c           info from freestream
            s(j,k,3)=-(p/rhog - 1./gamma)
            s(j,k,4)=-(vt  -  (xyy*uinf-xyx*vinf))
         endif
c
         do 5 m=1,4
         do 5 n=1,4
            tm1(n,m)=0.
            tm2(n,m)=0.
 5       continue

         tm1(1,1)=1.
         tm1(2,1)=-u*rhoinv
         tm1(3,1)=-v*rhoinv
         tm1(4,1)=gami*.5*(u**2+v**2)
         tm1(2,2)=rhoinv
         tm1(4,2)=-gami*u
         tm1(3,3)=rhoinv
         tm1(4,3)=-gami*v
         tm1(4,4)=gami
c
         tm2(1,1)=1.
         tm2(2,1)=-u2*rhoinv2
         tm2(3,1)=-v2*rhoinv2
         tm2(4,1)=gami*.5*(u2**2+v2**2)
         tm2(2,2)=rhoinv2
         tm2(4,2)=-gami*u2
         tm2(3,3)=rhoinv2
         tm2(4,3)=-gami*v2
         tm2(4,4)=gami
c
         do 9 m=1,4
         do 9 n=1,4
            sum=0.
            do 7 i=1,4
               sum=sum+c(j,k,m,i)*tm1(i,n)
 7          continue
            c(j,k,m,n)=sum*xyj(j,k)
c
            sum=0.
            do 8 i=1,4
               sum=sum+b(j,k,m,i)*tm2(i,n)
 8          continue
            b(j,k,m,n)=sum*xyj(j,k2)
 9       continue
c         if (numiter.eq.iend) print *,j,vn,vt
 10   continue
c
      return
      end
