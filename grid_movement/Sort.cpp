#ifndef TAGSORT_H
#include "Sort.h"
#endif

void tagSort(const int* dataArr, int *tagArr, const int& size)
{
    int k, indexOfMin, pass;
    for (k = 0; k < size; k++)
        tagArr[k] = k;

    for (pass = 0; pass < size - 1; pass++)
    {
        indexOfMin = pass;

        for (k = pass + 1; k < size; k++)
	  if (dataArr[ tagArr[k] ] < dataArr[tagArr[indexOfMin]])
                indexOfMin = k;

        if (indexOfMin != pass)
            swap2 (tagArr[pass], tagArr[indexOfMin] );
    }
}// end tagSort( )



// swap function defined for integers
void swap2 ( int& x, int& y )
{
	int temp;
	temp = x;
	x = y;
	y = temp;
}

// sort an array of integers and move the elements in
// the val array accorespondingly
void bubbleSort ( int*& arr, double*& val, const int& size )
{
   int last = size - 2;
   int isChanged = 1;
   int k;

   while ( last >= 0 && isChanged )
   {
           isChanged = 0;
           for ( k = 0; k <= last; k++ )
               if ( arr[k] > arr[k+1] )
               {
		 swap2 ( arr[k], arr[k+1] );
		 swap2 ( val[k], val[k+1] );
		 isChanged = 1;
               }
           last--;
   }
}

void swap2 ( double& x, double& y )
{
  double temp;
   temp = x;
   x = y;
   y = temp;
} 


// http://www.cise.ufl.edu/~ddd/cis3020/summer-97/lectures/lec17/
// http://linux.wku.edu/~lamonml/algor/sort/quick.html

void quickSort(int*& numbers, int& start, int& array_size)
{
  q_sort(numbers, start, array_size);
}


void q_sort(int*& numbers, int& left, int& right)
{
  int pivot, l_hold, r_hold;

  right = right-1;

  l_hold = left;
  r_hold = right;
  pivot = numbers[left];
  while (left < right)
  {
    while ((numbers[right] >= pivot) && (left < right))
      right--;
    if (left != right)
    {
      numbers[left] = numbers[right];
      left++;
    }
    while ((numbers[left] <= pivot) && (left < right))
      left++;
    if (left != right)
    {
      numbers[right] = numbers[left];
      right--;
    }
  }
  numbers[left] = pivot;
  pivot = left;
  left = l_hold;
  right = r_hold;
  if (left < pivot)
    q_sort(numbers, left, pivot);
  if (right > pivot)
    {
      pivot ++;
      right ++;
      q_sort(numbers, pivot, right);
    }
}
