C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C              SSSS   EEEEE  L      EEEEE   CCCC   TTTTT               C
C             S       E      L      E      C    C    T                 C
C              SSSS   EEEE   L      EEEE   C         T                 C
C                  S  E      L      E      C    C    T                 C
C              SSSS   EEEEE  LLLLL  EEEEE   CCCC     T                 C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE SELECT(N,nchrom,ngene,nobfun,pop,fit,irank,iselect,
     $     ndim,pop_acc,fit_acc,nacc,i_minmax,fit_init,nbin,is_bin,
     $     fitmin,fitmax,fitavg,ncfitmin,ncfitmax,ncrmsmin,ncrmsmax,
     $     fitrmsmin,fitrmsmax,debug)
      SAVE
      real*8 rand
      logical box_over,debug
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** THIS SUBROUTINE SELECTS THE NEW POPULATION              C
C              DISTRIBUTION IN A FASHION THAT IS HEAVILY BIASED        C
C              TOWARD THE MORE FIT INDIVIDUALS. AFTER SELECTION        C
C              FOUR OPERATORS ARE USED (PASS THROUGH, RANDOM           C
C              AVERAGE CROSSOVER, PERTURBATION MUTATION AND            C
C              RANDOM MUTATION) TO MODIFY THE SELECTED                 C
C              INDIVIDUALS FOR THE NEXT GENERATION ***                 C
C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      dimension pop(ngene,nchrom),fit(nobfun,nchrom),irank(nchrom)
      dimension pop_acc(ngene,ndim),fit_acc(nobfun,ndim)
      dimension fitmin(nobfun),fitmax(nobfun),fitavg(nobfun)
      dimension ncfitmin(nobfun),ncfitmax(nobfun)
C
      
      dimension ZZZ(ngene,nchrom),ZZZFIT(nobfun,nchrom),FITRMS(nchrom)
      dimension zzzrank(nchrom)
      dimension i_chrom(nchrom)
C-----------------------------------------------------------------------
      if(debug)write(49,*)' Call to select'
C
C-----------------------------------------------------------------------
C          *** SELECT THE INDIVIDUALS FROM THE CURRENT
C              POPULATION (POP) THAT WILL BE USED TO CREATE
C              THE NEW POPULATION. THESE VALUES ARE STORED
C              IN THE ZZZ HOLDING ARRAY. THE SELECTION 
C              PROCESS IS HEAVILY BIASED TOWARD THE MORE
C              FIT INDIVIDUALS IN THE OLD POPULATION
C              DISTRIBUTION. WHEN THE ZZZ ARRAY IS FILLED
C              WITH NCHROM INDIVIDUALS TERMINATE THE 
C              SELECTION PROCESS. THUS, THE POPULATION
C              SIZE WILL ALWAYS REMAIN THE SAME. TWO OPTIONS
C              ARE AVAILABLE FOR THE SELECTION OPERATOR:
C              ISELECT=1...GREEDY SELECTION.
C                          MULTIPLE PASS SELECTION. IN THIS
C                          APPROACH THE NEXT GENERATION OF
C                          CHROMOSOMES ARE SELECTED FROM THE
C                          PREVIOUS GENERATION BY USING 
C                          MULTIPLE PASSED THROUGH THE 
C                          POPULATION LIST. ON THE FIRST PASS
C                          ALL IRANK=1 CHROMOSOMES ARE INCLUDED
C                          ON THE SECOND PASS ALL IRANK.LE.2
C                          ARE INCLUDED, AND SO ON, UNTIL 
C                          NCHROM CHROMOSOMES ARE SELECTED.
C              ISELECT=2...TOURNAMENT SELECTION. THE FIRST 
C                          NOBFUN CHROMOSOMES SELECTED ARE 
C                          THOSE THAT HAVE THE NOBFUN MAXIMUM
C                          FITNESS VALUES. THE NOBFUN+1 
C                          CHROMOSOME SELECTED IS THE ONE THAT
C                          HAS THE BEST NORMALIZED RMS FITNESS.
C                          THE REMAINING CHROMOSOMES (NOBFUN+2,
C                          NOBFUN+3,...,NCHROM) ARE SELECTED
C                          USING A STANDARD RANDOM TOURNAMENT
C                          PROCESS ***
C
C              ISELECT=3...Same as iselect=1 without forcing max or min choices
C              ISELECT=4...Same as iselect=2 without forcing max or min choices
c
c              ISELECT=5...Selection from the accumulation file
c              ISELECT=6...Selection from the accumulation file with endpoints from
c                          active file
c              iselect=7...Simple bin selection ala Holst
c              iselect=8...New bin selection ala Holst
c

C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** MULTIPLE PASS SELECTION ("GREEDY SELECTION"). 
C              THIS LOGIC WILL WORK FOR ANY POPULATION 
C              DISTRIBUTION SO LONG AS THE NUMBER OF 
C              CHROMOSOMES DOES NOT EXCEED 2048 (CURRENT 
C              DIMENSIONS). IF A LARGER NUMBER OF INDIVIDUALS 
C              IS REQUIRED ITMAX MAY HAVE TO BE INCREASED ***
C
      if(debug)then
         write(49,*)'before selection'
         write(49,*)' nc    irank      fit = '
         write(49,*)' nc          pop = '
         do nc = 1,nchrom
            write(49,'(i5,2x,i5,3(2x,e24.16))')
     $           nc,irank(nc),(fit(no,nc),no=1,nobfun)
c$$$            write(49,'(i5,3(2x,e24.16))')
c$$$     $           nc,(pop(ng,nc),no=1,ngene)
         enddo
      endif

      IF (ISELECT.EQ.1 .or. iselect.eq.3 )then
C
C-----------------------------------------------------------------------
C          *** START THE SELECTION PROCESS BY INCLUDING THE 
C              NOBFUN CHROMOSOMES ASSOCIATED WITH THE MAXIMUM
C              FITNESS VALUES ***
C
         NCSTART = 1
         ICOUNT  = 1
         IF (ISELECT.EQ.1) THEN

            DO NC=1,NOBFUN
               if(i_minmax.eq.1)NC1 = NCFITMAX(NC)
               if(i_minmax.eq.0)NC1 = NCFITMIN(NC)
               DO NG=1,NGENE
                  ZZZ(NG,NC) = POP(NG,NC1)
               ENDDO
               DO NO=1,NOBFUN
                  ZZZFIT(NO,NC) = FIT(NO,NC1)
               ENDDO
               zzzrank(nc) = irank(nc1)
CCCC            NCSTART = NOBFUN+1
               if(debug)write(49,201)nc,irank(nc1),nc1
            ENDDO
            ICOUNT  = NOBFUN+1
         endif
C
C-----------------------------------------------------------------------
C          *** COMPLETE THE SELECTION PROCESS USING THE
C              MULTIPLE PASS LOGIC ***
C
         ITMAX  = 45
         DO IT=1,ITMAX
            DO NC=NCSTART,NCHROM
               IF (IRANK(NC).LE.IT) THEN
                  DO NG=1,NGENE
                     ZZZ(NG,ICOUNT) = POP(NG,NC)
                  ENDDO
                  DO NO=1,NOBFUN
                     ZZZFIT(NO,ICOUNT) = FIT(NO,NC)
                  ENDDO
                  zzzrank(icount) = irank(nc)
                  if(debug)write(49,201)icount,irank(nc),nc
                  ICOUNT         = ICOUNT+1
                  IF (ICOUNT.GT.NCHROM) GO TO 1000
               ENDIF
            ENDDO
         ENDDO
 1000    CONTINUE
      ENDIF
 201  format('Selected Chromo(',i5,') with rank (',
     $                 i5,')      from Chromo(',i5,')')
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** TOURNAMENT SELECTION ***
C-----------------------------------------------------------------------
C          *** FIND THE MAXIMUM RMS VALUE OF THE NORMALIZED 
C              FITNESS VECTOR ***
C
      IF (ISELECT.EQ.2 .or. iselect.eq.4      
     $     .OR.((ISELECT.EQ.7.or.iselect.eq.8).AND.NACC.LT.is_bin)) THEN
         nstart = 1 

         if( iselect.eq.2)then
            DO NC=1,NCHROM
               FITRMS(NC) = 0.0
               DO NO=1,NOBFUN
                  FITRMS(NC) = FITRMS(NC)+(FIT(NO,NC)/FITMAX(NO))**2
               ENDDO
               FITRMS(NC) = SQRT(FITRMS(NC)/FLOAT(NOBFUN))
            ENDDO
            if(i_minmax.eq.1)then
               FITRMSMAX = fit_init
               DO NC=1,NCHROM
                  IF (FITRMSMAX.LT.FITRMS(NC)) THEN
                     FITRMSMAX = FITRMS(NC)
                     NCRMSMAX  = NC
                  ENDIF
               ENDDO
            elseif(i_minmax.eq.0)then
               FITRMSMIN = fit_init
               DO NC=1,NCHROM
                  IF (FITRMSMIN.GT.FITRMS(NC)) THEN
                     FITRMSMIN = FITRMS(NC)
                     NCRMSMIN  = NC
                  ENDIF
               ENDDO
            endif
C
C-----------------------------------------------------------------------
C          *** START THE SELECTION PROCESS BY INCLUDING THE 
C              NOBFUN CHROMOSOMES ASSOCIATED WITH THE MAXIMUM
C              FITNESS VALUES ***
C
            DO NC=1,NOBFUN
               icount = icount + 1
               if(i_minmax.eq.1)NC1 = NCFITMAX(NC)
               if(i_minmax.eq.0)NC1 = NCFITMIN(NC)
               DO NG=1,NGENE
                  ZZZ(NG,NC) = POP(NG,NC1)
               ENDDO
               DO NO=1,NOBFUN
                  ZZZFIT(NO,NC) = FIT(NO,NC1)
               ENDDO
               zzzrank(nc) = irank(nc1)
               if(debug)write(49,201)nc,irank(nc1),nc1
            ENDDO
C
C-----------------------------------------------------------------------
C          *** NEXT INCLUDE THE CHROMOSOME WITH THE BEST
C              RMS LENGTH ***
C
            if(i_minmax.eq.1)NC1 = NCRMSMAX
            if(i_minmax.eq.0)NC1 = NCRMSMIN
            DO NG=1,NGENE
               ZZZ(NG,NOBFUN+1) = POP(NG,NC1)
            ENDDO
            DO NO=1,NOBFUN
               ZZZFIT(NO,NOBFUN+1) = FIT(NO,NC1)
            ENDDO
            zzzrank(nobfun+1) = irank(nc1)
            if(debug)write(49,201)nobfun+1,irank(nc1),nc1
            NSTART = NOBFUN+2
         endif
C
C-----------------------------------------------------------------------
C          *** COMPLETE THE SELECTION PROCESS USING TOURNAMENT
C              SELECTION ***
C
         DO NC=NSTART,NCHROM
            CALL RAN3(N,RAND)
            IR1 =  INT(RAND*FLOAT(NCHROM))+1
            CALL RAN3(N,RAND)
            IR2 =  INT(RAND*FLOAT(NCHROM))+1
            CALL RAN3(N,RAND)
            IR3 =  INT(RAND*FLOAT(NCHROM))+1
            IF (IRANK(IR1).GT.IRANK(IR2)) THEN
               IRH = IR2
               IR2 = IR1
               IR1 = IRH
            ENDIF
            IF (IRANK(IR2).GT.IRANK(IR3)) THEN
               IRH = IR3
               IR3 = IR2
               IR2 = IRH
            ENDIF
            IF (IRANK(IR1).GT.IRANK(IR2)) THEN
               IRH = IR2
               IR2 = IR1
               IR1 = IRH
            ENDIF
            DO NG=1,NGENE
               ZZZ(NG,NC) = POP(NG,IR1)
            ENDDO
            DO NO=1,NOBFUN
               ZZZFIT(NO,NC) = FIT(NO,IR1)
            ENDDO
            zzzrank(nc) = irank(ir1)
            if(debug)write(49,201)nc,irank(ir1),ir1
         ENDDO
      ENDIF
C
      if(iselect.eq.5 .or. iselect.eq.6)then
c
         ICOUNT  = 0
         if(iselect.eq.6)then
            DO NC=1,NOBFUN
               if(i_minmax.eq.1)NC1 = NCFITMAX(NC)
               if(i_minmax.eq.0)NC1 = NCFITMIN(NC)
               DO NG=1,NGENE
                  ZZZ(NG,NC) = POP(NG,NC1)
               ENDDO
               DO NO=1,NOBFUN
                  ZZZFIT(NO,NC) = FIT(NO,NC1)
               ENDDO
               zzzrank(nc) = irank(nc1)
CCCC            NCSTART = NOBFUN+1
               if(debug)write(49,201)nc,irank(nc1),nc1
            ENDDO
            ICOUNT  = NOBFUN
         endif

         if(nacc+icount.ge.nchrom)then
C     random selection from the whole list
            do nc = 1,nchrom    ! used to avoid choosing same accum point
               i_chrom(nc) = 0
            enddo

            do nc = 1,nchrom
 1001          CALL RAN3(N,RAND)
               IR1 =  INT(RAND*FLOAT(NACC))+1
               do ncc = 1,nc
                  if(ir1.eq.i_chrom(ncc))go to 1001
               enddo
               icount = icount + 1
               if(icount.gt.nchrom)go to 1002
               i_chrom(nc) = ir1
               do ng = 1,ngene
                  zzz(ng,icount) = pop_acc(ng,ir1)
               enddo               
               do no = 1,nobfun
                  zzzfit(no,icount) = fit_acc(no,ir1)
               enddo               
               zzzrank(icount) = 1
               if(debug)write(49,202)icount,ir1
            enddo
         endif
 202     format('Selected Chromo(',i5,') from accum Chromo(',i5,')')

         if (nacc+icount .lt. nchrom)then
            NLim = min(nchrom,nacc)
            if(NLim.ne.0)then
c     Take all the accumulation file data
               do nc = 1,NLim
                  icount = icount+1
                  if(icount.gt.nchrom)go to 1002
                  do ng = 1,ngene
                     zzz(ng,icount) = pop_acc(ng,nc)
                  enddo               
                  do no = 1,nobfun
                     zzzfit(no,icount) = fit_acc(no,nc)
                  enddo               
                  zzzrank(icount) = 1
                  if(debug)write(49,202)icount,ir1
               enddo
            endif
c     Tournament selection for the rest
c$$$            DO NC=NLim+1,NCHROM
            DO NC=icount+1,NCHROM
               CALL RAN3(N,RAND)
               IR1 =  INT(RAND*FLOAT(NCHROM))+1
               CALL RAN3(N,RAND)
               IR2 =  INT(RAND*FLOAT(NCHROM))+1
               CALL RAN3(N,RAND)
               IR3 =  INT(RAND*FLOAT(NCHROM))+1
               IF (IRANK(IR1).GT.IRANK(IR2)) THEN
                  IRH = IR2
                  IR2 = IR1
                  IR1 = IRH
               ENDIF
               IF (IRANK(IR2).GT.IRANK(IR3)) THEN
                  IRH = IR3
                  IR3 = IR2
                  IR2 = IRH
               ENDIF
               IF (IRANK(IR1).GT.IRANK(IR2)) THEN
                  IRH = IR2
                  IR2 = IR1
                  IR1 = IRH
               ENDIF
               DO NG=1,NGENE
                  ZZZ(NG,NC) = POP(NG,IR1)
               ENDDO
               DO NO=1,NOBFUN
                  ZZZFIT(NO,NC) = FIT(NO,IR1)
               ENDDO
               zzzrank(nc) = 1
               if(debug)write(49,202)nc,ir1
            ENDDO
         endif
 1002    continue
      endif
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** BIN SELECTION (ISELECT=7) ***
C-----------------------------------------------------------------------
C          *** FIRST SORT THE ACCUMULATION FILE AS WELL AS
C              THE LIST OF GENES ASSOCIATED WITH THE 
C              ACCUMULATION FILE ***
C
      IF (ISELECT.EQ.7.AND.NACC.GE.is_bin) THEN
c$$$         CALL SORTsp(NOBFUN,NACC,FIT_ACC,NGENE,pop_acc)
c$$$   accum file should already be sorted (see rank)
C     
c$$$CCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
c$$$         if(debug)then
c$$$            WRITE (49,920) N,FITMIN(1),FITMIN(2)
c$$$ 920        FORMAT (/'IPOP,FITMIN12=',I5,2F24.17)
c$$$            DO NC=1,NCHROM
c$$$               WRITE (49,921) NC,IRANK(NC),FIT(1,NC),FIT(2,NC)
c$$$ 921           FORMAT ('NC,IRANK,FIT12=',2I6,2F24.17)
c$$$            ENDDO
c$$$            DO NC=1,NACC
c$$$               WRITE (49,922) NC,FIT_ACC(1,NC),FIT_ACC(2,NC)
c$$$ 922           FORMAT ('SELECT:NC,FIT_ACC12=',I6,2F24.17)
c$$$            ENDDO
c$$$         endif
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C          *** PERFORM THE SELECTION FROM THE ACCUMULATION FILE ***
C
c$$$         NBIN = 5
         CALL BIN(ndim,NBIN,NACC,NOBFUN,FIT_ACC,NGENE,pop_acc,
     1            NCHROM,ZZZ,ZZZFIT,zzzrank)
      ENDIF

C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** BIN SELECTION (ISELECT=8) ***
C-----------------------------------------------------------------------

      IF (ISELECT.EQ.8.AND.NACC.GE.is_bin) THEN
C     
c$$$CCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
c$$$         if(debug)then
c$$$            WRITE (49,920) N,FITMIN(1),FITMIN(2)
c$$$ 920        FORMAT (/'IPOP,FITMIN12=',I5,2F24.17)
c$$$            DO NC=1,NCHROM
c$$$               WRITE (49,921) NC,IRANK(NC),FIT(1,NC),FIT(2,NC)
c$$$ 921           FORMAT ('NC,IRANK,FIT12=',2I6,2F24.17)
c$$$            ENDDO
c$$$            DO NC=1,NACC
c$$$               WRITE (49,922) NC,FIT_ACC(1,NC),FIT_ACC(2,NC)
c$$$ 922           FORMAT ('SELECT:NC,FIT_ACC12=',I6,2F24.17)
c$$$            ENDDO
c$$$         endif
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C          *** PERFORM THE SELECTION FROM THE ACCUMULATION FILE ***
C
         CALL BIN2(ndim,NBIN,NACC,NOBFUN,FIT_ACC,NGENE,pop_acc,
     1            NCHROM,ZZZ,ZZZFIT,zzzrank,i_minmax,debug)
      ENDIF

C          *** SELECTION OPERATION IS COMPLETE ***
C-----------------------------------------------------------------------
C Load selection into pop and fit (note fitness valid here, but not ofter operations)
      do nc = 1,nchrom
         do ng = 1,ngene
            pop(ng,nc) = zzz(ng,nc)
         enddo
         do no = 1,nobfun
            fit(no,nc) = zzzfit(no,nc)
         enddo
         irank(nc) = zzzrank(nc)
      enddo
         
C-----------------------------------------------------------------------
      if(debug)then
         write(49,*)'after selection'
         write(49,*)' nc     irank     fit = '
         write(49,*)' nc          pop = '
         do nc = 1,nchrom
            write(49,'(i5,2x,i5,3(2x,e24.16))')
     $           nc,irank(nc),(fit(no,nc),no=1,nobfun)
            write(49,'(i5,3(2x,e24.16))')
     $           nc,(pop(ng,nc),ng=1,ngene)
         enddo
      endif
C-----------------------------------------------------------------------
C
      RETURN
      END

