c ##########################
c ##                      ##
c ##  subroutine filtx    ##
c ##                      ##
c ##########################
c
      subroutine filtx (jdim,kdim,q,s,xyj,coef2,coef4,sndsp,work,
     >                    precon,tmp,tmp2)            
c
c******************************************************************
c   this subroutine follows very faithfully t. pulliam notes, p.31
c
c   fourth order smoothing, added explicitly to rhs                     
c   second order near shocks with pressure grd coeff.                   
c                                                                       
c  xi direction                                                         
c                                                                       
c   start differences each variable separately                          
c******************************************************************
c                                                                       
      include '../include/arcom.inc'
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim),sndsp(jdim,kdim)          
      dimension work(jdim,kdim,4),precon(jdim,kdim,6)
      dimension tmp(jdim,kdim,4),tmp2(jdim,kdim,4)
c
      do 10 n = 1,4                                                     
c----    1st-order forward difference ----
         do 15 k =klow,kup                                                
         do 15 j =jlow,jup                                                
            tmp(j,k,n) = q(j+1,k,n)*xyj(j+1,k) - q(j,k,n)*xyj(j,k)           
 15      continue
c  --    c mesh bc --
         if (.not.periodic) then                                          
            j1 = jbegin                                                    
            j2 = jend                                                      
            do 16 k = klow,kup                                             
               tmp(j1,k,n) = q(j1+1,k,n)*xyj(j1+1,k) -                       
     >                        q(j1,k,n)*xyj(j1,k)                           
               tmp(j2,k,n) = tmp(j2-1,k,n)                                  
 16         continue                                                       
         endif                                                          
c        
c        
c----    apply cent-dif to 1st-order forward ----
         do 17 k =klow,kup                                                
         do 17 j =jlow,jup                                                
            tmp2(j,k,n) = tmp(j+1,k,n)-2.d0*tmp(j,k,n)+tmp(j-1,k,n)     
 17      continue                                                          
c  --    c mesh bc --
         if (.not.periodic) then                                          
            j1 = jbegin                                                    
            j2 = jend                                                      
            do 18 k =klow,kup                                             
               tmp2(j1,k,n) = q(j1+2,k,n)*xyj(j1+2,k) -                       
     >              2.d0*q(j1+1,k,n)*xyj(j1+1,k) + q(j1,k,n)*xyj(j1,k)         
               tmp2(j2,k,n) = 0.                                              
 18         continue
     $              
         endif                                                          
 10   continue                                                          
c
      call predis2(jdim,kdim,jbegin,jup,klow,kup,
     &                       0,0,q,xyj,sndsp,precon,tmp,work)
      call predis2(jdim,kdim,jbegin,jup,klow,kup,
     &                       1,0,q,xyj,sndsp,precon,tmp,work)
c
      do 91 n=1,4
      do 91 k=klow,kup
      do 91 j=jbegin,jup
         work(j,k,n)=coef2(j,k)*work(j,k,n)+coef2(j+1,k)*tmp(j,k,n)
 91   continue
c
      call predis2(jdim,kdim,jbegin,jup,klow,kup,
     &                       0,0,q,xyj,sndsp,precon,tmp2,tmp)
      call predis2(jdim,kdim,jbegin,jup,klow,kup,
     &                       1,0,q,xyj,sndsp,precon,tmp2,tmp)
c      
      do 92 n=1,4
      do 92 k=klow,kup
      do 92 j=jbegin,jup
         work(j,k,n)=work(j,k,n)-
     &             (coef4(j,k)*tmp(j,k,n)+coef4(j+1,k)*tmp2(j,k,n))
 92   continue
c
c
c
c---- last differenciation and add in dissipation ----
      dtd = dt / (1. + phidt)
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
         s(j,k,n)=s(j,k,n) + (work(j,k,n) - work(j-1,k,n))*dtd
 200  continue
c
      return                                                            
      end                                                               
