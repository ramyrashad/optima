      subroutine predis2(jdim,kdim,jl,ju,kl,ku,jskip,kskip,q,xyj,sndsp,
     &                                 precon,tmp,work)
      include '../include/arcom.inc'
cdu
cdu  this routine multiplies the vector tmp by the preconditioner
cdu  in conversative variables
c
      double precision q(jdim,kdim,4),xyj(jdim,kdim),sndsp(jdim,kdim)
      double precision precon(jdim,kdim,8),tmp(jdim,kdim,4)
      double precision work(jdim,kdim,4)
c
c     note: - gami=gamma - 1
c           - The loops are repeated to avoid having an if statement
c             inside the one loop.     
c
      if (jskip.eq.1 .or. kskip.eq.1) then
         do 100 k=kl,ku
            kk=k+kskip
            do 100 j=jl,ju
               jj=j+jskip
               rho=q(jj,kk,1)*xyj(jj,kk)
               rhoinv=1./rho
               u=q(jj,kk,2)/q(jj,kk,1)
               v=q(jj,kk,3)/q(jj,kk,1)
               c=sndsp(jj,kk)
c        
c              -multiply by M inverse
c        
               t4=gami*(precon(jj,kk,2)*tmp(j,k,1)-u*tmp(j,k,2)-
     +              v*tmp(j,k,3)+tmp(j,k,4))
               t1=t4*rhoinv/c
               t4=t4-tmp(j,k,1)*c**2
               t2=(tmp(j,k,2)-u*tmp(j,k,1))*rhoinv
               t3=(tmp(j,k,3)-v*tmp(j,k,1))*rhoinv
c        
c              -multiply by Gamma
c        
               t1=t1/precon(jj,kk,1)
c        
c              -multiply by M
c        
               tmp(j,k,1)=(rho*t1-t4/c)/c
               tmp(j,k,2)=u*tmp(j,k,1)+rho*t2
               tmp(j,k,3)=v*tmp(j,k,1)+rho*t3
               tmp(j,k,4)=rho*((precon(jj,kk,2)/c+c/gami)*t1+
     +              u*t2+v*t3)-precon(jj,kk,2)*t4/c**2
 100     continue
c
      else
c
         do 1000 k=kl,ku
            kk=k+kskip
            do 1000 j=jl,ju
               jj=j+jskip
               rho=q(jj,kk,1)*xyj(jj,kk)
               rhoinv=1./rho
               u=q(jj,kk,2)/q(jj,kk,1)
               v=q(jj,kk,3)/q(jj,kk,1)
               c=sndsp(jj,kk)
c        
c              -multiply by M inverse
c        
               t4=gami*(precon(jj,kk,2)*tmp(j,k,1)-u*tmp(j,k,2)-
     +              v*tmp(j,k,3)+tmp(j,k,4))
               t1=t4*rhoinv/c
               t4=t4-tmp(j,k,1)*c**2
               t2=(tmp(j,k,2)-u*tmp(j,k,1))*rhoinv
               t3=(tmp(j,k,3)-v*tmp(j,k,1))*rhoinv
c        
c              -multiply by Gamma
c        
               t1=t1/precon(jj,kk,1)
c        
c              -multiply by M
c        
               work(j,k,1)=(rho*t1-t4/c)/c
               work(j,k,2)=u*work(j,k,1)+rho*t2
               work(j,k,3)=v*work(j,k,1)+rho*t3
               work(j,k,4)=rho*((precon(jj,kk,2)/c+c/gami)*t1+
     +              u*t2+v*t3)-precon(jj,kk,2)*t4/c**2
 1000    continue
      endif
c        
      return
      end
