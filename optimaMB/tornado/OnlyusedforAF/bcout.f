      subroutine bcout(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
     &                  k1,k2,jbc,idir)              
c
#include "../include/common.inc"
c                                                                       
      dimension q(jdim,kdim,4)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
      dimension turre(jdim,kdim),vk(jdim,kdim),ve(jdim,kdim)
c
      gi = 1./gamma                                                 
      gm1i = 1.d0/gami                                                
c                                                                    
      j = jbc
      jq= jbc
c     The normal points in the direction of increasing xi location ...
c     So Vn is positive for outflow at j=jmax and negative at j=1.
      sgn  = -float(idir)
      jadd = idir
c
c     **********************************************
c     *          Viscous Outflow Boundaries        *
c     **********************************************
      if (iord.eq.2) then
        do 250 k = k1,k2                                        
          jq=j+jadd
          rrj = xyj(jq,k)/xyj(j,k)                                  
          q(j,k,1) = q(jq,k,1)*rrj                                
          q(j,k,2) = q(jq,k,2)*rrj                                
          q(j,k,3) = q(jq,k,3)*rrj                                
          ppp = gami*(q(jq,k,4) -                                  
     *          0.5d0*(q(jq,k,2)**2+q(jq,k,3)**2)/q(jq,k,1) )   
          q(j,k,4) = ppp*gm1i*rrj +                                 
     *          0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)        
          turre(j,k)=turre(jq,k)
          vk(j,k)=vk(jq,k)
          ve(j,k)=ve(jq,k)
 250    continue
      else
c       -iord=4
        do 255 k = k1,k2                                        
          jq=j+jadd
          xyj2 = xyj(jq,k)                                           
          rho2 = q(jq,k,1)*xyj2                                     
          rinv = 1.d0/q(jq,k,1)                                      
          u2 = q(jq,k,2)*rinv                                        
          v2 = q(jq,k,3)*rinv
          e2 = q(jq,k,4)*xyj2
          p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c     
          jq=j+2*jadd
          xyj3 = xyj(jq,k)                                           
          rho3 = q(jq,k,1)*xyj3
          rinv = 1.d0/q(jq,k,1)                                
          u3 = q(jq,k,2)*rinv                                        
          v3 = q(jq,k,3)*rinv                                       
          e3 = q(jq,k,4)*xyj3
          p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c     
c         jq=j+3*jadd
c         xyj4 = xyj(jq,k)                                          
c         rho4 = q(jq,k,1)*xyj4
c         rinv = 1.d0/q(jq,k,1)                                       
c         u4 = q(jq,k,2)*rinv                                        
c         v4 = q(jq,k,3)*rinv                                       
c         e4 = q(jq,k,4)*xyj4
c         p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c     
c         -zeroeth-order extrapolation
c          or setting d(var)/dn=0 to first-order
c          rhoext=rho2
c          uext=  u2
c          vext=  v2
c          pext=  p2
c         -first-order extraplation
c          rhoext=2.d0*rho2-rho3
c          uext=  2.d0*u2-u3    
c          vext=  2.d0*v2-v3    
c          pext=  2.d0*p2-p3
c         -second-order 
c          rhoext= 3.*(rho2 - rho3) + rho4
c          uext= 3.*(u2 - u3) + u4
c          vext= 3.*(v2 - v3) + v4
c          pext= 3.*(p2 - p3) + p4
c         
c         -set d(var)/dn =0 to second-order
          third=1.d0/3.d0
          rhoext=(4.d0*rho2-rho3)*third
          uext=  (4.d0*u2 - u3)*third
          vext=  (4.d0*v2 - v3)*third
          pext=  (4.d0*p2 - p3)*third
c         
c         -set d(var)/dn =0 to third-order
c          denom=1.d0/1.1d1
c          rhoext=(18.d0*rho2 - 9.d0*rho3 + 2.d0*rho4)*denom
c          uext=(18.d0*u2 - 9.d0*u3 + 2.d0*u4)*denom
c          vext=(18.d0*v2 - 9.d0*v3 + 2.d0*v4)*denom
c          pext=(18.d0*p2 - 9.d0*p3 + 2.d0*p4)*denom
c                     
          rrj=1.d0/xyj(j,k)
          q(j,k,1) = rhoext*rrj                                
          q(j,k,2) = uext*q(j,k,1)
          q(j,k,3) = vext*q(j,k,1)
          ppp = pext*rrj
          q(j,k,4) = ppp*gm1i +                                 
     *          0.5d0*(q(j,k,2)**2+q(j,k,3)**2)/q(j,k,1)        
c
          jq=j+jadd
          turre(j,k)=turre(jq,k)
          vk(j,k)=vk(jq,k)
          ve(j,k)=ve(jq,k)
 255    continue
      endif                                                      
c
      return                                                            
      end                                                               
