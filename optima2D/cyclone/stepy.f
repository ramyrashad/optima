      subroutine stepy(jdim,kdim,q,s,press,sndsp,turmu,fmu,ds,xy,xyj,   
     *                               ett,coef2,coef4,a,b,c,d,e,dj,ddj)  
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),turmu(jdim,kdim)        
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension ett(jdim,kdim),ds(jdim,kdim),sndsp(jdim,kdim)           
      dimension coef2(jdim,kdim),coef4(jdim,kdim),fmu(jdim,kdim)        
c                                                                       
      dimension a(jdim*kdim*4),b(jdim*kdim*16),c(jdim*kdim*16),         
     >          d(jdim*kdim*16),e(jdim*kdim*4)                          
      dimension dj(jdim*kdim*12),ddj(jdim*kdim*12)                      
c                                                                       
c     y sweep                                                     
      call filpny(jdim,kdim,q,press,sndsp,coef2,coef4,                  
     >            ett,xy,xyj,turmu,fmu,ds,a,b,c,d,e,dj,ddj,s) 
c
      if (ibc) then
         call bpenty(jdim,kdim,jlow,jup,kbegin,kend,a,b,c,d,e,s)
c         call bpenty(jdim,kdim,jbegin,jend,kbegin,kend,a,b,c,d,e,s)
      else
         call bpenty(jdim,kdim,jlow,jup,klow,kup,a,b,c,d,e,s)
      endif
c                                                                       
      return                                                            
      end                                                               
