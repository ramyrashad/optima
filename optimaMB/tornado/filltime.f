c-----------------------------------------------------------------------
c     -- The time terms are added --
c-----------------------------------------------------------------------
      subroutine filltime(jmax, kmax, nmax, ibc1, ibc2, ibc3, ibc4,
     &     kminbnd, kmaxbnd, jminbnd, jmaxbnd, j1, j2, k1,k2,indx,pa,as, 
     &     icol,jacmat,precmat)

      implicit none

#include "../include/common.inc"

      integer jmax, kmax, nmax, ibc1, ibc2, ibc3, ibc4, indx(jmax,kmax)
      integer kminbnd, kmaxbnd, jminbnd, jmaxbnd,j1,j2,k1,k2, icol(*)
      integer j,k,n,ii,jj,js,je
      double precision pa(*),as(*),diag
      logical jacmat,precmat

      IF(imarch.EQ.2)THEN

c$$$         if (sbbc .and. numiter-istart .eq. 1) then
c$$$c        Implicit Euler at first iteration for suction/blowing BC
c$$$
c$$$            diag = 1.0d0/dt2

         if (numiter .ne. iend+nkskip+1 .or. nkskip .eq.0 ) then
c        BDF2 otherwise
            diag = 3.0d0/(2.0*dt2)

         else
c        special BDF2 at time step size change
            diag=(2.d0*dtsmall+dtbig)/(dtsmall*(dtsmall+dtbig)) 

         end if 
        
      ELSE IF(imarch.EQ.4)THEN

         diag = 1.d0/(ajk(jstage,jstage)*dt2)

      ELSE

         WRITE(*,*)"for unsteady runs imarch has to be set to 2 or 4"
         WRITE(*,*)"2 for BDF2 or 4 for ESDIRK4"
         WRITE(*,*) imarch
         WRITE(*,*)"Exit at filltime.f"
         STOP

      ENDIF




c     -- preconditioner --
      if ( precmat ) then 
      
         do n =1,nmax
            do k = k1,k2
               do j = j1,j2
                  ii = ( indx(j,k) - 1 )*nmax + n
                  jj = ( ii - 1 )*icol(5)
                  pa(jj+icol(2)+n) = pa(jj+icol(2)+n) + diag
               end do                                         
            end do
         end do

      end if  !precmat


c     -- second order jacobian --
      if ( jacmat ) then

c     -- interior nodes (two nodes interior from b.c.) --

         if (ibc2.EQ.0) then
            js = jminbnd 
         else
            js = jminbnd + 2 
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd - 2
         end if

         do k = kminbnd+2,kmaxbnd-2                                         
            do j =js,je                                             
               do n =1,nmax
                  ii = ( indx(j,k) - 1 )*nmax + n
                  jj = ( ii - 1 )*icol(9)                
                  as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag
               end do
            end do
         end do

c     -- first interior boundary nodes --

         k = kminbnd+1
         do j = js,je
            do n =1,nmax
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)           
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag             
            end do
         end do
         
         k = kmaxbnd-1
         do j = js,je 
            do n =1,nmax
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)               
               as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag
            end do
         end do

        if (ibc2.NE.0) then

           j   = jminbnd+1

           do k = kminbnd+2,kmaxbnd-2          
              do n =1,nmax
                 ii = ( indx(j,k) - 1 )*nmax + n
                 jj = ( ii - 1 )*icol(9)
                 as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag
              end do
           end do

            k = kminbnd+1
            do n =1,nmax
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)            
               as(jj+icol(2)+n) = as(jj+icol(2)+n) + diag
            end do

            k = kmaxbnd-1 
            do n =1,nmax
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)              
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag
            end do

         end if
         
         if (ibc4.NE.0) then 

            j   = jmaxbnd-1 
                 
            do k = kminbnd+2,kmaxbnd-2
               do n =1,nmax
                  ii = ( indx(j,k) - 1 )*nmax + n
                  jj = ( ii - 1 )*icol(9)                
                  as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag
               end do
            end do 

            k = kminbnd+1
            do n =1,nmax
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)              
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag
            end do

            k = kmaxbnd-1
            do n =1,nmax
               ii = ( indx(j,k) - 1 )*nmax + n
               jj = ( ii - 1 )*icol(9)             
               as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag
            end do

         end if
         
      end if

      return
      end                       ! filltime
