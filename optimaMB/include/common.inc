c This has been modified so that all variables are explicitly declared,
c so the "implicit none" statement may be used in subroutines that
c include this file.  (Markus Rumpfkeil)

      double precision dswall,sobmax,tinf,wtrat,circb,chord	

      common/geom/
     &  periodic,   cmesh,       orderxy,     wake,
     &  dswall,     sobmax,
     &  sharp,      cusp,        tinf,        wtrat,
     &  circb,      chord,
     &  circul,     bcairf,      bcfarf,      neudich

        double precision fsmach, alpha,gamma, gami
 	double precision dt,dtmin,dtrate,pi,dtbig	
	double precision strtit,dis2x,dis2y,dis4x
	double precision dis4y,smu,smuim,phidt,dtsmall
	double precision thetadt,dtsp,xobs,yobs
	double precision omegaa,omegaf,omega,sbeta,sdelta
	logical sbbc

	integer jacdt,meth,ispec,nproc,nnit_sp
	integer jobs,kobs,blobs,Nobs,Nsp,Spvec,outFWH
c
      common/parm/
     r  fsmach,     alpha,       gamma,       gami,
     r  dt,         dtmin,       dtrate,      pi,
     r  strtit,     dis2x,       dis2y,       dis4x,
     r  dis4y,      smu,         smuim,       phidt,
     r  thetadt,    dtsp,        dtbig,       dtsmall,
     r  omegaa,     omegaf,      sbeta,       sdelta,
     r  omega,      xobs,        yobs,
     i  jacdt,      meth,        ispec,       nproc,
     i  nnit_sp,    jobs,        kobs,        blobs,
     i  Nobs,       Nsp,         Spvec,       outFWH,
     l  sbbc

	dimension Spvec(50,5)

        double precision scalit,fineits,conmin,conmax
	double precision coninc,resid,residmx,fsmtem
	double precision alptem,retem,targetalpha
	integer numiter,istart,nsuper,iend
	integer iread,iprint,maxdq(2),nouteverybig
	integer nq,ncp,npcont,nres,noutevery
	integer nblkstart,nblkend,iramp,maxres(3)

      common/swtc/
     r  scalit,      fineits,    conmin,      conmax,
     r  coninc,      resid,      residmx,     fsmtem,
     r  alptem,      retem,      targetalpha, 
     i  numiter,     istart,     nsuper,      iend,
     i  iread,       iprint,     store,       maxdq,
     i  nq,          ncp,        npcont,      nres,
     i  nblkstart,   nblkend,    iramp,       maxres,
     l  ludecomp,    wideinterface, zerostart,
     l  iscray,      restart,    writemets,
     l  writedisp,   writeresid, psiavg,      matcharc2d,
     l  autowrite,   noutevery,  nouteverybig

      logical sngvalte,timing,zeroturre,mg,gseq,w_cycle,jacarea
      logical lomet, viscoutflow, grdseq_rest, badjac, fturb
      logical outtime, killed

      double precision visceig,tresid,tresid2,tresmx,ftfac
      integer iord,integ,mxtur(3),ifrzw 

      common/newstuf/
     r  visceig,    tresid,     tresid2,     tresmx,     ftfac,
     i     iord,    integ,      mxtur,       ifrzw,
     l sngvalte,    lomet,      zeroturre,   mg,    killed,
     l     gseq,    w_cycle,    jacarea,     viscoutflow,
     l grdseq_rest, badjac,     fturb,       outtime
c
      real*4 time1,time2,time3,time4
      real*4 tt,tarray(2),totime1,totime2,time1tmp,time2tmp
      common/timecode/
     r   time1,time2,time3,time4,tt,tarray,totime1,totime2,
     r   time1tmp,time2tmp,
     l   timing

      double precision re,re_input,ivis,retinf
      integer  iturb,ibcout
c
      common/visc/
     r  viscous,    turbulnt,    re,          re_input,   ivis,
     r  retinf,     visxi,       viseta,      viscross,    
     i  iturb,      ductflow,    poutflow,    windtunnel, ibcout
c
      logical restart,   store,    turbulnt,   viscous,
     &        periodic,  circul,   sharp,      cusp,
     &        bcairf,    bcfarf,   neudich,
     &        iscray,    visxi,    viseta,
     &        viscross,  cmesh,    orderxy,    wake,
     &        writemets, writedisp,  writeresid,
     &        ductflow,  psiavg,   poutflow,   windtunnel,
     &        ludecomp,  wideinterface, zerostart, matcharc2d,
     &        autowrite

      double precision clt,cdt,cmt,cli,cdi,cmi
      double precision clv,cdv,cmv,clit,cdit,cmit
      double precision clvt,cdvt,cmvt

      common/forces/
     r   clt,       cdt,         cmt,
     r   cli,       cdi,         cmi,
     r   clv,       cdv,         cmv,
     r   clit,      cdit,        cmit,
     r   clvt,      cdvt,        cmvt
c

      double precision rhoinf,uinf,vinf,einf,pinf

      common/infinty/
     r   rhoinf,uinf,vinf,einf,pinf
c

      double precision clinput,relaxcl,cltol
      integer iclfreq,iclstrt,isw
      logical clalpha	

      common/clalp/
     &   clinput,   relaxcl,     cltol,       iclfreq,
     &   iclstrt,   isw,         clalpha
 
c
      integer idmodel
      double precision  prec,prxi,prphi

      common/precon/prxi,prphi,prec

      double precision vnxi,vlxi,vneta,vleta,fsres,afres

      common/matrix/
     r   vnxi,      vlxi,        vneta,       vleta,
     i   idmodel

      common/conver/fsres,afres

      character*40 output_file_prefix,grid_file_prefix
      character*40 filena,restart_file_prefix
      common/filen/
     c   output_file_prefix,grid_file_prefix,restart_file_prefix,filena
c
c ********************************************************************
c data for determination of dp for downstream wind tunnel pressure
c ********************************************************************

      double precision pout, npavg, npblk, npj, npk 

      common/poutfl/ pout, npavg, npblk, npj, npk
      dimension npblk(2), npj(2), npk(2)
c

      double precision t_step_kw,vk_inf,ve_inf,turnu_inf
      double precision eps_ke,delta_ke,k2_l
      integer itertotal

      logical restart_ke
      common/zmentor1/ t_step_kw,vk_inf,ve_inf,turnu_inf
      common/zmentor2/ eps_ke,delta_ke,k2_l,itertotal
      common/zmentor3/ restart_ke

      integer nkits, nklfil, nkpfrz, nkimg, nkitg
      double precision  pdcnk 

      common/nkparms/ pdcnk, nkits, nklfil, nkpfrz, nkimg, nkitg

c     -- mn: space for aoa or mach number polars --
      integer ipolar
      double precision polar(100)

      common/sweep/ polar,ipolar

cmt -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
      double precision dt2, dualres
      double precision ajk(6,6)
      integer imarch, jstage, nkiends, nksubits, nkskip,klineconst
      logical unsteady

      common/mo/
     r  dt2, dualres, ajk,
     i  imarch, jstage, nkiends, nksubits, nkskip,klineconst,
     l  unsteady
cmt -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
