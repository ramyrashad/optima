      subroutine get_params(ipop,npop,nchrom,ga_file_prefix)
      
      character*80 ga_file_prefix
      character*80 arg_ument,More

      i_arg = 1
      call getarg(i_arg,arg_ument)
      if(arg_ument.eq.'')then
         call GA_version(6)
         print *,'usage: ',
     $        ' Evolver_($VERSION) NGA NPOP NCHROM GA_FILE_PREFIX'
         print *,' or::  -h:help, -v:version, -i inputs '
         print *,'Enter NGA,NPOP,NCHROM '
         print *,'Where '
         print *,'     NGA = ( 0: to initialize, >0: for current gen ',
     $        '      NPOP: to get stats '
         print *,'     NPOP = Total # of generations '
         print *,'     NCHROM = # of chromosomes '
         READ (5,*) IPOP,NPOP,NCHROM

         ga_file_prefix = 'chromo'
         print *,'Enter Chromosome File Name Prefix'
         read(*,'(a)')ga_file_prefix
      elseif(arg_ument.eq.'-HELP' .or. arg_ument.eq.'-help' .or.
     $        arg_ument.eq.'HELP' .or. arg_ument.eq.'help' .or.
     $        arg_ument.eq.'-H' .or. arg_ument.eq.'-h' .or.
     $        arg_ument.eq.'H' .or. arg_ument.eq.'h')then
         call GA_version(6)
         print *,'usage: ',
     $        ' Evolver_($VERSION) NGA NPOP NCHROM GA_FILE_PREFIX'
         print *,' or::  -h:help, -v:version, -i inputs '
         print *,''
         print *,' Parameters:'
         print *,'   NGA = '
         print *,'           0: to initialize'
         print *,'          >0: for current gen '
         print *,'        NPOP: to get stats '
         print *,'   NPOP = Total # of generations '
         print *,'   NCHROM = # of chromosomes '
         print *,'  GA_FILE_PREFIX: chromo.i_chrom prefixname'
         stop
      elseif(arg_ument.eq.'-V' .or. arg_ument.eq.'-v')then
         call GA_version(6)
         stop
      elseif(arg_ument.eq.'-INPUTS' .or. arg_ument.eq.'-inputs' .or.
     $        arg_ument.eq.'INPUT' .or. arg_ument.eq.'input' .or.
     $        arg_ument.eq.'-I' .or. arg_ument.eq.'-i' .or.
     $        arg_ument.eq.'I' .or. arg_ument.eq.'i')then

         print *,'-----------------------------------------------------'
         print *,'NAMELIST GCONTRL Variables'
         print *,'-----------------------------------------------------'
         print *,''
         print *,'ngene:          number of gene in chromsome (D=10)'
         print *,'gene(min/max):  array size=ngene '
         print *,'                min/max for genes'
         print *,'geneprec:       T: use gene precision (D=F)'
         print *,'igeneprec:      precision for each gene (all D=0)'
         print *,'                array of size (ngene)  '
         print *,'                forces each gene to that precision'
         print *,'          e.g 1.23456789 igeneprec=3 => 1.23000000'
         print *,''
         print *,'nobfun:         number of objective functions (D=1)'
         print *,''
         print *,'i_mnx...INTEGER PARAMETER THAT CONTROLS WHETHER THE '
         print *,'   OPTIMIZATION IS MAXIMIZATION OR MINIMIZATION'
         print *,'   I_MNX=1...MAXIMIZATION'
         print *,'   I_MNX=0...MINIMIZATION (D)'
         print *,''
         print *,'arc2d_io: Switch for chromo formats'
         print *,'      True:  Old format,  no labels'
         print *,'      False: Label format See field defs below (D)'
         print *,''
         print *,'int_real:  fixes data to integers (all D=0)'
         print *,''
         print *,'field_(ngene,pop,nobfun,fit,log): (see input.f for D)'
         print *,'       ngene:  field label for ngene'
         print *,'       pop:    field label for pop (ngene of these)'
         print *,'       nobfun: field label for number of obj funs'
         print *,'       fit:    field label for fit (nobfun of these)'
         print *,'       log:    field for logical swtich T/F'
         print *,''
         print *,'Enter q to quit or / to continue'
         read *,More
         if(More.eq.'q' .or. More.eq.'Q' .or. More.eq.'quit'
     $        .or. More.eq.'QUIT')stop
         print *,'-----------------------------------------------------'
         print *,'iselect:        selection choice'
         print *,'        1...GREEDY SELECTION+ min/max (D)'
         print *,'        2...TOURNAMENT SELECTION + min/max'
         print *,'        3...GREEDY SELECTION '
         print *,'        4...TOURNAMENT SELECTION'
         print *,'        5...Selection from the accumulation file'
         print *,'        6...Selection from the accumulation file '
         print *,'            with endpoints from active file'
         print *,'        7...Simple bin selection ala Holst'
         print *,'        8...New bin selection ala Holst'
         print *,'Nbin    number of bins for iselect 7,8 (D=5)'
         print *,'is_bin threshold for nunber of elements in accum file'
         print *,'        before binning starts (D=20)'
         print *,'eps_domin: epsilon dominance, dominance based on',
     $        ' fit > or < fit*(1+eps_domin)  (D=0)'
         print *,''
         print *,'ICULL...INTEGER PARAMETER THAT CONTROLS THE CULLING '
         print *,'    ALGORITHM. THE CULLING ALGORITHM LOGIC DISCARDS '
         print *,'    ENTRIES IN THE ACCUMULATION FILE WHEN THE THE '
         print *,'    NUMBER OF ENTRIS IN THE ACCUMULATION FILE EXCEEDS'
         print *,'    ICULL. THIS DISCARDING LOGIC ATTEMPTS TO FORCE '
         print *,'    EQUAL SPACING ALONG THE PARETO FRONT AND ONLY'
         print *,'    MAKES SENSE WHEN NOBFUN>1 AND IACCUM=1.'
         print *,'    ICULL=0...CULLING LOGIC IS TURNED OFF. (D)'
         print *,'    ICULL=N...CULLING LOGIC IS EXECUTED FOR EVERY'
         print *,'              GENERATION IN WHICH THE NUMBER OF '
         print *,'              ELEMENTS IN THE ACCUMULATION FILE'
         print *,'              EXCEEDS N. THIS OPTION UTILIZES AN'
         print *,'              ARC-LENGTH ALGORITHM AND IS ONLY VALID'
         print *,'              WHEN NOBFUN=2.'
         print *,'    ICULL=-N..CULLING LOGIC IS EXECUTED FOR EVERY'
         print *,'              GENERATION IN WHICH THE NUMBER OF '
         print *,'              ELEMENTS IN THE ACCUMULATION FILE'
         print *,'              EXCEEDS N. THIS OPTION UTILIZES A'
         print *,'              BINNING ALGROITHM AND IS VALID FOR'
         print *,'              ALL VALUES OF NOBFUN (NOBFUN>1).'
         print *,''
         print *,'Enter q to quit or / to continue'
         read *,More
         if(More.eq.'q' .or. More.eq.'Q' .or. More.eq.'quit'
     $        .or. More.eq.'QUIT')stop
         print *,''
         print *,'IACCUM: 1 for accumulation archive process (D=0)'
         print *,'-----------------------------------------------------'
         print *,'XBEST....PERCENT OF THOSE INDIVIDUALS IN THE '
         print *,'         CURRENT POPULATION THAT ARE PASSED'
         print *,'         THROUGHT TO THE NEXT GENERATION WITHOUT'
         print *,'         CROSSOVER OF MUTATION (REAL NUMBER '
         print *,'         BOUNDED BY 0.0 AND 100.0) ***  (D=10)'
         print *,'XAVG.....PERCENT OF THOSE INDIVIDUALS IN THE '
         print *,'         NEW POPULATION THAT ARE DETERMINED '
         print *,'         USING THE RANDOM AVERAGE CROSSOVER '
         print *,'         OPERATOR (REAL NUMBER BOUNDED BY 0.0 '
         print *,'         AND 100.0) ***   (D=20)'
         print *,'boxover: T use extended average crosover (D=F)'
         print *,'prob_avg: prob of applying average to gene (D=1)'
         print *,'i_avg_cho: 0 for orig average, 1 for new (D=0)'
         print *,'  0: Avg Oper based on gene/gene chromo/chromo '
         print *,'  1: Avg Oper across chromo gen by gene with prob_avg'
         print *,''
         print *,'XPERT....PERCENT OF THOSE INDIVIDUALS IN THE '
         print *,'         NEW POPULATION THAT ARE DETERMINED '
         print *,'         USING THE PERTURBATION MUTATION '
         print *,'         OPERATOR (REAL NUMBER BOUNDED BY 0.0 '
         print *,'         AND 100.0) ***  (D=30)'
         print *,'prob_pert: prob of applying pert to gene (D=0.2)'
         print *,'beta:    perturbation size for genes (D=0.3)'
         print *,'beta_min,beta_max,n_beta_s,n_beta_i(D=0,beta,300,300)'
         print *,'         Beta varied from beta_max to beta_min'
         print *,'         after n_beta_i function evals linearly '
         print *,'         for next function evals'
         print *,''
         print *,'XMUT.....PERCENT OF THOSE INDIVIDUALS IN THE '
         print *,'         NEW POPULATION THAT ARE DETERMINED '
         print *,'         USING THE RANDOM MUTATION OPERATOR '
         print *,'         (REAL NUMBER BOUNDED BY 0.0 AND'
         print *,'         100.0) ***  (D=40)'
         print *,'prob_mut: prob of applying mut to gene (D=0.2)'
         print *,''
         print *,'Enter q to quit or / to continue'
         read *,More
         if(More.eq.'q' .or. More.eq.'Q' .or. More.eq.'quit'
     $        .or. More.eq.'QUIT')stop
         print *,'-----------------------------------------------------'
         print *,'CBEST = FALSE, NBEST CHROMOSOMES HAVE PARML(1) = F'
         print *,'            SO THAT THE FITNESS IS NOT REEVALUATED'
         print *,'            : I.E. PASSED THROUGH CHROMOSOMES  (D)'
         print *,'CBEST = TRUE, NBEST CHROMOSOMES HAVE PARML(1) = T'
         print *,'             SO THAT THE FITNESS IS REEVALUATED'
         print *,'             : EVEN FOR PASSED THROUGH CHROMOSOMES'
         print *,' '
         print *,'init_ga:'
         print *,'     0: Initialize chromosomes with min/max average',
     $        ' + RANDOM across genes ' 
         print *,'     1: Initialize chromosomes with min/max average',
     $        ' all random across genes (D)'
         print *,'     2: Initialize chromosomes latin hypecube',
     $        ' all random across genes '
         print *,'     <0 and abs(init_ga)>1 ',
     $        ': Initialize chromosomes with a uniform distribution',
     $        ' across foreach gene between genemin -- genemax'
         print *,'NOTE: init_ga<0 MUST HAVE nchrom = abs(init_ga)^ngene'
         print *,'-----------------------------------------------------'
         print *,'compute_pass_thu: '
         print *,'COMPUTE_PASS_THU=FALSE, CHROMOS HAVE PARML(1)=F'
         print *,'    SO THAT THE FITNESS IS NOT REEVALUATED if chromo ' 
         print *,'    does not change : I.E. PASSED THROUGH CHROMOSOMES'
         print *,'COMPUTE_PASS_THU = TRUE, FITNESS IS EVALUATED '
         print *,'                even if chromo does not change (D=T)'
         print *,''
         print *,'rand_init_fit:T randomize intial fitness values D)'
         print *,'              F intial fitness values to fit_min/max'
         print *,' '
         print *,''
         print *,'i_ran_seed: '
         print *,'  >= 0 seed random number with -ipop-1-i_ran_seed'
         print *,'  < 0  seed random number with i_ran_seed'
         print *,'  Note for init_ga=2 (latin hypercube initial pop'
         print *,'           i_ran_seed used for seed of latin_random'


         
         stop
      else
         call GA_version(6)
         read(arg_ument,*)ipop
         i_arg = i_arg + 1
         call getarg(i_arg,arg_ument)
         read(arg_ument,*)NPOP
         i_arg = i_arg + 1
         call getarg(i_arg,arg_ument)
         read(arg_ument,*)NCHROM
         i_arg = i_arg + 1
         call getarg(i_arg,arg_ument)
         Ga_file_prefix = arg_ument

      endif

      return
      end
