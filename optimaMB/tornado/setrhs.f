c-----------------------------------------------------------------------
c     -- setup rhs for gmres solve --
c     -- m. nemec, sept. 2001 --
c     -- called from nksolve.f and adjoint.f --
c-----------------------------------------------------------------------
      subroutine setrhs(jmax, kmax, nmax, ks, ke, js, je, indx, s, rsa,
     &     rhs, flag)

      implicit none
      integer jmax, kmax, nmax, ks, ke, js, je, indx(jmax,kmax),j,k,n,jk
      double precision s(jmax,kmax,4), rsa(jmax,kmax), rhs(*)
      logical   flag

      do n = 1,4
         do k = ks,ke
            do j = js,je
               jk = (indx(j,k) - 1)*nmax + n
               rhs(jk) = s(j,k,n)
            end do
         end do
      end do 

      if (nmax.eq.5 .and. flag) then
         do k = ks,ke
            do j = js,je
               jk = (indx(j,k) - 1)*5 + 5
               rhs(jk) = rsa(j,k)
            end do
         end do
      end if

      return 
      end                       !setrhs
