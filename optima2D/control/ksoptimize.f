c----------------------------------------------------------------------
c     -- ksopt optimization control --
c     -- main calls to ksopt, evaluation of the objective and 
c        gradient --
c
c     written by: marian nemec
c     date: march 2000
c----------------------------------------------------------------------
      subroutine KSoptimize (jdim, kdim, ndim, ifirst, indx, icol, iex,
     &      q, cp, xy, xyj, x, y, fmu, vort, turmu, cp_tar,dvs, idv,
     &      bap, bcp, bt, bknot, work1, ia, ja, as, ipa, jpa, pa, iat,
     &      jat, ast, ipt, jpt, pat, cp_his, ac_his, opwk, xOrig, yOrig,
     &      dx, dy, lambda)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     -- solver arrays --
      integer jdim, kdim, ndim, ifirst, iex(jdim,kdim,4)

      double precision q(jdim,kdim,ndim), cp(jdim,kdim), y(jdim,kdim)
      double precision xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim)
      double precision xOrig(jdim,kdim), yOrig(jdim,kdim)
      double precision dx(jdim*kdim*incr), dy(jdim*kdim*incr)
      double precision lambda(jdim*kdim*incr), obj1, lcon
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)

c     -- optimization and b-spline arrays --
      integer idv(nc+mpopt), indx(jdim,kdim), icol(9)
      double precision cp_tar(jbody,2), bap(jbody,2), bcp(nc,2)
      double precision bt(jbody), bknot(jbsord+nc), dvs(nc+mpopt)
      double precision cp_his(jbody,200), ac_his(jbody,2,200)

c     -- sparse adjoint arrays --
clb
      integer ia(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer iat(jdim*kdim*ndim+2)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)
clb

c     -- ksopt arrays --
      double precision kswork(650)
      double precision obj(1), g(1), dvlb(ibsnc), dvub(ibsnc)
      double precision scale(ibsnc), df(1,ibsnc), dg(1,ibsnc)
      
      integer i, ncon, nobj, nside, nscale, iprnt, igrad
      integer isdrst, nomax, ngmax, ireq, isel
      double precision rdfun, adfun, fdelt, fdmin, rhomin
      double precision rhomax, rhodel

      logical cont

c     -- work arrays --
      double precision work1(maxjk,100), opwk(jdim*kdim,8)

      data ncon, nobj, nside / 0, 1, 0 /
      data rhomin, rhomax, rhodel / 3*0.0 /
      nscale = 1                ! resets when Hessian resets
      rdfun = opt_tol           ! termination crit.
      adfun = 0.0               ! termination crit.
      fdmin = fd_eta            ! absolute f. d. step size
      iprnt = 214               ! output info
      igrad = 1                 ! grad. calc
      isdrst = 0                ! search dir. restart
      ireq = 650                ! size of kswork array

c     -- upper and lower bound arrays --
c      do i = 1,ngdv
c        if (dvs(i).ge.0.d0) then
c          dvlb(i) = dvs(i)/1.d1
c          dvub(i) = dvs(i)*1.d1
c        else
c          dvlb(i) = dvs(i)*1.d1
c          dvub(i) = dvs(i)/1.d1
c        end if
c      end do

c     -- copy airfoil points -- 
      i = 1
      do j = jtail1,jtail2
        bap(i,1) = x(j,1)
        bap(i,2) = y(j,1)
        i = i + 1
      end do

c     -- initialize ksopt --
      call ksinit ( dvs, dvlb, dvub, scale, kswork, ndv, ncon, nobj,
     &      nside, nscale, iprnt, opt_iter, igrad, isdrst, rdfun, adfun,
     &      fd_eta, fdmin, rhomin, rhomax, rhodel, ksopt_unit, ireq)

      nomax = 1
      ngmax = 1

      cont = .true.
      ico = 0                   ! objective function counter
      icg = 0                   ! gradient counter
      obj(1) = 0.0
      gradmag = 0.0

      do while (cont)

        call ksopt ( isel, dvs, obj, g, df, dg, nomax, ngmax, kswork )
        
        if (isel.eq.0) then
          cont = .false.

        else if (isel.eq.1) then
c     -- compute objective function --

c     -- adjust grid --
          call regrid( -1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot, xOrig, yOrig, dx, dy)
c     -- adjust angle of attack --
          if (dvalfa(1)) alpha = dvs(ndv)

          frozen = .false.      !flag used for freezing dissipation

          call CalcObj (obj(1), jdim, kdim, ndim, ifirst, indx, icol, 
     &          iex, q, cp, xy, xyj, x, y, cp_tar, fmu, vort, turmu, 
     &          work1, ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, 
     &         pa, pat, obj1, lcon)
          ifirst = 0            !warm starts for cyclone runs if not 1
          ico = ico + 1

c     -- check T.E. slope --
          theta1 = atan2d( y(jtail2,1)-y(jtail2-1,1), x(jtail2,1) -
     &          x(jtail2-1,1) )

          theta2 = atan2d( y(jtail1,1)-y(jtail1+1,1), x(jtail1,1) -
     &          x(jtail1+1,1))
c          slope = tand( .5d0*(theta1+theta2) )  
          slope = 0.5d0*(theta1+theta2)

c     -- write current airfoil geometry and control points to file --
          if (acout) rewind (ac_unit)
          if (bcout) rewind (bc_unit)
          do j = jtail1,jtail2
            if (acout) write (ac_unit,100) x(j,1),y(j,1)
          end do
          do i = 1,nc
          if (bcout) write (bc_unit,100) bcp(i,1), bcp(i,2)
          end do
 100      format (3e24.16)
          if (acout) call flush (ac_unit)

          pobj = obj(1)
          if ( obj_func.eq.5 ) pobj = 1.d0/pobj
          if ( obj_func.eq.4 ) pobj = - pobj

c     -- write current design variables and objective function --
          write (ohis_unit,200) ico, icg, pobj, clt, cdt, alpha, resid
          write (*,210) ico, icg, obj(1), slope
          write (dvhis_unit,220) ico, (dvs(j),j=1,ndv)
 200      format (2i4, 4e16.8, e12.3)
 210      format (/3x,'ICO:',i4,1x,'ICG:',i4,1x,'OBJECTIVE:',e12.4,/3x
     &          ,'T.E. SLOPE:',f6.2)
 220      format (i4, 30e15.6)

        else if (isel.eq.2) then
c     -- compute obj. func. gradient --

          if (coef_frz) frozen = .true.

c     -- store pressure distribution and airfoil --
          if (icg .lt. 199) then
            i = 1
            do j = jtail1, jtail2
              cp_his(i,icg+1) = cp(j,1)
              ac_his(i,1,icg+1) = x(j,1)
              ac_his(i,2,icg+1) = y(j,1)
              i = i + 1
            end do
          end if

          call CalcGrad (obj(1), df, jdim, kdim, ndim, ifirst, q, cp,
     &          xy, xyj, x, y, xOrig, yOrig, dx, dy, lambda, fmu, vort,
     &          turmu, cp_tar, bap, bcp, bt,
     &          bknot, dvs, idv, indx, icol, iex, work1, ia, ja, ipa,
     &          jpa, iat, jat, ipt, jpt, as, ast, pa, pat, opwk)

c     -- calculate magnitude of gradient --
          gradmag = 0.d0
          do i=1,ndv
            gradmag = gradmag + df(1,i)*df(1,i)
          end do
          write (ghis_unit,300) ico, icg, dsqrt(gradmag), pobj,
     &          clt, cdt, alpha
          write (gvhis_unit,310) icg, (df(1,j),j=1,ndv)
 300      format(2i4, 5e16.8)
 310      format(i4,30e15.6)

          icg = icg + 1
c     -- terminate if only looking for gradient --
          if ( opt_iter.eq.1 .and. ico.eq.1 ) cont = .false.

        else
          write(opt_unit,*) ' -- Check isel -- '
        end if

      end do

c     -- write cp and geometry sequence to file --
c     -- store final pressure distribution and airfoil --
      icg = icg + 1
      if (icg .lt. 199) then
        i = 1
        do j = jtail1, jtail2
          cp_his(i,icg) = cp(j,1)
          ac_his(i,1,icg) = x(j,1)
          ac_his(i,2,icg) = y(j,1)
          i = i + 1
        end do
      end if

      if (cpout) rewind (cp_unit)
      do i = 1,jbody
        if (cpout) write(cp_unit,400)ac_his(i,1,1),(cp_his(i,j),j=1,icg)
      end do
         
      if (acout) rewind (ac_unit)
      do i = 1,jbody
        if (acout) write (ac_unit,400) ac_his(i,1,1), (ac_his(i,2,j),
     &     j=1,icg) 
      end do

 400  format (100e16.6)

      return
      end                       !ksoptimize
