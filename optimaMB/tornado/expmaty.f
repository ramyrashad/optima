      subroutine expmaty(jdim,kdim,q,coef2y,coef4y,sndsp,s,
     &     spect,xyj,press,ccy,vv,xy,temp,temp2,work,s1,s2,
     &     u,v,phi,h,jbegin,jend,kbegin,kend)

      implicit none

#include "../include/common.inc"
#include "../include/parms.inc"
c
      integer jdim,kdim,jbegin,jend,kbegin,kend,g,t,j,k,n,klow,kup
      integer kp1,kp,km,km1,kp2,kl,ku,tm1
      double precision q(jdim,kdim,4),coef2y(jdim,kdim)
      double precision sndsp(jdim,kdim),s(jdim,kdim,4),press(jdim,kdim)
      double precision spect(jdim,kdim,3),xyj(jdim,kdim)
      double precision ccy(jdim,kdim),vv(jdim,kdim),xy(jdim,kdim,4)
      double precision temp(jdim,kdim,4),temp2(jdim,kdim,4),v(jdim,kdim)
      double precision work(4,jdim,kdim,2),coef4y(jdim,kdim)
      double precision s1(jdim,kdim),s2(jdim,kdim),u(jdim,kdim)
      double precision phi(jdim,kdim),h(jdim,kdim)
      double precision dtd,rhoinv,a1,a2,cs,dot1,dot2,fact1,fact2
c
      klow=kbegin+1
      kup=kend-1
c
      dtd=dt/(1.d0 +phidt)
      do 11 n=1,4
        do 14 k=kbegin,kup
          kp1=k+1
          do 14 j=jbegin,jend
            work(n,j,k,1)=q(j,kp1,n)*xyj(j,kp1) - q(j,k,n)*xyj(j,k)
 14     continue
      
        do 15 j=jbegin,jend
          work(n,j,kend,1)=work(n,j,kup,1)
 15     continue
        
        do 18 k=klow,kup-1
          kp=k+1
          km=k-1
c         (q(j+2)-3q(j+1)+3q(j)-q(j-1))
          do 18 j=jbegin,jend
            work(n,j,k,2)=work(n,j,km,1)-2.*work(n,j,k,1)+work(n,j,kp,1)
 18     continue
c       
c        -boundary conditions a la Kyle
        k=kbegin
        do 20 j=jbegin,jend
          kp1=k+1
          kp2=k+2
          work(n,j,k,2)= q(j,k,n)*xyj(j,k)
     &          - 2.*q(j,kp1,n)*xyj(j,kp1) + q(j,kp2,n)*xyj(j,kp2)
          work(n,j,kup,2)=0.0
 20     continue
 11   continue
c
c
c
      do 100 k=kbegin,kend
      do 100 j=jbegin,jend
        rhoinv=1.d0/q(j,k,1)
        s1(j,k)=.5d0*(spect(j,k,2)+spect(j,k,3)-2.*spect(j,k,1))
        s2(j,k)=(spect(j,k,2)-spect(j,k,3))/(2.*ccy(j,k))
        u(j,k)=q(j,k,2)*rhoinv
        v(j,k)=q(j,k,3)*rhoinv
        phi(j,k)=.5d0*(u(j,k)**2+v(j,k)**2)         
        h(j,k)=(q(j,k,4)+press(j,k))*rhoinv
 100  continue
c
      if (iord.eq.4) then
        kl=klow+1
        ku=kup-1
      else
        kl=klow
        ku=kup
      endif
c
      if (dis2y.eq.0.) then
        do 103 n=1,4
        do 103 k=kbegin,kup
        do 103 j=jbegin,jend
          temp(j,k,n)=0.
 103    continue
      endif
c
      do 888 g=0,1
        if (dis2y.eq.0.) then
c         -by-pass second-difference computation
          do 105 k=kbegin,kup
            t=k+g
            do 104 j=jbegin,jend
               a1=xy(j,t,3)
               a2=xy(j,t,4)
               cs=sndsp(j,t)**2
c         
c     -for fourth difference
               dot1=phi(j,t)*work(1,j,k,2)
     &              -u(j,t)*work(2,j,k,2)
     &              -v(j,t)*work(3,j,k,2)
     &              + work(4,j,k,2)
               dot2=-vv(j,t)*work(1,j,k,2)
     &              + a1*work(2,j,k,2)
     &              + a2*work(3,j,k,2)
c     
               fact1=(s1(j,t)*dot1*(gami/cs)+s2(j,t)*dot2)
               fact2=((s1(j,t)*dot2)/(a1**2+a2**2)+s2(j,t)*gami*dot1)
c     
               temp2(j,k,1)=spect(j,t,1)*work(1,j,k,2)
     &              +fact1                     
               temp2(j,k,2)=spect(j,t,1)*work(2,j,k,2)
     &              +fact1*u(j,t)                 
     &              +fact2*a1                  
               temp2(j,k,3)=spect(j,t,1)*work(3,j,k,2)
     &              +fact1*v(j,t)                 
     &              +fact2*a2                  
               temp2(j,k,4)=spect(j,t,1)*work(4,j,k,2)
     &              +fact1*h(j,t)
     &              +fact2*vv(j,t)
 104        continue
 105      continue
        else
          do 112 k=kbegin,kup
            t=k+g
            do 111 j=jbegin,jend
c#include       "matloopy.inc"
               a1=xy(j,t,3)
               a2=xy(j,t,4)
               cs=sndsp(j,t)**2
c     
c     -for second difference
               dot1=phi(j,t)*work(1,j,k,1)
     &              -u(j,t)*work(2,j,k,1)
     &              -v(j,t)*work(3,j,k,1)
     &              + work(4,j,k,1)
               dot2=-vv(j,t)*work(1,j,k,1)
     &              + a1*work(2,j,k,1)
     &              + a2*work(3,j,k,1)

               fact1=(s1(j,t)*dot1*(gami/cs)+s2(j,t)*dot2)
               fact2=((s1(j,t)*dot2)/(a1**2+a2**2)+s2(j,t)*gami*dot1)
               
               temp(j,k,1)=spect(j,t,1)*work(1,j,k,1)
     &              +fact1                  
               temp(j,k,2)=spect(j,t,1)*work(2,j,k,1)
     &              +fact1*u(j,t)                
     &              +fact2*a1               
               temp(j,k,3)=spect(j,t,1)*work(3,j,k,1)
     &              +fact1*v(j,t)                
     &              +fact2*a2               
               temp(j,k,4)=spect(j,t,1)*work(4,j,k,1)
     &              +fact1*h(j,t)
     &              +fact2*vv(j,t)
c     
c               -for fourth difference
               dot1=phi(j,t)*work(1,j,k,2)
     &              -u(j,t)*work(2,j,k,2)
     &              -v(j,t)*work(3,j,k,2)
     &              + work(4,j,k,2)
               dot2=-vv(j,t)*work(1,j,k,2)
     &              + a1*work(2,j,k,2)
     &              + a2*work(3,j,k,2)
c     
               fact1=(s1(j,t)*dot1*(gami/cs)+s2(j,t)*dot2)
               fact2=((s1(j,t)*dot2)/(a1**2+a2**2)+s2(j,t)*gami*dot1)
c     
               temp2(j,k,1)=spect(j,t,1)*work(1,j,k,2)
     &              +fact1                     
               temp2(j,k,2)=spect(j,t,1)*work(2,j,k,2)
     &              +fact1*u(j,t)                 
     &              +fact2*a1                  
               temp2(j,k,3)=spect(j,t,1)*work(3,j,k,2)
     &              +fact1*v(j,t)                 
     &              +fact2*a2                  
               temp2(j,k,4)=spect(j,t,1)*work(4,j,k,2)
     &              +fact1*h(j,t)
     &              +fact2*vv(j,t)
 111        continue
 112      continue
        endif
c
c
c
        do 222 n=1,4
          do 141 k=kl,ku
            t=k+g
            tm1=t-1
            km1=k-1
            do 140 j=jbegin,jend
              s(j,k,n)=s(j,k,n) + dtd*( 
     &           coef2y(j,t)* temp(j,k,n)-coef2y(j,tm1)* temp(j,km1,n)
     &        - (coef4y(j,t)*temp2(j,k,n)-coef4y(j,tm1)*temp2(j,km1,n)))
 140        continue
 141      continue
 222    continue
 888  continue
c     
      if (iord.eq.4) then
c       Boundary Conditions
        do 2000 n=1,4
          do 2100 j=jbegin,jend
            k=klow
            work(n,j,kbegin,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j,k+1,n)*
     &                          xyj(j,k+1)-q(j,k+2,n)*xyj(j,k+2)
            k=kbegin
            work(n,j,klow,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j,k+1,n)*
     &                        xyj(j,k+1)-q(j,k+2,n)*xyj(j,k+2)
            k=kup-1
            work(n,j,k,2)=-q(j,k-1,n)*xyj(j,k-1)+2.d0*q(j,k,n)*xyj(j,k)
     &                     -q(j,k+1,n)*xyj(j,k+1)
            k=kup
            work(n,j,k,2)=-q(j,k-1,n)*xyj(j,k-1)+2.d0*q(j,k,n)*xyj(j,k)
     &                     -q(j,k+1,n)*xyj(j,k+1)
 2100     continue
 2000   continue
c
        do 2500 g=0,1
          do 2400 j=jbegin,jend
            do 2200 k=kbegin,klow
              t=k+g
c#include       "matloopy.inc"
              a1=xy(j,t,3)
              a2=xy(j,t,4)
              cs=sndsp(j,t)**2
c     
c     -for second difference
              dot1=phi(j,t)*work(1,j,k,1)
     &             -u(j,t)*work(2,j,k,1)
     &             -v(j,t)*work(3,j,k,1)
     &             + work(4,j,k,1)
              dot2=-vv(j,t)*work(1,j,k,1)
     &             + a1*work(2,j,k,1)
     &             + a2*work(3,j,k,1)
              
              fact1=(s1(j,t)*dot1*(gami/cs)+s2(j,t)*dot2)
              fact2=((s1(j,t)*dot2)/(a1**2+a2**2)+s2(j,t)*gami*dot1)
              
              temp(j,k,1)=spect(j,t,1)*work(1,j,k,1)
     &             +fact1                  
              temp(j,k,2)=spect(j,t,1)*work(2,j,k,1)
     &             +fact1*u(j,t)                
     &             +fact2*a1               
              temp(j,k,3)=spect(j,t,1)*work(3,j,k,1)
     &             +fact1*v(j,t)                
     &             +fact2*a2               
              temp(j,k,4)=spect(j,t,1)*work(4,j,k,1)
     &             +fact1*h(j,t)
     &             +fact2*vv(j,t)
c     
c               -for fourth difference
              dot1=phi(j,t)*work(1,j,k,2)
     &             -u(j,t)*work(2,j,k,2)
     &             -v(j,t)*work(3,j,k,2)
     &             + work(4,j,k,2)
              dot2=-vv(j,t)*work(1,j,k,2)
     &             + a1*work(2,j,k,2)
     &             + a2*work(3,j,k,2)
c     
              fact1=(s1(j,t)*dot1*(gami/cs)+s2(j,t)*dot2)
              fact2=((s1(j,t)*dot2)/(a1**2+a2**2)+s2(j,t)*gami*dot1)
c     
              temp2(j,k,1)=spect(j,t,1)*work(1,j,k,2)
     &             +fact1                     
              temp2(j,k,2)=spect(j,t,1)*work(2,j,k,2)
     &             +fact1*u(j,t)                 
     &             +fact2*a1                  
              temp2(j,k,3)=spect(j,t,1)*work(3,j,k,2)
     &             +fact1*v(j,t)                 
     &             +fact2*a2                  
               temp2(j,k,4)=spect(j,t,1)*work(4,j,k,2)
     &             +fact1*h(j,t)
     &             +fact2*vv(j,t)
 2200       continue
            do 2225 k=kup-1,kup
              t=k+g
c#include       "matloopy.inc"
              a1=xy(j,t,3)
              a2=xy(j,t,4)
              cs=sndsp(j,t)**2
c     
c     -for second difference
              dot1=phi(j,t)*work(1,j,k,1)
     &             -u(j,t)*work(2,j,k,1)
     &             -v(j,t)*work(3,j,k,1)
     &             + work(4,j,k,1)
              dot2=-vv(j,t)*work(1,j,k,1)
     &             + a1*work(2,j,k,1)
     &             + a2*work(3,j,k,1)
              
              fact1=(s1(j,t)*dot1*(gami/cs)+s2(j,t)*dot2)
              fact2=((s1(j,t)*dot2)/(a1**2+a2**2)+s2(j,t)*gami*dot1)
               
              temp(j,k,1)=spect(j,t,1)*work(1,j,k,1)
     &             +fact1                  
              temp(j,k,2)=spect(j,t,1)*work(2,j,k,1)
     &             +fact1*u(j,t)                
     &             +fact2*a1               
              temp(j,k,3)=spect(j,t,1)*work(3,j,k,1)
     &             +fact1*v(j,t)                
     &             +fact2*a2               
              temp(j,k,4)=spect(j,t,1)*work(4,j,k,1)
     &             +fact1*h(j,t)
     &             +fact2*vv(j,t)
c     
c               -for fourth difference
              dot1=phi(j,t)*work(1,j,k,2)
     &             -u(j,t)*work(2,j,k,2)
     &             -v(j,t)*work(3,j,k,2)
     &             + work(4,j,k,2)
              dot2=-vv(j,t)*work(1,j,k,2)
     &             + a1*work(2,j,k,2)
     &             + a2*work(3,j,k,2)
c     
              fact1=(s1(j,t)*dot1*(gami/cs)+s2(j,t)*dot2)
              fact2=((s1(j,t)*dot2)/(a1**2+a2**2)+s2(j,t)*gami*dot1)
c     
              temp2(j,k,1)=spect(j,t,1)*work(1,j,k,2)
     &             +fact1                     
              temp2(j,k,2)=spect(j,t,1)*work(2,j,k,2)
     &             +fact1*u(j,t)                 
     &             +fact2*a1                  
              temp2(j,k,3)=spect(j,t,1)*work(3,j,k,2)
     &             +fact1*v(j,t)                 
     &             +fact2*a2                  
              temp2(j,k,4)=spect(j,t,1)*work(4,j,k,2)
     &             +fact1*h(j,t)
     &             +fact2*vv(j,t)
 2225       continue
c        
            do 2300 n=1,4
            do 2300 k=klow,kup,kup-klow
              t=k+g
              tm1=t-1
              km1=k-1
              s(j,k,n)=s(j,k,n) + dtd*( 
     &           coef2y(j,t)* temp(j,k,n)-coef2y(j,tm1)* temp(j,km1,n)
     &        - (coef4y(j,t)*temp2(j,k,n)-coef4y(j,tm1)*temp2(j,km1,n)))
 2300       continue
 2400     continue
 2500   continue
      endif
      return
      end






