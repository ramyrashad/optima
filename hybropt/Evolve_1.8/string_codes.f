      SUBROUTINE INTCHA( IVAL,STRING,LSTRIN )
C
C   Convert an integer into a character string with no leading blanks.
C
      CHARACTER*(*) STRING
C
      CHARACTER*20 TEMP,OVERFL
      DATA OVERFL/'********************'/
C
C
C   Write into a temporary space first.
C
      WRITE(TEMP,1) IVAL
    1 FORMAT(I20)
C
C   Trim off leading and trailing blanks, and copy the result into STRING.
C
      CALL LTRIM ( TEMP,LTEMP )
      LSTRIN = LEN(STRING)
      IF (LTEMP.LE.LSTRIN) THEN
         STRING = TEMP
      ELSE
         STRING = OVERFL
      ENDIF
      LSTRIN = MIN(LTEMP,LSTRIN)
C
C
      RETURN
      END

      SUBROUTINE LTRIM(STRING,LSTRIN)
C
C   Remove leading blanks, nulls, or tabs from STRING, then return the length 
C   of STRING after trailing blanks and nulls have been removed.
C
      CHARACTER*(*) STRING
C
      CHARACTER NULL,TAB
      PARAMETER ( I1=1 )
C
C
C   Initialize null and tab characters.
C
      NULL   = CHAR(0)
      TAB    = CHAR(9)
C
      LSTRIN = LEN(STRING)
      IS     = 1
      DO 10 I = 1,LSTRIN
         IF (STRING(I:I).NE.' ' .AND. STRING(I:I).NE.NULL
     &                          .AND. STRING(I:I).NE.TAB) THEN
            IS     = I
            GOTO 20
         ENDIF
   10    CONTINUE
C
   20 CONTINUE
      CALL CSCOPY( STRING,IS,LSTRIN-IS+1,I1,LSTRIN )
      CALL TTRIM ( STRING,LSTRIN )
C
C
      RETURN
      END

      SUBROUTINE CSCOPY( STRING,IFROM,LFROM,ITO,LTO )
C
C   Copy part of a character string to another part of the same string.
C   Use FORTRAN 77 rules for blank padding or string truncation based on the
C   lengths LFROM and LTO.  This routine should work just as one would hope
C   the FORTRAN statement "STRING(ITO:ITO+LTO-1)=STRING(IFROM:IFROM+LFROM-1)"
C   would work.
C
      CHARACTER*(*) STRING
C
      PARAMETER ( I0=0 )
C
C   Check string lengths.
C
      LSTRIN = LEN(STRING)
      LFROMC = MAX(MIN(LFROM,LSTRIN-IFROM+1),I0)
      LTOC   = MAX(MIN(LTO,LSTRIN-ITO+1),I0)
C
C   Truncate if necessary.
C
      IS     = 1
      IE     = MIN(LFROMC,LTOC)
      II     = 1
C
C   If the "to" string starts after the "from" string, copy characters in
C   reverse order.
C
      IF (ITO.GT.IFROM) THEN
         ITEMP  = IS
         IS     = IE
         IE     = ITEMP
         II     = -1
      ENDIF
C
C   Copy the string.
C
      IF (IFROM.NE.ITO) THEN
         DO 10 I = IS,IE,II
            IF     = IFROM + I - 1
            IT     = ITO   + I - 1
            STRING(IT:IT) = STRING(IF:IF)
   10       CONTINUE
      ENDIF
C
C   Pad TO with blanks, if longer than FROM.
C
      DO 20 I = LFROMC+1,LTOC
         IT     = ITO   + I - 1
         STRING(IT:IT) = ' '
   20    CONTINUE
C
      RETURN
      END

      SUBROUTINE TTRIM ( STRING,LSTRIN )
C
C   Return the length of STRING after trailing blanks, nulls, and tabs have
C   been removed.
C
      CHARACTER*(*) STRING
C
      CHARACTER NULL,TAB
C
C
C   Initialize the null and tab characters.
C
      NULL   = CHAR(0)
      TAB    = CHAR(9)
C
C   Loop backwards through the character string and find the last nonblank,
C   nonnull character.
C
      LSTRIN = LEN(STRING)
      DO 10 L = LSTRIN,1,-1
         IF (STRING(L:L).NE.' ' .AND. STRING(L:L).NE.NULL
     &                          .AND. STRING(L:L).NE.TAB) THEN
            LSTRIN = L
            GOTO 20
         ENDIF
   10    CONTINUE
C
C   ALL blank or null or tabs!
C
      LSTRIN = 0
C
C
   20 CONTINUE
      RETURN
      END
