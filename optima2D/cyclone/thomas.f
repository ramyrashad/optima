c=====================================================================
c=====================================================================
c
c
c       subroutine for solving tridiagonal systems of equations
c       -------------------------------------------------------
c
c    ( taken from computational fluid dynamics and heat transfer )
c          ( by anderson, tannehill, and pletcher, 1984)
c      ( transcribed by c.p.t. groth, u.t.i.a.s., october 1985 )
c
c     subroutine:
c
c        1) thomas - solves a block tridiagonal system of equations
c                    using the thomas algorithm
c
c

c ************************************************************************
c ************************************************************************
c
c
      subroutine thomas(il,iu,bb,dd,aa,cc)
c
c        this routine solves a tridiagonal system of equations
c     following the thomas algorithm that is based on a gaussian
c     ellimination technique.
c
c     variable description:
c
c     (call statement parameter list)
c
c     il,iu       subscripts of the first and last equations in the
c                 linear system.  the arrays bb, dd, aa, and cc must
c                 be difined in the range from il to iu.
c
c     bb          coefficients in the diagonal of the system matrix
c                 located behind or just to the left of the main
c                 diagonal.
c
c     dd          coefficients in the main diagonal of the system
c                 matrix.
c
c     aa          coefficients in the diagonal of the system matrix
c                 located ahead or just to the right of the main
c                 diagonal.
c
c     cc          elements of the rhs vector.
c
c     programming notes:
c
c        1) the solution vector for the system is returned to
c           the calling program in the cc array.  the cc and dd
c           arrays are overwritten.  arrays aa and bb remain
c           unchanged.
c
c     begin subroutine thomas.
c
c
cc    implicit real*8(a-h,o-z)
      double precision aa(1),bb(1),cc(1),dd(1)
      double precision r
c
c     establish the upper triangular matrix.
c
      lp=il+1
      do 10 i=lp,iu
         r=bb(i)/dd(i-1)
         dd(i)=dd(i)-r*aa(i-1)
         cc(i)=cc(i)-r*cc(i-1)
   10 continue
c
c     perform the back substitution to obtain the solution to the
c     linear tridiagonal system.  save the solution in the cc array.
c
      cc(iu)=cc(iu)/dd(iu)
      do 20 i=lp,iu
         j=iu-i+il
         cc(j)=(cc(j)-aa(j)*cc(j+1))/dd(j)
   20 continue
c
c     end of subroutine thomas.
c
        return
        end
c
c
c=====================================================================
c=====================================================================
