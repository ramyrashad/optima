c     -- parameters for b-splines --
c     -- maximum number of b-spline segments = > maxbseg --
c     -- max. number of knots -> ibskt = ibsord + ibsnc --

	integer ibody, ibsnc, ibskt, maxbseg, ibsord
      parameter (ibody=400, ibsnc=100, ibskt=125, maxbseg=12, ibsord=25)
