#! /bin/sh

FSRC=$SNOPT/src
CSRC=$SNOPT/csrc
AMPLSRC=$SNOPT/ampl/solvers/snopt/src

cp $FSRC/sn03prnt.f   $AMPLSRC/
cp $FSRC/sn15blas.f   $AMPLSRC/

cp $FSRC/sn02lib.f    $AMPLSRC/
cp $FSRC/sn05wrpa.f   $AMPLSRC/
cp $FSRC/sn05wrpb.f   $AMPLSRC/
cp $FSRC/sn05wrpc.f   $AMPLSRC/
cp $FSRC/sn05wrpn.f   $AMPLSRC/
cp $FSRC/sn10mach.f   $AMPLSRC/
cp $FSRC/sn12ampl.f   $AMPLSRC/
cp $FSRC/sn17util.f   $AMPLSRC/
cp $FSRC/sn20amat.f   $AMPLSRC/
cp $FSRC/sn25bfac.f   $AMPLSRC/
cp $FSRC/sn27lu.f     $AMPLSRC/
cp $FSRC/sn30spec.f   $AMPLSRC/
cp $FSRC/sn35mps.f    $AMPLSRC/
cp $FSRC/sn37wrap.f   $AMPLSRC/
cp $FSRC/sn40bfil.f   $AMPLSRC/
cp $FSRC/sn50lp.f     $AMPLSRC/
cp $FSRC/sn55qp.f     $AMPLSRC/
cp $FSRC/sn56qncg.f   $AMPLSRC/
cp $FSRC/sn57qopt.f   $AMPLSRC/
cp $FSRC/sn60srch.f   $AMPLSRC/
cp $FSRC/sn65rmod.f   $AMPLSRC/
cp $FSRC/sn70nobj.f   $AMPLSRC/
cp $FSRC/sn80ncon.f   $AMPLSRC/
cp $FSRC/sn85hess.f   $AMPLSRC/
cp $FSRC/sn87sopt.f   $AMPLSRC/
cp $FSRC/sn90lmqn.f   $AMPLSRC/
cp $FSRC/sn95fmqn.f   $AMPLSRC/
cp $FSRC/np02lib.f    $AMPLSRC/
cp $FSRC/sq02lib.f    $AMPLSRC/
cp $FSRC/snoptq.f     $AMPLSRC/
cp $FSRC/sqopt.f      $AMPLSRC/
cp $FSRC/snopta.f     $AMPLSRC/
cp $FSRC/snoptb.f     $AMPLSRC/
cp $FSRC/snoptc.f     $AMPLSRC/
cp $FSRC/npopt.f      $AMPLSRC/


cp $CSRC/sn03prnt.c   $AMPLSRC/
cp $CSRC/sn15blas.c   $AMPLSRC/

cp $CSRC/sn02lib.c    $AMPLSRC/
cp $CSRC/sn05wrpa.c   $AMPLSRC/
cp $CSRC/sn05wrpb.c   $AMPLSRC/
cp $CSRC/sn05wrpc.c   $AMPLSRC/
cp $CSRC/sn05wrpn.c   $AMPLSRC/
cp $CSRC/sn10mach.c   $AMPLSRC/
cp $CSRC/sn12ampl.c   $AMPLSRC/
cp $CSRC/sn17util.c   $AMPLSRC/
cp $CSRC/sn20amat.c   $AMPLSRC/
cp $CSRC/sn25bfac.c   $AMPLSRC/
cp $CSRC/sn27lu.c     $AMPLSRC/
cp $CSRC/sn30spec.c   $AMPLSRC/
cp $CSRC/sn35mps.c    $AMPLSRC/
cp $CSRC/sn37wrap.c   $AMPLSRC/
cp $CSRC/sn40bfil.c   $AMPLSRC/
cp $CSRC/sn50lp.c     $AMPLSRC/
cp $CSRC/sn55qp.c     $AMPLSRC/
cp $CSRC/sn56qncg.c   $AMPLSRC/
cp $CSRC/sn57qopt.c   $AMPLSRC/
cp $CSRC/sn60srch.c   $AMPLSRC/
cp $CSRC/sn65rmod.c   $AMPLSRC/
cp $CSRC/sn70nobj.c   $AMPLSRC/
cp $CSRC/sn80ncon.c   $AMPLSRC/
cp $CSRC/sn85hess.c   $AMPLSRC/
cp $CSRC/sn87sopt.c   $AMPLSRC/
cp $CSRC/sn90lmqn.c   $AMPLSRC/
cp $CSRC/sn95fmqn.c   $AMPLSRC/
cp $CSRC/np02lib.c    $AMPLSRC/
cp $CSRC/sq02lib.c    $AMPLSRC/
cp $CSRC/snoptq.c     $AMPLSRC/
cp $CSRC/sqopt.c      $AMPLSRC/
cp $CSRC/snopta.c     $AMPLSRC/
cp $CSRC/snoptb.c     $AMPLSRC/
cp $CSRC/snoptc.c     $AMPLSRC/
cp $CSRC/npopt.c      $AMPLSRC/
