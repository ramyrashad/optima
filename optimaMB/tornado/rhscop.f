c     ------------------------------------------------------------------
c     -- copy trailing edge (and leading edge) points --
c     -- inviscid flow --
c     -- m. nemec, aug. 2001 --
c     ------------------------------------------------------------------
      subroutine rhscop( ijte1, ikte1, ijte2, ikte2, xyj1, q1, jmax1,
     &     kmax1, xyj2, q2, s, jmax2, kmax2, turre1, turre2, rsa,
     &     turbulnt)

      implicit none

      logical   turbulnt

      integer ijte1,ikte1,ijte2,ikte2,jmax1,kmax1,jmax2,kmax2,n
      double precision xyj1(jmax1,kmax1),   q1(jmax1,kmax1,4)
      double precision xyj2(jmax2,kmax2),   q2(jmax2,kmax2,4)
      double precision turre2(jmax2,kmax2), turre1(jmax1,kmax1)
      double precision s(jmax2,kmax2,4),    rsa(jmax2,kmax2)
 
      do n = 1,4
         s(ijte2,ikte2,n) = -( xyj2(ijte2,ikte2)*q2(ijte2,ikte2,n) -
     &        xyj1(ijte1,ikte1)*q1(ijte1,ikte1,n) )
      end do

      if (turbulnt) then
         rsa(ijte2,ikte2) = -( turre2(ijte2,ikte2) -
     &        turre1(ijte1,ikte1) )
      end if

c      write (*,*) ijte1, ikte1, ijte2, ikte2

      return
      end                       !rhscop
