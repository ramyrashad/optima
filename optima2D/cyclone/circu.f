      subroutine circu( jdim,kdim,q,x,y,kline, circ)                    
c
#include "../include/arcom.inc"
c                                                                       
c     Returns the circulation about the airfoil around a line          
c     which is kline grid lines from the body.                         
c                                                                       
      dimension q(jdim,kdim,4),x(jdim,kdim),y(jdim,kdim)                
c                                                                       
c     The o-grid capability                                     
      if ( periodic ) then                                              
      circ = 0.                                                         
      arcl = 0.                                                         
c                                                                       
c            integrate around the body                                  
c                                                                       
      k = kline                                                         
      do 50 j = jlow,jup                                                
         u = q( j, k, 2)/q( j, k, 1)                                    
         v = q( j, k, 3)/q( j, k, 1)                                    
         jp1 = jplus(j)                                                 
         u2 = q( jp1, k, 2)/q( jp1, k, 1)                               
         v2 = q( jp1, k, 3)/q( jp1, k, 1)                               
c                                                                       
         arcl = arcl +                                                  
     1         sqrt( ( x( jp1, k) - x( j, k) )**2 +                     
     2               ( y( jp1, k) - y( j, k) )**2 )                     
         circ = circ + .5 *                                             
     1            ( ( ( u+u2) * ( x( jp1, k) - x( j, k) ) ) +           
     2            ( (v+v2) * ( y( jp1, k) - y( j, k) ) ) )              
c                                                                       
50    continue                                                          
c                                                                       
      else                                                              
c                                                                       
      circ = 0.                                                         
      arcl = 0.                                                         
      k = kline                                                         
      jk = min0( k- 1, jtail1 - 1)                                      
c                                                                       
c            integrate around the body                                  
c                                                                       
      do 100 j = jtail1 - jk, jtail2 - 1 + jk                           
         u = q( j, k, 2)/q( j, k, 1)                                    
         v = q( j, k, 3)/q( j, k, 1)                                    
         u2 = q( j+1, k, 2)/q( j+1, k, 1)                               
         v2 = q( j+1, k, 3)/q( j+1, k, 1)                               
c                                                                       
         arcl = arcl +                                                  
     1         sqrt( ( x( j+1, k) - x( j, k) )**2 +                     
     2               ( y( j+1, k) - y( j, k) )**2 )                     
         circ = circ + .5 *                                             
     1            ( ( ( u+u2) * ( x( j+1, k) - x( j, k) ) ) +           
     2            ( (v+v2) * ( y( j+1, k) - y( j, k) ) ) )              
c                                                                       
100   continue                                                          
c                                                                       
c            integrate down perpendicularly through the wake.           
c                                                                       
      j = jtail2 + kline - 1                                            
      j = min0( j, jmax- 1 )                                            
      do 200 k = kline, 2, -1                                           
         u = q( j, k, 2)/q( j, k, 1)                                    
         v = q( j, k, 3)/q( j, k, 1)                                    
         u2 = q( j, k-1, 2)/q( j, k-1, 1)                               
         v2 = q( j, k-1, 3)/q( j, k-1, 1)                               
c                                                                       
         arcl = arcl +                                                  
     1         sqrt( ( x( j, k-1) - x( j, k) )**2 +                     
     2               ( y( j, k-1) - y( j, k) )**2 )                     
         circ = circ + .5 *                                             
     1            ( ( (u+u2) * ( x( j, k-1) - x( j, k) ) ) +            
     2            ( (v+v2) * ( y( j, k-1) - y( j, k) ) ) )              
c                                                                       
200   continue                                                          
c                                                                       
      j = jtail1 - kline + 1                                            
      j = max0( j, 2 )                                                  
c                                                                       
c            skip the first point so as not to double count.            
c                                                                       
      do 300 k = 2, kline, 1                                            
         u = q( j, k, 2)/q( j, k, 1)                                    
         v = q( j, k, 3)/q( j, k, 1)                                    
         u2 = q( j, k-1, 2)/q( j, k-1, 1)                               
         v2 = q( j, k-1, 3)/q( j, k-1, 1)                               
c                                                                       
         arcl = arcl +                                                  
     1         sqrt( ( x( j, k-1) - x( j, k) )**2 +                     
     2               ( y( j, k-1) - y( j, k) )**2 )                     
         circ = circ + .5 *                                             
     1            (( (u+u2) * ( x( j, k) - x( j, k-1) ) ) +             
     2            ( (v+v2) * ( y( j, k) - y( j, k-1) ) ) )              
c                                                                       
300   continue                                                          
c                                                                       
      endif                                                             
c                                                                       
c  scale circulation by fsmach                                          
c                                                                       
      circ = circ / fsmach                                              
c                                                                       
      return                                                            
      end                                                               
