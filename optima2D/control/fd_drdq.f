

      subroutine fd_drdq (jdim, kdim, ndim, indx, q, press, sndsp, xy,
     &      xyj, fmu, x, y, vort, xit, turmu, ipt, jpt, pat, work)
      
#include "../include/arcom.inc"

      integer jdim, kdim, ndim, indx(jdim,kdim)

      double precision q(jdim,kdim,ndim)
      double precision xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim)

      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision uu(maxj,maxk), vv(maxj,maxk) 
      double precision precon(maxj,maxk,6), xit(jdim,kdim)

      double precision ds(maxj,maxk), gam(maxj,maxk,16)
      double precision spect(maxj,maxk,3), turmu(jdim,kdim)
      double precision ccx(maxj,maxk), ccy(maxj,maxk)
      double precision coef4(maxj,maxk), coef2(maxj,maxk)
      double precision work(jdim,kdim,4)

      double precision sm(maxj,maxk,5), sp(maxj,maxk,5)
      double precision dR5dQ5(maxj,maxk,5)

clb
      integer ipa(maxjk*nblk+2)
      integer jpa(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      integer ipt(maxjk*nblk+2)
      integer jpt(maxjk*nblk2*9+1)
      double precision pa(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      double precision pat(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      
      double precision a(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      integer rw(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
      integer cl(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1)
clb
      double precision av(5)
      double precision dCLdQ, cleqp, cleqm
c     ------------------------
c     -- plot variables --      
       character*10 title
       character*2 munt 
       integer lines(1)
       munt = 'in'
c     ------------------------

      dt_tmp = dt
      dt = 1.d0

clb      clopt = .false.

      na = jdim*kdim*ndim
      if (clopt) na = jdim*kdim*ndim+1
      write(*,*) 'na',na

      if (clopt) then
clb   -- finding maximum xyj 
         xyjmax=0.d0
         do k = 1,kend
            do j = 1,jend
               xyjtest=xyj(j,k)
               if (xyjtest .gt. xyjmax) xyjmax=xyjtest
            end do
         end do
      end if

c     -- evaluate vector --
c      fds = 1.d-8
c      fds  = dsqrt(2.2d-16)
      fds = 1.d-12

      av(1) = 0.d0
      av(2) = 0.d0
      av(3) = 0.d0
      av(4) = 0.d0
      av(5) = 0.d0

      do j = 1,jend
        do k = 1,kend
          av(1) = av(1) + q(j,k,1)
          av(2) = av(2) + q(j,k,2)
          av(3) = av(3) + q(j,k,3)
          av(4) = av(4) + q(j,k,4)
        end do
      end do

      if (viscous .and. turbulnt .and. itmodel .eq.2) then
        do j = 1,jend
          do k = 1,kend
            av(5) = av(5) + q(j,k,5)
          end do
        end do
      end if

      do n = 1,ndim
        av(n) = av(n)/jend/kend
      end do

c      qnorm = 0.d0
c      do n = 1,5
c        do j = 1,jend
c          do k = 1,kend
c            qnorm =  qnorm + q(j,k,n)**2
c          end do
c        end do
c      end do
      
c      qnorm = dsqrt(qnorm)
    
c      eps = fds/qnorm


c      do k = 1,kend
c        do j = 1,jend
c          write (*,*) q(j,k,1),q(j,k,2),q(j,k,4)
c        end do
c      end do
c      stop

      write (*,*) 'Building FD Jacobian:'

      nz = 0
clb    ipa(1) = 1

c      do jj = 1,jend
c        write (*,*) 'J index:', jj, nz
c        do kk = 1,kend
c          do nn = 1,5
c
c            tmp = q(jj,kk,nn)
c            stepsize = fds*max( abs(q(jj,kk,nn)), abs(av(nn)) )
c     &            *sign(1.d0,q(jj,kk,nn)) 
c
c            
cc            stepsize = fds*q(jj,kk,nn)
cc
cc            if ( dabs(stepsize) .lt. 1.d-12 ) then
cc              stepsize = 1.d-12*dsign(1.d0,stepsize)
cc            end if
c
cc     -- evaluate (+) state --
c            q(jj,kk,nn) = q(jj,kk,nn) + stepsize
c
c            stepsize = q(jj,kk,nn) - tmp
c
c            call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
c
c            call get_rhs (jdim, kdim, ndim, q, xy,  xyj, x, y, precon,
c     &            coef2, coef4, uu, vv, ccx, ccy, ds, press, xit, spect,
c     &            gam, turmu, fmu, vort, sndsp, sp, work)
c
cc            if (jj.eq.80.and.kk.eq.1) then
cc              write (*,*) '+ s ',spect(jj,kk,1), vv(jj,kk), ccy(jj,kk)
cc     &              ,q(jj,kk,nn) 
cc              vvtmp1 = spect(jj,kk,1)
cc              vvtmpa = vv(jj,kk)
cc              ccya = ccy(jj,kk)
cc            end if
c
cc     -- evaluate (-) state -- 
c            q(jj,kk,nn) = tmp - stepsize
c
c            call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
c
c            call get_rhs (jdim, kdim, ndim, q, xy,  xyj, x, y, precon,
c     &            coef2, coef4, uu, vv, ccx, ccy, ds, press, xit, spect,
c     &            gam,turmu, fmu, vort, sndsp, sm, work)
c
cc            if (jj.eq.80.and.kk.eq.1) then
cc              write (*,*) '- s ',spect(jj,kk,1),vv(jj,kk),ccy(jj,kk)
cc     &              ,q(jj,kk,nn)  
cc              vvtmp2 = spect(jj,kk,1)
cc              vvtmpb = vv(jj,kk)
cc              ccyb = ccy(jj,kk)
cc              write (*,*) (vvtmp1 - vvtmp2)/(2.d0*stepsize)
cc              write (*,*) (vvtmpa - vvtmpb)/(2.d0*stepsize)
cc              write (*,*) (ccya -  ccyb)/(2.d0*stepsize)
cc            end if
c
c            q(jj,kk,nn) = tmp 
c
cc     -- evaluate drdq --
c            dxx = 1.d0/(2.d0*stepsize)
c            do n = 1,5
c              do k = 1,kend
c                do j = 1,jend
c                  dR5dQ5(j,k,n) = -( sp(j,k,n) - sm(j,k,n) )*dxx
c                end do
c              end do
c            end do
c
c            ic = ( indx(jj,kk) - 1 )*ndim + nn
c            do j=1,jend
c              do k=1,kend
c                do n = 1,5
c                  if (dabs(dR5dQ5(j,k,n)).gt.1.d-14) then
c
c                    ir = ( indx(j,k) - 1 )*ndim + n
c                    nz = nz+1
c                    jpa(nz) = ir
c                    pa(nz) = dR5dQ5(j,k,n)
c
cc                    if (jj.eq.139.and.kk.eq.27) then
cc                      if(j.eq.140.and.k.eq.27.and.n.eq.5) then
cc                        write (*,*) jj,kk,nn,j,k,n,pa(nz),dR5dQ5(j,k,n)
cc                      end if
cc                    end if
cc                    write (*,*) 'fd',j,k,n,dR5dQ5(j,k,n)
cc                    write (*,*) ic,ir,nz
cc                    write (*,*) 'pa',pa(nz),jpa(nz)
cc                    if (nz.eq.20) stop
c                  end if
c                end do
c              end do
c            end do
c
c            ipa(ic+1) = nz+1
cc            write (*,*) 'ipa',ipa(ic+1)
c          end do
c        end do
c      end do

      call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
      
      call get_rhs (jdim, kdim, ndim, q, xy,  xyj, x, y, precon,
     &      coef2, coef4, uu, vv, ccx, ccy, ds, press, xit, spect,
     &      gam, turmu, fmu, vort, sndsp, sm, work)

      if (clopt) then
         nscal = 0
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
     &        cli, cdi, cmi, clv, cdv, cmv) 
         clt = cli + clv
         write(*,*) 'cltm',clt
         cleqm=-(clt-clinput)/(xyjmax)
      end if      

      write (*,*) turmu(20,2), turmu(20,1)

      do jj = 1,jend
clb        write (*,*) 'J index:', jj, nz
        do kk = 1,kend
          do nn = 1,ndim

            tmp = q(jj,kk,nn)
            stepsize = fds*max( abs(q(jj,kk,nn)), abs(av(nn)) )
     &            *sign(1.d0,q(jj,kk,nn)) 
            
c            stepsize = fds*q(jj,kk,nn)
c
c            if ( dabs(stepsize) .lt. 1.d-12 ) then
c              stepsize = 1.d-12*dsign(1.d0,stepsize)
c            end if

c     -- evaluate (+) state --
            q(jj,kk,nn) = q(jj,kk,nn) + stepsize

            stepsize = q(jj,kk,nn) - tmp

            call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)

            call get_rhs (jdim, kdim, ndim, q, xy,  xyj, x, y, precon,
     &            coef2, coef4, uu, vv, ccx, ccy, ds, press, xit, spect,
     &            gam, turmu, fmu, vort, sndsp, sp, work)

            if (clopt) then
               nscal = 0
               call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
     &              cli, cdi, cmi, clv, cdv, cmv) 
               clt = cli + clv
clb               write(*,*) 'cltp',clt
               cleqp=-(clt-clinput)/(xyjmax)
            end if

c            if (jj.eq.89.and.kk.eq.2) then
c              write (*,*) '+ s ', vv(jj,kk), q(jj,kk,nn) 
c              vvtmpb = vv(jj,kk)
c              write (*,*) (vvtmpb - vvtmpa)/stepsize
c            end if

            q(jj,kk,nn) = tmp 

c     -- evaluate drdq --
            dxx = 1.d0/stepsize
            do n = 1,ndim
              do k = 1,kend
                do j = 1,jend
                  dR5dQ5(j,k,n) = -( sp(j,k,n) - sm(j,k,n) )*dxx
                end do
              end do
            end do
            if (clopt) dCLdQ = - (cleqp - cleqm)*dxx 

clb            if (jj.eq.20 .and. kk.eq.1) then
clb              write (*,*) 't',turmu(20,2), turmu(20,1)
clb              write (*,*) dR5dQ5(20,2,1), dR5dQ5(20,2,2),dR5dQ5(20,2,3)
clb     &              ,dR5dQ5(20,2,4), dR5dQ5(20,2,5)
clb            end if
            

            ic = ( indx(jj,kk) - 1 )*ndim + nn
            do j=1,jend
              do k=1,kend
                do n = 1,ndim
                  if (dabs(dR5dQ5(j,k,n)).gt.1.d-12) then

                    ir = ( indx(j,k) - 1 )*ndim + n
                    nz = nz+1
                    cl(nz) = ir
                    rw(nz) = ic
                    a(nz) = dR5dQ5(j,k,n)
clb                    jpa(nz) = ir
clb                    pa(nz) = dR5dQ5(j,k,n)

c                    if (jj.eq.139.and.kk.eq.27) then
c                      if(j.eq.140.and.k.eq.27.and.n.eq.5) then
c                        write (*,*) jj,kk,nn,j,k,n,pa(nz),dR5dQ5(j,k,n)
c                      end if
c                    end if
c                    write (*,*) 'fd',j,k,n,dR5dQ5(j,k,n)
c                    write (*,*) ic,ir,nz
c                    write (*,*) 'pa',pa(nz),jpa(nz)
c                    if (nz.eq.20) stop
                  end if
                end do
              end do
            end do
            if (clopt .and. dCLdQ.gt.1.d-14) then
               nz = nz+1
               cl(nz) = na
               rw(nz) = ic
               a(nz) = dCLdQ
               
clb               jpa(nz) = na
clb               pa(nz) = dCLdQ
            end if               

clb         ipa(ic+1) = nz+1
c            write (*,*) 'ipa',ipa(ic+1)
          end do
        end do
      end do

      if (clopt) then
         tmp = alpha
         write(*,*) 'alpha1',alpha
         stepsize = fds*max( abs(alpha), abs(av(nn)) )
     &        *sign(1.d0,alpha) 
         
c     stepsize = fds*q(jj,kk,nn)
c     
c     if ( dabs(stepsize) .lt. 1.d-12 ) then
c     stepsize = 1.d-12*dsign(1.d0,stepsize)
c     end if

c     -- evaluate (+) state --
         alpha = alpha + stepsize
         uinf = fsmach*cos(alpha*pi/180.d0)
         vinf = fsmach*sin(alpha*pi/180.d0)            
         write(*,*) 'alpha2',alpha
         stepsize = alpha - tmp
         
         call calcps ( jdim, kdim, q, press, sndsp, precon, xy, xyj)
         
         call get_rhs (jdim, kdim, ndim, q, xy,  xyj, x, y, precon,
     &        coef2, coef4, uu, vv, ccx, ccy, ds, press, xit, spect,
     &        gam, turmu, fmu, vort, sndsp, sp, work)
         
         if (clopt) then
            nscal = 0
            call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
     &           cli, cdi, cmi, clv, cdv, cmv) 
            clt = cli + clv
            write(*,*) 'cltp',clt
            cleqp=-(clt-clinput)/(xyjmax)
         end if
      
c     if (jj.eq.89.and.kk.eq.2) then
c     write (*,*) '+ s ', vv(jj,kk), q(jj,kk,nn) 
c     vvtmpb = vv(jj,kk)
c     write (*,*) (vvtmpb - vvtmpa)/stepsize
c     end if
         
         alpha = tmp 
         uinf = fsmach*cos(alpha*pi/180.d0)
         vinf = fsmach*sin(alpha*pi/180.d0) 
         write(*,*) 'alpha3',alpha
c     -- evaluate drdq --
         dxx = 1.d0/stepsize
         do n = 1,ndim
            do k = 1,kend
               do j = 1,jend
                  dR5dQ5(j,k,n) = -( sp(j,k,n) - sm(j,k,n) )*dxx
               end do
            end do
         end do
         dCLdQ = - (cleqp - cleqm)*dxx 
      
c     lb            if (jj.eq.20 .and. kk.eq.1) then
c     lb              write (*,*) 't',turmu(20,2), turmu(20,1)
c     lb              write (*,*) dR5dQ5(20,2,1), dR5dQ5(20,2,2),dR5dQ5(20,2,3)
c     lb     &              ,dR5dQ5(20,2,4), dR5dQ5(20,2,5)
c     lb            end if
            
         ic = na
         do j=1,jend
            do k=1,kend
               do n = 1,ndim
                  if (dabs(dR5dQ5(j,k,n)).gt.1.d-14) then
                     
                     ir = ( indx(j,k) - 1 )*ndim + n
                     nz = nz+1
                     cl(nz) = ir
                     rw(nz) = ic
                     a(nz) = dR5dQ5(j,k,n)

clb                     jpa(nz) = ir
clb                     pa(nz) = dR5dQ5(j,k,n)
                        
c     if (jj.eq.139.and.kk.eq.27) then
c     if(j.eq.140.and.k.eq.27.and.n.eq.5) then
c     write (*,*) jj,kk,nn,j,k,n,pa(nz),dR5dQ5(j,k,n)
c     end if
c     end if
c     write (*,*) 'fd',j,k,n,dR5dQ5(j,k,n)
c     write (*,*) ic,ir,nz
c     write (*,*) 'pa',pa(nz),jpa(nz)
c     if (nz.eq.20) stop
                  end if
               end do
            end do
         end do
         
         nz = nz+1
         cl(nz) = na
         rw(nz) = ic
         a(nz) = dCLdQ
clb         jpa(nz) = na
clb         pa(nz) = dCLdQ
         write(*,*) 'dcldalpha',dCLdQ
         
clb         ipa(ic+1) = nz+1
      end if

      write(*,*) 'nzmax - fddrdq', nz
      nzmax = nz

      call coocsr(na,nzmax,a,rw,cl,pa,jpa,ipa)

c     -- get csr format --
      write(*,*) 'na',na
      call csrcsc (na, 1, 1, pa, jpa, ipa, pat, jpt, ipt)
      open(unit=2005,file='test_unit',status='new',
     &     form='formatted')
 
      call pspltm (na, na, 0, jpa, ipa, title, 0, 7.0, munt, 0,
     &        lines,  2005)

      write(*,*) 'FD_DRDQ.f'
      stop

      write(*,*) 'after csrcsc1'
      call csrcsc (na, 1, 1, pat, jpt, ipt, pa, jpa, ipa)
      
      write(*,*) 'after csrcsc'
      stop
      dt = dt_tmp
      return
      end                       !fd_drdq
