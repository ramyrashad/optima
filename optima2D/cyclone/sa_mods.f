c     ------------------------------------------------------------------
c     -- Spalart-Allmaras one-equation turbulence model --
c     
c     -- NOTE: 
c     1. No grid sequencing
c     3. No trip terms
c     4. Only steady-state runs
c     5. First order far-field b.c.
c     --
c     
c     -- m. nemec, aug. 2001 --
c     ------------------------------------------------------------------
      subroutine sa_mods(jdim, kdim, q, press, turmu, fmu, x, y, xy,
     &     xyj, uu, vv, p, d, p1, d1, vorticity, p_prime, d_prime, work,
     &     workq, fnu, d_p_bar, bx, cx, dx, by, cy, dy, fy, fx, ffy,ffx,
     &     fby)
c     -- coded by : p. godin --
c     -- mods : s. de rango 10/04/96 --
c     -- mods : m. nemec 2000/01 --

#include "../include/arcom.inc"
#include "../include/sam.inc"
#include "../include/optcom.inc"

      dimension q(jdim,kdim,4),turmu(jdim,kdim)
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension fmu(jdim,kdim),turre2(maxj,maxk),uu(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim),vv(jdim,kdim)
      dimension tmpx(maxj,maxk),tmpy(maxj,maxk),workph(maxj,maxk)

      dimension 
     &     p(jdim,kdim),d(jdim,kdim),
     &     d_prime(jdim,kdim),
     &     p_prime(jdim,kdim),d_p_bar(jdim,kdim),
     &     p1(jdim,kdim),d1(jdim,kdim),vorticity(jdim,kdim)

      dimension
     &     work(jdim,kdim),workq(jdim,kdim,3),fnu(jdim,kdim)

      dimension    
     &     bx(kdim,jdim),cx(kdim,jdim),dx(kdim,jdim),
     &     by(jdim,kdim),cy(jdim,kdim),dy(jdim,kdim),
     &     fy(jdim,kdim),fx(kdim,jdim),
     &     ffy(jdim,kdim),ffx(kdim,jdim),fby(jdim,kdim),
     &     denom(maxj,maxk)

      reinv=1.0/re
      resiginv= sigmainv*reinv

c     -- time step --
      dtm=dtow

      do k=kbegin,kend
         do j=jbegin,jend
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)

            uu(j,k)= u*xy(j,k,1) + v*xy(j,k,2)
            vv(j,k)= u*xy(j,k,3) + v*xy(j,k,4)

            workq(j,k,1)=rho
            workq(j,k,2)=u
            workq(j,k,3)=v
c     -- compute inverse of fnu to avoid divisions later --
            fnu(j,k)=rho/fmu(j,k)
         end do
      end do
      
      if (numiter.eq.istart) then
c     -- compute generalized distance function --
         write (*,*) 'New Spalart!!'
         do k=kbegin,kend
            do j=jbegin,jend
               j_point=j
               if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
               xpt1=x(j_point,kbegin)
               ypt1=y(j_point,kbegin)
               smin(j,k)=sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
            end do
         end do

c     -- initialize turre --
         if (.not. restart .or. zeroturre) then
            write (*,*) '!TURRE set to retinf!'
            do k=kbegin,kend
               do j=jbegin,jend
                  turre(j,k)=retinf
               end do
            end do
            
            do j=jtail1,jtail2
               turre(j,kbegin) = 0.0
            enddo
         end if
      end if                    ! if numiter.eq.istart

c     -- calculate vorticity --
      do k=klow,kup
         kp1=k+1
         km1=k-1
         do j=jlow,jup
            jp1=j+1
            jm1=j-1
            ujm1=workq(jm1,k,2)
            ujp1=workq(jp1,k,2)
            ukm1=workq(j,km1,2)
            ukp1=workq(j,kp1,2)
            vjm1=workq(jm1,k,3)
            vjp1=workq(jp1,k,3)
            vkm1=workq(j,km1,3)
            vkp1=workq(j,kp1,3)

            vorticity(j,k)=max(0.5*abs(
     &           (vjp1-vjm1)*xy(j,k,1)+(vkp1-vkm1)*xy(j,k,3)
     &           -(ujp1-ujm1)*xy(j,k,2)-(ukp1-ukm1)*xy(j,k,4)),
     &           8.5e-10)
         end do
      end do

c     -----------------------------------------------------------------
c     -- begin S-A Model --
c     -----------------------------------------------------------------
c     -- z and zz are used as a temporary storage variables --
      do k=klow,kup
         do j=jlow,jup
            zz=1.0/(akarman*smin(j,k))**2
            chi_prime=fnu(j,k)
            chi=turre(j,k)*chi_prime

            z=chi**2/(chi**3+cv1_3)
            fv1=z*chi
            fv1_prime=3.0*z*chi_prime*(1.0-fv1)

            s=vorticity(j,k)*re

            z=1.0/(1.0+chi*fv1)
            fv2=1.0-chi*z
            fv2_prime=z*(-chi_prime +
     &           chi*(chi_prime*fv1+fv1_prime*chi)*z)

            s_tilda=s+turre(j,k)*fv2*zz
            s_tilda_prime=zz*(fv2+turre(j,k)*fv2_prime)
            
            z=zz/s_tilda
            r=turre(j,k)*z
            r_prime=z-r/s_tilda*s_tilda_prime

            if (abs(r).ge.10.0) then
               fw=const
               fw_prime=0.0
            else
               g=r+cw2*(r**6-r)
               z=((1.0+cw3_6)/(g**6+cw3_6))**expon 
               g_prime=r_prime+cw2*(6.0*r**5*r_prime-r_prime)
               fw=g*z
               fw_prime=g_prime*z*(1.-g**6/(g**6+cw3_6))
            end if
            ft2=ct3*exp(-ct4*chi**2)
c     mn: zero trip terms
            ft2 = 0.0
            ft2_prime=-ct4*2.0*chi*chi_prime*ft2
            
            
            p1(j,k)=cb1*(1.0-ft2)*s_tilda*reinv
            p(j,k)=p1(j,k)*turre(j,k)
            p_prime(j,k)=cb1*(-ft2_prime*s_tilda+
     &           (1.0-ft2)*s_tilda_prime)*reinv
            
            z=cb1/akarman**2
            z1=1.0/smin(j,k)**2
            z2=cw1*fw_prime-z*ft2_prime
            d1(j,k)=(cw1*fw-z*ft2)*z1*reinv
            d_prime(j,k)=turre(j,k)*z1*z2*reinv + d1(j,k)
            d1(j,k)=d1(j,k)*turre(j,k)
            d(j,k)=d1(j,k)*turre(j,k)
         end do
      end do

      do k=kbegin,kend
         do j=jbegin,jend
            work(j,k)=1.0/fnu(j,k)+turre(j,k)
         end do
      end do

c     -----------------------------------------------------------------
c     -- F_eta_eta Viscous Terms --
c     -----------------------------------------------------------------
      do k=klow,kup
         kp1 = k+1
         km1 = k-1
         do j=jlow,jup
            xy3p     = .5*(xy(j,k,3)+xy(j,kp1,3))
            xy4p     = .5*(xy(j,k,4)+xy(j,kp1,4))
            ttp      =  (xy3p*xy(j,k,3)+xy4p*xy(j,k,4))
            
            xy3m     = .5*(xy(j,k,3)+xy(j,km1,3))
            xy4m     = .5*(xy(j,k,4)+xy(j,km1,4))
            ttm      =  (xy3m*xy(j,k,3)+xy4m*xy(j,k,4))
            
            cnud=cb2*resiginv*work(j,k)
            
            cdp       =    ttp*cnud
            cdm       =    ttm*cnud
            
            trem =.5*(work(j,km1)+work(j,k))
            trep =.5*(work(j,k)+work(j,kp1))
            
            cap  =  ttp*trep*(1.0+cb2)*resiginv
            cam  =  ttm*trem*(1.0+cb2)*resiginv
            
            by(j,k)   = cdm-cam
            cy(j,k)   = -cdp+cap-cdm+cam
            dy(j,k)   = cdp-cap
            
            fy(j,k)   = - by(j,k)*turre(j,km1)
     &           - cy(j,k)*turre(j,k  )
     &           - dy(j,k)*turre(j,kp1)
            
         end do
      end do

c     -----------------------------------------------------------------
c     Advective Terms in Eta
c     -----------------------------------------------------------------
      do k=klow,kup
         do j=jlow,jup
            sgnu = sign(1.0,vv(j,k))
            app  = 0.5*(1.0+sgnu)
            apm  = 0.5*(1.0-sgnu)
            fy(j,k) = fy(j,k) - vv(j,k)*( app*(turre(j,k)  -turre(j,k-1)
     &           )+apm*(turre(j,k+1)-turre(j,k) ))
            by(j,k)   = by(j,k)   - vv(j,k)*app
            cy(j,k)   = cy(j,k)   + vv(j,k)*(app-apm)
            dy(j,k)   = dy(j,k)   + vv(j,k)*apm
         end do
      end do

c     -----------------------------------------------------------------
c     -- E_xi_xi Viscous Terms --
c     -----------------------------------------------------------------
      do k=klow,kup
         do j=jlow,jup
            jp1 = j+1
            jm1 = j-1
            xy1p     = .5*(xy(j,k,1)+xy(jp1,k,1))
            xy2p     = .5*(xy(j,k,2)+xy(jp1,k,2))
            ttp      =  (xy1p*xy(j,k,1)+xy2p*xy(j,k,2))
            
            xy1m     = .5*(xy(j,k,1)+xy(jm1,k,1))
            xy2m     = .5*(xy(j,k,2)+xy(jm1,k,2))
            ttm      =  (xy1m*xy(j,k,1)+xy2m*xy(j,k,2))
            
            cnud=cb2*resiginv*work(j,k)
            
            cdp       =    ttp*cnud
            cdm       =    ttm*cnud               
c     
            trem =.5*(work(jm1,k)+work(j,k))
            trep =.5*(work(j,k)+work(jp1,k))
c     
            cap  =  ttp*trep*(1.0+cb2)*resiginv
            cam  =  ttm*trem*(1.0+cb2)*resiginv
            
c     
            bx(k,j)   = cdm-cam
            cx(k,j)   = -cdp+cap-cdm+cam
            dx(k,j)   = cdp-cap
            
            fy(j,k)   =  fy(j,k) - bx(k,j)*turre(jm1,k)
     &           - cx(k,j)*turre(j,k  )
     &           - dx(k,j)*turre(jp1,k) 
         end do
      end do

c     -----------------------------------------------------------------
c     -- Advective terms in xi --
c     -----------------------------------------------------------------
      do k=klow,kup
         do j=jlow,jup
            sgnu = sign(1.,uu(j,k))
            app  = .5*(1.+sgnu)
            apm  = .5*(1.-sgnu)
            fy(j,k)= fy(j,k) - 
     &           uu(j,k)*(app*(turre(j,k)-turre(j-1,k))
     &           +apm*(turre(j+1,k)-turre(j,k)) )
            bx(k,j)   = bx(k,j)   - uu(j,k)*app
            cx(k,j)   = cx(k,j)   + uu(j,k)*(app-apm)
            dx(k,j)   = dx(k,j)   + uu(j,k)*apm
         end do
      end do

c     -----------------------------------------------------------------
c     -- Time Marching in Turbulence Model --
c     -----------------------------------------------------------------
      do k=klow,kup
         do j=jlow,jup
c     -- jacobian --
            d_p_bar(j,k)=(max(d1(j,k)-p1(j,k),0.0)+
     &           max(d_prime(j,k)-p_prime(j,k),0.0)*
     &           turre(j,k))

            denom(j,k)=1.0/(1.0+dtm*(cx(k,j)+cy(j,k)+d_p_bar(j,k)))

            bx(k,j) = bx(k,j)*dtm*denom(j,k)
            cx(k,j) = 1.0
            dx(k,j) = dx(k,j)*dtm*denom(j,k)
            by(j,k) = by(j,k)*dtm*denom(j,k)
            cy(j,k) = 1.0
            dy(j,k) = dy(j,k)*dtm*denom(j,k)
         end do
      end do

      do k=kbegin,kend
         do j=jbegin,jend
            turre2(j,k)=turre(j,k)
            ffy(j,k)=0.0
         end do
      end do

      do j=jbegin,jend
         do k=kbegin,kend
            ffx(k,j)=0.0
            fx(k,j)=0.0
         end do
      end do

c     -- invert LHS --
      isub=1
      do k = klow,kup
         do j = jlow,jup
            tt=p(j,k)-d(j,k)
            fby(j,k) = (fy(j,k)+tt)*dtm*denom(j,k)
         end do
      end do

      call triv(jdim,kdim,jlow,jup,klow,kup,work,by,cy,dy,fby)

      do k = klow,kup
         do j = jlow,jup
            fx(k,j) = fby(j,k)
         end do
      end do

      call triv(kdim,jdim,klow,kup,jlow,jup,work,bx,cx,dx,fx)

      negn = 0
      do k = klow,kup
         do j = jlow,jup
            turre(j,k) = turre2(j,k) + fx(k,j)
            if(turre(j,k) .lt. 0.0)then
               negn        =   negn + 1
               turre(j,k)  =   turre2(j,k)*0.5
            endif
         end do
      end do

      if (negn .ne. 0) write (*,*) 'NEGATIVE NU',numiter,negn

c     mn 
c     -- added division by xyj in res=fx(k,j)**2 line and res_spl2 used
c     in computing total residual in residl2.f --
      res_spl=0.0
      resmax=0.0
      jresmax=0
      kresmax=0
      do j=jlow,jup
         do k=klow,kup
            res=fx(k,j)**2/xyj(j,k)
            res_spl=res_spl + res
            if (res.gt.resmax) then
               resmax=res
               jresmax=j
               kresmax=k
            end if
         end do
      end do
      res_spl2 = res_spl
      res_spl=sqrt(res_spl)/float((jup-jlow+1)*(kup-klow+1))
      resmax=sqrt(resmax)
      if (thisout) write (turbhis_unit,100) numiter, res_spl, isub,
     &     jresmax, kresmax, resmax
 100  format(I7,1x,e15.8,3I6,1x,e15.8)
c     -- end time marching --

c     -----------------------------------------------------------------
c     -- apply boundary conditions --
c     -----------------------------------------------------------------
c     -- farfield and outflow boundaries --
      jb2=jbegin+2
      jb1=jbegin+1
      je1=jend-1
      je2=jend-2
      
c     -- ZEROETH ORDER => ispbc=0 --
c     -- FIRST ORDER   => ispbc=1 --
c     -- works well with ispbc=0 --
      ispbc = 0
      factor=float(ispbc)

c     -- mn mod --
c     -- outer-boundary  k = kmax --
      do j = jbegin+1,jend-1
c     -- metrics terms --
         par = sqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
         xy3 = xy(j,kend,3)/par
         xy4 = xy(j,kend,4)/par
         
         u   = q(j,kend,2)/q(j,kend,1)
         v   = q(j,kend,3)/q(j,kend,1)
         vn = xy3*u + xy4*v

         if (vn.le.0.0) then
c     -- inflow --
            turre(j,kend) = retinf
         else
c     -- outflow --          
            turre(j,kend) = turre(j,k-1)
         endif
      end do

c     -- extrapolate outflow --
      do k=kbegin,kend
         turre(jbegin,k)=(1.+factor)*turre(jb1,k)-factor*turre(jb2,k)
         turre(jend,k)  =(1.+factor)*turre(je1,k)-factor*turre(je2,k)
      enddo
c     -- mn mod end --

c     -- wake-cut --
      do j=1,jtail1-1
         jj = jend - j + 1
         turre(j,1)  = 0.5*(turre(j,2)+turre(jj,2))
         turre(jj,1) = turre(j,1)
      enddo

c     -- the eddy viscosity, turmu, is stored at half-points
c     in eta direction.  ie. at k+1/2 points --

      do k=kbegin,kup
         kp1=k+1
         do j=jbegin,jend
            chi=turre(j,k)*fnu(j,k)
            fv1=chi**3/(chi**3+cv1_3)
            tur1 = fv1*turre(j,k)*workq(j,k,1)

            chip1=turre(j,kp1)*fnu(j,kp1)
            fv1p1=chip1**3/(chip1**3+cv1_3)
            tur2 = fv1p1*turre(j,kp1)*workq(j,kp1,1)

            turmu(j,k) = 0.5*(tur1+tur2)
         end do
      end do

      return
      end                       ! sa_mods
