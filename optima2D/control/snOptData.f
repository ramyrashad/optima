************************************************************************
      !-- Program name: snOptData
      !-- Written by: Howard Buckley
      !-- Date: August 2008
************************************************************************
*     ==================================================================
*     snOptData defines input data for the optimization problem in the 
*     format required by the snoptA interface.
*     
*     On exit:
*        nF  is the number of objective and constraint functions
*               (including linear and nonlinear)
*        n    is the number of variables.
*
*        (iGfun(k),jGvar(k)), k = 1,2,...,neG, define the coordinates
*             of the nonzero problem derivatives.
*             If (iGfun(k),jGvar(k)) = (i,j), G(k) is the ijth element
*             of the problem vector F(i), i = 0,1,2,...,nF,  with
*             objective function in position 0 and constraint functions
*             in positions  1  through  m.
*
*        (iAfun(k),jAvar(k),a(k)), k = 1,2,...,neA, are the coordinates
*             of the nonzero constant problem derivatives.
*
*     ==================================================================
      subroutine snOptData
     &   ( Errors, maxF, maxn,
     &     iAfun, jAvar, lenA, neA, A,
     &     iGfun, jGvar, lenG, neG,
     &     Prob, nF, n,
     &     ObjAdd, ObjRow, xlow, xupp, Flow, Fupp,
     &     sn_x, xstate, xmul, F, Fstate, Fmul,
     &     cw, lencw, iw, leniw, rw, lenrw, dvs, dvscale)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit
     &     none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      !-- Declare Optima2D variables

      double precision
     &     dvscale(nc+mpopt), dvs(nc+mpopt)

      !-- Declare SNOPT variables

      integer
     &     Errors, maxF, maxn, neA, lenA, neG, lenG, nF, n,
     &     ObjRow, lencw, leniw, lenrw, xstate(maxn), Fstate(maxF),
     &     iAfun(lenA), jAvar(lenA), iGfun(lenG), jGvar(lenG),
     &     iw(leniw)
      double precision
     &     ObjAdd, diff, 
     &     A(lenA), xlow(maxn), xupp(maxn), Flow(maxF), Fupp(maxF),
     &     sn_x(maxn), F(maxF), xmul(maxn), Fmul(maxF), rw(lenrw),
     &     gdvavg, gdvsum
      character
     &     Prob*8, cw(lencw)*8
      integer
     &     i, Obj, r, d1, d10


*     ------------------------------------------------------------------
      double precision     zero,         one ,         two
      parameter           (zero =0.0d+0, one  =1.0d+0, two    =2.0d+0 )
      double precision     four,         five,         plInfy
      parameter           (four =4.0d+0, five =5.0d+0, plInfy =1.0d+20)
*     ------------------------------------------------------------------

      !-- Declare local variables

      integer
     &     j, k, m, h
      
*     Give the problem a name.

      Prob   = 'optima2D_SN'

*     Initialize some arrays
      do i=1,nc+mpopt
         dvscale(i) = 0.d0
      end do

*     Assign the dimensions of the constraint Jacobian.

      ngcon = ntcon+nrtcon
      if(wac.gt.0) ngcon = ntcon+nrtcon+1
                      
      !-- If geometric constraints are handled using the quadratic 
      !-- penalty method, then set the number of geometric constraints 
      !-- handled by SNOPT equal to zero.
      
      if (use_quad_penalty_meth) ngcon = 0     

      !-- Number of objective and constraint func's
      nF    = 1+noffcon+ngcon+nlcdm
      Obj    = 1          ! Objective function row
      ObjRow = 1          ! Could be 0
      n      = ndv        ! Number of design variables

*     Check that there is enough storage.

      Errors = 0
      if (nF     .gt. maxF ) Errors = 1
      if (n      .gt. maxn ) Errors = 1
      if (Errors .gt.     0) return

      
      !-- neA is the number of non-zero elements of the Jacobian of 
      !-- linear functions wrt design varibles
      
      neA = 0


*     Define the list of coordinates for the non-zero elements of the
*     Jacobian matrix 'G' (See snoptA user's guide for description of 
*     'G').  

      neG        =  0

      !-- First define the coordinates of the top row of the Jacobian
      !-- matrix.  The top row is the gradient of the objective function
      !-- wrt the design variables

      do i = 1, ndv

         neG        =  neG + 1
*        G(neG)     =  grad(i)
         iGfun(neG) =  Obj
         jGvar(neG) =  i
        
      end do

      !-- Now define the coordinates of remaining rows of the Jacobian
      !-- with each row corresponding to the gradient of a constraint
      !-- function wrt the design variables.

      do i = 2, nF

         do j = 1, ndv

            neG        =  neG + 1
*           G(neG)     =  dCdx(i-1, j)
            iGfun(neG) =  i
            jGvar(neG) =  j
        
         end do

      end do

*     Initialize design vairables
*     'sn_x' is the SNOPT design variables
*     'dvs' is the optima2d design variables

      if (sc_method .eq. 0) then 
         do i=1,ndv
            dvscale(i) = 1.0
         enddo

      elseif (sc_method .eq. 1) then 
         do i=1,ndv
            dvscale(i) = sign(1.0,dvs(i))*dvs(i)
         enddo

      elseif (sc_method .eq. 2) then 
         do i=1,ngdv
            dvscale(i) = sign(1.0,dvs(i))*dvs(i)
         enddo

         do i=ngdv+1,ndv
            dvscale(i) = 1.0 
         enddo

      elseif (sc_method .eq. 3) then

         !-- Calculate the average of the absoulte values of the 
         !-- bspline control point design variables
         gdvsum = 0.0
         do i=1,ngdv
            gdvsum = gdvsum + abs(dvs(i))
         enddo

         gdvavg = gdvsum/ngdv
         
         !-- Set the gdv scaling factor to the average value
         if (gdv_scale.eq.0.0) gdv_scale = gdvavg
 
         do i=1,ngdv
            dvscale(i) = gdv_scale
         enddo

         do i=ngdv+1,ndv
            dvscale(i) = aoadv_scale 
         enddo

      end if

      !-- Scale the Mach # design variables
      if (ndvmach.eq.1) then
         dvscale(ndv) = mdv_scale
      else if (ndvmach.eq.2) then
         dvscale(ndv-1) = mdv_scale
         dvscale(ndv) = mdv_scale
      end if

*     Set the SNOPT design variable array 'sn_x' equal to
*     the scaled design variables

      do i=1,ndv
         sn_x(i) = dvs(i)/dvscale(i)
      enddo

      ObjAdd = zero

*     Set upper and lower bounds on design varibles

      do i = 1, ngdv
         xlow(i)   =  sn_x(i) - abs(sn_x(i)*50)
         xupp(i)   =  sn_x(i) + abs(sn_x(i)*50)
         xstate(i) =  0
      end do

      do i = ngdv+1, ndv
         xlow(i)   =  -plInfy
         xupp(i)   =  +plInfy
         xstate(i) =  0
      end do

      if (ndvmach.eq.1) then
         xlow(ndv)   =  mdv_low/mdv_scale
         xupp(ndv)   =  mdv_upp/mdv_scale
         xstate(ndv) =  0
      else if (ndvmach.eq.2) then
         xlow(ndv)   =  mdv_low/mdv_scale
         xupp(ndv)   =  mdv_upp/mdv_scale
         xstate(ndv) =  0

         xlow(ndv-1)   =  mdv_low/mdv_scale
         xupp(ndv-1)   =  mdv_upp/mdv_scale
         xstate(ndv-1) =  0
      end if

      !-- Set upper and lower bounds on the objective function

      Flow(Obj) = -plInfy
      Fupp(Obj) =  plInfy

      if (noffcon.gt.0) then

         !-- Set bounds on off-design constraints:
         !-- off-desing constraints occupy the 'F' index range 
         !-- 2 thru (noffcon+1)

         j = 1

         do i=2,noffcon+1
            
            if (scale_con) then

               if (abs(c_low2(j)).lt.1e+20) then
                  Flow(i)   = c_low2(j)/c_low2(j)
                  Fupp(i)   = c_upp2(j)
               else
                  Flow(i)   = c_low2(j)
                  Fupp(i)   = c_upp2(j)/c_upp2(j)
               end if

               j = j + 1
            else
               Flow(i)   = c_low2(j)
               Fupp(i)   = c_upp2(j)
               j = j + 1
            end if

         end do

      end if
 
      if (ntcon.gt.0) then

         !-- Set bounds on fixed thickness constraints:
         !-- Thickness constraints occupy the 'F' index range 
         !-- (noffcon+2) thru (noffcon+ntcon+1)

         j = 1

         do i=noffcon+2,noffcon+ntcon+1

            if (scale_con) then
               Flow(i)   = cty_tar(j)/cty_tar(j) 
               Fupp(i)   = plInfy
               j = j + 1
            else
               Flow(i)   = cty_tar(j) 
               Fupp(i)   = plInfy
               j = j + 1
            end if

         end do

      end if
 

      if (nrtcon.gt.0) then

         !-- Set bounds on range thickness constraint:
         !-- Range thickness constraints occupy the 'F' index range 
         !-- (noffcon+ntcon+2) thru (noffcon+ntcon+nrtcon+1)

         do i=noffcon+ntcon+2,noffcon+ntcon+nrtcon+1

            if (scale_con) then
               Flow(i)   = crthtar(1)/crthtar(1)
               Fupp(i)   = plInfy
            else 
               Flow(i)   = crthtar(1)
               Fupp(i)   = plInfy
            end if

         end do

      end if

      if (wac.gt.0) then

         !-- Set bounds on area constraint:
         !-- The area constraint occupies the 'F' index: 
         !-- (noffcon+ngcon+1)

         i=noffcon+ngcon+1

         !-- No need for scale_con because area constraint is already
         !-- normalized by inital area value - similar to normalizing by 
         !-- constraint bound
         Flow(i)   = areafac
         Fupp(i)   = plInfy

      end if

      if (nlcdm.gt.0) then

         !-- Set bounds on lift constraints defined for points where
         !-- OBJ_FUNC=19:
         !-- The lift constraints occupy the 'F' index range:
         !-- (noffcon+ntcon+nrtcon+2) thru
         !-- (noffcon+ntcon+nrtcon+nlcdm+1)

         j = 1

         do i=noffcon+ngcon+2,noffcon+ngcon+nlcdm+1

            if (scale_con) then
               Flow(i)   = cltars2(j)/cltars2(j)
               Fupp(i)   = cltars2(j)/cltars2(j)
               j = j + 1
            else
               Flow(i)   = cltars2(j)/cltars2(j)
               Fupp(i)   = cltars2(j)/cltars2(j)
               j = j + 1
            end if
               
         end do

      end if

*     Initialize the vector of dual variables (Lagrange multipliers) 
*     for the general constraints

      do i = 1, nF
         Fmul(i) = zero
      end do

      end ! subroutine snOptData

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
