      subroutine kaboom (bombdate, bombmnth, bombyear)
c
c
c  Subroutine to terminate execution of the program if the current
c  date is past the time bombed date specified in  calling routine
c
c  The way this works is to use  the TIME external function to get
c  the number of seconds since 00:00:00 GMT January 1st. 1970, and 
c  compare this against the number of seconds to the timebomb date
c  This routine  should be Y2K  compliant  as long as the external
c  time function is Y2K compliant also (it is on IRIX).
c
c  I use the time function rather than a routine which returns the
c  current date as integer month, day and year (eg. idate & itime)
c  because these routines don't  exist on all  architectures. They
c  also return different arguments  on different architectires (eg
c  idate, on IRIX, gives date, month, year with  year as a 2-digit
c  integer whilst AIX version returns months, date, and year  with
c  year as a 4-digit integer).
c
c  This isn't a  very sophisticated routine, and  does not account
c  for changes to  daylight savings time. Also  will  only work on
c  the Eastern Standard Timezone. Other timezones should be fairly
c  easy to implement though.
c
c  David Jones (deHavilland)                       13 January 1998
c
c  Input Arguments:
c  ===============
c
c     bombdate   : day of the month when executable is to timebomb
c     bombmnth   : integer month (1-12) when  exec  is to timebomb
c     bombyear   : 4-digit integer year (eg 1999) when to timebomb
c
#include "../include/units.inc" 

      integer  bombdate, bombmnth, bombyear

      integer  months(12) /31,28,31,30,31,30,31,31,30,31,30,31/
c
#ifdef IBM
#define TIME    time_
#else
#define TIME    time
#endif

      external TIME
      integer  TIME

c
c_________________________________________________________________
c
c  Compute the number of seconds from 00:00:00 GMT January 1 1970
c  to 00:00:00 EST on the timebomb date specified by calling args
c  --------------------------------------------------------------
c
c  Note that GMT is 5 hours ahead of EST,  initialise accordingly
c
      nseconds = 5*3600
c
      iyear = 1970
      do 120 while (iyear .lt. bombyear)
         months(02) = 28
         if (mod(iyear,4) .eq. 0) months(02) = 29

         do 110 imnth = 1,12
            nseconds = nseconds + months(imnth)*24*3600
  110    continue

         iyear = iyear + 1
  120 continue
c
      months(02) = 28
      if (mod(iyear,4) .eq. 0) months(02) = 29
c
      imnth = 1
      do 130 while (imnth .lt. bombmnth)
         nseconds = nseconds + months(imnth)*24*3600
         imnth = imnth + 1
  130 continue
c
      nseconds = nseconds + (bombdate - 1)*24*3600
c
c  Now compare against the return from the external time function
c  --------------------------------------------------------------
c
      if (TIME() .gt. nseconds) then
         write (out_unit,6000)
         stop '- Executable has time-expired -'
      else
         return
      endif
c
 6000 format(' Sorry - The current evaluation license has Time-Expired'/
     /       ' Execution is terminating, please contact deHavilland if'/
     /       ' you require an extension (djones@dehavilland.ca)')
c
      end
