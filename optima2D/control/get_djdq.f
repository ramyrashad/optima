c---- get_dJdQ ---------------------------------------------------------
c Computes the derivative of the objective function, J, with respect to
c the flow variables, Q.  This is multiplied by the metric Jacobian, J,
c to give dJ/d{Q hat} (Done in add_dcdQ, below).
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c ndim: number of equations at each node
c Q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c cpa: pressure coefficient
c cp_tar: target pressure coefficient
c xy, xyj: metrics, and metric jacobian
c x, y: x- and y-coordinates of the grid nodes
c obj0: Initial objective function
c
c Returns:
c dJdQ: dJ/dQ
c
c Chad Oldfield, March 2005
c-----------------------------------------------------------------------
      subroutine get_dJdQ (jdim, kdim, ndim, Q, cpa, cp_tar, xy, xyj,
     |    x, y, dJdQ, obj0)
      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim, ndim
      double precision Q(jdim,kdim,4), cpa(jdim,kdim), cp_tar(jbody,2),
     |    xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision dJdQ(jdim,kdim,ndim)
      double precision obj0

c     Local variables
      double precision radians, cos_alpha, sin_alpha
c         Conversion from degrees to radians, the sine and cosine of the
c         angle of attack.
      double precision c_cc, c_cn, c_cm  ! The constants by which
c         dcc/dQ, dcn/dQ, and dcm/dQ are multiplied (for viscous forces)
      integer i, j, k, n
c         i, j are iterators.
      double precision temp

c     Initialize dJdQ
      do n = 1,ndim
        do k = 1,kdim
          do j = 1,jdim
            dJdQ(j,k,n) = 0.d0
          end do
        end do
      end do 

c     Variables related to angle of attack
      radians = pi/180.0
      cos_alpha = cos(alpha*radians)
      sin_alpha = sin(alpha*radians)

c     Differentiate the objective function
      if (obj_func == 1) then
c       Inverse design (match target pressure distribution).
c       (This is taken from the old implementation, dobjdq.f)
        if (.not. viscous) then
c         Inviscid flow
          i = 1
          do j = jtail1,jtail2

            dJdQ(j,1,1) = ( xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     |          /q(j,1,1)**2*( q(j,1,2)**2+ q(j,1,3)**2 )/(fsmach
     |          *fsmach))
            dJdQ(j,1,2) = ( - xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )
     |          *gami*q(j,1,2)/q(j,1,1)*2.d0/(fsmach*fsmach) )
            dJdQ(j,1,3) = (- xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     |          *q(j,1,3)/q(j,1,1)*2.d0/(fsmach*fsmach))
            dJdQ(j,1,4) = (xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     |          *2.d0/(fsmach*fsmach))
            i = i + 1
          end do
        else
c         Viscous flow
          i = 1
          do j = jtail1,jtail2
            dJdQ(j,1,1) = ( xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     |          /q(j,1,1)**2*( q(j,1,2)**2+ q(j,1,3)**2 )/(fsmach
     |          *fsmach))
            dJdQ(j,1,4) = (xyj(j,1)*( cpa(j,1) - cp_tar(i,2) )*gami
     |          *2.d0/(fsmach*fsmach))
            i = i + 1
          end do
        end if
      elseif (obj_func == 2) then
c       Not active.
      elseif (obj_func == 3) then
c       Not active.
      elseif (obj_func == 4) then
c       Not active.
      elseif (obj_func == 5) then
c       Maximize lift to drag ratio.
        c_cn = sin_alpha/clt - cos_alpha*cdt/clt**2
        c_cc = cos_alpha/clt + sin_alpha*cdt/clt**2
        c_cm = 0.d0
        call add_dcdQ(jdim, kdim, Q, xy, xyj, x, y, c_cc, c_cn,
     |      c_cm, obj0, dJdQ)
      elseif (obj_func == 6) then
c       Lift-constrained drag minimization.
        temp = 2.d0 * wfl * (1.d0 - clt/cl_tar)/(-cl_tar)
        c_cn = temp * cos_alpha
        c_cc = -temp * sin_alpha
        if (cdt > cd_tar) then
          temp = 2.d0 * wfd * (1.d0 - cdt/cd_tar)/(-cd_tar)
          c_cn = c_cn + temp * sin_alpha
          c_cc = c_cc + temp * cos_alpha
        endif
        c_cm = 0.d0
        call add_dcdQ(jdim, kdim, Q, xy, xyj, x, y, c_cc, c_cn,
     |      c_cm, obj0, dJdQ)
      elseif (obj_func == 7) then
c       Maximize lift subject to a moment constraint.
        c_cm = 2.d0*wfd*(1.d0-cmt/cd_tar)/(-cd_tar)
        temp = 2.d0*wfl*(1.d0-clt/cl_tar)/(-cl_tar)
        c_cc = -temp*sin_alpha
        c_cn = temp*cos_alpha + 0.25d0*c_cm
        call add_dcdQ(jdim, kdim, Q, xy, xyj, x, y, c_cc, c_cn,
     |      c_cm, obj0, dJdQ)
      elseif (obj_func == 8) then
c       Drag minimization (lift constraint treated in flow solver).
        c_cn = sin_alpha
        c_cc = cos_alpha
        c_cm = 0.d0
        call add_dcdQ(jdim, kdim, Q, xy, xyj, x, y, c_cc, c_cn,
     |      c_cm, obj0, dJdQ)
      endif

c     Thickness, floating thickness, area, and radius of curvature
c     constraints are independent of Q.

      end
c---- get_dJdQ ---------------------------------------------------------

c---- add_dcdQ ---------------------------------------------------------
c Computes the derivatives of the normal and chordwize forces on the
c airfoil, cn and cc, and the moment on the airfoil (at the leading
c edge), cm, with respect to the flow variables, Q.  These
c are multiplied by constants c_cn and c_cc, and added to dJdQ.
c
c Requires:
c jdim, kdim: number of nodes in xi- and eta-directions
c Q: conserved variables (flow solution), without the inverse of the
c   metric jacobian included (i.e. this is Q, not Q hat).
c xy, xyj: metrics, and metric jacobian
c x, y: x- and y-coordinates of the grid nodes
c c_cc, c_cn, c_cm: the constants by which dcc/dQ, dcn/dQ, and dcm/dQ
c   are multiplied.
c obj0: Initial objective function
c
c Returns:
c dJdQ: with c_cc * dcc/dQ + c_cn * dcn/dQ + c_cm * dcm/dQ added to it.
c
c Modified: Chad Oldfield, March 2006
c Markus Rumpfkeil
c-----------------------------------------------------------------------
      subroutine add_dcdQ (jdim, kdim, Q, xy, xyj, x, y,
     |    c_cc, c_cn, c_cm, obj0, dJdQ)
      implicit none

#include "../include/arcom.inc"

c     Argument data types; see above for descriptions
      integer jdim, kdim
      double precision Q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim),
     |    x(jdim,kdim), y(jdim,kdim)
      double precision c_cc, c_cn, c_cm, obj0
      double precision dJdQ(jdim, kdim, 4)

c     Local Variables
      integer j,k,n,jpl,jmi,jmi2 ! indices, jpl=j+1, jmi=j-1, jmi2=j-2
      double precision temp, const
c         const is a combination of c_cc, c_cn, and c_cm: a constant by
c         which to multiply derivatives wrt Q

      if (c_cm .ne. 0.d0) then
        write (*,*) 'Warning: c_cm not yet implemented in dJdQ'
      endif

c     Inviscid contribution: pressure integral
      temp = gami*fsmach**(-2)
      do j = jtail1+1, jtail2
        jpl=jplus(j)
        jmi=jminus(j)
        jmi2=jminus(jmi)
        const = temp * (c_cc*(y(j,1)-y(jmi,1)) + c_cn*(x(jmi,1)-x(j,1))
     |      + c_cm*(x(j,1)+x(jmi,1))*.5*(x(j,1) -x(jmi,1)))

        dJdQ(j,1,1) = dJdQ(j,1,1) + 0.5d0*const*
     |       (Q(j,1,2)**2/Q(j,1,1)**2+Q(j,1,3)**2/Q(j,1,1)**2)
        dJdQ(j,1,2) = dJdQ(j,1,2) - const*Q(j,1,2)/Q(j,1,1)
        dJdQ(j,1,3) = dJdQ(j,1,3) - const*Q(j,1,3)/Q(j,1,1)
        dJdQ(j,1,4) = dJdQ(j,1,4) + const

        dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + 0.5d0* const*
     |       (Q(jmi,1,2)**2/Q(jmi,1,1)**2+Q(jmi,1,3)**2/Q(jmi,1,1)**2)
        dJdQ(jmi,1,2) = dJdQ(jmi,1,2)-const*Q(jmi,1,2)
     |       /Q(jmi,1,1)
        dJdQ(jmi,1,3) = dJdQ(jmi,1,3)-const*Q(jmi,1,3)
     |       /Q(jmi,1,1)
        dJdQ(jmi,1,4) = dJdQ(jmi,1,4)+const
      enddo

c     Viscous contribution: shear stress integral
      if (viscous) then
        temp = 1.d0/(fsmach**2*re)
        do j = jtail1+1, jtail2
          jpl=jplus(j)
          jmi=jminus(j)
          jmi2=jminus(jmi)
          const = temp *
     |        (  c_cc*(x(j,1)-x(jmi,1))
     |         + c_cn*(y(j,1)-y(jmi,1))
     |         + c_cm*(  (x(j,1)+x(jmi,1))*.5*(y(j,1) -y(jmi,1))
     |                 - (y(j,1)+y(jmi,1))*.5*(x(j,1) -x(jmi,1))))

c         xiy*uxi-xix*vxi terms
          if (j > jtail1+1 .and. j < jtail2) then
c           Interior nodes, xiy*uxi-xix*vxi terms
            dJdQ(jpl,1,1) = dJdQ(jpl,1,1) - 0.5d0*const*
     |          (xy(j,1,2)*Q(jpl,1,2)-xy(j,1,1)*Q(jpl,1,3))
     |          /Q(jpl,1,1)**2
            dJdQ(jpl,1,2) = dJdQ(jpl,1,2) + 0.5d0*const*xy(j,1,2)/
     |          Q(jpl,1,1)
            dJdQ(jpl,1,3) = dJdQ(jpl,1,3) - 0.5d0*const*xy(j,1,1)/
     |          Q(jpl,1,1)

            dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + 0.5d0*const*
     |          (xy(j,1,2)*Q(jmi,1,2)-xy(j,1,1)*Q(jmi,1,3))
     |          /Q(jmi,1,1)**2
            dJdQ(jmi,1,2) = dJdQ(jmi,1,2) - 0.5d0*const*xy(j,1,2)/
     |          Q(jmi,1,1)
            dJdQ(jmi,1,3) = dJdQ(jmi,1,3) + 0.5d0*const*xy(j,1,1)/
     |          Q(jmi,1,1)

            dJdQ(j,1,1) = dJdQ(j,1,1) - 0.5d0*const*
     |          (xy(jmi,1,2)*Q(j,1,2)-xy(jmi,1,1)*Q(j,1,3))/Q(j,1,1)**2
            dJdQ(j,1,2) = dJdQ(j,1,2) + 0.5d0*const*xy(jmi,1,2)/Q(j,1,1)
            dJdQ(j,1,3) = dJdQ(j,1,3) - 0.5d0*const*xy(jmi,1,1)/Q(j,1,1)

            dJdQ(jmi2,1,1) = dJdQ(jmi2,1,1) + 0.5d0*const*
     |          (xy(jmi,1,2)*Q(jmi2,1,2)-xy(jmi,1,1)*Q(jmi2,1,3))/
     |          Q(jmi2,1,1)**2
            dJdQ(jmi2,1,2) = dJdQ(jmi2,1,2) -0.5d0*const*xy(jmi,1,2)/
     |          Q(jmi2,1,1)
            dJdQ(jmi2,1,3) = dJdQ(jmi2,1,3) +0.5d0*const*xy(jmi,1,1)/
     |          Q(jmi2,1,1)
          else if (j == jtail1+1) then
c           Boundary nodes, xiy*uxi-xix*vxi terms (uses upwind stencil)
            dJdQ(jpl,1,1) = dJdQ(jpl,1,1) - 0.5d0*const*
     |          (xy(j,1,2)*Q(jpl,1,2)-xy(j,1,1)*Q(jpl,1,3))
     |          /Q(jpl,1,1)**2
            dJdQ(jpl,1,2) = dJdQ(jpl,1,2) + 0.5d0*const*xy(j,1,2)/
     |          Q(jpl,1,1)
            dJdQ(jpl,1,3) = dJdQ(jpl,1,3) - 0.5d0*const*xy(j,1,1)/
     |          Q(jpl,1,1)

            dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + 0.5d0*const*
     |          (xy(j,1,2)*Q(jmi,1,2)-xy(j,1,1)*Q(jmi,1,3))
     |          /Q(jmi,1,1)**2
            dJdQ(jmi,1,2) = dJdQ(jmi,1,2) - 0.5d0*const*xy(j,1,2)/
     |          Q(jmi,1,1)
            dJdQ(jmi,1,3) = dJdQ(jmi,1,3) + 0.5d0*const*xy(j,1,1)/
     |          Q(jmi,1,1)

            dJdQ(j,1,1) = dJdQ(j,1,1) - const*
     |          (xy(jmi,1,2)*Q(j,1,2)-xy(jmi,1,1)*Q(j,1,3))/Q(j,1,1)**2
            dJdQ(j,1,2) = dJdQ(j,1,2) + const*xy(jmi,1,2)/Q(j,1,1)
            dJdQ(j,1,3) = dJdQ(j,1,3) - const*xy(jmi,1,1)/Q(j,1,1)

            dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + const*
     |          (xy(jmi,1,2)*Q(jmi,1,2)-xy(jmi,1,1)*Q(jmi,1,3))/
     |          Q(jmi,1,1)**2
            dJdQ(jmi,1,2) = dJdQ(jmi,1,2) -const*xy(jmi,1,2)/
     |          Q(jmi,1,1)
            dJdQ(jmi,1,3) = dJdQ(jmi,1,3) +const*xy(jmi,1,1)/
     |          Q(jmi,1,1)

          else if (j == jtail2) then
c           Boundary nodes, xiy*uxi-xix*vxi terms (uses upwind stencil)
            dJdQ(j,1,1) = dJdQ(j,1,1) - const*
     |          (xy(j,1,2)*Q(j,1,2)-xy(j,1,1)*Q(j,1,3))/Q(j,1,1)**2
            dJdQ(j,1,2) = dJdQ(j,1,2) + const*xy(j,1,2)/
     |          Q(j,1,1)
            dJdQ(j,1,3) = dJdQ(j,1,3) - const*xy(j,1,1)/
     |          Q(j,1,1)

            dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + const*
     |          (xy(j,1,2)*Q(jmi,1,2)-xy(j,1,1)*Q(jmi,1,3))
     |          /Q(jmi,1,1)**2
            dJdQ(jmi,1,2) = dJdQ(jmi,1,2) - const*xy(j,1,2)/
     |          Q(jmi,1,1)
            dJdQ(jmi,1,3) = dJdQ(jmi,1,3) + const*xy(j,1,1)/
     |          Q(jmi,1,1)

            dJdQ(j,1,1) = dJdQ(j,1,1) - 0.5d0*const*
     |          (xy(jmi,1,2)*Q(j,1,2)-xy(jmi,1,1)*Q(j,1,3))/Q(j,1,1)**2
            dJdQ(j,1,2) = dJdQ(j,1,2) + 0.5d0*const*xy(jmi,1,2)/Q(j,1,1)
            dJdQ(j,1,3) = dJdQ(j,1,3) - 0.5d0*const*xy(jmi,1,1)/Q(j,1,1)

            dJdQ(jmi2,1,1) = dJdQ(jmi2,1,1) + 0.5d0*const*
     |          (xy(jmi,1,2)*Q(jmi2,1,2)-xy(jmi,1,1)*Q(jmi2,1,3))/
     |          Q(jmi2,1,1)**2
            dJdQ(jmi2,1,2) = dJdQ(jmi2,1,2) -0.5d0*const*xy(jmi,1,2)/
     |          Q(jmi2,1,1)
            dJdQ(jmi2,1,3) = dJdQ(jmi2,1,3) +0.5d0*const*xy(jmi,1,1)/
     |          Q(jmi2,1,1)

          end if  ! xiy*uxi-xix*vxi terms

c         etay*ueta-etay*veta terms
          dJdQ(j,1,1) = dJdQ(j,1,1) + 1.5d0*const*
     |        (xy(j,1,4)*Q(j,1,2)-xy(j,1,3)*Q(j,1,3))/Q(j,1,1)**2
          dJdQ(j,1,2) = dJdQ(j,1,2) - 1.5d0*const*xy(j,1,4)/Q(j,1,1)
          dJdQ(j,1,3) = dJdQ(j,1,3) + 1.5d0*const*xy(j,1,3)/Q(j,1,1)

          dJdQ(jmi,1,1) = dJdQ(jmi,1,1) + 1.5d0*const*
     |        (xy(jmi,1,4)*Q(jmi,1,2)-xy(jmi,1,3)*Q(jmi,1,3))
     |        /Q(jmi,1,1)**2
          dJdQ(jmi,1,2) = dJdQ(jmi,1,2) - 1.5d0*const*xy(jmi,1,4)/
     |        Q(jmi,1,1)
          dJdQ(jmi,1,3) = dJdQ(jmi,1,3) + 1.5d0*const*xy(jmi,1,3)/
     |        Q(jmi,1,1)

          dJdQ(j,2,1) = dJdQ(j,2,1) - 2.0d0*const*
     |        (xy(j,1,4)*Q(j,2,2)-xy(j,1,3)*Q(j,2,3))/Q(j,2,1)**2
          dJdQ(j,2,2) = dJdQ(j,2,2) + 2.0d0*const*xy(j,1,4)/Q(j,2,1)
          dJdQ(j,2,3) = dJdQ(j,2,3) - 2.0d0*const*xy(j,1,3)/Q(j,2,1)

          dJdQ(jmi,2,1) = dJdQ(jmi,2,1) - 2.0d0*const*
     |        (xy(jmi,1,4)*Q(jmi,2,2)-xy(jmi,1,3)*Q(jmi,2,3))
     |        /Q(jmi,2,1)**2
          dJdQ(jmi,2,2) = dJdQ(jmi,2,2) + 2.0d0*const*xy(jmi,1,4)/
     |        Q(jmi,2,1)
          dJdQ(jmi,2,3) = dJdQ(jmi,2,3) - 2.0d0*const*xy(jmi,1,3)/
     |        Q(jmi,2,1)

          dJdQ(j,3,1) = dJdQ(j,3,1) + 0.5d0*const*
     |        (xy(j,1,4)*Q(j,3,2)-xy(j,1,3)*Q(j,3,3))/Q(j,3,1)**2
          dJdQ(j,3,2) = dJdQ(j,3,2) - 0.5d0*const*xy(j,1,4)/Q(j,3,1)
          dJdQ(j,3,3) = dJdQ(j,3,3) + 0.5d0*const*xy(j,1,3)/Q(j,3,1)

          dJdQ(jmi,3,1) = dJdQ(jmi,3,1) + 0.5d0*const*
     |        (xy(jmi,1,4)*Q(jmi,3,2)-xy(jmi,1,3)*Q(jmi,3,3))
     |        /Q(jmi,3,1)**2
          dJdQ(jmi,3,2) = dJdQ(jmi,3,2) - 0.5d0*const*xy(jmi,1,4)/
     |        Q(jmi,3,1)
          dJdQ(jmi,3,3) = dJdQ(jmi,3,3) + 0.5d0*const*xy(jmi,1,3)/
     |        Q(jmi,3,1)
        enddo
      endif   ! Viscous contribution

c     Multiply dJ/dQ by the metric Jacobian, to give dJ/d(Q hat), and scale by
c     initial value
      do j=jtail1, jtail2
        do k=1,3
          do n=1,4
            dJdQ(j,k,n) = dJdQ(j,k,n) * xyj(j,k) / obj0
          enddo
        enddo
      enddo

      end
c---- add_dcdQ ---------------------------------------------------------
