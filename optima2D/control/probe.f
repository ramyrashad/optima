c     -----------------------------------------------------------------
c     
c     ******    ******       ****     ******     ******
c     *     *   *     *     *    *    *     *    *
c     *     *   *     *    *      *   *     *    *
c     ******    ******     *      *   ******     *****
c     *         *    *     *      *   *     *    *
c     *         *     *     *    *    *     *    *
c     *         *      *     ****     ******     ******
c     
c     
c     An Implicit Central-differenced Two-Dimensional 
c     Euler & Navier Stokes Code     
c     
c     
c     Derived from ARC2D J.L. Steger (1976) & T. Pulliam (1984)     
c     Author: A. Pueyo (1994-96)
c     Modifications and O P T I M A 2D implementation: M. Nemec
c     S-A model linearization: M. Nemec (2000-2001)
c     Modifications for constant coefficient of lift: L. Billing (2005)
c     -----------------------------------------------------------------


      subroutine probe (jdim, kdim, ndim, indx, icol, iex, ia, ja, as,
     &     ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, qp, qold, 
     &     press, xy,
     &     xyj, x, y, turmu, fmu, vort, sndsp, tmet, ds, spect, 
     &     uu, vv, ccx, ccy, coef4, coef2, precon, gam, bcf, work1, s,
     &     rhs, sol, D_hat, exprhs,a_jk)

      use disscon_vars

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/sam.inc"

c     -- JG: store AF iterations --
      integer afiters
      common/afiter/afiters

      integer jdim, kdim, ndim, icol(9),na,nas,nap,j,k,n,i,nki
      integer ifirst,j2,k2,jresmax,kresmax, imarchtemp
      double precision resmax,absres,lamDissR,lamDiss2,rF_ratio,s_norm
      double precision rFT_ratio,lamDissTR,lamDissT2

      integer indx(jdim,kdim), iex(jdim,kdim,ndim), its_nk_val

      integer ia(jdim*kdim*ndim+2),iat(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      integer,dimension(:),allocatable :: iatmp,ipatmp,jatmp,jpatmp

c     -- ILU arrays --
      integer ierr, nz
      integer,dimension(:),allocatable :: levs,jw
      double precision alu(iwk)
      integer jlu(iwk),ju(maxjk*nblk)

c     -- gmres work array --
      integer iwk_gmres
      double precision,allocatable :: wk_gmres(:)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)
      double precision xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim)

      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)

      double precision s(jdim,kdim,ndim), rhs(jdim*kdim*ndim+1)
      double precision sol(jdim*kdim*ndim+1), bcf(jdim,kdim,4)
      double precision s0(jdim,kdim,ndim), fac

csi   for unsteady runs with 2nd order backwards time marching
      double precision q(jdim,kdim,ndim), qp(jdim,kdim,ndim)
      double precision qold(jdim,kdim,ndim)
csi

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,39)

c     -- first order accurate Frechet derivatives --
      external framux_o1

c     -- timing variables --
      real*4 time1, time2, t, tarray(2)

c     -- arrays required for tornado restart file --
      dimension jbmax(3),kbmax(3)

c     -- frozen jacobian arrays --
      double precision c2xf(maxj,maxk), c4xf(maxj,maxk)
      double precision c2yf(maxj,maxk), c4yf(maxj,maxk)
      double precision fmuf(maxj,maxk), turmuf(maxj,maxk)
      double precision vortf(maxj,maxk), sapdf(maxj,maxk)
      double precision sadx(maxj,maxk), sady(maxj,maxk)
      double precision suuf(maxj,maxk), svvf(maxj,maxk)

      common/frozen_diss/ c2xf, c4xf, c2yf, c4yf, fmuf, turmuf, vortf,
     &      sapdf, sadx, sady, suuf, svvf 

clb
      double precision rhsstore
      logical clalphatmp, scalena

      double precision rhsnamult
clb

csi   variables used for storing rhs values for esdirk time marching
      double precision D_hat(jdim,kdim,ndim,6)
      double precision exprhs(jdim,kdim,ndim)
      double precision a_jk(6,6)
csi

      integer nkouterend,nkouterstart,numitertmp

      double precision dtiseq(20),dtmins(20),dtow2(20)
      integer jmxi(20),kmxi(20),jskipi(20),iends(20)
      integer isequal,iseqlev, d1, d10

      character*20 filename

      logical hisoutput,frechet,imps

c     -- time vector arrays --
      double precision dtvec(maxj*maxk*nblk)
      integer dttmp(maxj*maxk*nblk)

      common/mesup/ dtiseq,dtmins,dtow2,isequal,iseqlev,
     &      jmxi,kmxi,jskipi,iends

c     ------------------------
c     -- plot variables --      
      character*10 title
      character*2 munt 
      integer lines(1)
      munt = 'in'
c     ------------------------

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c  na is the total size of the jacobian (na x na matrix).
c  nas is the maximum number of values stored in the jacobian.
c  nap is the maximum number of values stored in the preconditioner.
c  clalpha2 is the variable that controls whether or not the angle of 
c  attack varies in the NK stage of flow solution.  Set to TRUE for 
c  variable angle of attack in input file.  Default FALSE.
c  Implementing variable angle of attack means angle of attack is 
c  treated as a flow variable, and a new residual equation is required.
c  s(j,k,n) stores the rhs values in a matrix format.  Does not include 
c  new residual equation
c  rhs vector stores all rhs values.  Used in gmres.
c  rhsstore stores the new rhs value, for the variable angle of attack 
c  cases.
c  scalena controls whether or not this equation is scaled so that the 
c  diagonal is scaled to 1.
c  rhsnamult is the amount that the new rhs equation must be scaled by
c  if scalena is set to TRUE.
c  sol vector contains the update to the flow variables (and angle of 
c  attack variable) after gmres has been run.

      na = jmax*kdim*ndim
      nas = jmax*kdim*ndim*ndim*9
      nap = jmax*kdim*ndim*ndim*5
     
      if (clalpha2) then
         na=na+1
         nap=nap+1
         nas = jmax*kdim*ndim*(ndim*9+1)+jmax*ndim*3+1
      end if

cmpr  Allocate some of the big arrays dynamically
      iwk_gmres=(na+1)*(NK_IMGMR+2)+(NK_IMGMR+1)*NK_IMGMR
      allocate(levs(iwk),jw(3*maxjk*nblk))
      allocate(iatmp(maxjk*nblk+2),ipatmp(maxjk*nblk+2))
      allocate(jatmp(maxjk*nblk*(nblk*9+1)+maxj*nblk*3+1))
      allocate(jpatmp(maxjk*nblk2*5+1))
      allocate(wk_gmres(iwk_gmres))

      clalphatmp=clalpha
      clalpha=.false.

      !-- Initialize counter of dissipation-based continuation iter's

      if (dissCon) then
         dc_its = 0
         dcT_its = 0
      end if

      !-- Initialize boundary conditions array 'bcf'

      do 1 n=1,4
      do 1 k=1,kdim
      do 1 j=1,jdim
         bcf(j,k,n) = 0.0
 1    continue

c     -- reset gmres tolerance --
      fpar(1) = 0.0

c     -- ensure that time metric is set to zero --
      do k=1,kend
        do j=1,jend
          tmet(j,k) = 0.d0
        end do
      end do

c     -- copy pointer arrays --
      do j=1,na+1
        iatmp(j) = ia(j)
        ipatmp(j) = ipa(j)
      end do
      do j = 1,nas
        jatmp(j) = ja(j)
      end do
      do j = 1,nap
        jpatmp(j) = jpa(j)
      end do     

!     this forces ESDIRK4 for the first step of BDF2
!     
cmpr      imarchtemp = jesdirk
cmpr      if (unsted .and. jesdirk == 1 ) jesdirk = 4   

      jstagemx=1
      if(jesdirk.eq.4)then
         jstagemx=6
      elseif(jesdirk.eq.3)then
         jstagemx=4
      endif
      call set_ajk(a_jk)

c     save flow field at zeroth iteration if required for adjoint
      if ( unsted .and.(outtime .or. gradient .eq. 1) .and.   
     &     (opt_meth .eq. 2 .or. opt_meth .eq. 7)     )  then         
         call iomarkus(1,0,jdim,kdim,q,xyj,turmu)
      endif         

      jac_mat = .false.
c      jac_mat = .true.

c      frechet = .false.
      frechet = .false. ! RR: debug
      if (frechet) jac_mat = .true.

      if (unsted) then
         nkouterstart=iend+1
         nkouterend=iend+nk_skip+nk_iends
         dtsmall=dt2 
         call nk_preproc(jdim, kdim, ndim, qp, q, qold)
      else
         nkouterstart=numiter
         nkouterend=numiter
      end if

c     -- check abnormal or nan resid --
      if ( abs( resid - 1000.0) .lt. 0.01 ) then
         clalpha = clalphatmp
         do j=1,na+1
            ia(j) = iatmp(j)
            ipa(j) = ipatmp(j) 
         end do
         do j = 1,nas
            ja(j) = jatmp(j)
         end do
         do j = 1,nap
            jpa(j) = jpatmp(j) 
         end do   
         return
      end if
c---------------------------------
csi   -- start outer iterations --
c--------------------------------- 

      do 10 numitertmp = nkouterstart,nkouterend
         numiter=numitertmp
         if (unsted .and. .not. errest) then
            if (numiter .le. iend+nk_skip ) then
               dt2=dtbig
            else
               dt2=dtsmall
            end if
         else if (unsted .and. errest) then 
            dt2=dta(numiter-iend)
         else
            call reset_qp(jdim,kdim,ndim,qp,q)
         end if

         call calcps (jdim, kdim, qp, press, sndsp, precon, xy, xyj)
         if (viscous) call fmun(jdim, kdim, qp, press, fmu, sndsp)

         do 71 jstage=1,jstagemx
            jsubit=0
            if(jesdirk.eq.4 .or.jesdirk.eq.3)
     &           call nk_calc_exprhs(jdim,kdim,ndim,D_hat,exprhs,a_jk)

            resid=1.d0

            !-- Initialize dissipation-based continuation parameter
            if (dissCon) then
               lamDiss = lamDissMax
               lamDissT = lamDissMaxT
            end if

            !-- If 'lamLockstep=true' then lamDissT follows lamDiss in
            !-- lockstep
            if (lamLockstep) lamDissT = lamDissMaxT*lamDissMax

c------------------------------------------
c     -- start newton (inner) iterations --
c------------------------------------------
            do nki = 1,nk_its               

               if (.not. unsted) then
                  numiter=numiter+1
                  hisoutput = .false.
               end if
               
               t=etime(tarray)
               time1=tarray(1)
               time2=tarray(2)
               
c     -- flag to indicate the first iteration of GMRES, see framux --
               ifirst = 1

               !-- This 'if' statement allows the flow Jacobian 
               !-- preconditioning matrix to be updated until iteration
               !-- nk_pfrz, at which point the preconditioner is frozen.
               !-- (default nk_pfrz=1)

               if (nki.le.nk_pfrz .or. frechet) then
                  prec_mat = .true.
               else
                  prec_mat = .false.           
               end if
               
c     -- form RHS: -R(Q) stored in s --
                            
               if (jstage.eq.1.and.(jesdirk.eq.4.or.jesdirk.eq.3)) then

                  call get_rhs (jdim, kdim, ndim, qp, q, qold, xy, xyj, 
     &                 x, y, precon, coef2, coef4, uu, vv, ccx, ccy, 
     &                 ds, press, tmet, spect, gam, turmu, fmu, vort, 
     &                 sndsp, s, s0, work1, D_hat, exprhs,a_jk,.true.)
                  goto 71

               else          

                  call get_rhs (jdim, kdim, ndim, qp, q, qold, xy, xyj, 
     &                 x, y, precon, coef2, coef4, uu, vv, ccx, ccy, 
     &                 ds, press, tmet, spect, gam, turmu, fmu, vort, 
     &                 sndsp, s, s0, work1, D_hat,exprhs, a_jk,.false.) 
                   
                  !-- If dissipation-based continuation is to be used 
                  !-- as a globalization method for N-K, then modify
                  !-- the residual using the continuation parameter
                  !-- 'lamDiss'. NOTE: Modifications to the residual
                  !-- equations begin in GET_RHS
                  !-- The final modification to the residual is done
                  !-- in ADDDISSX and ADDDISSY after which the modified
                  !-- residual can be expressed as:
                  !-- s = (1 - lamDiss)*R(Q) + lamDiss*D(Q)
                  if (dissCon) then

                    call addDissx(jdim,kdim,qp,s,xyj)
                    call addDissy(jdim,kdim,qp,s,xyj)

                  endif

               end if

               if ( prec_mat .or. jac_mat ) then

c     -- form LHS: preconditioner and Jacobian -- 
c     -- arrays ia, ja, as store O(2) Jacobian --
c     -- arrays ipa, jpa, pa store O(1) preconditioner --
c     -- array ast is used as work array in get_lhs --
                  imps=.true.
                  call get_lhs (jdim, kdim, ndim, indx, icol, iex, 
     &                 ia, ja, as, ipa, jpa, pa, xy, xyj, x, y, qp, 
     &                 press, sndsp, fmu, turmu, uu, vv, ccx, ccy, 
     &                 coef2, coef4, tmet, precon, bcf, work1, 
     &                 ast, nk_pdc, a_jk(jstage,jstage),imps)

c     -- order arrays --
                  if (.not. unsted .and. prec_mat) then          
                     call csrcsc (na, 1, 1, pa, jpa, ipa, pat, jpt, ipt)
                     call csrcsc (na, 1, 1, pat, jpt, ipt, pa, jpa, ipa)
                     if (nki.eq.1) write (out_unit,30) ipa(na+1) -ipa(1)
 30                  format (3x,'Non-zeros in preconditioner:',i8)
                  end if                  
                  if (.not. unsted .and. jac_mat) then
                     call csrcsc (na, 1, 1, as, ja, ia, ast, jat, iat)
                     call csrcsc (na, 1, 1, ast, jat, iat, as, ja, ia)
                     if (nki.eq.1) write (out_unit,20) ia(na+1) - ia(1)
 20                  format (3x,'Non-zeros in Jacobian matrix:',i8)
                  end if
                  scalena = .false.
                  if (clalpha2 .and. nki.gt.1) scalena = .true.
c     -- diagonal scaling for all b.c. equations --
c     -- this is done here in order to reuse code for optimization --
                  call scale_bc (1,jdim,kdim,ndim,indx,iex,ia,ja,as,
     &                 ipa, jpa, pa, s, s0, bcf, work1, rhsnamult,
     &                 scalena)

               else

c     -- matrix free: setup -R(Q) b.c. scaling --
                  imps = .false.
                  
                  do n = 1,4
                     do k = kbegin,kend
                        do j = jbegin,jend          
                           iex(j,k,n) = n
                        end do
                     end do
                  end do

                  call impbc (jdim, kdim, ndim, indx, icol, iex, qp,
     &                 xy, xyj, x, y, as, pa, imps, bcf, press)
                  scalena = .false.
                  if (clalpha2 .and. nki.gt.1) scalena = .true.
                  call scale_bc (1,jdim,kdim,ndim,indx,iex,ia,ja,as,
     &                 ipa, jpa, pa, s, s0, bcf, work1, rhsnamult, 
     &                 scalena)

               end if
               
               !-- Subroutine for comparing a Frechet derivative
               !-- approximation of A*v to preconditioner M*v, where
               !-- v is a random vector. Only to be used for debugging
               ptest = .false. ! RR: debug 
               if (ptest) then
                call prec_test(na, jdim, kdim, ndim, indx, iex, qp,
     &          xy, xyj, x, y, precon, coef2, coef4, uu, vv, ccx, ccy, 
     &          ds, press, tmet, spect, gam, turmu, fmu, vort, sndsp, s,
     &          bcf, q, qnow, qold, as, work, rhsnamult, D _hat, exprhs,
     &          a_jk, pa, ipa, jpa)
               end if
              
clb   Calculating residual due to cl term

               if (clalpha2) then
                  nscal=0
                  call clcd (jdim, kdim, q, press, x, y, xy, xyj, nscal,
     &                       cli, cdi, cmi, clv, cdv, cmv) 
                  clt = cli + clv
                  rhsstore = -(clt-clinput)*rhsnamult
               else
                  rhsstore = 0.d0
               end if          
clb
c     -- compute residual --
               if (viscous .and. turbulnt .and. itmodel .eq. 2) then
c     -- total residual --
                  
                  if (dissCon) then
                     !-- Total residual = resid
                     !-- Mean-flow residual = residM
                     !-- Turbulent residual = residT
                     call residl2(jdim,kdim,ndim,s0,5,rhsstore)
                     call Bresidl2(jdim,kdim,ndim,s0,5,rhsstore)

                     if (nki.eq.1) then
                        open(unit=384,file='resid_v_lam'
     &                       ,status='unknown',form='formatted')
                     end if
                     write(384,385)
     &                 nki,total_residT,lamDissT,total_residM,lamDiss
 385                 format(i4,2x,4(2x,e12.6))

                  else
                     call residl2(jdim,kdim,ndim,s,5,rhsstore)
                     call Bresidl2(jdim,kdim,ndim,s,5,rhsstore) 
                  end if
          
c     -- quit if residual becomes too high or nan --
                  if (resid .ge. 1.0e4) then 
                     write(scr_unit,*) 
     &               'probe.f : residual is too high or nan, exit'
                     write(scr_unit,*) 'resid',resid
                     resid = 10000.0d0
                     return
                  end if

c     -- S-A nu_tilde residual --
                  res_spl = 0.d0
                  resmax = 0.d0
                  jresmax = 0
                  kresmax = 0
                  do k = klow,kup
                    do j = jlow,jup            
                      res = s(j,k,5)**2
                      res_spl = res_spl + res
                      if (res.gt.resmax) then
                        resmax = res
                        jresmax = j
                        kresmax = k
                      endif
                    enddo
                  enddo
                  res_spl=sqrt(res_spl/dble((jup-jlow+1)*(kup-klow+1)))
                  resmax = sqrt(resmax)

                  if (thisout) write (turbhis_unit,50) numitertmp, 
     &                               res_spl,0,jresmax, kresmax, resmax
                  if (thisout) call flush(turbhis_unit)
 50               format(I7,1x,e15.8,3I6,1x,e15.8)

          
               else
c     -- density residual --
                 if (dissCon) then
                    call residl2(jdim,kdim,ndim,s0,1,rhsstore)
                    call Bresidl2(jdim,kdim,ndim,s0,1,rhsstore)

                    if (nki.eq.1) then
                       open(unit=384,file='resid_v_lam'
     &                      ,status='unknown', form='formatted')
                    end if
                    write(384,386)
     &                   nki,resid,lamDiss
 386                format(i4,2x,2(2x,e12.6))

                 else
                    call residl2(jdim,kdim,ndim,s,1,rhsstore)     
                    call Bresidl2(jdim,kdim,ndim,s,1,rhsstore)         
                 end if
          
               end if

c     output iteration number and residual

               residtmp=0.d0
               do n = 1,4
                 do k = 1,kdim
                   do j = 1,jdim
                     if (dissCon) then 
                        residtmp = residtmp + s0(j,k,n)**2
                     else
                        residtmp = residtmp + s(j,k,n)**2
                     end if
                   enddo
                 enddo
               enddo
               residtmp = sqrt( residtmp / dble(jdim*kdim) )

               residtmp = DSQRT(residtmp**2+res_spl**2) 
        
               !-- Check convergence
               !-- Need to allow a couple of NK iterations
               if (unsted) then
                 if (resid.lt.SIMIN_RES .and. nki .gt.2) goto 100
               else if (.not.unsted) then
                 if (resid.lt.MIN_RES .and. nki .gt.2) goto 100
               end if

               if (dissCon) then

                  !-- Store the initial value of the residual norm
                  if (nki.lt.3) then
                     resid0  = 1.0 ! Total residual
                     if (viscous.and.turbulnt.and.itmodel.eq.2) then
                        residM0 = 1.0 ! Mean-flow residual
                        residT0 = 1.0 ! Turbulent residual
                     end if
                  end if

                  if (nki.eq.3) then
                     resid0  = resid ! Total residual
                     if (viscous.and.turbulnt.and.itmodel.eq.2) then
                        residM0 = residM ! Mean-flow residual
                        residT0 = residT ! Turbulent residual
                     end if
                  end if

                  !-- If dissipation-based continuation is to be used 
                  !-- as a globalization method for N-K, then evaluate 
                  !-- modified residual
                  write(dbc_unit,*)'NK iter:', nki
                  write(dbc_unit,*)'lamDiss=',lamDiss
                  if (viscous.and.turbulnt.and.itmodel.eq.2) then
                     write(dbc_unit,*)'lamDissT=',lamDissT
                  end if

                  !-- Calculate L2-norm of modified residual
                  if (viscous .and. turbulnt .and. itmodel .eq. 2) then

                     !-- Total residual = residF
                     !-- Mean-flow residual = residFM
                     !-- Turbulent residual = residFT
                     call residFl2(jdim,kdim,ndim,s,5,rhsstore)
                     call BresidFl2(jdim,kdim,ndim,s,5,rhsstore)

                  else

                     !-- Density residual 
                     call residFl2(jdim,kdim,ndim,s,1,rhsstore)
                     call BresidFl2(jdim,kdim,ndim,s,1,rhsstore)
          
                  end if

                  !-- If this is the second N-K iteration or the 
                  !-- continuation parameter 'lamDiss' has been changed
                  !-- then store the residual norm for reference

                  if (nki.eq.1) then
                     if (viscous.and.turbulnt.and.itmodel.eq.2) then
                        total_residFM0 = 1.0
                        total_residFT0 = 1.0
                        residFM0 = 1.0
                        residFT0 = 1.0
                        lamDiss_old = lamDiss
                        lamDissT_old = lamDissT
                     else
                        residF0  = 1.0
                        lamDiss_old = lamDiss
                     endif
                  end if

                  if ((nki.eq.2).or.(lamDiss.ne.lamDiss_old)) then 
                     if (viscous.and.turbulnt.and.itmodel.eq.2) then
                        residFM0 = residFM
                        total_residFM0 = total_residFM
                     else
                        residF0 = residF
                     endif
                  end if

                  if (viscous.and.turbulnt.and.itmodel.eq.2) then
                     if ((nki.eq.2).or.(lamDissT.ne.lamDissT_old)) then
                        residFT0 = residFT
                        total_residFT0 = total_residFT
                     end if
                  end if

               end if

               !-- If pseudo-transient continuation is to be used as a
               !-- globalization method for N-K, then construct a
               !-- residual-based time-step vector for implicit Euler
               !-- time marching
               !-------------------------------
               !---- time step vector (I/dt) --
               !-------------------------------

               do i=1,na
                 dtvec(i) = 0.0d0
               end do
        
               !-- Don't use pseudo transient continuation if 
               !-- dissipation based continuation is being used

               if ( afiters.eq.0 .and. .not.frechet .and.  
     &         ( (.not.unsted .and. residtmp.gt.af_minr) .or. 
     &         (unsted .and. residtmp.gt.1.d-2) )
     &         .and.(.not.dissCon)) then

                 call timevec (jdim,kdim,ndim,na,xyj,indx,qp,s,ipa,jpa,
     &                         pa,nki,residtmp,rhsstore,dtvec)

                 if (prec_mat) then
                   call apldia(na,0,pa,jpa,ipa,dtvec,pa,jpa,ipa,dttmp) 
                 end if
                 if (jac_mat) then
                   call apldia (na,0,as,ja,ia,dtvec,as,ja,ia,dttmp)
                 end if

               end if                                
             
               !-- Copy rhs 
               do n = 1,ndim
                 do k = 1,kend
                   do j = 1,jend
                     jk = (indx(j,k) - 1)*ndim + n
                     rhs(jk) = s(j,k,n)
                   end do
                 end do
               end do 
             
clb   -- adding additional residual term if angle of attack can vary
               if (clalpha2) then
                 rhs(na)=rhsstore
               end if

c     -- ILU preconditioner --
               if ( prec_mat ) call prec_ilu (jdim, kdim, ndim, nk_ilu, 
     &         nk_lfil,out_unit,ipa,jpa,pa,alu,jlu,ju,levs,jw,work1)

c     -- GMRES --
               ipar(1) = 0
               ipar(2) = 2
               ipar(3) = 1
               ipar(4) = iwk_gmres
               ipar(5) = nk_imgmr
               ipar(6) = nk_itgmr
               
               fpar(2) = 1.d-14
               fpar(11) = 0.0
               
c     -- inner iterations convergence tolerance -- 

               if (.not. unsted) then

                  if (afiters.eq.0) then

                     if (viscous .and. turbulnt .and. itmodel.eq.2) then
                        if (nki.le.20) then
                           fpar(1) = tol_gmres1
                        else
                           fpar(1) = tol_gmres2
                        end if
                     else  !laminar
                        if (nki.le.2) then
                           fpar(1) = tol_gmres1
                        else
                           fpar(1) = tol_gmres2
                        end if
                     end if

                  else  !pure Newton after AF

                     if (clalpha2) then
                        fpar(1) = 1.d-2
                     elseif (nki.lt.10) then
                        fpar(1) = 1.d-1
                     else
                        fpar(1) = 1.d-1
                     end if  

                  end if  !afiters.eq.0

               else   !it's unsteady

                  if (viscous .and. turbulnt .and. itmodel.eq.2) then
                     fpar(1) = 1.d-2
                  else  !laminar 
                     fpar(1) = 1.d-1
                  end if

               end if

cmpr
c     this part is added in order to check preconditioner or Jacobian 
c     problems. This is a debugging tool, so use it only if you have a 
c     bad feeling about the preconditioner, such as slow convergence of 
c     linear system.

               if (frechet) then   
                  call fr_test(jdim, kdim, ndim, indx, iex, q, qp, 
     &              qold, xy, xyj, x, y, precon, coef2, coef4, uu, vv,
     &              ccx, ccy, ds, press, tmet, spect, gam, turmu, fmu,
     &              vort, sndsp, s, bcf,as,ja,ia,pa,jpa,ipa,framux_o1, 
     &              rhsnamult,D_hat,exprhs, a_jk, its_nk_val,dtvec)
               end if
               
               iout = 0
               if (opt_meth.eq.1) iout = out_unit

               call run_gmres (jdim, kdim, ndim, indx, iex, q, qp, 
     &              qold, xy, xyj, x, y, precon, coef2, coef4, uu, vv,
     &              ccx, ccy, ds, press, tmet, spect, gam, turmu, fmu,
     &              vort, sndsp, s, bcf, ifirst, rhs, sol, as, ja, ia, 
     &              alu, jlu, ju, work1(1,1), work1(1,6), wk_gmres, 
     &              work1(1,11), 0, iout, framux_o1, rhsnamult, D_hat,
     &              exprhs, a_jk, its_nk_val,dtvec)

c     -- update solution --
               do n = 1,ndim     
                  do k = kbegin,kend
                     do j = jbegin,jend
                        jk = ( indx(j,k)-1)*ndim + n
                        qp(j,k,n) = qp(j,k,n) + sol(jk)
                     end do
                  end do
               end do

c     -- clip negative nutilde values --
               if (viscous .and. turbulnt .and. itmodel.eq.2) then
                  do k = kbegin,kend
                     do j = jbegin,jend
                        if (qp(j,k,5).lt.0.0d0 .and. resid .gt. 1.d-6) 
     &                       then
                           if (j.le.jtail2.and.j.ge.jtail1.and.k.eq.1) 
     &                          then
                              qp(j,k,5) = 1.0d-14/xyj(j,k)
                           else
                              qp(j,k,5) = 1.0d0/xyj(j,k)
                           end if
                        end if
                     end do
                  end do
               end if

clb     Update angle of attack from solution (when using variable alpha)
               if (clalpha2) then
                  delalp=sol(na)/1.d2
cmpr              Freeze angle at the begin for poor initial guesses
                  if(afiters.eq.0 .and. nki.lt.75 .and. 
     &               residtmp.gt.1.d-3) delalp=0.d0

cmpr              Prevent huge changes in alpha
                  if (abs(delalp).gt.1.d0) then
                     if (delalp.ge.0.d0) then
                        delalp=1.d0
                     else
                        delalp=-1.d0
                     end if
                  end if

                  alpha = alpha + delalp                           
                  
                  uinf = fsmach*cos(alpha*pi/180.d0)
                  vinf = fsmach*sin(alpha*pi/180.d0)
              
                  write (out_unit,*)
     &          '----------------------------------------------------'
                  write (out_unit,*)
     &          '|                 NK                               |'
                  write (out_unit,11) numiter,alpha
 11          format (1x,'| numiter = ',i6,' new alpha = ',f12.8,8x,'|')
                  write (out_unit,*)
     &          '|                                                  |'
                  write (out_unit,*)
     &          '----------------------------------------------------'
               end if

c     -- update pressure --
               call calcps ( jdim, kdim, qp, press, sndsp, precon, 
     &              xy, xyj)

c     -- update laminar viscosity --
               if (viscous) call fmun(jdim, kdim, qp, press, fmu, sndsp)
               
c     -- timing results (no I/O) --
c     -- totime1: user time --
c     -- totime2: user + system time --
               t=etime(tarray)
               time1=tarray(1)-time1
               time2=tarray(2)-time2
               totime1=totime1 + time1
               totime2=totime2 + time1 + time2
              
c     -- restore csr pointer arrays -- 
               do j=1,na+1
                  ia(j) = iatmp(j)
                  ipa(j) = ipatmp(j) 
               end do
               do j = 1,nas
                  ja(j) = jatmp(j)
               end do
               do j = 1,nap
                  jpa(j) = jpatmp(j) 
               end do   
               
               if (unsted) then
                  jsubit=jsubit+1
               endif

               if (.not. unsted) then

c     -- aerodynamic forces   note: pressure is already updated --     
                  nscal=0
                  call clcd (jdim, kdim, qp, press, x, y, xy, xyj, 
     &                 0, cli, cdi, cmi, clv, cdv, cmv)
                  clt = cli + clv
                  cdt = cdi + cdv
                  cmt = cmi + cmv  

c                 -- output to .his file: resid, max resid, cl, cd... --

                  ip = 19
                  call ioall(ip, junit, jdim, kdim, qp, qp, press,sndsp,
     &                 turmu,fmu, vort, xy, xyj, x, y)

                  hisoutput = .true.

                  ! RR: output flow residual to GNUplot data file
                  if (gnuplot_res) then
                     write(gnuplot_res_unit,*) numiter, resid, '   NK'
                  end if

               end if

               !-- Dissipation-based continuation:
               !-- 
               !-- If modified residual has been reduced by the 
               !-- prescribed amount, 'dcTol', or if max number of
               !-- iterations is reached, update continuation
               !-- parameter 'lamDiss'.
               
               if (dissCon) then

                  if (viscous .and. turbulnt .and. itmodel.eq.2) then

                     !-- Increment counter of iterations at current 
                     !--  value of lamDiss
                     dc_its = dc_its + 1
                     dcT_its = dcT_its + 1

                     !-- Store current value of the contination 
                     !-- parameter before update
                     lamDiss_old = lamDiss 
                     lamDissT_old = lamDissT
                     write(dbc_unit,*)'total_residM =', total_residM
                     write(dbc_unit,*)'residM =', residM
                     write(dbc_unit,*)'BresidM =', BresidM
                     write(dbc_unit,*)'residM0 =', residM0

                     write(dbc_unit,*)'total_residT =', total_residT
                     write(dbc_unit,*)'residT =', residT
                     write(dbc_unit,*)'BresidT =', BresidT
                     write(dbc_unit,*)'residT0 =', residT0

                     lamDissR = 0.9*(residM/residM0)**2
                     lamDissTR = 0.9*(residT/residT0)**2

                     write(dbc_unit,*)'lamDissR =', lamDissR
                     write(dbc_unit,*)'lamDissTR =', lamDissTR

                     lamDiss2 = lamDiss/2                     
                     lamDissT2 = lamDissT/2
                     write(dbc_unit,*)'lamDiss2 =', lamDiss2
                     write(dbc_unit,*)'lamDissT2 =', lamDissT2

                     write(dbc_unit,*)'total_residFM =', total_residFM
                     write(dbc_unit,*)'total_residFM0 =', total_residFM0
                     write(dbc_unit,*)'residFM =', residFM
                     write(dbc_unit,*)'BresidFM =', BresidFM
                     write(dbc_unit,*)'residFM0 =', residFM0
                     rFM_ratio = total_residFM/total_residFM0
                     write(dbc_unit,23) rFM_ratio
 23                  format(1x,'rFM_ratio =', f10.4)

                     write(dbc_unit,*)'total_residFT =', total_residFT
                     write(dbc_unit,*)'total_residFT0 =', total_residFT0
                     write(dbc_unit,*)'residFT =', residFT
                     write(dbc_unit,*)'BresidFT =', BresidFT
                     write(dbc_unit,*)'residFT0 =', residFT0
                     rFT_ratio = total_residFT/total_residFT0
                     write(dbc_unit,24) rFT_ratio
 24                  format(1x,'rFT_ratio =', f10.4,//)

                     !-- Reduce DBC continuation parameter for 
                     !-- mean-flow equations
                     if ((total_residM.le.lamKill)
     &                    .and.(nki.gt.10)) then
                        
                        lamDiss = 0.0

                     else if((((rFM_ratio).le.dcTol)
     &                       .or.(dc_its.ge.20)).and.(nki.gt.1)) then

                        if (lamRed.eq.0.0) then
                           lamDiss = min(lamDiss2,lamDissR)
                        else
                           lamDiss = lamRed*lamDiss
                        end if

                        !-- Reset counter                     
                        dc_its = 0                      

                     end if

                     !-- Reduce DBC continuation parameter for 
                     !-- turbulence equation
                     if ((total_residT.le.lamKill).and.(lamDiss.eq.0.0)
     &                    .and.(nki.gt.10)) then                    

                        lamDissT = 0.0
                        
                     elseif(((rFT_ratio.le.dcTol)
     &                       .or.(dcT_its.ge.20)).and.(nki.gt.1)) then

                        if (lamTRed.eq.0.0) then
                           lamDissT = min(lamDissT2,lamDissTR)
                        else
                           lamDissT = lamTRed*lamDissT
                        end if

                        if (total_residM.le.lamKill) then
                           lamDissT = 0.1*lamDissT
                        end if

                        !-- Reset counter                        
                        dcT_its = 0 
                        
                     end if

                     !if (residM.gt.lamKill) lamDissT = lamDissMaxT
                     !if (nki.lt.30) lamDissT = lamDissMaxT

                     !-- If 'lamLockstep=true' then lamDissT follows 
                     !-- lamDiss in lockstep
                     if (lamLockstep) lamDissT = lamDissMaxT*lamDiss
                  
                  else

                     !-- Increment counter of iterations at current 
                     !-- value of lamDiss
                     dc_its = dc_its + 1
                  
                     !-- Store current value of the contination 
                     !-- parameter before update
                     lamDiss_old = lamDiss                  

                     write(dbc_unit,*)'resid =', resid
                     write(dbc_unit,*)'Bresid =', Bresid
                     write(dbc_unit,*)'resid0 =', resid0

                     lamDissR = 0.9*(resid/resid0)**2

                     write(dbc_unit,*)'lamDissR =', lamDissR

                     lamDiss2 = lamDiss/2
                     write(dbc_unit,*)'lamDiss2 =', lamDiss2

                     write(dbc_unit,*)'residF =', residF
                     write(dbc_unit,*)'BresidF =', BresidF
                     write(dbc_unit,*)'residF0 =', residF0
                     rF_ratio = residF/residF0
                     write(dbc_unit,25) rf_ratio
 25                  format(1x,'rF_ratio =', f10.4,//)

                     if ((resid.le.lamKill).and.(nki.gt.5)) then
                        
                        lamDiss = 0.0
                        
                     elseif((((residF/residF0).le.dcTol)
     &                       .or.(dc_its.eq.30))
     &                       .and.(nki.gt.1)) then

                        if (lamRed.eq.0.0) then
                           lamDiss = min(lamDiss2,lamDissR)
                        else
                           lamDiss = lamRed*lamDiss
                        end if

                        !-- Reset counter
                     
                        dc_its = 0
                        
                     end if                
                  end if
                
               end if
                                         
            end do              !end of inner newton loop

            if (unsted) then
               if (resid.gt.1.d3*SIMIN_RES) write(scr_unit,*) 
     &          'Not converged', numiter, resid
c               if (resid.gt. 1000.d0*SIMIN_RES)  stop
            else
               if (resid.gt.1.0E-08) write(scr_unit,*) 
     &         'Not converged', numiter, resid
               if (resid.gt.1.0E-08)  then
                  clalpha = clalphatmp
                  do j=1,na+1
                     ia(j) = iatmp(j)
                     ipa(j) = ipatmp(j) 
                  end do
                  do j = 1,nas
                     ja(j) = jatmp(j)
                  end do
                  do j = 1,nap
                     jpa(j) = jpatmp(j) 
                  end do   
                  return
               end if
            end if
            
 100        continue

            if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5) then
               total_subit=total_subit+jsubit-1
            endif


            if (unsted .and. (jesdirk.eq.4 .or. jesdirk.eq.3) 
     &           .and. jstage.le.5) then

               call get_rhs (jdim, kdim, ndim, qp, q, qold, xy, xyj, 
     &              x, y, precon, coef2, coef4, uu, vv, ccx, ccy, 
     &              ds, press, tmet, spect, gam, turmu, fmu, vort, 
     &              sndsp, s, s0, work1, D_hat, exprhs, a_jk,.true.)

c---- save stage flow field every noutevery and nouteverybig iterations
               if ((outtime .or. gradient .eq. 1) .and. numiter.le.   
     &            iend+nk_skip.and.(mod(numiter-iend,nouteverybig).eq.0)
     &            .and.(opt_meth.eq.2 .or. opt_meth.eq.7)   )  then 
   
                  call iomarkusstage(1,jstage,(numiter-iend)/
     &            nouteverybig,jdim,kdim,q,xyj,turmu)

               endif

               if ((outtime .or. gradient .eq. 1) .and. numiter.gt.iend+
     &            nk_skip.and.(mod(numiter-iend-nk_skip,noutevery).eq.0)
     &            .and. (opt_meth.eq.2 .or. opt_meth.eq.7)   )  then 
   
                  call iomarkusstage(1,jstage,(numiter-iend-nk_skip)/
     &            noutevery+nk_skip/nouteverybig,jdim,kdim,q,xyj,turmu)
       
               endif

            end if

71      continue   !jstage
         
csi     update values
         call nk_postproc(jdim,kdim,ndim,qp,q,qold)

c     -- aerodynamic forces   note: pressure is already updated --
         nscal=0
         call clcd (jdim, kdim, q, press, x, y, xy, xyj, 
     &        0, cli, cdi, cmi, clv, cdv, cmv)                     
         clt = cli + clv
         cdt = cdi + cdv
         cmt = cmi + cmv  
 
c     -- output to .his file: resid, max resid, cl, cd... --  
         if (unsted .or. (.not. hisoutput)) then   
            ip = 19
            call ioall(ip, junit, jdim, kdim, q, q, press, sndsp, turmu,
     &           fmu, vort, xy, xyj, x, y)
         end if

c     -- update turbulent viscosity as well as turre and turold--
         if (viscous .and. turbulnt .and. itmodel.eq.2) then
            do j = jbegin,jend
               do k = kbegin,kend          
                  turre(j,k) = q(j,k,5)*xyj(j,k)
                  turold(j,k) = qold(j,k,5)*xyj(j,k)
               end do
            end do
            
            do k = kbegin,kup
               kp1 = k+1
               do j = jbegin,jend
                  rn = turre(j,k)*q(j,k,1)*xyj(j,k)
                  chi = rn/fmu(j,k)
                  chi3 = chi**3
                  fv1 = chi3/(chi3+cv1_3)
                  tur1 = fv1*rn
                  
                  rn = turre(j,kp1)*q(j,kp1,1)*xyj(j,kp1)
                  chi = rn/fmu(j,kp1)
                  chi3 = chi**3
                  fv1 = chi3/(chi3+cv1_3)
                  tur2 = fv1*rn
                  
                  turmu(j,k) = 0.5d0*(tur1+tur2)
               end do
            end do
         end if 

               
c---- save target flow field every noutevery iterations --

         if ( unsted .and. outtime .and. opt_meth .eq. 1  
     &        .and. numiter.gt.iend+nk_skip.and.     
     &         mod(numiter-iend-nk_skip,noutevery).eq.0) then

            call iomarkus(2,(numiter-iend-nk_skip)/noutevery+nk_skip/
     &           nouteverybig,jdim,kdim,q,xyj,turmu)
       
         endif


c---- save flow field every noutevery and nouteverybig iterations ----

         if (unsted .and.(outtime .or. gradient .eq. 1) .and. numiter   
     &       .le.iend+nk_skip .and.(mod(numiter-iend,nouteverybig).eq.0) 
     &         .and. (opt_meth .eq. 2 .or. opt_meth .eq. 7)     )  then 
   
            call iomarkus(1,(numiter-iend)/nouteverybig,
     &           jdim,kdim,q,xyj,turmu)

         endif

         if (unsted .and.(outtime .or. gradient .eq. 1) .and. numiter   
     &   .gt.iend+nk_skip.and.(mod(numiter-iend-nk_skip,noutevery).eq.0) 
     &   .and. (opt_meth .eq. 2 .or. opt_meth .eq. 7)     )  then 
   
            call iomarkus(1,(numiter-iend-nk_skip)/noutevery+nk_skip/
     &           nouteverybig,jdim,kdim,q,xyj,turmu)
       
         endif
               
         do j = 1,5
            if (ldout) backspace (ld_unit)
         end do

cmpr         jesdirk = imarchtemp
cmpr         if (jesdirk==1) jstagemx=1


 10   continue                  !end of outer iterations


c     -- restore csr pointer arrays -- 
      do j=1,na+1
         ia(j) = iatmp(j)
         ipa(j) = ipatmp(j) 
      end do
      do j = 1,nas
         ja(j) = jatmp(j)
      end do
      do j = 1,nap
         jpa(j) = jpatmp(j) 
      end do   
  
      clalpha = clalphatmp





c     -- frozen dissipation is used to verify accuracy of gradients --
      if ( coef_frz .and. .not. frozen) then

        goto (300, 400, 500) ifrz_what

 300    continue

        write (scr_unit,310)
 310    format (3x,'PROBE: Frozen absolute values!',/3x,
     &       'This includes the contravariant velocities in dissipation'
     &       ,/3x,'and for Spalart model also vorticity!!')        

        ix = 1                                                  
        iy = 2                                                  
        call eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,tmet,uu,ccx)  

        do k = 1,kend
          do j = 1,jend
            suuf(j,k) = uu(j,k)
          end do
        end do

        ix = 3                                                  
        iy = 4                                                  
        call eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,tmet,vv,ccy)

        do k = 1,kend
          do j = 1,jend
            svvf(j,k) = vv(j,k)
          end do
        end do

        do k=kbegin,kend
          do j=jbegin,jend
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
          
            spect(j,k,1)=rho
            spect(j,k,2)=u
            spect(j,k,3)=v
          end do
        end do
      
c     -- calculate vorticity at node j,k --
        call vort_o2 (jdim, kdim, spect, xy, vort)

        do k = 1,kend
          do j = 1,jend
            vortf(j,k) = vort(j,k)
          end do
        end do
        
        goto 1000

 400    continue

        write (scr_unit,410)
 410    format (3x,'PROBE: Frozen pressure switch!')

        ixy = 1                                                 
        call gradcoef (ixy,jdim,kdim,press,xyj,coef2,work1)     
        do k = 1,kend
          do j = 1,jend
            c2xf(j,k) = coef2(j,k)
          end do
        end do

        ixy = 2                                                 
        call gradcoef (ixy,jdim,kdim,press,xyj,coef2,work1)     
        do k = 1,kend
          do j = 1,jend
            c2yf(j,k) = coef2(j,k)
          end do
        end do

        goto 1000

 500    continue

        write (scr_unit,510)
 510    format (3x,'PROBE: Frozen pressure switch and absolute values!',
     &       /3x,'This includes the contravariant velocities',/3x,
     &       'in dissipation',/3x, 
     &       'and for Spalart model also vorticity!')

        ix = 1                                                  
        iy = 2                                                  
        call eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,tmet,uu,ccx)  

        do k = 1,kend
          do j = 1,jend
            suuf(j,k) = uu(j,k)
          end do
        end do

        ixy = 1                                                 
        call gradcoef (ixy,jdim,kdim,press,xyj,coef2,work1)     
        do k = 1,kend
          do j = 1,jend
            c2xf(j,k) = coef2(j,k)
          end do
        end do

        ix = 3                                                  
        iy = 4                                                  
        call eigval(ix,iy,jdim,kdim,q,press,sndsp,xyj,xy,tmet,vv,ccy)

        do k = 1,kend
          do j = 1,jend
            svvf(j,k) = vv(j,k)
          end do
        end do

        ixy = 2                                                 
        call gradcoef (ixy,jdim,kdim,press,xyj,coef2,work1)     
        do k = 1,kend
          do j = 1,jend
            c2yf(j,k) = coef2(j,k)
          end do
        end do

        do k=kbegin,kend
          do j=jbegin,jend
            rho = q(j,k,1)*xyj(j,k)
            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
          
            spect(j,k,1)=rho
            spect(j,k,2)=u
            spect(j,k,3)=v
          end do
        end do

c     -- calculate vorticity at node j,k --
        call vort_o2 (jdim, kdim, spect, xy, vort)

        do k = 1,kend
          do j = 1,jend
            vortf(j,k) = vort(j,k)
          end do
        end do

c        call coef24y (jdim,kdim,coef2,coef4,work1,work1(1,2),
c     &        work1(1,5),work1(1,6))
c
c        do k = 1,kend
c          do j = 1,jend
c            c2yf(j,k) = coef2(j,k)
c            c4yf(j,k) = coef4(j,k) 
c          end do
c        end do



c        do k = 1,kend
c          do j = 1,jend
c            turmuf(j,k) = turmu(j,k)
c          end do
c        end do
c
c        do k = 1,kend
c          do j = 1,jend
c            fmuf(j,k) = fmu(j,k)
c          end do
c        end do
c
c     do k = kbegin,kend
c     do j = jbegin,jend
c     rn = q(j,k,5)*q(j,k,1)*xyj(j,k)**2
c     chi = rn/fmu(j,k)
c     chi3 = chi**3
c     fv1 = chi3/(chi3+cv1_3)
c     
c     turmuf(j,k) = fv1
c     end do
c     end do
c
c        reinv = 1.d0/re
c        resiginv= sigmainv/re
c
c        do k = klow,kup
c          do j = jlow,jup
cc     -- nu_tilde --
c            tnu = q(j,k,5)*xyj(j,k)
cc     -- chi = = tnu*rho/mul --
c            chi = tnu*q(j,k,1)*xyj(j,k)/fmu(j,k)
cc     -- chi**3 --
c            chi3 = chi*chi*chi
c            fv1 = chi3/(chi3+cv1_3)
c            fv2 = 1.d0 - chi/(1+chi*fv1)
cc     -- t1 = 1/(k**2.d**2)
c            t1 = 1.d0/(akarman*smin(j,k))**2
cc     -- S_tilde --
c            ts = vort(j,k)*re + tnu*t1*fv2
cc     if (j.eq.110 .and. k.eq.4) write (*,*) 'ts_rsa',ts 
c            r = tnu/ts*t1
c
c            if (dabs(r).ge.10.d0) then
c              fw = const
c            else
c              g = r + cw2*(r**6-r)
c              fw = g*( (1.0+cw3_6)/(g**6+cw3_6) )**expon
c            endif
c
c            rinv = 1.d0/xyj(j,k)
cc     -- production term --
c            pr = rinv*cb1*reinv*ts*tnu
cc     -- destruction term --
c            t1 = tnu/smin(j,k)
c            dsa = rinv*cw1*reinv*fw*t1*t1
c
cc     -- S-A residual: storing -R(Q) for Newton's method --
c            sapdf(j,k) =  pr - dsa
c          end do
c        end do
c
c        do k = kbegin,kend
c          do j = jbegin,jend
c            tnu = q(j,k,5)*xyj(j,k)
c            spect(j,k,1) = fmu(j,k)/(q(j,k,1)*xyj(j,k)) + tnu
c          end do
c        end do
c
cc     -- diffusion terms in xi direction --
c        do k = klow,kup
c          do j = jlow,jup
c            jp1 = j+1
c            jm1 = j-1
c
c            xy1p = 0.5d0*( xy(j,k,1) + xy(jp1,k,1) )
c            xy2p = 0.5d0*( xy(j,k,2) + xy(jp1,k,2) )
c            ttp  = ( xy1p*xy(j,k,1) + xy2p*xy(j,k,2) )
c            
c            xy1m = 0.5d0*( xy(j,k,1) + xy(jm1,k,1) )
c            xy2m = 0.5d0*( xy(j,k,2) + xy(jm1,k,2) )
c            ttm  = ( xy1m*xy(j,k,1) + xy2m*xy(j,k,2) )
c            
c            rinv = 1.d0/xyj(j,k)
c
c            cnud = cb2*resiginv*spect(j,k,1)*rinv
c            
c            cdp       =    ttp*cnud
c            cdm       =    ttm*cnud
c
c            trem = 0.5d0*( spect(jm1,k,1) + spect(j,k,1) )
c            trep = 0.5d0*( spect(j,k,1) + spect(jp1,k,1) )
c
c            cap = ttp*trep*(1.0+cb2)*resiginv*rinv
c            cam = ttm*trem*(1.0+cb2)*resiginv*rinv
c
c            tnum = q(jm1,k,5)*xyj(jm1,k)
c            tnu  = q(j,k,5)*xyj(j,k)
c            tnup = q(jp1,k,5)*xyj(jp1,k)
c            
c            sadx(j,k) = cap*( tnup - tnu ) - cam*( tnu -
c     &            tnum ) - cdp*( tnup - tnu ) + cdm*( tnu - tnum )
c          end do
c        end do   
c
cc     -- diffusion terms in eta direction --
c        do k = klow,kup
c          kp1 = k+1
c          km1 = k-1
c          do j = jlow,jup
c            xy3p = 0.5d0*( xy(j,k,3) + xy(j,kp1,3) )
c            xy4p = 0.5d0*( xy(j,k,4) + xy(j,kp1,4) )
c            ttp  = ( xy3p*xy(j,k,3) + xy4p*xy(j,k,4) )
c            
c            xy3m = 0.5d0*( xy(j,k,3) + xy(j,km1,3) )
c            xy4m = 0.5d0*( xy(j,k,4) + xy(j,km1,4) )
c            ttm  = ( xy3m*xy(j,k,3) + xy4m*xy(j,k,4) )
c
c            rinv = 1.d0/xyj(j,k)
c
c            cnud = cb2*resiginv*spect(j,k,1)*rinv
c            
c            cdp = ttp*cnud
c            cdm = ttm*cnud
c            
c            trem = 0.5d0*(spect(j,km1,1)+spect(j,k,1))
c            trep = 0.5d0*(spect(j,k,1)+spect(j,kp1,1))
c            
c            cap  =  ttp*trep*(1.0+cb2)*resiginv*rinv
c            cam  =  ttm*trem*(1.0+cb2)*resiginv*rinv
c            
c            tnum = q(j,km1,5)*xyj(j,km1)
c            tnu  = q(j,k,5)*xyj(j,k)
c            tnup = q(j,kp1,5)*xyj(j,kp1)
c            
c            sady(j,k) = cap*( tnup - tnu ) - cam*( tnu -
c     &            tnum ) - cdp*( tnup - tnu ) + cdm*( tnu - tnum )
c          end do
c        end do
 1000   continue
      end if

cmpr  Deallocate the big arrays
      deallocate(levs,jw,wk_gmres)

      return
      end                       ! probe

      subroutine checkd( ib, jmax, kmax, nmax, indx, ipa, jpa, pa, js,
     &     je, ks, ke)

      dimension indx(jmax,kmax), ipa(*), jpa(*), pa(*)

      do n=1,nmax 
         do k=ks,ke
            do j=js,jmax
            
               ir = ( indx(j,k) - 1 )*nmax + n
               ic1 = ipa(ir)
               ic2 = ipa(ir+1)-1
               idiag = 0
               do m = ic1,ic2
                  if ( jpa(m).eq.ir ) then
                     idiag = m
                  end if
               end do

               if (idiag.ne.0) then
                  if ( abs(pa(idiag)) .lt. 1.e-10 ) then
                     write (scr_unit,*) 'zero on diagonal'
                     write (scr_unit,101) j,k,n,ir,idiag
                  end if
               else
                  write (scr_unit,*) 'idiag=0??'
                  write (scr_unit,*) j,k,n,ir,idiag
               end if

            end do
         end do
      end do

 101  format(4i7)

      return 
      end                       !checkd

