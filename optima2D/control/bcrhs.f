c----------------------------------------------------------------------
c     -- inviscid residuals at boundary nodes --
c     -- includes body and farfield residual --      
c     -- based on probe (a. pueyo: impfari.f impbodyi.f) --
c     -- m. nemec,  may 2000 --
c----------------------------------------------------------------------
      subroutine bcrhs ( jdim, kdim, x, y, xy, xyj, q, s, s0, press )

      use disscon_vars
#include "../include/arcom.inc"

      double precision hinf, xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim), vninf, vtinf
      double precision q(jdim,kdim,4), s(jdim,kdim,4), press(jdim,kdim)
      double precision s0(jdim,kdim,4)
      alphar = alpha*pi/180.d0     
      hstfs  = 1.d0/gami + 0.5d0*fsmach**2.d0

c     -----------------------------------------------------------------
c     -- outer-boundary  k = kmax --
c     -----------------------------------------------------------------

      if(.not.periodic) then
         jsta=jbegin+1
         jsto=jend-1
      else
         jsta=jbegin
         jsto=jend
      end if

      do j = jsta,jsto

      k = kend
c     -- metrics terms --
        par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
        xy3 = xy(j,kend,3)/par
        xy4 = xy(j,kend,4)/par
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) - 0.5d0*(q(j,k,2)**2 + q(j,k,3)**2)/
     &        q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn1 = xy3*u + xy4*v
        vt1 = xy4*u - xy3*v
        a1  = a
        rho1= rho
        pr1 = pr

c     -- reset free stream values with circulation correction --
        if (circul) then
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = dsqrt(af2)
        else
          uf = uinf
          vf = vinf
          af = dsqrt(gamma*pinf/rhoinf)
        endif
c     
        vninf = xy3*uf + xy4*vf
        vtinf = xy4*uf - xy3*vf
        ainf = af

c     -- k=kend-1 --
        k = kend-1
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 +
     &        q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = sqrt(gamma*pr/rho)                    
        vn2 = xy3*u + xy4*v
        vt2 = xy4*u - xy3*v
        a2  = a
        rho2= rho
        pr2 = pr

        k = kend
        if (dissCon) then
           s0(j,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s0(j,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
           s(j,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s(j,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
        else
           s(j,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s(j,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
        end if

c     -- inflow --
        if (vn1.le.0.d0) then
           if (dissCon) then
              s0(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s0(j,k,4) = vtinf - vt1
              s(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s(j,k,4) = vtinf - vt1
           else
              s(j,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s(j,k,4) = vtinf - vt1
           end if

c     -- outflow --
        else
           if (dissCon) then
              s0(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s0(j,k,4) = vt2 - vt1
              s(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s(j,k,4) = vt2 - vt1
           else
              s(j,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s(j,k,4) = vt2 - vt1
           end if              
        endif
      end do

      if(.not.periodic) then
c     -----------------------------------------------------------------
c     -- inflow boundary  j = 1 --
c     -----------------------------------------------------------------
      do k = kbegin,kend

c     -- metrics terms --
        par = dsqrt(xy(jbegin,k,1)**2+(xy(jbegin,k,2)**2))
        xy1 = xy(jbegin,k,1)/par
        xy2 = xy(jbegin,k,2)/par

        j = 1
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) - 0.5d0*(q(j,k,2)**2 +
     &        q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn1 = xy1*u + xy2*v
        vt1 =-xy2*u + xy1*v
        a1  = a
        rho1= rho
        pr1 = pr
c     -- reset free stream values with circulation correction --
        if (circul) then
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = dsqrt(af2)        
        else
          uf = uinf
          vf = vinf
          af = dsqrt(gamma*pinf/rhoinf)
        endif
        vninf = xy1*uf + xy2*vf
        vtinf =-xy2*uf + xy1*vf
        ainf = af

        j = 2
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 +
     &        q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = sqrt(gamma*pr/rho)                    
        vn2 = xy1*u + xy2*v
        vt2 =-xy2*u + xy1*v
        a2  = a
        rho2= rho
        pr2 = pr

        j = jbegin
        if (dissCon) then
           s0(jbegin,k,1) = vninf+2.d0*ainf/gami - (vn1+2.d0*a1/gami)
           s0(jbegin,k,2) = vn2-2.d0*a2/gami - (vn1-2.d0*a1/gami)
           s(jbegin,k,1) = vninf+2.d0*ainf/gami - (vn1+2.d0*a1/gami)
           s(jbegin,k,2) = vn2-2.d0*a2/gami - (vn1-2.d0*a1/gami)
        else
           s(jbegin,k,1) = vninf+2.d0*ainf/gami - (vn1+2.d0*a1/gami)
           s(jbegin,k,2) = vn2-2.d0*a2/gami - (vn1-2.d0*a1/gami)
        end if
c     -- inflow --
c        if (k.eq.1) write (*,*) s(jbegin,k,1)
        if (-vn1.le.0.d0) then
           if (dissCon) then
              s0(jbegin,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s0(jbegin,k,4) = vtinf - vt1
              s(jbegin,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s(jbegin,k,4) = vtinf - vt1
           else
              s(jbegin,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s(jbegin,k,4) = vtinf - vt1
           end if
c     -- outflow --
        else
           if (dissCon) then
              s0(jbegin,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s0(jbegin,k,4) = vt2 - vt1
              s(jbegin,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s(jbegin,k,4) = vt2 - vt1
           else
              s(jbegin,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s(jbegin,k,4) = vt2 - vt1
           end if
        endif
      end do

c     -----------------------------------------------------------------
c     -- outflow boundary  j = jend --
c     -----------------------------------------------------------------
      do k = kbegin,kend

c     -- metrics terms --
        par = dsqrt(xy(jend,k,1)**2+(xy(jend,k,2)**2))
        xy1 = xy(jend,k,1)/par
        xy2 = xy(jend,k,2)/par

        j = jend
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) - 0.5d0*(q(j,k,2)**2 +
     &        q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn1 = xy1*u + xy2*v
        vt1 =-xy2*u + xy1*v
        a1  = a
        rho1= rho
        pr1 = pr
c     -- reset free stream values with circulation correction --
        if (circul) then
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = dsqrt(af2)        
        else
          uf = uinf
          vf = vinf
          af = dsqrt(gamma*pinf/rhoinf)
        endif
        vninf = xy1*uf + xy2*vf
        vtinf =-xy2*uf + xy1*vf
        ainf = af

        j = jend-1
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = gami*(q(j,k,4) -                                   
     >        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        a   = sqrt(gamma*pr/rho)                    
        vn2 = xy1*u + xy2*v
        vt2 =-xy2*u + xy1*v
        a2  = a
        rho2= rho
        pr2 = pr
        
        j = jend
        if (dissCon) then
           s0(jend,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s0(jend,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
           s(jend,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s(jend,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
        else
           s(jend,k,1) = vninf-2.d0*ainf/gami - (vn1-2.d0*a1/gami)
           s(jend,k,2) = vn2+2.d0*a2/gami - (vn1+2.d0*a1/gami)
        end if
c     -- inflow --
        if (vn1.le.0.d0) then
           if (dissCon) then
              s0(jend,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s0(jend,k,4) = vtinf - vt1
              s(jend,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s(jend,k,4) = vtinf - vt1
           else
              s(jend,k,3) = rhoinf**gamma/pinf - rho1**gamma/pr1
              s(jend,k,4) = vtinf - vt1
           end if
c     -- outflow --
        else
           if (dissCon) then
              s0(jend,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s0(jend,k,4) = vt2 - vt1
              s(jend,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s(jend,k,4) = vt2 - vt1
           else
              s(jend,k,3) = rho2**gamma/pr2 - rho1**gamma/pr1
              s(jend,k,4) = vt2 - vt1
           end if
        endif
      end do

      end if  !not periodic

c     -----------------------------------------------------------------
c     -- airfoil body --
c     -----------------------------------------------------------------
      do j = jtail1,jtail2

c     -- metrics terms --
        par = sqrt(xy(j,1,3)**2+(xy(j,1,4)**2))
        xy3 = xy(j,1,3)/par
        xy4 = xy(j,1,4)/par

        k = 1                
        hinf = 1./gami + .5d0*fsmach**2
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        rho1= rho
        u1  = u
        v1  = v
        pr1 = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 +
     &        q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        vn1 = xy3*u + xy4*v
        vt1 = xy4*u - xy3*v

        !-- Calculate VN, and VT using freestream conditions
        vninf = xy3*uinf + xy4*vinf
        vtinf = xy4*uinf - xy3*vinf

        k = 2
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr2 = gami*(q(j,k,4) -                                   
     >        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        vt2 = xy4*u - xy3*v

        k = 3
        par = sqrt(xy(j,1,3)**2+(xy(j,1,4)**2))
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr3 = gami*(q(j,k,4) -                                   
     >        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        vt3 = xy4*u - xy3*v

        if (dissCon) then
           s0(j,1,1) = -( vn1 )
           s0(j,1,2) = -( vt1-2.d0*vt2+vt3 )
           s0(j,1,3) = -( pr1-2.d0*pr2+pr3 )
           s0(j,1,4) = 
     &       -( gamma/gami*pr1+.5d0*rho1*(u1**2+v1**2)-rho1*hinf )
           s(j,1,1) = -( vn1 + lamDiss*(vn1 - vninf))
           s(j,1,2) = -( vt1-2.d0*vt2+vt3 + lamDiss*(vt1 - vtinf))
           s(j,1,3) = -( pr1-2.d0*pr2+pr3 + lamDiss*(pr1 - pinf) )
           s(j,1,4) = 
     &       -( gamma/gami*pr1+.5d0*rho1*(u1**2+v1**2)-rho1*hinf )
        else
           s(j,1,1) = -( vn1 )
           s(j,1,2) = -( vt1-2.d0*vt2+vt3 )
           s(j,1,3) = -( pr1-2.d0*pr2+pr3 )
           s(j,1,4) = 
     &       -( gamma/gami*pr1+.5d0*rho1*(u1**2+v1**2)-rho1*hinf )
        end if

      end do

      return
      end                       !bcrhs
