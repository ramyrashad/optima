************************************************************************
      !-- Program name: geoCon
      !-- Written by: Howard Buckley
      !-- Date: March 2009
      !-- 
      !-- This subroutine evaluates the geometric constraints and the 
      !-- geometric constraint gradients wrt to the design variables
************************************************************************
      subroutine geoCon(sn_x, gCons, dgCdxs, Status, jdim, kdim, ndim, 
     &     dvs, idv, x, y, bap, bcp, bt, bknot, dvscale, 
     &     dx, dy, xsave, ysave, ifun)

#ifdef _MPI_VERSION
      use mpi
#endif

      
      !-- Declare variables

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer
     &     i, j, k, Status, jdim, kdim, ndim, idv(ibsnc),ifirst, mp,
     &     ifun, itfirst, indx(jdim,kdim), 
     &     iex(jdim,kdim,4), ia(jdim*kdim*ndim+2),
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1),
     &     iat(jdim*kdim*ndim+2),
     &     jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision
     &     dvs(nc+mpopt), sn_x(ibsnc), dvscale(ibsnc), bap(jbody,2),
     &     x(jdim,kdim), y(jdim,kdim), xsave(jdim,kdim), 
     &     ysave(jdim,kdim), bcp(nc,2), bt(jbody),
     &     bknot(jbsord+nc), tcon(ntcon), dgCdxs(maxtcon, ndv),
     &     q(jdim,kdim,ndim), xy(jdim,kdim,4), gCons(ntcon+nrtcon),
     &     xyj(jdim,kdim), xys(jdim,kdim,4), xyjs(jdim,kdim),
     &     xs(jdim,kdim), ys(jdim,kdim), dx(jdim*kdim*incr), 
     &     dy(jdim*kdim*incr), diff, grad(ibsnc), cp(jdim,kdim),
     &     fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim),
     &     as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), pat(jdim*kdim*ndim*ndim*5+1),
     &     cp_tar(jbody,2), obj0, acon

      !----------------------------------------------------------------
      
      !-- Set the Optima2D design variable array 'dvs' equal to the 
      !-- current values of the SNOPT design variables

      do i=1,ndv
         dvs(i) = sn_x(i)
      enddo

      !-- Unscale the design variables

      do i=1,ndv
         dvs(i) = dvs(i)*dvscale(i)
      enddo

      !-- Use current design variables to perturb grid --

      !-- Restore original grid --
      do k = 1,kdim
         do j = 1,jdim
            x(j,k) = xsave(j,k)
            y(j,k) = ysave(j,k)
         end do
      end do

      call regrid( -1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

      !-- Evaluate geometric constraints with perturbed grid

      if (ntcon.gt.0) then

         !-- Evaluate thickness constraints

         call CalcSNTCon(jdim,kdim,y,tcon)

      end if

      if (nrtcon.gt.0) then
         
         !-- Evaluate range thickness constraint

         call CalcSNRTCon(jdim,kdim,y)

      end if

      if (wac.gt.0.0) then
         
         !-- Evaluate area constraint

         call CalcSNACon(jdim, kdim, x, y, acon)

      end if

      !-- Store geometric constraint values in 'gCons' array

      do i = 1,ntcon

         gCons(i) = tcon(i)

      end do

      do i = ntcon+1, ntcon+nrtcon

         gCons(i) = maxrthc

      end do

      i = ntcon+nrtcon+1

      gCons(i) = acon

      !-- Evaluate constraint sensitivities wrt current
      !-- value of design variables

      call dCONdX (dgCdxs, jdim, kdim, ndim, dvs, idv, x, y, bap, 
     &            bcp, bt, bknot, xsave, ysave, dx, dy)

      !-- Write geometric constraint info to file
      !-- Only done on processor 0
#ifdef _MPI_VERSION
      if (rank.eq.0)then
#endif
         if ((ntcon.gt.0 .or. nrtcon.gt.0 .or. wac.gt.0)) then
            write (tcon_unit,720) ifun,(cty(i),i=1,ntcon), area,
     &           (crth(i),i=1,nrtcon), (crthx(i),i=1,nrtcon)
 720        format (i4, 32e16.8)   

            call flush(tcon_unit)
         endif
#ifdef _MPI_VERSION
      end if
#endif

      return

      end ! geoCon

************************************************************************
      !-- Program name: dCONdx
      !-- Written by: Howard Buckley 
      !-- Date: August 2008
      !-- 
      !-- This subroutisne evaulates the gradient of the thickness 
      !-- constraints wrt the design variables
************************************************************************

      subroutine dCONdX (dgCdxs, jdim, kdim, ndim, dvs, idv, x, y, bap,
     &             bcp, bt, bknot, xsave, ysave, dx, dy)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"


      integer 
     &     in, i, jdim, kdim, ndim, idv(nc+mpopt)

      double precision 
     &     tmp, stepsize, tconp(ntcon),
     &     tconm(ntcon), dgCdxs(maxtcon,ndv),
     &     x(jdim,kdim), y(jdim,kdim), bap(jbody,2), bcp(nc,2), 
     &     bt(jbody), dvs(ibsnc),
     &     bknot(jbsord+nc), tcon(ntcon),
     &     xsave(jdim,kdim), ysave(jdim,kdim),dx(jdim*kdim*incr),
     &     dy(jdim*kdim*incr), maxrthcp, maxrthcm, aconp, aconm

      if (ntcon.gt.0) then

         !-- Evaluate gradient of thickness constraint using 2nd order
         !-- centered differences

         do in = 1,ngdv
               
            tmp = dvs(in)
            stepsize = abs(fd_eta*dvs(in))

            !-- check for tiny stepsize --


            if ( abs(stepsize).lt.1.d-10 ) 
     &        stepsize = 1.d-10*sign(1.d0,stepsize)

            !-- Calculate CON at +ve stepsize

            dvs(in) = dvs(in) + stepsize

            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNTCon (jdim, kdim, y, tconp)

            !-- Calculate CON at -ve stepsize

            dvs(in) = tmp - stepsize
     
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)
 
            call CalcSNTCon (jdim, kdim, y, tconm)
  
            !-- restore dvs(in) and calculate 2nd order difference

            dvs(in) = tmp  

            do i=1,ntcon
               dgCdxs(i,in) = (tconp(i) - tconm(i)) / (2.d0*stepsize)

c               write(scr_unit,*)'i=',i
c               write(scr_unit,*)'in=',in
c               write(scr_unit,*)'tconp(i)=',tconp(i)
c               write(scr_unit,*)'tconm(i)=',tconm(i)
c               write(scr_unit,*)'stepsize=',stepsize
c               write(scr_unit,*)'dgCdxs(i,in)=',dgCdxs(i,in)

            enddo

         enddo

      end if

      if (nrtcon.gt.0) then

         !-- Evaluate gradient of range thickness constraint using 2nd 
         !-- order centered differences

         do in = 1,ngdv
               
            tmp = dvs(in)
            stepsize = abs(fd_eta*dvs(in))

            !-- check for tiny stepsize --

            if ( abs(stepsize).lt.1.d-10 ) 
     &        stepsize = 1.d-10*sign(1.d0,stepsize)

            !-- Calculate range thickness constraint value at +ve 
            !-- stepsize

            dvs(in) = dvs(in) + stepsize

            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNRTCon(jdim,kdim,y)

            maxrthcp = maxrthc

            !-- Calculate CON at -ve stepsize

            dvs(in) = tmp - stepsize
      
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNRTCon(jdim,kdim,y)

            maxrthcm = maxrthc

            !-- restore dvs(in) and calculate 2nd order difference

            dvs(in) = tmp  

            do i=ntcon+1,ntcon+nrtcon
               dgCdxs(i,in) = (maxrthcp - maxrthcm) / (2.d0*stepsize)

               !write(scr_unit,*)'i=',i
               !write(scr_unit,*)'in=',in
               !write(scr_unit,*)'maxrthcp=',maxrthcp
               !write(scr_unit,*)'maxrthcm=',maxrthcm
               !write(scr_unit,*)'stepsize=',stepsize
               !write(scr_unit,*)'dgCdxs(i,in)=',dgCdxs(i,in)

            enddo  

         enddo

      end if

      if (wac.gt.0.0) then

         !-- Evaluate gradient of area constraint using 2nd 
         !-- order centered differences

         do in = 1,ngdv
               
            tmp = dvs(in)
            stepsize = abs(fd_eta*dvs(in))

            !-- check for tiny stepsize --

            if ( abs(stepsize).lt.1.d-10 ) 
     &        stepsize = 1.d-10*sign(1.d0,stepsize)

            !-- Calculate range thickness constraint value at +ve 
            !-- stepsize

            dvs(in) = dvs(in) + stepsize

            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNACon(jdim, kdim, x, y, aconp)

            !-- Calculate CON at -ve stepsize

            dvs(in) = tmp - stepsize
      
            call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot, xsave, ysave, dx, dy)

            call CalcSNACon(jdim, kdim, x, y, aconm)

            !-- restore dvs(in) and calculate 2nd order difference

            dvs(in) = tmp  

            i=ntcon+nrtcon+1
            dgCdxs(i,in) = (aconp - aconm) / (2.d0*stepsize)

         enddo

      end if

      !-- dgCdxs (where gC is geometric constraint) with respect to AOA 
      !-- is zero 

      if (ndv .gt. ngdv) then
         do in = ngdv + 1, ndv
            do i=1,ntcon+nrtcon+1
               dgCdxs(i,in) = 0.d0
            enddo
         enddo
      endif

      return

      end ! dCONdx

************************************************************************
      !-- Program name: CalcSNTCon
      !-- Written by: Howard Buckley 
      !-- Date: August 2008
      !-- 
      !-- This subroutine evaulates the thickness constraints at the
      !-- current design variables
************************************************************************

      subroutine CalcSNTCon (jdim, kdim, y, tcon)

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer
     &     jdim, kdim, i

      double precision
     &     tcon(maxtcon), y(jdim,kdim)

      !-- Evaluate airfoil thickness at each thickness constraint
      !-- location

      do i = 1, ntcon

         tcon(i) = y(juptx(i),1) - y(jlotx(i),1)

         !-- Scale constraint by its bound
         if (scale_con) tcon(i) = tcon(i)/cty_tar(i)

      end do

      return
 
      end ! CalcSNTCon

************************************************************************
      !-- Program name: CalcSNRTCon
      !-- Written by: Howard Buckley 
      !-- Date: Feb 2009
      !-- 
      !-- This subroutine evaulates the range thickness constraint at the
      !-- current design variables
************************************************************************
      subroutine CalcSNRTCon(jdim,kdim,y)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer jdim,kdim
      double precision y(jdim,kdim)

      integer i
      
      maxrthc = 0.0
      maxrthctemp = 0.0

      do i=1,nrtcon

         do subi = 1, crtxn(i)+1

            maxrthctemp = y(juprtxsub(i,subi),1) - y(jlortxsub(i,subi
     &           ),1)

            if (maxrthc .lt. maxrthctemp) then

               crth(i) = maxrthctemp
               crthx(i) = crtxsub(i,subi)
               maxrthc = maxrthctemp

            end if

         end do

         !-- Scale constraint by its bound
         if (scale_con) maxrthc = maxrthc/crthtar(1)
  
      end do

      return
      end                       !CalcRTCon

************************************************************************
      !-- Program name: CalcSNACon
      !-- Written by: Howard Buckley 
      !-- Date: Nov 2009
      !-- 
      !-- This subroutine evaulates the area constraint at the
      !-- current design variables
************************************************************************

      subroutine CalcSNACon(jdim, kdim, x, y, acon)
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"

c     -- specify the variable type and dimensions--
      integer j,jdim, kdim
      double precision x(jdim,kdim), y(jdim,kdim), acon

      area = 0.0
      do j=jtail2,jtail1+1,-1
         area = area + ( x(j,1)-x(j-1,1) )*( (y(j,1)+y(j-1,1))/2 )
      end do

      acon = area/areainit

      return
      end                       !CalcSNACon


