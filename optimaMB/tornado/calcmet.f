c     -----------------------------------------------------------------
c     -- METRIC CALCULATIONS --
c     -- calling routine: xymets --
c     -----------------------------------------------------------------
      subroutine calcmet(iblk,nblks,nhalo,jdim,kdim,xy,xit,ett,xyj,  
     &     jbegin,jend,kbegin,kend)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/units.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer iblk,nblks,nhalo,jdim,kdim,jbegin,jend,kbegin,kend
      integer j,k,ii,kk
      double precision xy(jdim,kdim,4),xyj(jdim,kdim),dinv      
      double precision xit(jdim,kdim),ett(jdim,kdim)    

cmt ~~~ I prefer to initialize the xyj for each point including the corners (halo points)
cmt ~~~ so we can avoid having zeros or random numbers on the block corners
      DO k=1,kdim
         DO j=1,jdim
            xyj(j,k)=1.d0
         ENDDO
      ENDDO
cmt
c     -- form jacobian --
      do k=kbegin,kend       
         do j=jbegin,jend       
            dinv = ( xy(j,k,4) * xy(j,k,1) - xy(j,k,3) * xy(j,k,2) )
            if(dinv .le. 0.0d0) then
               if (mg .or. gseq) then
                  ii=mod(iblk,nblks)
                  kk=iblk/nblks
                  if (ii.eq.0) ii=nblks
                  if (ii.gt.0) kk=kk+1
                  write(n_out,100) kk
                  write(n_out,200) ii,j-nhalo,k-nhalo,dinv
                  stop
               else
                  write(n_out,200) iblk,j-nhalo,k-nhalo,dinv
                  write(n_out,*) jbegin,jend,kbegin,kend
                  badjac = .true.
                  goto 10
               endif
            endif
            xyj(j,k) = 1.0/dinv    
         end do
      end do
 100  format('On grid',i2)
 200  format('jacobian(iblk,j,k)=jac(',i3,',',i3,',',i3,')=',e16.8)

c     -- calculate metrics: xix=xy1, xiy=xy2, etax=xy3, etay=xy4 --
      do k=kbegin,kend       
         do j=jbegin,jend       
            dinv = xyj(j,k)        
            xy(j,k,1) =   xy(j,k,1)*dinv               
            xy(j,k,2) = - xy(j,k,2)*dinv               
            xy(j,k,3) = - xy(j,k,3)*dinv               
            xy(j,k,4) =   xy(j,k,4)*dinv               
         end do
      end do
      goto 20
      
 10   continue
      if (ioptm.lt.3) then
         write (*,*) 'BADJAC: see .out file'
         stop
      else
         write (n_out,*) 'BADJAC:',badjac
      end if
      
 20   return
      end   
