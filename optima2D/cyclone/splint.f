      subroutine splint(jdim,xa,ya,y2a,x,y)
c
c     Written by Stan De Rango 23/06/97

c     -this subroutine returns the interpolated function value, y,
c      of the splined function at location x.
c
      implicit none
      integer j,jhi,jlo,jdim
      double precision a,b,h,x,y,xa(jdim),ya(jdim),y2a(jdim)
c
      jlo=1
      jhi=jdim
 1    if (jhi-jlo.gt.1) then
         j=(jhi+jlo)/2
         if(xa(j).gt.x) then
            jhi=j
         else
            jlo=j
         endif
         goto 1
      endif
c
      h=xa(jhi)-xa(jlo)
      if (h.eq.0.d0) pause 'bad xa input in splint'
      a=(xa(jhi)-x)/h
      b=(x-xa(jlo))/h
c
      y= a*ya(jlo)+b*ya(jhi)+
     &   (a*(a**2-1.d0)*y2a(jlo) + b*(b**2-1.d0)*y2a(jhi)) * (h**2)/6.d0
      return
      end
