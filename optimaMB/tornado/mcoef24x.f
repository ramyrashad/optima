      subroutine mcoef24x(jdim,kdim,coef2x,coef4x,
     &                    jbegin,jend,kbegin,kend)

      implicit none

#include "../include/common.inc"           

      integer jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision coef2x(jdim,kdim),coef4x(jdim,kdim),c2,c4           
c                                                                       
c     coef2x comes in as coefx (pressure gradient coefficient)            
c     coef4x comes in as specx (spectral radius)                          
c                                                                       
c                                                                       
c     form 2nd and 4th order dissipation coefficients in y                 
c                                                                      
      do 20 k = kbegin,kend                                            
      do 20 j = jbegin,jend                                             
        c2=dis2x*coef2x(j,k)
        c4=dis4x-min(dis4x,c2)
        coef2x(j,k)=c2
        coef4x(j,k)=c4
 20   continue                                                       
                                                                       
      return                                                         
      end                                                            
