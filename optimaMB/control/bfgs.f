c     ------------------------------------------------------------------
c     -- minimize an unconstrained nonlinear scalar valued function of a
c     vector variable x by the bfgs algorithm --
c     -- based on: "numerical methods for unconstrained optimization
c     and nonlinear equations", dennis and schnabel, and "numerical
c     optimization", nocedal and wright --
c     -- code based on university of toronto csc course --
c     -- m. nemec, june 2001 --
c     ------------------------------------------------------------------
      subroutine bfgs(nmax,indx,q,qps,qos, xy, xyj,x,y,turmu,vort,turre,
     &     turps,turos,fmu, press, precon, sndsp, xit, ett, ds, vk, ve,
     &     coef2x,coef4x, coef2y, coef4y, s, uu, vv, ccx, ccy, spectxi,
     &     specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja, icol, nflag)

      implicit none

#include "../include/parms.inc"
#include "../include/mbcom.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
#include "../include/units.inc"

      integer nmax,ntvar,nflag,ifirst,mp,ib,ifoil,nx,ng,ncons,ndvtot
      integer i,j,iout,iter,ifun,ioutk,ico,icg,lg,lq,iseg,is,ifsd
      integer nxpi,ngpi,ij,ncons1,ii,ngpj
      double precision q(*),             press(*),       sndsp(*)
      double precision s(*),             xy(*),          xyj(*)
      double precision xit(*),           ett(*),         ds(*)
      double precision x(*),             y(*),           turmu(*)
      double precision vort(*),          turre(*),       fmu(*)
      double precision vk(*),            ve(*),          tmp(*)
      double precision uu(*),            vv(*),          ccx(*)
      double precision ccy(*),           coef2x(*),      coef2y(*)
      double precision coef4x(*),        coef4y(*),      precon(*)
      double precision spectxi(*),       specteta(*),    wk1(*)
      double precision qps(*),           qos(*)
      double precision turps(*),         turos(*)
    
      integer ia(*), ja(*), ipa(*), jpa(*), icol(*), indx(*)

c     -- objective function --
      double precision CalcObj

      double precision fmin,acc,atmp,re_in,obj,pobj,gradmag,dg1,xsq,gsq
      double precision alfa,ap,fp,dp,step,alfmx,xte1,yte1,dist,dal,u1,u2
      double precision at,u3,u4,dg

c     -- optimization and b-spline arrays --
      double precision wbfgs(ibsnc*(ibsnc+7)/2), dv_scal(ibsnc)

      logical rsw

c     -- arrays for initial grid --
      double precision xsave(maxjk), ysave(maxjk)

c     -- temp solution array for warm starts --
      double precision qtmp(maxjkq,ndpts), turretmp(maxjk,ndpts)

c     -- multi-point optimization variables --
      double precision grads(ndpts,ibsnc), objs(ndpts)

      logical frozen
      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

C     INITIALIZE ITER, IFUN, NFLAG, AND IOUTK, WHICH COUNTS OUTPUT
c     ITERATIONS.

      acc = 1.e-16
      iout = 1

      iter=0
      ifun=0
      ioutk=0
      nflag=0
      ico = 0
      icg = 0

c     -- store total number of design variables --
      ndvtot = ndv

c     -- zero multi-point objective and gradient arrays --
      do i = 1,mpopt
         objs(i) = 0.d0
         do j = 1,ndv
            grads(i,j) = 0.d0
         end do
      end do

c     -- store initial grid --
      do ib = 1,nblks
         lg = lgptr(ib)
         call copy_array( jmax(ib), kmax(ib), 1, x(lg), xsave(lg))
         call copy_array( jmax(ib), kmax(ib), 1, y(lg), ysave(lg))
      end do

c     -- scaling info --
      do i = 1,ndv
         dv_scal(i) = dvs(i)
      end do

C     SET PARAMETERS TO EXTRACT VECTORS FROM W.
C     WBFGS(1..n) HOLDS THE SEARCH VECTOR,WBFGS(n+1..2n) HOLDS THE BEST
c     CURRENT
C     ESTIMATE TO THE MINIMIZER,AND WBFGS(2n+1..3n) HOLDS THE GRADIENT
C     AT THE BEST CURRENT ESTIMATE.

      nx=ndv
      ng=nx+ndv

C     IF NMETH=1,WBFGS(3n..) HOLDS THE APPROXIMATE INVERSE HESSIAN.

 10   ncons=3*ndv

C     CALCULATE THE FUNCTION AND GRADIENT AT THE INITIAL
C     POINT AND INITIALIZE NRST,WHICH IS USED TO DETERMINE
C     WHETHER A BEALE RESTART IS BEING DONE. NRST=N MEANS THAT THIS
C     ITERATION IS A RESTART ITERATION. INITIALIZE RSW,WHICH INDICATES
C     THAT THE CURRENT SEARCH DIRECTION IS A GRADIENT DIRECTION.

c     -- compute objective function --
 20   continue

      ifun=ifun+1

      write (n_dvhis,720) ifun, (dvs(j),j=1,ndv)
      call flush(n_dvhis)

c     -- adjust grid --
      if (ngdv.ne.0) then
         do ib = 1,nblks
            lg = lgptr(ib)
            call copy_array( jmax(ib), kmax(ib), 1, xsave(lg), x(lg))
            call copy_array( jmax(ib), kmax(ib), 1, ysave(lg), y(lg))
         end do
         call regrid(-1, x, y)
      end if

c     -- write out airfoil history --
      do ifoil = 1, nfoils
         write (n_fhis,*) 'ZONE T="',ifoil,'"' 
         do i = 1, ibap(ifoil)
            write (n_fhis,202) bap(i,1,ifoil), bap(i,2,ifoil)
         end do
      end do
      call flush(n_fhis)
 202  format (2e18.8)

c     -- muti-point optimization logic --
c     -- if mpopt = 1 => single point design --
c     -- routines calcobj and calcgrad require ndv = ngdv+1 with alpha
c     as the last design variable --
      do mp = 1,mpopt

c     -- define number of design variables at design point --
c     -- adjust angle of attack --
         if ( dvalfa(mp) ) then
            ndv = ngdv+1
            atmp = dvs(ngdv+1)
            dvs(ngdv+1) = dvs(idvi(ngdv+mp))
            alpha = dvs(ngdv+1)
         else
            ndv = ngdv
            alpha = alphas(mp)
         end if

         targetalpha = alpha
         fsmach = fsmachs(mp)
         re_in = reno(mp)
         cl_tar = cltars(mp)
         cd_tar = cdtars(mp)
         wfl = wfls(mp)

c     -- frozen: flag used for optimization debug --
         frozen = .false.
         ifirst = 1             !first call to flow solver

c     -- load flow solutions at operating points --
c     -- for multi-point optimization, only the first design point can
c     be restarted from a saved solution. this is very useful for
c     optimization near max lift. the following code can be used to
c     restart each design point, see ioall.f.
c     if (mpopt.gt.1) then
c     -- mpit is used to load the restart solution in ioall --
c     mpit = mp
c     restart = .true.
c     end if

c     -- compute obj. function --      


c     -- compute obj. func. gradient --
         if (ifrzw.ne.0) frozen = .true.
c         write(*,*) 'bfgs. CalcObj 185'
         obj = CalcObj( ifirst, nmax, indx, q,qps,qos,xy,xyj,x,y,turmu,
     &        vort, turre,turps,turos,fmu,press,precon,sndsp,xit,ett,ds, 
     &        vk,ve,coef2x, coef4x, coef2y, coef4y, s, uu, vv,ccx, ccy,
     &        spectxi, specteta, tmp, wk1, ntvar, ipa, jpa, ia, ja,
     &        icol)
c         write(*,*) 'bfgs. obj = ', obj
         objs(mp) = obj

c     -- check convergence of flow solve --
         if (resid .gt. 1.e-7 ) then

            write (n_out,210) resid
 210        format(3x, 
     &           'Flow solver convergence problems on first solve!',
     &           ' Resid:',e12.4,' STOP in bfgs.f')
            stop
         else                   !flow solve converged
            if (wstart) then
c     -- save current solution --
               do ib=1,nblks
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  call copy_array( jmax(ib), kmax(ib), 4, q(lq),
     &                 qtmp(lq,mp))
                  call copy_array( jmax(ib), kmax(ib), 1, turre(lg),
     $                 turretmp(lg,mp))
               end do
            end if
         end if

c     -- output info --
         pobj = obj

         if (mp.eq.1 .and. (ntcon.gt.0 .or. ngapc.gt.0 .or. novc.gt.0))
     &        write (n_con,720) ifun, (cth(i),i=1,ntcon),
     &        (gapd(ifgapc(i)),i=1,ngapc), (ovd(ifovc(i)),i=1,novc)

c     -- write out cp history file --
         if (mpopt.eq.1) then
            do ifoil = 1,nfoils
               write (n_cphis,*) 'ZONE T="',ifun,'"' 
               do iseg = 1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  call writecptar(nhalo, jbmax(ib), kbmax(ib),is,
     &                 press(lgptr(ib)), x(lgptr(ib)),
     &                 xyj(lgptr(ib)), .false., n_cphis)
               end do
            end do
         else
            do ifoil = 1,nfoils
               write (n_mpcp(mp),*) 'ZONE T="',ifun,'"' 
               do iseg = 1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  call writecptar(nhalo, jbmax(ib), kbmax(ib),is,
     &                 press(lgptr(ib)), x(lgptr(ib)),
     &                 xyj(lgptr(ib)), .false., n_mpcp(mp))
               end do
            end do
         end if

c     -- compute obj. func. gradient --
         if (ifrzw.ne.0) frozen = .true.

c         write(*,*) 'bfgs. CalcGrad 253, visc=',viscous
         call CalcGrad( ifirst, nmax, indx, q, xy, xyj, x, y, turmu,
     &        vort, turre, fmu, press, precon, sndsp, xit, ett,ds, vk,
     &        ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,ccx, ccy,
     &        spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,ia, ja, icol,
     &        xsave, ysave)
c         write(*,*) 'bfgs. grad = ', (grad(i),i=1,ndv)

         do i=1,ndv
            grads(mp,i) = grad(i)
         end do

         gradmag = 0.d0
         do i=1,ndv
            gradmag = gradmag + grad(i)*grad(i)
         end do
         gradmag = sqrt(gradmag)

         ico = ico+1

c     -- re-adjust design variable vector --
         if ( dvalfa(mp) ) then
            dvs(idvi(ngdv+mp)) = dvs(ngdv+1)
            dvs(ngdv+1) = atmp
         end if

c     -- write current design variables and objective function --
         if (mpopt.gt.1) then 
            write (n_mpo(mp),700) ifun, iter, pobj, gradmag, clt, cdt,
     &           clt/cdt, cmt, alpha, resid
            call flush(n_mpo(mp))
         end if
         
         write (n_out,710) ico, iter, obj, gradmag
         call flush(n_out)
      end do                    ! mpopt

      if (wstart) ifirst = 0    !ifirst=0 => warm starts

c     -- reset ndv to total number of design variables --
c     -- set obj and grad to total number of design variables -- 
      ndv = ndvtot
      
      obj = 0.d0
      do i=1,ndv
         grad(i)=0.d0
      end do

      do mp = 1,mpopt
         obj = obj + wmpo(mp)*objs(mp)
      end do

      do mp = 1,mpopt
         do i=1,ngdv
            grad(i)=grad(i) + wmpo(mp)*grads(mp,i)
         end do
      end do

      do mp = 1,mpopt
         if ( dvalfa(mp) ) then
            grad(idvi(ngdv+mp)) = wmpo(mp)*grads(mp,ngdv+1)
         end if
      end do

      gradmag = 0.d0
      do i=1,ndv
         gradmag = gradmag + grad(i)*grad(i)
      end do
      gradmag = sqrt(gradmag)

c     -- write optimization convergence info --
      if (mpopt.gt.1) then
         write (n_ohis,705) ifun, ico, iter, obj, gradmag
      else
         write (n_ohis,700) ifun, iter, pobj, gradmag, clt, cdt,
     &        clt/cdt, cmt, alpha, resid
      end if
      call flush(n_ohis)

      write (n_gvhis,810) ifun, (grad(i),i=1,ndv)
      call flush(n_gvhis)

 700  format (2i4, 6e14.6, f9.4, e12.4)
 705  format (3i4, 2e16.8)
 710  format ( /3x, 'ICO:', i4, 3x, 'S.D.#:', i4, 3x, 'OBJ.:',
     &     e12.4, 3x, 'GRAD:', e12.4)
 720  format (i3, 30e16.8)   
 810  format (i3, 30e16.8)

      if (ifun.eq.mxfun .or. ifrzw.ne.0) then
         write (n_out,*)
     &        'Obj. Function and gradient calculated => exit'
         return
      end if

c     -- scale variables --
      do i = 1,ndv
         dvs(i)  = dvs(i)/dv_scal(i)
         grad(i) = grad(i)*dv_scal(i)
      end do

      rsw=.true.

C     CALCULATE THE INITIAL SEARCH DIRECTION, THE NORM OF X SQUARED,
C     AND THE NORM OF G SQUARED. DG1 IS THE CURRENT DIRECTIONAL
C     DERIVATIVE,WHILE XSQ AND GSQ ARE THE SQUARED NORMS.

      dg1=0.d0
      xsq=0.d0
      do i=1,ndv
         wbfgs(i) =    -grad(i)
         xsq  = xsq+dvs(i)*dvs(i)
         dg1  = dg1-grad(i)*grad(i)
      enddo
      gsq=-dg1

C     TEST WHETHER THE INITIAL POINT IS THE MINIMIZER.

      if (gsq .le. optol*optol*max(1.d0,xsq) ) return

c     BEGIN THE MAJOR ITERATION LOOP.
C     FMIN IS THE CURRENT FUNCTION VALUE.

 40   fmin = obj

C     IF OUTPUT IS DESIRED,TEST IF THIS IS THE CORRECT ITERATION
C     AND IF SO, WRITE OUTPUT.

      if (iout .eq.0) goto 60
      if (ioutk.ne.0) goto 50
      write(n_ghis,500) icg,ifun,fmin,gradmag
 50   ioutk=ioutk+1
      if (ioutk.eq.iout) ioutk=0

C     BEGIN LINEAR SEARCH. ALFA IS THE STEPLENGTH.
C     SET ALFA TO THE NONRESTART CONJUGATE GRADIENT ALFA.

 60   alfa=alfa*dg/dg1

C     IF NMETH=1 OR A RESTART HAS BEEN PERFORMED, SET ALFA=1.d0.

      alfa=1.d0

C     IF IT IS THE FIRST ITERATION, SET ALFA=1.d0/DSQRT(GSQ),
C     WHICH SCALES THE INITIAL SEARCH VECTOR TO UNITY.

      if (rsw) alfa = bfstep/sqrt(gsq)

C     THE LINEAR SEARCH FITS A CUBIC TO F AND DAL, THE FUNCTION AND ITS
C     DERIVATIVE AT ALFA, AND TO FP AND DP,THE FUNCTION
C     AND DERIVATIVE AT THE PREVIOUS TRIAL POINT AP.
C     INITIALIZE AP ,FP,AND DP.

      ap=0.d0
      fp=fmin
      dp=dg1

C     SAVE THE CURRENT DERIVATIVE TO SCALE THE NEXT SEARCH VECTOR.

      dg=dg1

C     UPDATE THE ITERATION.

      iter=iter+1
      icg=icg+1
      ifsd=0

C     CALCULATE THE CURRENT STEPLENGTH  AND STORE THE CURRENT X AND G.

      step=0.d0
      do i=1,ndv
         step=step+wbfgs(i)*wbfgs(i)
         nxpi=nx+i
         ngpi=ng+i
         wbfgs(nxpi)=dvs(i)
         wbfgs(ngpi)=grad(i)
      enddo
      step=sqrt(step)

c     BEGIN THE LINEAR SEARCH ITERATION.
C     TEST FOR FAILURE OF THE LINEAR SEARCH.

 80   continue

      if (ifsd.gt.50) then
         write (n_out,*) 'Line search is not converging!!'
         icg = icg+1
         write(n_ghis,500) icg,ifun,fmin,gradmag
         return
      end if

 200  if(alfa*step.gt.acc) goto 90

C     TEST IF DIRECTION IS A GRADIENT DIRECTION.

      if (.not.rsw) goto 20
      nflag=2
      return

C     CALCULATE THE TRIAL POINT.

 90   alfmx = alfa

c     -- compute new design variables --
c     nsame = 0
      do i=1,ndv
         nxpi = nx+i
c     dtmp = dvs(i)
         dvs(i) = wbfgs(nxpi)+alfa*wbfgs(i)
c     if ( abs(dtmp-dvs(i)).lt.1.e-14 ) nsame = nsame+1
      end do

c     if (nsame.eq.ndv) then
c     write (*,940)
c     stop
c     end if
c     940  format(/3x,'No change in DVS during BFGS iteration!',/3x, 
c     &     'Gap or Overlap constraints are active.',/3x,
c     &     'Regrid geometry with AMBER-2D and continue.')

C     EVALUATE THE FUNCTION AT THE TRIAL POINT.

      ifun=ifun+1

c     -- re-scale --
      do i = 1,ndv
         dvs(i) = dvs(i)*dv_scal(i)
      end do 
      write (n_dvhis,720) ifun, (dvs(j),j=1,ndv)
      call flush(n_dvhis)

c     -- adjust grid --
      do ib = 1,nblks
         lg = lgptr(ib)
         call copy_array( jmax(ib), kmax(ib), 1, xsave(lg), x(lg))
         call copy_array( jmax(ib), kmax(ib), 1, ysave(lg), y(lg))
      end do
      
      call regrid( -1, x, y)

c     -- check for airfoil surface cross-over --
c     -- for future: you should rotate the chord to horizontal --
      do i=1,ntcon
         ifoil = itcf(i)
         if ( bap(juptx(i),2,ifoil) .lt. bap(jlotx(i),2,ifoil) ) then
            write (n_out,900) bap(juptx(i),2,ifoil), juptx(i),
     &           bap(jlotx(i),2,ifoil), jlotx(i)
            alfa = 0.5*alfa
            ifun = ifun - 1
            goto 200
         end if
      end do
 900  format(3x, 'Surface cross-over, reducing alfa:', /3x,
     &     'Upper BAP and JUPTX', f10.5, i3,' Lower BAP and JLOTX ',
     &     f10.5, i3) 

c     -- check gap and overlap constraints --
      if (ngapc.ne.0 .or. novc.ne.0) then
         do ifoil = 2, nfoils
            xte1 = bap(1,1,ifoil-1)
            yte1 = bap(1,2,ifoil-1)
            gapd(ifoil) = 1.d0
            do i = ile(ifoil),ibap(ifoil)
               dist = sqrt( (bap(i,2,ifoil) - yte1)**2 + 
     &              (bap(i,1,ifoil)-xte1)**2 )
               if (dist.lt.gapd(ifoil)) gapd(ifoil) = dist
c     -- check for surface collision --
               if ( bap(i,2,ifoil).gt.yte1 .and.
     &              bap(i,1,ifoil).lt.xte1) then
                  write (n_out,902)
c     write (n_out,*) 'y2=',bap(i,2,ifoil),'yte1=',yte1
c     write (n_out,*) 'x2=',bap(i,1,ifoil),'xte1=',xte1
                  alfa = 0.5*alfa
                  ifun = ifun - 1
                  goto 200
               end if
            end do
            ovd(ifoil) = bap(ile(ifoil),1,ifoil) - xte1
         end do
      end if
 902  format(3x, 'Surface collision, reducing alfa') 

      do i=1,ngapc
         ifoil = ifgapc(i)
c     -- assume gap does not change sign and is never zero --
         if ( gapd(ifoil) .gt. 1.15*gapu(i) ) then
            write (n_out,904)
            alfa = 0.5*alfa
            ifun = ifun - 1
            goto 200
         end if
 904     format(3x,'Gap upper bound exceeded by more than 15%,',
     &        ' reducing alfa')
         
         if ( gapd(ifoil) .lt. 0.85*gapl(i) ) then
            write (n_out,906) 
            alfa = 0.5*alfa
            ifun = ifun - 1
            goto 200
         end if
 906     format(3x,'Gap lower bound exceeded by more than 15%,',
     &        ' reducing alfa')

      end do

      do i=1,novc
         ifoil = ifovc(i)
         
         if ( ovd(ifoil) .gt. (ovu(i) + abs(0.15*ovu(i))) ) then
            write (n_out,908)
            alfa = 0.5*alfa
            ifun = ifun - 1
            goto 200
         end if
 908     format(3x,'Overlap upper bound exceeded by more than 15%',
     &        ' reducing alfa')
         
         if ( ovd(ifoil) .lt. (ovl(i) - abs(0.15*ovl(i))) ) then
            write (n_out,909)
            alfa = 0.5*alfa
            ifun = ifun - 1
            goto 200
         end if
 909     format(3x,'Overlap lower bound exceeded by more than 15%',
     &        ' reducing alfa')
      end do

c     -- write out airfoil history --
      do ifoil = 1, nfoils
         write (n_fhis,*) 'ZONE T="',nfoils*(ifun-1)+ifoil,'"' 
         do i = 1, ibap(ifoil)
            write (n_fhis,202) bap(i,1,ifoil), bap(i,2,ifoil)
         end do
      end do
      call flush(n_fhis)

c     -- output current grid --
      call outgrd( x, y)

      do mp = 1,mpopt

c     -- define number of design variables at design point --
c     -- adjust angle of attack --
         if ( dvalfa(mp) ) then
            ndv = ngdv+1
            atmp = dvs(ngdv+1)
            dvs(ngdv+1) = dvs(idvi(ngdv+mp))
            alpha = dvs(ngdv+1)
         else
            ndv = ngdv
            alpha = alphas(mp)
         end if
         targetalpha = alpha
         fsmach = fsmachs(mp)
         re_in = reno(mp)
         cl_tar = cltars(mp)
         cd_tar = cdtars(mp)
         wfl = wfls(mp)

c     -- restore previous solution at operating point --
         if (mpopt.gt.1) then
            do ib=1,nblks
               lq=lqptr(ib)
               lg = lgptr(ib)
               call copy_array( jmax(ib), kmax(ib), 4, qtmp(lq,mp),
     &              q(lq))
               call copy_array( jmax(ib), kmax(ib), 1,
     &              turretmp(lg,mp), turre(lg))
            end do
         end if

c     -- compute obj. function -- 

c         write(*,*) 'bfgs. CalcObj 631'
         obj = CalcObj( ifirst, nmax, indx, q,qps,qos,xy,xyj,x,y,turmu,
     &        vort, turre,turps,turos,fmu,press,precon,sndsp,xit,ett,ds,
     &        vk,ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,ccx, ccy,
     &        spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,ia, ja, icol)
c         write(*,*) 'bfgs. obj = ', obj

c     -- detection of negative Jacobian --
         if (badjac) then
            write (n_out,905)
            badjac = .false.
            alfa = 0.5*alfa
            ifun = ifun - 1
            if ( dvalfa(mp) ) then
               dvs(idvi(ngdv+mp)) = dvs(ngdv+1)
               dvs(ngdv+1) = atmp
            end if
            ndv = ndvtot
            goto 200         
         end if
 905     format(3x,'Negative Jacobian detected, reducing alfa')

         objs(mp) = obj
      
c     -- check convergence of flow solve --
         if (resid .gt. 1.e-8 ) then

            write (n_out,910) resid
 910        format(3x,'Flow solver convergence problems! Resid:',e12.4)
            
            if (wstart) then
c     -- restore last converged solution --
               write (n_out,920)
 920           format(3x,'Restoring last converged solution')
               do ib=1,nblks
                  lq=lqptr(ib)
                  lg = lgptr(ib)
                  call copy_array( jmax(ib), kmax(ib), 4, qtmp(lq,mp),
     &                 q(lq))
                  call copy_array( jmax(ib), kmax(ib), 1,
     &                 turretmp(lg,mp), turre(lg))
               end do
            else
               write (n_out,930) 
 930           format(3x,'Solution reset to freestream')
               restart = .false.
               ifirst = 1
            end if              ! if wstart

            if ( dvalfa(mp) ) then
               dvs(idvi(ngdv+mp)) = dvs(ngdv+1)
               dvs(ngdv+1) = atmp
            end if
            ndv = ndvtot
            
            alfa = 0.5*alfa
            ifun = ifun - 1
            goto 200
         else                   !flow solve converged
            if (wstart) then 
               ifirst = 0       !warm starts for tornado        
c     -- save current solution --
               do ib=1,nblks
                  lq=lqptr(ib)
                  lg = lgptr(ib)
                  call copy_array( jmax(ib), kmax(ib), 4, q(lq),
     &                 qtmp(lq,mp))
                  call copy_array( jmax(ib), kmax(ib), 1, turre(lg),
     $                 turretmp(lg,mp))
               end do
            end if
         end if

c     -- output info --
         pobj = obj

         if (mp.eq.1 .and. (ntcon.gt.0 .or. ngapc.gt.0 .or. novc.gt.0)) 
     &        write (n_con,720) ifun, (cth(i),i=1,ntcon),
     &        (gapd(ifgapc(i)),i=1,ngapc), (ovd(ifovc(i)),i=1,novc)

c     -- write out cp history file --
         if (mpopt.eq.1) then
            do ifoil = 1,nfoils
               write (n_cphis,*) 'ZONE T="',ifun,'"' 
               do iseg = 1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  call writecptar(nhalo, jbmax(ib), kbmax(ib),is,
     &                 press(lgptr(ib)), x(lgptr(ib)),
     &                 xyj(lgptr(ib)), .false., n_cphis)
               end do
            end do
         else
            do ifoil = 1,nfoils
               write (n_mpcp(mp),*) 'ZONE T="',ifun,'"' 
               do iseg = 1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  call writecptar(nhalo, jbmax(ib), kbmax(ib),is,
     &                 press(lgptr(ib)), x(lgptr(ib)),
     &                 xyj(lgptr(ib)), .false., n_mpcp(mp))
               end do
            end do
         end if
         
c         write(*,*) 'bfgs. CalcGrad 736'
         call CalcGrad( ifirst, nmax, indx, q, xy, xyj, x, y, turmu,
     &        vort, turre, fmu, press, precon, sndsp, xit, ett,ds, vk,
     &        ve, coef2x, coef4x, coef2y, coef4y, s, uu, vv,ccx, ccy,
     &        spectxi, specteta, tmp, wk1, ntvar, ipa, jpa,ia, ja, icol,
     &        xsave, ysave)
c         write(*,*) 'bfgs. grad = ', (grad(i),i=1,ndv)

         do i=1,ndv
            grads(mp,i) = grad(i)
         end do

         gradmag = 0.d0
         do i=1,ndv
            gradmag = gradmag + grad(i)*grad(i)
         end do
         gradmag = sqrt(gradmag)

         ico = ico+1

c     -- re-adjust design variable vector --
         if ( dvalfa(mp) ) then
            dvs(idvi(ngdv+mp)) = dvs(ngdv+1)
            dvs(ngdv+1) = atmp
         end if         

c     -- write current design variables and objective function --
         if (mpopt.gt.1) then 
            write (n_mpo(mp),700) ifun, iter, pobj, gradmag, clt, cdt,
     &           clt/cdt, cmt, alpha, resid
            call flush(n_mpo(mp))
         end if
         
         write (n_out,710) ico, iter, obj, gradmag
         call flush(n_out)
      end do                    ! mpopt

c     -- track search directions --
      ifsd=ifsd+1

c     -- reset ndv to total number of design variables --
c     -- set obj and grad to total number of design variables -- 
      ndv = ndvtot

      obj = 0.d0
      do i=1,ndv
         grad(i)=0.d0
      end do

      do mp = 1,mpopt
         obj = obj + wmpo(mp)*objs(mp)
      end do

      do mp = 1,mpopt
         do i=1,ngdv
            grad(i)=grad(i) + wmpo(mp)*grads(mp,i)
         end do
      end do

      do mp = 1,mpopt
         if ( dvalfa(mp) ) then
            grad(idvi(ngdv+mp)) = wmpo(mp)*grads(mp,ngdv+1)
         end if
      end do

      gradmag = 0.d0
      do i=1,ndv
         gradmag = gradmag + grad(i)*grad(i)
      end do
      gradmag = sqrt(gradmag)

c     -- write optimization convergence info --
      if (mpopt.gt.1) then
         write (n_ohis,705) ifun, ico, iter, obj, gradmag
      else
         write (n_ohis,700) ifun, iter, pobj, gradmag, clt, cdt,
     &        clt/cdt, cmt, alpha, resid
      end if
      call flush(n_ohis)

      write (n_gvhis,810) ifun, (grad(i),i=1,ndv)
      call flush(n_gvhis)

      do i = 1,ndv
         dvs(i) = dvs(i)/dv_scal(i)
         grad(i) = grad(i)*dv_scal(i)
      end do

C     TEST IF THE MAXIMUM NUMBER OF FUNCTION CALLS HAVE BEEN USED.

      if(ifun.le.mxfun) goto 110
      nflag=1

c     -- rescale design variables --
      do i = 1,ndv
         dvs(i) = dvs(i)*dv_scal(i)
      end do      

      write(n_ghis,500) icg,ifun,fmin,gradmag

      return

C     COMPUTE THE DERIVATIVE OF F AT ALFA.

 110  dal=0.d0
      do i=1,ndv
         dal=dal+grad(i)*wbfgs(i)
      enddo
C     
C     TEST WHETHER THE NEW POINT HAS A NEGATIVE SLOPE BUT A HIGHER
C     FUNCTION VALUE THAN ALFA=0. IF THIS IS THE CASE,THE SEARCH
C     HAS PASSED THROUGH A LOCAL MAX AND IS HEADING FOR A DISTANT LOCAL
C     MINIMUM.
C     
      if (obj.gt.fmin .and. dal.lt.0.d0) goto 160
C     
C     IF NOT, TEST WHETHER THE STEPLENGTH CRITERIA HAVE BEEN MET.
C     
      if (obj.gt.(fmin+1.e-4*alfa*dg) .or. abs(dal/dg).gt.0.9)
     >     goto 130
C     
C     IF THEY HAVE BEEN MET, TEST IF TWO POINTS HAVE BEEN TRIED
C     IF NMETH=0 AND IF THE TRUE LINE MINIMUM HAS NOT BEEN FOUND.
C     
      goto 170
C     
C     A NEW POINT MUST BE TRIED. USE CUBIC INTERPOLATION TO FIND
C     THE TRIAL POINT AT.
C     
 130  u1=dp+dal-3.0*(fp-obj)/(ap-alfa)
      u2=u1*u1-dp*dal
      if(u2.lt.0.d0) u2=0.d0
      u2=sqrt(u2)
      at=alfa-(alfa-ap)*(dal+u2-u1)/(dal-dp+2.0*u2)
c     
C     TEST WHETHER THE LINE MINIMUM HAS BEEN BRACKETED.
C     
      if ((dal/dp).gt.0.) goto 140
C     
C     THE MINIMUM HAS BEEN BRACKETED. TEST WHETHER THE TRIAL POINT LIES
C     SUFFICIENTLY WITHIN THE BRACKETED INTERVAL.
C     IF IT DOES NOT, CHOOSE AT AS THE MIDPOINT OF THE INTERVAL.
C     
      if (at.lt.(1.01d0*dmin1(alfa,ap)) .or. 
     >     at.gt.(.99d0*dmax1(alfa,ap))      ) at=(alfa+ap)*.5d0
      goto 150
C     
C     THE MINIMUM HAS NOT BEEN BRACKETED. TEST IF BOTH POINTS ARE
C     GREATER THAN THE MINIMUM AND THE TRIAL POINT IS SUFFICIENTLY
C     SMALLER THAN EITHER.
C     
 140  if (dal.gt.0.d0 .and. 0.d0.lt.at .and.
     >     at.lt.(.99d0*dmin1(ap,alfa))     ) goto 150
C     
C     TEST IF BOTH POINTS ARE LESS THAN THE MINIMUM AND THE TRIAL POINT
C     IS SUFFICIENTLY LARGE.
C     
      if (dal.le.0.d0 .and. at.gt.(1.01d0*dmax1(ap,alfa))) goto 150
C     
C     IF THE TRIAL POINT IS TOO SMALL,DOUBLE THE LARGEST PRIOR POINT.
C     
      if (dal.le.0.d0) at=2.d0*dmax1(ap,alfa)
C     
C     IF THE TRIAL POINT IS TOO LARGE, HALVE THE SMALLEST PRIOR POINT.
C     
      if (dal.gt.0.d0) at=dmin1(ap,alfa)*.5d0
C     
C     SET AP=ALFA, ALFA=AT,AND CONTINUE SEARCH.
C     
 150  ap=alfa
      fp=obj
      dp=dal
      alfa=at
      goto 80
C     
C     A RELATIVE MAX HAS BEEN PASSED.REDUCE ALFA AND RESTART THE SEARCH.
C     
 160  alfa=alfa/3.0
      ap=0.d0
      fp=fmin
      dp=dg
      goto 80
c
C     THE LINE SEARCH HAS CONVERGED. TEST FOR CONVERGENCE OF THE
c     ALGORITHM.
c
 170  gsq=0.d0
      xsq=0.d0
      do i=1,ndv
         gsq=gsq+grad(i)*grad(i)
         xsq=xsq+dvs(i)*dvs(i)
      enddo

c     -- write out airfoil coordinates in amber2D format --
      rewind(n_ac)
      do ifoil=1,nfoils
         write (n_ac,175) ifoil
         do i = 1, ibap(ifoil)
            write (n_ac,202) bap(i,1,ifoil), bap(i,2,ifoil)
         end do
         write (n_ac,176)
      end do
 175  format ('#*NAME=',i3)
 176  format ('#*EOR')

c     -- write out design variables --
      do i = 1,ndv
         dvs(i) = dvs(i)*dv_scal(i)
      end do     
      rewind(n_dvr)
      write (n_dvr,177) ndv, nbdv, ntdv
      write (n_dvr,178) (idvf(i), idvs(i), idvi(i), dvs(i), i=1,ndv)      
      do i = 1,ndv
         dvs(i) = dvs(i)/dv_scal(i)
      end do
 177  format (3i5)
 178  format(2i3,i4,e24.16)

      if (gsq.le.optol*optol*dmax1(1.d0,xsq)) then
         write(n_ghis,500) icg,ifun,obj,gradmag
         return
      end if
c
C     SEARCH CONTINUES. SET WBFGS(I)=ALFA*WBFGS(I),THE FULL STEP VECTOR.
c
      do i=1,ndv
         wbfgs(i)=alfa*wbfgs(i)
      enddo
c
C     COMPUTE THE NEW SEARCH VECTOR. FIRST TEST WHETHER A
C     CONJUGATE GRADIENT OR A VARIABLE METRIC VECTOR IS USED.
c
      goto 330
c
C     ROUNDOFF PROBLEMS
 320  nflag=3
      return
C     
C     A VARIABLE METRIC ALGORITM IS BEING USED. CALCULATE Y AND D'Y.
C     
 330  u1=0.d0
      do i=1,ndv
         ngpi=ng+i
         wbfgs(ngpi)=grad(i)-wbfgs(ngpi)
         u1=u1+wbfgs(i)*wbfgs(ngpi)
      enddo
c     
C     IF RSW=.TRUE.,SET UP THE INITIAL SCALED APPROXIMATE HESSIAN.
C     
      if (.not.rsw) goto 380
C     
C     CALCULATE Y'Y.
C     
      u2=0.d0
      do i=1,ndv
         ngpi=ng+i
         u2=u2+wbfgs(ngpi)*wbfgs(ngpi)
      enddo
C     
C     CALCULATE THE INITIAL HESSIAN AS H=(P'Y/Y'Y)*I
C     AND THE INITIAL U2=Y'HY AND WBFGS(NX+I)=HY.
C     
      ij=1
      u3=u1/u2
      do i=1,ndv
         do j=i,ndv
            ncons1=ncons+ij
            wbfgs(ncons1)=0.d0
            if (i.eq.j) wbfgs(ncons1)=u3
            ij=ij+1
         enddo
         nxpi=nx+i
         ngpi=ng+i
         wbfgs(nxpi)=u3*wbfgs(ngpi)
      enddo
      u2=u3*u2
      goto 430
c     
C     CALCULATE WBFGS(NX+I)=HY AND U2=Y'HY.
C     
 380  u2=0.d0
      do i=1,ndv
         u3=0.d0
         ij=i
         if (i.eq.1) goto 400
         ii=i-1
         do j=1,ii
            ngpj=ng+j
            ncons1=ncons+ij
            u3=u3+wbfgs(ncons1)*wbfgs(ngpj)
            ij=ij+ndv-j
         enddo
 400     do j=i,ndv
            ncons1=ncons+ij
            ngpj=ng+j
            u3=u3+wbfgs(ncons1)*wbfgs(ngpj)
            ij=ij+1
         enddo
         ngpi=ng+i
         u2=u2+u3*wbfgs(ngpi)
         nxpi=nx+i
         wbfgs(nxpi)=u3
      enddo
c     
C     CALCULATE THE UPDATED APPROXIMATE HESSIAN.
C     
 430  u4=1.d0+u2/u1
      do i=1,ndv
         nxpi=nx+i
         ngpi=ng+i
         wbfgs(ngpi)=u4*wbfgs(i)-wbfgs(nxpi)
      enddo
      ij=1
      do i=1,ndv
         nxpi=nx+i
         u3=wbfgs(i)/u1
         u4=wbfgs(nxpi)/u1
         do j=i,ndv
            ncons1=ncons+ij
            ngpj=ng+j
            wbfgs(ncons1) = wbfgs(ncons1)+u3*wbfgs(ngpj)-u4*wbfgs(j)
            ij=ij+1
         enddo
      enddo
C     
C     CALCULATE THE NEW SEARCH DIRECTION WBFGS(I)=-HG AND ITS DERIVATIVE
C     
      dg1=0.d0
      do i=1,ndv
         u3=0.d0
         ij=i
         if (i.eq.1) goto 470
         ii=i-1
         do j=1,ii
            ncons1=ncons+ij
            u3=u3-wbfgs(ncons1)*grad(j)
            IJ=IJ+ndv-J
         enddo         
 470     do j=i,ndv
            ncons1=ncons+ij
            u3=u3-wbfgs(ncons1)*grad(j)
            ij=ij+1
         enddo
         dg1=dg1+u3*grad(i)
         wbfgs(i)=u3
      enddo
c     
C     TEST FOR A DOWNHILL DIRECTION.
C     
      if (dg1.gt.0.) goto 320
      rsw=.false.
      goto 40

 500  format(4x,i5,3x,i6,2x,d15.8,2x,d15.8)

      return
      end                       !BFGS

