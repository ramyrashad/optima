/*********************************************************************** 
    Module:  Notes
 
    Copyright (C) 1996 Harold Youngren, Mark Drela 
 
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Report problems to:    guppy@netcom.com 
                        or drela@henry.mit.edu  
***********************************************************************/ 

Xplot11  
C     Version 4.38  3/8/98

Released under GNU Library License 8/5/96


Notes:
  
These routines can be compiled either single or double precision by simply
setting a flag in the Makefile.  No source changes.


An option exists for the old Versatec compatibility routine SYMBOL (in 
plt_old.f) to take an integer or byte argument containing either the string
or an integer specifying a plot symbol.  The current arrangement uses 
character string arguments and handles plot symbols with the SYMBL routine.
This problem comes up with f77 compilers that do not support the older style
cramming of literal characters into an integer argument.  These compilers 
add additional length arguments to the argument list for literal character 
strings that are flagged as errors by the compiler (this happens for BSD f77 
compilers).  If you want old-style Versatec SYMBOL calls look at plt_old.f 
and make the suggested source changes.  You do lose the ability to call 
SYMBOL with character variables but you can always use the newer PLCHAR 
to plot character strings.




The only known bugs at this time are:

On IBM AIX systems there appears to be a compiler bug with the xlf compiler
that shows up when compiling the library double precision.  The polyline
routines are affected, getting every other data point.  This was discovered
in July 1996, compiler version unknown.  No fix, doesn't affect the single
precision library or other routines in double precision.

Some Xservers have shown problems with the grid routines, this showed up on 
OSF 3.2 on the DEC Alpha with the 8 bit X server running on a Mach32 card.  
The characteristic is that the masked grid lines show the wrong background 
color.  This is a server error, complain to your vendor.


Version 4.33   8/30/96

Added logfile for complex plots, fixed xaxis, yaxis to allow non-annotated 
but hash-marked axes.  Fixes to ps_linepattern to limit to 8 pattern entries.

Note: to make a more versatile library it may be better to break the routines
that make up the library into separate modules to permit linking with routines
of the same name that are user supplied. This will probably be done later but,
if you need to make a split library you can always use the makesplitlib shell 
script provided here (crude but it works). 

Additional notes 9/5/96

Changes were made to the ps_subs.f and Xwin.c to improve the linepattern 
routines, separating the fortran and C code a bit more with a fortran version
of mskbits (bitpat) for the postscript side.  The Makefile.DP was changed
to make the DP version of Xplot11 work with xlf90 on the RS6000.  This really 
should go in the new version (4.34)...HHY

Version 4.34   10/30/96

Changes were made to the bitpat routine in ps_subs.f to replace the iand() and ishft() calls with and() and rshift() calls that are more universal with f77 compilers, in particular f2c which is used on Linux.  Note that if you have any trouble with these calls you might replace and(xxx,yyy) with iand(xxx,yyy) and rshift(xxx,n) with ishft(xxx,-n)...HHY


Version 4.35   2/6/97

Fixed logfile to work for plots that replot and then add plot items.  Now 
the block size is written before each block of log data.  Makefiles revised.
Linux makefile added that includes target for shared ELF library.  ...HHY


Version 4.36   2/24/97

Updated symbol definitions in sym directory and in the xxx.INC character
font include files.


Version 4.37   6/24/97

Includes MD fix for XAXIS/YAXIS buglet  of 5/24/97 
Includes HY fix for AXISADJ bug         of 4/97
Includes MD fix for gw_curs calling bug of 6/28/97 

HHY & MD
 

Version 4.38  3/8/98

Fixed bug for unfilled polylines that do not close and are "closed"
automatically but incorrectly by the Sutherland-Hodgman clipper.  These
unfilled polylines are now processed by the regular line clipper by plotting
them as separate line segments. 

Added double-buffering by defining pixmap and 3 new routines to set graphics
destination (screen or buffer) and display buffer contents to screen.


