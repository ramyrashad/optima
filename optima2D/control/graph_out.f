c     This file contains 3 subroutines:
c     - initial_graph_out: to draw the initial airfoil at the beginning of the optimization run
c     - graph_out: draw cp and airfoil shape during the optimization run
c     - grid_aixs: auxiliary to the graph_out and initial_graph_out to draw axis


c     initial_graph_out: draw the initial airfoil
c     arguments x, y stores all the grid points
c     arguments jdim, kdim are max j and k
c     none of the argument is changed in this subroutine
      subroutine initial_graph_out (x,y,jdim,kdim)

#include "../include/arcom.inc"
#include "../include/optcom.inc"


c     airfoil data
      double precision xsurface(jbody),ysurface(jbody)
      integer jdim,kdim
      double precision x(jdim,kdim), y(jdim,kdim)

c     temporary loop index
      integer i,j
      

c     all graphic data
      integer ipslu, idev
      data    ipslu, idev /0, 1/

      double precision relsize
      data relsize /0.64/       

      double precision x_scale, y_scale, x_org, y_org
      data x_scale, y_scale, x_org, y_org
     &   /     8.0,     8.0,   0.0,   0.0 /

      double precision x_right, x_left, y_up, y_down
      data x_right, x_left, y_up, y_down
     &   /   1.1,   -0.1,    0.2,   -0.2/
           
      

c     obtain the airfoil surface
      i=1      
      do j=jtail1,jtail2
         xsurface(i)=x(j,1)
         ysurface(i)=y(j,1)
         i=i+1
      end do
      
clb      do i=1,jbody
clb         write(*,*), xsurface(i),ysurface(i)
clb      end do

c     initialize the colors to be used
      call plinitialize	
c     open the plotting window
      call plopen ( relsize, ipslu, idev )
      call newpen(3)
      call drawtoscreen
c     redefine the scales in the plotting window 
      call newfactors ( x_scale, y_scale )

      
       call newcolorname ('black')
c     redefine the position of origin
       x_org = 0.6 - x_left*x_scale
       y_org = 3.0 - y_down*y_scale

c     draw the x and y axis
       call gridaxis (x_org, y_org, x_scale, y_scale, x_left, x_right,
     &        y_down, y_up,y_down)

c     write the plot name
       call plchar(0.3,0.25, 0.02, 'Initial Airfoil Shape', 0.0,-1)


c     plot the airfoil shape
c     note that calling xyline only once may somtimes result in unexpected output, the reason is unclear
      call newcolorname('red')
      call xyline (jbody,xsurface,ysurface,0.0,1.0,0.0,1.0,1)
      call xyline (jbody,xsurface,ysurface,0.0,1.0,0.0,1.0,1)
      call xyline (jbody,xsurface,ysurface,0.0,1.0,0.0,1.0,1)
c     write the plot name
       call plchar(0.3,0.25, 0.02, 'Initial Airfoil Shape', 0.0,-1)
      end



c     subroutine graph_out: plot the output during optimization
c     note that maximum operation point to be drawn is 4
c     argument ac_his stores all the airfoil shapes since the beginning of the optimization
c     argument cp_his stores all the cp information for each airfoil and each operation point
c     arguments jdim and kdim store jmax and kmax
c     argument ifun is the number of iteration
c     none of the argument is changed in this subroutine
      subroutine graph_out(ac_his,cp_his,jdim,kdim,ifun,cd,cl,cm)
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/graph.inc"


c     arguments
      integer jdim,kdim,ifun
      double precision cp_his(jbody,300,mpopt),ac_his(jbody,300)


c     shape and cp information

c     current shape and cp
      double precision xsurface(jbody),ysurface(jbody),cpsurface(jbody)
c     initial shape and cp
      double precision cp_initial(jbody),y_initial(jbody)
c     shape and cp in the previous iteration
      double precision cp_previous(jbody),y_previous(jbody)
c     the cp and shape to be plotted(used only if graph_mode=2)
      double precision cp_temp(jbody),y_temp(jbody)

c     aerodynamic properties
      double precision cd(mpopt),cl(mpopt),cm(mpopt)

c     loop index
      integer index
      integer i,j
      integer pt

c     the max cp and min cp at each operation point
c     cp ratio is to rescale all the cp values to display them properly in the window
c     they are static variables
      double precision cpmax(4),cpmin(4),cpratio(4)
      double precision ymin(4)
      

c     graphic data
c     scale and org are set to be array of num_graph+1 elements because
c     1 graph for shape and num_graph of plots for cp at different operation points
      integer ipslu, idev
      data    ipslu, idev /0, 1/

      double precision relsize
      data relsize /0.9/       

      double precision x_scale(num_graph+1), y_scale(num_graph+1)

      double precision x_right, x_left, y_up, y_down
      data x_right, x_left, y_up, y_down
     &   /   1.1,   -0.1,    0.2,   -0.2/

      double precision x_org(num_graph+1),y_org(num_graph+1)

      save cpratio,cpmax,cpmin
      data cpmax,cpmin
     &   /0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0/

      integer color_index

     
c     --scale and orginate each coordinate system
      x_scale(1)=8.0
      y_scale(1)=5.0
      x_org(1)=0.8
      y_org(1)=1.2
    
      if(num_graph.eq.1)then
         x_scale(2)=8.0
         y_scale(2)=11.0
         x_org(2)=0.8
         y_org(2)=5.6
      else if(num_graph.eq.2)then
         x_scale(2)=3.8
         x_scale(3)=x_scale(2)
         y_scale(2)=11.0
         y_scale(3)=y_scale(2)
         x_org(2)=0.8
         x_org(3)=5.6
         y_org(2)=5.6
         y_org(3)=y_org(2)
      else
         x_scale(2)=3.8
         y_scale(2)=5.5
         x_org(2)=0.6
         y_org(2)=6.8

         x_scale(3)=x_scale(2)
         y_scale(3)=y_scale(2)
         x_org(3)=5.2
         y_org(3)=y_org(2)

         x_scale(4)=x_scale(2)
         y_scale(4)=y_scale(2)
         x_org(4)=x_org(2)
         y_org(4)=4

         if(num_graph.eq.4)then
            x_scale(5)=x_scale(2)
            y_scale(5)=y_scale(2)
            x_org(5)=x_org(3)
            y_org(5)=y_org(4)
         end if
       
      end if
      
     
c     --find cpmax in order to properly scale the cp into plotting window
c     each graph operation point has a cpmax and cpmin
      do pt=1,num_graph
         do j=jtail1,jtail2
            if(cp_his(j,ifun,graph_pt(pt)).gt.cpmax(pt))then
               cpmax(pt)=cp_his(j,ifun,graph_pt(pt))
            end if
            if(cp_his(j,ifun,graph_pt(pt)).le.cpmin(pt))then
               cpmin(pt)=cp_his(j,ifun,graph_pt(pt))
            end if
         end do
          
         if((-cpmin(pt)).le.cpmax(pt))then
            cpratio(pt)=cpmax(pt)/0.2
            ymin(pt)=-cpmax(pt)
         else
            cpratio(pt)=(-cpmin(pt))/0.2
            ymin(pt)=cpmin(pt)
         end if
      end do
     
c     --close the prevous window and open a new one
      call plclose
      call plinitialize	
      call plopen ( relsize, ipslu, idev )
      call newpen(3)
      call drawtoscreen
      
c     --if mode=1, draw the current,initial and previous
      if(graph_mode.eq.1) then

c     --draw the current, previous, and initial ac         

c     obtain the values of ac 
         do i=1,jbody 
            ysurface(i)=ac_his(i,ifun+1)
            y_previous(i)=ac_his(i,ifun)
            y_initial(i)=ac_his(i,2)
            xsurface(i)=ac_his(i,1)
         end do
c     set the scale and origin of plotting
         call newfactors(x_scale(1),y_scale(1))
         call plotabs ( x_org(1),y_org(1),-3)

c     after 1st iteration, draw both current and previous ac
         if(ifun.ne.1)then
            call newcolorname('green')
            call xyline (jbody,xsurface,ysurface,0.0,1.0,0.0,1.0,1)

              
            call newcolorname('blue')
            call xyline (jbody,xsurface,y_previous,0.0,1.0,0.0,1.0,1)

         end if
c     for all iterations, draw the initial ac
c     note that when ifun=2, the initial will replace the previous 
         call newcolorname('red')
         call xyline (jbody,xsurface,y_initial,0.0,1.0,0.0,1.0,1)

            
                  
c     --draw the current, previous, and initial cp

c     each iteration of the loop draws one cp graph at one operation point
         do pt=1,num_graph

c     draw the current cp and initial cp
c     note that if ifun=1, initial will replace current
               do i=1,jbody
                  cpsurface(i)=(cp_his(i,ifun,graph_pt(pt)))
     &                 /(-cpratio(pt))
                  cp_initial(i)=(cp_his(i,1,graph_pt(pt)))
     &                 /(-cpratio(pt))
                end do
                call newfactors(x_scale(pt+1),y_scale(pt+1))
                call plotabs ( x_org(pt+1),y_org(pt+1),-3)
                call newcolorname('green')
                call xyline (jbody,xsurface,cpsurface,0.0,1.0,0.0,1.0,1)


                call newcolorname('red')
                call xyline(jbody,xsurface,cp_initial,0.0,1.0,0.0,1.0,1)


c     for iteration number larger than 2, draws the previous cp as well
                if(ifun.gt.2)then
                  do i=1,jbody
                     cp_previous(i)=(cp_his(i,ifun-1,graph_pt(pt)))
     &                    /(-cpratio(pt))
                  end do
             
                  call newcolorname('blue')
              
                  call xyline (jbody,xsurface,cp_previous,0.0,1.0,0.0,
     &                 1.0,1)

               end if
            end do
     
c     --if mode=2
      else 

c     read in the airfoil x coordinates
         do i=1,jbody 
            xsurface(i)=ac_his(i,1)
         end do

c     for ifun larger than the value of interval, at least 2 iteration need to be plotted
         if(ifun.ge.interval)then
c     color_index stores which color to be used
c     there are 4 colors in total
            color_index=1


         
c     --draw ac and cp for each interval airfoils
            do i=interval,ifun,interval
c     4 colors are green, yellow, blue, and violet
               color_index=mod((i/interval),4)
               if(color_index.eq.1)then
                  call newcolorname('green')
               else if(color_index.eq.2)then
                  call newcolorname('yellow')
               else if(color_index.eq.3)then
                  call newcolorname('blue')
               else
                  call newcolorname('violet')
               end if
           
c     --draw the cp at each operation point designated
               do pt=1,num_graph
                  do j=1,jbody
                    cp_temp(j)=(cp_his(j,i,graph_pt(pt)))/(-cpratio(pt))
                  end do
                  call newfactors(x_scale(pt+1),y_scale(pt+1))
                 call plotabs ( x_org(pt+1),y_org(pt+1),-3)
                 call xyline (jbody,xsurface,cp_temp,0.0,1.0,0.0,1.0,1)
                 call xyline (jbody,xsurface,cp_temp,0.0,1.0,0.0,1.0,1)
              end do
c     --draw the airfoil shape
              do j=1,jbody
                 y_temp(j)=ac_his(j,i)
              end do
              call newfactors(x_scale(1),y_scale(1))
              call plotabs ( x_org(1),y_org(1),-3)
              call xyline (jbody,xsurface,y_temp,0.0,1.0,0.0,1.0,1)
              call xyline (jbody,xsurface,y_temp,0.0,1.0,0.0,1.0,1)
           end do           
        end if

c     draw the initial cp and shape
        call newcolorname('red')
        do pt=1,num_graph
           do j=1,jbody
              cp_temp(j)=(cp_his(j,1,graph_pt(pt)))/(-cpratio(pt))
           end do
           call newfactors(x_scale(pt+1),y_scale(pt+1))
           call plotabs ( x_org(pt+1),y_org(pt+1),-3)
           call xyline (jbody,xsurface,cp_temp,0.0,1.0,0.0,1.0,1)
           call xyline (jbody,xsurface,cp_temp,0.0,1.0,0.0,1.0,1)
        end do
c     --draw the airfoil shape
        do j=1,jbody
           y_temp(j)=ac_his(j,2)
        end do
        call newfactors(x_scale(1),y_scale(1))
        call plotabs ( x_org(1),y_org(1),-3)
        call xyline (jbody,xsurface,y_temp,0.0,1.0,0.0,1.0,1)
        call xyline (jbody,xsurface,y_temp,0.0,1.0,0.0,1.0,1)
      end if

      

c     --draw the axis
      call newcolorname ('black')
      call gridaxis(x_org(1), y_org(1), x_scale(1), y_scale(1), x_left,
     &     x_right,y_down, y_up,y_down)
      call plchar(0.4,0.2, 0.02, 'Airfoil Shape', 0.0,-1)

c     --make the legend
      if(graph_mode.eq.1)then
         call plchar(0.1,0.27,0.02,'legend:',0.0,-1)
         call newcolorname('red')
         call plchar(0.24,0.27,0.02,'initial',0.0,-1)
         if(ifun.gt.1)then
            call plchar(0.38,0.27,0.02,',',0.0,-1)
            call newcolorname('green')
            call plchar(0.42,0.27,0.02,'iteration',0.0,-1)
            call plnumb(0.62,0.27,0.02,real(ifun),0.0,-1)
         end if
         if(ifun.gt.2)then
            call plchar(0.64,0.27,0.02,',',0.0,-1)
            call newcolorname('blue')
            call plchar(0.68,0.27,0.02,'iteration',0.0,-1)
            call plnumb(0.88,0.27,0.02,real(ifun-1),0.0,-1)
         end if
         
      else
         call plchar(0.1,0.37,0.02,'legend:',0.0,-1)
         call newcolorname('red')
         call plchar(0.24,0.37,0.02,'initial',0.0,-1)
         if(ifun.ge.interval)then
            call plchar(0.38,0.37,0.02,',',0.0,-1)
            call newcolorname('green')
            call plchar(0.42,0.37,0.02,'iter.',0.0,-1)
            
            j=0
            do i=interval,ifun,interval*4
               call plnumb(0.52,0.37-0.02*j,0.02,real(i),0.0,-1)
            end do

            if(ifun.ge.interval*2)then
               call plchar(0.54,0.37,0.02,',',0.0,-1)
               call newcolorname('yellow')
               call plchar(0.58,0.37,0.02,'iter.',0.0,-1)
                j=0
                do i=interval*2,ifun,interval*4
                   call plnumb(0.68,0.37-0.02*j,0.02,real(i),0.0,-1)
                end do
                
                if(ifun.ge.interval*3)then
                   call plchar(0.70,0.37,0.02,',',0.0,-1)
                   call newcolorname('blue')
                   call plchar(0.74,0.37,0.02,'iter.',0.0,-1)
                   j=0
                   do i=interval*3,ifun,interval*4
                      call plnumb(0.84,0.37-0.02*j,0.02,real(i),0.0,-1)
                   end do

                   if(ifun.ge.interval*4)then
                      call plchar(0.86,0.37,0.02,',',0.0,-1)
                      call newcolorname('violet')
                      call plchar(0.90,0.37,0.02,'iter.',0.0,-1)
                      j=0
                      do i=interval*4,ifun,interval*4
                       call plnumb(1.00,0.37-0.02*j,0.02,real(i),0.0,-1)
                      end do
                   end if
                end if
             end if
          end if                
         end if

     
         
      
     
c     --draw cp axis
c     note that there are num_graph of cp plots
      call newcolorname('black')
     
      do pt=1,num_graph
         call gridaxis(x_org(pt+1), y_org(pt+1), x_scale(pt+1), 
     &        y_scale(pt+1), x_left,x_right, y_down, y_up,ymin(pt))
         
         call plchar(0.4,0.2,0.02,'Cp at point',0.0,-1)
         call plnumb(0.63,0.2,0.02,real(graph_pt(pt)),0.0,-1)
clb         write(*,*),'haha',graph_pt(pt),cd(1)
         call plchar(0.82,0.16,0.02,'cd=',0.0,-1)
         call plnumb(0.88,0.16,0.02,real(cd(graph_pt(pt))),0.0,5)
         call plchar(0.82,0.14,0.02,'cl=',0.0,-1)
         call plnumb(0.88,0.14,0.02,real(cl(graph_pt(pt))),0.0,5)
         call plchar(0.82,0.12,0.02,'cm=',0.0,-1)
         call plnumb(0.88,0.12,0.02,real(cm(graph_pt(pt))),0.0,5)
         call gridaxis(x_org(pt+1), y_org(pt+1), x_scale(pt+1), 
     &        y_scale(pt+1), x_left,x_right, y_down, y_up,ymin(pt))
      end do
      end



c     subroutine gridaxis: to draw the axis
      subroutine gridaxis  (x_org, y_org, x_scale, y_scale, 
     &      x_left, x_right, y_down, y_up,ymin)

      implicit none

      double precision  x_org, y_org, x_scale, y_scale, 
     &      x_left, x_right, y_down, y_up,ymin
      call newfactors(x_scale,y_scale)
      call plotabs ( x_org,y_org,-3)

c     -- middle axis --
      call xaxis(x_left, 0.0, x_right-x_left, 0.1, x_left, 0.1, 0.01, 2)

      if(ymin.eq.y_down)then
c     plotting it once may result in unexpected output
c     note for yaxis
c     argument: starting point of y axis(x cood. and y cood.), length of y axis, distance between annotations
C     first annotation value, delta annotation value, character width, number of digits to right of decimal point
         call yaxis(0.0 , y_down,  y_down-y_up, 0.1, y_down,0.1,0.01, 2)
      else
c     -- left axis --         
clb         call yaxis(0.0, y_down,y_down-y_up,0.1, ymin,-ymin/2, 0.015, 2)
         call yaxis(0.0, y_down,y_down-y_up,0.1, -ymin, ymin/2, 
     &        0.015, 2)
      end if
         
      end                       !gridaxis


