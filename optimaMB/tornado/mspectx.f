      subroutine mspectx(jdim,kdim,uu,ccx,xyj,xy,spect,                  
     &                   jbegin,jend,kbegin,kend)

      implicit none
c
#include "../include/common.inc"
c
      integer jdim,kdim,jbegin,jend,kbegin,kend,j,k
      double precision xyj(jdim,kdim),xy(jdim,kdim,4)                          
      double precision spect(jdim,kdim,3),uu(jdim,kdim),ccx(jdim,kdim)
      double precision temp,rj
c                                             
c     matrix spectral values
c                                                                       
      do 100 k=kbegin,kend                                         
      do 100 j=jbegin,jend                                        
        rj =1.d0/xyj(j,k)
        temp=(abs(uu(j,k))+ccx(j,k))*rj
        spect(j,k,1)=max(abs(uu(j,k))*rj,vlxi*temp)
        spect(j,k,2)=max(abs(uu(j,k)+ccx(j,k))*rj,vnxi*temp)
        spect(j,k,3)=max(abs(uu(j,k)-ccx(j,k))*rj,vnxi*temp)
 100  continue                                                     
c
      return                                                  
      end  


