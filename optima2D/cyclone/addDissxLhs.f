************************************************************************
      !-- Program name: addDissxLhs
      !-- Written by: Howard Buckley
      !-- Date: April 2010
      !-- 
      !-- If dissipation-based continuation is to be used as a
      !-- globalization method for N-K, then add additional 
      !-- dissipation to 1st order preconditioner for flow Jacobian
      !-- matrix.
************************************************************************

      subroutine  addDissxLhs(jdim,kdim,ndim,xyj,indx,icol,pa)

#ifdef _MPI_VERSION
      use mpi
#endif


      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
    
      !-- Declare variables

      integer
     &     j, jdim, jpl, jmi, k, kdim, ii, jj, n, ndim, indx(jdim,kdim),
     &     icol(9)

      double precision
     &     cf2xdc(jdim,kdim), c2, c2m,
     &     work(jdim,kdim,3), diag1, diag2, diag3,
     &     pa(jdim*kdim*ndim*ndim*5+1), xyj(jdim,kdim)

c$$$      call system('rm -f pa0.out')
c$$$      open(unit=4389,file='pa0.out',status='new',
c$$$     &         form='formatted') 
c$$$      write(4389,*) pa
c$$$      close(4389)

      !-- Calculate a scalar dissipation coefficient
      !-- governed by the continuation parameter 'lamDiss'

      do k = kbegin,kend
        do j = jbegin,jup
          jpl = jplus(j) 
          c2 = lamDiss*(spectx_dc(jpl,k)+spectx_dc(j,k))
          cf2xdc(j,k) = c2
        end do
      end do

      !-- Add aditional dissipation to 1st order preconditioner

      do j = jlow,jup
        do k = klow,kup
          jpl = jplus(j)
          jmi = jminus(j)
          c2 = cf2xdc(j,k)
          c2m = cf2xdc(jmi,k)

          diag1 = xyj(jmi,k)*c2m
          diag2 = xyj(j,k)  *(c2m + c2)
          diag3 = xyj(jpl,k)*c2
            
          do n =1,4
            ii = ( indx(j,k) - 1 )*ndim + n
            jj = ( ii - 1 )*icol(5)
            pa(jj+n)    = pa(jj+n)    - diag1
            pa(jj+icol(2)+n) = pa(jj+icol(2)+n) + diag2
            pa(jj+icol(4)+n) = pa(jj+icol(4)+n) - diag3
          end do                                         
        end do
      end do

c$$$      call system('rm -f pa1.out')
c$$$      open(unit=4389,file='pa1.out',status='new',
c$$$     &         form='formatted') 
c$$$      write(4389,*) pa
c$$$      close(4389)

      return
      end
