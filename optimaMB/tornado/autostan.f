c *******************************************************************
c ** Subroutine to automatically determine location of laminar     **
c ** to turbulent transition.  Works for multiple trips per block. **
c ** (in terms of percentage chord top/bottom for each airfoil)    **
c *******************************************************************
c calling routine: master
c mods: m. nemec, nov. 2002
c
c
      subroutine autostan(lvl,isequal,jdim,kdim,x,y,xc,yc,wout)

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

      integer lvl,isequal,jdim,kdim,ifoil,ii,i,kk,nstgstart,nstgend
      integer iblklo,iblkup,iblk,iblock,iside,iblkblnt,j,jj,nn,jle
      integer jkstart,jkend,iseg,i0,jjmp,iblkle,iupflag,iloflag,nextblk
      integer lenebor,isegle,j0,jl,ju,jdir,iblktran,iptr,jtran
      double precision x(jdim*kdim),y(jdim*kdim),xc(jdim),yc(jdim)
      double precision ielflag(maxfoil),iturbflag(maxfoil),xte,yte,xloc
      double precision isegblktmp(maxsegs,maxfoil),xtel,ytel,xteu,yteu
      double precision isegsidetmp(maxsegs,maxfoil),dist,dist2,x1,y1
      double precision xle,yle,gradc,gradp,dx,dy,ang,xtran,ytran,xle2,xq
      logical cgrd,hgrd,blunt,wout
c
c
c     **********************************************************************
c     - trip_present(blk#) =>  set to true if block contains trip point

c     - multi_trans(blk #) =>  set to true it there are more than one
c                              trip points set for the same side of a block

c     - itrans(blk #)      =>  number of trip points for given block

c     - itranside(blk #,1) =>  side that trip belongs to for lower
c                              surface trip point

c     - itranside(blk #,2) =>  side that trip belongs to for upper
c                              surface trip point
c     **********************************************************************
c
c      write (*,*) 'IN autostan'

      icoord=0
      ifoil = 0
      do 1000 ii=1,nfoils
        ielflag(ii) = 0
        iturbflag(ii) = 0
 1000 continue
c
      if (wout) then 
         write(n_out,*) 
         write(n_out,*) ('-',i=1,54)
         if (mg .or. gseq) then
            write(n_out,20) lvl
         else
            write(n_out,2)
         endif
         write(n_out,*) ('-',i=1,54)
         write(n_out,3)
         if (fturb) write(n_out,715)
 715     format(1x,1h|,14x,21h FULLY TURBULENT FLOW,17x, 1h|/, 1x, 1h|, 
     &        10x,30h IGNORE OUTPUT IN THIS SECTION, 12x, 1h|)
         write(n_out,1)
 1       format(1x,1h|,2x,'Note: Point numbers do not include',
     &                    ' halo pts.',6x,1h|)
 2       format(1x,1h|,15x,20hTransition Locations,17x,1h|)
 20      format(1x,1h|,10x,30hTransition Locations for Level,i2,10x,1h|)
 3       format(1x,1h|,52x,1h|)
      end if
c
csd   -the node number in itranj(blk,1) indicates lower transition pnt
csd   -the node number in itranj(blk,2) indicates upper transition pnt
csd    -they do not both necessariy have to exist for a given block
csd     but I am assuming that you have at most 2 transition points per
csd     block.

      do ii=1,maxblk
         itrans(ii)=0
         do kk=1,maxtrans
            itranj(ii,kk)=0
         enddo
      enddo

c main loop
c
      if (mg .or. gseq) then
        nstgstart = nstgtot/isequal*(lvl-1)+1
        nstgend   = nstgtot/isequal*lvl
      else
        nstgstart = 1
        nstgend   = nstg
      endif
c
c      write(n_out,777) nstgstart,nstgend,lvl,nstgtot,nstg
c 777  format('nstg',5i6)
c
      do 900 ii=nstgstart,nstgend
c
c       -determine the element number
c
        iblklo=0
        iblkup=0
        ifoil=0
        if (istgcnr(ii).eq.1.and.istgtyp(ii).eq.-1) then
           ifoil = abs(ibcdir(istgblk(ii),1))
           iblkup=istgblk(ii)
        endif
        if (istgcnr(ii).eq.4.and.istgtyp(ii).eq.-1) then
           ifoil = abs(ibcdir(istgblk(ii),3))
           iblklo=istgblk(ii)
        endif
c
        if (ifoil.ne.0) then
c         -check to see if we've already done this element
          if (iturbflag(ifoil).lt.0) goto 900
c
          if (translo(ifoil).eq.0.0.and.transup(ifoil).eq.0.0) then
c
c           -before running fully turbulent, check to see if transition
c            location was entered manually for this element in .con file.
            do j=1,nsegments(ifoil)
              iblk=isegblk(j,ifoil)
              iside=isegside(j,ifoil)
              if (jtranlo(iside,iblk).gt.0) iturbflag(ifoil) = 2
              if (jtranup(iside,iblk).gt.0) iturbflag(ifoil) = 2
            enddo
c
            if (iturbflag(ifoil).eq.0) then
               if (wout) then
                  write(n_out,3)
                  write(n_out,115) ifoil
                  write(n_out,116)
               end if
              iturbflag(ifoil) = 1
            endif
            if (iturbflag(ifoil).ne.2) goto 900
          endif
          iturbflag(ifoil)=-1
        endif
 115    format(1x,'|  No transition specified on foil #',i2,'.',14x,'|')
 116    format(1x,'|  => fully turbulent!',31x,'|')
c
c        write (*,*) 'ifoil',ifoil

        blunt=.false.
        cgrd=.false.
        hgrd=.false.

        if (ifoil.eq.0) goto 900

        if (iblklo.eq.0) then
          iblock=lblkpt(iblkup,4)
          iblk=iblock
          iblock=lblkpt(iblock,1)
c
          if (ibctype(iblock,2).eq.1 .or. ibctype(iblock,4).eq.1) then
c           -we have a blunt trailing edge
c           -note: if you have a blunt trailing edge you may get a value
c                  greater than 2 stored in itrans.  Don't worry,
c                  information is really only duplicated and the first
c                  two array allocations are used.
            blunt=.true.
            iblkblnt=iblock
            iblk=iblock
            if (ibctype(iblock,2).eq.1) then
              iblock=lblkpt(iblock,1)
            else
              iblock=lblkpt(iblock,3)
            endif
          endif
c
c         -if the blocks touching the lower surface had the same
c          x and y orientation as the ones above, then you would 
c          want to point to side 2 of block iblock for the next line.
c          i.e. this is what happens for H-grid
c         -but if all blocks that touch the airfoil element,
c          do so with side 1 (C-grd), the orientation is exactly opposite,
c          upside-down really for blocks on lower surface, so you must
c          ask to point in direction of side 4 instead. 
c
          if (lblkpt(iblock,1).eq.iblk) cgrd=.true.
          if (lblkpt(iblock,3).eq.iblk) hgrd=.true.
c
          if (cgrd) then
            iblklo=lblkpt(iblock,4)
          elseif (hgrd) then
            iblklo=lblkpt(iblock,2)
          endif
        endif
c
        if (wout) then
           write(n_out,3)
           write(n_out,4) ifoil
 4         format(1x,1h|,2x,9hElement #,i2,39x,1h|)
           if (cgrd) then
              write(n_out,5) 
 5            format(1x,1h|,4x,31hA C-type mesh for this element.,17x
     $             ,1h|)
           else if (hgrd) then
              if (blunt) write(n_out,7)
 7            format(1x,1h|,4x,33h<<<<   Blunt Trailing Edge   >>>>,15x
     $             ,1h|)
              write(n_out,6) 
 6            format(1x,1h|,4x,32hAn H-type mesh for this element.,16x
     $             ,1h|)
           endif
           write(n_out,8) iblkup,iblklo
 8         format(1x,1h|,4x,7hiblkup=,i3,5x,7hiblklo=,i3,23x,1h|)
        end if
c
c        write(n_out,*) 'cgrd,hgrd,blunt',cgrd,hgrd,blunt
c
c       -go along element from lower-surface trailing edge to
c        upper-surface trailing edge and put block numbers in a list.
c
cc        if (cgrd) then
cc          -note: June 25 1999
cc                 -this method works if all the sides touching
cc                  airfoil surface is side 1.
cc                 -if you generate grid by hand for some arbitrary
cc                  airfoil shape you may have a c-mech type grid
cc                  around most of element but it is possible to have sides
cc                  2,3 or 4 touch the airfoil ... in which case this branch
cc                  of code breaks down.
cc          kk=1
cc          isegblk(1,ifoil)=iblklo
cc          if (iblklo.ne.iblkup) then
cc 22         kk=kk+1
cc            isegblk(kk,ifoil)=lblkpt(isegblk(kk-1,ifoil),4)
cc            if (isegblk(kk,ifoil).ne.iblkup) goto 22
ccc            if (kk.lt.nsegments(ifoil)-j0) goto 22
ccc            kk=kk+1
ccc            isegblk(kk,ifoil)=iblkup
cc          endif
cc          if (blunt) isegblk(kk+1,ifoil)=iblkblnt 
ccc
cc          do j=1,nsegments(ifoil)
cc            do kk=1,4
cc              if (ibctype(isegblk(j,ifoil),kk).eq.1) then
cc                if (abs(ibcdir(isegblk(j,ifoil),kk)).eq.ifoil) then
ccc                 -this is the side that forms part of airfoil surface
cc                  isegside(j,ifoil)=kk
cc                endif
cc              endif
cc            enddo
cc          enddo
cc        else
c         -you could probably do this for cgrd case aswell          
c         -just reorder isegblk so that it starts with iblklo.
          do j=1,nsegments(ifoil)
            isegblktmp(j,ifoil)=isegblk(j,ifoil)
            isegsidetmp(j,ifoil)=isegside(j,ifoil)
            if (isegblk(j,ifoil).eq.iblklo) jj=j
          enddo
c
          do j=jj,jj+nsegments(ifoil)-1
            nn=j
            if (j.gt.nsegments(ifoil)) nn=j-nsegments(ifoil)
            kk=j-jj+1
            isegblk(kk,ifoil)=isegblktmp(nn,ifoil)
            isegside(kk,ifoil)=isegsidetmp(nn,ifoil)
          enddo
cc        endif
c
c        write(n_out,*)'ifoil=',ifoil
c        do 550 iseg=1,nsegments(ifoil)
c        write(n_out,*)'block,side',isegblk(iseg,ifoil),isegside(iseg,ifoil)
c 550    continue
c        write(n_out,*)
c
c       -find trailing edge coordinates (or average for a blunt t.e.).
c
c       -finding coordinate using iblklo
        iblk=isegblk(1,ifoil)
        iside=isegside(1,ifoil)
        call getbounds(iblk,iside,jkstart,jkend)
        xtel = x(jkstart)
        ytel = y(jkstart)
c
c       -finding coordinate using iblkup
        iblk=isegblk(nsegments(ifoil),ifoil)
        iside=isegside(nsegments(ifoil),ifoil)
        call getbounds(iblk,iside,jkstart,jkend)
        xteu = x(jkend)
        yteu = y(jkend)
c
c        write(n_out,551) xtel,ytel,xteu,yteu
c 551    format(4h xs=,f8.4,4x,3hys=,f8.4,4x,3hxe=,f8.4,4x,3hye=,f8.4)
c
        if (blunt) then
          xte=.5d0*(xteu+xtel)
          yte=.5d0*(yteu+ytel)
        else
          xte=xteu
          yte=yteu
        endif
c        xtrailedge(ifoil)=xte
c        ytrailedge(ifoil)=yte
c
c       -find leading edge coordinate.
        dist2 = 0.d0
        dist = 0.d0
        iseg=1
c       -start from lower surface trailing edge:
        iblock = iblklo
   70   continue
c       => (-1) added on to end of i0 calc. to offset addition of jj below
        i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
c       -this block side is one of possibly many segments that make up
c        the element surface, the variable/counter 'iseg' is keeping
c        track of just which segment this is ... we want to know the side
c        number of that segment
        iside=isegside(iseg,ifoil)
        if (iside.eq.1) then
          jjmp = 0
        else
c         -side 3
          jjmp = jbmax(iblock) + 2*nhalo
        endif
        do 80 jj=1,jbmax(iblock)
          icoord=icoord+1
          x1 = x(i0+jj + (kbmax(iblock)-1)*jjmp)
          y1 = y(i0+jj + (kbmax(iblock)-1)*jjmp)
          xcoord(icoord)=x1
          ycoord(icoord)=y1
c          write (n_out,888) iblock,iside,jj, icoord, xcoord(icoord
c     &         ),ycoord(icoord)
c 888      format(4i5,2e16.5)

          dist2 = (xte - x1)**2 + (yte - y1)**2
          if (dist2.gt.dist) then
            iblkle = iblock
            jle = jj
            xle = x1
            yle = y1
            dist = dist2
            iupflag = 0
            iloflag = 1
          endif
   80   continue

c     -- check memory declaration --
        if (icoord.gt.maxjfoil) then
           write (n_out,*) 'Insufficient memory in autostan'
           write (n_out,*) 'Increase maxjfoil'
           call flush(n_out)
        end if

        if (cgrd) then
          nextblk = lblkpt(iblock,4)
        else
          iseg=iseg+1
          if (iseg.gt.nsegments(ifoil)) goto 81
          nextblk = isegblk(iseg,ifoil)
        endif
c
csd     I think this if statement is redundant.
        if (ibctype(nextblk,1).eq.1 .or. ibctype(nextblk,3).eq.1) then
           iblock = nextblk
c     write (n_out,*) 'going 70',iblock,nextblk
           goto 70
        endif
 81     continue
c
        if (wout) then
           write(n_out,3)
           write(n_out,11) iblkle,jle
           write(n_out,9) xte,yte
 9         format(1x,1h|,4x,14hTE : (x,y) = (,f9.4,1h,,f9.4,1h),14x,1h|)
           write(n_out,10) xle,yle
 10        format(1x,1h|,4x,14hLE : (x,y) = (,f9.4,1h,,f9.4,1h),14x,1h|)
 11        format(1x,1h|,4x,9hLE Block=,i3,5x,4hjle=,i4,23x,1h|)
        end if
cc
c       calculate slope of chord line
        gradc=(yte-yle)/(xte-xle)
        if (matcharc2d) gradc=0.d0
c       calculate slope of line perpendicular to the chord (i.e. normal)
        if (abs(gradc).gt.1e-09) gradp = -1.0/gradc
c        write(n_out,*) 'gradc,gradp',gradc,gradp
c        write(n_out,*) 'trasup=',transup(ifoil)
c        write(n_out,*) 'traslo=',translo(ifoil)
c
c       set transition point on lower surface :
c       locate grid point closest to specified lower transition point
c       -start from iblklo and continue searching clockwise but stop 
c       looking if you've passed iblkle i.e. do not search in 
c       nebouring block until iblkle's right => lenebor
c        
c
c       -find angle between chord and horizontal
c        .... trailing edge is origin      
        dx=xle-xte
        dy=yle-yte
#ifdef IBM
        ang=-datan(dble(dy/dx))
#else
#ifdef YMP
        ang=-atan(dy/dx)
#else
        ang=-datan(dy/dx)
#endif
#endif
        dist=1.d0
        iblock=iblklo
        iseg=1
        do j=1,nsegments(ifoil)
           if (iblkle.eq.isegblk(j,ifoil)) then
              if (j.eq.nsegments(ifoil)) then
                 if (ibctype(iblkup,1).eq.1) then
                    lenebor=lblkpt(iblkup,4)
                 elseif (ibctype(iblkup,3).eq.1) then
                    lenebor=lblkpt(iblkup,2)
                 endif
              else
                 lenebor=isegblk(j+1,ifoil)
              endif
              isegle=j
           endif
        enddo
  110   continue
c
c         
c       -note: the term i0 has nhalo in it so that it points
c              to the point preceding the first point of the block
c              in memory.
        j0 = jbmax(iblock)
        i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
        if (ibctype(iblock,1).eq.1) then
          jjmp = 0
          jl=1
          ju=j0
          jdir=1
          if (iblock.eq.iblkle) ju = jle
        elseif (ibctype(iblock,3).eq.1) then
          jjmp = jbmax(iblock) + 2*nhalo
          jl=j0
          ju=1
          jdir=-1
          if (iblock.eq.iblkle) ju = jle
        endif
        
c       -put this if statement in June 22/99 
        if (jtranlo(1,iblock).gt.0) then
c         -if a number is set in the .con file then use that one
          itrans(iblock)=itrans(iblock)+1
          itranj(iblock,1)=jtranlo(1,iblock) + nhalo
          itranside(iblock,1)=1
          iblktran=iblock
          iptr=i0+jtranlo(1,iblock) + (kbmax(iblock)-1)*jjmp
          xtran=x(iptr)
          ytran=y(iptr)
          goto 222
        endif
c
        if (jtranlo(3,iblock).gt.0) then
c         -if a number is set in the .con file then use that one
          itrans(iblock)=itrans(iblock)+1
          itranj(iblock,1)=jtranlo(3,iblock) + nhalo
          itranside(iblock,1)=3
          iblktran=iblock
          iptr=i0+jtranlo(3,iblock) + (kbmax(iblock)-1)*jjmp
          xtran=x(iptr)
          ytran=y(iptr)
          goto 222
        endif
c     
c
        dx=xle-xte
        dy=yle-yte
        xle2= xte + dx*cos(ang) - dy*sin(ang)
c       yle2= yte + dx*sin(ang) + dy*cos(ang)
        do 119 jj=jl,ju,jdir
c         -rotate element about trailing edge to make chord horizontal
          xc(jj)= x(i0+jj + (kbmax(iblock)-1)*jjmp)
          yc(jj)= y(i0+jj + (kbmax(iblock)-1)*jjmp)
          dx=xc(jj)-xte
          dy=yc(jj)-yte
          xc(jj)= xte + dx*cos(ang) - dy*sin(ang)
c         yc(jj)= yte + dx*sin(ang) + dy*cos(ang)
 119    continue

c     write(n_out,*) 'help',iblkle,jle,jl,ju
c     call flush(n_out)

        do 120 jj=jl,ju,jdir
          iptr=i0+jj + (kbmax(iblock)-1)*jjmp
          xq= x(iptr)
          xloc = abs((xle2 - xc(jj))/(xle2 - xte))
          if (matcharc2d) then
            xloc=xq
            if (xloc .lt. translo(ifoil)) then
              iblktran = iblock
              jtran = jj
              xtran = xq
              ytran = y(iptr)
              goto 121
            endif
          else
            dist2 = abs(translo(ifoil)-xloc)
            if (dist2.lt.dist) then
              iblktran = iblock
              jtran = jj
              xtran = xq
              ytran = y(iptr)
              dist = dist2
            endif
          endif
 120    continue
        if (cgrd) then
          nextblk = lblkpt(iblock,4)
        else
          iseg=iseg+1
          if (iseg.gt.nsegments(ifoil)) goto 121
          nextblk = isegblk(iseg,ifoil)
        endif
c     
        if ((ibctype(nextblk,1).eq.1 .or. ibctype(nextblk,3).eq.1) 
     &        .and. nextblk.ne.lenebor)then
          iblock = nextblk
          goto 110
        endif
c     
 121    itrans(iblktran)=itrans(iblktran)+1
        itranj(iblktran,1)=jtran + nhalo
        do i=1,nsegments(ifoil)
          if (iblktran.eq.isegblk(i,ifoil)) then
            itranside(iblktran,1)=isegside(i,ifoil)
          endif
        enddo
c     

 222    if (wout) then
           write(n_out,3)
           write(n_out,12)
           write(n_out,13) iblktran
           write(n_out,14) itranside(iblktran,1), itranj(iblktran,1)
     $          -nhalo, itrans(iblktran) 
           write(n_out,15) xtran,ytran
 12        format(1x,1h|,4x,24hLower Surface Transition,24x,1h|)
 13        format(1x,1h|,6x,6hBlock=,i3,37x,1h|)
 14        format(1x,1h|,6x,6hside =,i3,5x,2hj=,i4,5x,7h#pnts=,
     $          i3,11x,1h|)
 15        format(1x,1h|,4x,14h     (x,y) = (,f9.6,1h,,f9.6,1h),14x,1h|)
        end if
c       write(n_out,*) 'iblktran  =',iblktran,itrans(iblktran)
c       write(n_out,*) 'jtran     =',itranj(iblktran,1)
c       write(n_out,*) 'itranside =',itranside(iblktran,1)
c     
c
c       ****************************************************************
c       set transition point on upper surface :
c       locate grid point closest to specified upper transition point
c       -start from iblkup and continue searching counter-clockwise
c       but stop looking if you've passed iblkle i.e. do not search
c       in nebouring block to iblkle's left => lenebor
c
c        write (*,*) 'going to upper surface',iblkle

        dist = 1.d0
        iblock = iblkup
c       -sometimes both side 1 and side 3 can be part of an element
c        surface.  i.e. side 3 can be lower surface of slat and side 1 can
c        upper surface of main element.  So checking if ibctype of side 1
c        or 3 (of block iblklo) is equal to 1 doesn't tell you if your
c        dealing with an H-grd or a C-grd.
c
c        write (*,*) 'isegle',isegle, cgrd,iblkup
c        do iseg=1,maxsegs
c           write (*,*) 'iseg',isegblk(iseg,1)
c        end do

        if (isegle.gt.1) then
          lenebor=isegblk(isegle-1,ifoil)
        else
          if (cgrd) then
            lenebor=lblkpt(iblklo,2)
          else
            lenebor=lblkpt(iblklo,4)
          endif
        endif

c        write(*,*) 'lenebor upper',lenebor

        iseg=nsegments(ifoil)
 90     continue
        i0 = lgptr(iblock)+nhalo*(jbmax(iblock)+2*nhalo)+nhalo-1
        dx=xle-xte
        dy=yle-yte
        xle2= xte + dx*cos(ang) - dy*sin(ang)
        if (ibctype(iblock,1).eq.1) then
          jl=jbmax(iblock)
          ju=1
          jdir=-1
          if (iblock.eq.iblkle) ju = jle
        elseif (ibctype(iblock,3).eq.1) then
          jl=1
          ju=jbmax(iblock)
          jdir=1
          if (iblock.eq.iblkle) ju = jle
        endif
c
c        write(*,*) 'lenebor upper2'

        if (jtranup(1,iblock).gt.0) then
c         -if a number is set in the .con file then use that one
          itrans(iblock)=itrans(iblock)+1
          itranj(iblock,2)=jtranup(1,iblock) + nhalo
          itranside(iblock,2)=1
          iblktran=iblock
          iptr=i0+jtranup(1,iblock)
          xtran=x(iptr)
          ytran=y(iptr)
c          write (*,*) 'going333'
          goto 333
        endif

c        write(*,*) 'jl,ju,jdir',jl,ju,jdir

        do 99 jj=jl,ju,jdir
c         -rotate element about trailing edge to bring horizontal
          xc(jj)=x(i0+jj)
          yc(jj)=y(i0+jj)
          dx=xc(jj)-xte
          dy=yc(jj)-yte
          xc(jj)= xte + dx*cos(ang) - dy*sin(ang)
 99     continue

c        write(*,*) 'done 99'

        do 100 jj=jl,ju,jdir
          xq=x(i0+jj)
          xloc = abs((xle2 - xc(jj))/(xle2 - xte))
          if (matcharc2d) then
            xloc=xq
            if (xloc .lt. transup(ifoil)) then
              iblktran = iblock
              jtran = jj
              xtran = xq
              ytran = y(i0+jj)
              goto 101
            endif
          else
            dist2 = abs(transup(ifoil)-xloc)
c            write (*,*) dist2,dist
            if (dist2.lt.dist) then
              iblktran = iblock
              jtran = jj
              xtran = xq
              ytran = y(i0+jj)
              dist = dist2
            endif
          endif
 100    continue
c        write(*,*) 'done 100',iblktran,jtran,xtran,ytran

        if (cgrd) then
c          write (*,*) 'next A'
          nextblk = lblkpt(iblock,2)
        else
          iseg=iseg-1
c     mn: comment out if (iseg.ne.0) for original code but keep what's
c     in the if statement 
          if (iseg.ne.0) then
c             write (*,*) 'next B', iseg, ifoil,isegblk(iseg,ifoil)
             nextblk = isegblk(iseg,ifoil)
          end if
        endif
c        write(*,*) 'done 100A'

c     mn: comment out if (iseg.ne.0) for original code but keep what's
c     in the if statement     
        if (iseg.ne.0) then
           if ((ibctype(nextblk,1).eq.1 .or. ibctype(nextblk,3).eq.1) 
     &          .and. nextblk.ne.lenebor)then
              iblock = nextblk
c              write(*,*) 'going 90'
              goto 90
           endif
        end if

 101    continue
c        write (*,*) '101'
        itrans(iblktran)=itrans(iblktran)+1
        itranj(iblktran,2)=jtran + nhalo
        do i=1,nsegments(ifoil)
          if (iblktran.eq.isegblk(i,ifoil)) then
            itranside(iblktran,2)=isegside(i,ifoil)
          endif
        enddo
c     
 333    if (wout) then
           write(n_out,3)
           write(n_out,16)
           write(n_out,13) iblktran
           write(n_out,14) itranside(iblktran,2),itranj(iblktran,2)
     $          -nhalo,itrans(iblktran) 
           write(n_out,15) xtran,ytran
 16        format(1x,1h|,4x,24hUpper Surface Transition,24x,1h|)
c
c        write(n_out,*) 'iblktran  =',iblktran,itrans(iblktran)
c        write(n_out,*) 'jtran     =',itranj(iblktran,2)
c        write(n_out,*) 'itranside =',itranside(iblktran,2)
c        write(n_out,*)
c        write(n_out,*)
c
           write(n_out,3)
           write(n_out,3)
           write(n_out,3)
        end if
 900  continue
c      write (*,*) 'at 900', blunt
c
      if (wout) then
         write(n_out,3)
         write(n_out,*) ('-',i=1,54)
      end if
c
c     -for blunt trailing edges, need to add coordinates to xcoord
c      and ycoord now since we never swept through those blocks to find
c      transition point
      if (blunt) then
        i0 = lgptr(iblkblnt)+nhalo*(jbmax(iblkblnt)+2*nhalo)+nhalo-1
        jjmp = jbmax(iblkblnt) + 2*nhalo
c       -if side 2 is touching body then i0 and jjmp are okay as is
c       -if side 4 is touching body then adjust i0
        if (ibctype(iblkblnt,4).eq.1) i0 = i0 + jbmax(iblkblnt)-1 
        do 60 kk=1,kbmax(iblkblnt)
          icoord=icoord+1
          x1 = x(i0+1 + (kk-1)*jjmp)
          y1 = y(i0+1 + (kk-1)*jjmp)
          xcoord(icoord)=x1
          ycoord(icoord)=y1
 60     continue
      endif
c     for debugging purposes.  
c      write(n_out,*)
c      write(n_out,*)
c      write(n_out,*)
c      write(n_out,*)
c      do ii=1,maxblk
c        if (itrans(ii).gt.0) then
c          i0 = lgptr(ii)+nhalo*(jbmax(ii)+2*nhalo)-1
c          if (ibctype(ii,1).eq.1) jjmp = 0
c          if (ibctype(ii,3).eq.1) jjmp = jbmax(ii) + 2*nhalo
c          write(n_out,*) 'block ',ii,i0,jjmp
c          jj=itranj(ii,1)
c          if (jj.ne.0) then
c            xp = x(i0+jj + (kbmax(ii)-1)*jjmp)
c            yp = y(i0+jj + (kbmax(ii)-1)*jjmp)
c            write(n_out,*) 'jt,x,y',jj-nhalo,xp,yp
c          endif
c          jj=itranj(ii,2)
c          if (jj.ne.0) then
c            xp = x(i0+jj + (kbmax(ii)-1)*jjmp)
c            yp = y(i0+jj + (kbmax(ii)-1)*jjmp)
c            write(n_out,*) 'jt,x,y',jj-nhalo,xp,yp
c          endif
c          write(n_out,*)
c        endif
c      enddo
c
c      write (*,*) 'done autostan'
      return
      end
