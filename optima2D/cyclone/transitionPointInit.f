      ! ================================================================
      ! ================================================================
      ! 
      subroutine transitionPointInit(jdim,kdim,x,y)
      ! 
      ! ================================================================
      ! ================================================================
      !
      ! Purpose: This subroutine will initialize the necessary variables
      ! based on the current iterations transition points (transup and
      ! translo)
      !
      ! Input:
      !   transup = upper transition location (x/c)
      !   translo = lower transition location (x/c)
      ! 
      ! Output:
      !   jtranup = upper surface transition node (nearest upstream node)
      !   jtranlo = lower surface transition node (nearest upstream node)
      !   jmid    = dividing node between upper and lower surfaces 
      !   d_trip  = distance of a given node to its respective transition 
      !             point
      !   delta_xup = grid spacing at upper transition point
      !   delta_xlo = grid spacing at lower transition point
      !
      ! Notes: 
      !   Most variables are GLOBAL variables (declared in arcom)
      ! 
      ! Author: Ramy Rashad
      ! ----------------------------------------------------------------
      
#ifdef _MPI_VERSION
      use mpi
#endif
      implicit none
      
#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
      
     
      ! _____________________
      ! Variable declarations
      logical 
     &   foundjtup, foundjtlo
      integer 
     &   j, k, jte, jdim, kdim
      double precision 
     &   x(jdim,kdim), y(jdim,kdim), fmin, xte, yte, 
     &   distx, disty, dist, dist2, chordTP, chordTP_2,
     &   ytranslo, ytransup, epsTP
      
      ! _____________________
      ! Function declarations
      double precision 
     &   linearInterp

      ! ______________________
      ! find leading edge node
      jte=jtail1
      xte=x(jte,1)
      yte=y(jte,1)
      do j=jtail1,jtail2
         distx=x(j,1)-xte
         disty=y(j,1)-yte
         dist2=sqrt(distx**2+disty**2)
         if (dist2.gt.dist) then
            dist=dist2
            jLEsetup=j
         endif
      enddo

      !  __________________________
      !  Transition Point Placement
      !  
      !  Assign nodes (jtranup & jtranlo) based on the transition point
      !  locations (transup & translo). The transition node is selected
      !  as the nearest upstream node to the transition point location
      !  specified on the given surface.

      foundjtup = .false.
      foundjtlo = .false.


      ! __________________________________
      ! Initial Guess on Transition Points
      if (iterTP.eq.1) then
         if (.not.TPGuessOveride) then
            ! Base initial guess on AoA (alpha) value
            if (alpha.gt.5) then
               transup = 0.20
               translo = 0.90
            else if (alpha.gt.2 .and. alpha.le.5) then
               transup = 0.40
               translo = 0.80
            else if (alpha.gt.0 .and. alpha.le.2) then
               transup = 0.60
               translo = 0.70
            else if (alpha.eq.0) then ! for symmetry on symmetric airfoils
               transup = 0.70
               translo = 0.70
            else if (alpha.ge.-2 .and. alpha.lt.0) then
               transup = 0.70
               translo = 0.60
            else if (alpha.ge.-5 .and. alpha.lt.-2) then
               transup = 0.80
               translo = 0.40
            else if (alpha.lt.-5) then
               transup = 0.90
               translo = 0.20
            end if
         else
            transup = transup_initGuess
            translo = translo_initGuess
         end if ! TPGuessOveride
      end if ! iterTP

      ! _____________
      ! lower surface
      do j = jtail1,jLEsetup
         if(x(j,1).le.translo)then                                
            ! take closest node:
            !jtranlo = j
            !d1 = translo - x(jtranlo,1)
            !d2 = x(jtranlo-1,1) - translo
            !if (d1.gt.d2) jtranlo = jtranlo-1
            ! take upstream node:
            jtranlo = j
            foundjtlo = .true.
            goto 451
         endif
      end do
 451  continue

      ! _____________
      ! upper surface
      do j = jLEsetup,jtail2
         if(x(j,1).ge.transup)then   
            ! take closest node:
            !jtranup = j
            !d1 = transup - x(jtranup,1)
            !d2 = x(jtranup+1,1) - transup
            !if (d1.gt.d2) jtranup = jtranup+1
            ! take upstream node:
            jtranup = j - 1
            foundjtup = .true.
            goto 453
         endif 
      end do
 453  continue

      ! If user enters x/c = 0 for transition points, set transition
      ! nodes to leading edge node. Also, in this case, the ft1 and ft2
      ! terms in the SA model will be manually set to 0 (via
      ! flagTPsAsZero=true)
      epsTP = 1.0E-10
      if (transup.lt.epsTP .and. translo.lt.epsTP) then
         flagTPsAsZero = .true.
         foundjtup     = .true.
         foundjtlo     = .true.
         jtranlo       = jLEsetup
         jtranup       = jLEsetup
      end if

      ! _______________________________________________________
      ! Make sure transition node is not on trailing edge node.
      ! If so, move transition node upstream by one node.
      if (foundjtlo .and. jtranlo.eq.jtail1) jtranlo = jtail1 + 1
      if (foundjtup .and. jtranup.eq.jtail2) jtranup = jtail2 - 1

      ! _____________________________
      ! If transition node not found:
      ! Set transition node equal to leading edge node
      if ( .not.foundjtup ) jtranup = jLEsetup
      if ( .not.foundjtlo ) jtranlo = jLEsetup

      ! ____________________________________________________________
      ! Find & Store dividing node between upper and lower surfaces:
      ! This is done with no a priori knowledge of the flow solution
      ! (or stagnation point) by simply finding the geometric
      ! mid-point between the upper and lower transition nodes.
      if ( jtranlo.ne.jtranup ) then
         ! compute chord length between lo and up transition points
         chordTP=0.0
         fmin=100.0
         do j=jtranlo,jtranup-1
            chordTP=chordTP+sqrt((x(j,1)-x(j+1,1))**2+
     &           (y(j,1)-y(j+1,1))**2)
         enddo
         ! find middle point using chord length
         chordTP_2=0.5*chordTP
         chordTP=0.0
         do j=jtranlo,jtranup-1
            chordTP=chordTP+sqrt((x(j,1)-x(j+1,1))**2+
     &           (y(j,1)-y(j+1,1))**2)
            if (abs(chordTP-chordTP_2).lt.fmin) then
               fmin=abs(chordTP-chordTP_2)
               jmid=j+1
            endif
         enddo
      else 
         ! fully-turbulent (such that jtranlo = jtranup = jLEsetup)
         jmid = jLEsetup
      end if ! jtranlo.ne.jtranup
      
      ! Interpolate for ytransup and ytranslo
      ! > ytransup/lo is the y-value of the up/lo transition pt.
      ! > transup/lo is the x-value (user input) of the up/lo transition pt.
      ytranslo = linearInterp(x(jtranlo,1),y(jtranlo,1),
     &     x(jtranlo-1,1),y(jtranlo-1,1),
     &     translo)
      ytransup = linearInterp(x(jtranup,1),y(jtranup,1),
     &     x(jtranup+1,1),y(jtranup+1,1),
     &     transup)

      ! _____________________________________________________________
      ! Compute & Store geometric distance of each node to trip point
      do k=klow,kup
         do j=jlow,jmid                  
            d_trip(j,k) = sqrt(
     &           (x(j,k)-translo)**2 + (y(j,k)-ytranslo)**2)
         enddo
         do j=jmid+1,jup
            d_trip(j,k) = sqrt(
     &           (x(j,k)-transup)**2 + (y(j,k)-ytransup)**2)
         enddo
      enddo
      
      ! __________________________________________________
      ! Compute & Store the grid spacing at the trip nodes
      delta_xlo=0.5*(sqrt((x(jtranlo-1,1)-x(jtranlo,1))**2+
     &     (y(jtranlo-1,1)-y(jtranlo,1))**2))
      delta_xup=0.5*(sqrt((x(jtranup+1,1)-x(jtranup,1))**2+
     &     (y(jtranup+1,1)-y(jtranup,1))**2))
      
      return
      end subroutine transitionPointInit
