c     These are 2 key routines used to evaluate the b-spline basis
c     functions and b-spline derivatives.

c-----------------------------------------------------------------------
c     calc. the value of all possibly nonzero b-spline basis functions
c     based on: carl de boor, 'practical guide to splines' pg. 134
c     
c     written by: marian nemec
c     date: Feb. 16, 2000
c-----------------------------------------------------------------------
      subroutine Bval ( knot, t, left, BIatT)

      implicit none

#include "bsord.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot 

      integer i, k, kp1, left
      
      double precision knot(iorder+nc+1), t, BIatT(ibsord), tmp 
      double precision deltaR(ibsord), deltaL(ibsord), saved

      k = 1
      BIatT(1) = 1.d0
      if (k.ge.iorder) return

      do while (k.lt.iorder)
        kp1 = k+1
        deltaR(k) = knot(left+k) - t
        deltaL(k) = t - knot(left+1-k)
        saved = 0.d0

        do i = 1,k
          tmp = BIatT(i)/(deltaR(i) + deltaL(kp1-i))
          BIatT(i) = saved + deltaR(i)*tmp
          saved = deltaL(kp1-i)*tmp
        end do
        BIatT(kp1) = saved
        k = kp1
      end do

      return 
      end                       !Bval

c-----------------------------------------------------------------------
c     -- calc. the j-th derivative of a b-spline curve --
c     -- jderiv is the order of the derivative --
c     -- knot(i) < t <= knot(i+1)
c     
c     based on: carl de boor, 'practical guide to splines' pg. 144
c     
c     written by: marian nemec
c     date: dec. 2000
c-----------------------------------------------------------------------
      double precision function dBsp (knot, bcp, t, i, jderiv)

      implicit none
      
#include "bsord.inc"

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot 

      integer jderiv, i, ilo, imk, j, jc, jcmin, jcmax, jj, kmj
      integer km1, nmi, n

      double precision knot(iorder+nc+1), t, bcp(nc+1)
      double precision aj(ibsord), dl(ibsord), dr(ibsord), fkmj

c     -- sorry about the goto statements but it's how C. de Boor has it
c     and it works well --

      n  = nc + 1
      dBsp = 0.d0
      
      if (jderiv .ge. iorder) goto 99

      km1 = iorder - 1
      if (km1 .gt. 0) goto 1
      dBsp = bcp(i)
      goto 99

 1    jcmin = 1
      imk = i - iorder
      if (imk .ge. 0) goto 8

      jcmin = 1 - imk

      do 5 j = 1,i
 5    dl(j) = t - knot(i+1-j)

      do 6 j = i,km1
        aj(iorder-j) = 0.d0
 6    dl(j) = dl(i)

      goto 10

 8    do 9 j = 1,km1
 9    dl(j) = t - knot(i+1-j)

 10   jcmax = iorder
      nmi = n - i
      if (nmi .ge. 0 ) goto 18
      jcmax = iorder + nmi
      do 15 j = 1,jcmax
 15   dr(j) = knot(i+j) - t

      do 16 j = jcmax,km1
        aj(j+1) = 0.d0
 16   dr(j) = dr(jcmax)

      goto 20

 18   do 19 j = 1,km1
 19   dr(j) = knot(i+j) - t

 20   do 21 jc = jcmin,jcmax
 21   aj(jc) = bcp(imk + jc)

      if (jderiv.eq.0) goto 30

      do 23 j = 1,jderiv
        kmj = iorder - j
        fkmj = dble(kmj)
        ilo = kmj
        do 23 jj = 1,kmj
          aj(jj) = ( ( aj(jj+1) - aj(jj) ) / ( dl(ilo) + dr(jj)))*fkmj
 23   ilo = ilo - 1

 30   if (jderiv .eq. km1) goto 39
      do 33 j = jderiv+1, km1
        kmj = iorder - j
        ilo = kmj
        do 33 jj = 1,kmj
          aj(jj) = ( aj(jj+1)*dl(ilo) + aj(jj)*dr(jj) )/( dl(ilo) +
     &          dr(jj) ) 
 33   ilo = ilo - 1
        
 39   dBsp = aj(1)
      
 99   return
      end                       !dBsp
c-----------------------------------------------------------------------
