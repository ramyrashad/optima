c---------------------------------------------------------------------
c     -- far-field boundary conditions for viscous flow --
c     -- extrapolation is 0-order in space --
c     -- based on a. pueyo's routine impfarv--
c     -- m. nemec, november 2000 --
c---------------------------------------------------------------------
      subroutine vimpfar (jdim, kdim, ndim, indx, icol, iex, q, xy,
     &      xyj, x, y, as, pa, imps, bcf, press)

#include "../include/arcom.inc"

      integer j, k, n1, n2, n3, nn, jdim, kdim, ndim,jsta,jsto
      integer indx(jdim,kdim), iex(jdim,kdim,ndim), icol(9)
      
      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim), press(jdim,kdim)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision bcf(jdim,kdim,4)

      double precision m(4,4), pn(4,4), cc(4,4), c1(4,4) 

      logical imps
      
      alphar = alpha*pi/180.d0     
      hstfs  = 1.d0/gami + 0.5d0*fsmach**2d0

c     -----------------------------------------------------------------
c     -- outer-boundary  k = kmax --
c     -----------------------------------------------------------------
      k = kend  

      if(.not.periodic) then
         jsta=jbegin+1
         jsto=jend-1
      else
         jsta=jbegin
         jsto=jend
      end if

      do j =jsta,jsto

c     -- metrics terms --
        par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
        xy3 = xy(j,kend,3)/par
        xy4 = xy(j,kend,4)/par

c     -- matrix for k=kend --
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = press(j,k)*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn1 = xy3*u + xy4*v
        vt1 = xy4*u - xy3*v

        a1  = a
        rho1= rho
        pr1 = pr
c     -- reset free stream values with circulation correction --
        if (circul) then
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/( radius* (1.- (fsmach*sin(angl-alphar))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5*(uf**2+vf**2))
          af = dsqrt(af2)        
        else
          uf = uinf
          vf = vinf
          af = dsqrt(gamma*pinf/rhoinf)
        endif
c     
        vninf = xy3*uf + xy4*vf
        vtinf = xy4*uf - xy3*vf
        ainf = af
c     -- matrix m1**-1 --
        m(1,1) = 1.d0
        m(2,1) = -u/rho
        m(3,1) = -v/rho
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(1,2) = 0.d0
        m(2,2) = 1.d0/rho
        m(3,2) = 0.d0
        m(4,2) = -u*gami
        m(1,3) = 0.d0
        m(2,3) = 0.d0
        m(3,3) = 1.d0/rho
        m(4,3) = -v*gami
        m(1,4) = 0.d0
        m(2,4) = 0.d0
        m(3,4) = 0.d0
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) = a/(gami*rho)
        pn(2,1) = -pn(1,1)
        pn(3,1) = gamma*rho**gami/pr
        pn(4,1) = 0.d0
        pn(1,2) = xy3
        pn(2,2) = xy3
        pn(3,2) = 0.d0
        pn(4,2) = xy4
        pn(1,3) = xy4
        pn(2,3) = xy4
        pn(3,3) = 0.d0
        pn(4,3) =-xy3
        pn(1,4) = -gamma/(gami*a*rho)
        pn(2,4) = -pn(1,4)
        pn(3,4) = -rho**gamma/pr**2
        pn(4,4) = 0.d0
c     -- multiplication & storage --
        do n1 = 1,4
          do n2 = 1,4
            cc(n1,n2) = 0.d0
            do nn = 1,4
              cc(n1,n2) = cc(n1,n2) + pn(n1,nn)*m(nn,n2)*xyj(j,k)
            end do
          end do
        end do

c     -- matrix for k=kend-1 ----
        k = kend-1
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr  = press(j,k)*xyj(j,k)
        a   = dsqrt(gamma*pr/rho)                    
        vn2 = xy3*u + xy4*v
        vt2 = xy4*u - xy3*v
        a2  = a
        rho2= rho
        pr2 = pr
c     -- matrix m2**-1 --
        m(1,1) = 1.d0
        m(2,1) = -u/rho
        m(3,1) = -v/rho
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(1,2) = 0.d0
        m(2,2) = 1.d0/rho
        m(3,2) = 0.d0
        m(4,2) = -u*gami
        m(1,3) = 0.d0
        m(2,3) = 0.d0
        m(3,3) = 1.d0/rho
        m(4,3) = -v*gami
        m(1,4) = 0.d0
        m(2,4) = 0.d0
        m(3,4) = 0.d0
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) = 0.d0
        pn(2,1) = -a/(gami*rho)
        pn(1,2) = 0.d0
        pn(2,2) = xy3
        pn(1,3) = 0.d0
        pn(2,3) = xy4
        pn(1,4) = 0.d0
        pn(2,4) = gamma/(gami*a*rho)
c     -- inflow --
        if (vn1.le.0.d0) then
          pn(3,1) = 0.d0
          pn(4,1) = 0.d0
          pn(3,2) = 0.d0
          pn(4,2) = 0.d0
          pn(3,3) = 0.d0
          pn(4,3) = 0.d0
          pn(3,4) = 0.d0
          pn(4,4) = 0.d0
c     -- outflow --
        else
          pn(3,1) = gamma*rho**gami/pr
          pn(4,1) = 0.d0
          pn(3,2) = 0.d0
          pn(4,2) = xy4
          pn(3,3) = 0.d0
          pn(4,3) = -xy3
          pn(3,4) = -rho**gamma/pr**2
          pn(4,4) = 0.d0
        endif
c     -- multiplication --
        k = kend
        do n1 = 1,4
          do n2 = 1,4
            c1(n1,n2) = 0.d0
            do nn = 1,4
              c1(n1,n2) = c1(n1,n2) - pn(n1,nn)*m(nn,n2)*xyj(j,k-1)
            end do
          end do
        end do

c     -- reordering  --
        k = kend
        
c        if (.not. frozen) then
c     -- finding the biggest --  
        do n2 = 1,3
          k1 = n2
          b0 = dabs( cc(n2,n2) )
          do n1 = n2+1,4
            b1 = dabs( cc(n1,n2) )
            if ( b1.gt.b0 ) then
              k1 = n1
              b0 = b1
            end if
          end do
c     -- exchanging rows --
          if ( k1.ne.n2 ) then
            do n3 =1,4
              b1 = cc(n2,n3)
              cc(n2,n3) = cc(k1,n3)
              cc(k1,n3) = b1
              b1 = c1(n2,n3)
              c1(n2,n3) = c1(k1,n3)
              c1(k1,n3) = b1
            end do
            itmp = iex(j,k,n2)
            iex(j,k,n2) = iex(j,k,k1)
            iex(j,k,k1) = itmp
          endif
        end do

c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
        if ( dabs(cc(4,4)).lt.1.d-7 ) then
          do n3 =1,4
            b1 = cc(2,n3)
            cc(2,n3) = cc(4,n3)
            cc(4,n3) = b1
            b1 = c1(2,n3)
            c1(2,n3) = c1(4,n3)
            c1(4,n3) = b1
          end do
          itmp = iex(j,k,2)
          iex(j,k,2) = iex(j,k,4)
          iex(j,k,4) = itmp
        endif 

c     -- diagonal scaling --
        do n1 =1,4
          bcf(j,k,n1) = 1.d0/cc(n1,n1)
        end do

c      end if
c     -- store in jacobian matrix --
        if (imps) then
          k = kend
          do n1 = 1,4
            ij = ( indx(j,k)-1)*ndim + n1
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1) 
            ip = ( ij - 1 )*icol(5)
            do n2 = 1,4
              as(ii+        n2) = c1(n1,n2)
              as(ii+icol(1)+n2) = cc(n1,n2)

              pa(ip+        n2) = c1(n1,n2)
              pa(ip+icol(1)+n2) = cc(n1,n2)
            end do
          end do
        end if
      end do

c     -----------------------------------------------------------------
c     -- inflow boundary  j = 1 --
c     -----------------------------------------------------------------
      if(.not.periodic) then

      do k = kbegin, kend

c     -- matrix for j=1 --
        j = 1
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr1 = press(j,k)*xyj(j,k)
c     -- multiplication --
        do n1 = 1,4
          do n2 = 1,4
            cc(n1,n2) = 0.d0
          enddo
        enddo
c     -- (ro)1   = (ro)2  or Tw = const. --
        cc(1,1) = 1.d0*xyj(j,k)
c     -- (ro*u)1 = (ro*u)2 --
        cc(2,2) = 1.d0*xyj(j,k)
c     -- (ro*v)1 = (ro*v)2 --
        cc(3,3) = 1.d0*xyj(j,k)
c     -- interp. press. --
        cc(4,1) = .5d0*(u**2+v**2)*gami*xyj(j,k)
        cc(4,2) = -u*gami*xyj(j,k)
        cc(4,3) = -v*gami*xyj(j,k)
        cc(4,4) = gami*xyj(j,k)

c     -- matrix for j=2 --
        j = 2
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr2 = press(j,k)*xyj(j,k)

        do n1 = 1,4
          do n2 = 1,4
            c1(n1,n2) = 0.d0
          enddo
        enddo
c     -- (ro)1   = (ro)2  or Tw = const. --
        c1(1,1) = -1.d0*xyj(j,k)
c     -- (ro*u)1 = (ro*u)2 --
        c1(2,2) = -1.d0*xyj(j,k)
c     -- (ro*v)1 = (ro*v)2 --
        c1(3,3) = -1.d0*xyj(j,k)
c     -- interp. press. --
        c1(4,1) = -.5d0*(u**2+v**2)*gami*xyj(j,k)
        c1(4,2) = u*gami*xyj(j,k)
        c1(4,3) = v*gami*xyj(j,k)
        c1(4,4) = -gami*xyj(j,k)

c     -- reordering ----
        j = 1
c     -- finding the biggest --      
        do n2 =1,3
          k1 = n2
          b0 = abs(cc(n2,n2))
          do n1 =n2+1,4
            b1 = abs(cc(n1,n2))
            if(b1.gt.b0) then
              k1 = n1
              b0 = b1
            end if
          end do
c     -- exchanging rows --
          if (k1.ne.n2) then
            do n3 =1,4
              b1 = cc(n2,n3)
              cc(n2,n3) = cc(k1,n3)
              cc(k1,n3) = b1
              b1 = c1(n2,n3)
              c1(n2,n3) = c1(k1,n3)
              c1(k1,n3) = b1
            end do
            itmp = iex(j,k,n2)
            iex(j,k,n2) = iex(j,k,k1)
            iex(j,k,k1) = itmp
          end if
        end do
c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
        if (dabs(cc(4,4)).lt.1.d-7) then
          do n3 =1,4
            b1 = cc(2,n3)
            cc(2,n3) = cc(4,n3)
            cc(4,n3) = b1
            b1 = c1(2,n3)
            c1(2,n3) = c1(4,n3)
            c1(4,n3) = b1
          end do
          itmp = iex(j,k,2)
          iex(j,k,2) = iex(j,k,4)
          iex(j,k,4) = itmp 
        endif 
c     -- diagonal scaling --
        do n1 =1,4
          bcf(j,k,n1) = 1.d0/cc(n1,n1)
        end do

c     -- store in jacobian matrix --
        if (imps) then
          j = jbegin
          do n1 = 1,4
            ij = ( indx(j,k) - 1)*ndim + n1
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1) 
            ip = ( ij - 1 )*icol(5)

            do n2 = 1,4
              as(ii+        n2) = cc(n1,n2)
              as(ii+icol(1)+n2) = c1(n1,n2)

              pa(ip+        n2) = cc(n1,n2)
              pa(ip+icol(1)+n2) = c1(n1,n2)
            end do
          end do
        end if
      end do

c     -----------------------------------------------------------------
c     -- outflow boundary  j = jend --
c     -----------------------------------------------------------------
      do k = kbegin,kend

c     -- matrix for j=jmax --
        j = jmax
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr1 = press(j,k)*xyj(j,k)

        do n1 = 1,4
          do n2 = 1,4
            cc(n1,n2) = 0.d0
          enddo
        enddo
c     -- (ro)1   = (ro)2  or Tw = const. --
        cc(1,1) = 1.d0*xyj(j,k)
c     -- (ro*u)1 = (ro*u)2 --
        cc(2,2) = 1.d0*xyj(j,k)
c     -- (ro*v)1 = (ro*v)2 --
        cc(3,3) = 1.d0*xyj(j,k)
c     -- interp. press. --
        cc(4,1) = .5d0*(u**2+v**2)*gami*xyj(j,k)
        cc(4,2) = -u*gami*xyj(j,k)
        cc(4,3) = -v*gami*xyj(j,k)
        cc(4,4) = gami*xyj(j,k)

c     -- matrix for j=max-1 --
        j   = jmax-1
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr2 = press(j,k)*xyj(j,k)

        do n1 = 1,4
          do n2 = 1,4
            c1(n1,n2) = 0.d0
          enddo
        enddo
c     -- (ro)1   = (ro)2  or Tw = const. --
        c1(1,1) = -1.d0*xyj(j,k)
c     -- (ro*u)1 = (ro*u)2 --
        c1(2,2) = -1.d0*xyj(j,k)
c     -- (ro*v)1 = (ro*v)2 --
        c1(3,3) = -1.d0*xyj(j,k)
c     -- interp. press. --
        c1(4,1) = -.5d0*(u**2+v**2)*gami*xyj(j,k)
        c1(4,2) = u*gami*xyj(j,k)
        c1(4,3) = v*gami*xyj(j,k)
        c1(4,4) = -gami*xyj(j,k)

c---- reordering ----
        j = jend
c     -- finding the biggest --      
        do n2 =1,3
          k1 = n2
          b0 = abs(cc(n2,n2))
          do n1 =n2+1,4
            b1 = abs(cc(n1,n2))
            if(b1.gt.b0) then
              k1 = n1
              b0 = b1
            endif
          end do
c     -- exchanging rows --
          if (k1.ne.n2) then
            do n3 =1,4
              b1 = cc(n2,n3)
              cc(n2,n3) = cc(k1,n3)
              cc(k1,n3) = b1
              b1 = c1(n2,n3)
              c1(n2,n3) = c1(k1,n3)
              c1(k1,n3) = b1
            end do
            itmp = iex(j,k,n2)
            iex(j,k,n2) = iex(j,k,k1)
            iex(j,k,k1) = itmp
          endif
        end do
c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
        if (dabs(cc(4,4)).lt.1.d-7) then
          do n3 =1,4
            b1 = cc(2,n3)
            cc(2,n3) = cc(4,n3)
            cc(4,n3) = b1
            b1 = c1(2,n3)
            c1(2,n3) = c1(4,n3)
            c1(4,n3) = b1
          end do
          itmp = iex(j,k,2)
          iex(j,k,2) = iex(j,k,4)
          iex(j,k,4) = itmp
        endif 

c     -- diagonal scaling --
        do n1 =1,4
          bcf(j,k,n1) = 1.d0/cc(n1,n1)
        end do

c     -- store in jacobian matrix --
        if (imps) then       
          j = jend
          do n1 = 1,4
            ij = ( indx(j,k)-1)*ndim + n1
            ii = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1) 
            ip = ( ij - 1 )*icol(5)

            do n2 = 1,4
              as(ii+        n2) = c1(n1,n2)
              as(ii+icol(1)+n2) = cc(n1,n2)

              pa(ip+        n2) = c1(n1,n2)
              pa(ip+icol(1)+n2) = cc(n1,n2)
            end do
          end do
        end if
      end do

      end if  !not periodic


c     -----------------------------------------------------------------
c     -- Periodic boundary -- set jmaxold point from j = 1 point
c     -----------------------------------------------------------------

c$$$      if(periodic) then
c$$$
c$$$      do k = kbegin,kend
c$$$
c$$$c     -- matrix for j=jmaxold --
c$$$        do n1 = 1,4
c$$$          do n2 = 1,4
c$$$            cc(n1,n2) = 0.d0
c$$$          enddo
c$$$        enddo
c$$$        cc(1,1) = 1.d0
c$$$        cc(2,2) = 1.d0
c$$$        cc(3,3) = 1.d0
c$$$        cc(4,4) = 1.d0
c$$$
c$$$c     -- matrix for j=1 --
c$$$
c$$$        do n1 = 1,4
c$$$          do n2 = 1,4
c$$$            c1(n1,n2) = 0.d0
c$$$          enddo
c$$$        enddo
c$$$
c$$$        c1(1,1) = -1.d0
c$$$        c1(2,2) = -1.d0
c$$$        c1(3,3) = -1.d0
c$$$        c1(4,4) = -1.d0
c$$$
c$$$
c$$$c     -- diagonal scaling --
c$$$        do n1 =1,4
c$$$           bcf(jmaxold,k,n1) = 1.d0/cc(n1,n1)
c$$$           bcf(1,k,n1) = 1.d0/cc(n1,n1)
c$$$        end do
c$$$
c$$$c     -- store in jacobian matrix --
c$$$        if (imps) then       
c$$$          do n1 = 1,4
c$$$            ij = ( indx(jmaxold,k)-1)*ndim + n1
c$$$            ii = ( ij - 1 )*icol(9)
c$$$            ip = ( ij - 1 )*icol(5)
c$$$
c$$$            do n2 = 1,4
c$$$              as(ii+        n2) = c1(n1,n2)
c$$$              as(ii+icol(1)+n2) = cc(n1,n2)
c$$$
c$$$              pa(ip+        n2) = c1(n1,n2)
c$$$              pa(ip+icol(1)+n2) = cc(n1,n2)
c$$$            end do
c$$$          end do
c$$$        end if
c$$$      end do
c$$$      end if

      return
      end                       !vimpfar
