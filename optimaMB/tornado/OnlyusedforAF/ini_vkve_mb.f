c
c     initianlizing varibles using the k-e model and then
c             computing w from w=e/k
c
c
      do k=kbegin,kend
         do j=jbegin,jend
            dk(j,k)=0.0
            de(j,k)=0.0
         enddo
      enddo
      if (.not.restart_ke) then
      do k=kbegin,kend
         do j=jbegin,jend
            ve(j,k)=ve_inf
            vk(j,k)=vk_inf
         enddo
      enddo      
      if (bnd(1)) then
         k=kbegin
         do j=jlow,jup
            ve(j,k)=10.*(6./(re*beta_w*smin(j,k+1)**2))
            vk(j,k)=0.0
            turnu(j,k)=0.0
         enddo
         do k=kbegin+1,kbegin+5
            do j=jlow,jup
               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
               vk(j,k)=turnu(j,k)*ve(j,k)
            enddo
         enddo
      endif
      if (bnd(2)) then
         j=jbegin
         do k=klow,kup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j+1,k)**2)
            do j=jbegin+1,jbegin+5
               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
               vk(j,k)=turnu(j,k)*ve(j,k)
            enddo
         enddo
      endif
      if (bnd(3)) then
         k=kend
         do j=jlow,jup
            ve(j,k)=10.*(6./(re*beta_w*smin(j,k-1)**2))
            vk(j,k)=0.0
            turnu(j,k)=0.0
         enddo
         do k=kend-1,kend-5,-1
            do j=jlow,jup
               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
               vk(j,k)=turnu(j,k)*ve(j,k)
            enddo
         enddo
      endif
      if (bnd(4)) then
         j=jend
         do k=klow,kup
            turnu(j,k)=0.0
            vk(j,k)=0.0
            ve(j,k)=10.*6.*fnu(j,k)/(beta_w*smin(j-1,k)**2)
            do j=jend-1,jend-5,-1
               ve(j,k)=6.*fnu(j,k)/(beta_w*smin(j,k)**2)
               vk(j,k)=turnu(j,k)*ve(j,k)
            enddo
         enddo
      endif
c         
      if (restart) then
         if ((.not.(bnd(1))).and.(.not.(bnd(3)))) then
            do k=klow,kup
               do j=jlow,jup
                  ve(j,k)=sqrt(p1(j,k)/0.09)
                  vk(j,k)=turnu(j,k)*ve(j,k)
                  turnu(j,k)=vk(j,k)/ve(j,k)
               enddo
            enddo
         endif
         if (bnd(1)) then
            do j=jlow,jup
               do k=kbegin+6,kbegin+19
                  b=sqrt(vk(j,k))
                  resid=1.0
                  do while (resid.gt.1.e-10)
                     df_db=-(0.41*0.09**.25*smin(j,k)
     $                    *(1.-exp(-b*smin(j,k)/
     $                    (70.*fnu(j,k)))))-b*0.41*.09**.25*smin(j,k)*
     $                    exp(-b*smin(j,k)/(70.*fnu(j,k)))*smin(j,k)/
     $                    (70.*fnu(j,k))
                     f=turnu(j,k)-b*(0.41*.09**.25*smin(j,k)*
     $                    (1.-exp(-b*smin(j,k)/(70.*fnu(j,k)))))
                     resid=-f/df_db
                     b=b+resid
                  enddo
                  vk(j,k)=b**2
                  fle=0.41*0.09**.25*smin(j,k)*(1.-exp(-b*smin(j,k)/
     $                 (5.0*fnu(j,k))))
                  ve(j,k)=b/fle
               enddo
               do k=kbegin+20,kup
                  ve(j,k)=sqrt(p1(j,k)/0.09)
                  vk(j,k)=turnu(j,k)*ve(j,k)
               enddo
               do k=kbegin+1,kbegin+4
                  ve(j,k)=6.*fnu(j,k)/(0.75*smin(j,k)**2)
               enddo
            enddo
         endif
         if (bnd(3)) then
            do j=jlow,jup
               do k=kend-6,kend-19,-1
                  b=sqrt(vk(j,k))
                  resid=1.0
                  do while (resid.gt.1.e-10)
                     df_db=-(0.41*0.09**.25*smin(j,k)
     $                    *(1.-exp(-b*smin(j,k)/
     $                    (70.*fnu(j,k)))))-b*0.41*.09**.25*smin(j,k)*
     $                    exp(-b*smin(j,k)/(70.*fnu(j,k)))*smin(j,k)/
     $                    (70.*fnu(j,k))
                     f=turnu(j,k)-b*(0.41*.09**.25*smin(j,k)*
     $                    (1.-exp(-b*smin(j,k)/(70.*fnu(j,k)))))
                     resid=-f/df_db
                     b=b+resid
                  enddo
                  vk(j,k)=b**2
                  fle=0.41*0.09**.25*smin(j,k)*(1.-exp(-b*smin(j,k)/
     $                    (5.0*fnu(j,k))))
                  ve(j,k)=b/fle
               enddo
               do k=kend-20,klow,-1
                  ve(j,k)=sqrt(p1(j,k)/0.09)
                  vk(j,k)=turnu(j,k)*ve(j,k)
               enddo
               do k=kend-4,kend-1
                  ve(j,k)=6.*fnu(j,k)/(0.75*smin(j,k)**2)
               enddo
            enddo
         endif
      endif
      endif
c     
