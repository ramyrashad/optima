      subroutine visrhsnc(jdim,kdim,q,press,s,turmu,fmu,xy,xyj,c0,c1,c2,
     *                  c3, c4, c5, fmutemp, ttemp, beta, vflux)        
c
#include "../include/arcom.inc"
#include "../include/visc.inc"
c                                                                       
      dimension q(jdim,kdim,4),press(jdim,kdim),turmu(jdim,kdim)        
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension fmu(jdim,kdim)                                          
c                                                                       
      dimension c0(jdim,kdim),beta(jdim,kdim),vflux(jdim,kdim,4)        
      dimension c1(jdim,kdim),c2(jdim,kdim),c3(jdim,kdim)               
      dimension c4(jdim,kdim),c5(jdim,kdim), fmutemp(jdim,kdim),        
     >          ttemp(jdim,kdim)                                        
c                                                                       
c                                                                       
c                                                                       
c                                                                       
c      parameter ( prlam = .72 , prturb=.90 )                            
c      parameter ( prlinv = 1./prlam , prtinv = 1./prturb )              
c      parameter ( f43 = 4./3.  , f13 = 1./3., f23 = 2./3. )             
c                                                                       
c         coded by tim barth -  1985                                    
c                                                                       
c         prlam   =  laminar prandtl number  = .72                      
c         prturb  =  turbulent prandtl number = .90                     
c         prlinv  =  1./(laminar prandtl number)                        
c         prtinv  =  1./(turbulent prandtl number)                      
c         f13     =  1/3                                                
c         f23     =  2/3                                                
c         f43     =  4/3                                                
c         hre     =  1/2 dt * reynolds number                           
c         fmu     =  viscosity                                          
c         turmu   =  turbulent viscosity                                
c         aa7      =  sound speed squared ... also temperature          
c         beta    =  fmu/prlam + muturb/prturb                          
c                                                                       
c                                                                       
c                                                                       
c      set up some temporary logical switches visxi,viseta,visxx        
c                                                                       
c                                                                       
c                                                                       
      hre   = .5*dt/(re* (1. + phidt) )                                 
      g1    = 1./gami                                                   
c                                                                       
cthp10/23/87                                                            
c                                                                       
c  for cmesh logic apply cross terms at k = 1 along wake cut with       
c                                                                       
c                    k+1 = 2, k-1 = 1, k = 1                            
c                                                                       
cthp10/23/87                                                            
         kmm = 1                                                        
         if(cmesh .and. wake) kmm = 0                                    
                                                                        
cthp10/23/87                                                            
c*********************************************************************  
c*******  common variables for xi, eta and cross terms  **************  
c*********************************************************************  
c                                                                       
c                                                                       
c  averaged fmu to get it at 1/2 grid pnts (turmu is already at 1/2 pts)
c                                                                       
c$$$      do 5  k = kbegin, kup                                             
c$$$      do 5  j = jbegin, jend                                            
c$$$        c0(j, k) = 0.5d0*(fmu(j,k)+fmu(j,k+1))                           
c$$$ 5    continue  
c$$$                                                        
c$$$      do 7 j = jbegin, jup                 
c$$$      do 7 k = kbegin, kup                                              
c$$$       fmutemp(j,k) = 0.5d0*(c0(j,k)+c0(jplus(j),k))  

c$$$ 7    continue                                                          
c$$$                                                                        
c$$$      do 10 j = jbegin, jup                                             
c$$$         jpl = jplus(j)                                                 
c$$$         do 10 k = kbegin, kup                                          
c$$$            turmd = 0.5d0*(turmu(jpl,k)+ turmu(j,k))                       
c$$$            beta(j,k) = (fmutemp(j,k)*prlinv + turmd*prtinv)*hre*g1
c$$$            c0(j,k)  = (fmutemp(j,k) + turmd)*hre                       
c$$$ 10   continue                                                          
c                                                                       
c*********************************************************************  
c*********************************************************************  
c                                                           
c       mu, turmu at node points for cross terms for more compactness
c                                                                       
        do 305 j = jbegin,jend                                          
        ttemp(j,1) = 0.0d0                                               
        do 305 k = klow,kend                                            
        ttemp(j,k) = 0.5d0*(turmu(j,k)+turmu(j,k-kmm))                     
  305   continue  
                                                      
        do 350 k = kbegin,kend                                         
        do 350 j = jbegin,jend                                         
           r1      = 1.d0/xyj(j,k)                                       
           beta(j,k) = r1*(fmu(j,k)*prlinv + ttemp(j,k)*prtinv)*hre*g1 
           c0(j,k)  = r1*(fmu(j,k) + ttemp(j,k))*hre                   
 350   continue                                                         
c
c                                                                       
c---------------------------------------------------------------------  
c       (e_eta)_xi    terms                      
c---------------------------------------------------------------------  
c                                                                       
c       first central difference in eta                             
c                                             
        do 310 k=klow,kup                                               
        do 310 j=jbegin,jend                                            
            t1      = xy(j,k,1)*xy(j,k,3)                               
            t2      = xy(j,k,1)*xy(j,k,4)                               
            t3      = xy(j,k,2)*xy(j,k,3)                               
            t4      = xy(j,k,2)*xy(j,k,4)                               
            c1(j,k) =  beta(j,k)*( t1     +   t4    )                   
            c2(j,k) =  c0(j,k)*( f43*t1 +   t4    )                     
            c3(j,k) =  c0(j,k)*( t1     +   f43*t4)                     
            c4(j,k) =  c0(j,k)*(-f23*t2 +   t3    )                     
            c5(j,k) =  c0(j,k)*( t2     -   f23*t3)                     
 310    continue          
 
        do 320 k=klow,kup                                               
        kp1 = k+1                                                       
        km1 = k-kmm                                                     
        do 320 j=jbegin,jend                                            
c                                                                       
c              note factor of 0.5 in built into fmu & beta              
c                                                                       
          rrm1  = 1./q(j,km1,1)                                       
          rr1   = 1./q(j,k,1)                                         
          rrp1  = 1./q(j,kp1,1)                                       
          uum1  = q(j,km1,2)*rrm1                                     
          uup1  = q(j,kp1,2)*rrp1                                     
          uu1   = q(j,k,2)*rr1                                      
          vvm1  = q(j,km1,3)*rrm1                                     
          vvp1  = q(j,kp1,3)*rrp1                                     
          vv1   = q(j,k,3)*rr1                                      
          ueta  = uup1 - uum1                                           
          veta  = vvp1 - vvm1                                           
          c2eta =  gamma*(press(j,kp1)*rrp1 - press(j,km1)*rrm1)
              
          vflux(j,k,2) = c2(j,k)*ueta + c4(j,k)*veta                      
          vflux(j,k,3) = c5(j,k)*ueta + c3(j,k)*veta                      
          vflux(j,k,4) = uu1*vflux(j,k,2)+vv1*vflux(j,k,3)+c1(j,k)*c2eta 
 320    continue   
                                                     
        do 400 j=jlow,jup                                                 
        jp1 = jplus(j)                                                    
        jm1 = jminus(j)                                                   
        do 400 k=klow,kup                                                 
           s(j,k,2) = s(j,k,2) + 0.5d0*(vflux(jp1,k,2) - vflux(jm1,k,2))    
           s(j,k,3) = s(j,k,3) + 0.5d0*(vflux(jp1,k,3) - vflux(jm1,k,3))    
           s(j,k,4) = s(j,k,4) + 0.5d0*(vflux(jp1,k,4) - vflux(jm1,k,4))    
 400    continue                                                        
c                                                                       
c...................................................................    
c      (f_xi)_eta    terms                       
c...................................................................    
c                                                                       
c       first central difference in xi                              
c                                                                       
        do 410 j=jlow,jup                                               
        do 410 k=kbegin,kend                                            
            t1      = xy(j,k,1)*xy(j,k,3)                               
            t2      = xy(j,k,1)*xy(j,k,4)                               
            t3      = xy(j,k,2)*xy(j,k,3)                               
            t4      = xy(j,k,2)*xy(j,k,4)                               
            c1(j,k) =  beta(j,k)*( t1     +   t4    )                   
            c2(j,k) =  c0(j,k)*( f43*t1 +   t4    )                     
            c3(j,k) =  c0(j,k)*( t1     +   f43*t4)                     
            c4(j,k) =  c0(j,k)*(-f23*t2 +   t3    )                     
            c5(j,k) =  c0(j,k)*( t2     -   f23*t3)                     
 410    continue                                                        
        do 420 j=jlow,jup                                               
        jp1 = jplus(j)                                                  
        jm1 = jminus(j)                                                 
        do 420 k=kbegin,kend                                            
c                                                                       
c              note factor of 0.5 in built into fmu & beta              
c                                                                       
          rrm1  = 1./q(jm1,k,1)                                       
          rr1   = 1./q(j,k,1)                                         
          rrp1  = 1./q(jp1,k,1)                                       
          uum1  = q(jm1,k,2)*rrm1                                     
          uup1  = q(jp1,k,2)*rrp1                                     
          uu1   = q(j,k,2)*rr1                                      
          vvm1  = q(jm1,k,3)*rrm1                                     
          vvp1  = q(jp1,k,3)*rrp1                                     
          vv1   = q(j,k,3)*rr1                                      
          uxi  = uup1 - uum1                                            
          vxi  = vvp1 - vvm1                                            
          c2xi = gamma*(press(jp1,k)*rrp1  - press(jm1,k)*rrm1)  
            
          vflux(j,k,2) = c2(j,k)*uxi + c5(j,k)*vxi                        
          vflux(j,k,3) = c4(j,k)*uxi + c3(j,k)*vxi                        
          vflux(j,k,4) = uu1*vflux(j,k,2)+vv1*vflux(j,k,3)+c1(j,k)*c2xi
 420    continue  
                                                      
        do 500 k=klow,kup                                                 
        kp1 = k+1                                                         
        km1 = k-kmm                                                       
        do 500 j=jlow,jup                                                 
           s(j,k,2) = s(j,k,2) + 0.5d0*(vflux(j,kp1,2) - vflux(j,km1,2))    
           s(j,k,3) = s(j,k,3) + 0.5d0*(vflux(j,kp1,3) - vflux(j,km1,3))    
           s(j,k,4) = s(j,k,4) + 0.5d0*(vflux(j,kp1,4) - vflux(j,km1,4))    
 500    continue                                                          
c                                                                       
      
      return                                                            
      end                                                               
