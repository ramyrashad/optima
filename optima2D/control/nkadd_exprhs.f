      subroutine nkadd_exprhs(jdim, kdim, ndim, s , exprhs)
 
#include "../include/arcom.inc"

      dimension exprhs(jdim,kdim,ndim)
      dimension s(jdim,kdim,ndim)

      do 500 n=1,ndim
      do 500 k=klow,kup
      do 500 j=jlow,jup
         s(j,k,n)=s(j,k,n)+exprhs(j,k,n)
 500  continue

      return
      end
