C ********************************************************************
C ***************** NON-CONSERVATIVE      ****************************
C ***************** EXPLICIT VISCOUS TERM ****************************
C ********************************************************************
c calling routine: etaexpl
      subroutine visncy(JDIM,KDIM,Q,PRESS,S,TURMU,FMU,XY,XYJ,
     &  C0,C1,C2,C3,C4,C5,FMUTEMP,TTEMP,BETA,VFLUX,
     &  jbegin,jend,jlow,jup,kbegin,kend,klow,kup)        
c
#include "../include/common.inc"
C                                                                       
      DIMENSION Q(JDIM,KDIM,4),PRESS(JDIM,KDIM),TURMU(JDIM,KDIM)        
      DIMENSION S(JDIM,KDIM,4),XY(JDIM,KDIM,4),XYJ(JDIM,KDIM)           
      DIMENSION FMU(JDIM,KDIM)                                          
C                                                                       
      DIMENSION C0(JDIM,KDIM),BETA(JDIM,KDIM),VFLUX(JDIM,KDIM,4)        
      DIMENSION C1(JDIM,KDIM),C2(JDIM,KDIM),C3(JDIM,KDIM)               
      DIMENSION C4(JDIM,KDIM),C5(JDIM,KDIM), FMUTEMP(JDIM,KDIM),        
     >          TTEMP(JDIM,KDIM)                                        
C                                                                       
C                                                                       
C                                                                       
C                                                                       
      PARAMETER ( PRLAM = .72 , PRTURB=.90 )                            
      PARAMETER ( PRLINV = 1./PRLAM , PRTINV = 1./PRTURB )              
      PARAMETER ( F43 = 4./3.  , F13 = 1./3., F23 = 2./3. )             
C                                                                       
C         PRLAM   =  LAMINAR PRANDTL NUMBER  = .72                      
C         PRTURB  =  TURBULENT PRANDTL NUMBER = .90                     
C         PRLINV  =  1./(LAMINAR PRANDTL NUMBER)                        
C         PRTINV  =  1./(TURBULENT PRANDTL NUMBER)                      
C         F13     =  1/3                                                
C         F23     =  2/3                                                
C         F43     =  4/3                                                
C         HRE     =  1/2 DT * REYNOLDS NUMBER                           
C         FMU     =  VISCOSITY                                          
C         TURMU   =  TURBULENT VISCOSITY                                
C         AA7      =  SOUND SPEED SQUARED ... ALSO TEMPERATURE          
C         BETA    =  FMU/PRLAM + MUTURB/PRTURB                          
C                                                                       
C                                                                       
      HRE   = .5*DT/(RE* (1. + PHIDT) )                                 
      G1    = 1./GAMI                                                   
C                                                                       
C*********************************************************************  
C*******  COMMON VARIABLES FOR XI, ETA AND CROSS TERMS  **************  
C*********************************************************************  
C                                                                       
C                                                                       
C AVERAGE FMU TO GET IT AT 1/2 GRID PNTS-(TURMU IS ALREADY AT 1/2 PTS)
C                                                                       
      DO 5  K = KBEGIN, KUP                                             
      DO 5  J = JLOW, JEND                                              
        FMUTEMP(J, K) = .5*(FMU(J, K)+FMU(J, K+1))                      
 5    CONTINUE                                                          
                                                                        
      DO 10 K = KBEGIN, KUP                                             
        DO 10 J = JLOW, JUP                                             
            TURMD =  TURMU(J,K)                                         
            BETA(J,K) =                                                 
     >       (FMUTEMP(J,K)*PRLINV + TURMD*PRTINV)*HRE*G1                
            C0(J,K)  = (FMUTEMP(J,K) + TURMD)*HRE                       
 10   CONTINUE                                                          
C ********************************************************************
C **          F_ETA_ETA VISCOUS TERMS                               **
C ********************************************************************
      DO 100 K=KBEGIN,KEND                                              
        DO 100 J=JLOW,JUP                                               
            T1      = XY(J,K,3)                               
            T2      = XY(J,K,4)                               
            T3      = XY(J,K,4)*XY(J,K,4)                               
            R1      = 1./XYJ(J,K)                                       
            C1(J,K)   =  R1*( T1     +   T3    )                        
            C2(J,K)   =  R1*( F43*T1 +   T3    )                        
            C3(J,K)   =  R1*( T1     +   F43*T3)                        
            C4(J,K)   =  R1*(     T2*F13     )                          
  100 CONTINUE                                                        
      DO 110 K=KBEGIN,KUP                                               
        KP1 = K+1
        DO 110 J=JLOW,JUP                                               
          RR1   = 1./Q(J, K, 1)                                         
          RRP1  = 1./Q(J, KP1, 1)                                       
          UUP1  = Q(J, KP1, 2)*RRP1                                     
          UU1   = Q(J, K,   2)*RR1                                      
          VVP1  = Q(J, KP1, 3)*RRP1                                     
          VV1   = Q(J, K,   3)*RR1                                      
          UETA  = UUP1 - UU1                                            
          VETA  = VVP1 - VV1                                            
          C2ETA =                                                       
     >    GAMMA*(PRESS(J, KP1)*RRP1  - PRESS(J, K)*RR1)                 
          FMUMD   = C0(J,K)                                             
          BETMD   = BETA(J,K)                                           
          UETAMU  = UETA*FMUMD                                          
          VETAMU  = VETA*FMUMD                                          
          C2ETAMU = C2ETA*BETMD                                         
           VFLUX(J,K,2)=                                                
     >       ( C2(J,KP1) + C2(J,K)  )*UETAMU                            
     >     + ( C4(J,KP1) + C4(J,K)  )*VETAMU                            
           VFLUX(J,K,3)=                                                
     >       ( C4(J,KP1) + C4(J,K)  )*UETAMU                            
     >     + ( C3(J,KP1) + C3(J,K)  )*VETAMU                            
           VFLUX(J,K,4)= 0.5*(                                          
     >     + (UUP1 + UU1)*VFLUX(J,K,2)                                  
     >     + (VVP1 + VV1)*VFLUX(J,K,3) )                                
     >     + ( C1(J,KP1) + C1(J,K) )*C2ETAMU                            
 110  CONTINUE                                                          
      DO 200 K=KLOW,KUP                                                 
      KM1 = K-1                                                         
      DO 200 J=JLOW,JUP                                                 
          S(J,K,2) = S(J,K,2) + VFLUX(J,K,2) - VFLUX(J,KM1,2)           
          S(J,K,3) = S(J,K,3) + VFLUX(J,K,3) - VFLUX(J,KM1,3)           
          S(J,K,4) = S(J,K,4) + VFLUX(J,K,4) - VFLUX(J,KM1,4)           
 200  CONTINUE                                                          
C                                                                       
      RETURN                                                            
      END                                                               
