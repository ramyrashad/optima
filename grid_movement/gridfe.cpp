#ifndef GRIDFE_H
#include "gridfe.h"
#endif

void form_faces (vector<Node>& node, vector<Face>& faces, 
		 const int& jmax, const int& kmax)
{
  int i(0), j(0), row(0), ind (0);
  Node** corners = new Node* [4];
  int *vertexID = NULL;

  bool counterclockwise = true;
  // determine the element orientation from the 1st element
  corners[0] = &node[i];
  corners[1] = &node[i+1];
  corners[2] = &node[i+jmax+1];
  corners[3] = &node[i+jmax];
  counterclockwise = arrange_counter_clockwise (corners);
 
  for (row=0; row<kmax-1; row++)
    {
      for (j=0; j<jmax-1; j++)
	{
	  switch (counterclockwise)
	    {
	    case true:
	      corners[0] = &node[i];
	      corners[1] = &node[i+1];
	      corners[2] = &node[i+jmax+1];
	      corners[3] = &node[i+jmax];
	      break;
	    case false:
	      corners[0] = &node[i];
	      corners[3] = &node[i+jmax];

	      corners[2] = &node[i+jmax+1];
	      corners[1] = &node[i+1];
	      break;
	    default:
	      cerr << "Unknown element orientation " << endl;
	      exit (10);
	      break;
	    }		  

	  faces[ind] = Face (corners, ind);		  
	      
	  // add opposite corner nodes of face to be neighbours
	  // use this info later to build global stiffness matrix
	  vertexID = faces[ind].get_vertexID();
	      
	  node[vertexID[0]].add_nei(node[vertexID[2]]);
	  node[vertexID[2]].add_nei(node[vertexID[0]]);
	  node[vertexID[1]].add_nei(node[vertexID[3]]);
	  node[vertexID[3]].add_nei(node[vertexID[1]]);
	      
	  i++;
	  ind++;
	}
      i++;
    }

  delete [] corners;    corners = NULL;
  vertexID = NULL;

  return;
}


void form_faces (vector<Node>& node, vector <Face>& faces, 
		 const int& nblk, const int* jmax, const int* kmax)
{
  int i(0), j(0), row(0), ind (0), n (0);
  Node** corners = new Node* [4];
  int *vertexID = NULL;
  bool counterclockwise = true;

  //ofstream fout ("faces.dat");

  for (n=0; n<nblk; n++)
    {
      // determine the element orientation from the 1st element
      corners[0] = &node[i];
      corners[1] = &node[i+1];
      corners[2] = &node[i+jmax[n]+1];
      corners[3] = &node[i+jmax[n]];
      counterclockwise = arrange_counter_clockwise (corners);
 

      for (row=0; row<kmax[n]-1; row++)
	{
	  for (j=0; j<jmax[n]-1; j++)
	    {
	      switch (counterclockwise)
		{
		case true:
		  corners[0] = &node[i];
		  corners[1] = &node[i+1];
		  corners[2] = &node[i+jmax[n]+1];
		  corners[3] = &node[i+jmax[n]];
		  break;
		case false:
		  corners[0] = &node[i];
		  corners[3] = &node[i+jmax[n]];
		  corners[2] = &node[i+jmax[n]+1];
		  corners[1] = &node[i+1];
		  break;
		default:
		  cerr << "Unknown element orientation " << endl;
		  exit (10);
		  break;
		}		  

	      faces[ind] = Face (corners, ind);		  

	      vertexID = faces[ind].get_vertexID();
	      
	      node[vertexID[0]].add_nei(node[vertexID[2]]);
	      node[vertexID[2]].add_nei(node[vertexID[0]]);
	      node[vertexID[1]].add_nei(node[vertexID[3]]);
	      node[vertexID[3]].add_nei(node[vertexID[1]]);
	      /* fout << setw(10) << ind << setw(10) << vertexID[0]
		 <<   setw(10) << vertexID[1]
		 <<   setw(10) << vertexID[2]
		 <<   setw(10) << vertexID[3] << endl;
	      */
	      i++;
	      ind++;
	    }
	  i++;
	}
      i += jmax[n];
    }

  //  fout.close();
  // exit (2);

  delete [] corners;    corners = NULL;
  vertexID = NULL;
  return;
}



bool arrange_counter_clockwise (Node** corners)
{
  double* i;  double* i1;  double* ijmax;   
  i = corners[0]->get_xyz();    i1 = corners[1]->get_xyz();
  ijmax = corners[3]->get_xyz();

  // perform cross product to determine element orientation
  double UxV = (i1[0]-i[0])*(ijmax[1]-i[1]) - 
    (i1[1]-i[1])*(ijmax[0]-i[0]);

  i = NULL;    i1 = NULL;    ijmax = NULL;
  
  if (UxV < 0)  // orientation is clockwise
    return false;
  else 
    return true;
}



// function to read a structured grid from optima2D
// makes the gridline extending from the TE to the farfield a boundary
void read_gridTE (char *filename, vector<Node>& node_loc, 
		int &moving_bnd_nodes, vector<Node*>& bnd_nodes, 
		const int &jtail1, const int& jtail2, vector <Face>& faces)
{

  int k, j, nbytes, jmax, kmax;
  const bool ThreeD = false;

  ifstream fin(filename, ios::in | ios::binary);
  if (! fin.is_open())
    cout << "Error opening file grid file in function move_grid (gridfe.cpp)"
	 << endl; 
 
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));

  fin.read(reinterpret_cast < char * > (&jmax), sizeof(int));
  ByteSwap5(jmax);

  fin.read(reinterpret_cast < char * > (&kmax), sizeof(int));
  ByteSwap5(kmax);
  
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(int));

  fin.read(reinterpret_cast < char * > (&j), sizeof(int));

  nbytes = jmax * kmax ;
  node_loc.resize(nbytes);
  double *xold = new double [nbytes];
  double *yold = new double [nbytes];
    
  for (k=0; k<nbytes ; k++)  
    {
      fin.read(reinterpret_cast < char * > (&xold[k]), sizeof(double));
      ByteSwap5(xold[k]);
    }
  for (k=0; k<nbytes; k++)
    {
      fin.read(reinterpret_cast < char * > (&yold[k]), sizeof(double));
      ByteSwap5(yold[k]);
    }
	
  while (fin.good())
    fin.read(reinterpret_cast < char * > (&j), sizeof(int));

  fin.close();

  // assign the nodal coordinates into the Node vector
  int index = 0;
  double *xy = new double[2];
  moving_bnd_nodes = 0;

  for (k=0; k<kmax; k++)
    {
      for (j=0; j<jmax; j++)
	{

	  xy[0] = xold[index];
	  xy[1] = yold[index];
	  node_loc[index] = Node(xy, index, ThreeD);
	
	  //if ( (index%jmax == 0) || ((index+1) % jmax == 0) || index<jtail1-1 )
	  //if ( (index%jmax == 0) || ((index+1) % jmax == 0))
	  if ( (index%jmax == 0) || ((index+1) % jmax == 0) || index<jtail1-1 )
	    node_loc[index].is_surf();
	  else if ((index>=jtail1-1) && (index<=jtail2-1)) 
	    {
	      node_loc[index].is_surf();
	      bnd_nodes[moving_bnd_nodes] = &node_loc[index];
	      moving_bnd_nodes ++;
	    }
	  else if ((index > jtail2-1) && (index < jmax))
	    {
	      node_loc[index].is_surf();
	      node_loc[index].overlaps(&node_loc[jmax-j-1]);
	    }
	  else if ( k==kmax-1 )
	    node_loc[index].is_surf();
	  index ++;
	}
    }

  node_loc[jtail2-1].overlaps(&node_loc[jtail1-1]);
  node_loc[jtail2-1].is_surf();
  node_loc[jmax-1].overlaps(&node_loc[0]);
  node_loc[jmax-1].is_surf();

  nbytes = (int) bnd_nodes.size();
  if (moving_bnd_nodes != nbytes)
    {
      bnd_nodes.resize(moving_bnd_nodes);
      cerr << "Error with size of moving boundary nodes array" 
	   << nbytes << " moving_bnd_nodes " << moving_bnd_nodes 
	   << endl;
      exit (9);
    }

  delete [] xy;      xy = NULL;
  delete [] xold;    xold = NULL;
  delete [] yold;    yold = NULL;

  read_connectivity(node_loc, jmax, kmax, jtail1, jtail2);
  
  // Form the faces of element
  faces.resize( (jmax-1)*(kmax-1) );
  form_faces (node_loc, faces, jmax, kmax);

  return;
}

// function to read a structured grid from optima2D
void read_grid (char *filename, vector<Node>& node_loc, 
		int &moving_bnd_nodes, vector<Node*>& bnd_nodes, 
		const int &jtail1, const int& jtail2, vector <Face>& faces)
{

  int k, j, nbytes, jmax, kmax;
  const bool ThreeD = false;

  ifstream fin(filename, ios::in | ios::binary);
  if (! fin.is_open())
    cout << "Error opening file grid file in function move_grid (gridfe.cpp)"
	 << endl; 
 
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(nbytes));

  fin.read(reinterpret_cast < char * > (&jmax), sizeof(int));
  ByteSwap5(jmax);

  fin.read(reinterpret_cast < char * > (&kmax), sizeof(int));
  ByteSwap5(kmax);
  
  fin.read(reinterpret_cast < char * > (&nbytes), sizeof(int));

  fin.read(reinterpret_cast < char * > (&j), sizeof(int));

  nbytes = jmax * kmax ;
  node_loc.resize(nbytes);
  double *xold = new double [nbytes];
  double *yold = new double [nbytes];
    
  for (k=0; k<nbytes ; k++)  
    {
      fin.read(reinterpret_cast < char * > (&xold[k]), sizeof(double));
      ByteSwap5(xold[k]);
    }
  for (k=0; k<nbytes; k++)
    {
      fin.read(reinterpret_cast < char * > (&yold[k]), sizeof(double));
      ByteSwap5(yold[k]);
    }
	
  while (fin.good())
    fin.read(reinterpret_cast < char * > (&j), sizeof(int));

  fin.close();

  // assign the nodal coordinates into the Node vector
  int index = 0;
  double *xy = new double[2];
  moving_bnd_nodes = 0;

  for (k=0; k<kmax; k++)
    {
      for (j=0; j<jmax; j++)
	{

	  xy[0] = xold[index];
	  xy[1] = yold[index];
	  node_loc[index] = Node(xy, index, ThreeD);
	
	  if ( (index%jmax == 0) || ((index+1) % jmax == 0))
	    node_loc[index].is_surf();
	  else if ((index>=jtail1-1) && (index<=jtail2-1)) 
	    {
	      node_loc[index].is_surf();
	      bnd_nodes[moving_bnd_nodes] = &node_loc[index];
	      moving_bnd_nodes ++;
	    }
	  else if ((index > jtail2-1) && (index < jmax))
	    {
	      node_loc[index].is_surf();
	      node_loc[index].overlaps(&node_loc[jmax-j-1]);
	    }
	  else if ( k==kmax-1 )
	    node_loc[index].is_surf();
	  index ++;
	}
    }

  node_loc[jtail2-1].overlaps(&node_loc[jtail1-1]);
  node_loc[jtail2-1].is_surf();
  node_loc[jmax-1].overlaps(&node_loc[0]);
  node_loc[jmax-1].is_surf();

  nbytes = (int) bnd_nodes.size();
  if (moving_bnd_nodes != nbytes)
    {
      bnd_nodes.resize(moving_bnd_nodes);
      cerr << "Error with size of moving boundary nodes array" 
	   << nbytes << " moving_bnd_nodes " << moving_bnd_nodes 
	   << endl;
      exit (9);
    }

  delete [] xy;      xy = NULL;
  delete [] xold;    xold = NULL;
  delete [] yold;    yold = NULL;

  read_connectivity(node_loc, jmax, kmax, jtail1, jtail2);
  
  // Form the faces of element
  faces.resize( (jmax-1)*(kmax-1) );
  form_faces (node_loc, faces, jmax, kmax);

  return;
}

void update (vector <Node>& node, const VECTOR_double& du)
{
  int count = 0;
  Node *overlap;
  double *coord;
  int size = (int) node.size();

  for (int j=0; j<size; j++)
    {
      if (node[j].if_surf() == false)
        {
	  node[j].deform(du[count], du[count+1]);
          count = count + 2;
        }
      else
        {
          overlap = node[j].get_overlap();
          if (overlap != NULL)
            {
              coord = overlap->get_coord();
              node[j].set_coord(coord);
            }
	}
    }                                                         
  overlap = NULL;
  coord = NULL;

  return;
}


void update_xyz (vector <Node>& node, const VECTOR_double& du)
{
  int i, j;
  double *xyz = NULL;

  // updates the new coordinates 
  update (node, du);

  // copy the new coordinates to the iniial xyz coordinates
  i = (int) node.size();
  for (j=0; j<i; j++)
    {
      xyz = node[j].get_coord();
      node[j].set_xyz(xyz);
    }          

  xyz = NULL;
}


void update_xyz (vector <Node>& node, const VECTOR_double& du,
    double* dx, double* dy, const int& incr)
// Overloaded to also return the changes dx and dy.
// incr is not the total number of increments in the mesh perturbation,
// but rather is the increment that is currently being computed.
{
  // updates the new coordinates 
  update (node, du);

  // copy the new coordinates to the initial xyz coordinates
  int size = int (node.size());
  int size_incr = incr * size;
  for (int j=0; j<size; j++)
  {
    dx[j + size_incr] = node[j].get_coord()[0] - node[j].get_xyz()[0];
    dy[j + size_incr] = node[j].get_coord()[1] - node[j].get_xyz()[1];
    node[j].set_xyz(node[j].get_coord());
  }          
}



void get_nonzerosfe (vector <Node> &node, int& num_inn_nodes, 
		     int& nonzeros)
{
  Node **nei = NULL;

  int j, i, num_nei, nnznei, prebnd;
  int size = (int) node.size();

  prebnd = 0;
  nonzeros = 0;
  for (j=0; j<size; j++) 
    {
      if (node[j].if_surf() == false) 
	{
	  nei = node[j].get_nei();
	  num_nei = node[j].get_num_nei();
	  num_inn_nodes = 2*(j-prebnd);
	  node[j].set_ind(num_inn_nodes);	    
	  
	  nnznei = 0;  // number of nonzero neighbours
	  for (i=0; i<num_nei; i++)
	    {
	      if (nei[i]->if_surf() == false)
		nnznei += 2;
	    }
	  nnznei += 2;
	  nonzeros = nonzeros + nnznei*2;
	}
      else
	prebnd ++;
    }
  num_inn_nodes = 2*(size-prebnd) ;
  
  nei = NULL;
  return;
}


void get_nonzerosfe_symm (vector <Node> &node, int& num_inn_nodes, 
			  int& nonzeros)
{
  Node **nei = NULL;

  int i=0, j=0, prebnd=0, num_nei=0, symm=0, nnznei=0;  
  // prebnd holds the number of pre_boundary nodes

  int size = (int) node.size();
  nonzeros = 0;

  for (i=0; i<size; i++)
    {
      //fout << setw(10) << i ;

      if (node[i].if_surf() == false)
	{
	  nei = node[i].get_nei();
	  num_nei = node[i].get_num_nei();

	  // formula to calculate the indexing of interior nodes
	  num_inn_nodes = 3*(i-prebnd) - symm;
	  node[i].set_ind(num_inn_nodes);	    

	  nnznei = 0;  // number of nonzero neighbours
	  for (j=0; j<num_nei; j++)   
	    {
	      if (nei[j]->if_surf() == false) 
		{
		  if (nei[j]->get_symm() > 0)
		    nnznei += 2;
		  else 
		    nnznei +=3;
		}
	    }

	  if (node[i].get_symm() > 0)
	    {
	      symm ++;
	      nnznei += 2;
	      nonzeros = nonzeros + nnznei*2;
	    }
	  else
	    {
	      nnznei +=3;
	      nonzeros = nonzeros + nnznei*3;
	    }

	}
      else
	prebnd ++;
      // fout<<setw(10)<< num_inn_nodes << endl;

    }

  num_inn_nodes = 3*(size-prebnd) - symm;

  //  cout << "number of symmetric nodes " << symm << endl;
  // cout << "nonzeros is " << nonzeros << "  num_inn_nodes " << num_inn_nodes
  //   << endl;
  // fout.close(); exit (10);
  
  nei = NULL;

  return;
}


// assembles the global stiffness matrix
void form_stiff (CompRow_Mat_double& alpha, const vector <Node>& node, 
		 const vector <Face>& face, VECTOR_double &F, const
		 double *elemQo, double& meshQ)
{
  int i, j, k;  // counter
  int* ind;

  int nvertices = 4;

  j = nvertices*2;
  double **K = new double*[j];  // element stiffness
  for (i=0; i<j; i++)
    K[i] = new double [j];
  vector < double*> vertices_coord (nvertices);

  int row, column;
  double val;
  double *coord;  double *xyz;
  int size = (int) face.size();

  for (i=0; i<size; i++)
    {
      ind = face[i].get_vertexID();
      nvertices  = face[i].get_nvertices();

      for (j=0; j<nvertices; j++)
	vertices_coord[j] = node[ind[j]].get_xyz();
    
      if (nvertices == 3)  // if element is triangular
	get_K_Tri (vertices_coord, K, elemQo[i], meshQ, face[i]);
      else if (nvertices == 4)  // default element is quadrilateral
	get_K (vertices_coord, K, elemQo[i], meshQ);
      else 
	{
	  cerr << "only tri and quad element stiffness matrices "
	       << "have been implemented";
	  exit(1);
	}

      for (j=0; j<nvertices; j++)
	{
	  if (node[ind[j]].if_surf() == false)
	    {
	      row = node[ind[j]].get_ind();

	      for (k=0; k<nvertices; k++)
		{
		  if (node[ind[k]].if_surf() == false)
		    {
		      column = node[ind[k]].get_ind();
		      
		      val = alpha(row, column) + K[j*2][k*2];
		      alpha.set(row, column) = val;

		      val = alpha(row, column+1) + K[j*2][k*2+1];
		      alpha.set(row, column+1) = val;

		      val = alpha(row+1, column) + K[j*2+1][k*2];
		      alpha.set(row+1, column) = val;

		      val = alpha(row+1, column+1) + K[j*2+1][k*2+1];
		      alpha.set(row+1, column+1) = val;

		    }
		  else
		    {
		      coord = node[ind[k]].get_coord();
		      xyz = node[ind[k]].get_xyz();
		      
		      F[row] = F[row] - K[j*2][k*2]*(coord[0]-xyz[0])
			- K[j*2][k*2+1]*(coord[1]-xyz[1]);
		      
		      F[row+1] = F[row+1] - K[j*2+1][k*2]*(coord[0]-xyz[0])
			- K[j*2+1][k*2+1]*(coord[1]-xyz[1]);
		
		    }
		}
	    }
	}

    }


  //  j = nvertices*2;
  for (i=0; i<8; i++)
    delete [] K[i];
  delete [] K;           K = NULL;
  xyz = NULL;            coord = NULL;     ind = NULL;

  return;
}




void construct_stiffness_matrix (int* colind, int* rowptr, 
				 const vector <Node>& node,
				 const int& dof)
{
  int i, j, k, num_nei, nnznei;
  Node ** nei = NULL;
  vector <int> labels;  vector <int> tag;

  int n=0; int m=0; // row and column counter
  rowptr[0] = 0;
  int size = (int) node.size();

  for (i=0; i<size; i++) 
    {
      if (node[i].if_surf() == false) 
	{
	  nei = node[i].get_nei();
	  num_nei = node[i].get_num_nei();
	  
	  labels.resize(num_nei+1);
	  tag.resize(num_nei+1);
	  
	  nnznei = 0;  // number of nonzero neighbours
	  for (j=0; j<num_nei; j++)   
	    {
	      labels[j] = nei[j]->get_ID();
	      tag[j] = j;

	      if (nei[j]->if_surf() == false) 
		nnznei += dof; 
	    }

	  labels[j] = i;
	  tag[j] = j;
	  j++;
	  nnznei += dof;
	  tagSort(labels, tag, j);

	  for (j=0; j<=num_nei; j++)    // column counter
	    {       
	      if ( node[labels[tag[j]]].if_surf() == false )
  		{
		  colind[n] = node[labels[tag[j]]].get_ind();
		  colind[n+1] = colind[n] + 1;
		  colind[n+nnznei] = colind[n];
		  colind[n+nnznei+1] = colind[n+1];

		  if (dof==3)
		    {
		      colind[n+2] = colind[n] + 2;
		      colind[n+nnznei+2] = colind[n] + 2;
		      colind[n+nnznei*2] = colind[n];
		      colind[n+nnznei*2+1] = colind[n+1];
		      colind[n+nnznei*2+2] = colind[n+2];

		    }

		  n += dof;
		}
	    }
	  m ++;
	  rowptr[m] = rowptr[m-1] + nnznei;
	  
	  for (k=1; k<dof; k++)
	    {
	      m ++;
	      rowptr[m] = rowptr[m-1] + nnznei;
	      n = n + nnznei;
	    }
	}
    }

  nei = NULL;
  return;
}

void construct_stiff_matrix_symm (int* colind, int* rowptr, 
				  const vector <Node>& node)
{
  int i, j, k, num_nei, nnznei;
  Node ** nei = NULL;
  vector <int> labels;  vector <int> tag;

  int n=0; int m=0; // row and column counter
  rowptr[0] = 0;
  int size = (int) node.size();
  int symm = 0;

  for (i=0; i<size; i++) 
    {
      if (node[i].if_surf() == false) 
	{
	  nei = node[i].get_nei();
	  num_nei = node[i].get_num_nei();
	  symm = node[i].get_symm();
	  
	  labels.resize(num_nei+1);
	  tag.resize(num_nei+1);

	  nnznei = 0;  // number of nonzero neighbours
	  for (j=0; j<num_nei; j++)   
	    {
	      labels[j] = nei[j]->get_ID();
	      tag[j] = j;

	      if (nei[j]->if_surf() == false) 
		{
		  if (nei[j]->get_symm() > 0)
		    nnznei += 2;
		  else 
		    nnznei +=3;
		}
	    }

	  labels[j] = i;
	  tag[j] = j;
	  j++;

	  if (symm > 0)
	    nnznei += 2;
	  else
	    nnznei +=3;

	  tagSort(labels, tag, j);

	  //cout << "out put indices, nnznei is  " << nnznei << endl;
	  for (j=0; j<=num_nei; j++)    // column counter
	    {       
	      if ( node[labels[tag[j]]].if_surf() == false )
  		{
		  k =  node[labels[tag[j]]].get_symm();
	      
		  //cout <<" n " << n << " n " << node[labels[tag[j]]].get_ind() 
		  // << "  " <<node[labels[tag[j]]].get_ind() +1<< endl;
		  if (symm == 0)
		    {
		      if ( k == 0 )
			{
			  colind[n] = node[labels[tag[j]]].get_ind();
			  colind[n+1] = colind[n] + 1;
			  colind[n+2] = colind[n] + 2;

			  colind[n+nnznei] = colind[n];
			  colind[n+nnznei+1] = colind[n+1];
			  colind[n+nnznei+2] = colind[n] + 2;

			  colind[n+nnznei*2] = colind[n];
			  colind[n+nnznei*2+1] = colind[n+1];
			  colind[n+nnznei*2+2] = colind[n+2];
			}
		      else
			{
			  colind[n] = node[labels[tag[j]]].get_ind();
			  colind[n+1] = colind[n] + 1;

			  colind[n+nnznei] = colind[n];
			  colind[n+nnznei+1] = colind[n+1];

			  colind[n+nnznei*2] = colind[n];
			  colind[n+nnznei*2+1] = colind[n+1];
			}
		    }
		  else
		    {
		      if (k == 0 )
			{
			  colind[n] = node[labels[tag[j]]].get_ind();
			  colind[n+1] = colind[n] + 1;
			  colind[n+2] = colind[n] + 2;

			  colind[n+nnznei] = colind[n];
			  colind[n+nnznei+1] = colind[n+1];
			  colind[n+nnznei+2] = colind[n] + 2;
			}
		      else
			{
			  colind[n] = node[labels[tag[j]]].get_ind();
			  colind[n+1] = colind[n] + 1;

			  colind[n+nnznei] = colind[n];
			  colind[n+nnznei+1] = colind[n+1];
			}
		    }

		  if (k==0)
		    n += 3;
		  else
		    n += 2;
		}
	    }
	  
	  m ++;
	  rowptr[m] = rowptr[m-1] + nnznei;
	 
	  m ++;
	  rowptr[m] = rowptr[m-1] + nnznei;
	  n = n + nnznei;
	  
	  if (symm == 0)
	    {
	      m ++;
	      rowptr[m] = rowptr[m-1] + nnznei;
	      n = n + nnznei;
	    }
	  //if (i==2)
	  //exit(12);
	}
    }
  
  nei = NULL;
  return;
}


void ini_stiff (CompRow_Mat_double& alpha, const vector <Node>& node, 
		const vector <Face>& face)
{
  
  int i, j, k, row, column;
  double val = 0.0;
  int *ind;
  int size = (int) face.size();
  int nvertices = face[0].get_nvertices();

  for (i=0; i<size; i++)
    {
      ind = face[i].get_vertexID();

      for (j=0; j<nvertices; j++)
	{
	  if (node[ind[j]].if_surf() == false)
	    {
	      for (k=0; k<nvertices; k++)
		{
		  if (node[ind[k]].if_surf() == false)
		    {
		      row = (node[ind[j]].get_ind() );
		      column = (node[ind[k]].get_ind());

		      alpha.set(row, column) = val;
		      alpha.set(row, column+1) = val;
		      alpha.set(row+1, column) = val;
		      alpha.set(row+1, column+1) = val;
		    }
		}
	    }
	}
    }

  ind = NULL;
  return;
}





// initial quality of the mesh
void get_ini_quality (const vector <Face>& faces, 
		      const vector <Node>& nodes,
		      double* elemQo, double& Qomax)
{
  // calculates the initial element quality and the initial mesh
  // quality Qomax

  int i, j, k;  // counter
  int nvertices = 4;

  vector<double*> vertices (nvertices);
  //  double *xyz;  
  int* vertexID;

  k = (int) faces.size();
  for (j=0; j<k; j++)
    {
      vertexID = faces[j].get_vertexID();
      nvertices = faces[j].get_nvertices(); 
    
      for (i=0; i<nvertices; i++)
	vertices[i] = nodes[vertexID[i]].get_xyz();
	
      if (nvertices == 3)
	{
	  Qomax = faces[j].get_area();  // Qomax stores the area of triangle
	  get_Q(vertices, Qomax, elemQo[j]);
	}
      else if (nvertices == 4)  // element is quadrilateral
	get_Q (vertices, elemQo[j]);  
      else
	{
	  cerr << "Only support tri and quad elements " << endl;
	  exit(1);
	}
    }
  
  Qomax = elemQo[0];
  for (j=1; j<k; j++)
    {
      if (elemQo[j] > Qomax)
	Qomax = elemQo[j];
    }

  vertexID = NULL;   // xyz = NULL;
  return;
}


void get_new_boundaryMB (char* filename, const vector <Node*>& bnd_nodes, 
			 double** deltaxy, const int& len, const double& f)
{
  // f = fraction: divisions or increments in going to the deformed bound.

  char * foil = strtok(filename,".");
  strcat(foil,".foil");
 
  ifstream fin (foil);
  int j;
  double *xyz;

  for (j=0; j<len; j++)
    {
      xyz = bnd_nodes[j]->get_xyz();
      fin >> deltaxy[j][0] >> deltaxy[j][1] ;

      deltaxy[j][0] = f * (deltaxy[j][0] - xyz[0]);
      deltaxy[j][1] = f * (deltaxy[j][1] - xyz[1]);
    }
  fin.close();
  
  foil = NULL;
  xyz = NULL;

  return;
}


// create the cubic polynomial from the 4 points: bisection, TE, FF, FF-1
void form_TE_bnd(vector<Node>& nodes, const int& jtail1,
		 const int& jtail2)
{
  int i, j;   // counter
  double* coord;
  double factor;

  // 5, 4, 3
  int end = int (0.4*jtail1);
  //int end = 1;
  // M is an augmented 4x5 matrix
  double** M = new double*[4];
  for (i=0; i<4; i++)
    M[i] = new double[5];

  coord = nodes[jtail1].get_coord();
  M[0][0] = 1.0;
  M[0][1] = coord[0];      M[0][2] = coord[0];  
  M[0][3] = coord[0];      M[0][4] = coord[1];

  coord = nodes[jtail2-2].get_coord();
  M[0][1] += coord[0];      M[0][2] += coord[0];  
  M[0][3] += coord[0];      M[0][4] += coord[1];

  M[0][1] = M[0][1]*0.5;      
  M[0][2] = M[0][2]*M[0][2]*0.25;  
  M[0][3] = M[0][3]*M[0][3]*M[0][3]*0.125;   
  M[0][4] = M[0][4]*0.5;

  coord = nodes[jtail1-1].get_coord();
  M[1][0] = 0.0;
  M[1][1] = coord[0];      M[1][2] = coord[0]*coord[0];  
  M[1][3] =  M[1][2]*coord[0];      M[1][4] = coord[1];

  // if (atan( fabs(M[0][4]-M[1][4]) / fabs(M[1][1]-M[0][1]) ) > 0.15)
  //{
  coord = nodes[end].get_coord();
  M[2][0] = 0.0;
  M[2][1] = coord[0];      M[2][2] = coord[0]*coord[0];  
  M[2][3] =  M[2][2]*coord[0];      M[2][4] = coord[1];

  coord = nodes[end-1].get_coord();
  M[3][0] = 0.0;
  M[3][1] = coord[0];      M[3][2] = coord[0]*coord[0];  
  M[3][3] =  M[3][2]*coord[0];      M[3][4] = coord[1];

  for (i=1; i<4; i++)
    for (j=1; j<5; j++)
      M[i][j] = M[i][j] - M[0][j];
      
  for (i=2; i<4; i++)
    {
      factor = M[i][1]/M[1][1];
      M[i][1] = 0.0;
      for (j=2; j<5; j++)
	M[i][j] = M[i][j] - M[1][j]*factor;
    }
  factor = M[0][1]/M[1][1];
  M[0][1] = 0.0;
  for (j=2; j<5; j++)
    M[0][j] = M[0][j] - M[1][j]*factor;

  for (i=0; i<2; i++)
    {
      factor = M[i][2]/M[2][2];
      M[i][2] = 0.0;
      for (j=3; j<5; j++)
	M[i][j] = M[i][j] - M[2][j]*factor;
    }
  factor = M[3][2]/M[2][2];
  M[3][2] = 0.0;
  M[3][3] = M[3][3] - M[2][3]*factor;
  M[3][4] = M[3][4] - M[2][4]*factor;
    
  for (i=0; i<3; i++)
    {
      factor = M[i][3]/M[3][3];
      M[i][3] = 0.0;
      M[i][4] = M[i][4] - M[3][4]*factor;
    }

  for (i=1; i<4; i++)
    {
      M[i][4] = M[i][4]/M[i][i];
      M[i][i] = 1.0;
    }

  j = jtail1-1;
  factor = nodes[j].get_xnew()-nodes[j].get_x();
  if (fabs(factor) > dxTEtol)
    modify_TE_FF (nodes,jtail1, factor);
  else
    {
      for (i=1; i<jtail1; i++)
	nodes[i].is_surf();
    }
  for (i=end; i<j; i++)
    {
      coord = nodes[i].get_coord();

      coord[1] = M[0][4] + M[1][4]*coord[0] 
	+ M[2][4]*coord[0]*coord[0] 
	+ M[3][4]*coord[0]*coord[0]*coord[0];
    } 
  // }

  for (i=0; i<4; i++)
    delete [] M[i];
  delete [] M;      M = NULL;
  coord = NULL;

  return;
}


// modifies the grid line from the TE to farfield boundary
void modify_TE_FF (vector <Node>& nodes, const int& jtail1, 
		   const double& dxTE)
{
  int j = jtail1-2;
  double* coord;
  double* S = new double[j]; 
  double Ltot = 0;   // total arc length
  int i;

  S[0] = nodes[j].get_x()-nodes[j+1].get_x();

  for (i=1; i<j; i++)
    S[i] = S[i-1]+ nodes[j-i].get_x()-nodes[j-i+1].get_x();
  
  Ltot = S[i-1] + nodes[0].get_x()-nodes[1].get_x();

  for (i=0; i<j; i++)
    S[i] = S[i]/Ltot;

  for (i=j; i>0; i--)
    {
      coord = nodes[i].get_coord();
      coord[0] += (1-S[j-i])*dxTE;
      // or more fancy method (doesn't make much difference, yet
      // lots more computations
      // coord[0] += (1+cos(3.14*S[j-i]))*0.5*dxTE;
    }

  delete [] S;  S = NULL;
  coord = NULL;
  
  return;
}


// assembles the global stiffness matrix in a more efficient manner
// this function is faster than the one above which uses the 
// sparselib compressed sparse row matrix alpha
void form_stiff (int*& rowptr, int*& colind, double*& stiff_val, 
		 const vector <Node>& node, 
		 const vector <Face>& face, VECTOR_double &F, const
		 double *elemQo, double& meshQ)
{
  int i, j, k;  // counter
  int* ind;

  int nvertices = 4;

  j = nvertices*2;
  double **K = new double*[j];  // element stiffness
  for (i=0; i<j; i++)
    K[i] = new double [j];
  vector < double*> vertices_coord (nvertices);

  int row, column, kx2, jx2;
  double *coord;  double *xyz;
  int size = (int) face.size();

  for (i=0; i<size; i++)
    {
      ind = face[i].get_vertexID();
      nvertices  = face[i].get_nvertices();

      for (j=0; j<nvertices; j++)
	vertices_coord[j] = node[ind[j]].get_xyz();
    
      if (nvertices == 3)  // if element is triangular
	get_K_Tri (vertices_coord, K, elemQo[i], meshQ, face[i]);
      else if (nvertices == 4)  // default element is quadrilateral
	get_K (vertices_coord, K, elemQo[i], meshQ);
      else 
	{
	  cerr << "only tri and quad element stiffness matrices "
	       << "have been implemented";
	  exit(1);
	}
      //cout << "in form_stiff, element " << i << " of " << size << endl;
      for (j=0; j<nvertices; j++)
	{
	  if (node[ind[j]].if_surf() == false)
	    {
	      row = node[ind[j]].get_ind();
	      jx2 = j*2;

	      for (k=0; k<nvertices; k++)
		{
		  kx2 = k*2;
		  if (node[ind[k]].if_surf() == false)
		    {
		      column = node[ind[k]].get_ind();
		      
		      add_val_csr (row, column, rowptr, colind, 
				   stiff_val, K[jx2][kx2]);

		      add_val_csr (row, column+1, rowptr, colind, 
				   stiff_val, K[jx2][kx2+1]);

		      add_val_csr (row+1, column, rowptr, colind, 
				   stiff_val, K[jx2+1][kx2]);

		      add_val_csr (row+1, column+1, rowptr, colind, 
				   stiff_val, K[jx2+1][kx2+1]);
		    }
		  else
		    {
		      coord = node[ind[k]].get_coord();
		      xyz = node[ind[k]].get_xyz();
		      
		      F[row] = F[row] - K[jx2][kx2]*(coord[0]-xyz[0])
			- K[jx2][kx2+1]*(coord[1]-xyz[1]);
		      
		      F[row+1] = F[row+1] - K[jx2+1][kx2]*(coord[0]-xyz[0])
			- K[jx2+1][kx2+1]*(coord[1]-xyz[1]);
		
		    }
		}
	    }
	}

    }


  //  j = nvertices*2;
  for (i=0; i<8; i++)
    delete [] K[i];
  delete [] K;           K = NULL;
  xyz = NULL;            coord = NULL;     ind = NULL;

  return;
}


// This function adds a value to the existing value in the compressed 
// sparse row matrix array
void add_val_csr ( const int& row, const int& col, int*& rowptr,
		   int*& colind, double*& val_array, const double& val)
{
  /*for (int i=rowptr[row]; i<rowptr[row+1]; i++)
      if (colind[i]==col)
	val_array[i] += val;
  */

  int i = rowptr[row];     //begin with the first cell of array
  while (colind[i]!=col)    // loop test condition
    i++;                            //increment counter by 1
  val_array[i] += val;
  

  return;
}


// initialize the array of size to zero
void init_array(double*& array, const double& size)
{
  for (int i=0; i<size; i++)
    array[i] = 0.0;

  return;
}

void init_array(VECTOR_double& array, const double& size)
{
  for (int i=0; i<size; i++)
    array[i] = 0.0;

  return;
}
