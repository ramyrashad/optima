      subroutine heapify(iarr,jj,ptr,er)
c
      integer ii,jj,ison,ison2,ilarg,itmp,ptr(iarr)
      double precision er(iarr),daber,dabison,dabison2
c
      ii=1
 10   ison=2*ii
      ison2=2*ii+1
c
c      if (ptr(ii).eq.0) goto 999
      if (ptr(ison).eq.0 .or. ptr(ison).gt.iarr) goto 999
      if (ptr(ison2).eq.0 .or. ptr(ison2).gt.iarr) goto 999
      daber=dabs(er(ptr(ii)))
      dabison=dabs(er(ptr(ison)))
      dabison2=dabs(er(ptr(ison2)))
c
      if (ii.le.jj/2 .and. 
     &      (dabison.gt.daber .or. dabison2.gt.daber)) then
c
         if (dabison.gt.dabison2) then
            ilarg=ison
         else
            ilarg=ison2
         endif
c
         itmp=ptr(ii)
         ptr(ii)=ptr(ilarg)
         ptr(ilarg)=itmp
         ii=ilarg
         goto 10
      else
         goto 999
      endif
c
 999  return
      end
