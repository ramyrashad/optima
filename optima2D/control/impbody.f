c---------------------------------------------------------------------
c     -- body boundary condition for inviscid flow --
c     -- based on a. pueyo's routine impbodyi --
c     -- m. nemec, may 2000 --
c---------------------------------------------------------------------
      subroutine impbody (jdim, kdim, indx, iex, q, xy, xyj, as, pa,
     &      imps, bcf)

      use disscon_vars
#include "../include/arcom.inc"

      integer j, k, n1, n2, nn, jdim, kdim, n
      integer indx(jdim,kdim), iex(jdim,kdim,4)
      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision as(jdim*kdim*4*(4*9+1)+jdim*4*3+1)
      double precision pa(jdim*kdim*4*20+1), bcfmin, cc_tmp(4,4)
      double precision m(4,4), pn(4,4), cc(4,4), c1(4,4), c2(4,4)
      
      double precision bcf(jdim,kdim,4), bcf_norm, cc_norm, bcfmax

      logical imps

      do j = jtail1,jtail2

c     -- metrics terms --
        par = sqrt(xy(j,1,3)**2+(xy(j,1,4)**2))
        xy3 = xy(j,1,3)/par
        xy4 = xy(j,1,4)/par

c     -- matrix for k=1 --
        k = 1                
        hinf = 1./gami + .5d0*fsmach**2
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        rho1= rho
        u1  = u
        v1  = v
        pr1 = gami*(q(j,k,4) -                                   
     >        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        vn1 = xy3*u + xy4*v
        vt1 = xy4*u - xy3*v
c     -- matrix m1**-1 --
        m(1,1) = 1.d0
        m(1,2) = 0.d0
        m(1,3) = 0.d0
        m(1,4) = 0.d0
        m(2,1) = -u/rho
        m(2,2) = 1.d0/rho
        m(2,3) = 0.d0
        m(2,4) = 0.d0
        m(3,1) = -v/rho
        m(3,2) = 0.d0
        m(3,3) = 1.d0/rho
        m(3,4) = 0.d0
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(4,2) = -u*gami
        m(4,3) = -v*gami
        m(4,4) = gami
c     -- matrix pn --
	if (dissCon) then
           pn(1,1) = 0.d0	
           pn(1,2) = xy3*(1.d0 + lamDiss)
           pn(1,3) = xy4*(1.d0 + lamDiss)
           pn(1,4) = 0.d0
           pn(2,1) = 0.d0
           pn(2,2) = xy4*(1.d0 + lamDiss)
           pn(2,3) =-xy3*(1.d0 + lamDiss)
           pn(2,4) = 0.d0
           pn(3,1) = 0.d0
           pn(3,2) = 0.d0
           pn(3,3) = 0.d0
           pn(3,4) = 1.d0 + lamDiss
           pn(4,1) = .5d0*(u**2+v**2) - hinf
           pn(4,2) = q(j,k,2)*xyj(j,k)
           pn(4,3) = q(j,k,3)*xyj(j,k)
           pn(4,4) = gamma/gami
        else
           pn(1,1) = 0.d0	
           pn(1,2) = xy3
           pn(1,3) = xy4
           pn(1,4) = 0.d0
           pn(2,1) = 0.d0
           pn(2,2) = xy4
           pn(2,3) =-xy3
           pn(2,4) = 0.d0
           pn(3,1) = 0.d0
           pn(3,2) = 0.d0
           pn(3,3) = 0.d0
           pn(3,4) = 1.d0
           pn(4,1) = .5d0*(u**2+v**2) - hinf
           pn(4,2) = q(j,k,2)*xyj(j,k)
           pn(4,3) = q(j,k,3)*xyj(j,k)
           pn(4,4) = gamma/gami
        end if
        
c     -- multiplication --
        do n2 =1,4
          do n1 =1,4
            cc(n1,n2) = 0.d0
            do nn =1,4
              cc(n1,n2) = cc(n1,n2) + pn(n1,nn)*m(nn,n2)*xyj(j,1)
            end do
          end do
        end do

c     -- matrix for k=2 --
        k = 2
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr2 = gami*(q(j,k,4) -                                   
     >        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        vn2 = xy3*u + xy4*v
        vt2 = xy4*u - xy3*v
c     -- matrix m2**-1 --
        m(1,1) = 1.d0
        m(1,2) = 0.d0
        m(1,3) = 0.d0
        m(1,4) = 0.d0
        m(2,1) = -u/rho
        m(2,2) = 1.d0/rho
        m(2,3) = 0.d0
        m(2,4) = 0.d0
        m(3,1) = -v/rho
        m(3,2) = 0.d0
        m(3,3) = 1.d0/rho
        m(3,4) = 0.d0
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(4,2) = -u*gami
        m(4,3) = -v*gami
        m(4,4) = gami
c     -- matrix pn --        
        pn(1,1) = 0.d0
        pn(1,2) = 0.d0
        pn(1,3) = 0.d0
        pn(1,4) = 0.d0
        pn(2,1) = 0.d0
        pn(2,2) = xy4
        pn(2,3) = -xy3
        pn(2,4) = 0.d0
        pn(3,1) = 0.d0
        pn(3,2) = 0.d0
        pn(3,3) = 0.d0
        pn(3,4) = 1.d0
        pn(4,1) = 0.d0
        pn(4,2) = 0.d0
        pn(4,3) = 0.d0
        pn(4,4) = 0.d0

c     -- multiplication --
        do n2 =1,4
          do n1 =1,4
            c1(n1,n2) = 0.d0
            do nn =1,4
              c1(n1,n2) = c1(n1,n2) -2.d0*pn(n1,nn)*m(nn,n2)
     >              *xyj(j,2)
            end do
          end do
        end do

c     -- matrix for k=3 --
        k = 3
        rho = q(j,k,1)*xyj(j,k)
        u   = q(j,k,2)/q(j,k,1)
        v   = q(j,k,3)/q(j,k,1)
        pr3 = gami*(q(j,k,4) -                                   
     >        0.5*(q(j,k,2)**2 + q(j,k,3)**2)/q(j,k,1))*xyj(j,k)
        vn3 = xy3*u + xy4*v
        vt3 = xy4*u - xy3*v
c     -- matrix m2**-1 --
        m(1,1) = 1.d0
        m(1,2) = 0.d0
        m(1,3) = 0.d0
        m(1,4) = 0.d0
        m(2,1) = -u/rho
        m(2,2) = 1.d0/rho
        m(2,3) = 0.d0
        m(2,4) = 0.d0
        m(3,1) = -v/rho
        m(3,2) = 0.d0
        m(3,3) = 1.d0/rho
        m(3,4) = 0.d0
        m(4,1) = .5d0*(u**2+v**2)*gami
        m(4,2) = -u*gami
        m(4,3) = -v*gami
        m(4,4) = gami
c     -- matrix pn --
        pn(1,1) = 0.d0
        pn(1,2) = 0.d0
        pn(1,3) = 0.d0
        pn(1,4) = 0.d0
        pn(2,1) = 0.d0
        pn(2,2) = xy4
        pn(2,3) =-xy3
        pn(2,4) = 0.d0
        pn(3,1) = 0.d0
        pn(3,2) = 0.d0
        pn(3,3) = 0.d0
        pn(3,4) = 1.d0
        pn(4,1) = 0.d0
        pn(4,2) = 0.d0
        pn(4,3) = 0.d0
        pn(4,4) = 0.d0
        
c     -- multiplication --
        do n2 =1,4
          do n1 =1,4
            c2(n1,n2) = 0.d0
            do nn =1,4
              c2(n1,n2) = c2(n1,n2) + pn(n1,nn)*m(nn,n2)*xyj(j,3)
            end do
          end do
        end do

        k = kbegin

c     -- reordering --

        !- Save CC matrix before reordering. If the reordered CC matrix
        !- has a zero on any diagonal element the revert to the original
        !- CC matrix
        do n1 = 1,4
           do n2 = 1,4
              cc_tmp(n1,n2) = cc(n1,n2)
           end do
        end do

c     -- finding the biggest --      
       do n2 =1,3
         k1 = n2
         b0 = dabs( cc(n2,n2) )
         do n1 =n2+1,4
           b1 = dabs( cc(n1,n2) )
           if (b1.gt.b0) then
             k1 = n1
             b0 = b1
           end if
         end do
c     -- interchanging --
         if (k1.ne.n2) then
           do n3 =1,4
             b1 = cc(n2,n3)
             cc(n2,n3) = cc(k1,n3)
             cc(k1,n3) = b1
             b1 = c1(n2,n3)
             c1(n2,n3) = c1(k1,n3)
             c1(k1,n3) = b1
             b1 = c2(n2,n3)
             c2(n2,n3) = c2(k1,n3)
             c2(k1,n3) = b1
           end do
c           ij = ( indx(j,1) - 1 )*4 + n2
c           ik = ( indx(j,1) - 1 )*4 + k1
c           itmp = iex(ij)
c           iex(ij) = iex(ik)
c           iex(ik) = itmp
           itmp = iex(j,k,n2)
           iex(j,k,n2) = iex(j,k,k1)
           iex(j,k,k1) = itmp
         endif
       end do

c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
       if ( dabs(cc(4,4)).lt.1.d-7 ) then
         do n3 =1,4
           b1 = cc(2,n3)
           cc(2,n3) = cc(4,n3)
           cc(4,n3) = b1
           b1 = c1(2,n3)
           c1(2,n3) = c1(4,n3)
           c1(4,n3) = b1
           b1 = c2(2,n3)
           c2(2,n3) = c2(4,n3)
           c2(4,n3) = b1
         end do
c         ij = ( indx(j,k)-1)*4 + 2
c         ik = ( indx(j,k)-1)*4 + 4
c         itmp = iex(ij)
c         iex(ij) = iex(ik)
c         iex(ik) = itmp
         itmp = iex(j,k,2)
         iex(j,k,2) = iex(j,k,4)
         iex(j,k,4) = itmp
       endif 

       !-- Check reordered CC matrix for zeros on the diagonal. If a 
       !-- zero is found, revert to the non-reordered CC matrix
       do n = 1,4
          if (cc(n,n).eq.0.0) then
             write(scr_unit,*)'impbody:'
             write(scr_unit,*)'Zero found on reordered CC diag...'
             write(scr_unit,*)'Revert to non-reordered CC matrix'
             write(scr_unit,*)''
             cc = cc_tmp
             goto 67
          end if
       end do

 67    continue

c     -- diagonal scaling --
c     -- for adjoint problem - DO NOT scale here --
       do n1 =1,4 

          if(cc(n1,n1).eq.0.0)then
             write(*,*)'impbody:'
             write(*,*)'cc(n1,n1) = ',cc(n1,n1)
             write(*,*)'n1 = ',n1
             stop
          end if
  
          bcf(j,k,n1) = 1.d0/cc(n1,n1)
       end do
     
c     -- store in jacobian matrix --
       if (imps) then
         do n1 = 1,4
           ij = ( indx(j,k)-1)*4 + n1
           ii = ( ij - 1 )*36
           if (clopt) ii = ( ij - 1 )*37
           ip = ( ij - 1 )*20

           do n2 = 1,4
             as(ii+n2) = cc(n1,n2) 
             as(ii+4+n2) = c1(n1,n2)
             as(ii+8+n2) = c2(n1,n2)

             pa(ip+n2) = cc(n1,n2) 
             pa(ip+4+n2) = c1(n1,n2)
             pa(ip+8+n2) = c2(n1,n2)
           end do
         end do
       end if
      end do

      return
      end                       !impbody
