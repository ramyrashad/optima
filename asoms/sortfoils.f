c *********************************************************************
c ** This routine automatically generates a list of blocks and sides **
c ** that define the individual elements.   The list defines the     **
c ** segments (i.e., blocks and sides) in such an order so as to     **
c ** define and traverse each element in a clockwise direction.      **
c ** Taken from TORNADO                                              **
c ** Mods: M. Nemec, April 2001                                      **
c *********************************************************************

      subroutine sortfoils(x, y)

      implicit none

#include "mxblk.inc"
#include "mxfoil.inc"
#include "mxseg.inc"
#include "mxjk.inc"

      integer maxjk
      parameter (maxjk=maxj*maxk)
      integer imaxblk, ifoil, iblk, iside, imaxseg, nlist, iseg
      integer isegment, jkstart, jkend, ii

      double precision xstart, ystart, xend, yend

      integer nfoils, nsegments(maxfoil)
      integer isegblk(maxsegs,maxfoil), isegside(maxsegs,maxfoil)
      common/sortedfoils/ nsegments, isegblk, isegside, nfoils

      integer lblkpt(maxblk,4), lsidept(maxblk,4), idir(maxblk,4)
      integer ibctype(maxblk,4), ibcdir(maxblk,4)
      common/connect/ lblkpt, lsidept, idir, ibctype, ibcdir

      integer nblks, jbmax(maxblk), kbmax(maxblk)
      common/mblock/ nblks, jbmax, kbmax
      
      double precision x(maxjk), y(maxjk)
      integer ilistblk(maxsegs,maxfoil), ilistside(maxsegs,maxfoil)

      write (*,510)

c     -zero data
      imaxblk = 0
      do 10 ifoil=1,maxfoil
        nsegments(ifoil) = 0
 10   continue

c     -find list of blocks and sides that make up each element
      do 225 ifoil = 1,nfoils
        do 200 iblk = 1,nblks
        do 200 iside = 1,4
          if (abs(ibcdir(iblk,iside)).eq.ifoil) then
            nsegments(ifoil) = nsegments(ifoil) + 1
            ilistblk(nsegments(ifoil),ifoil) = iblk
            ilistside(nsegments(ifoil),ifoil) = iside
          endif
 200    continue
c        write(99,*) ifoil,nsegments(ifoil)
 225  continue

c     MAIN LOOP:
C     SORT the list starting with the largest block number

      do 900 ifoil=1,nfoils
        imaxseg = 1
        nlist = nsegments(ifoil)
c
c       -find the segment that has the largest block number
c
        do 300 iseg=1,nsegments(ifoil)
          if (ilistblk(iseg,ifoil).gt.ilistblk(imaxseg,ifoil)) then
            imaxseg = iseg
          endif
  300   continue
c
c       -set segment with largest block number as first entry in
c        list of segments.
c
        iblk = ilistblk(imaxseg,ifoil)
        isegblk(1,ifoil) = iblk
        iside = ilistside(imaxseg,ifoil)
        isegside(1,ifoil) = iside
        isegment = 1

c       -set starting (x,y) point and ending (x,y) point on 1st segment
        call getbounds(iblk,iside,jkstart,jkend)

        if (iside.eq.2) then
c         -blunt trailing edge ... set xstart and ystart as
c          lower left corner of block
c         -note: original TORNADO did this internally in getbounds
          xstart = x(jkend)
          ystart = y(jkend)
          xend = x(jkstart)
          yend = y(jkstart)
        else
          xstart = x(jkstart)
          ystart = y(jkstart)
          xend = x(jkend)
          yend = y(jkend)
        endif

c       -remove this segment from the unsorted list of segments
        do 400 iseg=imaxseg+1,nsegments(ifoil)
          ilistblk(iseg-1,ifoil) = ilistblk(iseg,ifoil)
          ilistside(iseg-1,ifoil) = ilistside(iseg,ifoil)
  400   continue
        nlist = nlist - 1
c
c       Loop through unsorted entries and add segments to sorted list
c       as they connect
c
  410   continue

        do 500 iseg=1,nlist
           call getbounds(ilistblk(iseg,ifoil), ilistside(iseg,ifoil),
     &          jkstart, jkend)
c         Add segment to sorted segment list if it connects
          if (abs(x(jkstart)-xend).lt.1.0E-07.and.
     &        abs(y(jkstart)-yend).lt.1.0E-07) then
             isegment = isegment + 1
             isegblk(isegment,ifoil) = ilistblk(iseg,ifoil)
             isegside(isegment,ifoil) = ilistside(iseg,ifoil)
             xend = x(jkend)
             yend = y(jkend)

c           -remove connecting segment from unsorted segment list
            do 450 ii = iseg+1,nlist
              ilistblk(ii-1,ifoil) = ilistblk(ii,ifoil)
              ilistside(ii-1,ifoil) = ilistside(ii,ifoil)
 450        continue
            nlist = nlist - 1
c     
            if (nlist.gt.0) goto 410
          endif
 500    continue


 510    format (/3x,'Airfoil sorting ...'/)
        write (*,515) ifoil
 515    format (3x,'Element number:',i3)
        write (*,520)
 520    format (3x,'Block',3x,'Side')
        do 550 iseg=1,nsegments(ifoil)
        write (*,525) isegblk(iseg,ifoil),isegside(iseg,ifoil)
 550    continue
 525    format (3x,i3,5x,i2)
c
c Check for errors
c
        if (nlist.ne.0) write(*,*)'ERROR: nlist>0, foil=',ifoil
        if ((xend.ne.xstart).or.(yend.ne.ystart)) write(*,*)
     &    'ERROR: start & ending coordinates not equal, ifoil='
     &    ,ifoil
c
c That's it for now
c
  900 continue
c
      return
      end
