            if (.not.wake) then
               do k=kbegin,kend
               do j=jbegin,jend
                  j_point=j
c                  if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
                  xpt1=x(j_point,1)
                  ypt1=y(j_point,1)
                  smin(j,k)=sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
               enddo
               enddo
            else
               j_point=1
               do k=kbegin,kend
               do j=jbegin,jend
c                  j_point=j
c                  if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
                  xpt1=x(j_point,1)
                  ypt1=y(j_point,1)
                  smin(j,k)=sqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
               enddo
               enddo
            endif
c
            if (.not.wake) then
            chord=0.d0
            fmin=100.d0
            do j=jtranlo-jtail1+1,jtranup-1-jtail1+1
               chord=chord+sqrt((x(j,1)-x(j+1,1))**2+
     $              (y(j,1)-y(j+1,1))**2)
            enddo
            chord_2=chord/2.d0
            do j=jtranlo-jtail1+1,jtranup-1-jtail1+1
               chord=chord+sqrt((x(j,1)-x(j+1,1))**2+
     $              (y(j,1)-y(j+1,1))**2)
               if (abs(chord-chord_2).lt.fmin) then
                  fmin=abs(chord-chord_2)
                  jmid=j+1
               endif
            enddo
c
            if (.not.restart) then
               do k=kbegin,kend
                  do j=jbegin,jend
                     turre(j,k)=retinf
                  enddo
               enddo
            endif
c
            do k=klow,kup
               do j=2,jmid                  
                  d_trip(j,k)=sqrt((x(j,k)-x(jtranlo,1))**2
     $                                  +(y(j,k)-y(jtranlo,1))**2)
               enddo
               do j=jmid+jbegin,jup
                  d_trip(j,k)=sqrt((x(j,k)-x(jtranup,1))**2
     $                                  +(y(j,k)-y(jtranup,1))**2)
               enddo
            enddo
c        
            delta_xlo=0.5*(sqrt((x(jtranlo+1,1)-x(jtranlo-1,1))**2+
     $                 (y(jtranlo+1,1)-y(jtranlo-1,1))**2))
            delta_xup=0.5*(sqrt((x(jtranup+1,1)-x(jtranup-1,1))**2+
     $                 (y(jtranup+1,1)-y(jtranup-1,1))**2))
         endif
c     
c      
         do k=klow,kup
            do j=jlow,jmid                  
               delta_u(j,k)=max(1.d-50,
     $             sqrt((workq(j,k,2)-workq(jtranlo,1,2))**2
     $             +(workq(j,k,3)-workq(jtranlo,1,3))**2))
            enddo
            do j=jmid+1,jup
               delta_u(j,k)=max(1.d-50,
     $             sqrt((workq(j,k,2)-workq(jtranup,1,2))**2
     $             +(workq(j,k,3)-workq(jtranup,1,3))**2))
            enddo
         enddo

         do j=jtail1,jtail2
            turre(j,1) = 0.0
         enddo
