      double precision function bc(jdim,jbc,jadd,x,y)
c
c     Written by Stan De Rango 23/06/97
c
c     -this routine returns the second derivative of the 
c      function y w.r.t x at node jbc.
c     -it computes a one-sided fourth-order accurate second-derivative
c     -it is set up for non-uniform grids and can use
c      either a completely upwind or downwind five-point stencil 
c      depending on the sign of jadd.
c
      implicit none
      integer j,j1,j2,j3,j4,j5,jadd,jbc,jdim
      double precision x(jdim),y(jdim),sgn
      double precision a,b,c,d,e,f,alp,bet,xi,eta,gam,tmp
      double precision abx,eab,abg,xea,xga,ega,bxe,bxg,beg,xeg
c
      sgn=sign(1.d0,dble(jadd))
      j=jbc
      j1=j+jadd
      j2=j+2*jadd
      j3=j+3*jadd
      j4=j+4*jadd
      j5=j+5*jadd
      xi=sgn*(x(j1)-x(j))
      bet=xi+sgn*(x(j2)-x(j1))
      alp=bet+sgn*(x(j3)-x(j2))
      gam=alp+sgn*(x(j4)-x(j3))
      eta=gam+sgn*(x(j5)-x(j4))
c
      abx=alp*bet*xi
      eab=eta*alp*bet
      abg=alp*bet*gam
      xea=xi*eta*alp
      xga=xi*gam*alp
      ega=eta*gam*alp
      bxe=bet*xi*eta
      bxg=bet*xi*gam
      beg=bet*eta*gam
      xeg=xi*eta*gam
c
      a=-2.d0*(abx+eab+abg+xea+xga+ega+bxe+bxg+beg+xeg)
      a=a/(abx*eta*gam)
c
      tmp=eab+abg+ega+beg
      b=2.d0*tmp/xi
      b=b/(eta*abg-xi*(tmp-xi*((bet+eta)*(alp+gam)+bet*eta+alp*gam
     &    -xi*(alp+eta+gam+bet-xi))))
c
      tmp=xea+xga+ega+xeg
      c=-2.d0*tmp/bet
      c=c/(-alp*xeg+bet*(tmp-bet*((alp+gam)*(xi+eta)+xi*eta+alp*gam
     &    -bet*(alp+xi+eta+gam-bet))))
c
      tmp=bxe+bxg+beg+xeg
      d=2.d0*tmp/alp
      d=d/(bet*xeg-alp*(tmp-alp*((bet+gam)*(xi+eta)+xi*eta+bet*gam
     &    -alp*(bet+xi+eta+gam-alp))))
c
      tmp=abx+eab+xea+bxe
      e=2.d0*tmp/gam
      e=e/(eta*abx-gam*(tmp-gam*((bet+alp)*(xi+eta)+alp*bet+xi*eta
     &    -gam*(alp+bet+xi+eta-gam))))
c
      tmp=abx+abg+xga+bxg
      f=-2.d0*tmp/eta
      f=f/(-alp*bxg+eta*(tmp-eta*((bet+alp)*(xi+gam)+alp*bet+xi*gam
     &    -eta*(alp+bet+xi+gam-eta))))
c
c      write (*,998) abx,eab,abg,xea,xga,ega,bxe,bxg,beg,xeg
c      write (*,999) xi,bet,alp,gam,eta
c      write (*,999) a,b,c,d,e,f
c      write(*,*) 'a=',a
c      write(*,*) 'b=',b
c      write(*,*) 'c=',c
c      write(*,*) 'd=',d
c      write(*,*) 'e=',e
c      write(*,*) 'f=',f
c      write (*,*) a+b+c+d+e+f
c 998  format(10f6.3)
c 999  format(6f12.4)
      bc=-(f*y(j5)+e*y(j4)+d*y(j3)+c*y(j2)+b*y(j1)+a*y(j))
c
      end


      
