      subroutine sa_frz(jdim, kdim, q, rsa, x, y, xy, xyj, uu, vv, fmu,
     &      vort)

#include "../include/arcom.inc"

      double precision q(jdim,kdim,5), rsa(jdim,kdim,5)
      double precision xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision fmu(jdim,kdim), uu(jdim,kdim), vv(jdim,kdim)
      double precision x(jdim,kdim), y(jdim,kdim), vort(jdim,kdim)
      double precision work(maxj,maxk), workq(maxj,maxk,3)

      double precision fwf(maxj,maxk), dwf(maxj,maxk)
      logical saf
      common/samf/ fwf, dwf, saf

      saf = .true.

      expon=1.d0/6.d0
      reinv=1.d0/re
      akarman = .41
      cb1     = 0.1355
      sigmainv= 1.5d0
      resiginv= sigmainv*reinv
      cb2     = 0.622
      cw1     = cb1/(akarman**2)+(1.0+cb2)*sigmainv
      cw2     = 0.3
c     -- underscore is equivalent to '**' i.e. exponentiation --
      cw3     = 2.0
      cw3_6   = 64.0
c     -- const is used to aviod the computation of ( )**1/6 in a loop --
      const   =(1.d0+cw3_6)**expon
      cv1     = 7.1
      cv1_3   = 357.911
c     -- setting ct1 to 5 instead of 1 helps convergence --
      ct1     = 5.0
      ct2     = 2.0
      ct3     = 1.2
      ct4     = 0.5

c      do k = kbegin,kend
c        do j = jbegin,jend
c          fwf(j,k) = uu(j,k)
c          dwf(j,k) = vv(j,k)
c        end do
c      end do 

c     -- compute generalized distance function --
c      do k = kbegin,kend
c        do j = jbegin,jend
c          j_point=j
c          if ((j.lt.jtail1).or.(j.gt.jtail2)) j_point=jtail1
c          xpt1 = x(j_point,kbegin)
c          ypt1 = y(j_point,kbegin)
c          smin(j,k) = dsqrt((x(j,k)-xpt1)**2+(y(j,k)-ypt1)**2)
c        end do
c      end do
c
c      do k=kbegin,kend
c        do j=jbegin,jend
c          rho = q(j,k,1)*xyj(j,k)
c          u   = q(j,k,2)/q(j,k,1)
c          v   = q(j,k,3)/q(j,k,1)
c          
c          workq(j,k,1)=rho
c          workq(j,k,2)=u
c          workq(j,k,3)=v
c        end do
c      end do

c     -- calculate vorticity at node j,k --
c      call vort_o2 (jdim, kdim, workq, xy, vort)
c
c      do k=1,kend
c        do j=1,jend
c          dwf(j,k) = vort(j,k)
c        end do
c      end do

cc     -- calculate production and destruction terms --
c      do k = klow,kup
c        do j = jlow,jup
cc     -- nu_tilde --
c          tnu = q(j,k,5)*xyj(j,k)
cc     -- chi = = tnu*rho/mul --
c          chi = tnu*q(j,k,1)*xyj(j,k)/fmu(j,k)
cc     -- chi**3 --
c          chi3 = chi*chi*chi
c          fv1 = chi3/(chi3+cv1_3)
c          fv2 = 1.d0 - chi/(1+chi*fv1)
cc     -- t1 = 1/(k**2.d**2)
c          t1 = 1.d0/(akarman*smin(j,k))**2
cc     -- S_tilde --
c          ts = vort(j,k)*re + tnu*t1*fv2
c         if (j.eq.110 .and. k.eq.4) write (*,*) 'ts_rsa',ts 
c          r = tnu/ts*t1
c
cc          if (dabs(r).ge.10.d0) then
cc            fw = const
cc          else
c            g = r + cw2*(r**6-r)
c            fw = g*( (1.0+cw3_6)/(g**6+cw3_6) )**expon 
c            fwf(j,k) = fw
ccc          endif
cc
cc          rinv = 1.d0/xyj(j,k)
ccc     -- production term --
cc          pr = rinv*cb1*reinv*ts*tnu
ccc     -- destruction term --
cc          t1 = tnu/smin(j,k)
cc          ds = rinv*cw1*reinv*fw*t1*t1
cc
ccc     -- S-A residual: storing -R(Q) for Newton's method --
ccc          rsa(j,k,5) = pr - ds
cc          rsa(j,k,5) = - ds
c        end do
c      end do

cc     -- advective terms in xi direction --
c      do k = klow,kup
c        do j = jlow,jup
c          t0 = dsign(1.d0, uu(j,k))
c          t1 = 0.5d0*(1.d0 + t0)
c          t2 = 0.5d0*(1.d0 - t0)
c
c          tnum = q(j-1,k,5)*xyj(j-1,k)
c          tnu  = q(j,k,5)*xyj(j,k)
c          tnup = q(j+1,k,5)*xyj(j+1,k)
c
c          rinv = 1.d0/xyj(j,k)
cc     -- S-A residual: storing -R(Q) for Newton's method --
c          rsa(j,k,5) = rsa(j,k,5) - uu(j,k)*rinv*( t1*( tnu - tnum ) +
c     &          t2*( tnup - tnu ) )
c        end do
c      end do
c      
cc     -- advective terms in eta direction --
c      do k = klow,kup
c        do j = jlow,jup
c          t0 = dsign(1.d0, vv(j,k))
c          t1 = 0.5d0*(1.d0 + t0)
c          t2 = 0.5d0*(1.d0 - t0)
c
c          tnum = q(j,k-1,5)*xyj(j,k-1)
c          tnu  = q(j,k,5)*xyj(j,k)
c          tnup = q(j,k+1,5)*xyj(j,k+1)
c
c          rinv = 1.d0/xyj(j,k)
cc     -- S-A residual: storing -R(Q) for Newton's method --
c          rsa(j,k,5) = rsa(j,k,5) - vv(j,k)*rinv*( t1*( tnu - tnum) +
c     &          t2*( tnup - tnu ) )
c        enddo
c      enddo
c
c     -- work = nu_laminar + nu_turbulent --
      do k = kbegin,kend
        do j = jbegin,jend
          tnu = q(j,k,5)*xyj(j,k)
          fwf(j,k) = fmu(j,k)/(q(j,k,1)*xyj(j,k)) + tnu
        end do
      end do
      
cc     -- diffusion terms in xi direction --
c      do k = klow,kup
c        do j = jlow,jup
c          jp1 = j+1
c          jm1 = j-1
c
c          xy1p = 0.5d0*( xy(j,k,1) + xy(jp1,k,1) )
c          xy2p = 0.5d0*( xy(j,k,2) + xy(jp1,k,2) )
c          ttp  = ( xy1p*xy(j,k,1) + xy2p*xy(j,k,2) )
c          
c          xy1m = 0.5d0*( xy(j,k,1) + xy(jm1,k,1) )
c          xy2m = 0.5d0*( xy(j,k,2) + xy(jm1,k,2) )
c          ttm  = ( xy1m*xy(j,k,1) + xy2m*xy(j,k,2) )
c          
c          rinv = 1.d0/xyj(j,k)
c
c          cnud = cb2*resiginv*work(j,k)*rinv
c          
c          cdp       =    ttp*cnud
c          cdm       =    ttm*cnud               
c
c          trem = 0.5d0*( work(jm1,k) + work(j,k) )
c          trep = 0.5d0*( work(j,k) + work(jp1,k) )
c
c          cap = ttp*trep*(1.0+cb2)*resiginv*rinv
c          cam = ttm*trem*(1.0+cb2)*resiginv*rinv
c
c          tnum = q(jm1,k,5)*xyj(jm1,k)
c          tnu  = q(j,k,5)*xyj(j,k)
c          tnup = q(jp1,k,5)*xyj(jp1,k)
c          
cc          rsa(j,k,5) = rsa(j,k,5) + cap*( tnup - tnu ) - cam*( tnu -
cc     &          tnum ) - cdp*( tnup - tnu ) - cdm*( tnu - tnum )
c        end do
c      end do   
c
cc     -- diffusion terms in eta direction --
c      do k = klow,kup
c        kp1 = k+1
c        km1 = k-1
c        do j = jlow,jup
c          xy3p = 0.5d0*( xy(j,k,3) + xy(j,kp1,3) )
c          xy4p = 0.5d0*( xy(j,k,4) + xy(j,kp1,4) )
c          ttp  = ( xy3p*xy(j,k,3) + xy4p*xy(j,k,4) )
c          
c          xy3m = 0.5d0*( xy(j,k,3) + xy(j,km1,3) )
c          xy4m = 0.5d0*( xy(j,k,4) + xy(j,km1,4) )
c          ttm  = ( xy3m*xy(j,k,3) + xy4m*xy(j,k,4) )
c
c          rinv = 1.d0/xyj(j,k)
c
c          cnud = cb2*resiginv*work(j,k)*rinv
c          
c          cdp = ttp*cnud
c          cdm = ttm*cnud
c          
c          trem = 0.5d0*(work(j,km1)+work(j,k))
c          trep = 0.5d0*(work(j,k)+work(j,kp1))
c          
c          cap  =  ttp*trep*(1.0+cb2)*resiginv*rinv
c          cam  =  ttm*trem*(1.0+cb2)*resiginv*rinv
c          
c          tnum = q(j,km1,5)*xyj(j,km1)
c          tnu  = q(j,k,5)*xyj(j,k)
c          tnup = q(j,kp1,5)*xyj(j,kp1)
c          
cc          rsa(j,k,5) = rsa(j,k,5) + cap*( tnup - tnu ) - cam*( tnu -
cc     &          tnum )- cdp*( tnup - tnu ) - cdm*( tnu - tnum )
c        end do
c      end do

cc     -- boundary conditions --
c
cc     -----------------------------------------------------------------
cc     -- airfoil body => J^(-1).nu_tilde = 0 --
cc     -----------------------------------------------------------------
c      do j = jtail1, jtail2
c        rsa(j,kbegin,5) = - q(j,kbegin,5)*xyj(j,kbegin)
c      end do
c
cc     -----------------------------------------------------------------
cc     -- outer-boundary  k = kmax --
cc     -----------------------------------------------------------------
c      do j = jbegin+1,jend-1
cc     -- metrics terms --
c        par = dsqrt(xy(j,kend,3)**2+(xy(j,kend,4)**2))
c        xy3 = xy(j,kend,3)/par
c        xy4 = xy(j,kend,4)/par
c
c        u   = q(j,kend,2)/q(j,kend,1)
c        v   = q(j,kend,3)/q(j,kend,1)
c        vn = xy3*u + xy4*v
c
c        if (vn.le.0.d0) then
cc     -- inflow --
c          rsa(j,kend,5) = - retinf
c        else
cc     -- outflow --          
c          rsa(j,kend,5) = -( q(j,kend,5)*xyj(j,kend) - q(j,kup,5)
c     &          *xyj(j,kup) ) 
c        endif
c      end do
c
cc     -----------------------------------------------------------------
cc     -- j = 1 and j = jend boundary --
cc     -----------------------------------------------------------------
c      do k = kbegin,kend
c        rsa(1,k,5) = -( q(1,k,5)*xyj(1,k) - q(2,k,5)*xyj(2,k) )
c        rsa(jend,k,5) = -( q(jend,k,5)*xyj(jend,k) - q(jup,k,5)
c     &        *xyj(jup,k) )
c      end do
c
cc     -----------------------------------------------------------------
cc     -- wake-cut --
cc     -----------------------------------------------------------------
c      do j = 2,jtail1-1
c        jj = jend - j + 1
c        rsa(j,1,5)  = -( q(j,1,5)*xyj(j,1) - 0.5d0*(q(j,2,5)
c     &        *xyj(j,2) + q(jj,2,5)*xyj(jj,2) ) )  
c        rsa(jj,1,5)  = -( q(jj,1,5)*xyj(jj,1) - 0.5d0*(q(j,2,5)
c     &        *xyj(j,2) + q(jj,2,5)*xyj(jj,2) ) )
c      end do

      return
      end                       !res_sa
