c----------------------------------------------------------------------
c     -- fill sparse jacobian matrix and preconditioner: x direction --
c     written by: marian nemec
c     date: july, 2000
c----------------------------------------------------------------------
      subroutine fillasx (jdim, kdim, ndim, indx, icol, q, xy, as, pa,
     &      diag)
      use disscon_vars      
#include "../include/arcom.inc"

      integer j, k, m, n, ij, ik, jdim, kdim, ndim
      integer indx(jdim,kdim), icol(9),jsta,jsto

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision diag(4,4,jdim,kdim)
      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)

c     -- column-major order, i.e. matrix columns are stored first... --

      hd = 0.5d0
      
      do k = 1,kend                                            
         do j = 1,jend                                              
            r1 = xy(j,k,1)*hd
            r2 = xy(j,k,2)*hd
            rr = 1.d0 / q(j,k,1)
            u = q(j,k,2)*rr
            v = q(j,k,3)*rr
            ut = u*u + v*v
            c1 = gami *ut*.5d0
            c2 = q(j,k,4)*rr*gamma
            qs = u*r1 + v*r2
            diag(1,1,j,k) = 0.d0
            diag(2,1,j,k) = r1
            diag(3,1,j,k) = r2
            diag(4,1,j,k) = 0.d0
            diag(1,2,j,k) = ( -u*u +c1)*r1   -u*v*r2
            diag(2,2,j,k) = - ( gamma -3.)*u*r1 + v *r2
            diag(3,2,j,k) = - gami*v*r1 + u*r2
            diag(4,2,j,k) = gami*r1
            diag(1,3,j,k) = - u*v*r1 + ( -v*v +c1)*r2
            diag(2,3,j,k) = v*r1 - gami*u*r2
            diag(3,3,j,k) = u*r1 +(3.-gamma)*v*r2
            diag(4,3,j,k) = gami*r2
            diag(1,4,j,k) = ( - c2 + c1*2.)*qs
            diag(2,4,j,k) = ( c2 -c1)*r1 - gami*u*qs
            diag(3,4,j,k) = ( c2 - c1)*r2 - gami*v*qs
            diag(4,4,j,k) = gamma*qs                              
         end do
      end do
      

c     -- first order jacobian preconditioner --
      if ( prec_mat ) then

        do k = klow,kup
          do j = jlow,jup

            itmp = ( indx(j,k) - 1 )*ndim
            do n = 1,4 
              ij  = itmp + n
              ii  = ( ij - 1 )*icol(5)
              do m =1,4
                pa(ii+        m) = - diag(m,n,jminus(j),k)
                pa(ii+icol(4)+m) =   diag(m,n,jplus(j),k)
              end do
            end do
          end do
        end do 
      end if

c     -- second order jacobian --
c     -- apply centered differencing to interior points --
      if ( jac_mat ) then

         if(.not.periodic) then
            jsta=jlow+1
            jsto=jup-1
         else
            jsta=jlow
            jsto=jup
         end if

        do k = klow+1,kup-1
          do j = jsta,jsto
            itmp = ( indx(j,k) - 1 )*ndim
            do n = 1,4 
              ij  = itmp + n
              ii  = ( ij - 1 )*icol(9)
              if (clopt) ii = ( ij - 1 )*(icol(9)+1)
              do m =1,4
                as(ii+icol(1)+m) = - diag(m,n,jminus(j),k)
                as(ii+icol(7)+m) =   diag(m,n,jplus(j),k)
              end do
            end do
          end do
        end do  

c     -- boundary nodes --

        if(.not.periodic) then

        j = jlow
        do k = klow+1,kup-1
          itmp = ( indx(j,k) - 1 )*ndim
          do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m =1,4
              as(ii+        m) = - diag(m,n,jminus(j),k)
              as(ii+icol(6)+m) =   diag(m,n,jplus(j),k)
            end do
          end do
        end do

        j = jup
        do k = klow+1,kup-1
          itmp = ( indx(j,k) - 1 )*ndim
          do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m =1,4
              as(ii+icol(1)+m) = - diag(m,n,jminus(j),k)
              as(ii+icol(7)+m) =   diag(m,n,jplus(j),k)
            end do
          end do
        end do

        end if

        k = klow
        do j = jsta,jsto
          itmp = ( indx(j,k) - 1 )*ndim
          do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m =1,4
              as(ii+icol(1)+m) = - diag(m,n,jminus(j),k)
              as(ii+icol(6)+m) =   diag(m,n,jplus(j),k)
            end do
          end do
        end do

        k = kup
        do j = jsta,jsto
          itmp = ( indx(j,k) - 1 )*ndim
          do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            if (clopt) ii = ( ij - 1 )*(icol(9)+1)
            do m =1,4
              as(ii+icol(1)+m) = - diag(m,n,jminus(j),k)
              as(ii+icol(6)+m) =   diag(m,n,jplus(j),k)
            end do
          end do
        end do

        if(.not.periodic) then

        j = jlow
        k = klow
        itmp = ( indx(j,k) - 1 )*ndim
        do n = 1,4 
          ij  = itmp + n
          ii  = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m =1,4
            as(ii+        m) = - diag(m,n,jminus(j),k)
            as(ii+icol(5)+m) =   diag(m,n,jplus(j),k)
          end do
        end do

        j = jlow
        k = kup
        itmp = ( indx(j,k) - 1 )*ndim
        do n = 1,4 
          ij  = itmp + n
          ii  = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m =1,4
            as(ii+        m) = - diag(m,n,jminus(j),k)
            as(ii+icol(5)+m) =   diag(m,n,jplus(j),k)
          end do
        end do

        j = jup
        k = klow
        itmp = ( indx(j,k) - 1 )*ndim
        do n = 1,4 
          ij  = itmp + n
          ii  = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m =1,4
            as(ii+icol(1)+m) = - diag(m,n,jminus(j),k)
            as(ii+icol(6)+m) =   diag(m,n,jplus(j),k)
          end do
        end do

        j = jup
        k = kup
        itmp = ( indx(j,k) - 1 )*ndim
        do n = 1,4 
          ij  = itmp + n
          ii  = ( ij - 1 )*icol(9)
          if (clopt) ii = ( ij - 1 )*(icol(9)+1)
          do m =1,4
            as(ii+icol(1)+m) = - diag(m,n,jminus(j),k)
            as(ii+icol(6)+m) =   diag(m,n,jplus(j),k)
          end do
        end do

        end if

      end if

      return
      end                       !fillasx
