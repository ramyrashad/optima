#ifndef STIFFTRI_H
#include "stiffTri.h"
#endif

/*
  Purpose:  form the element stiffness matrix
  Element type:  linear triangle
*/


void get_K_Tri (const vector<double*>& vertices, 
		double** K, const double& elemQo, double& meshQ,
		const Face& face)
{
  int i, j;
  double Q ;   // Q = measure of element quality
  double area = face.get_area();  ; // area of triangle

  if (meshQ == -1) // 1st iteration
    Q = 1;
  else
    {
      get_Q (vertices, area, Q);  
      if (Q > meshQ)
	meshQ = Q;
      Q = pow(Q/elemQo, PowLaw);      
    }

   // set up material matrix, assume plane strain
  double* D = new double[3];
  D[0] = Q*(1.0-Poisson);
  D[1] = Q*Poisson;
  D[2] = Q*(0.5-Poisson);

  //D[0] = 1; D[1] = .3; D[2] = .35;

  vector < vector <double> > B (3, vector<double> (6,0.0) );    

  // initialize the elements in stiffness matrix
  for (i=0; i<6; i++)    
    for (j=0; j<6; j++)    
      K[i][j] = 0.0;  
  
  calc_B (vertices, B);

  // Ke = area*depth*BT*D*B
  // depth = 1
  // BT = transpose(B)
  // B = 1/Jacobian[ ]  where jacobian = 2*area

  calc_Ke (B, K, D);
   for (i=0; i<6; i++)    
    for (j=0; j<6; j++)    
      K[i][j] = K[i][j]/(4.0*area*area);  

   delete [] D;   D = NULL;

  return;

}



void calc_B (const vector <double*>& vertices, 
	     vector < vector <double> >& B)
{

  B[0][0] = vertices[1][1] - vertices[2][1]; 
  B[0][2] = vertices[2][1] - vertices[0][1];
  B[0][4] = vertices[0][1] - vertices[1][1];

  B[1][1] = vertices[2][0] - vertices[1][0];
  B[1][3] = vertices[0][0] - vertices[2][0];
  B[1][5] = vertices[1][0] - vertices[0][0];

  B[2][0] = B[1][1];          B[2][1] = B[0][0];
  B[2][2] = B[1][3];          B[2][3] = B[0][2];
  B[2][4] = B[1][5];          B[2][5] = B[0][4];

  return;
}


void calc_Ke (const vector < vector <double> >& B, double** I,
	      const double* D)
{
 
  vector < vector <double> > tempI (6, vector <double> (3, 0.0));

  // calculate tempI = transpose(B) * D
  tempI[0][0] = B[0][0]*D[0]; 
  tempI[0][1] = B[0][0]*D[1]; 
  tempI[0][2] = B[2][0]*D[2]; 

  tempI[1][0] = B[1][1]*D[1]; 
  tempI[1][1] = B[1][1]*D[0]; 
  tempI[1][2] = B[2][1]*D[2]; 

  tempI[2][0] = B[0][2]*D[0]; 
  tempI[2][1] = B[0][2]*D[1]; 
  tempI[2][2] = B[2][2]*D[2]; 

  tempI[3][0] = B[1][3]*D[1]; 
  tempI[3][1] = B[1][3]*D[0]; 
  tempI[3][2] = B[2][3]*D[2]; 

  tempI[4][0] = B[0][4]*D[0]; 
  tempI[4][1] = B[0][4]*D[1]; 
  tempI[4][2] = B[2][4]*D[2]; 

  tempI[5][0] = B[1][5]*D[1]; 
  tempI[5][1] = B[1][5]*D[0]; 
  tempI[5][2] = B[2][5]*D[2]; 

  // calculate I = tempI*B
  for (int i=0; i<6; i++)    
    for (int j=0; j<6; j++)    
      for (int k=0; k<3; k++)
	I[i][j] += tempI[i][k]*B[k][j];

 return;

}


void get_Q (const vector<double*>& vertices, 
	     const double& area, double& Q)
{
  
  // stores the lengths of the sides of the triangle
  double* len = new double[3];  
  
  Q = vertices[0][0] - vertices[1][0];  
  len[0] = vertices[0][1] - vertices[1][1];
  len[0] = sqrt( Q*Q + len[0]*len[0] );

  Q = vertices[1][0] - vertices[2][0] ;  
  len[1] = vertices[1][1] - vertices[2][1];
  len[1] = sqrt( Q*Q + len[1]*len[1] );

  Q = vertices[2][0] - vertices[0][0] ;  
  len[2] = vertices[2][1] - vertices[0][1];
  len[2] = sqrt( Q*Q + len[2]*len[2] );
  
  //  Q (element quality) is calculated using norm-2 
  Q = 0.125*(len[1]+len[0]+len[2])*len[1]*len[0]*len[2]/(area*area);

   delete [] len;    len = NULL;

  return;
}


