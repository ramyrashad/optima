c----------------------------------------------------------------------
c     -- differentiation of spectral radius for JST dissipation --
c     -- m. nemec, sept. 2001 --
c----------------------------------------------------------------------
      subroutine dspectx (jmax, kmax, nmax, indx, icol, q, xy, xyj, uu,
     &     sndsp, as, coef2, coef4, ibc1, ibc2, ibc3, ibc4, 
     &     jbegin, jend, jlow, jup, jminbnd, jmaxbnd, kminbnd, kmaxbnd, 
     &     dis2x, dis4x, g2)

      implicit none

#include "../include/parms.inc"

      integer ibc1, ibc2, ibc3, ibc4
      integer jmax,kmax,indx(jmax,kmax),icol(*),jbegin,jend,kbegin,kend
      integer jminbnd, jmaxbnd,kminbnd,kmaxbnd,jlow,jup,klow,kup,j,k,n,m
      integer js,je,ks,ke,ii,ij,itmp,nmax,j1,j2,jp,jm,jm1,jp1
      integer jjbegin,jjend,jjlow,jjup
      double precision q(jmax,kmax,4),xy(jmax,kmax,4),xyj(jmax,kmax)
      double precision sndsp(jmax,kmax),uu(jmax,kmax),coef2(jmax,kmax)
      double precision coef4(jmax,kmax),as(*)

c     -- local variables --
      double precision dspdq(4,maxjb,maxkb), work(maxjb,maxkb,2,4) 
      double precision pcoef(maxjb,maxkb),   spect(maxjb,maxkb)
      double precision c2(maxjb,maxkb),      c4(maxjb,maxkb)
      double precision db(4,4,maxjb,maxkb),  bm1(4,4,maxjb,maxkb)
      double precision bp1(4,4,maxjb,maxkb), duudq(3)
      double precision ri,ri2,t1,t2,t3,dadq1,dadq2,dadq3,dadq4,sqm,g2
      double precision d4m,d4,d4p,dis2x,dis4x,pcm,pc,pcp

      logical frozen
      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

      do k=1,maxkb
         do j=1,maxjb
            do n=1,4
               do m=1,4
                  bm1(m,n,j,k) = 0.d0
                  db(m,n,j,k)  = 0.d0
                  bp1(m,n,j,k) = 0.d0
               end do
            end do
         end do
      end do

      kbegin = kminbnd
      kend   = kmaxbnd
      klow   = kminbnd + 1
      kup    = kmaxbnd - 1

c     -- form derivative of spectral radius at node j,k --
c     -- first: take derivative of the speed of sound --
      do k = kbegin,kend
         do j = jbegin,jend

            ri  = 1.0/q(j,k,1)
            ri2 = ri*ri

            t1 = 0.5/sndsp(j,k)
            t2 = q(j,k,2)*q(j,k,2)
            t3 = q(j,k,3)*q(j,k,3)

            dadq1 = g2*ri2*( (t2+t3)*ri - q(j,k,4) )
            dadq2 = - g2*q(j,k,2)*ri2
            dadq3 = - g2*q(j,k,3)*ri2
            dadq4 = g2*ri

            sqm = sqrt( xy(j,k,1)**2+xy(j,k,2)**2 )*t1

            dspdq(1,j,k) = dadq1*sqm 
            dspdq(2,j,k) = dadq2*sqm 
            dspdq(3,j,k) = dadq3*sqm
            dspdq(4,j,k) = dadq4*sqm
         end do
      end do      

c     -- second: take derivative of contravariant velocity --
      if (.not. frozen) then
         do k = kbegin,kend
            do j = jbegin,jend
               
               ri  = 1.0/q(j,k,1)
               ri2 = ri*ri
               duudq(1) = - ri2*(q(j,k,2)*xy(j,k,1) +q(j,k,3)*xy(j,k,2))
               duudq(2) = ri*xy(j,k,1) 
               duudq(3) = ri*xy(j,k,2)
               
               if ( uu(j,k) .lt. 0.0 ) then
                  do n = 1,3
                     duudq(n) = - duudq(n)
                  end do
               end if
               dspdq(1,j,k) = duudq(1) + dspdq(1,j,k)
               dspdq(2,j,k) = duudq(2) + dspdq(2,j,k)
               dspdq(3,j,k) = duudq(3) + dspdq(3,j,k)
            end do
         end do      
      end if
cmt ~~~ needs clean up - begin ~~~
c     -- form dissipation differences --
      if (ibc2.EQ.0) then
         jjbegin = jminbnd - 2
         jjlow   = jminbnd - 1
      else
         jjbegin = jminbnd
         jjlow   = jminbnd + 1
      end if
      
      if (ibc4.EQ.0) then
         jjend = jmaxbnd + 2
         jjup  = jmaxbnd + 1
      else
         jjend = jmaxbnd
         jjup  = jmaxbnd - 1
      end if 

      do n = 1,4                                                     

c     -- 1st-order forward difference --
         do k =klow,kup                                                
            do j =jjbegin,jjup
               jp1=j+1
               work(j,k,1,n) = q(jp1,k,n)*xyj(jp1,k) - q(j,k,n)*xyj(j,k)
            end do
         end do

c     -- jmax bc --
         j = jjup+1
         do k = klow,kup                                             
            work(j,k,1,n) = work(j-1,k,1,n)
         end do

c     -- apply cent-dif to 1st-order forward --
         do k =klow,kup                                                
            do j =jjlow,jjup
               jp1 = j+1
               jm1 = j-1
               work(j,k,2,n) = work(jp1,k,1,n) - 2.0*work(j,k,1,n) +
     &              work(jm1,k,1,n) 
            end do
         end do

c     -- mesh bc --
         j1 = jjbegin                                                    
         j2 = jjend                                                      
         do k =klow,kup                                             
            work(j1,k,2,n) = q(j1+2,k,n)*xyj(j1+2,k) - 2.0*q(j1+1,k,n)
     &           *xyj(j1+1,k) + q(j1,k,n)*xyj(j1,k)         
            work(j2,k,2,n) = 0.0
         end do
      end do
cmt ~~~ needs clean up - end ~~~

c     -- taken from coef24x.f --
      do k = kbegin,kend                                              
         do j = jbegin,jend
            pcoef(j,k) = coef2(j,k)
            spect(j,k) = coef4(j,k)
         end do
      end do

      do k = kbegin,kend                                            
         do j = jbegin,jup                                         
            jp = j+1
            c2(j,k) = dis2x*(pcoef(jp,k)*spect(jp,k) +
     &           pcoef(j,k)*spect(j,k))
            coef2(j,k) = c2(j,k)
            c4(j,k) = dis4x*(spect(jp,k) + spect(j,k))
            coef4(j,k) = c4(j,k) - min( c4(j,k),c2(j,k) )
         end do
      end do

c     -- linearize c4 = c4 - min(c4,c2) --
c     -- pressure switch (pcoef) is treated as constant --
      do k = klow,kup
         do j = jlow,jup
            jm = j-1
            jp = j+1

            pcm = dis2x*pcoef(jm,k)/xyj(jm,k)
            pc  = dis2x*pcoef(j,k)/xyj(j,k)
            pcp = dis2x*pcoef(jp,k)/xyj(jp,k)

            d4m = dis4x/xyj(jm,k)
            d4  = dis4x/xyj(j,k)
            d4p = dis4x/xyj(jp,k)

            if ( c4(j,k) .le. c2(j,k) ) then
c     -- no O(3) dissipation: c4 = 0 --
               do n = 1,4
                  do m = 1,4
                     bm1(m,n,j,k) = pcm*dspdq(m,jm,k)*work(jm,k,1,n)
                     db(m,n,j,k) = - pc*dspdq(m,j,k)*(work(j,k,1,n) -
     &                    work(jm,k,1,n))
                     bp1(m,n,j,k) = - pcp*dspdq(m,jp,k)*work(j,k,1,n)
c     write (*,*) bm1(m,n,j,k),db(m,n,j,k),bp1(m,n,j,k)
                  end do
               end do
            else
c     -- mostly O(3) dissipation: c4 = c4 - c2 --
               do n = 1,4
                  do m = 1,4
                     bm1(m,n,j,k) = - d4m*dspdq(m,jm,k)*work(jm,k,2,n) +
     &                    pcm*dspdq(m,jm,k)*work(jm,k,1,n)  
                     db(m,n,j,k) = d4*dspdq(m,j,k)*(work(j,k,2,n) -
     &                    work(jm,k,2,n) ) - pc*dspdq(m,j,k)*( work(j,k
     &                    ,1,n)-work(jm,k,1,n) ) 
                     bp1(m,n,j,k) = d4p*dspdq(m,jp,k)*work(j,k,2,n) -
     &                    pcp*dspdq(m,jp,k)*work(j,k,1,n) 
c     write (*,*) bm1(m,n,j,k),db(m,n,j,k),bp1(m,n,j,k)
                  end do
               end do
            end if
         end do
      end do
cmt >>>third-end<<<

c     -- add to jacobian, follows routine fillax.f --
      ks = kminbnd + 2
      ke = kmaxbnd - 2
      if (ibc2.EQ.0) then
         js = jminbnd
      else
         js = jminbnd + 2
      end if
      if (ibc4.EQ.0) then
         je = jmaxbnd
      else
         je = jmaxbnd - 2
      end if

      do k = ks,ke
         do j = js,je
            itmp = ( indx(j,k) - 1 )*nmax
            do n = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
                  as(ii+icol(7)+m) = as(ii+icol(7)+m) + bp1(m,n,j,k)
               end do
            end do
         end do
      end do        

      if (ibc2.EQ.0) then
         js = jminbnd
      else
         js = jminbnd + 2
      end if
      if (ibc4.EQ.0) then
         je = jmaxbnd
      else
         je = jmaxbnd - 2
      end if

      k = kminbnd+1       
      do j = js,je
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k) 
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k)
            end do
         end do
      end do

      k = kmaxbnd-1
      do j = js,je
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k) 
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k) 
            end do
         end do
      end do


      if (ibc2.NE.0) then
         j   = jminbnd + 1
         ks  = kminbnd + 2
         ke  = kmaxbnd - 2

         do k = ks,ke
            itmp = ( indx(j,k) - 1 )*nmax
            do n = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii        +m) = as(ii        +m) + bm1(m,n,j,k) 
                  as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
                  as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k)
               end do
            end do
         end do

         k = kminbnd + 1
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii        +m) = as(ii        +m) + bm1(m,n,j,k) 
               as(ii+icol(2)+m) = as(ii+icol(2)+m) + db(m,n,j,k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
            end do
         end do

         k = kmaxbnd - 1
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii        +m) = as(ii        +m) + bm1(m,n,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(5)+m) = as(ii+icol(5)+m) + bp1(m,n,j,k)
            end do
         end do
      end if
      
      if (ibc4.NE.0) then
         j   = jmaxbnd - 1
         jm1 = j-1
         jp1 = j+1
         ks  = kminbnd + 2
         ke  = kmaxbnd - 2

         do k = ks,ke
            itmp = ( indx(j,k) - 1 )*nmax
            do n = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)  
                  as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
                  as(ii+icol(7)+m) = as(ii+icol(7)+m) + bp1(m,n,j,k) 
               end do
            end do
         end do


         k = kminbnd + 1 
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)
               as(ii+icol(3)+m) = as(ii+icol(3)+m) + db(m,n,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k)
            end do
         end do

         k = kmaxbnd - 1
         itmp = ( indx(j,k) - 1 )*nmax
         do n = 1,4 
            ij  = itmp + n
            ii  = ( ij - 1 )*icol(9)
            do m =1,4
               as(ii+icol(1)+m) = as(ii+icol(1)+m) + bm1(m,n,j,k)
               as(ii+icol(4)+m) = as(ii+icol(4)+m) + db(m,n,j,k)
               as(ii+icol(6)+m) = as(ii+icol(6)+m) + bp1(m,n,j,k)
            end do
         end do
      end if

      return
      end                       !dspectx
