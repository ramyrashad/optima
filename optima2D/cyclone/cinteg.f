c #########################
c ##                     ##
c ##  subroutine cinteg  ##
c ##                     ##
c #########################
c
      subroutine cinteg(jdim,kdim,qp,q,qold,s,press,sndsp,
     >      turmu,fmu,vort,x,y,xy,xyj,xit,ett,ds,spectx,specty,uu,vv,
     >      ccx,ccy,coef2x,coef2y,coef4x,coef4y,lux,luy,precon,gam)    
c
c                                                                      
#include "../include/arcom.inc"
c
      logical fwd,turbsave                                              
c
      dimension qp(jdim*kdim*4),q(jdim*kdim*4),qold(jdim*kdim*4)    
      dimension press(jdim*kdim),sndsp(jdim*kdim)                       
      dimension s(jdim*kdim*4),xy(jdim*kdim*4),xyj(jdim*kdim)           
      dimension xit(jdim*kdim),ett(jdim*kdim),ds(jdim*kdim)             
      dimension x(jdim*kdim),y(jdim*kdim),turmu(jdim*kdim)              
      dimension vort(jdim*kdim),fmu(jdim*kdim)
      dimension precon(jdim*kdim*6),gam(jdim*kdim*16)
c
      dimension spectx(jdim,kdim,3),specty(jdim,kdim,3)
      dimension uu(jdim*kdim),ccx(jdim*kdim)                            
      dimension vv(jdim*kdim),ccy(jdim*kdim)                            
      dimension coef2x(jdim*kdim),coef2y(jdim*kdim)                     
      dimension coef4x(jdim*kdim),coef4y(jdim*kdim)
      real*8    lux(jdim,kdim,4,5),luy(jdim*kdim*4*5)
c
      common/worksp/a0(maxj),b0(maxj),c0(maxj),f0(maxj),g0(maxj),       
     >       z0(maxj),qbcj(3,maxj,4),qbck(maxj,3,4)
c
      common/scratch/ a,b,c,d,e,work1

c     !!!!!! note : this use of d and e  array in equivalence to save      
c                storage is very dangerous.  in cases where d is        
c                used, such as the stepx and stepy routines values      
c                residing in equivalenced arrays may be bad.            
                                                                        
c                                                                       
      dimension a(maxpen,4),b(maxpen,16),c(maxpen,16)
      dimension d(maxjk,16),e(maxpen,4),work1(maxjk,10)
c      dimension worka(maxjk),workb(maxjk,8)
c      dimension workc(maxjk),workd(maxjk,4)
c                             
c      equivalence (d(1,1),worka),(d(1,2),workb),                        
c     *            (d(1,10),workc),(d(1,11),workd)                       
c                                                                       
c
c---- initializing some array indices 
c     (debugger may get picky if these are zero)
      ipdu0=1
      ipdu1=1
      iplu0=1
      iplu1=1
      ipett0=1
      ipett1=1
c
c
c---- save limits of integration ----
      jlowss   = jlow                                                
      jupss    = jup                                                 
      jbeginss = jbegin                                              
      jendss   = jend                                                
      klowss   = klow                                                
      kupss    = kup                                                 
      kbeginss = kbegin                                              
      kendss   = kend                                                
c
c                                                                       
c                                                                       
c---- use variable dt based on eigenvalues if jacdt > 1 ----
      if (jacdt.gt.1)
     >call varidt (jacdt,jdim,kdim,qp,sndsp,xy,xyj,ds,precon)      
c
c
c
c
c                      1.  laminar viscosity coef.
c=====================================================================
c
c
c---- compute laminar viscosity based on sutherlands law ----
      if(viscous) call fmun (jdim,kdim,qp,press,fmu)                 
c                                                                       
c     : for cmesh logic the wake cut is done implicitly.                     
c                                                                       
c
C
      if (meth.eq.1 .or. meth.eq.4 .or. meth.eq.5) then
c*********************************************************************
c*********************************************************************
c***          Subiteration component for explicit side             ***
c***                implemented by Stan De Rango                   ***
c***                       (Aug 12,1994)                           ***
c*********************************************************************
c*********************************************************************
c
       call subit(jdim,kdim,qp,q,qold,s)
c
c  logic for `c' mesh integration in xi requires special handleing      
c  of lines coming off of the trailing edge                             
c--------------------------------------------------------------------   
c  set integration limits                                               
c--------------------------------------------------------------------   
c                                                                       
         kbegin  = 1                                                    
         kend    = 1                                                    
         klow    = kbegin                                               
         kup     = kend                                                 
         jbegin  = jtail2                                               
         jend    = jmax                                                 
         jlow    = jbegin+1                                             
         jup     = jend-1                                               
c
       call subit(jdim,kdim,qp,q,qold,s)
c
c
c  overload k = 1; j = 1,jtail1  into k = 1 for j = jtail2 to jmax      
c                                                                       
c       ndim = 4                                                         
c       call overlap(jdim,kdim,ndim,s,jmax,jtail1)                       
c--------------------------------------------------------------------   
c  reset limits of integration                                          
c--------------------------------------------------------------------   
c                                                                       
          jlow   = jlowss                                               
          jup    = jupss                                                
          jbegin = jbeginss                                             
          jend   = jendss                                               
          klow   = klowss                                               
          kup    = kupss                                                
          kbegin = kbeginss                                             
          kend   = kendss
c
      endif
c
c                         2.  explicit xi rhs
c=====================================================================
c                               
c
c---- normal integration of interior in xi ----
      call xiexpl (jdim,kdim,qp,s,press,sndsp,turmu,fmu,vort,x,y,xy,     
     >             xyj,xit,ds,uu,ccx,coef2x,coef4x,spectx,precon,gam)
c
c
c      call modmet(jdim,kdim,qp,xy,xyj,x,y)
c---- integration of points in upper discontinuity --
c     : logic for `c' mesh integration in xi requires special handleing      
c     : of lines coming off of the trailing edge                             
c  -- set integration limits --
      kbegin  = 1                                                    
      kend    = 1                                                    
      klow    = kbegin                                               
      kup     = kend                                                 
      jbegin  = jtail2                                               
      jend    = jmax                                                 
      jlow    = jbegin+1                                             
      jup     = jend-1                                               
c  -- call explicit subroutine --                                     
      call xiexpl (jdim,kdim,qp,s,press,sndsp,turmu,fmu,vort,x,y,xy,     
     >             xyj,xit,ds,uu,ccx,coef2x,coef4x,spectx,precon,gam)
c  -- reset limits of integration --
      jlow   = jlowss                                               
      jup    = jupss                                                
      jbegin = jbeginss                                             
      jend   = jendss                                               
      klow   = klowss                                               
      kup    = kupss                                                
      kbegin = kbeginss                                             
      kend   = kendss                                               
c
c      call modmet2(jdim,kdim,qp,xy,xyj,x,y)
c
c---- integrate in eta ----
c     : !!!!!!!  note viscross not implemented for cmesh
c
c
c
c
c                       3.  explicit eta rhs
c=====================================================================
c
c
c                           shuffle data
c                           ------------
c                                                                       
      fwd = .true.                                                     
c                                                                       
c  -- shuffle q --  
      ndim = 4                                                         
      call shuffl(jdim,kdim,ndim,qp,work1,jtail1,jtail2,                
     >            jmax,kmax,fwd,ipq0,ipq1)                              
c  -- shuffle s --                                                            
      ndim = 4
      call shuffl(jdim,kdim,ndim,s,work1,jtail1,jtail2,
     >            jmax,kmax,fwd,ips0,ips1)
c  -- shuffle xy --
       ndim = 4
       call shuffl(jdim,kdim,ndim,xy,work1,jtail1,jtail2,               
     >             jmax,kmax,fwd,ipxy0,ipxy1)                            
c  -- shuffle xyj --
       ndim = 1
       call shuffl(jdim,kdim,ndim,xyj,work1,jtail1,jtail2,              
     *             jmax,kmax,fwd,ipxyj0,ipxyj1)                          
c  -- shuffle x --
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,x,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipx0,ipx1)                              
c  -- shuffle y --
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,y,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipy0,ipy1)                              
c  -- shuffle ett --
c       ndim = 1                                                         
c       call shuffl(jdim,kdim,ndim,ett,work1,jtail1,jtail2,              
c     *            jmax,kmax,fwd,ipett0,ipett1)                          
       if (viscous) then
c         -- shuffle turmu --
          ndim = 1                                                         
          call shuffl(jdim,kdim,ndim,turmu,work1,jtail1,jtail2,            
     *               jmax,kmax,fwd,iptu0,iptu1)                            
c         -- shuffle fmu --                                                         
          ndim = 1                                                         
          call shuffl(jdim,kdim,ndim,fmu,work1,jtail1,jtail2,              
     *               jmax,kmax,fwd,ipfu0,ipfu1)
c         -- shuffle vort --                                                        
          ndim = 1                                                         
          call shuffl(jdim,kdim,ndim,vort,work1,jtail1,jtail2,             
     *               jmax,kmax,fwd,ipvt0,ipvt1)                            
       endif
c  -- shuffle press --                                                       
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,press,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ippr0,ippr1)                            
c  -- shuffle sndsp --                                                       
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,sndsp,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ipsnd0,ipsnd1)                          
c  -- shuffle ds --                                                          
       ndim = 1                                                         
       call shuffl(jdim,kdim,ndim,ds,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipds0,ipds1)
c
      if (prec.gt.0) then
c        -- shuffle precon --
         ndim = 6                                                         
         call shuffl(jdim,kdim,ndim,precon,work1,jtail1,jtail2,            
     *           jmax,kmax,fwd,ipdu0,ipdu1) 
      endif
c
c  call y integrator                                                    
c
c
c
c                    airfoil block integration
c                    -------------------------
c
c---- integration ----                                           
      wake = .false.                                                    
c  -- set integration limits --
      jbegin   = 1                                                   
      jend     = jtail2-jtail1+1                                     
      jlow     = jbegin
      jup      = jend                                                
      kbegin   = 1                                                   
      kend     = kmax                                                
      klow     = kbegin+1                                            
      kup      = kend-1                                              
      idimj = jtail2-jtail1+1                                           
      idimk = kmax                                                      
      call etaexpl(idimj,idimk,qp(ipq0),s(ips0),press(ippr0),           
     >     sndsp(ipsnd0),turmu(iptu0),fmu(ipfu0),vort(ipvt0),x(ipx0),   
     >     y(ipy0),xy(ipxy0),xyj(ipxyj0),ett(ipett0),ds(ipds0),         
     >     uu(ipx0),vv(ipx0),ccy(ipx0),coef2y(ipx0),coef4y(ipx0),specty,
     >     precon(ipdu0),gam)   
c                                                                       
c
c
c---- 4.  compute residual and scale by variable dt ----
c  -- 4-a. compute l2 and max norms of density residual --
      nres = 1                                                      
      call residl2 (idimj,idimk,4,s(ips0),nres,0.d0)                        
      resid1 = resid                                                    
      resid1mx = residmx                                                
      mxresj1 = maxres(1)                                               
      mxresk1 = maxres(2)                                               
c
c
c
c  -- 4-b. scale with variable dt --
      call scaledt (idimj,idimk,s(ips0),ds(ipds0))                   
c                                                                       
c     : note !!!! for now turbulence model is turned off in the wake.        
c     :      this is because the turbulence model assumes that          
c     :      k = 1 is a surface and in the wake block k = 1 is a        
c     :      far field boundary.                                        
c     :                                                                 
ccccc :       turbsave = turbulnt                                       
ccccc :       turbulnt = .false.                                        
c
c
c
c                      wake block integration
c                      ----------------------
c
c---- integration ----
      wake = .true.                                                   
c  -- set integration limits --
      jbegin  = 1 
      jend    = jtail1-1
      jlow    = jbegin
      jup     = jend-1
      kbegin  = 1
      kend    = 2*kmax-1
      klow    = kbegin+1
      kup     = kend-1
      idimj = jtail1-1
      idimk = 2*kmax-1
c  -- multiply the metrics for j = 1,jtail1-1  k = 1,kmax by -1 to
c     ... account for the rearrangement of coordinate directions --    
      jb = 1                                                            
      je = idimj                                                        
      kb = 1                                                            
      ke = kmax-1                                                       
      nb = 1                                                            
      ne = 4                                                            
      ndim = 4                                                          
      call swsign(idimj,idimk,ndim,xy(ipxy1),nb,ne,jb,je,kb,ke)         
c  -- integrate --
      call etaexpl(idimj,idimk,qp(ipq1),s(ips1),press(ippr1),           
     >     sndsp(ipsnd1),turmu(iptu1),fmu(ipfu1),vort(ipvt1),x(ipx1),   
     >     y(ipy1),xy(ipxy1),xyj(ipxyj1),ett(ipett1),ds(ipds1),         
     >     uu(ipx1),vv(ipx1),ccy(ipx1),coef2y(ipx1),coef4y(ipx1),specty,
     >     precon(ipdu1),gam)   
c                                                                       
c
c---- 4.  compute residual and scale by variable dt ----
c  -- 4-a. compute l2 and max norms of density residual --
      nres = 2                                                      
      call residl2 (idimj,idimk,4,s(ips1),nres,0.d0)                        
      resid2 = resid                                                    
      resid2mx = residmx                                                
      mxresj2 = maxres(1)                                               
      mxresk2 = maxres(2)                                               
c  -- 4-b. scale with variable dt --
c           ? on pourrait faire tout le scale a la fin, une fois 
c           ? retrouve la topologie de la c-mesh. et pas besoin d'appeler 
c           ? une subroutine : directement! ca sauve du temps. 
      call scaledt (idimj,idimk,s(ips1),ds(ipds1))                   
c                                                                       
c                                                                       
c---- multiple the metrics for j = 1,jtail1-1  k = 1,kmax by -1 to 
c     ... account for the rearrangement of coordinate directions ----
      jb = 1
      je = idimj
      kb = 1
      ke = kmax-1
      nb = 1
      ne = 4
      ndim = 4
      call swsign (idimj,idimk,ndim,xy(ipxy1),nb,ne,jb,je,kb,ke)
c                                                                       
c                                                                       
c---- reset j limits ----
      jlow    = jlowss                                               
      jup     = jupss                                                
      jbegin  = jbeginss                                             
      jend    = jendss                                               
      klow    = klowss                                               
      kup     = kupss                                                
      kbegin  = kbeginss                                             
      kend    = kendss                                               
c                                                                       
c   reset turbulnt                ???                                       
cccc         turbulnt = turbsave  ???                                   
c
c
c
c                         unshuffle data
c                         --------------
c
      fwd = .false.                                                   
c                                                                       
c  -- shuffle q --
      ndim = 4                                                         
      call shuffl(jdim,kdim,ndim,qp,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipq0,ipq1)                              
c  -- shuffle s --
      ndim = 4                                                         
      call shuffl(jdim,kdim,ndim,s,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ips0,ips1)                              
c  -- shuffle xy --
      ndim = 4                                                         
      call shuffl(jdim,kdim,ndim,xy,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipxy0,ipxt1)                            
c  -- shuffle xyj --
      ndim = 1                                                         
      call shuffl(jdim,kdim,ndim,xyj,work1,jtail1,jtail2,              
     *            jmax,kmax,fwd,ipxyj0,ipxyj1)                          
c  -- shuffle x --
      ndim = 1                                                         
      call shuffl(jdim,kdim,ndim,x,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipx0,ipx1)                              
c  -- shuffle y --
      ndim = 1                                                         
      call shuffl(jdim,kdim,ndim,y,work1,jtail1,jtail2,                
     *            jmax,kmax,fwd,ipy0,ipy1)                              
c  -- shuffle ett --
c      ndim = 1                                                         
c      call shuffl(jdim,kdim,ndim,ett,work1,jtail1,jtail2,              
c     *            jmax,kmax,fwd,ipett0,ipett1)                          
      if (viscous) then
c        -- shuffle turmu --
         ndim = 1                                                         
         call shuffl(jdim,kdim,ndim,turmu,work1,jtail1,jtail2,            
     *               jmax,kmax,fwd,iptu0,iptu1)                            
c        -- shuffle fmu --
         ndim = 1                                                         
         call shuffl(jdim,kdim,ndim,fmu,work1,jtail1,jtail2,              
     *               jmax,kmax,fwd,ipfu0,ipfu1)                            
c        -- shuffle vort --
         ndim = 1                                                         
         call shuffl(jdim,kdim,ndim,vort,work1,jtail1,jtail2,             
     *               jmax,kmax,fwd,ipvt0,ipvt1)                            
      endif
c  -- shuffle press --
      ndim = 1                                                         
      call shuffl(jdim,kdim,ndim,press,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ippr0,ippr1)                            
c  -- shuffle sndsp --
      ndim = 1                                                         
      call shuffl(jdim,kdim,ndim,sndsp,work1,jtail1,jtail2,            
     *            jmax,kmax,fwd,ipsnd0,ipsnd1)                          
c  -- shuffle ds --
      ndim = 1                                                         
      call shuffl(jdim,kdim,ndim,ds,work1,jtail1,jtail2,               
     *            jmax,kmax,fwd,ipds0,ipds1)                            
c
      if (prec.gt.0) then
c        -- shuffle precon --
         ndim = 6                                                         
         call shuffl(jdim,kdim,ndim,precon,work1,jtail1,jtail2,            
     *           jmax,kmax,fwd,ipdu0,ipdu1) 
      endif
c
c
c
c                        4.  residuals
c=====================================================================
c  
c
      resid = .5d0*(resid1+resid2)
      residmx = resid1mx                                          
      iblok = 1                                                   
      maxres(1) = mxresj1                                         
      maxres(2) = mxresk1                                         
      if(resid2mx.gt.residmx)then                              
         residmx = resid2mx                                       
         iblok = 2                                                
         maxres(1) = mxresj2                                      
         maxres(2) = mxresk2                                      
      endif
c                                                    
c     write residual to unit 37
c     -this write has the time-step scaling in the residual output.
c     -the one in integrat.f does not.
      if(writeresid) then
        if (mod(numiter,100).eq.1 .or. numiter.eq.iend 
     &           .or. resid.lt.min_res) then
          ip = 21
          call ioall(ip,0,jdim,kdim,s,qold,ds,sndsp,turmu,fmu,
     >      vort,xy,xyj,x,y)
        endif
      endif
c
c
c              5.  left hand side and matrix inversion
c=====================================================================
c
c
      if (meth.eq.4 .or. meth.eq.5) then
         include 'lufactform.inc'
      else
         include 'factform.inc'
      endif
c
c
c
c
c                             6. update
c=====================================================================
c
c            if (numiter.eq.iend) then
c               ip=10
c               call ioall(ip,jdim,kdim,s,qold,press,sndsp,turmu,             
c     >                fmu,vort,xy,xyj,x,y)
c            endif
c
       call update(jdim,kdim,qp,s)                              
c                                                                       
c********************************************************************** 
c                                                                       
c  logic for `c' mesh integration in xi requires special handleing      
c  of lines coming off of the trailing edge                             
c                                                                       
c********************************************************************** 
c                                                                       
c  set integration limits                                               
c                                                                       
       if (.not.ibc) then
          kbegin  = 1                                                    
          kend    = 1                                                    
          klow    = kbegin                                               
          kup     = kend                                                 
          jbegin  = jtail2                                               
          jend    = jmax                                                 
          jlow    = jbegin+1                                             
          jup     = jend-1                                               
c*********************************************************************  
c***    8. update                                                  ***  
c*********************************************************************  
c                                                                       
          call update(jdim,kdim,qp,s)                              
c                                                                       
c-----------------------------------------------------------------      
c                                                                      
c         reset limits of integration                                          
c                                                                      
          jlow   = jlowss                                               
          jup    = jupss                                                
          jbegin = jbeginss                                             
          jend   = jendss                                               
          klow   = klowss                                               
          kup    = kupss                                                
          kbegin = kbeginss                                             
          kend   = kendss                                               
       endif
c                                                                       
c  overload k = 1; j = 1,jtail1  into k = 1 for j = jtail2 to jmax      
c                                                                       
        ndim = 4                                                         
c       if (meth.ne.1 .and. meth.ne.4 .and. meth.ne.5)
c     & call overlap(jdim,kdim,ndim,s,jmax,jtail1)                       
       call overlap(jdim,kdim,ndim,s,jmax,jtail1)                       
       call overlapq(jdim,kdim,ndim,qp,xyj,jmax,jtail1)                  
c                                                                       
c*********************************************************************  
c***    9. update pressure and sound speed                         ***  
c*********************************************************************  
c                                                                       
       call calcps(jdim,kdim,qp,press,sndsp,precon,xy,xyj)  
c                                                                       
c*********************************************************************  
c***   10. explicit boundary conditions                            ***  
c*********************************************************************  
c                                                                       
       if(bcairf)                                                       
     *      call bcupdate(jdim,kdim,qp,press,sndsp,xy,xit,ett,xyj,x,y,   
     >           a0,b0,c0,f0,g0,z0,qbcj,qbck)
       if(.not.bcairf)                                                  
     *      call bcplate(jdim,kdim,qp,press,sndsp,xy,xit,ett,xyj,x,y)    
c-----------------------------------------------------------------      
c                                                                       
                     return                                             
                     end                                                
