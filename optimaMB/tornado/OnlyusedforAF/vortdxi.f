C ********************************************************************
C ************** COMPUTE VORTICITY DERIVATIVES IN XI *****************
C ********************************************************************
      subroutine vortdxi(jdim,kdim,q,vortx,xy,xyj,
     &                    jlow,jup,kbegin,kend)
c     calling routine: xiexpl
c
#include "../include/common.inc"
c
      dimension q(jdim,kdim,4),vortx(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)
c
c     compute vorticity xi derivative
      if (iord.eq.2) then
        do 15 k = kbegin,kend
        do 15 j = jlow,jup
          jp1 = j+1
          jm1 = j-1
          rm1 = 1.d0/q(jm1,k,1)
          rp1 = 1.d0/q(jp1,k,1)
          um1 = q(jm1,k,2)*rm1
          up1 = q(jp1,k,2)*rp1
          vm1 = q(jm1,k,3)*rm1
          vp1 = q(jp1,k,3)*rp1
c
          taxi=.5d0*(xy(j,k,2)*(up1 - um1) - xy(j,k,1)*(vp1 - vm1))
          vortx(j,k) = taxi
 15     continue                                                        
      elseif (iord.eq.4) then
        tmp=1.d0/1.2d1
        tmp2=1.d0/6.d0
        do 150 k = kbegin,kend
          do 100 j = jlow+1,jup-1
            jm2=j-2
            jm1=j-1
            jp1=j+1
            jp2=j+2
c     
            rm2=1.d0/q(jm2,k,1)
            rm1=1.d0/q(jm1,k,1)
            rp1=1.d0/q(jp1,k,1)
            rp2=1.d0/q(jp2,k,1)
            um2=q(jm2,k,2)*rm2
            um1=q(jm1,k,2)*rm1
            up1=q(jp1,k,2)*rp1
            up2=q(jp2,k,2)*rp2
            vm2=q(jm2,k,3)*rm2
            vm1=q(jm1,k,3)*rm1
            vp1=q(jp1,k,3)*rp1
            vp2=q(jp2,k,3)*rp2
c     
            vortx(j,k)=tmp*(xy(j,k,2)*(um2 - 8.d0*(um1 - up1) -up2)
     &            - xy(j,k,1)*(vm2 - 8.d0*(vm1 - vp1) -vp2))
 100      continue
c     
c         -third order at first and last interior nodes
          j=jlow
          jm1=j-1
          jp1=j+1
          jp2=j+2
          rm1=1.d0/q(jm1,k,1)
          r  =1.d0/q(j,k,1)
          rp1=1.d0/q(jp1,k,1)
          rp2=1.d0/q(jp2,k,1)
          um1=q(jm1,k,2)*rm1
          u  =q(j,k,2)  *r
          up1=q(jp1,k,2)*rp1
          up2=q(jp2,k,2)*rp2
          vm1=q(jm1,k,3)*rm1
          v  =q(j,k,3)  *r  
          vp1=q(jp1,k,3)*rp1
          vp2=q(jp2,k,3)*rp2
c     
          vortx(j,k)=tmp2*(
     &          xy(j,k,2)*(-2.d0*um1 -3.d0*u +6.d0*up1 -up2)
     &        - xy(j,k,1)*(-2.d0*vm1 -3.d0*v +6.d0*vp1 -vp2))
c     
          j=jup
          jm2=j-2
          jm1=j-1
          jp1=j+1
          rm2=1.d0/q(jm2,k,1)
          rm1=1.d0/q(jm1,k,1)
          r  =1.d0/q(j,k,1)
          rp1=1.d0/q(jp1,k,1)
          um2=q(jm2,k,2)*rm2
          um1=q(jm1,k,2)*rm1
          u  =q(j,k,2)  *r
          up1=q(jp1,k,2)*rp1
          vm2=q(jm2,k,3)*rm2
          vm1=q(jm1,k,3)*rm1
          v  =q(j,k,3)  *r
          vp1=q(jp1,k,3)*rp1
c     
          vortx(j,k)=tmp2*(
     &          xy(j,k,2)*(um2 -6.d0*um1 +3.d0*u +2.d0*up1)
     &        - xy(j,k,1)*(vm2 -6.d0*vm1 +3.d0*v +2.d0*vp1))
 150    continue
      endif
c                                                                       
      return                                                          
      end                                                             
