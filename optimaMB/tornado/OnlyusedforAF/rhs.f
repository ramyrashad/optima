c ********************************************************************
c ********************* main integration routine *********************
c ********************************************************************
c calling routine:  multigrid.f
c
      subroutine rhs(lvl,mglev,npts2,
     &                jdim,kdim,q,pk,s,press,
     &                sndsp,turmu,fmu,vort,turre,vk,ve,
     &                x,y,xy,xyj,xit,ett,ds,
     &                uu,vv,ccx,ccy,coef2x,coef2y,coef4x,coef4y,
     &                spectxi,specteta,precon,tmp)
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer   npts2
      logical   turb
      dimension npts2(maxblk)
      dimension q(jdim*kdim*4),  press(jdim*kdim),  sndsp(jdim*kdim)
      dimension s(jdim*kdim*4),  xy(jdim*kdim*4),   xyj(jdim*kdim)
      dimension xit(jdim*kdim),  ett(jdim*kdim),    ds(jdim*kdim)
      dimension x(jdim*kdim),    y(jdim*kdim),      turmu(jdim*kdim)
      dimension vort(jdim*kdim), turre(jdim*kdim),  fmu(jdim*kdim)
      dimension vk(jdim*kdim),   ve(jdim*kdim),     pk(jdim*kdim*4)
      dimension spectxi(jdim*kdim*3),  specteta(jdim*kdim*3)
      dimension precon(jdim*kdim*8),   tmp(jdim*kdim*4)
c
      dimension uu(jdim*kdim),    ccx(jdim*kdim)
      dimension vv(jdim*kdim),    ccy(jdim*kdim)
      dimension coef2x(jdim*kdim),coef2y(jdim*kdim)
      dimension coef4x(jdim*kdim),coef4y(jdim*kdim)
c
#include "../include/scratch.inc"      
c
c     <<< explicit operators >>>
c
c     use variable dt based on eigenvalues if jacdt > 1
c     ds is initialized in the main, it only needs to be updated here
c     for jacdt > 1.  for jacdt=1 ds is constant.
c
c
      if(jacdt.gt.1) then
        do 10 i=nblkstart,nblkend
          call varidt(i,jacdt,jmax(i),kmax(i),q(lqptr(i)),
     &         sndsp(lgptr(i)),xy(lqptr(i)),xyj(lgptr(i)),ds(lgptr(i)),
     &         precon(lprecptr(i)),jbegin(i),jend(i),kbegin(i),kend(i))
   10   continue
      endif
c		
c **  Laminar viscosity coef.                             
c     Compute laminar viscosity based on sutherlands law 
c 
      if(viscous) then
        do 20 ii=nblkstart,nblkend
          call fmun(jmax(ii),kmax(ii),
     &          q(lqptr(ii)),press(lgptr(ii)),
     &          fmu(lgptr(ii)),sndsp(lgptr(ii)),
     &          jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &          kminbnd(ii)-nsd1(ii),kmaxbnd(ii)+nsd3(ii))
   20   continue
      endif
c
c
c **  Explicit xi rhs  -- pass entire arrays to xiexpl
c     Note:: order of explicit operators for vorticity calculation
c            must be xi then eta
c
      call xiexpl(
     &      jdim,kdim,q,s,press,sndsp,turmu,fmu,vort,turre,
     &      vk,ve,x,y,xy,xyj,xit,ds,uu,ccx,coef2x,coef4x,
     &      spectxi,precon,tmp)
c
c
c     -viscous cross terms
      if (viscous .and. viscross) then
        do 25 ii=nblkstart,nblkend
         call visrhsnc(jmax(ii),kmax(ii),q(lqptr(ii)),press(lgptr(ii)),
     &      s(lqptr(ii)),turmu(lgptr(ii)),fmu(lgptr(ii)),xy(lqptr(ii)),
     &      xyj(lgptr(ii)),
     &      d,d(1,2),d(1,3),d(1,4),d(1,5), 
     &      d(1,6),d(1,7),d(1,8),d(1,9),d(1,10),
     &      jbegin(ii),jend(ii),jlow(ii),jup(ii),
     &      kbegin(ii),kend(ii),klow(ii),kup(ii))
   25   continue
      endif

c **  explicit eta rhs without turbulence
      turb=turbulnt
      turbulnt=.false.
      call etaexpl(lvl,lev_top,mglev,
     &      jdim,kdim,q,s,press,sndsp,
     &      turmu,fmu,vort,turre,vk,ve,x,y,xy,xyj,ett,ds,vv,ccy,
     &      uu,coef2y,coef4y,specteta,precon,tmp)
      turbulnt=turb

c     ********************************************************************
c     ** Correct vv, ccy, coef2y, coef4y, s  by copying to adjacent blks**
c     ********************************************************************
c     Note: 1) it is not necessary to copied to halo points here but do it
c              anyway --- would be useful if impl. bc's were used
c           2) uu, ccx, coef2x were copied in xiexpl; don't copy them
c
        do 75 ii=nblkstart,nblkend
        do 75 nside1=2,4,2
          i2 = lblkpt(ii,nside1)
          if(i2.ne.0) then
            nside2 = lsidept(ii,nside1)
            call halocp(ii,nhalo,4,nside1,nside2,
     &            jmax(ii),kmax(ii),s(lqptr(ii)),
     &            jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &            kminbnd(ii),kmaxbnd(ii),
     &            jmax(i2),kmax(i2),s(lqptr(i2)),
     &            jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &            kminbnd(i2),kmaxbnd(i2))
          endif
 75     continue
        do 76 ii=nblkstart,nblkend
        do 76 nside1=1,3,2
          i2 = lblkpt(ii,nside1)
          if((i2.ne.0).and.(ibctype(ii,nside1).ne.5)) then
            nside2 = lsidept(ii,nside1)
            call halocp(ii,nhalo,4,nside1,nside2,
     &            jmax(ii),kmax(ii),s(lqptr(ii)),
     &            jbegin(ii)-nsd2(ii),jend(ii)+nsd4(ii),
     &            kminbnd(ii)-nsd1(ii),kmaxbnd(ii)+nsd3(ii),
     &            jmax(i2),kmax(i2),s(lqptr(i2)),
     &            jbegin(i2)-nsd2(i2),jend(i2)+nsd4(i2),
     &            kminbnd(i2)-nsd1(i2),kmaxbnd(i2)+nsd3(i2))
          endif
 76     continue

c     -scale with variable dt
c      do  40 ii=nblkstart,nblkend
c        call scaledt(jmax(ii),kmax(ii),s(lqptr(ii)),ds(lgptr(ii)),
c     &        jbegin(ii),jend(ii),kbegin(ii),kend(ii))
c   40 continue

 999  return
      end
