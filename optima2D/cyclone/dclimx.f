c     CUSP Dissipation
c     -- evaluation of limiter --
c     -- note: limiter=1 => 3rd order dissipation
c     limiter=2 => 1st order dissipation
c     limiter=3 => Zingg-Nemec limiter, pressure only
c     limiter=4 => Zingg-Nemec limiter
c     limiter=5 => Venkatakrishnan limiter

c     Written by: Marian Nemec
c     Date: April, 1998

      subroutine dclimx (jdim,kdim,q,xyj,x,y,tmp,p,n,swr)

#include "../include/arcom.inc"

      integer i,j,k,n

      dimension q(jdim,kdim,4),xyj(jdim,kdim),x(jdim,kdim),p(jdim,kdim)
      dimension y(jdim,kdim),tmp(jdim,kdim,4),swr(jdim,kdim)

      double precision top,x_xi,x_eta,us,vs,uspvs

      if (n.eq.1) then
        do i = 1,4
          do k = klow,kup
            do j = jbegin,jup
              tmp(j,k,i) = q(j+1,k,i)*xyj(j+1,k) - q(j,k,i)*xyj(j,k)
            end do
          end do
        end do
      end if
      
      if (limiter.eq.1) then
c     -- 3rd order dissipation only --
        do k = klow,kup
          do j = jbegin,jup
            swr(j,k) = 1.d0
          end do
        end do

      else if (limiter.eq.2) then
c     -- 1st order dissipation only --
        do k = klow,kup
          do j = jbegin,jup
            swr(j,k) = 0.d0
          end do
        end do

      else if (limiter.eq.3) then
c     -- Zingg-Nemec limiter based on pressure value --
        do k = klow,kup
          do j = jlow,jup-1
            us = p(j+2,k)*xyj(j+2,k) - p(j+1,k)*xyj(j+1,k)
            vs = p(j,k)*xyj(j,k) - p(j-1,k)*xyj(j-1,k)
            uspvs = dabs(us) + dabs(vs) + 1.d-12
            top = epz / uspvs
            swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
          end do
        end do

c     -- boundary nodes --

        j = jbegin
        do k = klow,kup
          us = p(j+2,k)*xyj(j+2,k) - p(j+1,k)*xyj(j+1,k)
          vs = p(j+1,k)*xyj(j+1,k) - p(j,k)*xyj(j,k)
          uspvs = dabs(us) + dabs(vs) + 1.d-12
          top = epz / uspvs
          swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
        end do

        j = jup
        do k = klow,kup
          us = p(j+1,k)*xyj(j+1,k) - p(j,k)*xyj(j,k)
          vs = p(j,k)*xyj(j,k) - p(j-1,k)*xyj(j-1,k)
          uspvs = dabs(us) + dabs(vs) + 1.d-12
          top = epz / uspvs
          swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
        end do

      else if (limiter.eq.4) then
c     -- Zingg-Nemec limiter --
        do k = klow,kup
          do j = jlow,jup-1
            us = tmp(j+1,k,n)
            vs = tmp(j-1,k,n)
            uspvs = dabs(us) + dabs(vs) + 1.d-12
            top = epz / uspvs
            swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
          end do
        end do

c     -- boundary nodes --

        j = jbegin
        do k = klow,kup
          us = tmp(j+1,k,n)
          vs = tmp(j,k,n)
          uspvs = dabs(us) + dabs(vs) + 1.d-12
          top = epz / uspvs
          swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
        end do

        j = jup
        do k = klow,kup
          us = tmp(j,k,n)
          vs = tmp(j-1,k,n)
          uspvs = dabs(us) + dabs(vs) + 1.d-12
          top = epz / uspvs
          swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
        end do

      else if (limiter.eq.5) then
c     -- Venkatakrishnan Limiter --
        do k = klow,kup
          do j = jlow,jup-1
            x_xi = 0.5d0*( x(j+1,k) - x(j-1,k) )
            x_eta = 0.5d0*( x(j,k+1) - x(j,k-1) )
            us = tmp(j+1,k,n)
            vs = tmp(j-1,k,n)
            top = dsqrt(( epv*dsqrt ( x_xi**2 + x_eta**2 ))**3)
            uspvs = dabs(us) + dabs(vs)
            swr(j,k) = 1.d0 - dabs( ( us - vs )/
     $            dmax1( uspvs,top ) )**2
          end do
        end do

c     -- boundary nodes --
        j = jbegin
        do k = klow,kup
          x_xi = x(j+1,k) - x(j,k)
          x_eta = 0.5d0*( x(j,k+1) - x(j,k-1) )
          us = tmp(j+1,k,n)
          vs = tmp(j,k,n)
          top = dsqrt(( epv*dsqrt ( x_xi**2 + x_eta**2 ))**3)
          uspvs = dabs(us) + dabs(vs)
          swr(j,k) = 1.d0 - dabs( ( us - vs )/
     $          dmax1( uspvs,top ) )**2
        end do

        j = jup
        do k = klow,kup
          x_xi = 0.5d0*( x(j+1,k) - x(j-1,k) )
          x_eta = 0.5d0*( x(j,k+1) - x(j,k-1) )
          us = tmp(j,k,n)
          vs = tmp(j-1,k,n) 
          top = dsqrt( (epv*dsqrt ( x_xi**2 + x_eta**2 ))**3)
          uspvs = dabs(us) + dabs(vs)
          swr(j,k) = 1.d0 - dabs( ( us - vs )/
     $          dmax1( uspvs,top ) )**2
        end do
      end if

      return
      end                       !dclimx
