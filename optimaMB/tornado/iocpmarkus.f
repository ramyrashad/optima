       subroutine iocpmarkus(junit,timeindex,jdim,kdim,press,xyj,
     &     x,y,cptar)

      implicit none 

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"
     
c     junit=1 write targetcp file
c     junit=2 read targetcp file


      integer junit,timeindex,namelen,numberlen,jdim,kdim,nx,ny
      character number*4
      double precision press(jdim*kdim),xyj(jdim*kdim),cptar(4*jdim)
      double precision x(jdim*kdim),y(jdim*kdim)
      double precision xval(4,jdim),ptval(4,jdim)
      double precision diff,dis,betastar
      double precision ax,bx,cx,aptx,bptx,cptx
      double precision ay,by,cy,apty,bpty,cpty,xwant(jdim),ywant(kdim)
      integer j,k,ib,lg,lenx(4),my,linecount


      call i_to_s(timeindex,number) 

      
      namelen = 5
      filena='tarcp'
     
      numberlen = len(number)      
      filena(namelen+1:namelen+numberlen) = number
   
      open(unit=n_all,file=filena,status='unknown',form='unformatted')                                                             

c     write CPTAR (Note: The pressure has the Jacobian scaling)

      if (junit.eq.1) then

         nx=NINT((remxmax-remxmin)/remdx+1.d0)
         ny=NINT((remymax-remymin)/remdy+1.d0)
 
         do j = 1,nx
            xwant(j)=remxmin+remdx*dble(j-1)
         end do
         do k = 1,ny
            ywant(k)=remymin+remdy*dble(k-1)
         end do 

         do k=1,4
            lenx(k)=0
         end do

         linecount=0

         do ib = 1,nblks  
            lg = lgptr(ib)
            call iointerpolatebox(jmax(ib),kmax(ib),press(lg),xyj(lg),
     &           kminbnd(ib),kmaxbnd(ib),jminbnd(ib),jmaxbnd(ib),
     &           x(lg),y(lg),xval,ptval,lenx)
         end do

c     calc lower x-arm target pressures

         do k=1,nx
            diff=2.d0
            do j=1,lenx(1)
c     figure out the closest point LEFT of the desired xvalue
               if ( abs(xwant(k)-xval(1,j)).lt.diff .and. 
     &              (xwant(k)-xval(1,j)).gt.0.d0 )  then
                  my = j
                  diff = abs(xwant(k)-xval(1,j))
               end if           
            end do 

c     parametrize and use quadratic to interpolate ptval
            cx=xval(1,my+1)
            bx=0.5d0*(4.d0*xval(1,my)-3.d0*cx-xval(1,my-1))
            ax=0.5d0*(cx-2.d0*xval(1,my)+xval(1,my-1))           

            dis=0.25d0*bx**2/ax**2-(cx-xwant(k))/ax

            betastar=-0.5d0*bx/ax-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*bx/ax+sqrt(dis)
            end if

            cptx=ptval(1,my+1)
            bptx=0.5d0*(4.d0*ptval(1,my)-3.d0*cptx-ptval(1,my-1))
            aptx=0.5d0*(cptx-2.d0*ptval(1,my)+ptval(1,my-1)) 

            linecount=linecount+1
            cptar(linecount)=aptx*betastar**2+bptx*betastar+cptx 
c            print *, linecount,ax*betastar**2+bx*betastar+cx,xwant(k)

         end do

c     calc right y-arm target pressures

         do k=1,ny
            diff=2.d0
            do j=1,lenx(2)
c     figure out the closest point ABOVE the desired yvalue
               if ( abs(ywant(k)-xval(2,j)).lt.diff .and. 
     &              (ywant(k)-xval(2,j)).lt.0.d0 )  then
                  my = j
                  diff = abs(ywant(k)-xval(2,j))
               end if           
            end do 

c     parametrize and use quadratic to interpolate ptval
            cy=xval(2,my+1)
            by=0.5d0*(4.d0*xval(2,my)-3.d0*cy-xval(2,my-1))
            ay=0.5d0*(cy-2.d0*xval(2,my)+xval(2,my-1))           

            dis=0.25d0*by**2/ay**2-(cy-ywant(k))/ay

            betastar=-0.5d0*by/ay-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*by/ay+sqrt(dis)
            end if

            cpty=ptval(2,my+1)
            bpty=0.5d0*(4.d0*ptval(2,my)-3.d0*cpty-ptval(2,my-1))
            apty=0.5d0*(cpty-2.d0*ptval(2,my)+ptval(2,my-1)) 

            linecount=linecount+1
            cptar(linecount)=apty*betastar**2+bpty*betastar+cpty
c            print *, linecount,ay*betastar**2+by*betastar+cy,ywant(k)

         end do

c     calc upper x-arm target pressures

         do k=1,nx
            diff=2.d0
            do j=1,lenx(3)
c     figure out the closest point LEFT of the desired xvalue
               if ( abs(xwant(k)-xval(3,j)).lt.diff .and. 
     &              (xwant(k)-xval(3,j)).gt.0.d0 )  then
                  my = j
                  diff = abs(xwant(k)-xval(3,j))
               end if           
            end do 

c     parametrize and use quadratic to interpolate ptval
            cx=xval(3,my+1)
            bx=0.5d0*(4.d0*xval(3,my)-3.d0*cx-xval(3,my-1))
            ax=0.5d0*(cx-2.d0*xval(3,my)+xval(3,my-1))           

            dis=0.25d0*bx**2/ax**2-(cx-xwant(k))/ax

            betastar=-0.5d0*bx/ax-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*bx/ax+sqrt(dis)
            end if

            cptx=ptval(3,my+1)
            bptx=0.5d0*(4.d0*ptval(3,my)-3.d0*cptx-ptval(3,my-1))
            aptx=0.5d0*(cptx-2.d0*ptval(3,my)+ptval(3,my-1)) 

            linecount=linecount+1
            cptar(linecount)=aptx*betastar**2+bptx*betastar+cptx
c            print *, linecount,ax*betastar**2+bx*betastar+cx,xwant(k)

         end do

c     calc left y-arm target pressures

         do k=1,ny
            diff=2.d0
            do j=1,lenx(4)
c     figure out the closest point ABOVE the desired yvalue
               if ( abs(ywant(k)-xval(4,j)).lt.diff .and. 
     &              (ywant(k)-xval(4,j)).lt.0.d0 )  then
                  my = j
                  diff = abs(ywant(k)-xval(4,j))
               end if           
            end do 

c     parametrize and use quadratic to interpolate ptval
            cy=xval(4,my+1)
            by=0.5d0*(4.d0*xval(4,my)-3.d0*cy-xval(4,my-1))
            ay=0.5d0*(cy-2.d0*xval(4,my)+xval(4,my-1))           

            dis=0.25d0*by**2/ay**2-(cy-ywant(k))/ay

            betastar=-0.5d0*by/ay-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*by/ay+sqrt(dis)
            end if

            cpty=ptval(4,my+1)
            bpty=0.5d0*(4.d0*ptval(4,my)-3.d0*cpty-ptval(4,my-1))
            apty=0.5d0*(cpty-2.d0*ptval(4,my)+ptval(4,my-1)) 

            linecount=linecount+1
            cptar(linecount)=apty*betastar**2+bpty*betastar+cpty 
c            print *, linecount,ay*betastar**2+by*betastar+cy,ywant(k)

         end do

         write(n_all) nx,ny,remxmin,remdx,remxmax,remymin,remdy,remymax
         call writecpmpr(n_all,cptar,4*jdim,1,linecount)
 

c     read CPTAR

      else if (junit.eq.2) then

        read(n_all) nx,ny,remxmin,remdx,remxmax,remymin,remdy,remymax  
        call readcpmpr(n_all,cptar,4*jdim,1,2*nx+2*ny)


      endif

      close (n_all)  
    
      return                                                            
      end 





      subroutine iointerpolatebox(jdim,kdim,press,xyj,ks,ke,js,
     &     je,x,y,xval,ptval,lenx)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      integer jdim,kdim,js,je,ks,ke,j,k
      double precision press(jdim,kdim),x(jdim,kdim),y(jdim,kdim)
      double precision xyj(jdim,kdim),xval(4,maxj),ptval(4,maxj)
      integer jmy,kmy,lenx(4)
      double precision xdiff,ydiff,cy,by,ay,cx,bx,ax,betastar,dis
      double precision ptpl,ptne,cptmy,bptmy,aptmy
      double precision inixmin,iniymin,inixmax,iniymax
     
      inixmin=0.5d0*abs(remxmin)
      iniymin=0.4d0*abs(remymin)
      inixmax=0.4d0*abs(remxmax-1.d0)
      iniymax=0.75d0*abs(remymax)
   
      do j = js+1,je
         kmy=0
         ydiff=iniymin
         do k = ks+1,ke
c     figure out the closest point ABOVE the lower x-arm
            if ( abs(y(j,k)-remymin).lt.ydiff .and. 
     &            (y(j,k)-remymin).gt.0.d0 )  then
               kmy=k
               ydiff= abs(y(j,k)-remymin)
            end if
              
         end do

c     parametrize and use quadratic to calculate xval as well as pval and ptval
         if (kmy .ne.0) then

            lenx(1)=lenx(1)+1

            cy=y(j,kmy-1)
            by=0.5d0*(4.d0*y(j,kmy)-3.d0*cy-y(j,kmy+1))
            ay=0.5d0*(cy-2.d0*y(j,kmy)+y(j,kmy+1))

            dis=0.25d0*by**2/ay**2-(cy-remymin)/ay

            betastar=-0.5d0*by/ay-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*by/ay+sqrt(dis)
            end if

            cx=x(j,kmy-1)
            bx=0.5d0*(4.d0*x(j,kmy)-3.d0*cx-x(j,kmy+1))
            ax=0.5d0*(cx-2.d0*x(j,kmy)+x(j,kmy+1))

            xval(1,lenx(1))=ax*betastar**2+bx*betastar+cx

            ptpl= press(j,kmy+1)*xyj(j,kmy+1)

            ptne= press(j,kmy)*xyj(j,kmy)
           
            cptmy=press(j,kmy-1)*xyj(j,kmy-1)
            bptmy=0.5d0*(4.d0*ptne-3.d0*cptmy-ptpl)
            aptmy=0.5d0*(cptmy-2.d0*ptne+ptpl)

            ptval(1,lenx(1))=aptmy*betastar**2+bptmy*betastar+cptmy

c$$$            print *, 'lowx',lenx(1),ay*betastar**2+by*betastar+cy,
c$$$     &           xval(1,lenx(1))

         end if

      end do

      do k = ke,ks+1,-1         
c     from top to bottom (blocks are numbered like that)
         jmy=0
         xdiff=inixmax
         do j = js+1,je
c     figure out the closest point LEFT of the right y-arm
            if ( abs(x(j,k)-remxmax).lt.xdiff .and. 
     &            (x(j,k)-remxmax).lt.0.d0 )  then
               jmy=j
               xdiff= abs(x(j,k)-remxmax)
            end if
              
         end do

c     parametrize and use quadratic to calculate xval as well as pval and ptval
         if (jmy .ne.0) then

            lenx(2)=lenx(2)+1

            cx=x(jmy+1,k)
            bx=0.5d0*(4.d0*x(jmy,k)-3.d0*cx-x(jmy-1,k))
            ax=0.5d0*(cx-2.d0*x(jmy,k)+x(jmy-1,k))           

            dis=0.25d0*bx**2/ax**2-(cx-remxmax)/ax

            betastar=-0.5d0*bx/ax-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*bx/ax+sqrt(dis)
            end if

            cy=y(jmy+1,k)
            by=0.5d0*(4.d0*y(jmy,k)-3.d0*cy-y(jmy-1,k))
            ay=0.5d0*(cy-2.d0*y(jmy,k)+y(jmy-1,k))

            xval(2,lenx(2))=ay*betastar**2+by*betastar+cy 

            ptpl= press(jmy-1,k)*xyj(jmy-1,k)

            ptne= press(jmy,k)*xyj(jmy,k)
           
            cptmy=press(jmy+1,k)*xyj(jmy+1,k)
            bptmy=0.5d0*(4.d0*ptne-3.d0*cptmy-ptpl)
            aptmy=0.5d0*(cptmy-2.d0*ptne+ptpl)

            ptval(2,lenx(2))=aptmy*betastar**2+bptmy*betastar+cptmy

c$$$            print *, 'righty',lenx(2),ax*betastar**2+bx*betastar+cx,
c$$$     &           xval(2,lenx(2))

         end if

      end do


      do j = js+1,je
         kmy=0
         ydiff=iniymax
         do k = ks+1,ke
c     figure out the closest point BELOW the upper x-arm
            if ( abs(y(j,k)-remymax).lt.ydiff .and. 
     &            (y(j,k)-remymax).lt.0.d0 )  then
               kmy=k
               ydiff= abs(y(j,k)-remymax)
            end if
              
         end do

c     parametrize and use quadratic to calculate xval as well as pval and ptval
         if (kmy .ne.0) then

            lenx(3)=lenx(3)+1

            cy=y(j,kmy+1)
            by=0.5d0*(4.d0*y(j,kmy)-3.d0*cy-y(j,kmy-1))
            ay=0.5d0*(cy-2.d0*y(j,kmy)+y(j,kmy-1))

            dis=0.25d0*by**2/ay**2-(cy-remymax)/ay

            betastar=-0.5d0*by/ay-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*by/ay+sqrt(dis)
            end if

            cx=x(j,kmy+1)
            bx=0.5d0*(4.d0*x(j,kmy)-3.d0*cx-x(j,kmy-1))
            ax=0.5d0*(cx-2.d0*x(j,kmy)+x(j,kmy-1))

            xval(3,lenx(3))=ax*betastar**2+bx*betastar+cx

            ptpl= press(j,kmy-1)*xyj(j,kmy-1)

            ptne= press(j,kmy)*xyj(j,kmy)
           
            cptmy=press(j,kmy+1)*xyj(j,kmy+1)
            bptmy=0.5d0*(4.d0*ptne-3.d0*cptmy-ptpl)
            aptmy=0.5d0*(cptmy-2.d0*ptne+ptpl)

            ptval(3,lenx(3))=aptmy*betastar**2+bptmy*betastar+cptmy 

c$$$            print *, 'upx',lenx(3),ay*betastar**2+by*betastar+cy,
c$$$     &           xval(3,lenx(3))

         end if

      end do

      do k = ke,ks+1,-1         
c     from top to bottom (blocks are numbered like that)
         jmy=0
         xdiff=inixmin
         do j = js+1,je
c     figure out the closest point RIGHT of the left y-arm
            if ( abs(x(j,k)-remxmin).lt.xdiff .and. 
     &            (x(j,k)-remxmin).gt.0.d0 )  then
               jmy=j
               xdiff= abs(x(j,k)-remxmin)
            end if
              
         end do

c     parametrize and use quadratic to calculate xval as well as pval and ptval
         if (jmy .ne.0) then

            lenx(4)=lenx(4)+1

            cx=x(jmy-1,k)
            bx=0.5d0*(4.d0*x(jmy,k)-3.d0*cx-x(jmy+1,k))
            ax=0.5d0*(cx-2.d0*x(jmy,k)+x(jmy+1,k))           

            dis=0.25d0*bx**2/ax**2-(cx-remxmin)/ax

            betastar=-0.5d0*bx/ax-sqrt(dis)

            if (betastar .lt. 0.d0 .or. betastar .gt. 1.d0) then
               betastar=-0.5d0*bx/ax+sqrt(dis)
            end if

            cy=y(jmy-1,k)
            by=0.5d0*(4.d0*y(jmy,k)-3.d0*cy-y(jmy+1,k))
            ay=0.5d0*(cy-2.d0*y(jmy,k)+y(jmy+1,k))

            xval(4,lenx(4))=ay*betastar**2+by*betastar+cy

            ptpl= press(jmy+1,k)*xyj(jmy+1,k)

            ptne= press(jmy,k)*xyj(jmy,k)
           
            cptmy=press(jmy-1,k)*xyj(jmy-1,k)
            bptmy=0.5d0*(4.d0*ptne-3.d0*cptmy-ptpl)
            aptmy=0.5d0*(cptmy-2.d0*ptne+ptpl)

            ptval(4,lenx(4))=aptmy*betastar**2+bptmy*betastar+cptmy 

c$$$            print *, 'lefty',lenx(4),ax*betastar**2+bx*betastar+cx,
c$$$     &           xval(4,lenx(4))
           
         end if

      end do
  

      return
      end    !iointerpolatebox



      subroutine writecpmpr(junit,cptar,jdim,j1,j2)

      implicit none

      integer jdim,j1,j2,j,junit
      double precision  cptar(jdim)

      write(junit) (cptar(j),j=j1,j2)

   
      return 
      end


      subroutine readcpmpr(junit,cptar,jdim,j1,j2)

      implicit none

      integer jdim,j1,j2,j,junit
      double precision  cptar(jdim)

      read(junit) (cptar(j),j=j1,j2)

   
      return 
      end

    


     
