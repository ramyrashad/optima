c----------------------------------------------------------------------
c     -- subroutine to get airfoil coordinates from grid --
c     -- m. nemec --
c----------------------------------------------------------------------

      subroutine getxy(ipoints, jdim, kdim, iside, x, y, bap, ibody)

      implicit none

      integer ipoints, jdim, kdim, iside, j, k, ibody

      double precision x(jdim,kdim), y(jdim,kdim), bap(ibody,2) 

c     -- side # 1 --

      if (iside.eq.1) then
         k = 1
         do j=1,jdim
            if ( ipoints.gt.1 ) then
               if ( abs(bap(ipoints-1,1)-x(j,k)).gt.1.d-12) then
                  bap(ipoints,1) = x(j,k)
                  bap(ipoints,2) = y(j,k)
                  ipoints = ipoints + 1
               end if
            else 
               bap(ipoints,1) = x(j,k)
               bap(ipoints,2) = y(j,k)
               ipoints = ipoints + 1
            end if
         end do

c     -- side # 2 --

      else if (iside.eq.2) then
         j = 1
         do k=kdim,1,-1
            if ( ipoints.gt.1 ) then
               if ( abs(bap(ipoints-1,1)-x(j,k)).gt.1.d-12) then
                  bap(ipoints,1) = x(j,k)
                  bap(ipoints,2) = y(j,k)
                  ipoints = ipoints + 1
               end if
            else 
               bap(ipoints,1) = x(j,k)
               bap(ipoints,2) = y(j,k)
               ipoints = ipoints + 1
            end if
         end do

c     -- side # 3 --

      else if (iside.eq.3) then
         k = kdim
         do j=jdim,1,-1
            if ( ipoints.gt.1 ) then
               if ( abs(bap(ipoints-1,1)-x(j,k)).gt.1.d-12) then
                  bap(ipoints,1) = x(j,k)
                  bap(ipoints,2) = y(j,k)
                  ipoints = ipoints + 1
               end if
            else 
               bap(ipoints,1) = x(j,k)
               bap(ipoints,2) = y(j,k)
               ipoints = ipoints + 1
            end if
         end do

c     -- side # 4 --

      else if (iside.eq.4) then
         j = jdim
         do k=1,kdim 
            if ( ipoints.gt.1 ) then
               if ( abs(bap(ipoints-1,1)-x(j,k)).gt.1.d-12) then
                  bap(ipoints,1) = x(j,k)
                  bap(ipoints,2) = y(j,k)
                  ipoints = ipoints + 1
               end if
            else 
               bap(ipoints,1) = x(j,k)
               bap(ipoints,2) = y(j,k)
               ipoints = ipoints + 1
            end if
         end do

      end if

      return
      end                       !getxy
