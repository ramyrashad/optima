      subroutine initia ( ifirst, jdim, kdim, q, qold, press, sndsp, s,
     &      xy, xyj, xit, ett, ds, x, y, turmu, fmu, vort, precon )  

c     called from 'cyclone'

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      dimension q(jdim,kdim,4),qold(jdim,kdim,4)
      dimension press(jdim,kdim),sndsp(jdim,kdim)                       
      dimension s(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)           
      dimension xit(jdim,kdim),ett(jdim,kdim),ds(jdim,kdim)             
      dimension x(jdim,kdim),y(jdim,kdim),turmu(jdim,kdim)              
      dimension vort(jdim,kdim),fmu(jdim,kdim)
      dimension precon(jdim,kdim,6)
      dimension workq(jdim,kdim,3)
      !-- Local variables for writing freestream 'q' to file
      integer d100, d10, d1, namelen
      character command*60

      ! _________________________
      ! Set freestream conditions

      cosang = cos(pi*alpha/180.d0)                               
      sinang = sin(pi*alpha/180.d0)
      uinf   = fsmach*cosang
      vinf   = fsmach*sinang
      gamma = 1.4d0
      gami = gamma -1.d0
      einf   = 1.d0/(gamma*gami) + .5d0*fsmach*fsmach
      re = re_in / fsmach
      rhoinf = 1.d0
      pinf   = 1.d0/gamma                                            

      if(re.ne.0.0)then
         write (out_unit,3) re
 3       format(1x,34hRe scaled by freestream, reset to :,e10.3)
      endif             
                                              

      if ((ifirst.eq.1).or.(.not.warm_start)) then

c     -- grid input --
        if (iread.eq.0) then
c     Get the default circular mesh for a cylinder
c     cylinder grid  by tim barth 3/19/84                        

          call cylgrid(jdim,kdim,x,y,jmax,kmax,sobmax,dswall)          
          go to 401                                                     
        endif
c     
 401    continue

c     -- initialize to free stream --
c     -- load free stream at angle of attack alpha --
        if ((.not. opt_restart).or.(.not.warm_start)) then
          
          if (ifirst.eq.1) then

            if ((warm1st) .or. (postProcessOnly)) then 

               write(scr_unit,502)
 502           format(/3x,
     & '1st iteration...',/3x,
     & 'Warm starting 1st flow solution',/3x)

               ip = 4 
               call ioall( ip, 0, jdim, kdim, q, qold, press, sndsp, 
     &         turmu, fmu, vort, xy, xyj, x, y )

               goto 503

            else

               write(scr_unit,402)
 402           format(/3x,
     & '1st iteration...',/3x,
     & 'Initializing flow solution to free-stream conditions',/3x)

            end if

          else
            write(scr_unit,403)
 403        format(/3x,
     & 'Warm-started flow solutions are turned OFF...',/3x,
     & 'Initializing flow solution to free-stream conditions',/3x)
          endif

          do k=1,kmax
            do j=1,jmax
              q(j,k,1) = rhoinf
              q(j,k,2) = rhoinf*uinf
              q(j,k,3) = rhoinf*vinf
              q(j,k,4) = einf
            end do
          end do
       end if ! (.not. opt_restart).or.(.not.warm_start)
      endif ! (ifirst.eq.1).or.(.not.warm_start)

 503  continue

c     -- option to restart previously stored solution --
      if ( restart.and.ifirst.eq.1.and.(warm_start) ) then
c        variables read in with jacobian scaling                
         ip = 4                                                            
         call ioall( ip, 0, jdim, kdim, q, qold, press, sndsp, turmu,
     &         fmu, vort, xy, xyj, x, y )
      endif

cmn   -- logic to warm start optimization run -- 
cmn   -- this avoids scaling updates in bcbody.f --
      if ( .not.flowAnalysisOnly
     &     .and. ifirst.ne.1
     &     .and. (warm_start)   ) then
         restart = .true. 
      endif

cmn   -- logic to avoid reseting turmu for optimization restarts --
cmn   -- also avoids scaling in bcbody.f when ifirst = 1 --
      if (opt_restart) restart = .true.

c     -- set up variables, indicies, and compute metrics --
      call setup ( jdim, kdim, s, xy, xyj, xit, ett, ds, x, y, turmu,
     &     fmu, vort )

c     -- negative jacobian detected --
      if (badjac) return
                                                                       
c     -- scale variables by jacobian --
      if (.not.unsted) then
        do n=1,4                                                       
          do k=1,kmax
            do j=1,jmax
              q(j,k,n) = q(j,k,n)/xyj(j,k)
            end do
          end do
        end do
      else
        do n=1,4                                                       
          do k=1,kmax
            do j=1,jmax
              q(j,k,n) = q(j,k,n)/xyj(j,k)
              qold(j,k,n) = qold(j,k,n)/xyj(j,k)
            end do
          end do
        end do
      endif                                                          

      ! ______________________________________________________________
      ! Compute various flow quantities for post-processsing. Normally
      ! they would be computed in the AF or NK flow solvers. However,
      ! for postProcessOnly, we will skip the calls to the flow solvers.
      if (postProcessOnly) then

         ! recompute the pressure and sound speed from solution data
         do k=1,kmax
            do j=1,jmax
               ! copied from calcps.f
               press(j,k) = gami*(q(j,k,4) -0.5*(q(j,k,2)**2 + q(j,k,3)
     &              **2)/q(j,k,1))
               sndsp(j,k) = dsqrt(gamma*press(j,k)/q(j,k,1)) 
               do n=1,3
                  workq(j,k,n) = q(j,k,n)
               end do
            end do
         end do
         if(viscous) then 
            ! recompute fluid viscosity, fmu, from solution data
            call fmun(jdim,kdim,q,press,fmu,sndsp)
            ! recompute vorticity, vort, from solution data 
            ! call vort_o2 (jdim, kdim, workq, xy, vort)
            do k = klow,kup
               kp1 = k+1
               km1 = k-1
               do j = jlow,jup
                  jp1 = jplus(j)
                  jm1 = jminus(j)
                  ujm1 = q(jm1,k,2)/q(jm1,k,1)
                  ujp1 = q(jp1,k,2)/q(jp1,k,1)
                  ukm1 = q(j,km1,2)/q(j,km1,1)
                  ukp1 = q(j,kp1,2)/q(j,kp1,1)
                  vjm1 = q(jm1,k,3)/q(jm1,k,1)
                  vjp1 = q(jp1,k,3)/q(jp1,k,1)
                  vkm1 = q(j,km1,3)/q(j,km1,1)
                  vkp1 = q(j,kp1,3)/q(j,kp1,1)

                  vort(j,k) = dabs(0.5d0*( (vjp1-vjm1)*xy(j,k,1) + (vkp1
     &                 -vkm1)*xy(j,k,3) - (ujp1-ujm1)*xy(j,k,2) - (ukp1
     &                 -ukm1)*xy(j,k,4) ))
               end do
            end do
         else ! set viscosity to unity and voriticity to zero
            do k=1,kmax
               do j=1,jmax
                  fmu(j,k) = 1d0
                  vort(j,k) = 0d0
               end do
            end do
         end if

      end if ! (postProcessOnly)

c     -- initialize press and sndsp --
      call calcps(jdim,kdim,q,press,sndsp,precon,xy,xyj)            

      return                                                            
      end                       ! initia
