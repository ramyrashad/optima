c----------------------------------------------------------------------
c
c     -- compute objective function value --
c
c     written by: marian nemec
c     date: march 2000
c----------------------------------------------------------------------
      subroutine CalcObj (obj, jdim, kdim, ndim, ifirst, indx, icol, 
     &     iex, q,cp, xy, xyj, x, y, cp_tar, fmu, vort, turmu, work1, 
     &     ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast, pa, pat,
     &     itfirst, obj0, mp, ifun, obj1, lcon)

#ifdef _MPI_VERSION
      use mpi
#endif


#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"
      
      integer i, j, jdim, kdim, ndim, ifirst, itfirst, icol(9)
      integer indx(jdim,kdim), iex(jdim,kdim,ndim), ifunc

      double precision obj, obj0, test, obj1, lcon
      double precision t3, a3, rt3, ks, ksMach, c_upp, c_low
      double precision q(jdim,kdim,ndim), cp(jdim,kdim), xy(jdim,kdim,4)
      double precision qp(maxjk,nblk), qold(maxjk,nblk)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim), turmu(jdim,kdim)
      double precision cp_tar(jbody,2), MaxMach, R
      double precision aMaxMach, uMaxMach, vMaxMach, pMaxMach

      integer jMaxMach, kMaxMach

c     -- sparse adjoint arrays --
clb
      integer ia(jdim*kdim*ndim+2)
      integer ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1)
      integer iat(jdim*kdim*ndim+2)
      integer jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      integer ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1)

      double precision as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1)
      double precision pa(jdim*kdim*ndim*ndim*5+1)
      double precision pat(jdim*kdim*ndim*ndim*5+1)

csi
      double precision D_hat(maxjk,nblk,6)
      double precision exprhs(maxjk,nblk)
      double precision a_jk(6,6)
csi

c     -- work array: compatible with cyclone --
      double precision work1(maxjk,100)

c     -- common block used for out of pareto info --
      common/pareto/ ftc, fcl, fcd

      !-- Local variables for writing '.his' file to /history folder
      integer d100, d10, d1, namelen
      character command*60, history_file_prefix*60

c  Comments for variables added L. Billing, July 2006
c  appologies for any confusing explanation - it should be better than
c  no explanation at all.
c
c  obj_func = 1 means that a target coefficient of pressure is the objective.
c    also called inverse design.
c  obj_func = 2 - 4 not generally used.  May not be fully implemented.
c    2 -> J = C_D (no thickness constraints)
c    3 -> J = w_D ( C_D-C_D* )**2 + w_L( C_L-C_L* )**2 (no t.c.)
c    4 -> J = -C_L (no t.c.)
c  obj_func = 5 -> maximization of lift to drag ratio.  Includes t.c.
c  obj_func = 6 -> target lift and drag.  Can be used for lift-constrained
c    drag minimization.  Includes t.c.
c    J = w_L (1 - C_L/C_L*) + w_D (1 - C_D/C_D*) + t.c.
c  obj_func = 7 -> target lift and moment.  Can be used for lift-constrained
c    moment minimization.  Target moment is stored in cd_tar.  Includes t.c.
c  obj_func = 8 -> lift-constrained drag minimization.  Must be run with
c    clalpha, clalpha2, and clopt all TRUE. Includes thickness constraints.
c    J = C_D
c  All objective functions are normalized by their initial value, obj0.  This
c    normalization does not include thickness constraints so that thickness
c    constraints have the same meaning regardless of the numerical value
c    of the objective function.  For example, if obj_func = 8, obj0 is the 
c    initial value of the drag coefficient, but does not include the initial
c    value of any geometrical constraints.
c     

      !-- Initialize some values
      obj = 0.d0
      obj1 = 0.d0

      if (hisout) rewind (his_unit)
      if (thisout) rewind (turbhis_unit)

      ifunc = ifun

      call flow_solve (jdim, kdim, ndim, ifirst, indx, icol, iex, ia,
     &      ja, as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, q, qp, 
     &      qold,cp,xy,xyj,x,y,fmu,vort,turmu,work1,D_hat,exprhs,a_jk,
     &      ifunc)

      !-- Write flow solver convergence history file for the current
      !-- design iteration to the 'history' folder

      if (flo_his) then

         !-- Create a unique history file prefix for each processor 
         !-- for example, if history_file_prefix is 'rest##', then 
         !-- processors with ranks 0, 1, 2,... will have 
         !-- history_file_prefix = rest##-00, rest##-01, rest##-02,...

         history_file_prefix = output_file_prefix0
      
         d10  = rank / 10
         d1   = rank - (d10*10)

         !-- Create a unique history file prefix for each processor

         namelen =  len_trim(history_file_prefix)
         history_file_prefix(namelen+1:namelen+3)
     &         = '-'//char(d10+48)//char(d1+48)

         !-- Build a 'copy' command that moves the .his file 
         !-- to the '/history/' directory
       
         namelen = len_trim(history_file_prefix)
         command = 'cp '
         command(4:39)=history_file_prefix
         command(namelen+4:39)='.his ./history/'
         command(namelen+19:39)=history_file_prefix
         namelen = len_trim(command)

         !-- Represent the design iteration number 'ifun' as a
         !-- string so that it can be appended to the file name

         d100 = ifun / 100
         d10  = (ifun - (d100*100) ) / 10
         d1   = ifun - (d100*100) - (d10*10)

         !-- Append the design iteration number and  file
         !-- extension '.t_arg' to the name of the file to be moved
         !-- to the '/history/' directory

         command(namelen+1:namelen+4) = 
     &       '-'//char(d100+48)//char(d10+48)//char(d1+48)
         command(namelen+5:namelen+14) = '.his'

         !-- Call the move command

         call system(command)

      end if ! (flo_his)

c     -- negative jacobian detected --
      if (badjac) return

c     -- calculate objective function --
      if (obj_func .eq. 1) then
        obj = 0.d0
        i = 1
        do j = jtail1, jtail2
          obj = obj + 0.5*( cp(j,1) - cp_tar(i,2) )**2
          i = i + 1
        end do

        if (itfirst.eq.1) then
          if (scale_obj) then
#ifdef _MPI_VERSION
             !-- Only normalize if used as an objective function
             if (abs(obj) .gt. 1.d-10.and.(.not.off_flags(rank+1))) then
#else
             if (abs(obj) .gt. 1.d-10.and.(.not.off_flags(1))) then
#endif
                obj0 = obj
             else
                obj0 = 1.d0
                write(scr_unit,*) 'Initial objective function zero'
                write(scr_unit,*) 'Normalization not used'
             end if
          else
            obj0 = 1.d0
          endif
        end if
        obj = obj/obj0

      else if (obj_func .eq. 2) then 
        obj = cdt

        if (itfirst.eq.1) then
          if (scale_obj) then
             if (abs(obj) .gt. 1.d-10) then
                obj0 = obj
             else
                obj0 = 1.d0
                write(scr_unit,*) 'Initial objective function zero'
                write(scr_unit,*) 'Normalization not used'
             end if
          else
            obj0 = 1.d0
          endif
        end if
        obj = obj/obj0

      else if (obj_func .eq. 3) then 

        if (ico.eq.0) then
           wfl = 0.5d0
           wfd = wfl*clt/cdt*wfactor
           write (opt_unit,100) wfactor, wfd, wfl
 100       format (3x,'Objective function weight factor is:',f8.2,/3x,
     &          'and the weights WFD and WFL are:',f10.5,f10.5)
        end if

        obj = wfd*( cdt-cd_tar )**2 + wfl*( clt-cl_tar )**2

        if (itfirst.eq.1) then
          if (scale_obj) then
             if (abs(obj) .gt. 1.d-10) then
                obj0 = obj
             else
                obj0 = 1.d0
                write(scr_unit,*) 'Initial objective function zero'
                write(scr_unit,*) 'Normalization not used'
             end if
          else
            obj0 = 1.d0
          endif

        end if

        obj = obj/obj0

      else if (obj_func .eq. 4) then
        
        obj = -clt

        if (itfirst.eq.1) then
          if (scale_obj) then
             if (abs(obj) .gt. 1.d-10) then
                obj0 = obj
             else
                obj0 = 1.d0
                write(scr_unit,*) 'Initial objective function zero'
                write(scr_unit,*) 'Normalization not used'
             end if
          else
            obj0 = 1.d0
          endif

        end if
        obj = obj/obj0
         
      else if (obj_func .eq. 5) then

        obj = cdt/clt

        if (itfirst.eq.1) then
          if (scale_obj) then
             if (abs(obj) .gt. 1.d-10) then
                obj0 = obj
             else
                obj0 = 1.d0
                write(scr_unit,*) 'Initial objective function zero'
                write(scr_unit,*) 'Normalization not used'
             end if
          else
            obj0 = 1.d0
          endif

        end if
        obj = obj/obj0

        call CalcTCon(jdim,kdim,y,t3,.false.)
        obj = obj + t3
         
c     -- range thickness penalty (find max thickness in x range) --
        call CalcRTCon(jdim,kdim,y,rt3)
        obj = obj + rt3

c     -- calculate area constraint penalty --
        call CalcACon(jdim,kdim,x,y,a3)
        obj = obj + a3
         
c       Calculate radius of curvature penalty (Chad Oldfield)
        call CalcRCCon(jdim,kdim,x,y,rc_con)
        obj = obj + rc_con

      else if (obj_func .eq. 6) then

        ftc = t3
        fcl = ( 1.0 - clt/cl_tar )**2

        if (cdt.gt.cd_tar) then
           obj = ( 1.0 - cdt/cd_tar )**2
           fcd = obj
           obj = wfd*obj
        else
           obj = 0.0
           fcd = 0.0
        end if

        obj = obj + wfl*( 1.d0 - clt/cl_tar )**2

        if (itfirst.eq.1) then
          if (scale_obj) then
             if (abs(obj) .gt. 1.d-10) then
                obj0 = obj
             else
                obj0 = 1.d0
                write(scr_unit,*) 'Initial objective function zero'
                write(scr_unit,*) 'Normalization not used'
             end if
          else
            obj0 = 1.d0
          endif

        end if

        obj = obj/obj0

        if (use_quad_penalty_meth) then

           !-- Add penalty term for violation of fixed thickness 
           !-- constraints

           call CalcTCon(jdim,kdim,y,t3)

           obj = obj + t3
       
           !-- Range thickness penalty (find max thickness in x range)

           call CalcRTCon(jdim,kdim,y,rt3)

           obj = obj + rt3

           !-- Calculate area constraint penalty --
   
           call CalcACon(jdim,kdim,x,y,a3)

           obj = obj + a3

           !-- Calculate radius of curvature penalty (Chad Oldfield)

           call CalcRCCon(jdim,kdim,x,y,rc_con)

           obj = obj + rc_con
           
        end if

      else if (obj_func .eq. 7) then

c     -- cd_tar is the target moment --

        call CalcTCon(jdim,kdim,y,t3,.false.)
        ftc = t3
        fcl = ( 1.0 - clt/cl_tar )**2

        obj = ( 1.0 - cmt/cd_tar )**2
        fcd = obj
        obj = wfd*obj

        obj = obj + wfl*( 1.d0 - clt/cl_tar )**2

        if (itfirst.eq.1) then
          if (scale_obj) then
             if (abs(obj) .gt. 1.d-10) then
                obj0 = obj
             else
                obj0 = 1.d0
                write(scr_unit,*) 'Initial objective function zero'
                write(scr_unit,*) 'Normalization not used'
             end if
          else
            obj0 = 1.d0
          endif

        end if
        obj = obj/obj0
        
        obj = obj + t3

c     -- range thickness penalty (find max thickness in x range) --
        call CalcRTCon(jdim,kdim,y,rt3)
        obj = obj + rt3

c     -- calculate area constraint penalty --
        call CalcACon(jdim,kdim,x,y,a3)
        obj = obj + a3

c       Calculate radius of curvature penalty (Chad Oldfield)
        call CalcRCCon(jdim,kdim,x,y,rc_con)
        obj = obj + rc_con

      else if (obj_func .eq. 8) then 
clb     New objective function to be used for variable angle of attack
clb     flow solves (minimize drag, constant lift)

        obj = cdt

        if (itfirst.eq.1) then
          if (scale_obj) then
             if (abs(obj) .gt. 1.d-10) then
                obj0 = obj
             else
                obj0 = 1.d0
                write(scr_unit,*) 'Initial objective function zero'
                write(scr_unit,*) 'Normalization not used'
             end if
          else
            obj0 = 1.d0
          endif

        end if

        !-- Un-normalized objective function
        obj1 = obj/1.d0

        !-- Normalized objective function
        obj = obj/obj0


        !-- If geometric constraints are to be handled using the
        !-- quadratic penalty method, then calculate penalty terms
        !-- and add to the objective function value

        if (use_quad_penalty_meth) then

           !-- Add penalty term for violation of fixed thickness 
           !-- constraints

           call CalcTCon(jdim,kdim,y,t3)

           obj = obj + t3
       
           !-- Range thickness penalty (find max thickness in x range)

           call CalcRTCon(jdim,kdim,y,rt3)

           obj = obj + rt3

           !-- Calculate area constraint penalty --
   
           call CalcACon(jdim,kdim,x,y,a3)

           obj = obj + a3

           !-- Calculate radius of curvature penalty (Chad Oldfield)

           call CalcRCCon(jdim,kdim,x,y,rc_con)

           obj = obj + rc_con
           
        end if

      !----------------------------------------------------------------
      !--- See 'unsteadyobjandderiv.f' for objective functions 9 - 14
      !----------------------------------------------------------------  

      else if (obj_func .eq. 15) then 

        !-- Off-design constraint function J = Lift
 
        obj = clt

        !-- Get bounds on off-design constraint
        if (off_flags(rank+1)) then
           c_upp = c_upp1(rank+1)
           c_low = c_low1(rank+1)
        end if

        !-- Scale constraint by its bound
        if (scale_con) then
          obj = obj/c_low
        end if

        obj0 = 1.0


      else if (obj_func.eq.16) then

         !-- Off-design constraint function J = maximum mach # in flow 
         !-- field

         call getMaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach)

         obj = MaxMach
         
         obj0 = 1.0
         
         obj = obj/obj0
         
         !-- Save max Mach number to file

         if (mout) write(mach_unit,131) ifun, iter, MaxMach, jMaxMach,
     &                                  kMaxMach
 131     format(i4, 3x, i4, 3x, e16.10, 3x, i4, 3x, i4)

      else if (obj_func.eq.17) then

         !-- Off-design constraint function J = max Mach # evaluated 
         !-- using the KS function
 
         call KSmaxMach(q,jdim,kdim,ndim,MaxMach,jMaxMach,kMaxMach,
     &             aMaxMach,uMaxMach,vMaxMach,pMaxMach,ks,ksMach)
         
         obj = ksMach

         !-- Get bounds on off-design constraint
         if (off_flags(rank+1)) then
            c_upp = c_upp1(rank+1)
            c_low = c_low1(rank+1)
         end if
         
         !-- Scale constraint by its bound
         if (scale_con) then
            obj = obj/c_upp
         end if

         obj0 = 1.0

         !-- Save max Mach number to file

         if (mout) write(mach_unit,132) ifun, iter, MaxMach, ksMach, 
     &                       jMaxMach, kMaxMach
 132     format(i4, 3x, i4, 3x, e16.10, 3x, e16.10, 3x, i4, 3x, i4)

      else if (obj_func .eq. 18) then

        !-- Off-design constraint function J = 1/Lift

        obj = 1.d0/clt

        !-- Un-normalized objective function
        obj1 = obj

        if (itfirst.eq.1) then
          obj0 = obj
        end if

        obj = obj/obj0

      else if (obj_func .eq. 19) then

        !-- Objective function J = lift-constrained drag minimization
        !-- with the lift constraint handled explicitly by SNOPT

        obj = cdt
        lcon = clt

        if (itfirst.eq.1) then
          if  (scale_obj) then
#ifdef _MPI_VERSION
             !-- Only normalize if used as an objective function
             if (abs(obj) .gt. 1.d-10.and.(.not.off_flags(rank+1))) then
#else
             if (abs(obj) .gt. 1.d-10.and.(.not.off_flags(1))) then
#endif
                obj0 = obj
             else
                obj0 = 1.d0
             end if
          else
             obj0 = 1.d0
          end if
        end if

        obj = obj/obj0      

      else if (obj_func .eq. 20) then

         !-- Inverse aircraft range factor for a jet airplane:
         !-- Objective function J = 1/R 
         !-- R = (Mach)*(Cl/Cd)
         
         R = fsmach*(clt/cdt)
         obj = 1.d0/R
         
         !-- Un-normalized objective function
         obj1 = obj/1.d0

         if (itfirst.eq.1) then
           if  (scale_obj) then
#ifdef _MPI_VERSION
             !-- Only normalize if used as an objective function
             if (abs(obj) .gt. 1.d-10.and.(.not.off_flags(rank+1))) then
#else
             if (abs(obj) .gt. 1.d-10.and.(.not.off_flags(1))) then
#endif
                obj0 = obj
             else
                obj0 = 1.d0
             end if
           else
             obj0 = 1.d0
           end if
        end if

        !-- Normalized objective function
        obj = obj/obj0

      else if (obj_func .eq. 21) then

         !-- Inverse aircraft endurance factor for a jet airplane:
         !-- Objective function J = Cd/Cl 
         
         obj = cdt/clt

         !-- Un-normalized objective function
         obj1 = obj

        if (itfirst.eq.1) then
          if  (scale_obj) then
#ifdef _MPI_VERSION
             !-- Only normalize if used as an objective function
             if (abs(obj) .gt. 1.d-10.and.(.not.off_flags(rank+1))) then
#else
             if (abs(obj) .gt. 1.d-10.and.(.not.off_flags(1))) then
#endif
                obj0 = obj
             else
                obj0 = 1.d0
             end if
          else
             obj0 = 1.d0
          end if
        end if

        !-- Normalized objective function
        obj = obj/obj0

        write(scr_unit,*)'calcobj: obj1 = ',obj1
         
      end if

      return
      end                       !CalcObj
