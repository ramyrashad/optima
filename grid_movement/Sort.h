#ifndef TAGSORT_H
#define TAGSORT_H

#include <vector>
#include <cmath>

using namespace std;

void tagSort (vector <int>& dataArr, vector <int>& tagArr, 
		     const int& size);

void tagSort(vector <int>& dataArr, int *tagArr, const int* size);

void tagSort(const int* dataArr, int *tagArr, const int& size);

void swap2 ( int& x, int& y );

void bubbleSort ( int*& arr, double*& val, const int& size );

void swap2 ( double& x, double& y );

void quickSort(int*& numbers, int& start, int& array_size);

void q_sort(int*& numbers, int& left, int& right);

#endif
