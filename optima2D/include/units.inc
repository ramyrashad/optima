c     -- include file which passes input/output file common block --
c     written by: marian nemec, 4/12/00

c     -- cyclone units --
      integer grid_unit, cylgrd_unit, res_unit, turb_unit, bud_unit
      integer time_unit, cf_unit, q2_unit, q_unit, out_unit, cp_unit
      integer ld_unit, rq_unit, rq2_unit, his_unit, turbhis_unit
      integer subit_unit, qc_unit, qb_unit, n_cphis, n_cfhis
      integer rqo_unit, blprop_unit, blvel_unit, gnuplot_res_unit
      integer tp_unit, dettp_unit

c     -- optimizer units --
      integer bdef_unit, ohis_unit, ghis_unit, opt_unit, cptar_unit
      integer dvs_unit, ksopt_unit, tcon_unit, gmres_unit, ac_unit
      integer bc_unit, dvhis_unit, gvhis_unit, bspr_unit, dvsr_unit
      integer ip_unit, op_unit, par_unit, n_best
      integer n_mpo, n_mpcp, prefix_unit
      integer dpgrad_unit, blvp_unit 
      integer hes_unit, hest_unit, fgt_unit, rccon_unit, obj_unit
      integer svhis_unit, input_unit, scr_unit, mach_unit, aoa_unit
      integer snopt_unit, snfg_unit, rest_unit, dbc_unit, wtint_unit
      integer opt_def_unit

      common/ units/ grid_unit, cylgrd_unit, res_unit, turb_unit,
     &     bud_unit, time_unit, cf_unit, q2_unit, q_unit, out_unit,
     &     cp_unit, ld_unit, rq_unit, rq2_unit, his_unit, turbhis_unit,
     &     subit_unit, qc_unit, qb_unit, n_cphis, n_cfhis, bdef_unit,
     &     ohis_unit, ghis_unit, opt_unit, cptar_unit, dvs_unit,
     &     ksopt_unit, tcon_unit, gmres_unit, ac_unit, bc_unit,
     &     dvhis_unit, gvhis_unit, bspr_unit, dvsr_unit, ip_unit,
     &     op_unit, par_unit,n_mpo, n_mpcp, n_best, prefix_unit, 
     &     hes_unit, hest_unit, fgt_unit, rccon_unit, rqo_unit,
     &     obj_unit, svhis_unit, input_unit, scr_unit, mach_unit,
     &     aoa_unit, snopt_unit, snfg_unit, rest_unit, dbc_unit,
     &     opt_def_unit, wtint_unit,dpgrad_unit, blvp_unit,
     &     blprop_unit, blvel_unit, gnuplot_res_unit, tp_unit,
     &     dettp_unit
