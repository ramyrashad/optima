      subroutine fr_test (jdim, kdim, ndim, indx, iex, qnow, q,
     &     qold, xy, xyj, x, y, precon, coef2, coef4, uu, vv, ccx, 
     &     ccy, ds, press, tmet, spect, gam, turmu, fmu, vort, 
     &     sndsp, s, bcf, as, ja, ia, pa, jpa, 
     &     ipa, framux, rhsnamult,D_hat, exprhs,a_jk, its_nk_val,dtvec)
      
#include "../include/arcom.inc"

      integer jdim, kdim, ndim, na, indx(jdim,kdim), i,j,k,n,ii
      integer its, ifirst, iex(jdim,kdim,4)
      integer ja(*), ia(*), jpa(*), ipa(*) 

      double precision q(jdim,kdim,ndim), xy(jdim,kdim,4)
      double precision qnow(jdim,kdim,ndim), qold(jdim,kdim,ndim)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision press(jdim,kdim), sndsp(jdim,kdim)
      double precision tmet(jdim,kdim), ds(jdim,kdim)
      double precision fmu(jdim,kdim), vort(jdim,kdim)
      double precision spect(jdim,kdim,3), turmu(jdim,kdim)
      double precision uu(jdim,kdim), vv(jdim,kdim)
      double precision ccx(jdim,kdim), ccy(jdim,kdim)
      double precision coef4(jdim,kdim), coef2(jdim,kdim)
      double precision precon(jdim,kdim,6), gam(jdim,kdim,16)
      double precision qtmp(jdim,kdim,ndim),stmp(jdim,kdim,ndim)

      double precision s(jdim,kdim,ndim),work(jdim,kdim,4)
      double precision bcf(jdim,kdim,4),as(*), pa(*)

      double precision D_hat(jdim,kdim,ndim,6),dtvec(*)
      double precision exprhs(jdim,kdim,ndim),a_jk(6,6)

      double precision wk_temp1(jdim*kdim*ndim),wk_temp2(jdim*kdim*ndim)
      double precision ones(jdim*kdim*ndim),q_temp(jdim*kdim*ndim)
      double precision qdummy(jdim,kdim,ndim),rhsnamult

c     -- Random Seed Variables
      integer, dimension(1) :: old, seed
      double precision harvest
      
      na   = jmax*kdim*ndim
      if (clalpha2) na=na+1

      ifirst = 0

      do n = 1,ndim                                                 
         do k = kbegin,kend
            do j = jbegin,jend
               qdummy(j,k,n)=0.d0
            end do
         end do
      end do



      do ii=1,na

c     seed an arbitrary vector of ones and a temporary work array

c$$$      seed = 12345
c$$$      call random_seed
c$$$      call random_seed(size=k)
c$$$      call random_seed(get=old)
c$$$      call random_number(harvest)
c$$$      call random_seed(get=old)
c$$$      call random_seed(put=seed)
c$$$      call random_seed(get=old)
c$$$
c$$$      do i=1,na
c$$$         call random_number(harvest)
c$$$         ones(i)=0.2d0*harvest+0.9d0        ! -- random perturbation array
c$$$         wk_temp1(i)=0.d0        ! -- zero work arrays
c$$$         wk_temp2(i)=0.d0
c$$$      end do

         do i=1,na
            ones(i)=0.d0        
            wk_temp1(i)=0.d0        
            wk_temp2(i)=0.d0
         end do

         ones(ii)=1.d0

         print *, 'perturb node',ii

         call framux (na, jdim, kdim, ndim, indx, iex, ones,
     &        wk_temp1, q, xy, xyj, x, y, precon, coef2, coef4, uu,
     &        vv, ccx, ccy, ds, press, tmet, spect, gam, turmu, fmu,
     &        vort, sndsp, s, bcf, qtmp, qnow, qold, stmp, 
     &        ifirst, as, work, rhsnamult, D_hat, exprhs, a_jk,dtvec)
    
     
c         call amux(na, ones, wk_temp2, pa, jpa, ipa)
         call amux(na, ones, wk_temp2, as, ja, ia)

         do i=1,na              ! calculates the difference
            if (dabs(wk_temp2(i)).gt.1.d-8) then
               q_temp(i)=(wk_temp1(i)-wk_temp2(i))/dabs(wk_temp2(i))
            else
c               print *, 'Small!',i,wk_temp2(i)
               q_temp(i)=dabs(wk_temp1(i)-wk_temp2(i))
            end if
         end do 

         do n = 1,ndim                                                 
            do k = kbegin,kend
               do j = jbegin,jend
                  jk = ( indx(j,k)-1)*ndim + n
                  if ( abs(q_temp(jk)) .gt. 5.d-2 .and. 
     &                 abs(wk_temp1(jk)-wk_temp2(jk)).gt.1.d-3    ) then 
                     write(*,10) j,k,n,q_temp(jk),wk_temp1(jk),
     &                    wk_temp2(jk)
                     qdummy(j,k,n)=q_temp(jk)
                  endif
               end do
            end do
         end do

 10      format(1x,i3,1x,i3,1x,i3,4x,e16.8,e16.8,e16.8)

      end do

      write(q_unit) jdim,kdim                                             
      write(q_unit) fsmach,alpha,re*fsmach,fsmach                            
      write(q_unit) (((qdummy(j,k,n),j=1,jdim) ,k=1,kdim) ,n=1,4)
      write(q_unit) jtail1, numiter                                          
c                                                                       
      if (itmodel.eq.2) then
        write(q_unit) ((turre(j,k), j=1,jdim) ,k=1,kdim)
        write(q_unit) ((turmu(j,k), j=1,jdim) ,k=1,kdim)
      endif
      rewind q_unit    
  

      stop
        
      return
      end                       !fr_test
