!
! =====================================================================
!
module Hybropt_Mod
! 
! ---------------------------------------------------------------------
! Purpose:
!    This module directs hybrid optimization process. 
!
! Author: 
!    Oleg Chernukhin, May 2010
! ---------------------------------------------------------------------

use mpi
use CommonH
use Sampling_Mod
use Database_Mod
use Snopt_Mod
use Surrogate_Mod
use SolverInterface_Mod
use GA_Mod

implicit none

! ________________________
! module subroutines
contains
!
! =====================================================================
!
subroutine optimize(husrfun, nchr, nppc, carr, cid, hybr_meth, &
                    sobol_file, usedb, mingen, maxgen, ranseed, &
                    selmethod, prob_pert, prob_mut, prob_avg, pen, &
                    xbest, xavg, xpert, xmut, sn_maj_lim, flow_sol_lim,&
                    sample_offset, cig, optf_output, opt_info_unit, &
                    opt_his_unit, opt_def_unit, dpfile_unit, &
                    glob_comm, lb_tol, sampl_seq_unit, csampl)
!
! ------------------------------------------------------------------
!
! Purpose: 
!    Driver subroutine that coordinates the optimization process
!
! -------------------------------------------------------------------
! ___________________________________
! variable specification

   !-- subroutine arguments
   interface 
      subroutine husrfun(dv, nvar, f, nF, G, lenG, comm, csz, cid, &
                         fsn_base, stat, needF, needG)
         integer :: nvar, nF, lenG, csz, cid, fsn_base, stat
         logical :: needF, needG
         real(kind(1.d0)) :: dv(nvar), f(nF), G(lenG)
         integer :: comm(csz)
      end subroutine
   end interface
   integer :: opt_info_unit, opt_his_unit, nppc, nchr, cid, hybr_meth,&
              maxgen, ranseed, selmethod, sn_maj_lim, flow_sol_lim, &
              sample_offset, opt_def_unit, optf_output, dpfile_unit, &
              glob_comm, sampl_seq_unit, mingen
   integer :: carr(nchr)
   real(kind=dp) :: prob_pert, prob_mut, &
                    prob_avg, xbest, xavg, xpert, xmut, pen, &
                    lb_tol
   character(128) :: sobol_file
   logical :: cig, usedb, csampl

   !-- local variables
   logical :: file_exists
   integer :: ii, jj, kk, snstat, ngen, ns, ng, d100, d10, d1, ncig
   integer :: file_stat
   integer :: statarr(nchr)
   integer, allocatable :: Xso(:), XsoRev(:), sampl_arr(:)
   real(kind=dp), allocatable :: ssample(:,:), popX(:,:), dvx(:,:), &
                                 popFnc(:,:), G(:), X(:), Fnc(:), &
                                 lowV(:), uppV(:)
   real(kind=dp) :: fnctmp1, fnctmp2, feas
   character(128) :: dpfile
   
! _____________________
! begin main execution
   !-- Wait for all processes to reach this entry point 
   call mpi_barrier(glob_comm, ierr)

   !-- Point hu_ptr procedure pointer to husrfun, so it is accessible
   !-- by other subroutines
   hu_ptr => husrfun

   !-- Assign parameter values
   HYBROPT_COMM = glob_comm
   ppc = nppc
   popsize = nchr
   allocate(comm(popsize))
   fsn_base = 0
   hybropt_method = hybr_meth
   snstat = 0
   statarr(:) = 0

   allocate(donebuffer(popsize), &
            done_recv_req(popsize), &
            doneflag(popsize), &
            sampl_arr(popsize))

   !-- Assign global mpi attributes
   call mpi_comm_size(HYBROPT_COMM, nproc_glob, ierr)
   call mpi_comm_rank(HYBROPT_COMM, myrank_glob, ierr)

   !-- Check if optimization.def exists
   inquire(file='optimization.def', exist=file_exists)
   call mpi_barrier(HYBROPT_COMM, ierr)

   if (myrank_glob .and. (.not. file_exists)) then
      write(*,*) 'ERROR: Hybropt_Mod:optimize :: ', &
                 'optimization.def file not found'
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif

   if (myrank_glob == 0) write(*,*) 'INFO: Reading in optimization.def'
  
   open(unit=opt_def_unit, file='optimization.def', &
        status='old', form='formatted', iostat=file_stat)
   if (file_stat /= 0) then
      write(*,*) 'ERROR: Hybropt_Mod:optimize :: ', &
                 'error opening optimization.dat file '
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif

   !-- Read nX, nFnc, nnzLinG, nnzGrad
   read(opt_def_unit, *) nX, nFnc, nnzLinG, nnzGrad

   !-- Allocate arrays
   allocate(iLfun(nnzLinG), jLvar(nnzLinG), &
            iGfun(nnzGrad), jGvar(nnzGrad), Fnc(nFnc), Xso(nX), &
            XsoRev(nX), &
            LinG(nnzLinG), X(nX), Xlow(nX), Xupp(nX), Xmul(nX), &
            Flow(nFnc), Fupp(nFnc), Fmul(nFnc), ssample(nchr,nX), &
            popX(nchr,nX), dvx(2,nX), popFnc(nchr,nFnc), &
            G(nnzGrad), lowV(nFnc+1), uppV(nFnc+1))

   !-- Read Xlow, X, Xupp, Xmul, Xso
   do ii = 1, nX
      read(opt_def_unit, *) Xlow(ii), X(ii), Xupp(ii), Xmul(ii), &
                            Xso(ii)
   end do

   !-- Read Flow, Fnc, Fupp, Fmul
   do ii = 1, nFnc
      read(opt_def_unit, *) Flow(ii), Fnc(ii), Fupp(ii), Fmul(ii)
   end do

   !-- Read iLfun, jLvar, LinG
   do ii = 1, nnzLinG
      read(opt_def_unit, *) iLfun(ii), jLvar(ii), LinG(ii)
   end do

   !-- Read iGfun, jGvar
   do ii = 1, nnzGrad
      read(opt_def_unit, *) iGfun(ii), jGvar(ii)
   end do

   write(*,*) 'INFO: Finished reading in optimization.def'
   close(opt_def_unit)

   opt_output = optf_output
   iPrintC = opt_info_unit
   iSummC = opt_his_unit
   useDatabase = usedb
   penalty = pen
   load_bal_tol = lb_tol

   !-- Assign local mpi attributes
   mycid = cid
   do ii = 1, popsize
      comm(ii) = carr(ii)
   end do
   call mpi_comm_rank(comm(mycid), myrank_loc, ierr)
   
   !-- Make sure we have enough processes
   if (nproc_glob < ppc * popsize) then
      if (myrank_glob == 0) then
         write (*, *) 'ERROR: insufficient number of processes'
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif
   endif

   !-- Fill XsoRev
   do ii = 1, size(Xso)
      XsoRev(Xso(ii)) = ii
   end do

   !-- Open optimization output files, if neccessary
   if (opt_output > 0 .and. myrank_loc == 0) then

      d100 = (mycid - 1) / 100
      d10  = (mycid - 1) / 10 - (d100 * 10)
      d1   = (mycid - 1) / 1  - (d100 * 100) - (d10 * 10)

      info_file = 'optimize'
      hist_file = 'optimize'
      
      info_file(len_trim(info_file)+1:len_trim(info_file)+9) &
         = '-'//achar(d100+48)//achar(d10+48)//achar(d1+48)//'.info'
      hist_file(len_trim(hist_file)+1:len_trim(hist_file)+8) &
         = '-'//achar(d100+48)//achar(d10+48)//achar(d1+48)//'.his'

      open (unit=iPrintC, file=info_file, status='new', &
            form='formatted', iostat=ierr)
      open (unit=iSummC, file=hist_file, status='new', &
            form='formatted', iostat=ierr)

   endif

   !-- Initialize database, if neccessary
   if (hybr_meth == 1 .or. &
       hybr_meth == 4 .or. &
       hybr_meth == 5 .or. &
       hybr_meth == 6) then
      !-- Open only one database file (used by global root)
      if (useDatabase) call initdb(.true.)
   else
      !-- Open a database file for each population member
      if (useDatabase) call initdb(.false.)
   endif

   !-- Decide which hybrid optimization path to take  
   select case (hybr_meth)

   case (1) !--------------------------------------------------------
      !-- Perform a normal SNOPT run with X as the initial guess
      !-- The idea is this should be equivalent to 'optimize' method
      !-- in jetstream.

      !-- Make sure popsize is 1
      if (popsize .ne. 1) then
         if (myrank_glob == 0) then
            write (*,*) 'ERROR: invalid hybr_meth value = ', hybr_meth
            call mpi_abort(HYBROPT_COMM, 0, ierr) 
         endif
         call mpi_barrier(HYBROPT_COMM, ierr)
      endif

      !-- Fill in popX, assuming its dimensions are (1,nX)
      do jj = 1, nX
         popX(1, jj) = X(jj)
      end do
 
      !-- Reset flow solve counters
      if (myrank_loc == 0) then
         fsn_base = (mycid-1)
      endif
      call mpi_bcast(fsn_base, 1, MPI_INTEGER, 0, comm(mycid), ierr)

      !-- Broadcast popX values to every process
      call mpi_bcast(popX(1,:), nX, MPI_DOUBLE_PRECISION, 0, &
                     HYBROPT_COMM, ierr)

      !-- Run SNOPT
      call runSNOPT(popX, popFnc, sn_maj_lim, opt_output, snstat, &
                    hsn_usrfun)
   
      statarr(mycid) = snstat

      !-- Output status and objective value
      if (myrank_loc == 0) then
         write(*,*) mycid, ':', 'SNOPT info = ', snstat
         write(*,*) mycid, ':', 'F(X) = ', popFnc(1,1)
      endif
  
      !-- Done case 1

   case (2) !--------------------------------------------------------
      !-- Perform full SNOPT runs (for each chromosome)
      !-- This is the gradient-based "multistart" procedure
      !-- done in parallel

      !-- Initialize sampling 
      if (myrank_loc == 0) call readDirNums(sobol_file, nX)

      if (csampl) then
         write(*,*) 'INFO: reading custom sample list'
         open (unit=sampl_seq_unit, file='sample_sequence.dat', &
               status='unknown', form='formatted', iostat=ierr)
         read (unit=sampl_seq_unit, fmt=*, iostat = ierr) ncig
         if (ncig .ne. popsize .and. myrank_glob == 0) then
            write(*,*) 'ERROR: number of samples must equal popsize', ncig
            call mpi_abort(HYBROPT_COMM, 0, ierr)
         endif

         do ii = 1, popsize
            read(unit=sampl_seq_unit, fmt=*, iostat=ierr) sampl_arr(ii)
         end do

         close(sampl_seq_unit)
      endif

      !-- Loop, in case we want to do more than popsize initial guesses
      do kk = sample_offset, sample_offset

         !-- Reset flow solve counters
         if (myrank_loc == 0) then
            fsn_base = (mycid-1)
         endif
         call mpi_bcast(fsn_base, 1, MPI_INTEGER, 0, comm(mycid), ierr)

         !-- Get sobol sample and fill popX
         if (myrank_loc == 0) then
            do ii = 1, popsize
               if (csampl) then
                  call getSobolPt(sampl_arr(ii), nX, ssample(ii,:))
               else
                  call getSobolPt(ii+popsize*kk, nX, ssample(ii,:))
               endif
            end do
            do ii = 1, popsize
               do jj = 1, nX
#if 1
                  !-- New and improved sampling method
                  call getLU(jj, Xso, XsoRev, nX, lowV, uppV, &
                             size(lowV), popX(ii,:))
                  popX(ii, Xso(jj)) = maxval(lowV(:)) + &
                                      ssample(ii, Xso(jj)) * &
                                      abs(minval(uppV(:)) - &
                                          maxval(lowV(:)))
#endif
#if 0
                  !-- Old sampling method
                  popX(ii, Xso(jj)) = Xlow(Xso(jj)) + &
                                      ssample(ii, Xso(jj)) * &
                                      abs(Xupp(Xso(jj)) - Xlow(Xso(jj)))
#endif
#if 0
               if (ii == 1) popX(ii, Xso(jj)) = Xlow(ii)
               if (ii == 2) popX(ii, Xso(jj)) = Xupp(ii)
#endif
               end do
            end do
         endif

         !-- If needed, insert specific initial guesses into the sample
         if (cig .and. myrank_glob == 0) then

            write(*,*) 'INFO: adding custom initial guesses'

            open (unit=dpfile_unit, file='design_point.dat', &
                  status='unknown', form='formatted', iostat=ierr)

            read (unit=dpfile_unit, fmt=*, iostat = ierr) ncig

            do ii = 1, ncig
               read (dpfile_unit, *)
               do jj = 1, nX
                  read (unit=dpfile_unit, fmt=*, iostat=ierr) d1, d10,&
                        popX(popsize-ii+1, jj)
               end do
            end do
            close(dpfile_unit)
 
         endif

         !-- Broadcast popX to all processes
         do jj = 1, popsize
            call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, 0, &
                           HYBROPT_COMM, ierr)
         end do

         !-- Run SNOPT
         ind_feval = 0
         call runSNOPT(popX, popFnc, 1000, opt_output, snstat, &
                       hsn_usrfun)             

#if 0
!-- Outputs total number of func. evals up to this point
do jj = 1, popsize
call mpi_bcast(popFnc(jj,:), nFnc, MPI_DOUBLE_PRECISION, &
               (jj-1)*ppc, HYBROPT_COMM, ierr)
end do
ngen = tot_feval
tot_feval = 0
call mpi_allreduce(ind_feval, tot_feval, 1, MPI_INTEGER, MPI_SUM, &
                   HYBROPT_COMM, ierr)
tot_feval = ngen + tot_feval
if (myrank_glob == 0) write(opt_his_unit, *) tot_feval, ',', minval(popFnc(:, 1))
#endif

         statarr(mycid) = snstat

         !-- Output status and objective value
         if (myrank_loc == 0) then
            write(*,*) mycid, ':', 'SNOPT info = ', snstat
            write(*,*) mycid, ':', 'F(X) = ', popFnc(mycid, 1)
         endif

      end do

      !-- Done case 2

   case (3) !--------------------------------------------------------
      !-- Hybrid optimization method
      !-- Run a few SNOPT iterations, then use GA to create new gen.

      !-- Initialize sampling
      if (myrank_loc == 0) call readDirNums(sobol_file, nX)

      if (csampl) then
         write(*,*) 'INFO: reading custom sample list'
         open (unit=sampl_seq_unit, file='sample_sequence.dat', &
               status='unknown', form='formatted', iostat=ierr)
         read (unit=sampl_seq_unit, fmt=*, iostat = ierr) ncig
         if (ncig .ne. popsize .and. myrank_glob == 0) then
            write(*,*) 'ERROR: number of samples must equal popsize', ncig
            call mpi_abort(HYBROPT_COMM, 0, ierr)
         endif

         do ii = 1, popsize
            read(unit=sampl_seq_unit, fmt=*, iostat=ierr) sampl_arr(ii)
         end do

         close(sampl_seq_unit)
      endif

      !-- Reset flow solve counters
      if (myrank_loc == 0) then
         fsn_base = (mycid-1)
      endif
      call mpi_bcast(fsn_base, 1, MPI_INTEGER, 0, comm(mycid), ierr)

      !-- Get sobol sample and fill popX
      if (myrank_loc == 0) then
         do ii = 1, popsize
            if (csampl) then
               call getSobolPt(sampl_arr(ii), nX, ssample(ii,:))
            else
               call getSobolPt(ii+popsize*kk, nX, ssample(ii,:))
            endif
         end do
         do ii = 1, popsize
            do jj = 1, nX
#if 1
               !-- New and improved sampling method
               call getLU(jj, Xso, XsoRev, nX, lowV, uppV, &
                          size(lowV), popX(ii,:))
               popX(ii, Xso(jj)) = maxval(lowV(:)) + &
                                   ssample(ii, Xso(jj)) * &
                                   abs(minval(uppV(:)) - &
                                       maxval(lowV(:)))
#endif
#if 0
               !-- Old sampling method
               popX(ii, Xso(jj)) = Xlow(Xso(jj)) + &
                                   ssample(ii, Xso(jj)) * &
                                   abs(Xupp(Xso(jj)) - Xlow(Xso(jj)))
#endif
            end do
         end do
      endif

      !-- If needed, insert specific initial guesses into the sample
      if (cig .and. myrank_glob == 0) then

         open (unit=dpfile_unit, file='design_point.dat', &
               status='unknown', form='formatted', iostat=ierr)

         read (unit=dpfile_unit, fmt=*, iostat = ierr) ncig

         do ii = 1, ncig
            read (dpfile_unit, *)
            do jj = 1, nX
               read (unit=dpfile_unit, fmt=*, iostat=ierr) d1, d10, &
                     popX(popsize-ii+1, jj)
            end do
         end do
         close(dpfile_unit)
      endif

      !-- Broadcast respective popX values to non-root processes
      call mpi_bcast(popX(mycid,:), nX, MPI_DOUBLE_PRECISION, 0, &
                     comm(mycid), ierr)

      !-- Call runGenetic
      call runGenetic(popX, popFnc, mingen, maxgen, ranseed, &
                      selmethod, prob_pert, prob_mut, &
                      prob_avg, xbest, xavg, xpert, xmut, &
                      .false., .true., sn_maj_lim, flow_sol_lim)

      !-- Run SNOPT to full convergence
      call runSNOPT(popX, popFnc, 1000, opt_output, snstat, &
                    hsn_usrfun)

      statarr(mycid) = snstat

      !-- Output status and objective value
      if (myrank_loc == 0) then
         write(*,*) mycid, ':', 'SNOPT info = ', snstat
         write(*,*) mycid, ':', 'F(X) = ', popFnc(mycid, 1)
      endif

      !-- Done case 3

   case (4) !--------------------------------------------------------
      !-- Perform pure GA optimization using Evolver
      !-- Nonlinear contraints handled using quad. penalty method

      !-- Reset flow solve counters
      if (myrank_loc == 0) then
         fsn_base = (mycid-1)
      endif
      call mpi_bcast(fsn_base, 1, MPI_INTEGER, 0, comm(mycid), ierr)

      !-- Initialize sampling
      if (myrank_loc == 0) call readDirNums(sobol_file, nX)

      if (csampl) then
         write(*,*) 'INFO: reading custom sample list'
         open (unit=sampl_seq_unit, file='sample_sequence.dat', &
               status='unknown', form='formatted', iostat=ierr)
         read (unit=sampl_seq_unit, fmt=*, iostat = ierr) ncig
         if (ncig .ne. popsize .and. myrank_glob == 0) then
            write(*,*) 'ERROR: number of samples must equal popsize', ncig
            call mpi_abort(HYBROPT_COMM, 0, ierr)
         endif

         do ii = 1, popsize
            read(unit=sampl_seq_unit, fmt=*, iostat=ierr) sampl_arr(ii)
         end do

         close(sampl_seq_unit)
      endif

      !-- Get sobol sample and fill popX
      if (myrank_loc == 0) then
         do ii = 1, popsize
            if (csampl) then
               call getSobolPt(sampl_arr(ii), nX, ssample(ii,:))
            else
               call getSobolPt(ii+popsize*kk, nX, ssample(ii,:))
            endif
         end do
         do ii = 1, popsize
            do jj = 1, nX
#if 1
               !-- New and improved sampling method
               call getLU(jj, Xso, XsoRev, nX, lowV, uppV, &
                          size(lowV), popX(ii,:))
               popX(ii, Xso(jj)) = maxval(lowV(:)) + &
                                   ssample(ii, Xso(jj)) * &
                                   abs(minval(uppV(:)) - &
                                       maxval(lowV(:)))
#endif
#if 0
               !-- Old sampling method
               popX(ii, Xso(jj)) = Xlow(Xso(jj)) + &
                                   ssample(ii, Xso(jj)) * &
                                   abs(Xupp(Xso(jj)) - Xlow(Xso(jj)))
#endif
            end do
         end do
      endif

      !-- If needed, insert specific initial guesses into the sample
      if (cig .and. myrank_glob == 0) then

         open (unit=dpfile_unit, file='design_point.dat', &
               status='unknown', form='formatted', iostat=ierr)

         read (unit=dpfile_unit, fmt=*, iostat = ierr) ncig

         do ii = 1, ncig
            read (dpfile_unit, *)
            do jj = 1, nX
               read (unit=dpfile_unit, fmt=*, iostat=ierr) d1, d10, &        
                     popX(popsize-ii+1, jj)
            end do
         end do 
         close(dpfile_unit)
      endif

      !-- Broadcast popX to all processes
      do jj = 1, popsize
         call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, 0, &
                        HYBROPT_COMM, ierr)
      end do

      !-- Call runGenetic
      call runGenetic(popX,popFnc,mingen,maxgen,ranseed,selmethod,&
                      prob_pert, prob_mut, &
                      prob_avg, xbest, xavg, xpert, xmut, &       
                      .false., .false., sn_maj_lim, flow_sol_lim)

      !-- Calculate the actual objective value
      do jj = 1, nnzLinG
         popFnc(mycid, 1) = popFnc(mycid, 1) + &
            LinG(jj) * popX(mycid, jGvar(jj))
      end do
    
      !-- Broadcast popFnc to all processes
      do jj = 1, popsize
         call mpi_bcast(popFnc(jj,:), nFnc, MPI_DOUBLE_PRECISION, &
                        (jj-1)*ppc, HYBROPT_COMM, ierr)
      end do

      if (myrank_glob == 0) then
         write(*,*) mycid, ':', 'Evolver info = ', 1
         write(*,*) mycid, ':', 'F(X) = ', minval(popFnc(:, 1))
      endif      
  
      !-- Done case 4

   case (5) !--------------------------------------------------------
      !-- Generate database for this optimization problem,
      !-- using Sobol sampling

      !-- Initialize sampling
      if (myrank_glob == 0) call readDirNums(sobol_file, nX)

      !-- Get initial sobol sample and fill popX
      if (myrank_glob == 0) then
         do ii = 1, popsize
            call getSobolPt(ii, nX, ssample(ii,:))
         end do
         do ii = 1, popsize
            do jj = 1, nX
               popX(ii, jj) = Xlow(jj) + ssample(ii, jj) * &
                              abs(Xupp(jj) - Xlow(jj))
            end do
         end do
      endif

      !-- Reset flow solve counters
      if (myrank_loc == 0) then
         fsn_base = (mycid-1)
      endif
      call mpi_bcast(fsn_base, 1, MPI_INTEGER, 0, comm(mycid), ierr)

      !-- Broadcast popX to all processes
      do jj = 1, popsize
         call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, 0, &
                        HYBROPT_COMM, ierr)
      end do

      !-- Keep flow-solving and sampling to populate the database
      do kk = 1, maxgen

         !-- Evaluate function at sample points
         if (kk == 1) snstat = 1
         if (kk >  1) snstat = 0
         call evalf(nX, popX(mycid,:), nFnc, popFnc(mycid,:), &
                    nnzGrad, G, .true., .true., snstat, .false., &
                    useDatabase)

         !-- Create new sobol sample
         if (myrank_glob == 0) then
            do ii = 1, popsize
               call getSobolPt(ii+popsize*kk, nX, ssample(ii,:))
            end do
            do ii = 1, popsize
               do jj = 1, nX
                  popX(ii, jj) = Xlow(jj) + ssample(ii, jj) * &
                                 abs(Xupp(jj) - Xlow(jj))
               end do
            end do
         endif

         !-- Broadcast popX to all processes
         do jj = 1, popsize
            call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, 0, &
                           HYBROPT_COMM, ierr)
         end do

      end do

      !-- Done case 5

   case (6) !--------------------------------------------------------
      !-- Run GA on surrogate model

      !-- Reset flow solve counters
      if (myrank_loc == 0) then
         fsn_base = (mycid-1)
      endif
      call mpi_bcast(fsn_base, 1, MPI_INTEGER, 0, comm(mycid), ierr)

      !-- Initialize sampling
      if (myrank_loc == 0) call readDirNums(sobol_file, nX)

      !-- Get initial sobol sample and fill popX
      if (myrank_glob == 0) then
         do ii = 1, popsize
            call getSobolPt(ii, nX, ssample(ii,:))
         end do
         do ii = 1, popsize
            do jj = 1, nX
               popX(ii, jj) = Xlow(jj) + ssample(ii, jj) * &
                              abs(Xupp(jj) - Xlow(jj))
            end do
         end do
      endif

      !-- If needed, insert specific initial guesses into the sample
      if (cig .and. myrank_glob == 0) then

         open (unit=dpfile_unit, file='design_point.dat', &
               status='unknown', form='formatted', iostat=ierr)

         read (unit=dpfile_unit, fmt=*, iostat = ierr) ncig

         do ii = 1, ncig
            read (dpfile_unit, *)
            do jj = 1, nX
               read (unit=dpfile_unit, fmt=*, iostat=ierr) d1, d10, &
                     popX(popsize-ii+1, jj)
            end do
         end do
         close(dpfile_unit)
      endif

      !-- Broadcast popX to all processes
      do jj = 1, popsize
         call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, 0, &
                        HYBROPT_COMM, ierr)
      end do

      !-- Call runGenetic on surrogate model
      call runGenetic(popX, popFnc, mingen,maxgen,ranseed,selmethod,&
                      prob_pert, prob_mut, &
                      prob_avg, xbest, xavg, xpert, xmut, &
                      .true., .false., sn_maj_lim, flow_sol_lim)

      !-- Calculate the actual objective value
      do jj = 1, nnzLinG
         popFnc(mycid, 1) = popFnc(mycid, 1) + &
                            LinG(jj) * popX(mycid, jGvar(jj))
      end do

      !-- Broadcast popFnc to all processes
      do jj = 1, popsize
         call mpi_bcast(popFnc(jj,:), nFnc, MPI_DOUBLE_PRECISION, &
                        (jj-1)*ppc, HYBROPT_COMM, ierr)
      end do

      !-- Should sort popFnc in case popFnc(1,:) is not the best value
      !-- But the chances of this are fairly slim
      if (myrank_glob == 0) then
         !-- Calculate feasibility
         feas = 0.0d0
         do ii = 2, nFnc
            if (popFnc(1, ii) < Flow(ii)) then
               feas = feas + abs(popFnc(1, ii) - Flow(ii))
            endif
            if (popFnc(1, ii) > Fupp(ii)) then
               feas = feas + abs(popFnc(1, ii) - Fupp(ii))
            endif
         end do
         write(*,*) mycid, ':', 'Evolver info = ', 1
         write(*,*) mycid, ':', 'F(X) = ', popFnc(1, 1)
         write(*,*) mycid, ':', 'Feasible = ', feas
         do ii = 1, nX
            write(*,*) mycid, ': X', ii, popX(1, ii)
         end do
         do ii = 1, nFnc
            write(*,*) mycid, ': Fnc', ii, popFnc(1, ii)
         end do
      endif

      !-- Done case 6

   case (7) !--------------------------------------------------------
      !-- Interpolates between two dv vectors in design_point file
      !-- (For animations)

      !-- Reset flow solve counters
      if (myrank_loc == 0) then
         fsn_base = (mycid-1)
      endif
      call mpi_bcast(fsn_base, 1, MPI_INTEGER, 0, comm(mycid), ierr)

      !-- Read dv vectors
      if (cig .and. myrank_glob == 0) then

         open (unit=dpfile_unit, file='design_point.dat', &
               status='unknown', form='formatted', iostat=ierr)

         read (unit=dpfile_unit, fmt=*, iostat = ierr) ncig

         do ii = 1, 2
            read (dpfile_unit, *)
            do jj = 1, nX
               read (unit=dpfile_unit, fmt=*, iostat=ierr) d1, d10, &
                     dvx(ii,jj)
            end do
         end do
         close(dpfile_unit)
      endif

      if (myrank_glob == 0) then
         feas = 1.0d0 / (popsize-1)
         do jj = 1, popsize
            popX(jj,:) = dvx(1,:) + (jj-1)*feas*(dvx(2,:)-dvx(1,:))
         end do
      endif

      !-- Broadcast popX to all processes
      do jj = 1, popsize
         call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, 0, &
                        HYBROPT_COMM, ierr)
      end do

      !-- Run SNOPT
      ind_feval = 0
      call runSNOPT(popX, popFnc, 1000, opt_output, snstat, &
                    hsn_usrfun)             

      !-- The end: presumably, the job will be killed before this point      

   case default !----------------------------------------------------
      if (myrank_glob == 0) then
         write (*,*) 'ERROR: invalid hybr_meth value = ', hybr_meth
         call mpi_abort(HYBROPT_COMM, 0, ierr)
      endif

   end select
  
   !-- Broadcast popX and popFnc to all processes
   do jj = 1, popsize
      call mpi_bcast(popX(jj,:), nX, MPI_DOUBLE_PRECISION, (jj-1)*ppc, &
                     HYBROPT_COMM, ierr)
      call mpi_bcast(popFnc(jj,:), nFnc, MPI_DOUBLE_PRECISION, &
                     (jj-1)*ppc, HYBROPT_COMM, ierr)
      call mpi_bcast(statarr(jj), 1, MPI_INTEGER, (jj-1)*ppc, &
                     HYBROPT_COMM, ierr)
   end do

   !-- Output status and objective value
   if (myrank_glob == 0) then
      write(*,*) '--- Hybropt Optimization Summary ---'
      do jj = 1, popsize
         write(*,*) jj, ',', statarr(jj), ',', popFnc(jj, 1)
      end do
   endif

   !-- Write out X to the 'design_point.dat' for future reference
   if (opt_output > 0 .and. myrank_glob == 0) then

      dpfile = 'design_point.dat'

      open (unit=dpfile_unit, file=dpfile, status='new', &
            form='formatted', iostat=ierr)

      write(dpfile_unit, *) popsize
      do ii = 1, popsize
#if 0
         write (dpfile_unit, *) popX(ii, 1), popX(ii, 2)
#endif
#if 1
         write(dpfile_unit, *) '-----------------------'
         do jj = 1, nX
            write(dpfile_unit, *) ii, jj, popX(ii, jj)
         end do
#endif
      end do

      close(dpfile_unit)

   endif

   !-- Close output files
   if (opt_output > 0 .and. myrank_loc == 0) then
      close(opt_info_unit)
      close(opt_his_unit)
   endif

   !-- Close the database file
   if (useDatabase) call closedb()

end subroutine
!
! ===================================================================
!
subroutine hsn_usrfun(stat, nvar, dv, needf, nF, f, needG, lenG, G, &
                      cu, lencu, iu, leniu, ru, lenru)
!
! -------------------------------------------------------------------
!
! Purpose:
!    Subroutine usrfun for SNOPT. Used whenever runSNOPT is called
!    from within this module.
! 
! -------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   integer, intent(in) :: nvar, needf, nF, needG, lenG, lencu, leniu, &
                          lenru
   real(kind=dp), intent(in) :: dv(nvar)
   character(len=8), intent(inout) :: cu(lencu)
   integer, intent(inout) :: stat, iu(leniu)
   real(kind=dp), intent(inout) :: f(nF), G(lenG), ru(lenru)

   !-- local variables
   logical :: needobj, needgrad

! __________________________
! begin main execution

   !-- Synchronize all variables
   call mpi_bcast(stat, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(nvar, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(dv, nvar, MPI_DOUBLE_PRECISION, 0, comm(mycid), ierr)
   call mpi_bcast(needf, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(nF, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(f, nF, MPI_DOUBLE_PRECISION, 0, comm(mycid), ierr)
   call mpi_bcast(needG, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(lenG, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(G, lenG, MPI_DOUBLE_PRECISION, 0, comm(mycid), ierr)
   call mpi_bcast(lencu, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(leniu, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(lenru, 1, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(cu, lencu, MPI_CHARACTER, 0, comm(mycid), ierr)
   call mpi_bcast(iu, leniu, MPI_INTEGER, 0, comm(mycid), ierr)
   call mpi_bcast(ru, lenru, MPI_DOUBLE_PRECISION, 0, comm(mycid),ierr)

#if 0
   stat = -2
   return
#endif

   !-- Exit if this is the last call to usrfun
   if (stat >= 2) return

   needobj = .true.
   if (needf == 0) needobj = .false.
   needgrad = .true.
   if (needG == 0) needgrad = .false.

   !-- Evaluate objective
   call evalf(nvar, dv, nF, f, lenG, G, needobj, needgrad, &
              stat, .false., useDatabase)

   call mpi_barrier(comm(mycid), ierr)

end subroutine
!
! ===================================================================
!
subroutine getLU(jj, Xso, XsoRev, nXso, lowV, uppV, lenV, Xdv)
!
! -------------------------------------------------------------------
!
! Purpose:
!    Calculates upper and lower bounds for a design variable,
!    within the linear feasible region.
!
! -------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   integer :: jj, nXso, lenV
   integer :: Xso(nXso), XsoRev(nXso)
   real(kind=dp) :: lowV(lenV), uppV(lenV), Xdv(nXso)

   !-- local variables
   integer :: ii, kk, mm, nlce, jje
   integer :: lfarr(nnzLinG), lvarr(nnzLinG)
   real(kind=dp) :: temp
   logical :: relevant_lin_con

! _____________________
! begin main execution

   lowV(:) = Xlow(Xso(jj))   
   uppV(:) = Xupp(Xso(jj))

   !-- This saves time
   if (Xlow(Xso(jj)) == Xupp(Xso(jj))) then
      return
   endif

   if (nnzLinG == 1 .and. LinG(1) == 0.0d0) then
      return
   endif

   do ii = 1, nFnc

      relevant_lin_con = .false.
      nlce = 0

      do kk = 1, nnzLinG

         if (iLfun(kk) == ii) then

            if (XsoRev(jLvar(kk)) .lt. jj) then

               nlce = nlce + 1
               lfarr(nlce) = kk
               lvarr(nlce) = kk

            elseif (XsoRev(jLvar(kk)) == jj) then

               relevant_lin_con = .true.
               jje = kk

            else

               relevant_lin_con = .false.
               !-- This should skip to the next 'ii'
               exit

            endif

         endif

      end do

      !-- Now calculate lowV, uppV entries
      if (relevant_lin_con) then

         lowV(ii) = Flow(ii)
         uppV(ii) = Fupp(ii)

         do mm = 1, nlce

            lowV(ii) = lowV(ii) - &
                       linG(lfarr(mm)) * Xdv(jLvar(lvarr(mm)))
            uppV(ii) = uppV(ii) - &
                       linG(lfarr(mm)) * Xdv(jLvar(lvarr(mm)))

         end do

         if (linG(jje) .gt. 0.0d0) then
            lowV(ii) = lowV(ii) / linG(jje)
            uppV(ii) = uppV(ii) / linG(jje)
         else
            temp = lowV(ii)
            lowV(ii) = uppV(ii) / linG(jje)
            uppV(ii) = temp / linG(jje)
         endif

      endif

   end do

end subroutine
!
! =====================================================================
!
end module Hybropt_Mod

