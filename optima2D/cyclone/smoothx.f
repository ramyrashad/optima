      subroutine smoothx(jdim,kdim,q,s,xyj,spect,dd)                    
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension spect(jdim,kdim),dd(jdim,kdim)
c                                                                       
c     fourth order smoothing, added explicitly to rhs                     
c     third order at fringes                                              
c                                                                       
      smudt = smu*dt /(1. + phidt)                                      
c                                                                       
c     first unscale jacobian from q                                        
c                                                                       
      do 1 n = 1,4                                                      
      do 1 k = kbegin,kend                                              
      do 1 j = jbegin,jend                                              
         q(j,k,n) = q(j,k,n)*xyj(j,k)                                      
1     continue                                                          
c                                                                       
c     xi direction smoothing                                           
c                                                                       
      if ( .not.periodic ) then                                     
c     nonperiodic                                                          
         do 300 n=1,4                                                      
c            second order difference                                        
             do 270 k = klow,kup                                           
               do 250 j = jlow,jup                                           
                  dd(j,k) = q(j+1,k,n) - 2.*q(j,k,n) + q(j-1,k,n)            
250            continue                                                      
               dd(jbegin,k) = dd(jlow,k)                                     
               dd(jend,k) = dd(jup,k)                                        
c                                                                            
               do 290 j = jlow,jup                                         
               s(j,k,n) = s(j,k,n)                                         
     *         - smudt*( dd(j+1,k) - 2.* dd(j,k) + dd(j-1,k))*spect(j,k)   
  290          continue                                                    
270          continue                                                       
300      continue                                                       
      else                                                           
c        periodic                                                           
         do 305 n=1,4                                                      
c           second difference                                               
            do 295 j=jlow,jup                                           
               jp1 = jplus(j)                                              
               jm1 = jminus(j)                                             
               do 296 k = klow,kup                                       
                 dd(j,k) = q(jp1,k,n) - 2.*q(j,k,n) + q(jm1,k,n)           
296            continue                                                  
295         continue                                                    
c        
c           fourth difference                                                
               do 297 j=jlow,jup                                           
                 jp1 = jplus(j)                                              
                 jm1 = jminus(j)                                             
                 do 298 k = klow,kup                                       
                    s(j,k,n) = s(j,k,n)                                   
     *              - smudt*(dd(jp1,k)-2.*dd(j,k)+dd(jm1,k))*spect(j,k) 
298              continue                                                  
297            continue                                                    
305      continue                                                        
      endif                                                          
c                                                                       
c     put back in jacobian scaling on q                                    
      do 700 n = 1,4                                                    
      do 700 k = kbegin,kend                                            
      do 700 j = jbegin,jend                                            
         q(j,k,n) = q(j,k,n)/xyj(j,k)                                      
700   continue                                                          
c                                                                       
      return                                                            
      end                                                               
