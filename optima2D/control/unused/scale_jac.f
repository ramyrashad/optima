c----------------------------------------------------------------------
c     -- jacobian scaling for b.c. blocks --

c     written by: marian nemec
c     date: july 2000
c----------------------------------------------------------------------
      subroutine scale_jac (jdim, kdim, index, iat, jat, ast, ipt, jpt,
     &      pat, dOdQ, diag)

#include "../include/arcom.inc"

      integer jdim, kdim, len, j, jj, k, n
      integer index(jdim,kdim), iat(jdim*kdim*4+1)
      integer jat(jdim*kdim*144), jpt(jdim*kdim*80), ipt(jdim*kdim*4+1)
      integer idiag(maxjk*4)

      double precision ast(jdim*kdim*144), pat(jdim*kdim*4*20)
      double precision dOdQ(jdim*kdim*4), diag(jdim*kdim*4)

      na = jdim*kdim*4

      call getdia (na,na,0,ast,jat,iat,len,diag,idiag,0)

c     -- check for zeros on diagonal of jacobian --
      do j = 1,jend
        do k = 1,kend
          do n = 1,4
            jj = ( index(j,k) - 1 )*4 + n
            if ( dabs(diag(jj)) .lt. 1.d-10 ) then
              write (*,10) j, k, n, jj, diag(jj)
              stop
            end if
          end do
        end do
      end do

c     -- scale jacobians at b.c. blocks --
c     -- 1. airfoil body --
      k = kbegin
      do j = jtail1, jtail2
        do n = 1,4
          jk = ( index(j,k) - 1 )*4 + n
          dv = 1.d0/diag(jk)
c     -- adjoint rhs --
          dOdQ(jk) = dOdQ(jk)*dv
c     -- adjoint lhs --
          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do
        end do
      end do
c     -- 2. far-field --
      k = kend
      do j = jbegin+1,jend-1
        do n=1,4
          jk = ( index(j,k) - 1 )*4 + n
          dv = 1.d0/diag(jk)
c     -- test alberto --
          dOdQ(jk) = dOdQ(jk)*dv
c--------
          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do 
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do   
        end do
      end do  
      j = jbegin
      do k = 1,kend
        do n = 1,4
          jk = ( index(j,k) - 1 )*4 + n
          dv = 1.d0/diag(jk)
c     -- test alberto --
          dOdQ(jk) = dOdQ(jk)*dv
c--------
          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do 
        end do
      end do
      j = jend
      do k = 1,kend
        do n = 1,4
          jk = ( index(j,k) - 1 )*4 + n
          dv = 1.d0/diag(jk)
c     -- test alberto --
          dOdQ(jk) = dOdQ(jk)*dv
c--------
          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do 
        end do
      end do
c     -- 3. wake-cut --
      k = kbegin
      n = 4
      do j = jbegin+1,jtail1-1
c        do n = 1,4
          jk = ( index(j,k) - 1 )*4 + n
          dv = 1.d0/diag(jk)
c     -- test alberto --
          dOdQ(jk) = dOdQ(jk)*dv
c--------
          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do 
c        end do
      end do
      n = 4
      do j = jtail2+1,jend-1
c        do n = 1,4
          jk = ( index(j,k) - 1 )*4 + n
          dv = 1.d0/diag(jk)
c     -- test alberto --
          dOdQ(jk) = dOdQ(jk)*dv
c--------
          jb = iat(jk)
          je = iat(jk+1) - 1
          jpb = ipt(jk)
          jpe = ipt(jk+1) - 1
          do ii = jb,je
            ast(ii) = ast(ii)*dv
          end do
          do ii = jpb,jpe
            pat(ii) = pat(ii)*dv
          end do 
c        end do
      end do

 10   format ('SCALE_JAC: ',3i5,i8,e12.4)

      return
      end                       !scale_jac
