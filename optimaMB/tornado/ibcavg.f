c     ------------------------------------------------------------------
c     -- average boundary condition, implicit side --
c     -- bctype = 5, side = 1 or 3 --
c     -- m. nemec, aug. 2001
c     ------------------------------------------------------------------
      subroutine ibcavg( is1, is2, indx, xyj, q, jmax, kmax, nmax, xyj2,
     &     q2, jmax2, kmax2, jb, je, ks1, ke1, ks2, ke2, pa, as, icol,
     &     gami, precmat, jacmat)

      implicit none

      integer is1,is2,jmax,kmax,nmax,jmax2,kmax2,jb,je,ks1,ke1,ks2,ke2 
      integer k1,k2,k3,if1,if2,j,jn,n,jk,jj
      double precision q(jmax,kmax,4),xyj(jmax,kmax),xyj2(jmax2,kmax2) 
      double precision q2(jmax2,kmax2,4),pa(*),as(*),rho,u,v,gami
      integer indx(jmax,kmax),icol(*)
      logical precmat,jacmat

c     -- logic to treat c- and h-grids --
      if ( is1.eq.1 ) then
         k1 = ks1
         k2 = ks1 + 1
      else                      ! is1=3
         k1 = ke1
         k2 = ke1 - 1 
      end if      

      if ( is2.eq.1 ) then
         k3 = ks2 + 1
      else                      ! is2=3 
         k3 = ke2 - 1
      end if

      if ( is1.eq.is2 ) then
         if1 = 1
         if2 = -1
      else
         if1 = 0
         if2 = 1
      end if     


      if (precmat) then
         do j = jb,je

            jn  = if1*(jmax2 + 1) + if2*j

            do n = 1,3
               jk = ( indx(j,k1) - 1 )*nmax + n
               jj = (jk-1)*icol(5)

               pa(jj+        n) =   1.0*xyj(j,k1)
               pa(jj+icol(1)+n) = - 0.5*xyj(j,k2) 
               pa(jj+icol(2)+n) = - 0.5*xyj2(jn,k3)
            end do
            
c     -- average pressure --
            jk = ( indx(j,k1) - 1 )*nmax + 4
            jj = ( jk-1 )*icol(5)

c     -- matrix for j,k1 --
            rho = q(j,k1,1)*xyj(j,k1)
            u   = q(j,k1,2)/q(j,k1,1)
            v   = q(j,k1,3)/q(j,k1,1)

            pa(jj+1) = 0.5*(u*u+v*v)*gami*xyj(j,k1)
            pa(jj+2) = -u*gami*xyj(j,k1)
            pa(jj+3) = -v*gami*xyj(j,k1)
            pa(jj+4) = gami*xyj(j,k1)

c     -- matrix for j,k2 --
            rho = q(j,k2,1)*xyj(j,k2)
            u   = q(j,k2,2)/q(j,k2,1)
            v   = q(j,k2,3)/q(j,k2,1)
            
            pa(jj+icol(1)+1) = -0.25*(u*u+v*v)*gami*xyj(j,k2)
            pa(jj+icol(1)+2) = -0.5*(-u)*gami*xyj(j,k2)
            pa(jj+icol(1)+3) = -0.5*(-v)*gami*xyj(j,k2)
            pa(jj+icol(1)+4) = -0.5*gami*xyj(j,k2)

c     -- matrix for jn,k3 --
            rho = q2(jn,k3,1)*xyj2(jn,k3)
            u   = q2(jn,k3,2)/q2(jn,k3,1)
            v   = q2(jn,k3,3)/q2(jn,k3,1)
            
            pa(jj+icol(2)+1) = -0.25*(u*u+v*v)*gami*xyj2(jn,k3)
            pa(jj+icol(2)+2) = -0.5*(-u)*gami*xyj2(jn,k3)
            pa(jj+icol(2)+3) = -0.5*(-v)*gami*xyj2(jn,k3)
            pa(jj+icol(2)+4) = -0.5*gami*xyj2(jn,k3)

            if (nmax.eq.5) then
               n  = 5
               jk = ( indx(j,k1) - 1 )*5 + n
               jj = ( jk - 1 )*icol(5)
               pa(jj+        n) =   1.0*xyj(j,k1)
               pa(jj+icol(1)+n) = - 0.5*xyj(j,k2)
               pa(jj+icol(2)+n) = - 0.5*xyj2(jn,k3)
            end if
         end do
      end if

      if (jacmat) then
         do j = jb,je

            jn  = if1*(jmax2 + 1) + if2*j
            
            do n = 1,3
               jk = ( indx(j,k1) - 1 )*nmax + n
               jj = (jk-1)*icol(9)

               as(jj+        n) =   1.0*xyj(j,k1)
               as(jj+icol(1)+n) = - 0.5*xyj(j,k2) 
               as(jj+icol(2)+n) = - 0.5*xyj2(jn,k3)
            end do
            
c     -- average pressure --
            jk = ( indx(j,k1) - 1 )*nmax + 4
            jj = ( jk-1 )*icol(9)

c     -- matrix for j,k1 --
            rho = q(j,k1,1)*xyj(j,k1)
            u   = q(j,k1,2)/q(j,k1,1)
            v   = q(j,k1,3)/q(j,k1,1)

            as(jj+1) = 0.5*(u*u+v*v)*gami*xyj(j,k1)
            as(jj+2) = -u*gami*xyj(j,k1)
            as(jj+3) = -v*gami*xyj(j,k1)
            as(jj+4) = gami*xyj(j,k1)

c     -- matrix for j,k2 --
            rho = q(j,k2,1)*xyj(j,k2)
            u   = q(j,k2,2)/q(j,k2,1)
            v   = q(j,k2,3)/q(j,k2,1)
            
            as(jj+icol(1)+1) = -0.25*(u*u+v*v)*gami*xyj(j,k2)
            as(jj+icol(1)+2) = -0.5*(-u)*gami*xyj(j,k2)
            as(jj+icol(1)+3) = -0.5*(-v)*gami*xyj(j,k2)
            as(jj+icol(1)+4) = -0.5*gami*xyj(j,k2)

c     -- matrix for jn,k3 --
            rho = q2(jn,k3,1)*xyj2(jn,k3)
            u   = q2(jn,k3,2)/q2(jn,k3,1)
            v   = q2(jn,k3,3)/q2(jn,k3,1)
            
            as(jj+icol(2)+1) = -0.25*(u*u+v*v)*gami*xyj2(jn,k3)
            as(jj+icol(2)+2) = -0.5*(-u)*gami*xyj2(jn,k3)
            as(jj+icol(2)+3) = -0.5*(-v)*gami*xyj2(jn,k3)
            as(jj+icol(2)+4) = -0.5*gami*xyj2(jn,k3)

            if (nmax.eq.5) then
               n  = 5
               jk = ( indx(j,k1) - 1 )*5 + n
               jj = ( jk - 1 )*icol(9)
               as(jj+        n) =   1.0*xyj(j,k1)
               as(jj+icol(1)+n) = - 0.5*xyj(j,k2)
               as(jj+icol(2)+n) = - 0.5*xyj2(jn,k3)
            end if
         end do
      end if

      return 
      end                       !ibcavg
