#include <cmath>
#include <vector>
#include "comprow_double.h"
#include "ilupre_double.h"
#include "cg.h"
#include "Node.h"
#include "Face.h"
#include "gridfe.h"
#include "gridfe.h"

using namespace std;

extern "C" {
  void movegridmbfe_(char *filena, const int& namelen, double* x, 
	        double* y, const int* ibap, const int* lgptr, 
		const int& nblk, const int& nfoils, const int& nhalo);
}
// bap is the array of nodes on airfoil (has "wall" boundary condition)
// ibap is the array of number of nodes on each airfoil 
// nhalo is the halo (ghost) points


void movegridmbfe_(char *filena, const int& namelen, double* x, double* y, 
		 const int* ibap, const int* lgptr, const int& nblk,
		 const int& nfoils, const int& nhalo)
{

  int j;
  char *filename = new char[namelen+1];
  for (j=0; j<namelen; j++)
    filename[j] = filena[j];
  filename[namelen] = '\0';

  // declare and initialize objects
  int *jmax = new int [nblk];
  int *kmax = new int [nblk];

  // VARIABLES IN SOLVING MATRIX
  int num_inn_nodes(0), nonzeros (0);  
  double tol;
  
  for (j=0; j<nfoils; j++)
    nonzeros += ibap[j];   // total number of nodes on airfoil surface

  vector <Node> node_loc;
  vector <Node*> bnd_nodes(nonzeros);
  vector <Face> faces;
	
  // CONJUGATE GRADIENT METHOD CONVERGENCE PARAMETERS
  double tolerance = 1.0e-7;
  int maxit = 50000; 
	
  read_grid_mb (filename, node_loc,bnd_nodes, ibap, nfoils,
		nblk, jmax, kmax);

  nonzeros = 0;  // reset to zero to use to count the number of quads
  for (j=0; j<nblk; j++)
    nonzeros = nonzeros + (jmax[j]-1)*(kmax[j]-1);
  faces.resize(nonzeros);	
  form_faces (node_loc, faces, nblk, jmax, kmax);

  deform_airfoil (filename, bnd_nodes);
       
  // counts the number of nonzeros in the stiffness matrix	
  nonzeros = 2;   // 2 is passed in to indicate # of DOF
  get_nonzerosfe (node_loc, num_inn_nodes, nonzeros);

  double *stiff_val = new double [nonzeros];
  init_array(stiff_val, nonzeros);
  int *colind = new int [nonzeros];
  int *rowptr = new int [num_inn_nodes+1];
	
  j = 2;   // number of degrees of freedom, dimension of problem
  construct_stiffness_matrix (colind, rowptr, node_loc, j);

  //CompRow_Mat_double alpha(num_inn_nodes, num_inn_nodes, nonzeros, 
  //stiff_val, rowptr, colind);

  // Solution, RHS vectors  
  VECTOR_double du(num_inn_nodes, 0.0), F(num_inn_nodes,0.0);

  // initial element quality
  double* elemQo = new double[faces.size()];
  // initial mesh quality
  double Qomax;  
  get_ini_quality (faces, node_loc, elemQo, Qomax);

  tol = -1;
  //cout << "Forming stiffness matrix ... " << endl;
  form_stiff (rowptr, colind, stiff_val, node_loc, 
	      faces, F, elemQo, tol);
  //  form_stiff (alpha, node_loc, faces, F, elemQo, Qomax);
  CompRow_Mat_double alpha(num_inn_nodes, num_inn_nodes, nonzeros, 
			    stiff_val, rowptr, colind);

  // ***** START SOLVING ... *****	
  CompRow_ILUPreconditioner_double P(alpha);     // ILU preconditioner
	
  // Solve using the conjugate gradient method
  nonzeros = CG(alpha, du, F, P, maxit, tolerance);
  cout<< "CG flag:  " << nonzeros << "     ";
  cout<< "Iterations performed:  " << maxit << "     ";
  cout<< "Tolerance achieved:  " << tolerance << endl;	

  //cout << "Updating nodal locations ... " << endl;
  update (node_loc, du);
	
  delete [] stiff_val;   stiff_val = NULL;
  delete [] colind;      colind = NULL;
  delete [] rowptr;      rowptr = NULL;
  delete [] filename;    filename = NULL;
  
  write_gridMB (node_loc, nblk, jmax, kmax, x, y, nhalo, lgptr);
  write_plot3DMB(node_loc, nblk, jmax, kmax);

  delete [] jmax;
  delete [] kmax;

  // clear all dynamically allocated memories associated with object Node
  
  maxit = (int) node_loc.size();
  for (j=0; j<maxit; j++)
    node_loc[j].clearmem();
  maxit = (int) faces.size();
  for (j=0; j<maxit; j++)
    faces[j].clearmem();

  return;
}













