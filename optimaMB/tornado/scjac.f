c ********************************************************************
c **            routine to scale q by the jacobian                  **
c ********************************************************************
c
c calling routine:  initia
c
      subroutine scjac(iblk,jdim,kdim,q,xyj,jbegin,jend,kbegin,kend)

      implicit none
c
#include "../include/units.inc"

      integer iblk,jdim,kdim,jbegin,jend,kbegin,kend,j,k,n
      double precision q(jdim,kdim,4), xyj(jdim,kdim)
c
c
c     S.D 10/07/98
c     -for explanation to if statement within the loop see
c      note in initia.f
      do 50 n=1,4
      do 50 k=kbegin,kend
      do 50 j=jbegin,jend
        if (xyj(j,k).eq.0.) then
          write(n_out,*) 
          write(n_out,*) 
          write(n_out,10) iblk,j,k
          write(n_out,11)
          write(n_out,12)
          xyj(j,k)=1.d4
        endif
        q(j,k,n) = q(j,k,n)/xyj(j,k)
   50 continue
 10   format(43hWARNING : Jacobian equal to zero ... block=,i4,/39x,
     &           4hj,k=,2i4)
 11   format(53hOccurs when nhalo=2 and you have blunt trailing edge.)
 12   format(45hSetting value to 10^4 to avoid fpe in scjac.f)
c
      return
      end
