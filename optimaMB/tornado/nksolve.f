c     ------------------------------------------------------------------
c     -- newton-krylov solver --
c     -- loosely based on "probe" by a. pueyo --
c     -- m.nemec, aug. 2001 --
c     ------------------------------------------------------------------
      subroutine nksolve( nmax, ntvar, q, qps, qos, xy, xyj, x, y, 
     &     turmu, vort, turre, turps, turos, fmu, precon, 
     &     press, sndsp, xit, ett, ds, vk, ve, coef2x,
     &     coef4x, coef2y, coef4y, s, uu,vv, ccx, ccy, spectxi,
     &     specteta, tmp, indx, ipa, jpa, ia,ja, icol)

c     TTTTTTT  OOOO  RRRRR  N    N   AA   DDDD    OOOO
c        T    O    O R    R NN   N  A  A  D   D  O    O
c        T    O    O RRRRR  N N  N A    A D    D O    O
c        T    O    O R  R   N  N N AAAAAA D    D O    O
c        T    O    O R   R  N   NN A    A D   D  O    O
c        T     OOOO  R    R N    N A    A DDDD    OOOO
c
c
c     An Implicit Central-differenced Two-Dimensional
c     Multi-Block Navier-Stokes Code
c     T.E.Nelson & A.R.Wilkinson 1993
c     Modifications etc., by S. De Rango 1997-2000
c                            M. Nemec    2000-2002
c     
c     Loosely based on the code ARC2D
c     originally written by J. L. Steger (1976)
c     Modifications etc., by Barton  and Pulliam (1982-84)

      implicit none

#include "../include/parms.inc"
#include "../include/units.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"
#include "../include/optcom.inc"
#include "../include/optima.inc"

      double precision q(*),       press(*),     sndsp(*)
      double precision s(*),       xy(*),        xyj(*)
      double precision xit(*),     ett(*),       ds(*)
      double precision x(*),       y(*),         turmu(*)
      double precision vort(*),    turre(*),     fmu(*)
      double precision vk(*),      ve(*),        tmp(*)
      double precision uu(*),      vv(*),        ccx(*)
      double precision ccy(*),     coef2x(*),    coef2y(*)
      double precision coef4x(*),  coef4y(*),    precon(*)
      double precision spectxi(*), specteta(*),  qtmp

      integer indx(*),nmax,is,ntvar,iblk

cmt ~~~
c   qps is the subiteration variable Q^p
c   q is the solution at step Q^n
c   qos is solution at step Q^(n-1)

      double precision qps(*),qos(*),turps(*),turos(*),fresid
cmt ~~~
c   exprhs is the RHS summation term in ESDIRK-4 
c   dhatq is the RHS D(Q) term in ESDIRK-4
c   (See Sammy Isono's M.A.Sc. thesis eq.3-9 p.11)

      double precision exprhs(maxjkq),dhatq(6*maxjkq),rsa(maxjk)
      double precision exprsa(maxjk),dhatt(6*maxjk)
cmt ~~~       
      integer nk_numiter,nwtn,npts,nptstmp,jn,kn,mglev
      integer maxjstage,ix,iy,nscal 
      double precision dttemp,pdc,cptar(4*maxj)

c     ntvar: total number of unknows
c     -- sparse arrays --
c     -- as, ja, ia: second order flux jacobian --
c     -- pa, jpa, ipa: preconditioner --
c     -- wa, jwa, iwa: sparse work array --
      integer ia(*),             ja(*)
      integer ipa(*),            jpa(*)
      double precision, dimension(:),allocatable :: as,pa


c     -- restart stuff --
      integer strlen, namelen, lentmp
      character*60  command,filena_restart*40

c     -- gmres arrays --
      double precision rhs(maxjk*jacb), sol(maxjk*jacb)

c     -- icol is used for setup of sparse arrays --
      integer icol(*)

c     -- shuffle arrays required to keep track of row exchanges,
c     necessary for the adjoint problem but also used in the n-k flow
c     solver --
c     -- iex: integer array of row exchanges --
c     -- bcf: boundary condition factors --
      integer iex(maxjb,4,4,mxbfine)
      double precision bcf(maxjb,4,4,mxbfine)      

c     -- local temp. sparse adjoint arrays --
      integer,dimension(:),allocatable :: iatmp,ipatmp,jatmp,jpatmp

c     -- ILU arrays and temporary transposed arrays --
      integer iwk
      parameter (iwk = maxjk*jacb2*5*3)
      integer,dimension(:),allocatable :: jlu,ju,levs,jw,iat,jat
      double precision,dimension(:),allocatable :: alu,ast,wk     

c     -- gmres control --
      integer ipar(16)
      double precision fpar(16)

c     -- gmres work array --
      integer iwk_gmres
      parameter (iwk_gmres=maxjk*jacb*80)
      double precision,allocatable :: wk_gmres(:) 

c     -- time vector arrays --
      double precision dtvc(5*maxjk)
      integer dtt(5*maxjk)

      integer nkstop,nkstop2,lq,lg,jjj,ib,i,j,jj,n,ierr,ifirst,ip,junit

      logical precmat, jacmat, imps, dcheck, outmat

      double precision dtiseq(-5:20),dtmins(-5:20),dtspiseq(-5:20)
      double precision vlx(-5:20),vnx(-5:20),vly(-5:20),vny(-5:20)
      integer iends(-5:20),jacdtseq(-5:20),isbiter(-5:20,2)      
      integer isequal
      common/mesup/ dtiseq, dtmins,dtspiseq, vlx, vnx, vly, vny, 
     &        isequal, iends, jacdtseq, isbiter

      real*4 etime
      logical frozen, frechet
      double precision fra1(maxjk), fra2(maxjk) 
      common/freeze/ fra1, fra2, frozen

c$$$      integer len,idiag(ntvar),k,jk,index1,index2
c$$$      double precision diag(ntvar),rhs2(maxjk*jacb)


cmpr  Allocate some of the big arrays dynamically
      allocate(as(maxjk*jacb2*9),pa(maxjk*jacb2*5),wk_gmres(iwk_gmres))
      allocate(iatmp(maxjk*jacb+1),jatmp(maxjk*jacb2*9))
      allocate(ipatmp(maxjk*jacb+1),jpatmp(maxjk*jacb2*5))
      allocate(jlu(iwk),ju(maxjk*jacb),levs(iwk),jw(3*maxjk*jacb))
      allocate(alu(iwk),wk(maxjk*jacb),ast(maxjk*jacb2*9))
      allocate(iat(maxjk*jacb+1),jat(maxjk*jacb2*9))

      
      
      
      
      
 


c     -- open restart files in case the run gets killed --

      namelen =  strlen(restart_file_prefix)
      filena_restart = restart_file_prefix
      filena_restart(namelen+1:namelen+2)='k'
      namelen=namelen+1

      filena = filena_restart
      filena(namelen+1:namelen+3) = '.q'
      open(unit=n_qk,file=filena,status='unknown',form='unformatted')

      if (unsteady) then 
         filena = filena_restart
         filena(namelen+1:namelen+4) = '.q2'
         open(unit=n_q2k,file=filena,status='unknown',
     &        form='unformatted')
      end if

      if (turbulnt) then         
         filena = filena_restart
         filena(namelen+1:namelen+3) = '.t'
         open(unit=n_tk,file=filena,status='unknown',form
     &        ='unformatted')
         if (unsteady) then
            filena = filena_restart
            filena(namelen+1:namelen+4) = '.t2'
            open(unit=n_t2k,file=filena,status='unknown',form
     &           ='unformatted')
         end if
      end if

c     -- restart from last output if run got killed

      if (killed) then

         if (unsteady .and. IMARCH.eq.2) then

            read(n_q2k) nblks
            read(n_q2k) (jbmax(i),kbmax(i),i=1,nblks)
            do ib=1,nblks
               call readq(n_q2k,qos(lqptr(ib)),
     &              jmax(ib),kmax(ib),jminbnd(ib),jmaxbnd(ib),
     &              kminbnd(ib),kmaxbnd(ib))
            end do
            
            read(n_q2k) numiter
         
            rewind(n_q2k)
         
            if (viscous.and.turbulnt) then
               read(n_t2k) nblks
               read(n_t2k) (jbmax(i),kbmax(i),i=1,nblks)
               do ib=1,nblks
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  call readarr4(n_t2k,vk(lg),ve(lg),turos(lg),turmu(lg), 
     &                 jmax(ib),kmax(ib),jminbnd(ib),jmaxbnd(ib), 
     &                 kminbnd(ib),kmaxbnd(ib))
               end do
               rewind(n_t2k)
            end if

            call halo_q(1,nblks,maxj,maxk,qos,turos)

            do ib=1,nblks
               call scjac(i,jmax(i),kmax(i),qos(lqptr(i)),xyj(lgptr(i)),            
     &              jbegin2(i),jend2(i),kbegin2(i),kend2(i))
            end do

         end if  !unsteady


         read(n_qk) nblks
         read(n_qk) (jbmax(i),kbmax(i),i=1,nblks)
         do ib=1,nblks
            call readq(n_qk,q(lqptr(ib)),jmax(ib),kmax(ib),
     &           jminbnd(ib),jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib))
         end do
         
         read(n_qk) numiter
         
         rewind(n_qk)
         
         if (viscous.and.turbulnt) then
            read(n_tk) nblks
            read(n_tk) (jbmax(i),kbmax(i),i=1,nblks)
            do ib=1,nblks
               lq = lqptr(ib)
               lg = lgptr(ib)
               call readarr4(n_tk, vk(lg),ve(lg),turre(lg), turmu(lg), 
     &              jmax(ib),kmax(ib),jminbnd(ib), jmaxbnd(ib), 
     &              kminbnd(ib), kmaxbnd(ib))
            end do
            rewind(n_tk)
         end if
               
         call halo_q(1,nblks,maxj,maxk,q,turre)

         do i=1,nblks
            call scjac(i,jmax(i),kmax(i),q(lqptr(i)),xyj(lgptr(i)),
     &           jbegin2(i),jend2(i),kbegin2(i),kend2(i))
         end do

c        -- close output_file_prefix.his and open it again after the 
c           restart version got copied over

         close(n_his)

         namelen =  strlen(restart_file_prefix)
         filena_restart = restart_file_prefix
         filena_restart(namelen+1:namelen+2)='k'
         filena_restart(namelen+2:namelen+6) = '.his'
c     
         namelen =  strlen(output_file_prefix)
         filena = output_file_prefix
         filena(namelen+1:namelen+5) = '.his'
c     
         command = 'cp '
         namelen = strlen(filena_restart)
         command(4:3+namelen) = filena_restart

         lentmp = namelen+3+1
         namelen = strlen(filena)
         command(lentmp+1:lentmp+namelen) = filena
         call system(command)

         open(unit=n_his,file=filena,status='unknown',access='append')

         write(*,*) 'Restart flow solution at iteration',numiter-iend
         write(*,*) 

      end if   !killed


c     -- reset gmres tolerance --
      fpar(1) = 0.0d0
c     
c     -- copy pointer arrays --
      do j=1,maxjk*jacb+1
         iatmp(j) = ia(j)
         ipatmp(j) = ipa(j)
      end do
      do j = 1,maxjk*jacb2*9
         jatmp(j) = ja(j)
      end do
      do j = 1,maxjk*jacb2*5
         jpatmp(j) = jpa(j)
      end do

cmt ~~~ sets the Butcher coefficients of the ESDIRK-4
      call set_ajk

c     -- set flag to indicate if jacobian matrix is required --




      jacmat = .false.
c      jacmat = .true.

      frechet=.false.
c      frechet=.true.
      if (frechet) jacmat = .true.
      








      IF (unsteady) THEN

c     if run got killed, numiter is not zero and the flow solve
c     has to finish the rest of the desired steps
         if (killed) then
            killed = .false.
            nkstop=nkiends+nkskip-numiter+iend
         else
            numiter=iend
            nkstop=nkiends+nkskip
         end if

         nkstop2=nksubits
         dtsmall=dt2 
         IF(imarch.EQ.2)THEN 
            DO jjj=1,maxjkq
               qps(jjj)=2.d0*q(jjj)-qos(jjj)
            ENDDO
         ENDIF
         IF(imarch.EQ.4)THEN
c        Don't try any extrapolation here! This is in fact the first (explicit) stage of the ESDIRK4 method
            DO jjj=1,maxjkq
               qps(jjj)=q(jjj)
            ENDDO
         ENDIF

         if (nmax .eq. 5) then 
c        Don't try any extrapolation here! This is in fact the first (explicit) stage of the ESDIRK4 method
            do jjj=1,maxjk
               turps(jjj)=turre(jjj)
            end do
         end if

      ELSE

         nkstop=1
         nkstop2=nkits
         DO jjj=1,maxjkq
            qps(jjj)=q(jjj)
         ENDDO

         if (nmax .eq. 5) then 
            do jjj=1,maxjk
               turps(jjj)=turre(jjj)
            end do
         end if

      ENDIF

      IF (unsteady.AND.imarch.EQ.4) THEN

         maxjstage=6
c        save flow field at zeroth iteration if required for adjoint
         if ((outtime .or. igrad .eq. 1) .and. IOPTM .eq. 6 )  then    
            call iomarkus(1,0,maxj,maxk,q,xyj,turre)
         endif

      ELSE

         maxjstage=1

      ENDIF

cmt   +----------------------------------+
cmt   |+--------------------------------+|
cmt   ||  physical loop (do 10) begins  ||
cmt   || ============================== ||

      do 10 nk_numiter = 1,nkstop

         IF(unsteady)THEN 
            numiter=numiter+1
            if (numiter .le. iend+nkskip ) then
               dt2=dtbig
            else
               dt2=dtsmall
            end if
         ENDIF

c     -- halo column copy for qps vector --
         call halo_q(1,nblks,maxj,maxk,qps,turps)

         do ib=1,nblks
            lq = lqptr(ib)
            lg = lgptr(ib)
            call calcps(ib,jmax(ib),kmax(ib),qps(lq),press(lg),
     &           sndsp(lg),precon(lprecptr(ib)),xy(lq),xyj(lg),
     &           jbegin2(ib),jend2(ib),kbegin2(ib),kend2(ib))
         end do
cmt   +-------------------------------------+
cmt   |+-----------------------------------+|
cmt   ||  Runge-Kutta loop (do 11) begins  ||
cmt   || ================================= ||
         DO 11 jstage = 1,maxjstage

c            write(*,*)"--- stage ---",jstage

            IF (unsteady.AND.imarch.EQ.4) THEN 
               DO ib=1,nblks
                  CALL calc_exprhs(jmax(ib),kmax(ib),nmax,
     &                 jlow(ib),jup(ib),klow(ib),kup(ib),
     &                 dhatq(ldhatqptr(ib)),exprhs(lqptr(ib)),
     &                 dhatt(ldhattptr(ib)),exprsa(lgptr(ib)))
               ENDDO
            ENDIF

cmt   +-----------------------------------+
cmt   |+---------------------------------+|
cmt   ||  subiteration loop (do) begins  ||
cmt   || =============================== ||


            do nwtn = 1,nkstop2

               if (.not.unsteady) numiter=numiter+1

c     -- set flag to indicate if preconditioner matrix is required --

               IF(nwtn.LE.nkpfrz .OR. frechet) THEN
                  precmat=.TRUE.
               ELSE
                  precmat=.FALSE.
               ENDIF
             
               tt=etime(tarray)
               time1=tarray(1)
               time2=tarray(2)
        
c     -- residual evaluation: -R(Q) stored in S --

               IF (unsteady.AND.imarch.EQ.4.AND.jstage.EQ.1) THEN

c                  write(*,*)"entered 2nd get_rhs in nksolve with true"
                  call get_rhs(qps,q,qos,press,sndsp,s,xy,xyj,xit,ett,
     &                 ds, x, y, turmu, vort, turps, turre, turos,fmu, 
     &                 vk,ve, tmp, uu, vv, ccx, ccy,
     &                 coef2x, coef2y, coef4x, coef4y,precon, spectxi, 
     &                 specteta,rsa,exprhs,dhatq,exprsa,dhatt,.true.)
c           write(*,*)"finished 2nd get_rhs in nksolve for jstage=1"

                  GOTO 11

               ELSE

c                  write(*,*)"entered 1st get_rhs in nksolve with false"
                  call get_rhs(qps,q,qos,press,sndsp,s,xy,xyj,xit,ett,
     &                 ds, x, y, turmu, vort, turps, turre, turos,fmu, 
     &                 vk,ve, tmp, uu, vv, ccx, ccy,
     &                 coef2x, coef2y, coef4x, coef4y,precon, spectxi, 
     &                 specteta,rsa,exprhs,dhatq,exprsa,dhatt,.false.)
c                  write(*,*)"finished 1st get_rhs in nksolve with false"

               ENDIF
c     -- compute residual --
               if (turbulnt) then
                  nres = 5
               else 
                  nres = 1
               end if

c     -- total residual --
               resid   = 0.0d0
c     -- s-a residual --
               tresid  = 0.0d0
c     -- maximum density residual --
               residmx = 0.0d0
c     -- maximum s-a residual --
               tresmx  = 0.0d0
               npts    = 0

               dttemp=1.d0

c     -- compute l2 and max norms of residual --          
               do ib = 1,nblks
                  lq = lqptr(ib)
                  lg = lgptr(ib)

                  nptstmp = (jlow(ib)-jup(ib)+1)*(klow(ib)-kup(ib)+1)
                  npts = npts + nptstmp

                  call residl2(jmax(ib),kmax(ib),ib,s(lq),rsa(lg), 
     &                 resid,residmx,maxres,tresid,tresmx,mxtur, 
     &                 nres,dttemp,jlow(ib),jup(ib),
     &                 klow(ib),kup(ib),nhalo)

                  
c$$$                  call residl2(jmax(ib),kmax(ib),ib,s(lq),rsa(lg), 
c$$$     &                 resid,residmx,maxres,tresid,tresmx,mxtur, 
c$$$     &                 nres,dttemp,jminbnd(ib),jmaxbnd(ib),
c$$$     &                 kminbnd(ib),kmaxbnd(ib),nhalo)
               end do

       
               fresid = sqrt( resid/float(npts) ) 

               if (nmax.eq.5) resid = resid + tresid 

               resid = sqrt( resid/float(npts) )
               
               tresid = sqrt( tresid/float(npts) )




c               print *, numiter,nwtn,fresid,tresid




c     -- check convergence

               if (resid .gt. 10000.d0) then
                  print *, 'Diverged in NK',numiter,nwtn,resid
                  stop
               end if

               if (unsteady) then
                  !if code converged goto 100
                  if ( resid.lt.dualres .and. nwtn .ge. 2) goto 100 

               else             !steady
                  nscal=0
                  call clcdmb(qps,press,x,y,xy,xyj,nscal,.false.)

c     -- output l2 residual to n_out --
                  if ( mod(nwtn,2).eq.0 ) then
                     write (n_out,50) nwtn-1, resid, totime1, 
     &                    fpar(1), fpar(12),
     &                    ipar(7)
                     call flush(n_out)
                  end if
 50               format (i8,e12.4,3f10.3,i6)

c     -- output residual to .his file -- 
                  write(n_his,60) numiter, totime1, resid, 
     &                 maxres(3), maxres(1),
     &                 maxres(2), residmx, clt, cdt, omega
 60               format(i5, e12.4, e13.5, 3i4,e10.2,e16.8,e16.8,e16.8)
                  call flush(n_his)

c     -- output l2 turbulence residual to n_this --
                  if (nmax.eq.5) then
                     tresid = sqrt( tresid/float(npts) )
                     write (n_this,70) numiter, tresid, 
     &                    mxtur(3), mxtur(1),
     &                    mxtur(2), tresmx
                     call flush(n_this)
                  end if
 70               format(i7,1x,e15.8,3i6,1x,e15.8)
                  !if code converged goto 100
                  if ( resid.lt.fsres .and. nwtn .ge. 2) goto 100 

               end if

               if ( precmat .or. jacmat ) then
c     -- form LHS: preconditioner and Jacobian -- 
c     -- arrays ia, ja, as store O(2) Jacobian --
c     -- arrays ipa, jpa, pa store O(1) preconditioner --
                  pdc = pdcnk
                  imps=.true.
                  call get_lhs (nmax,qps,press,sndsp,xy,xyj, 
     &                 precon,xit,ett,fmu,x,y,turmu,vort,turps, 
     &                 vk, ve,coef4x,coef4y,uu,vv,ccx,ccy, 
     &                 coef2x,coef2y,indx, pdc,pa,ipa,as,ia,icol, 
     &                 iex,bcf,ntvar,precmat,jacmat,imps) 

                  if (.not. unsteady .and. jacmat) then
c     -- order jacobian array --
                     call csrcsc (ntvar, 1, 1, as, ja, ia, ast,jat,iat )
                     call csrcsc (ntvar, 1, 1, ast, jat, iat, as,ja,ia)
c                     if (nki.eq.1) write (out_unit,20) ia(na+1) - ia(1)
c  20                 format (3x,'Non-zeros in Jacobian matrix:',i8)
                  end if
c     
                  if (.not. unsteady .and. precmat) then
c     -- order preconditioner array --
                     call csrcsc (ntvar, 1, 1, pa, jpa, ipa,ast,jat,iat)
                     call csrcsc (ntvar, 1, 1, ast, jat, iat,pa,jpa,ipa)
                  end if

c     -- check diagonal of pa for zeroes --
                  dcheck = .false.
                  if (precmat .and. dcheck) then
                     write (*,*) 'Checking diagonal ...'
                     do ib=1,nblks
                        call checkd(ib,jmax(ib),kmax(ib),nmax,
     &                       indx(lgptr(ib)),ipa,jpa,pa,jminbnd(ib),
     &                       jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib))
                     end do
                  end if

c     -- output lhs and rhs for a selected node --
                  outmat = .false.
                  if (precmat .and. outmat) then
                     iblk = 1
                     jn = 3
                     kn = 4
                     call wrmat(jn,kn,jmax(iblk),kmax(iblk),nmax,
     &                    indx(lgptr(iblk)),ipa,jpa,pa,
     &                    s(lqptr(iblk)), rsa(lgptr(iblk)))
                     stop 'wrote matrix to file'
                  end if

c     -- diagonal scaling for all b.c. equations --
c     -- this is done here in order to reuse code for optimization --
c     -- applies to airfoil body and farfield --

                  do ib=1,nblks
                     do is=1,4
                      if (ibctype(ib,is).gt.0 .and. ibctype(ib,is).lt.5)
     &                   call sdirect (ib, is,jmax(ib),kmax(ib),nmax,
     &                   kminbnd(ib),kmaxbnd(ib),jminbnd(ib),
     &                   jmaxbnd(ib),precmat,jacmat,iex,bcf,
     &                   indx(lgptr(ib)),s(lqptr(ib)),pa,as,ipa,ia,
     &                   maxjb,mxbfine)
                     end do
                  end do 

               else
c     -- matrix free: setup -R(Q) b.c. scaling --
c     -- initialize row index to keep track of row shuffling --
                  do ib = 1,nblks
                     do is = 1,4
                        do n = 1,4
                           do j = 1,maxjb
                              iex(j,n,is,ib) = n
                              bcf(j,n,is,ib) = 1.0
                           end do
                        end do
                     end do
                  end do

                  imps = .false.
                  call impbc( imps, nmax, indx, qps, xy, xyj, press,
     &                 pa, as, icol, bcf, iex, precmat, jacmat)

c     -- reset row index for row shuffling for singular points--
                  do i = 1,nsing
                     do n = 1,4
                        iex(js1(i),n,1,ibs1(i)) = n
                        iex(js2(i),n,3,ibs2(i)) = n
                        bcf(js1(i),n,1,ibs1(i)) = 1.0
                        bcf(js2(i),n,3,ibs1(i)) = 1.0
                     end do
                  end do

c     -- diagonal scaling for all b.c. equations --
                  do ib=1,nblks
                     do is=1,4
                        if (ibctype(ib,is).gt.0.and.ibctype(ib,is).lt.5)
     &                       call sdirect (ib,is,jmax(ib),kmax(ib),nmax,
     &                       kminbnd(ib), kmaxbnd(ib), jminbnd(ib),
     &                       jmaxbnd(ib), precmat, jacmat, iex, bcf,
     &                       indx(lgptr(ib)),s(lqptr(ib)),pa,as,ipa,ia,
     &                       maxjb, mxbfine) 
                     end do
                  end do
            
               end if


c     -- Add implicit Euler time step if desired --
               do i=1,ntvar
                  dtvc(i) = 0.0d0
               end do

               if (.not.frechet .and. (
     &            (.not.unsteady.and.(resid.gt.afres.or.nwtn.eq.1)).or.  
     &            (     unsteady.and.resid.gt.1.d-2) ) ) then 

                  do ib = 1,nblks
                     lg = lgptr(ib)
                     lq = lqptr(ib)
                     call timevec (jmax(ib),kmax(ib),nmax,klow(ib),
     &                    kup(ib),jlow(ib),jup(ib),xyj(lg),indx(lg),
     &                    turps(lg),rsa(lg),ipa,jpa,pa,nwtn,resid,
     &                    dtvc)
                  end do
                  if (precmat) then
                     call apldia(ntvar,0,pa,jpa,ipa,dtvc,pa,jpa,ipa,dtt) 
                  end if
                  if (jacmat) then
                     call apldia (ntvar,0,as,ja,ia,dtvc,as,ja,ia,dtt)
                  end if

               end if

c     -- solve --
c     -- set rhs for gmres --
               do ib = 1,nblks
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  call setrhs( jmax(ib), kmax(ib), nmax, kminbnd(ib),
     &                 kmaxbnd(ib),jminbnd(ib),jmaxbnd(ib), indx(lg),
     &                 s(lq), rsa(lg), rhs, .true.)
               end do

c     -- sparskit iluk(p) preconditioner --
               if (.not.frechet) then
                  ierr = 0
                  if (precmat) call iluk (ntvar,pa,jpa,ipa,nklfil,alu,
     &                 jlu,ju, levs, iwk, wk, jw, ierr)

                  if (ierr.ne.0) then
                     write (*,*) 'nksolve: ilu problem, ierr=',ierr
                     stop
                  end if
               end if

c     -- GMRES --
               ipar(1) = 0
               ipar(2) = 2
               ipar(3) = 1
               ipar(4) = iwk_gmres
               ipar(5) = nkimg
               ipar(6) = nkitg

               fpar(2) = 1.e-15
               fpar(11) = 0.0d0

c     -- flag to indicate the first iteration of GMRES, see framux --
               ifirst = 1

c     -- inner iterations convergence tolerance --
               IF (.not. unsteady) THEN
 
                  if (turbulnt.and.iends(1).eq.0) then
                     if (fsmach.lt.0.4d0 .and. alpha.lt. 2.0d0) then
                        if (nwtn.le.15) then
                           fpar(1) = 1.d-3
                        else
                           fpar(1) = 1.d-1
                        end if
                     else
                        if (nwtn.le.30) then
                           fpar(1) = 1.d-3
                        else
                           fpar(1) = 1.d-3
                        end if            
                     end if
                  else
                     if (nwtn.le.10) then
                        fpar(1) = 1.d-2
                     else
                        fpar(1) = 1.d-1
                     end if
                  end if 

               ELSE   ! it's unsteady

                  if (turbulnt) then
                     fpar(1) = 1.d-3
                  else
                     fpar(1) = 1.d-1
                  end if

               END IF


cmt ~~~
c     this part is added in order to check the preconditioner problems
c     this is a debugging tool, so use it only if you have a bad feeling 
c     about the preconditioner, such as slow convergence of linear system
               if (frechet) then
                  call fr_test(ifirst,ntvar,nmax,q,qps,qos,press,sndsp,
     &              s,rsa,xy,xyj,xit,ett,ds,x,y,turmu,vort,turre,
     &              turps,turos,fmu,vk,ve,tmp, uu,vv,ccx,ccy,
     &              coef2x,coef2y,coef4x,coef4y,precon,spectxi,
     &              specteta,pa,jpa,ipa,indx,iex,icol,bcf,exprhs,
     &              dhatq,exprsa,dhatt,dtvc, as, ja, ia)
               end if
cmt ~~~         

c               write(*,*)"entered run_gmres"
               call run_gmres( ifirst, nmax, ntvar, jacmat,q,qps,qos,
     &              press,sndsp,s,rsa,xy,xyj,xit,ett,ds,x,y,turmu,  
     &              vort,turre,turps,turos, fmu, vk, ve, tmp, uu, vv, 
     &              ccx, ccy, coef2x, coef2y, coef4x, coef4y, precon, 
     &              spectxi, specteta, indx, wk_gmres, alu,
     &              jlu, ju, as, ja, ia, rhs,sol, iex, bcf, 
     &              ipar, fpar, 0, 0,exprhs,dhatq,exprsa,dhatt,dtvc)
c               write(*,*)"finished run_gmres"

c     -- update solution --
c               write(*,*)"entered qupdate"
               do ib=1,nblks
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  call qupdate( ib, jmax(ib), kmax(ib), nmax, qps(lq),
     &                 indx(lg), turps(lg), xyj(lg), sol, jminbnd(ib),  
     &                 jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib))
               end do
c               write(*,*) 'finished qupdate'

c     -- halo column copy for qps vector --
               call halo_q(1,nblks,maxj,maxk,qps,turps)

c     -- calculate pressure and sound speed --
               do ib=1,nblks
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  call calcps(ib,jmax(ib),kmax(ib),qps(lq),press(lg),
     &               sndsp(lg),precon(lprecptr(ib)),xy(lq),xyj(lg),
     &               jbegin2(ib),jend2(ib),kbegin2(ib),kend2(ib))
               end do

c     -- timing results (no I/O) --
c     -- totime1: user time --
c     -- totime2: user + system time --
               tt=etime(tarray)
               time1=tarray(1)-time1
               time2=tarray(2)-time2
               totime1=totime1 + time1
               totime2=totime2 + time1 + time2

c     -- restore csr pointer arrays -- 
               do j=1,maxjk*jacb+1
                  ia(j) = iatmp(j)
                  ipa(j) = ipatmp(j) 
               end do
               do j = 1,maxjk*jacb2*9
                  ja(j) = jatmp(j)
               end do
               do j = 1,maxjk*jacb2*5
                  jpa(j) = jpatmp(j) 
               end do
               
            end do              !end of inner newton loop

cmt   || ============================= ||
cmt   ||  subiteration loop (do) ends  ||
cmt   |+-------------------------------+|
cmt   +---------------------------------+

            if (unsteady) then
               if (resid.gt.DUALRES) print *, 'Not converged',numiter,
     &              resid
c              if (resid.gt. 1000.d0*DUALRES)  stop
            else
               if (resid.gt.FSRES) print *, 'Not converged', numiter,
     &              resid
c               if (resid.gt. 100.d0*FSRES)  stop
            end if

 100     continue

c     mt ~~~ call get_rhs with getdhatq = true
         IF(unsteady .and. imarch.EQ.4 .and. jstage.le.5) THEN

c            write(*,*)"entered 3rd get_rhs in nksolve with true"
            call get_rhs(qps,q,qos,press,sndsp,s,xy,
     &           xyj,xit,ett,ds,x,y,turmu, vort,turps,turre,turos, 
     &           fmu, vk,ve, tmp, uu, vv, ccx, ccy,
     &           coef2x, coef2y, coef4x, coef4y,precon, spectxi, 
     &           specteta,rsa,exprhs,dhatq,exprsa,dhatt,.true.)
c             write(*,*)"finished 3rd get_rhs in nksolve with true"

c---- save stage flow field every noutevery and nouteverybig iterations ----

            if ((outtime .or. igrad .eq. 1) .and. numiter.le.   
     &       iend+nkskip .and.(mod(numiter-iend,nouteverybig).eq.0) 
     &       .and. IOPTM .eq. 6)  then 
   
               call iomarkusstage(1,jstage,(numiter-iend)/nouteverybig,
     &           maxj,maxk,qps,xyj,turps)

            endif

            if ((outtime .or. igrad .eq. 1) .and. numiter.gt.  
     &       iend+nkskip.and.(mod(numiter-iend-nkskip,noutevery).eq.0) 
     &       .and. IOPTM .eq. 6)  then 
   
               call iomarkusstage(1,jstage,(numiter-iend-nkskip)/
     &           noutevery+nkskip/nouteverybig,maxj,maxk,qps,xyj,turps)
       
            endif

         ENDIF

 11      continue
cmt   || =============================== ||
cmt   ||  Runge-Kutta loop (do 11) ends  ||
cmt   |+---------------------------------+|
cmt   +-----------------------------------+

         if (unsteady) then

c     calculate lift and drag (note: pressure is already updated)
            nscal=0
            call clcdmb(qps,press,x,y,xy,xyj,nscal,.false.)

c     update values  

            DO jjj=1,maxjkq
               qtmp = qos(jjj) 
               qos(jjj) = q(jjj)
               q(jjj) = qps(jjj)
c         quadratic extrapolation for the next physical time step               
c$$$               qps(jjj)=3.d0*q(jjj)-3.d0*qos(jjj)+qtmp
c     linear extrapolation for the next physical time step fo BDF2
c     and constant extrapolation for ESDIRK
                if (imarch.EQ.2) qps(jjj)=2.d0*q(jjj)-qos(jjj)
            ENDDO

            if (nmax .eq. 5) then 
               do jjj=1,maxjk
                  turos(jjj)=turre(jjj)
                  turre(jjj)=turps(jjj)
c     -- and constant extrapolation for the next physical time step turps=turre 
c     linear extrapolation for the next physical time step
c                  turps(jjj)=2.d0*turre(jjj)-turos(jjj)                 
               end do
            end if


c     -- store force and moment coefficients
            ip = 12
            junit=1
            call ioall( junit, mglev, ip, maxj, maxk, q,qos,press,
     &           sndsp,turmu,fmu,vort,turre,turos,vk,ve,xy,xyj,x,
     &           y, coef2x, coef4x, coef2y, coef4y)
            do jj=1,6
               backspace(n_ld)
            enddo


c     -- output pressure in a certain grid node

c$$$            jj=lgptr(blobs)+(jobs+nhalo-1)+(kobs+nhalo-1)*jmax(blobs) 
c$$$            print *,numiter,press(jj)*xyj(jj) 

c     -- store elemental force and moment coefficients --
            ip = 11
            call ioall(junit, mglev, ip, maxj, maxk, q, qos,press,sndsp, 
     &           turmu, fmu, vort, turre, turos, vk, ve, xy, xyj, x, y,
     &           coef2x, coef4x, coef2y, coef4y)
            backspace(n_eld)    ! only active when ioptm=2

c     -- output residual to .his file -- 
            write(n_his,61) numiter,totime1,resid,maxres(3),maxres(1),
     &           maxres(2), residmx, clt, cdt, omega
 61         format(i5, e12.4, e13.5, 3i4, e10.2, e16.8, e16.8, e16.8)
            call flush(n_his)

c     -- output l2 turbulence residual to n_this --
            if (nmax.eq.5) then
               tresid = sqrt( tresid/float(npts) )
               write (n_this,71) numiter, tresid, mxtur(3), mxtur(1),
     &              mxtur(2), tresmx
               call flush(n_this)
            end if
 71         format(i7,1x,e15.8,3i6,1x,e15.8)


c---- save potential restart .q and .his files

            if (mod(numiter-iend,nq).eq.0) then

               namelen =  strlen(restart_file_prefix)
               filena_restart = restart_file_prefix
               filena_restart(namelen+1:namelen+2)='k'
               filena_restart(namelen+2:namelen+6) = '.his'
c     
               namelen =  strlen(output_file_prefix)
               filena = output_file_prefix
               filena(namelen+1:namelen+5) = '.his'
c     
               command = 'cp '
               namelen = strlen(filena)
               command(4:3+namelen) = filena

               lentmp = namelen+3+1
               namelen = strlen(filena_restart)
               command(lentmp+1:lentmp+namelen) = filena_restart
               call system(command)

               if (unsteady) then

                  write(n_q2k) nblks
                  write(n_q2k) (jbmax(i),kbmax(i),i=1,nblks)
                  do ib=1,nblks
                     lq = lqptr(ib)
                     lg = lgptr(ib)
                     call writeq(n_q2k,qos(lq),xyj(lg),jmax(ib),
     &                    kmax(ib),jminbnd(ib),jmaxbnd(ib),
     &                    kminbnd(ib),kmaxbnd(ib))
                  end do
                  
                  write(n_q2k) numiter-1

                  rewind(n_q2k)

                  if (viscous.and.turbulnt) then
                     write(n_t2k) nblks
                     write(n_t2k) (jbmax(i),kbmax(i),i=1,nblks)
                     do ib=1,nblks
                        lq = lqptr(ib)
                        lg = lgptr(ib)
                        call writearr4(n_t2k, vk(lg),ve(lg),turos(lg),  
     &                       turmu(lg),jmax(ib),kmax(ib),jminbnd(ib),  
     &                       jmaxbnd(ib),kminbnd(ib),kmaxbnd(ib))
                     end do
                     rewind(n_t2k)
                  end if
                  
               end if

               write(n_qk) nblks
               write(n_qk) (jbmax(i),kbmax(i),i=1,nblks)
               do ib=1,nblks
                  lq = lqptr(ib)
                  lg = lgptr(ib)
                  call writeq(n_qk,q(lq),xyj(lg),jmax(ib),  
     &                 kmax(ib), jminbnd(ib), jmaxbnd(ib),
     &                 kminbnd(ib), kmaxbnd(ib))
               end do

               write(n_qk) numiter
               
               rewind(n_qk)

               if (viscous.and.turbulnt) then              
      
                  write(n_tk) nblks
                  write(n_tk) (jbmax(i),kbmax(i),i=1,nblks)
                  do ib=1,nblks
                     lq = lqptr(ib)
                     lg = lgptr(ib)
                     call writearr4(n_tk,vk(lg),ve(lg),turre(lg),
     &                    jmax(ib),kmax(ib),jminbnd(ib),jmaxbnd(ib), 
     &                    kminbnd(ib),kmaxbnd(ib))
                  end do
                  rewind(n_tk)

               end if

            end if

      
c---- save target flow field every noutevery iterations --

            if (outtime .and. IOPTM .eq. 1 .and. numiter.gt.iend+nkskip 
     &           .and. mod(numiter-iend-nkskip,noutevery).eq.0) then

               if (iobjf .eq. 1) then

                 call iomarkus(2,(numiter-iend-nkskip)/noutevery+nkskip/
     &              nouteverybig,maxj,maxk,q,xyj,turre)
                 
               else if (iobjf .eq. 8) then

                 call iocpmarkus(1,(numiter-iend-nkskip)/noutevery+
     &              nkskip/nouteverybig,maxj,maxk,press,xyj,x,y,cptar)

               end if
       
            endif

c---- save flow field every noutevery and nouteverybig iterations ----

            if ((outtime .or. igrad .eq. 1) .and. numiter.le.   
     &       iend+nkskip .and.(mod(numiter-iend,nouteverybig).eq.0) 
     &       .and. IOPTM .eq. 6 )  then 
   
               call iomarkus(1,(numiter-iend)/nouteverybig,maxj,maxk,
     &              q,xyj,turre)

            endif

            if ((outtime .or. igrad .eq. 1) .and. numiter.gt.  
     &       iend+nkskip.and.(mod(numiter-iend-nkskip,noutevery).eq.0) 
     &       .and. IOPTM .eq. 6 )  then 
   
               call iomarkus(1,(numiter-iend-nkskip)/noutevery+nkskip/
     &              nouteverybig,maxj,maxk,q,xyj,turre)
       
            endif


         end if   ! unsteady


cmt
 10   continue                  !end of outer iterations

cmt   || ============================ ||
cmt   ||  physical loop (do 10) ends  ||
cmt   |+------------------------------+|
cmt   +--------------------------------+


      if (.not. unsteady) then
         do jjj=1,maxjkq
            q(jjj) = qps(jjj)
         end do

         if (nmax .eq. 5) then 
            do jjj=1,maxjk            
               turre(jjj)=turps(jjj)
            end do
         end if

      end if

c     -- restore csr pointer arrays -- 
      do j=1,maxjk*jacb+1
         ia(j) = iatmp(j)
         ipa(j) = ipatmp(j) 
      end do
      do j = 1,maxjk*jacb2*9
         ja(j) = jatmp(j)
      end do
      do j = 1,maxjk*jacb2*5
         jpa(j) = jpatmp(j) 
      end do 

c     -- close restart files --

      close(n_qk)
      close(n_q2k)
      close(n_tk)
      close(n_t2k)










c     -- debug: freeze absolute values --
c     -- applies to dissipation and S-A model --
      if (ifrzw.ne.0 .and. .not.frozen) then

         write (n_out,200) ifrzw
 200     format (/3x,'Frozen absolute values: ifrz =',i3/)

         ix = 1
         iy = 2
         do ib=1,nblks
            lg=lgptr(ib)
            lq=lqptr(ib)
            call eigval(ix, iy, jmax(ib), kmax(ib), q(lq), press(lg),
     &           sndsp(lg), xyj(lg), xy(lq), xit(lg), uu(lg), ccx(lg),
     &           jbegin2(ib), jend2(ib), kminbnd(ib), kmaxbnd(ib))
            call copy_array( jmax(ib), kmax(ib), 1, uu(lg), fra1(lg))
         end do

         ix = 3
         iy = 4
         do ib=1,nblks
            lg=lgptr(ib)
            lq=lqptr(ib)
            call eigval( ix, iy, jmax(ib), kmax(ib), q(lq), press(lg),
     &           sndsp(lg), xyj(lg), xy(lq), ett(lg), vv(lg), ccy(lg),
     &           jminbnd(ib), jmaxbnd(ib), kbegin2(ib), kend2(ib))
            call copy_array( jmax(ib), kmax(ib), 1, vv(lg), fra2(lg))
         end do
      end if




cmpr  Deallocate the big arrays
      deallocate(as,pa,wk_gmres,iatmp,jatmp,ipatmp,jpatmp,jlu,levs,jw)
      deallocate(alu,wk,ast,iat,jat)


      return
      end                       !nksolve
