c----------------------------------------------------------------------
c     -- determine initial node order --
c     -- reorder nodes if required  --

c     written by: marian nemec
c     date: June 2000
c----------------------------------------------------------------------
      subroutine order (jdim, kdim, jt1, jt2, inord, ireord, indx, ja,
     &      ia, jat, iat, iren, iout)

      implicit none

#include "../include/arcom.inc"

      integer jdim, kdim, iblock, inord, ireord, jt1, jt2
      integer i, j, k, ii, j1, j2, j3, jf, n, nbi, nbe, nnz, ier
      integer neven, nge, loc, jn, nbj, nw, rmth, iout, na

      parameter (iblock = 10)

      integer indx(jdim,kdim), ia(jdim*kdim+2)
      integer ja(jdim*kdim*5*(5*9+1)+jdim*5*3+1), iat(jdim*kdim+2)
      integer jat(jdim*kdim*5*(5*9+1)+jdim*5*3+1), iren(jdim*kdim)

      double precision as

      character*10 title
      character*2 munt 
      integer lines(1)


      if (periodic) then
         jmax = jdim-1
      else
         jmax = jdim       
      end if

      kmax = kdim

      na = jmax*kdim
      nw = jmax*kdim*80

c     -- initial ordering scheme --
c     -- inord = 1: natural ordering --
c     -- inord = 2: double bandwidth (across wake-cut) --

c     -- reordering scheme: --
c     -- ireord = 0: no reordering --
c     -- ireord = 1: Cuthill-McKee --
c     -- ireord = 2: reverse Cuthill-McKee --

      if ( inord.eq. 1) write (iout,5)
      if ( inord.eq. 2) write (iout,6)
      
 5    format (/3x,'Natural initial node order.')
 6    format (/3x,'Double bandwidth initial node order.')

c     -- initial ordering of unknows --

      if ( inord.eq.1 ) then
c     -- natural (jk) ordering of unknowns -- 
        do j = 1,jmax
          do k = 1,kmax
            indx(j,k) = (j-1)*kmax + k
          end do
        end do
      else if ( inord.eq.2 ) then
c     -- double bandwidth (across wake-cut) ordering of unknowns --
        neven = mod(jmax,2)
        if (neven.eq.0) then
          ii = jmax/2
        else
          ii = (jmax-1)/2
        endif
        
        i = 0
        do j = 1,ii
          do k = kmax,1,-1
            i = i+1
            indx(j,k) = i
          enddo
          jf = jmax-j+1
          do k = 1,kmax
            i = i+1
            indx(jf,k) = i
          enddo
        enddo
        if (neven.ne.0) then
          j = ii+1
          do k = 1,kmax
            i = i+1
            indx(j,k) = i
          enddo
        endif
      else
        write (*,*) 'OPTSET: Wrong value for inord!!'
        stop
      end if

      if ( ireord.gt.0 ) then

         if (periodic) then
            jt1=1
            jt2=jdim-1
         end if

        do j = 1,na*iblock
          ja(j) = 0
        end do

        do j = 1,na+1
          ia(j) = 0
        end do

c     -- fill block jacobian matrix --
        call blk_jac (jdim, kdim, jt1, jt2, ja, ia, iblock, indx)

c     -- make graph symmetric for reordering --
        jn = 0
        do i = 1,na
          nbi = (i-1)*iblock
          do 10 j1 = 1,ia(i)
            j = ja(nbi+j1)
            nbj = (j-1)*iblock
            do j2 = 1,ia(j)
              j3 = ja(nbj+j2)
              if (j3.eq.i) goto 10
            end do
            ia(j) = ia(j)+1
            if ( ia(j) .gt. iblock ) then
              write (*,*) 'SYMGRAPH: problem at row', j,ia(j)
              stop
            end if
            ja(nbj+ia(j)) = i
            jn = jn + 1
 10       continue
        end do

        write (iout,20) jn
 20     format (3x,'Added',i4,' blocks to make graph symmetric.')

c     -- remove zeros --
        nbi = 1
        j2 = ia(1)
        ia(1) = 1
        do ii = 1,na
          nbj = (ii-1)*iblock
          do j1 = nbj+1,nbj+j2
            ja(nbi) = ja(j1)
            nbi = nbi+1
          end do
          j2 = ia(ii+1)
          ia(ii+1) = nbi
        end do

c     -- order within rows --
        call csrcsc (na,0,1,as,ja,ia,as,jat,iat)
        call csrcsc (na,0,1,as,jat,iat,as,ja,ia)

        nnz = ia(na+1)-1

        do i = 1,nw
          jat(i) = 0
        end do

        do i = 1,na
          iat(i) = 0
          iren(i) = 0
        enddo

c     -- reorder --
c     JG: send the dimensions for optimal root node placement
        call cmk (ireord, nnz, na, iren, ja, ia, iat, iout, nw,
     &        jat, ier, jdim, kdim)
        
        if (ier.ne.0) then
          write (*,*) 'ORDER: ERROR IN ORDERING !! '
          write (*,*) 'error code = ',ier
          write (*,*) 'program terminated'
          write (*,*) '----------------------------'
          stop
        endif

        do k = 1,kmax
          do j = 1,jmax
            indx(j,k) = iren(indx(j,k))
          end do
        end do

cc     -- plot reordered block jacobian --
c        
c        do j = 1,na*iblock
c          ja(j) = 0
c        end do
c        
c        do j = 1,na+1
c          ia(j) = 0
c        end do
c        
c        call blk_jac (jdim, kdim, jt1, jt2, ja, ia, iblock, indx)
c        
cc     -- remove zeros --
c        nbi = 1
c        j2 = ia(1)
c        ia(1) = 1
c        do ii = 1,na
c          nbj = (ii-1)*iblock
c          do j1 = nbj+1,nbj+j2
c            ja(nbi) = ja(j1)
c            nbi = nbi+1
c          end do
c          j2 = ia(ii+1)
c          ia(ii+1) = nbi
c        end do
c
cc     -- order within rows --
c        call csrcsc (n,0,1,as,ja,ia,as,jat,iat)
c        call csrcsc (n,0,1,as,jat,iat,as,ja,ia)
c        
c        munt = 'in'
c        call pspltm (na, na, 0, ja, ia, title, 0, 7.0, munt, 0, lines,
c     &        86)
c        stop
        
      end if

      return
      end                       !order
