c-----------------------------------------------------------------------
c     -- contribution from inviscid flux Jacobian: y direction --
c     -- m. nemec, july 2001 --
c-----------------------------------------------------------------------
      subroutine fillay(jmax, kmax, nmax, ibc1, ibc2, ibc3, ibc4, 
     &     kminbnd, kmaxbnd, jminbnd, jmaxbnd, j1, j2, k1, k2, 
     &     indx, q, xy, pa, as, icol,
     &     gamma, gami, precmat, jacmat, maxjb, maxkb)

      implicit none

      integer jmax,kmax,nmax,ibc1, ibc2,ibc3,ibc4,j,k, maxjb, maxkb
      integer kminbnd, kmaxbnd, jminbnd, jmaxbnd, j1, j2,k1,k2,ks,ke
      integer indx(jmax,kmax),icol(*),itmp,km1,kp1,n,ij,ii,m,js,je
      double precision q(jmax,kmax,4),xy(jmax,kmax,4)
      double precision as(*),pa(*),gamma,gami
      double precision hd,r1,r2,rr,u,v,u2,v2,c1,c2,qs
      logical precmat,jacmat

c     -- local array --      
      double precision diag(4,4,maxjb,maxkb)


c     -- column-major order, i.e. matrix columns are stored first... --
      hd = 0.5d0
      DO k = k1,k2                                            
         DO j = j1,j2                                              
            r1 = xy(j,k,3)*hd
            r2 = xy(j,k,4)*hd
            rr = 1.0d0 / q(j,k,1)
            u  = q(j,k,2)*rr
            v  = q(j,k,3)*rr
            u2 = u*u
            v2 = v*v
            c1 = gami*( u2+v2 )*hd
            c2 = q(j,k,4)*rr*gamma
            qs = u*r1 + v*r2
            diag(1,1,j,k) = 0.0d0
            diag(2,1,j,k) = r1
            diag(3,1,j,k) = r2
            diag(4,1,j,k) = 0.0d0
            diag(1,2,j,k) = ( -u2 + c1 )*r1 - u*v*r2
            diag(2,2,j,k) = - ( gamma - 3.0d0 )*u*r1 + v *r2
            diag(3,2,j,k) = - gami*v*r1 + u*r2
            diag(4,2,j,k) = gami*r1
            diag(1,3,j,k) = - u*v*r1 + ( -v2 + c1)*r2
            diag(2,3,j,k) = v*r1 - gami*u*r2
            diag(3,3,j,k) = u*r1 + ( 3.0d0 - gamma )*v*r2                  
            diag(4,3,j,k) = gami*r2
            diag(1,4,j,k) = ( - c2 + c1*2.0d0 )*qs
            diag(2,4,j,k) = ( c2 - c1 )*r1 - gami*u*qs
            diag(3,4,j,k) = ( c2 - c1 )*r2 - gami*v*qs
            diag(4,4,j,k) = gamma*qs                              
         ENDDO
      ENDDO

c     -- first order jacobian preconditioner --
      IF (precmat) THEN
         DO k = k1+1,k2-1
            km1 = k-1
            kp1 = k+1
            DO j = j1,j2
               itmp = ( indx(j,k) - 1 )*nmax
               DO n  = 1,4 
                  ij  = itmp + n
                  ii  = ( ij - 1 )*icol(5)
                  DO m = 1,4
                     pa(ii+icol(1)+m) = pa(ii+icol(1)+m)-diag(m,n,j,km1)
                     pa(ii+icol(3)+m) = pa(ii+icol(3)+m)+diag(m,n,j,kp1)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDIF

c     -- second order jacobian --
      if (jacmat) then

c     -- linearize centered differencing at interior nodes --
         ks = kminbnd + 2
         ke = kmaxbnd - 2
         if (ibc2.EQ.0) then
            js = jminbnd
         else
            js = jminbnd + 2
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd - 2
         end if 

         do k = ks,ke
            km1 = k-1
            kp1 = k+1
            do j = js,je
               itmp = ( indx(j,k) - 1 )*nmax
               do n  = 1,4 
                  ij  = itmp + n
                  ii  = ( ij - 1 )*icol(9)
                  do m =1,4
                     as(ii+icol(3)+m) = as(ii+icol(3)+m)-diag(m,n,j,km1)
                     as(ii+icol(5)+m) = as(ii+icol(5)+m)+diag(m,n,j,kp1)
                  end do
               end do
            end do
         end do
         
c     -- first interior boundary nodes --
         if (ibc2.EQ.0) then
            js = jminbnd
         else
            js = jminbnd + 2
         end if
         if (ibc4.EQ.0) then
            je = jmaxbnd
         else
            je = jmaxbnd - 2
         end if
         k = kminbnd+1
         km1 = k-1
         kp1 = k+1

         do j = js,je
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(2)+m) = as(ii+icol(2)+m)-diag(m,n,j,km1)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m)+diag(m,n,j,kp1)
               end do
            end do
         end do

         k = kmaxbnd-1
         km1 = k-1
         kp1 = k+1
         do j = js,je
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(3)+m) = as(ii+icol(3)+m)-diag(m,n,j,km1)
                  as(ii+icol(5)+m) = as(ii+icol(5)+m)+diag(m,n,j,kp1)
               end do
            end do
         end do

         if (ibc2.NE.0) then
            j   = jminbnd + 1
            ks  = kminbnd + 2
            ke  = kmaxbnd - 2
            do k = ks,ke
               km1 = k-1
               kp1 = k+1
               itmp = ( indx(j,k) - 1 )*nmax
               do n  = 1,4 
                  ij  = itmp + n
                  ii  = ( ij - 1 )*icol(9)
                  do m =1,4
                     as(ii+icol(2)+m) = as(ii+icol(2)+m)-diag(m,n,j,km1)
                     as(ii+icol(4)+m) = as(ii+icol(4)+m)+diag(m,n,j,kp1)
                  end do
               end do
            end do
            
            k = kminbnd + 1
            km1 = k-1
            kp1 = k+1
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(1)+m) = as(ii+icol(1)+m)-diag(m,n,j,km1)
                  as(ii+icol(3)+m) = as(ii+icol(3)+m)+diag(m,n,j,kp1)
               end do
            end do
            
            k = kmaxbnd - 1
            km1 = k-1
            kp1 = k+1
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(2)+m) = as(ii+icol(2)+m)-diag(m,n,j,km1)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m)+diag(m,n,j,kp1)
               end do
            end do            
         end if

         if (ibc4.NE.0) then
            j   = jmaxbnd - 1
            ks  = kminbnd + 2
            ke  = kmaxbnd - 2
            
            do k = ks,ke
               km1 = k-1
               kp1 = k+1
               itmp = ( indx(j,k) - 1 )*nmax
               do n  = 1,4 
                  ij  = itmp + n
                  ii  = ( ij - 1 )*icol(9)
                  do m =1,4
                     as(ii+icol(3)+m) = as(ii+icol(3)+m)-diag(m,n,j,km1)
                     as(ii+icol(5)+m) = as(ii+icol(5)+m)+diag(m,n,j,kp1)
                  end do
               end do
            end do
            
            k = kminbnd + 1
            km1 = k-1
            kp1 = k+1
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(2)+m) = as(ii+icol(2)+m)-diag(m,n,j,km1)
                  as(ii+icol(4)+m) = as(ii+icol(4)+m)+diag(m,n,j,kp1)
               end do
            end do
            
            k = kmaxbnd - 1
            km1 = k-1
            kp1 = k+1
            itmp = ( indx(j,k) - 1 )*nmax
            do n  = 1,4 
               ij  = itmp + n
               ii  = ( ij - 1 )*icol(9)
               do m =1,4
                  as(ii+icol(3)+m) = as(ii+icol(3)+m)-diag(m,n,j,km1)
                  as(ii+icol(5)+m) = as(ii+icol(5)+m)+diag(m,n,j,kp1)
               end do
            end do      
         end if
      end if
      
      return
      end                       !fillay
