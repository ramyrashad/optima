*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*     File  sn_open.f                 Open and close files Fortran style
*
*     snopen   snclose    getfilename
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snopen
     &   ( fname, lunit, inform )

      character*(*)
     &     fname
      integer
     &     inform, lunit
*     ------------------------------------------------------------------
*     snopen opens a file Fortran style
*     ------------------------------------------------------------------
      inform = 0

      open( unit = lunit, file = fname, status = 'UNKNOWN', err = 800 )
      goto 900

 800  status = 1
 900  end ! subroutine snopen

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine snclose( lunit )

      integer        lunit
*     ------------------------------------------------------------------
*     snclose closes a file Fortran style
*     ------------------------------------------------------------------

      close( unit = lunit )

      end ! subroutine snclose

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine getfilename
     &   ( lunit , fname )

      character*(*)
     &     fname
      integer
     &     lunit
!

      logical
     &     fnamed
      integer
     &     last
!
      inquire( unit = lunit , named = fnamed )

      if (fnamed) then
         inquire( unit = lunit , name = fname )
      else

!        Append unit number to 'fort.'

         if (lunit .le. 9) then
            write(fname, '(a,i1)') 'fort.', lunit
            last = 6
         else
            write(fname, '(a,i2)') 'fort.', lunit
            last = 7
         endif

!        fname = 'fort.4'
      endif

      end ! subroutine getfilename

