      subroutine filltime(jdim, kdim, ndim, xyj, coef, coef2, coef4,
     &     indx, icol, as, pa, pdc,akk)

      implicit none

#include "../include/arcom.inc"

      integer j, k, ii, jdim, kdim, ndim, jp1, jm1, jp2, jm2, n, jj
      integer indx(jdim,kdim), icol(9),jsta,jsto

      double precision xyj(jdim,kdim), coef2(jdim,kdim)
      double precision coef(jdim,kdim), coef4(jdim,kdim)
      double precision as(jdim*kdim*ndim*ndim*9)
      double precision pa(jdim*kdim*ndim*ndim*5)

      double precision c2m, c4m, c2, c4, pdc, dttmp
      double precision diag
      double precision akk
c     -----------------------------------------------------------------

      if (jesdirk.eq.4 .or. jesdirk.eq.3)then

         diag = (1.d0/(akk*dt2))

      else   !BDF2 time marching 

         if (numiter-istart .ne. 1) then
c        Not first time step

            if (.not. errest) then

               if (numiter .ne. iend+nk_skip+1 .or. nk_skip .eq.0 ) then              
c              normal BDF2 with constant time steps
                  diag = 3.0d0/(2.0*dt2)
               else
c              special BDF2 at time step size change
                  diag=(2.d0*dtsmall+dtbig)/(dtsmall*(dtsmall+dtbig))
               end if

            else
c           special BDF2 with variable time steps
               dttmp=dta(numiter-iend-1)
               diag=(2.d0*dt2+dttmp)/(dt2*(dt2+dttmp))
              
            end if

         else
c        First time step

            if (.not. sbbc) then
c           special BDF2 at first time step
               diag=(2.d0*dt2+dtold)/(dt2*(dt2+dtold))
               
            else
c           Implicit Euler at first iteration for suction/blowing BC
               diag=1.d0/dt2
            
            end if

         end if                 !First time step?

      end if !jesdirk?


      if ( prec_mat ) then

            do j = jlow,jup
               do k = klow,kup
                  do n =1,ndim
                     ii = ( indx(j,k) - 1 )*ndim + n
                     jj = ( ii - 1 )*icol(5)
                     pa(jj+icol(2)+n) = pa(jj+icol(2)+n) + diag
                  end do
               end do
            end do

      end if
c     -----------------------------------------------------------------

      if ( jac_mat ) then

         if(.not.periodic) then
            jsta=jlow+1
            jsto=jup-1
         else
            jsta=jlow
            jsto=jup
         end if

         do j = jsta,jsto
            do k = klow+1,kup-1                 
               do n =1,ndim
                  ii = ( indx(j,k) - 1 )*ndim + n
                  jj = ( ii - 1 )*icol(9)
                  if (clopt) jj = ( ii - 1 )*(icol(9)+1)
                  as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag
               end do
            end do
         end do

         if(.not.periodic) then

            j = jlow
            
            do k =klow+1,kup-1                                                 
               do n =1,ndim
                  ii = ( indx(j,k) - 1 )*ndim + n
                  jj = ( ii - 1 )*icol(9) 
                  if (clopt) jj = ( ii - 1 )*(icol(9)+1)
                  as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag            
               end do
            end do

            j = jup
            
            do k =klow+1,kup-1                                                   
               do n =1,ndim
                  ii = ( indx(j,k) - 1 )*ndim + n
                  jj = ( ii - 1 )*icol(9) 
                  if (clopt) jj = ( ii - 1 )*(icol(9)+1)
                  as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag           
               end do
            end do

         end if

         k = klow
         do j =jsta,jsto                                                       
            do n =1,ndim
               ii = ( indx(j,k) - 1 )*ndim + n
               jj = ( ii - 1 )*icol(9)
               if (clopt) jj = ( ii - 1 )*(icol(9)+1)
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag           
            end do
         end do

         k = kup
         do j =jsta,jsto                                                      
            do n =1,ndim
               ii = ( indx(j,k) - 1 )*ndim + n
               jj = ( ii - 1 )*icol(9)
               if (clopt) jj = ( ii - 1 )*(icol(9)+1)
               as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag            
            end do
         end do
         
         if(.not.periodic) then
            
            j = jlow
            k = klow                              
            do n =1,ndim
               ii = ( indx(j,k) - 1 )*ndim + n
               jj = ( ii - 1 )*icol(9) 
               if (clopt) jj = ( ii - 1 )*(icol(9)+1)
               as(jj+icol(2)+n) = as(jj+icol(2)+n) + diag         
            end do

            j = jlow
            k = kup       
            do n =1,ndim
               ii = ( indx(j,k) - 1 )*ndim + n
               jj = ( ii - 1 )*icol(9)
               if (clopt) jj = ( ii - 1 )*(icol(9)+1)
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag         
            end do
           
            j = jup
            k = klow       
            do n =1,ndim
               ii = ( indx(j,k) - 1 )*ndim + n
               jj = ( ii - 1 )*icol(9)
               if (clopt) jj = ( ii - 1 )*(icol(9)+1)
               as(jj+icol(3)+n) = as(jj+icol(3)+n) + diag          
            end do
            
            j = jup
            k = kup       
            do n =1,ndim
               ii = ( indx(j,k) - 1 )*ndim + n
               jj = ( ii - 1 )*icol(9)
               if (clopt) jj = ( ii - 1 )*(icol(9)+1)
               as(jj+icol(4)+n) = as(jj+icol(4)+n) + diag         
            end do
            
         end if

      end if

      return
      end                       ! filltime
