      subroutine update(jdim,kdim,qp,s,xyj)

#include "../include/arcom.inc"

      dimension qp(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)      
      dimension tm1(4,4)
c                                                                       
      phic = phidt/(1.+phidt)                                        
c                                                                       
c     update solution
c
c     the 'kbegin.ne.kend' is to prevent re-updating the body
c     when cmesh=true since update.f called twice
      if (ibc .and. kbegin.ne.kend) then
         jl=jbegin
         ju=jend
         kl=kbegin
         ku=kend    
      else
         jl=jlow       
         ju=jup
         kl=klow
         ku=kup
      endif
c
c
      do 650 n = 1,4                                                 
      do 650 k = kl,ku  
      do 650 j = jl,ju                                            
         qp(j,k,n) = qp(j,k,n) + scaledq*s(j,k,n)
 650  continue
c
c                                                                       
c     initialize rhs for next iteration if meth is not any of the
c     subiteration schemes.
      if (meth.eq.2 .or. meth.eq.3) then
         do 670 n = 1,4                                                 
         do 670 k = klow,kup                                            
         do 670 j = jlow,jup                                            
            s(j,k,n) = phic * s(j,k,n)  
 670     continue         
      endif
c                                                                       
      return                                                          
      end                                                             














c            if (numiter.eq.iend) then
c            do 639 n=1,4
c               write(99,*) ' '
c               write(99,*) ' '
c               write(99,*) '**** BODY ****'
c               k=kbegin
c               do 634 j=jbegin,jend
c                  write(99,999) n,j,k,q(j,k,n),s(j,k,n),xyj(j,k)
c 634           continue
c               write(99,*) ' '
c               write(99,*) ' '
c               write(99,*) '**** FARFIELD ****'
c               k=kend
c               do 635 j=jbegin,jend
c                  write(99,999) n,j,k,q(j,k,n),s(j,k,n),xyj(j,k)
c 635           continue
c               write(99,*) ' '
c               write(99,*) ' '
c               write(99,*) '**** OUTFLOW ****'
c               j=jbegin
c               do 636 k=kbegin,kend
c                  write(99,999) n,j,k,q(j,k,n),s(j,k,n),xyj(j,k)
c 636           continue
c               j=jend
c               do 637 k=kbegin,kend
c                  write(99,999) n,j,k,q(j,k,n),s(j,k,n),xyj(j,k)
c 637           continue
c 639        continue
c            endif
c 999        format(3I5,3e20.10)
c            call flush(99)
c            stop
