      subroutine flushit (ioc)
c
c  This routine  is written to  provide  a common flush routine for
c  both SGI and IBM machines. The IBM machines with XLF version 2.x
c  compiler  do not have the  flush utility routine available, thus 
c  this routine will only invoke the flush routine on SGI machines.
c
c  Written by David Jones (R&D Methods de-Havilland Inc) 18 July 95
c
c
      integer   *4     ioc
c
c______________________________________________________________________
c
c
#ifdef IBM

      call flush_ (ioc)

#else
#ifndef YMP

      call flush  (ioc)

#endif
#endif
c
      return
c
      end
