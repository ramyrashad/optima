      subroutine intg(lmax0,xf,yf,zf,lmaxp,xc,yc,zc)                   
c                                                                       
c===============================================================        
#include "../include/parm.inc"
      dimension xf(2),yf(2),zf(2)                                      
      dimension xc(2),yc(2),zc(2)                                      
      dimension s(maxj)                                                
      dimension ri(maxj)                                               
      dimension ss(maxj)                                               
      dimension rri(maxj)                                              
                                                                       
      s(1)  = 0.0                                                      
      ri(1) = 0.0                                                      
      do 10 l=2,lmax0                                                  
         s(l)=s(l-1)                                                    
     >            + sqrt( (xf(l)-xf(l-1))**2 + (yf(l)-yf(l-1))**2
     >            + (zf(l)-zf(l-1))**2 )                          
         ri(l) = float(l-1)
10    continue                                                         
                                                                       
      if (s(lmax0) .gt. 1.e-20) then  
                                                                       
         do 20 l=1,lmaxp                                                  
            rri(l) = float(l-1)*ri(lmax0)/float(lmaxp-1)         
20       continue                                                         

         ri(lmax0) = rri(lmaxp)                                           
         call csplin(ri,s,lmax0,rri,ss,lmaxp)                             
         s(lmax0) = ss(lmaxp)                                             
                                                                       
         call csplin(s,xf,lmax0,ss,xc,lmaxp)                              
         call csplin(s,yf,lmax0,ss,yc,lmaxp)                              
         call csplin(s,zf,lmax0,ss,zc,lmaxp)                              
      else                                                             
c                                                                       
c        .... singularity detected
c                                                                       
         sumx = 0.0                                                       
         sumy = 0.0                                                       
         sumz = 0.0                                                       
         do 30 l=1,lmax0                                                  
           sumx = sumx + xf(l)
           sumy = sumy + yf(l)
           sumz = sumz + zf(l)
 30      continue                                                         
         sumx = sumx/float(lmax0)                                        
         sumy = sumy/float(lmax0)                                        
         sumz = sumz/float(lmax0)                                        
         do 40 l=1,lmaxp                                                  
            xc(l) = sumx                                                    
            yc(l) = sumy                                                    
            zc(l) = sumz                                                    
 40      continue                                                         
      endif                                                            
c
      return                                                           
      end                                                              
