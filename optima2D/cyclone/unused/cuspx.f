      subroutine cuspx(jdim,kdim,q,coef2x,coef4x,sndsp,s,
     >spect,xyj,press,ccx,uu,xy,x,y)
c
c     prototype cusp disspation subroutine
c     october 5, 1995
c
c
c
      include '../include/arcom.inc'
c
c **************************************************************
c may.95  budget() added by kyle frew
c ***************************************************************
       include 'budget.inc'

       dimension q(jdim,kdim,4),coef2x(jdim,kdim),coef4x(jdim,kdim),
     &           sndsp(jdim,kdim),s(jdim,kdim,4),work(maxj,maxk,4,2),
     &           spect(maxj,maxk,3),xyj(jdim,kdim),press(jdim,kdim),
     &      ccx(jdim,kdim),uu(jdim,kdim),xy(jdim,kdim,4),x(jdim,kdim)
     $      ,y(jdim,kdim),alph(maxj,maxk),beta(maxj,maxk)


       double precision u,s1,s2,cs,dot1,dot2,fact1,fact2,
     &      temp(maxj,maxk,4),phi,a1,a2,h,dtd,temp2(maxj,maxk,4)
       
       integer i,j,k,g,l,t,j1,j2
c
c ** k.frew june 1995
c
       do i=1,4
       do k=kbegin,kend
          do j=jbegin,jend
            temp2(j,k,i)=0.0
            budget2(j,k,1)=0.0
          end do
       end do
       end do
c
c ** end of k.frew 
c
       dtd=dt/(1.+phidt)
       do 11 i=1,4
          do 14 k=klow,kup
             do 14 j=jbegin,jup
                work(j,k,i,1)= q(j+1,k,i)*xyj(j+1,k)
     &                        -q(j,k,i)  *xyj(j,k)
 14       continue
          
             do 15 k=klow,kup
                work(jend,k,i,1)=work(jend-1,k,i,1)
 15          continue



          do 18 k=klow,kup
             do 18 j=jlow,jup
                 work(j,k,i,2)=     work(j+1,k,i,1)
     &                         -2.*work(j,k,i,1)
     &                             +work(j-1,k,i,1)
  18    continue
c       
c   boundary conditions big time
c234567
       j1=jbegin
       j2=jend
       do 20 k=klow,kend
          work(j1,k,i,2)=     q(j1+2,k,i)*xyj(j1+2,k)
     &                   -2.0*q(j1+1,k,i)*xyj(j1+1,k)
     &                       +q(j1,k,i)  *xyj(j1,k)
          work(j2,k,i,2)=0.0
 20    continue
 11    continue
c
c calculate coefficients for cusp scheme
c     october 1995 (non-curvilinear coordinates)!!
c frew
c
c       do k=kbegin,kend
c          do j=jbegin,jup
c             dot1=spect(j,k,3)
c             dot2=spect(j,k,2)
c             s2=q(j,k,2)/q(j,k,1)
c             s1=s2/sndsp(j,k)
c             alph(j,k)=abs(s1)
c     subsonic
c             if ((s1.ge.0).and.(s1.le.1)) then
c                beta(j,k)=max(0,2*s1-1)/2.
c             else if ((s1.ge.-1).and.(s1.le.0)) then
c                beta(j,k)=max(0,2*s1+1)/2.
c             endif
c             s1=q(j,k,2)/q(j,k,1)
c             s2=q(j+1,k,2)/q(j+1,k,1)
c             s1=(s1+s2)/2.
c             alph(j,k)=(alph(j,k)*sndsp(j,k)-beta(j,k)*s1)
c     &        /2./xyj(j,k)
c     sonic
c             if ((s1.ge.0).and.(s1.le.1.0)) then
c                beta(j,k)=max(0,(uu+dot1)/(uu-dot1))/2
c             else if ((s1.ge.-1.).and.(s1.le.0)) then
c                beta(j,k)=max(0,(uu+dot2)/(uu-dot2))/2
c             else if
c                beta(j,k)=sign(1,sndsp(j,k))/2
c             endif
c             alph(j,k)=(alph(j,k)*s1
c     &            -beta(j,k)*(uu(j,k)+uu(j+1,k))/2.)/2.
c          end do
c       end do
c       do k=klow,kup
c          beta(jend,k)=beta(jup,k)
c          alph(jend,k)=alph(jup,k)/2./xyj(j,k)
c       end do
c calculate coefficients for cusp scheme
c     november 1995 (curvilinear co-ordinates)!!!!
c frew
c
       do k=klow,kup
          do j=jlow,jup
c             dot1=spect(j,k,3)
c             dot2=spect(j,k,2)
             s2=uu(j,k)
             s1=uu(j,k)/ccx(j,k)
             alph(j,k)=abs(s1)
c     subsonic
             if ((s1.ge.0).and.(s1.le.1)) then
                beta(j,k)=max(0,2*s1-1)/2.
             else if ((s1.ge.-1).and.(s1.le.0)) then
                beta(j,k)=min(0,2*s1+1)/2.
             endif
             s1=(uu(j,k)+uu(j+1,k))/2.
             alph(j,k)=(alph(j,k)*ccx(j,k)-beta(j,k)*s1)
     &        /2./xyj(j,k)
c     sonic
c             if ((s1.ge.0).and.(s1.le.1.0)) then
c                beta(j,k)=max(0,(uu+dot1)/(uu-dot1))/2
c             else if ((s1.ge.-1.).and.(s1.le.0)) then
c                beta(j,k)=max(0,(uu+dot2)/(uu-dot2))/2
c             else if
c                beta(j,k)=sign(1,sndsp(j,k))/2
c             endif
c             alph(j,k)=(alph(j,k)*s1
c     &            -beta(j,k)*(uu(j,k)+uu(j+1,k))/2.)/2.
          end do
       end do
       do k=kbegin,kend
          beta(jend,k)=beta(jup,k)
          alph(jend,k)=alph(jup,k)
          beta(jbegin,k)=beta(jlow,k)
          alph(jbegin,k)=alph(jlow,k)
c          beta(jend,k)=0.0
c          alph(jend,k)=0.0
c          beta(jbegin,k)=0.0
c          alph(jbegin,k)=0.0
       end do
c     
c     
c multiply matrix
c
       do 888 g=0,1
          do 888 i=1,2
             do 111 k=klow,kup
                do 111 j=jbegin,jup
                   t=j+g
                   a1=xy(t,k,1)
                   a2=xy(t,k,2)
                   cs=sndsp(t,k)**2
                   s1=(spect(t,k,2)+spect(t,k,3)-2.0*spect(t,k,1))
     &                  /2.0*(gami/cs)
                   s2=(spect(t,k,2)-spect(t,k,3))/ccx(t,k)
                   u=q(t,k,2)/q(t,k,1)
                   v=q(t,k,3)/q(t,k,1)
                   phi=(u**2+v**2)/2
                   dot1=phi*work(j,k,1,i)
     &                   -u*work(j,k,2,i)
     &                   -v*work(j,k,3,i)
     &                  + 1.*work(j,k,4,i)
                   dot2=-uu(j,k)*work(j,k,1,i)
     &                       + a1*work(j,k,2,i)
     &                       + a2*work(j,k,3,i)
                   fact1=(s1*dot1+s2*dot2)
                   fact2=((s1*dot2)/(a1**2+a2**2)+s2*gami*dot1)
c                   h=(q(t,k,3)+press(t,k))*xyj(t,k)
                   h=(q(t,k,3)/q(t,k,1))+press(t,k)*xyj(t,k)
                   temp(j,k,1)=spect(j,k,1)*work(j,k,1,i)
     &                         +fact1
                   temp(j,k,2)=spect(j,k,1)*work(j,k,2,i)
     &                         +fact1*u
     &                         +fact2*a1
                   temp(j,k,3)=spect(j,k,1)*work(j,k,3,i)
     &                         +fact1*v
     &                         +fact2*a2
                   temp(j,k,4)=spect(j,k,1)*work(j,k,4,i)
     &                         +fact1*h
     &                         +fact2*uu(j,k)
 111         continue
c23456789 123456789 123456789 123456789 123456789 123456789 1234567890 
                   do 222 l=1,4
                      if(i.eq.1)then
c                         do 140 k=klow,kup
c                            do 140 j=jlow,jup
c                            t=j+g
c                            s(j,k,l)=s(j,k,l)+
c     &                      beta(j,k)*(temp(j,k,l)
c     &                     -temp(j-1,k,l))*dtd
c                            temp2(j,k,l)=temp2(j,k,l)+
c     &                      beta(j,k)*(temp(j,k,l)
c     &                     -temp(j-1,k,l))*dtd
c
c                            if (l.eq.ib) then
c                               budget2(j,k,1)=budget2(j,k,1)-
c matrix 2nd difference
c     &                      beta(t,k)*(temp(j,k,l)
c     &                     -temp(j-1,k,l))*dtd
c                               endif
c
c 140                     continue
                      else
                         do 150 k=klow,kup
                            do 150 j=jlow,jup
                            t=j+g
                            s(j,k,l)=s(j,k,l)-
     &                           (beta(t,k)*temp(j,k,l)
     &                           -beta(t-1,k)*temp(j-1,k,l))*dtd-
     &                           (alph(t,k)*work(j,k,l,i)-alph(t-1,k)
     $                           *work(j-1,k,l,i))*dtd
                            temp2(j,k,l)=temp2(j,k,l)-
     &                           (beta(t,k)*temp(j,k,l)
     &                           -beta(t-1,k)*temp(j-1,k,l))*dtd-(alph(t
     $                           ,k)*work(j,k,l,i)-alph(t-1,k)*work(j-1
     $                           ,k,l,i))*dtd

                            if (l.eq.ib) then
                               budget2(j,k,1)=budget2(j,k,1)-
     &                           (beta(t,k)*temp(j,k,l)
     &                           -beta(t-1,k)*temp(j-1,k,l))*dtd-(alph(t
     $                           ,k)*work(j,k,l,i)-alph(t-1,k)*work(j-1
     $                           ,k,l,i))*dtd

                            endif
 150                     continue
                   
                      endif
 222                  continue
 888               continue
                 if ((.not.wake).and.(mod(numiter,100).eq.0)) then
                         write(91,*)
     $                        'variables= "x","y","rho","xm","ym","e"'
                         write(91,*) 'zone f=point,i
     $                        =',jtail2-jtail1+1,', j=',kmax
                         do k=1,kmax
                            do j=jtail1,jtail2
                               write(91,3) x(j,k),y(j,k),temp2(j,k,1)
     $                              ,temp2(j,k,2),temp2(j,k,3),temp2(j,k
     $                              ,4)
                            end do
                         end do
                         rewind 91
 3                       format(6(e18.11,1x))
                      endif

                      return
                      end




















