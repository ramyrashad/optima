
Using the SNOPT mex Files in $SNOPT/matlab
==========================================

 Run Matlab in the directory $SNOPT/matlab.

 Typing
  >> more on
  >> help Contents
from Matlab provides an overview of the package.

 At the Matlab prompt, type

  >> runAllExamples

This script sets an appropriate matlab path.

The subdirectory  ./examples  contains sample problems that
demonstrate how to use the various snOpt Matlab interfaces.
Read the "Contents" for more information.

Problems may be run individually from $SNOPT/matlab as follows.

    >> setpath    % (if you haven't already called runAllExamples)

    >> addpath examples
    >> addpath examples/snmain
    >> snoptmain  % (this is one of the examples)
