!
! =====================================================================
!
module SolverInterface_Mod
!
! ---------------------------------------------------------------------
! Purpose:
!    Contains subroutines for calls to the flow solver
!
! Author:
!    Oleg Chernukhin, Sept 2010
!
! ---------------------------------------------------------------------

use CommonH
use Database_Mod
use Surrogate_Mod

implicit none

! ___________________
! module subroutines
contains
!
! ===================================================================
!
subroutine evalf(nX, X, nF, F, nG, G, needF, needG, stat, doSurr, &
                 usedb)
!
! ------------------------------------------------------------------- 
!
! Purpose:
!    Subroutine used for objective evaluation. It either calls
!    surrogate model to approximate objective value at X or
!    calls flow solver to find the exact objective value.
!    In the latter case, it can still avoid full flow solve by
!    getting the value from the database, if this has already
!    been evaluated.
!
! -------------------------------------------------------------------
! _______________________
! variable specification

   !-- subroutine arguments
   integer :: nX, nF, nG, stat
   logical, intent(in) :: needF, needG, doSurr, usedb
   real(kind=dp) :: X(nX), F(nF), G(nG)

   !-- local variables
   integer :: ii, stype
   logical :: existent, haveG, stop_exists, syncstop

! __________________________
! begin main execution

   stop_exists = .false.
   syncstop = .false.
   existent = .false.

   if (usedb) then
      !-- Read-in the database
      !-- Make sure everyone in global communicator calls this
      call readdb(nX, nF, nG)
 
      !-- Go to database and find out whether this flowsolve has been done
      call checkdbentry(nX, X, nF, F, haveG, nG, G, stat, existent)

      call mpi_bcast(existent, 1, MPI_LOGICAL, 0, comm(mycid), ierr)
   endif

   !-- Perform full flowsolve
   if (.not. existent) then
      if (doSurr) then
         !-- Calculate surrogate model
         stype = 1
         call evalSurrogate(nX, X, nF, F, nG, G, stat, stype)
      else
         !-- Actual flow solve
         call hu_ptr(X, nX, F, nF, G, nG, comm, popsize, mycid, &
                     fsn_base, stat, needF, needG)
         !-- Increment flow solve counter on local root
         if (myrank_loc == 0) ind_feval = ind_feval + 1
      endif
   endif

   call mpi_bcast(stat, 1, MPI_INTEGER, 0, comm(mycid), ierr)

   if (usedb .and. (.not. doSurr)) then
      !-- Update database with the new values
      call writedb(nX, X, nF, F, needG, nG, G, stat, existent)
   endif

   !-- Increment flow solve counters
   if (myrank_loc == 0) then
      fsn_base = fsn_base + popsize
   endif
   call mpi_bcast(fsn_base, 1, MPI_INTEGER, 0, comm(mycid), ierr)

   !-- Check for STOP file
   call mpi_barrier(comm(mycid), ierr)
   stop_exists = .false.
   inquire(file='STOP', exist=stop_exists)
   call mpi_barrier(comm(mycid), ierr)
   call mpi_allreduce(stop_exists, syncstop, 1, MPI_LOGICAL, &
                      MPI_LAND, comm(mycid), ierr)
   if (syncstop) then
      if (myrank_loc == 0) write (*,*) mycid, ': STOP file found'
      call mpi_barrier(HYBROPT_COMM, ierr)
      call mpi_abort(HYBROPT_COMM, 0, ierr)
   endif

   return

end subroutine
!
! ===================================================================
!
end module

