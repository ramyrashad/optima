C***********************************************************************
C***************** N INVERSE MULTIPLY ON S **************************** 
C***********************************************************************
      subroutine ninver(JDIM,KDIM,S,XY,IXX,IXY,IYX,IYY,
     &        JMAX,KMAX,JM,KM,JBEGIN,JEND,KBEGIN,KEND,
     &        JLOW,JUP,KLOW,KUP)
C                                                                       
C   IXX = 1, IXY = 2, IYX = 3, IYY = 4                                  
C          -1                                                           
C   N  =  T    T                                                        
c          xi   eta                                                     
C                                                                       
C   IXX = 3, IXY = 4, IYX = 1, IYY = 2                                  
C          -1                                                           
C   N  =  T    T                                                        
c          eta  xi                                                      
C                                                                       
#include "../include/common.inc"
c                                                                       
      DIMENSION S(JDIM,KDIM,4),XY(JDIM,KDIM,4)                          
c                                                                       
      DIMENSION RG(4,4),SR(4)                                           
C                                                                       
C  PARAMETERS FOR DIAGONALIZATION                                       
C                                                                       
      BT = 1./SQRT(2.)                                                  
      BT2 = 0.5                                                         
c                                                                       
        do 1 k = klow,kup                                               
          DO 1 J = jlow,jup                                             
           RR1   = XY(J,K,IXX)**2+XY(J,K,IXY)**2                        
           RR2   = XY(J,K,IYX)**2+XY(J,K,IYY)**2                        
           RR12   = 1./SQRT(RR1*RR2)                                    
c                                                                       
           RM1   = (XY(J,K,IXX)*XY(J,K,IYX)+XY(J,K,IXY)*XY(J,K,IYY))    
     *                                          *RR12                   
           RM2   = (XY(J,K,IXX)*XY(J,K,IYY)-XY(J,K,IXY)*XY(J,K,IYX))    
     *                                          *RR12                   
               S34M  = S(J,K,3) - S(J,K,4)                              
               S34P  = S(J,K,3) + S(J,K,4)                              
               S2 =  RM1*S(J,K,2)    + BT*S34M*RM2                      
               SS = BT*S(J,K,2)*RM2 - BT2*RM1*S34M                      
               ST = BT2*S34P                                            
               S3 = ST - SS                                             
               S4 = ST + SS                                             
               S(J,K,2) = S2                                            
               S(J,K,3) = S3                                            
               S(J,K,4) = S4                                            
1      continue                                                         
c                                                                       
         return                                                         
         end                                                            
