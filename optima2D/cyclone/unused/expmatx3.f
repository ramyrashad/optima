      subroutine expmatx3(jdim,kdim,q,coef2x,coef4x,sndsp,s,
     > spect,xyj,press,ccx,uu,xy,x,y,work,s1,s2,u,v,phi,h,temp,temp2)
c
      include '../include/arcom.inc'

      integer g,t
      dimension q(jdim,kdim,4),coef2x(jdim,kdim),coef4x(jdim,kdim)
      dimension sndsp(jdim,kdim),s(jdim,kdim,4),work(4,jdim,kdim,2)
      dimension spect(jdim,kdim,3),xyj(jdim,kdim),press(jdim,kdim)
      dimension ccx(jdim,kdim),uu(jdim,kdim),xy(jdim,kdim,4)
      dimension x(jdim,kdim),y(jdim,kdim)
      dimension temp(4,jdim,kdim),temp2(4,jdim,kdim)
      dimension s1(jdim,kdim),s2(jdim,kdim),u(jdim,kdim),v(jdim,kdim)
      dimension phi(jdim,kdim),h(jdim,kdim)
c
      dtd=dt/(1.+phidt)
c     (q(j+1)-q(j))
      do 25 i=1,4
         do 14 k=klow,kup
         do 14 j=jbegin,jup
            jp1=jplus(j)
            work(i,j,k,1)= q(jp1,k,i)*xyj(jp1,k) - q(j,k,i)*xyj(j,k)
 14      continue
c            
         if (.not.periodic) then
            do 15 k=klow,kup
               work(i,jend,k,1)=work(i,jend-1,k,1)
 15         continue
         endif
c     
c       (q(j+2)-3q(j+1)+3q(j)-q(j-1))
         do 18 k=klow,kup
         do 18 j=jlow,jup
         work(i,j,k,2)=work(i,j+1,k,1)-2.*work(i,j,k,1)+work(i,j-1,k,1)
  18     continue
c      
c        boundary conditions a la Kyle
         do 20 k=klow,kend
            work(i,jbegin,k,2)=q(jbegin+2,k,i)*xyj(jbegin+2,k)
     &                         -2.d0*q(jbegin+1,k,i)*xyj(jbegin+1,k)
     &                         +q(jbegin,k,i)*xyj(jbegin,k)
            work(i,jend,k,2)=0.0
 20      continue
 25   continue
c     
c     
      do 100 k=klow,kup
      do 100 j=jbegin,jend
         s1(j,k)=.5d0*(spect(j,k,2)+spect(j,k,3)-2.*spect(j,k,1))
         s2(j,k)=(spect(j,k,2)-spect(j,k,3))/
     &                                   (2.*ccx(j,k))
c     &                                   (2.*sndsp(j,k)*ccx(j,k))
         u(j,k)=q(j,k,2)/q(j,k,1)
         v(j,k)=q(j,k,3)/q(j,k,1)
         phi(j,k)=.5d0*(u(j,k)**2+v(j,k)**2)         
         h(j,k)=(q(j,k,4)+press(j,k))/q(j,k,1)
 100  continue
c     
      zdisx=0.d0
      do 888 g=0,1
         do 111 k=klow,kup
         do 111 j=jbegin,jup
            t=j+g
            include 'matloopx.inc'
 111     continue
c      
         do 222 n=1,4
            do 140 k=klow,kup
            do 140 j=jlow+1,jup-1
               t=j+g
c               if (n.eq.1) then
c               zdis = (
c     &         coef2x(t,k)*temp(n,j,k)-coef2x(t-1,k)*temp(n,j-1,k)
c     &    - (coef4x(t,k)*temp2(n,j,k)-coef4x(t-1,k)*temp2(n,j-1,k)))
c               zdisx = zdisx + zdis**2
c               endif
               s(j,k,n)=s(j,k,n) + dtd*(
     &         coef2x(t,k)*temp(n,j,k)-coef2x(t-1,k)*temp(n,j-1,k)
     &       - (coef4x(t,k)*temp2(n,j,k)-coef4x(t-1,k)*temp2(n,j-1,k)))
 140        continue
 222     continue
 888  continue
c
c
cc     Boundary Conditions
      do 2000 n=1,4
         do 2100 k=klow,kup
          j=jlow
c            j=jbegin
          work(n,jbegin,k,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j+1,k,n)*
     &                       xyj(j+1,k)-q(j+2,k,n)*xyj(j+2,k)
c          work(n,j,k,2)=q(j,k,n)*xyj(j,k)-3.d0*(q(j+1,k,n)*xyj(j+1,k)
c     &                  -q(j+2,k,n)*xyj(j+2,k))-q(j+3,k,n)*xyj(j+3,k)
          j=jbegin
c          j=jlow
          work(n,jlow,k,2)=-q(j,k,n)*xyj(j,k)+2.d0*q(j+1,k,n)*xyj(j+1,k)
     &                     -q(j+2,k,n)*xyj(j+2,k)
c          work(n,j,k,2)=q(j,k,n)*xyj(j,k)-3.d0*(q(j+1,k,n)*xyj(j+1,k)
c     &                  -q(j+2,k,n)*xyj(j+2,k))-q(j+3,k,n)*xyj(j+3,k)
          j=jup-1
          work(n,j,k,2)=-q(j-1,k,n)*xyj(j-1,k)+2.d0*q(j,k,n)*xyj(j,k)
     &                  -q(j+1,k,n)*xyj(j+1,k)
c          work(n,j,k,2)=q(j+1,k,n)*xyj(j+1,k)-3.d0*(q(j,k,n)*xyj(j,k)
c     &                  -q(j-1,k,n)*xyj(j-1,k))-q(j-2,k,n)*xyj(j-2,k)
          j=jup
          work(n,j,k,2)=-q(j-1,k,n)*xyj(j-1,k)+2.d0*q(j,k,n)*xyj(j,k)
     &                  -q(j+1,k,n)*xyj(j+1,k)
c          work(n,j,k,2)=q(j+1,k,n)*xyj(j+1,k)-3.d0*(q(j,k,n)*xyj(j,k)
c     &                  -q(j-1,k,n)*xyj(j-1,k))-q(j-2,k,n)*xyj(j-2,k)
 2100    continue
 2000 continue
c     
      do 2500 g=0,1
         do 2400 k=klow,kup
            do 2200 j=jbegin,jlow
               t=j+g
               include 'matloopx.inc'
 2200       continue
            do 2250 j=jup-1,jup
               t=j+g
               include 'matloopx.inc'
 2250       continue
c     
            do 2300 n=1,4
            do 2300 j=jlow,jup,jup-jlow
             t=j+g
c               if (n.eq.1) then
c               zdisx = zdisx + (
c     &         coef2x(t,k)*temp(n,j,k)-coef2x(t-1,k)*temp(n,j-1,k)
c     &    - (coef4x(t,k)*temp2(n,j,k)-coef4x(t-1,k)*temp2(n,j-1,k)))**2
c               endif
             s(j,k,n)=s(j,k,n) + dtd*(
     &       coef2x(t,k)*temp(n,j,k)-coef2x(t-1,k)*temp(n,j-1,k)
     &     - (coef4x(t,k)*temp2(n,j,k)-coef4x(t-1,k)*temp2(n,j-1,k)))
 2300       continue
 2400    continue
 2500 continue
c      zdisx=dsqrt(zdisx/dble((jup-jlow-1)*(kup-klow+1)))
c      if (numiter.eq.iend) 
c     &write (*,*) 'Xi density diss. norm =',zdisx
c
      return
      end
