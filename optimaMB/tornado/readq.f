C ********************************************************************
C ***        subroutine to input q                                 ***
C ********************************************************************
      subroutine readq(junit,q,jdim,kdim,j1,j2,k1,k2)

      implicit none
      integer junit,jdim,kdim,j1,j2,k1,k2,j,k,n
      double precision q(jdim,kdim,4)
      read(junit) (((q(j,k,n),j=j1,j2),k=k1,k2),n=1,4)
c
      return 
      end 
