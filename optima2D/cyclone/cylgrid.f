
!=======================================================================

      subroutine cylgrid(jdim,kdim,x,y,jmax,kmax,smax,ds)               

!-----------------------------------------------------------------------
!     | purpose: generates cylindrical grid
!     |
!     |    has overlap at j = 1 and jmax
!     |    if ds = 0.0  uniform grid in xi and eta                              
!     |    if ds > 0.0  stretched in xi and eta                                 
!     |    if ds < 0.0  uniform grid in xi and stretched in eta                 
!     |
!     |    Note: delta controls the clustering:
!     |    the smaller the delta, the finer clustering at the back
!     | 
!     | inputs: geometry, farfield location, offwall spacing
!     | outputs: grid (x,y)
!     | calling routine: initia
!     | modifications: M. Tabesh, October 2008
!-----------------------------------------------------------------------

#include "../include/parm.inc"
#include "../include/units.inc" 

      dimension x(jdim,kdim),y(jdim,kdim),vt(jdim),tet(jdim)
      common/worksp/r(maxj),work(maxj,29)

      pi = 4.d0*atan(1.d0)

      if(ds.gt.0.d0)then        ! stretched in xi and eta

!     creates r for polar coordinates
         epsi = epsil(smax-0.5d0,0.d0,ds,kmax,0.00004d0,20,1)
         r(1) = 0.5d0
         do k=2,kmax
            r(k) = r(k-1) + ds*(1.d0+epsi)**(k-2)
         enddo
!     creates theta for polar coordinates
         tmin  = 0.d0
         tmax  = 1.d0
         stept = (tmax-tmin)/(jmax-1)
         do j = 1,jmax
            vt(j) = tmin+(j-1)*stept
         enddo
         delta = 0.1d0         ! clustering parameter
         a     = 0.5d0
         h     = 2.d0*pi
         b     = (1-delta/h)**(-0.5d0)
         do j=1,jmax
            z1  = (b+1)/(b-1)
            z2  = (vt(j)-a)/(1-a)
            z3  = (b+2*a)*(z1**z2)-b+2*a
            z4  = (2*a+1)*(1+(z1**z2))
            tet(j) = -h*z3/z4
         enddo
!     creates the grid in (x,y)
         do j=1,jmax
            do k=1,kmax
               x(j,k)= r(k)*cos(tet(j))
               y(j,k)= r(k)*sin(tet(j))
            enddo
         enddo

      elseif(ds.eq.0.0)then     ! uniform grid

         delr = (smax-0.5)/(kmax-1)
         delt = 2.*pi/(jmax-1)
         do k = 1,kmax
            radius = delr*(k-1)+0.5
            do j = 1,jmax
               theta = -delt*(j-1)
               x(j,k) = radius*cos(theta)
               y(j,k) = radius*sin(theta)
            enddo
         enddo

      elseif(ds.lt.0.0)then     ! uniform grid in xi and stretched in eta

         dsa = abs(ds)
         epsi = epsil(smax-.5,0.0,dsa,kmax,0.00004,20,1)
         r(1) = .5d0
         do k=2,kmax
            r(k) = r(k-1) + dsa*(1.+epsi)**(k-2)
         enddo
         delt = 2.*pi/(jmax-1)
         do j=1,jmax
            do k=1,kmax
               theta = -delt*(j-1)
               x(j,k)= r(k)*cos(theta)
               y(j,k)= r(k)*sin(theta)
            enddo
         enddo

      endif

c     fix cut to be exact

      do k = 1,kmax
         x(jmax,k) = x(1,k)
         y(jmax,k) = y(1,k)
      enddo

c     output to cylgrd_unit
c     this insures that grid sequencing will work

      write(cylgrd_unit) jmax,kmax
      write(cylgrd_unit)  ( ( x(j,k),j=1,jmax) ,k=1,kmax),
     &                    ( ( y(j,k),j=1,jmax) ,k=1,kmax)

      return
      end                       ! subroutine cylgrid
