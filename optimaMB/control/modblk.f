c----------------------------------------------------------------------
c     -- modify grid blocks with sides next to airfoil --
c     -- m. nemec, may 2001 --
c----------------------------------------------------------------------
      subroutine modblk(node, ifoil, jdim, kdim, jminb, jmaxb, kminb,
     &     kmaxb, iside, x, y, xo, yo, seg, xandy)

      implicit none

#include "../include/common.inc"

      integer node, ifoil, jdim, kdim, jminb, jmaxb, kminb, kmaxb, iside
      integer ip, j, k, js, je, ks, ke, ixy, istep
      double precision x(jdim,kdim), y(jdim,kdim)
      double precision xo(jdim,kdim), yo(jdim,kdim)
      double precision seg(jdim*kdim) 
      logical xandy

      do j=jminb,jmaxb
         do k=kminb,kmaxb
            xo(j,k) = x(j,k)
            yo(j,k) = y(j,k)
         end do
      end do

c     -- side # 1 --

      if (iside.eq.1) then

c     -- adjusting normal direction --
         js = jminb
         je = jmaxb
         ks = kminb
         ke = kmaxb
         istep = 1
         ixy = 2

         call adjgrd(node, ip, ifoil, js, je, ks, ke, istep, ixy, jdim,
     &        kdim, y, xo, yo, seg)

c     -- adjusting streamwise direction --
         if (xandy) then
            ixy = 1
            call adjgrd(node, ip, ifoil, js, je, ks, ke, istep, ixy,
     &           jdim, kdim, x, xo, yo, seg)
         end if

         node = ip - 1

c     -- side # 2 --

      else if (iside.eq.2) then
c     -- modify grid block with side 2 next to the airfoil (BTE) --
c     -- adjusting streamwise direction --
         js = jminb
         je = jmaxb
         ks = kmaxb
         ke = kminb
         istep = -1
         ixy = 1

         call adjgrdsd2(node, ip, ifoil, js, je, ks, ke, istep, ixy, 
     &        jdim, kdim, x, xo, yo, seg)

c     -- adjusting normal direction --
         if (xandy) then
            ixy = 2
            call adjgrdsd2(node, ip, ifoil, js, je, ks, ke, istep, ixy, 
     &           jdim, kdim, y, xo, yo, seg)
         end if
         node = ip - 1

c     -- side # 3 --

      else if (iside.eq.3) then

c     -- adjusting normal direction --
         js = jmaxb
         je = jminb
         ks = kmaxb
         if ( klineconst .eq. 0) then
            ke = kminb
         else
            ke=klineconst
         end if
         istep = -1
         ixy = 2

         call adjgrd(node, ip, ifoil, js, je, ks, ke, istep, ixy, jdim,
     &        kdim, y, xo, yo, seg)

c     -- adjusting streamwise direction --
         if (xandy) then
            ixy = 1
            call adjgrd(node, ip, ifoil, js, je, ks, ke, istep, ixy,
     &           jdim, kdim, x, xo, yo, seg)
         end if

         node = ip - 1
         
c     -- side # 4 --

      else if (iside.eq.4) then
c     -- modify grid block with side 4 next to the airfoil (BTE) --
         write(*,*) 'modblk: Warning, not tested for side 4'
         js = jmaxb
         je = jminb
         ks = kminb
         ke = kmaxb
         istep = 1
         ixy = 1
         call adjgrdsd2(node, ip, ifoil, js, je, ks, ke, istep, ixy, 
     &        jdim, kdim, x, xo, yo, seg)

         if (xandy) then
            ixy = 2
            call adjgrdsd2(node, ip, ifoil, js, je, ks, ke, istep, ixy, 
     &           jdim, kdim, y, xo, yo, seg)
         end if
         node = ip - 1

      end if

      return
      end                       !modblk
