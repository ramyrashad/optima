C     ********************************************************************
C     ***************** INITIALIZE INDICES, ZERO VARIABLES
C     ***************** Copy halo points onto neighbors
C     ***************** COMPUTE METRICS, SET UP CONSTANTS
C     ********************************************************************
c     calling subroutine: initia

      subroutine setup( coef2x, coef2y, vk, ve, s, xy, xyj,
     &     xit, ett, ds, x, y, turmu, fmu, vort)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"

      integer nbs,ii,kk
      double precision s(maxjkq),xy(maxjkq),xyj(maxjk),turmu(maxjk)           
      double precision xit(maxjk),ett(maxjk),ds(maxjk),vort(maxjk)          
      double precision x(maxjk),y(maxjk),vk(maxjk),ve(maxjk)
      double precision fmu(maxjk),coef2x(maxjk),coef2y(maxjk)

      if (mg .or. gseq) then
         nbs=nblkstot
      else
         nbs=nblks
      endif
      

c     ********************************************************************
c     initstuf       initialize turmu, vort and fmu
c                    initialize time metrics and variable dt
c     xymets         metric calculation
c                    s used for work space here
c     resets         reset s to zero  --- this is necessary
c     ********************************************************************

      do ii=1,nbs
         call initstuf(jmax(ii),kmax(ii),turmu(lgptr(ii)),
     &        vort(lgptr(ii)),fmu(lgptr(ii)),
     &        coef2x(lgptr(ii)),coef2y(lgptr(ii)),
     &        vk(lgptr(ii)),ve(lgptr(ii)),
     &        xit(lgptr(ii)),ett(lgptr(ii)),ds(lgptr(ii)))
         do kk=1,4
c     sd: ibctype=1 indicates side is part of airfoil/wall surface
            if((ibctype(ii,kk).eq.1).and.(ibcdir(ii,kk).ne.0)) then
               bnd(kk,ii) = .true.
            else
               bnd(kk,ii) = .false.
            end if
         end do

         call xymets(ii,nblks,nhalo,jmax(ii),kmax(ii),
     &        x(lgptr(ii)),y(lgptr(ii)),xy(lqptr(ii)),
     &        xit(lgptr(ii)),ett(lgptr(ii)),xyj(lgptr(ii)), 
     &        jbegin(ii),jend(ii),kbegin(ii),kend(ii),
     &        jminbnd(ii),jmaxbnd(ii),kminbnd(ii),kmaxbnd(ii))

c     -- detected negative Jacobian, optimization run --
         if (badjac) return

         call resets(jmax(ii),kmax(ii),s(lqptr(ii)))
      end do

c     note: metrics are copied back in routine initia 
      
      return                                                            
      end                       !setup

      subroutine halopt(jmax1,kmax1,x1,y1,jmax2,kmax2,x2,y2,j1,k1,j2,k2)
      implicit none
      integer jmax1,kmax1,jmax2,kmax2,j1,k1,j2,k2
      double precision x1(jmax1,kmax1), y1(jmax1,kmax1)
      double precision x2(jmax2,kmax2), y2(jmax2,kmax2)
      x1(j1,k1) = x2(j2,k2)
      y1(j1,k1) = y2(j2,k2)
      return
      end
