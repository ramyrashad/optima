c----------------------------------------------------------------------
c     -- subroutine to get airfoil coordinates from block --
c     -- m. nemec, june 2001
c----------------------------------------------------------------------

c----------------------------------------------------------------------
c     Major modification are made to this subroutine
c
c     1. the first point of each surface is ignored.  The do loop start 
c        from the second points of each surface, except for the very
c        first point of the first surface.  This is to prevent a problem 
c        that occurs when the corner points of two connecting blocks are
c        farther than 1.d-12.  
c
c     2. codes for side 2 and side 4 are modified from difference of X 
c        value to the difference of Y value
c
c     -- P.Y. Jay Liu -- June, 2004
c
c----------------------------------------------------------------------

      subroutine getxy(ipoints, jdim, kdim, jbegin, jend, kbegin, kend,
     &     iside, x, y, bap, ibody)
c     
      implicit none
c     
      integer ipoints, jdim, kdim, jbegin, jend, kbegin, kend, iside
      integer j, k, ibody
c     
      double precision x(jdim,kdim), y(jdim,kdim), bap(ibody,2) 
c     
c     -- side # 1 --
c     
      if (iside.eq.1) then
         k = kbegin
         if (ipoints.eq.1) then
            j = jbegin
            bap(ipoints,1) = x(j,k)
            bap(ipoints,2) = y(j,k)
            ipoints = ipoints + 1
         endif
         do j = jbegin+1,jend
            if ( abs(bap(ipoints-1,1)-x(j,k)).gt.1.d-12) then
               bap(ipoints,1) = x(j,k)
               bap(ipoints,2) = y(j,k)
               ipoints = ipoints + 1
            end if
         end do
c     
c     -- side # 2 --
c     
      else if (iside.eq.2) then
         j = jbegin
         if (ipoints.eq.1) then
            k = kend
            bap(ipoints,1) = x(j,k)
            bap(ipoints,2) = y(j,k)
            ipoints = ipoints + 1
         endif
         do k = kend-1,kbegin,-1
            if ( abs(bap(ipoints-1,2)-y(j,k)).gt.1.d-12) then
               bap(ipoints,1) = x(j,k)
               bap(ipoints,2) = y(j,k)
               ipoints = ipoints + 1
            end if
         end do
c     
c     -- side # 3 --
c     
      else if (iside.eq.3) then
         k = kend
         if (ipoints.eq.1) then
            j = jend
            bap(ipoints,1) = x(j,k)
            bap(ipoints,2) = y(j,k)
            ipoints = ipoints + 1
         endif
         do j = jend-1,jbegin,-1
            if ( abs(bap(ipoints-1,1)-x(j,k)).gt.1.d-12) then
               bap(ipoints,1) = x(j,k)
               bap(ipoints,2) = y(j,k)
               ipoints = ipoints + 1
            end if
         end do
c     
c     -- side # 4 --
c     
      else if (iside.eq.4) then
         j = jend
         if (ipoints.eq.1) then
            k = kbegin
            bap(ipoints,1) = x(j,k)
            bap(ipoints,2) = y(j,k)
            ipoints = ipoints + 1
         endif
         do k=kbegin+1,kend 
            if ( abs(bap(ipoints-1,1)-x(j,k)).gt.1.d-12) then
               bap(ipoints,1) = x(j,k)
               bap(ipoints,2) = y(j,k)
               ipoints = ipoints + 1
            end if
         end do
c     
      end if
c     
      return
      end                       !getxy
