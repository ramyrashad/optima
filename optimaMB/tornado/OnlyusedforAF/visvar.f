C***********************************************************************
C*************** VISCOUS VARIABLES   ***********************************
C***********************************************************************
      subroutine visvar(jdim,kdim,q,press,vort,turmu,fmu,x,y,xy,xyj)
      parameter (maxj=497, maxk=97)
c
#include "../include/common.inc"
#include "../include/units.inc"
c
      dimension q(jdim,kdim,4),turmu(jdim,kdim),vort(jdim,kdim)
      dimension press(jdim,kdim),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension fmu(jdim,kdim)
      dimension x(jdim,kdim),y(jdim,kdim)
c
      dimension snor(maxj),tas(maxj),work(maxj,3)
      common/worksp/snor,tas,work
c
      common/turout/tauh(maxj), ustar(maxj),
     >              yplus(maxj, maxk), uplus(maxj, maxk)
c
      do 80 j = jtail1,jtail2
c
c     find vorticity tas(k) and total velocity utot(k)
c
        do 11 k = kbegin,kup
         tas(k)     = vort(j,k)
         snor(k)    = 0.0
   11   continue
c
c      compute normal distance snor(k)
c
        snor(1) = 0.
        do 20 k = klow,kup
         scis = abs(xy(j,k-1,3)*xy(j,k,3)+xy(j,k-1,4)*xy(j,k,4))
         scal = 1.0/sqrt(scis)
         snor(k) = snor(k-1) + scal
   20   continue
c
c            compute ustar = int| sqrt(rho/rhowall)*du| 0->u
c            at node points
c
      tauh(j) = .5*(fmu(j,1)+fmu(j,2))*tas(1)/re
      rhoa = .5*(q(j,2,1)*xyj(j,2) + q(j,1,1)*xyj(j,1))
      uf2  = abs(tauh(j)/rhoa) + 1.e-20
      ustar(1) = 0.0
      rhow = q(j,1,1)*xyj(j,1)
      do 887 k=klow, kend
      rhoav = .5*(q(j,k,1)*xyj(j,k) + q(j,k-1,1)*xyj(j,k-1) )
      ustar(k) = ustar(k-1) + sqrt(rhoav/rhow)*( q(j,k,2)/q(j,k,1)
     >    - q(j,k-1,2)/q(j,k-1,1) )
887   continue
c
c               yplus - uplus at grid points dump
c
      do 85 k=kbegin, kup
        snorm = snor(k)
        yplus(j,k) = sqrt(uf2)*re*snorm*rhoa/fmu(j,1)
        uplus(j,k) = ustar(k)/sqrt(uf2)
   85 continue
c
   80 continue
c
      write(n_out,*)' ++++++++++++++++ viscous output ++++++++++++++'
      k = 2
      write(n_out,100)
100   format(1h0, '  j  ',5x,'x',9x,'y',9x,'tauh',9x,
     >            'yplus(k=2)',6x,'uplus(k=2)')
      do 90 j = jtail1,jtail2
      write(n_out,101)
     >     j,x(j,k),y(j,k),tauh(j),yplus(j,k),uplus(j,k)
90    continue
101   format(1h ,i5,2(2x,f8.4),3(2x,e12.6))
      write(n_out,*)' ++++++++++++++++++++++++++++++++++++++++++++++'
      return
      end
