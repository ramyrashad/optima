c *********************************************************************
c ** This routine finds the starting and ending pointers for the     **
c ** (x,y) pair that define the start and end of a side along a foil **
c ** Note that the sign convention is positive clockwize             **
c *********************************************************************
c calling routine: sortfoils
c
c
      subroutine getbounds(iblk,iside,jkstart,jkend)

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer iblk,iside,jkstart,jkend,i0,jjmp
c
      i0 = lgptr(iblk)+nhalo*(jbmax(iblk)+2*nhalo)+nhalo-1
      jjmp = jbmax(iblk) + 2*nhalo
c
      if (iside.eq.1) then
        jkstart = i0 + 1
        jkend = i0 + jbmax(iblk)
c       isegdir = 1
      elseif (iside.eq.2) then
        jkend = i0 + 1 + (kbmax(iblk)-1)*jjmp
        jkstart = i0 + 1
c       isegdir = -1
      elseif (iside.eq.3) then
csd     I think you should use the next two lines but then there is
csd     a problem in autostan ... I think.  24/02/2000
c        jkend = i0 + jbmax(iblk) + (kbmax(iblk)-1)*jjmp
c        jkstart = i0 + 1 + (kbmax(iblk)-1)*jjmp
        jkstart = i0 + jbmax(iblk) + (kbmax(iblk)-1)*jjmp
        jkend = i0 + 1 + (kbmax(iblk)-1)*jjmp
c       isegdir = -1
      elseif (iside.eq.4) then
        jkstart = i0 + jbmax(iblk)
        jkend = i0 + jbmax(iblk) + (kbmax(iblk)-1)*jjmp
c       isegdir = 1
      endif
c
      return
      end
