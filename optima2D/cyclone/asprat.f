
c #########################
c ##                     ##
c ##  subroutine asprat  ##
c ##                     ##
c #########################

      subroutine asprat(jdim,kdim,x,y)
c
c*********************************************************************
c  Computes the average and the maximum aspect ration of the cells on 
c  a given structured grid.
c*********************************************************************
c     
c
#include "../include/units.inc"

      dimension x(jdim,kdim),y(jdim,kdim)

      asravg = 0.d0
      asrmax = 0.d0
      do 10 k=1,kdim-1                                            
      do 10 j=1,jdim-1                                            
         xl1 = sqrt ((x(j+1,k)-x(j,k))**2+(y(j+1,k)-y(j,k))**2)
         xl2 = sqrt ((x(j+1,k+1)-x(j,k+1))**2+(y(j+1,k+1)-y(j,k+1))**2)
         xl = .5d0*(xl1+xl2)
         yl1 = sqrt ((x(j,k+1)-x(j,k))**2+(y(j,k+1)-y(j,k))**2)
         yl2 = sqrt ((x(j+1,k+1)-x(j+1,k))**2+(y(j+1,k+1)-y(j+1,k))**2)
         yl = .5d0*(yl1+yl2)
         asr = abs(xl)/abs(yl)
         if (asr.lt.1.d0) asr = 1.d0/asr
         asravg = asravg + asr
         if (asrmax.lt.asr) then
            asrmax = asr
            jmxar = j
            kmxar = k
         endif
 10   continue
      asravg = asravg/((jdim-1)*(kdim-1))
c
c
      write (out_unit,*)
     &      ' ---------------------------------------------------'
      write (out_unit,11) asravg,asrmax
 11   format(11H |  asravg=,f8.1,5x,7Hasrmax=,f11.1,10x,2H |)
      write (out_unit,12)
 12   format (54H |  Maximum aspect ratio occured at node ...         |)
      write (out_unit,13) jmxar,kmxar
 13   format(11H |       j=,I3,3x,2Hk=,I3,30x,2H |)
      write (out_unit,*)
     &      ' ---------------------------------------------------'

      return                                                            
      end                                                               
