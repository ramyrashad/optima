c ######################
c ##                  ##
c ##  subroutine ilu  ##
c ##                  ##
c ######################
c
      subroutine ilup(n,p,a,ja,ia,alu,jlu,ju,wu,lf,nzl,nzn,nzt,ierr,nz)
c
c***********************************************************************
c                    ***   ilu(p) preconditioner.   ***                
c
c Data structure of the L+U matrix is saved in MCSR format.
c
c Author: Alberto Pueyo
c Date..: July 1995
c- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c
c on entry:
c---------
c n       = dimension of matrix
c p       = fill in level
c a, ja,
c ia      = original matrix in compressed sparse row storage.
c
c on return:
c-----------
c alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
c           the L and U factors together. The diagonal (stored in
c           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
c           contains the i-th row of L (excluding the diagonal entry=1)
c           followed by the i-th row of U.
c
c ju	  = pointer to the diagonal elements in alu, jlu.
c
c ierr	  = integer indicating error code on return
c	     ierr = 0 --> normal return
c	     ierr = k --> code encountered a zero pivot at step k.
c work arrays:
c-------------
c wu        = real work array of length n.
c lf	    = integer work array of length same as alu,jlu.
c------------
c IMPORTANT
c-----------
c it is assumed that the the elements in the input matrix are stored
c    in such a way that in each row the lower part comes first and
c    then the upper part. To get the correct ILU factorization, it is
c    also necessary to have the elements of L sorted by increasing
c    column number. It may therefore be necessary to sort the
c    elements of a, ja, ia prior to calling ilu0. This can be
c    achieved by transposing the matrix twice using csrcsc.
c
c***********************************************************************
c
c
c      implicit real*8 (a-h,o-z)
      implicit none
      real*8 a(*),alu(*),wu(*),tl
      integer ja(*),ia(*),ju(*),jlu(*),lf(*),nzl(*),nzn(*),nzt(*)
      integer n,p,ierr,ju0,i,ii,il,in,i2,js,j,jcol,jj,nz,
     >        nzl0,nzn0,nzt0
      integer nnzl,nnzu
c
      ju0 = n+2
      jlu(1) = ju0
c
c---- initialize work vectors to zero's ----
      do 10 i = 1,n
         wu(i) = 0.d0
         lf(i) = 99999
 10   continue
c
c
c---- store first line in mscr format ----
c  -- invert and store diagonal element --
      if (a(1) .eq. 0.0d0) goto 600
      alu(1) = 1.0d0/a(1)
      ju(1)  = ju0
c  -- rest of the elements and level-fill_in --
      do 11 j = 2,ia(2)-1  
         lf(ju0)  = 0
         alu(ju0) = a(j)
         jlu(ju0) = ja(j)
         ju0 = ju0+1
 11   continue
      jlu(2) = ju0
c
c
c
c                             main loop
c========================================================================
                         do 2 ii = 2,n
c
c
c---- copy row ii of a into wu ----
      i = 1
      do 20 j =ia(ii),ia(ii+1)-1
         jcol = ja(j)
         nzl(i)   = jcol
         i = i+1
         wu(jcol) = a(j)
         lf(jcol) = 0
 20   continue
      nzl0 = i-1
c
c
c---- compute pivots (line ii of L) IF needed ----
      if (nzl(1).ne.ii) then
      i = 1
 21   jj = nzl(i)
      tl = wu(jj)*alu(jj)
      wu(jj) = tl
c
c
c---- perform linear combination (line ii) ----
      in = 1
      do 210 j = ju(jj),jlu(jj+1)-1
         jcol = jlu(j)
c         if (wu(jcol) .ne. 0.d0) then
         if (lf(jcol) .le. p) then
            wu(jcol) = wu(jcol) - tl*alu(j)
         else
            lf(jcol) = lf(jj) + lf(j) + 1
            if (lf(jcol).le.p) then
               wu(jcol) = -tl*alu(j)
               nzn(in)  = jcol
               in = in+1
            endif
         endif
 210  continue
      nzn0 = in-1
c
c
c---- adding the new NZ to the list ----
c  -- total number of NZ --
      nzt0 =  nzl0 + nzn0
c  -- merge nzl and nzn into nzt --
      il = 1
      in = 1
      nzl(nzl0+1) = n+1
      nzn(nzn0+1) = n+1
      do 211 i2 = 1,nzt0
         if (nzl(il).lt.nzn(in)) then
            nzt(i2) = nzl(il)
            il = il+1
         else 
            nzt(i2) = nzn(in)
            in = in+1
         endif
 211  continue
c  -- copy nzt into nzl --
      nzl0 = nzt0
      do 212 in = 1,nzt0
         nzl(in) = nzt(in)
         nzt(in) = 0
 212  continue
c---- new pivot ----
      i = i+1
      if (nzl(i).lt.ii) goto 21
      endif
c
c
c---- store line ii in alu in MCSR format ---
      do 23 i = 1,nzl0
      jj = nzl(i)
c  -- invert and store diagonal element --
      if (jj .eq. ii) then
         if (wu(jj) .eq. 0.0d0) goto 600
         alu(ii) = 1.0d0/wu(jj)
         ju(ii)  = ju0
c  -- rest of the elements and level-fill_in --
      else if (wu(jj).ne.0.d0) then
c         else if (dabs(wu(jj)).gt.1.d-17) then
         lf(ju0)  = lf(jj)
         alu(ju0) = wu(jj)
         jlu(ju0) = jj
         ju0 = ju0+1
      endif
 23   continue
      jlu(ii+1) = ju0
c
c
c---- reset wu to zero ----
      do 22  j = jlu(ii),jlu(ii+1)-1
         jj = jlu(j)
         wu(jj) = 0.d0
c         lf(jj) = 0
         lf(jj) = 99999
 22   continue
      wu(ii) = 0.d0
c      lf(ii) = 0
      lf(ii) = 99999
c
c
 2                                continue
c
cc---- compute number of NZ ----
cc  -- total --
      nz = ju0-2
c      nnzl = 0
c      nnzu = 0
c      do 40 ii = 1,n
c         nnzl = nnzl + ju(ii) - jlu(ii)
c         nnzu = nnzu + jlu(ii+1) - ju(ii) + 1
c 40   continue
c      print *,p,nz,nnzl,nnzu,nnzl+nnzu
c
c---- return statements ----
c  -- normal return --  
      ierr = 0
      return
c  -- zero pivot --  
 600  ierr = ii
      return
c
c
      end
c
