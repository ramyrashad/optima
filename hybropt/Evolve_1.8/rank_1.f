C
C----------------------------------------------------------------------C
C
      SUBROUTINE RANK_1(nchrom,nobfun,fit,irank,i_mnx)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      dimension fit(nobfun,nchrom),irank(nchrom)
C
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** COMPUTE RANKING ARRAY (IRANK) ***
C
      DO NC=1,NCHROM
         IRANK(NC) = 0
      ENDDO
      DO ICOUNT=1,NCHROM
         INC = 0
         DO NC=1,NCHROM
            IF (IRANK(NC).NE.0) THEN
               INC = INC+1
               GO TO 1001
            ENDIF
            DO NCC=1,NCHROM
               IF (IRANK(NCC).NE.0.AND.IRANK(NCC).NE.ICOUNT) GO TO 1002
               INOF1 = 0
               INOF2 = 0
               DO NO=1,NOBFUN
                  IF (FIT(NO,NC).LE.FIT(NO,NCC).and.i_mnx.eq.1) THEN
                     INOF1 = INOF1+1
                  elseIF (FIT(NO,NC).ge.FIT(NO,NCC).and.i_mnx.eq.0) THEN
                     INOF1 = INOF1+1
                  ENDIF
                  IF (FIT(NO,NC).LT.FIT(NO,NCC).and.i_mnx.eq.1) THEN
                     INOF2 = 1
                  elseIF (FIT(NO,NC).gT.FIT(NO,NCC).and.i_mnx.eq.0) THEN
                     INOF2 = 1
                  ENDIF
               ENDDO
               IF (INOF1.EQ.NOBFUN.AND.INOF2.EQ.1) GO TO 1001
 1002          CONTINUE
            ENDDO
            IRANK(NC) = ICOUNT
 1001       CONTINUE
         ENDDO
         IF (INC.EQ.NCHROM) GO TO 1003
      ENDDO
 1003 CONTINUE

C
C-----------------------------------------------------------------------
C
      RETURN
      END
