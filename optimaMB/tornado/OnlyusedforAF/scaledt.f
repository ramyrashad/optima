C ********************************************************************
c ************  SCALE RHS WITH VARIABLE DT     ***********************
C ********************************************************************

      subroutine scaledt(jdim,kdim,s,ds,pk,jbegin,jend,kbegin,kend)
c                                                                       
#include "../include/common.inc"
c                                                                       
      dimension s(jdim,kdim,4),ds(jdim,kdim),pk(jdim,kdim,4)
c                                                                       
c                                                                       
c     scale with spatially variable time step                              
c                                                                       
      if (mg) then
        do 5 m = 1,4
        do 5 k = kbegin,kend
        do 5 j = jbegin,jend
          s(j,k,m) = (s(j,k,m) + pk(j,k,m))*ds(j,k)
 5     continue
      else
        do 10 m = 1,4
        do 10 k = kbegin,kend
        do 10 j = jbegin,jend
          s(j,k,m) = s(j,k,m)*ds(j,k)
 10     continue
      endif
c                                                                       
      return                                                            
      end                                                               
