      subroutine bcfar(jdim,kdim,q,turre,vk,ve,xy,xit,ett,xyj,x,y,
     &                  index1,index2,ibc,idir,iside)              
c
#include "../include/common.inc"
c                                                                       
      dimension q(jdim,kdim,4),turre(jdim,kdim)
      dimension vk(jdim,kdim),ve(jdim,kdim)
      dimension xy(jdim,kdim,4),xyj(jdim,kdim)                          
      dimension xit(jdim,kdim),ett(jdim,kdim)                           
      dimension x(jdim,kdim),y(jdim,kdim)                               
c
      dimension vinff(4),vint(4),vinfp(4),vintp(4),qbar(4),qdif(4)
      dimension vec(4),xx(4,4),tmp(4)
c     -far field bc for o mesh outer boundary and c mesh k = kmax          
c                                                                       
c   .....................................................................  
c     Far field circulation based on potential vortex added to reduce      
c     the dependency on outer boundary location.  works well.  for         
c     instance, a naca0012 at m=0.63 a=2. shows no dependency on ob from   
c     4 - 96 chords.                                                       
c   .....................................................................  
c                                                                       
      gi = 1.0/gamma                                                 
      gm1i = 1.0/gami                                                
c                                                                    
      alphar = alpha*pi/180.0
c      cosang = cos( alphar )                                         
c      sinang = sin( alphar )                                         
c      ainf = dsqrt(gamma*pinf/rhoinf)                                 
      hstfs = 1.0/gami + 0.50*fsmach**2                                
c                                                                       
c                                                                       
c     for Vn to point outward at farfield, sgn should be +1
c      sgn=1.d0
      sgn=-float(idir)
c
      goto (100,200,100,200) iside
c
 100  continue
c     ****************************************
c     * side 1, k=kbegin  or side 3, k=kend  *
c     ****************************************
      k = ibc
      kq= ibc
      kadd = idir
      j1=index1
      j2=index2
      if (prec.eq.0) then
        do 60 j = j1,j2                                                
c                                                                      
c         -reset free stream values with circulation correction         
c         -this if statement is here so that when I test for uniform
c          flow convergence I don't get a floating point exception when
c          using this routine for the body too.
c         -circul should be set to false when testing uniform flow.
          if (circul) then
            xa = x(j,k) - chord/4.d0
            ya = y(j,k)
            radius = dsqrt(xa**2+ya**2)
            angl = atan2(ya,xa)
            cjam = cos(angl)
            sjam = sin(angl)
            qcirc =circb/(radius*(1.d0-(fsmach*sin(angl-alphar))**2))
            uf = uinf + qcirc*sjam
            vf = vinf - qcirc*cjam
          else
            uf = uinf
            vf = vinf
          endif
          af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))
          af = sqrt(af2)
c                                                         
c         -metrics for normals
          rx=xy(j,k,3)
          ry=xy(j,k,4)
          rxy2=1.d0/dsqrt(rx**2+ry**2)
c            print *,'rxy2=',j,rxy2
          rx=rx*rxy2*sgn
          ry=ry*rxy2*sgn
c
c         -get extrapolated variables                                
          if (iord.eq.4) then
            kq=k+kadd
            xyj2 = xyj(j,kq)
            rho2 = q(j,kq,1)*xyj2                                     
            rinv = 1.d0/q(j,kq,1)                                       
            u2 = q(j,kq,2)*rinv                                        
            v2 = q(j,kq,3)*rinv
            e2 = q(j,kq,4)*xyj2
            p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c
            kq=k+2*kadd
            xyj3 = xyj(j,kq)
            rho3 = q(j,kq,1)*xyj3
            rinv = 1.d0/q(j,kq,1)                                       
            u3 = q(j,kq,2)*rinv                                        
            v3 = q(j,kq,3)*rinv                                       
            e3 = q(j,kq,4)*xyj3
            p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c     
            kq=k+3*kadd
            xyj4 = xyj(j,kq)
            rho4 = q(j,kq,1)*xyj4
            rinv = 1.d0/q(j,kq,1)
            u4 = q(j,kq,2)*rinv                                        
            v4 = q(j,kq,3)*rinv                                       
            e4 = q(j,kq,4)*xyj4
            p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c     
c            kq=k+4*kadd
c            xyj5 = xyj(j,kq)                                           
c            rho5 = q(j,kq,1)*xyj5
c            rinv = 1.d0/q(j,kq,1)                                     
c            u5 = q(j,kq,2)*rinv                                        
c            v5 = q(j,kq,3)*rinv                                       
c            e5 = q(j,kq,4)*xyj5
c            p5 = gami*(e5 - 0.5d0*rho5*(u5**2+v5**2))
c              
c           -3rd order extrapolation
c            rhoext= 4.d0*(rho2+rho4) - 6.d0*rho3 -rho5
c            uext= 4.d0*(u2+u4) -6.d0*u3 -u5
c            vext= 4.d0*(v2+v4) -6.d0*v3 -v5
c            pext= 4.d0*(p2+p4) -6.d0*p3 -p5
c           -2nd order extrapolation
            rhoext= 3.d0*(rho2 - rho3) + rho4
            uext= 3.d0*(u2 - u3) + u4
            vext= 3.d0*(v2 - v3) + v4
            pext= 3.d0*(p2 - p3) + p4
c           -1st order extrapolation
c            rhoext= 2.d0*rho2 - rho3
c            uext= 2.d0*u2 - u3
c            vext= 2.d0*v2 - v3
c            pext= 2.d0*p2 - p3
c           -Zeroeth order extrapolation
c            rhoext= rho2
c            uext= u2
c            vext= v2
c            pext= p2
          else
            kq=k+kadd
            xyj2 = xyj(j,kq)
            rho2 = q(j,kq,1)*xyj2                                     
            rinv = 1.d0/q(j,kq,1)                                       
            u2 = q(j,kq,2)*rinv                                        
            v2 = q(j,kq,3)*rinv
            e2 = q(j,kq,4)*xyj2
            p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c
c            kq=k+2*kadd
c            xyj3 = xyj(j,kq)                                           
c            rho3 = q(j,kq,1)*xyj3
c            rinv = 1.d0/q(j,kq,1)                                       
c            u3 = q(j,kq,2)*rinv                                        
c            v3 = q(j,kq,3)*rinv                                       
c            e3 = q(j,kq,4)*xyj3
c            p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c           
c           -1st order extrapolation
c            rhoext= 2.d0*rho2 - rho3
c            uext= 2.d0*u2 - u3
c            vext= 2.d0*v2 - v3
c            pext= 2.d0*p2 - p3
c           -Zeroth-order extrapolation
            rhoext= rho2
            uext= u2
            vext= v2
            pext= p2
          endif
c          entext=pext/rhoext**gamma
          hext= gamma*pext/rhoext/gami + 0.5d0*(uext**2+vext**2)
          a2ext= gamma*pext/rhoext
c                                                                 
          if (turbulnt) then
c           -determine the riemann invarients,
c                                     r1 far field, r2 extrapolated
            r1 = (rx*uf + ry*vf)   - 2.*af*gm1i
            r2 = (rx*uext+ry*vext) + 2.*sqrt(a2ext)*gm1i
c           -determine the normal velocity and speed of
c                                                sound from r1 & r2
            qn = (r1 + r2)*0.5
            kq=k+kadd
            if (qn.le.0.) then
              turre(j,k)=retinf
              vk(j,k)=vk_inf
              ve(j,k)=ve_inf
            else
              turre(j,k)=turre(j,kq)
              vk(j,k)=vk(j,kq)
              ve(j,k)=ve(j,kq)
            endif
          endif
c
c         -calc conserved and primitive variables for both
c          infinity and interior values
c         
c         -conserved variables at inf
c         -note     entinf=1/gamma so rhoinf=(ainf**2/(gamma*entinf))**gm1i
c                                           =(ainf**2)**gm1i
c          vinff(1)= rhoinf
          vinff(1)= af2**gm1i
          vinff(2)= vinff(1)*uf
          vinff(3)= vinff(1)*vf
          pf     = vinff(1)*af2*gi
          vinff(4)= pf*gm1i +0.5d0*(uf**2+vf**2)*vinff(1)
c          vinff(4)= pf*gm1i +0.5d0*(vinff(2)**2+vinff(3)**2)/vinff(1)
c
c         -primitive variables at inf
c          vinfp(1)= vinff(1)
c          vinfp(2)= uf
c          vinfp(3)= vf
c          vinfp(4)= pf
c         
c         -conserved variables extrapolated from interior
c         -formula used for vint(1) here is equivalent to rhoext
c          vint(1)= (a2ext/(gamma*entext))**gm1i
c          write(6,*) 'numiter,j',numiter,j
c          write(6,*) rhoext,vint(1),dabs(rhoext-vint(1))
          vint(1)= rhoext
          vint(2)= vint(1)*uext
          vint(3)= vint(1)*vext
          vint(4)= pext*gm1i + 0.5d0*(uext**2+vext**2)*vint(1)
c          vint(4)= pext*gm1i + 0.5d0*(vint(2)**2+vint(3)**2)/vint(1)
c
c         -primitve variables extrapolated from interior
c          vintp(1)= vint(1)
c          vintp(2)= uext
c          vintp(3)= vext
c          vintp(4)= pext
c         
c         
c         -form simple average (conservative variables)
          qbar(1)= 0.5d0*(vinff(1) + vint(1))
          qbar(2)= 0.5d0*(vinff(2) + vint(2))
          qbar(3)= 0.5d0*(vinff(3) + vint(3))
          qbar(4)= 0.5d0*(vinff(4) + vint(4))
c
c         -form difference (conservative variables)
          qdif(1)= vinff(1) - vint(1)
          qdif(2)= vinff(2) - vint(2)
          qdif(3)= vinff(3) - vint(3)
          qdif(4)= vinff(4) - vint(4)
c          qdif(1)= 0.5d0*(vinff(1) - vint(1))
c          qdif(2)= 0.5d0*(vinff(2) - vint(2))
c          qdif(3)= 0.5d0*(vinff(3) - vint(3))
c          qdif(4)= 0.5d0*(vinff(4) - vint(4))
c
c         -calc Roe's average using primitive variables
c          (see pg 465 of Hirsh Vol.2)
c          he = gamma*vinfp(4)/gami/vinfp(1) + 
c     &                                 0.5d0*(vinfp(2)**2+vinfp(3)**2)
c          hi = gamma*vintp(4)/gami/vintp(1) + 
c     &                                 0.5d0*(vintp(2)**2+vintp(3)**2)
cc        
c          sre    = dsqrt(vinfp(1))
c          sri    = dsqrt(vintp(1))
c          denom  = sre + sri
c          rhoa   = sre*sri
c          ua     = (vinfp(2)*sre + vintp(2)*sri)/denom
c          va     = (vinfp(3)*sre + vintp(3)*sri)/denom
c          ha     = (he*sre + hi*sri)/denom
c          ca     = dsqrt( gami*(ha - 0.5d0*(ua**2 + va**2)) )
c         
c         
c         -for simple average instead of roe average use following
          rho=qbar(1)
          rhoinv=1.d0/rho
          u=qbar(2)*rhoinv
          v=qbar(3)*rhoinv
          e=qbar(4)
          p=gami*(e-0.5d0*rho*(u**2+v**2))
          a2bar=gamma*p*rhoinv
          ca=dsqrt(a2bar)
          snr=1.d0/ca
c         for roe average
c          rho=rhoa
c          rhoinv=1.d0/rho
c          u=ua
c          v=va
c          snr=1.d0/ca
c         
c         
c         
c         *******************************************************
c         -multiply by Tkinv
          bt=1.d0/sqrt(2.d0)
          vcap=xy(j,k,3)*u + xy(j,k,4)*v
c          vcap=rx*u + ry*v
          vtot2=0.5d0*(u**2+v**2)
          beta=bt*rhoinv*snr
          ryu=ry*u
          rxu=rx*u
          ryv=ry*v
          rxv=rx*v
          
          t1= gami*(u*qdif(2) + v*qdif(3) - qdif(4) - vtot2*qdif(1))
          
          vec(1) = qdif(1) + t1*snr**2
          vec(2) = ((-ryu+rxv)*qdif(1)+ry*qdif(2)-rx*qdif(3))*rhoinv
          
          t1= t1*beta
          t2= ((rxu+ryv)*qdif(1) - rx*qdif(2) -ry*qdif(3))*bt*rhoinv
          
          vec(3) = -(t1+t2)
          vec(4) = -(t1-t2)
c         *******************************************************
c
c         -multipy by sign of eigenvalue matrix
          t1=sign(1.0,vcap)
          vec(1) = t1*vec(1)
          vec(2) = t1*vec(2)
          vec(3) = sign(1.0,vcap+ca)*vec(3)
          vec(4) = sign(1.0,vcap-ca)*vec(4)
c
c         *******************************************************
c         -multiply by Tk
          alp=rho*bt*snr
          t1= alp*(vec(3)+vec(4))
          t2= rho*bt*(vec(3)-vec(4))
          t3= rho*vec(2)
          t4= alp*t1
          tmp(1)= vec(1) + t1
          tmp(2)= vec(1)*u + ry*t3 + u*t4 + rx*t2
          tmp(3)= vec(1)*v - rx*t3 + v*t4 + ry*t2
          tmp(4)= vtot2*vec(1) + (ryu - rxv)*t3 +
     &          (vtot2+ca**2*gm1i)*t1 + (rxu + ryv)*t2
c          tmp(2)= vec(1)*u + rho*ry*vec(2) + alp*u*t1 + rx*t2
c          tmp(3)= vec(1)*v - rho*rx*vec(2) + alp*v*t1 + ry*t2
c          tmp(4)= vtot2*vec(1) + rho*(ry*u - rx*v)*vec(2) +
c     &             (vtot2+ca**2*gm1i)*t1 + (rx*u + ry*v)*t2
c
c         *******************************************************
c
          tmp(1)=qbar(1)-0.5d0*tmp(1)
          tmp(2)=qbar(2)-0.5d0*tmp(2)
          tmp(3)=qbar(3)-0.5d0*tmp(3)
          tmp(4)=qbar(4)-0.5d0*tmp(4)
          
c         -add jacobian                                             
          rjj = 1.0/xyj(j,k)                                        
          q(j,k,1) = tmp(1)*rjj
          q(j,k,2) = tmp(2)*rjj                             
          q(j,k,3) = tmp(3)*rjj
          q(j,k,4) = tmp(4)*rjj
 60     continue
      else
c
        if (prec.ne.3) then
          print *,'BCFAR only written for prec=3'
          stop
        endif
c
c
cdu     -use the preconditioned characteristics to solve the bc's
c
        do 75 j=j1,j2
c     
c          -do circulation correction
          xa = x(j,k) - chord/4.
          ya = y(j,k)
          radius = dsqrt(xa**2+ya**2)                                   
          angl = atan2(ya,xa)
          cjam = cos(angl)
          sjam = sin(angl)
          qcirc = circb/(radius* (1.d0-(fsmach*SIN(ANGL-ALPHAR))**2))
          uf = uinf + qcirc*sjam
          vf = vinf - qcirc*cjam
          af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))                    
          af = dsqrt(af2)                                          
c     
c         -calculate values for vinff (farfield values)
          vinff(1)=af2**gm1i
          vinff(2)=vinff(1)*uf
          vinff(3)=vinff(1)*vf
          pf=vinff(1)*af2*gi
          vinff(4)=pf*gm1i+0.5d0*(uf**2+vf**2)*vinff(1)
c
c       -extrapolate for vint from interior (no Jacobian in these)
c         if ((j.lt.(j2-3)).and.(j.gt.(j1+3))) then
c            vint(1)=2.0*q(j,kq-1,1)*xyj(j,k-1)-q(j,kq-2,1)*xyj(j,k-2)
c            vint(2)=2.0*q(j,kq-1,2)*xyj(j,k-1)-q(j,kq-2,2)*xyj(j,k-2)
c            vint(3)=2.0*q(j,kq-1,3)*xyj(j,k-1)-q(j,kq-2,3)*xyj(j,k-2)
c            vint(4)=2.0*q(j,kq-1,4)*xyj(j,k-1)-q(j,kq-2,4)*xyj(j,k-2)
c         else
          vint(1)=q(j,kq-1,1)*xyj(j,k-1)
          vint(2)=q(j,kq-1,2)*xyj(j,k-1)
          vint(3)=q(j,kq-1,3)*xyj(j,k-1)
          vint(4)=q(j,kq-1,4)*xyj(j,k-1)
c         endif
c
csdc           modification by Stan: 16/01/98
csdc           primitive variable at infinity.
csd            vinffp(1)=vinff(1)
csd            vinffp(2)=uf
csd            vinffp(3)=vf
csd            vinffp(4)=pf
csdc
csdc           primitive variables extrapolated from interior.
csd            vintp(1)=vint(1)
csd            vintp(2)=vint(2)/vint(1)
csd            vintp(3)=vint(3)/vint(1)
csd            vintp(4)=gami*(vint(4)-.5d0*vint(1)*
csd     &                          (vintp(2)**2+vintp(3)**2))
csdc           end of modification by Stan.
c
c         -form simple average
          qbar(1)=0.5d0*(vinff(1)+vint(1))
          qbar(2)=0.5d0*(vinff(2)+vint(2))
          qbar(3)=0.5d0*(vinff(3)+vint(3))
          qbar(4)=0.5d0*(vinff(4)+vint(4))
c
c         -form difference
          qdif(1)=vinff(1)-vint(1)
          qdif(2)=vinff(2)-vint(2)
          qdif(3)=vinff(3)-vint(3)
          qdif(4)=vinff(4)-vint(4)
c
csdc           Stan: 16/01/98
csd                  I found no difference in drag results when
csd                  using Roe's average or a simple average of the
csd                  properties at the mean state.  Hence Dave's
csd                  version remains.
csd
csdc           -calc Roe's average using primitive variables
csdc            (see pg 465 of Hirsh Vol.2)
csd            he = gamma*vinffp(4)/gami/vinffp(1) + 
csd     &                               0.5d0*(vinffp(2)**2+vinffp(3)**2)
csd            hi = gamma*vintp(4)/gami/vintp(1) + 
csd     &                               0.5d0*(vintp(2)**2+vintp(3)**2)
csdc
csd            sre    = dsqrt(vinffp(1))
csd            sri    = dsqrt(vintp(1))
csd            denom  = sre + sri
csd            rhoa   = sre*sri
csd            ua     = (vinffp(2)*sre + vintp(2)*sri)/denom
csd            va     = (vinffp(3)*sre + vintp(3)*sri)/denom
csd            ha     = (he*sre + hi*sri)/denom
csd            ca     = dsqrt( gami*(ha - 0.5d0*(ua**2 + va**2)) )
csdc
csd            rho=rhoa
csd            u=ua
csd            v=va
csd            uv2=u**2+v**2
csd            a2bar=ca**2
csd            dma2=uv2/a2bar
c
c         -calculate properties at mean state
          rho=qbar(1)
          rhoinv=1.d0/rho
          u=qbar(2)/rho
          v=qbar(3)/rho
          uv2=u**2+v**2
          p=gami*(qbar(4)-0.5d0*uv2*rho)
          a2bar=gamma*p/rho
          ca=dsqrt(a2bar)
          snr=1.d0/ca
          dma2=uv2/a2bar
c
          duw=xy(j,k,1)**2+xy(j,k,2)**2
          dul=xy(j,k,3)**2+xy(j,k,4)**2
c
          epsilon1=prphi*fsmach**2
          epsilon2=prxi*rhoinf*dsqrt(max(duw,dul))*rhoinv/(re*fsmach)

          dulim=max(epsilon1,epsilon2)
          epsilon=min(1.0,max(dma2,dulim))
c          epsilon=1.d0
c
          rx=xy(j,k,3)
          ry=xy(j,k,4)
          ucap=rx*u+ry*v
c
c         -compute components of preconditioned eigenvalue
          dua=.5d0*(1.d0+epsilon)*ucap
          dub=dsqrt((.5*(1.d0-epsilon)*ucap)**2+epsilon*dul*a2bar)
c
          rxy=dsqrt(dul)
          rx=rx/rxy
          ry=ry/rxy
c
c         ********************************************************
c         -multiply by Minv
          du4=gami*(.5*uv2*qdif(1)-u*qdif(2)-v*qdif(3)+qdif(4))
          du1=du4*rhoinv*snr
          du4=du4-a2bar*qdif(1)
          du2=(qdif(2)-u*qdif(1))*rhoinv
          du3=(qdif(3)-v*qdif(1))*rhoinv
c
c         ********************************************************
c         -multiply by Tkinv
          t1=du4
          t2=ry*du2-rx*du3
          z1=(rx*du2+ry*du3)
          z2=.5d0/dub
          z3=rxy*ca*du1
          t3=(z3+(ucap-dua+dub)*z1)*z2
          t4=(z3+(ucap-dua-dub)*z1)*z2
c
c         ********************************************************
c         -multiply by sign(Lambda)
          du1=sign(1.0,ucap)*t1
          du2=sign(1.0,ucap)*t2
          du3=sign(1.0,dua+dub)*t3
          du4=sign(1.0,dua-dub)*t4
c
c         ********************************************************
c         -multiply by Tk
          t1=((dua+dub-ucap)*du3+(ucap-dua+dub)*du4)*snr/rxy
          t2=ry*du2+rx*(du3-du4)
          t3=-rx*du2+ry*(du3-du4)
          t4=du1
c
c         ********************************************************
c         -multiply by M
          du1=(rho*t1-t4*snr)*snr
          du2=u*du1+rho*t2
          du3=v*du1+rho*t3
          du4=rho*((.5*uv2*snr+ca/gami)*t1+u*t2+v*t3)-.5*uv2/a2bar*t4
c
c         -calculate the boundry Q
          t1=qbar(1)-.5d0*du1
          t2=qbar(2)-.5d0*du2
          t3=qbar(3)-.5d0*du3
          t4=qbar(4)-.5d0*du4
c
c         -apply jacobian scaling
          q(j,kq,1)=t1/xyj(j,k)
          q(j,kq,2)=t2/xyj(j,k)
          q(j,kq,3)=t3/xyj(j,k)
          q(j,kq,4)=t4/xyj(j,k)
 75     continue
      endif
      goto 999
c
 200  continue
c     ****************************************
c     * side 2, j=jbegin  or side 4, j=jend  *
c     ****************************************
      j = ibc
      jq= ibc
      jadd = idir
      k1=index1
      k2=index2
      if (prec.eq.0) then
        do 160 k = k1,k2                                                
c                                                                          
c         -reset free stream values with circulation correction
c         -this if statement is here so that when I test for uniform
c          flow convergence I don't get a floating point exception when
c          using this routine for the body too.
c         -circul should be set to false when testing uniform flow.
          if (circul) then
            xa = x(j,k) - chord/4.d0
            ya = y(j,k)
            radius = dsqrt(xa**2+ya**2)
            angl = atan2(ya,xa)
            cjam = cos(angl)
            sjam = sin(angl)
            qcirc =circb/(radius*(1.d0-(fsmach*sin(angl-alphar))**2))
            uf = uinf + qcirc*sjam
            vf = vinf - qcirc*cjam
          else
            uf = uinf
            vf = vinf
          endif
          af2 = gami*(hstfs - 0.5d0*(uf**2+vf**2))
          af = dsqrt(af2)        
c                                                                          
c         -metrics for normals
          rx=xy(j,k,1)
          ry=xy(j,k,2)
          rxy2=1.d0/dsqrt(rx**2+ry**2)
c            print *,'rxy2=',j,rxy2
          rx=rx*rxy2*sgn
          ry=ry*rxy2*sgn
c
c         -get extrapolated variables                                
          if (iord.eq.4) then
            jq=j+jadd
            xyj2 = xyj(jq,k)
            rho2 = q(jq,k,1)*xyj2                                     
            rinv = 1.d0/q(jq,k,1)                                       
            u2 = q(jq,k,2)*rinv                                        
            v2 = q(jq,k,3)*rinv
            e2 = q(jq,k,4)*xyj2
            p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c
            jq=j+2*jadd
            xyj3 = xyj(jq,k)
            rho3 = q(jq,k,1)*xyj3
            rinv = 1.d0/q(jq,k,1)                                       
            u3 = q(jq,k,2)*rinv                                        
            v3 = q(jq,k,3)*rinv                                       
            e3 = q(jq,k,4)*xyj3
            p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c     
            jq=j+3*jadd
            xyj4 = xyj(jq,k)
            rho4 = q(jq,k,1)*xyj4
            rinv = 1.d0/q(jq,k,1)
            u4 = q(jq,k,2)*rinv                                        
            v4 = q(jq,k,3)*rinv                                       
            e4 = q(jq,k,4)*xyj4
            p4 = gami*(e4 - 0.5d0*rho4*(u4**2+v4**2))
c     
c            jq=j+4*jadd
c            xyj5 = xyj(jq,k)                                           
c            rho5 = q(jq,k,1)*xyj5
c            rinv = 1.d0/q(jq,k,1)                                     
c            u5 = q(jq,k,2)*rinv                                        
c            v5 = q(jq,k,3)*rinv                                       
c            e5 = q(jq,k,4)*xyj5
c            p5 = gami*(e5 - 0.5d0*rho5*(u5**2+v5**2))
c              
c           -3rd order extrapolation
c            rhoext= 4.d0*(rho2+rho4) - 6.d0*rho3 -rho5
c            uext= 4.d0*(u2+u4) -6.d0*u3 -u5
c            vext= 4.d0*(v2+v4) -6.d0*v3 -v5
c            pext= 4.d0*(p2+p4) -6.d0*p3 -p5
c           -2nd order extrapolation
            rhoext= 3.d0*(rho2 - rho3) + rho4
            uext= 3.d0*(u2 - u3) + u4
            vext= 3.d0*(v2 - v3) + v4
            pext= 3.d0*(p2 - p3) + p4
c           -1st order extrapolation
c            rhoext= 2.d0*rho2 - rho3
c            uext= 2.d0*u2 - u3
c            vext= 2.d0*v2 - v3
c            pext= 2.d0*p2 - p3
c           -Zeroeth order extrapolation
c            rhoext= rho2
c            uext= u2
c            vext= v2
c            pext= p2
          else
            jq=j+jadd
            xyj2 = xyj(jq,k)
            rho2 = q(jq,k,1)*xyj2                                     
            rinv = 1.d0/q(jq,k,1)                                       
            u2 = q(jq,k,2)*rinv                                        
            v2 = q(jq,k,3)*rinv
            e2 = q(jq,k,4)*xyj2
            p2 = gami*(e2 - 0.5d0*rho2*(u2**2+v2**2))
c
c            kq=k+2*kadd
c            xyj3 = xyj(jq,k)                                           
c            rho3 = q(jq,k,1)*xyj3
c            rinv = 1.d0/q(jq,k,1)                                       
c            u3 = q(jq,k,2)*rinv                                        
c            v3 = q(jq,k,3)*rinv                                       
c            e3 = q(jq,k,4)*xyj3
c            p3 = gami*(e3 - 0.5d0*rho3*(u3**2+v3**2))
c           
c           -1st order extrapolation
c            rhoext= 2.d0*rho2 - rho3
c            uext= 2.d0*u2 - u3
c            vext= 2.d0*v2 - v3
c            pext= 2.d0*p2 - p3
c           -Zeroth-order extrapolation
            rhoext= rho2
            uext= u2
            vext= v2
            pext= p2
          endif
c          entext=pext/rhoext**gamma
          hext= gamma*pext/rhoext/gami + 0.5d0*(uext**2+vext**2)
          a2ext= gamma*pext/rhoext
c                                             
          if (turbulnt) then
c           -determine the riemann invarients,
c                                       r1 far field, r2 extrapolated
            r1 = (rx*uf + ry*vf)   - 2.*af*gm1i
            r2 = (rx*uext+ry*vext) + 2.*sqrt(a2ext)*gm1i
c           -determine the normal velocity and speed of sound
c                                                        from r1 & r2
            qn = (r1 + r2)*0.5
            jq=j+jadd
            if (qn.le.0.) then
              turre(j,k)=retinf
              vk(j,k)=vk_inf
              ve(j,k)=ve_inf
            else
              turre(j,k)=turre(jq,k)
              vk(j,k)=vk(jq,k)
              ve(j,k)=ve(jq,k)
            endif
          endif
c
c         -calc conserved and primitive variables for both
c          infinity and interior values
c         
c         -conserved variables at inf
c         -note     entinf=1/gamma so rhoinf=(ainf**2/(gamma*entinf))**gm1i
c                                           =(ainf**2)**gm1i
c          vinff(1)= rhoinf
          vinff(1)= af2**gm1i
          vinff(2)= vinff(1)*uf
          vinff(3)= vinff(1)*vf
          pf     = vinff(1)*af2*gi
          vinff(4)= pf*gm1i +0.5d0*(uf**2+vf**2)*vinff(1)
c          vinff(4)= pf*gm1i +0.5d0*(vinff(2)**2+vinff(3)**2)/vinff(1)
c
c         -primitive variables at inf
c          vinfp(1)= vinff(1)
c          vinfp(2)= uf
c          vinfp(3)= vf
c          vinfp(4)= pf
c         
c         -conserved variables extrapolated from interior
c         -formula used for vint(1) here is equivalent to rhoext
c          vint(1)= (a2ext/(gamma*entext))**gm1i
c          write(6,*) 'numiter,j',numiter,j
c          write(6,*) rhoext,vint(1),dabs(rhoext-vint(1))
          vint(1)= rhoext
          vint(2)= vint(1)*uext
          vint(3)= vint(1)*vext
          vint(4)= pext*gm1i + 0.5d0*(uext**2+vext**2)*vint(1)
c          vint(4)= pext*gm1i + 0.5d0*(vint(2)**2+vint(3)**2)/vint(1)
c
c         -primitve variables extrapolated from interior
c          vintp(1)= vint(1)
c          vintp(2)= uext
c          vintp(3)= vext
c          vintp(4)= pext
c         
c         
c         -form simple average (conservative variables)
          qbar(1)= 0.5d0*(vinff(1) + vint(1))
          qbar(2)= 0.5d0*(vinff(2) + vint(2))
          qbar(3)= 0.5d0*(vinff(3) + vint(3))
          qbar(4)= 0.5d0*(vinff(4) + vint(4))
c
c         -form difference (conservative variables)
          qdif(1)= vinff(1) - vint(1)
          qdif(2)= vinff(2) - vint(2)
          qdif(3)= vinff(3) - vint(3)
          qdif(4)= vinff(4) - vint(4)
c          qdif(1)= 0.5d0*(vinff(1) - vint(1))
c          qdif(2)= 0.5d0*(vinff(2) - vint(2))
c          qdif(3)= 0.5d0*(vinff(3) - vint(3))
c          qdif(4)= 0.5d0*(vinff(4) - vint(4))
c
c         -calc Roe's average using primitive variables
c          (see pg 465 of Hirsh Vol.2)
c          he = gamma*vinfp(4)/gami/vinfp(1) + 
c     &                                 0.5d0*(vinfp(2)**2+vinfp(3)**2)
c          hi = gamma*vintp(4)/gami/vintp(1) + 
c     &                                 0.5d0*(vintp(2)**2+vintp(3)**2)
cc        
c          sre    = dsqrt(vinfp(1))
c          sri    = dsqrt(vintp(1))
c          denom  = sre + sri
c          rhoa   = sre*sri
c          ua     = (vinfp(2)*sre + vintp(2)*sri)/denom
c          va     = (vinfp(3)*sre + vintp(3)*sri)/denom
c          ha     = (he*sre + hi*sri)/denom
c          ca     = dsqrt( gami*(ha - 0.5d0*(ua**2 + va**2)) )
c         
c         
c         -for simple average instead of roe average use following
          rho=qbar(1)
          rhoinv=1.d0/rho
          u=qbar(2)*rhoinv
          v=qbar(3)*rhoinv
          e=qbar(4)
          p=gami*(e-0.5d0*rho*(u**2+v**2))
          a2bar=gamma*p*rhoinv
          ca=dsqrt(a2bar)
          snr=1.d0/ca
c         for roe average
c          rho=rhoa
c          rhoinv=1.d0/rho
c          u=ua
c          v=va
c          snr=1.d0/ca
c         
c         
c         
c         *******************************************************
c         -multiply by Tkinv
          bt=1.d0/sqrt(2.d0)
          ucap=xy(j,k,1)*u + xy(j,k,2)*v
c          ucap=rx*u + ry*v
          vtot2=0.5*(u**2+v**2)
          beta=bt*rhoinv*snr
          ryu=ry*u
          rxu=rx*u
          ryv=ry*v
          rxv=rx*v
          
          t1= gami*(u*qdif(2) + v*qdif(3) - qdif(4) - vtot2*qdif(1))
          
          vec(1) = qdif(1) + t1*snr**2
          vec(2) = ((-ryu+rxv)*qdif(1)+ry*qdif(2)-rx*qdif(3))*rhoinv
          
          t1= t1*beta
          t2= ((rxu+ryv)*qdif(1) - rx*qdif(2) -ry*qdif(3))*bt*rhoinv
          
          vec(3) = -(t1+t2)
          vec(4) = -(t1-t2)
c         *******************************************************
c
c         -multipy by sign of eigenvalue matrix
          t1=sign(1.0,ucap)
          vec(1) = t1*vec(1)
          vec(2) = t1*vec(2)
          vec(3) = sign(1.0,ucap+ca)*vec(3)
          vec(4) = sign(1.0,ucap-ca)*vec(4)
c
c         *******************************************************
c         -multiply by Tk
          alp=rho*bt*snr
          t1= alp*(vec(3)+vec(4))
          t2= rho*bt*(vec(3)-vec(4))
          t3= rho*vec(2)
          t4= alp*t1
          tmp(1)= vec(1) + t1
          tmp(2)= vec(1)*u + ry*t3 + u*t4 + rx*t2
          tmp(3)= vec(1)*v - rx*t3 + v*t4 + ry*t2
          tmp(4)= vtot2*vec(1) + (ryu - rxv)*t3 +
     &          (vtot2+ca**2*gm1i)*t1 + (rxu + ryv)*t2
c          tmp(2)= vec(1)*u + rho*ry*vec(2) + alp*u*t1 + rx*t2
c          tmp(3)= vec(1)*v - rho*rx*vec(2) + alp*v*t1 + ry*t2
c          tmp(4)= vtot2*vec(1) + rho*(ry*u - rx*v)*vec(2) +
c     &             (vtot2+ca**2*gm1i)*t1 + (rx*u + ry*v)*t2
c
c         *******************************************************
c
          tmp(1)=qbar(1)-0.5d0*tmp(1)
          tmp(2)=qbar(2)-0.5d0*tmp(2)
          tmp(3)=qbar(3)-0.5d0*tmp(3)
          tmp(4)=qbar(4)-0.5d0*tmp(4)
c          write(99,991) j,k,tmp(1),tmp(2),tmp(3),tmp(4)
 991      format(2i3,2x,4f15.6)
c
c         -add jacobian                                             
          rjj = 1.d0/xyj(j,k)                                        
          q(j,k,1) = tmp(1)*rjj
          q(j,k,2) = tmp(2)*rjj                             
          q(j,k,3) = tmp(3)*rjj
          q(j,k,4) = tmp(4)*rjj
 160    continue
      endif
c
 999  continue
      return                                                            
      end                                                               
