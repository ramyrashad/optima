*     ------------------------------------------------------------------
*     Program name:  HybridOptimize
*     Written by: Oleg Chernukhin
*     Date: May 2010
*
*     This subroutine is the interface between Optima2D and HYBROPT. 
*
*     ------------------------------------------------------------------
      subroutine hybridoptimize(jdim, kdim, ndim, ifirst, indx, icol, 
     &        iex, q, cp, xy, xyj, x, y, dx, dy, lambda,fmu,vort,turmu,
     &        cp_tar, dvs, dvlow, dvupp, idv, bap,bcp,bt, bknot, work1, 
     &        ia, ja, as,
     &        ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, cp_his,
     &        ac_his, opwk, ifun, iter, nflag,
     &        xsave, ysave, qtmp, itfirst, obj0s, ruIndex)

#ifdef _MPI_VERSION
      use mpi

      implicit none

#include "../include/arcom.inc"
#include "../include/optcom.inc"
#include "../include/mpi_info.inc"

      integer
     &     jdim, kdim, ndim, ifirst, indx(jdim,kdim), icol(9), i,
     &     iex(jdim,kdim,4), idv(nc+mpopt), j ,k ,l,
     &     ia(jdim*kdim*ndim+2), ifun, iter, nflag, ifun_tmp,
     &     ja(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1), mp,
     &     ipa(jdim*kdim*ndim+2), jpa(jdim*kdim*ndim*ndim*5+1),
     &     iat(jdim*kdim*ndim+2),
     &     jat(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ipt(jdim*kdim*ndim+2), jpt(jdim*kdim*ndim*ndim*5+1), itfirst,
     &     gradtemp, varStat, ruIndex

      double precision
     &     q(jdim,kdim,ndim), cp(jdim,kdim), obj0, obj, fp, fmin, alfa,
     &     xy(jdim,kdim,4), xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim),
     &     dx(jdim*kdim*incr), dy(jdim*kdim*incr), fmu(jdim,kdim), 
     &     lambda(2*jdim*kdim*incr), vort(jdim,kdim), turmu(jdim,kdim),
     &     cp_tar(jbody,2), dvs(nc+mpopt), bt(jbody), bknot(jbsord+nc),
     &     bap(jbody,2), bcp(nc,2), work1(jdim*kdim,100),
     &     as(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     ast(jdim*kdim*ndim*(ndim*9+1)+jdim*ndim*3+1),
     &     pa(jdim*kdim*ndim*ndim*5+1), dvlow(nc+mpopt),dvupp(nc+mpopt),
     &     pat(jdim*kdim*ndim*ndim*5+1), cp_his(jbody,300,mpopt),
     &     ac_his(jbody,300), opwk(jdim*kdim,8), s_vector(ndv),
     &     xsave(jdim,kdim), ysave(jdim,kdim),dvscale(nc+mpopt),
     &     at, ap, gsq, dg, dg1, dp, step, acc, dal, u1, u2, u3, u4,
     &     eps, xsq, dv_scal(ibsnc), test, dv_scalMag, sdMag, numer,
     &     denom, tmp, stmp, gtmp(ibsnc), gtemp(ibsnc), fd_temp,
     &     qs(jdim,kdim,4), xs(jdim,kdim), ys(jdim,kdim),
     &     aleft, dleft, fleft, aright, dright, fright,
     &     garbage, dvs_avg, gmin(ibsnc), dgTest, gminMag,
     &     cd(mpopt),cl(mpopt),cm(mpopt), grad(ibsnc),
     &     qtmp, obj0s, q_init, grads, objs,
     &     qwarm(warmset,maxj,maxk,nblk,nopc)

      logical
     &     rsw

      dimension
     &     q_init(warmset,jdim,kdim,ndim,mpopt),
     &     grads(20,ibsnc), objs(20), obj0s(20),
     &     qtmp(jdim,kdim,ndim,mpopt)

      character
     &     command*60, filename*40

      !-- SNOPT/HYBROPT variables
      integer
     &     maxF, maxn, nxname, nFname, lenA, lenG
      parameter
     &   ( maxF   = 30,
     &     maxn   = 50,
     &     lenA   = 50, lenG   = 600,
     &     nxname =  1, nFname =   1 )
      integer
     &     iAfun(lenA), jAvar(lenA), iGfun(lenG), jGvar(lenG),
     &     xstate(maxn), Fstate(maxF), Xso(maxn)
      character
     &     lfile*20, Prob*8, xnames(nxname)*8, Fnames(nFname)*8
      double precision
     &     ObjAdd, sInf, A(lenA), Flow(maxF), Fupp(maxF), F(maxF),
     &     xlow(maxn), xupp(maxn), sn_x(maxn), Fmul(maxF), xmul(maxn)
      integer
     &     Errors, neA, neG, ObjRow, INFO, iPrt, iPrint, iSpecs, iSum,
     &     iSumm, Major, mincw, miniw, minrw, nF, n, nInf, nOut, nS,
     &     d1, d10, namelen, file_stat, dpfile_unit,
     &     sampl_seq_unit
      external
     &     husrfun, usrfun

      logical
     &     file_exists, csampl
    
*     ------------------------------------------------------------------
*     SNOPT workspace

      integer               lenrw
      parameter          (  lenrw = 20000)
      double precision   rw(lenrw)
      integer               leniw
      parameter          (  leniw = 20000)
      integer            iw(leniw)
      integer               lencw
      parameter          (  lencw =   500)
      character          cw(lencw)*8

      !-- These should be moved to input.f
      iSumm  =  6
      iPrint =  9
      dpfile_unit = 1990
      sampl_seq_unit = 1970
      csampl = .false.


      !-- Setup the optimization parameters and variables
      Errors = 0
      call snOptData
     &   ( Errors, maxF, maxn,
     &     iAfun, jAvar, lenA, neA, A,
     &     iGfun, jGvar, lenG, neG,
     &     Prob, nF, n,
     &     ObjAdd, ObjRow, xlow, xupp, Flow, Fupp,
     &     sn_x, xstate, xmul, F, Fstate, Fmul,
     &     cw, lencw, iw, leniw, rw, lenrw, dvs, dvscale)
      if (Errors .gt. 0) then
         print *, 'ERROR: calling snOptData...' 
         call mpi_abort(COMM_CURRENT, 0, ierr)
      endif

      !-- Set upper and lower bounds on design variables
      do i = 1, ngdv
         sn_x(i) = dvs(i)
         xlow(i) = dvlow(i)
         xupp(i) = dvupp(i)
      end do

      do i = ngdv+1, ndv
         sn_x(i) = dvs(i)
         !-- =========TEMPORARY!!!!!!!==========
         xlow(i) =  1.0d0 
         xupp(i) =  17.0d0
      end do

      !-- Reset dvscale to 1 so it has no effect
c     do i = 1, n
c        dvscale(i) = 1.0d0
c     end do

      !-- Pack 'dvs' amd dvscale' into user work array 'ru' 
      !-- so they can be used by 'usrfun'

      i = ruIndex
      do j=1,ibsnc
         ru_global(i) = dvs(j)
         ru_global(ibsnc+i) = dvscale(j)
         i = i+1
      end do

      do i = 1, n
         Xso(i) = i
      end do

      !-- Output optimization definition file
      inquire(file='optimization.def', exist=file_exists)
      call mpi_barrier(MPI_COMM_WORLD, ierr)   

      if (rank_glob == 0 .and. (file_exists)) then
         write(*,*) 'INFO: Optimization problem already defined'
      endif

      if (rank_glob == 0 .and. (.not. file_exists)) then

         write(*,*) 'INFO: Creating optimization definition file'
         open (unit=opt_def_unit, file='optimization.def', 
     &         status='new', form='formatted', iostat=file_stat)
         if (file_stat /= 0) then
            write(*,*) rank_glob, 
     &         ': Error in Optimize:initHybrOptimize :: ', 
     &         'error opening optimization.dat file '
            call mpi_abort(COMM_CURRENT,0,ierr)
         endif

         !-- Write nX, nFnc, nnzLinG, nnzGrad
         write(opt_def_unit, *) n, nF, neA, neG

         !-- Write Xlow, X, Xupp, Xmul, Xso
         do i = 1, n
            write(opt_def_unit, *) xlow(i), sn_x(i), xupp(i), Xmul(i), 
     &                             Xso(i)
         end do

         !-- Write Flow, Fnc, Fupp, Fmul
         do i = 1, nF
            write(opt_def_unit, *) Flow(i), F(i), Fupp(i), Fmul(i)
         end do

         !-- Write iLfun, jLvar, LinG
         do i = 1, neA
            write(opt_def_unit, *) iAfun(i), jAvar(i), A(i)
         end do

         !-- Write iGfun, jGvar
         do i = 1, neG
            write(opt_def_unit, *) iGfun(i), jGvar(i)
         end do

         close(opt_def_unit)

      endif

      !-- Call Hybropt ... 
      first_husrfun = .true.
      call hybrOptA(husrfun, popsize, procs_per_chromo, commarr, 
     &              mycommid, hybr_meth, sobol_file, use_db, mingen,
     &              maxgen, ranseed, selmethod, prob_pert, 
     &              prob_mut, prob_avg, pen, xbest, xavg, xpert, 
     &              xmut, sn_maj_lim, flow_sol_lim, sample_offset, 
     &              cig, optf_output, iPrint, iSumm, opt_def_unit, 
     &              dpfile_unit, MPI_COMM_WORLD, load_bal_tol, 
     &              sampl_seq_unit, csampl)


#else 
      stop 'hybridoptimize.f: hybrid opt. requires compilation with MPI'
#endif

      end ! hybridOptimize

*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
