      subroutine ksinit (x,xlb,xub,scale,work,mdv,mcon,mobj,mside,
     .                   mscale,jprnt,jtmax,jgrad,jsdrst,rdfun,adfun,
     .                   fdelt,fdmin,rhomn,rhomx,rhodl,munit,ireq)
      common /kscomm/ rdf   ,adf   ,fdl   ,fdm   ,rho   ,drho  ,rhomax,
     .                fun0  ,slope ,delx  ,alpha ,alpmax,a1    ,a2    ,
     .                a3    ,a4    ,f1    ,f2    ,f3    ,f4    ,alim  ,
     .                atest ,ftest ,ifscl ,ifoff ,isx   ,isx0  ,isxlb ,
     .                isxub ,iscl  ,ig0   ,idf   ,islp  ,iobj0 ,iy    ,
     .                ip    ,ih    ,ihess ,iside ,isact ,idobj ,idg   ,
     .                itmp1 ,itmp2 ,inext ,jnext ,jsel  ,itcnt ,icntr ,
     .                icnta ,isdflg,isdrst,ifncl ,nunit ,ndv   ,ncon  ,
     .                nobj  ,nside ,nscale,iprnt ,itmax ,igrad ,limit
      dimension x(*),xlb(*),xub(*),scale(*),work(*)
c
c          routine to initialize optimization parameters
c          for routine ksopt
c
c          input parameters are
c
c               x     -- initial design variable values
c
c               xlb   -- lower bounds on design variables
c
c               xub   -- upper bounds on design variables
c
c               scale -- scale factors for design variables
c
c               work  -- scratch array
c
c               mdv   -- number of design variables
c
c               mcon  -- number of constraints
c
c               mobj  -- number of objective functions
c
c               mside -- flag  =0 if no side constraints
c
c               mscale-- flag selecting design variable scaling
c                        =0 -- no scaling
c                        <0 -- user supplied scaling in vector scale
c                        >0 -- automatic scaling by ksopt every mscale
c                              iterations
c
c               jprnt -- print level flag (3 digit)
c                        each print level includes all lower print levels
c
c                        hundreds digit - one-dimensional search print
c                                         0 = no print
c                                         1 = alpha and k-s function
c                                         2 = proposed d.v. vector
c
c                        tens     digit - gradient print
c                                         0 = no print
c                                         1 = df and dg
c
c                        ones     digit - d.v. and constraint print
c                                         0 = no print
c                                         1 = initial and final iterations
c                                         2 = all iterations
c                                         3 = slope and search direction
c                                         4 = hessian matrix
c
c               jtmax -- maximum number of iterations
c                        (default is 20)
c
c               jgrad -- flag selecting user supplied information
c                        =0 -- user supplies function and constraints
c                              with gradients computed by finite
c                              differences from within ksopt
c                        =1 -- user supplies all function, constraint,
c                              and gradient information
c                        (default is 0)
c
c               jsdrst-- number of iterations before restarting search
c                        direction with steepest descent
c                        (default is ndv+1)
c
c               rdfun -- relative function change for termination
c                        (default is 0.01)
c
c               adfun -- absolute function change for termination
c                        (default is 0.0)
c
c               fdelt -- step size for computing finite differences
c                        derivatives
c                        (default is 0.01)
c
c               fdmin -- minimum difference for computing
c                        finite differences
c                        (default is 0.0001)
c
c               rhomn -- minimum multiplier for ks function
c                        (default is 5.0)
c
c               rhomx -- maximum multiplier for ks function
c                        (default is 100.0)
c
c               rhodl -- increment for rho
c                        (default is computed internally)
c
c               munit -- fortran unit number for output file
c                        (default is 0)
c
c               ireq  -- dimensined length of work array
c
c          output parameters are
c
c               work  -- array containing information required
c                        by routine ksopt.  this array must not
c                        be altered during execution of ksopt.
c
c               ireq  -- required length of work array
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification -  2 February 1998
c***********************************************************************
c          these subroutines are compatible with fortran 77
c
      jsel   = 1
      ndv    = mdv
      ncon   = mcon
      nobj   = mobj
      nside  = 0
      nscale = mscale
      iprnt  = 1
      itmax  = 20
      igrad  = 0
      isdrst = jsdrst
      rdf    = 0.01
      adf    = 0.0
      fdl    = 0.01
      fdm    = 0.0001
      rho    = 5.0
      rhomax = 100.0
      drho   = 0.0
      nunit  = 0
      itcnt  = 1
      icntr  = 0
      icnta  = 0
      isdflg = 0
      ifncl  = 0
c
      if (mdv   .le. 0)   ndv   = 0
      if (mcon  .le. 0)   ncon  = 0
      if (mobj  .le. 0)   nobj  = 0
      if (mside .ne. 0)   nside = 1
      if (jprnt .ge. 0)   iprnt = jprnt
      if (jtmax .gt. 0)   itmax = jtmax
      if (jgrad .ne. 0)   igrad = 1
      if (jsdrst.le. 0)   isdrst= ndv + 1
      if (rdfun .gt. 0.0) rdf   = rdfun
      if (adfun .gt. 0.0) adf   = adfun
      if (fdelt .gt. 0.0) fdl   = fdelt
      if (fdmin .gt. 0.0) fdm   = fdmin
      if (rhomn .gt. 0.0) rho   = rhomn
      if (rhomx .ge. rho) rhomax= rhomx
      if (rhodl .gt. 0.0) drho  = rhodl
      if (munit .gt. 0)   nunit = munit
c
      ntmp  = 2 * ndv
      if (ntmp .lt. (nobj + ncon)) ntmp = nobj + ncon
      ifscl = 64
      ifoff = ifscl + nobj
      isx   = ifoff + nobj
      isx0  = isx   + ndv
      isxlb = isx0  + ndv
      isxub = isxlb + ndv
      iscl  = isxub + ndv
      ig0   = iscl  + 2 * ndv
      idf   = ig0   + ncon
      islp  = idf   + ndv
      iobj0 = islp  + ndv
      iy    = iobj0 + nobj
      ip    = iy    + ndv
      ih    = ip    + ndv
      ihess = ih    + ndv * (ndv + 1) / 2
      iside = ihess + ndv * (ndv + 1) / 2
      isact = iside + ndv
      idobj = isact + ndv
      idg   = idobj + nobj * ndv
      itmp1 = idg   + ncon * ndv
      itmp2 = itmp1 + ntmp
      jreq  = itmp2 + ntmp - 1
      if (jreq .gt. ireq) then
        print *,'  work array insufficient'
        print *,'  required  = ',jreq
        print *,'  allocated = ',ireq
        stop
      endif
      ireq = jreq
c
      if (ndv .eq. 0) go to 40
      do 30 i = 1,ndv
        j = i - 1
        xx  = x(i)
        if (nside .eq. 0) go to 20
        xbl = xlb(i)
        xbu = xub(i)
        if (xbl .le. xbu) go to 10
        xb = (xbl + xbu) / 2.0
        xbl = xb
        xbu = xb
   10 continue
        if (xx .lt. xbl) xx = xbl
        if (xx .gt. xbu) xx = xbu
        x(i) = xx
   20 continue
        ss = 1.0
        if (nscale .gt. 0) ss = abs(xx)
        if (nscale .lt. 0) ss = abs(scale(i))
        if (ss .lt. 1.0e-04) ss = 1.0
        work(iscl  + j) = ss
        work(iscl  + j + ndv) = ss
        work(isx   + j) = xx / ss
        work(isx0  + j) = xx / ss
        if (nside .eq. 0) go to 30
        work(isxlb + j) = xbl / ss
        work(isxub + j) = xbu / ss
   30 continue
c
      if (drho .gt. 0.0) go to 40
      drho = (rhomax - rho) / 5.0
      if (drho .lt. 10.0) drho = 10.0
      if (drho .gt. 40.0) drho = 40.0
c
   40 continue
c
      if (iprnt .eq. 0 .and. ndv .gt. 0 .and. nobj .gt. 0) go to 60
      write (nunit,70)
      write (nunit,80) ireq
      if (ndv .eq. 0) write (nunit,90)
      if (nobj .eq. 0) write (nunit,100)
      write (nunit,110) ndv,ncon,nobj,nside,nscale,iprnt,itmax,igrad
      write (nunit,120) rdf,adf,fdl,fdm,rho,rhomax,drho
      if (nside .ne. 0) go to 50
      write (nunit,130)
      write (nunit,140) (i,x(i),work(iscl+i-1),i=1,ndv)
      go to 60
   50 continue
      write (nunit,150)
      write (nunit,160) (i,x(i),work(iscl+i-1),
     .                   work(isxlb+i-1)*work(iscl+i-1),
     .                   work(isxub+i-1)*work(iscl+i-1),i=1,ndv)
   60 continue
      if (ndv .eq. 0 .or. nobj .eq. 0) jsel = 0
      call kscomp (work(1))
      return
c
   70 format (
     .        20x,'========================================'/
     .        21x,'=                                      ='/
     .        21x,'=    KSOPT Multiobjective Optimizer    ='/
     .        21x,'=                                      ='/
     .        21x,'=    Version 2.8      2 February 98    ='/
     .        21x,'=                                      ='/
     .        21x,'=    Written by    Gregory A. Wrenn    ='/
     .        21x,'=                                      ='/
     .        21x,'========================================'///)
   80 format (10x,'The work array must be dimensioned at least',
     .        i10,' words long.'//)
   90 format (/'   Number of design variables is zero -- optimization',
     .         ' is not possible'//)
  100 format (/'   Number of objective functions is zero -- ',
     .         'optimization is not possible'//)
  110 format (/'      ndv=',i5,'     ncon=',i5,'     nobj=',i5,
     .         '    nside=',i5,//'   nscale=',i5,'   iprint=',i5,
     .         '    itmax=',i5,'    igrad=',i5)
  120 format (/'    rdfun=',e14.7,2x,'    adfun=',e14.7/
     .        /'    fdelt=',e14.7,2x,'    fdmin=',e14.7/
     .        /'   rhomin=',e14.7,2x,'   rhomax=',e14.7,2x,
     .         '     drho=',e14.7)
  130 format (/42('-')/
     .        5x,' d.v. ',6x,'initial',9x,' scale'/
     .        5x,'number',6x,' value ',9x,'factor'/
     .        1x,42('-'))
  140 format (4x,i5,4x,e12.5,4x,e12.5)
  150 format (/73('-')/
     .        5x,' d.v. ',6x,'initial',9x,' scale',10x,'lower',
     .        11x,'upper'/
     .        5x,'number',6x,' value ',9x,'factor',10x,'bound',
     .        11x,'bound'/
     .        1x,73('-'))
  160 format (4x,i5,4x,e12.5,4x,e12.5,4x,e12.5,4x,e12.5)
      end


      subroutine ksopt (isel,x,obj,g,df,dg,nomax,ngmax,work)
      common /kscomm/ rdf   ,adf   ,fdl   ,fdm   ,rho   ,drho  ,rhomax,
     .                fun0  ,slope ,delx  ,alpha ,alpmax,a1    ,a2    ,
     .                a3    ,a4    ,f1    ,f2    ,f3    ,f4    ,alim  ,
     .                atest ,ftest ,ifscl ,ifoff ,isx   ,isx0  ,isxlb ,
     .                isxub ,iscl  ,ig0   ,idf   ,islp  ,iobj0 ,iy    ,
     .                ip    ,ih    ,ihess ,iside ,isact ,idobj ,idg   ,
     .                itmp1 ,itmp2 ,inext ,jnext ,jsel  ,itcnt ,icntr ,
     .                icnta ,isdflg,isdrst,ifncl ,nunit ,ndv   ,ncon  ,
     .                nobj  ,nside ,nscale,iprnt ,itmax ,igrad ,limit
      dimension x(*),obj(*),g(*),df(nomax,*),dg(ngmax,*),work(*)
c
c          KSOPT is a general purpose multiobjective optimization
c          program which performs a constrained to unconstrained
c          transformation on the given optimization problem.
c          The problem is then solved using a Davidon-Fletcher-Powell
c          unconstrained optimization method.
c
c          input parameters are
c               obj   -- current set of objective function values
c               g     -- current set of constraints
c               df    -- derivatives of objective funcs. w.r.t. x-vector
c               dg    -- derivatives of constraints w.r.t. x-vector
c               nomax -- first dimension of df matrix
c               ngmax -- first dimension of dg matrix
c               work  -- working array from routine ksinit
c                        which must not be altered during optimization.
c
c          output parameters are
c               isel  -- flag requesting user supplied information
c                        =0 -- minimization terminated
c                        =1 -- user supplies obj and g
c                        =2 -- user supplies df and dg
c               x     -- new set of design variables
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
c
c          choose a path based on the value of jsel
c
      call kscomg (work(1))
      iprnt3 = iprnt / 100
      ileft  = mod(iprnt,100)
      iprnt2 = ileft / 10
      iprnt1 = mod(ileft,10)
      nodim  = nobj
      if (nodim .le. 0) nodim = 1
      ncdim  = ncon
      if (ncdim .le. 0) ncdim = 1
c
      isel = 0
      if (jsel .le. 0) return
c
      go to (100,200,300,400,500,600) jsel
c
c          begin an iteration
c
  100 continue
      isel = 1
      jsel = 2
      inext = 0
      if (itcnt .eq. 1) go to 900
c
c          compute each objective function's scale and offset
c
  200 continue
      call ksando (obj,g,work(ifscl),work(ifoff),ncon,nobj)
c
c          calculate initial unconstrained function
c
      call ksfun (fun0,obj,g,rho,work(ifscl),work(ifoff),ncon,nobj,
     .            work(itmp1))
      if (igrad .eq. 1) go to 350
c
c          get finite difference gradients of obj and constraints
c
  300 continue
      jsel = 3
      isel = 1
      call ksgrad (inext,work(isx),work(isx0),work(isxlb),work(isxub),
     .             g,work(ig0),obj,work(iobj0),work(idobj),work(idg),
     .             work(iscl),delx,ndv,ncon,nobj,nside,fdl,fdm,
     .             nodim,ncdim)
      if (inext .ne. 0) go to 900
c
      do 310 i = 1,nobj
        obj(i) = work(iobj0 + i - 1)
  310 continue
c
      if (ncon .le. 0) go to 330
      do 320 i = 1,ncon
        g(i) = work(ig0 + i - 1)
  320 continue
  330 continue
      go to 440
c
c          get analytical gradients of obj and constraints
c
  350 continue
      do 360 i = 1,nobj
        work(iobj0 + i - 1) = obj(i)
  360 continue
c
      if (ncon .eq. 0) go to 380
      do 370 i = 1,ncon
        work(ig0 + i - 1) = g(i)
  370 continue
  380 continue
      isel = 2
      jsel = 4
      go to 900
c
c          scale analytical gradients of obj and constraints
c
  400 continue
      do 430 i = 1,ndv
        ss = work(iscl + i - 1)
c
        do 410 l = 1,nobj
          j = (i - 1) * nobj + l
          work(idobj + j - 1) = df(l,i) * ss
  410   continue
c
        if (ncon .eq. 0) go to 430
        do 420 k = 1,ncon
          j = (i - 1) * ncon + k
          work(idg + j - 1) = dg(k,i) * ss
  420   continue
  430 continue
c
c          compute gradient of unconstrained function
c          to be minimizied
c
  440 continue
      call ksdfun (work(idf),work(iobj0),work(ifscl),work(ifoff),
     .             work(idobj),work(ig0),work(idg),rho,ndv,ncon,nobj,
     .             work(itmp1),work(itmp2),nodim,ncdim)
c
c          compute an initial or restart hessian matrix
c
      call kshess (work(ihess),work(iobj0),work(ifscl),work(ifoff),
     .             work(idobj),work(ig0),work(idg),work(iscl),rho,
     .             ndv,ncon,nobj,work(itmp1),work(itmp2),nodim,ncdim)
c
c          check for side constraint violation
c
      call ksside (work(isx),work(isxlb),work(isxub),work(iside),
     .             work(idf),ndv,nside)
c
c          compute search direction
c
      call ksdfp (work(isx),work(iside),work(isact),work(idf),ndv,
     .            work(islp),slope,work(iy),work(ip),work(ih),
     .            work(ihess),isdflg)
c
c          print beginning of iteration information
c
      ipflag = 1
      call ksprnt (ipflag,iprnt1,iprnt2,x,work(iobj0),work(ig0),
     .             work(idobj),work(idg),work(iside),work(iscl),
     .             nodim,ncdim,work(itmp1),work(1))
c
c          compute unconstrained function to be minimized
c
  500 continue
      if (inext .eq. 0) go to 510
      call ksfun (fun,obj,g,rho,work(ifscl),work(ifoff),ncon,nobj,
     .            work(itmp1))
  510 continue
c
c          perform one-dimensional search
c
      jsel = 5
      isel = 1
      call ksoned (inext,jnext,work(isx),work(isx0),work(isxlb),
     .             work(isxub),fun,fun0,work(islp),slope,alpha,alpmax,
     .             ndv,a1,a2,a3,a4,f1,f2,f3,f4,alim,atest,ftest,
     .             nside,limit,nunit,iprnt3,work(iscl),work(itmp1),
     .             isdflg)
      if (inext .ne. 0) go to 900
      jsel = 6
      isel = 1
      go to 900
c
c          test for termination criteria
c
  600 continue
      call ksfun (fun,obj,g,rho,work(ifscl),work(ifoff),ncon,nobj,
     .            work(itmp1))
      if (itcnt .ge. itmax) go to 700
      af    = abs(fun)
      adelf = abs(fun - fun0)
      rdelf = adelf
      if (af .gt. 1.0e-12) rdelf = adelf / af
      icntr = icntr + 1
      icnta = icnta + 1
      if (rdelf .ge. rdf) icntr = 0
      if (adelf .ge. adf) icnta = 0
      if (icntr .ge. 3 .or. icnta .ge. 3) go to 700
c
c          go to next iteration
c
      itcnt = itcnt + 1
c
c          increment rho when necessary
c
      if (icntr .eq. 0 .and. icnta .eq. 0) go to 650
      rho = rho + drho
      if (rho .gt. rhomax) rho = rhomax
  650 continue
c
c          set flag for hessian matrix restart
c
      isdflg = isdflg + 1
      if (isdflg .gt. isdrst) isdflg = 0
c
c          re-scale when necessary
c
      iscale = 0
      if (nscale .gt. 0) iscale = nscale
      if (iscale .ne. 0) itflag = mod(itcnt - 1,iscale)
      if (iscale .eq. 0) itflag = 1
c
c          currently re-scale only when hessian matrix is reset
c          28 March 1991  g.a.w.
c
      itflag = 1
      if (isdflg .eq. 0) itflag = 0
      if (itflag .eq. 0) call ksscal (work(isx),work(isx0),work(isxlb),
     .                                work(isxub),work(iscl),
     .                                ndv,nside,nscale)
      go to 100
c
c          terminate
c
  700 continue
      isel = 0
c
      ipflag = 2
      call ksprnt (ipflag,iprnt1,iprnt2,x,obj,g,work(idobj),work(idg),
     .             work(iside),work(iscl),nodim,ncdim,work(itmp1),
     .             work(1))
c
c          un-scale design variables and return to calling routine
c
  900 continue
      call ksxlim (work(isx),work(isxlb),work(isxub),ndv,nside)
      call ksunsc (x,work(isx),work(iscl),ndv)
      if (isel .eq. 1) ifncl = ifncl + 1
      call kscomp (work(1))
      return
c
      end


      subroutine ksgeti (iout,rout,work)
      common /kscomm/ rdf   ,adf   ,fdl   ,fdm   ,rho   ,drho  ,rhomax,
     .                fun0  ,slope ,delx  ,alpha ,alpmax,a1    ,a2    ,
     .                a3    ,a4    ,f1    ,f2    ,f3    ,f4    ,alim  ,
     .                atest ,ftest ,ifscl ,ifoff ,isx   ,isx0  ,isxlb ,
     .                isxub ,iscl  ,ig0   ,idf   ,islp  ,iobj0 ,iy    ,
     .                ip    ,ih    ,ihess ,iside ,isact ,idobj ,idg   ,
     .                itmp1 ,itmp2 ,inext ,jnext ,jsel  ,itcnt ,icntr ,
     .                icnta ,isdflg,isdrst,ifncl ,nunit ,ndv   ,ncon  ,
     .                nobj  ,nside ,nscale,iprnt ,itmax ,igrad ,limit
      dimension iout(15),rout(25),work(*)
c
c          This user called routine returns the current values of
c          several common block variables to satisfy the curiosity
c          of the user.
c
c          This routine may be called at any time after
c          routine ksinit is executed.
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      call kscomg (work(1))
c
      iout(1)  = inext
      iout(2)  = jnext
      iout(3)  = jsel
      iout(4)  = itcnt
      iout(5)  = icntr
      iout(6)  = icnta
      iout(7)  = isdflg
      iout(8)  = isdrst
      iout(9)  = ifncl
      iout(10) = nunit
      iout(11) = iprnt
      iout(12) = itmax
      iout(13) = igrad
      iout(14) = limit
      iout(15) = -1
c
      rout(1)  = rdf
      rout(2)  = adf
      rout(3)  = fdl
      rout(4)  = fdm
      rout(5)  = rho
      rout(6)  = drho
      rout(7)  = rhomax
      rout(8)  = fun0
      rout(9)  = slope
      rout(10) = delx
      rout(11) = alpha
      rout(12) = alpmax
      rout(13) = a1
      rout(14) = a2
      rout(15) = a3
      rout(16) = a4
      rout(17) = f1
      rout(18) = f2
      rout(19) = f3
      rout(20) = f4
      rout(21) = alim
      rout(22) = atest
      rout(23) = ftest
      rout(24) = -1.0
      rout(25) = -1.0
c
      return
      end


      subroutine ksputi (iin,rin,work)
      common /kscomm/ rdf   ,adf   ,fdl   ,fdm   ,rho   ,drho  ,rhomax,
     .                fun0  ,slope ,delx  ,alpha ,alpmax,a1    ,a2    ,
     .                a3    ,a4    ,f1    ,f2    ,f3    ,f4    ,alim  ,
     .                atest ,ftest ,ifscl ,ifoff ,isx   ,isx0  ,isxlb ,
     .                isxub ,iscl  ,ig0   ,idf   ,islp  ,iobj0 ,iy    ,
     .                ip    ,ih    ,ihess ,iside ,isact ,idobj ,idg   ,
     .                itmp1 ,itmp2 ,inext ,jnext ,jsel  ,itcnt ,icntr ,
     .                icnta ,isdflg,isdrst,ifncl ,nunit ,ndv   ,ncon  ,
     .                nobj  ,nside ,nscale,iprnt ,itmax ,igrad ,limit
      dimension iin(15),rin(25),work(*)
c
c          This user called routine resets the values of certain
c          common block variables in ksopt and is the compliment
c          of routine ksgeti.  Only certain parameters are allowed
c          to be changed to protect the user from his or her self.
c
c          This routine may be called at any time after routine
c          ksinit is executed.
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      call kscomg (work(1))
c
c     inext  = iin(1)        not allowed
c     jnext  = iin(2)        not allowed
c     jsel   = iin(3)        not allowed
c     itcnt  = iin(4)        not allowed
      icntr  = iin(5)
      icnta  = iin(6)
      isdflg = iin(7)
      isdrst = iin(8)
c     ifncl  = iin(9)        not allowed
      nunit  = iin(10)
      iprnt  = iin(11)
      itmax  = iin(12)
      igrad  = iin(13)
c     limit  = iin(14)       not allowed
c     ijunk  = iin(15)       not allowed
c
      rdf    = rin(1)
      adf    = rin(2)
      fdl    = rin(3)
      fdm    = rin(4)
      rho    = rin(5)
      drho   = rin(6)
      rhomax = rin(7)
c     fun0   = rin(8)        not allowed
c     slope  = rin(9)        not allowed
      delx   = rin(10)
c     alpha  = rin(11)       not allowed
c     alpmax = rin(12)       not allowed
c     a1     = rin(13)       not allowed
c     a2     = rin(14)       not allowed
c     a3     = rin(15)       not allowed
c     a4     = rin(16)       not allowed
c     f1     = rin(17)       not allowed
c     f2     = rin(18)       not allowed
c     f3     = rin(19)       not allowed
c     f4     = rin(20)       not allowed
c     alim   = rin(21)       not allowed
c     atest  = rin(22)       not allowed
c     ftest  = rin(23)       not allowed
c     rjunk  = rin(24)       not allowed
c     rjunk  = rin(25)       not allowed
c
      call kscomp (work(1))
c
      return
      end


      subroutine ksresc (scale,work)
      common /kscomm/ rdf   ,adf   ,fdl   ,fdm   ,rho   ,drho  ,rhomax,
     .                fun0  ,slope ,delx  ,alpha ,alpmax,a1    ,a2    ,
     .                a3    ,a4    ,f1    ,f2    ,f3    ,f4    ,alim  ,
     .                atest ,ftest ,ifscl ,ifoff ,isx   ,isx0  ,isxlb ,
     .                isxub ,iscl  ,ig0   ,idf   ,islp  ,iobj0 ,iy    ,
     .                ip    ,ih    ,ihess ,iside ,isact ,idobj ,idg   ,
     .                itmp1 ,itmp2 ,inext ,jnext ,jsel  ,itcnt ,icntr ,
     .                icnta ,isdflg,isdrst,ifncl ,nunit ,ndv   ,ncon  ,
     .                nobj  ,nside ,nscale,iprnt ,itmax ,igrad ,limit
      dimension scale(*),work(*)
c
c          This user called routine updates the scaling vector with
c          the input vector scale.  The new scale factors will be used
c          the next time re-scaling is performed in routine ksopt.
c
c          This routine may be called at any time after routine
c          ksinit is executed.
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      call kscomg (work(1))
c
      do 10 i = 1,ndv
        j = i - 1
        work(iscl + j + ndv) = scale(i)
   10 continue
c
      isdflg = isdrst
c
      call kscomp (work(1))
c
      return
      end


      subroutine kscomg (work)
      common /kscomm/ rdf   ,adf   ,fdl   ,fdm   ,rho   ,drho  ,rhomax,
     .                fun0  ,slope ,delx  ,alpha ,alpmax,a1    ,a2    ,
     .                a3    ,a4    ,f1    ,f2    ,f3    ,f4    ,alim  ,
     .                atest ,ftest ,ifscl ,ifoff ,isx   ,isx0  ,isxlb ,
     .                isxub ,iscl  ,ig0   ,idf   ,islp  ,iobj0 ,iy    ,
     .                ip    ,ih    ,ihess ,iside ,isact ,idobj ,idg   ,
     .                itmp1 ,itmp2 ,inext ,jnext ,jsel  ,itcnt ,icntr ,
     .                icnta ,isdflg,isdrst,ifncl ,nunit ,ndv   ,ncon  ,
     .                nobj  ,nside ,nscale,iprnt ,itmax ,igrad ,limit
      dimension work(*)
c
c          copy variables from work array to common block kscomm
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      rdf    = work(1)
      adf    = work(2)
      fdl    = work(3)
      fdm    = work(4)
      rho    = work(5)
      drho   = work(6)
      rhomax = work(7)
      fun0   = work(8)
      slope  = work(9)
      delx   = work(10)
      alpha  = work(11)
      alpmax = work(12)
      a1     = work(13)
      a2     = work(14)
      a3     = work(15)
      a4     = work(16)
      f1     = work(17)
      f2     = work(18)
      f3     = work(19)
      f4     = work(20)
      alim   = work(21)
      atest  = work(22)
      ftest  = work(23)
      ifscl  = int(work(24))
      ifoff  = int(work(25))
      isx    = int(work(26))
      isx0   = int(work(27))
      isxlb  = int(work(28))
      isxub  = int(work(29))
      iscl   = int(work(30))
      ig0    = int(work(31))
      idf    = int(work(32))
      islp   = int(work(33))
      iobj0  = int(work(34))
      iy     = int(work(35))
      ip     = int(work(36))
      ih     = int(work(37))
      ihess  = int(work(38))
      iside  = int(work(39))
      isact  = int(work(40))
      idobj  = int(work(41))
      idg    = int(work(42))
      itmp1  = int(work(43))
      itmp2  = int(work(44))
      inext  = int(work(45))
      jnext  = int(work(46))
      jsel   = int(work(47))
      itcnt  = int(work(48))
      icntr  = int(work(49))
      icnta  = int(work(50))
      isdflg = int(work(51))
      isdrst = int(work(52))
      ifncl  = int(work(53))
      nunit  = int(work(54))
      ndv    = int(work(55))
      ncon   = int(work(56))
      nobj   = int(work(57))
      nside  = int(work(58))
      nscale = int(work(59))
      iprnt  = int(work(60))
      itmax  = int(work(61))
      igrad  = int(work(62))
      limit  = int(work(63))
c
      return
      end


      subroutine kscomp (work)
      common /kscomm/ rdf   ,adf   ,fdl   ,fdm   ,rho   ,drho  ,rhomax,
     .                fun0  ,slope ,delx  ,alpha ,alpmax,a1    ,a2    ,
     .                a3    ,a4    ,f1    ,f2    ,f3    ,f4    ,alim  ,
     .                atest ,ftest ,ifscl ,ifoff ,isx   ,isx0  ,isxlb ,
     .                isxub ,iscl  ,ig0   ,idf   ,islp  ,iobj0 ,iy    ,
     .                ip    ,ih    ,ihess ,iside ,isact ,idobj ,idg   ,
     .                itmp1 ,itmp2 ,inext ,jnext ,jsel  ,itcnt ,icntr ,
     .                icnta ,isdflg,isdrst,ifncl ,nunit ,ndv   ,ncon  ,
     .                nobj  ,nside ,nscale,iprnt ,itmax ,igrad ,limit
      dimension work(*)
c
c          copy variables from common block kscomm to work array
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      work(1)  = rdf
      work(2)  = adf
      work(3)  = fdl
      work(4)  = fdm
      work(5)  = rho
      work(6)  = drho
      work(7)  = rhomax
      work(8)  = fun0
      work(9)  = slope
      work(10) = delx
      work(11) = alpha
      work(12) = alpmax
      work(13) = a1
      work(14) = a2
      work(15) = a3
      work(16) = a4
      work(17) = f1
      work(18) = f2
      work(19) = f3
      work(20) = f4
      work(21) = alim
      work(22) = atest
      work(23) = ftest
      work(24) = float(ifscl)
      work(25) = float(ifoff)
      work(26) = float(isx)
      work(27) = float(isx0)
      work(28) = float(isxlb)
      work(29) = float(isxub)
      work(30) = float(iscl)
      work(31) = float(ig0)
      work(32) = float(idf)
      work(33) = float(islp)
      work(34) = float(iobj0)
      work(35) = float(iy)
      work(36) = float(ip)
      work(37) = float(ih)
      work(38) = float(ihess)
      work(39) = float(iside)
      work(40) = float(isact)
      work(41) = float(idobj)
      work(42) = float(idg)
      work(43) = float(itmp1)
      work(44) = float(itmp2)
      work(45) = float(inext)
      work(46) = float(jnext)
      work(47) = float(jsel)
      work(48) = float(itcnt)
      work(49) = float(icntr)
      work(50) = float(icnta)
      work(51) = float(isdflg)
      work(52) = float(isdrst)
      work(53) = float(ifncl)
      work(54) = float(nunit)
      work(55) = float(ndv)
      work(56) = float(ncon)
      work(57) = float(nobj)
      work(58) = float(nside)
      work(59) = float(nscale)
      work(60) = float(iprnt)
      work(61) = float(itmax)
      work(62) = float(igrad)
      work(63) = float(limit)
c
      return
      end


      subroutine ksprnt (ipflag,iprnt1,iprnt2,x,obj,g,df,dg,side,scale,
     .                   nodim,ncdim,temp,work)
      common /kscomm/ rdf   ,adf   ,fdl   ,fdm   ,rho   ,drho  ,rhomax,
     .                fun0  ,slope ,delx  ,alpha ,alpmax,a1    ,a2    ,
     .                a3    ,a4    ,f1    ,f2    ,f3    ,f4    ,alim  ,
     .                atest ,ftest ,ifscl ,ifoff ,isx   ,isx0  ,isxlb ,
     .                isxub ,iscl  ,ig0   ,idf   ,islp  ,iobj0 ,iy    ,
     .                ip    ,ih    ,ihess ,iside ,isact ,idobj ,idg   ,
     .                itmp1 ,itmp2 ,inext ,jnext ,jsel  ,itcnt ,icntr ,
     .                icnta ,isdflg,isdrst,ifncl ,nunit ,ndv   ,ncon  ,
     .                nobj  ,nside ,nscale,iprnt ,itmax ,igrad ,limit
      character*4 ip1
      dimension x(*),obj(*),g(*),df(nodim,*),dg(ncdim,*)
      dimension side(*),scale(*),temp(*),work(*)
      data ip1 /'(   '/
c
c          main print routine
c            if (ipflag = 1) print iteration data
c            if (ipflag = 2) print final optimization data
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 2 February 1998
c
      if (ipflag .ne. 1) go to 200
c
c          iteration data
c
  100 continue
      if (iprnt1 .eq. 0) go to 300
      if (iprnt1 .lt. 2 .and. itcnt .gt. 1) go to 300
c
      call ksunsc (x,work(isx),work(iscl),ndv)
      write (nunit,1100) itcnt
      write (nunit,1110) rho
c
c          design variables
c
      write (nunit,1120)
      write (nunit,1000) (ip1,i,x(i),i=1,ndv)
c
c          scaling vector
c
      if (nscale .eq. 0) go to 110
      write (nunit,1130)
      write (nunit,1000) (ip1,i,scale(i),i=1,ndv)
  110 continue
c
c          objective functions and their gradients
c
      do 120 j = 1,nobj
        write (nunit,1140) j,obj(j)
        if (iprnt2 .eq. 0) go to 120
        write (nunit,1150)
        write (nunit,1000) (ip1,i,(df(j,i) / scale(i)),i=1,ndv)
  120 continue
c
c          constraints and their gradients
c
      if (ncon .eq. 0) go to 140
      write (nunit,1160)
      write (nunit,1000) (ip1,i,g(i),i=1,ncon)
      if (iprnt2 .eq. 0) go to 140
      do 130 j = 1,ncon
        write (nunit,1170) j
        write (nunit,1000) (ip1,i,(dg(j,i) / scale(i)),i=1,ndv)
  130 continue
  140 continue
c
c          side constraints
c
      if (nside .eq. 0) go to 160
      m = 0
      do 150 i = 1,ndv
        if (side(i) .eq. 0.0) go to 150
        m = m + 1
        if (side(i) .eq.  -1.0) temp(m) = float(-i)
        if (side(i) .eq.   1.0) temp(m) = float(i)
        if (side(i) .ne. 999.0) go to 150
        temp(m) = float(-i)
        m = m + 1
        temp(m) = float(i)
  150 continue
      if (m .eq. 0) go to 160
      write (nunit,1180)
      write (nunit,1190) (int(temp(i)),i=1,m)
  160 continue
c
c          print search direction and slope
c
      if (iprnt1 .lt. 3) go to 300
      write (nunit,1340) slope
      write (nunit,1000) (ip1,i,work(islp+i-1),i=1,ndv)
c
c          print hessian matrix
c
      if (iprnt1 .lt. 4) go to 300
      if (isdflg .eq. 0) write (nunit,1350)
      k = 1
      write (nunit,1360)
      do 170 i = 1,ndv
        l = k + i - 1
        write (nunit,1370) i,(work(ih+j-1),j=k,l)
        k = l + 1
  170 continue
      go to 300
c
c          final optimization data
c
  200 continue
      if (ipflag .ne. 2) go to 300
      if (iprnt1 .eq. 0) go to 300
c
      call ksunsc (x,work(isx),work(iscl),ndv)
      write (nunit,1200)
      write (nunit,1210)
      write (nunit,1000) (ip1,i,x(i),i=1,ndv)
      write (nunit,1220) rho
      do 210 j = 1,nobj
        write (nunit,1230) j,obj(j)
  210 continue
      if (ncon .eq. 0) go to 220
      write (nunit,1240)
      write (nunit,1000) (ip1,i,g(i),i=1,ncon)
  220 continue
      if (nside .eq. 0) go to 240
      m = 0
      do 230 i = 1,ndv
        if (side(i) .eq. 0.0) go to 230
        m = m + 1
        if (side(i) .eq.  -1.0) temp(m) = float(-i)
        if (side(i) .eq.   1.0) temp(m) = float(i)
        if (side(i) .ne. 999.0) go to 230
        temp(m) = float(-i)
        m = m + 1
        temp(m) = float(i)
  230 continue
      if (m .eq. 0) go to 240
      write (nunit,1250)
      write (nunit,1260) (int(temp(i)),i=1,m)
  240 continue
      write (nunit,1270)
      write (nunit,1280) itcnt
      write (nunit,1290) ifncl
      write (nunit,1300)
      if (itcnt .ge. itmax) write (nunit,1310)
      if (icntr .ge. 3    ) write (nunit,1320)
      if (icnta .ge. 3    ) write (nunit,1330)
c
  300 continue
      return
c
 1000 format (3(3x,a1,i5,')=',e14.7))
 1100 format (/33('##')/'       Beginning of iteration ',i4)
 1110 format ( '       current value of rho is    ',e12.5)
 1120 format (/'       design variables')
 1130 format (/'       scaling vector')
 1140 format (/'       objective function number ',i3,' = ',e24.16)
 1150 format (/'       gradients of objective function')
 1160 format (/'       constraint values')
 1170 format (/'       gradients for constraint ',i4)
 1180 format (/'       upper and lower bounds')
 1190 format (5x,12i5)
c
 1200 format (/////'       final optimization information')
 1210 format (/'       final design variables')
 1220 format (/'       final value of rho is    ',e12.5)
 1230 format (/'       final objective function number ',i3,' = ',
     .        e24.16)
 1240 format (/'       final constraint values')
 1250 format (/'       final upper and lower bounds')
 1260 format (5x,12i5)
 1270 format (/'       termination information')
 1280 format (/'       number of iterations=',i6)
 1290 format (/'       objective functions were evaluated ',i4,
     .         ' times')
 1300 format (/'       optimization terminated by ')
 1310 format (/'       iteration count exceeded itmax')
 1320 format (/'       relative change less than rdfun for 3',
     .         ' iterations')
 1330 format (/'       absolute change less than adfun for 3',
     .         ' iterations')
 1340 format (/'       slope d(ks)/d(alpha)=',e24.16/
     .        /'       search direction')
 1350 format (/'       hessian matrix reset')
 1360 format (/'       hessian matrix'/)
 1370 format ('       row ',i3,3x,6e10.3/(17x,6e10.3/))
c
      end


      subroutine ksside (x,xlb,xub,side,dfun,ndv,nside)
      dimension x(*),xlb(*),xub(*),side(*),dfun(*)
c
c          routine to compute a vector of flags side(i),i=1,ndv
c          side(i) =  0  -- d.v. i is not at a side constraint
c                  = -1  -- d.v. i is at a lower bound
c                  = +1  -- d.v. i is at an upper bound
c                  = 999 -- d.v. i is at both upper and lower bounds
c
c          also zero components of dfun that would violate
c          side constraints
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      do 10 i = 1,ndv
        side(i) = 0.0
        if (nside .eq. 0) go to 10
c
        xl = abs(xlb(i))
        if (xl .lt. 1.0) xl = 1.0
        xx = (xlb(i) - x(i)) / xl
        if (xx .ge. -1.0e-6) side(i) = -1.0
        if (side(i) .lt. 0.0 .and. dfun(i) .gt. 0.0) dfun(i) = 0.0
c
        xu = abs(xub(i))
        if (xu .lt. 1.0) xu = 1.0
        xx = (x(i) - xub(i)) / xu
        if (xx .ge. -1.0e-6  .and. side(i) .ne. 0.0) side(i) = 999.0
        if (xx .ge. -1.0e-6  .and. side(i) .eq. 0.0) side(i) = 1.0
        if (side(i) .gt. 0.0 .and. dfun(i) .lt. 0.0) dfun(i) = 0.0
   10 continue
c
      return
      end


      subroutine ksscal (x,x0,xlb,xub,scale,ndv,nside,nscale)
      dimension x(*),x0(*),xlb(*),xub(*),scale(ndv,2)
c
c          routine to compute new scaling vector and re-scale
c          design variables and lower and upper bounds
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 17 July 1996
c
      do 10 i = 1,ndv
        xx    = x(i)
        xx0   = x0(i)
        sold  = scale(i,1)
        snew  = sold
        if (nscale .gt. 0) snew = abs(xx * sold)
        if (nscale .lt. 0) snew = abs(scale(i,2))
        if (snew .lt. 1.0e-08) snew = 1.0
        scale(i,1) = snew
        x(i)    = xx  * sold / snew
        x0(i)   = xx0 * sold / snew
        if (nside .le. 0) go to 10
        xlb(i)  = xlb(i) * sold / snew
        xub(i)  = xub(i) * sold / snew
   10 continue
c
      return
      end


      subroutine ksunsc (x,sx,scale,ndv)
      dimension x(*),sx(*),scale(ndv,2)
c
c          routine to un-scale design variables before returning
c          to the real world
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 17 July 1996
c
      do 10 i = 1,ndv
        ss = scale(i,1)
        x(i) = sx(i) * ss
   10 continue
c
      return
      end


      subroutine ksxlim (x,xlb,xub,ndv,nside)
      dimension x(*),xlb(*),xub(*)
c
c          routine to insure x-vector does not violate
c          upper or lower bounds
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 17 July 1996
c
      if (nside .eq. 0) go to 20
      do 10 i = 1,ndv
        xx  = x(i)
        xl  = xlb(i)
        xu  = xub(i)
        if (xx .lt. xl) xx = xl
        if (xx .gt. xu) xx = xu
        x(i) = xx
   10 continue
   20 continue
c
      return
      end


      subroutine ks (f,g,ng,rho)
      dimension g(ng)
      data toler /-40.0/
c
c          routine to compute k-s function (type 2)
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 9 July 1988
c
      sum = 0.0
      gmax = g(1)
      if (ng .lt. 2) go to 30
      do 10 i = 2,ng
        if (g(i) .gt. gmax) gmax = g(i)
   10 continue
      do 20 i = 1,ng
        val = rho * (g(i) - gmax)
        if (val .lt. toler) go to 20
        sum = sum + exp(val)
   20 continue
   30 continue
      f = gmax
      if (ng .gt. 1) f = gmax + log(sum) / rho
c
      return
      end


      subroutine ksd (df,g,dgdx,ng,rho)
      dimension g(ng),dgdx(ng)
      data toler /-40.0/
c
c          routine to compute partial k-s w.r.t. x (type 2).
c          partial of g(i) w.r.t. x (e.g.  by finite difference)
c          must be supplied in array dgdx(i).
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 9 July 1988
c
      sum1 = 0.0
      sum2 = 0.0
      gmax = g(1)
      if (ng .lt. 2) go to 30
      do 10 i = 2,ng
        if (g(i) .gt. gmax) gmax = g(i)
   10 continue
      do 20 i = 1,ng
        val = rho * (g(i) - gmax)
        if (val .lt. toler) go to 20
        sum1 = sum1 + exp(val) * dgdx(i)
        sum2 = sum2 + exp(val)
   20 continue
   30 continue
      df = dgdx(1)
      if (ng .gt. 1) df = sum1 / sum2
c
      return
      end


      subroutine ksfun (fun,obj,g,rho,fscale,offset,ncon,nobj,temp)
      dimension obj(*),g(*),fscale(*),offset(*),temp(*)
c
c          routine to compute unconstrained function to be minimized
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 17 July 1996
c
      do 10 i = 1,nobj
        temp(i) = obj(i) / fscale(i) + offset(i)
   10 continue
      j = nobj
      if (ncon .le. 0) go to 30
      do 20 i = 1,ncon
        j = j + 1
        temp(j) = g(i)
   20 continue
   30 continue
c
      call ks (fun,temp,j,rho)
c
      return
      end


      subroutine ksdfun (dfun,obj,fscale,offset,df,g,dg,rho,ndv,ncon,
     .                   nobj,temp1,temp2,nodim,ncdim)
      dimension dfun(*),obj(*),fscale(*),offset(*),df(nodim,*)
      dimension g(*),dg(ncdim,*),temp1(*),temp2(*)
c
c          routine to compute gradients of function to be minimized
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      do 10 i = 1,nobj
        temp1(i) = obj(i) / fscale(i) + offset(i)
   10 continue
      j = nobj
      if (ncon .le. 0) go to 30
      do 20 i = 1,ncon
        j = j + 1
        temp1(j) = g(i)
   20 continue
   30 continue
c
      do 70 n = 1,ndv
        do 40 i = 1,nobj
          temp2(i) = df(i,n) / fscale(i)
   40   continue
        j = nobj
        if (ncon .le. 0) go to 60
        do 50 i = 1,ncon
          j = j + 1
          temp2(j) = dg(i,n)
   50   continue
   60   continue
c
      call ksd (dfun(n),temp1,temp2,j,rho)
c
   70 continue
c
      return
      end


      subroutine kshess (hess,obj,fscale,offset,df,g,dg,scale,rho,
     .                   ndv,ncon,nobj,temp1,temp2,nodim,ncdim)
      dimension hess(*),obj(*),fscale(*),offset(*),df(nodim,*)
      dimension g(*),dg(ncdim,*),scale(ndv,2),temp1(*),temp2(2,*)
      data toler /-40.0/
c
c          routine to compute approximate hessian matrix from
c          first order gradient information
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      do 10 i = 1,nobj
        temp1(i) = obj(i) / fscale(i) + offset(i)
   10 continue
      j = nobj
      if (ncon .le. 0) go to 30
      do 20 i = 1,ncon
        j = j + 1
        temp1(j) = g(i)
   20 continue
   30 continue
c
      kk = 0
      do 140 m = 1,ndv
c
        do 40 i = 1,nobj
          temp2(1,i) = df(i,m) / fscale(i)
   40   continue
        j = nobj
        if (ncon .le. 0) go to 60
        do 50 i = 1,ncon
          j = j + 1
          temp2(1,j) = dg(i,m)
   50   continue
   60   continue
c
        do 130 n = 1,m
c
          do 70 i = 1,nobj
            temp2(2,i) = df(i,n) / fscale(i)
   70     continue
          j = nobj
          if (ncon .le. 0) go to 90
          do 80 i = 1,ncon
            j = j + 1
            temp2(2,j) = dg(i,n)
   80     continue
   90     continue
c
          sum1 = 0.0
          sum2 = 0.0
          sum3 = 0.0
          sum4 = 0.0
          ng = nobj + ncon
          gmax = temp1(1)
          if (ng .lt. 2) go to 110
          do 100 i = 2,ng
            if (temp1(i) .gt. gmax) gmax = temp1(i)
  100     continue
  110     continue
          do 120 i = 1,ng
            d2grad = 0.0
            if (n .eq. m) d2grad = 1.0
            val = rho * (temp1(i) - gmax)
            if (val .lt. toler) go to 120
            sum1 = sum1 + exp(val)
            sum2 = sum2 + exp(val) * temp2(1,i)
            sum3 = sum3 + exp(val) * rho * temp2(2,i)
            sum4 = sum4 + exp(val) * (rho * temp2(1,i) * temp2(2,i)
     .                                    + d2grad)
  120     continue
c
          kk = kk + 1
          if (n .eq. m) hess(kk) = (sum4 / sum1) -
     .                             (sum2 * sum3) / (sum1 * sum1)
          if (n .ne. m) hess(kk) = 0.0
c
  130   continue
  140 continue
c
      return
      end


      subroutine ksgrad (inext,x,x0,xlb,xub,g,g0,obj,obj0,df,dg,scale,
     .                   delx,ndv,ncon,nobj,nside,fdelt,fdmin,nodim,
     .                   ncdim)
      dimension x(*),x0(*),xlb(*),xub(*),g(*),g0(*)
      dimension obj(*),obj0(*),df(nodim,*),dg(ncdim,*),scale(ndv,2)
c
c          routine to determine df and dg by finite differences
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      if (inext .gt. 0) go to 40
c
c          initialize obj0, x0, and g0
c
      do 10 i = 1,nobj
        obj0(i) = obj(i)
   10 continue
c
      do 20 i = 1,ndv
        x0(i) = x(i)
   20 continue

      if (ncon .le. 0) go to 40
      do 30 i = 1,ncon
        g0(i) = g(i)
   30 continue
c
c          calculate gradients and restore design variables
c
c          note - these gradients are already scaled by the
c                 design variable scale factors
c
   40 continue
      if (inext .eq. 0) go to 80
c
      do 50 i = 1,nobj
        df(i,inext) = (obj(i) - obj0(i)) / delx
   50 continue
c
      if (ncon .le. 0) go to 70
      do 60 i = 1,ncon
        dg(i,inext) = (g(i) - g0(i)) / delx
   60 continue
   70 continue
      x(inext) = x0(inext)
c
c          increment design variable inext and get obj and g
c
   80 continue
      inext = inext + 1
      if (inext .gt. ndv) go to 100
      xhere = x0(inext)
      delmin = fdmin / scale(inext,1)
      delx = abs(xhere * fdelt)
      if (delx .lt. delmin) delx = delmin
c
      if (nside .le. 0) go to 90
c
      xtest = xhere + delx
      if (xtest .le. xub(inext)) go to 90
c
c          adjust delta-x for upper bound
 
      delx1 = delx
      delx = abs(xub(inext) - xhere)
      if (delx .ge. delmin) go to 90
c
c          adjust delta-x for lower bound
c
      delx = -delx1
      xtest = xhere + delx
      if (xtest .lt. xlb(inext)) delx = xlb(inext) - xhere
c
   90 continue
      x(inext) = x0(inext) + delx
      go to 110
c
c          reset inext to zero when finished
c
  100 continue
      inext = 0
  110 continue
c
      return
      end


      subroutine ksando (obj,g,fscale,offset,ncon,nobj)
      dimension obj(*),g(*),fscale(*),offset(*)
      data eps/1.0e-6/
c
c          routine to compute scale and offset for obj
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 17 July 1996
c
      if (ncon .le. 0) go to 20
      gmax = g(1)
      if (ncon .eq. 1) go to 20
      do 10 i = 2,ncon
        if (g(i) .gt. gmax) gmax = g(i)
   10 continue
c
   20 continue
      do 30 j = 1,nobj
        fscl = abs(obj(j))
        foff = abs(obj(j))
        if (fscl .lt. 1.0) fscl = 1.0
        if (foff .gt. 1.0) foff = 1.0
        fscale(j) = fscl
        offset(j) = 0.0
c
c          compute offset if ncon > 0
c
        if (ncon .le. 0) go to 30
        if (obj(j) .ge. 0.0) offset(j) = -foff - gmax - eps
        if (obj(j) .lt. 0.0) offset(j) =  foff - gmax + eps
   30 continue
c
      return
      end


      subroutine ksoned (inext,jtry,x,x0,xlb,xub,fun,fun0,s,slope,
     .                   alpha,alpmax,ndv,a1,a2,a3,a4,f1,f2,f3,f4,
     .                   alim,atest,ftest,nside,limit,nunit,iprnt3,
     .                   scale,temp,isdflg)
      character*4 ip1
      dimension x(*),x0(*),xlb(*),xub(*),s(*),scale(ndv,2),temp(*)
      data ip1 /'(   '/
      data rtoler /0.00001/
      data atoler /0.001/
      data jmax /20/
c
c          routine to perform one-dimensional search
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      if (inext .gt. 0) go to 50
c
c          initialization
c
      do 10 i = 1,ndv
        x0(i) = x(i)
   10 continue
      alpha = 0.0
      alim = 0.0
      a4 = -1.0
      atest = 0.0
      if (slope .ge. 0.0) go to 500
c
c          estimate initial alpha to limit obj to a small change
c
      alpha  = abs((0.10 * fun0) / slope)
      otest1 = abs(0.10 / slope)
      if (alpha .gt. otest1) alpha = otest1
c
c          estimate initial alpha to limit x(i) to a small change
c
      do 20 i = 1,ndv
        si = abs(s(i))
        if (si .lt. 1.0e-08) go to 20
        xtest  = abs((0.10 * x(i)) / si)
        xtest1 = abs(0.10 / si)
        if (xtest .lt. xtest1) xtest = xtest1
        if (alpha .gt. xtest) alpha = xtest
   20 continue
c
c          estimate initial alpha to prevent a bounds violation
c
      alpmax = 1.0e+10
      if (nside .eq. 0) go to 40
      do 30 i = 1,ndv
        if (abs(s(i)) .lt. 1.0e-08) go to 30
        btest = (xub(i) - x(i)) / s(i)
        if (btest .gt. 0.0 .and. btest .lt. alpmax) alpmax = btest
        btest = (xlb(i) - x(i)) / s(i)
        if (btest .gt. 0.0 .and. btest .lt. alpmax) alpmax = btest
   30 continue
c
      if (alpha .gt. alpmax) alpha = alpmax
      if (alpha .lt. 1.0e-06) alpha = 1.0e-06
c
   40 continue
c
      if (iprnt3 .gt. 0) write (nunit,1010) alpha,alpmax,fun0
c
      a1 = 0.0
      f1 = fun0
      jtry = 0
      inext = 1
      go to 600
c
c          store fun in appropriate location
c
   50 continue
      if (iprnt3 .gt. 0) write (nunit,1050) alpha,fun
      go to (100,200,300,400) inext
c
c          store trial point a2
c
  100 continue
      a2 = alpha
      f2 = fun
      if (f2 .gt. f1) go to 150
      if (limit .eq. 1) go to 500
      alpha = 2.0 * a2
      inext = 3
      go to 600
c
  150 continue
      a3 = a2
      f3 = f2
      alpha = 0.1 * (9.0 * a1 + a3)
      if (alpha .le. 1.0e-08) go to 500
      inext = 2
      go to 600
c
c          point between a1 and a3
c
  200 continue
      jtry = jtry + 1
      if (jtry .gt. jmax) go to 500
c
      a2 = alpha
      f2 = fun
      if (f2 .gt. f1) go to 150
      go to 350
c
c          point after a2
c
  300 continue
c
      jtry = jtry + 1
      if (jtry .gt. jmax) go to 500
c
      a3 = alpha
      f3 = fun
      if (f3 .lt. f2) go to 310
      f12 = f1 - f2
      f32 = f3 - f2
      if (f12 .lt. 1.e-6) f12 = 1.e-6
      if (f32 .lt. 10.0 * f12) go to 350
      alpha = 0.5 * (a2 + a3)
      alim = a3
      inext = 3
      go to 600
  310 continue
      if (limit .eq. 1) go to 500
      call ksales (a1,a2,a3,f1,f2,f3,alim,alpha)
      a1 = a2
      f1 = f2
      a2 = a3
      f2 = f3
      inext = 3
      go to 600
c
c          perform quadratic or cubic interpolation
c
  350 continue
      if (a4 .le. 0.0) call ksquad (a1,a2,a3,f1,f2,f3,astar,fstar)
      if (a4 .gt. 0.0) call kscube (a1,a2,a3,a4,f1,f2,f3,f4,astar,fstar)
      alpha = astar
      inext = 4
      go to 600
c
c          check for convergence
c
  400 continue
c
      jtry = jtry + 1
      if (jtry .gt. jmax) go to 500
c
      if (atest .le. 0.0) go to 450
      adiv = alpha
      fdiv = abs(fun)
      if (adiv .lt. 1.e-06) adiv = 1.0
      if (fdiv .lt. 1.e-06) fdiv = 1.0
      aa = abs(atest - alpha)
      ff = abs(ftest - fun)
      atol = atoler * abs(alpha)
      ftol = atoler * abs(fun)
      if (atol .lt. 1.e-06) atol = 1.0e-06
      if (ftol .lt. 1.e-06) ftol = 1.0e-06
      if (aa .lt. atol .or. ff .lt. ftol) go to 500
      aa = aa / adiv
      ff = ff / fdiv
      if (aa .lt. rtoler .or. ff .lt. rtoler) go to 500
  450 continue
      atest = alpha
      ftest = fun
      call ksqmin (a1,a2,a3,a4,alpha,f1,f2,f3,f4,fun)
      go to 350
c
c          end of one-dimensional search
c
  500 continue
      inext = 0
      if (jtry .gt. jmax .or. limit .eq. 1) isdflg = -1
c
c          compute x-vector and get new function
c
  600 continue
      limit = 0
      if ( alpha .lt. alpmax) go to 610
      alpha = alpmax
      limit = 1
  610 continue
      do 620 i = 1,ndv
        x(i) = x0(i) + alpha * s(i)
  620 continue
      if (iprnt3 .lt. 1) return
      if (inext .gt. 0) write (nunit,1020) inext,jtry,alpha
      if (inext .eq. 0) write (nunit,1080) inext,jtry,alpha
      if (limit .eq. 1) write (nunit,1060)
      if (iprnt3 .lt. 2) return
      call ksunsc (temp,x,scale,ndv)
      if (inext .ne. 0) write (nunit,1030)
      if (inext .eq. 0) write (nunit,1070)
      write (nunit,1000) (ip1,i,temp(i),i=1,ndv)
      write (nunit,1040)
      return
c
 1000 format (3(3x,a1,i5,')=',e14.7))
 1010 format (/'       one-dimensional minimization started'/
     .        /'       initial alpha estimate = ',e12.6/
     .         '       maximum alpha allowed  = ',e12.6/
     .         '       initial k-s function   = ',e12.6/)
 1020 format ('       inext = ',i2,2x,' jtry = ',i2,2x,
     .        ' proposed alpha = ',e12.6)
 1030 format (/'       proposed design variable set'/)
 1040 format (/)
 1050 format (/'       current alpha = ',e12.6,'   function = ',e12.6)
 1060 format (/'       upper or lower bounds limit has been reached')
 1070 format (/'       final design variable set'/)
 1080 format ('       inext = ',i2,2x,' jtry = ',i2,2x,
     .        ' final alpha = ',e12.6)
      end


      subroutine ksdfp (x,side,act,dfun,ndv,s,slope,y,p,h,hess,
     .                  isdflg)
      dimension x(*),side(*),act(*),dfun(*),s(*),y(*),p(*),h(*)
      dimension hess(*)
c
c          routine to compute search direction using the
c          davidon-fletcher-powell method
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 19 July 1996
c
      if (isdflg .gt. 0) go to 40
c
c          reset approximate hessian matrix to initial condition
c
   10 continue
c
c          set h matrix to hess matrix
c
      isdflg = 0
      nh = ndv * (ndv + 1) / 2
      do 20 i = 1,nh
        h(i) = hess(i)
   20 continue
      do 30 i = 1,ndv
        act(i) = side(i)
   30 continue
      go to 80
c
c          dfp algorithm
c
   40 continue
c
c          check for validity of hessian matrix
c          in the case of side constraints
c
      do 50 i = 1,ndv
        if (side(i) .ne. act(i)) go to 10
   50 continue
c
c          compute vectors p and y
c
      do 60 i = 1,ndv
        y(i) = dfun(i) - y(i)
        p(i) = x(i) - p(i)
   60 continue
c
c          compute update to hessian matrix
c
      call kshmul (h,y,s,ndv)
      call ksvprd (p,y,sigma,ndv)
      call ksvprd (y,s,tau,ndv)
      if (abs(sigma) .lt. 1.0e-08) sigma = 1.0e-08
      if (abs(tau) .lt. 1.0e-08) tau = 1.0e-08
      sigma = 1.0 / sigma
      tau = 1.0 / tau
c
      k = 0
      do 70 i = 1,ndv
      do 70 j = 1,i
        k = k + 1
        h(k) = h(k) + sigma * p(i) * p(j) - tau * s(i) * s(j)
   70 continue
c
   80 continue
      call kshmul (h,dfun,s,ndv)
c
c          negate s-vector
c
      smax = 1.0e-08
      do 90 i = 1,ndv
        s(i) = -s(i)
        si = abs(s(i))
        if (si .gt. smax) smax = si
   90 continue
c
c          normalize search direction to unit maximum
c          and save previous gradients in y-vector
c          and previous design variables in p-vector
c
      do 100 i = 1,ndv
        s(i) = s(i) / smax
        y(i) = dfun(i)
        p(i) = x(i)
  100 continue
c
c          compute slope
c
      call ksvprd (s,dfun,slope,ndv)
      if (slope .gt. 0.0 .and. isdflg .gt. 0) go to 10
      if (slope .gt. 0.0) slope = 0.0
c
      return
      end


      subroutine ksquad (a1,a2,a3,f1,f2,f3,astar,fstar)
c
c          routine to compute the minimum of a quadratic
c          curve through points a1,f1 a2,f2 and a3,f3.
c          f2 must be less than f1 and f3.
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 27 April 1989
c
      f21 = f2 - f1
      a21 = a2 - a1
      if (a21 .lt. 1.0e-12) go to 10
      f31 = f3 - f1
      a31 = a3 - a1
      a3121 = a31 / a21
      a21s = a2 * a2 - a1 * a1
      a31s = a3 * a3 - a1 * a1
c
      d = a31s - a21s * a3121
      if (d .lt. 1.0e-12) go to 10
c
      a = (f31 - f21 * a3121) / d
      b = (f21 - a * a21s) / a21
      c = f1 - b * a1 - a * a1 * a1
c
      if (a .lt. 1.0e-12) go to 10
      astar = -b / (2.0 * a)
      if (astar .lt. a1 .or. astar .gt. a3) go to 10
      fstar = a * astar * astar + b * astar + c
      go to 30
c
   10 continue
      astar = a1
      fstar = f1
      if (f2 .ge. fstar) go to 20
      astar = a2
      fstar = f2
   20 continue
      if (f3 .ge. fstar) go to 30
      astar = a3
      fstar = f3
c
   30 continue
      return
      end


      subroutine ksqmin (a1,a2,a3,a4,alpha,f1,f2,f3,f4,fun)
c
c          routine to place alpha and fun in the appropriate
c          position relative to the three or four previously
c          defined points a1,f1 through a4,f4, dropping the
c          most distant point from the current minimum.
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 26 April 1989
c
      if (a4 .gt. 0.0) go to 20
c
      if (alpha .lt. a2) go to 10
      a4 = a3
      f4 = f3
      a3 = alpha
      f3 = fun
      go to 50
   10 continue
      a4 = a3
      f4 = f3
      a3 = a2
      f3 = f2
      a2 = alpha
      f2 = fun
      go to 50
   20 continue
      if (alpha .le. a2) go to 10
      if (alpha .lt. a3) go to 30
      a1 = a2
      f1 = f2
      a2 = a3
      f2 = f3
      a3 = alpha
      f3 = fun
      go to 50
   30 continue
      if ( (alpha - a1) .gt. (a4 - alpha) ) go to 40
      a4 = a3
      f4 = f3
      a3 = alpha
      f3 = fun
      go to 50
   40 continue
      a1 = a2
      f1 = f2
      a2 = alpha
      f2 = fun
c
   50 continue
      return
      end


      subroutine kshmul (a,b,x,nrow)
      dimension a(*),b(*),x(*)
c
c          routine to perform matrix multiplication a * b = x
c          for a triangular matrix a
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 17 July 1996
c
      k = 0
      do 10 i = 1,nrow
        x(i) = 0.0
   10 continue
      do 20 i = 1,nrow
      do 20 j = 1,i
        k = k + 1
        x(i) = x(i) + a(k) * b(j)
        if (j .lt. i) x(j) = x(j) + a(k) * b(i)
   20 continue
c
      return
      end


      subroutine ksvprd (x,y,prod,nrow)
      dimension x(*),y(*)
c
c          routine to multiply vectors x and y
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 17 July 1996
c
      prod = 0.0
      do 10 i = 1,nrow
        prod = prod + x(i) * y(i)
   10 continue
c
      return
      end


      subroutine ksales (a1,a2,a3,f1,f2,f3,alim,alpha)
c
c          routine to estimate the next one-dimensional
c          step size alpha, based on the slopes of the
c          previous two trial points.
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 21 April 1989
c
      alpha = 0.0
      alpmin = 2.0 * a3 - a2
      alpmax = 3.0 * a3
c
      if (abs(a2 - a1) .lt. 1.0e-08) go to 10
      x1 = a2
      y1 = (f2 - f1) / (a2 - a1)
c
      if (abs(a3 - a2) .lt. 1.0e-08) go to 10
      x2 = a3
      y2 = (f3 - f2) / (a3 - a2)
c
      a = (y2 - y1) / (x2 - x1)
      b = y1 - a * x1
c
      if (abs(a) .ge. 1.0e-08) alpha = -b / a
   10 continue
      if (alpha .le. 0.0   ) alpha = alpmax
      if (alpha .lt. alpmin) alpha = alpmin
      if (alpha .gt. alpmax) alpha = alpmax
      if (alim .gt. 0.0 .and. alpha .ge. alim) alpha = 0.5 * (a3 + alim)
c
      return
      end


      subroutine kscube (a1,a2,a3,a4,f1,f2,f3,f4,astar,fstar)
c
c          routine to compute the minimum of a cubic
c          curve through points a1,f1 a2,f2 a3,f3 and a4,f4.
c
c          author   - Gregory A. Wrenn
c          location - Lockheed Engineering and Sciences Co.
c                     144 Research Drive
c                     Hampton, Va. 23666
c
c          last modification - 27 April 1989
c
      a1c = a1 * a1 * a1
      a2c = a2 * a2 * a2
      a3c = a3 * a3 * a3
      a4c = a4 * a4 * a4
c
      q1 = a3c * (a2 - a1) - a2c * (a3 - a1) + a1c * (a3 - a2)
      q2 = a4c * (a2 - a1) - a2c * (a4 - a1) + a1c * (a4 - a2)
      q3 = (a3 - a2) * (a2 - a1) * (a3 - a1)
      q4 = (a4 - a2) * (a2 - a1) * (a4 - a1)
      q5 = f3 * (a2 - a1) - f2 * (a3 - a1) + f1 * (a3 - a2)
      q6 = f4 * (a2 - a1) - f2 * (a4 - a1) + f1 * (a4 - a2)
c
      d1 = q2 * q3 - q1 * q4
      d2 = a2 - a1
      if (d1 .lt. 1.0e-12 .or. d2 .lt. 1.0e-12) go to 10
c
      b3 = (q3 * q6 - q4 * q5) / d1
      b2 = (q5 - b3 * q1) / q3
      b1 = (f2 - f1) / d2 - b3 * (a2c - a1c) / d2 - b2 * (a1 + a2)
      b0 = f1 - b1 * a1 - b2 * a1 * a1 - b3 * a1c
c
      bb = b2 * b2 - 3.0 * b1 * b3
      if (bb .lt. 0.0 .or. abs(b3) .lt. 1.0e-12) go to 10
      astar = (-b2 + sqrt(bb)) / (3.0 * b3)
      if (astar .lt. a1 .or. astar .gt. a4) go to 10
      as2 = astar * astar
      as3 = astar * as2
      fstar = b0 + b1 * astar + b2 * as2 + b3 * as3
      go to 40
c
   10 continue
      astar = a1
      fstar = f1
      if (f2 .ge. fstar) go to 20
      astar = a2
      fstar = f2
   20 continue
      if (f3 .ge. fstar) go to 30
      astar = a3
      fstar = f3
   30 continue
      if (f4 .ge. fstar) go to 40
      astar = a4
      fstar = f4
c
   40 continue
      return
      end
