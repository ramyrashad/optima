      subroutine modmet2(jdim,kdim,q,xy,xyj,x,y)
c
      include '../include/arcom.inc'
c
      dimension xy(jdim,kdim,4),x(jdim,kdim),y(jdim,kdim),xyj(jdim,kdim)
      dimension q(jdim,kdim,4)
c
      tmp=1.d0/6.d0
      k=1
      j=jtail2
      jj=jtail1
      q(j,k,1)=q(j,k,1)*xyj(j,k)
      q(j,k,2)=q(j,k,2)*xyj(j,k)
      q(j,k,3)=q(j,k,3)*xyj(j,k)
      q(j,k,4)=q(j,k,4)*xyj(j,k)
c
        xy4= .5d0*tmp*(-x(j+2,k)+8.d0*(x(j+1,k)-x(j-1,k))+x(j-2,k)) 
        xy3= .5d0*tmp*(-y(j+2,k)+8.d0*(y(j+1,k)-y(j-1,k))+y(j-2,k))
        xy2= .5d0*tmp*(-x(j,k+2)+8.d0*(x(j,k+1)-x(jj,k+1))+x(jj,k+2))    
        xy1= .5d0*tmp*(-y(j,k+2)+8.d0*(y(j,k+1)-y(jj,k+1))+y(jj,k+2))
c        xy2=tmp*(-11.d0*x(j,k) + 18.d0*x(j,k+1) - 
c     >                               9.d0*x(j,k+2) + 2.d0*x(j,k+3))   
c        xy1=tmp*(-11.d0*y(j,k) + 18.d0*y(j,k+1) -
c     >                                   9.d0*y(j,k+2) + 2.d0*y(j,k+3))   
        dinv = xy4*xy1 - xy2*xy3       
        xyj(j,k)=1.d0/dinv
        dinv=xyj(j,k)
         xy(j,k,1) =   xy1*dinv                                   
         xy(j,k,2) = - xy2*dinv                                   
         xy(j,k,3) = - xy3*dinv                                   
         xy(j,k,4) =   xy4*dinv                                   
c
      q(j,k,1)=q(j,k,1)/xyj(j,k)
      q(j,k,2)=q(j,k,2)/xyj(j,k)
      q(j,k,3)=q(j,k,3)/xyj(j,k)
      q(j,k,4)=q(j,k,4)/xyj(j,k)
       return
       end
