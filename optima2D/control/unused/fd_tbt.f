c----------------------------------------------------------------------
c     -- calculate gradient using finite differences term-by-term --
c     -- used to verify gradient calculation --
c     
c     written by: marian nemec
c     date: october, 2000
c----------------------------------------------------------------------

      subroutine fd_tbt (obj, grad, jdim, kdim, ifirst, index, iex,
     &      q, cp, xy, xyj, x, y, cp_tar, bap, bcp, bt, bknot, dvs,
     &      idv, work1, ia, ja, ipa, jpa, iat, jat, ipt, jpt, as, ast,
     &      pa, pat)

#include "../include/arcom.inc"
#include "../include/optcom.inc"

      integer i, j, k, jdim, kdim, ifirst, idv(nc), iex(jdim,kdim,4)
      integer index(jdim,kdim)

      double precision obj, grad(nc), CalcObj
      double precision q(jdim,kdim,4), cp(jdim,kdim), xy(jdim,kdim,4)
      double precision xyj(jdim,kdim), x(jdim,kdim), y(jdim,kdim)
      double precision cp_tar(jbody,2), dvs(nc), bap(jbody,2)
      double precision bcp(nc,2), bt(jbody), bknot(jbsord+nc)
      
      double precision qplus(maxj,maxk,4), qminus(maxj,maxk,4)
      double precision dQdX(maxj,maxk,4), dOdQ(maxj,maxk,4)
      double precision press(maxj,maxk), sndsp(maxj,maxk)

c     -- sparse adjoint arrays --
      integer ia(jdim*kdim*4+1), ja(jdim*kdim*4*9*4)
      integer ipa(jdim*kdim*4+1), jpa(jdim*kdim*4*5*4)
      integer iat(jdim*kdim*4+1), jat(jdim*kdim*4*9*4)
      integer ipt(jdim*kdim*4+1), jpt(jdim*kdim*4*5*4)

      double precision as(jdim*kdim*4*9*4), ast(jdim*kdim*4*9*4)
      double precision pa(jdim*kdim*4*5*4), pat(jdim*kdim*4*5*4)

c     -- work arrays --
c     -- compatible with cyclone --
      double precision work1(jdim*kdim,95)

      double precision a(maxpen,4), b(maxpen,16), c(maxpen,16)
      double precision d(maxjk,16), e(maxpen,4), work2(maxjk,10)
      common/scratch/ a,b,c,d,e,work2

      double precision a2(maxj),b2(maxj),c2(maxj),d2(maxj),e2(maxj)
      double precision f2(maxj),g2(maxj),work3(maxj,30)
      common/wksp/ a2,b2,c2,d2,e2,f2,g2
      common/worksp/ work3

c     -- local variables --
      double precision tmp, stepsize, objp, objm

c     -- determine dJ/dQ --

      write (*,*) 'FD_TBT'

      call calcps ( jdim, kdim, q, press, sndsp, work1, xy, xyj)
      call dOBJdQ (jdim, kdim, x, y, xy, xyj, q, dOdQ, cp_tar, index,
     &      cp, press)

      do n = 1,4
        do j = jtail1,jtail2
          dOdQ(j,1,n) = dOdQ(j,1,n)/xyj(j,1)
        end do
      end do

      do in = 1,ndv

c     -- determine dQdX --
        if (.not.cdf) then

          tmp = dvs(in)
          stepsize = fd_eta*dvs(in)
          dvs(in) = dvs(in) + stepsize
          stepsize = dvs(in) - tmp
          
c     -- regrid domain --
          call regrid (in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot )
          
c     -- calculate new state vector value (+) --
          rewind (his_unit)
          do n=1,4
            do k = 1,kend
              do j= 1,jend
                qplus(j,k,n) = q(j,k,n)
              end do
            end do
          end do
          
          call flow_solve (jdim, kdim, ifirst, index, iex, ia, ja,
     &          as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, qplus,
     &          cp, xy, xyj, x, y, work1)
          
c     -- calculate state vector sensitivity --
          dxx = 1.d0/stepsize
          do n=1,4
            do k = 1,kend
              do j= 1,jend
                dQdX(j,k,n) = ( qplus(j,k,n) - q(j,k,n) )*dxx
              end do
            end do
          end do
          dvs(in) = tmp
          
        else
c     -- centered-difference approximation --
          
          tmp = dvs(in)
          stepsize = fd_eta*dvs(in)
          dvs(in) = dvs(in) + stepsize
          stepsize = dvs(in) - tmp
          
c     -- regrid domain --
          call regrid ( in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot )
          
c     -- calculate new state vector value (+) --
          rewind (his_unit)
          do n=1,4
            do k = 1,kend
              do j= 1,jend
                qplus(j,k,n) = q(j,k,n)
              end do
            end do
          end do

          call flow_solve (jdim, kdim, ifirst, index, iex, ia, ja,
     &          as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, qplus,
     &          cp, xy, xyj, x, y, work1)
          
          dvs(in) = tmp - stepsize
          call regrid ( in, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &          bknot )
          
c     -- calculate new state vector value (-) --
          rewind (his_unit)
          do n=1,4
            do k = 1,kend
              do j= 1,jend
                qminus(j,k,n) = q(j,k,n)
              end do
            end do
          end do
          
          call flow_solve (jdim, kdim, ifirst, index, iex, ia, ja,
     &          as, ipa, jpa, pa, iat, jat, ast, ipt, jpt, pat, qminus,
     &          cp, xy, xyj, x, y, work1)
          
c     -- calculate state vector sensitivity --
          dxx = 1.d0/(2.d0*stepsize)
          do n=1,4
            do k = 1,kend
              do j= 1,jend
                dQdX(j,k,n) = ( qplus(j,k,n) - qminus(j,k,n) )*dxx
              end do
            end do
          end do
          dvs(in) = tmp

        end if

c     -- restore original grid --
        call regrid (-1, jdim, kdim, dvs, idv, x, y, bap, bcp, bt,
     &        bknot)
        
c     -- recalculate metrics --
        call xidif ( jdim, kdim, x, y, xy)
        call etadif ( jdim, kdim, x, y, xy)
        call calcmet ( jdim, kdim, xy, xyj)

        grad(in) = 0.d0
        do n = 1,4
          do k = 1,kend
            do j = 1,jend
              grad(in) = grad(in) + dOdQ(j,k,n)*dQdX(j,k,n)
            end do
          end do
        end do
        
c     -- drag minimization requires sensitivity wrt design variables --
        call dObjdX (in, dOdX, jdim, kdim, dvs, idv, x, y, bap, bcp,
     &        bt, bknot, q, press, xy, xyj)
          
        grad(in) = grad(in) + dOdX
      end do      

      return
      end                       !fd_verify
