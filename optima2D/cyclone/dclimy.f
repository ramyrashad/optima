c     CUSP Dissipation
c     -- evaluation of limiter --
c     -- note: limiter=1 => 3rd order dissipation
c     limiter=2 => 1st order dissipation
c     limiter=3 => Zingg-Nemec limiter, pressure only
c     limiter=4 => Zingg-Nemec limiter
c     limiter=5 => Venkatakrishnan limiter

c     Written by: Marian Nemec
c     Date: April 1998
c     Mods: March 2000

      subroutine dclimy (jdim,kdim,q,xyj,x,y,tmp,p,n,swr)
     
#include "../include/arcom.inc"
     
      integer i,j,k,n
     
      dimension q(jdim,kdim,4),xyj(jdim,kdim),x(jdim,kdim),p(jdim,kdim)
      dimension y(jdim,kdim),tmp(jdim,kdim,4),swr(jdim,kdim)
     
      double precision top,y_xi,y_eta,us,vs,uspvs
     
      if ( n.eq.1 ) then
        do i = 1,4
          do k = kbegin,kup
            do j = jlow,jup
              tmp(j,k,i) = q(j,k+1,i)*xyj(j,k+1) - q(j,k,i)*xyj(j,k)
            end do
          end do
        end do
      end if

      if (limiter.eq.1) then
c     -- 3rd order dissipation only --
        do k = kbegin,kup
          do j = jlow,jup
            swr(j,k) = 1.d0
          end do
        end do

      else if (limiter.eq.2) then
c     -- 1st order disspation only --
        do k = kbegin,kup
          do j = jlow,jup
            swr(j,k) = 0.d0
          end do
        end do

      else if (limiter.eq.3) then
c     -- Zingg-Nemec limiter based on pressure value --
        do k = klow,kup-1
          do j = jlow,jup
            us = p(j,k+2)*xyj(j,k+2) - p(j,k+1)*xyj(j,k+1)
            vs = p(j,k)*xyj(j,k) - p(j,k-1)*xyj(j,k-1)
            uspvs = dabs(us) + dabs(vs) + 1.d-12
            top = epz / uspvs
            swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
          end do
        end do

c     -- boundary nodes --

        k = kbegin
        do j = jlow,jup
          us = p(j,k+2)*xyj(j,k+2) - p(j,k+1)*xyj(j,k+1)
          vs = p(j,k+1)*xyj(j,k+1) - p(j,k)*xyj(j,k)
          uspvs = dabs(us) + dabs(vs) + 1.d-12
          top = epz / uspvs
          swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
        end do

        k = kup
        do j = jlow,jup
          us = p(j,k+1)*xyj(j,k+1) - p(j,k)*xyj(j,k)
          vs = p(j,k)*xyj(j,k) - p(j,k-1)*xyj(j,k-1)
          uspvs = dabs(us) + dabs(vs) + 1.d-12
          top = epz / uspvs
          swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
        end do

      else if (limiter.eq.4) then
c     -- Zingg-Nemec limiter --
        do k = klow,kup-1
          do j = jlow,jup
            us = tmp(j,k+1,n)
            vs = tmp(j,k-1,n)
            uspvs = dabs(us) + dabs(vs) + 1.d-12
            top = epz / uspvs
            swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
          end do
        end do

c     -- boundary nodes --

        k = kbegin
        do j = jlow,jup
          us = tmp(j,k+1,n)
          vs = tmp(j,k,n)
          uspvs = dabs(us) + dabs(vs) + 1.d-12
          top = epz / uspvs
          swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
        end do 

        k = kup
        do j = jlow,jup
          us = tmp(j,k,n)
          vs = tmp(j,k-1,n)
          uspvs = dabs(us) + dabs(vs) + 1.d-12
          top = epz / uspvs
          swr(j,k) = 1.d0 - dabs( ( us - vs )/( uspvs + top ) )**2
        end do

      else if (limiter.eq.5) then
c     -- Venkatakrishnan limiter --
        do k = klow,kup-1
          do j = jlow,jup
            y_eta = 0.5d0*( y(j,k+1) - y(j,k-1) )
            y_xi = 0.5d0*( y(j+1,k) - y(j-1,k) )
            us = tmp(j,k+1,n)
            vs = tmp(j,k-1,n)
            top = dsqrt(( epv*dsqrt ( y_xi**2 + y_eta**2 ))**3)
            uspvs = dabs(us) + dabs(vs)
            swr(j,k) = 1.d0 - dabs( ( us - vs )/
     $            dmax1( uspvs,top ) )**2
          end do
        end do

c     -- boundary nodes --

        k = kbegin
        do j = jlow,jup
          y_eta = y(j,k+1) - y(j,k)
          y_xi = 0.5d0*( y(j+1,k) - y(j-1,k) )
          us = tmp(j,k+1,n)
          vs = tmp(j,k,n)
          top = dsqrt(( epv*dsqrt ( y_xi**2 + y_eta**2 ))**3)
          uspvs = dabs(us) + dabs(vs)
          swr(j,k) = 1.d0 - dabs( ( us - vs )/
     $          dmax1( uspvs,top ) )**2
        end do 

        k = kup
        do j = jlow,jup
          y_eta = 0.5d0*( y(j,k+1) - y(j,k-1) )
          y_xi = 0.5d0*( y(j+1,k) - y(j-1,k) )
          us = tmp(j,k,n)
          vs = tmp(j,k-1,n)
          top = dsqrt( (epv*dsqrt ( y_xi**2 + y_eta**2 ))**3)
          uspvs = dabs(us) + dabs(vs)
          swr(j,k) = 1.d0 - dabs( ( us - vs )/
     $          dmax1( uspvs,top ) )**2
        end do
      end if

      return
      end                       !dclimy
