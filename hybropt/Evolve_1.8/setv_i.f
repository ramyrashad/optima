c======================================================================
c
      subroutine setv_i(m,is,int)

C     ARC2D V3.00 by Thomas H. Pulliam
C     NASA Ames Research Center
C     Copyright 1992 
C     Restricted to United States (NO Foreign Dissemination!!)

      dimension int(m)
      do i= 1,m
         int(i)= is
      enddo

      return
      end
