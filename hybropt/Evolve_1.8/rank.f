C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C                     RRRRR   AAAAAA  NN    N  K   K                   C
C                     R    R  A    A  N N   N  K  K                    C
C                     RRRRR   AAAAAA  N  N  N  KK                      C
C                     R    R  A    A  N   N N  K  K                    C
C                     R    R  A    A  N    NN  K   K                   C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE RANK(N,nchrom,ngene,nobfun,pop,fit,irank,
     $     ndim,nacc,pop_acc,fit_acc,i_acc,iaccum,i_mnx,fit_init,
     $     eps_domin,debug)
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C           *** THIS SUBROUTINE COMPUTES THE RANKING FOR EACH          C
C               OF THE NCHROM CHROMOSOMES. FOR MULTI-OBJECTIVE         C
C               CASES (NOBFUN>1) GOLDBERG RANKING IS USED ***          C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
      dimension fit(nobfun,nchrom),irank(nchrom),pop(ngene,nchrom)
C
      dimension fit_acc(nobfun,ndim), i_acc(2,ndim), pop_acc(ngene,ndim)
      DIMENSION         FITDUM(nobfun,ndim), popdum(ngene,ndim)
      DIMENSION         IACCDUM(2,ndim)
      logical debug
C
C-----------------------------------------------------------------------
      if(debug)write(49,*)' RANK routine'
C-----------------------------------------------------------------------
      if(debug)then
         write(49,*)'In rank before rank'
         write(49,*)' nc          fit = '
         do nc = 1,nchrom
            write(49,'(i5,3(2x,e24.16))')
     $           nc,(fit(no,nc),no=1,nobfun)
         enddo
      endif
C-----------------------------------------------------------------------
C          *** COMPUTE RANKING ARRAY (IRANK) ***
C
      DO NC=1,NCHROM
         IRANK(NC) = 0
      ENDDO
      DO ICOUNT=1,NCHROM
         INC = 0
         DO NC=1,NCHROM
            IF (IRANK(NC).NE.0) THEN
               INC = INC+1
               GO TO 1001
            ENDIF
            DO NCC=1,NCHROM
               IF (IRANK(NCC).NE.0.AND.IRANK(NCC).NE.ICOUNT) GO TO 1002
               INOF1 = 0
               INOF2 = 0
               DO NO=1,NOBFUN
                  IF (FIT(NO,NC).LE.FIT(NO,NCC).and.i_mnx.eq.1) THEN
                     INOF1 = INOF1+1
                  elseIF (FIT(NO,NC).ge.FIT(NO,NCC).and.i_mnx.eq.0) THEN
                     INOF1 = INOF1+1
                  ENDIF
                  IF (FIT(NO,NC).LT.FIT(NO,NCC).and.i_mnx.eq.1) THEN
                     INOF2 = 1
                  elseIF (FIT(NO,NC).gT.FIT(NO,NCC).and.i_mnx.eq.0) THEN
                     INOF2 = 1
                  ENDIF
               ENDDO
               IF (INOF1.EQ.NOBFUN.AND.INOF2.EQ.1) GO TO 1001
 1002          CONTINUE
            ENDDO
            IRANK(NC) = ICOUNT
 1001       CONTINUE
         ENDDO
         IF (INC.EQ.NCHROM) GO TO 1003
      ENDDO
 1003 CONTINUE

C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** CHECK CURRENT RANKS AGAINST THE ACCUMULATED FILE
C              OF NUMBER ONE RANKS (FIT_ACC) SO THAT THE PARETO FRONT
C              WILL NOT DIGRESS. (NOBFUN>1 AND IACCUM=1) ***
C
C      IACCUM = 1
C
C-----------------------------------------------------------------------
C          *** SAVE CURRENT NUMBER ONE RANKED CHROMOSOMES--FITNESS
C              VALUES ONLY--INTO LARGE ACCUMULATION ARRAY (FIT_ACC).
C              DO THIS FOR IPOP=1, NOBFUN>1 AND IACCUM=1 ONLY ***
C
      IF (N.EQ.1.AND.NOBFUN.GT.1.AND.IACCUM.EQ.1) THEN
         NACC = 0
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               NACC = NACC+1
               DO NO=1,NOBFUN
                  FIT_ACC(NO,NACC) = FIT(NO,NC)
               ENDDO
               do ng = 1,ngene
                  pop_acc(ng,nacc) = pop(ng,nc)
               enddo
               I_ACC(1,NACC) = N
               I_ACC(2,NACC) = NC
            ENDIF
         ENDDO
      ENDIF
C
C-----------------------------------------------------------------------
C          *** REDUCE RANKING (I.E., INCREASE IRANK VALUE) FOR 
C              MEMBERS OF CURRENT GENERATION THAT ARE DOMINATED 
C              BY AT LEAST ONE INDIVIDUAL FROM THE ACCUMULATED 
C              FILE (FIT_ACC). DO THIS ONLY WHEN NOBFUN>1, IPOP>1 
C              AND IACCUM=1 ***
C
      IF (NOBFUN.GT.1.AND.N.GT.1.AND.IACCUM.EQ.1) THEN
         DO NC=1,NCHROM
            IF (IRANK(NC).GE.2) IRANK(NC) = IRANK(NC)+1
         ENDDO
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               DO NCC=1,NACC
                  ITEST1 = 0
                  ITEST2 = 0
                  DO NO=1,NOBFUN
                     IF(FIT_ACC(NO,NCC).GE.FIT(NO,NC)
     $                    .and.i_mnx.eq.1)THEN
                        ITEST1 = ITEST1+1
                     elseIF(FIT_ACC(NO,NCC).LE.FIT(NO,NC)
     $                       .and.i_mnx.eq.0)THEN
                        ITEST1 = ITEST1+1
                     ENDIF
                  ENDDO
                  DO NO=1,NOBFUN
                     IF (FIT_ACC(NO,NCC).GT.FIT(NO,NC)
     $                    .and.i_mnx.eq.1)THEN
                        ITEST2 = 1
                     ELSEIF (FIT_ACC(NO,NCC).LT.FIT(NO,NC)
     $                    .and.i_mnx.eq.0)THEN
                        ITEST2 = 1
                     ENDIF
                  ENDDO
                  IF (ITEST1.EQ.NOBFUN.AND.ITEST2.EQ.1) THEN
                     IRANK(NC) = 2
                  ENDIF
               ENDDO
            ENDIF
         ENDDO
      ENDIF
C
cthp_2004C-----------------------------------------------------------------------
cthp_2004C          *** ADD INDIVIDUALS TO THE ACCUMULATED FILE, BUT
cthp_2004C              ONLY IF THEY DO NOT ALREADY EXIST IN THE 
cthp_2004C              ACCUMULATED FILE. DO THIS ONLY WHEN NOBFUN>1, 
cthp_2004C              IPOP>1 AND IACCUM=1 ***
cthp_2004C
cthp_2004      IF (NOBFUN.GT.1.AND.N.GT.1.AND.IACCUM.EQ.1) THEN
cthp_2004         DO NC=1,NCHROM
cthp_2004            IF (IRANK(NC).EQ.1) THEN
cthp_2004               DO NCC=1,NACC
cthp_2004                  ITEST = 0
cthp_2004                  DO NO=1,NOBFUN
cthp_2004                     IF (FIT_ACC(NO,NCC).EQ.FIT(NO,NC)) THEN
cthp_2004                        ITEST = ITEST+1
cthp_2004                     ENDIF
cthp_2004                  ENDDO
cthp_2004                  IF (ITEST.EQ.NOBFUN) GO TO 2001
cthp_2004               ENDDO
cthp_2004C
cthp_2004               NACC = NACC+1
cthp_2004               DO NO=1,NOBFUN
cthp_2004                  FIT_ACC(NO,NACC) = FIT(NO,NC)
cthp_2004               ENDDO
cthp_2004               do ng = 1,ngene
cthp_2004                  pop_acc(ng,nacc) = pop(ng,nc)
cthp_2004               enddo
cthp_2004               I_ACC(1,NACC) = N
cthp_2004               I_ACC(2,NACC) = NC
cthp_2004            ENDIF
cthp_2004 2001    CONTINUE
cthp_2004         ENDDO
cthp_2004      ENDIF
C
C-----------------------------------------------------------------------
C          *** REMOVE INDIVIDUALS FROM THE ACCUMULATED FILE
C              OF NUMBER ONE RANKS IF AN INDIVIDUAL FROM THE 
C              ACTIVE FILE (FIT) DOMINATES. DO THIS ONLY WHEN 
C              NOBFUN>1, IPOP>1 AND IACCUM=1 ***
C
      IF (NOBFUN.GT.1.AND.N.GT.1.AND.IACCUM.EQ.1) THEN
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               DO NCC=1,NACC
                  ITEST1 = 0
                  ITEST2 = 0
                  DO NO=1,NOBFUN
                     IF (FIT(NO,NC)
     $                    *(1.0+eps_domin)
     $                    .GE.FIT_ACC(NO,NCC)
     $                    .and.i_mnx.eq.1)THEN
                        ITEST1 = ITEST1+1
                     ElSEIF (FIT(NO,NC)
     $                       /(1.0+eps_domin)
     $                       .LE.FIT_ACC(NO,NCC)
     $                    .and.i_mnx.eq.0)THEN
                        ITEST1 = ITEST1+1
                     ENDIF
                  ENDDO
                  DO NO=1,NOBFUN
                     IF (FIT(NO,NC)
     $                    *(1.0+eps_domin)
     $                    .GT.FIT_ACC(NO,NCC)
     $                    .and.i_mnx.eq.1)THEN
                        ITEST2 = 1
                     elseIF (FIT(NO,NC)
     $                    /(1.0+eps_domin)
     $                       .LT.FIT_ACC(NO,NCC)
     $                    .and.i_mnx.eq.0)THEN
                        ITEST2 = 1
                     ENDIF
                  ENDDO
                  IF (ITEST1.EQ.NOBFUN.AND.ITEST2.EQ.1) THEN
                        DO NO=1,NOBFUN
                           FIT_ACC(NO,NCC) = fit_init
                        ENDDO
                  ENDIF
               ENDDO
            ENDIF
         ENDDO
C
         IAC  = NACC
         ICOUNT = 0
         DO NC=1,NACC
            ITEST = 0
            DO NO=1,NOBFUN
               IF (FIT_ACC(NO,NC).EQ. fit_init)then
                  ITEST = ITEST+1
               ENDIF
            ENDDO
            IF (ITEST.NE.NOBFUN) THEN
               ICOUNT = ICOUNT+1
               DO NO=1,NOBFUN
                  FITDUM(NO,ICOUNT) = FIT_ACC(NO,NC)
               ENDDO
               do ng = 1,ngene
                  popdum(ng,icount) = pop_acc(ng,nc)
               enddo
               IACCDUM(1,ICOUNT) = I_ACC(1,NC)
               IACCDUM(2,ICOUNT) = I_ACC(2,NC)
            ELSE
               IAC = IAC-1
            ENDIF
         ENDDO
C
         NACC = IAC
         DO NC=1,NACC
            DO NO=1,NOBFUN
               FIT_ACC(NO,NC) = FITDUM(NO,NC)
            ENDDO
            do ng = 1,ngene
               pop_acc(ng,nc) = popdum(ng,nc)
            enddo
            I_ACC(1,NC) = IACCDUM(1,NC)
            I_ACC(2,NC) = IACCDUM(2,NC)
         ENDDO
C
chtp  8/25/2004 Moved this from above otherwise eps_domin logic wipes out accum file
C-----------------------------------------------------------------------
C          *** ADD INDIVIDUALS TO THE ACCUMULATED FILE, BUT
C              ONLY IF THEY DO NOT ALREADY EXIST IN THE 
C              ACCUMULATED FILE. DO THIS ONLY WHEN NOBFUN>1, 
C              IPOP>1 AND IACCUM=1 ***
C
      IF (NOBFUN.GT.1.AND.N.GT.1.AND.IACCUM.EQ.1) THEN
         DO NC=1,NCHROM
            IF (IRANK(NC).EQ.1) THEN
               DO NCC=1,NACC
                  ITEST = 0
                  DO NO=1,NOBFUN
                     IF (FIT_ACC(NO,NCC).EQ.FIT(NO,NC)) THEN
                        ITEST = ITEST+1
                     ENDIF
                  ENDDO
                  IF (ITEST.EQ.NOBFUN) GO TO 2001
               ENDDO
C
               NACC = NACC+1
               DO NO=1,NOBFUN
                  FIT_ACC(NO,NACC) = FIT(NO,NC)
               ENDDO
               do ng = 1,ngene
                  pop_acc(ng,nacc) = pop(ng,nc)
               enddo
               I_ACC(1,NACC) = N
               I_ACC(2,NACC) = NC
            ENDIF
 2001    CONTINUE
         ENDDO
      ENDIF
C-----------------------------------------------------------------------
C           *** SORT THE ACCUMULATED FILE PARETO FRONT USING
C               THE FIRST FITNESS VALUE ***
         CALL SORT_par(NOBFUN,NACC,FIT_ACC,ngene,pop_acc,I_ACC)
C
C-----------------------------------------------------------------------
C
      ENDIF
C          *** RANKING OPERATION COMPLETE ***
C-----------------------------------------------------------------------
      if(debug)then
         write(49,*)'In rank after rank'
         write(49,*)' nc      irank        fit = '
         do nc = 1,nchrom
            write(49,'(i5,2x,i5,3(2x,e24.16))')
     $           nc,irank(nc),(fit(no,nc),no=1,nobfun)
         enddo

         write(52,*)'# Ranking at Gen = ',N
         write(52,*)'# Chromo#     rank        fit = '
         do nc = 1,nchrom
            write(52,'(3x,i5,2x,i5,3(2x,e24.16))')
     $           nc,irank(nc),(fit(no,nc),no=1,nobfun)
         enddo
      endif
C-----------------------------------------------------------------------
C
      RETURN
      END
