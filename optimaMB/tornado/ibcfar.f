c     ------------------------------------------------------------------
c     -- farfield boundary conditions --
c     -- bctype = 2, side 1 or 3 --
c     -- loosely based on probe (a. pueyo) --
c     -- m. nemec, aug. 2001 --
c     ------------------------------------------------------------------
      subroutine ibcfs13( imps, iblk, iside, jmax, kmax, nmax, jbegin,
     &     jend, k1, k2, indx, q, xy, xyj, sgn, pa, as, icol, bcf, iex,
     &     gamma, gami, maxjb, mxbfine, precmat, jacmat)

      implicit none

      integer iblk, iside, jmax, kmax, nmax, jbegin, jend, k1, k2 
      integer j,k,maxjb,mxbfine,n1,n2,n3,nn,ki,ij,ii,ip,itmp
      integer iex(maxjb,4,4,mxbfine),indx(jmax,kmax),icol(*)
      double precision q(jmax,kmax,4),xy(jmax,kmax,4),pa(*),as(*)
      double precision bcf(maxjb,4,4,mxbfine),xyj(jmax,kmax),sgn,b0
      double precision xy3,xy4,par,rho,u,v,pr,a,vn1,gamma,gami,vn,b1        
      logical   precmat,jacmat,imps

c     -- local variables --
      double precision pm(4,4), pn(4,4), cc(4,4), c1(4,4)

c      write (*,*) 'ibcfs13', precmat, jacmat, nmax
      
      do j=jbegin,jend

         k = k1         
c     -- metrics terms --
         par = sqrt(xy(j,k,3)**2+xy(j,k,4)**2)
         xy3 = xy(j,k,3)/par*sgn
         xy4 = xy(j,k,4)/par*sgn

c     -- matrix for k=k1 --
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &         q(j,k,1))*xyj(j,k)
         a   = sqrt(gamma*pr/rho)                    
         vn1 =  xy3*u + xy4*v

c     -- matrix pm**-1 --
         pm(1,1) = 1.0
         pm(2,1) = -u/rho
         pm(3,1) = -v/rho
         pm(4,1) = 0.5*(u*u+v*v)*gami
         pm(1,2) = 0.0
         pm(2,2) = 1.0/rho
         pm(3,2) = 0.0
         pm(4,2) = -u*gami
         pm(1,3) = 0.0
         pm(2,3) = 0.0
         pm(3,3) = 1.0/rho
         pm(4,3) = -v*gami
         pm(1,4) = 0.0
         pm(2,4) = 0.0
         pm(3,4) = 0.0
         pm(4,4) = gami
c     -- matrix pn --
         pn(1,1) = a/(gami*rho)
         pn(2,1) = -pn(1,1)
         pn(3,1) = gamma*rho**gami/pr
         pn(4,1) = 0.0
         pn(1,2) = xy3
         pn(2,2) = xy3
         pn(3,2) = 0.0
         pn(4,2) = -xy4
         pn(1,3) = xy4
         pn(2,3) = xy4
         pn(3,3) = 0.0
         pn(4,3) = xy3
         pn(1,4) = -gamma/(gami*a*rho)
         pn(2,4) = -pn(1,4)
         pn(3,4) = -rho**gamma/pr**2
         pn(4,4) = 0.0

c     -- multiplication & storage --
         do n1 = 1,4
            do n2 = 1,4
               cc(n1,n2) = 0.0
               do nn = 1,4
                  cc(n1,n2) = cc(n1,n2) + pn(n1,nn)*pm(nn,n2)*xyj(j,k)
               end do
            end do
         end do

c     -- matrix for interior node ----
         k = k2
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &        q(j,k,1))*xyj(j,k)
         a   = sqrt(gamma*pr/rho)                    

c     -- matrix m2**-1 --
         pm(1,1) = 1.0
         pm(2,1) = -u/rho
         pm(3,1) = -v/rho
         pm(4,1) = 0.5*(u*u+v*v)*gami
         pm(1,2) = 0.0
         pm(2,2) = 1.0/rho
         pm(3,2) = 0.0
         pm(4,2) = -u*gami
         pm(1,3) = 0.0
         pm(2,3) = 0.0
         pm(3,3) = 1.0/rho
         pm(4,3) = -v*gami
         pm(1,4) = 0.0
         pm(2,4) = 0.0
         pm(3,4) = 0.0
         pm(4,4) = gami
c     -- matrix pn --
         pn(1,1) = 0.0
         pn(2,1) = -a/(gami*rho)
         pn(1,2) = 0.0
         pn(2,2) = xy3
         pn(1,3) = 0.0
         pn(2,3) = xy4
         pn(1,4) = 0.0
         pn(2,4) = gamma/(gami*a*rho)
c     -- inflow --
         if ( vn1.le.0.0 ) then
            pn(3,1) = 0.0
            pn(4,1) = 0.0
            pn(3,2) = 0.0
            pn(4,2) = 0.0
            pn(3,3) = 0.0
            pn(4,3) = 0.0
            pn(3,4) = 0.0
            pn(4,4) = 0.0
c     -- outflow --
         else
            pn(3,1) = gamma*rho**gami/pr
            pn(4,1) = 0.0
            pn(3,2) = 0.0
            pn(4,2) = -xy4
            pn(3,3) = 0.0
            pn(4,3) = xy3
            pn(3,4) = -rho**gamma/pr**2
            pn(4,4) = 0.0
         endif
c     -- multiplication --
         do n1 = 1,4
            do n2 = 1,4
               c1(n1,n2) = 0.0
               do nn = 1,4
                  c1(n1,n2) = c1(n1,n2) - pn(n1,nn)*pm(nn,n2)*xyj(j,k)
               end do
            end do
         end do

         k = k1
c     -- reordering  --
c     -- finding the biggest --  
         do n2 = 1,3
            ki = n2
            b0 = dabs( cc(n2,n2) )
            do n1 = n2+1,4
               b1 = dabs( cc(n1,n2) )
               if ( b1.gt.b0 ) then
                  ki = n1
                  b0 = b1
               end if
            end do
c     -- exchanging rows --
            if ( ki.ne.n2 ) then
               do n3 =1,4
                  b1 = cc(n2,n3)
                  cc(n2,n3) = cc(ki,n3)
                  cc(ki,n3) = b1
                  b1 = c1(n2,n3)
                  c1(n2,n3) = c1(ki,n3)
                  c1(ki,n3) = b1
               end do
               itmp = iex(j,n2,iside,iblk)
               iex(j,n2,iside,iblk) = iex(j,ki,iside,iblk)
               iex(j,ki,iside,iblk) = itmp
            endif
         end do

c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
         if ( abs(cc(4,4)).lt.1.e-7 ) then
            do n3 =1,4
               b1 = cc(2,n3)
               cc(2,n3) = cc(4,n3)
               cc(4,n3) = b1
               b1 = c1(2,n3)
               c1(2,n3) = c1(4,n3)
               c1(4,n3) = b1
            end do
            itmp = iex(j,2,iside,iblk)
            iex(j,2,iside,iblk) = iex(j,4,iside,iblk)
            iex(j,4,iside,iblk) = itmp
         endif 

c     -- store diagonal scaling --
         do n1 =1,4
            bcf(j,n1,iside,iblk) = 1.0/cc(n1,n1)
         end do

c     -- store in jacobian matrix --
         if (imps) then

            if (precmat) then
               k = k1
               do n1 = 1,4
                  ij = ( indx(j,k)-1)*nmax + n1
                  ip = ( ij - 1 )*icol(5)
                  do n2 = 1,4
                     pa(ip        +n2) = cc(n1,n2)
                     pa(ip+icol(1)+n2) = c1(n1,n2)
                  end do
               end do
            end if

            if (jacmat) then
               k = k1
               do n1 = 1,4
                  ij = ( indx(j,k)-1)*nmax + n1
                  ii = ( ij - 1 )*icol(9)
                  do n2 = 1,4
                     as(ii        +n2) = cc(n1,n2)
                     as(ii+icol(1)+n2) = c1(n1,n2)
                  end do
               end do
            end if

         end if
      end do

      if (nmax.eq.5 .and. imps) then
c     -- s-a model outer-boundary --
         k = k1       
         do j = jbegin,jend
            par = sqrt(xy(j,k,3)**2 + xy(j,k,4)**2)
            xy3 = xy(j,k,3)/par*sgn
            xy4 = xy(j,k,4)/par*sgn

            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)
            vn  = xy3*u + xy4*v

            n1 = 5
            n2 = 5

            if (vn .le. 0.0) then
c     -- inflow --
               ij = ( indx(j,k)-1)*nmax + n1
               ii = ( ij - 1 )*icol(9)
               ip = ( ij - 1 )*icol(5)

               if (jacmat)  as(ii+n2) = xyj(j,k1)
               if (precmat) pa(ip+n2) = xyj(j,k1)
            else
c     -- outflow --          
               ij = ( indx(j,k)-1)*nmax + n1
               ii = ( ij - 1 )*icol(9)
               ip = ( ij - 1 )*icol(5)

               if (jacmat) then
                  as(ii+        n2) =  xyj(j,k1)
                  as(ii+icol(1)+n2) = -xyj(j,k2)
               end if

               if (precmat) then
                  pa(ip+        n2) =  xyj(j,k1)
                  pa(ip+icol(1)+n2) = -xyj(j,k2)
               end if
            endif
         end do
      end if
      
      return 
      end                       !ibcfs13
c     ------------------------------------------------------------------
c     -- bctype = 2, side 2 or 4 --
c     ------------------------------------------------------------------
      subroutine ibcfs24( imps, iblk, iside, jmax, kmax, nmax, j1, j2, 
     &     kbegin, kend, indx, q, xy, xyj, sgn, pa, as, icol, bcf, iex,
     &     gamma, gami, maxjb, mxbfine, precmat, jacmat)

      implicit none

      integer iblk, iside, jmax, kmax, nmax, kbegin, kend, j1, j2 
      integer j,k,maxjb,mxbfine,n1,n2,n3,nn,ki,ij,ii,ip,itmp
      integer iex(maxjb,4,4,mxbfine),indx(jmax,kmax),icol(*)
      double precision q(jmax,kmax,4),xy(jmax,kmax,4),pa(*),as(*)
      double precision bcf(maxjb,4,4,mxbfine),xyj(jmax,kmax),sgn,b0
      double precision xy1,xy2,par,rho,u,v,pr,a,vn1,gamma,gami,vn,b1        
      logical   precmat,jacmat,imps

c     -- local variables --
      double precision pm(4,4), pn(4,4), cc(4,4), c1(4,4)

c      write (*,*) 'ibcfs24', precmat, jacmat, nmax

      do k = kbegin, kend
         j = j1

c     -- matrix for j1 --
         par = sqrt(xy(j,k,1)**2+xy(j,k,2)**2)
         xy1 = xy(j,k,1)/par*sgn
         xy2 = xy(j,k,2)/par*sgn

         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &        q(j,k,1))*xyj(j,k)
         a   = sqrt(gamma*pr/rho)                    
         vn1 = xy1*u + xy2*v

c     -- matrix m**-1 --
         pm(1,1) = 1.0
         pm(2,1) = -u/rho
         pm(3,1) = -v/rho
         pm(4,1) = 0.5*(u*u+v*v)*gami
         pm(1,2) = 0.0
         pm(2,2) = 1.0/rho
         pm(3,2) = 0.0
         pm(4,2) = -u*gami
         pm(1,3) = 0.0
         pm(2,3) = 0.0
         pm(3,3) = 1.0/rho
         pm(4,3) = -v*gami
         pm(1,4) = 0.0
         pm(2,4) = 0.0
         pm(3,4) = 0.0
         pm(4,4) = gami
c     -- matrix pn --
         pn(1,1) = a/(gami*rho)
         pn(2,1) = -pn(1,1)
         pn(3,1) = gamma*rho**gami/pr
         pn(4,1) = 0.0
         pn(1,2) = xy1
         pn(2,2) = xy1
         pn(3,2) = 0.0
         pn(4,2) = xy2
         pn(1,3) = xy2
         pn(2,3) = xy2
         pn(3,3) = 0.0
         pn(4,3) = -xy1
         pn(1,4) = -gamma/(gami*a*rho)
         pn(2,4) = -pn(1,4)
         pn(3,4) = -rho**gamma/pr**2
         pn(4,4) = 0.0
c     -- multiplication --
         do n2 =1,4
            do n1 =1,4
               cc(n1,n2) = 0.0
               do nn =1,4
                  cc(n1,n2) = cc(n1,n2) + pn(n1,nn)*pm(nn,n2)*xyj(j,k)
               end do
            end do
         end do

c     -- matrix for j2 ----
         j = j2
         rho = q(j,k,1)*xyj(j,k)
         u   = q(j,k,2)/q(j,k,1)
         v   = q(j,k,3)/q(j,k,1)
         pr  = gami*(q(j,k,4) - 0.5*(q(j,k,2)**2 + q(j,k,3)**2)/
     &        q(j,k,1))*xyj(j,k)
         a   = sqrt(gamma*pr/rho)                    

c     -- matrix m**-1 --
         pm(1,1) = 1.0
         pm(2,1) = -u/rho
         pm(3,1) = -v/rho
         pm(4,1) = 0.5*(u*u+v*v)*gami
         pm(1,2) = 0.0
         pm(2,2) = 1.0/rho
         pm(3,2) = 0.0
         pm(4,2) = -u*gami
         pm(1,3) = 0.0
         pm(2,3) = 0.0
         pm(3,3) = 1.0/rho
         pm(4,3) = -v*gami
         pm(1,4) = 0.0
         pm(2,4) = 0.0
         pm(3,4) = 0.0
         pm(4,4) = gami
c     -- matrix pn --
         pn(1,1) = 0.0
         pn(2,1) = -a/(gami*rho)
         pn(1,2) = 0.0
         pn(2,2) = xy1
         pn(1,3) = 0.0
         pn(2,3) = xy2
         pn(1,4) = 0.0
         pn(2,4) = gamma/(gami*a*rho)
c     -- inflow --
         if (vn1.le.0.0) then
            pn(3,1) = 0.0
            pn(4,1) = 0.0
            pn(3,2) = 0.0
            pn(4,2) = 0.0
            pn(3,3) = 0.0
            pn(4,3) = 0.0
            pn(3,4) = 0.0
            pn(4,4) = 0.0
c     -- outflow --
         else
            pn(3,1) = gamma*rho**gami/pr
            pn(4,1) = 0.0
            pn(3,2) = 0.0
            pn(4,2) = xy2
            pn(3,3) = 0.0
            pn(4,3) = -xy1
            pn(3,4) = -rho**gamma/pr**2
            pn(4,4) = 0.0
         endif
c     -- multiplication --
         j = j1
         do n2 =1,4
            do n1 =1,4
               c1(n1,n2) = 0.0
               do nn =1,4
                  c1(n1,n2) = c1(n1,n2) - pn(n1,nn)*pm(nn,n2)*xyj(j2,k)
               end do
            end do
         end do

         j = j1
c     -- reordering --
c     -- finding the biggest --      
         do n2 =1,3
            ki = n2
            b0 = dabs( cc(n2,n2) )
            do n1 =n2+1,4
               b1 = dabs( cc(n1,n2) )
               if ( b1.gt.b0 ) then
                  ki = n1
                  b0 = b1
               endif
            end do
c     -- exchanging rows --
            if (ki.ne.n2) then
               do n3 =1,4
                  b1 = cc(n2,n3)
                  cc(n2,n3) = cc(ki,n3)
                  cc(ki,n3) = b1
                  b1 = c1(n2,n3)
                  c1(n2,n3) = c1(ki,n3)
                  c1(ki,n3) = b1
               end do
               itmp = iex(k,n2,iside,iblk)
               iex(k,n2,iside,iblk) = iex(k,ki,iside,iblk)
               iex(k,ki,iside,iblk) = itmp
            endif
         end do
c     -- exchange lines 2 & 4 if cc(j,k,4,4) = 0 --
         if ( abs(cc(4,4)).lt.1.e-7 ) then
            do n3 =1,4
               b1 = cc(2,n3)
               cc(2,n3) = cc(4,n3)
               cc(4,n3) = b1
               b1 = c1(2,n3)
               c1(2,n3) = c1(4,n3)
               c1(4,n3) = b1
            end do
            itmp = iex(k,2,iside,iblk)
            iex(k,2,iside,iblk) = iex(k,4,iside,iblk)
            iex(k,4,iside,iblk) = itmp
         endif 
c     -- diagonal scaling --
         do n1 =1,4
            bcf(k,n1,iside,iblk) = 1.0/cc(n1,n1)
         end do

c     -- store in jacobian matrix --
         if (imps) then

            if (precmat) then
               j = j1
               do n1 = 1,4
                  ij = ( indx(j,k)-1)*nmax + n1
                  ip = ( ij - 1 )*icol(5)
                  do n2 = 1,4
                     pa(ip        +n2) = cc(n1,n2)
                     pa(ip+icol(1)+n2) = c1(n1,n2)
                  end do
               end do
            end if

            if (jacmat) then
               j = j1
               do n1 = 1,4
                  ij = ( indx(j,k)-1)*nmax + n1
                  ii = ( ij - 1 )*icol(9)
                  do n2 = 1,4
                     as(ii        +n2) = cc(n1,n2)
                     as(ii+icol(1)+n2) = c1(n1,n2)
                  end do
               end do
            end if

         end if
      end do

      if (nmax.eq.5 .and. imps) then
c     -- s-a model outer-boundary --
         j = j1
         do k = kbegin, kend

            par = sqrt(xy(j,k,1)**2+xy(j,k,2)**2)
            xy1 = xy(j,k,1)/par*sgn
            xy2 = xy(j,k,2)/par*sgn

            u   = q(j,k,2)/q(j,k,1)
            v   = q(j,k,3)/q(j,k,1)         
            vn  = xy1*u + xy2*v

            n1 = 5
            n2 = 5

            if (vn .le. 0.0) then
c     -- inflow --
               ij = ( indx(j,k)-1)*nmax + n1
               ii = ( ij - 1 )*icol(9)
               ip = ( ij - 1 )*icol(5)

               if (jacmat)  as(ii+n2) = xyj(j1,k)
               if (precmat) pa(ip+n2) = xyj(j1,k)
            else
c     -- outflow --          
               ij = ( indx(j,k)-1)*nmax + n1
               ii = ( ij - 1 )*icol(9)
               ip = ( ij - 1 )*icol(5)

               if (jacmat) then
                  as(ii+        n2) =  xyj(j1,k)
                  as(ii+icol(1)+n2) = -xyj(j2,k)
               end if

               if (precmat) then
                  pa(ip+        n2) =  xyj(j1,k)
                  pa(ip+icol(1)+n2) = -xyj(j2,k)
               end if
            endif
         end do
      end if

      return 
      end                       !ibcfs24
