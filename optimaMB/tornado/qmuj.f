c     ------------------------------------------------------------------
c     -- remove jacobian from q vector --
c     -- m. nemec, oct. 2001 --
c     ------------------------------------------------------------------
      subroutine qmuj( jmax, kmax, q, xyj, jbegin, jend, kbegin, kend)

      implicit none
      integer jmax, kmax, jbegin, jend, kbegin, kend, j, k, n
      double precision q(jmax,kmax,4), xyj(jmax,kmax)

      do n=1,4
         do k=kbegin,kend
            do j=jbegin,jend
               q(j,k,n) = q(j,k,n)*xyj(j,k)
            end do
         end do
      end do
         
      return
      end                       ! qmuj
