c----------------------------------------------------------------------
c     -- modify grid blocks around airfoil, including l. e. blocks --
c     -- m. nemec, may 2001 --
c----------------------------------------------------------------------
      subroutine modall (ifoil, flag, x, y, gapopt)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"      
#include "../include/common.inc"
c     -- new common block for blunt t.e. --
#include "../include/bteinfo.inc"

      integer ifoil,node,i,iseg,ib,is,iblock,iblkup,iblkdn,iblkfr
      integer iblkaft,kfinal
      double precision x(maxjk), y(maxjk)
      logical flag, gapopt

c     -- local variables --
      double precision work2(maxjk), xo(maxjkb), yo(maxjkb)

c     -- recall BTEINFO common block --
      integer bteblock

c_orig      node = 1
c     -- start with the first point on the lower surface
c     -- which is 1 for sharp te and ite(ifoil) for bte
      node = ite(ifoil)

c     -- adjust grid blocks with sides next to airfoil --
c      write (*,10) 
c 10   format (/3x,'Modifying blocks with sides in contact',
c     &     ' with surface of airfoil...',/3x,'Block',3x,'Side',3x,
c     &     'Node Index')
      do iseg=1,nsegments(ifoil)
         ib = isegblk(iseg,ifoil)
         is = isegside(iseg,ifoil)

         ! AUTOSTAN reorder the bspline segments, so sometimes
         ! the first iseg is the lower surface, then upper surface
         ! and then wrap around to the blunt trailing edge surface
         ! B.T.E. is on side 2, and start with node=1
         if (is .eq. 2) node = 1

         call modblk(node, ifoil, jmax(ib), kmax(ib), jminbnd(ib),
     &        jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib), is, x(lgptr(ib)),
     &        y(lgptr(ib)), xo, yo, work2, flag)

      end do
c 20   format (4x,i3,5x,i2,6x,i4)

c     -- modify blocks from l.e. stagnation points --
c     -- h-mesh topology --
      do i = 1,nstg

c     -- trap l.e. stagnation point --
         if (istgtyp(i).eq.1 .and. nblks.gt.4) then

c     -- find the upper and lower blocks in front of the l.e. point --
            if (istgcnr(i).eq.2) then
               iblkup = istgblk(i)
               iblkfr = lblkpt(istgblk(i),2)
               iblock = lblkpt(iblkfr,1)
               iblkdn = lblkpt(iblock,4)
c     write (*,*) 'L.E. is in LL corner of block',iblkup
c     write (*,*) 'Block ahead',iblkfr
c     write (*,*) 'Block under',iblock
c     write (*,*) 'Block down',iblkdn

c     -- check if l.e. point is on the current airfoil -- 
               flag = .false.
               do iseg=1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  if (is.eq.1 .and. ib.eq.iblkup) flag = .true.
               end do

            else
               write (*,*) 'modify: problem with stg. location!!'
               stop
            endif

            if (flag) then
c     -- modify l.e. block on upper surface --
c               write (*,30) iblkfr, iblkup
               call modle( kminbnd(iblkup), kmaxbnd(iblkup), 1,
     &              jminbnd(iblkup), jmaxbnd(iblkfr), jmaxbnd(iblkfr)-1,
     &              jminbnd(iblkfr), -1, jmaxbnd(iblkfr),
     &              jminbnd(iblkfr), jmax(iblkup), kmax(iblkup),
     &              x(lgptr(iblkup)),y(lgptr(iblkup)), jmax(iblkfr),
     &              kmax(iblkfr),x(lgptr(iblkfr)), y(lgptr(iblkfr)),
     &              work2, pi)

c     -- modify l.e. block on lower surface --
c               write (*,30) iblock, iblkdn
               
               if ( klineconst .eq. 0) then
                  kfinal=kminbnd(iblkdn)
               else
                  kfinal=klineconst
               end if

               call modle( kmaxbnd(iblkdn), kfinal, -1,
     &              jminbnd(iblkdn), jmaxbnd(iblock), jmaxbnd(iblock)-1,
     &              jminbnd(iblock), -1, jmaxbnd(iblock),
     &              jminbnd(iblock), jmax(iblkdn), kmax(iblkdn),
     &              x(lgptr(iblkdn)), y(lgptr(iblkdn)), jmax(iblock),
     &              kmax(iblock), x(lgptr(iblock)), y(lgptr(iblock)),
     &              work2, pi)
c     30            format (3x,'Modifying L. E. block',i3,
c     &              ' based on block',i3)
            end if
         end if

c     -- trap t.e. stagnation point --
         if (istgtyp(i).eq.-1 .and. nblks.gt.4 .and. gapopt) then

c     -- find the upper and lower blocks aft of the t.e. point --
            if (istgcnr(i).eq.1) then
               iblkup  = istgblk(i)
               iblkaft = lblkpt(istgblk(i),4)
c     -- find upper and lower blocks aft of the sharp/blunt t.e. point --x
               if (bte(ifoil) .eq. .false.) then
                  iblock  = lblkpt(iblkaft,1)
                  iblkdn  = lblkpt(iblock,2)
               else
                  bteblock= lblkpt(iblkaft,1)
                  iblock  = lblkpt(bteblock,1)
                  iblkdn  = lblkpt(iblock,2)
               endif

c     -- check if t.e. point is on the current airfoil -- 
               flag = .false.
               do iseg=1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  if (is.eq.1 .and. ib.eq.iblkup) flag = .true.
               end do
c     -- stg pt. is specified at lower block UR corner --
            elseif (istgcnr(i).eq.4) then

               iblkdn  = istgblk(i)
               iblock  = lblkpt(istgblk(i),4)
               if (bte(ifoil) .eq. .false.) then
                  iblkaft = lblkpt(iblock,3)
                  iblkup  = lblkpt(iblkaft,2)
               else
                  bteblock= lblkpt(iblock,3)
                  iblkaft = lblkpt(bteblock,3)
                  iblkup  = lblkpt(iblkaft,2)
               endif
               flag = .false.
               do iseg=1,nsegments(ifoil)
                  ib = isegblk(iseg,ifoil)
                  is = isegside(iseg,ifoil)
                  if (is.eq.3 .and. ib.eq.iblkdn) flag = .true.
               end do

            else
               stop 'modify: problem with t.e. stg. location!!'
            endif

            if (flag) then
c     -- modify t.e. block on upper surface --
c     write (*,40) iblkaft, iblkup

               call modle( kminbnd(iblkup), kmaxbnd(iblkup), 1,
     &              jmaxbnd(iblkup), jminbnd(iblkaft), jminbnd(iblkaft),
     &              jmaxbnd(iblkaft)-1, 1, jminbnd(iblkaft),
     &              jmaxbnd(iblkaft), jmax(iblkup), kmax(iblkup),
     &              x(lgptr(iblkup)), y(lgptr(iblkup)), jmax(iblkaft),
     &              kmax(iblkaft), x(lgptr(iblkaft)), y(lgptr(iblkaft)),
     &              work2, pi)

c     -- modify t.e. block on lower surface --

c               write (*,40) iblock, iblkdn

               if ( klineconst .eq. 0) then
                  kfinal=kminbnd(iblkdn)
               else
                  kfinal=klineconst
               end if

               call modle( kmaxbnd(iblkdn), kfinal, -1,
     &              jmaxbnd(iblkdn), jminbnd(iblock), jminbnd(iblock),
     &              jmaxbnd(iblock)-1, 1, jminbnd(iblock),
     &              jmaxbnd(iblock), jmax(iblkdn), kmax(iblkdn),
     &              x(lgptr(iblkdn)), y(lgptr(iblkdn)), jmax(iblock),
     &              kmax(iblock), x(lgptr(iblock)), y(lgptr(iblock)),
     &              work2, pi)
c     40            format (3x,'Modifying T. E. block',i3,
c     &              ' based on block',i3)
            end if
         end if

      end do

c     write (*,*) 'done modall'

      return
      end                       !modall
