c ********************************************************************
c **    averaging the dissipation of the turbulence                 **
c **    at the trailing edge point                                  **
c ********************************************************************
c
c When copying data, the sign does not change.
c
c
      subroutine fixomega(jte1,kte1,jdim1,kdim1,s1,
     &                    jte2,kte2,jdim2,kdim2,s2)

c
      dimension s1(jdim1,kdim1)
      dimension s2(jdim2,kdim2)
c ********************************************************************
c ** averaging the t.e. point between blocks 1 and 2 wich are the
c ** blocks on the right side of the trailing edge point
c ********************************************************************
c
      savg=0.5*(s1(jte1,kte1)+s2(jte2,kte2))
      s1(jte1,kte1)=savg
      s2(jte2,kte2)=savg
c
      return
      end
