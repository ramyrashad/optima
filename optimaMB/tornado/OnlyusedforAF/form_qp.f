***********************************************************************
****       subroutine to make new q (corrected q in multigrid)     ****
***********************************************************************
c---- calling subroutine: mk_qp.f
c
      subroutine form_qp (iblk,jdim,kdim,qp,q,s)
c
c#include "../include/parms.inc"
c#include "../include/index.inc"
c#include "../include/common.inc"
c#include "../include/mbcom.inc"
c
      integer iblk
      dimension qp(jdim,kdim,4),q(jdim,kdim,4),s(jdim,kdim,4)
c     
c
      do 10 n = 1,4
      do 10 k = 1,kdim
      do 10 j = 1,jdim
c      S.D.R.
c      -if I use these loop indecies and nhalo=2 then I get
c       floating point exception (warrants further investigation)
c      do 10 k = kminbnd(iblk),kmaxbnd(iblk)
c      do 10 j = jminbnd(iblk),jmaxbnd(iblk)
         qp(j,k,n) = q(j,k,n) + s(j,k,n)
c         qp(j,k,n) = q(j,k,n)
 10   continue
c
      return
      end
