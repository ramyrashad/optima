      subroutine imfarout2(jdim,kdim,q,xy,xyj,b,c,d,s,press)
c
c     implicit boundary conditions for airfoil body
      include '../include/arcom.inc'
c
      dimension q(jdim,kdim,4),xy(jdim,kdim,4),xyj(jdim,kdim)
      dimension b(jdim,kdim,4,4),c(jdim,kdim,4,4),d(jdim,kdim,4,4)
      dimension s(jdim,kdim,4),press(jdim,kdim)
c
c     ********************************************************
c     **            OUTFLOW FARFIELD  (j=1, j=jmax)         **
c     ********************************************************
c     no variation of metrics w.r.t to time taken into account
c
c
      j=jend
      do 1 m=1,4
      do 1 n=1,4
      do 1 k=klow,kup
c         b(j,k,n,m)=0.
         c(j,k,n,m)=0.
         d(j,k,n,m)=0.
c         if (m.eq.n) c(j,k,n,n)=1.
 1    continue
c
      do 9 k=klow,kup
c         j=jbegin
c         jj=j+1
c         s(j,k,1)=-(q(j,k,1)*xyj(j,k)-q(jj,k,1)*xyj(jj,k))
c         s(j,k,2)=-(q(j,k,2)*xyj(j,k)-q(jj,k,2)*xyj(jj,k))
c         s(j,k,3)=-(q(j,k,3)*xyj(j,k)-q(jj,k,3)*xyj(jj,k))
c         s(j,k,4)=-(press(j,k)*xyj(j,k)-press(jj,k)*xyj(jj,k))
c         j=jend
         jj=j-1
         s(j,k,1)=-(q(j,k,1)*xyj(j,k)-q(jj,k,1)*xyj(jj,k))
         s(j,k,2)=-(q(j,k,2)*xyj(j,k)-q(jj,k,2)*xyj(jj,k))
         s(j,k,3)=-(q(j,k,3)*xyj(j,k)-q(jj,k,3)*xyj(jj,k))
         s(j,k,4)=-(press(j,k)*xyj(j,k)-press(jj,k)*xyj(jj,k))
 9    continue
c
      do 10 k=klow,kup
c      do 10 j=jbegin,jend,jskip
         rr=1./q(j,k,1)
         u=q(j,k,2)*rr
         v=q(j,k,3)*rr
c       
         xg=xyj(j,k)*gami
         c(j,k,1,1)=xyj(j,k)
         c(j,k,2,2)=xyj(j,k)
         c(j,k,3,3)=xyj(j,k)
         c(j,k,4,1)=xg*.5*(q(j,k,2)**2+q(j,k,3)**2)*rr**2
         c(j,k,4,2)=-xg*q(j,k,2)*rr
         c(j,k,4,3)=-xg*q(j,k,3)*rr
         c(j,k,4,4)=xg
c 
c         if (j.eq.jbegin) then
c            jj=j+1
c            rr2=1./q(jj,k,1)
c            u2=q(jj,k,2)*rr2
c            v2=q(jj,k,3)*rr2
c            xg=xyj(jj,k)*gami
c            d(j,k,1,1)=-xyj(jj,k)
c            d(j,k,2,2)=-xyj(jj,k)
c            d(j,k,3,3)=-xyj(jj,k)
c            d(j,k,4,1)=-xg*.5*(q(jj,k,2)**2+q(jj,k,3)**2)*rr2**2
c            d(j,k,4,2)=xg*q(jj,k,2)*rr2
c            d(j,k,4,3)=xg*q(jj,k,3)*rr2
c            d(j,k,4,4)=-xg                                      
c         else
            jj=j-1
            rr2=1./q(jj,k,1)
            u2=q(jj,k,2)*rr2
            v2=q(jj,k,3)*rr2
            xg=xyj(jj,k)*gami
            b(j,k,1,1)=-xyj(jj,k)
            b(j,k,2,2)=-xyj(jj,k)
            b(j,k,3,3)=-xyj(jj,k)
            b(j,k,4,1)=-xg*.5*(q(jj,k,2)**2+q(jj,k,3)**2)*rr2**2
            b(j,k,4,2)=xg*q(jj,k,2)*rr2
            b(j,k,4,3)=xg*q(jj,k,3)*rr2
            b(j,k,4,4)=-xg                                      
c         endif
c
 10   continue
c
      return
      end
