      subroutine unscaleq(JDIM,KDIM,Q,XYJ)                              
c                                                                       
      COMMON/INDEX/                                                     
     1  JMAX,       KMAX,        JM,          KM,                       
     1  JBEGIN,     JEND,        KBEGIN,      KEND,                     
     1  JLOW,       JUP,         KLOW,        KUP,                      
     1  JMAXOLD,    jplus(1999), jminus(1999)                           
C                                                                       
      COMMON/GEOM/                                                      
     1  PERIODIC,   CMESH,       ORDERXY,     WAKE,                     
     1  JTAIL1,     JTAIL2,      BCAIRF,      CIRCUL,                   
     1  DSWALL,     SOBMAX,      SHARP,       CUSP,                     
     1  TINF,       WTRAT,       CIRCB,                                 
     1  CHORD                                                           
C                                                                       
      COMMON/PARM/                                                      
     1  FSMACH,     ALPHA,       GAMMA,       GAMI,                     
     1  DT,         DTMIN,       JACDT,       DTRATE,                   
     1  METH,       ISPEC,       PI,          STRTIT,                   
     1  DIS2X,      DIS2Y,       DIS4X,       DIS4Y,                    
     1  SMU,        SMUIM,       PHIDT,       THETADT                   
C                                                                       
      COMMON/SWTC/                                                      
     1  NUMITER,    ISTART,      IEND,                                  
     1  IREAD,      IPRINT,      RESTART,     STORE,                    
     1  NP,         NPCP,        SCALIT,      FINEITS,                  
     1  NPCONT,     CONMIN,      CONMAX,      CONINC,                   
     1  RESID,      RESIDMX,     MAXRES(2),   MAXDQ(2),                 
     1  TOTIME,     CRAY,        CPUTIME,     NSUPER                    
C                                                                       
      COMMON/VISC/                                                      
     1  VISCOUS,    TURBULNT,    RE,          IVIS,                     
     1  VISXI,      VISETA,      VISCROSS,                              
     1  TRANSUP,    TRANSLO,     JTRANUP,     JTRANLO                   
C                                                                       
      LOGICAL RESTART,  STORE,  TURBULNT,   VISCOUS,                    
     1        PERIODIC, CIRCUL, SHARP,      CUSP,                       
     1        BCAIRF,   CRAY,   VISXI,      VISETA,                     
     1        VISCROSS, CMESH,  ORDERXY,    WAKE                        
C                                                                       
      COMMON/FORCES/                                                    
     1   CLT,       CDT,         CMT,                                   
     1   CLI,       CDI,         CMI,                                   
     1   CLV,       CDV,         CMV                                    
C                                                                       
      COMMON/INFINTY/                                                   
     1   RHOINF,    UINF,        VINF,        EINF,      PINF           
C                                                                       
      COMMON/TVDVAR/                                                    
     1   EPSTVD,    ROEAVE,      MEANAVE,     LIMITBC                   
      LOGICAL ROEAVE,   MEANAVE                                         
C                                                                       
       COMMON/CLALP/                                                    
     1   CLALPHA,   CLINPUT,     ICLFREQ,     ICLSTRT,                  
     1   RELAXCL,   CLTOL,       ISW                                    
       LOGICAL CLALPHA                                                  
c                                                                       
      DIMENSION Q(JDIM,KDIM,4),XYJ(JDIM,KDIM)                           
c                                                                       
C  UNSCALE JACOBIAN FROM Q                                              
C                                                                       
       DO 1 N = 1,4                                                     
       DO 1 K = KBEGIN,KEND                                             
       DO 1 J = JBEGIN,JEND                                             
C                                                                       
       Q(J,K,N) = Q(J,K,N)*XYJ(J,K)                                     
1      CONTINUE                                                         
C                                                                       
       RETURN                                                           
       END                                                              
