
c #########################
c ##                     ##
c ##  subroutine asprat  ##
c ##                     ##
c #########################

      subroutine asprat(x,y,wout)

      implicit none
c
c*********************************************************************
c  Computes the average and the maximum aspect ration of the cells on 
c  a given structured grid.
c*********************************************************************
c                                                                       
c
#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/units.inc"
c
      integer nodetot,jmxar,kmxar,iblk,iblkmx,i
      double precision x(maxjk),y(maxjk),asrmax,asravg
      logical wout
c
      asrmax = 0.d0
      asravg = 0.d0
      nodetot= 0
c
      do 5 iblk=nblkstart,nblkend
        call ratio(iblk,jmax(iblk),kmax(iblk),
     &             jminbnd(iblk),jmaxbnd(iblk),
     &             kminbnd(iblk),kmaxbnd(iblk),
     &             nhalo,nodetot,jmxar,kmxar,iblkmx,
     &             asrmax,asravg,x(lgptr(iblk)),y(lgptr(iblk)))
 5    continue
c
      asravg = asravg/float(nodetot)
c
      if (wout) then
         write(n_out,14) ('-',i=1,52)
         write(n_out,11) asravg,asrmax
 11      format(11H |  asravg=,f8.1,5x,7Hasrmax=,f11.1,12x,1H|)
         write(n_out,12) iblkmx
 12      format(41H |  Maximum aspect ratio occured in block,i3,10x,1h|)
         write(n_out,13) jmxar,kmxar
 13      format(11H |       j=,I3,3x,2Hk=,I3,32x,1H|)
         write(n_out,14) ('-',i=1,52)
 14      format(2h  ,52a)
      end if
c
      return                                                            
      end                                                               
