      subroutine ga_int_conv(nchrom,ngene,pop,int_real)
      dimension pop(ngene,nchrom),int_real(ngene)
c     fixes data to integers
      do ng = 1,ngene
         if(int_real(ng).eq.1)then
            do nc = 1,nchrom
               c = abs(pop(ng,nc))+0.5
               d = int(c)
               pop(ng,nc) = sign(1.0,pop(ng,nc))*d
            enddo
         endif
      enddo
      return
      end
      
