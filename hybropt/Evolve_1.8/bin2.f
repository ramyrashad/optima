C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C                        BBBB   III  N   N   222                       C
C                        B   B   I   NN  N  2   2                      C
C                        BBBB    I   N N N     2                       C
C                        B   B   I   N  NN   2                         C
C                        BBBB   III  N   N  22222                      C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C
      SUBROUTINE BIN2(ndim,NDUM,NACC,NOBFUN,FITACC,NGENE,GENES,
     1               NCHROM,ZZZ,ZZZFIT,zzzrank,I_MINMAX,debug)
      SAVE
      real*8 rand
      logical debug
C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C                                                                      C
C          *** THIS SUBROUTINE PERFORMS THE SELECTION OPERATION        C
C              USING A BIN SELECTION ALGROITHM. CHROMOSOMES ARE        C
C              SELECTED FROM THE ACCUMULATION FILE (IACCUM=1).         C
C              THIS ALGORITHM COMPUTES BIN LOCATIONS USING             C
C              INTERGER ARTHMETIC AND IS VALID FOR N OBJECTIVES        C
C              (NOBFUN>1). IABS(NDUM) IS THE NUMBER OF INTERVALS       C
C              USED IN EACH OBJECTIVE DIRECTION. WHEN NDUM IS          C
C              POSITIVE, THE ACCUMULATION FILE ENDPOINTS ARE           C
C              SELECTED FIRST (ENDPOINT RETENTION OPTION). WHEN        C
C              NDUM IS NEGATIVE THE ENDPOINT RETENTION OPTION IS       C
C              NOT USED. THIS SELECTION SUBROUTINE IS IMPLEMENTED      C
C              WHEN ISELECT=8 ***                                      C
C                                                                      C
C----------------------------------------------------------------------C
C----------------------------------------------------------------------C
C          *** FORMAL PARAMETER DEFINITIONS ***
C
C       NDUM...IABS(NDUM) IS THE NUMBER OF INTERVALS THAT EACH 
C              OBJECTIVE DIRECTION IS DIVIDED INTO. WHEN NDUM IS 
C              POSITIVE, THE ACCUMULATION FILE ENDPOINTS ARE 
C              SELECTED FIRST (ENDPOINT RETENTION OPTION). WHEN
C              NDUM IS NEGATIVE THE ENDPOINT RETENTION OPTION IS 
C              NOT USED.
C       NACC...NUMBER OF ELMENTS IN THE CURRENT ACCUMULATION FILE 
C              (PARETO FRONT)
C     NOBFUN...NUMBER OF OBJECTIVE FUNCTIONS
C  FITACC(,)...DOUBLY-SUBSCRIPTED ARRAY CONTAINING NOBFUN FITNESS
C              VALUES FOR EACH ELEMENT IN THE ACCUMULATION FILE
C      NGENE...NUMBER OF GENES ASSOCIATED WITH EACH CHROMOSOME
C   GENES(,)...DOUBLY-SBSCRIPTED ARRAY CONTAINING NGENE GENES
C              FOR EACH ELEMENT IN THE ACCUMULATION FILE
C     NCHROM...NUMBER OF CHROMOSOMES USED IN THE ACTIVE FILE
C     ZZZ(,)...DOUBLY-SUBSCRIPTED WORKING ARRAY IN WHICH NEWLY
C              SELECTED GENES ARE RETURNED TO THE CALLING ROUTINE
C  ZZZFIT(,)...DOUBLY-SUBSCRIPTED WORKING ARRAY IN WHICH NEWLY
C              SELECTED FITNESS VALUES ARE RETURNED TO THE CALLING
C              ROUTINE
C   I_MINMAX...INTEGER PARAMETER THAT CONTROLS WHETHER THE 
C              OPTIMIZATION IS MAXIMIZATION OR MINIMIZATION
C              I_MINMAX=1...MAXIMIZATION
C              I_MINMAX=0...MINIMIZATION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C
      DIMENSION         FITACC(nobfun,ndim)
      DIMENSION         GENES(ngene,ndim)
      DIMENSION         ZZZ(ngene,nchrom)
      DIMENSION         ZZZFIT(nobfun,nchrom),zzzrank(nchrom)
      DIMENSION         FITMAX(nobfun),           FITMIN(nobfun)
      DIMENSION         IBIN(ndim)
      DIMENSION         IB(nobfun)
      DIMENSION         NBIN_LIST(1000),      NFIL(1000)
      DIMENSION         NAMAX(nobfun),            NAMIN(nobfun)
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** SET INITIAL PARAMETERS ***
C
      NINT = ABS(NDUM)
      IDUM = 1
C
C-----------------------------------------------------------------------
C          *** COMPUTE MAX AND MIN VALUES IN THE ACCUMULATION FILE
C              FOR EACH OBJECTIVE-SPACE DIMENSION ***
C
      DO NO=1,NOBFUN
         FITMAX(NO) = -1.0E30
         FITMIN(NO) =  1.0E30
      ENDDO
      DO NA=1,NACC
         DO NO=1,NOBFUN
            IF (FITACC(NO,NA).GT.FITMAX(NO)) THEN
               FITMAX(NO) = FITACC(NO,NA)
               NAMAX(NO)  = NA
            ENDIF
            IF (FITACC(NO,NA).LT.FITMIN(NO)) THEN
               FITMIN(NO) = FITACC(NO,NA)
               NAMIN(NO)  = NA
            ENDIF
         ENDDO
      ENDDO
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
c$$$      if(debug)then
c$$$         DO NO=1,NOBFUN
c$$$            WRITE (49,531) NO,FITMAX(NO),FITMIN(NO),NAMAX(NO),NAMIN(NO)
c$$$ 531        FORMAT('BIN2: NO,FITMAX,FITMIN,NAMAX,NAMIN =',
c$$$     $           I6,2F22.14,2I5)
c$$$         ENDDO
c$$$      endif
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** PERFORM SELECTION ***
C              IBIN()...ARRAY CONTAINING A BIN NUMBER FOR EACH ENTRY
C                       IN THE ACCUMULATION FILE. THERE ARE NACC
C                       ELEMENTS IN IBIN.
C              NBIN.....NUMBER OF ACTIVE BINS, I.E., NUMBER OF BINS
C                       THAT CONTAIN AT LEAST ONE ELEMENT
C              NBIN_LIST()..ARRAY CONTAINING THE LIST OF ALL UNIQUE 
C                       BIN NUMBERS. THERE ARE NBIN ELEMENTS IN 
C                       NBIN_LIST.
C              NFIL()...COUNTER CONTAINING CURRENT NUMBER OF ELEMENTS
C                       THAT HAVE BEEN SELECTED FROM EACH ACTIVE
C                       BIN. THERE ARE NBIN ELEMENTS IN NFIL.
C              NINT.....NUMBER OF INTERVALS THAT EACH OBJECTIVE
C                       DIMENSION IS DIVIDED INTO
C              NMAX.....MAXIMUM NUMBER OF ELEMENTS TO BE TAKEN FROM 
C                       EACH BIN (CAN BE EXCEEDED BY ONE)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C          *** COMPUTE BIN LOCATION FOR EACH ENTRY IN THE
C              ACCUMULATION FILE (PARETO FRONT) ***
C
      NBIN = 0
      DO NC=1,NACC
         DO NO=1,NOBFUN
cthp_2005            IB(NO) = IFIX((FITACC(NO,NC)-FITMIN(NO))/
cthp_2005     1                    (FITMAX(NO)-FITMIN(NO))*FLOAT(NINT))+1
            IB(NO) = INT((FITACC(NO,NC)-FITMIN(NO))/
     1                    (FITMAX(NO)-FITMIN(NO))*FLOAT(NINT))+1
            IF (IB(NO).GT.NINT) IB(NO) = NINT
         ENDDO
         IBIN(NC) = IB(1)
         DO NO=2,NOBFUN
            IBIN(NC) = IBIN(NC)+(IB(NO)-1)*NINT**(NO-1)
         ENDDO
         IF (NBIN.EQ.0) THEN
            NBIN = NBIN+1
            NBIN_LIST(NBIN) = IBIN(NC)
            GO TO 1000
         ENDIF
         DO NBI=1,NBIN
            IF (IBIN(NC).EQ.NBIN_LIST(NBI)) GO TO 1000
         ENDDO
         NBIN = NBIN+1
         NBIN_LIST(NBIN) = IBIN(NC)
 1000    CONTINUE
      ENDDO
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
c$$$      if(debug)then
c$$$         DO NC=1,NACC
c$$$            WRITE (49,330) NC,FITACC(1,NC),FITACC(2,NC),IBIN(NC)
c$$$ 330        FORMAT ('BIN2:NC,FITACC1,FITACC2,IBIN=',I5,2F12.6,I5)
c$$$         ENDDO
c$$$         DO NBI=1,NBIN
c$$$            WRITE (49,332) NBI,NBIN_LIST(NBI)
c$$$ 332        FORMAT ('BIN2: NBI,NBIN_LIST =',2I6)
c$$$         ENDDO
c$$$      endif
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C-----------------------------------------------------------------------
C          *** SELECT THE NEW CHROMOSOMES SO THAT EACH BIN
C              HAS EQUAL REPRESENTATION. IF NDUM IS POSITIVE
C              USE ACCUMULCATION FILE ENDPOINT RETENTION. THIS
C              PART OF LOGIC SELECTS NMAX ELEMENTS FROM EACH
C              ACTIVE BIN ***
C
      NMAX = NCHROM/NBIN
      N1   = NMAX*NBIN
      N2   = N1+1
      DO NBI=1,NBIN
         NFIL(NBI) = 0
      ENDDO
      DO NC=1,N1
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (34,431) NC,NMAX
C  431 FORMAT ('BIN2: NC,NMAX =',2I6)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 3000    CONTINUE
         IF (NDUM.GT.0.AND.NC.LE.NOBFUN) THEN
            IF (I_MINMAX.EQ.1)  THEN 
               IR = NAMAX(NC)
            ELSE
               IR = NAMIN(NC)
            ENDIF
         ELSE
            CALL RAN3(IDUM,RAND)
cthp_2005            IR = IFIX(RAND*FLOAT(NACC))+1
            IR = INT(RAND*FLOAT(NACC))+1
         ENDIF
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (34,432) NC,IR,(NFIL(NBI),NBI=1,NBIN)
C  432 FORMAT ('BIN2: NC,IR,NFIL =',2I6,20I4)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
         DO NBI=1,NBIN
            IF (IBIN(IR).EQ.NBIN_LIST(NBI)) THEN
               IF (NFIL(NBI).LT.NMAX) THEN
                  NFIL(NBI) = NFIL(NBI)+1
                  DO NG=1,NGENE
                     ZZZ(NG,NC) = GENES(NG,IR)
                  ENDDO
                  DO NO=1,NOBFUN
                     ZZZFIT(NO,NC) = FITACC(NO,IR)
                  ENDDO
                  zzzrank(nc) = 1
                  GO TO 2000
               ELSE
                  GO TO 3000
               ENDIF
            ENDIF
         ENDDO
 2000    CONTINUE
      ENDDO
C
C-----------------------------------------------------------------------
C          *** COMPLETE SELECTION PROCESS BY SELECTING AT
C              RANDOM AS NEEDED, MAKING SURE THAT ALL ADDITIONAL
C              CHROMOSOMES ARE SELECTED FROM UNIQUELY DIFFERENT
C              BINS ***
C
      IF (N2.LE.NCHROM) THEN
         DO NC=N2,NCHROM
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (34,433) NC,NMAX
C  433 FORMAT ('BIN2-2: NC,NMAX =',2I6)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 5000       CONTINUE
            CALL RAN3(IDUM,RAND)
cthp_2005            IR = IFIX(RAND*FLOAT(NACC))+1
            IR = INT(RAND*FLOAT(NACC))+1
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
C      WRITE (34,434) NC,IR,(NFIL(NBI),NBI=1,NBIN)
C  434 FORMAT ('BIN2-2: NC,IR,NFIL =',2I6,20I4)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
            DO NBI=1,NBIN
               IF (IBIN(IR).EQ.NBIN_LIST(NBI)) THEN
                  IF (NFIL(NBI).LE.NMAX) THEN
                     NFIL(NBI) = NFIL(NBI)+1
                     DO NG=1,NGENE
                        ZZZ(NG,NC) = GENES(NG,IR)
                     ENDDO
                     DO NO=1,NOBFUN
                        ZZZFIT(NO,NC) = FITACC(NO,IR)
                     ENDDO
                     zzzrank(nc) = 1
                     GO TO 4000
                  ELSE
                     GO TO 5000
                  ENDIF
               ENDIF
            ENDDO
 4000       CONTINUE
         ENDDO
      ENDIF
C
CCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
c$$$      if(debug)then
c$$$         DO NBI=1,NBIN
c$$$            WRITE (49,333) NBI,NBIN_LIST(NBI)
c$$$ 333        FORMAT ('BIN2: NBI,NBIN_LIST =',2I6)
c$$$         ENDDO
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCC DEBUG OUTPUT CCCCC
c$$$         DO NC=1,NCHROM
c$$$            WRITE (49,721) NC,ZZZ(1,NC),ZZZ(2,NC)
c$$$ 721        FORMAT ('BIN1:NC,ZZZ1/2=',I6,2F24.17)
c$$$         ENDDO
c$$$      endif
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C           *** SELECTION COMPLETE ***
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
      RETURN
      END


