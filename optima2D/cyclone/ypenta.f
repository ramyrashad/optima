       subroutine ypenta(JDIM,KDIM,a,b,c,d,e,x,y,f,kl,ku,jl,ju)         
       double precision ld,ld1,ld2,ldi                                              
       dimension a(JDIM,KDIM),b(JDIM,KDIM),c(JDIM,KDIM),d(JDIM,KDIM)
       dimension e(JDIM,KDIM),x(JDIM,KDIM),y(JDIM,KDIM),f(JDIM,KDIM)    
C                                                                       
c	! start forward generation process and sweep                          
C                                                                       
        k = kl                                                          
        do 10 j = jl,ju                                                 
        ld = c(j,k)                                                     
        ldi = 1./ld                                                     
        f(j,k) = f(j,k)*ldi                                             
        x(j,k) = d(j,k)*ldi                                             
        y(j,k) = e(j,k)*ldi                                             
10      continue                                                        
C                                                                       
        k = kl+1                                                        
        do 20 j = jl,ju                                                 
        ld1 = b(j,k)                                                    
        ld = c(j,k) - ld1*x(j,k-1)                                      
        ldi = 1./ld                                                     
        f(j,k) = (f(j,k) - ld1*f(j,k-1))*ldi                            
        x(j,k) = (d(j,k) - ld1*y(j,k-1))*ldi                            
        y(j,k) = e(j,k)*ldi                                             
20      continue                                                        
C                                                                       
            do 1 k = kl+2,ku-2                                          
                do 11 j = jl,ju                                         
                ld2 = a(j,k)                                            
                ld1 = b(j,k) - ld2*x(j,k-2)                             
                ld = c(j,k) - (ld2*y(j,k-2) + ld1*x(j,k-1))             
                ldi = 1./ld                                             
                f(j,k) = (f(j,k) - ld2*f(j,k-2) - ld1*f(j,k-1))*ldi     
                x(j,k) = (d(j,k) - ld1*y(j,k-1))*ldi                    
                y(j,k) = e(j,k)*ldi                                     
11              continue                                                
1            continue                                                   
c                                                                       
        k = ku-1                                                        
        do 12 j = jl,ju                                                 
        ld2 = a(j,k)                                                    
        ld1 = b(j,k) - ld2*x(j,k-2)                                     
        ld = c(j,k) - (ld2*y(j,k-2) + ld1*x(j,k-1))                     
        ldi = 1./ld                                                     
        f(j,k) = (f(j,k) - ld2*f(j,k-2) - ld1*f(j,k-1))*ldi             
        x(j,k) = (d(j,k) - ld1*y(j,k-1))*ldi                            
12      continue                                                        
c                                                                       
                k = ku                                                  
                do 22 j = jl,ju                                         
                ld2 = a(j,k)                                            
                ld1 = b(j,k) - ld2*x(j,k-2)                             
                ld = c(j,k) - (ld2*y(j,k-2) + ld1*x(j,k-1))             
                ldi = 1./ld                                             
                f(j,k) = (f(j,k) - ld2*f(j,k-2) - ld1*f(j,k-1))*ldi     
22              continue                                                
c                                                                       
c        !  back sweep solution                                         
c                                                                       
        do 32 j = jl,ju                                                 
        f(j,ku) = f(j,ku)                                               
        f(j,ku-1) = f(j,ku-1) - x(j,ku-1)*f(j,ku)                       
32      continue                                                        
c                                                                       
        do 2 k = 2,ku-KL                                                
           kx = ku-k                                                    
           do 13 j = jl,ju                                              
           f(j,kx) = f(j,kx) - x(j,kx)*f(j,kx+1) - y(j,kx)*f(j,kx+2)    
13         continue                                                     
2       continue                                                        
c                                                                       
        return                                                          
        end                                                             
