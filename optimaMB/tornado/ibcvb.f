c----------------------------------------------------------------------
c     -- body boundary condition for viscous flow --
c     -- based on a. pueyo's routine impbodyv --
c     -- m. nemec, sept. 2001 --
c----------------------------------------------------------------------
      subroutine ibcvb(ib, k1, k2, js, je, jmaxt, kmaxt, nmax, q, xyj,
     &     press, as, pa, icol, indx, jacmat, precmat)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"

      integer k1, k2, js,je,jmaxt,kmaxt,nmax,indx(jmaxt,kmaxt),icol(*)
      integer ib,j,n1,n2,k,ij,ip,ii
      double precision q(jmaxt,kmaxt,4),xyj(jmaxt,kmaxt),as(*),pa(*)
      double precision press(jmaxt,kmaxt),u,v,pr1,pr2,timetmp
      logical  jacmat,precmat,sbtrue

c     -- local arrays --
      double precision cc(4,4), c1(4,4)


      sbtrue = sbbc .and. ib.eq.8 .and. k1.eq.kminbnd(ib)

c$$$      if ( sbtrue )  then
c$$$c     --Calculate Omega=omegaa*sin(2*pi*omegaf*t) or Omega=omegaa for suction/blowing BC
c$$$
c$$$         if (omegaf .eq. 0.d0) then
c$$$            omega=omegaa
c$$$         else
c$$$            if (numiter-istart .le. nkskip) then
c$$$               timetmp=(numiter-istart)*dt2
c$$$            else
c$$$               timetmp=(numiter-istart-nkskip)*dt2+nkskip*dtbig
c$$$            end if
c$$$            omega=omegaa*dsin(2.d0*pi*omegaf*timetmp)
c$$$         endif
c$$$
c$$$      end if  !Calculate Omega


      do j = js,je

         do n1 = 1,4
            do n2 = 1,4
               cc(n1,n2) = 0.0
               c1(n1,n2) = 0.0
            end do
         end do

c     -- matrix for k=k1 --
         k = k1                
         u = q(j,k,2)/q(j,k,1)
         v = q(j,k,3)/q(j,k,1)
         pr1 = press(j,k)*xyj(j,k)
c     -- ro1 = ro2  or Tw = const. --
         cc(1,1) = 1.0*xyj(j,k)
c     -- ro*u = 0 --
         cc(2,2) = 1.0*xyj(j,k)
c     -- ro*v = 0 --
         cc(3,3) = 1.0*xyj(j,k)
c     -- interp. press. --
         cc(4,1) = 0.5*(u*u+v*v)*gami*xyj(j,k)
         cc(4,2) = -u*gami*xyj(j,k)
         cc(4,3) = -v*gami*xyj(j,k)
         cc(4,4) = gami*xyj(j,k)

c     -- matrix for k=k2 --
         k = k2
         u = q(j,k,2)/q(j,k,1)
         v = q(j,k,3)/q(j,k,1)
         pr2 = press(j,k)*xyj(j,k)

c         if (.not.sbtrue .or. (sbtrue.and. j.ne.15 .and. j.ne.16)) then  !suction/blowing BC
c        -- ro1 = ro2  or Tw = const. --
            c1(1,1) = -1.0*xyj(j,k)
c         end if

c     -- interp. press. --
         c1(4,1) = -0.5*(u*u+v*v)*gami*xyj(j,k)
         c1(4,2) =  u*gami*xyj(j,k)
         c1(4,3) =  v*gami*xyj(j,k)
         c1(4,4) = -gami*xyj(j,k)

c     -- diagonal scaling --
c     -- for adjoint problem - CANNOT scale here --
c     do n1 =1,4
c     bcf(j,n1,iside,iblk) = 1.0/cc(n1,n1)
c     bcf(j,n1,iside,iblk) = 1.0
c     end do

c     -- store --
         k = k1

         if (precmat) then
            do n1 = 1,4
               ij = ( indx(j,k)-1)*nmax + n1
               ip = ( ij - 1 )*icol(5)
               
               do n2 = 1,4
                  pa(ip+        n2) = cc(n1,n2) 
                  pa(ip+icol(1)+n2) = c1(n1,n2)
               end do
            end do
         end if

         if (jacmat) then
            do n1 = 1,4
               ij = ( indx(j,k)-1)*nmax + n1
               ii = ( ij - 1 )*icol(9)

               do n2 = 1,4
                  as(ii+        n2) = cc(n1,n2) 
                  as(ii+icol(1)+n2) = c1(n1,n2)
               end do
            end do
         end if
      end do

c     -- s-a model => nu_tilde = 0 --
      if (nmax.eq.5) then
         n1 = 5
         n2 = 5
         k  = k1
         if (precmat) then
            do j = js, je
               ij = ( indx(j,k)-1)*nmax + n1
               ip = ( ij - 1 )*icol(5)
               pa(ip+n2) = xyj(j,k)
            end do
         end if

         if (jacmat) then
            do j = js, je
               ij = ( indx(j,k)-1)*nmax + n1
               ii = ( ij - 1 )*icol(9)
               as(ii+n2) = xyj(j,k)
            end do
         end if
      end if

      return
      end                       !ibcvb


c----------------------------------------------------------------------
      subroutine ibcvb24(ib, j1, j2, ks, ke, jmaxt, kmaxt, nmax, q, xyj,
     &     press, as, pa, icol, indx, jacmat, precmat)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"

      integer ib, j1, j2, ks, ke, jmaxt, kmaxt, nmax, indx(jmaxt,kmaxt)
      integer j,n1,n2,k,ij,ip,ii,icol(*)
      double precision q(jmaxt,kmaxt,4),xyj(jmaxt,kmaxt),as(*),pa(*)
      double precision press(jmaxt,kmaxt),u,v,pr1,pr2,timetmp
      logical   jacmat,precmat,sbtrue

c     -- local arrays --
      double precision cc(4,4), c1(4,4)


      sbtrue = sbbc .and. ib.eq.4 .and. j1.eq.jminbnd(ib)

c$$$      if ( sbtrue )  then
c$$$c     --Calculate Omega=omegaa*sin(2*pi*omegaf*t) or Omega=omegaa for suction/blowing BC
c$$$
c$$$         if (omegaf .eq. 0.d0) then
c$$$            omega=omegaa
c$$$         else
c$$$            if (numiter-istart .le. nkskip) then
c$$$               timetmp=(numiter-istart)*dt2
c$$$            else
c$$$               timetmp=(numiter-istart-nkskip)*dt2+nkskip*dtbig
c$$$            end if
c$$$            omega=omegaa*dsin(2.d0*pi*omegaf*timetmp)
c$$$         endif
c$$$
c$$$      end if  !Calculate Omega



      do k = ks,ke

         do n1 = 1,4
            do n2 = 1,4
               cc(n1,n2) = 0.d0
               c1(n1,n2) = 0.d0
            end do
         end do

c     -- matrix for j=j1 --
         j = j1                
         u = q(j,k,2)/q(j,k,1)
         v = q(j,k,3)/q(j,k,1)
         pr1 = press(j,k)*xyj(j,k)
c     -- ro1 = ro2  or Tw = const. --
         cc(1,1) = 1.0*xyj(j,k)
c     -- ro*u = 0 --
         cc(2,2) = 1.0*xyj(j,k)
c     -- ro*v = 0 --
         cc(3,3) = 1.0*xyj(j,k)
c     -- interp. press. --
         cc(4,1) = 0.5*(u*u+v*v)*gami*xyj(j,k)
         cc(4,2) = -u*gami*xyj(j,k)
         cc(4,3) = -v*gami*xyj(j,k)
         cc(4,4) = gami*xyj(j,k)

c     -- matrix for j=j2 --
         j = j2
         u = q(j,k,2)/q(j,k,1)
         v = q(j,k,3)/q(j,k,1)
         pr2 = press(j,k)*xyj(j,k)

c         if (.not.sbtrue .or. 
c     &        (sbtrue.and. k.ne.36 .and. k.ne.37 .and. k.ne.38)) then !suction/blowing BC
c        -- ro1 = ro2  or Tw = const. --
            c1(1,1) = -1.0*xyj(j,k)
c         end if

c     -- interp. press. --
         c1(4,1) = -0.5*(u*u+v*v)*gami*xyj(j,k)
         c1(4,2) =  u*gami*xyj(j,k)
         c1(4,3) =  v*gami*xyj(j,k)
         c1(4,4) = -gami*xyj(j,k)

c     -- diagonal scaling --
c     -- for adjoint problem - CANNOT scale here --
c     do n1 =1,4
c     bcf(j,n1,iside,iblk) = 1.0/cc(n1,n1)
c     bcf(j,n1,iside,iblk) = 1.0
c     end do

c     -- store --
         j = j1

         if (precmat) then
            do n1 = 1,4
               ij = ( indx(j,k)-1)*nmax + n1
               ip = ( ij - 1 )*icol(5)
               
               do n2 = 1,4
                  pa(ip+        n2) = cc(n1,n2) 
                  pa(ip+icol(1)+n2) = c1(n1,n2)
               end do
            end do
         end if

         if (jacmat) then
            do n1 = 1,4
               ij = ( indx(j,k)-1)*nmax + n1
               ii = ( ij - 1 )*icol(9)

               do n2 = 1,4
                  as(ii+        n2) = cc(n1,n2) 
                  as(ii+icol(1)+n2) = c1(n1,n2)
               end do
            end do
         end if
      end do

c     -- s-a model => nu_tilde = 0 --
      if (nmax.eq.5) then
         n1 = 5
         n2 = 5
         j  = j1
         if (precmat) then
            do k = ks, ke
               ij = ( indx(j,k)-1)*nmax + n1
               ip = ( ij - 1 )*icol(5)
               pa(ip+n2) = xyj(j,k)
            end do
         end if

         if (jacmat) then
            do k = ks, ke
               ij = ( indx(j,k)-1)*nmax + n1
               ii = ( ij - 1 )*icol(9)
               as(ii+n2) = xyj(j,k)
            end do
         end if
      end if

      return
      end                       !ibcvb
