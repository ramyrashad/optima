% function snsetr( option, rvalue )
%     Sets a optional REAL VALUED parameter of snopt.
%     The string "option" will be read
%     by snopt and assigned value "rvalue". If the string contains a
%     setting that snopt understands, snopt will set internal parameters
%     accordingly. For a description of available parameters, see
%     the snopt documentation.
%
%     Do not try to set the unit number of the summary or print file.
%     Use the MATLAB functions snsummary and snprintfile instead.
function snsetr( option, rvalue )

%setoption = snoptcmex( 0, 'SetOptionR' );

setoption = 4;
snoptcmex( setoption, option, rvalue );