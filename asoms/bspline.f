c----------------------------------------------------------------------
c     airfoil geometry definition
c     fit a b-spline curve through given data points
c
c     references:
c     'curves and surfaces for computer-aided geometric design', 
c     4th ed., g. farin, 1997.
c     
c     `mathematical elements for computer graphics', 2nd ed., rogers 
c     and adams, 1990.
c
c     'intrinsic parametrization for approximation', j. hoschek, 
c     computer aided geometric design, 1988 (5), pg 27.
c
c     program uses lapack subroutines for least-squares problem
c     use flag -lcomplib.sgimath to compile on sgi machines
c
c     written by: marian nemec 
c     date: feb. 2000
c     mods: dec. 2000
c----------------------------------------------------------------------

      subroutine bspline ( xdata, ydata, bcp, t, knot, x, y, xp, yp,
     &      errorx, errory, corr, dist, rhs)

c     xdata, ydata => input: arrays of original data to be splined
c     x, y => output: arrays of approximated data points
c     jbody => input: number of data points (max. 400)
c     nc => input: number of control points
c     order => input: order of B-Spline basis 
c     MaxIter => input: maximum number of iterations ( < 50 ) used to
c                improve the approximation.
c     tol => convergence tolerance ( < 8.d-5 ) for the approximation.

c     the order of the B-Spline can range from 2 (linear) to jbody.
c     when the number of control points is equal to jbody, the 
c     resulting representation is a Bezier curve of degree jbody-1.

      implicit none

#include "bsord.inc"     
#include "bsp.inc"
#include "units.inc"

      integer i, ii, j, left, iter

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot 

      double precision xdata(jbody), ydata(jbody), t(jbody), totalarc
      double precision x(jbody), y(jbody), knot(nc+iorder), bcp(nc,2) 
      double precision xp(jbody), yp(jbody), corr(jbody), dist(jbody)
      double precision errorx(jbody), errory(jbody)
            
      double precision BIatT(ibsord), Ncoef(ibody,ibsnc)

      double precision le_of_int, le_of_poly, NewDist, tn, resid, tmp
      double precision fac, nc_half, segment

      logical cont, werr

c     -- variables for QR decomposition using LAPACK --
      integer info,lwork
      double precision work(ibody*ibsnc), rhs(jbody,2)
      character trans

      double precision dBsp
      
      double precision tnot

      integer iknt
      double precision rknt
      logical rep, cont2

c     -- set variables for LAPACK routine dgels --
      lwork = ibody*ibsnc
      trans = 'N'

      nc = nc - 1

c      write (*,6) nc+1
c      write (*,7) iorder
c 6    format (/3x,'Number of control points:',i4)
c 7    format (3x,'Order of B-spline basis:',i3)

      if (jbody.gt.ibody) then
        write (*,*) 'JBODY is too large - check ibody: bspline.f!!'
        stop
      end if
      if (iorder.gt.ibsord) then
        write (*,*) 'ORDER is too large - check ibsord and ibskt!!'
        stop
      end if
      if ( (nc+1).gt.ibsnc) then
        write (*,*) 'NC is too large - check ibsnc and ibskt!!'
        stop
      end if

c     -- centripetal chord length parametrization --
c     -- to obtain chord length parametrization remove dsqrt --
c        from dsqrt(segment) --
      t(1) = 0.d0
      totalarc = 0.d0
      do i = 2, jbody 
        segment = dsqrt( (xdata(i)-xdata(i-1))**2 +
     &        (ydata(i)-ydata(i-1))**2 )
        totalarc = totalarc + dsqrt(segment) 
        t(i) = t(i-1) + dsqrt(segment)
      end do
      do i = 2, jbody-1
        t(i) = t(i)/totalarc*dble(nc-iorder+2)
      end do
      t(jbody) = dble(nc-iorder+2)

c     -- set knot vector --
      do i=1,iorder
        knot(i) = 0.d0
      end do
      
      nc_half = dble(nc-iorder+2)
      fac = 1.d0/dble(nc-iorder+2)
      tmp = 0.d0

      do i=1,nc-iorder+2
        tmp = tmp + fac
        ii = i+iorder
c     knot(ii) = nc_half*0.5d0*(1.d0 -dcos(3.1415926535d0*tmp))
        knot(ii) = nc_half*0.5d0*(1.d0 - ( -2.0/3.1415926535d0*( 
     &       3.1415926535d0*tmp) + 1.0 ))   
      end do

      do i=nc+2,nc+iorder+1
        knot(i) = dble(nc-iorder+2)
      end do

c     -- display knot vector --
      write (*,*) 'Knot vector:'
      do i=1,nc+iorder+1
c     write (*,15) ( knot(i), i=1,nc+iorder+1 )
         write (*,15) i, knot(i)
      end do
c 15   format (5f18.8)
 15   format (i4, f18.8)
      write (*,*)

      rep = .false.
      call askl('Modify knot vector?^',rep)
      if (rep) then
         cont2 = .true.
         do while (cont2)
            call aski('Enter knot index (999 to exit):^',iknt)
            if (iknt.eq.999) then
               cont2 = .false.
            else
               call askr('Enter knot value: ^',rknt)
               knot(iknt) = rknt
            end if
         end do
      end if

c      write (*,*) 'Knot vector:'
c      write (*,15) ( knot(i), i=1,nc+iorder+1 )
c      write (*,*)

c     -- display knot vector --
      if (rep) then
         write (*,*) 'Knot vector:'
         do i=1,nc+iorder+1
            write (*,15) i, knot(i)
         end do
         write (*,*)
      end if

cc     -- display parameter vector --
c      write (*,*) 'Parameter vector:'
c      write (*,17) ( t(i), i=1,jbody )
c      write (*,*)
c 17   format (5f24.16)
      
c     -- start least squares approximation with parameter correction --
      resid = 1.d0
      iter = 0
c      write (*,18)
c      write (*,19)
c 18   format (3x,'** Using QR decomposition to solve ')
c 19   format (6x,'the Least-Squares fit. **',/)

 20   continue                  ! main least squares loop
      iter = iter + 1

c      write (*,21) iter
c 21   format (3x,'Iteration:',i4)

c     -- form matrix of B-spline basis functions [ N ] --
c     -- work only on increasing sequence of the knot vector --
      do j=1,jbody
        do i=1,nc+1
          Ncoef(j,i) = 0.d0
        end do
      end do

      tmp = -1.d0
      do j=1,jbody
        do left=iorder,nc+1

          if ( knot(left).le.t(j) .and. t(j).le.knot(left+1)
     &          .and. t(j).ne.tmp ) then 
            tmp = t(j)
            call Bval( knot, t(j), left, BIatT)
            do i=1,iorder
              Ncoef(j,i+left-iorder) = BIatT(i)
            end do
          end if

        end do
        rhs(j,1) = xdata(j)
        rhs(j,2) = ydata(j)
      end do

c     -- display b-spline basis function matrix [ N ] --
c     write (*,*) 'N-Matrix:'
c     do j=1,jbody
c     write (*,25) ( Ncoef(j,i), i=1,nc+1 ) 
c     end do
c     25   format (6f8.4)

c     -- solve for control points using QR decomposition (LAPACK) --
      call dgels(trans,jbody,nc+1,2,Ncoef,ibody,rhs,jbody,work, 
     &      lwork,info)
      if (info.eq.0) then
        if (dble(lwork).lt.work(1)) write (*,*) 'Increase lwork to:',
     &        work(1)
      else
        write (*,*)
        write (*,*) '*** Problems with QR decomposition. ***'
      end if

c     -- force T.E. of airfoil to avoid roundoff --
      rhs(1,1) = xdata(1)
      rhs(1,2) = ydata(1)
      rhs(nc+1,1) = xdata(jbody)
      rhs(nc+1,2) = ydata(jbody)

c     -- evaluate b-spline given control points --
      call curve(knot, t, x, y, rhs)

c     -- compute error for the approximation --
      werr = .false.
      call error(x, y, xdata, ydata, resid, errorx, errory, dist, werr)

c     -- check exit condition --
      if ( (resid.lt.tol).or.(iter.ge.MaxIter) ) goto 40

c     -- proceed with parameter correction --
c     -- find tangents using analytical derivative --
c     call tangents( knot, t, x, y, xp, yp, rhs)

      do j = 1, nc+1
        bcp(j,1) = rhs(j,1)
        bcp(j,2) = rhs(j,2)
      end do

      tmp = -1.d0
      do j=1,jbody
        do left=iorder,nc+1

          if ( knot(left).le.t(j) .and. t(j).le.knot(left+1)
     &          .and. t(j).ne.tmp ) then 
            tmp = t(j)
            xp(j) = dBsp (knot, bcp(1,1), t(j), left, 1)
            yp(j) = dBsp (knot, bcp(1,2), t(j), left, 1)
          end if

        end do
      end do    

c     -- length of interval --
      le_of_int = knot(nc+2) - knot(1)

c     -- length of control polygon --
      le_of_poly = 0.d0
      do i=1,nc
        le_of_poly = le_of_poly + dsqrt( ( rhs(i+1,1) - rhs(i,1) )**2
     &        +( rhs(i+1,2) - rhs(i,2) )**2 )
      end do

c     -- parameter correction --
      do j=1,jbody
        corr(j) = ( errorx(j)*xp(j) + errory(j)*yp(j) )/dsqrt(
     &        xp(j)**2+ yp(j)**2 )*le_of_int/le_of_poly
      end do

c     -- update parameter array t(j) --
      tmp = -1.d0
      fac = 1.d0
      do j=1,jbody
        cont = .true.          
        do while (cont)
          tn = t(j) + fac*corr(j) 

          do left=iorder,nc+1

            if ( knot(left).le.tn .and. tn.le.knot(left+1)
     &            .and. tn.ne.tmp ) then 
              tmp = tn
              call Bval( knot, t(j), left, BIatT)

              x(j) = 0.d0
              y(j) = 0.d0
              do i=1,iorder
                x(j) = x(j) + rhs(i+left-iorder,1)*BIatT(i)
                y(j) = y(j) + rhs(i+left-iorder,2)*BIatT(i)
              end do

            end if

          end do
          
c     -- compute new distance between points: if new distance is --
c     -- smaller then update t(j), else halve the correction and --
c     -- try again --
          NewDist = dsqrt( (xdata(j)-x(j))**2 + (ydata(j)-y(j))**2 )
          if (NewDist.gt.dist(j)) then
c             write (*,*) 't',j,NewDist,dist(j) 
             fac = fac*0.5d0
          else
            t(j) = tn
            cont = .false.
c            write (*,*) 'f',j,NewDist,dist(j)
            fac = 1.d0
          end if
        end do
      end do
      goto 20                   !least-squares correction loop

c     -- done parameter correction --
 40   continue

      werr = .true.
      call error( x, y, xdata, ydata, resid, errorx, errory, dist, werr)
      write (*,*)

cmn        write (etec_unit,110)
cmn        write (etec_unit,120) ( i, dist(i), i=1, jbody)
cmn 110    format ('VARIABLES= "Airfoil Node", Error')
cmn 120    format (i5,e16.8)

      do i=1,jbody
        xdata(i) = x(i)
        ydata(i) = y(i)
      end do
 
      nc = nc+1

      do i=1,nc
        bcp(i,1)=rhs(i,1)
        bcp(i,2)=rhs(i,2)
      end do

c     -- display knot vector --
c      write (*,*) 'Knot vector:'
c      write (*,15) ( knot(i), i=1,nc+iorder+1 )
c      write (*,*)

c     -- display parameter vector --
c      write (*,*) 'Parameter vector:'
c      write (*,17) ( t(i), i=1,jbody )
c      write (*,*)
      
      return
      end                       !bspline

c-----------------------------------------------------------------------
c-----------------------------------------------------------------------

c     given knots, parameter vector and control points find curve
c     coordinates x and y
c     m. nemec

      subroutine curve( knot, t, x, y, rhs )

      implicit none

#include "bsord.inc"     

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer i, j, left

      double precision t(jbody), knot(nc+iorder+1), x(jbody), y(jbody)
      double precision rhs(jbody,2), tmp, BIatT(ibsord)

      tmp = -1.d0
      do j=1,jbody
        do left=iorder,nc+1

          if ( knot(left).le.t(j) .and. t(j).le.knot(left+1)
     &          .and. t(j).ne.tmp ) then 
            tmp = t(j)
            call Bval( knot, t(j), left, BIatT)

            x(j) = 0.d0
            y(j) = 0.d0
            do i=1,iorder
              x(j) = x(j) + rhs(i+left-iorder,1)*BIatT(i)
              y(j) = y(j) + rhs(i+left-iorder,2)*BIatT(i)
            end do

          end if

        end do
      end do    

      return
      end                       !curve
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------

c     compute error of approximation and max. error
c     m. nemec

      subroutine error( x, y, xdata, ydata, resid, errorx, errory, dist,
     &      werr)

      implicit none

      integer iorder, nc, maxiter, jbody
      double precision tol
      logical tecplot
      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot

      integer j, MaxNode

      double precision resid, MaxErr, errorx(jbody), errory(jbody)
      double precision dist(jbody), xdata(jbody), ydata(jbody)
      double precision x(jbody), y(jbody)

      logical werr

      resid = 0.d0
      MaxErr = 0.d0
      do j=1,jbody
        errorx(j) = xdata(j) - x(j)
        errory(j) = ydata(j) - y(j)
        dist(j) = dsqrt( errorx(j)**2 + errory(j)**2 ) 
        resid = resid + dist(j)
        if ( dist(j).gt.MaxErr ) then
          MaxErr = dist(j)
          MaxNode = j
        end if
      end do
      resid = resid/dble(jbody)

      if (werr) then
        write (*,15) resid
        write (*,20) MaxErr, MaxNode
      end if

 15   format (3x,'Average error of approximation:',e12.4)
 20   format (3x,'Max. error of approximation:',e12.4,1x,'at node',i4)

      return
      end                       !error
c----------------------------------------------------------------------
c----------------------------------------------------------------------
c     find tangents
c     use o(2) centred-difference formula
c
c      subroutine tangents (knot, t, x, y, xp, yp, rhs)
c
c      implicit none
c
c#include "params.inc"
c
c      integer iorder, nc, maxiter, jbody, j  
c      double precision tol
c      logical tecplot
c      common/bspl/  tol, iorder, nc, maxiter, jbody, tecplot
c
c      double precision t(jbody), knot(nc+iorder+1), x(jbody), y(jbody)
c      double precision xp(jbody), yp(jbody), rhs(jbody,2)
c
c      double precision stepsize, tnew(ibody), xplus(ibody), yplus(ibody)
c      double precision xminus(ibody), yminus(ibody)
c
c      stepsize = 1.d-8
c
c      do j=1,jbody-1
c        tnew(j) = t(j) + stepsize
c      end do
c      tnew(jbody) = t(jbody)
c      
c      call curve(knot, tnew, xplus, yplus, rhs)
c
c      tnew(1) = t(1)
c      do j=2,jbody
c        tnew(j) = t(j) - stepsize
c      end do 
c
c      call curve(knot, tnew, xminus, yminus, rhs)
c
c      do j=2,jbody-1
c        xp(j) = ( xplus(j) - xminus(j) ) / (2.d0*stepsize)
c        yp(j) = ( yplus(j) - yminus(j) ) / (2.d0*stepsize)
c      end do
c
c      xp(1) = ( xplus(1) - x(1) ) / stepsize
c      yp(1) = ( yplus(1) - y(1) ) / stepsize
c
c      xp(jbody) = ( x(jbody) - xminus(jbody) ) / stepsize
c      yp(jbody) = ( y(jbody) - yminus(jbody) ) / stepsize
c
c      return
c      end                       !tangents
c-----------------------------------------------------------------------
