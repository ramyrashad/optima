      subroutine vortdeta(jdim,kdim,q,vort,xy,work)
c
#include "../include/arcom.inc"
c                                                                       
      dimension q(jdim,kdim,4),vort(jdim,kdim)                          
      dimension xy(jdim,kdim,4),work(jdim,kdim)          
c                                                                       
c                                                                       
c     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
c     note vortdxi must be call first                                     
c            vort comes in with contribution to vorticity in xi           
c     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
c                                                                       
c     compute vorticity a 1/2 k points i.e. k+1/2
c                                                                       
      if (iord.eq.2) then
         do 10 k = kbegin,kup
            kp1=k+1                                
            do 9 j = jlow,jup
               xy3h= xy(j,k,3)+xy(j,kp1,3)
               xy4h= xy(j,k,4)+xy(j,kp1,4)
c
               r   = 1.d0/q(j,k,1)
               rp1 = 1.d0/q(j,kp1,1)
               u   = q(j,k,2)*r
               up1 = q(j,kp1,2)*rp1
               v   = q(j,k,3)*r
               vp1 = q(j,kp1,3)*rp1
c
               tau  = 0.5d0*(xy4h*(up1-u) - xy3h*(vp1-v))
               taxi = 0.5d0*(vort(j,k) + vort(j,kp1))
               work(j,k) = tau + taxi
 9          continue
 10      continue
      elseif (iord.eq.4) then
         tmp=1.d0/24.d0
         tmp2=1.d0/16.d0
         do 150 k=klow,kup-1
            km1=k-1
            kp1=k+1
            kp2=k+2
            do 100 j=jlow,jup
               rm1=1.d0/q(j,km1,1)
               r=1.d0/q(j,k,1)
               rp1=1.d0/q(j,kp1,1)
               rp2=1.d0/q(j,kp2,1)
               um1=q(j,km1,2)*rm1
               u  =q(j,k,2)*r
               up1=q(j,kp1,2)*rp1
               up2=q(j,kp2,2)*rp2
               vm1=q(j,km1,3)*rm1
               v  =q(j,k,3)*r
               vp1=q(j,kp1,3)*rp1
               vp2=q(j,kp2,3)*rp2
c
               tau =
c              -fourth order interpolation of metrics       
c                  -fourth order velocity derivatives at half nodes
     &              tmp2*
     &          (-xy(j,km1,4)+9.d0*(xy(j,k,4)+xy(j,kp1,4))-xy(j,kp2,4))*
     &                  tmp*(um1 + 27.d0*(up1-u) - up2)
     &              -tmp2*
     &          (-xy(j,km1,3)+9.d0*(xy(j,k,3)+xy(j,kp1,3))-xy(j,kp2,3))*
     &                  tmp*(vm1 + 27.d0*(vp1-v) - vp2)
c
c              extrapolate vorticity from xi contribution to half node
               taxi = tmp2*(-vort(j,km1)
     &                +9.d0*(vort(j,k)+vort(j,kp1))-vort(j,kp2))
               work(j,k) = tau + taxi
 100        continue
 150     continue
c
c        farfield boundary
c        -third-order interpolation with second-order derivative
         k=kup
         kq=kup+1
         tmp2=1.d0/8.d0
         do 175 j=jlow,jup
c           rm2=1.d0/q(j,k-2,1)
c           rm1=1.d0/q(j,k-1,1)
           r=1.d0/q(j,k,1)
           rp1=1.d0/q(j,kq,1)
c           um2=q(j,k-2,2)*rm2
c           um1=q(j,k-1,2)*rm1
           u  =q(j,k,2)*r
           up1=q(j,kq,2)*rp1
c           vm2=q(j,k-2,3)*rm2
c           vm1=q(j,k-1,3)*rm1
           v  =q(j,k,3)*r
           vp1=q(j,kq,3)*rp1
c           tau =tmp2*(-xy(j,k-2,4)+9.d0*(xy(j,k-1,4)+xy(j,k,4))
c     &                                                 -xy(j,kq,4)) *
c     &          tmp*(um2 + 27.d0*(u-um1) - up1) -
c     &          tmp2*(-xy(j,k-2,3)+9.d0*(xy(j,k-1,3)+xy(j,k,3))
c     &                                                 -xy(j,kq,3)) *
c     &          tmp*(vm2 + 27.d0*(v-vm1) - vp1)
c           taxi = tmp2*(-vort(j,k-2)
c     &                +9.d0*(vort(j,k-1)+vort(j,k))-vort(j,kq))
           tau =tmp2*((-xy(j,kq-2,4)+6.d0*xy(j,kq-1,4)+3.d0*xy(j,kq,4))*
     &          (up1-u) -
     &          (-xy(j,kq-2,3)+6.d0*xy(j,kq-1,3)+3.d0*xy(j,kq,3))*
     &          (vp1-v))
           taxi = tmp2*(-vort(j,kq-2)+6.d0*vort(j,kq-1)+3.d0*vort(j,kq))
           work(j,k) = tau + taxi
 175     continue
c        airfoil body
         k=kbegin
         kp1=k+1
         kp2=k+2
c        -third-order interpolation with second-order derivative
         do 200 j=jlow,jup
           r=1.d0/q(j,k,1)
           rp1=1.d0/q(j,kp1,1)
           u  =q(j,k,2)*r
           up1=q(j,kp1,2)*rp1
           v  =q(j,k,3)*r
           vp1=q(j,kp1,3)*rp1
           tau =tmp2*(3.d0*xy(j,k,4)+6.d0*xy(j,kp1,4)-xy(j,kp2,4))
     &              *(up1-u) -
     &          tmp2*(3.d0*xy(j,k,3)+6.d0*xy(j,kp1,3)-xy(j,kp2,3))
     &              *(vp1-u)
           taxi = tmp2*(3.d0*vort(j,k)+6.d0*vort(j,kp1)-vort(j,kp2))
           work(j,k) = tau + taxi
 200     continue
      endif
c                                                                       
c     load vorticity into vort                                             
c                                                                       
      do 1000 k = kbegin,kup                                            
      do 1000 j = jlow,jup                                              
         vort(j,k) = work(j,k)                                           
c         if (numiter.ne.20) vort(j,k) = work(j,k)
c         if (iord.eq.2) then
c            if (numiter.eq.20 .and. k.eq.kup) write(92,*) j,work(j,k)
c         else
c            if (numiter.eq.20 .and. k.eq.kup) write(94,*) j,work(j,k)
c         endif
 1000 continue
c                                                                       
      return                                                          
      end                                                             
