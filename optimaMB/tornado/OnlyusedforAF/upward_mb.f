c
c START SOLVING
c
c UPWARD SWEEP
c
      do k=klow,kup
         do j=jlow,jup
            denomk=1.-(dtm*ds(j,k))*(q11(j,k)+cx_k(j,k)+cy_k(j,k))
     $           /q(j,k,1)
            denome=1.-(dtm*ds(j,k))*(q22(j,k)+cx_e(j,k)+cy_e(j,k))
     $           /q(j,k,1)
            bbk(j)=-bx_k(j,k)/denomk
            bbe(j)=-bx_e(j,k)/denome
c
            aak(j)=-dx_k(j,k)/denomk
            aae(j)=-dx_e(j,k)/denome
c
            ddk(j)=q(j,k,1)/(dtm*ds(j,k))
            dde(j)=q(j,k,1)/(dtm*ds(j,k))
c
            cck(j)=rhs_k(j,k)/denomk
     $           +0.5*(isign(1,isub-2)+isign(1,isub-1))*
     $           (aak(j)*(dk_star(j+1,k)-dk(j+1,k))
     $           +bbk(j)*(dk_star(j-1,k)-dk(j-1,k)))
            cce(j)=rhs_e(j,k)/denome
     $           +0.5*(isign(1,isub-2)+isign(1,isub-1))*
     $           (aae(j)*(de_star(j+1,k)-de(j+1,k))
     $           +bbe(j)*(de_star(j-1,k)-de(j-1,k)))
         enddo
            call thomas(jlow,jup,bbk,ddk,aak,cck)
            call thomas(jlow,jup,bbe,dde,aae,cce)
            do j=jlow,jup
               dk_star(j,k)=cck(j)
               de_star(j,k)=cce(j)
            enddo
      enddo
