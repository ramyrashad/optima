c ##########################
c ##                      ##
c ##  subroutine filtery  ##
c ##                      ##
c ##########################
c
      subroutine filtery3 (jdim,kdim,q,s,xyj,coef2,coef4,sndsp,work,
     >                    precon,tmp)            
c
c********************************************************************
c                                                                       
c    fourth order smoothing, added explicitly to rhs                     
c second order near shocks with pressure grd coeff.                   
c                                                                       
c               eta direction
c                                                                       
c   start differences each variable separatly                           
c********************************************************************
c
      include '../include/arcom.inc'
c
      dimension q(jdim,kdim,4),s(jdim,kdim,4),xyj(jdim,kdim)            
      dimension coef2(jdim,kdim),coef4(jdim,kdim)                       
      dimension work(jdim,kdim,3),precon(jdim,kdim,6),tmp(jdim,kdim,4)   
      dimension tmp2(maxj,maxk,4)
      do 39 n = 1,4                                                     
c
c
c---- 1st-order forward difference ----
      do 35 k = kbegin,kup                                              
      kpl = k+1
      do 35 j = jlow,jup                                                
         work(j,k,1) = q(j,kpl,n)*xyj(j,kpl) - q(j,k,n)*xyj(j,k)           
 35   continue                                                          
c
c
c---- for fourth order: apply cent-dif to 1st-order forward ----
      do 36 k = klow,kup-1                                              
      kpl = k+1                                                         
      kmi = k-1                                                         
      do 36 j = jlow,jup                                                
         work(j,k,2) = work(j,kpl,1) - 2.* work(j,k,1) + work(j,kmi,1)     
 36   continue                                                          
c  -- next points to boundaries: rows 2 & kmax-1 --
      kb = kbegin
      do 37 j = jlow,jup                                                
      work(j,kbegin,2) = q(j,kb+2,n)*xyj(j,kb+2) -                          
     >                 2.*q(j,kb+1,n)*xyj(j,kb+1) + q(j,kb,n)*xyj(j,kb)
      work(j,kup,2)    = work(j,kup-1,1) - work(j,kup,1)                   
 37   continue                                                          
c                                                                       
c                                                                       
c---- form dissipation term before last dif ----
      do 38 k = kbegin,kup                                              
      do 38 j = jlow,jup                                                
         tmp(j,k,n) = (coef2(j,k)*work(j,k,1)-coef4(j,k)*work(j,k,2))  
         tmp2(j,k,n)=tmp(j,k,n)
 38   continue                                                          
c
 39   continue                                                          
c                                                                       
cdu   precondition the dissipation operator
c 
      dtd = dt / (1. + phidt)                                           
      if (prec.gt.0) then
         call predis2(jdim,kdim,0,0,q,xyj,sndsp,precon,tmp)
         do 90 n=1,4
         do 90 k=klow,kup
         do 90 j=jlow,jup
           s(j,k,n)=s(j,k,n)+(tmp(j,k,n) - tmp(j,k-1,n))*dtd
 90      continue
         do 100 n=1,4
         do 100 k=kbegin,kend
         do 100 j=jbegin,jend       
            tmp(j,k,n)=tmp2(j,k,n)
 100     continue
         call predis2(jdim,kdim,0,1,q,xyj,sndsp,precon,tmp)
      endif
c
c---- last differenciation and add in dissipation ----
      do 200 n=1,4
      do 200 k=klow,kup
      do 200 j=jlow,jup
         s(j,k,n)=s(j,k,n) + (tmp(j,k,n)-tmp(j,k-1,n))*dtd
 200  continue
      return                                                            
      end                                                               
