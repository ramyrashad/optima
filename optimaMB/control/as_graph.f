c     -----------------------------------------------------------------
c     -- setup of jacobian pointer arrays ia and ja --
c     -- m. nemec, sept. 2001 --
c     -----------------------------------------------------------------
      subroutine as_int ( indx, jmax, kmax, nmax, js, je, ks, ke, ja,
     &     ia, icol) 

      implicit none
      integer jmax, kmax, nmax, js, je, ks, ke
      integer j,k,n,jk,jj,jm2,jm1,jp1,jp2,km2,km1,kp1,kp2,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)

c     -- interior nodes --
      do k = ks,ke
         do j = js,je
            do n = 1,nmax

               jk = ( indx(j,k) - 1 )*nmax + n
               jj = ( jk - 1 )*icol(9)
               
               jm2 = ( indx(j-2,k) - 1 )*nmax
               jm1 = ( indx(j-1,k) - 1 )*nmax
               jp1 = ( indx(j+1,k) - 1 )*nmax
               jp2 = ( indx(j+2,k) - 1 )*nmax

               km2 = ( indx(j,k-2) - 1 )*nmax
               km1 = ( indx(j,k-1) - 1 )*nmax
               kp1 = ( indx(j,k+1) - 1 )*nmax
               kp2 = ( indx(j,k+2) - 1 )*nmax

               do m = 1,nmax
                  ja(jj+        m) = jm2 + m 
                  ja(jj+icol(1)+m) = jm1 + m  
                  ja(jj+icol(2)+m) = km2 + m
                  ja(jj+icol(3)+m) = km1 + m
                  ja(jj+icol(4)+m) = jk  - n + m
                  ja(jj+icol(5)+m) = kp1 + m
                  ja(jj+icol(6)+m) = kp2 + m
                  ja(jj+icol(7)+m) = jp1 + m
                  ja(jj+icol(8)+m) = jp2 + m
               end do
               ia(jk) = 9*nmax
            end do
         end do
      end do

      return
      end                       !as_int
c     -----------------------------------------------------------------
      subroutine as_bc( ib, iside, nmax, indx, ja, ia, icol)

      implicit none

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"
#include "../include/common.inc"

      integer ib, iside, nmax
      integer k1,k2,k3,j1,j2,j3,ibn,isn
      integer indx(*), ja(*), ia(*), icol(*)

      if (ibctype(ib,iside).eq.1) then
c     -- B.C. body surface --
         if (.not.viscous) then
            if (iside.eq.1) then
               k1 = kminbnd(ib)
               k2 = k1+1
               k3 = k1+2
               call jbcbi( k1, k2, k3, jminbnd(ib), jmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, ja, ia,
     &              icol)

            else if (iside.eq.3) then
               k1 = kmaxbnd(ib)
               k2 = k1-1
               k3 = k1-2
               call jbcbi( k1, k2, k3, jminbnd(ib), jmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, ja, ia,
     &              icol)

            else if (iside.eq.2) then 
               j1 = jminbnd(ib)
               j2 = j1+1
               j3 = j1+2
               call jbcbi24( j1, j2, j3, kminbnd(ib), kmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, ja, ia,
     &              icol)

            else
               write(*,*) 'order: problem on body (inviscid)'
               stop
            end if
         else                   !viscous flow
            if (iside.eq.1) then
               k1 = kminbnd(ib)
               k2 = k1+1
               call jbcbv( k1, k2,  jminbnd(ib), jmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, ja, ia,
     &              icol)

            else if (iside.eq.3) then 
               k1 = kmaxbnd(ib)
               k2 = k1-1
               call jbcbv( k1, k2,  jminbnd(ib), jmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, ja, ia,
     &              icol)

            else if (iside.eq.2) then 
               j1 = jminbnd(ib)
               j2 = j1+1
               call jbcbv24( j1, j2,  kminbnd(ib), kmaxbnd(ib),
     &              indx(lgptr(ib)), jmax(ib), kmax(ib), nmax, ja, ia,
     &              icol)

            else
               write(*,*) 'order: problem on body (viscous)'
               stop
            end if
         end if
      else if (ibctype(ib,iside).eq.2) then
c     -- B.C. inviscid far field (Riemann) --
         if (iside .eq. 1) then
            k1 = kminbnd(ib)
            k2 = k1+1
            call jbc2s13( k1, k2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &           nmax, jminbnd(ib), jmaxbnd(ib), ja, ia, icol)
         else if (iside .eq. 2) then  
            j1 = jminbnd(ib)
            j2 = j1+1
            call jbc2s24( j1, j2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &           nmax, kminbnd(ib)+1, kmaxbnd(ib)-1, ja, ia, icol)
         else if (iside .eq. 3) then
            k1 = kmaxbnd(ib)
            k2 = k1-1
            call jbc2s13( k1, k2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &           nmax, jminbnd(ib), jmaxbnd(ib), ja, ia, icol)
         else if (iside .eq. 4) then 
            j1 = jmaxbnd(ib)
            j2 = j1-1
            call jbc2s24( j1, j2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &           nmax, kminbnd(ib)+1, kmaxbnd(ib)-1, ja, ia, icol)
         end if

      else if (ibctype(ib,iside).eq.3) then
c     -- viscous outflow at j=jmin side 2 --
         j1 = jminbnd(ib)
         j2 = j1+1
         call jbc2s24( j1, j2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &        nmax, kminbnd(ib)+1, kmaxbnd(ib)-1, ja, ia, icol)
         
      else if (ibctype(ib,iside).eq.4) then
c     -- viscous outflow at j=jmax side 4 --
         j1 = jmaxbnd(ib)
         j2 = j1-1
         call jbc2s24( j1, j2, indx(lgptr(ib)), jmax(ib), kmax(ib),
     &        nmax, kminbnd(ib)+1, kmaxbnd(ib)-1, ja, ia, icol)

      else if (ibctype(ib,iside).eq.5 ) then
c     -- average BC across the boundary --
         if (iside .eq. 1 .or. iside .eq. 3) then
            ibn = lblkpt(ib,iside)
            isn = lsidept(ib,iside)
            call jbc5s13(  iside, isn, indx(lgptr(ib)), jmax(ib),
     &           kmax(ib), indx(lgptr(ibn)), jmax(ibn), kmax(ibn),
     &           jminbnd(ib), jmaxbnd(ib), kminbnd(ib), kmaxbnd(ib),
     &           kminbnd(ibn), kmaxbnd(ibn), nmax, ja, ia, icol)
         else
            write (*,*) 'order: ibctype = 5'
            write (*,*) 'side != 1 or 3'
            stop
         end if
      end if

      return
      end                       !as_bc
c     -----------------------------------------------------------------
      subroutine as_fin1( k, js, je, nmax, indx, jmax, kmax, ja, ia,
     &     icol)

      implicit none
      integer k, js, je, nmax,jmax, kmax
      integer j,n,jk,jj,jm2,jm1,jp1,jp2,km1,kp1,kp2,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)     

      do j = js,je
         do n = 1,nmax

            jk  = ( indx(j,k) - 1 )*nmax + n
            jj  = ( jk - 1 )*icol(9)

            jm2 = ( indx(j-2,k) - 1 )*nmax
            jm1 = ( indx(j-1,k) - 1 )*nmax
            jp1 = ( indx(j+1,k) - 1 )*nmax
            jp2 = ( indx(j+2,k) - 1 )*nmax

            km1 = ( indx(j,k-1) - 1 )*nmax
            kp1 = ( indx(j,k+1) - 1 )*nmax
            kp2 = ( indx(j,k+2) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = jm2 + m
               ja(jj+icol(1)+m) = jm1 + m
               ja(jj+icol(2)+m) = km1 + m
               ja(jj+icol(3)+m) = jk  - n + m
               ja(jj+icol(4)+m) = kp1 + m
               ja(jj+icol(5)+m) = kp2 + m
               ja(jj+icol(6)+m) = jp1 + m
               ja(jj+icol(7)+m) = jp2 + m
            end do
            ia(jk) = 8*nmax
         end do
      end do

      return
      end                       !as_fin1
c     -----------------------------------------------------------------
      subroutine as_fin2( j, ks, ke, nmax, indx, jmax, kmax, ja, ia,
     &     icol)

      implicit none
      integer j, ks, ke, nmax, jmax, kmax
      integer k,n,jk,jj,jm1,jp1,jp2,km2,km1,kp1,kp2,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)     

      do k = ks,ke
         do n = 1,nmax

            jk  = ( indx(j,k) - 1 )*nmax + n
            jj  = ( jk - 1 )*icol(9)

            jm1 = ( indx(j-1,k) - 1 )*nmax
            jp1 = ( indx(j+1,k) - 1 )*nmax
            jp2 = ( indx(j+2,k) - 1 )*nmax

            km2 = ( indx(j,k-2) - 1 )*nmax
            km1 = ( indx(j,k-1) - 1 )*nmax
            kp1 = ( indx(j,k+1) - 1 )*nmax
            kp2 = ( indx(j,k+2) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = jm1 + m  
               ja(jj+icol(1)+m) = km2 + m
               ja(jj+icol(2)+m) = km1 + m
               ja(jj+icol(3)+m) = jk  - n + m
               ja(jj+icol(4)+m) = kp1 + m
               ja(jj+icol(5)+m) = kp2 + m
               ja(jj+icol(6)+m) = jp1 + m
               ja(jj+icol(7)+m) = jp2 + m
            end do
            ia(jk) = 8*nmax
         end do
      end do

      return
      end                       !as_fin2
c     -----------------------------------------------------------------
      subroutine as_fin3( k, js, je, nmax, indx, jmax, kmax, ja, ia,
     &     icol)

      implicit none
      integer k, js, je, nmax, jmax, kmax
      integer j,n,jk,jj,jm2,jm1,jp1,jp2,km2,km1,kp1,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)     

      do j = js,je
         do n = 1,nmax

            jk  = ( indx(j,k) - 1 )*nmax + n
            jj  = ( jk - 1 )*icol(9)

            jm2 = ( indx(j-2,k) - 1 )*nmax
            jm1 = ( indx(j-1,k) - 1 )*nmax
            jp1 = ( indx(j+1,k) - 1 )*nmax
            jp2 = ( indx(j+2,k) - 1 )*nmax

            km2 = ( indx(j,k-2) - 1 )*nmax
            km1 = ( indx(j,k-1) - 1 )*nmax
            kp1 = ( indx(j,k+1) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = jm2 + m
               ja(jj+icol(1)+m) = jm1 + m
               ja(jj+icol(2)+m) = km2 + m
               ja(jj+icol(3)+m) = km1 + m
               ja(jj+icol(4)+m) = jk  - n + m
               ja(jj+icol(5)+m) = kp1 + m
               ja(jj+icol(6)+m) = jp1 + m
               ja(jj+icol(7)+m) = jp2 + m
            end do
            ia(jk) = 8*nmax
         end do
      end do

      return
      end                       !as_fin3
c     -----------------------------------------------------------------
      subroutine as_fin4( j, ks, ke, nmax, indx, jmax, kmax, ja, ia,
     &     icol)

      implicit none
      integer j, ks, ke, nmax,  jmax, kmax
      integer k,n,jk,jj,jm2,jm1,jp1,km2,km1,kp1,kp2,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)     

      do k = ks,ke
         do n = 1,nmax

            jk  = ( indx(j,k) - 1 )*nmax + n
            jj  = ( jk - 1 )*icol(9)

            jm2 = ( indx(j-2,k) - 1 )*nmax
            jm1 = ( indx(j-1,k) - 1 )*nmax
            jp1 = ( indx(j+1,k) - 1 )*nmax

            km2 = ( indx(j,k-2) - 1 )*nmax
            km1 = ( indx(j,k-1) - 1 )*nmax
            kp1 = ( indx(j,k+1) - 1 )*nmax
            kp2 = ( indx(j,k+2) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = jm2 + m
               ja(jj+icol(1)+m) = jm1 + m
               ja(jj+icol(2)+m) = km2 + m
               ja(jj+icol(3)+m) = km1 + m
               ja(jj+icol(4)+m) = jk  - n + m
               ja(jj+icol(5)+m) = kp1 + m
               ja(jj+icol(6)+m) = kp2 + m
               ja(jj+icol(7)+m) = jp1 + m
            end do
            ia(jk) = 8*nmax
         end do
      end do

      return
      end                       !as_fin4
c     -----------------------------------------------------------------
      subroutine as_ic1( j, k, nmax, indx, jmax, kmax, ja, ia, icol)

      implicit none 
      integer j, k, nmax, jmax, kmax
      integer n,jk,jj,jm1,jp1,jp2,km1,kp1,kp2,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)     
      
      do n = 1,nmax

         jk  = ( indx(j,k) - 1 )*nmax + n
         jj  = ( jk - 1 )*icol(9)

         jm1 = ( indx(j-1,k) - 1 )*nmax
         jp1 = ( indx(j+1,k) - 1 )*nmax
         jp2 = ( indx(j+2,k) - 1 )*nmax

         km1 = ( indx(j,k-1) - 1 )*nmax
         kp1 = ( indx(j,k+1) - 1 )*nmax
         kp2 = ( indx(j,k+2) - 1 )*nmax

         do m = 1,nmax
            ja(jj+        m) = jm1 + m  
            ja(jj+icol(1)+m) = km1 + m
            ja(jj+icol(2)+m) = jk  - n + m
            ja(jj+icol(3)+m) = kp1 + m
            ja(jj+icol(4)+m) = kp2 + m
            ja(jj+icol(5)+m) = jp1 + m
            ja(jj+icol(6)+m) = jp2 + m
         end do
         ia(jk) = 7*nmax
      end do

      return
      end                       !as_ic1
c     -----------------------------------------------------------------
      subroutine as_ic2( j, k, nmax, indx, jmax, kmax, ja, ia, icol)

      implicit none 
      integer j, k, nmax, jmax, kmax
      integer n,jk,jj,jm2,jm1,jp1,km1,kp1,kp2,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)     

      do n = 1,nmax

         jk  = ( indx(j,k) - 1 )*nmax + n
         jj  = ( jk - 1 )*icol(9)

         jm2 = ( indx(j-2,k) - 1 )*nmax
         jm1 = ( indx(j-1,k) - 1 )*nmax
         jp1 = ( indx(j+1,k) - 1 )*nmax
         
         km1 = ( indx(j,k-1) - 1 )*nmax
         kp1 = ( indx(j,k+1) - 1 )*nmax
         kp2 = ( indx(j,k+2) - 1 )*nmax

         do m = 1,nmax
            ja(jj+        m) = jm2 + m
            ja(jj+icol(1)+m) = jm1 + m
            ja(jj+icol(2)+m) = km1 + m
            ja(jj+icol(3)+m) = jk  - n + m
            ja(jj+icol(4)+m) = kp1 + m
            ja(jj+icol(5)+m) = kp2 + m
            ja(jj+icol(6)+m) = jp1 + m
         end do
         ia(jk) = 7*nmax
      end do

      return
      end                       !as_ic2
c     -----------------------------------------------------------------
      subroutine as_ic3( j, k, nmax, indx, jmax, kmax, ja, ia, icol)

      implicit none 
      integer j, k, nmax, jmax, kmax
      integer n,jk,jj,jm1,jp1,jp2,km2,km1,kp1,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)  

      do n = 1,nmax

         jk  = ( indx(j,k) - 1 )*nmax + n
         jj  = ( jk - 1 )*icol(9)

         jm1 = ( indx(j-1,k) - 1 )*nmax
         jp1 = ( indx(j+1,k) - 1 )*nmax
         jp2 = ( indx(j+2,k) - 1 )*nmax

         km2 = ( indx(j,k-2) - 1 )*nmax
         km1 = ( indx(j,k-1) - 1 )*nmax
         kp1 = ( indx(j,k+1) - 1 )*nmax

         do m = 1,nmax
            ja(jj+        m) = jm1 + m
            ja(jj+icol(1)+m) = km2 + m
            ja(jj+icol(2)+m) = km1 + m
            ja(jj+icol(3)+m) = jk  - n + m
            ja(jj+icol(4)+m) = kp1 + m
            ja(jj+icol(5)+m) = jp1 + m
            ja(jj+icol(6)+m) = jp2 + m
         end do
         ia(jk) = 7*nmax
      end do

      return
      end                       !as_ic3
c     -----------------------------------------------------------------
      subroutine as_ic4( j, k, nmax, indx, jmax, kmax, ja, ia, icol)

      implicit none 
      integer j, k, nmax, jmax, kmax
      integer n,jk,jj,jm2,jm1,jp1,km2,km1,kp1,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)  

      do n = 1,nmax

         jk  = ( indx(j,k) - 1 )*nmax + n
         jj  = ( jk - 1 )*icol(9)

         jm2 = ( indx(j-2,k) - 1 )*nmax
         jm1 = ( indx(j-1,k) - 1 )*nmax
         jp1 = ( indx(j+1,k) - 1 )*nmax
         
         km2 = ( indx(j,k-2) - 1 )*nmax
         km1 = ( indx(j,k-1) - 1 )*nmax
         kp1 = ( indx(j,k+1) - 1 )*nmax

         do m = 1,nmax
            ja(jj+        m) = jm2 + m
            ja(jj+icol(1)+m) = jm1 + m
            ja(jj+icol(2)+m) = km2 + m
            ja(jj+icol(3)+m) = km1 + m
            ja(jj+icol(4)+m) = jk  - n + m
            ja(jj+icol(5)+m) = kp1 + m
            ja(jj+icol(6)+m) = jp1 + m
         end do
         ia(jk) = 7*nmax
      end do

      return
      end                       !as_ic4
c     -----------------------------------------------------------------
      subroutine as_sng( js1, ks1, js2, ks2, javg1, kavg1, javg2,
     &     kavg2, indx1, jmax1, kmax1, indx2, jmax2, kmax2, nmax, ja,
     &     ia, icol)

      implicit none 
      integer js1,ks1,js2,ks2,javg1,kavg1,javg2,kavg2,jmax1,kmax1 
      integer jmax2,kmax2,nmax,n,m,jk,jj,jk0,jk1,jk2
      integer indx1(jmax1,kmax1), indx2(jmax2,kmax2)
      integer ja(*), ia(*), icol(*) 

c     -- leading edge sindular points --

c     -- overwrite entries in sparse matrix --
      do n = 1,nmax

         jk = ( indx1(js1,ks1) - 1 )*nmax + n
         jj = (jk-1)*icol(9)

         jk0 = ( indx1(js1,ks1) - 1 )*nmax
         jk1 = ( indx1(javg1,kavg1) - 1 )*nmax
         jk2 = ( indx2(javg2,kavg2) - 1 )*nmax

         do m = 1,nmax
            ja(jj+        m) = jk0 + m
            ja(jj+icol(1)+m) = jk1 + m
            ja(jj+icol(2)+m) = jk2 + m
            ja(jj+icol(3)+m) = 0
            ja(jj+icol(4)+m) = 0
         end do
         ia(jk) = 3*nmax
      end do   

      do n = 1,nmax

         jk = ( indx2(js2,ks2) - 1 )*nmax + n
         jj = (jk-1)*icol(9)

         jk0 = ( indx2(js2,ks2) - 1 )*nmax
         jk1 = ( indx2(javg2,kavg2) - 1 )*nmax
         jk2 = ( indx1(javg1,kavg1) - 1 )*nmax

         do m = 1,nmax
            ja(jj+        m) = jk0 + m
            ja(jj+icol(1)+m) = jk1 + m
            ja(jj+icol(2)+m) = jk2 + m
            ja(jj+icol(3)+m) = 0
            ja(jj+icol(4)+m) = 0
         end do
         ia(jk) = 3*nmax
      end do   

      return
      end                       !as_sng
c     -----------------------------------------------------------------
      subroutine as_cop( ijte1, ikte1, ijte2, ikte2, indx1, jmax1,
     &     kmax1, indx2, jmax2, kmax2, nmax, ja, ia, icol)


      implicit none 
      integer ijte1,ikte1,ijte2,ikte2,jmax1,kmax1,jmax2,kmax2,nmax
      integer n,m,jk,jj,jk0,jk1
      integer indx1(jmax1,kmax1), indx2(jmax2,kmax2)
      integer ja(*), ia(*), icol(*) 

c     -- copy points --

c     -- overwrite previous entries in sparse matrix --
      do n = 1,nmax

         jk = ( indx2(ijte2,ikte2) - 1 )*nmax + n
         jj = (jk-1)*icol(9)

         jk0 = ( indx2(ijte2,ikte2) - 1)*nmax
         jk1 = ( indx1(ijte1,ikte1) - 1)*nmax

         do m = 1,nmax
            ja(jj+        m) = jk0 + m
            ja(jj+icol(1)+m) = jk1 + m
            ja(jj+icol(2)+m) = 0
            ja(jj+icol(3)+m) = 0
            ja(jj+icol(4)+m) = 0
         end do
         ia(jk) = 2*nmax
      end do            

      return
      end                       !as_cop
c     -----------------------------------------------------------------
c     -----------------------------------------------------------------
c     -----------------------------------------------------------------
      subroutine jbcbi( k1, k2, k3, js, je, indx, jmax, kmax, nmax, ja,
     &     ia, icol)

      implicit none
      integer k1, k2, k3, js, je, jmax, kmax, nmax
      integer j,n,m,jk,jj,kp0,kp1,kp2
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)

      do j = js,je
         do n = 1,nmax
            jk = ( indx(j,k1) - 1 )*nmax + n
            jj = (jk-1)*icol(9)

            kp0 = ( indx(j,k1) - 1 )*nmax
            kp1 = ( indx(j,k2) - 1 )*nmax
            kp2 = ( indx(j,k3) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = kp0 + m
               ja(jj+icol(1)+m) = kp1 + m
               ja(jj+icol(2)+m) = kp2 + m
            end do
            ia(jk) = 3*nmax
         end do
      end do
      
      return 
      end                       !jbcbi
c     -----------------------------------------------------------------
      subroutine jbcbv(k1, k2, js, je, indx, jmax, kmax, nmax, ja, ia,
     &     icol)

      implicit none
      integer k1, k2, js, je, jmax, kmax, nmax
      integer j,n,jk,jj,m,kp0,kp1
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)

      do j = js,je
         do n = 1,nmax
            jk = ( indx(j,k1) - 1 )*nmax + n
            jj = (jk-1)*icol(9)

            kp0 = ( indx(j,k1) - 1 )*nmax
            kp1 = ( indx(j,k2) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = kp0 + m
               ja(jj+icol(1)+m) = kp1 + m
            end do
            ia(jk) = 2*nmax
         end do
      end do

      return 
      end                       !jbcbv
c     ------------------------------------------------------------------
      subroutine jbc2s13( k1, k2, indx, jmax, kmax, nmax, js, je, ja,
     &     ia, icol)

      implicit none
      integer k1, k2, jmax, kmax, nmax, js, je
      integer j,n,m,jk,jj,kp0,kp1
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)

      do j = js,je
         do n = 1,nmax
            jk = ( indx(j,k1) - 1 )*nmax + n
            jj = (jk-1)*icol(9)

            kp0 = ( indx(j,k1) - 1 )*nmax
            kp1 = ( indx(j,k2) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = kp0 + m
               ja(jj+icol(1)+m) = kp1 + m
            end do
            ia(jk) = 2*nmax
         end do
      end do

      return 
      end                       !jbc2s13
c     ------------------------------------------------------------------
      subroutine jbc2s24( j1, j2, indx, jmax, kmax, nmax, ks, ke, ja,
     &     ia, icol)

      implicit none
      integer j1, j2, jmax, kmax, nmax, ks, ke
      integer k,n,jk,jj,m,jp0,jp1
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)

      do k = ks,ke
         do n = 1,nmax
            jk = ( indx(j1,k) - 1 )*nmax + n
            jj = (jk-1)*icol(9)

            jp0 = ( indx(j1,k) - 1 )*nmax
            jp1 = ( indx(j2,k) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = jp0 + m
               ja(jj+icol(1)+m) = jp1 + m
            end do
            ia(jk) = 2*nmax
         end do     
      end do

      return 
      end                       !jbc2s24
c     -----------------------------------------------------------------
      subroutine jbc5s13( is1, is2, indx, jmax, kmax, indxn, jmaxn,
     &     kmaxn, js, je, ks1, ke1, ks2, ke2, nmax, ja, ia, icol)

      implicit none 
      integer is1,is2,jmax,kmax,jmaxn,kmaxn,js,je,ks1,ke1,ks2,ke2,nmax
      integer k1,k2,k3,if1,if2,j,n,m,jk,jj,kp0,kp1,kp2,jn
      integer indx(jmax,kmax), indxn(jmaxn,kmaxn)
      integer ja(*), ia(*), icol(*) 

c     -- logic to treat c- and h-grids --
      if ( is1.eq.1 ) then
         k1 = ks1
         k2 = ks1 + 1
      else                      ! is1=3
         k1 = ke1
         k2 = ke1 - 1 
      end if      

      if ( is2.eq.1 ) then
         k3 = ks2 + 1
      else                      ! is2=3 
         k3 = ke2 - 1
      end if

      if ( is1.eq.is2 ) then
         if1 = 1
         if2 = -1
      else
         if1 = 0
         if2 = 1
      end if     

      do j = js,je
         do n = 1,nmax
            jk = ( indx(j,k1) - 1 )*nmax + n
            jj = (jk-1)*icol(9)

            jn  = if1*(jmaxn + 1) + if2*j            

            kp0 = ( indx(j,k1) - 1 )*nmax
            kp1 = ( indx(j,k2) - 1 )*nmax
            kp2 = ( indxn(jn,k3) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = kp0 + m
               ja(jj+icol(1)+m) = kp1 + m
               ja(jj+icol(2)+m) = kp2 + m
            end do               
            ia(jk) = 3*nmax
         end do
      end do

      return 
      end                       !jbc5s13
c     -----------------------------------------------------------------

      subroutine jbcbv24(j1, j2, ks, ke, indx, jmax, kmax, nmax, ja, 
     &     ia, icol)

      implicit none
      integer j1, j2, ks, ke, jmax, kmax, nmax
      integer k,n,m,jj,jk,jp0,jp1
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)

      do k = ks,ke
         do n = 1,nmax
            jk = ( indx(j1,k) - 1 )*nmax + n
            jj = (jk-1)*icol(9)

            jp0 = ( indx(j1,k) - 1 )*nmax
            jp1 = ( indx(j2,k) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = jp0 + m
               ja(jj+icol(1)+m) = jp1 + m
            end do
            ia(jk) = 2*nmax
         end do
      end do

      return 
      end                       !jbcbv

c     -----------------------------------------------------------------

      subroutine jbcbi24( j1, j2, j3, ks, ke, indx, jmax, kmax, nmax, 
     &     ja, ia, icol)

      implicit none
      integer j1, j2, j3, ks, ke, jmax, kmax, nmax
      integer k,n,jk,jj,jp0,jp1,jp2,m
      integer indx(jmax,kmax), ja(*), ia(*), icol(*)

      do k = ks,ke
         do n = 1,nmax
            jk = ( indx(j1,k) - 1 )*nmax + n
            jj = (jk-1)*icol(9)

            jp0 = ( indx(j1,k) - 1 )*nmax
            jp1 = ( indx(j2,k) - 1 )*nmax
            jp2 = ( indx(j3,k) - 1 )*nmax

            do m = 1,nmax
               ja(jj+        m) = jp0 + m
               ja(jj+icol(1)+m) = jp1 + m
               ja(jj+icol(2)+m) = jp2 + m
            end do
            ia(jk) = 3*nmax
         end do
      end do
      
      return 
      end                       !jbcbi
c     -----------------------------------------------------------------


