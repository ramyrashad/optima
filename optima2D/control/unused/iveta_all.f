c----------------------------------------------------------------------
c     -- implicit thin-layer viscous terms in y-direction --
c     -- complete differentiation of all terms, including laminar and
c     turbulent viscosities --
c     -- written by: marian nemec --
c     -- date: jan. 2001
c----------------------------------------------------------------------

      subroutine iveta_all (jdim, kdim, index, xy, xyj, q, fmu, turmu,
     &      as, pa, press, sndsp, a1, a2, a3, a4, q2, dj, ddj, hfmu,
     &      vflux)

#include "../include/arcom.inc"

      integer jdim, kdim, index(jdim,kdim)

      double precision q(jdim,kdim,4), xy(jdim,kdim,4), xyj(jdim,kdim)
      double precision as(jdim*kdim*4*36), pa(jdim*kdim*4*20)
      double precision fmu(jdim,kdim), turmu(jdim,kdim)
      double precision dj(jdim,kdim,3,4), ddj(jdim,kdim,3,4)
      double precision q2(jdim,kdim,4), press(jdim,kdim)
      double precision sndsp(jdim,kdim)

      double precision vflux(jdim,kdim,3), hfmu(jdim,kdim)       
      double precision a1(jdim,kdim), a2(jdim,kdim), a3(jdim,kdim)
      double precision a4(jdim,kdim)               

      double precision dmudq(maxj,maxk,4), dmu(maxj,maxk,3,4),
     &      dmup(maxj,maxk,3,4)

c     prlam   =  laminar prandtl number  = .72                      
c     prturb  =  turbulent prandtl number = .90                     
c     prlinv  =  1./(laminar prandtl number)                        
c     prtinv  =  1./(turbulent prandtl number)                      
c     f13     =  1/3                                                
c     f43     =  4/3                                                
c     hre     =  1/2 * reynolds number                           
c     fmu     =  laminar viscosity
c     turmu   =  turbulent viscosity      

      parameter ( prlam = .72d0 , prturb=.9d0 )                            
      parameter ( prlinv = 1.d0/prlam , prtinv = 1.d0/prturb )       
      parameter ( f43 = 4.d0/3.d0  , f13 = 1.d0/3.d0)             

      hre = 1.d0/(2.d0*re)                                 
      g1  = 1.d0/gami*prlinv
      g2  = gamma*gami

c     -- calculate alfas and non-conserv. variables --
      do k = kbegin,kend                                              
        do j = jlow,jup 
c     t1 = eta_x **2
c     t2 = eta_x*eta_y
c     t3 = eta_y **2          
          t1      = xy(j,k,3)*xy(j,k,3)                               
          t2      = xy(j,k,3)*xy(j,k,4)                               
          t3      = xy(j,k,4)*xy(j,k,4)                               
          r1      = hre/xyj(j,k)
                    
          a1(j,k) = r1*( f43*t1 + t3    ) 
          a2(j,k) = r1*(      t2*f13    ) 
          a3(j,k) = r1*( t1     + f43*t3)                        
          a4(j,k) = r1*( t1     + t3    )

          r1   = 1.d0/q(j,k,1)                                        
          q2(j,k,1) = r1                                           
          q2(j,k,2) = r1*q(j,k,2)                                  
          q2(j,k,3) = r1*q(j,k,3)                                  
          q2(j,k,4) = r1*q(j,k,4) - (q2(j,k,2)**2 +q2(j,k,3)**2)
        end do
      end do

c     -- calculate viscous flux vector without laminar viscosity --
      do k = kbegin,kup                                               
        kp = k+1  
        do j = jlow,jup                                               
          r1    = 1.d0/q(j,k,1)                                         
          rp1   = 1.d0/q(j,kp,1)                                       
          up1   = q(j,kp,2)*rp1                                     
          u1    = q(j,k,2)*r1                                      
          vp1   = q(j,kp,3)*rp1                                     
          v1    = q(j,k,3)*r1                                      
          ueta  = up1 - u1                                            
          veta  = vp1 - v1                                            
          c2eta = gamma*(press(j,kp)*rp1 - press(j,k)*r1)*g1

          ah1 = a1(j,kp) + a1(j,k)
          ah2 = a2(j,kp) + a2(j,k)
          ah3 = a3(j,kp) + a3(j,k)
          ah4 = a4(j,kp) + a4(j,k)

          vflux(j,k,1) = ah1*ueta + ah2*veta
          vflux(j,k,2) = ah2*ueta + ah3*veta
          vflux(j,k,3) = 0.5d0*(up1 + u1)*vflux(j,k,1) + 0.5d0*(vp1 +v1)
     &          *vflux(j,k,2) + ah4*c2eta

c     -- extrapolate fmu to get it at 1/2 grid pts --
          hfmu(j,k) = 0.5d0*(fmu(j,k)+fmu(j,kp))
        end do
      end do

c     -- compute d(mu)/dQ vector --
      c2b = 198.6/tinf                                                  
      c2bp = c2b + 1.
      do k = kbegin,kend
        do j = jlow,jup

          t1 = sndsp(j,k)*sndsp(j,k)
          t2 = t1 + 3.d0*c2b
          t3 = 1.d0/(t1 + c2b)
          dmuda = c2bp*t1*t2*t3*t3

          t1 = 0.5d0/sndsp(j,k)
          t2 = q(j,k,2)*q(j,k,2)
          t3 = q(j,k,3)*q(j,k,3)
          t4 = 1.d0/q(j,k,1)
          t5 = t4*t4
          dadq1 = g2*t5*( (t2+t3)*t4 - q(j,k,4) )
          dadq2 = - g2*q(j,k,2)*t5
          dadq3 = - g2*q(j,k,3)*t5
          dadq4 = g2*t4

          dmudq(j,k,1) = dmuda*dadq1*t1
          dmudq(j,k,2) = dmuda*dadq2*t1
          dmudq(j,k,3) = dmuda*dadq3*t1
          dmudq(j,k,4) = dmuda*dadq4*t1
        end do
      end do

c     -- compute derivative terms due to laminar viscosity --
      do k = kbegin,kup
        kp = k + 1
        do j = jlow,jup
          do n = 1,4
            dmu(j,k,1,n) = 0.5d0*vflux(j,k,1)*dmudq(j,k,n)
            dmu(j,k,2,n) = 0.5d0*vflux(j,k,2)*dmudq(j,k,n)
            dmu(j,k,3,n) = 0.5d0*vflux(j,k,3)*dmudq(j,k,n)

            dmup(j,k,1,n) = 0.5d0*vflux(j,k,1)*dmudq(j,kp,n)
            dmup(j,k,2,n) = 0.5d0*vflux(j,k,2)*dmudq(j,kp,n)
            dmup(j,k,3,n) = 0.5d0*vflux(j,k,3)*dmudq(j,kp,n)
          end do
        end do
      end do

c     -- entries for viscous Jacobian, treating mu as constant -- 
      do k = kbegin,kup
        kp = k + 1
        do j = jlow,jup

c     -- evaluate alfas at k+1/2 --
          ah1 = ( a1(j,k) + a1(j,kp) )*hfmu(j,k)
          ah2 = ( a2(j,k) + a2(j,kp) )*hfmu(j,k)
          ah3 = ( a3(j,k) + a3(j,kp) )*hfmu(j,k)
          ah4 = ( a4(j,k) + a4(j,kp) )*hfmu(j,k)*gamma*prlinv

          dj  (j,k,1,1) = -( ah1*q2(j,k,2)  + ah2*q2(j,k,3)) *q2(j,k,1)
          ddj (j,k,1,1) = -( ah1*q2(j,kp,2) + ah2*q2(j,kp,3))*q2(j,kp,1)
          dj  (j,k,1,2) = ah1*q2(j,k,1)                              
          ddj (j,k,1,2) = ah1*q2(j,kp,1)
          dj  (j,k,1,3) = ah2*q2(j,k,1)
          ddj (j,k,1,3) = ah2*q2(j,kp,1)
          dj  (j,k,1,4) = 0.d0
          ddj (j,k,1,4) = 0.d0
          dj  (j,k,2,1) = -( ah2*q2(j,k,2) + ah3*q2(j,k,3))  *q2(j,k,1)
          ddj (j,k,2,1) = -( ah2*q2(j,kp,2) + ah3*q2(j,kp,3))*q2(j,kp,1)
          dj  (j,k,2,2) = ah2*q2( j, k,1)                              
          ddj (j,k,2,2) = ah2*q2( j, kp,1)                            
          dj  (j,k,2,3) = ah3*q2( j, k,1)                              
          ddj (j,k,2,3) = ah3*q2( j, kp,1)                            
          dj  (j,k,2,4) = 0.d0                                          
          ddj (j,k,2,4) = 0.d0                                         
          dj  (j,k,3,1) = -(ah4*q2(j,k,4) + ah1*q2(j,k,2)**2 + 2.*ah2
     &          *q2(j,k,2)*q2(j,k,3) + ah3*q2(j,k,3)**2)*q2(j,k,1)
          ddj (j,k,3,1) = -(ah4*q2(j,kp,4) + ah1*q2(j,kp,2)**2 + 2.*ah2
     &          *q2(j,kp,2)*q2(j,kp,3) + ah3*q2(j,kp,3)**2)*q2(j,kp,1)
          dj  (j,k,3,2) = -ah4*q2(j,k,2)*q2(j,k,1) - dj(j,k,1,1)        
          ddj (j,k,3,2) = -ah4*q2(j,kp,2)*q2(j,kp,1) - ddj(j,k,1,1)    
          dj  (j,k,3,3) = -ah4*q2(j,k,3)*q2(j,k,1) - dj(j,k,2,1)        
          ddj (j,k,3,3) = -ah4*q2(j,kp,3)*q2(j,kp,1) - ddj(j,k,2,1)    
          dj  (j,k,3,4) = ah4*q2(j,k,1)                              
          ddj (j,k,3,4) = ah4*q2(j,kp,1)                            
        end do
      end do

c     -- preconditioner: differencing and storing --
      if ( prec_mat ) then 
        do n  = 2,4 
          do k = klow,kup
            do j = jlow,jup
              ij  = ( index(j,k) - 1 )*4 + n
              ii  = ( ij - 1 )*20
              do m = 1,4
                pa(ii+4+m)  = pa(ii+4+m)  - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m) 
                pa(ii+8+m)  = pa(ii+8+m)  + dj(j,k,n-1,m)
     &                + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m)
                pa(ii+12+m) = pa(ii+12+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)  
              end do
            end do
          end do
        end do
      end if

c     -- second order jacobian: differencing and storing --
c     -- interior points --
      if ( jac_mat ) then
        do j = jlow+1,jup-1
          do k = klow+1,kup-1
            do n  = 2,4 
              ij  = ( index(j,k) - 1 )*4 + n
              ii  = ( ij - 1 )*36
              do m =1,4
                as(ii+12+m) = as(ii+12+m) - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m) 
                as(ii+16+m) = as(ii+16+m) + dj(j,k,n-1,m)
     &                + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m) 
                as(ii+20+m) = as(ii+20+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)  
              end do
            end do
          end do
        end do

c     -- boundary nodes --
        j = jlow
        do k = klow+1,kup-1
          do n  = 2,4 
            ij  = ( index(j,k) - 1 )*4 + n
            ii  = ( ij - 1 )*36
            do m =1,4
              as(ii+8+m)  = as(ii+8+m)  - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m) 
              as(ii+12+m) = as(ii+12+m) + dj(j,k,n-1,m)
     &              + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m) 
              as(ii+16+m) = as(ii+16+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)   
            end do
          end do
        end do

        j = jup
        do k = klow+1,kup-1
          do n  = 2,4 
            ij  = ( index(j,k) - 1 )*4 + n
            ii  = ( ij - 1 )*36
            do m =1,4
              as(ii+12+m) = as(ii+12+m) - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m) 
              as(ii+16+m) = as(ii+16+m) + dj(j,k,n-1,m)
     &              + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m)
              as(ii+20+m) = as(ii+20+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)
            end do
          end do
        end do

        k = klow
        do j = jlow+1,jup-1
          do n  = 2,4 
            ij  = ( index(j,k) - 1 )*4 + n
            ii  = ( ij - 1 )*36
            do m =1,4
              as(ii+8+m)  = as(ii+8+m)  - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m)   
              as(ii+12+m) = as(ii+12+m) + dj(j,k,n-1,m)
     &              + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m) 
              as(ii+16+m) = as(ii+16+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)
            end do
          end do
        end do

        k = kup
        do j = jlow+1,jup-1
          do n  = 2,4 
            ij  = ( index(j,k) - 1 )*4 + n
            ii  = ( ij - 1 )*36
            do m =1,4
              as(ii+12+m) = as(ii+12+m) - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m)     
              as(ii+16+m) = as(ii+16+m) + dj(j,k,n-1,m)
     &              + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m)
              as(ii+20+m) = as(ii+20+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)
            end do
          end do
        end do

        j = jlow
        k = klow
        do n  = 2,4 
          ij  = ( index(j,k) - 1 )*4 + n
          ii  = ( ij - 1 )*36
          do m =1,4
            as(ii+4+m)  = as(ii+4+m)  - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m)      
            as(ii+8+m)  = as(ii+8+m)  + dj(j,k,n-1,m)
     &            + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m)
            as(ii+12+m) = as(ii+12+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)
          end do
        end do

        j = jlow
        k = kup
        do n  = 2,4 
          ij  = ( index(j,k) - 1 )*4 + n
          ii  = ( ij - 1 )*36
          do m =1,4
            as(ii+8+m)  = as(ii+8+m)  - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m)        
            as(ii+12+m) = as(ii+12+m) + dj(j,k,n-1,m)
     &            + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m)
            as(ii+16+m) = as(ii+16+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)
          end do
        end do

        j = jup
        k = klow
        do n  = 2,4 
          ij  = ( index(j,k) - 1 )*4 + n
          ii  = ( ij - 1 )*36
          do m =1,4
            as(ii+8+m)  = as(ii+8+m)  - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m)          
            as(ii+12+m) = as(ii+12+m) + dj(j,k,n-1,m)
     &            + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m)
            as(ii+16+m) = as(ii+16+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)
          end do
        end do

        j = jup
        k = kup
        do n  = 2,4 
          ij  = ( index(j,k) - 1 )*4 + n
          ii  = ( ij - 1 )*36
          do m =1,4
            as(ii+12+m) = as(ii+12+m) - dj(j,k-1,n-1,m) +
     &                dmu(j,k-1,n-1,m)  
            as(ii+16+m) = as(ii+16+m) + dj(j,k,n-1,m)
     &            + ddj(j,k-1,n-1,m)  - dmu(j,k,n-1,m)  +
     &                dmup(j,k-1,n-1,m)
            as(ii+20+m) = as(ii+20+m) - ddj(j,k,n-1,m)  -
     &                dmup(j,k,n-1,m)
          end do
        end do      
      end if

      return                                                            
      end                       !iveta_all
