      subroutine fd_drdq (nmax, q, xy, xyj,
     &     x, y, turmu, vort, turre, fmu, precon, press, sndsp, xit,
     &     ett, ds, vk, ve, coef2x, coef4x, coef2y, coef4y, s, rsa, uu,
     &     vv, ccx, ccy, spectxi, specteta, tmp, indx, ipf, jpf, pf,
     &     ntvar)

#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/mbcom.inc"

      dimension q(*),         press(*),     sndsp(*)
      dimension s(*),         xy(*),        xyj(*)
      dimension xit(*),       ett(*),       ds(*)
      dimension x(*),         y(*),         turmu(*)
      dimension vort(*),      turre(*),     fmu(*)
      dimension vk(*),        ve(*),        tmp(*)
      dimension uu(*),        vv(*),        ccx(*)
      dimension ccy(*),       coef2x(*),    coef2y(*)
      dimension coef4x(*),    coef4y(*),    precon(*)
      dimension spectxi(*),   specteta(*),  indx(*)
      dimension rsa(*)

c     -- finite difference jacobian --
      dimension ipf(*), jpf(*), pf(*)
      dimension iwa(maxjk*jacb+1), jwa(maxjk*jacb2*9), wa(maxjk*jacb2*9)

c     -- sp => perturbed rhs array --
      dimension sp(maxjk*4), cdrdq(maxjk*4), rsap(maxjk), dsadq(maxjk)

      iblk = 1
      jo = jminbnd(iblk) 
      ko = kminbnd(iblk)
      no = 1

      nz = 0
      ipf(1) = 1

 10   continue

c      write (*,*) 'going fd_dq',nmax
      call fd_dq( jo, ko, no, jmax(iblk), kmax(iblk), nmax,
     &     q(lqptr(iblk)), turre(lgptr(iblk)), xyj(lgptr(iblk)), tmpv,
     &     stepsize)
c      write (*,*) 'done fd_dq'

c     -- halo column copy for q vector --
      do ii1 = 1,nblks
         do is1=2,4,2
            ii2 = lblkpt(ii1,is1)
            if (ii2.ne.0) then
               is2=lsidept(ii1,is1)
               call halocp(ii1, nhalo, 4, is1, is2, jmax(ii1),
     &              kmax(ii1), q(lqptr(ii1)), jbegin(ii1)-nsd2(ii1),
     &              jend(ii1)+nsd4(ii1), kminbnd(ii1), kmaxbnd(ii1),
     &              jmax(ii2), kmax(ii2), q(lqptr(ii2)), jbegin(ii2)
     &              -nsd2(ii2), jend(ii2)+nsd4(ii2), kminbnd(ii2),
     &              kmaxbnd(ii2))
            endif
         end do
      end do

      if (nmax.eq.5) then
         do ib1 = 1,nblks
            do is1=2,4,2
               ib2 = lblkpt(ib1,is1)
               if (ib2.ne.0) then
                  is2=lsidept(ib1,is1)
                  call halocp(ib1, nhalo, 1, is1,is2, jmax(ib1),
     &                 kmax(ib1), turre(lgptr(ib1)),
     &                 jbegin(ib1)-nsd2(ib1), jend(ib1)+nsd4(ib1),
     &                 kminbnd(ib1), kmaxbnd(ib1), jmax(ib2), kmax(ib2),
     &                 turre(lgptr(ib2)), jbegin(ib2)-nsd2(ib2),
     &                 jend(ib2)+nsd4(ib2), kminbnd(ib2), kmaxbnd(ib2))
               endif
            end do
         end do
      end if

c     -- calculate pressure and sound speed --
c      write (*,*) 'going calcps'
      do ii=1,nblks
         lq = lqptr(ii)
         lg = lgptr(ii)
         call calcps( ii, jmax(ii), kmax(ii), q(lq), press(lg),
     &        sndsp(lg), precon(lprecptr(ii)), xy(lq), xyj(lg),
     &        jbegin(ii)-nsd2(ii), jend(ii)+nsd4(ii), 
c     &        kbegin(ii)-nsd1(ii), kend(ii)+nsd3(ii))
     &        kminbnd(ii), kmaxbnd(ii))
      end do
c      write (*,*) 'done calcps'
      
c      write (*,*) 'going rhs'
c     -- residual evaluation: -R(Q) stored in S --
      call get_rhs( q, press, sndsp,
     &     sp, xy, xyj, xit, ett, ds, x, y, turmu, vort, turre, fmu, vk,
     &     ve, tmp, uu, vv, ccx, ccy, coef2x , coef2y, coef4x, coef4y,
     &     precon, spectxi, specteta, rsap)
c      write (*,*) 'done rhs'

c     -- reset q vector --
      call fd_qr( jo, ko, no, jmax(iblk), kmax(iblk), nmax, 
     &     q(lqptr(iblk)), turre(lgptr(iblk)), tmpv)

c     -- evaluate drdq --
      dxx = 1.0/stepsize
      do ii = 1,nblks
         lq = lqptr(ii)
         lg = lgptr(ii)
         call fd_sdiff(nmax, kminbnd(ii), kmaxbnd(ii), jminbnd(ii),
     &        jmaxbnd(ii), jmax(ii), kmax(ii), cdrdq(lq), sp(lq), s(lq),
     &        dsadq(lg), rsap(lg), rsa(lg), dxx)
      end do

      ic = 0
      do ii = 1,nblks
c         write (*,*) 'calling fd_mat',ii,nmax
c         write (*,*) 
         call fd_mat( nz, ic, jo, ko, no, jminbnd(ii), jmaxbnd(ii),
     &        kminbnd(ii), kmaxbnd(ii), jmax(ii), kmax(ii), nmax,
     &        indx(lgptr(ii)), jmax(iblk), kmax(iblk),
     &        indx(lgptr(iblk)), cdrdq(lqptr(ii)), dsadq(lgptr(ii)),
     &        jpf, pf)
      end do
      write (*,100) ic, nz, iblk, jo,ko,no,maxjk*jacb2*9
 100  format (8i10)
c      write (*,*) 'ic',ic,nz,ntvar
      ipf(ic+1) = nz+1
c      write (*,*) 'done fd_mat 2'
c      stop
      if (no.lt.nmax) then
         no = no + 1
         goto 10
      else
         no = 1
         if ( ko.lt.kmaxbnd(iblk) ) then
            ko = ko + 1
            goto 10
         else
            ko = kminbnd(iblk)
            if ( jo.lt.jmaxbnd(iblk) ) then
               jo = jo + 1
               goto 10
            else
               if (iblk.lt.nblks) then
                  iblk = iblk + 1
                  jo = jminbnd(iblk)
                  goto 10
               end if
            end if
         end if
      end if
      
      call csrcsc (ntvar, 1, 1, pf, jpf, ipf, wa, jwa, iwa)
      call copmat (ntvar, wa, jwa, iwa, pf, jpf, ipf, 1, 1) 

      return
      end                       !fd_drdq
