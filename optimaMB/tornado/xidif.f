C ********************************************************************
C ******************    DIFFERENCE X, Y  IN XI    ********************
C ********************************************************************
c Note: for now backward difference at the boundaries 
c
      subroutine xidif(jdim,kdim,x,y,xy,                                
     &                 jbegin,jend,kbegin,kend)

      implicit none
c                                                                       
#include "../include/common.inc"

      integer jdim,kdim,jbegin,jend,kbegin,kend,j,k,n,jlow,jup,jp1,jm1
      double precision xy(jdim,kdim,4),tmp                               
      double precision x(jdim,kdim),y(jdim,kdim)                               
c                                                                       
c
cmt ~~~ I prefer to initialize the xy3,xy4 for each point including the corners (halo points)
cmt ~~~ so we can avoid having zeros or random numbers on the block corners
      DO n=3,4
         DO k=1,kdim
            DO j=1,jdim
               xy(j,k,n)=1.d0
            ENDDO
         ENDDO
      ENDDO
cmt
      jlow=jbegin+1
      jup=jend-1
c
c     -xy4 =xxi, xy3 = yxi
c
      if (iord.eq.4) then
        tmp=1.d0/12.d0
        do 6 k=kbegin,kend
          do 5 j=jbegin+2,jend-2   
            xy(j,k,4)= tmp*
     &            (-x(j+2,k)+8.d0*(x(j+1,k)-x(j-1,k))+x(j-2,k)) 
            xy(j,k,3)= tmp*
     &            (-y(j+2,k)+8.d0*(y(j+1,k)-y(j-1,k))+y(j-2,k))
c            write(99,777) j,k,xy(j,k,3),xy(j,k,4)
 5        continue
c                                                                          
          j = jbegin+1
c         -second order
c          xy(j,k,4) = (x(j+1,k) - x(j-1,k))*.5d0                       
c          xy(j,k,3) = (y(j+1,k) - y(j-1,k))*.5d0                       
c         - third order
          xy(j,k,4)=2.d0*tmp*(-2.d0*x(j-1,k) - 3.d0*x(j,k) + 
     &          6.d0*x(j+1,k) - x(j+2,k))        
          xy(j,k,3)=2.d0*tmp*(-2.d0*y(j-1,k) - 3.d0*y(j,k) + 
     &          6.d0*y(j+1,k) - y(j+2,k))
c          
          j = jend-1                                                    
c          -second order
c           xy(j,k,4) = (x(j+1,k) - x(j-1,k))*.5d0                       
c           xy(j,k,3) = (y(j+1,k) - y(j-1,k))*.5d0                       
c          - third order
          xy(j,k,4)=2.d0*tmp*(x(j-2,k) - 6.d0*x(j-1,k) +
     &          3.d0*x(j,k) + 2.d0*x(j+1,k))       
          xy(j,k,3)=2.d0*tmp*(y(j-2,k) - 6.d0*y(j-1,k) +
     &          3.d0*y(j,k) + 2.d0*y(j+1,k))      
c
c         -second order
c         j = jbegin                                                  
c         xy(j,k,4) = .5d0*( -3.d0*x(j,k) +4.d0*x(j+1,k) - x(j+2,k))   
c         xy(j,k,3) = .5d0*( -3.d0*y(j,k) +4.d0*y(j+1,k) - y(j+2,k))   
c         j = jend                                                    
c         xy(j,k,4) = ( 3.d0*x(j,k) -4.d0*x(j-1,k) + x(j-2,k))*.5d0   
c         xy(j,k,3) = ( 3.d0*y(j,k) -4.d0*y(j-1,k) + y(j-2,k))*.5d0 
c         -third order
c         
          j = jbegin                                                  
          xy(j,k,4)=2.d0*tmp*(-11.d0*x(j,k) + 18.d0*x(j+1,k) - 
     &          9.d0*x(j+2,k) + 2.d0*x(j+3,k))   
          xy(j,k,3)=2.d0*tmp*(-11.d0*y(j,k) + 18.d0*y(j+1,k) - 
     &          9.d0*y(j+2,k) + 2.d0*y(j+3,k))    
c         
          j = jend                                                    
          xy(j,k,4)=2.d0*tmp*(11.d0*x(j,k) - 18.d0*x(j-1,k) + 
     &          9.d0*x(j-2,k) - 2.d0*x(j-3,k))    
          xy(j,k,3)=2.d0*tmp*(11.d0*y(j,k) - 18.d0*y(j-1,k) + 
     &          9.d0*y(j-2,k) - 2.d0*y(j-3,k))
 6      continue
 777    format(2i5,2e20.8)
      else
        do 11 k=kbegin,kend                                
          do 10 j=jlow,jup                                 
            jp1 = j+1                                 
            jm1 = j-1                                 
            xy(j,k,4) = (x(jp1,k) - x(jm1,k))*.5            
            xy(j,k,3) = (y(jp1,k) - y(jm1,k))*.5             
 10       continue                                             
          j = jbegin                                           
          xy(j,k,4) = .5*( -3.*x(j,k) +4.*x(j+1,k) - x(j+2,k)) 
          xy(j,k,3) = .5*( -3.*y(j,k) +4.*y(j+1,k) - y(j+2,k)) 
          j = jend                                             
          xy(j,k,4) = ( 3.*x(j,k) -4.*x(j-1,k) + x(j-2,k))*.5  
          xy(j,k,3) = ( 3.*y(j,k) -4.*y(j-1,k) + y(j-2,k))*.5  
 11     continue                                                
      endif
c                                                             
      return                                                
      end                                                    
