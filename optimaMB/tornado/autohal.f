c *******************************************************************
c ** subroutine to automatically generate the number of halo       **
c ** points around each block for multi-block execution iff        **
c ** the file 'tornado.hal' does not exist.                        **
c *******************************************************************
c calling routine: ioall
c
c
c
      subroutine autohal

      implicit none
c
#include "../include/parms.inc"
#include "../include/index.inc"
#include "../include/common.inc"
#include "../include/mbcom.inc"

      integer nbs,iblock,iside,ii,jblock,nblock
c
c ** INITIALIZE 'jkhalo' ARRAY TO ZERO **
c
      if (mg .or. gseq) then
        nbs=nblkstot
      else
        nbs=nblks
      endif

      do 10 iblock=1,nbs
        do 10 iside=1,4
          jkhalo(iblock,iside) = 0
   10 continue
      if (psiavg) then
c
c ** ADD 1 ROW OF HALO POINTS ON SIDES 2 OR 4 FOR ALL BLOCKS **
c ** AND NO HALO POINTS ON SIDES 1 AND 3                     **
c
        do 50 iblock=1,nbs
          if (ibctype(iblock,2).eq.0) jkhalo(iblock,2) = 1
          if (ibctype(iblock,4).eq.0) jkhalo(iblock,4) = 1
   50   continue
      else
        nblock=0 ! S.D.R. I put this line in to avoid warning from alpha
c       Note: Stan D.R. Oct 5/00
c           -this branch of if statement written by Tom Nelson
c            I've never used this branch, use at own risk

c
c ** ADD 2 ROWS OF HALO POINTS ON SIDES 2 OR 4 FOR BLOCKS **
c ** CONTAINING STAGNATION POINTS IN THE FOLLOWING CASES  **
c
        do 100 ii=1,nstg
          iblock = 0
c
c ** Lower Right CORNER (TE):
          if (istgcnr(ii).eq.1) then
            if (istgtyp(ii).eq.-1) then
              jkhalo(istgblk(ii),4) = 2
c also do block below airfoil
              iblock = lblkpt(istgblk(ii),4)
              iblock = lblkpt(iblock,1)
              if (ibctype(iblock,2).eq.1) then
                iblock = lblkpt(iblock,1)
              endif
              iblock = lblkpt(iblock,2)
              jkhalo(iblock,4) = 2
            endif
c
c ** Upper Right CORNER (TE):
          elseif (istgcnr(ii).eq.4) then
            if (istgtyp(ii).eq.-1) then
              jkhalo(istgblk(ii),4) = 2
c also do block above airfoil
              iblock = lblkpt(istgblk(ii),4)
              iblock = lblkpt(iblock,3)
              if (ibctype(iblock,2).eq.1) then
                iblock = lblkpt(iblock,3)
              endif
              iblock = lblkpt(iblock,2)
              jkhalo(iblock,4) = 2
            endif
c
c ** Lower Left CORNER (LE):
          elseif (istgcnr(ii).eq.2) then
            if (istgtyp(ii).eq.1) then
              jkhalo(istgblk(ii),2) = 2
c also do block below airfoil
              iblock = lblkpt(istgblk(ii),2)
              iblock = lblkpt(iblock,1)
              iblock = lblkpt(iblock,4)
              jkhalo(iblock,2) = 2
            endif
c ** Lower Left CORNER (TE for C-Mesh):
            if (istgtyp(ii).eq.-1.and.nblock.le.4) then
              jkhalo(istgblk(ii),2) = 2
            endif
c
c ** Upper Left CORNER (LE):
          elseif (istgcnr(ii).eq.3) then
            if (istgtyp(ii).eq.1) then
              jkhalo(istgblk(ii),2) = 2
c also do block above airfoil
              iblock = lblkpt(istgblk(ii),2)
              iblock = lblkpt(iblock,3)
              iblock = lblkpt(iblock,4)
              jkhalo(iblock,2) = 2
            endif
c ** Upper Left CORNER (TE for C-Mesh):
            if (istgtyp(ii).eq.-1.and.nblock.le.4) then
              jkhalo(istgblk(ii),2) = 2
            endif
          endif
c
  100   continue
c
c ** NOW ENSURE THAT BLOCKS ABOVE AND BELOW BLOCKS **
c ** THAT HAVE HALO POINT ROWS ALSO HAVE THE SAME  **
c
        do 200 iblock=1,nbs
c
c blocks ABOVE for halo point rows on side 2
          if (jkhalo(iblock,2).eq.2) then
            jblock = iblock
  120       continue
            jblock=lblkpt(jblock,3)
            if (jblock.ne.0) then
              if (jkhalo(jblock,2).ne.2) jkhalo(jblock,2)=2
              goto 120
            endif
c blocks BELOW for halo point rows on side 2
            jblock = iblock
  130       continue
            jblock=lblkpt(jblock,1)
            if (jblock.ne.0) then
              if (jkhalo(jblock,2).ne.2) jkhalo(jblock,2)=2
              goto 130
            endif
          endif
c
c blocks ABOVE for halo point rows on side 4
          if (jkhalo(iblock,4).eq.2) then
            jblock = iblock
  140       continue
            jblock=lblkpt(jblock,3)
            if (jblock.ne.0) then
              if (jkhalo(jblock,4).ne.2) jkhalo(jblock,4)=2
              goto 140
            endif
c blocks BELOW for halo point rows on side 4
            jblock = iblock
  150       continue
            jblock=lblkpt(jblock,1)
            if (jblock.ne.0) then
              if (jkhalo(jblock,4).ne.2) jkhalo(jblock,4)=2
              goto 150
            endif
          endif
c
  200   continue
c
c ** SIDES 1 & 3 ARE TO HAVE 1 ROW OF HALO POINTS AS LONG **
c ** AS THAT SIDE IS NOT A WALL OR A FAR FIELD BOUNDARY   **
c
        do 300 iblock=1,nbs
          if (ibctype(iblock,1).eq.0) jkhalo(iblock,1)=1
          if (ibctype(iblock,3).eq.0) jkhalo(iblock,3)=1
  300   continue
      endif
c
c ** THAT'S IT !
c
      return
      end
