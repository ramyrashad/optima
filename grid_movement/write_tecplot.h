#ifndef TEC_H
#define TEC_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include "Node.h"
#include "Face.h"
#include "Vol.h"

using namespace std;

void write_tecplot (const vector <Node>& nodes, const vector <Face>& elems);

void write_tecplot (const vector <Node>& nodes, const vector <Vol>& elems);

#endif
