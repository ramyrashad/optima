      program bfoil
c----------------------------------------------------------------------
c     b-spline airfoil geometry definition
c     fit a b-spline curve through given data points
c
c     references:
c     'curves and surfaces for computer-aided geometric design', 
c     4th ed., g. farin, 1997.
c     
c     `mathematical elements for computer graphics', 2nd ed., rogers 
c     and adams, 1990.
c
c     'intrinsic parametrization for approximation', j. hoschek, 
c     computer aided geometric design, 1988 (5), pg 27.
c
c     program uses lapack subroutines for least-squares problem
c     use flag -lcomplib.sgimath to compile on sgi machines
c
c     written by: marian nemec 
c     date: feb. 2000, UTIAS
c     mods: aug. 2003, NASA Ames
c
c     number of control points must be odd
c     resulting b-spline control polygon is constructed in such a way
c     that the leading edge of the airfoil is fixed for cubic b-splines
c----------------------------------------------------------------------
      implicit none

      integer max_ord,jmax,nc_max,kt_max
c     -- max. number of knots -> kt_max = max_ord + nc_max --
      parameter (max_ord=20, jmax=4000, nc_max=80, kt_max=100)

      integer i,ii,j,nc,ndata,order,left,iter,MaxIter,jle,iknt
      integer imid,ikseq,jbrk(10),ncbrk,icw,ios
      integer idv(nc_max)

      double precision xbrk(10),ybrk(10),cwfac,yavg
      double precision xdata(jmax),ydata(jmax),t(jmax),totalarc,segment
      double precision BIatT(max_ord),Ncoef(jmax,nc_max),tmp,dkfac
      double precision x(jmax),y(jmax),knot(kt_max),xp(jmax),yp(jmax)
      double precision errorx(jmax),errory(jmax),le_of_int,le_of_poly
      double precision corr(jmax),NewDist,dist(jmax),tn,resid,rot
      double precision fac,tol,dy,dx,nc_half,dist1,dist2,rknt
      double precision dvs(nc_max)

      character*71 notes
      logical cont, tecplot, rep, cont2, rotate

c     -- variables for QR decomposition using LAPACK --
      integer info,lwork
      double precision work(jmax*nc_max), rhs(jmax,2)
      character trans

c     -- variables for userio --
      integer ivar(3), ndv, ne, idvi, idvf, iskp
      logical errf
     
      open (unit=11,file='airfoil.dat', iostat=ios, err=1000,
     &     status='old')
      open (unit=14,file='bfoil.inp', iostat=ios, err=1000,
     &     status='old')

 1000 continue
      if (ios.ne.0) then
         stop 'airfoil.dat or bfoil.inp not found!'
      endif

c     -- set variables for LAPACK routine dgels --
      lwork = jmax*nc_max
      trans = 'N'

c     -- read input file --
      read (14,5) notes
      read (14,*) nc
      read (14,5) notes
      read (14,*) order
      read (14,5) notes
      read (14,*) MaxIter
      read (14,5) notes
      read (14,*) tol
      read (14,5) notes
      read (14,*) tecplot
      read (14,5) notes
      read (14,*) ikseq
      read (14,5) notes
      read (14,*) dkfac
      read (14,5) notes
      read (14,*) icw
      read (14,5) notes
      read (14,*) ncbrk
      read (14,5) notes
      do i=1,ncbrk
         read (14,*) xbrk(i), ybrk(i)
      end do

c     -- ordering of data --
      if (icw.eq.0) then
c     -- clockwise --
         cwfac = 1.0
      else
c     -- counter clockwise --
         cwfac = -1.0
      end if

      nc = nc - 1
c     -- note: total number of control points is nc+1 --
c     -- this is in notation of de Boor, 1978 --
 5    format (a71)
      write (*,6) nc+1
      write (*,7) order
      write (*,8) lwork !tml temp
 6    format (/3x,'Number of control points:',i4)
 7    format (3x,'Order of B-spline basis:',i3)
 8    format (3x,'lwork:',i5) !tml temp

      if (mod(nc+1,2).eq.0) then
         write (*,*) 'Number of control points must be odd!'
         stop
      end if

c     -- read data points --
      ii = 0
      do i=1,9999
         read(11,*,end=10,err=10) xdata(i),ydata(i)
         ii = ii + 1
      end do
 10   continue
      if (i.gt.jmax) then
        write (*,*) 'Maximum data points exceeded, check jmax!!'
        stop
      end if
      write(*,*)
      ndata = ii

c     -- find leading edge of airfoil --
      dist1=0.0
      do j=2,ndata-1
         dist2 = sqrt((xdata(j)-xdata(1))**2 + (ydata(j)-ydata(1))**2)
         if (dist2.gt.dist1) then
            dist1 = dist2
            jle = j
         endif
      end do

c     -- set leading edge for break point --
      do i=1,ncbrk
         if ( abs(xbrk(i)-999.0).lt.1.e-14 .and. abs(ybrk(i)-999.0).lt.
     &        1.e-14 ) then
            xbrk(i) = xdata(jle)
            ybrk(i) = ydata(jle)
         end if
      end do
     
c     -- find orientation of chord line --
c     -- watch out for blunt T.E. --
      yavg = (ydata(1) + ydata(ndata))/2.0
      dx = xdata(1) - xdata(jle)
      dy = yavg - ydata(jle)

c     -- rotate airfoil to horizontal to compute bspline --
      if (abs(dy) .gt. 1.e-15) then
         rotate = .TRUE.
         rot = -atand(dy/dx)
         write (*,11) rot
 11      format ('Rotation angle: ',e16.8)

c     -- rotate data points, skip leading edge --
         do i=1,jle-1
            dx = xdata(i) - xdata(jle)
            dy = ydata(i) - ydata(jle)
            xdata(i)= xdata(jle) + dx*cosd(rot) - dy*sind(rot)
            ydata(i)= ydata(jle) + dx*sind(rot) + dy*cosd(rot)
         end do
         do i=jle+1,ndata
            dx = xdata(i) - xdata(jle)
            dy = ydata(i) - ydata(jle)
            xdata(i)= xdata(jle) + dx*cosd(rot) - dy*sind(rot)
            ydata(i)= ydata(jle) + dx*sind(rot) + dy*cosd(rot)
         end do
      end if

c     -- centripetal chord length parametrization --
c     -- to obtain chord length parametrization remove dsqrt --
c     -- from dsqrt(segment) --
      t(1) = 0.d0
      totalarc = 0.d0
      do i = 2, ndata 
        segment = dsqrt( (xdata(i)-xdata(i-1))**2 +
     &        (ydata(i)-ydata(i-1))**2 )
        totalarc = totalarc + dsqrt(segment) 
        t(i) = t(i-1) + dsqrt(segment)
      end do
c     -- parameter and knot vector are scaled by the same factor --
      do i = 2, ndata-1
c     t(i) = t(i)/totalarc*dble(nc-order+2)
         t(i) = t(i)/totalarc*dble(nc-order)
      end do
c     t(ndata) = dble(nc-order+2)
      t(ndata) = dble(nc-order)

Ctml temp
c     -- display parameter vector --
c      open (unit=12,file='param1b', status='new')
c      write (12,*) 'Parameter vector:'
c      write (12,17) ( t(i), i=1,ndata )
c      write (12,*)
ctml end temp

c     -- set knot vector --
      do i=1,order
        knot(i) = 0.d0
      end do

c     -- b-spline is fitted from the lower surface trailing edge, around
c     the leading edge, to the upper surface trailing edge --

c     -- nc_half: value of knot at the leading edge --
c     -- knot sequence is repeated at the leading edge to fix the 
c     leading edge --

c     nc_half = dble(nc-order+2)*0.5
      nc_half = dble(nc-order)*0.5
c     fac = 2.0/dble(nc-order+2-2)
      fac = 2.0/dble(nc-order)
      tmp = 0.0

c     -- set knot order for lower surface, loop stops just before
c     leading edge --
      write (*,12) ikseq
 12   format('Knot sequence, IKSEQ = ', i2/)
      
c     do i=1,(nc-order+2)/2
      do i=1,(nc-order)/2
        tmp = tmp + fac
        ii = i+order
        if (ikseq.eq.1) then
c     -- linear knot distribution --
           knot(ii) = i
        else if (ikseq.eq.2) then
c     -- knots are clustered at the trailing and leading edges --
           knot(ii) = nc_half*0.5d0*(1.0d0 - dcos(3.1415926536d0*tmp))
        else if (ikseq.eq.3) then
c     -- knots are only clustered at the leading edge --
           knot(ii) = nc_half*sin(3.1415926536/2.0*tmp)
        end if
      end do
c     -- imid is the location of the leading edge (or middle) knot --
c     -- repeat three knots around the leading edge --
      imid         = (nc-order)/2 + order + 1 
      knot(imid)   = knot(imid-1)
      knot(imid+1) = knot(imid-1)

c     -- when using the trig knot distribution, the knots may be
c     clustered too tightly around the leading or trailing edges. the
c     clustering is relaxed here --
      if (ikseq.eq.2) then
         write (*,14) dkfac
         knot(1+order) = dkfac*knot(1+order)
         do i=2,(nc-order)/2
            ii = i+order
            knot(ii) = knot(ii) + knot(1+order)*(1.0-knot(ii)/
     &           knot((nc-order)/2+order))
         end do        
      else if (ikseq.eq.3) then     
         write (*,14) dkfac
         tmp = knot(imid-1) - knot(imid-2)
         tmp = dkfac*tmp
         fac = knot(imid-2)
         do i = imid-2,1+order ,-1
            knot(i) = knot(i) - tmp*knot(i)/fac
         end do
      end if
 14   format(' MODIFYING KNOT SEQUENCE, DKFAC =',f6.2/)

c     -- copy lower surface knots to upper surface --
      ii = imid-2
      do i=imid+2,nc+1          
         knot(i) = nc_half + ( nc_half - knot(ii) )
         ii = ii - 1
      end do

      do i=nc+2,nc+order+1
c     knot(i) = dble(nc-order+2)
         knot(i) = dble(nc-order)
      end do

      write (*,*) 'Knot vector:'
      do i=1,nc+order+1
         write (*,16) i,knot(i)
      end do

      rep = .false.
      call askl('Modify knot vector?^',rep)
      if (rep) then
         cont2 = .true.
         do while (cont2)
            call aski('Enter knot index (999 to exit):^',iknt)
            if (iknt.eq.999) then
               cont2 = .false.
            else
               call askr('Enter knot value: ^',rknt)
               knot(iknt) = rknt
            end if
         end do
      end if

c     -- display knot vector --
      write (*,*)
      write (*,*) 'Final knot vector:'
      write (*,15) ( knot(i), i=1,nc+order+1 )
      write (*,*)
 15   format (8f8.4)
 16   format (i3,f8.4)
 17   format (5f12.8)

ctml temp
c      open (unit=13,file='knot1b', status='new')
c      write (13,15) ( knot(i), i=1,nc+order+1 )
ctml

c     -- start least squares approximation with parameter correction --
      resid = 1.d0
      iter = 0
      write (*,18)
      write (*,19)
 18   format (3x,'** Using QR decomposition to solve ')
 19   format (6x,'the Least-Squares fit. **',/)

 20   continue                  ! main least squares loop
      iter = iter + 1

ctml temp uncommented
c      write (*,21) iter
c 21   format (3x,'Iteration:',i4)
ctml temp uncommented end

c     -- form matrix of B-spline basis functions [ N ] --
c     -- work only on increasing sequence of the knot vector --
      do j=1,ndata
        do i=1,nc+1
          Ncoef(j,i) = 0.d0
        end do
      end do

      tmp = -1.d0
      do j=1,ndata
        do left=order,nc+1

          if ( dble(knot(left)).le.t(j) .and.
     &          t(j).le.dble(knot(left+1)) .and. t(j).ne.tmp ) then 
            tmp = t(j)
            call Bval(knot,order,t(j),left,BIatT,max_ord,kt_max)
            do i=1,order
              Ncoef(j,i+left-order) = BIatT(i)
            end do
          end if

        end do
        rhs(j,1) = xdata(j)
        rhs(j,2) = ydata(j)
      end do

ctml temp
c     -- display b-spline basis function matrix [ N ] --
c      if (iter .eq. 2) then
c         open (unit=13,file='n-matrix1b', status='new')
c         write (13,*) 'N-Matrix:'
c         do j=1,ndata
c            write (13,25) ( Ncoef(j,i), i=1,nc+1 ) 
c         end do
c      endif
c 25   format (6f8.4)
ctml temp end

c     -- solve for control points using QR decomposition (LAPACK) --
      call dgels(trans,ndata,nc+1,2,Ncoef,jmax,rhs,jmax,work, 
     &      lwork,info)
      if (info.eq.0) then
        if (dble(lwork).lt.work(1)) write (*,*) 'Increase lwork to:',
     &        work(1)
      else
        write (*,*)
        write (*,*) '*** Problems with QR decomposition. ***'
      end if

c     -- force T.E. of airfoil to avoid roundoff --
      rhs(1,1) = xdata(1)
      rhs(1,2) = ydata(1)
      rhs(nc+1,1) = xdata(ndata)
      rhs(nc+1,2) = ydata(ndata)

c     -- force knots at L.E. of airfoil --
      imid = (nc+1+1)/2
      rhs(imid,1)   = xdata(jle)
      rhs(imid-1,1) = rhs(imid,1)
      rhs(imid+1,1) = rhs(imid,1)      

      dist1 = abs( rhs(imid-1,2) - rhs(imid,2) )
      dist2 = abs( rhs(imid+1,2) - rhs(imid,2) )
      tmp = (dist1+dist2)/2.0
      rhs(imid,2) = ydata(jle)
      rhs(imid-1,2) = rhs(imid,2) - cwfac*tmp
      rhs(imid+1,2) = rhs(imid,2) + cwfac*tmp

c     -- evaluate b-spline given control points --
      call curve(knot,t,ndata,order,nc,x,y,rhs,max_ord,kt_max,jmax)

c     -- compute error for the approximation --
      call error(jmax,ndata,x,y,xdata,ydata,resid,errorx,errory,dist,
     &     .FALSE.)

c     -- check exit condition --
      if ( (resid.lt.tol).or.(iter.ge.MaxIter) ) goto 40

c     -- proceed with parameter correction --
c     -- find tangents --
      call tangents(knot,t,ndata,order,nc,x,y,xp,yp,rhs,max_ord,
     &      kt_max,jmax)

c     -- length of interval --
      le_of_int = knot(nc+2) - knot(1)

c     -- length of control polygon --
      le_of_poly = 0.d0
      do i=1,nc
        le_of_poly = le_of_poly + dsqrt( ( rhs(i+1,1) - rhs(i,1) )**2
     &        +( rhs(i+1,2) - rhs(i,2) )**2 )
      end do

c     -- parameter correction --
      do j=1,ndata
         corr(j) = ( errorx(j)*xp(j) + errory(j)*yp(j) )/dsqrt(
     &        xp(j)**2+ yp(j)**2 )*le_of_int/le_of_poly
      end do
      
c     -- update parameter array t(j) --
      tmp = -1.d0
      fac = 1.d0
c      do j=1,ndata
      do j=2,ndata-1
        cont = .true.          
        do while (cont)
          tn = t(j) + fac*corr(j) 

          do left=order,nc+1

            if ( dble(knot(left)).le.tn .and.
     &            tn.le.dble(knot(left+1)) .and. tn.ne.tmp ) then 
              tmp = tn
              call Bval(knot,order,tn,left,BIatT,max_ord,kt_max)

              x(j) = 0.d0
              y(j) = 0.d0
              do i=1,order
                x(j) = x(j) + rhs(i+left-order,1)*BIatT(i)
                y(j) = y(j) + rhs(i+left-order,2)*BIatT(i)
              end do

            end if

          end do
          
c     -- compute new distance between points: if new distance is --
c     -- smaller then update t(j), else halve the correction and --
c     -- try again --
          NewDist = dsqrt( (xdata(j)-x(j))**2 + (ydata(j)-y(j))**2 )
          if (NewDist.gt.dist(j)) then
            fac = fac*0.5d0
          else
            t(j) = tn
            cont = .false.
            fac = 1.d0
          end if
        end do
      end do

c     -- display parameter vector --
c     write (*,*) 'Parameter vector:'
c     write (*,17) ( t(i), i=1,ndata )

      goto 20                   !least-squares correction loop

c     -- done parameter correction --
 40   continue

      write (*,41) jle,xdata(jle),ydata(jle)
 41   format (/3x,'Original airfoil L.E. is at node',i4,' at location:',
     &      e12.4,',',e12.4) 
      write (*,42) x(jle),y(jle)
 42   format (3x,'L.E. of approximated airfoil is at',e12.4,',',e12.4/)

      write (*,*) 'B-spline control points:'
      write (*,200) (i, rhs(i,1), rhs(i,2), i=1, nc+1)
 200  format (i3,2e16.8)

      rep = .false.
      call askl('Modify control point?^',rep)
      if (rep) then
         cont2 = .true.
         do while (cont2)
            call aski('Enter CP index (999 to exit):^',iknt)
            if (iknt.eq.999) then
               cont2 = .false.
            else
               call askr('Enter x value: ^',rknt)
               rhs(iknt,1) = rknt
               call askr('Enter y value: ^',rknt)
               rhs(iknt,2) = rknt
            end if
         end do
      end if

c     -- generate final geometry --
      call curve(knot,t,ndata,order,nc,x,y,rhs,max_ord,kt_max,jmax)

      write (*,44) x(jle),y(jle)
 44   format (/3x,'L.E. coordinates:',e12.4,','e12.4/)

c     -- check leading edge 'curvature' --
      if ( (x(jle-1).le.x(jle)) .or. (x(jle+1).le.x(jle)) ) write (*,*)
     &      '***** Check leading edge of airfoil!! *****' 

      write (*,*) 'Final B-spline control points:'
      write (*,200) (i, rhs(i,1), rhs(i,2), i=1, nc+1)

c     -- compute final error of approximation and max. error --
      write (*,50)
 50   format (/3x,'Final error results:')
      call error(jmax,ndata,x,y,xdata,ydata,resid,errorx,errory,dist,
     &     .TRUE.)
      write (*,*)

c     -- find bspline index closest to break point --
      do i = 1,ncbrk
         dist1=1.0
         do j=2,ndata-1
            dist2 = sqrt((xdata(j)-xbrk(i))**2 + (ydata(j)-ybrk(i))**2)
            if (dist2.lt.dist1) then
               dist1 = dist2
               jbrk(i) = j
            endif
         end do
      end do

c     -- done with b-spline, select design variables --
      write (*,300)
 300  format (/1x,
     &      'Define control points to be used as design variables:',
     &      /1x,'Enter Control Point index OR', /1x,
     &      'Enter start end increment Control Point index OR', /1x,
     &      'Enter -1 to EXIT')

      ndv = 0
      ivar(1) = -1
      ivar(2) = -1
      ivar(3) = -1 

c     -- main loop for design variables --
 500  continue

      ndv = ndv + 1
      
      write (*,310)
 310  format (/1x,'Input design variable(s)   i>  ',$)
      ne = 3
      call readi( ne, ivar, errf)

      if (errf) then
        write (*,*) 'Your input is wrong => Try again!!'
        ndv = ndv - 1
        errf = .false.
        goto 500
      end if

c     -- exit condition --
      if (ivar(1).eq.-1) goto 1500 

c     -- ne holds the number of input integers --
      if (ne .eq. 1) then
c     -- one control point entered --
        idv(ndv) = ivar(1)
        dvs(ndv) = rhs(idv(ndv),2)
        ivar(1) = -1
        goto 500
        
      else if (ne .eq. 2) then
c     -- input doesn't make sense --
        write (*,*) 'You must enter increment => Try again!'
        ndv = ndv - 1
        goto 500

      else if (ne .eq. 3) then
c     -- triplet entered --
        idvi = ivar(1)
        idvf = ivar(2)
        iskp = ivar(3)

        cont = .true.
        do while (cont)
          idv(ndv) = idvi
          dvs(ndv) = rhs(idv(ndv),2)
          idvi = idvi + iskp
          ndv = ndv + 1
          if (idvi.gt.idvf) then
            ndv = ndv - 1
            cont = .false.
          end if
        end do
        ivar(1) = -1
        ivar(2) = -1
        ivar(3) = -1

        goto 500

      else
        write (*,*) 'ne value is wrong => program stopped'
        stop
      end if

 1500 continue 

      ndv = ndv - 1

      if (ndv.ne.0) then
        write (*,320) ndv
 320    format (/1x,'The selected',i4,' design variable(s) are:')
        do j = 1,ndv
          write (*,325) j, idv(j), dvs(j)
        end do
 325    format(2i4,e24.16)
      else
        write (*,330) 
 330    format (/1x,'No design variables selected.')
      end if

c     -- rotate control points to original configuration --
      if (rotate) then
         rot = -rot

         do i=1,nc+1
            if (i .ne. imid) then
               dx = rhs(i,1) - rhs(imid,1)
               dy = rhs(i,2) - rhs(imid,2)
               rhs(i,1) = rhs(imid,1) + dx*cosd(rot) - dy*sind(rot)
               rhs(i,2) = rhs(imid,2) + dx*sind(rot) + dy*cosd(rot)
            end if
         end do

c     -- rotate back data points, skip leading edge --
         do i=1,jle-1
            dx = xdata(i) - xdata(jle)
            dy = ydata(i) - ydata(jle)
            xdata(i)= xdata(jle) + dx*cosd(rot) - dy*sind(rot)
            ydata(i)= ydata(jle) + dx*sind(rot) + dy*cosd(rot)
         end do
         do i=jle+1,ndata
            dx = xdata(i) - xdata(jle)
            dy = ydata(i) - ydata(jle)
            xdata(i)= xdata(jle) + dx*cosd(rot) - dy*sind(rot)
            ydata(i)= ydata(jle) + dx*sind(rot) + dy*cosd(rot)
         end do

         write (*,*)
         write (*,*) 'Rotated B-spline control points:'
         write (*,200) (i, rhs(i,1), rhs(i,2), i=1, nc+1)
         write (*,*)

c     -- generate final geometry --
         call curve(knot,t,ndata,order,nc,x,y,rhs,max_ord,kt_max,jmax)
      end if

      if (tecplot) then
c     -- tecplot format --
        open (unit=12,file='bspline.tec')
        open (unit=13,file='error.tec')
        write (12,60)
        write (12,70)
        write (12,100) (xdata(i), ydata(i), i=1, ndata)
        write (12,80)
        write (12,100) (x(i), y(i), i=1, ndata)
        write (12,90)
        write (12,100) (rhs(i,1), rhs(i,2), i=1, nc+1)

 60     format ('VARIABLES= x, y')
 70     format ('ZONE T="Original Points"')
 80     format ('ZONE T="Fitted Points"')      
 90     format ('ZONE T="Control Points"')
 100    format (2e16.8)

        write (13,110)
        write (13,120) ( i, dist(i), i=1, ndata)
 110    format ('VARIABLES= "Airfoil Node", Error')
 120    format (i5,e16.8)
      end if

c     -- data output --
      open (unit=15,file='bspline.gnu')
      open (unit=16,file='cntlp.gnu')
      write (15,130)
      write (15,140) ( i, xdata(i), ydata(i), x(i), y(i), dist(i),
     &      i=1, ndata )
 130  format ('# Original points, Fitted points, Error')
 140  format (i4,5e16.8)

      write (16,150)
      write (16,160) ( rhs(i,1), rhs(i,2), i=1, nc+1 )
 150  format ('# Control points')
 160  format (2e16.8)

c     -- output bspline control file --
      open (unit=17,file='aeroSec.bsp')
      write (17,170) order
      write (17,170) nc+1
      write (17,170) ncbrk+1
      if ( ncbrk .gt. 0 ) then
         write (17,170) ( jbrk(j), j=1, ncbrk )
      end if
      write (17,170) ndata
      write (17,180) ( rhs(i,1), rhs(i,2), i=1, nc+1 )
      write (17,190) ( t(j), j=1, ndata )
      write (17,190) ( knot(j), j=1, order+nc+1 )
c     write (17,180) ( x(i), y(i), i=1, ndata)
      close (17)

c     -- output design variables --
      open (unit=17,file='aeroSec.dvs')
      write (17,*) ndv
      write (17,210) ( idv(j), dvs(j), j=1, ndv )
      close (17)

 170  format (i5)
 180  format (2e24.16)
 190  format (e24.16)
 210  format(i5,e24.16)

      end                       !bfoil

c-----------------------------------------------------------------------

c     -- calc. the value of all possibly nonzero basis functions --
c     -- based on: carl de boor, 'practical guide to splines' pg. 134 --
c     -- written by: marian nemec --
c     -- date: Feb. 16, 2000 --

      subroutine Bval(knot,order,t,left,BIatT,max_ord,kt_max)

      implicit none

      integer i, k, kp1, order, kt_max, left, max_ord
c     integer knot(kt_max)
      
      double precision knot(kt_max)
      double precision t, BIatT(order), tmp, deltaR(max_ord)
      double precision deltaL(max_ord), saved

      k = 1
      BIatT(1) = 1.d0
      if (k.ge.order) return

      do while (k.lt.order)
        kp1 = k+1
        deltaR(k) = knot(left+k) - t
        deltaL(k) = t - knot(left+1-k)
        saved = 0.d0

        do i = 1,k
          tmp = BIatT(i)/(deltaR(i) + deltaL(kp1-i))
          BIatT(i) = saved + deltaR(i)*tmp
          saved = deltaL(kp1-i)*tmp
        end do
        BIatT(kp1) = saved
        k = kp1
      end do

      return 
      end                       !Bval
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------

c     -- find tangents --
c     -- use o(2) centred-difference formula --
c     -- written by: marian nemec --

      subroutine tangents(knot,t,ndata,order,nc,x,y,xp,yp,rhs,max_ord,
     &      kt_max,jmax)

      implicit none

      integer j,ndata,order,nc,max_ord,kt_max,jmax
      
      double precision t(jmax),knot(kt_max),x(jmax),y(jmax),rhs(jmax,2)
      double precision xp(jmax),yp(jmax),stepsize,tnew(jmax)
      double precision xplus(jmax),yplus(jmax),xminus(jmax),yminus(jmax)

      stepsize = 1.d-10
      
      do j=1,ndata-1
        tnew(j) = t(j) + stepsize
      end do

      call curve(knot,tnew,ndata,order,nc,xplus,yplus,rhs,max_ord,
     &      kt_max,jmax)

      do j=2,ndata
        tnew(j) = t(j) - stepsize
      end do 

      call curve(knot,tnew,ndata,order,nc,xminus,yminus,rhs,max_ord,
     &      kt_max,jmax)

      do j=2,ndata-1
        xp(j) = ( xplus(j) - xminus(j) ) / (2.d0*stepsize)
        yp(j) = ( yplus(j) - yminus(j) ) / (2.d0*stepsize)
      end do

      xp(1) = ( xplus(1) - x(1) ) / stepsize
      yp(1) = ( yplus(1) - y(1) ) / stepsize

      xp(ndata) = ( x(ndata) - xminus(ndata) ) / stepsize
      yp(ndata) = ( y(ndata) - yminus(ndata) ) / stepsize

      return
      end                       !tangents
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------

c     -- given knots, parameter vector and control points find curve
c     coordinates x and y --
c     -- written by: marian nemec --

      subroutine curve(knot,t,ndata,order,nc,x,y,rhs,max_ord,
     &      kt_max,jmax)

      implicit none

      integer i,j,left,ndata,order,nc,max_ord,kt_max,jmax

      double precision t(jmax),knot(kt_max),x(jmax),y(jmax),rhs(jmax,2)
      double precision tmp,BIatT(max_ord)

      tmp = -1.d0
      do j=1,ndata
        do left=order,nc+1

          if ( dble(knot(left)).le.t(j) .and.
     &          t(j).le.dble(knot(left+1)) .and. t(j).ne.tmp ) then 
            tmp = t(j)
            call Bval(knot,order,t(j),left,BIatT,max_ord,kt_max)

            x(j) = 0.d0
            y(j) = 0.d0
            do i=1,order
              x(j) = x(j) + rhs(i+left-order,1)*BIatT(i)
              y(j) = y(j) + rhs(i+left-order,2)*BIatT(i)
            end do

          end if

        end do
      end do    

      return
      end                       !curve
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------

c     -- compute error of approximation and max. error --
c     -- written by: marian nemec --

      subroutine error(jmax,ndata,x,y,xdata,ydata,resid,errorx,
     &      errory,dist,werr)

      implicit none

      integer j,jmax,MaxNode,ndata

      double precision resid,MaxErr,errorx(jmax),errory(jmax)
      double precision dist(jmax),xdata(jmax),ydata(jmax)
      double precision x(jmax),y(jmax)

      logical werr

      resid = 0.d0
      MaxErr = 0.d0
      do j=1,ndata
        errorx(j) = xdata(j) - x(j)
        errory(j) = ydata(j) - y(j)
        dist(j) = dsqrt( errorx(j)**2 + errory(j)**2 ) 
        resid = resid + dist(j)
        if ( dist(j).gt.MaxErr ) then
          MaxErr = dist(j)
          MaxNode = j
        end if
      end do
      resid = resid/dble(ndata)

      if (werr) then
         write (*,15) resid
         write (*,20) MaxErr, MaxNode
      end if
 15   format (3x,'Average error of approximation:',e12.4)
 20   format (3x,'Max. error of approximation:',e12.4,1x,'at node',i4)

      return
      end                       !error
c-----------------------------------------------------------------------
