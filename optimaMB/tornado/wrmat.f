c     ------------------------------------------------------------------
c     -- display jacobian and rhs values --
c     -- m. nemec, sept. 2001 --
c     ------------------------------------------------------------------
      subroutine wrmat( jvn, kvn, jmax, kmax, nmax, indx, ipa, jpa, pa,
     &     s, rsa)

      implicit none

#include "../include/parms.inc"
#include "../include/common.inc"
#include "../include/units.inc"

      integer strlen,jvn, kvn, jmax, kmax, nmax
      integer i1,i2,i,k1,k2,namelen,k,n
      double precision  pa(*), s(jmax,kmax,4), rsa(jmax,kmax)
      integer ipa(*), jpa(*), indx(jmax,kmax)
      
      i1 = ( indx(jvn,kvn) - 1 )*nmax + 1
      i2 = ( indx(jvn,kvn) - 1 )*nmax + nmax

      write (*,*) 'node: ',jvn,kvn,i1,i2

      namelen =  strlen(output_file_prefix)
      filena = output_file_prefix
      filena(namelen+1:namelen+6) = '.node'
      open(unit=n_all,file=filena,status='unknown')
      do i=i1, i2
         write(n_all,200) i
         k1 = ipa(i)
         k2 = ipa(i+1)-1
         write (n_all,201) (jpa(k),pa(k),k=k1,k2)
      end do

      write (n_all,*)
      do n=1,4
         write (n_all,202) s(jvn,kvn,n)
      end do

      if (nmax.eq.5) write (n_all,202) rsa(jvn,kvn)
 
      close(n_all)

 200  format (1h ,34(1h-),' row',i5,1x,34(1h-),/
     *     3('  columns :     values   * ') )
c     -- xiiiiiihhhhhhdddddddddddd-*- --
 201  format(3(1h ,i6,5h   : ,D12.5,3h * ) ) 

 202  format(e14.6)

      return
      end                       !wrmat
