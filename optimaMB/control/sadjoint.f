c     ------------------------------------------------------------------
c     -- adjoint problem scaling for body and farfield b.c. -- 
c     -- m. nemec, oct. 2001 --
c     -- called from adjoint.f --
c     ------------------------------------------------------------------
      subroutine sadjoint( ib, is, jmax, kmax, nmax, ks, ke, js, je,
     &     bcf, indx, rhs, pa, as, ipa, ia)

      implicit none

#include "../include/parms.inc"

      integer ib,is,jmax,kmax,nmax,ks,ke,js,je,j,k,n,ii,jk,jpb,jpe
      integer indx(jmax,kmax), ipa(*), ia(*)
      double precision as(*),pa(*),bcf(maxjb,4,4,mxbfine)
      double precision rhs(jmax,kmax,4),dv

      if (is.eq.1) then
         
         k = ks
         do j = js, je
            do n = 1,4
               dv  = bcf(j,n,is,ib)
               jk  = ( indx(j,k) - 1 )*nmax + n

               rhs(j,k,n) = rhs(j,k,n)*dv

               jpb = ipa(jk)
               jpe = ipa(jk+1) - 1
               do ii = jpb,jpe
                  pa(ii) = pa(ii)*dv
               end do
               jpb = ia(jk)
               jpe = ia(jk+1) - 1
               do ii = jpb,jpe
                  as(ii) = as(ii)*dv
               end do
            end do
         end do

      else if (is.eq.2) then

         j = js
         do k = ks+1, ke-1
            do n = 1,4
               dv  = bcf(k,n,is,ib)
               jk  = ( indx(j,k) - 1 )*nmax + n

               rhs(j,k,n) = rhs(j,k,n)*dv

               jpb = ipa(jk)
               jpe = ipa(jk+1) - 1
               do ii = jpb,jpe
                  pa(ii) = pa(ii)*dv
               end do
               jpb = ia(jk)
               jpe = ia(jk+1) - 1
               do ii = jpb,jpe
                  as(ii) = as(ii)*dv
               end do
            end do
         end do
         
      else if (is.eq.3) then

         k = ke
         do j = js, je
            do n = 1,4
               dv  = bcf(j,n,is,ib)
               jk  = ( indx(j,k) - 1 )*nmax + n

               rhs(j,k,n) = rhs(j,k,n)*dv

               jpb = ipa(jk)
               jpe = ipa(jk+1) - 1
               do ii = jpb,jpe
                  pa(ii) = pa(ii)*dv
               end do
               jpb = ia(jk)
               jpe = ia(jk+1) - 1
               do ii = jpb,jpe
                  as(ii) = as(ii)*dv
               end do
            end do
         end do

      else if (is.eq.4) then

         j = je
         do k = ks+1, ke-1
            do n = 1,4
               dv  = bcf(k,n,is,ib)
               jk  = ( indx(j,k) - 1 )*nmax + n

               rhs(j,k,n) = rhs(j,k,n)*dv
               
               jpb = ipa(jk)
               jpe = ipa(jk+1) - 1
               do ii = jpb,jpe
                  pa(ii) = pa(ii)*dv
               end do
               jpb = ia(jk)
               jpe = ia(jk+1) - 1
               do ii = jpb,jpe
                  as(ii) = as(ii)*dv
               end do
            end do
         end do 

      end if      

      return
      end                       !sadjoint
