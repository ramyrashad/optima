#include <vector>
#include "comprow_double.h"
#include "ilupre_double.h"
#include "cg.h"
#include "Node.h"
#include "Face.h"
#include "gridfe.h"
#include "stopwatch.h"

#define moveTE 0

extern "C" {
  void movegridfe_(char *filena, const int& namelen, const int &jtail1, 
		   const int& jtail2, double* x, double* y, const double* by,
		   double* dx, double* dy, const int& incr,
		   const int& update_dxy, const double& gptol, const int& gpmaxit);
}

void movegridfe_(char *filena, const int& namelen, const int &jtail1, 
		 const int& jtail2, double* x, double* y, const double* by,
		 double* dx, double* dy, const int& incr,
		 const int& update_dxy, const double& gptol, const int& gpmaxit)
{

  cout << "\nElasticity grid perturbation" << endl;

  int i, j, n;   // counter
  double temp;

  char *filename = new char[namelen+1];
  for (j=0; j<namelen; j++)
    filename[j] = filena[j];
  filename[namelen] = '\0';

  // declare and initialize objects
  vector <Node> node_loc;
  vector <Face> faces;
  int moving_bnd_nodes (jtail2-jtail1+1);	
  vector <Node*> bnd_nodes(moving_bnd_nodes);     

  if (moveTE)
    read_gridTE (filename, node_loc, moving_bnd_nodes, bnd_nodes, 
		 jtail1, jtail2, faces);
  else 
    read_grid (filename, node_loc, moving_bnd_nodes, bnd_nodes, 
		 jtail1, jtail2, faces);

  // set new y-coordinates
  // for (j=0; j<moving_bnd_nodes; j++)
  //  bnd_nodes[j]->set_new_y(by[j]);

  double *xyz;
  double *deltay = new double [moving_bnd_nodes];
  temp = 1.0/incr;
  for (j=0; j<moving_bnd_nodes; j++)
    {
      xyz = bnd_nodes[j]->get_xyz();
      deltay[j] = temp*(by[j]-xyz[1]);
    }

  // VARIABLES IN SOLVING MATRIX
  // counts the number of nonzeros in the stiffness matrix 
  // and number of interior nodes	
  int num_inn_nodes(0);
  int nonzeros = 2;  // nonzeros is set to 2 to indicate the 
                     // degree of freedom of the problem
                     // it will be used/changed to store the number
                     // of nonzeros in the global stiffness matrix 
                     // in the function get_nonzerosfe(...)
  
  // CONJUGATE GRADIENT METHOD CONVERGENCE PARAMETERS
  
  get_nonzerosfe (node_loc, num_inn_nodes, nonzeros);

  double *stiff_val = new double [nonzeros];
  init_array(stiff_val, nonzeros);
  //for (j=0; j<nonzeros; j++)
  //stiff_val[j] = 0.0;
  int *colind = new int [nonzeros];
  int *rowptr = new int [num_inn_nodes+1];

  j = 2;   // number of degrees of freedom, dimension of problem
  // construct the stiffness matrix 
  construct_stiffness_matrix (colind, rowptr, node_loc, j);
  //CompRow_Mat_double alpha (num_inn_nodes, num_inn_nodes, nonzeros, 
  //		    stiff_val, rowptr, colind);

  VECTOR_double du(num_inn_nodes, 0.0), F(num_inn_nodes, 0.0);     
  
  // initial element quality
  double* elemQo = new double[faces.size()];
  // initial mesh quality
  double Qomax;  
  get_ini_quality (faces, node_loc, elemQo, Qomax);

  for (j=0; j<incr; j++) 
    {
      temp = 0.0;
      for (i=0; i<moving_bnd_nodes; i++)
	bnd_nodes[i]->deform(temp, deltay[i]);
      if (moveTE)  // form the cubic polynomial for the TE
	form_TE_bnd(node_loc, jtail1, jtail2);
	  	  
      //cout <<"done form_TE and incr is " << incr << endl;
      if (j==0)
	{
	  // temp stores the mesh quality at the given iteration
	  // no need to calculate the quality on the 1st iteration
	  temp = -1;
	  //cout << "before form_stiff" << endl;
	  //form_stiff (alpha, node_loc, faces, F, elemQo, temp);
	  form_stiff (rowptr, colind, stiff_val, node_loc, 
		      faces, F, elemQo, temp);
	  //cout << "done form_stiff first time " << endl;
	}
      else
	{
	  // initial the global stiffness matrix alpha and 
	  // displacement + force vectors
	  //ini_stiff (alpha, node_loc, faces);
	  init_array(stiff_val, nonzeros);
	    /* for (i=0; i<num_inn_nodes; i++)
	    {
	      du[i] = 0.0;
	      F[i] = 0.0;
	      }*/
	  init_array (du, num_inn_nodes);
	  init_array (F, num_inn_nodes);
	  temp = 0.0;
	  //form_stiff (alpha, node_loc, faces, F, elemQo, temp);
	  form_stiff (rowptr, colind, stiff_val, node_loc, 
		      faces, F, elemQo, temp);

//	  cout << "Normalized measure of element quality is " << temp/Qomax
//	       << endl;
	}
      
      n = (int) node_loc.size();
      // Warm-start du (Chad Oldfield)
      for (i=0; i<n; i++)
        if (!node_loc[i].if_surf())
        {
          du[node_loc[i].get_ind() ] = dx[i+j*n];
          du[node_loc[i].get_ind()+1] = dy[i+j*n];
        }

      CompRow_Mat_double alpha (num_inn_nodes, num_inn_nodes, nonzeros, 
			    stiff_val, rowptr, colind);
      
      CompRow_ILUPreconditioner_double P(alpha);     // ILU preconditioner
      temp = gptol;	  
      n = gpmaxit;
      cout << "calling CG " << endl;
      i = CG(alpha, du, F, P, n, temp);  
  
      if (i > 0)
	cerr << "WARNING: final grid was not achieved " 
	     << "within specified tolerance! " << endl;

      if (update_dxy == 1)
        update_xyz (node_loc, du, dx, dy, j);
      else
        update_xyz (node_loc, du);
//      cout << "tolerance achieved " << temp 
//	   << "  Num. iter " << nonzeros << endl; 
    }      

  // write grid to a plot3D format
  //write_plot3D (filename, node_loc);

  // write grid back to optima2D
  write_grid (node_loc, x, y);

  delete [] stiff_val;    stiff_val = NULL;
  delete [] colind;       colind = NULL;
  delete [] rowptr;       rowptr = NULL;
  delete [] filename;     filename = NULL;
  delete [] deltay;       deltay = NULL;
  delete [] elemQo;       elemQo = NULL;

  xyz = NULL;
  // free memories
  i = (int) node_loc.size();
  for (j=0; j<i; j++)
    node_loc[j].clearmem();
  i = (int) faces.size();
  for (j=0; j<i; j++)
    faces[j].clearmem();
  	
  return;
}

