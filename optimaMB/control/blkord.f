c     ------------------------------------------------------------------
c     -- natural node order per block --
c     -- m. nemec, july 2001 --
c     ------------------------------------------------------------------
      subroutine blkord (lastblk, jmax, kmax, indx, jb, je, idir, kb,
     &     ke, nhalo)

      implicit none
      integer lastblk, jmax, kmax, jb, je, idir, kb, ke, nhalo
      integer jc,kc,j,k,kbmax
      integer indx(jmax,kmax)

      kbmax = kmax-2*nhalo

      jc = 0
      kc = 0

      do j = jb,je,idir
         jc = jc+1
         do k = kb,ke
            kc = kc+1
            indx(j,k) = (jc-1)*kbmax + kc + lastblk
         end do
         kc = 0
      end do
      lastblk = indx(je,ke)

      return
      end                       ! blkord
